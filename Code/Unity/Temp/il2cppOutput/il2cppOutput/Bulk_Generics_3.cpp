﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2998460458;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3966474481;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t259346668;
// System.ArgumentNullException
struct ArgumentNullException_t3857807348;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t2351962793;
// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t1138939824;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1809826521;
// System.Collections.Generic.KeyNotFoundException
struct KeyNotFoundException_t4143119173;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t2335218917;
// System.ArgumentException
struct ArgumentException_t1812645948;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t1842647776;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t2416567665;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct Dictionary_2_t2330366688;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>[]
struct KeyValuePair_2U5BU5D_t3558549595;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>>
struct IEnumerator_1_t470846054;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct KeyCollection_t1174554006;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct ValueCollection_t1748473895;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<BMGlyph/Kerning>
struct DefaultComparer_t2814965600;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineClearShot/Pair>
struct DefaultComparer_t705587384;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCollider/CompiledCurbFeeler>
struct DefaultComparer_t4077126285;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCore/UpdateStatus>
struct DefaultComparer_t1992322633;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct DefaultComparer_t1497241569;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>
struct DefaultComparer_t1587381731;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t2591787268;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct DefaultComparer_t748816070;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t2527459328;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t124933572;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t4275621449;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t3140156738;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>
struct DefaultComparer_t1395652423;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t4016490495;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t3433315258;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t2025547845;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>
struct DefaultComparer_t2652825765;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t694070604;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>
struct DefaultComparer_t454640904;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t1301135589;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2979502892;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>
struct DefaultComparer_t3405523699;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.ParticleCollisionEvent>
struct DefaultComparer_t620881928;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Playables.Playable>
struct DefaultComparer_t1168914024;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
struct DefaultComparer_t239768801;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct DefaultComparer_t2034722193;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct DefaultComparer_t3866642327;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>
struct DefaultComparer_t4139373424;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct DefaultComparer_t640796519;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>
struct DefaultComparer_t3864900772;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>
struct DefaultComparer_t1486492014;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>
struct DefaultComparer_t2224300771;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>
struct DefaultComparer_t3026552126;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>
struct DefaultComparer_t2791901073;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>
struct DefaultComparer_t670542555;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>
struct DefaultComparer_t1457051623;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>
struct DefaultComparer_t3591882732;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>
struct DefaultComparer_t3574947625;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>
struct DefaultComparer_t2708723802;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>
struct DefaultComparer_t3363706239;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>
struct DefaultComparer_t2993942467;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>
struct DefaultComparer_t2288526179;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t394533152;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t4063660965;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3810042532;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t2496102905;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t4154522382;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t308921856;
// System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>
struct EqualityComparer_1_t917514390;
// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>
struct EqualityComparer_1_t3103103470;
// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>
struct EqualityComparer_1_t2179675075;
// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>
struct EqualityComparer_1_t94871423;
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t3894757655;
// System.Collections.Generic.EqualityComparer`1<System.Char>
struct EqualityComparer_1_t3984897817;
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct EqualityComparer_1_t694336058;
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct EqualityComparer_1_t3146332156;
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t630008118;
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct EqualityComparer_1_t2522449658;
// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct EqualityComparer_1_t2378170239;
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t1242705528;
// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct EqualityComparer_1_t3793168509;
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t2119039285;
// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct EqualityComparer_1_t1535864048;
// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct EqualityComparer_1_t128096635;
// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t755374555;
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t3091586690;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>
struct EqualityComparer_1_t2852156990;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>
struct EqualityComparer_1_t3698651675;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
struct EqualityComparer_1_t1082051682;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>
struct EqualityComparer_1_t1508072489;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>
struct EqualityComparer_1_t3018398014;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>
struct EqualityComparer_1_t3566430110;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>
struct EqualityComparer_1_t2637284887;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct EqualityComparer_1_t137270983;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct EqualityComparer_1_t1969191117;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>
struct EqualityComparer_1_t2241922214;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct EqualityComparer_1_t3038312605;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>
struct EqualityComparer_1_t1967449562;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct EqualityComparer_1_t3884008100;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>
struct EqualityComparer_1_t326849561;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>
struct EqualityComparer_1_t1129100916;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>
struct EqualityComparer_1_t894449863;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>
struct EqualityComparer_1_t3068058641;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>
struct EqualityComparer_1_t3854567709;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>
struct EqualityComparer_1_t1694431522;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>
struct EqualityComparer_1_t1677496415;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>
struct EqualityComparer_1_t811272592;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>
struct EqualityComparer_1_t1466255029;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>
struct EqualityComparer_1_t1096491257;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>
struct EqualityComparer_1_t391074969;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct EqualityComparer_1_t2792049238;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct EqualityComparer_1_t2166209755;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
struct EqualityComparer_1_t1912591322;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>
struct EqualityComparer_1_t598651695;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>
struct EqualityComparer_1_t2257071172;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>
struct EqualityComparer_1_t2706437942;
// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t3580239271;
// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t1177713515;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3030196201;
// System.Object[]
struct ObjectU5BU5D_t2737604620;
// UnityEngine.Timeline.TimelinePlayable/ConnectionCache[]
struct ConnectionCacheU5BU5D_t1280499476;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache,System.Collections.DictionaryEntry>
struct Transform_1_t4224324972;
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t73912674;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t3017738170;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Collections.ArrayList
struct ArrayList_t4250946984;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t4119529693;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.Sprite
struct Sprite_t2544745708;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4215975066;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// System.Void
struct Void_t653366341;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t3528993905;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Reflection.MemberFilter
struct MemberFilter_t2357198184;
// System.String[]
struct StringU5BU5D_t2511808107;
// UnityEngine.UI.Selectable
struct Selectable_t1782068360;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;

extern RuntimeClass* ArgumentNullException_t3857807348_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral18330253;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m2568078342_MetadataUsageId;
extern RuntimeClass* DictionaryEntryU5BU5D_t2824327403_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m2819969725_MetadataUsageId;
extern RuntimeClass* KeyNotFoundException_t4143119173_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_get_Item_m232732215_MetadataUsageId;
extern const uint32_t Dictionary_2_set_Item_m4236924855_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2666580753;
extern const uint32_t Dictionary_2_Init_m4287122603_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t1965588061_il2cpp_TypeInfo_var;
extern RuntimeClass* LinkU5BU5D_t3030196201_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m3698297277_MetadataUsageId;
extern RuntimeClass* ArgumentException_t1812645948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1848165604;
extern Il2CppCodeGenString* _stringLiteral829727757;
extern Il2CppCodeGenString* _stringLiteral1407751802;
extern Il2CppCodeGenString* _stringLiteral164315354;
extern const uint32_t Dictionary_2_CopyToCheck_m1268130962_MetadataUsageId;
extern RuntimeClass* Hashtable_t2354558714_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m2355567942_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral693071875;
extern const uint32_t Dictionary_2_Add_m1261102796_MetadataUsageId;
extern const uint32_t Dictionary_2_ContainsKey_m3215165030_MetadataUsageId;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2619799764;
extern Il2CppCodeGenString* _stringLiteral195080435;
extern Il2CppCodeGenString* _stringLiteral2031386274;
extern Il2CppCodeGenString* _stringLiteral1195014714;
extern const uint32_t Dictionary_2_OnDeserialization_m4240004054_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern RuntimeClass* TextEditOp_t2367146867_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Remove_m2323936049_MetadataUsageId;
extern const uint32_t Dictionary_2_TryGetValue_m2856795776_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3610996681;
extern const uint32_t Dictionary_2_ToTKey_m1342912345_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2721451384;
extern const uint32_t Dictionary_2_ToTValue_m2208033321_MetadataUsageId;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m4254931569_MetadataUsageId;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m2345618998_MetadataUsageId;
extern const uint32_t Dictionary_2_get_Item_m1232224721_MetadataUsageId;
extern const uint32_t Dictionary_2_set_Item_m1372169477_MetadataUsageId;
extern const uint32_t Dictionary_2_Init_m336683669_MetadataUsageId;
extern const uint32_t Dictionary_2_InitArrays_m210013102_MetadataUsageId;
extern const uint32_t Dictionary_2_CopyToCheck_m95678206_MetadataUsageId;
extern const uint32_t Dictionary_2_Resize_m3049133820_MetadataUsageId;
extern const uint32_t Dictionary_2_Add_m2534200906_MetadataUsageId;
extern const uint32_t Dictionary_2_ContainsKey_m3562032535_MetadataUsageId;
extern const uint32_t Dictionary_2_OnDeserialization_m3452922474_MetadataUsageId;
extern RuntimeClass* ConnectionCache_t1699053097_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Remove_m3052265532_MetadataUsageId;
extern const uint32_t Dictionary_2_TryGetValue_m1227816124_MetadataUsageId;
extern const uint32_t Dictionary_2_ToTKey_m72071518_MetadataUsageId;
extern const uint32_t Dictionary_2_ToTValue_m3105797160_MetadataUsageId;
extern const RuntimeType* GenericEqualityComparer_1_t310718524_0_0_0_var;
extern RuntimeClass* TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3460897571_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m826911158_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1261087480_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1563994124_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1362601712_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2102658363_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m619178934_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2625545329_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2648746884_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4271347042_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2310194830_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m400248887_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2770816535_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m646046064_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m439758934_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3412048117_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2695851466_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2018202085_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m72992021_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3352180286_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3038343834_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1027209859_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1946045186_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4102383611_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m686459453_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1155318220_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2316346491_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m189220968_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1246168296_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1712828565_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3334382730_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m47459148_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1176802744_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2505764369_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1795385373_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3433650550_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4198411519_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3623065037_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3844703269_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3875088020_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2391794083_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m330497758_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3389135030_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2991137691_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1510462659_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3471529348_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1060605086_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1968421493_MetadataUsageId;

struct KeyValuePair_2U5BU5D_t2351962793;
struct DictionaryEntryU5BU5D_t2824327403;
struct Int32U5BU5D_t1965588061;
struct LinkU5BU5D_t3030196201;
struct ObjectU5BU5D_t2737604620;
struct TextEditOpU5BU5D_t73912674;
struct KeyValuePair_2U5BU5D_t3558549595;
struct ConnectionCacheU5BU5D_t1280499476;
struct TypeU5BU5D_t1985992169;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EQUALITYCOMPARER_1_T2241922214_H
#define EQUALITYCOMPARER_1_T2241922214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>
struct  EqualityComparer_1_t2241922214  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2241922214_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2241922214 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2241922214_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2241922214 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2241922214 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2241922214 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2241922214_H
#ifndef EQUALITYCOMPARER_1_T137270983_H
#define EQUALITYCOMPARER_1_T137270983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct  EqualityComparer_1_t137270983  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t137270983_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t137270983 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t137270983_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t137270983 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t137270983 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t137270983 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T137270983_H
#ifndef EQUALITYCOMPARER_1_T3566430110_H
#define EQUALITYCOMPARER_1_T3566430110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>
struct  EqualityComparer_1_t3566430110  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3566430110_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3566430110 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3566430110_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3566430110 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3566430110 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3566430110 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3566430110_H
#ifndef EQUALITYCOMPARER_1_T3018398014_H
#define EQUALITYCOMPARER_1_T3018398014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>
struct  EqualityComparer_1_t3018398014  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3018398014_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3018398014 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3018398014_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3018398014 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3018398014 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3018398014 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3018398014_H
#ifndef EQUALITYCOMPARER_1_T1508072489_H
#define EQUALITYCOMPARER_1_T1508072489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>
struct  EqualityComparer_1_t1508072489  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1508072489_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1508072489 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1508072489_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1508072489 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1508072489 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1508072489 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1508072489_H
#ifndef EQUALITYCOMPARER_1_T3698651675_H
#define EQUALITYCOMPARER_1_T3698651675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>
struct  EqualityComparer_1_t3698651675  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3698651675_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3698651675 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3698651675_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3698651675 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3698651675 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3698651675 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3698651675_H
#ifndef EQUALITYCOMPARER_1_T2852156990_H
#define EQUALITYCOMPARER_1_T2852156990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>
struct  EqualityComparer_1_t2852156990  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2852156990_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2852156990 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2852156990_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2852156990 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2852156990 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2852156990 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2852156990_H
#ifndef EQUALITYCOMPARER_1_T3091586690_H
#define EQUALITYCOMPARER_1_T3091586690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct  EqualityComparer_1_t3091586690  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3091586690_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3091586690 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3091586690_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3091586690 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3091586690 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3091586690 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3091586690_H
#ifndef EQUALITYCOMPARER_1_T755374555_H
#define EQUALITYCOMPARER_1_T755374555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Single>
struct  EqualityComparer_1_t755374555  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t755374555_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t755374555 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t755374555_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t755374555 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t755374555 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t755374555 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T755374555_H
#ifndef EQUALITYCOMPARER_1_T128096635_H
#define EQUALITYCOMPARER_1_T128096635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct  EqualityComparer_1_t128096635  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t128096635_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t128096635 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t128096635_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t128096635 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t128096635 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t128096635 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T128096635_H
#ifndef EQUALITYCOMPARER_1_T1535864048_H
#define EQUALITYCOMPARER_1_T1535864048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct  EqualityComparer_1_t1535864048  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1535864048_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1535864048 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1535864048_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1535864048 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1535864048 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1535864048 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1535864048_H
#ifndef EQUALITYCOMPARER_1_T3793168509_H
#define EQUALITYCOMPARER_1_T3793168509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct  EqualityComparer_1_t3793168509  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3793168509_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3793168509 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3793168509_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3793168509 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3793168509 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3793168509 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3793168509_H
#ifndef EQUALITYCOMPARER_1_T1242705528_H
#define EQUALITYCOMPARER_1_T1242705528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct  EqualityComparer_1_t1242705528  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1242705528_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1242705528 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1242705528_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1242705528 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1242705528 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1242705528 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1242705528_H
#ifndef EQUALITYCOMPARER_1_T2378170239_H
#define EQUALITYCOMPARER_1_T2378170239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct  EqualityComparer_1_t2378170239  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2378170239_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2378170239 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2378170239_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2378170239 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2378170239 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2378170239 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2378170239_H
#ifndef EQUALITYCOMPARER_1_T2522449658_H
#define EQUALITYCOMPARER_1_T2522449658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct  EqualityComparer_1_t2522449658  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2522449658_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2522449658 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2522449658_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2522449658 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2522449658 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2522449658 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2522449658_H
#ifndef EQUALITYCOMPARER_1_T630008118_H
#define EQUALITYCOMPARER_1_T630008118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct  EqualityComparer_1_t630008118  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t630008118_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t630008118 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t630008118_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t630008118 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t630008118 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t630008118 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T630008118_H
#ifndef EQUALITYCOMPARER_1_T3146332156_H
#define EQUALITYCOMPARER_1_T3146332156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct  EqualityComparer_1_t3146332156  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3146332156_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3146332156 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3146332156_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3146332156 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3146332156 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3146332156 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3146332156_H
#ifndef EQUALITYCOMPARER_1_T694336058_H
#define EQUALITYCOMPARER_1_T694336058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  EqualityComparer_1_t694336058  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t694336058_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t694336058 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t694336058_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t694336058 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t694336058 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t694336058 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T694336058_H
#ifndef EQUALITYCOMPARER_1_T3984897817_H
#define EQUALITYCOMPARER_1_T3984897817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Char>
struct  EqualityComparer_1_t3984897817  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3984897817_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3984897817 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3984897817_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3984897817 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3984897817 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3984897817 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3984897817_H
#ifndef EQUALITYCOMPARER_1_T3038312605_H
#define EQUALITYCOMPARER_1_T3038312605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct  EqualityComparer_1_t3038312605  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3038312605_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3038312605 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3038312605_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3038312605 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3038312605 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3038312605 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3038312605_H
#ifndef EQUALITYCOMPARER_1_T1967449562_H
#define EQUALITYCOMPARER_1_T1967449562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>
struct  EqualityComparer_1_t1967449562  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1967449562_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1967449562 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1967449562_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1967449562 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1967449562 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1967449562 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1967449562_H
#ifndef EQUALITYCOMPARER_1_T3884008100_H
#define EQUALITYCOMPARER_1_T3884008100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct  EqualityComparer_1_t3884008100  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3884008100_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3884008100 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3884008100_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3884008100 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3884008100 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3884008100 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3884008100_H
#ifndef EQUALITYCOMPARER_1_T326849561_H
#define EQUALITYCOMPARER_1_T326849561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>
struct  EqualityComparer_1_t326849561  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t326849561_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t326849561 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t326849561_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t326849561 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t326849561 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t326849561 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T326849561_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef COMPARER_1_T103882021_H
#define COMPARER_1_T103882021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct  Comparer_1_t103882021  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t103882021_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t103882021 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t103882021_StaticFields, ____default_0)); }
	inline Comparer_1_t103882021 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t103882021 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t103882021 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T103882021_H
#ifndef COMPARER_1_T2506407777_H
#define COMPARER_1_T2506407777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTime>
struct  Comparer_1_t2506407777  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t2506407777_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t2506407777 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t2506407777_StaticFields, ____default_0)); }
	inline Comparer_1_t2506407777 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t2506407777 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t2506407777 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T2506407777_H
#ifndef EQUALITYCOMPARER_1_T2706437942_H
#define EQUALITYCOMPARER_1_T2706437942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>
struct  EqualityComparer_1_t2706437942  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2706437942_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2706437942 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2706437942_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2706437942 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2706437942 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2706437942 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2706437942_H
#ifndef EQUALITYCOMPARER_1_T2257071172_H
#define EQUALITYCOMPARER_1_T2257071172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>
struct  EqualityComparer_1_t2257071172  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2257071172_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2257071172 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2257071172_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2257071172 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2257071172 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2257071172 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2257071172_H
#ifndef EQUALITYCOMPARER_1_T598651695_H
#define EQUALITYCOMPARER_1_T598651695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>
struct  EqualityComparer_1_t598651695  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t598651695_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t598651695 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t598651695_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t598651695 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t598651695 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t598651695 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T598651695_H
#ifndef EQUALITYCOMPARER_1_T1912591322_H
#define EQUALITYCOMPARER_1_T1912591322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
struct  EqualityComparer_1_t1912591322  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1912591322_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1912591322 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1912591322_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1912591322 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1912591322 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1912591322 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1912591322_H
#ifndef EQUALITYCOMPARER_1_T2166209755_H
#define EQUALITYCOMPARER_1_T2166209755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct  EqualityComparer_1_t2166209755  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2166209755_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2166209755 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2166209755_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2166209755 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2166209755 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2166209755 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2166209755_H
#ifndef EQUALITYCOMPARER_1_T3894757655_H
#define EQUALITYCOMPARER_1_T3894757655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct  EqualityComparer_1_t3894757655  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3894757655_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3894757655 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3894757655_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3894757655 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3894757655 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3894757655 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3894757655_H
#ifndef EQUALITYCOMPARER_1_T2792049238_H
#define EQUALITYCOMPARER_1_T2792049238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct  EqualityComparer_1_t2792049238  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2792049238_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2792049238 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2792049238_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2792049238 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2792049238 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2792049238 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2792049238_H
#ifndef EQUALITYCOMPARER_1_T1096491257_H
#define EQUALITYCOMPARER_1_T1096491257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>
struct  EqualityComparer_1_t1096491257  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1096491257_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1096491257 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1096491257_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1096491257 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1096491257 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1096491257 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1096491257_H
#ifndef EQUALITYCOMPARER_1_T1466255029_H
#define EQUALITYCOMPARER_1_T1466255029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>
struct  EqualityComparer_1_t1466255029  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1466255029_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1466255029 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1466255029_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1466255029 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1466255029 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1466255029 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1466255029_H
#ifndef EQUALITYCOMPARER_1_T811272592_H
#define EQUALITYCOMPARER_1_T811272592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>
struct  EqualityComparer_1_t811272592  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t811272592_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t811272592 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t811272592_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t811272592 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t811272592 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t811272592 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T811272592_H
#ifndef EQUALITYCOMPARER_1_T1677496415_H
#define EQUALITYCOMPARER_1_T1677496415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>
struct  EqualityComparer_1_t1677496415  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1677496415_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1677496415 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1677496415_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1677496415 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1677496415 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1677496415 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1677496415_H
#ifndef EQUALITYCOMPARER_1_T1694431522_H
#define EQUALITYCOMPARER_1_T1694431522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>
struct  EqualityComparer_1_t1694431522  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1694431522_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1694431522 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1694431522_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1694431522 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1694431522 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1694431522 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1694431522_H
#ifndef EQUALITYCOMPARER_1_T3854567709_H
#define EQUALITYCOMPARER_1_T3854567709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>
struct  EqualityComparer_1_t3854567709  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3854567709_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3854567709 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3854567709_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3854567709 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3854567709 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3854567709 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3854567709_H
#ifndef EQUALITYCOMPARER_1_T3068058641_H
#define EQUALITYCOMPARER_1_T3068058641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>
struct  EqualityComparer_1_t3068058641  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3068058641_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3068058641 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3068058641_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3068058641 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3068058641 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3068058641 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3068058641_H
#ifndef EQUALITYCOMPARER_1_T894449863_H
#define EQUALITYCOMPARER_1_T894449863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>
struct  EqualityComparer_1_t894449863  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t894449863_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t894449863 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t894449863_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t894449863 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t894449863 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t894449863 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T894449863_H
#ifndef EQUALITYCOMPARER_1_T1129100916_H
#define EQUALITYCOMPARER_1_T1129100916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>
struct  EqualityComparer_1_t1129100916  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1129100916_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1129100916 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1129100916_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1129100916 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1129100916 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1129100916 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1129100916_H
#ifndef EQUALITYCOMPARER_1_T391074969_H
#define EQUALITYCOMPARER_1_T391074969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>
struct  EqualityComparer_1_t391074969  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t391074969_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t391074969 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t391074969_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t391074969 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t391074969 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t391074969 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T391074969_H
#ifndef EQUALITYCOMPARER_1_T94871423_H
#define EQUALITYCOMPARER_1_T94871423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>
struct  EqualityComparer_1_t94871423  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t94871423_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t94871423 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t94871423_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t94871423 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t94871423 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t94871423 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T94871423_H
#ifndef EQUALITYCOMPARER_1_T1082051682_H
#define EQUALITYCOMPARER_1_T1082051682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
struct  EqualityComparer_1_t1082051682  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1082051682_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1082051682 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1082051682_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1082051682 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1082051682 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1082051682 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1082051682_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef DICTIONARY_2_T2330366688_H
#define DICTIONARY_2_T2330366688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  Dictionary_2_t2330366688  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t1965588061* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3030196201* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2737604620* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ConnectionCacheU5BU5D_t1280499476* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t259346668 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___table_4)); }
	inline Int32U5BU5D_t1965588061* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t1965588061** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t1965588061* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___linkSlots_5)); }
	inline LinkU5BU5D_t3030196201* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3030196201** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3030196201* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___keySlots_6)); }
	inline ObjectU5BU5D_t2737604620* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2737604620** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2737604620* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___valueSlots_7)); }
	inline ConnectionCacheU5BU5D_t1280499476* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ConnectionCacheU5BU5D_t1280499476** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ConnectionCacheU5BU5D_t1280499476* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___serialization_info_13)); }
	inline SerializationInfo_t259346668 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t259346668 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t259346668 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2330366688_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4224324972 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2330366688_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4224324972 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4224324972 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4224324972 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2330366688_H
#ifndef VALUECOLLECTION_T2416567665_H
#define VALUECOLLECTION_T2416567665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  ValueCollection_t2416567665  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t2998460458 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t2416567665, ___dictionary_0)); }
	inline Dictionary_2_t2998460458 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2998460458 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2998460458 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T2416567665_H
#ifndef KEYCOLLECTION_T1842647776_H
#define KEYCOLLECTION_T1842647776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  KeyCollection_t1842647776  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_t2998460458 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(KeyCollection_t1842647776, ___dictionary_0)); }
	inline Dictionary_2_t2998460458 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2998460458 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2998460458 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTION_T1842647776_H
#ifndef DICTIONARY_2_T2998460458_H
#define DICTIONARY_2_T2998460458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  Dictionary_2_t2998460458  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t1965588061* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3030196201* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2737604620* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TextEditOpU5BU5D_t73912674* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t259346668 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___table_4)); }
	inline Int32U5BU5D_t1965588061* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t1965588061** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t1965588061* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___linkSlots_5)); }
	inline LinkU5BU5D_t3030196201* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3030196201** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3030196201* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___keySlots_6)); }
	inline ObjectU5BU5D_t2737604620* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2737604620** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2737604620* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___valueSlots_7)); }
	inline TextEditOpU5BU5D_t73912674* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TextEditOpU5BU5D_t73912674** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TextEditOpU5BU5D_t73912674* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___serialization_info_13)); }
	inline SerializationInfo_t259346668 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t259346668 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t259346668 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2998460458_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3017738170 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2998460458_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3017738170 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3017738170 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3017738170 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2998460458_H
#ifndef EQUALITYCOMPARER_1_T1969191117_H
#define EQUALITYCOMPARER_1_T1969191117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  EqualityComparer_1_t1969191117  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1969191117_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1969191117 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1969191117_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1969191117 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1969191117 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1969191117 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1969191117_H
#ifndef KEYCOLLECTION_T1174554006_H
#define KEYCOLLECTION_T1174554006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  KeyCollection_t1174554006  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_t2330366688 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(KeyCollection_t1174554006, ___dictionary_0)); }
	inline Dictionary_2_t2330366688 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2330366688 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2330366688 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTION_T1174554006_H
#ifndef VALUECOLLECTION_T1748473895_H
#define VALUECOLLECTION_T1748473895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  ValueCollection_t1748473895  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t2330366688 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t1748473895, ___dictionary_0)); }
	inline Dictionary_2_t2330366688 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2330366688 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2330366688 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T1748473895_H
#ifndef SERIALIZATIONINFO_T259346668_H
#define SERIALIZATIONINFO_T259346668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t259346668  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t2354558714 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t4250946984 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___serialized_0)); }
	inline Hashtable_t2354558714 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t2354558714 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t2354558714 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___values_1)); }
	inline ArrayList_t4250946984 * get_values_1() const { return ___values_1; }
	inline ArrayList_t4250946984 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t4250946984 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T259346668_H
#ifndef EQUALITYCOMPARER_1_T917514390_H
#define EQUALITYCOMPARER_1_T917514390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>
struct  EqualityComparer_1_t917514390  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t917514390_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t917514390 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t917514390_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t917514390 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t917514390 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t917514390 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T917514390_H
#ifndef EQUALITYCOMPARER_1_T2119039285_H
#define EQUALITYCOMPARER_1_T2119039285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t2119039285  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2119039285_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2119039285 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2119039285_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2119039285 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2119039285 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2119039285 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2119039285_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EQUALITYCOMPARER_1_T2637284887_H
#define EQUALITYCOMPARER_1_T2637284887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>
struct  EqualityComparer_1_t2637284887  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2637284887_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2637284887 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2637284887_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2637284887 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2637284887 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2637284887 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2637284887_H
#ifndef EQUALITYCOMPARER_1_T2179675075_H
#define EQUALITYCOMPARER_1_T2179675075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>
struct  EqualityComparer_1_t2179675075  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2179675075_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2179675075 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2179675075_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2179675075 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2179675075 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2179675075 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2179675075_H
#ifndef EQUALITYCOMPARER_1_T3103103470_H
#define EQUALITYCOMPARER_1_T3103103470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>
struct  EqualityComparer_1_t3103103470  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3103103470_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3103103470 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3103103470_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3103103470 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3103103470 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3103103470 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3103103470_H
#ifndef DEFAULTCOMPARER_T4063660965_H
#define DEFAULTCOMPARER_T4063660965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct  DefaultComparer_t4063660965  : public EqualityComparer_1_t2166209755
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4063660965_H
#ifndef DEFAULTCOMPARER_T2288526179_H
#define DEFAULTCOMPARER_T2288526179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>
struct  DefaultComparer_t2288526179  : public EqualityComparer_1_t391074969
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2288526179_H
#ifndef DEFAULTCOMPARER_T640796519_H
#define DEFAULTCOMPARER_T640796519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct  DefaultComparer_t640796519  : public EqualityComparer_1_t3038312605
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T640796519_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef DEFAULTCOMPARER_T4139373424_H
#define DEFAULTCOMPARER_T4139373424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>
struct  DefaultComparer_t4139373424  : public EqualityComparer_1_t2241922214
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4139373424_H
#ifndef DEFAULTCOMPARER_T3866642327_H
#define DEFAULTCOMPARER_T3866642327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  DefaultComparer_t3866642327  : public EqualityComparer_1_t1969191117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3866642327_H
#ifndef DEFAULTCOMPARER_T308921856_H
#define DEFAULTCOMPARER_T308921856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>
struct  DefaultComparer_t308921856  : public EqualityComparer_1_t2706437942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T308921856_H
#ifndef DEFAULTCOMPARER_T2708723802_H
#define DEFAULTCOMPARER_T2708723802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>
struct  DefaultComparer_t2708723802  : public EqualityComparer_1_t811272592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2708723802_H
#ifndef DEFAULTCOMPARER_T2034722193_H
#define DEFAULTCOMPARER_T2034722193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct  DefaultComparer_t2034722193  : public EqualityComparer_1_t137270983
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2034722193_H
#ifndef DEFAULTCOMPARER_T239768801_H
#define DEFAULTCOMPARER_T239768801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
struct  DefaultComparer_t239768801  : public EqualityComparer_1_t2637284887
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T239768801_H
#ifndef VECTOR4_T2436299922_H
#define VECTOR4_T2436299922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2436299922 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2436299922_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2436299922  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2436299922  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2436299922  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2436299922  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2436299922  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2436299922 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2436299922  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___oneVector_6)); }
	inline Vector4_t2436299922  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2436299922 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2436299922  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2436299922  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2436299922 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2436299922  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2436299922  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2436299922 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2436299922  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2436299922_H
#ifndef DEFAULTCOMPARER_T2993942467_H
#define DEFAULTCOMPARER_T2993942467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>
struct  DefaultComparer_t2993942467  : public EqualityComparer_1_t1096491257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2993942467_H
#ifndef DEFAULTCOMPARER_T1168914024_H
#define DEFAULTCOMPARER_T1168914024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Playables.Playable>
struct  DefaultComparer_t1168914024  : public EqualityComparer_1_t3566430110
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1168914024_H
#ifndef GENERICCOMPARER_1_T3580239271_H
#define GENERICCOMPARER_1_T3580239271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct  GenericComparer_1_t3580239271  : public Comparer_1_t2506407777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T3580239271_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef DEFAULTCOMPARER_T4154522382_H
#define DEFAULTCOMPARER_T4154522382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct  DefaultComparer_t4154522382  : public EqualityComparer_1_t2257071172
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4154522382_H
#ifndef DEFAULTCOMPARER_T3574947625_H
#define DEFAULTCOMPARER_T3574947625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>
struct  DefaultComparer_t3574947625  : public EqualityComparer_1_t1677496415
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3574947625_H
#ifndef DEFAULTCOMPARER_T3864900772_H
#define DEFAULTCOMPARER_T3864900772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>
struct  DefaultComparer_t3864900772  : public EqualityComparer_1_t1967449562
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3864900772_H
#ifndef DEFAULTCOMPARER_T670542555_H
#define DEFAULTCOMPARER_T670542555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>
struct  DefaultComparer_t670542555  : public EqualityComparer_1_t3068058641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T670542555_H
#ifndef UILINEINFO_T1896071735_H
#define UILINEINFO_T1896071735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t1896071735 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t1896071735, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t1896071735, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t1896071735, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t1896071735, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T1896071735_H
#ifndef DEFAULTCOMPARER_T3810042532_H
#define DEFAULTCOMPARER_T3810042532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct  DefaultComparer_t3810042532  : public EqualityComparer_1_t1912591322
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3810042532_H
#ifndef DEFAULTCOMPARER_T2791901073_H
#define DEFAULTCOMPARER_T2791901073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>
struct  DefaultComparer_t2791901073  : public EqualityComparer_1_t894449863
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2791901073_H
#ifndef LINK_T1037336632_H
#define LINK_T1037336632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Link
struct  Link_t1037336632 
{
public:
	// System.Int32 System.Collections.Generic.Link::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.Link::Next
	int32_t ___Next_1;

public:
	inline static int32_t get_offset_of_HashCode_0() { return static_cast<int32_t>(offsetof(Link_t1037336632, ___HashCode_0)); }
	inline int32_t get_HashCode_0() const { return ___HashCode_0; }
	inline int32_t* get_address_of_HashCode_0() { return &___HashCode_0; }
	inline void set_HashCode_0(int32_t value)
	{
		___HashCode_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Link_t1037336632, ___Next_1)); }
	inline int32_t get_Next_1() const { return ___Next_1; }
	inline int32_t* get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(int32_t value)
	{
		___Next_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T1037336632_H
#ifndef DEFAULTCOMPARER_T394533152_H
#define DEFAULTCOMPARER_T394533152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct  DefaultComparer_t394533152  : public EqualityComparer_1_t2792049238
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T394533152_H
#ifndef DEFAULTCOMPARER_T3026552126_H
#define DEFAULTCOMPARER_T3026552126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>
struct  DefaultComparer_t3026552126  : public EqualityComparer_1_t1129100916
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3026552126_H
#ifndef DEFAULTCOMPARER_T2496102905_H
#define DEFAULTCOMPARER_T2496102905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>
struct  DefaultComparer_t2496102905  : public EqualityComparer_1_t598651695
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2496102905_H
#ifndef DEFAULTCOMPARER_T2224300771_H
#define DEFAULTCOMPARER_T2224300771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>
struct  DefaultComparer_t2224300771  : public EqualityComparer_1_t326849561
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2224300771_H
#ifndef DEFAULTCOMPARER_T3591882732_H
#define DEFAULTCOMPARER_T3591882732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>
struct  DefaultComparer_t3591882732  : public EqualityComparer_1_t1694431522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3591882732_H
#ifndef SPRITESTATE_T120936949_H
#define SPRITESTATE_T120936949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t120936949 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t2544745708 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t2544745708 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t2544745708 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t120936949, ___m_HighlightedSprite_0)); }
	inline Sprite_t2544745708 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t2544745708 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t2544745708 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t120936949, ___m_PressedSprite_1)); }
	inline Sprite_t2544745708 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t2544745708 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t2544745708 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t120936949, ___m_DisabledSprite_2)); }
	inline Sprite_t2544745708 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t2544745708 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t2544745708 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t120936949_marshaled_pinvoke
{
	Sprite_t2544745708 * ___m_HighlightedSprite_0;
	Sprite_t2544745708 * ___m_PressedSprite_1;
	Sprite_t2544745708 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t120936949_marshaled_com
{
	Sprite_t2544745708 * ___m_HighlightedSprite_0;
	Sprite_t2544745708 * ___m_PressedSprite_1;
	Sprite_t2544745708 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T120936949_H
#ifndef DEFAULTCOMPARER_T1486492014_H
#define DEFAULTCOMPARER_T1486492014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>
struct  DefaultComparer_t1486492014  : public EqualityComparer_1_t3884008100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1486492014_H
#ifndef DEFAULTCOMPARER_T1992322633_H
#define DEFAULTCOMPARER_T1992322633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCore/UpdateStatus>
struct  DefaultComparer_t1992322633  : public EqualityComparer_1_t94871423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1992322633_H
#ifndef DICTIONARYENTRY_T2438591966_H
#define DICTIONARYENTRY_T2438591966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t2438591966 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t2438591966, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t2438591966, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t2438591966_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t2438591966_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T2438591966_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef DEFAULTCOMPARER_T1457051623_H
#define DEFAULTCOMPARER_T1457051623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>
struct  DefaultComparer_t1457051623  : public EqualityComparer_1_t3854567709
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1457051623_H
#ifndef DEFAULTCOMPARER_T620881928_H
#define DEFAULTCOMPARER_T620881928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.ParticleCollisionEvent>
struct  DefaultComparer_t620881928  : public EqualityComparer_1_t3018398014
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T620881928_H
#ifndef GENERICCOMPARER_1_T1177713515_H
#define GENERICCOMPARER_1_T1177713515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct  GenericComparer_1_t1177713515  : public Comparer_1_t103882021
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T1177713515_H
#ifndef DEFAULTCOMPARER_T3363706239_H
#define DEFAULTCOMPARER_T3363706239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>
struct  DefaultComparer_t3363706239  : public EqualityComparer_1_t1466255029
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3363706239_H
#ifndef DEFAULTCOMPARER_T2814965600_H
#define DEFAULTCOMPARER_T2814965600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<BMGlyph/Kerning>
struct  DefaultComparer_t2814965600  : public EqualityComparer_1_t917514390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2814965600_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t4215975066 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t4215975066 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t4215975066 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t4215975066 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DEFAULTCOMPARER_T4275621449_H
#define DEFAULTCOMPARER_T4275621449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct  DefaultComparer_t4275621449  : public EqualityComparer_1_t2378170239
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4275621449_H
#ifndef KERNING_T647376370_H
#define KERNING_T647376370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMGlyph/Kerning
struct  Kerning_t647376370 
{
public:
	// System.Int32 BMGlyph/Kerning::previousChar
	int32_t ___previousChar_0;
	// System.Int32 BMGlyph/Kerning::amount
	int32_t ___amount_1;

public:
	inline static int32_t get_offset_of_previousChar_0() { return static_cast<int32_t>(offsetof(Kerning_t647376370, ___previousChar_0)); }
	inline int32_t get_previousChar_0() const { return ___previousChar_0; }
	inline int32_t* get_address_of_previousChar_0() { return &___previousChar_0; }
	inline void set_previousChar_0(int32_t value)
	{
		___previousChar_0 = value;
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(Kerning_t647376370, ___amount_1)); }
	inline int32_t get_amount_1() const { return ___amount_1; }
	inline int32_t* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(int32_t value)
	{
		___amount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNING_T647376370_H
#ifndef DEFAULTCOMPARER_T124933572_H
#define DEFAULTCOMPARER_T124933572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct  DefaultComparer_t124933572  : public EqualityComparer_1_t2522449658
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T124933572_H
#ifndef DEFAULTCOMPARER_T705587384_H
#define DEFAULTCOMPARER_T705587384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineClearShot/Pair>
struct  DefaultComparer_t705587384  : public EqualityComparer_1_t3103103470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T705587384_H
#ifndef DEFAULTCOMPARER_T1395652423_H
#define DEFAULTCOMPARER_T1395652423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>
struct  DefaultComparer_t1395652423  : public EqualityComparer_1_t3793168509
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1395652423_H
#ifndef DEFAULTCOMPARER_T2527459328_H
#define DEFAULTCOMPARER_T2527459328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct  DefaultComparer_t2527459328  : public EqualityComparer_1_t630008118
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2527459328_H
#ifndef KEYVALUEPAIR_2_T424198038_H
#define KEYVALUEPAIR_2_T424198038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t424198038 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t424198038, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t424198038, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T424198038_H
#ifndef PAIR_T2832965450_H
#define PAIR_T2832965450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineClearShot/Pair
struct  Pair_t2832965450 
{
public:
	// System.Int32 Cinemachine.CinemachineClearShot/Pair::a
	int32_t ___a_0;
	// System.Single Cinemachine.CinemachineClearShot/Pair::b
	float ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(Pair_t2832965450, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(Pair_t2832965450, ___b_1)); }
	inline float get_b_1() const { return ___b_1; }
	inline float* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(float value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIR_T2832965450_H
#ifndef DEFAULTCOMPARER_T2591787268_H
#define DEFAULTCOMPARER_T2591787268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  DefaultComparer_t2591787268  : public EqualityComparer_1_t694336058
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2591787268_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef DEFAULTCOMPARER_T4077126285_H
#define DEFAULTCOMPARER_T4077126285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCollider/CompiledCurbFeeler>
struct  DefaultComparer_t4077126285  : public EqualityComparer_1_t2179675075
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4077126285_H
#ifndef DEFAULTCOMPARER_T1587381731_H
#define DEFAULTCOMPARER_T1587381731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>
struct  DefaultComparer_t1587381731  : public EqualityComparer_1_t3984897817
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1587381731_H
#ifndef DEFAULTCOMPARER_T1497241569_H
#define DEFAULTCOMPARER_T1497241569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct  DefaultComparer_t1497241569  : public EqualityComparer_1_t3894757655
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1497241569_H
#ifndef DEFAULTCOMPARER_T748816070_H
#define DEFAULTCOMPARER_T748816070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct  DefaultComparer_t748816070  : public EqualityComparer_1_t3146332156
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T748816070_H
#ifndef DEFAULTCOMPARER_T4016490495_H
#define DEFAULTCOMPARER_T4016490495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
struct  DefaultComparer_t4016490495  : public EqualityComparer_1_t2119039285
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4016490495_H
#ifndef DEFAULTCOMPARER_T3140156738_H
#define DEFAULTCOMPARER_T3140156738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
struct  DefaultComparer_t3140156738  : public EqualityComparer_1_t1242705528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3140156738_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef DEFAULTCOMPARER_T1301135589_H
#define DEFAULTCOMPARER_T1301135589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct  DefaultComparer_t1301135589  : public EqualityComparer_1_t3698651675
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1301135589_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef DEFAULTCOMPARER_T3433315258_H
#define DEFAULTCOMPARER_T3433315258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct  DefaultComparer_t3433315258  : public EqualityComparer_1_t1535864048
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3433315258_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef DEFAULTCOMPARER_T454640904_H
#define DEFAULTCOMPARER_T454640904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>
struct  DefaultComparer_t454640904  : public EqualityComparer_1_t2852156990
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T454640904_H
#ifndef COLOR32_T3428513655_H
#define COLOR32_T3428513655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t3428513655 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T3428513655_H
#ifndef TIMESPAN_T2821448670_H
#define TIMESPAN_T2821448670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2821448670 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2821448670_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2821448670  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2821448670  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2821448670  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2821448670  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2821448670 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2821448670  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2821448670  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2821448670 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2821448670  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2821448670  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2821448670 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2821448670  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2821448670_H
#ifndef DEFAULTCOMPARER_T3405523699_H
#define DEFAULTCOMPARER_T3405523699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>
struct  DefaultComparer_t3405523699  : public EqualityComparer_1_t1508072489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3405523699_H
#ifndef DEFAULTCOMPARER_T694070604_H
#define DEFAULTCOMPARER_T694070604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct  DefaultComparer_t694070604  : public EqualityComparer_1_t3091586690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T694070604_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef DEFAULTCOMPARER_T2652825765_H
#define DEFAULTCOMPARER_T2652825765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>
struct  DefaultComparer_t2652825765  : public EqualityComparer_1_t755374555
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2652825765_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T4152925911_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T4152925911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t4152925911 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t4152925911, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t4152925911, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t4152925911_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t4152925911_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T4152925911_H
#ifndef DEFAULTCOMPARER_T2025547845_H
#define DEFAULTCOMPARER_T2025547845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct  DefaultComparer_t2025547845  : public EqualityComparer_1_t128096635
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2025547845_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef DEFAULTCOMPARER_T2979502892_H
#define DEFAULTCOMPARER_T2979502892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct  DefaultComparer_t2979502892  : public EqualityComparer_1_t1082051682
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2979502892_H
#ifndef DIRECTION_T826353237_H
#define DIRECTION_T826353237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t826353237 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t826353237, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T826353237_H
#ifndef MODE_T3892779056_H
#define MODE_T3892779056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t3892779056 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3892779056, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3892779056_H
#ifndef TRANSITION_T1196117009_H
#define TRANSITION_T1196117009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1196117009 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1196117009, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1196117009_H
#ifndef UICHARINFO_T2521911218_H
#define UICHARINFO_T2521911218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t2521911218 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t328513675  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t2521911218, ___cursorPos_0)); }
	inline Vector2_t328513675  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t328513675 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t328513675  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t2521911218, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T2521911218_H
#ifndef PLAYABLEHANDLE_T743382320_H
#define PLAYABLEHANDLE_T743382320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t743382320 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T743382320_H
#ifndef DATETIMEKIND_T1636762993_H
#define DATETIMEKIND_T1636762993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t1636762993 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t1636762993, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T1636762993_H
#ifndef CAMERAEVENT_T549426807_H
#define CAMERAEVENT_T549426807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t549426807 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t549426807, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T549426807_H
#ifndef UIVERTEX_T1642453302_H
#define UIVERTEX_T1642453302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t1642453302 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t1986933152  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t1986933152  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t3428513655  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t328513675  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t328513675  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t328513675  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t328513675  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t2436299922  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___position_0)); }
	inline Vector3_t1986933152  get_position_0() const { return ___position_0; }
	inline Vector3_t1986933152 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t1986933152  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___normal_1)); }
	inline Vector3_t1986933152  get_normal_1() const { return ___normal_1; }
	inline Vector3_t1986933152 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t1986933152  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___color_2)); }
	inline Color32_t3428513655  get_color_2() const { return ___color_2; }
	inline Color32_t3428513655 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t3428513655  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___uv0_3)); }
	inline Vector2_t328513675  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t328513675 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t328513675  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___uv1_4)); }
	inline Vector2_t328513675  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t328513675 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t328513675  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___uv2_5)); }
	inline Vector2_t328513675  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t328513675 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t328513675  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___uv3_6)); }
	inline Vector2_t328513675  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t328513675 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t328513675  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302, ___tangent_7)); }
	inline Vector4_t2436299922  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t2436299922 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t2436299922  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t1642453302_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t3428513655  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t2436299922  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t1642453302  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t3428513655  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t3428513655 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t3428513655  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2436299922  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2436299922 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2436299922  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t1642453302_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t1642453302  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t1642453302 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t1642453302  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T1642453302_H
#ifndef TEXTEDITOP_T2367146867_H
#define TEXTEDITOP_T2367146867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/TextEditOp
struct  TextEditOp_t2367146867 
{
public:
	// System.Int32 UnityEngine.TextEditor/TextEditOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextEditOp_t2367146867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTEDITOP_T2367146867_H
#ifndef STREAMINGCONTEXTSTATES_T968392603_H
#define STREAMINGCONTEXTSTATES_T968392603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t968392603 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t968392603, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T968392603_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef COLORBLOCK_T1697311542_H
#define COLORBLOCK_T1697311542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t1697311542 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2582018970  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2582018970  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2582018970  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2582018970  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_NormalColor_0)); }
	inline Color_t2582018970  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2582018970 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2582018970  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_HighlightedColor_1)); }
	inline Color_t2582018970  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2582018970 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2582018970  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_PressedColor_2)); }
	inline Color_t2582018970  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2582018970 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2582018970  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_DisabledColor_3)); }
	inline Color_t2582018970  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2582018970 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2582018970  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t1697311542, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T1697311542_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#define TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1971784194 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1971784194, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#ifndef UPDATESTATUS_T4119700699_H
#define UPDATESTATUS_T4119700699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/UpdateStatus
struct  UpdateStatus_t4119700699 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::frame
	int32_t ___frame_0;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::subframe
	int32_t ___subframe_1;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::lastFixedUpdate
	int32_t ___lastFixedUpdate_2;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineCore/UpdateStatus::targetPos
	Matrix4x4_t1237934469  ___targetPos_3;

public:
	inline static int32_t get_offset_of_frame_0() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___frame_0)); }
	inline int32_t get_frame_0() const { return ___frame_0; }
	inline int32_t* get_address_of_frame_0() { return &___frame_0; }
	inline void set_frame_0(int32_t value)
	{
		___frame_0 = value;
	}

	inline static int32_t get_offset_of_subframe_1() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___subframe_1)); }
	inline int32_t get_subframe_1() const { return ___subframe_1; }
	inline int32_t* get_address_of_subframe_1() { return &___subframe_1; }
	inline void set_subframe_1(int32_t value)
	{
		___subframe_1 = value;
	}

	inline static int32_t get_offset_of_lastFixedUpdate_2() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___lastFixedUpdate_2)); }
	inline int32_t get_lastFixedUpdate_2() const { return ___lastFixedUpdate_2; }
	inline int32_t* get_address_of_lastFixedUpdate_2() { return &___lastFixedUpdate_2; }
	inline void set_lastFixedUpdate_2(int32_t value)
	{
		___lastFixedUpdate_2 = value;
	}

	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___targetPos_3)); }
	inline Matrix4x4_t1237934469  get_targetPos_3() const { return ___targetPos_3; }
	inline Matrix4x4_t1237934469 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Matrix4x4_t1237934469  value)
	{
		___targetPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATUS_T4119700699_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T1265726028_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T1265726028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t1265726028 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t4152925911  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t1265726028, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t4152925911  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t4152925911 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t4152925911  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t1265726028, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t1265726028_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t4152925911_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t1265726028_marshaled_com
{
	CustomAttributeTypedArgument_t4152925911_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T1265726028_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef RAYCASTRESULT_T811913662_H
#define RAYCASTRESULT_T811913662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t811913662 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t2557347079 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t3528993905 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t1986933152  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t1986933152  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t328513675  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___m_GameObject_0)); }
	inline GameObject_t2557347079 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t2557347079 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t2557347079 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___module_1)); }
	inline BaseRaycaster_t3528993905 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t3528993905 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t3528993905 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___worldPosition_7)); }
	inline Vector3_t1986933152  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t1986933152 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t1986933152  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___worldNormal_8)); }
	inline Vector3_t1986933152  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t1986933152 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t1986933152  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t811913662, ___screenPosition_9)); }
	inline Vector2_t328513675  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t328513675 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t328513675  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t811913662_marshaled_pinvoke
{
	GameObject_t2557347079 * ___m_GameObject_0;
	BaseRaycaster_t3528993905 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t1986933152  ___worldPosition_7;
	Vector3_t1986933152  ___worldNormal_8;
	Vector2_t328513675  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t811913662_marshaled_com
{
	GameObject_t2557347079 * ___m_GameObject_0;
	BaseRaycaster_t3528993905 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t1986933152  ___worldPosition_7;
	Vector3_t1986933152  ___worldNormal_8;
	Vector2_t328513675  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T811913662_H
#ifndef PARTICLECOLLISIONEVENT_T2748259994_H
#define PARTICLECOLLISIONEVENT_T2748259994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleCollisionEvent
struct  ParticleCollisionEvent_t2748259994 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Intersection
	Vector3_t1986933152  ___m_Intersection_0;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Normal
	Vector3_t1986933152  ___m_Normal_1;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Velocity
	Vector3_t1986933152  ___m_Velocity_2;
	// System.Int32 UnityEngine.ParticleCollisionEvent::m_ColliderInstanceID
	int32_t ___m_ColliderInstanceID_3;

public:
	inline static int32_t get_offset_of_m_Intersection_0() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Intersection_0)); }
	inline Vector3_t1986933152  get_m_Intersection_0() const { return ___m_Intersection_0; }
	inline Vector3_t1986933152 * get_address_of_m_Intersection_0() { return &___m_Intersection_0; }
	inline void set_m_Intersection_0(Vector3_t1986933152  value)
	{
		___m_Intersection_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Normal_1)); }
	inline Vector3_t1986933152  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t1986933152 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t1986933152  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_2() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Velocity_2)); }
	inline Vector3_t1986933152  get_m_Velocity_2() const { return ___m_Velocity_2; }
	inline Vector3_t1986933152 * get_address_of_m_Velocity_2() { return &___m_Velocity_2; }
	inline void set_m_Velocity_2(Vector3_t1986933152  value)
	{
		___m_Velocity_2 = value;
	}

	inline static int32_t get_offset_of_m_ColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_ColliderInstanceID_3)); }
	inline int32_t get_m_ColliderInstanceID_3() const { return ___m_ColliderInstanceID_3; }
	inline int32_t* get_address_of_m_ColliderInstanceID_3() { return &___m_ColliderInstanceID_3; }
	inline void set_m_ColliderInstanceID_3(int32_t value)
	{
		___m_ColliderInstanceID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISIONEVENT_T2748259994_H
#ifndef DIRECTION_T541134572_H
#define DIRECTION_T541134572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t541134572 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t541134572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T541134572_H
#ifndef ASPECTMODE_T2768174585_H
#define ASPECTMODE_T2768174585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t2768174585 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t2768174585, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T2768174585_H
#ifndef RUNTIMETYPEHANDLE_T3069131627_H
#define RUNTIMETYPEHANDLE_T3069131627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3069131627 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3069131627, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3069131627_H
#ifndef FITMODE_T3613870080_H
#define FITMODE_T3613870080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t3613870080 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t3613870080, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T3613870080_H
#ifndef ARGUMENTEXCEPTION_T1812645948_H
#define ARGUMENTEXCEPTION_T1812645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1812645948  : public SystemException_t2062748594
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1812645948, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1812645948_H
#ifndef FILLMETHOD_T56711541_H
#define FILLMETHOD_T56711541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t56711541 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t56711541, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T56711541_H
#ifndef TYPE_T858962896_H
#define TYPE_T858962896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t858962896 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t858962896, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T858962896_H
#ifndef COMPILEDCURBFEELER_T1909537055_H
#define COMPILEDCURBFEELER_T1909537055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct  CompiledCurbFeeler_t1909537055 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider/CompiledCurbFeeler::LocalVector
	Vector3_t1986933152  ___LocalVector_0;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::RayDistance
	float ___RayDistance_1;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::DampingConstant
	float ___DampingConstant_2;
	// System.Boolean Cinemachine.CinemachineCollider/CompiledCurbFeeler::IsHit
	bool ___IsHit_3;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::HitDistance
	float ___HitDistance_4;

public:
	inline static int32_t get_offset_of_LocalVector_0() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___LocalVector_0)); }
	inline Vector3_t1986933152  get_LocalVector_0() const { return ___LocalVector_0; }
	inline Vector3_t1986933152 * get_address_of_LocalVector_0() { return &___LocalVector_0; }
	inline void set_LocalVector_0(Vector3_t1986933152  value)
	{
		___LocalVector_0 = value;
	}

	inline static int32_t get_offset_of_RayDistance_1() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___RayDistance_1)); }
	inline float get_RayDistance_1() const { return ___RayDistance_1; }
	inline float* get_address_of_RayDistance_1() { return &___RayDistance_1; }
	inline void set_RayDistance_1(float value)
	{
		___RayDistance_1 = value;
	}

	inline static int32_t get_offset_of_DampingConstant_2() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___DampingConstant_2)); }
	inline float get_DampingConstant_2() const { return ___DampingConstant_2; }
	inline float* get_address_of_DampingConstant_2() { return &___DampingConstant_2; }
	inline void set_DampingConstant_2(float value)
	{
		___DampingConstant_2 = value;
	}

	inline static int32_t get_offset_of_IsHit_3() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___IsHit_3)); }
	inline bool get_IsHit_3() const { return ___IsHit_3; }
	inline bool* get_address_of_IsHit_3() { return &___IsHit_3; }
	inline void set_IsHit_3(bool value)
	{
		___IsHit_3 = value;
	}

	inline static int32_t get_offset_of_HitDistance_4() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___HitDistance_4)); }
	inline float get_HitDistance_4() const { return ___HitDistance_4; }
	inline float* get_address_of_HitDistance_4() { return &___HitDistance_4; }
	inline void set_HitDistance_4(float value)
	{
		___HitDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct CompiledCurbFeeler_t1909537055_marshaled_pinvoke
{
	Vector3_t1986933152  ___LocalVector_0;
	float ___RayDistance_1;
	float ___DampingConstant_2;
	int32_t ___IsHit_3;
	float ___HitDistance_4;
};
// Native definition for COM marshalling of Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct CompiledCurbFeeler_t1909537055_marshaled_com
{
	Vector3_t1986933152  ___LocalVector_0;
	float ___RayDistance_1;
	float ___DampingConstant_2;
	int32_t ___IsHit_3;
	float ___HitDistance_4;
};
#endif // COMPILEDCURBFEELER_T1909537055_H
#ifndef CHARACTERVALIDATION_T624311843_H
#define CHARACTERVALIDATION_T624311843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t624311843 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t624311843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T624311843_H
#ifndef INPUTTYPE_T3584429689_H
#define INPUTTYPE_T3584429689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t3584429689 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t3584429689, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T3584429689_H
#ifndef CONTENTTYPE_T2797920621_H
#define CONTENTTYPE_T2797920621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t2797920621 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t2797920621, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T2797920621_H
#ifndef LINETYPE_T1424293502_H
#define LINETYPE_T1424293502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t1424293502 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t1424293502, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T1424293502_H
#ifndef KEYNOTFOUNDEXCEPTION_T4143119173_H
#define KEYNOTFOUNDEXCEPTION_T4143119173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyNotFoundException
struct  KeyNotFoundException_t4143119173  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYNOTFOUNDEXCEPTION_T4143119173_H
#ifndef KEYVALUEPAIR_2_T942443640_H
#define KEYVALUEPAIR_2_T942443640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  KeyValuePair_2_t942443640 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t942443640, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t942443640, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T942443640_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3069131627  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3069131627  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3069131627 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3069131627  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1985992169* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2357198184 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2357198184 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2357198184 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1985992169* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1985992169* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2357198184 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2357198184 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2357198184 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2357198184 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2357198184 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2357198184 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ARGUMENTNULLEXCEPTION_T3857807348_H
#define ARGUMENTNULLEXCEPTION_T3857807348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t3857807348  : public ArgumentException_t1812645948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T3857807348_H
#ifndef STREAMINGCONTEXT_T648298805_H
#define STREAMINGCONTEXT_T648298805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t648298805 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t648298805, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t648298805, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t648298805_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t648298805_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T648298805_H
#ifndef PLAYABLE_T3296292090_H
#define PLAYABLE_T3296292090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t3296292090 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t3296292090, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t3296292090_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t3296292090  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t3296292090_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t3296292090  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t3296292090 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t3296292090  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T3296292090_H
#ifndef KEYVALUEPAIR_2_T2876194136_H
#define KEYVALUEPAIR_2_T2876194136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>
struct  KeyValuePair_2_t2876194136 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2876194136, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2876194136, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2876194136_H
#ifndef DATETIME_T359870098_H
#define DATETIME_T359870098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t359870098 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2821448670  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___ticks_0)); }
	inline TimeSpan_t2821448670  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2821448670 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2821448670  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t359870098_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t359870098  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t359870098  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t2511808107* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t2511808107* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t2511808107* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t2511808107* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t2511808107* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t2511808107* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t2511808107* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t1965588061* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t1965588061* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MaxValue_2)); }
	inline DateTime_t359870098  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t359870098 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t359870098  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MinValue_3)); }
	inline DateTime_t359870098  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t359870098 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t359870098  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t2511808107* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t2511808107* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t2511808107* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t2511808107* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t2511808107* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t2511808107* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t2511808107* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t2511808107* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t2511808107* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t2511808107* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t2511808107* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t2511808107** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t2511808107* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t2511808107* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t2511808107** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t2511808107* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t1965588061* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t1965588061* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t1965588061* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t1965588061* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T359870098_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t2335218917  : public ArgumentException_t1812645948
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t2335218917, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#ifndef NAVIGATION_T1407358395_H
#define NAVIGATION_T1407358395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1407358395 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1782068360 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1782068360 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1782068360 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1782068360 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1407358395, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1407358395, ___m_SelectOnUp_1)); }
	inline Selectable_t1782068360 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1782068360 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1782068360 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1407358395, ___m_SelectOnDown_2)); }
	inline Selectable_t1782068360 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1782068360 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1782068360 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1407358395, ___m_SelectOnLeft_3)); }
	inline Selectable_t1782068360 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1782068360 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1782068360 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1407358395, ___m_SelectOnRight_4)); }
	inline Selectable_t1782068360 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1782068360 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1782068360 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1407358395_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1782068360 * ___m_SelectOnUp_1;
	Selectable_t1782068360 * ___m_SelectOnDown_2;
	Selectable_t1782068360 * ___m_SelectOnLeft_3;
	Selectable_t1782068360 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1407358395_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1782068360 * ___m_SelectOnUp_1;
	Selectable_t1782068360 * ___m_SelectOnDown_2;
	Selectable_t1782068360 * ___m_SelectOnLeft_3;
	Selectable_t1782068360 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1407358395_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef TRANSFORM_1_T3017738170_H
#define TRANSFORM_1_T3017738170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct  Transform_1_t3017738170  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_1_T3017738170_H
#ifndef DATETIMEOFFSET_T2252311638_H
#define DATETIMEOFFSET_T2252311638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t2252311638 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t359870098  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t2821448670  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2252311638, ___dt_2)); }
	inline DateTime_t359870098  get_dt_2() const { return ___dt_2; }
	inline DateTime_t359870098 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t359870098  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2252311638, ___utc_offset_3)); }
	inline TimeSpan_t2821448670  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t2821448670 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t2821448670  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t2252311638_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t2252311638  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t2252311638  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2252311638_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t2252311638  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t2252311638 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t2252311638  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2252311638_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t2252311638  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t2252311638 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t2252311638  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T2252311638_H
#ifndef TRANSFORM_1_T1521589844_H
#define TRANSFORM_1_T1521589844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct  Transform_1_t1521589844  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_1_T1521589844_H
#ifndef ENUMERATOR_T2171009033_H
#define ENUMERATOR_T2171009033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  Enumerator_t2171009033 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2998460458 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t942443640  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2171009033, ___dictionary_0)); }
	inline Dictionary_2_t2998460458 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2998460458 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2998460458 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2171009033, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2171009033, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2171009033, ___current_3)); }
	inline KeyValuePair_2_t942443640  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t942443640 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t942443640  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2171009033_H
#ifndef CONNECTIONCACHE_T1699053097_H
#define CONNECTIONCACHE_T1699053097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable/ConnectionCache
struct  ConnectionCache_t1699053097 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::playable
	Playable_t3296292090  ___playable_0;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable/ConnectionCache::port
	int32_t ___port_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::parent
	Playable_t3296292090  ___parent_2;
	// System.Single UnityEngine.Timeline.TimelinePlayable/ConnectionCache::evalWeight
	float ___evalWeight_3;

public:
	inline static int32_t get_offset_of_playable_0() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___playable_0)); }
	inline Playable_t3296292090  get_playable_0() const { return ___playable_0; }
	inline Playable_t3296292090 * get_address_of_playable_0() { return &___playable_0; }
	inline void set_playable_0(Playable_t3296292090  value)
	{
		___playable_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___parent_2)); }
	inline Playable_t3296292090  get_parent_2() const { return ___parent_2; }
	inline Playable_t3296292090 * get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Playable_t3296292090  value)
	{
		___parent_2 = value;
	}

	inline static int32_t get_offset_of_evalWeight_3() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___evalWeight_3)); }
	inline float get_evalWeight_3() const { return ___evalWeight_3; }
	inline float* get_address_of_evalWeight_3() { return &___evalWeight_3; }
	inline void set_evalWeight_3(float value)
	{
		___evalWeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCACHE_T1699053097_H
#ifndef WEIGHTINFO_T4162100259_H
#define WEIGHTINFO_T4162100259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct  WeightInfo_t4162100259 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t3296292090  ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t3296292090  ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::modulate
	bool ___modulate_3;

public:
	inline static int32_t get_offset_of_mixer_0() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___mixer_0)); }
	inline Playable_t3296292090  get_mixer_0() const { return ___mixer_0; }
	inline Playable_t3296292090 * get_address_of_mixer_0() { return &___mixer_0; }
	inline void set_mixer_0(Playable_t3296292090  value)
	{
		___mixer_0 = value;
	}

	inline static int32_t get_offset_of_parentMixer_1() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___parentMixer_1)); }
	inline Playable_t3296292090  get_parentMixer_1() const { return ___parentMixer_1; }
	inline Playable_t3296292090 * get_address_of_parentMixer_1() { return &___parentMixer_1; }
	inline void set_parentMixer_1(Playable_t3296292090  value)
	{
		___parentMixer_1 = value;
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_modulate_3() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___modulate_3)); }
	inline bool get_modulate_3() const { return ___modulate_3; }
	inline bool* get_address_of_modulate_3() { return &___modulate_3; }
	inline void set_modulate_3(bool value)
	{
		___modulate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t4162100259_marshaled_pinvoke
{
	Playable_t3296292090  ___mixer_0;
	Playable_t3296292090  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t4162100259_marshaled_com
{
	Playable_t3296292090  ___mixer_0;
	Playable_t3296292090  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
#endif // WEIGHTINFO_T4162100259_H
#ifndef KEYVALUEPAIR_2_T274349870_H
#define KEYVALUEPAIR_2_T274349870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  KeyValuePair_2_t274349870 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ConnectionCache_t1699053097  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t274349870, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t274349870, ___value_1)); }
	inline ConnectionCache_t1699053097  get_value_1() const { return ___value_1; }
	inline ConnectionCache_t1699053097 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ConnectionCache_t1699053097  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T274349870_H
#ifndef SHIMENUMERATOR_T492542897_H
#define SHIMENUMERATOR_T492542897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct  ShimEnumerator_t492542897  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator::host_enumerator
	Enumerator_t2171009033  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(ShimEnumerator_t492542897, ___host_enumerator_0)); }
	inline Enumerator_t2171009033  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t2171009033 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t2171009033  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIMENUMERATOR_T492542897_H
#ifndef TRANSFORM_1_T4224324972_H
#define TRANSFORM_1_T4224324972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache,System.Collections.DictionaryEntry>
struct  Transform_1_t4224324972  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_1_T4224324972_H
#ifndef ENUMERATOR_T1502915263_H
#define ENUMERATOR_T1502915263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  Enumerator_t1502915263 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2330366688 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t274349870  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1502915263, ___dictionary_0)); }
	inline Dictionary_2_t2330366688 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2330366688 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2330366688 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1502915263, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1502915263, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1502915263, ___current_3)); }
	inline KeyValuePair_2_t274349870  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t274349870 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t274349870  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1502915263_H
#ifndef TRANSFORM_1_T2060082876_H
#define TRANSFORM_1_T2060082876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>>
struct  Transform_1_t2060082876  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_1_T2060082876_H
#ifndef SHIMENUMERATOR_T4119416423_H
#define SHIMENUMERATOR_T4119416423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct  ShimEnumerator_t4119416423  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator::host_enumerator
	Enumerator_t1502915263  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(ShimEnumerator_t4119416423, ___host_enumerator_0)); }
	inline Enumerator_t1502915263  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t1502915263 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t1502915263  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIMENUMERATOR_T4119416423_H
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t2351962793  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t942443640  m_Items[1];

public:
	inline KeyValuePair_2_t942443640  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t942443640 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t942443640  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t942443640  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t942443640 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t942443640  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t2824327403  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DictionaryEntry_t2438591966  m_Items[1];

public:
	inline DictionaryEntry_t2438591966  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DictionaryEntry_t2438591966 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DictionaryEntry_t2438591966  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DictionaryEntry_t2438591966  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DictionaryEntry_t2438591966 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DictionaryEntry_t2438591966  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t1965588061  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3030196201  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Link_t1037336632  m_Items[1];

public:
	inline Link_t1037336632  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t1037336632 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t1037336632  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t1037336632  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t1037336632 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t1037336632  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2737604620  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t73912674  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>[]
struct KeyValuePair_2U5BU5D_t3558549595  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t274349870  m_Items[1];

public:
	inline KeyValuePair_2_t274349870  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t274349870 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t274349870  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t274349870  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t274349870 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t274349870  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Timeline.TimelinePlayable/ConnectionCache[]
struct ConnectionCacheU5BU5D_t1280499476  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ConnectionCache_t1699053097  m_Items[1];

public:
	inline ConnectionCache_t1699053097  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConnectionCache_t1699053097 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConnectionCache_t1699053097  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ConnectionCache_t1699053097  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConnectionCache_t1699053097 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConnectionCache_t1699053097  value)
	{
		m_Items[index] = value;
	}
};
// System.Type[]
struct TypeU5BU5D_t1985992169  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2425097099_gshared (KeyValuePair_2_t942443640 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1757472643_gshared (KeyValuePair_2_t942443640 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m758440257_gshared (Enumerator_t2171009033 * __this, Dictionary_2_t2998460458 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3048120282_gshared (KeyValuePair_2_t942443640 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m992697691_gshared (KeyValuePair_2_t274349870 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Value()
extern "C"  ConnectionCache_t1699053097  KeyValuePair_2_get_Value_m2160297420_gshared (KeyValuePair_2_t274349870 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m660202656_gshared (Enumerator_t1502915263 * __this, Dictionary_2_t2330366688 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3294063440_gshared (KeyValuePair_2_t274349870 * __this, RuntimeObject * p0, ConnectionCache_t1699053097  p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2968844594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1941537752 (ArgumentNullException_t3857807348 * __this, String_t* ___paramName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m2425097099(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t942443640 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2425097099_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m1757472643(__this, method) ((  int32_t (*) (KeyValuePair_2_t942443640 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1757472643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m758440257(__this, p0, method) ((  void (*) (Enumerator_t2171009033 *, Dictionary_2_t2998460458 *, const RuntimeMethod*))Enumerator__ctor_m758440257_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyNotFoundException::.ctor()
extern "C"  void KeyNotFoundException__ctor_m691171439 (KeyNotFoundException_t4143119173 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m2309020267 (ArgumentOutOfRangeException_t2335218917 * __this, String_t* ___paramName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m2853335497 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m560113655 (ArgumentException_t1812645948 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3048120282(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t942443640 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3048120282_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Hashtable::ToPrime(System.Int32)
extern "C"  int32_t Hashtable_ToPrime_m3346435304 (RuntimeObject * __this /* static, unused */, int32_t ___x0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m4212338798 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m417523339 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.SerializationInfo::GetInt32(System.String)
extern "C"  int32_t SerializationInfo_GetInt32_m3926319644 (SerializationInfo_t259346668 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m125071402 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3069131627  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern "C"  RuntimeObject * SerializationInfo_GetValue_m4047567073 (SerializationInfo_t259346668 * __this, String_t* ___name0, Type_t * ___type1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2395435812 (RuntimeObject * __this /* static, unused */, String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m4009562449 (ArgumentException_t1812645948 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsValueType()
extern "C"  bool Type_get_IsValueType_m2077620381 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryEntry::.ctor(System.Object,System.Object)
extern "C"  void DictionaryEntry__ctor_m1248003780 (DictionaryEntry_t2438591966 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Key()
#define KeyValuePair_2_get_Key_m992697691(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t274349870 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m992697691_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Value()
#define KeyValuePair_2_get_Value_m2160297420(__this, method) ((  ConnectionCache_t1699053097  (*) (KeyValuePair_2_t274349870 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2160297420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m660202656(__this, p0, method) ((  void (*) (Enumerator_t1502915263 *, Dictionary_2_t2330366688 *, const RuntimeMethod*))Enumerator__ctor_m660202656_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3294063440(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t274349870 *, RuntimeObject *, ConnectionCache_t1699053097 , const RuntimeMethod*))KeyValuePair_2__ctor_m3294063440_gshared)(__this, p0, p1, method)
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1182996543 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Object)
extern "C"  bool Boolean_Equals_m3109793822 (bool* __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Char::GetHashCode()
extern "C"  int32_t Char_GetHashCode_m2611881626 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Char::Equals(System.Object)
extern "C"  bool Char_Equals_m3027354238 (Il2CppChar* __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m3795860923 (DateTime_t359870098 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.Object)
extern "C"  bool DateTime_Equals_m3603903985 (DateTime_t359870098 * __this, RuntimeObject * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::GetHashCode()
extern "C"  int32_t DateTimeOffset_GetHashCode_m615932961 (DateTimeOffset_t2252311638 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.Object)
extern "C"  bool DateTimeOffset_Equals_m2785200874 (DateTimeOffset_t2252311638 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::GetHashCode()
extern "C"  int32_t Guid_GetHashCode_m4179828150 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::Equals(System.Object)
extern "C"  bool Guid_Equals_m3579853263 (Guid_t * __this, RuntimeObject * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m3909839438 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Object)
extern "C"  bool Int32_Equals_m3852929443 (int32_t* __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::GetHashCode()
extern "C"  int32_t IntPtr_GetHashCode_m2577333566 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::Equals(System.Object)
extern "C"  bool IntPtr_Equals_m1135827289 (intptr_t* __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.CustomAttributeNamedArgument::GetHashCode()
extern "C"  int32_t CustomAttributeNamedArgument_GetHashCode_m1752217775 (CustomAttributeNamedArgument_t1265726028 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeNamedArgument::Equals(System.Object)
extern "C"  bool CustomAttributeNamedArgument_Equals_m821038975 (CustomAttributeNamedArgument_t1265726028 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.CustomAttributeTypedArgument::GetHashCode()
extern "C"  int32_t CustomAttributeTypedArgument_GetHashCode_m2694225328 (CustomAttributeTypedArgument_t4152925911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeTypedArgument::Equals(System.Object)
extern "C"  bool CustomAttributeTypedArgument_Equals_m1392479546 (CustomAttributeTypedArgument_t4152925911 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m2097607317 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Object)
extern "C"  bool Single_Equals_m3943440730 (float* __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m1771870684 (TimeSpan_t2821448670 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::Equals(System.Object)
extern "C"  bool TimeSpan_Equals_m996157339 (TimeSpan_t2821448670 * __this, RuntimeObject * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m4075930393 (Color_t2582018970 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m3159899910 (Color_t2582018970 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m2735696709 (Matrix4x4_t1237934469 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m911010623 (Matrix4x4_t1237934469 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.ColorBlock::GetHashCode()
extern "C"  int32_t ColorBlock_GetHashCode_m1105366439 (ColorBlock_t1697311542 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ColorBlock::Equals(System.Object)
extern "C"  bool ColorBlock_Equals_m2861682062 (ColorBlock_t1697311542 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m3492920259 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m3452260837 (Vector2_t328513675 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m4139137898 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m3593875679 (Vector3_t1986933152 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3459216764 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m754502683 (Vector4_t2436299922 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  RuntimeObject * Activator_CreateInstance_m2717577512 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::CompareTo(System.DateTime)
extern "C"  int32_t DateTime_CompareTo_m3902980031 (DateTime_t359870098 * __this, DateTime_t359870098  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::CompareTo(System.DateTimeOffset)
extern "C"  int32_t DateTimeOffset_CompareTo_m1226323519 (DateTimeOffset_t2252311638 * __this, DateTimeOffset_t2252311638  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C"  void Dictionary_2__ctor_m251385180_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2998460458 *)__this, (int32_t)((int32_t)10), (RuntimeObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1742617585_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2998460458 *)__this, (int32_t)((int32_t)10), (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3698181390_gshared (Dictionary_2_t2998460458 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2998460458 *)__this, (int32_t)L_0, (RuntimeObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1486242694_gshared (Dictionary_2_t2998460458 * __this, SerializationInfo_t259346668 * ___info0, StreamingContext_t648298805  ___context1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t259346668 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  RuntimeObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1618801947_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		if (!((RuntimeObject *)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		int32_t L_5 = ((  int32_t (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2291850405_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		RuntimeObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m420310985_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		RuntimeObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2568078342_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m2568078342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___key0;
		if (!((RuntimeObject *)IsInst((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  bool (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m212007451_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2840241965_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m230241207_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m583617514_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2_t942443640  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = KeyValuePair_2_get_Key_m2425097099((KeyValuePair_2_t942443640 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		int32_t L_1 = KeyValuePair_2_get_Value_m1757472643((KeyValuePair_2_t942443640 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m369555431_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2_t942443640  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t942443640  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2998460458 *, KeyValuePair_2_t942443640 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2998460458 *)__this, (KeyValuePair_2_t942443640 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2673844522_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2U5BU5D_t2351962793* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_t2351962793* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, KeyValuePair_2U5BU5D_t2351962793*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2998460458 *)__this, (KeyValuePair_2U5BU5D_t2351962793*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m721063346_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2_t942443640  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t942443640  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2998460458 *, KeyValuePair_2_t942443640 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2998460458 *)__this, (KeyValuePair_2_t942443640 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m2425097099((KeyValuePair_2_t942443640 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t2998460458 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2819969725_gshared (Dictionary_2_t2998460458 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m2819969725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2U5BU5D_t2351962793* V_0 = NULL;
	DictionaryEntryU5BU5D_t2824327403* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t2824327403* G_B5_1 = NULL;
	Dictionary_2_t2998460458 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t2824327403* G_B4_1 = NULL;
	Dictionary_2_t2998460458 * G_B4_2 = NULL;
	{
		RuntimeArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t2351962793*)((KeyValuePair_2U5BU5D_t2351962793*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
		KeyValuePair_2U5BU5D_t2351962793* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2351962793* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, KeyValuePair_2U5BU5D_t2351962793*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2998460458 *)__this, (KeyValuePair_2U5BU5D_t2351962793*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}

IL_0016:
	{
		RuntimeArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeArray *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		RuntimeArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t2824327403*)((DictionaryEntryU5BU5D_t2824327403*)IsInst((RuntimeObject*)L_6, DictionaryEntryU5BU5D_t2824327403_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t2824327403* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t2824327403* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t3017738170 * L_10 = ((Dictionary_2_t2998460458_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2998460458 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2998460458 *)(__this));
			goto IL_0046;
		}
	}
	{
		intptr_t L_11 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17);
		Transform_1_t3017738170 * L_12 = (Transform_1_t3017738170 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (Transform_1_t3017738170 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_12, (RuntimeObject *)NULL, (intptr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		((Dictionary_2_t2998460458_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2998460458 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t3017738170 * L_13 = ((Dictionary_2_t2998460458_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t2998460458 *)G_B5_2);
		((  void (*) (Dictionary_2_t2998460458 *, DictionaryEntryU5BU5D_t2824327403*, int32_t, Transform_1_t3017738170 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((Dictionary_2_t2998460458 *)G_B5_2, (DictionaryEntryU5BU5D_t2824327403*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t3017738170 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		return;
	}

IL_0051:
	{
		RuntimeArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		intptr_t L_16 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21);
		Transform_1_t1521589844 * L_17 = (Transform_1_t1521589844 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (Transform_1_t1521589844 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_17, (RuntimeObject *)NULL, (intptr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeArray *, int32_t, Transform_1_t1521589844 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeArray *)L_14, (int32_t)L_15, (Transform_1_t1521589844 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3512781384_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2171009033  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m758440257((&L_0), (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		Enumerator_t2171009033  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1394525151_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2171009033  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m758440257((&L_0), (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		Enumerator_t2171009033  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3643002784_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		ShimEnumerator_t492542897 * L_0 = (ShimEnumerator_t492542897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 27));
		((  void (*) (ShimEnumerator_t492542897 *, Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)(L_0, (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2100614653_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m232732215_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m232732215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		TextEditOpU5BU5D_t73912674* L_21 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t3030196201* L_25 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t4143119173 * L_29 = (KeyNotFoundException_t4143119173 *)il2cpp_codegen_object_new(KeyNotFoundException_t4143119173_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m691171439(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4236924855_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m4236924855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		RuntimeObject* L_16 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_17 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		RuntimeObject * L_21 = ___key0;
		NullCheck((RuntimeObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_16, (RuntimeObject *)L_20, (RuntimeObject *)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t3030196201* L_24 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		int32_t L_33 = V_0;
		Int32U5BU5D_t1965588061* L_34 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t3030196201* L_40 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t3030196201* L_43 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		Int32U5BU5D_t1965588061* L_45 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_49 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t3030196201* L_52 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		ObjectU5BU5D_t2737604620* L_55 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		RuntimeObject * L_57 = ___key0;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (RuntimeObject *)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t3030196201* L_59 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		LinkU5BU5D_t3030196201* L_61 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t3030196201* L_64 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		Int32U5BU5D_t1965588061* L_66 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_70 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		TextEditOpU5BU5D_t73912674* L_73 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		int32_t L_75 = ___value1;
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (int32_t)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4287122603_gshared (Dictionary_2_t2998460458 * __this, int32_t ___capacity0, RuntimeObject* ___hcp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m4287122603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Dictionary_2_t2998460458 * G_B4_0 = NULL;
	Dictionary_2_t2998460458 * G_B3_0 = NULL;
	RuntimeObject* G_B5_0 = NULL;
	Dictionary_2_t2998460458 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t2335218917 * L_1 = (ArgumentOutOfRangeException_t2335218917 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2309020267(L_1, (String_t*)_stringLiteral2666580753, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t2998460458 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2998460458 *)(__this));
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_3 = ___hcp1;
		V_0 = (RuntimeObject*)L_3;
		RuntimeObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2998460458 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		EqualityComparer_1_t2119039285 * L_5 = ((  EqualityComparer_1_t2119039285 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		G_B5_0 = ((RuntimeObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t2998460458 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((Dictionary_2_t2998460458 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3698297277_gshared (Dictionary_2_t2998460458 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m3698297277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t1965588061*)SZArrayNew(Int32U5BU5D_t1965588061_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t3030196201*)SZArrayNew(LinkU5BU5D_t3030196201_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((ObjectU5BU5D_t2737604620*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 34), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((TextEditOpU5BU5D_t73912674*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t1965588061* L_4 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1268130962_gshared (Dictionary_2_t2998460458 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m1268130962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral1848165604, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t2335218917 * L_3 = (ArgumentOutOfRangeException_t2335218917 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2309020267(L_3, (String_t*)_stringLiteral829727757, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		RuntimeArray * L_5 = ___array0;
		NullCheck((RuntimeArray *)L_5);
		int32_t L_6 = Array_get_Length_m2853335497((RuntimeArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t1812645948 * L_7 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_7, (String_t*)_stringLiteral1407751802, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		RuntimeArray * L_8 = ___array0;
		NullCheck((RuntimeArray *)L_8);
		int32_t L_9 = Array_get_Length_m2853335497((RuntimeArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t1812645948 * L_12 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_12, (String_t*)_stringLiteral164315354, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t942443640  Dictionary_2_make_pair_m4247588046_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		int32_t L_1 = ___value1;
		KeyValuePair_2_t942443640  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m3048120282((&L_2), (RuntimeObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
extern "C"  RuntimeObject * Dictionary_2_pick_key_m4006532909_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m4140321775_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3162932817_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2U5BU5D_t2351962793* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_t2351962793* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeArray *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		KeyValuePair_2U5BU5D_t2351962793* L_2 = ___array0;
		int32_t L_3 = ___index1;
		intptr_t L_4 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21);
		Transform_1_t1521589844 * L_5 = (Transform_1_t1521589844 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (Transform_1_t1521589844 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_5, (RuntimeObject *)NULL, (intptr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, KeyValuePair_2U5BU5D_t2351962793*, int32_t, Transform_1_t1521589844 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2998460458 *)__this, (KeyValuePair_2U5BU5D_t2351962793*)L_2, (int32_t)L_3, (Transform_1_t1521589844 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern "C"  void Dictionary_2_Resize_m2355567942_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m2355567942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t1965588061* V_1 = NULL;
	LinkU5BU5D_t3030196201* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t2737604620* V_7 = NULL;
	TextEditOpU5BU5D_t73912674* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t1965588061* L_0 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t2354558714_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m3346435304(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t1965588061*)((Int32U5BU5D_t1965588061*)SZArrayNew(Int32U5BU5D_t1965588061_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t3030196201*)((LinkU5BU5D_t3030196201*)SZArrayNew(LinkU5BU5D_t3030196201_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t1965588061* L_4 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t3030196201* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		RuntimeObject* L_10 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_11 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((RuntimeObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_10, (RuntimeObject *)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t3030196201* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		Int32U5BU5D_t1965588061* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t3030196201* L_29 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t1965588061* L_35 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t3030196201* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (ObjectU5BU5D_t2737604620*)((ObjectU5BU5D_t2737604620*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 34), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (TextEditOpU5BU5D_t73912674*)((TextEditOpU5BU5D_t73912674*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (uint32_t)L_39));
		ObjectU5BU5D_t2737604620* L_40 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		ObjectU5BU5D_t2737604620* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m4212338798(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_40, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		TextEditOpU5BU5D_t73912674* L_43 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		TextEditOpU5BU5D_t73912674* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m4212338798(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_43, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		ObjectU5BU5D_t2737604620* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		TextEditOpU5BU5D_t73912674* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1261102796_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m1261102796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t3030196201* L_11 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_15 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_16 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		RuntimeObject * L_20 = ___key0;
		NullCheck((RuntimeObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_15, (RuntimeObject *)L_19, (RuntimeObject *)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t1812645948 * L_22 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_22, (String_t*)_stringLiteral693071875, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t3030196201* L_23 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		int32_t L_31 = V_0;
		Int32U5BU5D_t1965588061* L_32 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t3030196201* L_38 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t3030196201* L_41 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t3030196201* L_44 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		Int32U5BU5D_t1965588061* L_46 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_50 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		ObjectU5BU5D_t2737604620* L_53 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		RuntimeObject * L_55 = ___key0;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (RuntimeObject *)L_55);
		TextEditOpU5BU5D_t73912674* L_56 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		int32_t L_58 = ___value1;
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (int32_t)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C"  void Dictionary_2_Clear_m922672322_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t1965588061* L_0 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		Int32U5BU5D_t1965588061* L_1 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t2737604620* L_2 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		ObjectU5BU5D_t2737604620* L_3 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		TextEditOpU5BU5D_t73912674* L_4 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		TextEditOpU5BU5D_t73912674* L_5 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t3030196201* L_6 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		LinkU5BU5D_t3030196201* L_7 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3215165030_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m3215165030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t3030196201* L_21 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m935530296_gshared (Dictionary_2_t2998460458 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41));
		EqualityComparer_1_t2637284887 * L_0 = ((  EqualityComparer_1_t2637284887 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40));
		V_0 = (RuntimeObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t1965588061* L_1 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		RuntimeObject* L_5 = V_0;
		TextEditOpU5BU5D_t73912674* L_6 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int32_t L_10 = ___value0;
		NullCheck((RuntimeObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 42), (RuntimeObject*)L_5, (int32_t)L_9, (int32_t)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t1965588061* L_18 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m4240004054_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___sender0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m4240004054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t2351962793* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t259346668 * L_0 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t259346668 * L_1 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t259346668 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m3926319644((SerializationInfo_t259346668 *)L_1, (String_t*)_stringLiteral2619799764, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t259346668 * L_3 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 43)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t259346668 *)L_3);
		RuntimeObject * L_5 = SerializationInfo_GetValue_m4047567073((SerializationInfo_t259346668 *)L_3, (String_t*)_stringLiteral195080435, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((RuntimeObject*)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29))));
		SerializationInfo_t259346668 * L_6 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t259346668 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m3926319644((SerializationInfo_t259346668 *)L_6, (String_t*)_stringLiteral2031386274, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t259346668 * L_8 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 44)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t259346668 *)L_8);
		RuntimeObject * L_10 = SerializationInfo_GetValue_m4047567073((SerializationInfo_t259346668 *)L_8, (String_t*)_stringLiteral1195014714, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t2351962793*)((KeyValuePair_2U5BU5D_t2351962793*)Castclass((RuntimeObject*)L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((Dictionary_2_t2998460458 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t2351962793* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t2351962793* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		RuntimeObject * L_16 = KeyValuePair_2_get_Key_m2425097099((KeyValuePair_2_t942443640 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		KeyValuePair_2U5BU5D_t2351962793* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = KeyValuePair_2_get_Value_m1757472643((KeyValuePair_2_t942443640 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		((  void (*) (Dictionary_2_t2998460458 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_16, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t2351962793* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t259346668 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2323936049_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m2323936049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RuntimeObject * V_4 = NULL;
	int32_t V_5 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_16 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_17 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		RuntimeObject * L_21 = ___key0;
		NullCheck((RuntimeObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_16, (RuntimeObject *)L_20, (RuntimeObject *)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t3030196201* L_24 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_31 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t3030196201* L_33 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t3030196201* L_36 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		LinkU5BU5D_t3030196201* L_38 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t3030196201* L_41 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t3030196201* L_45 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		ObjectU5BU5D_t2737604620* L_47 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_4));
		RuntimeObject * L_49 = V_4;
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (RuntimeObject *)L_49);
		TextEditOpU5BU5D_t73912674* L_50 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (TextEditOp_t2367146867_il2cpp_TypeInfo_var, (&V_5));
		int32_t L_52 = V_5;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2856795776_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, int32_t* ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m2856795776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		int32_t* L_21 = ___value1;
		TextEditOpU5BU5D_t73912674* L_22 = (TextEditOpU5BU5D_t73912674*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		*(int32_t*)L_21 = L_25;
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t3030196201* L_26 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t* L_30 = ___value1;
		Initobj (TextEditOp_t2367146867_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_31 = V_2;
		*(int32_t*)L_30 = L_31;
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Keys()
extern "C"  KeyCollection_t1842647776 * Dictionary_2_get_Keys_m2620604179_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		KeyCollection_t1842647776 * L_0 = (KeyCollection_t1842647776 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 45));
		((  void (*) (KeyCollection_t1842647776 *, Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(L_0, (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C"  ValueCollection_t2416567665 * Dictionary_2_get_Values_m1900169821_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		ValueCollection_t2416567665 * L_0 = (ValueCollection_t2416567665 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		((  void (*) (ValueCollection_t2416567665 *, Dictionary_2_t2998460458 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 48)->methodPointer)(L_0, (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 48));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern "C"  RuntimeObject * Dictionary_2_ToTKey_m1342912345_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m1342912345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___key0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 49)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2395435812(NULL /*static, unused*/, (String_t*)_stringLiteral3610996681, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t1812645948 * L_6 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_6, (String_t*)L_5, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		RuntimeObject * L_7 = ___key0;
		return ((RuntimeObject *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m2208033321_gshared (Dictionary_2_t2998460458 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m2208033321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m2077620381((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (TextEditOp_t2367146867_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		RuntimeObject * L_4 = ___value0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2395435812(NULL /*static, unused*/, (String_t*)_stringLiteral3610996681, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t1812645948 * L_8 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_8, (String_t*)L_7, (String_t*)_stringLiteral2721451384, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		RuntimeObject * L_9 = ___value0;
		return ((*(int32_t*)((int32_t*)UnBox(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2516306475_gshared (Dictionary_2_t2998460458 * __this, KeyValuePair_2_t942443640  ___pair0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = KeyValuePair_2_get_Key_m2425097099((KeyValuePair_2_t942443640 *)(&___pair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		NullCheck((Dictionary_2_t2998460458 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2998460458 *, RuntimeObject *, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 51)->methodPointer)((Dictionary_2_t2998460458 *)__this, (RuntimeObject *)L_0, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 51));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41));
		EqualityComparer_1_t2637284887 * L_2 = ((  EqualityComparer_1_t2637284887 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40));
		int32_t L_3 = KeyValuePair_2_get_Value_m1757472643((KeyValuePair_2_t942443640 *)(&___pair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_4 = V_0;
		NullCheck((EqualityComparer_1_t2637284887 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::Equals(T,T) */, (EqualityComparer_1_t2637284887 *)L_2, (int32_t)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C"  Enumerator_t2171009033  Dictionary_2_GetEnumerator_m523329111_gshared (Dictionary_2_t2998460458 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2171009033  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m758440257((&L_0), (Dictionary_2_t2998460458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t2438591966  Dictionary_2_U3CCopyToU3Em__0_m1317283159_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		int32_t L_1 = ___value1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		DictionaryEntry_t2438591966  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m1248003780((&L_4), (RuntimeObject *)L_0, (RuntimeObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor()
extern "C"  void Dictionary_2__ctor_m3738614628_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2330366688 *)__this, (int32_t)((int32_t)10), (RuntimeObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4014907887_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2330366688 *)__this, (int32_t)((int32_t)10), (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2934879246_gshared (Dictionary_2_t2330366688 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2330366688 *)__this, (int32_t)L_0, (RuntimeObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2949110849_gshared (Dictionary_2_t2330366688 * __this, SerializationInfo_t259346668 * ___info0, StreamingContext_t648298805  ___context1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t259346668 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  RuntimeObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2307457153_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		if (!((RuntimeObject *)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		ConnectionCache_t1699053097  L_5 = ((  ConnectionCache_t1699053097  (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ConnectionCache_t1699053097  L_6 = L_5;
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m827513314_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		RuntimeObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		ConnectionCache_t1699053097  L_3 = ((  ConnectionCache_t1699053097  (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeObject *, ConnectionCache_t1699053097 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_1, (ConnectionCache_t1699053097 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m4015526166_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		RuntimeObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		ConnectionCache_t1699053097  L_3 = ((  ConnectionCache_t1699053097  (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeObject *, ConnectionCache_t1699053097 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_1, (ConnectionCache_t1699053097 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m4254931569_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m4254931569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___key0;
		if (!((RuntimeObject *)IsInst((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  bool (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m445296780_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m614955071_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2992587207_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1859534999_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2_t274349870  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = KeyValuePair_2_get_Key_m992697691((KeyValuePair_2_t274349870 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		ConnectionCache_t1699053097  L_1 = KeyValuePair_2_get_Value_m2160297420((KeyValuePair_2_t274349870 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeObject *, ConnectionCache_t1699053097 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_0, (ConnectionCache_t1699053097 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3427797175_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2_t274349870  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t274349870  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2330366688 *, KeyValuePair_2_t274349870 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2330366688 *)__this, (KeyValuePair_2_t274349870 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m566921814_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2U5BU5D_t3558549595* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_t3558549595* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, KeyValuePair_2U5BU5D_t3558549595*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2330366688 *)__this, (KeyValuePair_2U5BU5D_t3558549595*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m894950615_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2_t274349870  ___keyValuePair0, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t274349870  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2330366688 *, KeyValuePair_2_t274349870 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2330366688 *)__this, (KeyValuePair_2_t274349870 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m992697691((KeyValuePair_2_t274349870 *)(&___keyValuePair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t2330366688 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2345618998_gshared (Dictionary_2_t2330366688 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m2345618998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2U5BU5D_t3558549595* V_0 = NULL;
	DictionaryEntryU5BU5D_t2824327403* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t2824327403* G_B5_1 = NULL;
	Dictionary_2_t2330366688 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t2824327403* G_B4_1 = NULL;
	Dictionary_2_t2330366688 * G_B4_2 = NULL;
	{
		RuntimeArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t3558549595*)((KeyValuePair_2U5BU5D_t3558549595*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
		KeyValuePair_2U5BU5D_t3558549595* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3558549595* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, KeyValuePair_2U5BU5D_t3558549595*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2330366688 *)__this, (KeyValuePair_2U5BU5D_t3558549595*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}

IL_0016:
	{
		RuntimeArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeArray *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		RuntimeArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t2824327403*)((DictionaryEntryU5BU5D_t2824327403*)IsInst((RuntimeObject*)L_6, DictionaryEntryU5BU5D_t2824327403_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t2824327403* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t2824327403* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t4224324972 * L_10 = ((Dictionary_2_t2330366688_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2330366688 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2330366688 *)(__this));
			goto IL_0046;
		}
	}
	{
		intptr_t L_11 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17);
		Transform_1_t4224324972 * L_12 = (Transform_1_t4224324972 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (Transform_1_t4224324972 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_12, (RuntimeObject *)NULL, (intptr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		((Dictionary_2_t2330366688_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2330366688 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t4224324972 * L_13 = ((Dictionary_2_t2330366688_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t2330366688 *)G_B5_2);
		((  void (*) (Dictionary_2_t2330366688 *, DictionaryEntryU5BU5D_t2824327403*, int32_t, Transform_1_t4224324972 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((Dictionary_2_t2330366688 *)G_B5_2, (DictionaryEntryU5BU5D_t2824327403*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t4224324972 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		return;
	}

IL_0051:
	{
		RuntimeArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		intptr_t L_16 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21);
		Transform_1_t2060082876 * L_17 = (Transform_1_t2060082876 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (Transform_1_t2060082876 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_17, (RuntimeObject *)NULL, (intptr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeArray *, int32_t, Transform_1_t2060082876 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeArray *)L_14, (int32_t)L_15, (Transform_1_t2060082876 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3666957711_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1502915263  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m660202656((&L_0), (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		Enumerator_t1502915263  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26085358_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1502915263  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m660202656((&L_0), (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		Enumerator_t1502915263  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IDictionary.GetEnumerator()
extern "C"  RuntimeObject* Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3150120725_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		ShimEnumerator_t4119416423 * L_0 = (ShimEnumerator_t4119416423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 27));
		((  void (*) (ShimEnumerator_t4119416423 *, Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)(L_0, (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3597529777_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Item(TKey)
extern "C"  ConnectionCache_t1699053097  Dictionary_2_get_Item_m1232224721_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m1232224721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		ConnectionCacheU5BU5D_t1280499476* L_21 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		ConnectionCache_t1699053097  L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t3030196201* L_25 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t4143119173 * L_29 = (KeyNotFoundException_t4143119173 *)il2cpp_codegen_object_new(KeyNotFoundException_t4143119173_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m691171439(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1372169477_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m1372169477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		RuntimeObject* L_16 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_17 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		RuntimeObject * L_21 = ___key0;
		NullCheck((RuntimeObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_16, (RuntimeObject *)L_20, (RuntimeObject *)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t3030196201* L_24 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		int32_t L_33 = V_0;
		Int32U5BU5D_t1965588061* L_34 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t3030196201* L_40 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t3030196201* L_43 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		Int32U5BU5D_t1965588061* L_45 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_49 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t3030196201* L_52 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		ObjectU5BU5D_t2737604620* L_55 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		RuntimeObject * L_57 = ___key0;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (RuntimeObject *)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t3030196201* L_59 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		LinkU5BU5D_t3030196201* L_61 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t3030196201* L_64 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		Int32U5BU5D_t1965588061* L_66 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_70 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		ConnectionCacheU5BU5D_t1280499476* L_73 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		ConnectionCache_t1699053097  L_75 = ___value1;
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (ConnectionCache_t1699053097 )L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m336683669_gshared (Dictionary_2_t2330366688 * __this, int32_t ___capacity0, RuntimeObject* ___hcp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m336683669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Dictionary_2_t2330366688 * G_B4_0 = NULL;
	Dictionary_2_t2330366688 * G_B3_0 = NULL;
	RuntimeObject* G_B5_0 = NULL;
	Dictionary_2_t2330366688 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t2335218917 * L_1 = (ArgumentOutOfRangeException_t2335218917 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2309020267(L_1, (String_t*)_stringLiteral2666580753, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t2330366688 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2330366688 *)(__this));
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_3 = ___hcp1;
		V_0 = (RuntimeObject*)L_3;
		RuntimeObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2330366688 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		EqualityComparer_1_t2119039285 * L_5 = ((  EqualityComparer_1_t2119039285 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		G_B5_0 = ((RuntimeObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t2330366688 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((Dictionary_2_t2330366688 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m210013102_gshared (Dictionary_2_t2330366688 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m210013102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t1965588061*)SZArrayNew(Int32U5BU5D_t1965588061_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t3030196201*)SZArrayNew(LinkU5BU5D_t3030196201_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((ObjectU5BU5D_t2737604620*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 34), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((ConnectionCacheU5BU5D_t1280499476*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t1965588061* L_4 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m95678206_gshared (Dictionary_2_t2330366688 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m95678206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral1848165604, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t2335218917 * L_3 = (ArgumentOutOfRangeException_t2335218917 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2309020267(L_3, (String_t*)_stringLiteral829727757, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		RuntimeArray * L_5 = ___array0;
		NullCheck((RuntimeArray *)L_5);
		int32_t L_6 = Array_get_Length_m2853335497((RuntimeArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t1812645948 * L_7 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_7, (String_t*)_stringLiteral1407751802, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		RuntimeArray * L_8 = ___array0;
		NullCheck((RuntimeArray *)L_8);
		int32_t L_9 = Array_get_Length_m2853335497((RuntimeArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t1812645948 * L_12 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_12, (String_t*)_stringLiteral164315354, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t274349870  Dictionary_2_make_pair_m905785026_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		ConnectionCache_t1699053097  L_1 = ___value1;
		KeyValuePair_2_t274349870  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m3294063440((&L_2), (RuntimeObject *)L_0, (ConnectionCache_t1699053097 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::pick_key(TKey,TValue)
extern "C"  RuntimeObject * Dictionary_2_pick_key_m156166116_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::pick_value(TKey,TValue)
extern "C"  ConnectionCache_t1699053097  Dictionary_2_pick_value_m3746642544_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	{
		ConnectionCache_t1699053097  L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4101588313_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2U5BU5D_t3558549595* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_t3558549595* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeArray *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		KeyValuePair_2U5BU5D_t3558549595* L_2 = ___array0;
		int32_t L_3 = ___index1;
		intptr_t L_4 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21);
		Transform_1_t2060082876 * L_5 = (Transform_1_t2060082876 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (Transform_1_t2060082876 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_5, (RuntimeObject *)NULL, (intptr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, KeyValuePair_2U5BU5D_t3558549595*, int32_t, Transform_1_t2060082876 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2330366688 *)__this, (KeyValuePair_2U5BU5D_t3558549595*)L_2, (int32_t)L_3, (Transform_1_t2060082876 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Resize()
extern "C"  void Dictionary_2_Resize_m3049133820_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m3049133820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t1965588061* V_1 = NULL;
	LinkU5BU5D_t3030196201* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t2737604620* V_7 = NULL;
	ConnectionCacheU5BU5D_t1280499476* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t1965588061* L_0 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t2354558714_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m3346435304(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t1965588061*)((Int32U5BU5D_t1965588061*)SZArrayNew(Int32U5BU5D_t1965588061_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t3030196201*)((LinkU5BU5D_t3030196201*)SZArrayNew(LinkU5BU5D_t3030196201_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t1965588061* L_4 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t3030196201* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		RuntimeObject* L_10 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_11 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((RuntimeObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_10, (RuntimeObject *)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t3030196201* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		Int32U5BU5D_t1965588061* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t3030196201* L_29 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t1965588061* L_35 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t3030196201* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (ObjectU5BU5D_t2737604620*)((ObjectU5BU5D_t2737604620*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 34), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (ConnectionCacheU5BU5D_t1280499476*)((ConnectionCacheU5BU5D_t1280499476*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (uint32_t)L_39));
		ObjectU5BU5D_t2737604620* L_40 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		ObjectU5BU5D_t2737604620* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m4212338798(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_40, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ConnectionCacheU5BU5D_t1280499476* L_43 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		ConnectionCacheU5BU5D_t1280499476* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m4212338798(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_43, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		ObjectU5BU5D_t2737604620* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		ConnectionCacheU5BU5D_t1280499476* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2534200906_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m2534200906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t3030196201* L_11 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_15 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_16 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		RuntimeObject * L_20 = ___key0;
		NullCheck((RuntimeObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_15, (RuntimeObject *)L_19, (RuntimeObject *)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t1812645948 * L_22 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_22, (String_t*)_stringLiteral693071875, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t3030196201* L_23 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		int32_t L_31 = V_0;
		Int32U5BU5D_t1965588061* L_32 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t3030196201* L_38 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t3030196201* L_41 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t3030196201* L_44 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		Int32U5BU5D_t1965588061* L_46 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t1965588061* L_50 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		ObjectU5BU5D_t2737604620* L_53 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		RuntimeObject * L_55 = ___key0;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (RuntimeObject *)L_55);
		ConnectionCacheU5BU5D_t1280499476* L_56 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		ConnectionCache_t1699053097  L_58 = ___value1;
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (ConnectionCache_t1699053097 )L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Clear()
extern "C"  void Dictionary_2_Clear_m1046171593_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t1965588061* L_0 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		Int32U5BU5D_t1965588061* L_1 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t2737604620* L_2 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		ObjectU5BU5D_t2737604620* L_3 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ConnectionCacheU5BU5D_t1280499476* L_4 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		ConnectionCacheU5BU5D_t1280499476* L_5 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t3030196201* L_6 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		LinkU5BU5D_t3030196201* L_7 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m417523339(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3562032535_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m3562032535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t3030196201* L_21 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2773766865_gshared (Dictionary_2_t2330366688 * __this, ConnectionCache_t1699053097  ___value0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41));
		EqualityComparer_1_t1969191117 * L_0 = ((  EqualityComparer_1_t1969191117 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40));
		V_0 = (RuntimeObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t1965588061* L_1 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		RuntimeObject* L_5 = V_0;
		ConnectionCacheU5BU5D_t1280499476* L_6 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		ConnectionCache_t1699053097  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		ConnectionCache_t1699053097  L_10 = ___value0;
		NullCheck((RuntimeObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, ConnectionCache_t1699053097 , ConnectionCache_t1699053097  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 42), (RuntimeObject*)L_5, (ConnectionCache_t1699053097 )L_9, (ConnectionCache_t1699053097 )L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t1965588061* L_18 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3452922474_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___sender0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m3452922474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t3558549595* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t259346668 * L_0 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t259346668 * L_1 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t259346668 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m3926319644((SerializationInfo_t259346668 *)L_1, (String_t*)_stringLiteral2619799764, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t259346668 * L_3 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 43)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t259346668 *)L_3);
		RuntimeObject * L_5 = SerializationInfo_GetValue_m4047567073((SerializationInfo_t259346668 *)L_3, (String_t*)_stringLiteral195080435, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((RuntimeObject*)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29))));
		SerializationInfo_t259346668 * L_6 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t259346668 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m3926319644((SerializationInfo_t259346668 *)L_6, (String_t*)_stringLiteral2031386274, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t259346668 * L_8 = (SerializationInfo_t259346668 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 44)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t259346668 *)L_8);
		RuntimeObject * L_10 = SerializationInfo_GetValue_m4047567073((SerializationInfo_t259346668 *)L_8, (String_t*)_stringLiteral1195014714, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t3558549595*)((KeyValuePair_2U5BU5D_t3558549595*)Castclass((RuntimeObject*)L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((Dictionary_2_t2330366688 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t3558549595* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t3558549595* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		RuntimeObject * L_16 = KeyValuePair_2_get_Key_m992697691((KeyValuePair_2_t274349870 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		KeyValuePair_2U5BU5D_t3558549595* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		ConnectionCache_t1699053097  L_19 = KeyValuePair_2_get_Value_m2160297420((KeyValuePair_2_t274349870 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		((  void (*) (Dictionary_2_t2330366688 *, RuntimeObject *, ConnectionCache_t1699053097 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_16, (ConnectionCache_t1699053097 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t3558549595* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t259346668 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3052265532_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m3052265532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RuntimeObject * V_4 = NULL;
	ConnectionCache_t1699053097  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1965588061* L_6 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t3030196201* L_12 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		RuntimeObject* L_16 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_17 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		RuntimeObject * L_21 = ___key0;
		NullCheck((RuntimeObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_16, (RuntimeObject *)L_20, (RuntimeObject *)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t3030196201* L_24 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t1965588061* L_31 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t3030196201* L_33 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t3030196201* L_36 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		LinkU5BU5D_t3030196201* L_38 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t3030196201* L_41 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t3030196201* L_45 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		ObjectU5BU5D_t2737604620* L_47 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_4));
		RuntimeObject * L_49 = V_4;
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (RuntimeObject *)L_49);
		ConnectionCacheU5BU5D_t1280499476* L_50 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (ConnectionCache_t1699053097_il2cpp_TypeInfo_var, (&V_5));
		ConnectionCache_t1699053097  L_52 = V_5;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (ConnectionCache_t1699053097 )L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1227816124_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, ConnectionCache_t1699053097 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m1227816124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ConnectionCache_t1699053097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_hcp_12();
		RuntimeObject * L_3 = ___key0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_2, (RuntimeObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t1965588061* L_5 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t1965588061* L_7 = (Int32U5BU5D_t1965588061*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t3030196201* L_10 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_hcp_12();
		ObjectU5BU5D_t2737604620* L_15 = (ObjectU5BU5D_t2737604620*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		RuntimeObject * L_19 = ___key0;
		NullCheck((RuntimeObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 29), (RuntimeObject*)L_14, (RuntimeObject *)L_18, (RuntimeObject *)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		ConnectionCache_t1699053097 * L_21 = ___value1;
		ConnectionCacheU5BU5D_t1280499476* L_22 = (ConnectionCacheU5BU5D_t1280499476*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		ConnectionCache_t1699053097  L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		*(ConnectionCache_t1699053097 *)L_21 = L_25;
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t3030196201* L_26 = (LinkU5BU5D_t3030196201*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		ConnectionCache_t1699053097 * L_30 = ___value1;
		Initobj (ConnectionCache_t1699053097_il2cpp_TypeInfo_var, (&V_2));
		ConnectionCache_t1699053097  L_31 = V_2;
		*(ConnectionCache_t1699053097 *)L_30 = L_31;
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Keys()
extern "C"  KeyCollection_t1174554006 * Dictionary_2_get_Keys_m562068528_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		KeyCollection_t1174554006 * L_0 = (KeyCollection_t1174554006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 45));
		((  void (*) (KeyCollection_t1174554006 *, Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(L_0, (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Values()
extern "C"  ValueCollection_t1748473895 * Dictionary_2_get_Values_m2831641216_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		ValueCollection_t1748473895 * L_0 = (ValueCollection_t1748473895 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		((  void (*) (ValueCollection_t1748473895 *, Dictionary_2_t2330366688 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 48)->methodPointer)(L_0, (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 48));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::ToTKey(System.Object)
extern "C"  RuntimeObject * Dictionary_2_ToTKey_m72071518_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m72071518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___key0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 49)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2395435812(NULL /*static, unused*/, (String_t*)_stringLiteral3610996681, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t1812645948 * L_6 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_6, (String_t*)L_5, (String_t*)_stringLiteral18330253, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		RuntimeObject * L_7 = ___key0;
		return ((RuntimeObject *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::ToTValue(System.Object)
extern "C"  ConnectionCache_t1699053097  Dictionary_2_ToTValue_m3105797160_gshared (Dictionary_2_t2330366688 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m3105797160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionCache_t1699053097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m2077620381((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (ConnectionCache_t1699053097_il2cpp_TypeInfo_var, (&V_0));
		ConnectionCache_t1699053097  L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		RuntimeObject * L_4 = ___value0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2395435812(NULL /*static, unused*/, (String_t*)_stringLiteral3610996681, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t1812645948 * L_8 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_8, (String_t*)L_7, (String_t*)_stringLiteral2721451384, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		RuntimeObject * L_9 = ___value0;
		return ((*(ConnectionCache_t1699053097 *)((ConnectionCache_t1699053097 *)UnBox(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m4164735361_gshared (Dictionary_2_t2330366688 * __this, KeyValuePair_2_t274349870  ___pair0, const RuntimeMethod* method)
{
	ConnectionCache_t1699053097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject * L_0 = KeyValuePair_2_get_Key_m992697691((KeyValuePair_2_t274349870 *)(&___pair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		NullCheck((Dictionary_2_t2330366688 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2330366688 *, RuntimeObject *, ConnectionCache_t1699053097 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 51)->methodPointer)((Dictionary_2_t2330366688 *)__this, (RuntimeObject *)L_0, (ConnectionCache_t1699053097 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 51));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41));
		EqualityComparer_1_t1969191117 * L_2 = ((  EqualityComparer_1_t1969191117 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 40));
		ConnectionCache_t1699053097  L_3 = KeyValuePair_2_get_Value_m2160297420((KeyValuePair_2_t274349870 *)(&___pair0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		ConnectionCache_t1699053097  L_4 = V_0;
		NullCheck((EqualityComparer_1_t1969191117 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, ConnectionCache_t1699053097 , ConnectionCache_t1699053097  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Equals(T,T) */, (EqualityComparer_1_t1969191117 *)L_2, (ConnectionCache_t1699053097 )L_3, (ConnectionCache_t1699053097 )L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::GetEnumerator()
extern "C"  Enumerator_t1502915263  Dictionary_2_GetEnumerator_m1896809760_gshared (Dictionary_2_t2330366688 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1502915263  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m660202656((&L_0), (Dictionary_2_t2330366688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t2438591966  Dictionary_2_U3CCopyToU3Em__0_m4084820530_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___key0, ConnectionCache_t1699053097  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		ConnectionCache_t1699053097  L_1 = ___value1;
		ConnectionCache_t1699053097  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		DictionaryEntry_t2438591966  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m1248003780((&L_4), (RuntimeObject *)L_0, (RuntimeObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<BMGlyph/Kerning>::.ctor()
extern "C"  void DefaultComparer__ctor_m2000465764_gshared (DefaultComparer_t2814965600 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t917514390 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t917514390 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t917514390 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<BMGlyph/Kerning>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1596116210_gshared (DefaultComparer_t2814965600 * __this, Kerning_t647376370  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Kerning_t647376370 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<BMGlyph/Kerning>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3068949595_gshared (DefaultComparer_t2814965600 * __this, Kerning_t647376370  ___x0, Kerning_t647376370  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Kerning_t647376370  L_1 = ___y1;
		Kerning_t647376370  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Kerning_t647376370  L_4 = ___y1;
		Kerning_t647376370  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(Kerning_t647376370 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineClearShot/Pair>::.ctor()
extern "C"  void DefaultComparer__ctor_m734825553_gshared (DefaultComparer_t705587384 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3103103470 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3103103470 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3103103470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineClearShot/Pair>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m22431459_gshared (DefaultComparer_t705587384 * __this, Pair_t2832965450  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Pair_t2832965450 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineClearShot/Pair>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3179638298_gshared (DefaultComparer_t705587384 * __this, Pair_t2832965450  ___x0, Pair_t2832965450  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Pair_t2832965450  L_1 = ___y1;
		Pair_t2832965450  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Pair_t2832965450  L_4 = ___y1;
		Pair_t2832965450  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(Pair_t2832965450 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::.ctor()
extern "C"  void DefaultComparer__ctor_m141621692_gshared (DefaultComparer_t4077126285 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2179675075 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2179675075 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2179675075 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3541863446_gshared (DefaultComparer_t4077126285 * __this, CompiledCurbFeeler_t1909537055  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(CompiledCurbFeeler_t1909537055 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1344395091_gshared (DefaultComparer_t4077126285 * __this, CompiledCurbFeeler_t1909537055  ___x0, CompiledCurbFeeler_t1909537055  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		CompiledCurbFeeler_t1909537055  L_1 = ___y1;
		CompiledCurbFeeler_t1909537055  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CompiledCurbFeeler_t1909537055  L_4 = ___y1;
		CompiledCurbFeeler_t1909537055  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(CompiledCurbFeeler_t1909537055 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCore/UpdateStatus>::.ctor()
extern "C"  void DefaultComparer__ctor_m1646439947_gshared (DefaultComparer_t1992322633 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t94871423 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t94871423 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t94871423 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCore/UpdateStatus>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2877814015_gshared (DefaultComparer_t1992322633 * __this, UpdateStatus_t4119700699  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(UpdateStatus_t4119700699 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Cinemachine.CinemachineCore/UpdateStatus>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3965506869_gshared (DefaultComparer_t1992322633 * __this, UpdateStatus_t4119700699  ___x0, UpdateStatus_t4119700699  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		UpdateStatus_t4119700699  L_1 = ___y1;
		UpdateStatus_t4119700699  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UpdateStatus_t4119700699  L_4 = ___y1;
		UpdateStatus_t4119700699  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(UpdateStatus_t4119700699 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::.ctor()
extern "C"  void DefaultComparer__ctor_m189782810_gshared (DefaultComparer_t1497241569 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3894757655 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3894757655 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3894757655 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m176247435_gshared (DefaultComparer_t1497241569 * __this, bool ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Boolean_GetHashCode_m1182996543((bool*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3298368301_gshared (DefaultComparer_t1497241569 * __this, bool ___x0, bool ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		bool L_1 = ___y1;
		bool L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		bool L_4 = ___y1;
		bool L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m3109793822((bool*)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::.ctor()
extern "C"  void DefaultComparer__ctor_m785883563_gshared (DefaultComparer_t1587381731 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3984897817 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3984897817 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3984897817 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4225558488_gshared (DefaultComparer_t1587381731 * __this, Il2CppChar ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Char_GetHashCode_m2611881626((Il2CppChar*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3321485255_gshared (DefaultComparer_t1587381731 * __this, Il2CppChar ___x0, Il2CppChar ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Il2CppChar L_1 = ___y1;
		Il2CppChar L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Il2CppChar L_4 = ___y1;
		Il2CppChar L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Char_Equals_m3027354238((Il2CppChar*)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m59253032_gshared (DefaultComparer_t2591787268 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t694336058 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t694336058 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t694336058 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3793485047_gshared (DefaultComparer_t2591787268 * __this, KeyValuePair_2_t424198038  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(KeyValuePair_2_t424198038 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m844588299_gshared (DefaultComparer_t2591787268 * __this, KeyValuePair_2_t424198038  ___x0, KeyValuePair_2_t424198038  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		KeyValuePair_2_t424198038  L_1 = ___y1;
		KeyValuePair_2_t424198038  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		KeyValuePair_2_t424198038  L_4 = ___y1;
		KeyValuePair_2_t424198038  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(KeyValuePair_2_t424198038 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m3475265982_gshared (DefaultComparer_t748816070 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3146332156 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3146332156 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3146332156 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m278771483_gshared (DefaultComparer_t748816070 * __this, KeyValuePair_2_t2876194136  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(KeyValuePair_2_t2876194136 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3315331850_gshared (DefaultComparer_t748816070 * __this, KeyValuePair_2_t2876194136  ___x0, KeyValuePair_2_t2876194136  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		KeyValuePair_2_t2876194136  L_1 = ___y1;
		KeyValuePair_2_t2876194136  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		KeyValuePair_2_t2876194136  L_4 = ___y1;
		KeyValuePair_2_t2876194136  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(KeyValuePair_2_t2876194136 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C"  void DefaultComparer__ctor_m2476535084_gshared (DefaultComparer_t2527459328 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t630008118 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t630008118 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t630008118 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3846021480_gshared (DefaultComparer_t2527459328 * __this, DateTime_t359870098  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTime_GetHashCode_m3795860923((DateTime_t359870098 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2357445620_gshared (DefaultComparer_t2527459328 * __this, DateTime_t359870098  ___x0, DateTime_t359870098  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTime_t359870098  L_1 = ___y1;
		DateTime_t359870098  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTime_t359870098  L_4 = ___y1;
		DateTime_t359870098  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m3603903985((DateTime_t359870098 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C"  void DefaultComparer__ctor_m855148298_gshared (DefaultComparer_t124933572 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2522449658 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2522449658 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2522449658 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3817489010_gshared (DefaultComparer_t124933572 * __this, DateTimeOffset_t2252311638  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTimeOffset_GetHashCode_m615932961((DateTimeOffset_t2252311638 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1650243490_gshared (DefaultComparer_t124933572 * __this, DateTimeOffset_t2252311638  ___x0, DateTimeOffset_t2252311638  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTimeOffset_t2252311638  L_1 = ___y1;
		DateTimeOffset_t2252311638  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTimeOffset_t2252311638  L_4 = ___y1;
		DateTimeOffset_t2252311638  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = DateTimeOffset_Equals_m2785200874((DateTimeOffset_t2252311638 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C"  void DefaultComparer__ctor_m2384913991_gshared (DefaultComparer_t4275621449 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2378170239 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2378170239 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2378170239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4027658155_gshared (DefaultComparer_t4275621449 * __this, Guid_t  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Guid_GetHashCode_m4179828150((Guid_t *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m686736320_gshared (DefaultComparer_t4275621449 * __this, Guid_t  ___x0, Guid_t  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Guid_t  L_1 = ___y1;
		Guid_t  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Guid_t  L_4 = ___y1;
		Guid_t  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Guid_Equals_m3579853263((Guid_t *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m3165281228_gshared (DefaultComparer_t3140156738 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1242705528 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1242705528 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1242705528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2683218680_gshared (DefaultComparer_t3140156738 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int32_GetHashCode_m3909839438((int32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m646864844_gshared (DefaultComparer_t3140156738 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m3852929443((int32_t*)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::.ctor()
extern "C"  void DefaultComparer__ctor_m3940190058_gshared (DefaultComparer_t1395652423 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3793168509 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3793168509 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3793168509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m925447817_gshared (DefaultComparer_t1395652423 * __this, intptr_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = IntPtr_GetHashCode_m2577333566((intptr_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2480856946_gshared (DefaultComparer_t1395652423 * __this, intptr_t ___x0, intptr_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		intptr_t L_1 = ___y1;
		intptr_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		intptr_t L_4 = ___y1;
		intptr_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = IntPtr_Equals_m1135827289((intptr_t*)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m1485700976_gshared (DefaultComparer_t4016490495 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2119039285 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2119039285 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2119039285 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2665508662_gshared (DefaultComparer_t4016490495 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((RuntimeObject *)(*(&___obj0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)(*(&___obj0)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1291185599_gshared (DefaultComparer_t4016490495 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_1 = ___y1;
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RuntimeObject * L_2 = ___y1;
		NullCheck((RuntimeObject *)(*(&___x0)));
		bool L_3 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)(*(&___x0)), (RuntimeObject *)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m3610245794_gshared (DefaultComparer_t3433315258 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1535864048 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1535864048 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1535864048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2818392003_gshared (DefaultComparer_t3433315258 * __this, CustomAttributeNamedArgument_t1265726028  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = CustomAttributeNamedArgument_GetHashCode_m1752217775((CustomAttributeNamedArgument_t1265726028 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m787762369_gshared (DefaultComparer_t3433315258 * __this, CustomAttributeNamedArgument_t1265726028  ___x0, CustomAttributeNamedArgument_t1265726028  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		CustomAttributeNamedArgument_t1265726028  L_1 = ___y1;
		CustomAttributeNamedArgument_t1265726028  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CustomAttributeNamedArgument_t1265726028  L_4 = ___y1;
		CustomAttributeNamedArgument_t1265726028  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = CustomAttributeNamedArgument_Equals_m821038975((CustomAttributeNamedArgument_t1265726028 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m4211447106_gshared (DefaultComparer_t2025547845 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t128096635 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t128096635 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t128096635 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3545434267_gshared (DefaultComparer_t2025547845 * __this, CustomAttributeTypedArgument_t4152925911  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = CustomAttributeTypedArgument_GetHashCode_m2694225328((CustomAttributeTypedArgument_t4152925911 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4133060255_gshared (DefaultComparer_t2025547845 * __this, CustomAttributeTypedArgument_t4152925911  ___x0, CustomAttributeTypedArgument_t4152925911  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		CustomAttributeTypedArgument_t4152925911  L_1 = ___y1;
		CustomAttributeTypedArgument_t4152925911  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CustomAttributeTypedArgument_t4152925911  L_4 = ___y1;
		CustomAttributeTypedArgument_t4152925911  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = CustomAttributeTypedArgument_Equals_m1392479546((CustomAttributeTypedArgument_t4152925911 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::.ctor()
extern "C"  void DefaultComparer__ctor_m587460948_gshared (DefaultComparer_t2652825765 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t755374555 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t755374555 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t755374555 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3646600943_gshared (DefaultComparer_t2652825765 * __this, float ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Single_GetHashCode_m2097607317((float*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1588017711_gshared (DefaultComparer_t2652825765 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		float L_1 = ___y1;
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		float L_4 = ___y1;
		float L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m3943440730((float*)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m598072058_gshared (DefaultComparer_t694070604 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3091586690 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3091586690 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3091586690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m211583761_gshared (DefaultComparer_t694070604 * __this, TimeSpan_t2821448670  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m1771870684((TimeSpan_t2821448670 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3051546993_gshared (DefaultComparer_t694070604 * __this, TimeSpan_t2821448670  ___x0, TimeSpan_t2821448670  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		TimeSpan_t2821448670  L_1 = ___y1;
		TimeSpan_t2821448670  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TimeSpan_t2821448670  L_4 = ___y1;
		TimeSpan_t2821448670  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m996157339((TimeSpan_t2821448670 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::.ctor()
extern "C"  void DefaultComparer__ctor_m3408600928_gshared (DefaultComparer_t454640904 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2852156990 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2852156990 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2852156990 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1265871862_gshared (DefaultComparer_t454640904 * __this, Color_t2582018970  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Color_GetHashCode_m4075930393((Color_t2582018970 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m214167453_gshared (DefaultComparer_t454640904 * __this, Color_t2582018970  ___x0, Color_t2582018970  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Color_t2582018970  L_1 = ___y1;
		Color_t2582018970  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Color_t2582018970  L_4 = ___y1;
		Color_t2582018970  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Color_Equals_m3159899910((Color_t2582018970 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C"  void DefaultComparer__ctor_m925109125_gshared (DefaultComparer_t1301135589 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3698651675 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3698651675 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3698651675 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3735230742_gshared (DefaultComparer_t1301135589 * __this, Color32_t3428513655  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Color32_t3428513655 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3011723931_gshared (DefaultComparer_t1301135589 * __this, Color32_t3428513655  ___x0, Color32_t3428513655  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Color32_t3428513655  L_1 = ___y1;
		Color32_t3428513655  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Color32_t3428513655  L_4 = ___y1;
		Color32_t3428513655  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(Color32_t3428513655 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m2138865534_gshared (DefaultComparer_t2979502892 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1082051682 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1082051682 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1082051682 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3119115398_gshared (DefaultComparer_t2979502892 * __this, RaycastResult_t811913662  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(RaycastResult_t811913662 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1740800128_gshared (DefaultComparer_t2979502892 * __this, RaycastResult_t811913662  ___x0, RaycastResult_t811913662  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		RaycastResult_t811913662  L_1 = ___y1;
		RaycastResult_t811913662  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RaycastResult_t811913662  L_4 = ___y1;
		RaycastResult_t811913662  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(RaycastResult_t811913662 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::.ctor()
extern "C"  void DefaultComparer__ctor_m2485650273_gshared (DefaultComparer_t3405523699 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1508072489 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1508072489 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1508072489 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2187500568_gshared (DefaultComparer_t3405523699 * __this, Matrix4x4_t1237934469  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Matrix4x4_GetHashCode_m2735696709((Matrix4x4_t1237934469 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m33911517_gshared (DefaultComparer_t3405523699 * __this, Matrix4x4_t1237934469  ___x0, Matrix4x4_t1237934469  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Matrix4x4_t1237934469  L_1 = ___y1;
		Matrix4x4_t1237934469  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Matrix4x4_t1237934469  L_4 = ___y1;
		Matrix4x4_t1237934469  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Matrix4x4_Equals_m911010623((Matrix4x4_t1237934469 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.ParticleCollisionEvent>::.ctor()
extern "C"  void DefaultComparer__ctor_m1956531019_gshared (DefaultComparer_t620881928 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3018398014 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3018398014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3018398014 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.ParticleCollisionEvent>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3963144051_gshared (DefaultComparer_t620881928 * __this, ParticleCollisionEvent_t2748259994  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(ParticleCollisionEvent_t2748259994 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.ParticleCollisionEvent>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m473484748_gshared (DefaultComparer_t620881928 * __this, ParticleCollisionEvent_t2748259994  ___x0, ParticleCollisionEvent_t2748259994  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		ParticleCollisionEvent_t2748259994  L_1 = ___y1;
		ParticleCollisionEvent_t2748259994  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ParticleCollisionEvent_t2748259994  L_4 = ___y1;
		ParticleCollisionEvent_t2748259994  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(ParticleCollisionEvent_t2748259994 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Playables.Playable>::.ctor()
extern "C"  void DefaultComparer__ctor_m2521457264_gshared (DefaultComparer_t1168914024 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3566430110 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3566430110 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3566430110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Playables.Playable>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m154206656_gshared (DefaultComparer_t1168914024 * __this, Playable_t3296292090  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Playable_t3296292090 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Playables.Playable>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2964495354_gshared (DefaultComparer_t1168914024 * __this, Playable_t3296292090  ___x0, Playable_t3296292090  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Playable_t3296292090  L_1 = ___y1;
		Playable_t3296292090  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Playable_t3296292090  L_4 = ___y1;
		Playable_t3296292090  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(Playable_t3296292090 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C"  void DefaultComparer__ctor_m3052298791_gshared (DefaultComparer_t239768801 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2637284887 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2637284887 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2637284887 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2672081760_gshared (DefaultComparer_t239768801 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3093591940_gshared (DefaultComparer_t239768801 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3669649565_gshared (DefaultComparer_t2034722193 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t137270983 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t137270983 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t137270983 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2017492713_gshared (DefaultComparer_t2034722193 * __this, WeightInfo_t4162100259  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(WeightInfo_t4162100259 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1133374711_gshared (DefaultComparer_t2034722193 * __this, WeightInfo_t4162100259  ___x0, WeightInfo_t4162100259  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		WeightInfo_t4162100259  L_1 = ___y1;
		WeightInfo_t4162100259  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		WeightInfo_t4162100259  L_4 = ___y1;
		WeightInfo_t4162100259  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(WeightInfo_t4162100259 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor()
extern "C"  void DefaultComparer__ctor_m2036839126_gshared (DefaultComparer_t3866642327 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1969191117 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1969191117 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1969191117 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1809583481_gshared (DefaultComparer_t3866642327 * __this, ConnectionCache_t1699053097  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(ConnectionCache_t1699053097 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1102599500_gshared (DefaultComparer_t3866642327 * __this, ConnectionCache_t1699053097  ___x0, ConnectionCache_t1699053097  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		ConnectionCache_t1699053097  L_1 = ___y1;
		ConnectionCache_t1699053097  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ConnectionCache_t1699053097  L_4 = ___y1;
		ConnectionCache_t1699053097  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(ConnectionCache_t1699053097 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1627641913_gshared (DefaultComparer_t4139373424 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2241922214 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2241922214 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2241922214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3232456487_gshared (DefaultComparer_t4139373424 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2969345066_gshared (DefaultComparer_t4139373424 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>::.ctor()
extern "C"  void DefaultComparer__ctor_m3430308883_gshared (DefaultComparer_t640796519 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3038312605 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3038312605 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3038312605 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3089887199_gshared (DefaultComparer_t640796519 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3631783982_gshared (DefaultComparer_t640796519 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>::.ctor()
extern "C"  void DefaultComparer__ctor_m1261503664_gshared (DefaultComparer_t3864900772 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1967449562 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1967449562 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1967449562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2916484750_gshared (DefaultComparer_t3864900772 * __this, ColorBlock_t1697311542  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = ColorBlock_GetHashCode_m1105366439((ColorBlock_t1697311542 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1608058787_gshared (DefaultComparer_t3864900772 * __this, ColorBlock_t1697311542  ___x0, ColorBlock_t1697311542  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		ColorBlock_t1697311542  L_1 = ___y1;
		ColorBlock_t1697311542  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ColorBlock_t1697311542  L_4 = ___y1;
		ColorBlock_t1697311542  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = ColorBlock_Equals_m2861682062((ColorBlock_t1697311542 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>::.ctor()
extern "C"  void DefaultComparer__ctor_m1264618412_gshared (DefaultComparer_t1486492014 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3884008100 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3884008100 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3884008100 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1995944205_gshared (DefaultComparer_t1486492014 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3840112731_gshared (DefaultComparer_t1486492014 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>::.ctor()
extern "C"  void DefaultComparer__ctor_m3693184378_gshared (DefaultComparer_t2224300771 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t326849561 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t326849561 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t326849561 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1277534296_gshared (DefaultComparer_t2224300771 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m926086763_gshared (DefaultComparer_t2224300771 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>::.ctor()
extern "C"  void DefaultComparer__ctor_m982133643_gshared (DefaultComparer_t3026552126 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1129100916 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1129100916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1129100916 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3831961363_gshared (DefaultComparer_t3026552126 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4174580946_gshared (DefaultComparer_t3026552126 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>::.ctor()
extern "C"  void DefaultComparer__ctor_m2941041589_gshared (DefaultComparer_t2791901073 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t894449863 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t894449863 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t894449863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m77516883_gshared (DefaultComparer_t2791901073 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1924899457_gshared (DefaultComparer_t2791901073 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1370790734_gshared (DefaultComparer_t670542555 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3068058641 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3068058641 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3068058641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3845445053_gshared (DefaultComparer_t670542555 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1824992206_gshared (DefaultComparer_t670542555 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3212943729_gshared (DefaultComparer_t1457051623 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3854567709 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3854567709 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3854567709 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m327323268_gshared (DefaultComparer_t1457051623 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2292950076_gshared (DefaultComparer_t1457051623 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>::.ctor()
extern "C"  void DefaultComparer__ctor_m4273725581_gshared (DefaultComparer_t3591882732 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1694431522 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1694431522 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1694431522 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2116792523_gshared (DefaultComparer_t3591882732 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2448328987_gshared (DefaultComparer_t3591882732 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>::.ctor()
extern "C"  void DefaultComparer__ctor_m1305569783_gshared (DefaultComparer_t3574947625 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1677496415 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1677496415 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1677496415 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4026160258_gshared (DefaultComparer_t3574947625 * __this, Navigation_t1407358395  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Navigation_t1407358395 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2754000624_gshared (DefaultComparer_t3574947625 * __this, Navigation_t1407358395  ___x0, Navigation_t1407358395  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Navigation_t1407358395  L_1 = ___y1;
		Navigation_t1407358395  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Navigation_t1407358395  L_4 = ___y1;
		Navigation_t1407358395  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(Navigation_t1407358395 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>::.ctor()
extern "C"  void DefaultComparer__ctor_m3174195737_gshared (DefaultComparer_t2708723802 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t811272592 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t811272592 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t811272592 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1060802260_gshared (DefaultComparer_t2708723802 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4118788379_gshared (DefaultComparer_t2708723802 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>::.ctor()
extern "C"  void DefaultComparer__ctor_m671911650_gshared (DefaultComparer_t3363706239 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1466255029 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1466255029 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1466255029 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2201275567_gshared (DefaultComparer_t3363706239 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3176294514_gshared (DefaultComparer_t3363706239 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>::.ctor()
extern "C"  void DefaultComparer__ctor_m12359280_gshared (DefaultComparer_t2993942467 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1096491257 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1096491257 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1096491257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m58847241_gshared (DefaultComparer_t2993942467 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(int32_t*)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2505279381_gshared (DefaultComparer_t2993942467 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(int32_t*)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>::.ctor()
extern "C"  void DefaultComparer__ctor_m2471810449_gshared (DefaultComparer_t2288526179 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t391074969 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t391074969 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t391074969 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1942558986_gshared (DefaultComparer_t2288526179 * __this, SpriteState_t120936949  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(SpriteState_t120936949 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3324716000_gshared (DefaultComparer_t2288526179 * __this, SpriteState_t120936949  ___x0, SpriteState_t120936949  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		SpriteState_t120936949  L_1 = ___y1;
		SpriteState_t120936949  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		SpriteState_t120936949  L_4 = ___y1;
		SpriteState_t120936949  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(SpriteState_t120936949 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2114668889_gshared (DefaultComparer_t394533152 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2792049238 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2792049238 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2792049238 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2194163688_gshared (DefaultComparer_t394533152 * __this, UICharInfo_t2521911218  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(UICharInfo_t2521911218 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4292692504_gshared (DefaultComparer_t394533152 * __this, UICharInfo_t2521911218  ___x0, UICharInfo_t2521911218  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		UICharInfo_t2521911218  L_1 = ___y1;
		UICharInfo_t2521911218  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UICharInfo_t2521911218  L_4 = ___y1;
		UICharInfo_t2521911218  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(UICharInfo_t2521911218 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m823981632_gshared (DefaultComparer_t4063660965 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2166209755 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2166209755 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2166209755 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2336856101_gshared (DefaultComparer_t4063660965 * __this, UILineInfo_t1896071735  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(UILineInfo_t1896071735 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2198657551_gshared (DefaultComparer_t4063660965 * __this, UILineInfo_t1896071735  ___x0, UILineInfo_t1896071735  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		UILineInfo_t1896071735  L_1 = ___y1;
		UILineInfo_t1896071735  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UILineInfo_t1896071735  L_4 = ___y1;
		UILineInfo_t1896071735  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(UILineInfo_t1896071735 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C"  void DefaultComparer__ctor_m3782477191_gshared (DefaultComparer_t3810042532 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1912591322 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1912591322 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1912591322 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1479471442_gshared (DefaultComparer_t3810042532 * __this, UIVertex_t1642453302  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(UIVertex_t1642453302 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1365643904_gshared (DefaultComparer_t3810042532 * __this, UIVertex_t1642453302  ___x0, UIVertex_t1642453302  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		UIVertex_t1642453302  L_1 = ___y1;
		UIVertex_t1642453302  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UIVertex_t1642453302  L_4 = ___y1;
		UIVertex_t1642453302  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		RuntimeObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((RuntimeObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_7, (RuntimeObject *)L_6);
		*(&___x0) = *(UIVertex_t1642453302 *)UnBox(L_7);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C"  void DefaultComparer__ctor_m1411469975_gshared (DefaultComparer_t2496102905 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t598651695 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t598651695 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t598651695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1892881845_gshared (DefaultComparer_t2496102905 * __this, Vector2_t328513675  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector2_GetHashCode_m3492920259((Vector2_t328513675 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2792616906_gshared (DefaultComparer_t2496102905 * __this, Vector2_t328513675  ___x0, Vector2_t328513675  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Vector2_t328513675  L_1 = ___y1;
		Vector2_t328513675  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector2_t328513675  L_4 = ___y1;
		Vector2_t328513675  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector2_Equals_m3452260837((Vector2_t328513675 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C"  void DefaultComparer__ctor_m1023455681_gshared (DefaultComparer_t4154522382 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2257071172 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2257071172 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2257071172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1248966499_gshared (DefaultComparer_t4154522382 * __this, Vector3_t1986933152  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector3_GetHashCode_m4139137898((Vector3_t1986933152 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m660473946_gshared (DefaultComparer_t4154522382 * __this, Vector3_t1986933152  ___x0, Vector3_t1986933152  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Vector3_t1986933152  L_1 = ___y1;
		Vector3_t1986933152  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector3_t1986933152  L_4 = ___y1;
		Vector3_t1986933152  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m3593875679((Vector3_t1986933152 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C"  void DefaultComparer__ctor_m104597395_gshared (DefaultComparer_t308921856 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2706437942 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2706437942 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2706437942 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2047911289_gshared (DefaultComparer_t308921856 * __this, Vector4_t2436299922  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector4_GetHashCode_m3459216764((Vector4_t2436299922 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2163742527_gshared (DefaultComparer_t308921856 * __this, Vector4_t2436299922  ___x0, Vector4_t2436299922  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Vector4_t2436299922  L_1 = ___y1;
		Vector4_t2436299922  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector4_t2436299922  L_4 = ___y1;
		Vector4_t2436299922  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector4_Equals_m754502683((Vector4_t2436299922 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1899068549_gshared (EqualityComparer_1_t917514390 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3460897571_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3460897571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t917514390_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t917514390 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2814965600 * L_8 = (DefaultComparer_t2814965600 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2814965600 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t917514390_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3819562949_gshared (EqualityComparer_1_t917514390 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t917514390 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Kerning_t647376370  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::GetHashCode(T) */, (EqualityComparer_1_t917514390 *)__this, (Kerning_t647376370 )((*(Kerning_t647376370 *)((Kerning_t647376370 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1403201622_gshared (EqualityComparer_1_t917514390 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t917514390 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Kerning_t647376370 , Kerning_t647376370  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::Equals(T,T) */, (EqualityComparer_1_t917514390 *)__this, (Kerning_t647376370 )((*(Kerning_t647376370 *)((Kerning_t647376370 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Kerning_t647376370 )((*(Kerning_t647376370 *)((Kerning_t647376370 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<BMGlyph/Kerning>::get_Default()
extern "C"  EqualityComparer_1_t917514390 * EqualityComparer_1_get_Default_m2378435474_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t917514390 * L_0 = ((EqualityComparer_1_t917514390_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2942663273_gshared (EqualityComparer_1_t3103103470 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m826911158_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m826911158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3103103470_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3103103470 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t705587384 * L_8 = (DefaultComparer_t705587384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t705587384 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3103103470_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4144017892_gshared (EqualityComparer_1_t3103103470 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3103103470 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Pair_t2832965450  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::GetHashCode(T) */, (EqualityComparer_1_t3103103470 *)__this, (Pair_t2832965450 )((*(Pair_t2832965450 *)((Pair_t2832965450 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1025562160_gshared (EqualityComparer_1_t3103103470 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3103103470 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Pair_t2832965450 , Pair_t2832965450  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::Equals(T,T) */, (EqualityComparer_1_t3103103470 *)__this, (Pair_t2832965450 )((*(Pair_t2832965450 *)((Pair_t2832965450 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Pair_t2832965450 )((*(Pair_t2832965450 *)((Pair_t2832965450 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineClearShot/Pair>::get_Default()
extern "C"  EqualityComparer_1_t3103103470 * EqualityComparer_1_get_Default_m4246557970_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3103103470 * L_0 = ((EqualityComparer_1_t3103103470_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2866625016_gshared (EqualityComparer_1_t2179675075 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1261087480_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1261087480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2179675075_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2179675075 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4077126285 * L_8 = (DefaultComparer_t4077126285 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4077126285 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2179675075_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m212668852_gshared (EqualityComparer_1_t2179675075 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2179675075 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CompiledCurbFeeler_t1909537055  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::GetHashCode(T) */, (EqualityComparer_1_t2179675075 *)__this, (CompiledCurbFeeler_t1909537055 )((*(CompiledCurbFeeler_t1909537055 *)((CompiledCurbFeeler_t1909537055 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1774600046_gshared (EqualityComparer_1_t2179675075 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2179675075 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, CompiledCurbFeeler_t1909537055 , CompiledCurbFeeler_t1909537055  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::Equals(T,T) */, (EqualityComparer_1_t2179675075 *)__this, (CompiledCurbFeeler_t1909537055 )((*(CompiledCurbFeeler_t1909537055 *)((CompiledCurbFeeler_t1909537055 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CompiledCurbFeeler_t1909537055 )((*(CompiledCurbFeeler_t1909537055 *)((CompiledCurbFeeler_t1909537055 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCollider/CompiledCurbFeeler>::get_Default()
extern "C"  EqualityComparer_1_t2179675075 * EqualityComparer_1_get_Default_m3212893367_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2179675075 * L_0 = ((EqualityComparer_1_t2179675075_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2205633745_gshared (EqualityComparer_1_t94871423 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1563994124_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1563994124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t94871423_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t94871423 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1992322633 * L_8 = (DefaultComparer_t1992322633 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1992322633 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t94871423_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1494204938_gshared (EqualityComparer_1_t94871423 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t94871423 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UpdateStatus_t4119700699  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::GetHashCode(T) */, (EqualityComparer_1_t94871423 *)__this, (UpdateStatus_t4119700699 )((*(UpdateStatus_t4119700699 *)((UpdateStatus_t4119700699 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3447445986_gshared (EqualityComparer_1_t94871423 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t94871423 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UpdateStatus_t4119700699 , UpdateStatus_t4119700699  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::Equals(T,T) */, (EqualityComparer_1_t94871423 *)__this, (UpdateStatus_t4119700699 )((*(UpdateStatus_t4119700699 *)((UpdateStatus_t4119700699 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UpdateStatus_t4119700699 )((*(UpdateStatus_t4119700699 *)((UpdateStatus_t4119700699 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Cinemachine.CinemachineCore/UpdateStatus>::get_Default()
extern "C"  EqualityComparer_1_t94871423 * EqualityComparer_1_get_Default_m1096846788_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t94871423 * L_0 = ((EqualityComparer_1_t94871423_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2569677600_gshared (EqualityComparer_1_t3894757655 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1362601712_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1362601712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3894757655_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3894757655 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1497241569 * L_8 = (DefaultComparer_t1497241569 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1497241569 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3894757655_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2999524752_gshared (EqualityComparer_1_t3894757655 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3894757655 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(T) */, (EqualityComparer_1_t3894757655 *)__this, (bool)((*(bool*)((bool*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3642153785_gshared (EqualityComparer_1_t3894757655 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3894757655 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(T,T) */, (EqualityComparer_1_t3894757655 *)__this, (bool)((*(bool*)((bool*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (bool)((*(bool*)((bool*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::get_Default()
extern "C"  EqualityComparer_1_t3894757655 * EqualityComparer_1_get_Default_m3996673284_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3894757655 * L_0 = ((EqualityComparer_1_t3894757655_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Char>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m188941474_gshared (EqualityComparer_1_t3984897817 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Char>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2102658363_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2102658363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3984897817_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3984897817 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1587381731 * L_8 = (DefaultComparer_t1587381731 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1587381731 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3984897817_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Char>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m355123837_gshared (EqualityComparer_1_t3984897817 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3984897817 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Il2CppChar >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Char>::GetHashCode(T) */, (EqualityComparer_1_t3984897817 *)__this, (Il2CppChar)((*(Il2CppChar*)((Il2CppChar*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Char>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1727759644_gshared (EqualityComparer_1_t3984897817 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3984897817 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Il2CppChar, Il2CppChar >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Char>::Equals(T,T) */, (EqualityComparer_1_t3984897817 *)__this, (Il2CppChar)((*(Il2CppChar*)((Il2CppChar*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Il2CppChar)((*(Il2CppChar*)((Il2CppChar*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Char>::get_Default()
extern "C"  EqualityComparer_1_t3984897817 * EqualityComparer_1_get_Default_m4266739255_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3984897817 * L_0 = ((EqualityComparer_1_t3984897817_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m796086700_gshared (EqualityComparer_1_t694336058 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m619178934_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m619178934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t694336058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t694336058 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2591787268 * L_8 = (DefaultComparer_t2591787268 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2591787268 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t694336058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3073487739_gshared (EqualityComparer_1_t694336058 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t694336058 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t424198038  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T) */, (EqualityComparer_1_t694336058 *)__this, (KeyValuePair_2_t424198038 )((*(KeyValuePair_2_t424198038 *)((KeyValuePair_2_t424198038 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1012939243_gshared (EqualityComparer_1_t694336058 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t694336058 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, KeyValuePair_2_t424198038 , KeyValuePair_2_t424198038  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T) */, (EqualityComparer_1_t694336058 *)__this, (KeyValuePair_2_t424198038 )((*(KeyValuePair_2_t424198038 *)((KeyValuePair_2_t424198038 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t424198038 )((*(KeyValuePair_2_t424198038 *)((KeyValuePair_2_t424198038 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Default()
extern "C"  EqualityComparer_1_t694336058 * EqualityComparer_1_get_Default_m422338501_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t694336058 * L_0 = ((EqualityComparer_1_t694336058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1157877186_gshared (EqualityComparer_1_t3146332156 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2625545329_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2625545329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3146332156_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3146332156 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t748816070 * L_8 = (DefaultComparer_t748816070 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t748816070 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3146332156_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4292155152_gshared (EqualityComparer_1_t3146332156 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3146332156 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t2876194136  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::GetHashCode(T) */, (EqualityComparer_1_t3146332156 *)__this, (KeyValuePair_2_t2876194136 )((*(KeyValuePair_2_t2876194136 *)((KeyValuePair_2_t2876194136 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1700965996_gshared (EqualityComparer_1_t3146332156 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3146332156 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, KeyValuePair_2_t2876194136 , KeyValuePair_2_t2876194136  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::Equals(T,T) */, (EqualityComparer_1_t3146332156 *)__this, (KeyValuePair_2_t2876194136 )((*(KeyValuePair_2_t2876194136 *)((KeyValuePair_2_t2876194136 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t2876194136 )((*(KeyValuePair_2_t2876194136 *)((KeyValuePair_2_t2876194136 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::get_Default()
extern "C"  EqualityComparer_1_t3146332156 * EqualityComparer_1_get_Default_m4150092635_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3146332156 * L_0 = ((EqualityComparer_1_t3146332156_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m953533035_gshared (EqualityComparer_1_t630008118 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2648746884_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2648746884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t630008118_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t630008118 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2527459328 * L_8 = (DefaultComparer_t2527459328 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2527459328 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t630008118_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3280215296_gshared (EqualityComparer_1_t630008118 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t630008118 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTime_t359870098  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::GetHashCode(T) */, (EqualityComparer_1_t630008118 *)__this, (DateTime_t359870098 )((*(DateTime_t359870098 *)((DateTime_t359870098 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1787608445_gshared (EqualityComparer_1_t630008118 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t630008118 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTime_t359870098 , DateTime_t359870098  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(T,T) */, (EqualityComparer_1_t630008118 *)__this, (DateTime_t359870098 )((*(DateTime_t359870098 *)((DateTime_t359870098 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTime_t359870098 )((*(DateTime_t359870098 *)((DateTime_t359870098 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::get_Default()
extern "C"  EqualityComparer_1_t630008118 * EqualityComparer_1_get_Default_m4225658258_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t630008118 * L_0 = ((EqualityComparer_1_t630008118_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4187757256_gshared (EqualityComparer_1_t2522449658 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4271347042_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4271347042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2522449658_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2522449658 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t124933572 * L_8 = (DefaultComparer_t124933572 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t124933572 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2522449658_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2267668654_gshared (EqualityComparer_1_t2522449658 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2522449658 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTimeOffset_t2252311638  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::GetHashCode(T) */, (EqualityComparer_1_t2522449658 *)__this, (DateTimeOffset_t2252311638 )((*(DateTimeOffset_t2252311638 *)((DateTimeOffset_t2252311638 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m631520682_gshared (EqualityComparer_1_t2522449658 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2522449658 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTimeOffset_t2252311638 , DateTimeOffset_t2252311638  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::Equals(T,T) */, (EqualityComparer_1_t2522449658 *)__this, (DateTimeOffset_t2252311638 )((*(DateTimeOffset_t2252311638 *)((DateTimeOffset_t2252311638 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTimeOffset_t2252311638 )((*(DateTimeOffset_t2252311638 *)((DateTimeOffset_t2252311638 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::get_Default()
extern "C"  EqualityComparer_1_t2522449658 * EqualityComparer_1_get_Default_m1714915139_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2522449658 * L_0 = ((EqualityComparer_1_t2522449658_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1882599584_gshared (EqualityComparer_1_t2378170239 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2310194830_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2310194830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2378170239_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2378170239 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4275621449 * L_8 = (DefaultComparer_t4275621449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4275621449 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2378170239_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m469225129_gshared (EqualityComparer_1_t2378170239 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2378170239 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Guid_t  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::GetHashCode(T) */, (EqualityComparer_1_t2378170239 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2654407211_gshared (EqualityComparer_1_t2378170239 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2378170239 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Guid_t , Guid_t  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T) */, (EqualityComparer_1_t2378170239 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::get_Default()
extern "C"  EqualityComparer_1_t2378170239 * EqualityComparer_1_get_Default_m2478093993_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2378170239 * L_0 = ((EqualityComparer_1_t2378170239_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m597361491_gshared (EqualityComparer_1_t1242705528 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m400248887_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m400248887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1242705528_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1242705528 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3140156738 * L_8 = (DefaultComparer_t3140156738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3140156738 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1242705528_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m211908516_gshared (EqualityComparer_1_t1242705528 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1242705528 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(T) */, (EqualityComparer_1_t1242705528 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2366093421_gshared (EqualityComparer_1_t1242705528 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1242705528 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T) */, (EqualityComparer_1_t1242705528 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::get_Default()
extern "C"  EqualityComparer_1_t1242705528 * EqualityComparer_1_get_Default_m527518009_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1242705528 * L_0 = ((EqualityComparer_1_t1242705528_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2290852666_gshared (EqualityComparer_1_t3793168509 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2770816535_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2770816535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3793168509_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3793168509 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1395652423 * L_8 = (DefaultComparer_t1395652423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1395652423 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3793168509_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3596131951_gshared (EqualityComparer_1_t3793168509 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3793168509 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, intptr_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::GetHashCode(T) */, (EqualityComparer_1_t3793168509 *)__this, (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3394981559_gshared (EqualityComparer_1_t3793168509 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3793168509 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, intptr_t, intptr_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::Equals(T,T) */, (EqualityComparer_1_t3793168509 *)__this, (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.IntPtr>::get_Default()
extern "C"  EqualityComparer_1_t3793168509 * EqualityComparer_1_get_Default_m3558789843_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3793168509 * L_0 = ((EqualityComparer_1_t3793168509_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2022245540_gshared (EqualityComparer_1_t2119039285 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m646046064_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m646046064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2119039285_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2119039285 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4016490495 * L_8 = (DefaultComparer_t4016490495 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4016490495 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2119039285_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4206017816_gshared (EqualityComparer_1_t2119039285 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2119039285 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(T) */, (EqualityComparer_1_t2119039285 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1417761958_gshared (EqualityComparer_1_t2119039285 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2119039285 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t2119039285 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
extern "C"  EqualityComparer_1_t2119039285 * EqualityComparer_1_get_Default_m2426281994_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2119039285 * L_0 = ((EqualityComparer_1_t2119039285_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4107768165_gshared (EqualityComparer_1_t1535864048 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m439758934_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m439758934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1535864048_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1535864048 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3433315258 * L_8 = (DefaultComparer_t3433315258 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3433315258 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1535864048_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4151429576_gshared (EqualityComparer_1_t1535864048 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1535864048 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t1265726028  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::GetHashCode(T) */, (EqualityComparer_1_t1535864048 *)__this, (CustomAttributeNamedArgument_t1265726028 )((*(CustomAttributeNamedArgument_t1265726028 *)((CustomAttributeNamedArgument_t1265726028 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m235906967_gshared (EqualityComparer_1_t1535864048 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1535864048 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, CustomAttributeNamedArgument_t1265726028 , CustomAttributeNamedArgument_t1265726028  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T) */, (EqualityComparer_1_t1535864048 *)__this, (CustomAttributeNamedArgument_t1265726028 )((*(CustomAttributeNamedArgument_t1265726028 *)((CustomAttributeNamedArgument_t1265726028 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeNamedArgument_t1265726028 )((*(CustomAttributeNamedArgument_t1265726028 *)((CustomAttributeNamedArgument_t1265726028 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C"  EqualityComparer_1_t1535864048 * EqualityComparer_1_get_Default_m1267598675_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1535864048 * L_0 = ((EqualityComparer_1_t1535864048_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m411095775_gshared (EqualityComparer_1_t128096635 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3412048117_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3412048117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t128096635_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t128096635 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2025547845 * L_8 = (DefaultComparer_t2025547845 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2025547845 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t128096635_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2633241573_gshared (EqualityComparer_1_t128096635 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t128096635 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t4152925911  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T) */, (EqualityComparer_1_t128096635 *)__this, (CustomAttributeTypedArgument_t4152925911 )((*(CustomAttributeTypedArgument_t4152925911 *)((CustomAttributeTypedArgument_t4152925911 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2479052198_gshared (EqualityComparer_1_t128096635 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t128096635 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, CustomAttributeTypedArgument_t4152925911 , CustomAttributeTypedArgument_t4152925911  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T) */, (EqualityComparer_1_t128096635 *)__this, (CustomAttributeTypedArgument_t4152925911 )((*(CustomAttributeTypedArgument_t4152925911 *)((CustomAttributeTypedArgument_t4152925911 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeTypedArgument_t4152925911 )((*(CustomAttributeTypedArgument_t4152925911 *)((CustomAttributeTypedArgument_t4152925911 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::get_Default()
extern "C"  EqualityComparer_1_t128096635 * EqualityComparer_1_get_Default_m2003880288_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t128096635 * L_0 = ((EqualityComparer_1_t128096635_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1889245348_gshared (EqualityComparer_1_t755374555 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2695851466_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2695851466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t755374555_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t755374555 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2652825765 * L_8 = (DefaultComparer_t2652825765 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2652825765 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t755374555_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2334255977_gshared (EqualityComparer_1_t755374555 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t755374555 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, float >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::GetHashCode(T) */, (EqualityComparer_1_t755374555 *)__this, (float)((*(float*)((float*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2110775421_gshared (EqualityComparer_1_t755374555 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t755374555 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t755374555 *)__this, (float)((*(float*)((float*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (float)((*(float*)((float*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C"  EqualityComparer_1_t755374555 * EqualityComparer_1_get_Default_m4250974990_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t755374555 * L_0 = ((EqualityComparer_1_t755374555_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2774202212_gshared (EqualityComparer_1_t3091586690 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2018202085_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2018202085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3091586690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3091586690 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t694070604 * L_8 = (DefaultComparer_t694070604 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t694070604 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3091586690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2458992560_gshared (EqualityComparer_1_t3091586690 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3091586690 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TimeSpan_t2821448670  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T) */, (EqualityComparer_1_t3091586690 *)__this, (TimeSpan_t2821448670 )((*(TimeSpan_t2821448670 *)((TimeSpan_t2821448670 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1527784580_gshared (EqualityComparer_1_t3091586690 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3091586690 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, TimeSpan_t2821448670 , TimeSpan_t2821448670  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T) */, (EqualityComparer_1_t3091586690 *)__this, (TimeSpan_t2821448670 )((*(TimeSpan_t2821448670 *)((TimeSpan_t2821448670 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t2821448670 )((*(TimeSpan_t2821448670 *)((TimeSpan_t2821448670 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
extern "C"  EqualityComparer_1_t3091586690 * EqualityComparer_1_get_Default_m3915245605_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3091586690 * L_0 = ((EqualityComparer_1_t3091586690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2151278162_gshared (EqualityComparer_1_t2852156990 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m72992021_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m72992021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2852156990_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2852156990 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t454640904 * L_8 = (DefaultComparer_t454640904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t454640904 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2852156990_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1797335893_gshared (EqualityComparer_1_t2852156990 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2852156990 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Color_t2582018970  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::GetHashCode(T) */, (EqualityComparer_1_t2852156990 *)__this, (Color_t2582018970 )((*(Color_t2582018970 *)((Color_t2582018970 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3917489896_gshared (EqualityComparer_1_t2852156990 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2852156990 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Color_t2582018970 , Color_t2582018970  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::Equals(T,T) */, (EqualityComparer_1_t2852156990 *)__this, (Color_t2582018970 )((*(Color_t2582018970 *)((Color_t2582018970 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color_t2582018970 )((*(Color_t2582018970 *)((Color_t2582018970 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::get_Default()
extern "C"  EqualityComparer_1_t2852156990 * EqualityComparer_1_get_Default_m1944617714_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2852156990 * L_0 = ((EqualityComparer_1_t2852156990_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2961311247_gshared (EqualityComparer_1_t3698651675 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3352180286_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3352180286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3698651675_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3698651675 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1301135589 * L_8 = (DefaultComparer_t1301135589 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1301135589 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3698651675_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1934372556_gshared (EqualityComparer_1_t3698651675 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3698651675 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Color32_t3428513655  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::GetHashCode(T) */, (EqualityComparer_1_t3698651675 *)__this, (Color32_t3428513655 )((*(Color32_t3428513655 *)((Color32_t3428513655 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1583800098_gshared (EqualityComparer_1_t3698651675 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3698651675 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Color32_t3428513655 , Color32_t3428513655  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::Equals(T,T) */, (EqualityComparer_1_t3698651675 *)__this, (Color32_t3428513655 )((*(Color32_t3428513655 *)((Color32_t3428513655 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color32_t3428513655 )((*(Color32_t3428513655 *)((Color32_t3428513655 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::get_Default()
extern "C"  EqualityComparer_1_t3698651675 * EqualityComparer_1_get_Default_m887221558_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3698651675 * L_0 = ((EqualityComparer_1_t3698651675_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2129673668_gshared (EqualityComparer_1_t1082051682 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3038343834_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3038343834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1082051682_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1082051682 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2979502892 * L_8 = (DefaultComparer_t2979502892 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2979502892 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1082051682_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m317318266_gshared (EqualityComparer_1_t1082051682 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1082051682 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, RaycastResult_t811913662  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T) */, (EqualityComparer_1_t1082051682 *)__this, (RaycastResult_t811913662 )((*(RaycastResult_t811913662 *)((RaycastResult_t811913662 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2658725372_gshared (EqualityComparer_1_t1082051682 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1082051682 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, RaycastResult_t811913662 , RaycastResult_t811913662  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::Equals(T,T) */, (EqualityComparer_1_t1082051682 *)__this, (RaycastResult_t811913662 )((*(RaycastResult_t811913662 *)((RaycastResult_t811913662 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (RaycastResult_t811913662 )((*(RaycastResult_t811913662 *)((RaycastResult_t811913662 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::get_Default()
extern "C"  EqualityComparer_1_t1082051682 * EqualityComparer_1_get_Default_m3114407279_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1082051682 * L_0 = ((EqualityComparer_1_t1082051682_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m77828479_gshared (EqualityComparer_1_t1508072489 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1027209859_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1027209859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1508072489_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1508072489 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3405523699 * L_8 = (DefaultComparer_t3405523699 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3405523699 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1508072489_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1769533774_gshared (EqualityComparer_1_t1508072489 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1508072489 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Matrix4x4_t1237934469  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::GetHashCode(T) */, (EqualityComparer_1_t1508072489 *)__this, (Matrix4x4_t1237934469 )((*(Matrix4x4_t1237934469 *)((Matrix4x4_t1237934469 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2621972223_gshared (EqualityComparer_1_t1508072489 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1508072489 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Matrix4x4_t1237934469 , Matrix4x4_t1237934469  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::Equals(T,T) */, (EqualityComparer_1_t1508072489 *)__this, (Matrix4x4_t1237934469 )((*(Matrix4x4_t1237934469 *)((Matrix4x4_t1237934469 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Matrix4x4_t1237934469 )((*(Matrix4x4_t1237934469 *)((Matrix4x4_t1237934469 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::get_Default()
extern "C"  EqualityComparer_1_t1508072489 * EqualityComparer_1_get_Default_m4049725646_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1508072489 * L_0 = ((EqualityComparer_1_t1508072489_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m136316982_gshared (EqualityComparer_1_t3018398014 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1946045186_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1946045186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3018398014_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3018398014 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t620881928 * L_8 = (DefaultComparer_t620881928 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t620881928 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3018398014_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2725955735_gshared (EqualityComparer_1_t3018398014 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3018398014 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ParticleCollisionEvent_t2748259994  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::GetHashCode(T) */, (EqualityComparer_1_t3018398014 *)__this, (ParticleCollisionEvent_t2748259994 )((*(ParticleCollisionEvent_t2748259994 *)((ParticleCollisionEvent_t2748259994 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2169873177_gshared (EqualityComparer_1_t3018398014 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3018398014 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ParticleCollisionEvent_t2748259994 , ParticleCollisionEvent_t2748259994  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::Equals(T,T) */, (EqualityComparer_1_t3018398014 *)__this, (ParticleCollisionEvent_t2748259994 )((*(ParticleCollisionEvent_t2748259994 *)((ParticleCollisionEvent_t2748259994 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ParticleCollisionEvent_t2748259994 )((*(ParticleCollisionEvent_t2748259994 *)((ParticleCollisionEvent_t2748259994 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.ParticleCollisionEvent>::get_Default()
extern "C"  EqualityComparer_1_t3018398014 * EqualityComparer_1_get_Default_m3894371193_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3018398014 * L_0 = ((EqualityComparer_1_t3018398014_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2601412129_gshared (EqualityComparer_1_t3566430110 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4102383611_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4102383611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3566430110_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3566430110 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1168914024 * L_8 = (DefaultComparer_t1168914024 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1168914024 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3566430110_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3550732779_gshared (EqualityComparer_1_t3566430110 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3566430110 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Playable_t3296292090  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::GetHashCode(T) */, (EqualityComparer_1_t3566430110 *)__this, (Playable_t3296292090 )((*(Playable_t3296292090 *)((Playable_t3296292090 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1984617798_gshared (EqualityComparer_1_t3566430110 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3566430110 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Playable_t3296292090 , Playable_t3296292090  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::Equals(T,T) */, (EqualityComparer_1_t3566430110 *)__this, (Playable_t3296292090 )((*(Playable_t3296292090 *)((Playable_t3296292090 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Playable_t3296292090 )((*(Playable_t3296292090 *)((Playable_t3296292090 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Playables.Playable>::get_Default()
extern "C"  EqualityComparer_1_t3566430110 * EqualityComparer_1_get_Default_m515329615_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3566430110 * L_0 = ((EqualityComparer_1_t3566430110_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3410962667_gshared (EqualityComparer_1_t2637284887 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m686459453_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m686459453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2637284887_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2637284887 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t239768801 * L_8 = (DefaultComparer_t239768801 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t239768801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2637284887_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4005831018_gshared (EqualityComparer_1_t2637284887 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2637284887 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::GetHashCode(T) */, (EqualityComparer_1_t2637284887 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2962716846_gshared (EqualityComparer_1_t2637284887 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2637284887 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::Equals(T,T) */, (EqualityComparer_1_t2637284887 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::get_Default()
extern "C"  EqualityComparer_1_t2637284887 * EqualityComparer_1_get_Default_m2990001907_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2637284887 * L_0 = ((EqualityComparer_1_t2637284887_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1667240503_gshared (EqualityComparer_1_t137270983 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1155318220_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1155318220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t137270983_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t137270983 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2034722193 * L_8 = (DefaultComparer_t2034722193 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2034722193 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t137270983_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2185525023_gshared (EqualityComparer_1_t137270983 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t137270983 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, WeightInfo_t4162100259  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::GetHashCode(T) */, (EqualityComparer_1_t137270983 *)__this, (WeightInfo_t4162100259 )((*(WeightInfo_t4162100259 *)((WeightInfo_t4162100259 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1077367925_gshared (EqualityComparer_1_t137270983 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t137270983 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, WeightInfo_t4162100259 , WeightInfo_t4162100259  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::Equals(T,T) */, (EqualityComparer_1_t137270983 *)__this, (WeightInfo_t4162100259 )((*(WeightInfo_t4162100259 *)((WeightInfo_t4162100259 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (WeightInfo_t4162100259 )((*(WeightInfo_t4162100259 *)((WeightInfo_t4162100259 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>::get_Default()
extern "C"  EqualityComparer_1_t137270983 * EqualityComparer_1_get_Default_m2555859329_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t137270983 * L_0 = ((EqualityComparer_1_t137270983_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m176672924_gshared (EqualityComparer_1_t1969191117 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2316346491_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2316346491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1969191117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1969191117 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3866642327 * L_8 = (DefaultComparer_t3866642327 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3866642327 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1969191117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1197210777_gshared (EqualityComparer_1_t1969191117 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1969191117 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ConnectionCache_t1699053097  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::GetHashCode(T) */, (EqualityComparer_1_t1969191117 *)__this, (ConnectionCache_t1699053097 )((*(ConnectionCache_t1699053097 *)((ConnectionCache_t1699053097 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3717336218_gshared (EqualityComparer_1_t1969191117 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1969191117 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ConnectionCache_t1699053097 , ConnectionCache_t1699053097  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::Equals(T,T) */, (EqualityComparer_1_t1969191117 *)__this, (ConnectionCache_t1699053097 )((*(ConnectionCache_t1699053097 *)((ConnectionCache_t1699053097 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ConnectionCache_t1699053097 )((*(ConnectionCache_t1699053097 *)((ConnectionCache_t1699053097 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Timeline.TimelinePlayable/ConnectionCache>::get_Default()
extern "C"  EqualityComparer_1_t1969191117 * EqualityComparer_1_get_Default_m2146226779_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1969191117 * L_0 = ((EqualityComparer_1_t1969191117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3098870709_gshared (EqualityComparer_1_t2241922214 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m189220968_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m189220968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2241922214_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2241922214 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4139373424 * L_8 = (DefaultComparer_t4139373424 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4139373424 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2241922214_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3829025157_gshared (EqualityComparer_1_t2241922214 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2241922214 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::GetHashCode(T) */, (EqualityComparer_1_t2241922214 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1788458631_gshared (EqualityComparer_1_t2241922214 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2241922214 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::Equals(T,T) */, (EqualityComparer_1_t2241922214 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::get_Default()
extern "C"  EqualityComparer_1_t2241922214 * EqualityComparer_1_get_Default_m2401031081_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2241922214 * L_0 = ((EqualityComparer_1_t2241922214_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2075932024_gshared (EqualityComparer_1_t3038312605 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1246168296_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1246168296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3038312605_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3038312605 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t640796519 * L_8 = (DefaultComparer_t640796519 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t640796519 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3038312605_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3212021949_gshared (EqualityComparer_1_t3038312605 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3038312605 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::GetHashCode(T) */, (EqualityComparer_1_t3038312605 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4034295760_gshared (EqualityComparer_1_t3038312605 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3038312605 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::Equals(T,T) */, (EqualityComparer_1_t3038312605 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::get_Default()
extern "C"  EqualityComparer_1_t3038312605 * EqualityComparer_1_get_Default_m1184082890_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3038312605 * L_0 = ((EqualityComparer_1_t3038312605_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m932929877_gshared (EqualityComparer_1_t1967449562 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1712828565_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1712828565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1967449562_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1967449562 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3864900772 * L_8 = (DefaultComparer_t3864900772 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3864900772 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1967449562_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2395486782_gshared (EqualityComparer_1_t1967449562 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1967449562 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ColorBlock_t1697311542  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::GetHashCode(T) */, (EqualityComparer_1_t1967449562 *)__this, (ColorBlock_t1697311542 )((*(ColorBlock_t1697311542 *)((ColorBlock_t1697311542 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1665740613_gshared (EqualityComparer_1_t1967449562 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1967449562 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ColorBlock_t1697311542 , ColorBlock_t1697311542  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::Equals(T,T) */, (EqualityComparer_1_t1967449562 *)__this, (ColorBlock_t1697311542 )((*(ColorBlock_t1697311542 *)((ColorBlock_t1697311542 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ColorBlock_t1697311542 )((*(ColorBlock_t1697311542 *)((ColorBlock_t1697311542 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::get_Default()
extern "C"  EqualityComparer_1_t1967449562 * EqualityComparer_1_get_Default_m807766980_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1967449562 * L_0 = ((EqualityComparer_1_t1967449562_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2105922042_gshared (EqualityComparer_1_t3884008100 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3334382730_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3334382730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3884008100_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3884008100 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1486492014 * L_8 = (DefaultComparer_t1486492014 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1486492014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3884008100_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3927598629_gshared (EqualityComparer_1_t3884008100 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3884008100 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::GetHashCode(T) */, (EqualityComparer_1_t3884008100 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3765121784_gshared (EqualityComparer_1_t3884008100 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3884008100 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::Equals(T,T) */, (EqualityComparer_1_t3884008100 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::get_Default()
extern "C"  EqualityComparer_1_t3884008100 * EqualityComparer_1_get_Default_m3893640594_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3884008100 * L_0 = ((EqualityComparer_1_t3884008100_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2743062121_gshared (EqualityComparer_1_t326849561 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m47459148_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m47459148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t326849561_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t326849561 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2224300771 * L_8 = (DefaultComparer_t2224300771 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2224300771 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t326849561_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1077687968_gshared (EqualityComparer_1_t326849561 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t326849561 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::GetHashCode(T) */, (EqualityComparer_1_t326849561 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2600770435_gshared (EqualityComparer_1_t326849561 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t326849561 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::Equals(T,T) */, (EqualityComparer_1_t326849561 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::get_Default()
extern "C"  EqualityComparer_1_t326849561 * EqualityComparer_1_get_Default_m2344880772_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t326849561 * L_0 = ((EqualityComparer_1_t326849561_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2680414325_gshared (EqualityComparer_1_t1129100916 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1176802744_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1176802744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1129100916_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1129100916 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3026552126 * L_8 = (DefaultComparer_t3026552126 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3026552126 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1129100916_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1877358523_gshared (EqualityComparer_1_t1129100916 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1129100916 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::GetHashCode(T) */, (EqualityComparer_1_t1129100916 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2168080188_gshared (EqualityComparer_1_t1129100916 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1129100916 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::Equals(T,T) */, (EqualityComparer_1_t1129100916 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::get_Default()
extern "C"  EqualityComparer_1_t1129100916 * EqualityComparer_1_get_Default_m3865591932_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1129100916 * L_0 = ((EqualityComparer_1_t1129100916_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1705575706_gshared (EqualityComparer_1_t894449863 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2505764369_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2505764369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t894449863_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t894449863 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2791901073 * L_8 = (DefaultComparer_t2791901073 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2791901073 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t894449863_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3506377793_gshared (EqualityComparer_1_t894449863 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t894449863 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::GetHashCode(T) */, (EqualityComparer_1_t894449863 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2340428033_gshared (EqualityComparer_1_t894449863 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t894449863 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::Equals(T,T) */, (EqualityComparer_1_t894449863 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::get_Default()
extern "C"  EqualityComparer_1_t894449863 * EqualityComparer_1_get_Default_m1630612983_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t894449863 * L_0 = ((EqualityComparer_1_t894449863_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2028218548_gshared (EqualityComparer_1_t3068058641 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1795385373_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1795385373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3068058641_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3068058641 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t670542555 * L_8 = (DefaultComparer_t670542555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t670542555 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3068058641_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m267479150_gshared (EqualityComparer_1_t3068058641 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3068058641 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::GetHashCode(T) */, (EqualityComparer_1_t3068058641 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3177379145_gshared (EqualityComparer_1_t3068058641 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3068058641 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::Equals(T,T) */, (EqualityComparer_1_t3068058641 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::get_Default()
extern "C"  EqualityComparer_1_t3068058641 * EqualityComparer_1_get_Default_m447882892_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3068058641 * L_0 = ((EqualityComparer_1_t3068058641_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3812819598_gshared (EqualityComparer_1_t3854567709 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3433650550_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3433650550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3854567709_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3854567709 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1457051623 * L_8 = (DefaultComparer_t1457051623 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1457051623 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3854567709_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m313359212_gshared (EqualityComparer_1_t3854567709 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3854567709 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::GetHashCode(T) */, (EqualityComparer_1_t3854567709 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2792857388_gshared (EqualityComparer_1_t3854567709 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3854567709 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::Equals(T,T) */, (EqualityComparer_1_t3854567709 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::get_Default()
extern "C"  EqualityComparer_1_t3854567709 * EqualityComparer_1_get_Default_m3444775958_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3854567709 * L_0 = ((EqualityComparer_1_t3854567709_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2168831831_gshared (EqualityComparer_1_t1694431522 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4198411519_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4198411519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1694431522_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1694431522 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3591882732 * L_8 = (DefaultComparer_t3591882732 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3591882732 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1694431522_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m213250434_gshared (EqualityComparer_1_t1694431522 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1694431522 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::GetHashCode(T) */, (EqualityComparer_1_t1694431522 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m159293310_gshared (EqualityComparer_1_t1694431522 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1694431522 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::Equals(T,T) */, (EqualityComparer_1_t1694431522 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::get_Default()
extern "C"  EqualityComparer_1_t1694431522 * EqualityComparer_1_get_Default_m3837240169_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1694431522 * L_0 = ((EqualityComparer_1_t1694431522_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2714398988_gshared (EqualityComparer_1_t1677496415 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3623065037_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3623065037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1677496415_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1677496415 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3574947625 * L_8 = (DefaultComparer_t3574947625 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3574947625 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1677496415_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m855587613_gshared (EqualityComparer_1_t1677496415 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1677496415 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Navigation_t1407358395  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::GetHashCode(T) */, (EqualityComparer_1_t1677496415 *)__this, (Navigation_t1407358395 )((*(Navigation_t1407358395 *)((Navigation_t1407358395 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m851424694_gshared (EqualityComparer_1_t1677496415 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1677496415 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Navigation_t1407358395 , Navigation_t1407358395  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::Equals(T,T) */, (EqualityComparer_1_t1677496415 *)__this, (Navigation_t1407358395 )((*(Navigation_t1407358395 *)((Navigation_t1407358395 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Navigation_t1407358395 )((*(Navigation_t1407358395 *)((Navigation_t1407358395 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::get_Default()
extern "C"  EqualityComparer_1_t1677496415 * EqualityComparer_1_get_Default_m2979752082_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1677496415 * L_0 = ((EqualityComparer_1_t1677496415_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1711128496_gshared (EqualityComparer_1_t811272592 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3844703269_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3844703269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t811272592_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t811272592 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2708723802 * L_8 = (DefaultComparer_t2708723802 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2708723802 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t811272592_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m208539647_gshared (EqualityComparer_1_t811272592 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t811272592 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::GetHashCode(T) */, (EqualityComparer_1_t811272592 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2643662142_gshared (EqualityComparer_1_t811272592 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t811272592 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::Equals(T,T) */, (EqualityComparer_1_t811272592 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::get_Default()
extern "C"  EqualityComparer_1_t811272592 * EqualityComparer_1_get_Default_m2765703411_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t811272592 * L_0 = ((EqualityComparer_1_t811272592_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m511304804_gshared (EqualityComparer_1_t1466255029 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3875088020_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3875088020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1466255029_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1466255029 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3363706239 * L_8 = (DefaultComparer_t3363706239 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3363706239 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1466255029_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3854583591_gshared (EqualityComparer_1_t1466255029 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1466255029 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::GetHashCode(T) */, (EqualityComparer_1_t1466255029 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m925484208_gshared (EqualityComparer_1_t1466255029 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1466255029 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::Equals(T,T) */, (EqualityComparer_1_t1466255029 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::get_Default()
extern "C"  EqualityComparer_1_t1466255029 * EqualityComparer_1_get_Default_m118819379_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1466255029 * L_0 = ((EqualityComparer_1_t1466255029_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3288351125_gshared (EqualityComparer_1_t1096491257 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2391794083_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2391794083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1096491257_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1096491257 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2993942467 * L_8 = (DefaultComparer_t2993942467 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2993942467 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1096491257_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1958329723_gshared (EqualityComparer_1_t1096491257 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1096491257 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::GetHashCode(T) */, (EqualityComparer_1_t1096491257 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2435972876_gshared (EqualityComparer_1_t1096491257 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1096491257 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::Equals(T,T) */, (EqualityComparer_1_t1096491257 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::get_Default()
extern "C"  EqualityComparer_1_t1096491257 * EqualityComparer_1_get_Default_m209553160_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1096491257 * L_0 = ((EqualityComparer_1_t1096491257_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1963217725_gshared (EqualityComparer_1_t391074969 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m330497758_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m330497758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t391074969_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t391074969 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2288526179 * L_8 = (DefaultComparer_t2288526179 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2288526179 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t391074969_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3615341386_gshared (EqualityComparer_1_t391074969 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t391074969 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, SpriteState_t120936949  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::GetHashCode(T) */, (EqualityComparer_1_t391074969 *)__this, (SpriteState_t120936949 )((*(SpriteState_t120936949 *)((SpriteState_t120936949 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m591514004_gshared (EqualityComparer_1_t391074969 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t391074969 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, SpriteState_t120936949 , SpriteState_t120936949  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::Equals(T,T) */, (EqualityComparer_1_t391074969 *)__this, (SpriteState_t120936949 )((*(SpriteState_t120936949 *)((SpriteState_t120936949 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (SpriteState_t120936949 )((*(SpriteState_t120936949 *)((SpriteState_t120936949 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::get_Default()
extern "C"  EqualityComparer_1_t391074969 * EqualityComparer_1_get_Default_m2217531214_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t391074969 * L_0 = ((EqualityComparer_1_t391074969_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2411063545_gshared (EqualityComparer_1_t2792049238 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3389135030_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3389135030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2792049238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2792049238 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t394533152 * L_8 = (DefaultComparer_t394533152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t394533152 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2792049238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m313604804_gshared (EqualityComparer_1_t2792049238 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2792049238 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UICharInfo_t2521911218  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::GetHashCode(T) */, (EqualityComparer_1_t2792049238 *)__this, (UICharInfo_t2521911218 )((*(UICharInfo_t2521911218 *)((UICharInfo_t2521911218 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16975387_gshared (EqualityComparer_1_t2792049238 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2792049238 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UICharInfo_t2521911218 , UICharInfo_t2521911218  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::Equals(T,T) */, (EqualityComparer_1_t2792049238 *)__this, (UICharInfo_t2521911218 )((*(UICharInfo_t2521911218 *)((UICharInfo_t2521911218 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UICharInfo_t2521911218 )((*(UICharInfo_t2521911218 *)((UICharInfo_t2521911218 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C"  EqualityComparer_1_t2792049238 * EqualityComparer_1_get_Default_m4113599029_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2792049238 * L_0 = ((EqualityComparer_1_t2792049238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m955648062_gshared (EqualityComparer_1_t2166209755 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2991137691_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2991137691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2166209755_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2166209755 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4063660965 * L_8 = (DefaultComparer_t4063660965 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4063660965 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2166209755_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m793345622_gshared (EqualityComparer_1_t2166209755 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2166209755 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UILineInfo_t1896071735  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::GetHashCode(T) */, (EqualityComparer_1_t2166209755 *)__this, (UILineInfo_t1896071735 )((*(UILineInfo_t1896071735 *)((UILineInfo_t1896071735 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4194926492_gshared (EqualityComparer_1_t2166209755 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2166209755 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UILineInfo_t1896071735 , UILineInfo_t1896071735  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::Equals(T,T) */, (EqualityComparer_1_t2166209755 *)__this, (UILineInfo_t1896071735 )((*(UILineInfo_t1896071735 *)((UILineInfo_t1896071735 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t1896071735 )((*(UILineInfo_t1896071735 *)((UILineInfo_t1896071735 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  EqualityComparer_1_t2166209755 * EqualityComparer_1_get_Default_m1787184942_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2166209755 * L_0 = ((EqualityComparer_1_t2166209755_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m625134803_gshared (EqualityComparer_1_t1912591322 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1510462659_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1510462659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1912591322_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1912591322 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3810042532 * L_8 = (DefaultComparer_t3810042532 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3810042532 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1912591322_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2686125463_gshared (EqualityComparer_1_t1912591322 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1912591322 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UIVertex_t1642453302  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::GetHashCode(T) */, (EqualityComparer_1_t1912591322 *)__this, (UIVertex_t1642453302 )((*(UIVertex_t1642453302 *)((UIVertex_t1642453302 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3806764856_gshared (EqualityComparer_1_t1912591322 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1912591322 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UIVertex_t1642453302 , UIVertex_t1642453302  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::Equals(T,T) */, (EqualityComparer_1_t1912591322 *)__this, (UIVertex_t1642453302 )((*(UIVertex_t1642453302 *)((UIVertex_t1642453302 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t1642453302 )((*(UIVertex_t1642453302 *)((UIVertex_t1642453302 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  EqualityComparer_1_t1912591322 * EqualityComparer_1_get_Default_m2184077681_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1912591322 * L_0 = ((EqualityComparer_1_t1912591322_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m563898674_gshared (EqualityComparer_1_t598651695 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3471529348_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3471529348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t598651695_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t598651695 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2496102905 * L_8 = (DefaultComparer_t2496102905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2496102905 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t598651695_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4148108478_gshared (EqualityComparer_1_t598651695 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t598651695 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector2_t328513675  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::GetHashCode(T) */, (EqualityComparer_1_t598651695 *)__this, (Vector2_t328513675 )((*(Vector2_t328513675 *)((Vector2_t328513675 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1959370627_gshared (EqualityComparer_1_t598651695 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t598651695 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector2_t328513675 , Vector2_t328513675  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::Equals(T,T) */, (EqualityComparer_1_t598651695 *)__this, (Vector2_t328513675 )((*(Vector2_t328513675 *)((Vector2_t328513675 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t328513675 )((*(Vector2_t328513675 *)((Vector2_t328513675 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  EqualityComparer_1_t598651695 * EqualityComparer_1_get_Default_m2589604946_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t598651695 * L_0 = ((EqualityComparer_1_t598651695_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4282668543_gshared (EqualityComparer_1_t2257071172 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1060605086_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1060605086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2257071172_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2257071172 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4154522382 * L_8 = (DefaultComparer_t4154522382 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4154522382 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2257071172_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1768920654_gshared (EqualityComparer_1_t2257071172 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2257071172 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector3_t1986933152  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::GetHashCode(T) */, (EqualityComparer_1_t2257071172 *)__this, (Vector3_t1986933152 )((*(Vector3_t1986933152 *)((Vector3_t1986933152 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2014701627_gshared (EqualityComparer_1_t2257071172 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2257071172 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector3_t1986933152 , Vector3_t1986933152  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::Equals(T,T) */, (EqualityComparer_1_t2257071172 *)__this, (Vector3_t1986933152 )((*(Vector3_t1986933152 *)((Vector3_t1986933152 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t1986933152 )((*(Vector3_t1986933152 *)((Vector3_t1986933152 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  EqualityComparer_1_t2257071172 * EqualityComparer_1_get_Default_m1725470245_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2257071172 * L_0 = ((EqualityComparer_1_t2257071172_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m977448328_gshared (EqualityComparer_1_t2706437942 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2968844594((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1968421493_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1968421493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(GenericEqualityComparer_1_t310718524_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_4 = (TypeU5BU5D_t1985992169*)((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, (RuntimeTypeHandle_t3069131627 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1985992169*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2706437942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2706437942 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t308921856 * L_8 = (DefaultComparer_t308921856 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t308921856 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2706437942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m678576236_gshared (EqualityComparer_1_t2706437942 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2706437942 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector4_t2436299922  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::GetHashCode(T) */, (EqualityComparer_1_t2706437942 *)__this, (Vector4_t2436299922 )((*(Vector4_t2436299922 *)((Vector4_t2436299922 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1270446137_gshared (EqualityComparer_1_t2706437942 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2706437942 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector4_t2436299922 , Vector4_t2436299922  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::Equals(T,T) */, (EqualityComparer_1_t2706437942 *)__this, (Vector4_t2436299922 )((*(Vector4_t2436299922 *)((Vector4_t2436299922 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t2436299922 )((*(Vector4_t2436299922 *)((Vector4_t2436299922 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  EqualityComparer_1_t2706437942 * EqualityComparer_1_get_Default_m3557748113_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2706437942 * L_0 = ((EqualityComparer_1_t2706437942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericComparer_1__ctor_m449815183_gshared (GenericComparer_1_t3580239271 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t2506407777 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2506407777 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2506407777 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1032073350_gshared (GenericComparer_1_t3580239271 * __this, DateTime_t359870098  ___x0, DateTime_t359870098  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t359870098  L_3 = ___y1;
		int32_t L_4 = DateTime_CompareTo_m3902980031((DateTime_t359870098 *)(&___x0), (DateTime_t359870098 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2542439427_gshared (GenericComparer_1_t1177713515 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t103882021 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t103882021 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t103882021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3450071319_gshared (GenericComparer_1_t1177713515 * __this, DateTimeOffset_t2252311638  ___x0, DateTimeOffset_t2252311638  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t2252311638  L_3 = ___y1;
		int32_t L_4 = DateTimeOffset_CompareTo_m1226323519((DateTimeOffset_t2252311638 *)(&___x0), (DateTimeOffset_t2252311638 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
