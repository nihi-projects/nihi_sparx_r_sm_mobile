﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.WWW
struct WWW_t3599262362;
// System.String
struct String_t;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t2273300952;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t84658793;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t1011823049;
// UnityEngine.WWWForm
struct WWWForm_t815382151;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1060102516;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t859941321;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t200615742;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t217099739;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t3833085680;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2480214856;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// System.Text.Encoding
struct Encoding_t4004889433;
// System.Text.DecoderFallback
struct DecoderFallback_t1140074740;
// System.Text.EncoderFallback
struct EncoderFallback_t1057161456;
// System.Reflection.Assembly
struct Assembly_t3018166672;
// System.Object[]
struct ObjectU5BU5D_t2737604620;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3030196201;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2772158654;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t259346668;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t2260858376;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t3849213472;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// System.Void
struct Void_t653366341;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t3739209734;
// UnityEngine.AssetBundle
struct AssetBundle_t3848723172;

extern RuntimeClass* UnityWebRequest_t84658793_il2cpp_TypeInfo_var;
extern RuntimeClass* UploadHandlerRaw_t859941321_il2cpp_TypeInfo_var;
extern RuntimeClass* DownloadHandlerBuffer_t217099739_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2281908325_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3223534350_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m2414193123_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1585841855_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3330050507_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m198833214_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1175114534;
extern Il2CppCodeGenString* _stringLiteral2457803851;
extern Il2CppCodeGenString* _stringLiteral3042054665;
extern const uint32_t WWW__ctor_m595852094_MetadataUsageId;
extern RuntimeClass* Int64_t1541278839_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1792448719;
extern const uint32_t WWW_get_error_m607331358_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4185710657;
extern const uint32_t WWW_get_text_m3297283267_MetadataUsageId;
extern RuntimeClass* Texture2D_t3063074017_il2cpp_TypeInfo_var;
extern const uint32_t WWW_CreateTextureFromDownloadedData_m4097086976_MetadataUsageId;
extern RuntimeClass* Debug_t3419099923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral801152855;
extern Il2CppCodeGenString* _stringLiteral2731015071;
extern const uint32_t WWW_WaitUntilDoneIfPossible_m1227673390_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral926182066;
extern Il2CppCodeGenString* _stringLiteral775335656;
extern Il2CppCodeGenString* _stringLiteral3937582660;
extern Il2CppCodeGenString* _stringLiteral1227851613;
extern Il2CppCodeGenString* _stringLiteral2175902511;
extern Il2CppCodeGenString* _stringLiteral2575790786;
extern Il2CppCodeGenString* _stringLiteral1143792069;
extern Il2CppCodeGenString* _stringLiteral4231565991;
extern Il2CppCodeGenString* _stringLiteral1795542722;
extern Il2CppCodeGenString* _stringLiteral3115644037;
extern Il2CppCodeGenString* _stringLiteral3025402805;
extern Il2CppCodeGenString* _stringLiteral3609986982;
extern Il2CppCodeGenString* _stringLiteral777220928;
extern Il2CppCodeGenString* _stringLiteral656468698;
extern Il2CppCodeGenString* _stringLiteral1002768097;
extern Il2CppCodeGenString* _stringLiteral1073510373;
extern Il2CppCodeGenString* _stringLiteral3832650110;
extern Il2CppCodeGenString* _stringLiteral3608717552;
extern Il2CppCodeGenString* _stringLiteral1983486527;
extern Il2CppCodeGenString* _stringLiteral1661384898;
extern Il2CppCodeGenString* _stringLiteral528683789;
extern Il2CppCodeGenString* _stringLiteral2135323971;
extern Il2CppCodeGenString* _stringLiteral1376671957;
extern Il2CppCodeGenString* _stringLiteral1556635899;
extern Il2CppCodeGenString* _stringLiteral2042537999;
extern Il2CppCodeGenString* _stringLiteral2792834529;
extern Il2CppCodeGenString* _stringLiteral4232457407;
extern Il2CppCodeGenString* _stringLiteral4275030961;
extern Il2CppCodeGenString* _stringLiteral1203508084;
extern Il2CppCodeGenString* _stringLiteral434391053;
extern Il2CppCodeGenString* _stringLiteral2165137379;
extern Il2CppCodeGenString* _stringLiteral3648165571;
extern Il2CppCodeGenString* _stringLiteral642399291;
extern Il2CppCodeGenString* _stringLiteral573233689;
extern Il2CppCodeGenString* _stringLiteral1180654445;
extern Il2CppCodeGenString* _stringLiteral787061741;
extern Il2CppCodeGenString* _stringLiteral398941627;
extern Il2CppCodeGenString* _stringLiteral1905332390;
extern const uint32_t WWW_GetStatusCodeName_m3112770414_MetadataUsageId;
extern RuntimeClass* Encoding_t4004889433_il2cpp_TypeInfo_var;
extern const uint32_t WWW_UnEscapeURL_m2890420115_MetadataUsageId;
extern RuntimeClass* WWWTranscoder_t3195377113_il2cpp_TypeInfo_var;
extern const uint32_t WWW_UnEscapeURL_m3473740146_MetadataUsageId;
struct UnityWebRequest_t84658793_marshaled_com;

struct ByteU5BU5D_t434619169;


#ifndef U3CMODULEU3E_T1429447281_H
#define U3CMODULEU3E_T1429447281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447281 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447281_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef YIELDINSTRUCTION_T3270995273_H
#define YIELDINSTRUCTION_T3270995273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3270995273  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3270995273_H
#ifndef ENCODING_T4004889433_H
#define ENCODING_T4004889433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t4004889433  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t1140074740 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1057161456 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___decoder_fallback_3)); }
	inline DecoderFallback_t1140074740 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t1140074740 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t1140074740 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___encoder_fallback_4)); }
	inline EncoderFallback_t1057161456 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1057161456 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1057161456 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t4004889433_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t3018166672 * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t2737604620* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t4004889433 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t4004889433 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t4004889433 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t4004889433 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t4004889433 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t4004889433 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t4004889433 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t4004889433 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t4004889433 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t4004889433 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t4004889433 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t3018166672 * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t3018166672 ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t3018166672 * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t2737604620* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t2737604620** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t2737604620* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t4004889433 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t4004889433 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t4004889433 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t4004889433 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t4004889433 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t4004889433 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t4004889433 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t4004889433 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t4004889433 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t4004889433 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t4004889433 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t4004889433 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t4004889433 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t4004889433 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t4004889433 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t4004889433 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t4004889433 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t4004889433 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t4004889433 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t4004889433 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t4004889433 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t4004889433 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t4004889433 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t4004889433 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t4004889433 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t4004889433 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t4004889433 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t4004889433 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t4004889433 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t4004889433 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T4004889433_H
#ifndef DICTIONARY_2_T1060102516_H
#define DICTIONARY_2_T1060102516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1060102516  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t1965588061* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3030196201* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t2511808107* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t2511808107* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t259346668 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___table_4)); }
	inline Int32U5BU5D_t1965588061* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t1965588061** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t1965588061* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___linkSlots_5)); }
	inline LinkU5BU5D_t3030196201* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3030196201** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3030196201* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___keySlots_6)); }
	inline StringU5BU5D_t2511808107* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t2511808107** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t2511808107* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___valueSlots_7)); }
	inline StringU5BU5D_t2511808107* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t2511808107** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t2511808107* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___serialization_info_13)); }
	inline SerializationInfo_t259346668 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t259346668 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t259346668 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1060102516_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2260858376 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1060102516_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2260858376 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2260858376 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2260858376 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1060102516_H
#ifndef WWWFORM_T815382151_H
#define WWWFORM_T815382151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t815382151  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_t3849213472 * ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_t4069179741 * ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_t4069179741 * ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_t4069179741 * ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_t434619169* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;

public:
	inline static int32_t get_offset_of_formData_0() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___formData_0)); }
	inline List_1_t3849213472 * get_formData_0() const { return ___formData_0; }
	inline List_1_t3849213472 ** get_address_of_formData_0() { return &___formData_0; }
	inline void set_formData_0(List_1_t3849213472 * value)
	{
		___formData_0 = value;
		Il2CppCodeGenWriteBarrier((&___formData_0), value);
	}

	inline static int32_t get_offset_of_fieldNames_1() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___fieldNames_1)); }
	inline List_1_t4069179741 * get_fieldNames_1() const { return ___fieldNames_1; }
	inline List_1_t4069179741 ** get_address_of_fieldNames_1() { return &___fieldNames_1; }
	inline void set_fieldNames_1(List_1_t4069179741 * value)
	{
		___fieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldNames_1), value);
	}

	inline static int32_t get_offset_of_fileNames_2() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___fileNames_2)); }
	inline List_1_t4069179741 * get_fileNames_2() const { return ___fileNames_2; }
	inline List_1_t4069179741 ** get_address_of_fileNames_2() { return &___fileNames_2; }
	inline void set_fileNames_2(List_1_t4069179741 * value)
	{
		___fileNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNames_2), value);
	}

	inline static int32_t get_offset_of_types_3() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___types_3)); }
	inline List_1_t4069179741 * get_types_3() const { return ___types_3; }
	inline List_1_t4069179741 ** get_address_of_types_3() { return &___types_3; }
	inline void set_types_3(List_1_t4069179741 * value)
	{
		___types_3 = value;
		Il2CppCodeGenWriteBarrier((&___types_3), value);
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___boundary_4)); }
	inline ByteU5BU5D_t434619169* get_boundary_4() const { return ___boundary_4; }
	inline ByteU5BU5D_t434619169** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(ByteU5BU5D_t434619169* value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_containsFiles_5() { return static_cast<int32_t>(offsetof(WWWForm_t815382151, ___containsFiles_5)); }
	inline bool get_containsFiles_5() const { return ___containsFiles_5; }
	inline bool* get_address_of_containsFiles_5() { return &___containsFiles_5; }
	inline void set_containsFiles_5(bool value)
	{
		___containsFiles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T815382151_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef CUSTOMYIELDINSTRUCTION_T2273300952_H
#define CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t2273300952  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef BYTE_T131097440_H
#define BYTE_T131097440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t131097440 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t131097440, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T131097440_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef KEYVALUEPAIR_2_T3299052994_H
#define KEYVALUEPAIR_2_T3299052994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t3299052994 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3299052994, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3299052994, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3299052994_H
#ifndef KEYVALUEPAIR_2_T424198038_H
#define KEYVALUEPAIR_2_T424198038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t424198038 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t424198038, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t424198038, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T424198038_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef INT64_T1541278839_H
#define INT64_T1541278839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t1541278839 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t1541278839, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T1541278839_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef ASYNCOPERATION_T1468153686_H
#define ASYNCOPERATION_T1468153686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1468153686  : public YieldInstruction_t3270995273
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t3739209734 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1468153686, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1468153686, ___m_completeCallback_1)); }
	inline Action_1_t3739209734 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t3739209734 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t3739209734 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686_marshaled_pinvoke : public YieldInstruction_t3270995273_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686_marshaled_com : public YieldInstruction_t3270995273_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1468153686_H
#ifndef THREADPRIORITY_T3576530062_H
#define THREADPRIORITY_T3576530062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadPriority
struct  ThreadPriority_t3576530062 
{
public:
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadPriority_t3576530062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T3576530062_H
#ifndef STRINGCOMPARISON_T3113500776_H
#define STRINGCOMPARISON_T3113500776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparison
struct  StringComparison_t3113500776 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringComparison_t3113500776, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARISON_T3113500776_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef ENUMERATOR_T1652763431_H
#define ENUMERATOR_T1652763431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t1652763431 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2480214856 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t424198038  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1652763431, ___dictionary_0)); }
	inline Dictionary_2_t2480214856 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2480214856 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2480214856 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1652763431, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1652763431, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1652763431, ___current_3)); }
	inline KeyValuePair_2_t424198038  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t424198038 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t424198038  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1652763431_H
#ifndef ENUMERATOR_T232651091_H
#define ENUMERATOR_T232651091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct  Enumerator_t232651091 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1060102516 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3299052994  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t232651091, ___dictionary_0)); }
	inline Dictionary_2_t1060102516 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1060102516 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1060102516 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t232651091, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t232651091, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t232651091, ___current_3)); }
	inline KeyValuePair_2_t3299052994  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3299052994 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3299052994  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T232651091_H
#ifndef DOWNLOADHANDLER_T3833085680_H
#define DOWNLOADHANDLER_T3833085680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t3833085680  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t3833085680, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t3833085680_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t3833085680_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T3833085680_H
#ifndef UPLOADHANDLER_T200615742_H
#define UPLOADHANDLER_T200615742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t200615742  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t200615742, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t200615742_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t200615742_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T200615742_H
#ifndef UNITYWEBREQUEST_T84658793_H
#define UNITYWEBREQUEST_T84658793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t84658793  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t84658793, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_t84658793, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityWebRequest_t84658793, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t84658793_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t84658793_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_7;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_8;
};
#endif // UNITYWEBREQUEST_T84658793_H
#ifndef DOWNLOADHANDLERBUFFER_T217099739_H
#define DOWNLOADHANDLERBUFFER_T217099739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerBuffer
struct  DownloadHandlerBuffer_t217099739  : public DownloadHandler_t3833085680
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t217099739_marshaled_pinvoke : public DownloadHandler_t3833085680_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t217099739_marshaled_com : public DownloadHandler_t3833085680_marshaled_com
{
};
#endif // DOWNLOADHANDLERBUFFER_T217099739_H
#ifndef UNITYWEBREQUESTASYNCOPERATION_T1011823049_H
#define UNITYWEBREQUESTASYNCOPERATION_T1011823049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct  UnityWebRequestAsyncOperation_t1011823049  : public AsyncOperation_t1468153686
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::m_webRequest
	UnityWebRequest_t84658793 * ___m_webRequest_2;

public:
	inline static int32_t get_offset_of_m_webRequest_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_t1011823049, ___m_webRequest_2)); }
	inline UnityWebRequest_t84658793 * get_m_webRequest_2() const { return ___m_webRequest_2; }
	inline UnityWebRequest_t84658793 ** get_address_of_m_webRequest_2() { return &___m_webRequest_2; }
	inline void set_m_webRequest_2(UnityWebRequest_t84658793 * value)
	{
		___m_webRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_webRequest_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t1011823049_marshaled_pinvoke : public AsyncOperation_t1468153686_marshaled_pinvoke
{
	UnityWebRequest_t84658793_marshaled_pinvoke ___m_webRequest_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t1011823049_marshaled_com : public AsyncOperation_t1468153686_marshaled_com
{
	UnityWebRequest_t84658793_marshaled_com* ___m_webRequest_2;
};
#endif // UNITYWEBREQUESTASYNCOPERATION_T1011823049_H
#ifndef TEXTURE_T2119925672_H
#define TEXTURE_T2119925672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2119925672  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2119925672_H
#ifndef WWW_T3599262362_H
#define WWW_T3599262362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3599262362  : public CustomYieldInstruction_t2273300952
{
public:
	// UnityEngine.ThreadPriority UnityEngine.WWW::<threadPriority>k__BackingField
	int32_t ___U3CthreadPriorityU3Ek__BackingField_0;
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t84658793 * ____uwr_1;
	// UnityEngine.AssetBundle UnityEngine.WWW::_assetBundle
	AssetBundle_t3848723172 * ____assetBundle_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::_responseHeaders
	Dictionary_2_t1060102516 * ____responseHeaders_3;

public:
	inline static int32_t get_offset_of_U3CthreadPriorityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WWW_t3599262362, ___U3CthreadPriorityU3Ek__BackingField_0)); }
	inline int32_t get_U3CthreadPriorityU3Ek__BackingField_0() const { return ___U3CthreadPriorityU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CthreadPriorityU3Ek__BackingField_0() { return &___U3CthreadPriorityU3Ek__BackingField_0; }
	inline void set_U3CthreadPriorityU3Ek__BackingField_0(int32_t value)
	{
		___U3CthreadPriorityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__uwr_1() { return static_cast<int32_t>(offsetof(WWW_t3599262362, ____uwr_1)); }
	inline UnityWebRequest_t84658793 * get__uwr_1() const { return ____uwr_1; }
	inline UnityWebRequest_t84658793 ** get_address_of__uwr_1() { return &____uwr_1; }
	inline void set__uwr_1(UnityWebRequest_t84658793 * value)
	{
		____uwr_1 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_1), value);
	}

	inline static int32_t get_offset_of__assetBundle_2() { return static_cast<int32_t>(offsetof(WWW_t3599262362, ____assetBundle_2)); }
	inline AssetBundle_t3848723172 * get__assetBundle_2() const { return ____assetBundle_2; }
	inline AssetBundle_t3848723172 ** get_address_of__assetBundle_2() { return &____assetBundle_2; }
	inline void set__assetBundle_2(AssetBundle_t3848723172 * value)
	{
		____assetBundle_2 = value;
		Il2CppCodeGenWriteBarrier((&____assetBundle_2), value);
	}

	inline static int32_t get_offset_of__responseHeaders_3() { return static_cast<int32_t>(offsetof(WWW_t3599262362, ____responseHeaders_3)); }
	inline Dictionary_2_t1060102516 * get__responseHeaders_3() const { return ____responseHeaders_3; }
	inline Dictionary_2_t1060102516 ** get_address_of__responseHeaders_3() { return &____responseHeaders_3; }
	inline void set__responseHeaders_3(Dictionary_2_t1060102516 * value)
	{
		____responseHeaders_3 = value;
		Il2CppCodeGenWriteBarrier((&____responseHeaders_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3599262362_H
#ifndef UPLOADHANDLERRAW_T859941321_H
#define UPLOADHANDLERRAW_T859941321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandlerRaw
struct  UploadHandlerRaw_t859941321  : public UploadHandler_t200615742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t859941321_marshaled_pinvoke : public UploadHandler_t200615742_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t859941321_marshaled_com : public UploadHandler_t200615742_marshaled_com
{
};
#endif // UPLOADHANDLERRAW_T859941321_H
#ifndef TEXTURE2D_T3063074017_H
#define TEXTURE2D_T3063074017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3063074017  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3063074017_H
// System.Byte[]
struct ByteU5BU5D_t434619169  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1652763431  Dictionary_2_GetEnumerator_m3921559177_gshared (Dictionary_2_t2480214856 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t424198038  Enumerator_get_Current_m3081909716_gshared (Enumerator_t1652763431 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1473050447_gshared (KeyValuePair_2_t424198038 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2794047228_gshared (KeyValuePair_2_t424198038 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1781776524_gshared (Enumerator_t1652763431 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3192646211_gshared (Enumerator_t1652763431 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m900305041 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
extern "C"  UnityWebRequest_t84658793 * UnityWebRequest_Get_m100339720 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
extern "C"  UnityWebRequestAsyncOperation_t1011823049 * UnityWebRequest_SendWebRequest_m1881229276 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern "C"  UnityWebRequest_t84658793 * UnityWebRequest_Post_m1012623769 (RuntimeObject * __this /* static, unused */, String_t* p0, WWWForm_t815382151 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
extern "C"  void UnityWebRequest__ctor_m560934577 (UnityWebRequest_t84658793 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
extern "C"  void UploadHandlerRaw__ctor_m3147137550 (UploadHandlerRaw_t859941321 * __this, ByteU5BU5D_t434619169* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UploadHandler::set_contentType(System.String)
extern "C"  void UploadHandler_set_contentType_m3994784318 (UploadHandler_t200615742 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest_set_uploadHandler_m1238778139 (UnityWebRequest_t84658793 * __this, UploadHandler_t200615742 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m4281382071 (DownloadHandlerBuffer_t217099739 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
extern "C"  void UnityWebRequest_set_downloadHandler_m2868007698 (UnityWebRequest_t84658793 * __this, DownloadHandler_t3833085680 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2281908325(__this, method) ((  Enumerator_t232651091  (*) (Dictionary_2_t1060102516 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3921559177_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m3223534350(__this, method) ((  KeyValuePair_2_t3299052994  (*) (Enumerator_t232651091 *, const RuntimeMethod*))Enumerator_get_Current_m3081909716_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m2414193123(__this, method) ((  String_t* (*) (KeyValuePair_2_t3299052994 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1473050447_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1585841855(__this, method) ((  String_t* (*) (KeyValuePair_2_t3299052994 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2794047228_gshared)(__this, method)
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_SetRequestHeader_m1697762075 (UnityWebRequest_t84658793 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m3330050507(__this, method) ((  bool (*) (Enumerator_t232651091 *, const RuntimeMethod*))Enumerator_MoveNext_m1781776524_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m198833214(__this, method) ((  void (*) (Enumerator_t232651091 *, const RuntimeMethod*))Enumerator_Dispose_m3192646211_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
extern "C"  bool UnityWebRequest_get_isDone_m651063786 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
extern "C"  bool UnityWebRequest_get_isNetworkError_m1101974306 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C"  String_t* UnityWebRequest_get_error_m1462904086 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
extern "C"  int64_t UnityWebRequest_get_responseCode_m979005630 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::GetStatusCodeName(System.Int64)
extern "C"  String_t* WWW_GetStatusCodeName_m3112770414 (WWW_t3599262362 * __this, int64_t ___statusCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m3571813163 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Networking.UnityWebRequest::get_downloadProgress()
extern "C"  float UnityWebRequest_get_downloadProgress_m509608481 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern "C"  bool WWW_WaitUntilDoneIfPossible_m1227673390 (WWW_t3599262362 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t3833085680 * UnityWebRequest_get_downloadHandler_m1252135942 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.DownloadHandler::get_text()
extern "C"  String_t* DownloadHandler_get_text_m1277061073 (DownloadHandler_t3833085680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m1290530517 (Texture2D_t3063074017 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandler::get_data()
extern "C"  ByteU5BU5D_t434619169* DownloadHandler_get_data_m1102722116 (DownloadHandler_t3833085680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
extern "C"  bool ImageConversion_LoadImage_m268167230 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * p0, ByteU5BU5D_t434619169* p1, bool p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::CreateTextureFromDownloadedData(System.Boolean)
extern "C"  Texture2D_t3063074017 * WWW_CreateTextureFromDownloadedData_m4097086976 (WWW_t3599262362 * __this, bool ___markNonReadable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Networking.UnityWebRequest::get_uploadProgress()
extern "C"  float UnityWebRequest_get_uploadProgress_m1231594717 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.UnityWebRequest::get_url()
extern "C"  String_t* UnityWebRequest_get_url_m531070724 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
extern "C"  void UnityWebRequest_Dispose_m3265626382 (UnityWebRequest_t84658793 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_url()
extern "C"  String_t* WWW_get_url_m4143263653 (WWW_t3599262362 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String,System.StringComparison)
extern "C"  bool String_StartsWith_m3101960047 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3935077967 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C"  Encoding_t4004889433 * Encoding_get_UTF8_m3585116113 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::UnEscapeURL(System.String,System.Text.Encoding)
extern "C"  String_t* WWW_UnEscapeURL_m3473740146 (RuntimeObject * __this /* static, unused */, String_t* ___s0, Encoding_t4004889433 * ___e1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m1949547872 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWWTranscoder::URLDecode(System.String,System.Text.Encoding)
extern "C"  String_t* WWWTranscoder_URLDecode_m2453216676 (RuntimeObject * __this /* static, unused */, String_t* p0, Encoding_t4004889433 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m1114098236 (WWW_t3599262362 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		CustomYieldInstruction__ctor_m900305041(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_t84658793 * L_1 = UnityWebRequest_Get_m100339720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__uwr_1(L_1);
		UnityWebRequest_t84658793 * L_2 = __this->get__uwr_1();
		NullCheck(L_2);
		UnityWebRequest_SendWebRequest_m1881229276(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C"  void WWW__ctor_m1960873430 (WWW_t3599262362 * __this, String_t* ___url0, WWWForm_t815382151 * ___form1, const RuntimeMethod* method)
{
	{
		CustomYieldInstruction__ctor_m900305041(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		WWWForm_t815382151 * L_1 = ___form1;
		UnityWebRequest_t84658793 * L_2 = UnityWebRequest_Post_m1012623769(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set__uwr_1(L_2);
		UnityWebRequest_t84658793 * L_3 = __this->get__uwr_1();
		NullCheck(L_3);
		UnityWebRequest_SendWebRequest_m1881229276(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void WWW__ctor_m595852094 (WWW_t3599262362 * __this, String_t* ___url0, ByteU5BU5D_t434619169* ___postData1, Dictionary_2_t1060102516 * ___headers2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW__ctor_m595852094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	UploadHandler_t200615742 * V_1 = NULL;
	KeyValuePair_2_t3299052994  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t232651091  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B3_0 = NULL;
	{
		CustomYieldInstruction__ctor_m900305041(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t434619169* L_0 = ___postData1;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = _stringLiteral1175114534;
		goto IL_001c;
	}

IL_0017:
	{
		G_B3_0 = _stringLiteral2457803851;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_1 = ___url0;
		String_t* L_2 = V_0;
		UnityWebRequest_t84658793 * L_3 = (UnityWebRequest_t84658793 *)il2cpp_codegen_object_new(UnityWebRequest_t84658793_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m560934577(L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->set__uwr_1(L_3);
		ByteU5BU5D_t434619169* L_4 = ___postData1;
		UploadHandlerRaw_t859941321 * L_5 = (UploadHandlerRaw_t859941321 *)il2cpp_codegen_object_new(UploadHandlerRaw_t859941321_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m3147137550(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		UploadHandler_t200615742 * L_6 = V_1;
		NullCheck(L_6);
		UploadHandler_set_contentType_m3994784318(L_6, _stringLiteral3042054665, /*hidden argument*/NULL);
		UnityWebRequest_t84658793 * L_7 = __this->get__uwr_1();
		UploadHandler_t200615742 * L_8 = V_1;
		NullCheck(L_7);
		UnityWebRequest_set_uploadHandler_m1238778139(L_7, L_8, /*hidden argument*/NULL);
		UnityWebRequest_t84658793 * L_9 = __this->get__uwr_1();
		DownloadHandlerBuffer_t217099739 * L_10 = (DownloadHandlerBuffer_t217099739 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t217099739_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m4281382071(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityWebRequest_set_downloadHandler_m2868007698(L_9, L_10, /*hidden argument*/NULL);
		Dictionary_2_t1060102516 * L_11 = ___headers2;
		NullCheck(L_11);
		Enumerator_t232651091  L_12 = Dictionary_2_GetEnumerator_m2281908325(L_11, /*hidden argument*/Dictionary_2_GetEnumerator_m2281908325_RuntimeMethod_var);
		V_3 = L_12;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0086;
		}

IL_0065:
		{
			KeyValuePair_2_t3299052994  L_13 = Enumerator_get_Current_m3223534350((&V_3), /*hidden argument*/Enumerator_get_Current_m3223534350_RuntimeMethod_var);
			V_2 = L_13;
			UnityWebRequest_t84658793 * L_14 = __this->get__uwr_1();
			String_t* L_15 = KeyValuePair_2_get_Key_m2414193123((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m2414193123_RuntimeMethod_var);
			String_t* L_16 = KeyValuePair_2_get_Value_m1585841855((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m1585841855_RuntimeMethod_var);
			NullCheck(L_14);
			UnityWebRequest_SetRequestHeader_m1697762075(L_14, L_15, L_16, /*hidden argument*/NULL);
		}

IL_0086:
		{
			bool L_17 = Enumerator_MoveNext_m3330050507((&V_3), /*hidden argument*/Enumerator_MoveNext_m3330050507_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_0065;
			}
		}

IL_0092:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_0097);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0097;
	}

FINALLY_0097:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m198833214((&V_3), /*hidden argument*/Enumerator_Dispose_m198833214_RuntimeMethod_var);
		IL2CPP_END_FINALLY(151)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(151)
	{
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_00a5:
	{
		UnityWebRequest_t84658793 * L_18 = __this->get__uwr_1();
		NullCheck(L_18);
		UnityWebRequest_SendWebRequest_m1881229276(L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m607331358 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_error_m607331358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m651063786(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = (String_t*)NULL;
		goto IL_0087;
	}

IL_0018:
	{
		UnityWebRequest_t84658793 * L_2 = __this->get__uwr_1();
		NullCheck(L_2);
		bool L_3 = UnityWebRequest_get_isNetworkError_m1101974306(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		UnityWebRequest_t84658793 * L_4 = __this->get__uwr_1();
		NullCheck(L_4);
		String_t* L_5 = UnityWebRequest_get_error_m1462904086(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0087;
	}

IL_0039:
	{
		UnityWebRequest_t84658793 * L_6 = __this->get__uwr_1();
		NullCheck(L_6);
		int64_t L_7 = UnityWebRequest_get_responseCode_m979005630(L_6, /*hidden argument*/NULL);
		if ((((int64_t)L_7) < ((int64_t)(((int64_t)((int64_t)((int32_t)400)))))))
		{
			goto IL_0080;
		}
	}
	{
		UnityWebRequest_t84658793 * L_8 = __this->get__uwr_1();
		NullCheck(L_8);
		int64_t L_9 = UnityWebRequest_get_responseCode_m979005630(L_8, /*hidden argument*/NULL);
		int64_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int64_t1541278839_il2cpp_TypeInfo_var, &L_10);
		UnityWebRequest_t84658793 * L_12 = __this->get__uwr_1();
		NullCheck(L_12);
		int64_t L_13 = UnityWebRequest_get_responseCode_m979005630(L_12, /*hidden argument*/NULL);
		String_t* L_14 = WWW_GetStatusCodeName_m3112770414(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m3571813163(NULL /*static, unused*/, _stringLiteral1792448719, L_11, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_0087;
	}

IL_0080:
	{
		V_0 = (String_t*)NULL;
		goto IL_0087;
	}

IL_0087:
	{
		String_t* L_16 = V_0;
		return L_16;
	}
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C"  bool WWW_get_isDone_m1414930012 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m651063786(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.WWW::get_progress()
extern "C"  float WWW_get_progress_m2371339333 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		float L_1 = UnityWebRequest_get_downloadProgress_m509608481(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		V_0 = (0.0f);
	}

IL_001e:
	{
		float L_3 = V_0;
		V_1 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// System.String UnityEngine.WWW::get_text()
extern "C"  String_t* WWW_get_text_m3297283267 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_text_m3297283267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DownloadHandler_t3833085680 * V_1 = NULL;
	{
		bool L_0 = WWW_WaitUntilDoneIfPossible_m1227673390(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		V_0 = _stringLiteral4185710657;
		goto IL_005b;
	}

IL_0017:
	{
		UnityWebRequest_t84658793 * L_1 = __this->get__uwr_1();
		NullCheck(L_1);
		bool L_2 = UnityWebRequest_get_isNetworkError_m1101974306(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = _stringLiteral4185710657;
		goto IL_005b;
	}

IL_0032:
	{
		UnityWebRequest_t84658793 * L_3 = __this->get__uwr_1();
		NullCheck(L_3);
		DownloadHandler_t3833085680 * L_4 = UnityWebRequest_get_downloadHandler_m1252135942(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DownloadHandler_t3833085680 * L_5 = V_1;
		if (L_5)
		{
			goto IL_004f;
		}
	}
	{
		V_0 = _stringLiteral4185710657;
		goto IL_005b;
	}

IL_004f:
	{
		DownloadHandler_t3833085680 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = DownloadHandler_get_text_m1277061073(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_005b;
	}

IL_005b:
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.Texture2D UnityEngine.WWW::CreateTextureFromDownloadedData(System.Boolean)
extern "C"  Texture2D_t3063074017 * WWW_CreateTextureFromDownloadedData_m4097086976 (WWW_t3599262362 * __this, bool ___markNonReadable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_CreateTextureFromDownloadedData_m4097086976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3063074017 * V_0 = NULL;
	DownloadHandler_t3833085680 * V_1 = NULL;
	Texture2D_t3063074017 * V_2 = NULL;
	{
		bool L_0 = WWW_WaitUntilDoneIfPossible_m1227673390(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Texture2D_t3063074017 * L_1 = (Texture2D_t3063074017 *)il2cpp_codegen_object_new(Texture2D_t3063074017_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1290530517(L_1, 2, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0066;
	}

IL_0019:
	{
		UnityWebRequest_t84658793 * L_2 = __this->get__uwr_1();
		NullCheck(L_2);
		bool L_3 = UnityWebRequest_get_isNetworkError_m1101974306(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		V_0 = (Texture2D_t3063074017 *)NULL;
		goto IL_0066;
	}

IL_0030:
	{
		UnityWebRequest_t84658793 * L_4 = __this->get__uwr_1();
		NullCheck(L_4);
		DownloadHandler_t3833085680 * L_5 = UnityWebRequest_get_downloadHandler_m1252135942(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DownloadHandler_t3833085680 * L_6 = V_1;
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		V_0 = (Texture2D_t3063074017 *)NULL;
		goto IL_0066;
	}

IL_0049:
	{
		Texture2D_t3063074017 * L_7 = (Texture2D_t3063074017 *)il2cpp_codegen_object_new(Texture2D_t3063074017_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1290530517(L_7, 2, 2, /*hidden argument*/NULL);
		V_2 = L_7;
		Texture2D_t3063074017 * L_8 = V_2;
		DownloadHandler_t3833085680 * L_9 = V_1;
		NullCheck(L_9);
		ByteU5BU5D_t434619169* L_10 = DownloadHandler_get_data_m1102722116(L_9, /*hidden argument*/NULL);
		bool L_11 = ___markNonReadable0;
		ImageConversion_LoadImage_m268167230(NULL /*static, unused*/, L_8, L_10, L_11, /*hidden argument*/NULL);
		Texture2D_t3063074017 * L_12 = V_2;
		V_0 = L_12;
		goto IL_0066;
	}

IL_0066:
	{
		Texture2D_t3063074017 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C"  Texture2D_t3063074017 * WWW_get_texture_m400216147 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	Texture2D_t3063074017 * V_0 = NULL;
	{
		Texture2D_t3063074017 * L_0 = WWW_CreateTextureFromDownloadedData_m4097086976(__this, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Texture2D_t3063074017 * L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.WWW::get_uploadProgress()
extern "C"  float WWW_get_uploadProgress_m1834202512 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		float L_1 = UnityWebRequest_get_uploadProgress_m1231594717(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		V_0 = (0.0f);
	}

IL_001e:
	{
		float L_3 = V_0;
		V_1 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// System.String UnityEngine.WWW::get_url()
extern "C"  String_t* WWW_get_url_m4143263653 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		String_t* L_1 = UnityWebRequest_get_url_m531070724(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.WWW::get_keepWaiting()
extern "C"  bool WWW_get_keepWaiting_m2502766189 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m651063786(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C"  void WWW_Dispose_m2553229472 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		UnityWebRequest_Dispose_m3265626382(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern "C"  bool WWW_WaitUntilDoneIfPossible_m1227673390 (WWW_t3599262362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_WaitUntilDoneIfPossible_m1227673390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		UnityWebRequest_t84658793 * L_0 = __this->get__uwr_1();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m651063786(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_005f;
	}

IL_0018:
	{
		String_t* L_2 = WWW_get_url_m4143263653(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m3101960047(L_2, _stringLiteral801152855, 5, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		goto IL_0036;
	}

IL_0034:
	{
	}

IL_0036:
	{
		UnityWebRequest_t84658793 * L_4 = __this->get__uwr_1();
		NullCheck(L_4);
		bool L_5 = UnityWebRequest_get_isDone_m651063786(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_005f;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_LogError_m3935077967(NULL /*static, unused*/, _stringLiteral2731015071, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_005f;
	}

IL_005f:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.String UnityEngine.WWW::GetStatusCodeName(System.Int64)
extern "C"  String_t* WWW_GetStatusCodeName_m3112770414 (WWW_t3599262362 * __this, int64_t ___statusCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_GetStatusCodeName_m3112770414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int64_t L_0 = ___statusCode0;
		if ((((int64_t)L_0) < ((int64_t)(((int64_t)((int64_t)((int32_t)400)))))))
		{
			goto IL_006b;
		}
	}
	{
		int64_t L_1 = ___statusCode0;
		if ((((int64_t)L_1) > ((int64_t)(((int64_t)((int64_t)((int32_t)416)))))))
		{
			goto IL_006b;
		}
	}
	{
		int64_t L_2 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_2-(int64_t)(((int64_t)((int64_t)((int32_t)400))))))))))
		{
			case 0:
			{
				goto IL_01d9;
			}
			case 1:
			{
				goto IL_01e4;
			}
			case 2:
			{
				goto IL_01ef;
			}
			case 3:
			{
				goto IL_01fa;
			}
			case 4:
			{
				goto IL_0205;
			}
			case 5:
			{
				goto IL_0210;
			}
			case 6:
			{
				goto IL_021b;
			}
			case 7:
			{
				goto IL_0226;
			}
			case 8:
			{
				goto IL_0231;
			}
			case 9:
			{
				goto IL_023c;
			}
			case 10:
			{
				goto IL_0247;
			}
			case 11:
			{
				goto IL_0252;
			}
			case 12:
			{
				goto IL_025d;
			}
			case 13:
			{
				goto IL_0268;
			}
			case 14:
			{
				goto IL_0273;
			}
			case 15:
			{
				goto IL_027e;
			}
			case 16:
			{
				goto IL_0289;
			}
		}
	}

IL_006b:
	{
		int64_t L_3 = ___statusCode0;
		if ((((int64_t)L_3) < ((int64_t)(((int64_t)((int64_t)((int32_t)200)))))))
		{
			goto IL_00ad;
		}
	}
	{
		int64_t L_4 = ___statusCode0;
		if ((((int64_t)L_4) > ((int64_t)(((int64_t)((int64_t)((int32_t)206)))))))
		{
			goto IL_00ad;
		}
	}
	{
		int64_t L_5 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_5-(int64_t)(((int64_t)((int64_t)((int32_t)200))))))))))
		{
			case 0:
			{
				goto IL_013f;
			}
			case 1:
			{
				goto IL_014a;
			}
			case 2:
			{
				goto IL_0155;
			}
			case 3:
			{
				goto IL_0160;
			}
			case 4:
			{
				goto IL_016b;
			}
			case 5:
			{
				goto IL_0176;
			}
			case 6:
			{
				goto IL_0181;
			}
		}
	}

IL_00ad:
	{
		int64_t L_6 = ___statusCode0;
		if ((((int64_t)L_6) < ((int64_t)(((int64_t)((int64_t)((int32_t)300)))))))
		{
			goto IL_00f3;
		}
	}
	{
		int64_t L_7 = ___statusCode0;
		if ((((int64_t)L_7) > ((int64_t)(((int64_t)((int64_t)((int32_t)307)))))))
		{
			goto IL_00f3;
		}
	}
	{
		int64_t L_8 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_8-(int64_t)(((int64_t)((int64_t)((int32_t)300))))))))))
		{
			case 0:
			{
				goto IL_018c;
			}
			case 1:
			{
				goto IL_0197;
			}
			case 2:
			{
				goto IL_01a2;
			}
			case 3:
			{
				goto IL_01ad;
			}
			case 4:
			{
				goto IL_01b8;
			}
			case 5:
			{
				goto IL_01c3;
			}
			case 6:
			{
				goto IL_00f3;
			}
			case 7:
			{
				goto IL_01ce;
			}
		}
	}

IL_00f3:
	{
		int64_t L_9 = ___statusCode0;
		if ((((int64_t)L_9) < ((int64_t)(((int64_t)((int64_t)((int32_t)500)))))))
		{
			goto IL_0131;
		}
	}
	{
		int64_t L_10 = ___statusCode0;
		if ((((int64_t)L_10) > ((int64_t)(((int64_t)((int64_t)((int32_t)505)))))))
		{
			goto IL_0131;
		}
	}
	{
		int64_t L_11 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_11-(int64_t)(((int64_t)((int64_t)((int32_t)500))))))))))
		{
			case 0:
			{
				goto IL_029f;
			}
			case 1:
			{
				goto IL_02aa;
			}
			case 2:
			{
				goto IL_02b5;
			}
			case 3:
			{
				goto IL_02c0;
			}
			case 4:
			{
				goto IL_02cb;
			}
			case 5:
			{
				goto IL_02d6;
			}
		}
	}

IL_0131:
	{
		int64_t L_12 = ___statusCode0;
		if ((((int64_t)L_12) == ((int64_t)(((int64_t)((int64_t)((int32_t)41)))))))
		{
			goto IL_0294;
		}
	}
	{
		goto IL_02e1;
	}

IL_013f:
	{
		V_0 = _stringLiteral926182066;
		goto IL_02ec;
	}

IL_014a:
	{
		V_0 = _stringLiteral775335656;
		goto IL_02ec;
	}

IL_0155:
	{
		V_0 = _stringLiteral3937582660;
		goto IL_02ec;
	}

IL_0160:
	{
		V_0 = _stringLiteral1227851613;
		goto IL_02ec;
	}

IL_016b:
	{
		V_0 = _stringLiteral2175902511;
		goto IL_02ec;
	}

IL_0176:
	{
		V_0 = _stringLiteral2575790786;
		goto IL_02ec;
	}

IL_0181:
	{
		V_0 = _stringLiteral1143792069;
		goto IL_02ec;
	}

IL_018c:
	{
		V_0 = _stringLiteral4231565991;
		goto IL_02ec;
	}

IL_0197:
	{
		V_0 = _stringLiteral1795542722;
		goto IL_02ec;
	}

IL_01a2:
	{
		V_0 = _stringLiteral3115644037;
		goto IL_02ec;
	}

IL_01ad:
	{
		V_0 = _stringLiteral3025402805;
		goto IL_02ec;
	}

IL_01b8:
	{
		V_0 = _stringLiteral3609986982;
		goto IL_02ec;
	}

IL_01c3:
	{
		V_0 = _stringLiteral777220928;
		goto IL_02ec;
	}

IL_01ce:
	{
		V_0 = _stringLiteral656468698;
		goto IL_02ec;
	}

IL_01d9:
	{
		V_0 = _stringLiteral1002768097;
		goto IL_02ec;
	}

IL_01e4:
	{
		V_0 = _stringLiteral1073510373;
		goto IL_02ec;
	}

IL_01ef:
	{
		V_0 = _stringLiteral3832650110;
		goto IL_02ec;
	}

IL_01fa:
	{
		V_0 = _stringLiteral3608717552;
		goto IL_02ec;
	}

IL_0205:
	{
		V_0 = _stringLiteral1983486527;
		goto IL_02ec;
	}

IL_0210:
	{
		V_0 = _stringLiteral1661384898;
		goto IL_02ec;
	}

IL_021b:
	{
		V_0 = _stringLiteral528683789;
		goto IL_02ec;
	}

IL_0226:
	{
		V_0 = _stringLiteral2135323971;
		goto IL_02ec;
	}

IL_0231:
	{
		V_0 = _stringLiteral1376671957;
		goto IL_02ec;
	}

IL_023c:
	{
		V_0 = _stringLiteral1556635899;
		goto IL_02ec;
	}

IL_0247:
	{
		V_0 = _stringLiteral2042537999;
		goto IL_02ec;
	}

IL_0252:
	{
		V_0 = _stringLiteral2792834529;
		goto IL_02ec;
	}

IL_025d:
	{
		V_0 = _stringLiteral4232457407;
		goto IL_02ec;
	}

IL_0268:
	{
		V_0 = _stringLiteral4275030961;
		goto IL_02ec;
	}

IL_0273:
	{
		V_0 = _stringLiteral1203508084;
		goto IL_02ec;
	}

IL_027e:
	{
		V_0 = _stringLiteral434391053;
		goto IL_02ec;
	}

IL_0289:
	{
		V_0 = _stringLiteral2165137379;
		goto IL_02ec;
	}

IL_0294:
	{
		V_0 = _stringLiteral3648165571;
		goto IL_02ec;
	}

IL_029f:
	{
		V_0 = _stringLiteral642399291;
		goto IL_02ec;
	}

IL_02aa:
	{
		V_0 = _stringLiteral573233689;
		goto IL_02ec;
	}

IL_02b5:
	{
		V_0 = _stringLiteral1180654445;
		goto IL_02ec;
	}

IL_02c0:
	{
		V_0 = _stringLiteral787061741;
		goto IL_02ec;
	}

IL_02cb:
	{
		V_0 = _stringLiteral398941627;
		goto IL_02ec;
	}

IL_02d6:
	{
		V_0 = _stringLiteral1905332390;
		goto IL_02ec;
	}

IL_02e1:
	{
		V_0 = _stringLiteral4185710657;
		goto IL_02ec;
	}

IL_02ec:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.String UnityEngine.WWW::UnEscapeURL(System.String)
extern "C"  String_t* WWW_UnEscapeURL_m2890420115 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_UnEscapeURL_m2890420115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t4004889433_il2cpp_TypeInfo_var);
		Encoding_t4004889433 * L_1 = Encoding_get_UTF8_m3585116113(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = WWW_UnEscapeURL_m3473740146(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.WWW::UnEscapeURL(System.String,System.Text.Encoding)
extern "C"  String_t* WWW_UnEscapeURL_m3473740146 (RuntimeObject * __this /* static, unused */, String_t* ___s0, Encoding_t4004889433 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_UnEscapeURL_m3473740146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___s0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = (String_t*)NULL;
		goto IL_003e;
	}

IL_000e:
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m1949547872(L_1, ((int32_t)37), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_3 = ___s0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m1949547872(L_3, ((int32_t)43), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_5 = ___s0;
		V_0 = L_5;
		goto IL_003e;
	}

IL_0031:
	{
		String_t* L_6 = ___s0;
		Encoding_t4004889433 * L_7 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t3195377113_il2cpp_TypeInfo_var);
		String_t* L_8 = WWWTranscoder_URLDecode_m2453216676(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_003e;
	}

IL_003e:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
