﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Func`2<System.Reflection.Assembly,System.String>
struct Func_2_t3060583653;
// System.Predicate`1<System.Type>
struct Predicate_1_t3570888326;
// System.String
struct String_t;
// System.Predicate`1<System.Reflection.Assembly>
struct Predicate_1_t425819614;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t890753613;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// MagicArsenal.MagicLoopScript
struct MagicLoopScript_t2356792626;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1069640613;
// System.Object[]
struct ObjectU5BU5D_t2737604620;
// Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding
struct TargetBinding_t1523791741;
// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey0
struct U3CScanFieldsU3Ec__AnonStorey0_t1209448285;
// CinemachineExampleWindow
struct CinemachineExampleWindow_t2933416263;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t32224225;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1954517238;
// DecalDestroyer
struct DecalDestroyer_t2158806118;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434;
// UnityEngine.UI.Image
struct Image_t2816987602;
// UnityEngine.UI.Text
struct Text_t1790657652;
// ProvinceScript
struct ProvinceScript_t1528091046;
// Province
struct Province_t1688297463;
// ExtinguishableFire
struct ExtinguishableFire_t2564158560;
// System.Collections.Generic.HashSet`1<UnityEngine.Object>
struct HashSet_1_t873097153;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t2557508663;
// Cinemachine.CinemachineBrain
struct CinemachineBrain_t679733552;
// Cinemachine.Blackboard.Reactor/BlackboardExpression/Line[]
struct LineU5BU5D_t2399996771;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Void
struct Void_t653366341;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// UnityEngine.Collider
struct Collider_t1485601975;
// UnityEngine.Object
struct Object_t692178351;
// Cinemachine.CinemachineCore/AxisInputDelegate
struct AxisInputDelegate_t2029212819;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain>
struct List_1_t4094327855;
// System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera>
struct List_1_t2897348067;
// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCore/UpdateStatus>
struct Dictionary_2_t3112813203;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;
// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/OnLeafFieldDelegate
struct OnLeafFieldDelegate_t4271637819;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// Cinemachine.NoiseSettings/TransformNoiseParams[]
struct TransformNoiseParamsU5BU5D_t4184490513;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t2074033451;
// UnityEngine.AnimationClip
struct AnimationClip_t1145879031;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t1461289625;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_t1943312966;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t1733651323;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t2389961773;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t1609610637;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t3874041483;
// Cinemachine.Blackboard.Reactor/TargetModifier[]
struct TargetModifierU5BU5D_t2219681128;
// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_t2803177733;
// Cinemachine.CinemachineFreeLook
struct CinemachineFreeLook_t411363320;
// UnityEngine.PostProcessing.PostProcessingProfile
struct PostProcessingProfile_t1367512122;
// UnityEngine.PostProcessing.PostProcessingBehaviour
struct PostProcessingBehaviour_t1299966383;
// System.Action
struct Action_t370180854;
// System.String[]
struct StringU5BU5D_t2511808107;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_t2642749565;
// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate
struct OnPostPipelineStageDelegate_t938695173;
// LevelTwoScript
struct LevelTwoScript_t1243006306;
// TokenHolderLegacy
struct TokenHolderLegacy_t1380075429;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2248283753;
// UnityEngine.Camera
struct Camera_t2839736942;
// UnityEngine.Light
struct Light_t1775228881;
// UnityEngine.RectTransform
struct RectTransform_t859616204;
// UnityEngine.Camera[]
struct CameraU5BU5D_t4045078555;
// UnityEngine.UI.RawImage
struct RawImage_t88488728;
// CapturedDataInput
struct CapturedDataInput_t2616152122;
// UnityEngine.UI.InputField
struct InputField_t3123460221;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t602728325;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t4274210507;
// MagicArsenal.MagicFireProjectile
struct MagicFireProjectile_t3063101194;
// MagicProjectileScript
struct MagicProjectileScript_t1654175834;
// UnityEngine.Transform
struct Transform_t362059596;
// MagicArsenal.MagicButtonScript
struct MagicButtonScript_t1297805390;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t27653605;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t892035751;
// UnityEngine.LineRenderer
struct LineRenderer_t2362162701;
// UnityEngine.UI.Slider
struct Slider_t1188261235;
// EffectAttachPoint[]
struct EffectAttachPointU5BU5D_t1753991996;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1417780692;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1676974086;
// ParticleExamples[]
struct ParticleExamplesU5BU5D_t3470023393;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1867887001;
// UnityEngine.Animator
struct Animator_t2768715325;
// GunAim
struct GunAim_t211531317;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;

struct Line_t1381376710_marshaled_pinvoke;
struct Line_t1381376710_marshaled_com;
struct AnimationCurve_t2757045165_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef REFLECTIONHELPERS_T1786918968_H
#define REFLECTIONHELPERS_T1786918968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.ReflectionHelpers
struct  ReflectionHelpers_t1786918968  : public RuntimeObject
{
public:

public:
};

struct ReflectionHelpers_t1786918968_StaticFields
{
public:
	// System.Func`2<System.Reflection.Assembly,System.String> Cinemachine.Utility.ReflectionHelpers::<>f__am$cache0
	Func_2_t3060583653 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReflectionHelpers_t1786918968_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3060583653 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3060583653 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3060583653 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONHELPERS_T1786918968_H
#ifndef U3CGETTYPESINASSEMBLYU3EC__ANONSTOREY0_T3109715705_H
#define U3CGETTYPESINASSEMBLYU3EC__ANONSTOREY0_T3109715705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.ReflectionHelpers/<GetTypesInAssembly>c__AnonStorey0
struct  U3CGetTypesInAssemblyU3Ec__AnonStorey0_t3109715705  : public RuntimeObject
{
public:
	// System.Predicate`1<System.Type> Cinemachine.Utility.ReflectionHelpers/<GetTypesInAssembly>c__AnonStorey0::predicate
	Predicate_1_t3570888326 * ___predicate_0;

public:
	inline static int32_t get_offset_of_predicate_0() { return static_cast<int32_t>(offsetof(U3CGetTypesInAssemblyU3Ec__AnonStorey0_t3109715705, ___predicate_0)); }
	inline Predicate_1_t3570888326 * get_predicate_0() const { return ___predicate_0; }
	inline Predicate_1_t3570888326 ** get_address_of_predicate_0() { return &___predicate_0; }
	inline void set_predicate_0(Predicate_1_t3570888326 * value)
	{
		___predicate_0 = value;
		Il2CppCodeGenWriteBarrier((&___predicate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTYPESINASSEMBLYU3EC__ANONSTOREY0_T3109715705_H
#ifndef U3CGETTYPEINALLLOADEDASSEMBLIESU3EC__ANONSTOREY1_T2664077166_H
#define U3CGETTYPEINALLLOADEDASSEMBLIESU3EC__ANONSTOREY1_T2664077166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.ReflectionHelpers/<GetTypeInAllLoadedAssemblies>c__AnonStorey1
struct  U3CGetTypeInAllLoadedAssembliesU3Ec__AnonStorey1_t2664077166  : public RuntimeObject
{
public:
	// System.String Cinemachine.Utility.ReflectionHelpers/<GetTypeInAllLoadedAssemblies>c__AnonStorey1::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(U3CGetTypeInAllLoadedAssembliesU3Ec__AnonStorey1_t2664077166, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTYPEINALLLOADEDASSEMBLIESU3EC__ANONSTOREY1_T2664077166_H
#ifndef U3CGETTYPESINLOADEDASSEMBLIESU3EC__ANONSTOREY2_T1670705979_H
#define U3CGETTYPESINLOADEDASSEMBLIESU3EC__ANONSTOREY2_T1670705979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.ReflectionHelpers/<GetTypesInLoadedAssemblies>c__AnonStorey2
struct  U3CGetTypesInLoadedAssembliesU3Ec__AnonStorey2_t1670705979  : public RuntimeObject
{
public:
	// System.Predicate`1<System.Reflection.Assembly> Cinemachine.Utility.ReflectionHelpers/<GetTypesInLoadedAssemblies>c__AnonStorey2::assemblyPredicate
	Predicate_1_t425819614 * ___assemblyPredicate_0;

public:
	inline static int32_t get_offset_of_assemblyPredicate_0() { return static_cast<int32_t>(offsetof(U3CGetTypesInLoadedAssembliesU3Ec__AnonStorey2_t1670705979, ___assemblyPredicate_0)); }
	inline Predicate_1_t425819614 * get_assemblyPredicate_0() const { return ___assemblyPredicate_0; }
	inline Predicate_1_t425819614 ** get_address_of_assemblyPredicate_0() { return &___assemblyPredicate_0; }
	inline void set_assemblyPredicate_0(Predicate_1_t425819614 * value)
	{
		___assemblyPredicate_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyPredicate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTYPESINLOADEDASSEMBLIESU3EC__ANONSTOREY2_T1670705979_H
#ifndef UNITYVECTOREXTENSIONS_T2558878399_H
#define UNITYVECTOREXTENSIONS_T2558878399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityVectorExtensions
struct  UnityVectorExtensions_t2558878399  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVECTOREXTENSIONS_T2558878399_H
#ifndef UNITYQUATERNIONEXTENSIONS_T834130850_H
#define UNITYQUATERNIONEXTENSIONS_T834130850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityQuaternionExtensions
struct  UnityQuaternionExtensions_t834130850  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYQUATERNIONEXTENSIONS_T834130850_H
#ifndef UNITYRECTEXTENSIONS_T296430228_H
#define UNITYRECTEXTENSIONS_T296430228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityRectExtensions
struct  UnityRectExtensions_t296430228  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRECTEXTENSIONS_T296430228_H
#ifndef BLACKBOARD_T108108094_H
#define BLACKBOARD_T108108094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Blackboard
struct  Blackboard_t108108094  : public RuntimeObject
{
public:
	// System.String Cinemachine.Blackboard.Blackboard::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> Cinemachine.Blackboard.Blackboard::mValues
	Dictionary_2_t890753613 * ___mValues_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Blackboard_t108108094, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_mValues_2() { return static_cast<int32_t>(offsetof(Blackboard_t108108094, ___mValues_2)); }
	inline Dictionary_2_t890753613 * get_mValues_2() const { return ___mValues_2; }
	inline Dictionary_2_t890753613 ** get_address_of_mValues_2() { return &___mValues_2; }
	inline void set_mValues_2(Dictionary_2_t890753613 * value)
	{
		___mValues_2 = value;
		Il2CppCodeGenWriteBarrier((&___mValues_2), value);
	}
};

struct Blackboard_t108108094_StaticFields
{
public:
	// Cinemachine.Blackboard.Blackboard Cinemachine.Blackboard.Blackboard::CinemachineBlackboard
	Blackboard_t108108094 * ___CinemachineBlackboard_0;

public:
	inline static int32_t get_offset_of_CinemachineBlackboard_0() { return static_cast<int32_t>(offsetof(Blackboard_t108108094_StaticFields, ___CinemachineBlackboard_0)); }
	inline Blackboard_t108108094 * get_CinemachineBlackboard_0() const { return ___CinemachineBlackboard_0; }
	inline Blackboard_t108108094 ** get_address_of_CinemachineBlackboard_0() { return &___CinemachineBlackboard_0; }
	inline void set_CinemachineBlackboard_0(Blackboard_t108108094 * value)
	{
		___CinemachineBlackboard_0 = value;
		Il2CppCodeGenWriteBarrier((&___CinemachineBlackboard_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLACKBOARD_T108108094_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef U3CEFFECTLOOPU3EC__ITERATOR0_T989201242_H
#define U3CEFFECTLOOPU3EC__ITERATOR0_T989201242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0
struct  U3CEffectLoopU3Ec__Iterator0_t989201242  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0::<effectPlayer>__0
	GameObject_t2557347079 * ___U3CeffectPlayerU3E__0_0;
	// MagicArsenal.MagicLoopScript MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0::$this
	MagicLoopScript_t2356792626 * ___U24this_1;
	// System.Object MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MagicArsenal.MagicLoopScript/<EffectLoop>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CeffectPlayerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CEffectLoopU3Ec__Iterator0_t989201242, ___U3CeffectPlayerU3E__0_0)); }
	inline GameObject_t2557347079 * get_U3CeffectPlayerU3E__0_0() const { return ___U3CeffectPlayerU3E__0_0; }
	inline GameObject_t2557347079 ** get_address_of_U3CeffectPlayerU3E__0_0() { return &___U3CeffectPlayerU3E__0_0; }
	inline void set_U3CeffectPlayerU3E__0_0(GameObject_t2557347079 * value)
	{
		___U3CeffectPlayerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeffectPlayerU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEffectLoopU3Ec__Iterator0_t989201242, ___U24this_1)); }
	inline MagicLoopScript_t2356792626 * get_U24this_1() const { return ___U24this_1; }
	inline MagicLoopScript_t2356792626 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MagicLoopScript_t2356792626 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEffectLoopU3Ec__Iterator0_t989201242, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEffectLoopU3Ec__Iterator0_t989201242, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEffectLoopU3Ec__Iterator0_t989201242, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEFFECTLOOPU3EC__ITERATOR0_T989201242_H
#ifndef TARGETBINDING_T1523791741_H
#define TARGETBINDING_T1523791741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding
struct  TargetBinding_t1523791741  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo[] Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding::mTargetFieldInfo
	FieldInfoU5BU5D_t1069640613* ___mTargetFieldInfo_0;
	// System.Object[] Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding::mTargetFieldOwner
	ObjectU5BU5D_t2737604620* ___mTargetFieldOwner_1;
	// System.Single Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding::mInitialValue
	float ___mInitialValue_2;

public:
	inline static int32_t get_offset_of_mTargetFieldInfo_0() { return static_cast<int32_t>(offsetof(TargetBinding_t1523791741, ___mTargetFieldInfo_0)); }
	inline FieldInfoU5BU5D_t1069640613* get_mTargetFieldInfo_0() const { return ___mTargetFieldInfo_0; }
	inline FieldInfoU5BU5D_t1069640613** get_address_of_mTargetFieldInfo_0() { return &___mTargetFieldInfo_0; }
	inline void set_mTargetFieldInfo_0(FieldInfoU5BU5D_t1069640613* value)
	{
		___mTargetFieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFieldInfo_0), value);
	}

	inline static int32_t get_offset_of_mTargetFieldOwner_1() { return static_cast<int32_t>(offsetof(TargetBinding_t1523791741, ___mTargetFieldOwner_1)); }
	inline ObjectU5BU5D_t2737604620* get_mTargetFieldOwner_1() const { return ___mTargetFieldOwner_1; }
	inline ObjectU5BU5D_t2737604620** get_address_of_mTargetFieldOwner_1() { return &___mTargetFieldOwner_1; }
	inline void set_mTargetFieldOwner_1(ObjectU5BU5D_t2737604620* value)
	{
		___mTargetFieldOwner_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFieldOwner_1), value);
	}

	inline static int32_t get_offset_of_mInitialValue_2() { return static_cast<int32_t>(offsetof(TargetBinding_t1523791741, ___mInitialValue_2)); }
	inline float get_mInitialValue_2() const { return ___mInitialValue_2; }
	inline float* get_address_of_mInitialValue_2() { return &___mInitialValue_2; }
	inline void set_mInitialValue_2(float value)
	{
		___mInitialValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETBINDING_T1523791741_H
#ifndef U3CBINDTARGETU3EC__ANONSTOREY0_T4020504393_H
#define U3CBINDTARGETU3EC__ANONSTOREY0_T4020504393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding/<BindTarget>c__AnonStorey0
struct  U3CBindTargetU3Ec__AnonStorey0_t4020504393  : public RuntimeObject
{
public:
	// System.String Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding/<BindTarget>c__AnonStorey0::fieldName
	String_t* ___fieldName_0;
	// Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding/<BindTarget>c__AnonStorey0::binding
	TargetBinding_t1523791741 * ___binding_1;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(U3CBindTargetU3Ec__AnonStorey0_t4020504393, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_0), value);
	}

	inline static int32_t get_offset_of_binding_1() { return static_cast<int32_t>(offsetof(U3CBindTargetU3Ec__AnonStorey0_t4020504393, ___binding_1)); }
	inline TargetBinding_t1523791741 * get_binding_1() const { return ___binding_1; }
	inline TargetBinding_t1523791741 ** get_address_of_binding_1() { return &___binding_1; }
	inline void set_binding_1(TargetBinding_t1523791741 * value)
	{
		___binding_1 = value;
		Il2CppCodeGenWriteBarrier((&___binding_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBINDTARGETU3EC__ANONSTOREY0_T4020504393_H
#ifndef U3CSCANFIELDSU3EC__ANONSTOREY0_T1209448285_H
#define U3CSCANFIELDSU3EC__ANONSTOREY0_T1209448285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey0
struct  U3CScanFieldsU3Ec__AnonStorey0_t1209448285  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo[] Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey0::fields
	FieldInfoU5BU5D_t1069640613* ___fields_0;

public:
	inline static int32_t get_offset_of_fields_0() { return static_cast<int32_t>(offsetof(U3CScanFieldsU3Ec__AnonStorey0_t1209448285, ___fields_0)); }
	inline FieldInfoU5BU5D_t1069640613* get_fields_0() const { return ___fields_0; }
	inline FieldInfoU5BU5D_t1069640613** get_address_of_fields_0() { return &___fields_0; }
	inline void set_fields_0(FieldInfoU5BU5D_t1069640613* value)
	{
		___fields_0 = value;
		Il2CppCodeGenWriteBarrier((&___fields_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCANFIELDSU3EC__ANONSTOREY0_T1209448285_H
#ifndef U3CSCANFIELDSU3EC__ANONSTOREY1_T3194405036_H
#define U3CSCANFIELDSU3EC__ANONSTOREY1_T3194405036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey1
struct  U3CScanFieldsU3Ec__AnonStorey1_t3194405036  : public RuntimeObject
{
public:
	// System.Int32 Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey1::i
	int32_t ___i_0;
	// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey0 Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/<ScanFields>c__AnonStorey1::<>f__ref$0
	U3CScanFieldsU3Ec__AnonStorey0_t1209448285 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CScanFieldsU3Ec__AnonStorey1_t3194405036, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CScanFieldsU3Ec__AnonStorey1_t3194405036, ___U3CU3Ef__refU240_1)); }
	inline U3CScanFieldsU3Ec__AnonStorey0_t1209448285 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CScanFieldsU3Ec__AnonStorey0_t1209448285 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CScanFieldsU3Ec__AnonStorey0_t1209448285 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCANFIELDSU3EC__ANONSTOREY1_T3194405036_H
#ifndef PLAYABLEBEHAVIOUR_T3096602576_H
#define PLAYABLEBEHAVIOUR_T3096602576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t3096602576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T3096602576_H
#ifndef COMMONLISTCODE_T272715144_H
#define COMMONLISTCODE_T272715144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommonListCode
struct  CommonListCode_t272715144  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONLISTCODE_T272715144_H
#ifndef U3CONGUIU3EC__ANONSTOREY0_T1275810891_H
#define U3CONGUIU3EC__ANONSTOREY0_T1275810891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CinemachineExampleWindow/<OnGUI>c__AnonStorey0
struct  U3COnGUIU3Ec__AnonStorey0_t1275810891  : public RuntimeObject
{
public:
	// System.Single CinemachineExampleWindow/<OnGUI>c__AnonStorey0::maxWidth
	float ___maxWidth_0;
	// CinemachineExampleWindow CinemachineExampleWindow/<OnGUI>c__AnonStorey0::$this
	CinemachineExampleWindow_t2933416263 * ___U24this_1;

public:
	inline static int32_t get_offset_of_maxWidth_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey0_t1275810891, ___maxWidth_0)); }
	inline float get_maxWidth_0() const { return ___maxWidth_0; }
	inline float* get_address_of_maxWidth_0() { return &___maxWidth_0; }
	inline void set_maxWidth_0(float value)
	{
		___maxWidth_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey0_t1275810891, ___U24this_1)); }
	inline CinemachineExampleWindow_t2933416263 * get_U24this_1() const { return ___U24this_1; }
	inline CinemachineExampleWindow_t2933416263 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CinemachineExampleWindow_t2933416263 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY0_T1275810891_H
#ifndef GAUSSIANWINDOW1D_1_T3723530233_H
#define GAUSSIANWINDOW1D_1_T3723530233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector3>
struct  GaussianWindow1d_1_t3723530233  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector3U5BU5D_t32224225* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t2843050510* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t3723530233, ___mData_0)); }
	inline Vector3U5BU5D_t32224225* get_mData_0() const { return ___mData_0; }
	inline Vector3U5BU5D_t32224225** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector3U5BU5D_t32224225* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t3723530233, ___mKernel_1)); }
	inline SingleU5BU5D_t2843050510* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t2843050510** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t2843050510* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t3723530233, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t3723530233, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t3723530233, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T3723530233_H
#ifndef GAUSSIANWINDOW1D_1_T2440788680_H
#define GAUSSIANWINDOW1D_1_T2440788680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Quaternion>
struct  GaussianWindow1d_1_t2440788680  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	QuaternionU5BU5D_t1954517238* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t2843050510* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2440788680, ___mData_0)); }
	inline QuaternionU5BU5D_t1954517238* get_mData_0() const { return ___mData_0; }
	inline QuaternionU5BU5D_t1954517238** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(QuaternionU5BU5D_t1954517238* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2440788680, ___mKernel_1)); }
	inline SingleU5BU5D_t2843050510* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t2843050510** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t2843050510* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2440788680, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2440788680, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2440788680, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T2440788680_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3124061724_H
#define U3CSTARTU3EC__ITERATOR0_T3124061724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecalDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3124061724  : public RuntimeObject
{
public:
	// DecalDestroyer DecalDestroyer/<Start>c__Iterator0::$this
	DecalDestroyer_t2158806118 * ___U24this_0;
	// System.Object DecalDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DecalDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DecalDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3124061724, ___U24this_0)); }
	inline DecalDestroyer_t2158806118 * get_U24this_0() const { return ___U24this_0; }
	inline DecalDestroyer_t2158806118 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(DecalDestroyer_t2158806118 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3124061724, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3124061724, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3124061724, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3124061724_H
#ifndef GAUSSIANWINDOW1D_1_T2065110756_H
#define GAUSSIANWINDOW1D_1_T2065110756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector2>
struct  GaussianWindow1d_1_t2065110756  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector2U5BU5D_t1220531434* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t2843050510* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2065110756, ___mData_0)); }
	inline Vector2U5BU5D_t1220531434* get_mData_0() const { return ___mData_0; }
	inline Vector2U5BU5D_t1220531434** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector2U5BU5D_t1220531434* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2065110756, ___mKernel_1)); }
	inline SingleU5BU5D_t2843050510* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t2843050510** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t2843050510* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2065110756, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2065110756, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2065110756, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T2065110756_H
#ifndef PROVINCE_T1688297463_H
#define PROVINCE_T1688297463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Province
struct  Province_t1688297463  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image Province::provinceImage
	Image_t2816987602 * ___provinceImage_0;
	// UnityEngine.UI.Text Province::provinceText
	Text_t1790657652 * ___provinceText_1;
	// ProvinceScript Province::script
	ProvinceScript_t1528091046 * ___script_2;

public:
	inline static int32_t get_offset_of_provinceImage_0() { return static_cast<int32_t>(offsetof(Province_t1688297463, ___provinceImage_0)); }
	inline Image_t2816987602 * get_provinceImage_0() const { return ___provinceImage_0; }
	inline Image_t2816987602 ** get_address_of_provinceImage_0() { return &___provinceImage_0; }
	inline void set_provinceImage_0(Image_t2816987602 * value)
	{
		___provinceImage_0 = value;
		Il2CppCodeGenWriteBarrier((&___provinceImage_0), value);
	}

	inline static int32_t get_offset_of_provinceText_1() { return static_cast<int32_t>(offsetof(Province_t1688297463, ___provinceText_1)); }
	inline Text_t1790657652 * get_provinceText_1() const { return ___provinceText_1; }
	inline Text_t1790657652 ** get_address_of_provinceText_1() { return &___provinceText_1; }
	inline void set_provinceText_1(Text_t1790657652 * value)
	{
		___provinceText_1 = value;
		Il2CppCodeGenWriteBarrier((&___provinceText_1), value);
	}

	inline static int32_t get_offset_of_script_2() { return static_cast<int32_t>(offsetof(Province_t1688297463, ___script_2)); }
	inline ProvinceScript_t1528091046 * get_script_2() const { return ___script_2; }
	inline ProvinceScript_t1528091046 ** get_address_of_script_2() { return &___script_2; }
	inline void set_script_2(ProvinceScript_t1528091046 * value)
	{
		___script_2 = value;
		Il2CppCodeGenWriteBarrier((&___script_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVINCE_T1688297463_H
#ifndef U3CUNLOCKU3EC__ITERATOR0_T3597604007_H
#define U3CUNLOCKU3EC__ITERATOR0_T3597604007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Province/<Unlock>c__Iterator0
struct  U3CUnlockU3Ec__Iterator0_t3597604007  : public RuntimeObject
{
public:
	// System.Single Province/<Unlock>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single Province/<Unlock>c__Iterator0::_delay
	float ____delay_1;
	// System.Int32 Province/<Unlock>c__Iterator0::_level
	int32_t ____level_2;
	// Province Province/<Unlock>c__Iterator0::$this
	Province_t1688297463 * ___U24this_3;
	// System.Object Province/<Unlock>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Province/<Unlock>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Province/<Unlock>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of__delay_1() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ____delay_1)); }
	inline float get__delay_1() const { return ____delay_1; }
	inline float* get_address_of__delay_1() { return &____delay_1; }
	inline void set__delay_1(float value)
	{
		____delay_1 = value;
	}

	inline static int32_t get_offset_of__level_2() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ____level_2)); }
	inline int32_t get__level_2() const { return ____level_2; }
	inline int32_t* get_address_of__level_2() { return &____level_2; }
	inline void set__level_2(int32_t value)
	{
		____level_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ___U24this_3)); }
	inline Province_t1688297463 * get_U24this_3() const { return ___U24this_3; }
	inline Province_t1688297463 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Province_t1688297463 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUnlockU3Ec__Iterator0_t3597604007, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNLOCKU3EC__ITERATOR0_T3597604007_H
#ifndef U3CEXTINGUISHINGU3EC__ITERATOR0_T3585316312_H
#define U3CEXTINGUISHINGU3EC__ITERATOR0_T3585316312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire/<Extinguishing>c__Iterator0
struct  U3CExtinguishingU3Ec__Iterator0_t3585316312  : public RuntimeObject
{
public:
	// System.Single ExtinguishableFire/<Extinguishing>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Single ExtinguishableFire/<Extinguishing>c__Iterator0::<ratio>__1
	float ___U3CratioU3E__1_1;
	// ExtinguishableFire ExtinguishableFire/<Extinguishing>c__Iterator0::$this
	ExtinguishableFire_t2564158560 * ___U24this_2;
	// System.Object ExtinguishableFire/<Extinguishing>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ExtinguishableFire/<Extinguishing>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ExtinguishableFire/<Extinguishing>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U24this_2)); }
	inline ExtinguishableFire_t2564158560 * get_U24this_2() const { return ___U24this_2; }
	inline ExtinguishableFire_t2564158560 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ExtinguishableFire_t2564158560 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ec__Iterator0_t3585316312, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXTINGUISHINGU3EC__ITERATOR0_T3585316312_H
#ifndef U3CSTARTINGFIREU3EC__ITERATOR1_T824662880_H
#define U3CSTARTINGFIREU3EC__ITERATOR1_T824662880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire/<StartingFire>c__Iterator1
struct  U3CStartingFireU3Ec__Iterator1_t824662880  : public RuntimeObject
{
public:
	// System.Single ExtinguishableFire/<StartingFire>c__Iterator1::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Single ExtinguishableFire/<StartingFire>c__Iterator1::<ratio>__1
	float ___U3CratioU3E__1_1;
	// ExtinguishableFire ExtinguishableFire/<StartingFire>c__Iterator1::$this
	ExtinguishableFire_t2564158560 * ___U24this_2;
	// System.Object ExtinguishableFire/<StartingFire>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ExtinguishableFire/<StartingFire>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 ExtinguishableFire/<StartingFire>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U24this_2)); }
	inline ExtinguishableFire_t2564158560 * get_U24this_2() const { return ___U24this_2; }
	inline ExtinguishableFire_t2564158560 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ExtinguishableFire_t2564158560 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ec__Iterator1_t824662880, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTINGFIREU3EC__ITERATOR1_T824662880_H
#ifndef CINEMACHINEGAMEWINDOWDEBUG_T4056967167_H
#define CINEMACHINEGAMEWINDOWDEBUG_T4056967167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.CinemachineGameWindowDebug
struct  CinemachineGameWindowDebug_t4056967167  : public RuntimeObject
{
public:

public:
};

struct CinemachineGameWindowDebug_t4056967167_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Object> Cinemachine.Utility.CinemachineGameWindowDebug::mClients
	HashSet_1_t873097153 * ___mClients_0;

public:
	inline static int32_t get_offset_of_mClients_0() { return static_cast<int32_t>(offsetof(CinemachineGameWindowDebug_t4056967167_StaticFields, ___mClients_0)); }
	inline HashSet_1_t873097153 * get_mClients_0() const { return ___mClients_0; }
	inline HashSet_1_t873097153 ** get_address_of_mClients_0() { return &___mClients_0; }
	inline void set_mClients_0(HashSet_1_t873097153 * value)
	{
		___mClients_0 = value;
		Il2CppCodeGenWriteBarrier((&___mClients_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEGAMEWINDOWDEBUG_T4056967167_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef PROPERTYATTRIBUTE_T381499487_H
#define PROPERTYATTRIBUTE_T381499487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t381499487  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T381499487_H
#ifndef CINEMACHINESHOTPLAYABLE_T3687665838_H
#define CINEMACHINESHOTPLAYABLE_T3687665838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineShotPlayable
struct  CinemachineShotPlayable_t3687665838  : public PlayableBehaviour_t3096602576
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.Timeline.CinemachineShotPlayable::VirtualCamera
	CinemachineVirtualCameraBase_t2557508663 * ___VirtualCamera_0;

public:
	inline static int32_t get_offset_of_VirtualCamera_0() { return static_cast<int32_t>(offsetof(CinemachineShotPlayable_t3687665838, ___VirtualCamera_0)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_VirtualCamera_0() const { return ___VirtualCamera_0; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_VirtualCamera_0() { return &___VirtualCamera_0; }
	inline void set_VirtualCamera_0(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___VirtualCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualCamera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESHOTPLAYABLE_T3687665838_H
#ifndef CINEMACHINEMIXER_T3106063062_H
#define CINEMACHINEMIXER_T3106063062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineMixer
struct  CinemachineMixer_t3106063062  : public PlayableBehaviour_t3096602576
{
public:
	// Cinemachine.CinemachineBrain Cinemachine.Timeline.CinemachineMixer::mBrain
	CinemachineBrain_t679733552 * ___mBrain_0;
	// System.Int32 Cinemachine.Timeline.CinemachineMixer::mBrainOverrideId
	int32_t ___mBrainOverrideId_1;

public:
	inline static int32_t get_offset_of_mBrain_0() { return static_cast<int32_t>(offsetof(CinemachineMixer_t3106063062, ___mBrain_0)); }
	inline CinemachineBrain_t679733552 * get_mBrain_0() const { return ___mBrain_0; }
	inline CinemachineBrain_t679733552 ** get_address_of_mBrain_0() { return &___mBrain_0; }
	inline void set_mBrain_0(CinemachineBrain_t679733552 * value)
	{
		___mBrain_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBrain_0), value);
	}

	inline static int32_t get_offset_of_mBrainOverrideId_1() { return static_cast<int32_t>(offsetof(CinemachineMixer_t3106063062, ___mBrainOverrideId_1)); }
	inline int32_t get_mBrainOverrideId_1() const { return ___mBrainOverrideId_1; }
	inline int32_t* get_address_of_mBrainOverrideId_1() { return &___mBrainOverrideId_1; }
	inline void set_mBrainOverrideId_1(int32_t value)
	{
		___mBrainOverrideId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEMIXER_T3106063062_H
#ifndef DISCRETETIME_T1558908690_H
#define DISCRETETIME_T1558908690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t1558908690 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t1558908690_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t1558908690  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t1558908690  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t1558908690 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t1558908690  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T1558908690_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef PROPERTYNAME_T65259757_H
#define PROPERTYNAME_T65259757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t65259757 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t65259757, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T65259757_H
#ifndef SAVEDURINGPLAYATTRIBUTE_T1252963312_H
#define SAVEDURINGPLAYATTRIBUTE_T1252963312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.SaveDuringPlayAttribute
struct  SaveDuringPlayAttribute_t1252963312  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDURINGPLAYATTRIBUTE_T1252963312_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef BLACKBOARDEXPRESSION_T4195482571_H
#define BLACKBOARDEXPRESSION_T4195482571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/BlackboardExpression
struct  BlackboardExpression_t4195482571 
{
public:
	// Cinemachine.Blackboard.Reactor/BlackboardExpression/Line[] Cinemachine.Blackboard.Reactor/BlackboardExpression::m_Lines
	LineU5BU5D_t2399996771* ___m_Lines_0;

public:
	inline static int32_t get_offset_of_m_Lines_0() { return static_cast<int32_t>(offsetof(BlackboardExpression_t4195482571, ___m_Lines_0)); }
	inline LineU5BU5D_t2399996771* get_m_Lines_0() const { return ___m_Lines_0; }
	inline LineU5BU5D_t2399996771** get_address_of_m_Lines_0() { return &___m_Lines_0; }
	inline void set_m_Lines_0(LineU5BU5D_t2399996771* value)
	{
		___m_Lines_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lines_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.Blackboard.Reactor/BlackboardExpression
struct BlackboardExpression_t4195482571_marshaled_pinvoke
{
	Line_t1381376710_marshaled_pinvoke* ___m_Lines_0;
};
// Native definition for COM marshalling of Cinemachine.Blackboard.Reactor/BlackboardExpression
struct BlackboardExpression_t4195482571_marshaled_com
{
	Line_t1381376710_marshaled_com* ___m_Lines_0;
};
#endif // BLACKBOARDEXPRESSION_T4195482571_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef GAUSSIANWINDOW1D_VECTOR3_T1200723002_H
#define GAUSSIANWINDOW1D_VECTOR3_T1200723002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_Vector3
struct  GaussianWindow1D_Vector3_t1200723002  : public GaussianWindow1d_1_t3723530233
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_VECTOR3_T1200723002_H
#ifndef GAUSSIANWINDOW1D_QUATERNION_T2095249446_H
#define GAUSSIANWINDOW1D_QUATERNION_T2095249446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_Quaternion
struct  GaussianWindow1D_Quaternion_t2095249446  : public GaussianWindow1d_1_t2440788680
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_QUATERNION_T2095249446_H
#ifndef GAUSSIANWINDOW1D_CAMERAROTATION_T90163852_H
#define GAUSSIANWINDOW1D_CAMERAROTATION_T90163852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_CameraRotation
struct  GaussianWindow1D_CameraRotation_t90163852  : public GaussianWindow1d_1_t2065110756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_CAMERAROTATION_T90163852_H
#ifndef NOISEPARAMS_T633381832_H
#define NOISEPARAMS_T633381832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings/NoiseParams
struct  NoiseParams_t633381832 
{
public:
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Amplitude
	float ___Amplitude_0;
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Frequency
	float ___Frequency_1;

public:
	inline static int32_t get_offset_of_Amplitude_0() { return static_cast<int32_t>(offsetof(NoiseParams_t633381832, ___Amplitude_0)); }
	inline float get_Amplitude_0() const { return ___Amplitude_0; }
	inline float* get_address_of_Amplitude_0() { return &___Amplitude_0; }
	inline void set_Amplitude_0(float value)
	{
		___Amplitude_0 = value;
	}

	inline static int32_t get_offset_of_Frequency_1() { return static_cast<int32_t>(offsetof(NoiseParams_t633381832, ___Frequency_1)); }
	inline float get_Frequency_1() const { return ___Frequency_1; }
	inline float* get_address_of_Frequency_1() { return &___Frequency_1; }
	inline void set_Frequency_1(float value)
	{
		___Frequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEPARAMS_T633381832_H
#ifndef LENSSETTINGS_T1505430577_H
#define LENSSETTINGS_T1505430577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.LensSettings
struct  LensSettings_t1505430577 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// System.Boolean Cinemachine.LensSettings::<Orthographic>k__BackingField
	bool ___U3COrthographicU3Ek__BackingField_6;
	// System.Single Cinemachine.LensSettings::<Aspect>k__BackingField
	float ___U3CAspectU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_U3COrthographicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___U3COrthographicU3Ek__BackingField_6)); }
	inline bool get_U3COrthographicU3Ek__BackingField_6() const { return ___U3COrthographicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3COrthographicU3Ek__BackingField_6() { return &___U3COrthographicU3Ek__BackingField_6; }
	inline void set_U3COrthographicU3Ek__BackingField_6(bool value)
	{
		___U3COrthographicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAspectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___U3CAspectU3Ek__BackingField_7)); }
	inline float get_U3CAspectU3Ek__BackingField_7() const { return ___U3CAspectU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAspectU3Ek__BackingField_7() { return &___U3CAspectU3Ek__BackingField_7; }
	inline void set_U3CAspectU3Ek__BackingField_7(float value)
	{
		___U3CAspectU3Ek__BackingField_7 = value;
	}
};

struct LensSettings_t1505430577_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_t1505430577  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577_StaticFields, ___Default_0)); }
	inline LensSettings_t1505430577  get_Default_0() const { return ___Default_0; }
	inline LensSettings_t1505430577 * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_t1505430577  value)
	{
		___Default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_t1505430577_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_t1505430577_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
#endif // LENSSETTINGS_T1505430577_H
#ifndef EFFECTATTACHPOINT_T460027681_H
#define EFFECTATTACHPOINT_T460027681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectAttachPoint
struct  EffectAttachPoint_t460027681  : public RuntimeObject
{
public:
	// UnityEngine.GameObject EffectAttachPoint::obj
	GameObject_t2557347079 * ___obj_0;
	// UnityEngine.Vector3 EffectAttachPoint::offset
	Vector3_t1986933152  ___offset_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(EffectAttachPoint_t460027681, ___obj_0)); }
	inline GameObject_t2557347079 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t2557347079 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t2557347079 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_offset_1() { return static_cast<int32_t>(offsetof(EffectAttachPoint_t460027681, ___offset_1)); }
	inline Vector3_t1986933152  get_offset_1() const { return ___offset_1; }
	inline Vector3_t1986933152 * get_address_of_offset_1() { return &___offset_1; }
	inline void set_offset_1(Vector3_t1986933152  value)
	{
		___offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTATTACHPOINT_T460027681_H
#ifndef SPACEENUM_T1146975981_H
#define SPACEENUM_T1146975981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicRotation/spaceEnum
struct  spaceEnum_t1146975981 
{
public:
	// System.Int32 MagicArsenal.MagicRotation/spaceEnum::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(spaceEnum_t1146975981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEENUM_T1146975981_H
#ifndef CAMERASTATE_T382403230_H
#define CAMERASTATE_T382403230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CameraState
struct  CameraState_t382403230 
{
public:
	union
	{
		struct
		{
			// Cinemachine.LensSettings Cinemachine.CameraState::<Lens>k__BackingField
			LensSettings_t1505430577  ___U3CLensU3Ek__BackingField_0;
			// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceUp>k__BackingField
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceLookAt>k__BackingField
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			// UnityEngine.Vector3 Cinemachine.CameraState::<RawPosition>k__BackingField
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			// UnityEngine.Quaternion Cinemachine.CameraState::<RawOrientation>k__BackingField
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			// System.Single Cinemachine.CameraState::<ShotQuality>k__BackingField
			float ___U3CShotQualityU3Ek__BackingField_6;
			// UnityEngine.Vector3 Cinemachine.CameraState::<PositionCorrection>k__BackingField
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			// UnityEngine.Quaternion Cinemachine.CameraState::<OrientationCorrection>k__BackingField
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CLensU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CLensU3Ek__BackingField_0)); }
	inline LensSettings_t1505430577  get_U3CLensU3Ek__BackingField_0() const { return ___U3CLensU3Ek__BackingField_0; }
	inline LensSettings_t1505430577 * get_address_of_U3CLensU3Ek__BackingField_0() { return &___U3CLensU3Ek__BackingField_0; }
	inline void set_U3CLensU3Ek__BackingField_0(LensSettings_t1505430577  value)
	{
		___U3CLensU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceUpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CReferenceUpU3Ek__BackingField_1)); }
	inline Vector3_t1986933152  get_U3CReferenceUpU3Ek__BackingField_1() const { return ___U3CReferenceUpU3Ek__BackingField_1; }
	inline Vector3_t1986933152 * get_address_of_U3CReferenceUpU3Ek__BackingField_1() { return &___U3CReferenceUpU3Ek__BackingField_1; }
	inline void set_U3CReferenceUpU3Ek__BackingField_1(Vector3_t1986933152  value)
	{
		___U3CReferenceUpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CReferenceLookAtU3Ek__BackingField_2)); }
	inline Vector3_t1986933152  get_U3CReferenceLookAtU3Ek__BackingField_2() const { return ___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline Vector3_t1986933152 * get_address_of_U3CReferenceLookAtU3Ek__BackingField_2() { return &___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline void set_U3CReferenceLookAtU3Ek__BackingField_2(Vector3_t1986933152  value)
	{
		___U3CReferenceLookAtU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRawPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CRawPositionU3Ek__BackingField_4)); }
	inline Vector3_t1986933152  get_U3CRawPositionU3Ek__BackingField_4() const { return ___U3CRawPositionU3Ek__BackingField_4; }
	inline Vector3_t1986933152 * get_address_of_U3CRawPositionU3Ek__BackingField_4() { return &___U3CRawPositionU3Ek__BackingField_4; }
	inline void set_U3CRawPositionU3Ek__BackingField_4(Vector3_t1986933152  value)
	{
		___U3CRawPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawOrientationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CRawOrientationU3Ek__BackingField_5)); }
	inline Quaternion_t704191599  get_U3CRawOrientationU3Ek__BackingField_5() const { return ___U3CRawOrientationU3Ek__BackingField_5; }
	inline Quaternion_t704191599 * get_address_of_U3CRawOrientationU3Ek__BackingField_5() { return &___U3CRawOrientationU3Ek__BackingField_5; }
	inline void set_U3CRawOrientationU3Ek__BackingField_5(Quaternion_t704191599  value)
	{
		___U3CRawOrientationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CShotQualityU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CShotQualityU3Ek__BackingField_6)); }
	inline float get_U3CShotQualityU3Ek__BackingField_6() const { return ___U3CShotQualityU3Ek__BackingField_6; }
	inline float* get_address_of_U3CShotQualityU3Ek__BackingField_6() { return &___U3CShotQualityU3Ek__BackingField_6; }
	inline void set_U3CShotQualityU3Ek__BackingField_6(float value)
	{
		___U3CShotQualityU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPositionCorrectionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CPositionCorrectionU3Ek__BackingField_7)); }
	inline Vector3_t1986933152  get_U3CPositionCorrectionU3Ek__BackingField_7() const { return ___U3CPositionCorrectionU3Ek__BackingField_7; }
	inline Vector3_t1986933152 * get_address_of_U3CPositionCorrectionU3Ek__BackingField_7() { return &___U3CPositionCorrectionU3Ek__BackingField_7; }
	inline void set_U3CPositionCorrectionU3Ek__BackingField_7(Vector3_t1986933152  value)
	{
		___U3CPositionCorrectionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3COrientationCorrectionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3COrientationCorrectionU3Ek__BackingField_8)); }
	inline Quaternion_t704191599  get_U3COrientationCorrectionU3Ek__BackingField_8() const { return ___U3COrientationCorrectionU3Ek__BackingField_8; }
	inline Quaternion_t704191599 * get_address_of_U3COrientationCorrectionU3Ek__BackingField_8() { return &___U3COrientationCorrectionU3Ek__BackingField_8; }
	inline void set_U3COrientationCorrectionU3Ek__BackingField_8(Quaternion_t704191599  value)
	{
		___U3COrientationCorrectionU3Ek__BackingField_8 = value;
	}
};

struct CameraState_t382403230_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CameraState::kNoPoint
	Vector3_t1986933152  ___kNoPoint_3;

public:
	inline static int32_t get_offset_of_kNoPoint_3() { return static_cast<int32_t>(offsetof(CameraState_t382403230_StaticFields, ___kNoPoint_3)); }
	inline Vector3_t1986933152  get_kNoPoint_3() const { return ___kNoPoint_3; }
	inline Vector3_t1986933152 * get_address_of_kNoPoint_3() { return &___kNoPoint_3; }
	inline void set_kNoPoint_3(Vector3_t1986933152  value)
	{
		___kNoPoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CameraState
struct CameraState_t382403230_marshaled_pinvoke
{
	union
	{
		struct
		{
			LensSettings_t1505430577_marshaled_pinvoke ___U3CLensU3Ek__BackingField_0;
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			float ___U3CShotQualityU3Ek__BackingField_6;
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};
};
// Native definition for COM marshalling of Cinemachine.CameraState
struct CameraState_t382403230_marshaled_com
{
	union
	{
		struct
		{
			LensSettings_t1505430577_marshaled_com ___U3CLensU3Ek__BackingField_0;
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			float ___U3CShotQualityU3Ek__BackingField_6;
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};
};
#endif // CAMERASTATE_T382403230_H
#ifndef PARTICLEEXAMPLES_T356210336_H
#define PARTICLEEXAMPLES_T356210336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleExamples
struct  ParticleExamples_t356210336  : public RuntimeObject
{
public:
	// System.String ParticleExamples::title
	String_t* ___title_0;
	// System.String ParticleExamples::description
	String_t* ___description_1;
	// System.Boolean ParticleExamples::isWeaponEffect
	bool ___isWeaponEffect_2;
	// UnityEngine.GameObject ParticleExamples::particleSystemGO
	GameObject_t2557347079 * ___particleSystemGO_3;
	// UnityEngine.Vector3 ParticleExamples::particlePosition
	Vector3_t1986933152  ___particlePosition_4;
	// UnityEngine.Vector3 ParticleExamples::particleRotation
	Vector3_t1986933152  ___particleRotation_5;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}

	inline static int32_t get_offset_of_isWeaponEffect_2() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___isWeaponEffect_2)); }
	inline bool get_isWeaponEffect_2() const { return ___isWeaponEffect_2; }
	inline bool* get_address_of_isWeaponEffect_2() { return &___isWeaponEffect_2; }
	inline void set_isWeaponEffect_2(bool value)
	{
		___isWeaponEffect_2 = value;
	}

	inline static int32_t get_offset_of_particleSystemGO_3() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___particleSystemGO_3)); }
	inline GameObject_t2557347079 * get_particleSystemGO_3() const { return ___particleSystemGO_3; }
	inline GameObject_t2557347079 ** get_address_of_particleSystemGO_3() { return &___particleSystemGO_3; }
	inline void set_particleSystemGO_3(GameObject_t2557347079 * value)
	{
		___particleSystemGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemGO_3), value);
	}

	inline static int32_t get_offset_of_particlePosition_4() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___particlePosition_4)); }
	inline Vector3_t1986933152  get_particlePosition_4() const { return ___particlePosition_4; }
	inline Vector3_t1986933152 * get_address_of_particlePosition_4() { return &___particlePosition_4; }
	inline void set_particlePosition_4(Vector3_t1986933152  value)
	{
		___particlePosition_4 = value;
	}

	inline static int32_t get_offset_of_particleRotation_5() { return static_cast<int32_t>(offsetof(ParticleExamples_t356210336, ___particleRotation_5)); }
	inline Vector3_t1986933152  get_particleRotation_5() const { return ___particleRotation_5; }
	inline Vector3_t1986933152 * get_address_of_particleRotation_5() { return &___particleRotation_5; }
	inline void set_particleRotation_5(Vector3_t1986933152  value)
	{
		___particleRotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEEXAMPLES_T356210336_H
#ifndef ANIMATIONCURVE_T2757045165_H
#define ANIMATIONCURVE_T2757045165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t2757045165  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2757045165, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T2757045165_H
#ifndef MEDIATYPE_T673317257_H
#define MEDIATYPE_T673317257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/MediaType
struct  MediaType_t673317257 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/MediaType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MediaType_t673317257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPE_T673317257_H
#ifndef STAGE_T2941156404_H
#define STAGE_T2941156404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/Stage
struct  Stage_t2941156404 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/Stage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Stage_t2941156404, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGE_T2941156404_H
#ifndef UPDATESTATUS_T4119700699_H
#define UPDATESTATUS_T4119700699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/UpdateStatus
struct  UpdateStatus_t4119700699 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::frame
	int32_t ___frame_0;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::subframe
	int32_t ___subframe_1;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::lastFixedUpdate
	int32_t ___lastFixedUpdate_2;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineCore/UpdateStatus::targetPos
	Matrix4x4_t1237934469  ___targetPos_3;

public:
	inline static int32_t get_offset_of_frame_0() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___frame_0)); }
	inline int32_t get_frame_0() const { return ___frame_0; }
	inline int32_t* get_address_of_frame_0() { return &___frame_0; }
	inline void set_frame_0(int32_t value)
	{
		___frame_0 = value;
	}

	inline static int32_t get_offset_of_subframe_1() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___subframe_1)); }
	inline int32_t get_subframe_1() const { return ___subframe_1; }
	inline int32_t* get_address_of_subframe_1() { return &___subframe_1; }
	inline void set_subframe_1(int32_t value)
	{
		___subframe_1 = value;
	}

	inline static int32_t get_offset_of_lastFixedUpdate_2() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___lastFixedUpdate_2)); }
	inline int32_t get_lastFixedUpdate_2() const { return ___lastFixedUpdate_2; }
	inline int32_t* get_address_of_lastFixedUpdate_2() { return &___lastFixedUpdate_2; }
	inline void set_lastFixedUpdate_2(int32_t value)
	{
		___lastFixedUpdate_2 = value;
	}

	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(UpdateStatus_t4119700699, ___targetPos_3)); }
	inline Matrix4x4_t1237934469  get_targetPos_3() const { return ___targetPos_3; }
	inline Matrix4x4_t1237934469 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Matrix4x4_t1237934469  value)
	{
		___targetPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATUS_T4119700699_H
#ifndef UPDATEFILTER_T463341921_H
#define UPDATEFILTER_T463341921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/UpdateFilter
struct  UpdateFilter_t463341921 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateFilter_t463341921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEFILTER_T463341921_H
#ifndef LENSSETTINGSPROPERTYATTRIBUTE_T2647531223_H
#define LENSSETTINGSPROPERTYATTRIBUTE_T2647531223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.LensSettingsPropertyAttribute
struct  LensSettingsPropertyAttribute_t2647531223  : public PropertyAttribute_t381499487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSSETTINGSPROPERTYATTRIBUTE_T2647531223_H
#ifndef CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T3768968087_H
#define CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T3768968087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinitionPropertyAttribute
struct  CinemachineBlendDefinitionPropertyAttribute_t3768968087  : public PropertyAttribute_t381499487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T3768968087_H
#ifndef NOSAVEDURINGPLAYATTRIBUTE_T4285710416_H
#define NOSAVEDURINGPLAYATTRIBUTE_T4285710416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoSaveDuringPlayAttribute
struct  NoSaveDuringPlayAttribute_t4285710416  : public PropertyAttribute_t381499487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOSAVEDURINGPLAYATTRIBUTE_T4285710416_H
#ifndef MINATTRIBUTE_T3533541894_H
#define MINATTRIBUTE_T3533541894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.MinAttribute
struct  MinAttribute_t3533541894  : public PropertyAttribute_t381499487
{
public:
	// System.Single Cinemachine.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t3533541894, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T3533541894_H
#ifndef GETSETATTRIBUTE_T3750549756_H
#define GETSETATTRIBUTE_T3750549756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.GetSetAttribute
struct  GetSetAttribute_t3750549756  : public PropertyAttribute_t381499487
{
public:
	// System.String Cinemachine.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean Cinemachine.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t3750549756, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t3750549756, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T3750549756_H
#ifndef FACETYPE_T2671696802_H
#define FACETYPE_T2671696802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HowIFeltFaces/FaceType
struct  FaceType_t2671696802 
{
public:
	// System.Int32 HowIFeltFaces/FaceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FaceType_t2671696802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACETYPE_T2671696802_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef TRANSFORMNOISEPARAMS_T2565733168_H
#define TRANSFORMNOISEPARAMS_T2565733168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings/TransformNoiseParams
struct  TransformNoiseParams_t2565733168 
{
public:
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::X
	NoiseParams_t633381832  ___X_0;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Y
	NoiseParams_t633381832  ___Y_1;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Z
	NoiseParams_t633381832  ___Z_2;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t2565733168, ___X_0)); }
	inline NoiseParams_t633381832  get_X_0() const { return ___X_0; }
	inline NoiseParams_t633381832 * get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(NoiseParams_t633381832  value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t2565733168, ___Y_1)); }
	inline NoiseParams_t633381832  get_Y_1() const { return ___Y_1; }
	inline NoiseParams_t633381832 * get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(NoiseParams_t633381832  value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Z_2() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t2565733168, ___Z_2)); }
	inline NoiseParams_t633381832  get_Z_2() const { return ___Z_2; }
	inline NoiseParams_t633381832 * get_address_of_Z_2() { return &___Z_2; }
	inline void set_Z_2(NoiseParams_t633381832  value)
	{
		___Z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMNOISEPARAMS_T2565733168_H
#ifndef STYLE_T4107124224_H
#define STYLE_T4107124224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition/Style
struct  Style_t4107124224 
{
public:
	// System.Int32 Cinemachine.CinemachineBlendDefinition/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t4107124224, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T4107124224_H
#ifndef COMBINEMODE_T3158640028_H
#define COMBINEMODE_T3158640028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/CombineMode
struct  CombineMode_t3158640028 
{
public:
	// System.Int32 Cinemachine.Blackboard.Reactor/CombineMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CombineMode_t3158640028, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEMODE_T3158640028_H
#ifndef RAYCASTHIT_T1706347245_H
#define RAYCASTHIT_T1706347245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1706347245 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t1986933152  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t1986933152  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t328513675  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1485601975 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Point_0)); }
	inline Vector3_t1986933152  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t1986933152 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t1986933152  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Normal_1)); }
	inline Vector3_t1986933152  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t1986933152 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t1986933152  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_UV_4)); }
	inline Vector2_t328513675  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t328513675 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t328513675  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Collider_5)); }
	inline Collider_t1485601975 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1485601975 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1485601975 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1706347245_marshaled_pinvoke
{
	Vector3_t1986933152  ___m_Point_0;
	Vector3_t1986933152  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t328513675  ___m_UV_4;
	Collider_t1485601975 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1706347245_marshaled_com
{
	Vector3_t1986933152  ___m_Point_0;
	Vector3_t1986933152  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t328513675  ___m_UV_4;
	Collider_t1485601975 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1706347245_H
#ifndef EXPOSEDREFERENCE_1_T2356016056_H
#define EXPOSEDREFERENCE_1_T2356016056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<Cinemachine.CinemachineVirtualCameraBase>
struct  ExposedReference_1_t2356016056 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t65259757  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_t692178351 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_t2356016056, ___exposedName_0)); }
	inline PropertyName_t65259757  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t65259757 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t65259757  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_t2356016056, ___defaultValue_1)); }
	inline Object_t692178351 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_t692178351 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_t692178351 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSEDREFERENCE_1_T2356016056_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef LEVEL_T3916434835_H
#define LEVEL_T3916434835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.DocumentationSortingAttribute/Level
struct  Level_t3916434835 
{
public:
	// System.Int32 Cinemachine.DocumentationSortingAttribute/Level::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Level_t3916434835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T3916434835_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef CINEMACHINEBLENDDEFINITION_T3981438518_H
#define CINEMACHINEBLENDDEFINITION_T3981438518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition
struct  CinemachineBlendDefinition_t3981438518 
{
public:
	// Cinemachine.CinemachineBlendDefinition/Style Cinemachine.CinemachineBlendDefinition::m_Style
	int32_t ___m_Style_0;
	// System.Single Cinemachine.CinemachineBlendDefinition::m_Time
	float ___m_Time_1;

public:
	inline static int32_t get_offset_of_m_Style_0() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3981438518, ___m_Style_0)); }
	inline int32_t get_m_Style_0() const { return ___m_Style_0; }
	inline int32_t* get_address_of_m_Style_0() { return &___m_Style_0; }
	inline void set_m_Style_0(int32_t value)
	{
		___m_Style_0 = value;
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3981438518, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDDEFINITION_T3981438518_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef CINEMACHINECORE_T1985905913_H
#define CINEMACHINECORE_T1985905913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore
struct  CinemachineCore_t1985905913  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain> Cinemachine.CinemachineCore::mActiveBrains
	List_1_t4094327855 * ___mActiveBrains_4;
	// System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera> Cinemachine.CinemachineCore::mActiveCameras
	List_1_t2897348067 * ___mActiveCameras_5;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCore/UpdateStatus> Cinemachine.CinemachineCore::mUpdateStatus
	Dictionary_2_t3112813203 * ___mUpdateStatus_6;
	// Cinemachine.CinemachineCore/UpdateFilter Cinemachine.CinemachineCore::<CurrentUpdateFilter>k__BackingField
	int32_t ___U3CCurrentUpdateFilterU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_mActiveBrains_4() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913, ___mActiveBrains_4)); }
	inline List_1_t4094327855 * get_mActiveBrains_4() const { return ___mActiveBrains_4; }
	inline List_1_t4094327855 ** get_address_of_mActiveBrains_4() { return &___mActiveBrains_4; }
	inline void set_mActiveBrains_4(List_1_t4094327855 * value)
	{
		___mActiveBrains_4 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBrains_4), value);
	}

	inline static int32_t get_offset_of_mActiveCameras_5() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913, ___mActiveCameras_5)); }
	inline List_1_t2897348067 * get_mActiveCameras_5() const { return ___mActiveCameras_5; }
	inline List_1_t2897348067 ** get_address_of_mActiveCameras_5() { return &___mActiveCameras_5; }
	inline void set_mActiveCameras_5(List_1_t2897348067 * value)
	{
		___mActiveCameras_5 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveCameras_5), value);
	}

	inline static int32_t get_offset_of_mUpdateStatus_6() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913, ___mUpdateStatus_6)); }
	inline Dictionary_2_t3112813203 * get_mUpdateStatus_6() const { return ___mUpdateStatus_6; }
	inline Dictionary_2_t3112813203 ** get_address_of_mUpdateStatus_6() { return &___mUpdateStatus_6; }
	inline void set_mUpdateStatus_6(Dictionary_2_t3112813203 * value)
	{
		___mUpdateStatus_6 = value;
		Il2CppCodeGenWriteBarrier((&___mUpdateStatus_6), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUpdateFilterU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913, ___U3CCurrentUpdateFilterU3Ek__BackingField_7)); }
	inline int32_t get_U3CCurrentUpdateFilterU3Ek__BackingField_7() const { return ___U3CCurrentUpdateFilterU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CCurrentUpdateFilterU3Ek__BackingField_7() { return &___U3CCurrentUpdateFilterU3Ek__BackingField_7; }
	inline void set_U3CCurrentUpdateFilterU3Ek__BackingField_7(int32_t value)
	{
		___U3CCurrentUpdateFilterU3Ek__BackingField_7 = value;
	}
};

struct CinemachineCore_t1985905913_StaticFields
{
public:
	// System.String Cinemachine.CinemachineCore::kVersionString
	String_t* ___kVersionString_0;
	// Cinemachine.CinemachineCore Cinemachine.CinemachineCore::sInstance
	CinemachineCore_t1985905913 * ___sInstance_1;
	// System.Boolean Cinemachine.CinemachineCore::sShowHiddenObjects
	bool ___sShowHiddenObjects_2;
	// Cinemachine.CinemachineCore/AxisInputDelegate Cinemachine.CinemachineCore::GetInputAxis
	AxisInputDelegate_t2029212819 * ___GetInputAxis_3;
	// Cinemachine.CinemachineCore/AxisInputDelegate Cinemachine.CinemachineCore::<>f__mg$cache0
	AxisInputDelegate_t2029212819 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_kVersionString_0() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913_StaticFields, ___kVersionString_0)); }
	inline String_t* get_kVersionString_0() const { return ___kVersionString_0; }
	inline String_t** get_address_of_kVersionString_0() { return &___kVersionString_0; }
	inline void set_kVersionString_0(String_t* value)
	{
		___kVersionString_0 = value;
		Il2CppCodeGenWriteBarrier((&___kVersionString_0), value);
	}

	inline static int32_t get_offset_of_sInstance_1() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913_StaticFields, ___sInstance_1)); }
	inline CinemachineCore_t1985905913 * get_sInstance_1() const { return ___sInstance_1; }
	inline CinemachineCore_t1985905913 ** get_address_of_sInstance_1() { return &___sInstance_1; }
	inline void set_sInstance_1(CinemachineCore_t1985905913 * value)
	{
		___sInstance_1 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_1), value);
	}

	inline static int32_t get_offset_of_sShowHiddenObjects_2() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913_StaticFields, ___sShowHiddenObjects_2)); }
	inline bool get_sShowHiddenObjects_2() const { return ___sShowHiddenObjects_2; }
	inline bool* get_address_of_sShowHiddenObjects_2() { return &___sShowHiddenObjects_2; }
	inline void set_sShowHiddenObjects_2(bool value)
	{
		___sShowHiddenObjects_2 = value;
	}

	inline static int32_t get_offset_of_GetInputAxis_3() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913_StaticFields, ___GetInputAxis_3)); }
	inline AxisInputDelegate_t2029212819 * get_GetInputAxis_3() const { return ___GetInputAxis_3; }
	inline AxisInputDelegate_t2029212819 ** get_address_of_GetInputAxis_3() { return &___GetInputAxis_3; }
	inline void set_GetInputAxis_3(AxisInputDelegate_t2029212819 * value)
	{
		___GetInputAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputAxis_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(CinemachineCore_t1985905913_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline AxisInputDelegate_t2029212819 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline AxisInputDelegate_t2029212819 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(AxisInputDelegate_t2029212819 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECORE_T1985905913_H
#ifndef DOCUMENTATIONSORTINGATTRIBUTE_T681625788_H
#define DOCUMENTATIONSORTINGATTRIBUTE_T681625788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.DocumentationSortingAttribute
struct  DocumentationSortingAttribute_t681625788  : public Attribute_t1924466020
{
public:
	// System.Single Cinemachine.DocumentationSortingAttribute::<SortOrder>k__BackingField
	float ___U3CSortOrderU3Ek__BackingField_0;
	// Cinemachine.DocumentationSortingAttribute/Level Cinemachine.DocumentationSortingAttribute::<Category>k__BackingField
	int32_t ___U3CCategoryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSortOrderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentationSortingAttribute_t681625788, ___U3CSortOrderU3Ek__BackingField_0)); }
	inline float get_U3CSortOrderU3Ek__BackingField_0() const { return ___U3CSortOrderU3Ek__BackingField_0; }
	inline float* get_address_of_U3CSortOrderU3Ek__BackingField_0() { return &___U3CSortOrderU3Ek__BackingField_0; }
	inline void set_U3CSortOrderU3Ek__BackingField_0(float value)
	{
		___U3CSortOrderU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCategoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentationSortingAttribute_t681625788, ___U3CCategoryU3Ek__BackingField_1)); }
	inline int32_t get_U3CCategoryU3Ek__BackingField_1() const { return ___U3CCategoryU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCategoryU3Ek__BackingField_1() { return &___U3CCategoryU3Ek__BackingField_1; }
	inline void set_U3CCategoryU3Ek__BackingField_1(int32_t value)
	{
		___U3CCategoryU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTATIONSORTINGATTRIBUTE_T681625788_H
#ifndef LINE_T1381376710_H
#define LINE_T1381376710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/BlackboardExpression/Line
struct  Line_t1381376710 
{
public:
	// Cinemachine.Blackboard.Reactor/CombineMode Cinemachine.Blackboard.Reactor/BlackboardExpression/Line::m_Operation
	int32_t ___m_Operation_0;
	// System.String Cinemachine.Blackboard.Reactor/BlackboardExpression/Line::m_BlackboardKey
	String_t* ___m_BlackboardKey_1;
	// System.Boolean Cinemachine.Blackboard.Reactor/BlackboardExpression/Line::m_Remap
	bool ___m_Remap_2;
	// UnityEngine.AnimationCurve Cinemachine.Blackboard.Reactor/BlackboardExpression/Line::m_RemapCurve
	AnimationCurve_t2757045165 * ___m_RemapCurve_3;

public:
	inline static int32_t get_offset_of_m_Operation_0() { return static_cast<int32_t>(offsetof(Line_t1381376710, ___m_Operation_0)); }
	inline int32_t get_m_Operation_0() const { return ___m_Operation_0; }
	inline int32_t* get_address_of_m_Operation_0() { return &___m_Operation_0; }
	inline void set_m_Operation_0(int32_t value)
	{
		___m_Operation_0 = value;
	}

	inline static int32_t get_offset_of_m_BlackboardKey_1() { return static_cast<int32_t>(offsetof(Line_t1381376710, ___m_BlackboardKey_1)); }
	inline String_t* get_m_BlackboardKey_1() const { return ___m_BlackboardKey_1; }
	inline String_t** get_address_of_m_BlackboardKey_1() { return &___m_BlackboardKey_1; }
	inline void set_m_BlackboardKey_1(String_t* value)
	{
		___m_BlackboardKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlackboardKey_1), value);
	}

	inline static int32_t get_offset_of_m_Remap_2() { return static_cast<int32_t>(offsetof(Line_t1381376710, ___m_Remap_2)); }
	inline bool get_m_Remap_2() const { return ___m_Remap_2; }
	inline bool* get_address_of_m_Remap_2() { return &___m_Remap_2; }
	inline void set_m_Remap_2(bool value)
	{
		___m_Remap_2 = value;
	}

	inline static int32_t get_offset_of_m_RemapCurve_3() { return static_cast<int32_t>(offsetof(Line_t1381376710, ___m_RemapCurve_3)); }
	inline AnimationCurve_t2757045165 * get_m_RemapCurve_3() const { return ___m_RemapCurve_3; }
	inline AnimationCurve_t2757045165 ** get_address_of_m_RemapCurve_3() { return &___m_RemapCurve_3; }
	inline void set_m_RemapCurve_3(AnimationCurve_t2757045165 * value)
	{
		___m_RemapCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemapCurve_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.Blackboard.Reactor/BlackboardExpression/Line
struct Line_t1381376710_marshaled_pinvoke
{
	int32_t ___m_Operation_0;
	char* ___m_BlackboardKey_1;
	int32_t ___m_Remap_2;
	AnimationCurve_t2757045165_marshaled_pinvoke ___m_RemapCurve_3;
};
// Native definition for COM marshalling of Cinemachine.Blackboard.Reactor/BlackboardExpression/Line
struct Line_t1381376710_marshaled_com
{
	int32_t ___m_Operation_0;
	Il2CppChar* ___m_BlackboardKey_1;
	int32_t ___m_Remap_2;
	AnimationCurve_t2757045165_marshaled_com* ___m_RemapCurve_3;
};
#endif // LINE_T1381376710_H
#ifndef TARGETMODIFIER_T1423884133_H
#define TARGETMODIFIER_T1423884133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/TargetModifier
struct  TargetModifier_t1423884133 
{
public:
	// System.String Cinemachine.Blackboard.Reactor/TargetModifier::m_Field
	String_t* ___m_Field_0;
	// Cinemachine.Blackboard.Reactor/CombineMode Cinemachine.Blackboard.Reactor/TargetModifier::m_Operation
	int32_t ___m_Operation_1;
	// Cinemachine.Blackboard.Reactor/BlackboardExpression Cinemachine.Blackboard.Reactor/TargetModifier::m_Expression
	BlackboardExpression_t4195482571  ___m_Expression_2;
	// Cinemachine.Blackboard.Reactor/TargetModifier/TargetBinding Cinemachine.Blackboard.Reactor/TargetModifier::<Binding>k__BackingField
	TargetBinding_t1523791741 * ___U3CBindingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Field_0() { return static_cast<int32_t>(offsetof(TargetModifier_t1423884133, ___m_Field_0)); }
	inline String_t* get_m_Field_0() const { return ___m_Field_0; }
	inline String_t** get_address_of_m_Field_0() { return &___m_Field_0; }
	inline void set_m_Field_0(String_t* value)
	{
		___m_Field_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Field_0), value);
	}

	inline static int32_t get_offset_of_m_Operation_1() { return static_cast<int32_t>(offsetof(TargetModifier_t1423884133, ___m_Operation_1)); }
	inline int32_t get_m_Operation_1() const { return ___m_Operation_1; }
	inline int32_t* get_address_of_m_Operation_1() { return &___m_Operation_1; }
	inline void set_m_Operation_1(int32_t value)
	{
		___m_Operation_1 = value;
	}

	inline static int32_t get_offset_of_m_Expression_2() { return static_cast<int32_t>(offsetof(TargetModifier_t1423884133, ___m_Expression_2)); }
	inline BlackboardExpression_t4195482571  get_m_Expression_2() const { return ___m_Expression_2; }
	inline BlackboardExpression_t4195482571 * get_address_of_m_Expression_2() { return &___m_Expression_2; }
	inline void set_m_Expression_2(BlackboardExpression_t4195482571  value)
	{
		___m_Expression_2 = value;
	}

	inline static int32_t get_offset_of_U3CBindingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TargetModifier_t1423884133, ___U3CBindingU3Ek__BackingField_3)); }
	inline TargetBinding_t1523791741 * get_U3CBindingU3Ek__BackingField_3() const { return ___U3CBindingU3Ek__BackingField_3; }
	inline TargetBinding_t1523791741 ** get_address_of_U3CBindingU3Ek__BackingField_3() { return &___U3CBindingU3Ek__BackingField_3; }
	inline void set_U3CBindingU3Ek__BackingField_3(TargetBinding_t1523791741 * value)
	{
		___U3CBindingU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindingU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.Blackboard.Reactor/TargetModifier
struct TargetModifier_t1423884133_marshaled_pinvoke
{
	char* ___m_Field_0;
	int32_t ___m_Operation_1;
	BlackboardExpression_t4195482571_marshaled_pinvoke ___m_Expression_2;
	TargetBinding_t1523791741 * ___U3CBindingU3Ek__BackingField_3;
};
// Native definition for COM marshalling of Cinemachine.Blackboard.Reactor/TargetModifier
struct TargetModifier_t1423884133_marshaled_com
{
	Il2CppChar* ___m_Field_0;
	int32_t ___m_Operation_1;
	BlackboardExpression_t4195482571_marshaled_com ___m_Expression_2;
	TargetBinding_t1523791741 * ___U3CBindingU3Ek__BackingField_3;
};
#endif // TARGETMODIFIER_T1423884133_H
#ifndef GAMEOBJECTFIELDSCANNER_T1257740105_H
#define GAMEOBJECTFIELDSCANNER_T1257740105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner
struct  GameObjectFieldScanner_t1257740105  : public RuntimeObject
{
public:
	// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/OnLeafFieldDelegate Cinemachine.Blackboard.Reactor/GameObjectFieldScanner::OnLeafField
	OnLeafFieldDelegate_t4271637819 * ___OnLeafField_0;
	// System.Reflection.BindingFlags Cinemachine.Blackboard.Reactor/GameObjectFieldScanner::bindingFlags
	int32_t ___bindingFlags_1;

public:
	inline static int32_t get_offset_of_OnLeafField_0() { return static_cast<int32_t>(offsetof(GameObjectFieldScanner_t1257740105, ___OnLeafField_0)); }
	inline OnLeafFieldDelegate_t4271637819 * get_OnLeafField_0() const { return ___OnLeafField_0; }
	inline OnLeafFieldDelegate_t4271637819 ** get_address_of_OnLeafField_0() { return &___OnLeafField_0; }
	inline void set_OnLeafField_0(OnLeafFieldDelegate_t4271637819 * value)
	{
		___OnLeafField_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnLeafField_0), value);
	}

	inline static int32_t get_offset_of_bindingFlags_1() { return static_cast<int32_t>(offsetof(GameObjectFieldScanner_t1257740105, ___bindingFlags_1)); }
	inline int32_t get_bindingFlags_1() const { return ___bindingFlags_1; }
	inline int32_t* get_address_of_bindingFlags_1() { return &___bindingFlags_1; }
	inline void set_bindingFlags_1(int32_t value)
	{
		___bindingFlags_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFIELDSCANNER_T1257740105_H
#ifndef CUSTOMBLEND_T4150975672_H
#define CUSTOMBLEND_T4150975672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlenderSettings/CustomBlend
struct  CustomBlend_t4150975672 
{
public:
	// System.String Cinemachine.CinemachineBlenderSettings/CustomBlend::m_From
	String_t* ___m_From_0;
	// System.String Cinemachine.CinemachineBlenderSettings/CustomBlend::m_To
	String_t* ___m_To_1;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineBlenderSettings/CustomBlend::m_Blend
	CinemachineBlendDefinition_t3981438518  ___m_Blend_2;

public:
	inline static int32_t get_offset_of_m_From_0() { return static_cast<int32_t>(offsetof(CustomBlend_t4150975672, ___m_From_0)); }
	inline String_t* get_m_From_0() const { return ___m_From_0; }
	inline String_t** get_address_of_m_From_0() { return &___m_From_0; }
	inline void set_m_From_0(String_t* value)
	{
		___m_From_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_From_0), value);
	}

	inline static int32_t get_offset_of_m_To_1() { return static_cast<int32_t>(offsetof(CustomBlend_t4150975672, ___m_To_1)); }
	inline String_t* get_m_To_1() const { return ___m_To_1; }
	inline String_t** get_address_of_m_To_1() { return &___m_To_1; }
	inline void set_m_To_1(String_t* value)
	{
		___m_To_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_To_1), value);
	}

	inline static int32_t get_offset_of_m_Blend_2() { return static_cast<int32_t>(offsetof(CustomBlend_t4150975672, ___m_Blend_2)); }
	inline CinemachineBlendDefinition_t3981438518  get_m_Blend_2() const { return ___m_Blend_2; }
	inline CinemachineBlendDefinition_t3981438518 * get_address_of_m_Blend_2() { return &___m_Blend_2; }
	inline void set_m_Blend_2(CinemachineBlendDefinition_t3981438518  value)
	{
		___m_Blend_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineBlenderSettings/CustomBlend
struct CustomBlend_t4150975672_marshaled_pinvoke
{
	char* ___m_From_0;
	char* ___m_To_1;
	CinemachineBlendDefinition_t3981438518  ___m_Blend_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineBlenderSettings/CustomBlend
struct CustomBlend_t4150975672_marshaled_com
{
	Il2CppChar* ___m_From_0;
	Il2CppChar* ___m_To_1;
	CinemachineBlendDefinition_t3981438518  ___m_Blend_2;
};
#endif // CUSTOMBLEND_T4150975672_H
#ifndef AXISINPUTDELEGATE_T2029212819_H
#define AXISINPUTDELEGATE_T2029212819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/AxisInputDelegate
struct  AxisInputDelegate_t2029212819  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTDELEGATE_T2029212819_H
#ifndef GETDESCRIPTIONDELEGATE_T976785689_H
#define GETDESCRIPTIONDELEGATE_T976785689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommonListCode/GetDescriptionDelegate
struct  GetDescriptionDelegate_t976785689  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDESCRIPTIONDELEGATE_T976785689_H
#ifndef ONPOSTPIPELINESTAGEDELEGATE_T938695173_H
#define ONPOSTPIPELINESTAGEDELEGATE_T938695173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate
struct  OnPostPipelineStageDelegate_t938695173  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPOSTPIPELINESTAGEDELEGATE_T938695173_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef NOISESETTINGS_T2082871546_H
#define NOISESETTINGS_T2082871546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings
struct  NoiseSettings_t2082871546  : public ScriptableObject_t1804531341
{
public:
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::m_Position
	TransformNoiseParamsU5BU5D_t4184490513* ___m_Position_2;
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::m_Orientation
	TransformNoiseParamsU5BU5D_t4184490513* ___m_Orientation_3;

public:
	inline static int32_t get_offset_of_m_Position_2() { return static_cast<int32_t>(offsetof(NoiseSettings_t2082871546, ___m_Position_2)); }
	inline TransformNoiseParamsU5BU5D_t4184490513* get_m_Position_2() const { return ___m_Position_2; }
	inline TransformNoiseParamsU5BU5D_t4184490513** get_address_of_m_Position_2() { return &___m_Position_2; }
	inline void set_m_Position_2(TransformNoiseParamsU5BU5D_t4184490513* value)
	{
		___m_Position_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Position_2), value);
	}

	inline static int32_t get_offset_of_m_Orientation_3() { return static_cast<int32_t>(offsetof(NoiseSettings_t2082871546, ___m_Orientation_3)); }
	inline TransformNoiseParamsU5BU5D_t4184490513* get_m_Orientation_3() const { return ___m_Orientation_3; }
	inline TransformNoiseParamsU5BU5D_t4184490513** get_address_of_m_Orientation_3() { return &___m_Orientation_3; }
	inline void set_m_Orientation_3(TransformNoiseParamsU5BU5D_t4184490513* value)
	{
		___m_Orientation_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Orientation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISESETTINGS_T2082871546_H
#ifndef PLAYABLEASSET_T1461289625_H
#define PLAYABLEASSET_T1461289625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t1461289625  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T1461289625_H
#ifndef ONLEAFFIELDDELEGATE_T4271637819_H
#define ONLEAFFIELDDELEGATE_T4271637819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor/GameObjectFieldScanner/OnLeafFieldDelegate
struct  OnLeafFieldDelegate_t4271637819  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONLEAFFIELDDELEGATE_T4271637819_H
#ifndef CINEMACHINESHOT_T558745826_H
#define CINEMACHINESHOT_T558745826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineShot
struct  CinemachineShot_t558745826  : public PlayableAsset_t1461289625
{
public:
	// UnityEngine.ExposedReference`1<Cinemachine.CinemachineVirtualCameraBase> Cinemachine.Timeline.CinemachineShot::VirtualCamera
	ExposedReference_1_t2356016056  ___VirtualCamera_2;

public:
	inline static int32_t get_offset_of_VirtualCamera_2() { return static_cast<int32_t>(offsetof(CinemachineShot_t558745826, ___VirtualCamera_2)); }
	inline ExposedReference_1_t2356016056  get_VirtualCamera_2() const { return ___VirtualCamera_2; }
	inline ExposedReference_1_t2356016056 * get_address_of_VirtualCamera_2() { return &___VirtualCamera_2; }
	inline void set_VirtualCamera_2(ExposedReference_1_t2356016056  value)
	{
		___VirtualCamera_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESHOT_T558745826_H
#ifndef TRACKASSET_T2823685959_H
#define TRACKASSET_T2823685959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2823685959  : public PlayableAsset_t1461289625
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_2;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_3;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_4;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t1145879031 * ___m_AnimClip_5;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t1461289625 * ___m_Parent_6;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t1943312966 * ___m_Children_7;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_8;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t1733651323* ___m_ClipsCache_9;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t1558908690  ___m_Start_10;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t1558908690  ___m_End_11;
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackAsset::m_MediaType
	int32_t ___m_MediaType_12;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t1609610637 * ___m_Clips_14;

public:
	inline static int32_t get_offset_of_m_Locked_2() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Locked_2)); }
	inline bool get_m_Locked_2() const { return ___m_Locked_2; }
	inline bool* get_address_of_m_Locked_2() { return &___m_Locked_2; }
	inline void set_m_Locked_2(bool value)
	{
		___m_Locked_2 = value;
	}

	inline static int32_t get_offset_of_m_Muted_3() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Muted_3)); }
	inline bool get_m_Muted_3() const { return ___m_Muted_3; }
	inline bool* get_address_of_m_Muted_3() { return &___m_Muted_3; }
	inline void set_m_Muted_3(bool value)
	{
		___m_Muted_3 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_CustomPlayableFullTypename_4)); }
	inline String_t* get_m_CustomPlayableFullTypename_4() const { return ___m_CustomPlayableFullTypename_4; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_4() { return &___m_CustomPlayableFullTypename_4; }
	inline void set_m_CustomPlayableFullTypename_4(String_t* value)
	{
		___m_CustomPlayableFullTypename_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_4), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_AnimClip_5)); }
	inline AnimationClip_t1145879031 * get_m_AnimClip_5() const { return ___m_AnimClip_5; }
	inline AnimationClip_t1145879031 ** get_address_of_m_AnimClip_5() { return &___m_AnimClip_5; }
	inline void set_m_AnimClip_5(AnimationClip_t1145879031 * value)
	{
		___m_AnimClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_5), value);
	}

	inline static int32_t get_offset_of_m_Parent_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Parent_6)); }
	inline PlayableAsset_t1461289625 * get_m_Parent_6() const { return ___m_Parent_6; }
	inline PlayableAsset_t1461289625 ** get_address_of_m_Parent_6() { return &___m_Parent_6; }
	inline void set_m_Parent_6(PlayableAsset_t1461289625 * value)
	{
		___m_Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_6), value);
	}

	inline static int32_t get_offset_of_m_Children_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Children_7)); }
	inline List_1_t1943312966 * get_m_Children_7() const { return ___m_Children_7; }
	inline List_1_t1943312966 ** get_address_of_m_Children_7() { return &___m_Children_7; }
	inline void set_m_Children_7(List_1_t1943312966 * value)
	{
		___m_Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_7), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ItemsHash_8)); }
	inline int32_t get_m_ItemsHash_8() const { return ___m_ItemsHash_8; }
	inline int32_t* get_address_of_m_ItemsHash_8() { return &___m_ItemsHash_8; }
	inline void set_m_ItemsHash_8(int32_t value)
	{
		___m_ItemsHash_8 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ClipsCache_9)); }
	inline TimelineClipU5BU5D_t1733651323* get_m_ClipsCache_9() const { return ___m_ClipsCache_9; }
	inline TimelineClipU5BU5D_t1733651323** get_address_of_m_ClipsCache_9() { return &___m_ClipsCache_9; }
	inline void set_m_ClipsCache_9(TimelineClipU5BU5D_t1733651323* value)
	{
		___m_ClipsCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_9), value);
	}

	inline static int32_t get_offset_of_m_Start_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Start_10)); }
	inline DiscreteTime_t1558908690  get_m_Start_10() const { return ___m_Start_10; }
	inline DiscreteTime_t1558908690 * get_address_of_m_Start_10() { return &___m_Start_10; }
	inline void set_m_Start_10(DiscreteTime_t1558908690  value)
	{
		___m_Start_10 = value;
	}

	inline static int32_t get_offset_of_m_End_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_End_11)); }
	inline DiscreteTime_t1558908690  get_m_End_11() const { return ___m_End_11; }
	inline DiscreteTime_t1558908690 * get_address_of_m_End_11() { return &___m_End_11; }
	inline void set_m_End_11(DiscreteTime_t1558908690  value)
	{
		___m_End_11 = value;
	}

	inline static int32_t get_offset_of_m_MediaType_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_MediaType_12)); }
	inline int32_t get_m_MediaType_12() const { return ___m_MediaType_12; }
	inline int32_t* get_address_of_m_MediaType_12() { return &___m_MediaType_12; }
	inline void set_m_MediaType_12(int32_t value)
	{
		___m_MediaType_12 = value;
	}

	inline static int32_t get_offset_of_m_Clips_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Clips_14)); }
	inline List_1_t1609610637 * get_m_Clips_14() const { return ___m_Clips_14; }
	inline List_1_t1609610637 ** get_address_of_m_Clips_14() { return &___m_Clips_14; }
	inline void set_m_Clips_14(List_1_t1609610637 * value)
	{
		___m_Clips_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_14), value);
	}
};

struct TrackAsset_t2823685959_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t2389961773 * ___s_TrackBindingTypeAttributeCache_13;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t3874041483 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___s_TrackBindingTypeAttributeCache_13)); }
	inline Dictionary_2_t2389961773 * get_s_TrackBindingTypeAttributeCache_13() const { return ___s_TrackBindingTypeAttributeCache_13; }
	inline Dictionary_2_t2389961773 ** get_address_of_s_TrackBindingTypeAttributeCache_13() { return &___s_TrackBindingTypeAttributeCache_13; }
	inline void set_s_TrackBindingTypeAttributeCache_13(Dictionary_2_t2389961773 * value)
	{
		___s_TrackBindingTypeAttributeCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Comparison_1_t3874041483 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Comparison_1_t3874041483 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Comparison_1_t3874041483 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2823685959_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef REACTOR_T1306100108_H
#define REACTOR_T1306100108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Blackboard.Reactor
struct  Reactor_t1306100108  : public MonoBehaviour_t1618594486
{
public:
	// Cinemachine.Blackboard.Reactor/TargetModifier[] Cinemachine.Blackboard.Reactor::m_TargetMappings
	TargetModifierU5BU5D_t2219681128* ___m_TargetMappings_2;

public:
	inline static int32_t get_offset_of_m_TargetMappings_2() { return static_cast<int32_t>(offsetof(Reactor_t1306100108, ___m_TargetMappings_2)); }
	inline TargetModifierU5BU5D_t2219681128* get_m_TargetMappings_2() const { return ___m_TargetMappings_2; }
	inline TargetModifierU5BU5D_t2219681128** get_address_of_m_TargetMappings_2() { return &___m_TargetMappings_2; }
	inline void set_m_TargetMappings_2(TargetModifierU5BU5D_t2219681128* value)
	{
		___m_TargetMappings_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetMappings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTOR_T1306100108_H
#ifndef SCRIPTINGEXAMPLE_T2272617098_H
#define SCRIPTINGEXAMPLE_T2272617098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScriptingExample
struct  ScriptingExample_t2272617098  : public MonoBehaviour_t1618594486
{
public:
	// Cinemachine.CinemachineVirtualCamera ScriptingExample::vcam
	CinemachineVirtualCamera_t2803177733 * ___vcam_2;
	// Cinemachine.CinemachineFreeLook ScriptingExample::freelook
	CinemachineFreeLook_t411363320 * ___freelook_3;
	// System.Single ScriptingExample::lastSwapTime
	float ___lastSwapTime_4;

public:
	inline static int32_t get_offset_of_vcam_2() { return static_cast<int32_t>(offsetof(ScriptingExample_t2272617098, ___vcam_2)); }
	inline CinemachineVirtualCamera_t2803177733 * get_vcam_2() const { return ___vcam_2; }
	inline CinemachineVirtualCamera_t2803177733 ** get_address_of_vcam_2() { return &___vcam_2; }
	inline void set_vcam_2(CinemachineVirtualCamera_t2803177733 * value)
	{
		___vcam_2 = value;
		Il2CppCodeGenWriteBarrier((&___vcam_2), value);
	}

	inline static int32_t get_offset_of_freelook_3() { return static_cast<int32_t>(offsetof(ScriptingExample_t2272617098, ___freelook_3)); }
	inline CinemachineFreeLook_t411363320 * get_freelook_3() const { return ___freelook_3; }
	inline CinemachineFreeLook_t411363320 ** get_address_of_freelook_3() { return &___freelook_3; }
	inline void set_freelook_3(CinemachineFreeLook_t411363320 * value)
	{
		___freelook_3 = value;
		Il2CppCodeGenWriteBarrier((&___freelook_3), value);
	}

	inline static int32_t get_offset_of_lastSwapTime_4() { return static_cast<int32_t>(offsetof(ScriptingExample_t2272617098, ___lastSwapTime_4)); }
	inline float get_lastSwapTime_4() const { return ___lastSwapTime_4; }
	inline float* get_address_of_lastSwapTime_4() { return &___lastSwapTime_4; }
	inline void set_lastSwapTime_4(float value)
	{
		___lastSwapTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGEXAMPLE_T2272617098_H
#ifndef CINEMACHINEEXAMPLEWINDOW_T2933416263_H
#define CINEMACHINEEXAMPLEWINDOW_T2933416263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CinemachineExampleWindow
struct  CinemachineExampleWindow_t2933416263  : public MonoBehaviour_t1618594486
{
public:
	// System.String CinemachineExampleWindow::m_Title
	String_t* ___m_Title_2;
	// System.String CinemachineExampleWindow::m_Description
	String_t* ___m_Description_3;
	// System.Boolean CinemachineExampleWindow::mShowingHelpWindow
	bool ___mShowingHelpWindow_4;

public:
	inline static int32_t get_offset_of_m_Title_2() { return static_cast<int32_t>(offsetof(CinemachineExampleWindow_t2933416263, ___m_Title_2)); }
	inline String_t* get_m_Title_2() const { return ___m_Title_2; }
	inline String_t** get_address_of_m_Title_2() { return &___m_Title_2; }
	inline void set_m_Title_2(String_t* value)
	{
		___m_Title_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Title_2), value);
	}

	inline static int32_t get_offset_of_m_Description_3() { return static_cast<int32_t>(offsetof(CinemachineExampleWindow_t2933416263, ___m_Description_3)); }
	inline String_t* get_m_Description_3() const { return ___m_Description_3; }
	inline String_t** get_address_of_m_Description_3() { return &___m_Description_3; }
	inline void set_m_Description_3(String_t* value)
	{
		___m_Description_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Description_3), value);
	}

	inline static int32_t get_offset_of_mShowingHelpWindow_4() { return static_cast<int32_t>(offsetof(CinemachineExampleWindow_t2933416263, ___mShowingHelpWindow_4)); }
	inline bool get_mShowingHelpWindow_4() const { return ___mShowingHelpWindow_4; }
	inline bool* get_address_of_mShowingHelpWindow_4() { return &___mShowingHelpWindow_4; }
	inline void set_mShowingHelpWindow_4(bool value)
	{
		___mShowingHelpWindow_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEEXAMPLEWINDOW_T2933416263_H
#ifndef CINEMACHINEPOSTFX_T1775947432_H
#define CINEMACHINEPOSTFX_T1775947432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.PostFX.CinemachinePostFX
struct  CinemachinePostFX_t1775947432  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile Cinemachine.PostFX.CinemachinePostFX::m_Profile
	PostProcessingProfile_t1367512122 * ___m_Profile_2;
	// System.Boolean Cinemachine.PostFX.CinemachinePostFX::m_FocusTracksTarget
	bool ___m_FocusTracksTarget_3;
	// System.Single Cinemachine.PostFX.CinemachinePostFX::m_FocusOffset
	float ___m_FocusOffset_4;
	// Cinemachine.CinemachineBrain Cinemachine.PostFX.CinemachinePostFX::mBrain
	CinemachineBrain_t679733552 * ___mBrain_5;
	// UnityEngine.PostProcessing.PostProcessingBehaviour Cinemachine.PostFX.CinemachinePostFX::mPostProcessingBehaviour
	PostProcessingBehaviour_t1299966383 * ___mPostProcessingBehaviour_6;

public:
	inline static int32_t get_offset_of_m_Profile_2() { return static_cast<int32_t>(offsetof(CinemachinePostFX_t1775947432, ___m_Profile_2)); }
	inline PostProcessingProfile_t1367512122 * get_m_Profile_2() const { return ___m_Profile_2; }
	inline PostProcessingProfile_t1367512122 ** get_address_of_m_Profile_2() { return &___m_Profile_2; }
	inline void set_m_Profile_2(PostProcessingProfile_t1367512122 * value)
	{
		___m_Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Profile_2), value);
	}

	inline static int32_t get_offset_of_m_FocusTracksTarget_3() { return static_cast<int32_t>(offsetof(CinemachinePostFX_t1775947432, ___m_FocusTracksTarget_3)); }
	inline bool get_m_FocusTracksTarget_3() const { return ___m_FocusTracksTarget_3; }
	inline bool* get_address_of_m_FocusTracksTarget_3() { return &___m_FocusTracksTarget_3; }
	inline void set_m_FocusTracksTarget_3(bool value)
	{
		___m_FocusTracksTarget_3 = value;
	}

	inline static int32_t get_offset_of_m_FocusOffset_4() { return static_cast<int32_t>(offsetof(CinemachinePostFX_t1775947432, ___m_FocusOffset_4)); }
	inline float get_m_FocusOffset_4() const { return ___m_FocusOffset_4; }
	inline float* get_address_of_m_FocusOffset_4() { return &___m_FocusOffset_4; }
	inline void set_m_FocusOffset_4(float value)
	{
		___m_FocusOffset_4 = value;
	}

	inline static int32_t get_offset_of_mBrain_5() { return static_cast<int32_t>(offsetof(CinemachinePostFX_t1775947432, ___mBrain_5)); }
	inline CinemachineBrain_t679733552 * get_mBrain_5() const { return ___mBrain_5; }
	inline CinemachineBrain_t679733552 ** get_address_of_mBrain_5() { return &___mBrain_5; }
	inline void set_mBrain_5(CinemachineBrain_t679733552 * value)
	{
		___mBrain_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBrain_5), value);
	}

	inline static int32_t get_offset_of_mPostProcessingBehaviour_6() { return static_cast<int32_t>(offsetof(CinemachinePostFX_t1775947432, ___mPostProcessingBehaviour_6)); }
	inline PostProcessingBehaviour_t1299966383 * get_mPostProcessingBehaviour_6() const { return ___mPostProcessingBehaviour_6; }
	inline PostProcessingBehaviour_t1299966383 ** get_address_of_mPostProcessingBehaviour_6() { return &___mPostProcessingBehaviour_6; }
	inline void set_mPostProcessingBehaviour_6(PostProcessingBehaviour_t1299966383 * value)
	{
		___mPostProcessingBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPostProcessingBehaviour_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPOSTFX_T1775947432_H
#ifndef CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#define CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCameraBase
struct  CinemachineVirtualCameraBase_t2557508663  : public MonoBehaviour_t1618594486
{
public:
	// System.Action Cinemachine.CinemachineVirtualCameraBase::CinemachineGUIDebuggerCallback
	Action_t370180854 * ___CinemachineGUIDebuggerCallback_2;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_HideHeaderInInspector
	bool ___m_HideHeaderInInspector_3;
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_t2511808107* ___m_ExcludedPropertiesInInspector_4;
	// Cinemachine.CinemachineCore/Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_t2642749565* ___m_LockStageInInspector_5;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_6;
	// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate Cinemachine.CinemachineVirtualCameraBase::OnPostPipelineStage
	OnPostPipelineStageDelegate_t938695173 * ___OnPostPipelineStage_7;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::<PreviousStateInvalid>k__BackingField
	bool ___U3CPreviousStateInvalidU3Ek__BackingField_8;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_9;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_t2557508663 * ___m_parentVcam_10;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_11;

public:
	inline static int32_t get_offset_of_CinemachineGUIDebuggerCallback_2() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___CinemachineGUIDebuggerCallback_2)); }
	inline Action_t370180854 * get_CinemachineGUIDebuggerCallback_2() const { return ___CinemachineGUIDebuggerCallback_2; }
	inline Action_t370180854 ** get_address_of_CinemachineGUIDebuggerCallback_2() { return &___CinemachineGUIDebuggerCallback_2; }
	inline void set_CinemachineGUIDebuggerCallback_2(Action_t370180854 * value)
	{
		___CinemachineGUIDebuggerCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___CinemachineGUIDebuggerCallback_2), value);
	}

	inline static int32_t get_offset_of_m_HideHeaderInInspector_3() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_HideHeaderInInspector_3)); }
	inline bool get_m_HideHeaderInInspector_3() const { return ___m_HideHeaderInInspector_3; }
	inline bool* get_address_of_m_HideHeaderInInspector_3() { return &___m_HideHeaderInInspector_3; }
	inline void set_m_HideHeaderInInspector_3(bool value)
	{
		___m_HideHeaderInInspector_3 = value;
	}

	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_ExcludedPropertiesInInspector_4)); }
	inline StringU5BU5D_t2511808107* get_m_ExcludedPropertiesInInspector_4() const { return ___m_ExcludedPropertiesInInspector_4; }
	inline StringU5BU5D_t2511808107** get_address_of_m_ExcludedPropertiesInInspector_4() { return &___m_ExcludedPropertiesInInspector_4; }
	inline void set_m_ExcludedPropertiesInInspector_4(StringU5BU5D_t2511808107* value)
	{
		___m_ExcludedPropertiesInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExcludedPropertiesInInspector_4), value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_LockStageInInspector_5)); }
	inline StageU5BU5D_t2642749565* get_m_LockStageInInspector_5() const { return ___m_LockStageInInspector_5; }
	inline StageU5BU5D_t2642749565** get_address_of_m_LockStageInInspector_5() { return &___m_LockStageInInspector_5; }
	inline void set_m_LockStageInInspector_5(StageU5BU5D_t2642749565* value)
	{
		___m_LockStageInInspector_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LockStageInInspector_5), value);
	}

	inline static int32_t get_offset_of_m_Priority_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_Priority_6)); }
	inline int32_t get_m_Priority_6() const { return ___m_Priority_6; }
	inline int32_t* get_address_of_m_Priority_6() { return &___m_Priority_6; }
	inline void set_m_Priority_6(int32_t value)
	{
		___m_Priority_6 = value;
	}

	inline static int32_t get_offset_of_OnPostPipelineStage_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___OnPostPipelineStage_7)); }
	inline OnPostPipelineStageDelegate_t938695173 * get_OnPostPipelineStage_7() const { return ___OnPostPipelineStage_7; }
	inline OnPostPipelineStageDelegate_t938695173 ** get_address_of_OnPostPipelineStage_7() { return &___OnPostPipelineStage_7; }
	inline void set_OnPostPipelineStage_7(OnPostPipelineStageDelegate_t938695173 * value)
	{
		___OnPostPipelineStage_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostPipelineStage_7), value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateInvalidU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___U3CPreviousStateInvalidU3Ek__BackingField_8)); }
	inline bool get_U3CPreviousStateInvalidU3Ek__BackingField_8() const { return ___U3CPreviousStateInvalidU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CPreviousStateInvalidU3Ek__BackingField_8() { return &___U3CPreviousStateInvalidU3Ek__BackingField_8; }
	inline void set_U3CPreviousStateInvalidU3Ek__BackingField_8(bool value)
	{
		___U3CPreviousStateInvalidU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___mSlaveStatusUpdated_9)); }
	inline bool get_mSlaveStatusUpdated_9() const { return ___mSlaveStatusUpdated_9; }
	inline bool* get_address_of_mSlaveStatusUpdated_9() { return &___mSlaveStatusUpdated_9; }
	inline void set_mSlaveStatusUpdated_9(bool value)
	{
		___mSlaveStatusUpdated_9 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_parentVcam_10)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_m_parentVcam_10() const { return ___m_parentVcam_10; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_m_parentVcam_10() { return &___m_parentVcam_10; }
	inline void set_m_parentVcam_10(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___m_parentVcam_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentVcam_10), value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_QueuePriority_11)); }
	inline int32_t get_m_QueuePriority_11() const { return ___m_QueuePriority_11; }
	inline int32_t* get_address_of_m_QueuePriority_11() { return &___m_QueuePriority_11; }
	inline void set_m_QueuePriority_11(int32_t value)
	{
		___m_QueuePriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#ifndef CINEMACHINETRACK_T127841354_H
#define CINEMACHINETRACK_T127841354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineTrack
struct  CinemachineTrack_t127841354  : public TrackAsset_t2823685959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRACK_T127841354_H
#ifndef CLOSEICETRIGGER_T760838387_H
#define CLOSEICETRIGGER_T760838387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseIceTrigger
struct  CloseIceTrigger_t760838387  : public MonoBehaviour_t1618594486
{
public:
	// LevelTwoScript CloseIceTrigger::l2S
	LevelTwoScript_t1243006306 * ___l2S_2;

public:
	inline static int32_t get_offset_of_l2S_2() { return static_cast<int32_t>(offsetof(CloseIceTrigger_t760838387, ___l2S_2)); }
	inline LevelTwoScript_t1243006306 * get_l2S_2() const { return ___l2S_2; }
	inline LevelTwoScript_t1243006306 ** get_address_of_l2S_2() { return &___l2S_2; }
	inline void set_l2S_2(LevelTwoScript_t1243006306 * value)
	{
		___l2S_2 = value;
		Il2CppCodeGenWriteBarrier((&___l2S_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEICETRIGGER_T760838387_H
#ifndef COLLECTIBLE_T2045918932_H
#define COLLECTIBLE_T2045918932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Collectible
struct  Collectible_t2045918932  : public MonoBehaviour_t1618594486
{
public:
	// System.String Collectible::strID
	String_t* ___strID_2;
	// TokenHolderLegacy Collectible::instanceTHL
	TokenHolderLegacy_t1380075429 * ___instanceTHL_3;

public:
	inline static int32_t get_offset_of_strID_2() { return static_cast<int32_t>(offsetof(Collectible_t2045918932, ___strID_2)); }
	inline String_t* get_strID_2() const { return ___strID_2; }
	inline String_t** get_address_of_strID_2() { return &___strID_2; }
	inline void set_strID_2(String_t* value)
	{
		___strID_2 = value;
		Il2CppCodeGenWriteBarrier((&___strID_2), value);
	}

	inline static int32_t get_offset_of_instanceTHL_3() { return static_cast<int32_t>(offsetof(Collectible_t2045918932, ___instanceTHL_3)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceTHL_3() const { return ___instanceTHL_3; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceTHL_3() { return &___instanceTHL_3; }
	inline void set_instanceTHL_3(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceTHL_3 = value;
		Il2CppCodeGenWriteBarrier((&___instanceTHL_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIBLE_T2045918932_H
#ifndef CINEMACHINEPATHBASE_T3902360541_H
#define CINEMACHINEPATHBASE_T3902360541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePathBase
struct  CinemachinePathBase_t3902360541  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPATHBASE_T3902360541_H
#ifndef CUTSCENEHOLDER_T29886913_H
#define CUTSCENEHOLDER_T29886913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CutsceneHolder
struct  CutsceneHolder_t29886913  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject CutsceneHolder::gemPlacementCutscene
	GameObject_t2557347079 * ___gemPlacementCutscene_3;

public:
	inline static int32_t get_offset_of_gemPlacementCutscene_3() { return static_cast<int32_t>(offsetof(CutsceneHolder_t29886913, ___gemPlacementCutscene_3)); }
	inline GameObject_t2557347079 * get_gemPlacementCutscene_3() const { return ___gemPlacementCutscene_3; }
	inline GameObject_t2557347079 ** get_address_of_gemPlacementCutscene_3() { return &___gemPlacementCutscene_3; }
	inline void set_gemPlacementCutscene_3(GameObject_t2557347079 * value)
	{
		___gemPlacementCutscene_3 = value;
		Il2CppCodeGenWriteBarrier((&___gemPlacementCutscene_3), value);
	}
};

struct CutsceneHolder_t29886913_StaticFields
{
public:
	// CutsceneHolder CutsceneHolder::instance
	CutsceneHolder_t29886913 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CutsceneHolder_t29886913_StaticFields, ___instance_2)); }
	inline CutsceneHolder_t29886913 * get_instance_2() const { return ___instance_2; }
	inline CutsceneHolder_t29886913 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CutsceneHolder_t29886913 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUTSCENEHOLDER_T29886913_H
#ifndef DESTROYPARTICLE_T2595860380_H
#define DESTROYPARTICLE_T2595860380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyParticle
struct  DestroyParticle_t2595860380  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYPARTICLE_T2595860380_H
#ifndef DECALDESTROYER_T2158806118_H
#define DECALDESTROYER_T2158806118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecalDestroyer
struct  DecalDestroyer_t2158806118  : public MonoBehaviour_t1618594486
{
public:
	// System.Single DecalDestroyer::lifeTime
	float ___lifeTime_2;

public:
	inline static int32_t get_offset_of_lifeTime_2() { return static_cast<int32_t>(offsetof(DecalDestroyer_t2158806118, ___lifeTime_2)); }
	inline float get_lifeTime_2() const { return ___lifeTime_2; }
	inline float* get_address_of_lifeTime_2() { return &___lifeTime_2; }
	inline void set_lifeTime_2(float value)
	{
		___lifeTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECALDESTROYER_T2158806118_H
#ifndef EXTINGUISHABLEFIRE_T2564158560_H
#define EXTINGUISHABLEFIRE_T2564158560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire
struct  ExtinguishableFire_t2564158560  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.ParticleSystem ExtinguishableFire::fireParticleSystem
	ParticleSystem_t2248283753 * ___fireParticleSystem_2;
	// UnityEngine.ParticleSystem ExtinguishableFire::smokeParticleSystem
	ParticleSystem_t2248283753 * ___smokeParticleSystem_3;
	// System.Boolean ExtinguishableFire::m_isExtinguished
	bool ___m_isExtinguished_4;

public:
	inline static int32_t get_offset_of_fireParticleSystem_2() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t2564158560, ___fireParticleSystem_2)); }
	inline ParticleSystem_t2248283753 * get_fireParticleSystem_2() const { return ___fireParticleSystem_2; }
	inline ParticleSystem_t2248283753 ** get_address_of_fireParticleSystem_2() { return &___fireParticleSystem_2; }
	inline void set_fireParticleSystem_2(ParticleSystem_t2248283753 * value)
	{
		___fireParticleSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___fireParticleSystem_2), value);
	}

	inline static int32_t get_offset_of_smokeParticleSystem_3() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t2564158560, ___smokeParticleSystem_3)); }
	inline ParticleSystem_t2248283753 * get_smokeParticleSystem_3() const { return ___smokeParticleSystem_3; }
	inline ParticleSystem_t2248283753 ** get_address_of_smokeParticleSystem_3() { return &___smokeParticleSystem_3; }
	inline void set_smokeParticleSystem_3(ParticleSystem_t2248283753 * value)
	{
		___smokeParticleSystem_3 = value;
		Il2CppCodeGenWriteBarrier((&___smokeParticleSystem_3), value);
	}

	inline static int32_t get_offset_of_m_isExtinguished_4() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t2564158560, ___m_isExtinguished_4)); }
	inline bool get_m_isExtinguished_4() const { return ___m_isExtinguished_4; }
	inline bool* get_address_of_m_isExtinguished_4() { return &___m_isExtinguished_4; }
	inline void set_m_isExtinguished_4(bool value)
	{
		___m_isExtinguished_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTINGUISHABLEFIRE_T2564158560_H
#ifndef GUNAIM_T211531317_H
#define GUNAIM_T211531317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GunAim
struct  GunAim_t211531317  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 GunAim::borderLeft
	int32_t ___borderLeft_2;
	// System.Int32 GunAim::borderRight
	int32_t ___borderRight_3;
	// System.Int32 GunAim::borderTop
	int32_t ___borderTop_4;
	// System.Int32 GunAim::borderBottom
	int32_t ___borderBottom_5;
	// UnityEngine.Camera GunAim::parentCamera
	Camera_t2839736942 * ___parentCamera_6;
	// System.Boolean GunAim::isOutOfBounds
	bool ___isOutOfBounds_7;

public:
	inline static int32_t get_offset_of_borderLeft_2() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___borderLeft_2)); }
	inline int32_t get_borderLeft_2() const { return ___borderLeft_2; }
	inline int32_t* get_address_of_borderLeft_2() { return &___borderLeft_2; }
	inline void set_borderLeft_2(int32_t value)
	{
		___borderLeft_2 = value;
	}

	inline static int32_t get_offset_of_borderRight_3() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___borderRight_3)); }
	inline int32_t get_borderRight_3() const { return ___borderRight_3; }
	inline int32_t* get_address_of_borderRight_3() { return &___borderRight_3; }
	inline void set_borderRight_3(int32_t value)
	{
		___borderRight_3 = value;
	}

	inline static int32_t get_offset_of_borderTop_4() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___borderTop_4)); }
	inline int32_t get_borderTop_4() const { return ___borderTop_4; }
	inline int32_t* get_address_of_borderTop_4() { return &___borderTop_4; }
	inline void set_borderTop_4(int32_t value)
	{
		___borderTop_4 = value;
	}

	inline static int32_t get_offset_of_borderBottom_5() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___borderBottom_5)); }
	inline int32_t get_borderBottom_5() const { return ___borderBottom_5; }
	inline int32_t* get_address_of_borderBottom_5() { return &___borderBottom_5; }
	inline void set_borderBottom_5(int32_t value)
	{
		___borderBottom_5 = value;
	}

	inline static int32_t get_offset_of_parentCamera_6() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___parentCamera_6)); }
	inline Camera_t2839736942 * get_parentCamera_6() const { return ___parentCamera_6; }
	inline Camera_t2839736942 ** get_address_of_parentCamera_6() { return &___parentCamera_6; }
	inline void set_parentCamera_6(Camera_t2839736942 * value)
	{
		___parentCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___parentCamera_6), value);
	}

	inline static int32_t get_offset_of_isOutOfBounds_7() { return static_cast<int32_t>(offsetof(GunAim_t211531317, ___isOutOfBounds_7)); }
	inline bool get_isOutOfBounds_7() const { return ___isOutOfBounds_7; }
	inline bool* get_address_of_isOutOfBounds_7() { return &___isOutOfBounds_7; }
	inline void set_isOutOfBounds_7(bool value)
	{
		___isOutOfBounds_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNAIM_T211531317_H
#ifndef IMPROVELIGHTQUALITY_T466835790_H
#define IMPROVELIGHTQUALITY_T466835790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImproveLightQuality
struct  ImproveLightQuality_t466835790  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Color ImproveLightQuality::startColour
	Color_t2582018970  ___startColour_2;
	// UnityEngine.Color ImproveLightQuality::endColour
	Color_t2582018970  ___endColour_3;
	// System.Single ImproveLightQuality::startIntensity
	float ___startIntensity_4;
	// System.Single ImproveLightQuality::endIntensity
	float ___endIntensity_5;
	// UnityEngine.Light ImproveLightQuality::light
	Light_t1775228881 * ___light_6;

public:
	inline static int32_t get_offset_of_startColour_2() { return static_cast<int32_t>(offsetof(ImproveLightQuality_t466835790, ___startColour_2)); }
	inline Color_t2582018970  get_startColour_2() const { return ___startColour_2; }
	inline Color_t2582018970 * get_address_of_startColour_2() { return &___startColour_2; }
	inline void set_startColour_2(Color_t2582018970  value)
	{
		___startColour_2 = value;
	}

	inline static int32_t get_offset_of_endColour_3() { return static_cast<int32_t>(offsetof(ImproveLightQuality_t466835790, ___endColour_3)); }
	inline Color_t2582018970  get_endColour_3() const { return ___endColour_3; }
	inline Color_t2582018970 * get_address_of_endColour_3() { return &___endColour_3; }
	inline void set_endColour_3(Color_t2582018970  value)
	{
		___endColour_3 = value;
	}

	inline static int32_t get_offset_of_startIntensity_4() { return static_cast<int32_t>(offsetof(ImproveLightQuality_t466835790, ___startIntensity_4)); }
	inline float get_startIntensity_4() const { return ___startIntensity_4; }
	inline float* get_address_of_startIntensity_4() { return &___startIntensity_4; }
	inline void set_startIntensity_4(float value)
	{
		___startIntensity_4 = value;
	}

	inline static int32_t get_offset_of_endIntensity_5() { return static_cast<int32_t>(offsetof(ImproveLightQuality_t466835790, ___endIntensity_5)); }
	inline float get_endIntensity_5() const { return ___endIntensity_5; }
	inline float* get_address_of_endIntensity_5() { return &___endIntensity_5; }
	inline void set_endIntensity_5(float value)
	{
		___endIntensity_5 = value;
	}

	inline static int32_t get_offset_of_light_6() { return static_cast<int32_t>(offsetof(ImproveLightQuality_t466835790, ___light_6)); }
	inline Light_t1775228881 * get_light_6() const { return ___light_6; }
	inline Light_t1775228881 ** get_address_of_light_6() { return &___light_6; }
	inline void set_light_6(Light_t1775228881 * value)
	{
		___light_6 = value;
		Il2CppCodeGenWriteBarrier((&___light_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPROVELIGHTQUALITY_T466835790_H
#ifndef JUMPBUTTON_T162985334_H
#define JUMPBUTTON_T162985334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpButton
struct  JumpButton_t162985334  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.RectTransform JumpButton::rt
	RectTransform_t859616204 * ___rt_2;

public:
	inline static int32_t get_offset_of_rt_2() { return static_cast<int32_t>(offsetof(JumpButton_t162985334, ___rt_2)); }
	inline RectTransform_t859616204 * get_rt_2() const { return ___rt_2; }
	inline RectTransform_t859616204 ** get_address_of_rt_2() { return &___rt_2; }
	inline void set_rt_2(RectTransform_t859616204 * value)
	{
		___rt_2 = value;
		Il2CppCodeGenWriteBarrier((&___rt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPBUTTON_T162985334_H
#ifndef DEMOCAMERASELECTOR_T3185491292_H
#define DEMOCAMERASELECTOR_T3185491292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoCameraSelector
struct  DemoCameraSelector_t3185491292  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera[] DemoCameraSelector::cameras
	CameraU5BU5D_t4045078555* ___cameras_2;
	// System.Int32 DemoCameraSelector::index
	int32_t ___index_3;
	// UnityEngine.UI.RawImage DemoCameraSelector::diff
	RawImage_t88488728 * ___diff_4;
	// UnityEngine.UI.RawImage DemoCameraSelector::heig
	RawImage_t88488728 * ___heig_5;
	// UnityEngine.UI.Text DemoCameraSelector::power
	Text_t1790657652 * ___power_6;
	// UnityEngine.UI.Text DemoCameraSelector::speed
	Text_t1790657652 * ___speed_7;
	// UnityEngine.UI.Text DemoCameraSelector::meshName
	Text_t1790657652 * ___meshName_8;

public:
	inline static int32_t get_offset_of_cameras_2() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___cameras_2)); }
	inline CameraU5BU5D_t4045078555* get_cameras_2() const { return ___cameras_2; }
	inline CameraU5BU5D_t4045078555** get_address_of_cameras_2() { return &___cameras_2; }
	inline void set_cameras_2(CameraU5BU5D_t4045078555* value)
	{
		___cameras_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameras_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_diff_4() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___diff_4)); }
	inline RawImage_t88488728 * get_diff_4() const { return ___diff_4; }
	inline RawImage_t88488728 ** get_address_of_diff_4() { return &___diff_4; }
	inline void set_diff_4(RawImage_t88488728 * value)
	{
		___diff_4 = value;
		Il2CppCodeGenWriteBarrier((&___diff_4), value);
	}

	inline static int32_t get_offset_of_heig_5() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___heig_5)); }
	inline RawImage_t88488728 * get_heig_5() const { return ___heig_5; }
	inline RawImage_t88488728 ** get_address_of_heig_5() { return &___heig_5; }
	inline void set_heig_5(RawImage_t88488728 * value)
	{
		___heig_5 = value;
		Il2CppCodeGenWriteBarrier((&___heig_5), value);
	}

	inline static int32_t get_offset_of_power_6() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___power_6)); }
	inline Text_t1790657652 * get_power_6() const { return ___power_6; }
	inline Text_t1790657652 ** get_address_of_power_6() { return &___power_6; }
	inline void set_power_6(Text_t1790657652 * value)
	{
		___power_6 = value;
		Il2CppCodeGenWriteBarrier((&___power_6), value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___speed_7)); }
	inline Text_t1790657652 * get_speed_7() const { return ___speed_7; }
	inline Text_t1790657652 ** get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(Text_t1790657652 * value)
	{
		___speed_7 = value;
		Il2CppCodeGenWriteBarrier((&___speed_7), value);
	}

	inline static int32_t get_offset_of_meshName_8() { return static_cast<int32_t>(offsetof(DemoCameraSelector_t3185491292, ___meshName_8)); }
	inline Text_t1790657652 * get_meshName_8() const { return ___meshName_8; }
	inline Text_t1790657652 ** get_address_of_meshName_8() { return &___meshName_8; }
	inline void set_meshName_8(Text_t1790657652 * value)
	{
		___meshName_8 = value;
		Il2CppCodeGenWriteBarrier((&___meshName_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOCAMERASELECTOR_T3185491292_H
#ifndef LEVELSELECTOR_T1659548950_H
#define LEVELSELECTOR_T1659548950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelector
struct  LevelSelector_t1659548950  : public MonoBehaviour_t1618594486
{
public:
	// System.String LevelSelector::levelString
	String_t* ___levelString_2;
	// CapturedDataInput LevelSelector::dataInput
	CapturedDataInput_t2616152122 * ___dataInput_3;
	// System.Boolean LevelSelector::sendOffMoreCalls
	bool ___sendOffMoreCalls_4;
	// UnityEngine.UI.InputField LevelSelector::usernameField
	InputField_t3123460221 * ___usernameField_5;
	// System.Boolean LevelSelector::reset
	bool ___reset_6;
	// System.Collections.Generic.List`1<System.String> LevelSelector::options
	List_1_t4069179741 * ___options_7;

public:
	inline static int32_t get_offset_of_levelString_2() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___levelString_2)); }
	inline String_t* get_levelString_2() const { return ___levelString_2; }
	inline String_t** get_address_of_levelString_2() { return &___levelString_2; }
	inline void set_levelString_2(String_t* value)
	{
		___levelString_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelString_2), value);
	}

	inline static int32_t get_offset_of_dataInput_3() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___dataInput_3)); }
	inline CapturedDataInput_t2616152122 * get_dataInput_3() const { return ___dataInput_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_dataInput_3() { return &___dataInput_3; }
	inline void set_dataInput_3(CapturedDataInput_t2616152122 * value)
	{
		___dataInput_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataInput_3), value);
	}

	inline static int32_t get_offset_of_sendOffMoreCalls_4() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___sendOffMoreCalls_4)); }
	inline bool get_sendOffMoreCalls_4() const { return ___sendOffMoreCalls_4; }
	inline bool* get_address_of_sendOffMoreCalls_4() { return &___sendOffMoreCalls_4; }
	inline void set_sendOffMoreCalls_4(bool value)
	{
		___sendOffMoreCalls_4 = value;
	}

	inline static int32_t get_offset_of_usernameField_5() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___usernameField_5)); }
	inline InputField_t3123460221 * get_usernameField_5() const { return ___usernameField_5; }
	inline InputField_t3123460221 ** get_address_of_usernameField_5() { return &___usernameField_5; }
	inline void set_usernameField_5(InputField_t3123460221 * value)
	{
		___usernameField_5 = value;
		Il2CppCodeGenWriteBarrier((&___usernameField_5), value);
	}

	inline static int32_t get_offset_of_reset_6() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___reset_6)); }
	inline bool get_reset_6() const { return ___reset_6; }
	inline bool* get_address_of_reset_6() { return &___reset_6; }
	inline void set_reset_6(bool value)
	{
		___reset_6 = value;
	}

	inline static int32_t get_offset_of_options_7() { return static_cast<int32_t>(offsetof(LevelSelector_t1659548950, ___options_7)); }
	inline List_1_t4069179741 * get_options_7() const { return ___options_7; }
	inline List_1_t4069179741 ** get_address_of_options_7() { return &___options_7; }
	inline void set_options_7(List_1_t4069179741 * value)
	{
		___options_7 = value;
		Il2CppCodeGenWriteBarrier((&___options_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECTOR_T1659548950_H
#ifndef LIGHTHOLDER_T677068996_H
#define LIGHTHOLDER_T677068996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightHolder
struct  LightHolder_t677068996  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] LightHolder::lights
	GameObjectU5BU5D_t2988620542* ___lights_2;

public:
	inline static int32_t get_offset_of_lights_2() { return static_cast<int32_t>(offsetof(LightHolder_t677068996, ___lights_2)); }
	inline GameObjectU5BU5D_t2988620542* get_lights_2() const { return ___lights_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_lights_2() { return &___lights_2; }
	inline void set_lights_2(GameObjectU5BU5D_t2988620542* value)
	{
		___lights_2 = value;
		Il2CppCodeGenWriteBarrier((&___lights_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTHOLDER_T677068996_H
#ifndef LISTSCREEN_T2784779182_H
#define LISTSCREEN_T2784779182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ListScreen
struct  ListScreen_t2784779182  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject ListScreen::dragAndDropPrefab
	GameObject_t2557347079 * ___dragAndDropPrefab_2;
	// UnityEngine.UI.Text ListScreen::title
	Text_t1790657652 * ___title_3;
	// UnityEngine.UI.Text ListScreen::description
	Text_t1790657652 * ___description_4;
	// UnityEngine.RectTransform ListScreen::leftWordsSpawner
	RectTransform_t859616204 * ___leftWordsSpawner_5;
	// UnityEngine.RectTransform ListScreen::rightWordsSpawner
	RectTransform_t859616204 * ___rightWordsSpawner_6;
	// System.Single ListScreen::snapDistance
	float ___snapDistance_7;
	// UnityEngine.RectTransform[] ListScreen::snapPoints
	RectTransformU5BU5D_t602728325* ___snapPoints_8;
	// System.Single ListScreen::ySpacing
	float ___ySpacing_9;
	// UnityEngine.GameObject ListScreen::nextButton
	GameObject_t2557347079 * ___nextButton_10;
	// System.Boolean ListScreen::bReadyToContinue
	bool ___bReadyToContinue_11;
	// System.String[] ListScreen::descriptions
	StringU5BU5D_t2511808107* ___descriptions_12;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> ListScreen::dragAndDrops
	List_1_t4274210507 * ___dragAndDrops_13;

public:
	inline static int32_t get_offset_of_dragAndDropPrefab_2() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___dragAndDropPrefab_2)); }
	inline GameObject_t2557347079 * get_dragAndDropPrefab_2() const { return ___dragAndDropPrefab_2; }
	inline GameObject_t2557347079 ** get_address_of_dragAndDropPrefab_2() { return &___dragAndDropPrefab_2; }
	inline void set_dragAndDropPrefab_2(GameObject_t2557347079 * value)
	{
		___dragAndDropPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___dragAndDropPrefab_2), value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___title_3)); }
	inline Text_t1790657652 * get_title_3() const { return ___title_3; }
	inline Text_t1790657652 ** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(Text_t1790657652 * value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___description_4)); }
	inline Text_t1790657652 * get_description_4() const { return ___description_4; }
	inline Text_t1790657652 ** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(Text_t1790657652 * value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_leftWordsSpawner_5() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___leftWordsSpawner_5)); }
	inline RectTransform_t859616204 * get_leftWordsSpawner_5() const { return ___leftWordsSpawner_5; }
	inline RectTransform_t859616204 ** get_address_of_leftWordsSpawner_5() { return &___leftWordsSpawner_5; }
	inline void set_leftWordsSpawner_5(RectTransform_t859616204 * value)
	{
		___leftWordsSpawner_5 = value;
		Il2CppCodeGenWriteBarrier((&___leftWordsSpawner_5), value);
	}

	inline static int32_t get_offset_of_rightWordsSpawner_6() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___rightWordsSpawner_6)); }
	inline RectTransform_t859616204 * get_rightWordsSpawner_6() const { return ___rightWordsSpawner_6; }
	inline RectTransform_t859616204 ** get_address_of_rightWordsSpawner_6() { return &___rightWordsSpawner_6; }
	inline void set_rightWordsSpawner_6(RectTransform_t859616204 * value)
	{
		___rightWordsSpawner_6 = value;
		Il2CppCodeGenWriteBarrier((&___rightWordsSpawner_6), value);
	}

	inline static int32_t get_offset_of_snapDistance_7() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___snapDistance_7)); }
	inline float get_snapDistance_7() const { return ___snapDistance_7; }
	inline float* get_address_of_snapDistance_7() { return &___snapDistance_7; }
	inline void set_snapDistance_7(float value)
	{
		___snapDistance_7 = value;
	}

	inline static int32_t get_offset_of_snapPoints_8() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___snapPoints_8)); }
	inline RectTransformU5BU5D_t602728325* get_snapPoints_8() const { return ___snapPoints_8; }
	inline RectTransformU5BU5D_t602728325** get_address_of_snapPoints_8() { return &___snapPoints_8; }
	inline void set_snapPoints_8(RectTransformU5BU5D_t602728325* value)
	{
		___snapPoints_8 = value;
		Il2CppCodeGenWriteBarrier((&___snapPoints_8), value);
	}

	inline static int32_t get_offset_of_ySpacing_9() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___ySpacing_9)); }
	inline float get_ySpacing_9() const { return ___ySpacing_9; }
	inline float* get_address_of_ySpacing_9() { return &___ySpacing_9; }
	inline void set_ySpacing_9(float value)
	{
		___ySpacing_9 = value;
	}

	inline static int32_t get_offset_of_nextButton_10() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___nextButton_10)); }
	inline GameObject_t2557347079 * get_nextButton_10() const { return ___nextButton_10; }
	inline GameObject_t2557347079 ** get_address_of_nextButton_10() { return &___nextButton_10; }
	inline void set_nextButton_10(GameObject_t2557347079 * value)
	{
		___nextButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_10), value);
	}

	inline static int32_t get_offset_of_bReadyToContinue_11() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___bReadyToContinue_11)); }
	inline bool get_bReadyToContinue_11() const { return ___bReadyToContinue_11; }
	inline bool* get_address_of_bReadyToContinue_11() { return &___bReadyToContinue_11; }
	inline void set_bReadyToContinue_11(bool value)
	{
		___bReadyToContinue_11 = value;
	}

	inline static int32_t get_offset_of_descriptions_12() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___descriptions_12)); }
	inline StringU5BU5D_t2511808107* get_descriptions_12() const { return ___descriptions_12; }
	inline StringU5BU5D_t2511808107** get_address_of_descriptions_12() { return &___descriptions_12; }
	inline void set_descriptions_12(StringU5BU5D_t2511808107* value)
	{
		___descriptions_12 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_12), value);
	}

	inline static int32_t get_offset_of_dragAndDrops_13() { return static_cast<int32_t>(offsetof(ListScreen_t2784779182, ___dragAndDrops_13)); }
	inline List_1_t4274210507 * get_dragAndDrops_13() const { return ___dragAndDrops_13; }
	inline List_1_t4274210507 ** get_address_of_dragAndDrops_13() { return &___dragAndDrops_13; }
	inline void set_dragAndDrops_13(List_1_t4274210507 * value)
	{
		___dragAndDrops_13 = value;
		Il2CppCodeGenWriteBarrier((&___dragAndDrops_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTSCREEN_T2784779182_H
#ifndef MAGICBUTTONSCRIPT_T1297805390_H
#define MAGICBUTTONSCRIPT_T1297805390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicButtonScript
struct  MagicButtonScript_t1297805390  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject MagicArsenal.MagicButtonScript::Button
	GameObject_t2557347079 * ___Button_2;
	// UnityEngine.UI.Text MagicArsenal.MagicButtonScript::MyButtonText
	Text_t1790657652 * ___MyButtonText_3;
	// System.String MagicArsenal.MagicButtonScript::projectileParticleName
	String_t* ___projectileParticleName_4;
	// MagicArsenal.MagicFireProjectile MagicArsenal.MagicButtonScript::effectScript
	MagicFireProjectile_t3063101194 * ___effectScript_5;
	// MagicProjectileScript MagicArsenal.MagicButtonScript::projectileScript
	MagicProjectileScript_t1654175834 * ___projectileScript_6;
	// System.Single MagicArsenal.MagicButtonScript::buttonsX
	float ___buttonsX_7;
	// System.Single MagicArsenal.MagicButtonScript::buttonsY
	float ___buttonsY_8;
	// System.Single MagicArsenal.MagicButtonScript::buttonsSizeX
	float ___buttonsSizeX_9;
	// System.Single MagicArsenal.MagicButtonScript::buttonsSizeY
	float ___buttonsSizeY_10;
	// System.Single MagicArsenal.MagicButtonScript::buttonsDistance
	float ___buttonsDistance_11;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___Button_2)); }
	inline GameObject_t2557347079 * get_Button_2() const { return ___Button_2; }
	inline GameObject_t2557347079 ** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(GameObject_t2557347079 * value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier((&___Button_2), value);
	}

	inline static int32_t get_offset_of_MyButtonText_3() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___MyButtonText_3)); }
	inline Text_t1790657652 * get_MyButtonText_3() const { return ___MyButtonText_3; }
	inline Text_t1790657652 ** get_address_of_MyButtonText_3() { return &___MyButtonText_3; }
	inline void set_MyButtonText_3(Text_t1790657652 * value)
	{
		___MyButtonText_3 = value;
		Il2CppCodeGenWriteBarrier((&___MyButtonText_3), value);
	}

	inline static int32_t get_offset_of_projectileParticleName_4() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___projectileParticleName_4)); }
	inline String_t* get_projectileParticleName_4() const { return ___projectileParticleName_4; }
	inline String_t** get_address_of_projectileParticleName_4() { return &___projectileParticleName_4; }
	inline void set_projectileParticleName_4(String_t* value)
	{
		___projectileParticleName_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectileParticleName_4), value);
	}

	inline static int32_t get_offset_of_effectScript_5() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___effectScript_5)); }
	inline MagicFireProjectile_t3063101194 * get_effectScript_5() const { return ___effectScript_5; }
	inline MagicFireProjectile_t3063101194 ** get_address_of_effectScript_5() { return &___effectScript_5; }
	inline void set_effectScript_5(MagicFireProjectile_t3063101194 * value)
	{
		___effectScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___effectScript_5), value);
	}

	inline static int32_t get_offset_of_projectileScript_6() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___projectileScript_6)); }
	inline MagicProjectileScript_t1654175834 * get_projectileScript_6() const { return ___projectileScript_6; }
	inline MagicProjectileScript_t1654175834 ** get_address_of_projectileScript_6() { return &___projectileScript_6; }
	inline void set_projectileScript_6(MagicProjectileScript_t1654175834 * value)
	{
		___projectileScript_6 = value;
		Il2CppCodeGenWriteBarrier((&___projectileScript_6), value);
	}

	inline static int32_t get_offset_of_buttonsX_7() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___buttonsX_7)); }
	inline float get_buttonsX_7() const { return ___buttonsX_7; }
	inline float* get_address_of_buttonsX_7() { return &___buttonsX_7; }
	inline void set_buttonsX_7(float value)
	{
		___buttonsX_7 = value;
	}

	inline static int32_t get_offset_of_buttonsY_8() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___buttonsY_8)); }
	inline float get_buttonsY_8() const { return ___buttonsY_8; }
	inline float* get_address_of_buttonsY_8() { return &___buttonsY_8; }
	inline void set_buttonsY_8(float value)
	{
		___buttonsY_8 = value;
	}

	inline static int32_t get_offset_of_buttonsSizeX_9() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___buttonsSizeX_9)); }
	inline float get_buttonsSizeX_9() const { return ___buttonsSizeX_9; }
	inline float* get_address_of_buttonsSizeX_9() { return &___buttonsSizeX_9; }
	inline void set_buttonsSizeX_9(float value)
	{
		___buttonsSizeX_9 = value;
	}

	inline static int32_t get_offset_of_buttonsSizeY_10() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___buttonsSizeY_10)); }
	inline float get_buttonsSizeY_10() const { return ___buttonsSizeY_10; }
	inline float* get_address_of_buttonsSizeY_10() { return &___buttonsSizeY_10; }
	inline void set_buttonsSizeY_10(float value)
	{
		___buttonsSizeY_10 = value;
	}

	inline static int32_t get_offset_of_buttonsDistance_11() { return static_cast<int32_t>(offsetof(MagicButtonScript_t1297805390, ___buttonsDistance_11)); }
	inline float get_buttonsDistance_11() const { return ___buttonsDistance_11; }
	inline float* get_address_of_buttonsDistance_11() { return &___buttonsDistance_11; }
	inline void set_buttonsDistance_11(float value)
	{
		___buttonsDistance_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICBUTTONSCRIPT_T1297805390_H
#ifndef MAGICDRAGMOUSEORBIT_T2106366795_H
#define MAGICDRAGMOUSEORBIT_T2106366795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicDragMouseOrbit
struct  MagicDragMouseOrbit_t2106366795  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform MagicArsenal.MagicDragMouseOrbit::target
	Transform_t362059596 * ___target_2;
	// System.Single MagicArsenal.MagicDragMouseOrbit::distance
	float ___distance_3;
	// System.Single MagicArsenal.MagicDragMouseOrbit::xSpeed
	float ___xSpeed_4;
	// System.Single MagicArsenal.MagicDragMouseOrbit::ySpeed
	float ___ySpeed_5;
	// System.Single MagicArsenal.MagicDragMouseOrbit::yMinLimit
	float ___yMinLimit_6;
	// System.Single MagicArsenal.MagicDragMouseOrbit::yMaxLimit
	float ___yMaxLimit_7;
	// System.Single MagicArsenal.MagicDragMouseOrbit::distanceMin
	float ___distanceMin_8;
	// System.Single MagicArsenal.MagicDragMouseOrbit::distanceMax
	float ___distanceMax_9;
	// System.Single MagicArsenal.MagicDragMouseOrbit::smoothTime
	float ___smoothTime_10;
	// System.Single MagicArsenal.MagicDragMouseOrbit::rotationYAxis
	float ___rotationYAxis_11;
	// System.Single MagicArsenal.MagicDragMouseOrbit::rotationXAxis
	float ___rotationXAxis_12;
	// System.Single MagicArsenal.MagicDragMouseOrbit::velocityX
	float ___velocityX_13;
	// System.Single MagicArsenal.MagicDragMouseOrbit::velocityY
	float ___velocityY_14;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___target_2)); }
	inline Transform_t362059596 * get_target_2() const { return ___target_2; }
	inline Transform_t362059596 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t362059596 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_xSpeed_4() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___xSpeed_4)); }
	inline float get_xSpeed_4() const { return ___xSpeed_4; }
	inline float* get_address_of_xSpeed_4() { return &___xSpeed_4; }
	inline void set_xSpeed_4(float value)
	{
		___xSpeed_4 = value;
	}

	inline static int32_t get_offset_of_ySpeed_5() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___ySpeed_5)); }
	inline float get_ySpeed_5() const { return ___ySpeed_5; }
	inline float* get_address_of_ySpeed_5() { return &___ySpeed_5; }
	inline void set_ySpeed_5(float value)
	{
		___ySpeed_5 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_6() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___yMinLimit_6)); }
	inline float get_yMinLimit_6() const { return ___yMinLimit_6; }
	inline float* get_address_of_yMinLimit_6() { return &___yMinLimit_6; }
	inline void set_yMinLimit_6(float value)
	{
		___yMinLimit_6 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_7() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___yMaxLimit_7)); }
	inline float get_yMaxLimit_7() const { return ___yMaxLimit_7; }
	inline float* get_address_of_yMaxLimit_7() { return &___yMaxLimit_7; }
	inline void set_yMaxLimit_7(float value)
	{
		___yMaxLimit_7 = value;
	}

	inline static int32_t get_offset_of_distanceMin_8() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___distanceMin_8)); }
	inline float get_distanceMin_8() const { return ___distanceMin_8; }
	inline float* get_address_of_distanceMin_8() { return &___distanceMin_8; }
	inline void set_distanceMin_8(float value)
	{
		___distanceMin_8 = value;
	}

	inline static int32_t get_offset_of_distanceMax_9() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___distanceMax_9)); }
	inline float get_distanceMax_9() const { return ___distanceMax_9; }
	inline float* get_address_of_distanceMax_9() { return &___distanceMax_9; }
	inline void set_distanceMax_9(float value)
	{
		___distanceMax_9 = value;
	}

	inline static int32_t get_offset_of_smoothTime_10() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___smoothTime_10)); }
	inline float get_smoothTime_10() const { return ___smoothTime_10; }
	inline float* get_address_of_smoothTime_10() { return &___smoothTime_10; }
	inline void set_smoothTime_10(float value)
	{
		___smoothTime_10 = value;
	}

	inline static int32_t get_offset_of_rotationYAxis_11() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___rotationYAxis_11)); }
	inline float get_rotationYAxis_11() const { return ___rotationYAxis_11; }
	inline float* get_address_of_rotationYAxis_11() { return &___rotationYAxis_11; }
	inline void set_rotationYAxis_11(float value)
	{
		___rotationYAxis_11 = value;
	}

	inline static int32_t get_offset_of_rotationXAxis_12() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___rotationXAxis_12)); }
	inline float get_rotationXAxis_12() const { return ___rotationXAxis_12; }
	inline float* get_address_of_rotationXAxis_12() { return &___rotationXAxis_12; }
	inline void set_rotationXAxis_12(float value)
	{
		___rotationXAxis_12 = value;
	}

	inline static int32_t get_offset_of_velocityX_13() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___velocityX_13)); }
	inline float get_velocityX_13() const { return ___velocityX_13; }
	inline float* get_address_of_velocityX_13() { return &___velocityX_13; }
	inline void set_velocityX_13(float value)
	{
		___velocityX_13 = value;
	}

	inline static int32_t get_offset_of_velocityY_14() { return static_cast<int32_t>(offsetof(MagicDragMouseOrbit_t2106366795, ___velocityY_14)); }
	inline float get_velocityY_14() const { return ___velocityY_14; }
	inline float* get_address_of_velocityY_14() { return &___velocityY_14; }
	inline void set_velocityY_14(float value)
	{
		___velocityY_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICDRAGMOUSEORBIT_T2106366795_H
#ifndef MAGICFIREPROJECTILE_T3063101194_H
#define MAGICFIREPROJECTILE_T3063101194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicFireProjectile
struct  MagicFireProjectile_t3063101194  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.RaycastHit MagicArsenal.MagicFireProjectile::hit
	RaycastHit_t1706347245  ___hit_2;
	// UnityEngine.GameObject[] MagicArsenal.MagicFireProjectile::projectiles
	GameObjectU5BU5D_t2988620542* ___projectiles_3;
	// UnityEngine.Transform MagicArsenal.MagicFireProjectile::spawnPosition
	Transform_t362059596 * ___spawnPosition_4;
	// System.Int32 MagicArsenal.MagicFireProjectile::currentProjectile
	int32_t ___currentProjectile_5;
	// System.Single MagicArsenal.MagicFireProjectile::speed
	float ___speed_6;
	// MagicArsenal.MagicButtonScript MagicArsenal.MagicFireProjectile::selectedProjectileButton
	MagicButtonScript_t1297805390 * ___selectedProjectileButton_7;

public:
	inline static int32_t get_offset_of_hit_2() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___hit_2)); }
	inline RaycastHit_t1706347245  get_hit_2() const { return ___hit_2; }
	inline RaycastHit_t1706347245 * get_address_of_hit_2() { return &___hit_2; }
	inline void set_hit_2(RaycastHit_t1706347245  value)
	{
		___hit_2 = value;
	}

	inline static int32_t get_offset_of_projectiles_3() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___projectiles_3)); }
	inline GameObjectU5BU5D_t2988620542* get_projectiles_3() const { return ___projectiles_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_projectiles_3() { return &___projectiles_3; }
	inline void set_projectiles_3(GameObjectU5BU5D_t2988620542* value)
	{
		___projectiles_3 = value;
		Il2CppCodeGenWriteBarrier((&___projectiles_3), value);
	}

	inline static int32_t get_offset_of_spawnPosition_4() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___spawnPosition_4)); }
	inline Transform_t362059596 * get_spawnPosition_4() const { return ___spawnPosition_4; }
	inline Transform_t362059596 ** get_address_of_spawnPosition_4() { return &___spawnPosition_4; }
	inline void set_spawnPosition_4(Transform_t362059596 * value)
	{
		___spawnPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPosition_4), value);
	}

	inline static int32_t get_offset_of_currentProjectile_5() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___currentProjectile_5)); }
	inline int32_t get_currentProjectile_5() const { return ___currentProjectile_5; }
	inline int32_t* get_address_of_currentProjectile_5() { return &___currentProjectile_5; }
	inline void set_currentProjectile_5(int32_t value)
	{
		___currentProjectile_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_selectedProjectileButton_7() { return static_cast<int32_t>(offsetof(MagicFireProjectile_t3063101194, ___selectedProjectileButton_7)); }
	inline MagicButtonScript_t1297805390 * get_selectedProjectileButton_7() const { return ___selectedProjectileButton_7; }
	inline MagicButtonScript_t1297805390 ** get_address_of_selectedProjectileButton_7() { return &___selectedProjectileButton_7; }
	inline void set_selectedProjectileButton_7(MagicButtonScript_t1297805390 * value)
	{
		___selectedProjectileButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectedProjectileButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICFIREPROJECTILE_T3063101194_H
#ifndef MAGICLOADSCENEONCLICK_T1944786205_H
#define MAGICLOADSCENEONCLICK_T1944786205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicLoadSceneOnClick
struct  MagicLoadSceneOnClick_t1944786205  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICLOADSCENEONCLICK_T1944786205_H
#ifndef MAGICLOOPSCRIPT_T2356792626_H
#define MAGICLOOPSCRIPT_T2356792626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicLoopScript
struct  MagicLoopScript_t2356792626  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject MagicArsenal.MagicLoopScript::chosenEffect
	GameObject_t2557347079 * ___chosenEffect_2;
	// System.Single MagicArsenal.MagicLoopScript::loopTimeLimit
	float ___loopTimeLimit_3;

public:
	inline static int32_t get_offset_of_chosenEffect_2() { return static_cast<int32_t>(offsetof(MagicLoopScript_t2356792626, ___chosenEffect_2)); }
	inline GameObject_t2557347079 * get_chosenEffect_2() const { return ___chosenEffect_2; }
	inline GameObject_t2557347079 ** get_address_of_chosenEffect_2() { return &___chosenEffect_2; }
	inline void set_chosenEffect_2(GameObject_t2557347079 * value)
	{
		___chosenEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___chosenEffect_2), value);
	}

	inline static int32_t get_offset_of_loopTimeLimit_3() { return static_cast<int32_t>(offsetof(MagicLoopScript_t2356792626, ___loopTimeLimit_3)); }
	inline float get_loopTimeLimit_3() const { return ___loopTimeLimit_3; }
	inline float* get_address_of_loopTimeLimit_3() { return &___loopTimeLimit_3; }
	inline void set_loopTimeLimit_3(float value)
	{
		___loopTimeLimit_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICLOOPSCRIPT_T2356792626_H
#ifndef HOWIFELTFACES_T4227992676_H
#define HOWIFELTFACES_T4227992676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HowIFeltFaces
struct  HowIFeltFaces_t4227992676  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Image HowIFeltFaces::happyFace
	Image_t2816987602 * ___happyFace_2;
	// UnityEngine.UI.Image HowIFeltFaces::neutralFace
	Image_t2816987602 * ___neutralFace_3;
	// UnityEngine.UI.Image HowIFeltFaces::sadFace
	Image_t2816987602 * ___sadFace_4;
	// UnityEngine.Sprite[] HowIFeltFaces::faceSprites
	SpriteU5BU5D_t27653605* ___faceSprites_5;
	// UnityEngine.UI.Image[] HowIFeltFaces::faces
	ImageU5BU5D_t892035751* ___faces_6;
	// HowIFeltFaces/FaceType HowIFeltFaces::activeFace
	int32_t ___activeFace_7;

public:
	inline static int32_t get_offset_of_happyFace_2() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___happyFace_2)); }
	inline Image_t2816987602 * get_happyFace_2() const { return ___happyFace_2; }
	inline Image_t2816987602 ** get_address_of_happyFace_2() { return &___happyFace_2; }
	inline void set_happyFace_2(Image_t2816987602 * value)
	{
		___happyFace_2 = value;
		Il2CppCodeGenWriteBarrier((&___happyFace_2), value);
	}

	inline static int32_t get_offset_of_neutralFace_3() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___neutralFace_3)); }
	inline Image_t2816987602 * get_neutralFace_3() const { return ___neutralFace_3; }
	inline Image_t2816987602 ** get_address_of_neutralFace_3() { return &___neutralFace_3; }
	inline void set_neutralFace_3(Image_t2816987602 * value)
	{
		___neutralFace_3 = value;
		Il2CppCodeGenWriteBarrier((&___neutralFace_3), value);
	}

	inline static int32_t get_offset_of_sadFace_4() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___sadFace_4)); }
	inline Image_t2816987602 * get_sadFace_4() const { return ___sadFace_4; }
	inline Image_t2816987602 ** get_address_of_sadFace_4() { return &___sadFace_4; }
	inline void set_sadFace_4(Image_t2816987602 * value)
	{
		___sadFace_4 = value;
		Il2CppCodeGenWriteBarrier((&___sadFace_4), value);
	}

	inline static int32_t get_offset_of_faceSprites_5() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___faceSprites_5)); }
	inline SpriteU5BU5D_t27653605* get_faceSprites_5() const { return ___faceSprites_5; }
	inline SpriteU5BU5D_t27653605** get_address_of_faceSprites_5() { return &___faceSprites_5; }
	inline void set_faceSprites_5(SpriteU5BU5D_t27653605* value)
	{
		___faceSprites_5 = value;
		Il2CppCodeGenWriteBarrier((&___faceSprites_5), value);
	}

	inline static int32_t get_offset_of_faces_6() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___faces_6)); }
	inline ImageU5BU5D_t892035751* get_faces_6() const { return ___faces_6; }
	inline ImageU5BU5D_t892035751** get_address_of_faces_6() { return &___faces_6; }
	inline void set_faces_6(ImageU5BU5D_t892035751* value)
	{
		___faces_6 = value;
		Il2CppCodeGenWriteBarrier((&___faces_6), value);
	}

	inline static int32_t get_offset_of_activeFace_7() { return static_cast<int32_t>(offsetof(HowIFeltFaces_t4227992676, ___activeFace_7)); }
	inline int32_t get_activeFace_7() const { return ___activeFace_7; }
	inline int32_t* get_address_of_activeFace_7() { return &___activeFace_7; }
	inline void set_activeFace_7(int32_t value)
	{
		___activeFace_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOWIFELTFACES_T4227992676_H
#ifndef HEATHAZEPLANE_T953234327_H
#define HEATHAZEPLANE_T953234327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeatHazePlane
struct  HeatHazePlane_t953234327  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEATHAZEPLANE_T953234327_H
#ifndef MAGICBEAMSCRIPT_T875858234_H
#define MAGICBEAMSCRIPT_T875858234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicBeamScript
struct  MagicBeamScript_t875858234  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] MagicBeamScript::beamLineRendererPrefab
	GameObjectU5BU5D_t2988620542* ___beamLineRendererPrefab_2;
	// UnityEngine.GameObject[] MagicBeamScript::beamStartPrefab
	GameObjectU5BU5D_t2988620542* ___beamStartPrefab_3;
	// UnityEngine.GameObject[] MagicBeamScript::beamEndPrefab
	GameObjectU5BU5D_t2988620542* ___beamEndPrefab_4;
	// System.Int32 MagicBeamScript::currentBeam
	int32_t ___currentBeam_5;
	// UnityEngine.GameObject MagicBeamScript::beamStart
	GameObject_t2557347079 * ___beamStart_6;
	// UnityEngine.GameObject MagicBeamScript::beamEnd
	GameObject_t2557347079 * ___beamEnd_7;
	// UnityEngine.GameObject MagicBeamScript::beam
	GameObject_t2557347079 * ___beam_8;
	// UnityEngine.LineRenderer MagicBeamScript::line
	LineRenderer_t2362162701 * ___line_9;
	// System.Single MagicBeamScript::beamEndOffset
	float ___beamEndOffset_10;
	// System.Single MagicBeamScript::textureScrollSpeed
	float ___textureScrollSpeed_11;
	// System.Single MagicBeamScript::textureLengthScale
	float ___textureLengthScale_12;
	// UnityEngine.UI.Slider MagicBeamScript::endOffSetSlider
	Slider_t1188261235 * ___endOffSetSlider_13;
	// UnityEngine.UI.Slider MagicBeamScript::scrollSpeedSlider
	Slider_t1188261235 * ___scrollSpeedSlider_14;
	// UnityEngine.UI.Text MagicBeamScript::textBeamName
	Text_t1790657652 * ___textBeamName_15;

public:
	inline static int32_t get_offset_of_beamLineRendererPrefab_2() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamLineRendererPrefab_2)); }
	inline GameObjectU5BU5D_t2988620542* get_beamLineRendererPrefab_2() const { return ___beamLineRendererPrefab_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_beamLineRendererPrefab_2() { return &___beamLineRendererPrefab_2; }
	inline void set_beamLineRendererPrefab_2(GameObjectU5BU5D_t2988620542* value)
	{
		___beamLineRendererPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___beamLineRendererPrefab_2), value);
	}

	inline static int32_t get_offset_of_beamStartPrefab_3() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamStartPrefab_3)); }
	inline GameObjectU5BU5D_t2988620542* get_beamStartPrefab_3() const { return ___beamStartPrefab_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_beamStartPrefab_3() { return &___beamStartPrefab_3; }
	inline void set_beamStartPrefab_3(GameObjectU5BU5D_t2988620542* value)
	{
		___beamStartPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___beamStartPrefab_3), value);
	}

	inline static int32_t get_offset_of_beamEndPrefab_4() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamEndPrefab_4)); }
	inline GameObjectU5BU5D_t2988620542* get_beamEndPrefab_4() const { return ___beamEndPrefab_4; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_beamEndPrefab_4() { return &___beamEndPrefab_4; }
	inline void set_beamEndPrefab_4(GameObjectU5BU5D_t2988620542* value)
	{
		___beamEndPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___beamEndPrefab_4), value);
	}

	inline static int32_t get_offset_of_currentBeam_5() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___currentBeam_5)); }
	inline int32_t get_currentBeam_5() const { return ___currentBeam_5; }
	inline int32_t* get_address_of_currentBeam_5() { return &___currentBeam_5; }
	inline void set_currentBeam_5(int32_t value)
	{
		___currentBeam_5 = value;
	}

	inline static int32_t get_offset_of_beamStart_6() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamStart_6)); }
	inline GameObject_t2557347079 * get_beamStart_6() const { return ___beamStart_6; }
	inline GameObject_t2557347079 ** get_address_of_beamStart_6() { return &___beamStart_6; }
	inline void set_beamStart_6(GameObject_t2557347079 * value)
	{
		___beamStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___beamStart_6), value);
	}

	inline static int32_t get_offset_of_beamEnd_7() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamEnd_7)); }
	inline GameObject_t2557347079 * get_beamEnd_7() const { return ___beamEnd_7; }
	inline GameObject_t2557347079 ** get_address_of_beamEnd_7() { return &___beamEnd_7; }
	inline void set_beamEnd_7(GameObject_t2557347079 * value)
	{
		___beamEnd_7 = value;
		Il2CppCodeGenWriteBarrier((&___beamEnd_7), value);
	}

	inline static int32_t get_offset_of_beam_8() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beam_8)); }
	inline GameObject_t2557347079 * get_beam_8() const { return ___beam_8; }
	inline GameObject_t2557347079 ** get_address_of_beam_8() { return &___beam_8; }
	inline void set_beam_8(GameObject_t2557347079 * value)
	{
		___beam_8 = value;
		Il2CppCodeGenWriteBarrier((&___beam_8), value);
	}

	inline static int32_t get_offset_of_line_9() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___line_9)); }
	inline LineRenderer_t2362162701 * get_line_9() const { return ___line_9; }
	inline LineRenderer_t2362162701 ** get_address_of_line_9() { return &___line_9; }
	inline void set_line_9(LineRenderer_t2362162701 * value)
	{
		___line_9 = value;
		Il2CppCodeGenWriteBarrier((&___line_9), value);
	}

	inline static int32_t get_offset_of_beamEndOffset_10() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___beamEndOffset_10)); }
	inline float get_beamEndOffset_10() const { return ___beamEndOffset_10; }
	inline float* get_address_of_beamEndOffset_10() { return &___beamEndOffset_10; }
	inline void set_beamEndOffset_10(float value)
	{
		___beamEndOffset_10 = value;
	}

	inline static int32_t get_offset_of_textureScrollSpeed_11() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___textureScrollSpeed_11)); }
	inline float get_textureScrollSpeed_11() const { return ___textureScrollSpeed_11; }
	inline float* get_address_of_textureScrollSpeed_11() { return &___textureScrollSpeed_11; }
	inline void set_textureScrollSpeed_11(float value)
	{
		___textureScrollSpeed_11 = value;
	}

	inline static int32_t get_offset_of_textureLengthScale_12() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___textureLengthScale_12)); }
	inline float get_textureLengthScale_12() const { return ___textureLengthScale_12; }
	inline float* get_address_of_textureLengthScale_12() { return &___textureLengthScale_12; }
	inline void set_textureLengthScale_12(float value)
	{
		___textureLengthScale_12 = value;
	}

	inline static int32_t get_offset_of_endOffSetSlider_13() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___endOffSetSlider_13)); }
	inline Slider_t1188261235 * get_endOffSetSlider_13() const { return ___endOffSetSlider_13; }
	inline Slider_t1188261235 ** get_address_of_endOffSetSlider_13() { return &___endOffSetSlider_13; }
	inline void set_endOffSetSlider_13(Slider_t1188261235 * value)
	{
		___endOffSetSlider_13 = value;
		Il2CppCodeGenWriteBarrier((&___endOffSetSlider_13), value);
	}

	inline static int32_t get_offset_of_scrollSpeedSlider_14() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___scrollSpeedSlider_14)); }
	inline Slider_t1188261235 * get_scrollSpeedSlider_14() const { return ___scrollSpeedSlider_14; }
	inline Slider_t1188261235 ** get_address_of_scrollSpeedSlider_14() { return &___scrollSpeedSlider_14; }
	inline void set_scrollSpeedSlider_14(Slider_t1188261235 * value)
	{
		___scrollSpeedSlider_14 = value;
		Il2CppCodeGenWriteBarrier((&___scrollSpeedSlider_14), value);
	}

	inline static int32_t get_offset_of_textBeamName_15() { return static_cast<int32_t>(offsetof(MagicBeamScript_t875858234, ___textBeamName_15)); }
	inline Text_t1790657652 * get_textBeamName_15() const { return ___textBeamName_15; }
	inline Text_t1790657652 ** get_address_of_textBeamName_15() { return &___textBeamName_15; }
	inline void set_textBeamName_15(Text_t1790657652 * value)
	{
		___textBeamName_15 = value;
		Il2CppCodeGenWriteBarrier((&___textBeamName_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICBEAMSCRIPT_T875858234_H
#ifndef MAGICLIGHTFADE_T2834402439_H
#define MAGICLIGHTFADE_T2834402439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicLightFade
struct  MagicLightFade_t2834402439  : public MonoBehaviour_t1618594486
{
public:
	// System.Single MagicArsenal.MagicLightFade::life
	float ___life_2;
	// System.Boolean MagicArsenal.MagicLightFade::killAfterLife
	bool ___killAfterLife_3;
	// UnityEngine.Light MagicArsenal.MagicLightFade::li
	Light_t1775228881 * ___li_4;
	// System.Single MagicArsenal.MagicLightFade::initIntensity
	float ___initIntensity_5;

public:
	inline static int32_t get_offset_of_life_2() { return static_cast<int32_t>(offsetof(MagicLightFade_t2834402439, ___life_2)); }
	inline float get_life_2() const { return ___life_2; }
	inline float* get_address_of_life_2() { return &___life_2; }
	inline void set_life_2(float value)
	{
		___life_2 = value;
	}

	inline static int32_t get_offset_of_killAfterLife_3() { return static_cast<int32_t>(offsetof(MagicLightFade_t2834402439, ___killAfterLife_3)); }
	inline bool get_killAfterLife_3() const { return ___killAfterLife_3; }
	inline bool* get_address_of_killAfterLife_3() { return &___killAfterLife_3; }
	inline void set_killAfterLife_3(bool value)
	{
		___killAfterLife_3 = value;
	}

	inline static int32_t get_offset_of_li_4() { return static_cast<int32_t>(offsetof(MagicLightFade_t2834402439, ___li_4)); }
	inline Light_t1775228881 * get_li_4() const { return ___li_4; }
	inline Light_t1775228881 ** get_address_of_li_4() { return &___li_4; }
	inline void set_li_4(Light_t1775228881 * value)
	{
		___li_4 = value;
		Il2CppCodeGenWriteBarrier((&___li_4), value);
	}

	inline static int32_t get_offset_of_initIntensity_5() { return static_cast<int32_t>(offsetof(MagicLightFade_t2834402439, ___initIntensity_5)); }
	inline float get_initIntensity_5() const { return ___initIntensity_5; }
	inline float* get_address_of_initIntensity_5() { return &___initIntensity_5; }
	inline void set_initIntensity_5(float value)
	{
		___initIntensity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICLIGHTFADE_T2834402439_H
#ifndef MAGICROTATION_T2833249381_H
#define MAGICROTATION_T2833249381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicArsenal.MagicRotation
struct  MagicRotation_t2833249381  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 MagicArsenal.MagicRotation::rotateVector
	Vector3_t1986933152  ___rotateVector_2;
	// MagicArsenal.MagicRotation/spaceEnum MagicArsenal.MagicRotation::rotateSpace
	int32_t ___rotateSpace_3;

public:
	inline static int32_t get_offset_of_rotateVector_2() { return static_cast<int32_t>(offsetof(MagicRotation_t2833249381, ___rotateVector_2)); }
	inline Vector3_t1986933152  get_rotateVector_2() const { return ___rotateVector_2; }
	inline Vector3_t1986933152 * get_address_of_rotateVector_2() { return &___rotateVector_2; }
	inline void set_rotateVector_2(Vector3_t1986933152  value)
	{
		___rotateVector_2 = value;
	}

	inline static int32_t get_offset_of_rotateSpace_3() { return static_cast<int32_t>(offsetof(MagicRotation_t2833249381, ___rotateSpace_3)); }
	inline int32_t get_rotateSpace_3() const { return ___rotateSpace_3; }
	inline int32_t* get_address_of_rotateSpace_3() { return &___rotateSpace_3; }
	inline void set_rotateSpace_3(int32_t value)
	{
		___rotateSpace_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICROTATION_T2833249381_H
#ifndef HAPPYLITTLEFOGMACHINE_T3641353099_H
#define HAPPYLITTLEFOGMACHINE_T3641353099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HappyLittleFogMachine
struct  HappyLittleFogMachine_t3641353099  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject HappyLittleFogMachine::fogPrefab
	GameObject_t2557347079 * ___fogPrefab_2;
	// EffectAttachPoint[] HappyLittleFogMachine::characterAttachPoint
	EffectAttachPointU5BU5D_t1753991996* ___characterAttachPoint_3;

public:
	inline static int32_t get_offset_of_fogPrefab_2() { return static_cast<int32_t>(offsetof(HappyLittleFogMachine_t3641353099, ___fogPrefab_2)); }
	inline GameObject_t2557347079 * get_fogPrefab_2() const { return ___fogPrefab_2; }
	inline GameObject_t2557347079 ** get_address_of_fogPrefab_2() { return &___fogPrefab_2; }
	inline void set_fogPrefab_2(GameObject_t2557347079 * value)
	{
		___fogPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___fogPrefab_2), value);
	}

	inline static int32_t get_offset_of_characterAttachPoint_3() { return static_cast<int32_t>(offsetof(HappyLittleFogMachine_t3641353099, ___characterAttachPoint_3)); }
	inline EffectAttachPointU5BU5D_t1753991996* get_characterAttachPoint_3() const { return ___characterAttachPoint_3; }
	inline EffectAttachPointU5BU5D_t1753991996** get_address_of_characterAttachPoint_3() { return &___characterAttachPoint_3; }
	inline void set_characterAttachPoint_3(EffectAttachPointU5BU5D_t1753991996* value)
	{
		___characterAttachPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterAttachPoint_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAPPYLITTLEFOGMACHINE_T3641353099_H
#ifndef GEMEFFECTHELPER_T1002598843_H
#define GEMEFFECTHELPER_T1002598843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GemEffectHelper
struct  GemEffectHelper_t1002598843  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.ParticleSystem[] GemEffectHelper::systems
	ParticleSystemU5BU5D_t1417780692* ___systems_3;

public:
	inline static int32_t get_offset_of_systems_3() { return static_cast<int32_t>(offsetof(GemEffectHelper_t1002598843, ___systems_3)); }
	inline ParticleSystemU5BU5D_t1417780692* get_systems_3() const { return ___systems_3; }
	inline ParticleSystemU5BU5D_t1417780692** get_address_of_systems_3() { return &___systems_3; }
	inline void set_systems_3(ParticleSystemU5BU5D_t1417780692* value)
	{
		___systems_3 = value;
		Il2CppCodeGenWriteBarrier((&___systems_3), value);
	}
};

struct GemEffectHelper_t1002598843_StaticFields
{
public:
	// GemEffectHelper GemEffectHelper::instance
	GemEffectHelper_t1002598843 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GemEffectHelper_t1002598843_StaticFields, ___instance_2)); }
	inline GemEffectHelper_t1002598843 * get_instance_2() const { return ___instance_2; }
	inline GemEffectHelper_t1002598843 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GemEffectHelper_t1002598843 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEMEFFECTHELPER_T1002598843_H
#ifndef FRAMECOUNT_T1086700601_H
#define FRAMECOUNT_T1086700601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrameCount
struct  FrameCount_t1086700601  : public MonoBehaviour_t1618594486
{
public:
	// System.Single FrameCount::updateInterval
	float ___updateInterval_3;
	// System.Single FrameCount::accum
	float ___accum_4;
	// System.Int32 FrameCount::frames
	int32_t ___frames_5;
	// System.Single FrameCount::timeleft
	float ___timeleft_6;

public:
	inline static int32_t get_offset_of_updateInterval_3() { return static_cast<int32_t>(offsetof(FrameCount_t1086700601, ___updateInterval_3)); }
	inline float get_updateInterval_3() const { return ___updateInterval_3; }
	inline float* get_address_of_updateInterval_3() { return &___updateInterval_3; }
	inline void set_updateInterval_3(float value)
	{
		___updateInterval_3 = value;
	}

	inline static int32_t get_offset_of_accum_4() { return static_cast<int32_t>(offsetof(FrameCount_t1086700601, ___accum_4)); }
	inline float get_accum_4() const { return ___accum_4; }
	inline float* get_address_of_accum_4() { return &___accum_4; }
	inline void set_accum_4(float value)
	{
		___accum_4 = value;
	}

	inline static int32_t get_offset_of_frames_5() { return static_cast<int32_t>(offsetof(FrameCount_t1086700601, ___frames_5)); }
	inline int32_t get_frames_5() const { return ___frames_5; }
	inline int32_t* get_address_of_frames_5() { return &___frames_5; }
	inline void set_frames_5(int32_t value)
	{
		___frames_5 = value;
	}

	inline static int32_t get_offset_of_timeleft_6() { return static_cast<int32_t>(offsetof(FrameCount_t1086700601, ___timeleft_6)); }
	inline float get_timeleft_6() const { return ___timeleft_6; }
	inline float* get_address_of_timeleft_6() { return &___timeleft_6; }
	inline void set_timeleft_6(float value)
	{
		___timeleft_6 = value;
	}
};

struct FrameCount_t1086700601_StaticFields
{
public:
	// FrameCount FrameCount::instanceRef
	FrameCount_t1086700601 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(FrameCount_t1086700601_StaticFields, ___instanceRef_2)); }
	inline FrameCount_t1086700601 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline FrameCount_t1086700601 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(FrameCount_t1086700601 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECOUNT_T1086700601_H
#ifndef FOLIAGE_T1566384678_H
#define FOLIAGE_T1566384678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Foliage
struct  Foliage_t1566384678  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] Foliage::foliageLevels
	GameObjectU5BU5D_t2988620542* ___foliageLevels_2;

public:
	inline static int32_t get_offset_of_foliageLevels_2() { return static_cast<int32_t>(offsetof(Foliage_t1566384678, ___foliageLevels_2)); }
	inline GameObjectU5BU5D_t2988620542* get_foliageLevels_2() const { return ___foliageLevels_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_foliageLevels_2() { return &___foliageLevels_2; }
	inline void set_foliageLevels_2(GameObjectU5BU5D_t2988620542* value)
	{
		___foliageLevels_2 = value;
		Il2CppCodeGenWriteBarrier((&___foliageLevels_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLIAGE_T1566384678_H
#ifndef ENTERTEXTDIALOG_T1585287947_H
#define ENTERTEXTDIALOG_T1585287947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnterTextDialog
struct  EnterTextDialog_t1585287947  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 EnterTextDialog::numFields
	int32_t ___numFields_2;
	// UnityEngine.GameObject EnterTextDialog::fieldPrefab
	GameObject_t2557347079 * ___fieldPrefab_3;
	// System.Single EnterTextDialog::fieldGap
	float ___fieldGap_4;
	// System.Single EnterTextDialog::offset
	float ___offset_5;
	// UnityEngine.UI.Text EnterTextDialog::title
	Text_t1790657652 * ___title_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EnterTextDialog::inputFields
	List_1_t1676974086 * ___inputFields_7;

public:
	inline static int32_t get_offset_of_numFields_2() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___numFields_2)); }
	inline int32_t get_numFields_2() const { return ___numFields_2; }
	inline int32_t* get_address_of_numFields_2() { return &___numFields_2; }
	inline void set_numFields_2(int32_t value)
	{
		___numFields_2 = value;
	}

	inline static int32_t get_offset_of_fieldPrefab_3() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___fieldPrefab_3)); }
	inline GameObject_t2557347079 * get_fieldPrefab_3() const { return ___fieldPrefab_3; }
	inline GameObject_t2557347079 ** get_address_of_fieldPrefab_3() { return &___fieldPrefab_3; }
	inline void set_fieldPrefab_3(GameObject_t2557347079 * value)
	{
		___fieldPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___fieldPrefab_3), value);
	}

	inline static int32_t get_offset_of_fieldGap_4() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___fieldGap_4)); }
	inline float get_fieldGap_4() const { return ___fieldGap_4; }
	inline float* get_address_of_fieldGap_4() { return &___fieldGap_4; }
	inline void set_fieldGap_4(float value)
	{
		___fieldGap_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___offset_5)); }
	inline float get_offset_5() const { return ___offset_5; }
	inline float* get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(float value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___title_6)); }
	inline Text_t1790657652 * get_title_6() const { return ___title_6; }
	inline Text_t1790657652 ** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(Text_t1790657652 * value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((&___title_6), value);
	}

	inline static int32_t get_offset_of_inputFields_7() { return static_cast<int32_t>(offsetof(EnterTextDialog_t1585287947, ___inputFields_7)); }
	inline List_1_t1676974086 * get_inputFields_7() const { return ___inputFields_7; }
	inline List_1_t1676974086 ** get_address_of_inputFields_7() { return &___inputFields_7; }
	inline void set_inputFields_7(List_1_t1676974086 * value)
	{
		___inputFields_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputFields_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTERTEXTDIALOG_T1585287947_H
#ifndef PARTICLEMENU_T3804455348_H
#define PARTICLEMENU_T3804455348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleMenu
struct  ParticleMenu_t3804455348  : public MonoBehaviour_t1618594486
{
public:
	// ParticleExamples[] ParticleMenu::particleSystems
	ParticleExamplesU5BU5D_t3470023393* ___particleSystems_2;
	// UnityEngine.GameObject ParticleMenu::gunGameObject
	GameObject_t2557347079 * ___gunGameObject_3;
	// System.Int32 ParticleMenu::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject ParticleMenu::currentGO
	GameObject_t2557347079 * ___currentGO_5;
	// UnityEngine.Transform ParticleMenu::spawnLocation
	Transform_t362059596 * ___spawnLocation_6;
	// UnityEngine.UI.Text ParticleMenu::title
	Text_t1790657652 * ___title_7;
	// UnityEngine.UI.Text ParticleMenu::description
	Text_t1790657652 * ___description_8;
	// UnityEngine.UI.Text ParticleMenu::navigationDetails
	Text_t1790657652 * ___navigationDetails_9;

public:
	inline static int32_t get_offset_of_particleSystems_2() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___particleSystems_2)); }
	inline ParticleExamplesU5BU5D_t3470023393* get_particleSystems_2() const { return ___particleSystems_2; }
	inline ParticleExamplesU5BU5D_t3470023393** get_address_of_particleSystems_2() { return &___particleSystems_2; }
	inline void set_particleSystems_2(ParticleExamplesU5BU5D_t3470023393* value)
	{
		___particleSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystems_2), value);
	}

	inline static int32_t get_offset_of_gunGameObject_3() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___gunGameObject_3)); }
	inline GameObject_t2557347079 * get_gunGameObject_3() const { return ___gunGameObject_3; }
	inline GameObject_t2557347079 ** get_address_of_gunGameObject_3() { return &___gunGameObject_3; }
	inline void set_gunGameObject_3(GameObject_t2557347079 * value)
	{
		___gunGameObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___gunGameObject_3), value);
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_currentGO_5() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___currentGO_5)); }
	inline GameObject_t2557347079 * get_currentGO_5() const { return ___currentGO_5; }
	inline GameObject_t2557347079 ** get_address_of_currentGO_5() { return &___currentGO_5; }
	inline void set_currentGO_5(GameObject_t2557347079 * value)
	{
		___currentGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentGO_5), value);
	}

	inline static int32_t get_offset_of_spawnLocation_6() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___spawnLocation_6)); }
	inline Transform_t362059596 * get_spawnLocation_6() const { return ___spawnLocation_6; }
	inline Transform_t362059596 ** get_address_of_spawnLocation_6() { return &___spawnLocation_6; }
	inline void set_spawnLocation_6(Transform_t362059596 * value)
	{
		___spawnLocation_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLocation_6), value);
	}

	inline static int32_t get_offset_of_title_7() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___title_7)); }
	inline Text_t1790657652 * get_title_7() const { return ___title_7; }
	inline Text_t1790657652 ** get_address_of_title_7() { return &___title_7; }
	inline void set_title_7(Text_t1790657652 * value)
	{
		___title_7 = value;
		Il2CppCodeGenWriteBarrier((&___title_7), value);
	}

	inline static int32_t get_offset_of_description_8() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___description_8)); }
	inline Text_t1790657652 * get_description_8() const { return ___description_8; }
	inline Text_t1790657652 ** get_address_of_description_8() { return &___description_8; }
	inline void set_description_8(Text_t1790657652 * value)
	{
		___description_8 = value;
		Il2CppCodeGenWriteBarrier((&___description_8), value);
	}

	inline static int32_t get_offset_of_navigationDetails_9() { return static_cast<int32_t>(offsetof(ParticleMenu_t3804455348, ___navigationDetails_9)); }
	inline Text_t1790657652 * get_navigationDetails_9() const { return ___navigationDetails_9; }
	inline Text_t1790657652 ** get_address_of_navigationDetails_9() { return &___navigationDetails_9; }
	inline void set_navigationDetails_9(Text_t1790657652 * value)
	{
		___navigationDetails_9 = value;
		Il2CppCodeGenWriteBarrier((&___navigationDetails_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEMENU_T3804455348_H
#ifndef PARTICLECOLLISION_T144936607_H
#define PARTICLECOLLISION_T144936607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleCollision
struct  ParticleCollision_t144936607  : public MonoBehaviour_t1618594486
{
public:
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> ParticleCollision::m_CollisionEvents
	List_1_t1867887001 * ___m_CollisionEvents_2;
	// UnityEngine.ParticleSystem ParticleCollision::m_ParticleSystem
	ParticleSystem_t2248283753 * ___m_ParticleSystem_3;

public:
	inline static int32_t get_offset_of_m_CollisionEvents_2() { return static_cast<int32_t>(offsetof(ParticleCollision_t144936607, ___m_CollisionEvents_2)); }
	inline List_1_t1867887001 * get_m_CollisionEvents_2() const { return ___m_CollisionEvents_2; }
	inline List_1_t1867887001 ** get_address_of_m_CollisionEvents_2() { return &___m_CollisionEvents_2; }
	inline void set_m_CollisionEvents_2(List_1_t1867887001 * value)
	{
		___m_CollisionEvents_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionEvents_2), value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_3() { return static_cast<int32_t>(offsetof(ParticleCollision_t144936607, ___m_ParticleSystem_3)); }
	inline ParticleSystem_t2248283753 * get_m_ParticleSystem_3() const { return ___m_ParticleSystem_3; }
	inline ParticleSystem_t2248283753 ** get_address_of_m_ParticleSystem_3() { return &___m_ParticleSystem_3; }
	inline void set_m_ParticleSystem_3(ParticleSystem_t2248283753 * value)
	{
		___m_ParticleSystem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISION_T144936607_H
#ifndef GUNSHOOT_T1510360140_H
#define GUNSHOOT_T1510360140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GunShoot
struct  GunShoot_t1510360140  : public MonoBehaviour_t1618594486
{
public:
	// System.Single GunShoot::fireRate
	float ___fireRate_2;
	// System.Single GunShoot::weaponRange
	float ___weaponRange_3;
	// UnityEngine.Transform GunShoot::gunEnd
	Transform_t362059596 * ___gunEnd_4;
	// UnityEngine.ParticleSystem GunShoot::muzzleFlash
	ParticleSystem_t2248283753 * ___muzzleFlash_5;
	// UnityEngine.ParticleSystem GunShoot::cartridgeEjection
	ParticleSystem_t2248283753 * ___cartridgeEjection_6;
	// UnityEngine.GameObject GunShoot::metalHitEffect
	GameObject_t2557347079 * ___metalHitEffect_7;
	// UnityEngine.GameObject GunShoot::sandHitEffect
	GameObject_t2557347079 * ___sandHitEffect_8;
	// UnityEngine.GameObject GunShoot::stoneHitEffect
	GameObject_t2557347079 * ___stoneHitEffect_9;
	// UnityEngine.GameObject GunShoot::waterLeakEffect
	GameObject_t2557347079 * ___waterLeakEffect_10;
	// UnityEngine.GameObject GunShoot::waterLeakExtinguishEffect
	GameObject_t2557347079 * ___waterLeakExtinguishEffect_11;
	// UnityEngine.GameObject[] GunShoot::fleshHitEffects
	GameObjectU5BU5D_t2988620542* ___fleshHitEffects_12;
	// UnityEngine.GameObject GunShoot::woodHitEffect
	GameObject_t2557347079 * ___woodHitEffect_13;
	// System.Single GunShoot::nextFire
	float ___nextFire_14;
	// UnityEngine.Animator GunShoot::anim
	Animator_t2768715325 * ___anim_15;
	// GunAim GunShoot::gunAim
	GunAim_t211531317 * ___gunAim_16;

public:
	inline static int32_t get_offset_of_fireRate_2() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___fireRate_2)); }
	inline float get_fireRate_2() const { return ___fireRate_2; }
	inline float* get_address_of_fireRate_2() { return &___fireRate_2; }
	inline void set_fireRate_2(float value)
	{
		___fireRate_2 = value;
	}

	inline static int32_t get_offset_of_weaponRange_3() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___weaponRange_3)); }
	inline float get_weaponRange_3() const { return ___weaponRange_3; }
	inline float* get_address_of_weaponRange_3() { return &___weaponRange_3; }
	inline void set_weaponRange_3(float value)
	{
		___weaponRange_3 = value;
	}

	inline static int32_t get_offset_of_gunEnd_4() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___gunEnd_4)); }
	inline Transform_t362059596 * get_gunEnd_4() const { return ___gunEnd_4; }
	inline Transform_t362059596 ** get_address_of_gunEnd_4() { return &___gunEnd_4; }
	inline void set_gunEnd_4(Transform_t362059596 * value)
	{
		___gunEnd_4 = value;
		Il2CppCodeGenWriteBarrier((&___gunEnd_4), value);
	}

	inline static int32_t get_offset_of_muzzleFlash_5() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___muzzleFlash_5)); }
	inline ParticleSystem_t2248283753 * get_muzzleFlash_5() const { return ___muzzleFlash_5; }
	inline ParticleSystem_t2248283753 ** get_address_of_muzzleFlash_5() { return &___muzzleFlash_5; }
	inline void set_muzzleFlash_5(ParticleSystem_t2248283753 * value)
	{
		___muzzleFlash_5 = value;
		Il2CppCodeGenWriteBarrier((&___muzzleFlash_5), value);
	}

	inline static int32_t get_offset_of_cartridgeEjection_6() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___cartridgeEjection_6)); }
	inline ParticleSystem_t2248283753 * get_cartridgeEjection_6() const { return ___cartridgeEjection_6; }
	inline ParticleSystem_t2248283753 ** get_address_of_cartridgeEjection_6() { return &___cartridgeEjection_6; }
	inline void set_cartridgeEjection_6(ParticleSystem_t2248283753 * value)
	{
		___cartridgeEjection_6 = value;
		Il2CppCodeGenWriteBarrier((&___cartridgeEjection_6), value);
	}

	inline static int32_t get_offset_of_metalHitEffect_7() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___metalHitEffect_7)); }
	inline GameObject_t2557347079 * get_metalHitEffect_7() const { return ___metalHitEffect_7; }
	inline GameObject_t2557347079 ** get_address_of_metalHitEffect_7() { return &___metalHitEffect_7; }
	inline void set_metalHitEffect_7(GameObject_t2557347079 * value)
	{
		___metalHitEffect_7 = value;
		Il2CppCodeGenWriteBarrier((&___metalHitEffect_7), value);
	}

	inline static int32_t get_offset_of_sandHitEffect_8() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___sandHitEffect_8)); }
	inline GameObject_t2557347079 * get_sandHitEffect_8() const { return ___sandHitEffect_8; }
	inline GameObject_t2557347079 ** get_address_of_sandHitEffect_8() { return &___sandHitEffect_8; }
	inline void set_sandHitEffect_8(GameObject_t2557347079 * value)
	{
		___sandHitEffect_8 = value;
		Il2CppCodeGenWriteBarrier((&___sandHitEffect_8), value);
	}

	inline static int32_t get_offset_of_stoneHitEffect_9() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___stoneHitEffect_9)); }
	inline GameObject_t2557347079 * get_stoneHitEffect_9() const { return ___stoneHitEffect_9; }
	inline GameObject_t2557347079 ** get_address_of_stoneHitEffect_9() { return &___stoneHitEffect_9; }
	inline void set_stoneHitEffect_9(GameObject_t2557347079 * value)
	{
		___stoneHitEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___stoneHitEffect_9), value);
	}

	inline static int32_t get_offset_of_waterLeakEffect_10() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___waterLeakEffect_10)); }
	inline GameObject_t2557347079 * get_waterLeakEffect_10() const { return ___waterLeakEffect_10; }
	inline GameObject_t2557347079 ** get_address_of_waterLeakEffect_10() { return &___waterLeakEffect_10; }
	inline void set_waterLeakEffect_10(GameObject_t2557347079 * value)
	{
		___waterLeakEffect_10 = value;
		Il2CppCodeGenWriteBarrier((&___waterLeakEffect_10), value);
	}

	inline static int32_t get_offset_of_waterLeakExtinguishEffect_11() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___waterLeakExtinguishEffect_11)); }
	inline GameObject_t2557347079 * get_waterLeakExtinguishEffect_11() const { return ___waterLeakExtinguishEffect_11; }
	inline GameObject_t2557347079 ** get_address_of_waterLeakExtinguishEffect_11() { return &___waterLeakExtinguishEffect_11; }
	inline void set_waterLeakExtinguishEffect_11(GameObject_t2557347079 * value)
	{
		___waterLeakExtinguishEffect_11 = value;
		Il2CppCodeGenWriteBarrier((&___waterLeakExtinguishEffect_11), value);
	}

	inline static int32_t get_offset_of_fleshHitEffects_12() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___fleshHitEffects_12)); }
	inline GameObjectU5BU5D_t2988620542* get_fleshHitEffects_12() const { return ___fleshHitEffects_12; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_fleshHitEffects_12() { return &___fleshHitEffects_12; }
	inline void set_fleshHitEffects_12(GameObjectU5BU5D_t2988620542* value)
	{
		___fleshHitEffects_12 = value;
		Il2CppCodeGenWriteBarrier((&___fleshHitEffects_12), value);
	}

	inline static int32_t get_offset_of_woodHitEffect_13() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___woodHitEffect_13)); }
	inline GameObject_t2557347079 * get_woodHitEffect_13() const { return ___woodHitEffect_13; }
	inline GameObject_t2557347079 ** get_address_of_woodHitEffect_13() { return &___woodHitEffect_13; }
	inline void set_woodHitEffect_13(GameObject_t2557347079 * value)
	{
		___woodHitEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___woodHitEffect_13), value);
	}

	inline static int32_t get_offset_of_nextFire_14() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___nextFire_14)); }
	inline float get_nextFire_14() const { return ___nextFire_14; }
	inline float* get_address_of_nextFire_14() { return &___nextFire_14; }
	inline void set_nextFire_14(float value)
	{
		___nextFire_14 = value;
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___anim_15)); }
	inline Animator_t2768715325 * get_anim_15() const { return ___anim_15; }
	inline Animator_t2768715325 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Animator_t2768715325 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_gunAim_16() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140, ___gunAim_16)); }
	inline GunAim_t211531317 * get_gunAim_16() const { return ___gunAim_16; }
	inline GunAim_t211531317 ** get_address_of_gunAim_16() { return &___gunAim_16; }
	inline void set_gunAim_16(GunAim_t211531317 * value)
	{
		___gunAim_16 = value;
		Il2CppCodeGenWriteBarrier((&___gunAim_16), value);
	}
};

struct GunShoot_t1510360140_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GunShoot::<>f__switch$map0
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_17() { return static_cast<int32_t>(offsetof(GunShoot_t1510360140_StaticFields, ___U3CU3Ef__switchU24map0_17)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map0_17() const { return ___U3CU3Ef__switchU24map0_17; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map0_17() { return &___U3CU3Ef__switchU24map0_17; }
	inline void set_U3CU3Ef__switchU24map0_17(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNSHOOT_T1510360140_H
#ifndef MAGICPROJECTILESCRIPT_T1654175834_H
#define MAGICPROJECTILESCRIPT_T1654175834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicProjectileScript
struct  MagicProjectileScript_t1654175834  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject MagicProjectileScript::impactParticle
	GameObject_t2557347079 * ___impactParticle_2;
	// UnityEngine.GameObject MagicProjectileScript::projectileParticle
	GameObject_t2557347079 * ___projectileParticle_3;
	// UnityEngine.GameObject MagicProjectileScript::muzzleParticle
	GameObject_t2557347079 * ___muzzleParticle_4;
	// UnityEngine.GameObject[] MagicProjectileScript::trailParticles
	GameObjectU5BU5D_t2988620542* ___trailParticles_5;
	// UnityEngine.Vector3 MagicProjectileScript::impactNormal
	Vector3_t1986933152  ___impactNormal_6;
	// System.Boolean MagicProjectileScript::hasCollided
	bool ___hasCollided_7;

public:
	inline static int32_t get_offset_of_impactParticle_2() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___impactParticle_2)); }
	inline GameObject_t2557347079 * get_impactParticle_2() const { return ___impactParticle_2; }
	inline GameObject_t2557347079 ** get_address_of_impactParticle_2() { return &___impactParticle_2; }
	inline void set_impactParticle_2(GameObject_t2557347079 * value)
	{
		___impactParticle_2 = value;
		Il2CppCodeGenWriteBarrier((&___impactParticle_2), value);
	}

	inline static int32_t get_offset_of_projectileParticle_3() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___projectileParticle_3)); }
	inline GameObject_t2557347079 * get_projectileParticle_3() const { return ___projectileParticle_3; }
	inline GameObject_t2557347079 ** get_address_of_projectileParticle_3() { return &___projectileParticle_3; }
	inline void set_projectileParticle_3(GameObject_t2557347079 * value)
	{
		___projectileParticle_3 = value;
		Il2CppCodeGenWriteBarrier((&___projectileParticle_3), value);
	}

	inline static int32_t get_offset_of_muzzleParticle_4() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___muzzleParticle_4)); }
	inline GameObject_t2557347079 * get_muzzleParticle_4() const { return ___muzzleParticle_4; }
	inline GameObject_t2557347079 ** get_address_of_muzzleParticle_4() { return &___muzzleParticle_4; }
	inline void set_muzzleParticle_4(GameObject_t2557347079 * value)
	{
		___muzzleParticle_4 = value;
		Il2CppCodeGenWriteBarrier((&___muzzleParticle_4), value);
	}

	inline static int32_t get_offset_of_trailParticles_5() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___trailParticles_5)); }
	inline GameObjectU5BU5D_t2988620542* get_trailParticles_5() const { return ___trailParticles_5; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_trailParticles_5() { return &___trailParticles_5; }
	inline void set_trailParticles_5(GameObjectU5BU5D_t2988620542* value)
	{
		___trailParticles_5 = value;
		Il2CppCodeGenWriteBarrier((&___trailParticles_5), value);
	}

	inline static int32_t get_offset_of_impactNormal_6() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___impactNormal_6)); }
	inline Vector3_t1986933152  get_impactNormal_6() const { return ___impactNormal_6; }
	inline Vector3_t1986933152 * get_address_of_impactNormal_6() { return &___impactNormal_6; }
	inline void set_impactNormal_6(Vector3_t1986933152  value)
	{
		___impactNormal_6 = value;
	}

	inline static int32_t get_offset_of_hasCollided_7() { return static_cast<int32_t>(offsetof(MagicProjectileScript_t1654175834, ___hasCollided_7)); }
	inline bool get_hasCollided_7() const { return ___hasCollided_7; }
	inline bool* get_address_of_hasCollided_7() { return &___hasCollided_7; }
	inline void set_hasCollided_7(bool value)
	{
		___hasCollided_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICPROJECTILESCRIPT_T1654175834_H
#ifndef HIDEHAIR_T2671652350_H
#define HIDEHAIR_T2671652350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HideHair
struct  HideHair_t2671652350  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 HideHair::numHairStyles
	int32_t ___numHairStyles_2;

public:
	inline static int32_t get_offset_of_numHairStyles_2() { return static_cast<int32_t>(offsetof(HideHair_t2671652350, ___numHairStyles_2)); }
	inline int32_t get_numHairStyles_2() const { return ___numHairStyles_2; }
	inline int32_t* get_address_of_numHairStyles_2() { return &___numHairStyles_2; }
	inline void set_numHairStyles_2(int32_t value)
	{
		___numHairStyles_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEHAIR_T2671652350_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (CustomBlend_t4150975672)+ sizeof (RuntimeObject), sizeof(CustomBlend_t4150975672_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[3] = 
{
	CustomBlend_t4150975672::get_offset_of_m_From_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomBlend_t4150975672::get_offset_of_m_To_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomBlend_t4150975672::get_offset_of_m_Blend_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (CinemachineCore_t1985905913), -1, sizeof(CinemachineCore_t1985905913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[9] = 
{
	CinemachineCore_t1985905913_StaticFields::get_offset_of_kVersionString_0(),
	CinemachineCore_t1985905913_StaticFields::get_offset_of_sInstance_1(),
	CinemachineCore_t1985905913_StaticFields::get_offset_of_sShowHiddenObjects_2(),
	CinemachineCore_t1985905913_StaticFields::get_offset_of_GetInputAxis_3(),
	CinemachineCore_t1985905913::get_offset_of_mActiveBrains_4(),
	CinemachineCore_t1985905913::get_offset_of_mActiveCameras_5(),
	CinemachineCore_t1985905913::get_offset_of_mUpdateStatus_6(),
	CinemachineCore_t1985905913::get_offset_of_U3CCurrentUpdateFilterU3Ek__BackingField_7(),
	CinemachineCore_t1985905913_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (Stage_t2941156404)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2402[5] = 
{
	Stage_t2941156404::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (AxisInputDelegate_t2029212819), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (UpdateStatus_t4119700699)+ sizeof (RuntimeObject), sizeof(UpdateStatus_t4119700699 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[4] = 
{
	UpdateStatus_t4119700699::get_offset_of_frame_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t4119700699::get_offset_of_subframe_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t4119700699::get_offset_of_lastFixedUpdate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t4119700699::get_offset_of_targetPos_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (UpdateFilter_t463341921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2405[4] = 
{
	UpdateFilter_t463341921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (CinemachineGameWindowDebug_t4056967167), -1, sizeof(CinemachineGameWindowDebug_t4056967167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2406[1] = 
{
	CinemachineGameWindowDebug_t4056967167_StaticFields::get_offset_of_mClients_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (CinemachinePathBase_t3902360541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (LensSettingsPropertyAttribute_t2647531223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (CinemachineBlendDefinitionPropertyAttribute_t3768968087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (SaveDuringPlayAttribute_t1252963312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (NoSaveDuringPlayAttribute_t4285710416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (MinAttribute_t3533541894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[1] = 
{
	MinAttribute_t3533541894::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (GetSetAttribute_t3750549756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[2] = 
{
	GetSetAttribute_t3750549756::get_offset_of_name_0(),
	GetSetAttribute_t3750549756::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (DocumentationSortingAttribute_t681625788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[2] = 
{
	DocumentationSortingAttribute_t681625788::get_offset_of_U3CSortOrderU3Ek__BackingField_0(),
	DocumentationSortingAttribute_t681625788::get_offset_of_U3CCategoryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (Level_t3916434835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[4] = 
{
	Level_t3916434835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (CinemachineVirtualCameraBase_t2557508663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[10] = 
{
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_CinemachineGUIDebuggerCallback_2(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_HideHeaderInInspector_3(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_ExcludedPropertiesInInspector_4(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_LockStageInInspector_5(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_Priority_6(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_OnPostPipelineStage_7(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_U3CPreviousStateInvalidU3Ek__BackingField_8(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_mSlaveStatusUpdated_9(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_parentVcam_10(),
	CinemachineVirtualCameraBase_t2557508663::get_offset_of_m_QueuePriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (OnPostPipelineStageDelegate_t938695173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (GaussianWindow1D_Vector3_t1200723002), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (GaussianWindow1D_Quaternion_t2095249446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (GaussianWindow1D_CameraRotation_t90163852), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (LensSettings_t1505430577)+ sizeof (RuntimeObject), sizeof(LensSettings_t1505430577_marshaled_pinvoke), sizeof(LensSettings_t1505430577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2424[8] = 
{
	LensSettings_t1505430577_StaticFields::get_offset_of_Default_0(),
	LensSettings_t1505430577::get_offset_of_FieldOfView_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_OrthographicSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_NearClipPlane_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_FarClipPlane_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_Dutch_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_U3COrthographicU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t1505430577::get_offset_of_U3CAspectU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (NoiseSettings_t2082871546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[2] = 
{
	NoiseSettings_t2082871546::get_offset_of_m_Position_2(),
	NoiseSettings_t2082871546::get_offset_of_m_Orientation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (NoiseParams_t633381832)+ sizeof (RuntimeObject), sizeof(NoiseParams_t633381832 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2426[2] = 
{
	NoiseParams_t633381832::get_offset_of_Amplitude_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NoiseParams_t633381832::get_offset_of_Frequency_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (TransformNoiseParams_t2565733168)+ sizeof (RuntimeObject), sizeof(TransformNoiseParams_t2565733168 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2427[3] = 
{
	TransformNoiseParams_t2565733168::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t2565733168::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t2565733168::get_offset_of_Z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (ReflectionHelpers_t1786918968), -1, sizeof(ReflectionHelpers_t1786918968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2428[1] = 
{
	ReflectionHelpers_t1786918968_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (U3CGetTypesInAssemblyU3Ec__AnonStorey0_t3109715705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[1] = 
{
	U3CGetTypesInAssemblyU3Ec__AnonStorey0_t3109715705::get_offset_of_predicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (U3CGetTypeInAllLoadedAssembliesU3Ec__AnonStorey1_t2664077166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[1] = 
{
	U3CGetTypeInAllLoadedAssembliesU3Ec__AnonStorey1_t2664077166::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (U3CGetTypesInLoadedAssembliesU3Ec__AnonStorey2_t1670705979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[1] = 
{
	U3CGetTypesInLoadedAssembliesU3Ec__AnonStorey2_t1670705979::get_offset_of_assemblyPredicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (UnityVectorExtensions_t2558878399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (UnityQuaternionExtensions_t834130850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (UnityRectExtensions_t296430228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (Blackboard_t108108094), -1, sizeof(Blackboard_t108108094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[3] = 
{
	Blackboard_t108108094_StaticFields::get_offset_of_CinemachineBlackboard_0(),
	Blackboard_t108108094::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Blackboard_t108108094::get_offset_of_mValues_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (Reactor_t1306100108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[1] = 
{
	Reactor_t1306100108::get_offset_of_m_TargetMappings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (CombineMode_t3158640028)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2437[6] = 
{
	CombineMode_t3158640028::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (BlackboardExpression_t4195482571)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	BlackboardExpression_t4195482571::get_offset_of_m_Lines_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (Line_t1381376710)+ sizeof (RuntimeObject), sizeof(Line_t1381376710_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2439[4] = 
{
	Line_t1381376710::get_offset_of_m_Operation_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t1381376710::get_offset_of_m_BlackboardKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t1381376710::get_offset_of_m_Remap_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t1381376710::get_offset_of_m_RemapCurve_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (TargetModifier_t1423884133)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	TargetModifier_t1423884133::get_offset_of_m_Field_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetModifier_t1423884133::get_offset_of_m_Operation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetModifier_t1423884133::get_offset_of_m_Expression_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetModifier_t1423884133::get_offset_of_U3CBindingU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (TargetBinding_t1523791741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	TargetBinding_t1523791741::get_offset_of_mTargetFieldInfo_0(),
	TargetBinding_t1523791741::get_offset_of_mTargetFieldOwner_1(),
	TargetBinding_t1523791741::get_offset_of_mInitialValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (U3CBindTargetU3Ec__AnonStorey0_t4020504393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[2] = 
{
	U3CBindTargetU3Ec__AnonStorey0_t4020504393::get_offset_of_fieldName_0(),
	U3CBindTargetU3Ec__AnonStorey0_t4020504393::get_offset_of_binding_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (GameObjectFieldScanner_t1257740105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[2] = 
{
	GameObjectFieldScanner_t1257740105::get_offset_of_OnLeafField_0(),
	GameObjectFieldScanner_t1257740105::get_offset_of_bindingFlags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (OnLeafFieldDelegate_t4271637819), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (U3CScanFieldsU3Ec__AnonStorey0_t1209448285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	U3CScanFieldsU3Ec__AnonStorey0_t1209448285::get_offset_of_fields_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (U3CScanFieldsU3Ec__AnonStorey1_t3194405036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[2] = 
{
	U3CScanFieldsU3Ec__AnonStorey1_t3194405036::get_offset_of_i_0(),
	U3CScanFieldsU3Ec__AnonStorey1_t3194405036::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (ScriptingExample_t2272617098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[3] = 
{
	ScriptingExample_t2272617098::get_offset_of_vcam_2(),
	ScriptingExample_t2272617098::get_offset_of_freelook_3(),
	ScriptingExample_t2272617098::get_offset_of_lastSwapTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (CinemachineExampleWindow_t2933416263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[4] = 
{
	CinemachineExampleWindow_t2933416263::get_offset_of_m_Title_2(),
	CinemachineExampleWindow_t2933416263::get_offset_of_m_Description_3(),
	CinemachineExampleWindow_t2933416263::get_offset_of_mShowingHelpWindow_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (U3COnGUIU3Ec__AnonStorey0_t1275810891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	U3COnGUIU3Ec__AnonStorey0_t1275810891::get_offset_of_maxWidth_0(),
	U3COnGUIU3Ec__AnonStorey0_t1275810891::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (CinemachinePostFX_t1775947432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[5] = 
{
	CinemachinePostFX_t1775947432::get_offset_of_m_Profile_2(),
	CinemachinePostFX_t1775947432::get_offset_of_m_FocusTracksTarget_3(),
	CinemachinePostFX_t1775947432::get_offset_of_m_FocusOffset_4(),
	CinemachinePostFX_t1775947432::get_offset_of_mBrain_5(),
	CinemachinePostFX_t1775947432::get_offset_of_mPostProcessingBehaviour_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (CinemachineMixer_t3106063062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[2] = 
{
	CinemachineMixer_t3106063062::get_offset_of_mBrain_0(),
	CinemachineMixer_t3106063062::get_offset_of_mBrainOverrideId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (CinemachineShotPlayable_t3687665838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[1] = 
{
	CinemachineShotPlayable_t3687665838::get_offset_of_VirtualCamera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (CinemachineShot_t558745826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	CinemachineShot_t558745826::get_offset_of_VirtualCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (CinemachineTrack_t127841354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (CloseIceTrigger_t760838387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[1] = 
{
	CloseIceTrigger_t760838387::get_offset_of_l2S_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (Collectible_t2045918932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[2] = 
{
	Collectible_t2045918932::get_offset_of_strID_2(),
	Collectible_t2045918932::get_offset_of_instanceTHL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CommonListCode_t272715144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (GetDescriptionDelegate_t976785689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (CutsceneHolder_t29886913), -1, sizeof(CutsceneHolder_t29886913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2459[2] = 
{
	CutsceneHolder_t29886913_StaticFields::get_offset_of_instance_2(),
	CutsceneHolder_t29886913::get_offset_of_gemPlacementCutscene_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (DestroyParticle_t2595860380), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (DecalDestroyer_t2158806118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[1] = 
{
	DecalDestroyer_t2158806118::get_offset_of_lifeTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (U3CStartU3Ec__Iterator0_t3124061724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[4] = 
{
	U3CStartU3Ec__Iterator0_t3124061724::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3124061724::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3124061724::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3124061724::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (ExtinguishableFire_t2564158560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[4] = 
{
	ExtinguishableFire_t2564158560::get_offset_of_fireParticleSystem_2(),
	ExtinguishableFire_t2564158560::get_offset_of_smokeParticleSystem_3(),
	ExtinguishableFire_t2564158560::get_offset_of_m_isExtinguished_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (U3CExtinguishingU3Ec__Iterator0_t3585316312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[6] = 
{
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U3CratioU3E__1_1(),
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U24this_2(),
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U24current_3(),
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U24disposing_4(),
	U3CExtinguishingU3Ec__Iterator0_t3585316312::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (U3CStartingFireU3Ec__Iterator1_t824662880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[6] = 
{
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U3CratioU3E__1_1(),
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U24this_2(),
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U24current_3(),
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U24disposing_4(),
	U3CStartingFireU3Ec__Iterator1_t824662880::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (GunAim_t211531317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[6] = 
{
	GunAim_t211531317::get_offset_of_borderLeft_2(),
	GunAim_t211531317::get_offset_of_borderRight_3(),
	GunAim_t211531317::get_offset_of_borderTop_4(),
	GunAim_t211531317::get_offset_of_borderBottom_5(),
	GunAim_t211531317::get_offset_of_parentCamera_6(),
	GunAim_t211531317::get_offset_of_isOutOfBounds_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (GunShoot_t1510360140), -1, sizeof(GunShoot_t1510360140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[16] = 
{
	GunShoot_t1510360140::get_offset_of_fireRate_2(),
	GunShoot_t1510360140::get_offset_of_weaponRange_3(),
	GunShoot_t1510360140::get_offset_of_gunEnd_4(),
	GunShoot_t1510360140::get_offset_of_muzzleFlash_5(),
	GunShoot_t1510360140::get_offset_of_cartridgeEjection_6(),
	GunShoot_t1510360140::get_offset_of_metalHitEffect_7(),
	GunShoot_t1510360140::get_offset_of_sandHitEffect_8(),
	GunShoot_t1510360140::get_offset_of_stoneHitEffect_9(),
	GunShoot_t1510360140::get_offset_of_waterLeakEffect_10(),
	GunShoot_t1510360140::get_offset_of_waterLeakExtinguishEffect_11(),
	GunShoot_t1510360140::get_offset_of_fleshHitEffects_12(),
	GunShoot_t1510360140::get_offset_of_woodHitEffect_13(),
	GunShoot_t1510360140::get_offset_of_nextFire_14(),
	GunShoot_t1510360140::get_offset_of_anim_15(),
	GunShoot_t1510360140::get_offset_of_gunAim_16(),
	GunShoot_t1510360140_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (ParticleCollision_t144936607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[2] = 
{
	ParticleCollision_t144936607::get_offset_of_m_CollisionEvents_2(),
	ParticleCollision_t144936607::get_offset_of_m_ParticleSystem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (ParticleExamples_t356210336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[6] = 
{
	ParticleExamples_t356210336::get_offset_of_title_0(),
	ParticleExamples_t356210336::get_offset_of_description_1(),
	ParticleExamples_t356210336::get_offset_of_isWeaponEffect_2(),
	ParticleExamples_t356210336::get_offset_of_particleSystemGO_3(),
	ParticleExamples_t356210336::get_offset_of_particlePosition_4(),
	ParticleExamples_t356210336::get_offset_of_particleRotation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (ParticleMenu_t3804455348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[8] = 
{
	ParticleMenu_t3804455348::get_offset_of_particleSystems_2(),
	ParticleMenu_t3804455348::get_offset_of_gunGameObject_3(),
	ParticleMenu_t3804455348::get_offset_of_currentIndex_4(),
	ParticleMenu_t3804455348::get_offset_of_currentGO_5(),
	ParticleMenu_t3804455348::get_offset_of_spawnLocation_6(),
	ParticleMenu_t3804455348::get_offset_of_title_7(),
	ParticleMenu_t3804455348::get_offset_of_description_8(),
	ParticleMenu_t3804455348::get_offset_of_navigationDetails_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (EnterTextDialog_t1585287947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[6] = 
{
	EnterTextDialog_t1585287947::get_offset_of_numFields_2(),
	EnterTextDialog_t1585287947::get_offset_of_fieldPrefab_3(),
	EnterTextDialog_t1585287947::get_offset_of_fieldGap_4(),
	EnterTextDialog_t1585287947::get_offset_of_offset_5(),
	EnterTextDialog_t1585287947::get_offset_of_title_6(),
	EnterTextDialog_t1585287947::get_offset_of_inputFields_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (Foliage_t1566384678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[1] = 
{
	Foliage_t1566384678::get_offset_of_foliageLevels_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (FrameCount_t1086700601), -1, sizeof(FrameCount_t1086700601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2473[5] = 
{
	FrameCount_t1086700601_StaticFields::get_offset_of_instanceRef_2(),
	FrameCount_t1086700601::get_offset_of_updateInterval_3(),
	FrameCount_t1086700601::get_offset_of_accum_4(),
	FrameCount_t1086700601::get_offset_of_frames_5(),
	FrameCount_t1086700601::get_offset_of_timeleft_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (GemEffectHelper_t1002598843), -1, sizeof(GemEffectHelper_t1002598843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2474[2] = 
{
	GemEffectHelper_t1002598843_StaticFields::get_offset_of_instance_2(),
	GemEffectHelper_t1002598843::get_offset_of_systems_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (EffectAttachPoint_t460027681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[2] = 
{
	EffectAttachPoint_t460027681::get_offset_of_obj_0(),
	EffectAttachPoint_t460027681::get_offset_of_offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (HappyLittleFogMachine_t3641353099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[2] = 
{
	HappyLittleFogMachine_t3641353099::get_offset_of_fogPrefab_2(),
	HappyLittleFogMachine_t3641353099::get_offset_of_characterAttachPoint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (HeatHazePlane_t953234327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (HideHair_t2671652350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[1] = 
{
	HideHair_t2671652350::get_offset_of_numHairStyles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (HowIFeltFaces_t4227992676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[6] = 
{
	HowIFeltFaces_t4227992676::get_offset_of_happyFace_2(),
	HowIFeltFaces_t4227992676::get_offset_of_neutralFace_3(),
	HowIFeltFaces_t4227992676::get_offset_of_sadFace_4(),
	HowIFeltFaces_t4227992676::get_offset_of_faceSprites_5(),
	HowIFeltFaces_t4227992676::get_offset_of_faces_6(),
	HowIFeltFaces_t4227992676::get_offset_of_activeFace_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (FaceType_t2671696802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2480[7] = 
{
	FaceType_t2671696802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (ImproveLightQuality_t466835790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[5] = 
{
	ImproveLightQuality_t466835790::get_offset_of_startColour_2(),
	ImproveLightQuality_t466835790::get_offset_of_endColour_3(),
	ImproveLightQuality_t466835790::get_offset_of_startIntensity_4(),
	ImproveLightQuality_t466835790::get_offset_of_endIntensity_5(),
	ImproveLightQuality_t466835790::get_offset_of_light_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (JumpButton_t162985334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[1] = 
{
	JumpButton_t162985334::get_offset_of_rt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (DemoCameraSelector_t3185491292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[7] = 
{
	DemoCameraSelector_t3185491292::get_offset_of_cameras_2(),
	DemoCameraSelector_t3185491292::get_offset_of_index_3(),
	DemoCameraSelector_t3185491292::get_offset_of_diff_4(),
	DemoCameraSelector_t3185491292::get_offset_of_heig_5(),
	DemoCameraSelector_t3185491292::get_offset_of_power_6(),
	DemoCameraSelector_t3185491292::get_offset_of_speed_7(),
	DemoCameraSelector_t3185491292::get_offset_of_meshName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (LevelSelector_t1659548950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[6] = 
{
	LevelSelector_t1659548950::get_offset_of_levelString_2(),
	LevelSelector_t1659548950::get_offset_of_dataInput_3(),
	LevelSelector_t1659548950::get_offset_of_sendOffMoreCalls_4(),
	LevelSelector_t1659548950::get_offset_of_usernameField_5(),
	LevelSelector_t1659548950::get_offset_of_reset_6(),
	LevelSelector_t1659548950::get_offset_of_options_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (LightHolder_t677068996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[1] = 
{
	LightHolder_t677068996::get_offset_of_lights_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (ListScreen_t2784779182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[12] = 
{
	ListScreen_t2784779182::get_offset_of_dragAndDropPrefab_2(),
	ListScreen_t2784779182::get_offset_of_title_3(),
	ListScreen_t2784779182::get_offset_of_description_4(),
	ListScreen_t2784779182::get_offset_of_leftWordsSpawner_5(),
	ListScreen_t2784779182::get_offset_of_rightWordsSpawner_6(),
	ListScreen_t2784779182::get_offset_of_snapDistance_7(),
	ListScreen_t2784779182::get_offset_of_snapPoints_8(),
	ListScreen_t2784779182::get_offset_of_ySpacing_9(),
	ListScreen_t2784779182::get_offset_of_nextButton_10(),
	ListScreen_t2784779182::get_offset_of_bReadyToContinue_11(),
	ListScreen_t2784779182::get_offset_of_descriptions_12(),
	ListScreen_t2784779182::get_offset_of_dragAndDrops_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (MagicButtonScript_t1297805390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[10] = 
{
	MagicButtonScript_t1297805390::get_offset_of_Button_2(),
	MagicButtonScript_t1297805390::get_offset_of_MyButtonText_3(),
	MagicButtonScript_t1297805390::get_offset_of_projectileParticleName_4(),
	MagicButtonScript_t1297805390::get_offset_of_effectScript_5(),
	MagicButtonScript_t1297805390::get_offset_of_projectileScript_6(),
	MagicButtonScript_t1297805390::get_offset_of_buttonsX_7(),
	MagicButtonScript_t1297805390::get_offset_of_buttonsY_8(),
	MagicButtonScript_t1297805390::get_offset_of_buttonsSizeX_9(),
	MagicButtonScript_t1297805390::get_offset_of_buttonsSizeY_10(),
	MagicButtonScript_t1297805390::get_offset_of_buttonsDistance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (MagicDragMouseOrbit_t2106366795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[13] = 
{
	MagicDragMouseOrbit_t2106366795::get_offset_of_target_2(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_distance_3(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_xSpeed_4(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_ySpeed_5(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_yMinLimit_6(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_yMaxLimit_7(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_distanceMin_8(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_distanceMax_9(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_smoothTime_10(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_rotationYAxis_11(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_rotationXAxis_12(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_velocityX_13(),
	MagicDragMouseOrbit_t2106366795::get_offset_of_velocityY_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (MagicFireProjectile_t3063101194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[6] = 
{
	MagicFireProjectile_t3063101194::get_offset_of_hit_2(),
	MagicFireProjectile_t3063101194::get_offset_of_projectiles_3(),
	MagicFireProjectile_t3063101194::get_offset_of_spawnPosition_4(),
	MagicFireProjectile_t3063101194::get_offset_of_currentProjectile_5(),
	MagicFireProjectile_t3063101194::get_offset_of_speed_6(),
	MagicFireProjectile_t3063101194::get_offset_of_selectedProjectileButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (MagicLoadSceneOnClick_t1944786205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (MagicLoopScript_t2356792626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[2] = 
{
	MagicLoopScript_t2356792626::get_offset_of_chosenEffect_2(),
	MagicLoopScript_t2356792626::get_offset_of_loopTimeLimit_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (U3CEffectLoopU3Ec__Iterator0_t989201242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[5] = 
{
	U3CEffectLoopU3Ec__Iterator0_t989201242::get_offset_of_U3CeffectPlayerU3E__0_0(),
	U3CEffectLoopU3Ec__Iterator0_t989201242::get_offset_of_U24this_1(),
	U3CEffectLoopU3Ec__Iterator0_t989201242::get_offset_of_U24current_2(),
	U3CEffectLoopU3Ec__Iterator0_t989201242::get_offset_of_U24disposing_3(),
	U3CEffectLoopU3Ec__Iterator0_t989201242::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (MagicProjectileScript_t1654175834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[6] = 
{
	MagicProjectileScript_t1654175834::get_offset_of_impactParticle_2(),
	MagicProjectileScript_t1654175834::get_offset_of_projectileParticle_3(),
	MagicProjectileScript_t1654175834::get_offset_of_muzzleParticle_4(),
	MagicProjectileScript_t1654175834::get_offset_of_trailParticles_5(),
	MagicProjectileScript_t1654175834::get_offset_of_impactNormal_6(),
	MagicProjectileScript_t1654175834::get_offset_of_hasCollided_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (MagicBeamScript_t875858234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[14] = 
{
	MagicBeamScript_t875858234::get_offset_of_beamLineRendererPrefab_2(),
	MagicBeamScript_t875858234::get_offset_of_beamStartPrefab_3(),
	MagicBeamScript_t875858234::get_offset_of_beamEndPrefab_4(),
	MagicBeamScript_t875858234::get_offset_of_currentBeam_5(),
	MagicBeamScript_t875858234::get_offset_of_beamStart_6(),
	MagicBeamScript_t875858234::get_offset_of_beamEnd_7(),
	MagicBeamScript_t875858234::get_offset_of_beam_8(),
	MagicBeamScript_t875858234::get_offset_of_line_9(),
	MagicBeamScript_t875858234::get_offset_of_beamEndOffset_10(),
	MagicBeamScript_t875858234::get_offset_of_textureScrollSpeed_11(),
	MagicBeamScript_t875858234::get_offset_of_textureLengthScale_12(),
	MagicBeamScript_t875858234::get_offset_of_endOffSetSlider_13(),
	MagicBeamScript_t875858234::get_offset_of_scrollSpeedSlider_14(),
	MagicBeamScript_t875858234::get_offset_of_textBeamName_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (MagicLightFade_t2834402439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[4] = 
{
	MagicLightFade_t2834402439::get_offset_of_life_2(),
	MagicLightFade_t2834402439::get_offset_of_killAfterLife_3(),
	MagicLightFade_t2834402439::get_offset_of_li_4(),
	MagicLightFade_t2834402439::get_offset_of_initIntensity_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (MagicRotation_t2833249381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[2] = 
{
	MagicRotation_t2833249381::get_offset_of_rotateVector_2(),
	MagicRotation_t2833249381::get_offset_of_rotateSpace_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (spaceEnum_t1146975981)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2497[3] = 
{
	spaceEnum_t1146975981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (Province_t1688297463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[3] = 
{
	Province_t1688297463::get_offset_of_provinceImage_0(),
	Province_t1688297463::get_offset_of_provinceText_1(),
	Province_t1688297463::get_offset_of_script_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (U3CUnlockU3Ec__Iterator0_t3597604007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[7] = 
{
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of_U3CtimeU3E__0_0(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of__delay_1(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of__level_2(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of_U24this_3(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of_U24current_4(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of_U24disposing_5(),
	U3CUnlockU3Ec__Iterator0_t3597604007::get_offset_of_U24PC_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
