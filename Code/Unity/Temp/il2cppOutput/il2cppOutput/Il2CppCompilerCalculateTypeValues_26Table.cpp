﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.GameObject
struct GameObject_t2557347079;
// UpdateManager/OnUpdate
struct OnUpdate_t172587279;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1618594486;
// UnityEngine.Object
struct Object_t692178351;
// UpdateManager
struct UpdateManager_t1291149948;
// UnityEngine.AudioListener
struct AudioListener_t3253228504;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.String
struct String_t;
// System.Collections.Generic.List`1<BMGlyph/Kerning>
struct List_1_t4061970673;
// BMGlyph[]
struct BMGlyphU5BU5D_t157392106;
// System.Collections.Generic.List`1<BMGlyph>
struct List_1_t2006393738;
// System.Collections.Generic.List`1<BMSymbol>
struct List_1_t507139136;
// System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>
struct Dictionary_2_t2746063763;
// System.String[]
struct StringU5BU5D_t2511808107;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t1419994223;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t3851579516;
// System.Void
struct Void_t653366341;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t3872312760;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// UnityEngine.Collider
struct Collider_t1485601975;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t442302241;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t3078850060;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1883882744;
// UnityEngine.Transform
struct Transform_t362059596;
// UIWidget
struct UIWidget_t1961100141;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t1249652403;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t4170205938;
// UnityEngine.Camera
struct Camera_t2839736942;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UITweener
struct UITweener_t1705010025;
// SpringPosition
struct SpringPosition_t1843463491;
// UIEventListener/VoidDelegate
struct VoidDelegate_t817628917;
// UIEventListener/BoolDelegate
struct BoolDelegate_t192583560;
// UIEventListener/FloatDelegate
struct FloatDelegate_t284650127;
// UIEventListener/VectorDelegate
struct VectorDelegate_t1991147574;
// UIEventListener/ObjectDelegate
struct ObjectDelegate_t2232498032;
// UIEventListener/StringDelegate
struct StringDelegate_t3231667884;
// UIEventListener/KeyCodeDelegate
struct KeyCodeDelegate_t1109435781;
// UnityEngine.Material
struct Material_t2815264910;
// System.Collections.Generic.List`1<UIAtlas/Sprite>
struct List_1_t2158807275;
// UnityEngine.Mesh
struct Mesh_t4030024733;
// UnityEngine.MeshFilter
struct MeshFilter_t2059166712;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2851551746;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// UnityEngine.TextAsset[]
struct TextAssetU5BU5D_t2688711784;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1060102516;
// UILabel
struct UILabel_t3478074517;
// UIInput/Validator
struct Validator_t1053183372;
// UIInput/OnSubmit
struct OnSubmit_t2511512930;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t1357543767;
// UnityEngine.Texture
struct Texture_t2119925672;
// UIPanel
struct UIPanel_t825970685;
// UIGeometry
struct UIGeometry_t76808639;
// BMFont
struct BMFont_t2958998632;
// UIAtlas
struct UIAtlas_t1815452364;
// UIAtlas/Sprite
struct Sprite_t3039180268;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1701645977;
// System.Collections.Generic.List`1<UpdateManager/UpdateEntry>
struct List_1_t1921793510;
// BetterList`1<UpdateManager/DestroyEntry>
struct BetterList_1_t2800363178;
// System.Comparison`1<UpdateManager/UpdateEntry>
struct Comparison_1_t4186224356;
// UICamera/MouseOrTouch
struct MouseOrTouch_t115752944;
// System.Collections.Generic.List`1<UICamera>
struct List_1_t2963576205;
// System.Collections.Generic.List`1<UICamera/Highlighted>
struct List_1_t1157524252;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t1271829073;
// System.Collections.Generic.Dictionary`2<System.Int32,UICamera/MouseOrTouch>
struct Dictionary_2_t4270017272;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t3090405098;
// System.Comparison`1<UICamera>
struct Comparison_1_t933039755;
// System.Collections.Generic.List`1<UIRoot>
struct List_1_t57033334;
// UISlicedSprite
struct UISlicedSprite_t3319190482;
// UIWidget[]
struct UIWidgetU5BU5D_t2376853568;
// System.Collections.Generic.List`1<UITextList/Paragraph>
struct List_1_t2870954769;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1676974086;
// PickAThingDialog/PickAThingCallback
struct PickAThingCallback_t4122863115;
// System.Collections.Specialized.OrderedDictionary
struct OrderedDictionary_t648530623;
// BetterList`1<UIWidget>
struct BetterList_1_t416469230;
// BetterList`1<UnityEngine.Material>
struct BetterList_1_t1270633999;
// BetterList`1<UIDrawCall>
struct BetterList_1_t1143290928;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t891669011;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t3776653899;
// BetterList`1<UINode>
struct BetterList_1_t3635274674;
// System.Comparison`1<UIWidget>
struct Comparison_1_t3345157994;
// UISprite
struct UISprite_t279728715;
// UIRoot
struct UIRoot_t937406327;
// UnityEngine.Animation
struct Animation_t3821138400;
// UIFont
struct UIFont_t2730669065;
// SpringPosition/OnFinished
struct OnFinished_t3829816540;
// UnityEngine.Shader
struct Shader_t1881769421;
// UITweener/OnFinished
struct OnFinished_t2354188770;
// SpringPanel/OnFinished
struct OnFinished_t2500445478;
// UIDraggablePanel
struct UIDraggablePanel_t2580300900;
// UnityEngine.AudioSource
struct AudioSource_t4025721661;
// UITable
struct UITable_t2217632677;
// UnityEngine.Light
struct Light_t1775228881;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef HIGHLIGHTED_T2037897245_H
#define HIGHLIGHTED_T2037897245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/Highlighted
struct  Highlighted_t2037897245  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UICamera/Highlighted::go
	GameObject_t2557347079 * ___go_0;
	// System.Int32 UICamera/Highlighted::counter
	int32_t ___counter_1;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(Highlighted_t2037897245, ___go_0)); }
	inline GameObject_t2557347079 * get_go_0() const { return ___go_0; }
	inline GameObject_t2557347079 ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_t2557347079 * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_counter_1() { return static_cast<int32_t>(offsetof(Highlighted_t2037897245, ___counter_1)); }
	inline int32_t get_counter_1() const { return ___counter_1; }
	inline int32_t* get_address_of_counter_1() { return &___counter_1; }
	inline void set_counter_1(int32_t value)
	{
		___counter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTED_T2037897245_H
#ifndef UPDATEENTRY_T2802166503_H
#define UPDATEENTRY_T2802166503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager/UpdateEntry
struct  UpdateEntry_t2802166503  : public RuntimeObject
{
public:
	// System.Int32 UpdateManager/UpdateEntry::index
	int32_t ___index_0;
	// UpdateManager/OnUpdate UpdateManager/UpdateEntry::func
	OnUpdate_t172587279 * ___func_1;
	// UnityEngine.MonoBehaviour UpdateManager/UpdateEntry::mb
	MonoBehaviour_t1618594486 * ___mb_2;
	// System.Boolean UpdateManager/UpdateEntry::isMonoBehaviour
	bool ___isMonoBehaviour_3;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(UpdateEntry_t2802166503, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_func_1() { return static_cast<int32_t>(offsetof(UpdateEntry_t2802166503, ___func_1)); }
	inline OnUpdate_t172587279 * get_func_1() const { return ___func_1; }
	inline OnUpdate_t172587279 ** get_address_of_func_1() { return &___func_1; }
	inline void set_func_1(OnUpdate_t172587279 * value)
	{
		___func_1 = value;
		Il2CppCodeGenWriteBarrier((&___func_1), value);
	}

	inline static int32_t get_offset_of_mb_2() { return static_cast<int32_t>(offsetof(UpdateEntry_t2802166503, ___mb_2)); }
	inline MonoBehaviour_t1618594486 * get_mb_2() const { return ___mb_2; }
	inline MonoBehaviour_t1618594486 ** get_address_of_mb_2() { return &___mb_2; }
	inline void set_mb_2(MonoBehaviour_t1618594486 * value)
	{
		___mb_2 = value;
		Il2CppCodeGenWriteBarrier((&___mb_2), value);
	}

	inline static int32_t get_offset_of_isMonoBehaviour_3() { return static_cast<int32_t>(offsetof(UpdateEntry_t2802166503, ___isMonoBehaviour_3)); }
	inline bool get_isMonoBehaviour_3() const { return ___isMonoBehaviour_3; }
	inline bool* get_address_of_isMonoBehaviour_3() { return &___isMonoBehaviour_3; }
	inline void set_isMonoBehaviour_3(bool value)
	{
		___isMonoBehaviour_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEENTRY_T2802166503_H
#ifndef DESTROYENTRY_T50026793_H
#define DESTROYENTRY_T50026793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager/DestroyEntry
struct  DestroyEntry_t50026793  : public RuntimeObject
{
public:
	// UnityEngine.Object UpdateManager/DestroyEntry::obj
	Object_t692178351 * ___obj_0;
	// System.Single UpdateManager/DestroyEntry::time
	float ___time_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(DestroyEntry_t50026793, ___obj_0)); }
	inline Object_t692178351 * get_obj_0() const { return ___obj_0; }
	inline Object_t692178351 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Object_t692178351 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(DestroyEntry_t50026793, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYENTRY_T50026793_H
#ifndef U3CCOROUTINEFUNCTIONU3EC__ITERATOR0_T2169398617_H
#define U3CCOROUTINEFUNCTIONU3EC__ITERATOR0_T2169398617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager/<CoroutineFunction>c__Iterator0
struct  U3CCoroutineFunctionU3Ec__Iterator0_t2169398617  : public RuntimeObject
{
public:
	// UpdateManager UpdateManager/<CoroutineFunction>c__Iterator0::$this
	UpdateManager_t1291149948 * ___U24this_0;
	// System.Object UpdateManager/<CoroutineFunction>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UpdateManager/<CoroutineFunction>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UpdateManager/<CoroutineFunction>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoroutineFunctionU3Ec__Iterator0_t2169398617, ___U24this_0)); }
	inline UpdateManager_t1291149948 * get_U24this_0() const { return ___U24this_0; }
	inline UpdateManager_t1291149948 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UpdateManager_t1291149948 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoroutineFunctionU3Ec__Iterator0_t2169398617, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoroutineFunctionU3Ec__Iterator0_t2169398617, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoroutineFunctionU3Ec__Iterator0_t2169398617, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINEFUNCTIONU3EC__ITERATOR0_T2169398617_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef NGUITOOLS_T582569761_H
#define NGUITOOLS_T582569761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUITools
struct  NGUITools_t582569761  : public RuntimeObject
{
public:

public:
};

struct NGUITools_t582569761_StaticFields
{
public:
	// UnityEngine.AudioListener NGUITools::mListener
	AudioListener_t3253228504 * ___mListener_0;
	// System.Boolean NGUITools::mLoaded
	bool ___mLoaded_1;
	// System.Single NGUITools::mGlobalVolume
	float ___mGlobalVolume_2;

public:
	inline static int32_t get_offset_of_mListener_0() { return static_cast<int32_t>(offsetof(NGUITools_t582569761_StaticFields, ___mListener_0)); }
	inline AudioListener_t3253228504 * get_mListener_0() const { return ___mListener_0; }
	inline AudioListener_t3253228504 ** get_address_of_mListener_0() { return &___mListener_0; }
	inline void set_mListener_0(AudioListener_t3253228504 * value)
	{
		___mListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___mListener_0), value);
	}

	inline static int32_t get_offset_of_mLoaded_1() { return static_cast<int32_t>(offsetof(NGUITools_t582569761_StaticFields, ___mLoaded_1)); }
	inline bool get_mLoaded_1() const { return ___mLoaded_1; }
	inline bool* get_address_of_mLoaded_1() { return &___mLoaded_1; }
	inline void set_mLoaded_1(bool value)
	{
		___mLoaded_1 = value;
	}

	inline static int32_t get_offset_of_mGlobalVolume_2() { return static_cast<int32_t>(offsetof(NGUITools_t582569761_StaticFields, ___mGlobalVolume_2)); }
	inline float get_mGlobalVolume_2() const { return ___mGlobalVolume_2; }
	inline float* get_address_of_mGlobalVolume_2() { return &___mGlobalVolume_2; }
	inline void set_mGlobalVolume_2(float value)
	{
		___mGlobalVolume_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUITOOLS_T582569761_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef NGUIMATH_T929756146_H
#define NGUIMATH_T929756146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIMath
struct  NGUIMath_t929756146  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUIMATH_T929756146_H
#ifndef BYTEREADER_T2713471465_H
#define BYTEREADER_T2713471465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteReader
struct  ByteReader_t2713471465  : public RuntimeObject
{
public:
	// System.Byte[] ByteReader::mBuffer
	ByteU5BU5D_t434619169* ___mBuffer_0;
	// System.Int32 ByteReader::mOffset
	int32_t ___mOffset_1;

public:
	inline static int32_t get_offset_of_mBuffer_0() { return static_cast<int32_t>(offsetof(ByteReader_t2713471465, ___mBuffer_0)); }
	inline ByteU5BU5D_t434619169* get_mBuffer_0() const { return ___mBuffer_0; }
	inline ByteU5BU5D_t434619169** get_address_of_mBuffer_0() { return &___mBuffer_0; }
	inline void set_mBuffer_0(ByteU5BU5D_t434619169* value)
	{
		___mBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_0), value);
	}

	inline static int32_t get_offset_of_mOffset_1() { return static_cast<int32_t>(offsetof(ByteReader_t2713471465, ___mOffset_1)); }
	inline int32_t get_mOffset_1() const { return ___mOffset_1; }
	inline int32_t* get_address_of_mOffset_1() { return &___mOffset_1; }
	inline void set_mOffset_1(int32_t value)
	{
		___mOffset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEREADER_T2713471465_H
#ifndef BMSYMBOL_T1387512129_H
#define BMSYMBOL_T1387512129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMSymbol
struct  BMSymbol_t1387512129  : public RuntimeObject
{
public:
	// System.String BMSymbol::sequence
	String_t* ___sequence_0;
	// System.Int32 BMSymbol::x
	int32_t ___x_1;
	// System.Int32 BMSymbol::y
	int32_t ___y_2;
	// System.Int32 BMSymbol::width
	int32_t ___width_3;
	// System.Int32 BMSymbol::height
	int32_t ___height_4;
	// System.Int32 BMSymbol::mLength
	int32_t ___mLength_5;

public:
	inline static int32_t get_offset_of_sequence_0() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___sequence_0)); }
	inline String_t* get_sequence_0() const { return ___sequence_0; }
	inline String_t** get_address_of_sequence_0() { return &___sequence_0; }
	inline void set_sequence_0(String_t* value)
	{
		___sequence_0 = value;
		Il2CppCodeGenWriteBarrier((&___sequence_0), value);
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_mLength_5() { return static_cast<int32_t>(offsetof(BMSymbol_t1387512129, ___mLength_5)); }
	inline int32_t get_mLength_5() const { return ___mLength_5; }
	inline int32_t* get_address_of_mLength_5() { return &___mLength_5; }
	inline void set_mLength_5(int32_t value)
	{
		___mLength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMSYMBOL_T1387512129_H
#ifndef UNIFORMS_T3304466817_H
#define UNIFORMS_T3304466817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms
struct  Uniforms_t3304466817  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3304466817_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Intensity
	int32_t ____Intensity_0;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Radius
	int32_t ____Radius_1;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Downsample
	int32_t ____Downsample_2;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_SampleCount
	int32_t ____SampleCount_3;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture1
	int32_t ____OcclusionTexture1_4;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture2
	int32_t ____OcclusionTexture2_5;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture
	int32_t ____OcclusionTexture_6;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_MainTex
	int32_t ____MainTex_7;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_TempRT
	int32_t ____TempRT_8;

public:
	inline static int32_t get_offset_of__Intensity_0() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____Intensity_0)); }
	inline int32_t get__Intensity_0() const { return ____Intensity_0; }
	inline int32_t* get_address_of__Intensity_0() { return &____Intensity_0; }
	inline void set__Intensity_0(int32_t value)
	{
		____Intensity_0 = value;
	}

	inline static int32_t get_offset_of__Radius_1() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____Radius_1)); }
	inline int32_t get__Radius_1() const { return ____Radius_1; }
	inline int32_t* get_address_of__Radius_1() { return &____Radius_1; }
	inline void set__Radius_1(int32_t value)
	{
		____Radius_1 = value;
	}

	inline static int32_t get_offset_of__Downsample_2() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____Downsample_2)); }
	inline int32_t get__Downsample_2() const { return ____Downsample_2; }
	inline int32_t* get_address_of__Downsample_2() { return &____Downsample_2; }
	inline void set__Downsample_2(int32_t value)
	{
		____Downsample_2 = value;
	}

	inline static int32_t get_offset_of__SampleCount_3() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____SampleCount_3)); }
	inline int32_t get__SampleCount_3() const { return ____SampleCount_3; }
	inline int32_t* get_address_of__SampleCount_3() { return &____SampleCount_3; }
	inline void set__SampleCount_3(int32_t value)
	{
		____SampleCount_3 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture1_4() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____OcclusionTexture1_4)); }
	inline int32_t get__OcclusionTexture1_4() const { return ____OcclusionTexture1_4; }
	inline int32_t* get_address_of__OcclusionTexture1_4() { return &____OcclusionTexture1_4; }
	inline void set__OcclusionTexture1_4(int32_t value)
	{
		____OcclusionTexture1_4 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture2_5() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____OcclusionTexture2_5)); }
	inline int32_t get__OcclusionTexture2_5() const { return ____OcclusionTexture2_5; }
	inline int32_t* get_address_of__OcclusionTexture2_5() { return &____OcclusionTexture2_5; }
	inline void set__OcclusionTexture2_5(int32_t value)
	{
		____OcclusionTexture2_5 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture_6() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____OcclusionTexture_6)); }
	inline int32_t get__OcclusionTexture_6() const { return ____OcclusionTexture_6; }
	inline int32_t* get_address_of__OcclusionTexture_6() { return &____OcclusionTexture_6; }
	inline void set__OcclusionTexture_6(int32_t value)
	{
		____OcclusionTexture_6 = value;
	}

	inline static int32_t get_offset_of__MainTex_7() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____MainTex_7)); }
	inline int32_t get__MainTex_7() const { return ____MainTex_7; }
	inline int32_t* get_address_of__MainTex_7() { return &____MainTex_7; }
	inline void set__MainTex_7(int32_t value)
	{
		____MainTex_7 = value;
	}

	inline static int32_t get_offset_of__TempRT_8() { return static_cast<int32_t>(offsetof(Uniforms_t3304466817_StaticFields, ____TempRT_8)); }
	inline int32_t get__TempRT_8() const { return ____TempRT_8; }
	inline int32_t* get_address_of__TempRT_8() { return &____TempRT_8; }
	inline void set__TempRT_8(int32_t value)
	{
		____TempRT_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3304466817_H
#ifndef BMGLYPH_T2886766731_H
#define BMGLYPH_T2886766731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMGlyph
struct  BMGlyph_t2886766731  : public RuntimeObject
{
public:
	// System.Int32 BMGlyph::index
	int32_t ___index_0;
	// System.Int32 BMGlyph::x
	int32_t ___x_1;
	// System.Int32 BMGlyph::y
	int32_t ___y_2;
	// System.Int32 BMGlyph::width
	int32_t ___width_3;
	// System.Int32 BMGlyph::height
	int32_t ___height_4;
	// System.Int32 BMGlyph::offsetX
	int32_t ___offsetX_5;
	// System.Int32 BMGlyph::offsetY
	int32_t ___offsetY_6;
	// System.Int32 BMGlyph::advance
	int32_t ___advance_7;
	// System.Int32 BMGlyph::channel
	int32_t ___channel_8;
	// System.Collections.Generic.List`1<BMGlyph/Kerning> BMGlyph::kerning
	List_1_t4061970673 * ___kerning_9;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_offsetX_5() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___offsetX_5)); }
	inline int32_t get_offsetX_5() const { return ___offsetX_5; }
	inline int32_t* get_address_of_offsetX_5() { return &___offsetX_5; }
	inline void set_offsetX_5(int32_t value)
	{
		___offsetX_5 = value;
	}

	inline static int32_t get_offset_of_offsetY_6() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___offsetY_6)); }
	inline int32_t get_offsetY_6() const { return ___offsetY_6; }
	inline int32_t* get_address_of_offsetY_6() { return &___offsetY_6; }
	inline void set_offsetY_6(int32_t value)
	{
		___offsetY_6 = value;
	}

	inline static int32_t get_offset_of_advance_7() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___advance_7)); }
	inline int32_t get_advance_7() const { return ___advance_7; }
	inline int32_t* get_address_of_advance_7() { return &___advance_7; }
	inline void set_advance_7(int32_t value)
	{
		___advance_7 = value;
	}

	inline static int32_t get_offset_of_channel_8() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___channel_8)); }
	inline int32_t get_channel_8() const { return ___channel_8; }
	inline int32_t* get_address_of_channel_8() { return &___channel_8; }
	inline void set_channel_8(int32_t value)
	{
		___channel_8 = value;
	}

	inline static int32_t get_offset_of_kerning_9() { return static_cast<int32_t>(offsetof(BMGlyph_t2886766731, ___kerning_9)); }
	inline List_1_t4061970673 * get_kerning_9() const { return ___kerning_9; }
	inline List_1_t4061970673 ** get_address_of_kerning_9() { return &___kerning_9; }
	inline void set_kerning_9(List_1_t4061970673 * value)
	{
		___kerning_9 = value;
		Il2CppCodeGenWriteBarrier((&___kerning_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMGLYPH_T2886766731_H
#ifndef BMFONT_T2958998632_H
#define BMFONT_T2958998632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMFont
struct  BMFont_t2958998632  : public RuntimeObject
{
public:
	// BMGlyph[] BMFont::mGlyphs
	BMGlyphU5BU5D_t157392106* ___mGlyphs_0;
	// System.Int32 BMFont::mSize
	int32_t ___mSize_1;
	// System.Int32 BMFont::mBase
	int32_t ___mBase_2;
	// System.Int32 BMFont::mWidth
	int32_t ___mWidth_3;
	// System.Int32 BMFont::mHeight
	int32_t ___mHeight_4;
	// System.String BMFont::mSpriteName
	String_t* ___mSpriteName_5;
	// System.Collections.Generic.List`1<BMGlyph> BMFont::mSaved
	List_1_t2006393738 * ___mSaved_6;
	// System.Collections.Generic.List`1<BMSymbol> BMFont::mSymbols
	List_1_t507139136 * ___mSymbols_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph> BMFont::mDict
	Dictionary_2_t2746063763 * ___mDict_8;

public:
	inline static int32_t get_offset_of_mGlyphs_0() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mGlyphs_0)); }
	inline BMGlyphU5BU5D_t157392106* get_mGlyphs_0() const { return ___mGlyphs_0; }
	inline BMGlyphU5BU5D_t157392106** get_address_of_mGlyphs_0() { return &___mGlyphs_0; }
	inline void set_mGlyphs_0(BMGlyphU5BU5D_t157392106* value)
	{
		___mGlyphs_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGlyphs_0), value);
	}

	inline static int32_t get_offset_of_mSize_1() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mSize_1)); }
	inline int32_t get_mSize_1() const { return ___mSize_1; }
	inline int32_t* get_address_of_mSize_1() { return &___mSize_1; }
	inline void set_mSize_1(int32_t value)
	{
		___mSize_1 = value;
	}

	inline static int32_t get_offset_of_mBase_2() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mBase_2)); }
	inline int32_t get_mBase_2() const { return ___mBase_2; }
	inline int32_t* get_address_of_mBase_2() { return &___mBase_2; }
	inline void set_mBase_2(int32_t value)
	{
		___mBase_2 = value;
	}

	inline static int32_t get_offset_of_mWidth_3() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mWidth_3)); }
	inline int32_t get_mWidth_3() const { return ___mWidth_3; }
	inline int32_t* get_address_of_mWidth_3() { return &___mWidth_3; }
	inline void set_mWidth_3(int32_t value)
	{
		___mWidth_3 = value;
	}

	inline static int32_t get_offset_of_mHeight_4() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mHeight_4)); }
	inline int32_t get_mHeight_4() const { return ___mHeight_4; }
	inline int32_t* get_address_of_mHeight_4() { return &___mHeight_4; }
	inline void set_mHeight_4(int32_t value)
	{
		___mHeight_4 = value;
	}

	inline static int32_t get_offset_of_mSpriteName_5() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mSpriteName_5)); }
	inline String_t* get_mSpriteName_5() const { return ___mSpriteName_5; }
	inline String_t** get_address_of_mSpriteName_5() { return &___mSpriteName_5; }
	inline void set_mSpriteName_5(String_t* value)
	{
		___mSpriteName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_5), value);
	}

	inline static int32_t get_offset_of_mSaved_6() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mSaved_6)); }
	inline List_1_t2006393738 * get_mSaved_6() const { return ___mSaved_6; }
	inline List_1_t2006393738 ** get_address_of_mSaved_6() { return &___mSaved_6; }
	inline void set_mSaved_6(List_1_t2006393738 * value)
	{
		___mSaved_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSaved_6), value);
	}

	inline static int32_t get_offset_of_mSymbols_7() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mSymbols_7)); }
	inline List_1_t507139136 * get_mSymbols_7() const { return ___mSymbols_7; }
	inline List_1_t507139136 ** get_address_of_mSymbols_7() { return &___mSymbols_7; }
	inline void set_mSymbols_7(List_1_t507139136 * value)
	{
		___mSymbols_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSymbols_7), value);
	}

	inline static int32_t get_offset_of_mDict_8() { return static_cast<int32_t>(offsetof(BMFont_t2958998632, ___mDict_8)); }
	inline Dictionary_2_t2746063763 * get_mDict_8() const { return ___mDict_8; }
	inline Dictionary_2_t2746063763 ** get_address_of_mDict_8() { return &___mDict_8; }
	inline void set_mDict_8(Dictionary_2_t2746063763 * value)
	{
		___mDict_8 = value;
		Il2CppCodeGenWriteBarrier((&___mDict_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMFONT_T2958998632_H
#ifndef PARAGRAPH_T3751327762_H
#define PARAGRAPH_T3751327762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList/Paragraph
struct  Paragraph_t3751327762  : public RuntimeObject
{
public:
	// System.String UITextList/Paragraph::text
	String_t* ___text_0;
	// System.String[] UITextList/Paragraph::lines
	StringU5BU5D_t2511808107* ___lines_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(Paragraph_t3751327762, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_lines_1() { return static_cast<int32_t>(offsetof(Paragraph_t3751327762, ___lines_1)); }
	inline StringU5BU5D_t2511808107* get_lines_1() const { return ___lines_1; }
	inline StringU5BU5D_t2511808107** get_address_of_lines_1() { return &___lines_1; }
	inline void set_lines_1(StringU5BU5D_t2511808107* value)
	{
		___lines_1 = value;
		Il2CppCodeGenWriteBarrier((&___lines_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAGRAPH_T3751327762_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T1129784405_H
#define POSTPROCESSINGCOMPONENTBASE_T1129784405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t1129784405  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t1419994223 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t1129784405, ___context_0)); }
	inline PostProcessingContext_t1419994223 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t1419994223 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t1419994223 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T1129784405_H
#ifndef UNIFORMS_T477084279_H
#define UNIFORMS_T477084279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent/Uniforms
struct  Uniforms_t477084279  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t477084279_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_0;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Threshold
	int32_t ____Threshold_1;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Curve
	int32_t ____Curve_2;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_PrefilterOffs
	int32_t ____PrefilterOffs_3;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_SampleScale
	int32_t ____SampleScale_4;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BaseTex
	int32_t ____BaseTex_5;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BloomTex
	int32_t ____BloomTex_6;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_Settings
	int32_t ____Bloom_Settings_7;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtTex
	int32_t ____Bloom_DirtTex_8;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtIntensity
	int32_t ____Bloom_DirtIntensity_9;

public:
	inline static int32_t get_offset_of__AutoExposure_0() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____AutoExposure_0)); }
	inline int32_t get__AutoExposure_0() const { return ____AutoExposure_0; }
	inline int32_t* get_address_of__AutoExposure_0() { return &____AutoExposure_0; }
	inline void set__AutoExposure_0(int32_t value)
	{
		____AutoExposure_0 = value;
	}

	inline static int32_t get_offset_of__Threshold_1() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____Threshold_1)); }
	inline int32_t get__Threshold_1() const { return ____Threshold_1; }
	inline int32_t* get_address_of__Threshold_1() { return &____Threshold_1; }
	inline void set__Threshold_1(int32_t value)
	{
		____Threshold_1 = value;
	}

	inline static int32_t get_offset_of__Curve_2() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____Curve_2)); }
	inline int32_t get__Curve_2() const { return ____Curve_2; }
	inline int32_t* get_address_of__Curve_2() { return &____Curve_2; }
	inline void set__Curve_2(int32_t value)
	{
		____Curve_2 = value;
	}

	inline static int32_t get_offset_of__PrefilterOffs_3() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____PrefilterOffs_3)); }
	inline int32_t get__PrefilterOffs_3() const { return ____PrefilterOffs_3; }
	inline int32_t* get_address_of__PrefilterOffs_3() { return &____PrefilterOffs_3; }
	inline void set__PrefilterOffs_3(int32_t value)
	{
		____PrefilterOffs_3 = value;
	}

	inline static int32_t get_offset_of__SampleScale_4() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____SampleScale_4)); }
	inline int32_t get__SampleScale_4() const { return ____SampleScale_4; }
	inline int32_t* get_address_of__SampleScale_4() { return &____SampleScale_4; }
	inline void set__SampleScale_4(int32_t value)
	{
		____SampleScale_4 = value;
	}

	inline static int32_t get_offset_of__BaseTex_5() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____BaseTex_5)); }
	inline int32_t get__BaseTex_5() const { return ____BaseTex_5; }
	inline int32_t* get_address_of__BaseTex_5() { return &____BaseTex_5; }
	inline void set__BaseTex_5(int32_t value)
	{
		____BaseTex_5 = value;
	}

	inline static int32_t get_offset_of__BloomTex_6() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____BloomTex_6)); }
	inline int32_t get__BloomTex_6() const { return ____BloomTex_6; }
	inline int32_t* get_address_of__BloomTex_6() { return &____BloomTex_6; }
	inline void set__BloomTex_6(int32_t value)
	{
		____BloomTex_6 = value;
	}

	inline static int32_t get_offset_of__Bloom_Settings_7() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____Bloom_Settings_7)); }
	inline int32_t get__Bloom_Settings_7() const { return ____Bloom_Settings_7; }
	inline int32_t* get_address_of__Bloom_Settings_7() { return &____Bloom_Settings_7; }
	inline void set__Bloom_Settings_7(int32_t value)
	{
		____Bloom_Settings_7 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____Bloom_DirtTex_8)); }
	inline int32_t get__Bloom_DirtTex_8() const { return ____Bloom_DirtTex_8; }
	inline int32_t* get_address_of__Bloom_DirtTex_8() { return &____Bloom_DirtTex_8; }
	inline void set__Bloom_DirtTex_8(int32_t value)
	{
		____Bloom_DirtTex_8 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtIntensity_9() { return static_cast<int32_t>(offsetof(Uniforms_t477084279_StaticFields, ____Bloom_DirtIntensity_9)); }
	inline int32_t get__Bloom_DirtIntensity_9() const { return ____Bloom_DirtIntensity_9; }
	inline int32_t* get_address_of__Bloom_DirtIntensity_9() { return &____Bloom_DirtIntensity_9; }
	inline void set__Bloom_DirtIntensity_9(int32_t value)
	{
		____Bloom_DirtIntensity_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T477084279_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef VECTOR4_T2436299922_H
#define VECTOR4_T2436299922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2436299922 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2436299922_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2436299922  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2436299922  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2436299922  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2436299922  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2436299922  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2436299922 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2436299922  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___oneVector_6)); }
	inline Vector4_t2436299922  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2436299922 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2436299922  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2436299922  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2436299922 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2436299922  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2436299922  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2436299922 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2436299922  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2436299922_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef PROPERTYATTRIBUTE_T381499487_H
#define PROPERTYATTRIBUTE_T381499487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t381499487  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T381499487_H
#ifndef KERNING_T647376370_H
#define KERNING_T647376370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMGlyph/Kerning
struct  Kerning_t647376370 
{
public:
	// System.Int32 BMGlyph/Kerning::previousChar
	int32_t ___previousChar_0;
	// System.Int32 BMGlyph/Kerning::amount
	int32_t ___amount_1;

public:
	inline static int32_t get_offset_of_previousChar_0() { return static_cast<int32_t>(offsetof(Kerning_t647376370, ___previousChar_0)); }
	inline int32_t get_previousChar_0() const { return ___previousChar_0; }
	inline int32_t* get_address_of_previousChar_0() { return &___previousChar_0; }
	inline void set_previousChar_0(int32_t value)
	{
		___previousChar_0 = value;
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(Kerning_t647376370, ___amount_1)); }
	inline int32_t get_amount_1() const { return ___amount_1; }
	inline int32_t* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(int32_t value)
	{
		___amount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNING_T647376370_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3830868970_H
#define POSTPROCESSINGCOMPONENT_1_T3830868970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponent_1_t3830868970  : public PostProcessingComponentBase_t1129784405
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AmbientOcclusionModel_t3851579516 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3830868970, ___U3CmodelU3Ek__BackingField_1)); }
	inline AmbientOcclusionModel_t3851579516 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AmbientOcclusionModel_t3851579516 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AmbientOcclusionModel_t3851579516 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3830868970_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3851602214_H
#define POSTPROCESSINGCOMPONENT_1_T3851602214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponent_1_t3851602214  : public PostProcessingComponentBase_t1129784405
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BloomModel_t3872312760 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3851602214, ___U3CmodelU3Ek__BackingField_1)); }
	inline BloomModel_t3872312760 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BloomModel_t3872312760 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BloomModel_t3872312760 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3851602214_H
#ifndef LAYERMASK_T246267875_H
#define LAYERMASK_T246267875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t246267875 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t246267875, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T246267875_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef SYMBOLSTYLE_T3548150665_H
#define SYMBOLSTYLE_T3548150665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFont/SymbolStyle
struct  SymbolStyle_t3548150665 
{
public:
	// System.Int32 UIFont/SymbolStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SymbolStyle_t3548150665, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSTYLE_T3548150665_H
#ifndef STYLE_T2647733567_H
#define STYLE_T2647733567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList/Style
struct  Style_t2647733567 
{
public:
	// System.Int32 UITextList/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t2647733567, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T2647733567_H
#ifndef EFFECT_T450438489_H
#define EFFECT_T450438489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Effect
struct  Effect_t450438489 
{
public:
	// System.Int32 UILabel/Effect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Effect_t450438489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECT_T450438489_H
#ifndef DEBUGINFO_T3987478991_H
#define DEBUGINFO_T3987478991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/DebugInfo
struct  DebugInfo_t3987478991 
{
public:
	// System.Int32 UIPanel/DebugInfo::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugInfo_t3987478991, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFO_T3987478991_H
#ifndef ALIGNMENT_T93203161_H
#define ALIGNMENT_T93203161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFont/Alignment
struct  Alignment_t93203161 
{
public:
	// System.Int32 UIFont/Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t93203161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T93203161_H
#ifndef STYLE_T2353225633_H
#define STYLE_T2353225633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStretch/Style
struct  Style_t2353225633 
{
public:
	// System.Int32 UIStretch/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t2353225633, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T2353225633_H
#ifndef KEYCODE_T1957070546_H
#define KEYCODE_T1957070546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t1957070546 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t1957070546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T1957070546_H
#ifndef OCCLUSIONSOURCE_T1463809940_H
#define OCCLUSIONSOURCE_T1463809940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource
struct  OcclusionSource_t1463809940 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OcclusionSource_t1463809940, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONSOURCE_T1463809940_H
#ifndef MINATTRIBUTE_T1517261931_H
#define MINATTRIBUTE_T1517261931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MinAttribute
struct  MinAttribute_t1517261931  : public PropertyAttribute_t381499487
{
public:
	// System.Single UnityEngine.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t1517261931, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T1517261931_H
#ifndef TRACKBALLATTRIBUTE_T3757329946_H
#define TRACKBALLATTRIBUTE_T3757329946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t3757329946  : public PropertyAttribute_t381499487
{
public:
	// System.String UnityEngine.PostProcessing.TrackballAttribute::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t3757329946, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T3757329946_H
#ifndef TRACKBALLGROUPATTRIBUTE_T2573285421_H
#define TRACKBALLGROUPATTRIBUTE_T2573285421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballGroupAttribute
struct  TrackballGroupAttribute_t2573285421  : public PropertyAttribute_t381499487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLGROUPATTRIBUTE_T2573285421_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2323363883_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2323363883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponentRenderTexture_1_t2323363883  : public PostProcessingComponent_1_t3851602214
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2323363883_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T3427873780_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T3427873780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponentCommandBuffer_1_t3427873780  : public PostProcessingComponent_1_t3830868970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T3427873780_H
#ifndef RAYCASTHIT_T1706347245_H
#define RAYCASTHIT_T1706347245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1706347245 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t1986933152  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t1986933152  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t328513675  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1485601975 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Point_0)); }
	inline Vector3_t1986933152  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t1986933152 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t1986933152  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Normal_1)); }
	inline Vector3_t1986933152  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t1986933152 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t1986933152  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_UV_4)); }
	inline Vector2_t328513675  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t328513675 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t328513675  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1706347245, ___m_Collider_5)); }
	inline Collider_t1485601975 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1485601975 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1485601975 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1706347245_marshaled_pinvoke
{
	Vector3_t1986933152  ___m_Point_0;
	Vector3_t1986933152  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t328513675  ___m_UV_4;
	Collider_t1485601975 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1706347245_marshaled_com
{
	Vector3_t1986933152  ___m_Point_0;
	Vector3_t1986933152  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t328513675  ___m_UV_4;
	Collider_t1485601975 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1706347245_H
#ifndef FILLDIRECTION_T2955200481_H
#define FILLDIRECTION_T2955200481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFilledSprite/FillDirection
struct  FillDirection_t2955200481 
{
public:
	// System.Int32 UIFilledSprite/FillDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillDirection_t2955200481, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLDIRECTION_T2955200481_H
#ifndef GETSETATTRIBUTE_T596627455_H
#define GETSETATTRIBUTE_T596627455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GetSetAttribute
struct  GetSetAttribute_t596627455  : public PropertyAttribute_t381499487
{
public:
	// System.String UnityEngine.PostProcessing.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean UnityEngine.PostProcessing.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t596627455, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t596627455, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T596627455_H
#ifndef DIRECTION_T411333392_H
#define DIRECTION_T411333392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Direction
struct  Direction_t411333392 
{
public:
	// System.Int32 AnimationOrTween.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t411333392, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T411333392_H
#ifndef KEYBOARDTYPE_T2903380147_H
#define KEYBOARDTYPE_T2903380147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/KeyboardType
struct  KeyboardType_t2903380147 
{
public:
	// System.Int32 UIInput/KeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyboardType_t2903380147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDTYPE_T2903380147_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef CLICKNOTIFICATION_T601501802_H
#define CLICKNOTIFICATION_T601501802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ClickNotification
struct  ClickNotification_t601501802 
{
public:
	// System.Int32 UICamera/ClickNotification::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClickNotification_t601501802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKNOTIFICATION_T601501802_H
#ifndef PIVOT_T822355240_H
#define PIVOT_T822355240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/Pivot
struct  Pivot_t822355240 
{
public:
	// System.Int32 UIWidget/Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t822355240, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T822355240_H
#ifndef SIDE_T3253602166_H
#define SIDE_T3253602166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAnchor/Side
struct  Side_t3253602166 
{
public:
	// System.Int32 UIAnchor/Side::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Side_t3253602166, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIDE_T3253602166_H
#ifndef METHOD_T2440318679_H
#define METHOD_T2440318679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Method
struct  Method_t2440318679 
{
public:
	// System.Int32 UITweener/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t2440318679, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T2440318679_H
#ifndef UIGEOMETRY_T76808639_H
#define UIGEOMETRY_T76808639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGeometry
struct  UIGeometry_t76808639  : public RuntimeObject
{
public:
	// BetterList`1<UnityEngine.Vector3> UIGeometry::verts
	BetterList_1_t442302241 * ___verts_0;
	// BetterList`1<UnityEngine.Vector2> UIGeometry::uvs
	BetterList_1_t3078850060 * ___uvs_1;
	// BetterList`1<UnityEngine.Color32> UIGeometry::cols
	BetterList_1_t1883882744 * ___cols_2;
	// BetterList`1<UnityEngine.Vector3> UIGeometry::mRtpVerts
	BetterList_1_t442302241 * ___mRtpVerts_3;
	// UnityEngine.Vector3 UIGeometry::mRtpNormal
	Vector3_t1986933152  ___mRtpNormal_4;
	// UnityEngine.Vector4 UIGeometry::mRtpTan
	Vector4_t2436299922  ___mRtpTan_5;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___verts_0)); }
	inline BetterList_1_t442302241 * get_verts_0() const { return ___verts_0; }
	inline BetterList_1_t442302241 ** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(BetterList_1_t442302241 * value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_uvs_1() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___uvs_1)); }
	inline BetterList_1_t3078850060 * get_uvs_1() const { return ___uvs_1; }
	inline BetterList_1_t3078850060 ** get_address_of_uvs_1() { return &___uvs_1; }
	inline void set_uvs_1(BetterList_1_t3078850060 * value)
	{
		___uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_1), value);
	}

	inline static int32_t get_offset_of_cols_2() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___cols_2)); }
	inline BetterList_1_t1883882744 * get_cols_2() const { return ___cols_2; }
	inline BetterList_1_t1883882744 ** get_address_of_cols_2() { return &___cols_2; }
	inline void set_cols_2(BetterList_1_t1883882744 * value)
	{
		___cols_2 = value;
		Il2CppCodeGenWriteBarrier((&___cols_2), value);
	}

	inline static int32_t get_offset_of_mRtpVerts_3() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___mRtpVerts_3)); }
	inline BetterList_1_t442302241 * get_mRtpVerts_3() const { return ___mRtpVerts_3; }
	inline BetterList_1_t442302241 ** get_address_of_mRtpVerts_3() { return &___mRtpVerts_3; }
	inline void set_mRtpVerts_3(BetterList_1_t442302241 * value)
	{
		___mRtpVerts_3 = value;
		Il2CppCodeGenWriteBarrier((&___mRtpVerts_3), value);
	}

	inline static int32_t get_offset_of_mRtpNormal_4() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___mRtpNormal_4)); }
	inline Vector3_t1986933152  get_mRtpNormal_4() const { return ___mRtpNormal_4; }
	inline Vector3_t1986933152 * get_address_of_mRtpNormal_4() { return &___mRtpNormal_4; }
	inline void set_mRtpNormal_4(Vector3_t1986933152  value)
	{
		___mRtpNormal_4 = value;
	}

	inline static int32_t get_offset_of_mRtpTan_5() { return static_cast<int32_t>(offsetof(UIGeometry_t76808639, ___mRtpTan_5)); }
	inline Vector4_t2436299922  get_mRtpTan_5() const { return ___mRtpTan_5; }
	inline Vector4_t2436299922 * get_address_of_mRtpTan_5() { return &___mRtpTan_5; }
	inline void set_mRtpTan_5(Vector4_t2436299922  value)
	{
		___mRtpTan_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGEOMETRY_T76808639_H
#ifndef UINODE_T884938289_H
#define UINODE_T884938289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UINode
struct  UINode_t884938289  : public RuntimeObject
{
public:
	// System.Int32 UINode::mVisibleFlag
	int32_t ___mVisibleFlag_0;
	// UnityEngine.Transform UINode::trans
	Transform_t362059596 * ___trans_1;
	// UIWidget UINode::widget
	UIWidget_t1961100141 * ___widget_2;
	// System.Boolean UINode::lastActive
	bool ___lastActive_3;
	// UnityEngine.Vector3 UINode::lastPos
	Vector3_t1986933152  ___lastPos_4;
	// UnityEngine.Quaternion UINode::lastRot
	Quaternion_t704191599  ___lastRot_5;
	// UnityEngine.Vector3 UINode::lastScale
	Vector3_t1986933152  ___lastScale_6;
	// System.Int32 UINode::changeFlag
	int32_t ___changeFlag_7;
	// UnityEngine.GameObject UINode::mGo
	GameObject_t2557347079 * ___mGo_8;

public:
	inline static int32_t get_offset_of_mVisibleFlag_0() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___mVisibleFlag_0)); }
	inline int32_t get_mVisibleFlag_0() const { return ___mVisibleFlag_0; }
	inline int32_t* get_address_of_mVisibleFlag_0() { return &___mVisibleFlag_0; }
	inline void set_mVisibleFlag_0(int32_t value)
	{
		___mVisibleFlag_0 = value;
	}

	inline static int32_t get_offset_of_trans_1() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___trans_1)); }
	inline Transform_t362059596 * get_trans_1() const { return ___trans_1; }
	inline Transform_t362059596 ** get_address_of_trans_1() { return &___trans_1; }
	inline void set_trans_1(Transform_t362059596 * value)
	{
		___trans_1 = value;
		Il2CppCodeGenWriteBarrier((&___trans_1), value);
	}

	inline static int32_t get_offset_of_widget_2() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___widget_2)); }
	inline UIWidget_t1961100141 * get_widget_2() const { return ___widget_2; }
	inline UIWidget_t1961100141 ** get_address_of_widget_2() { return &___widget_2; }
	inline void set_widget_2(UIWidget_t1961100141 * value)
	{
		___widget_2 = value;
		Il2CppCodeGenWriteBarrier((&___widget_2), value);
	}

	inline static int32_t get_offset_of_lastActive_3() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___lastActive_3)); }
	inline bool get_lastActive_3() const { return ___lastActive_3; }
	inline bool* get_address_of_lastActive_3() { return &___lastActive_3; }
	inline void set_lastActive_3(bool value)
	{
		___lastActive_3 = value;
	}

	inline static int32_t get_offset_of_lastPos_4() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___lastPos_4)); }
	inline Vector3_t1986933152  get_lastPos_4() const { return ___lastPos_4; }
	inline Vector3_t1986933152 * get_address_of_lastPos_4() { return &___lastPos_4; }
	inline void set_lastPos_4(Vector3_t1986933152  value)
	{
		___lastPos_4 = value;
	}

	inline static int32_t get_offset_of_lastRot_5() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___lastRot_5)); }
	inline Quaternion_t704191599  get_lastRot_5() const { return ___lastRot_5; }
	inline Quaternion_t704191599 * get_address_of_lastRot_5() { return &___lastRot_5; }
	inline void set_lastRot_5(Quaternion_t704191599  value)
	{
		___lastRot_5 = value;
	}

	inline static int32_t get_offset_of_lastScale_6() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___lastScale_6)); }
	inline Vector3_t1986933152  get_lastScale_6() const { return ___lastScale_6; }
	inline Vector3_t1986933152 * get_address_of_lastScale_6() { return &___lastScale_6; }
	inline void set_lastScale_6(Vector3_t1986933152  value)
	{
		___lastScale_6 = value;
	}

	inline static int32_t get_offset_of_changeFlag_7() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___changeFlag_7)); }
	inline int32_t get_changeFlag_7() const { return ___changeFlag_7; }
	inline int32_t* get_address_of_changeFlag_7() { return &___changeFlag_7; }
	inline void set_changeFlag_7(int32_t value)
	{
		___changeFlag_7 = value;
	}

	inline static int32_t get_offset_of_mGo_8() { return static_cast<int32_t>(offsetof(UINode_t884938289, ___mGo_8)); }
	inline GameObject_t2557347079 * get_mGo_8() const { return ___mGo_8; }
	inline GameObject_t2557347079 ** get_address_of_mGo_8() { return &___mGo_8; }
	inline void set_mGo_8(GameObject_t2557347079 * value)
	{
		___mGo_8 = value;
		Il2CppCodeGenWriteBarrier((&___mGo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINODE_T884938289_H
#ifndef SPRITE_T3039180268_H
#define SPRITE_T3039180268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas/Sprite
struct  Sprite_t3039180268  : public RuntimeObject
{
public:
	// System.String UIAtlas/Sprite::name
	String_t* ___name_0;
	// UnityEngine.Rect UIAtlas/Sprite::outer
	Rect_t3039462994  ___outer_1;
	// UnityEngine.Rect UIAtlas/Sprite::inner
	Rect_t3039462994  ___inner_2;
	// System.Single UIAtlas/Sprite::paddingLeft
	float ___paddingLeft_3;
	// System.Single UIAtlas/Sprite::paddingRight
	float ___paddingRight_4;
	// System.Single UIAtlas/Sprite::paddingTop
	float ___paddingTop_5;
	// System.Single UIAtlas/Sprite::paddingBottom
	float ___paddingBottom_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_outer_1() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___outer_1)); }
	inline Rect_t3039462994  get_outer_1() const { return ___outer_1; }
	inline Rect_t3039462994 * get_address_of_outer_1() { return &___outer_1; }
	inline void set_outer_1(Rect_t3039462994  value)
	{
		___outer_1 = value;
	}

	inline static int32_t get_offset_of_inner_2() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___inner_2)); }
	inline Rect_t3039462994  get_inner_2() const { return ___inner_2; }
	inline Rect_t3039462994 * get_address_of_inner_2() { return &___inner_2; }
	inline void set_inner_2(Rect_t3039462994  value)
	{
		___inner_2 = value;
	}

	inline static int32_t get_offset_of_paddingLeft_3() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___paddingLeft_3)); }
	inline float get_paddingLeft_3() const { return ___paddingLeft_3; }
	inline float* get_address_of_paddingLeft_3() { return &___paddingLeft_3; }
	inline void set_paddingLeft_3(float value)
	{
		___paddingLeft_3 = value;
	}

	inline static int32_t get_offset_of_paddingRight_4() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___paddingRight_4)); }
	inline float get_paddingRight_4() const { return ___paddingRight_4; }
	inline float* get_address_of_paddingRight_4() { return &___paddingRight_4; }
	inline void set_paddingRight_4(float value)
	{
		___paddingRight_4 = value;
	}

	inline static int32_t get_offset_of_paddingTop_5() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___paddingTop_5)); }
	inline float get_paddingTop_5() const { return ___paddingTop_5; }
	inline float* get_address_of_paddingTop_5() { return &___paddingTop_5; }
	inline void set_paddingTop_5(float value)
	{
		___paddingTop_5 = value;
	}

	inline static int32_t get_offset_of_paddingBottom_6() { return static_cast<int32_t>(offsetof(Sprite_t3039180268, ___paddingBottom_6)); }
	inline float get_paddingBottom_6() const { return ___paddingBottom_6; }
	inline float* get_address_of_paddingBottom_6() { return &___paddingBottom_6; }
	inline void set_paddingBottom_6(float value)
	{
		___paddingBottom_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T3039180268_H
#ifndef DISABLECONDITION_T4056368895_H
#define DISABLECONDITION_T4056368895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.DisableCondition
struct  DisableCondition_t4056368895 
{
public:
	// System.Int32 AnimationOrTween.DisableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisableCondition_t4056368895, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONDITION_T4056368895_H
#ifndef COORDINATES_T3113092471_H
#define COORDINATES_T3113092471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas/Coordinates
struct  Coordinates_t3113092471 
{
public:
	// System.Int32 UIAtlas/Coordinates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Coordinates_t3113092471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATES_T3113092471_H
#ifndef ENABLECONDITION_T266497438_H
#define ENABLECONDITION_T266497438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.EnableCondition
struct  EnableCondition_t266497438 
{
public:
	// System.Int32 AnimationOrTween.EnableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnableCondition_t266497438, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECONDITION_T266497438_H
#ifndef CLIPPING_T1987716881_H
#define CLIPPING_T1987716881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/Clipping
struct  Clipping_t1987716881 
{
public:
	// System.Int32 UIDrawCall/Clipping::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Clipping_t1987716881, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_T1987716881_H
#ifndef STYLE_T187796108_H
#define STYLE_T187796108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Style
struct  Style_t187796108 
{
public:
	// System.Int32 UITweener/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t187796108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T187796108_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef BLOOMCOMPONENT_T3971738366_H
#define BLOOMCOMPONENT_T3971738366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent
struct  BloomComponent_t3971738366  : public PostProcessingComponentRenderTexture_1_t2323363883
{
public:
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer1
	RenderTextureU5BU5D_t1249652403* ___m_BlurBuffer1_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer2
	RenderTextureU5BU5D_t1249652403* ___m_BlurBuffer2_4;

public:
	inline static int32_t get_offset_of_m_BlurBuffer1_3() { return static_cast<int32_t>(offsetof(BloomComponent_t3971738366, ___m_BlurBuffer1_3)); }
	inline RenderTextureU5BU5D_t1249652403* get_m_BlurBuffer1_3() const { return ___m_BlurBuffer1_3; }
	inline RenderTextureU5BU5D_t1249652403** get_address_of_m_BlurBuffer1_3() { return &___m_BlurBuffer1_3; }
	inline void set_m_BlurBuffer1_3(RenderTextureU5BU5D_t1249652403* value)
	{
		___m_BlurBuffer1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer1_3), value);
	}

	inline static int32_t get_offset_of_m_BlurBuffer2_4() { return static_cast<int32_t>(offsetof(BloomComponent_t3971738366, ___m_BlurBuffer2_4)); }
	inline RenderTextureU5BU5D_t1249652403* get_m_BlurBuffer2_4() const { return ___m_BlurBuffer2_4; }
	inline RenderTextureU5BU5D_t1249652403** get_address_of_m_BlurBuffer2_4() { return &___m_BlurBuffer2_4; }
	inline void set_m_BlurBuffer2_4(RenderTextureU5BU5D_t1249652403* value)
	{
		___m_BlurBuffer2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMCOMPONENT_T3971738366_H
#ifndef AMBIENTOCCLUSIONCOMPONENT_T2853146608_H
#define AMBIENTOCCLUSIONCOMPONENT_T2853146608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct  AmbientOcclusionComponent_t2853146608  : public PostProcessingComponentCommandBuffer_1_t3427873780
{
public:
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.AmbientOcclusionComponent::m_MRT
	RenderTargetIdentifierU5BU5D_t4170205938* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(AmbientOcclusionComponent_t2853146608, ___m_MRT_4)); }
	inline RenderTargetIdentifierU5BU5D_t4170205938* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderTargetIdentifierU5BU5D_t4170205938** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderTargetIdentifierU5BU5D_t4170205938* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONCOMPONENT_T2853146608_H
#ifndef MOUSEORTOUCH_T115752944_H
#define MOUSEORTOUCH_T115752944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/MouseOrTouch
struct  MouseOrTouch_t115752944  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 UICamera/MouseOrTouch::pos
	Vector2_t328513675  ___pos_0;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::delta
	Vector2_t328513675  ___delta_1;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::totalDelta
	Vector2_t328513675  ___totalDelta_2;
	// UnityEngine.Camera UICamera/MouseOrTouch::pressedCam
	Camera_t2839736942 * ___pressedCam_3;
	// UnityEngine.GameObject UICamera/MouseOrTouch::current
	GameObject_t2557347079 * ___current_4;
	// UnityEngine.GameObject UICamera/MouseOrTouch::pressed
	GameObject_t2557347079 * ___pressed_5;
	// System.Single UICamera/MouseOrTouch::clickTime
	float ___clickTime_6;
	// UICamera/ClickNotification UICamera/MouseOrTouch::clickNotification
	int32_t ___clickNotification_7;
	// System.Boolean UICamera/MouseOrTouch::touchBegan
	bool ___touchBegan_8;
	// System.Boolean UICamera/MouseOrTouch::dragStarted
	bool ___dragStarted_9;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___pos_0)); }
	inline Vector2_t328513675  get_pos_0() const { return ___pos_0; }
	inline Vector2_t328513675 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector2_t328513675  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_delta_1() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___delta_1)); }
	inline Vector2_t328513675  get_delta_1() const { return ___delta_1; }
	inline Vector2_t328513675 * get_address_of_delta_1() { return &___delta_1; }
	inline void set_delta_1(Vector2_t328513675  value)
	{
		___delta_1 = value;
	}

	inline static int32_t get_offset_of_totalDelta_2() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___totalDelta_2)); }
	inline Vector2_t328513675  get_totalDelta_2() const { return ___totalDelta_2; }
	inline Vector2_t328513675 * get_address_of_totalDelta_2() { return &___totalDelta_2; }
	inline void set_totalDelta_2(Vector2_t328513675  value)
	{
		___totalDelta_2 = value;
	}

	inline static int32_t get_offset_of_pressedCam_3() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___pressedCam_3)); }
	inline Camera_t2839736942 * get_pressedCam_3() const { return ___pressedCam_3; }
	inline Camera_t2839736942 ** get_address_of_pressedCam_3() { return &___pressedCam_3; }
	inline void set_pressedCam_3(Camera_t2839736942 * value)
	{
		___pressedCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___pressedCam_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___current_4)); }
	inline GameObject_t2557347079 * get_current_4() const { return ___current_4; }
	inline GameObject_t2557347079 ** get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(GameObject_t2557347079 * value)
	{
		___current_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_4), value);
	}

	inline static int32_t get_offset_of_pressed_5() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___pressed_5)); }
	inline GameObject_t2557347079 * get_pressed_5() const { return ___pressed_5; }
	inline GameObject_t2557347079 ** get_address_of_pressed_5() { return &___pressed_5; }
	inline void set_pressed_5(GameObject_t2557347079 * value)
	{
		___pressed_5 = value;
		Il2CppCodeGenWriteBarrier((&___pressed_5), value);
	}

	inline static int32_t get_offset_of_clickTime_6() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___clickTime_6)); }
	inline float get_clickTime_6() const { return ___clickTime_6; }
	inline float* get_address_of_clickTime_6() { return &___clickTime_6; }
	inline void set_clickTime_6(float value)
	{
		___clickTime_6 = value;
	}

	inline static int32_t get_offset_of_clickNotification_7() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___clickNotification_7)); }
	inline int32_t get_clickNotification_7() const { return ___clickNotification_7; }
	inline int32_t* get_address_of_clickNotification_7() { return &___clickNotification_7; }
	inline void set_clickNotification_7(int32_t value)
	{
		___clickNotification_7 = value;
	}

	inline static int32_t get_offset_of_touchBegan_8() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___touchBegan_8)); }
	inline bool get_touchBegan_8() const { return ___touchBegan_8; }
	inline bool* get_address_of_touchBegan_8() { return &___touchBegan_8; }
	inline void set_touchBegan_8(bool value)
	{
		___touchBegan_8 = value;
	}

	inline static int32_t get_offset_of_dragStarted_9() { return static_cast<int32_t>(offsetof(MouseOrTouch_t115752944, ___dragStarted_9)); }
	inline bool get_dragStarted_9() const { return ___dragStarted_9; }
	inline bool* get_address_of_dragStarted_9() { return &___dragStarted_9; }
	inline void set_dragStarted_9(bool value)
	{
		___dragStarted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORTOUCH_T115752944_H
#ifndef FLOATDELEGATE_T284650127_H
#define FLOATDELEGATE_T284650127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/FloatDelegate
struct  FloatDelegate_t284650127  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATDELEGATE_T284650127_H
#ifndef ONUPDATE_T172587279_H
#define ONUPDATE_T172587279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager/OnUpdate
struct  OnUpdate_t172587279  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPDATE_T172587279_H
#ifndef ONSUBMIT_T2511512930_H
#define ONSUBMIT_T2511512930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/OnSubmit
struct  OnSubmit_t2511512930  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSUBMIT_T2511512930_H
#ifndef VALIDATOR_T1053183372_H
#define VALIDATOR_T1053183372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/Validator
struct  Validator_t1053183372  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATOR_T1053183372_H
#ifndef KEYCODEDELEGATE_T1109435781_H
#define KEYCODEDELEGATE_T1109435781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/KeyCodeDelegate
struct  KeyCodeDelegate_t1109435781  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEDELEGATE_T1109435781_H
#ifndef OBJECTDELEGATE_T2232498032_H
#define OBJECTDELEGATE_T2232498032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/ObjectDelegate
struct  ObjectDelegate_t2232498032  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDELEGATE_T2232498032_H
#ifndef STRINGDELEGATE_T3231667884_H
#define STRINGDELEGATE_T3231667884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/StringDelegate
struct  StringDelegate_t3231667884  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGDELEGATE_T3231667884_H
#ifndef PICKATHINGCALLBACK_T4122863115_H
#define PICKATHINGCALLBACK_T4122863115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickAThingDialog/PickAThingCallback
struct  PickAThingCallback_t4122863115  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKATHINGCALLBACK_T4122863115_H
#ifndef VECTORDELEGATE_T1991147574_H
#define VECTORDELEGATE_T1991147574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/VectorDelegate
struct  VectorDelegate_t1991147574  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORDELEGATE_T1991147574_H
#ifndef ONFINISHED_T2354188770_H
#define ONFINISHED_T2354188770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/OnFinished
struct  OnFinished_t2354188770  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T2354188770_H
#ifndef BOOLDELEGATE_T192583560_H
#define BOOLDELEGATE_T192583560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/BoolDelegate
struct  BoolDelegate_t192583560  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLDELEGATE_T192583560_H
#ifndef ONFINISHED_T3829816540_H
#define ONFINISHED_T3829816540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPosition/OnFinished
struct  OnFinished_t3829816540  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T3829816540_H
#ifndef ONFINISHED_T2500445478_H
#define ONFINISHED_T2500445478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPanel/OnFinished
struct  OnFinished_t2500445478  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T2500445478_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef VOIDDELEGATE_T817628917_H
#define VOIDDELEGATE_T817628917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/VoidDelegate
struct  VoidDelegate_t817628917  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOIDDELEGATE_T817628917_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef UIEVENTLISTENER_T1748975512_H
#define UIEVENTLISTENER_T1748975512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener
struct  UIEventListener_t1748975512  : public MonoBehaviour_t1618594486
{
public:
	// System.Object UIEventListener::parameter
	RuntimeObject * ___parameter_2;
	// UIEventListener/VoidDelegate UIEventListener::onSubmit
	VoidDelegate_t817628917 * ___onSubmit_3;
	// UIEventListener/VoidDelegate UIEventListener::onClick
	VoidDelegate_t817628917 * ___onClick_4;
	// UIEventListener/VoidDelegate UIEventListener::onDoubleClick
	VoidDelegate_t817628917 * ___onDoubleClick_5;
	// UIEventListener/BoolDelegate UIEventListener::onHover
	BoolDelegate_t192583560 * ___onHover_6;
	// UIEventListener/BoolDelegate UIEventListener::onPress
	BoolDelegate_t192583560 * ___onPress_7;
	// UIEventListener/BoolDelegate UIEventListener::onSelect
	BoolDelegate_t192583560 * ___onSelect_8;
	// UIEventListener/FloatDelegate UIEventListener::onScroll
	FloatDelegate_t284650127 * ___onScroll_9;
	// UIEventListener/VectorDelegate UIEventListener::onDrag
	VectorDelegate_t1991147574 * ___onDrag_10;
	// UIEventListener/ObjectDelegate UIEventListener::onDrop
	ObjectDelegate_t2232498032 * ___onDrop_11;
	// UIEventListener/StringDelegate UIEventListener::onInput
	StringDelegate_t3231667884 * ___onInput_12;
	// UIEventListener/KeyCodeDelegate UIEventListener::onKey
	KeyCodeDelegate_t1109435781 * ___onKey_13;

public:
	inline static int32_t get_offset_of_parameter_2() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___parameter_2)); }
	inline RuntimeObject * get_parameter_2() const { return ___parameter_2; }
	inline RuntimeObject ** get_address_of_parameter_2() { return &___parameter_2; }
	inline void set_parameter_2(RuntimeObject * value)
	{
		___parameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_2), value);
	}

	inline static int32_t get_offset_of_onSubmit_3() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onSubmit_3)); }
	inline VoidDelegate_t817628917 * get_onSubmit_3() const { return ___onSubmit_3; }
	inline VoidDelegate_t817628917 ** get_address_of_onSubmit_3() { return &___onSubmit_3; }
	inline void set_onSubmit_3(VoidDelegate_t817628917 * value)
	{
		___onSubmit_3 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_3), value);
	}

	inline static int32_t get_offset_of_onClick_4() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onClick_4)); }
	inline VoidDelegate_t817628917 * get_onClick_4() const { return ___onClick_4; }
	inline VoidDelegate_t817628917 ** get_address_of_onClick_4() { return &___onClick_4; }
	inline void set_onClick_4(VoidDelegate_t817628917 * value)
	{
		___onClick_4 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_4), value);
	}

	inline static int32_t get_offset_of_onDoubleClick_5() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onDoubleClick_5)); }
	inline VoidDelegate_t817628917 * get_onDoubleClick_5() const { return ___onDoubleClick_5; }
	inline VoidDelegate_t817628917 ** get_address_of_onDoubleClick_5() { return &___onDoubleClick_5; }
	inline void set_onDoubleClick_5(VoidDelegate_t817628917 * value)
	{
		___onDoubleClick_5 = value;
		Il2CppCodeGenWriteBarrier((&___onDoubleClick_5), value);
	}

	inline static int32_t get_offset_of_onHover_6() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onHover_6)); }
	inline BoolDelegate_t192583560 * get_onHover_6() const { return ___onHover_6; }
	inline BoolDelegate_t192583560 ** get_address_of_onHover_6() { return &___onHover_6; }
	inline void set_onHover_6(BoolDelegate_t192583560 * value)
	{
		___onHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___onHover_6), value);
	}

	inline static int32_t get_offset_of_onPress_7() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onPress_7)); }
	inline BoolDelegate_t192583560 * get_onPress_7() const { return ___onPress_7; }
	inline BoolDelegate_t192583560 ** get_address_of_onPress_7() { return &___onPress_7; }
	inline void set_onPress_7(BoolDelegate_t192583560 * value)
	{
		___onPress_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_7), value);
	}

	inline static int32_t get_offset_of_onSelect_8() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onSelect_8)); }
	inline BoolDelegate_t192583560 * get_onSelect_8() const { return ___onSelect_8; }
	inline BoolDelegate_t192583560 ** get_address_of_onSelect_8() { return &___onSelect_8; }
	inline void set_onSelect_8(BoolDelegate_t192583560 * value)
	{
		___onSelect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_8), value);
	}

	inline static int32_t get_offset_of_onScroll_9() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onScroll_9)); }
	inline FloatDelegate_t284650127 * get_onScroll_9() const { return ___onScroll_9; }
	inline FloatDelegate_t284650127 ** get_address_of_onScroll_9() { return &___onScroll_9; }
	inline void set_onScroll_9(FloatDelegate_t284650127 * value)
	{
		___onScroll_9 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_9), value);
	}

	inline static int32_t get_offset_of_onDrag_10() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onDrag_10)); }
	inline VectorDelegate_t1991147574 * get_onDrag_10() const { return ___onDrag_10; }
	inline VectorDelegate_t1991147574 ** get_address_of_onDrag_10() { return &___onDrag_10; }
	inline void set_onDrag_10(VectorDelegate_t1991147574 * value)
	{
		___onDrag_10 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_10), value);
	}

	inline static int32_t get_offset_of_onDrop_11() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onDrop_11)); }
	inline ObjectDelegate_t2232498032 * get_onDrop_11() const { return ___onDrop_11; }
	inline ObjectDelegate_t2232498032 ** get_address_of_onDrop_11() { return &___onDrop_11; }
	inline void set_onDrop_11(ObjectDelegate_t2232498032 * value)
	{
		___onDrop_11 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_11), value);
	}

	inline static int32_t get_offset_of_onInput_12() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onInput_12)); }
	inline StringDelegate_t3231667884 * get_onInput_12() const { return ___onInput_12; }
	inline StringDelegate_t3231667884 ** get_address_of_onInput_12() { return &___onInput_12; }
	inline void set_onInput_12(StringDelegate_t3231667884 * value)
	{
		___onInput_12 = value;
		Il2CppCodeGenWriteBarrier((&___onInput_12), value);
	}

	inline static int32_t get_offset_of_onKey_13() { return static_cast<int32_t>(offsetof(UIEventListener_t1748975512, ___onKey_13)); }
	inline KeyCodeDelegate_t1109435781 * get_onKey_13() const { return ___onKey_13; }
	inline KeyCodeDelegate_t1109435781 ** get_address_of_onKey_13() { return &___onKey_13; }
	inline void set_onKey_13(KeyCodeDelegate_t1109435781 * value)
	{
		___onKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___onKey_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTLISTENER_T1748975512_H
#ifndef UIATLAS_T1815452364_H
#define UIATLAS_T1815452364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas
struct  UIAtlas_t1815452364  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Material UIAtlas::material
	Material_t2815264910 * ___material_2;
	// System.Collections.Generic.List`1<UIAtlas/Sprite> UIAtlas::sprites
	List_1_t2158807275 * ___sprites_3;
	// UIAtlas/Coordinates UIAtlas::mCoordinates
	int32_t ___mCoordinates_4;
	// System.Single UIAtlas::mPixelSize
	float ___mPixelSize_5;
	// UIAtlas UIAtlas::mReplacement
	UIAtlas_t1815452364 * ___mReplacement_6;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(UIAtlas_t1815452364, ___material_2)); }
	inline Material_t2815264910 * get_material_2() const { return ___material_2; }
	inline Material_t2815264910 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t2815264910 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_sprites_3() { return static_cast<int32_t>(offsetof(UIAtlas_t1815452364, ___sprites_3)); }
	inline List_1_t2158807275 * get_sprites_3() const { return ___sprites_3; }
	inline List_1_t2158807275 ** get_address_of_sprites_3() { return &___sprites_3; }
	inline void set_sprites_3(List_1_t2158807275 * value)
	{
		___sprites_3 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_3), value);
	}

	inline static int32_t get_offset_of_mCoordinates_4() { return static_cast<int32_t>(offsetof(UIAtlas_t1815452364, ___mCoordinates_4)); }
	inline int32_t get_mCoordinates_4() const { return ___mCoordinates_4; }
	inline int32_t* get_address_of_mCoordinates_4() { return &___mCoordinates_4; }
	inline void set_mCoordinates_4(int32_t value)
	{
		___mCoordinates_4 = value;
	}

	inline static int32_t get_offset_of_mPixelSize_5() { return static_cast<int32_t>(offsetof(UIAtlas_t1815452364, ___mPixelSize_5)); }
	inline float get_mPixelSize_5() const { return ___mPixelSize_5; }
	inline float* get_address_of_mPixelSize_5() { return &___mPixelSize_5; }
	inline void set_mPixelSize_5(float value)
	{
		___mPixelSize_5 = value;
	}

	inline static int32_t get_offset_of_mReplacement_6() { return static_cast<int32_t>(offsetof(UIAtlas_t1815452364, ___mReplacement_6)); }
	inline UIAtlas_t1815452364 * get_mReplacement_6() const { return ___mReplacement_6; }
	inline UIAtlas_t1815452364 ** get_address_of_mReplacement_6() { return &___mReplacement_6; }
	inline void set_mReplacement_6(UIAtlas_t1815452364 * value)
	{
		___mReplacement_6 = value;
		Il2CppCodeGenWriteBarrier((&___mReplacement_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIATLAS_T1815452364_H
#ifndef UIDRAWCALL_T2687921839_H
#define UIDRAWCALL_T2687921839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall
struct  UIDrawCall_t2687921839  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform UIDrawCall::mTrans
	Transform_t362059596 * ___mTrans_2;
	// UnityEngine.Material UIDrawCall::mSharedMat
	Material_t2815264910 * ___mSharedMat_3;
	// UnityEngine.Mesh UIDrawCall::mMesh0
	Mesh_t4030024733 * ___mMesh0_4;
	// UnityEngine.Mesh UIDrawCall::mMesh1
	Mesh_t4030024733 * ___mMesh1_5;
	// UnityEngine.MeshFilter UIDrawCall::mFilter
	MeshFilter_t2059166712 * ___mFilter_6;
	// UnityEngine.MeshRenderer UIDrawCall::mRen
	MeshRenderer_t2851551746 * ___mRen_7;
	// UIDrawCall/Clipping UIDrawCall::mClipping
	int32_t ___mClipping_8;
	// UnityEngine.Vector4 UIDrawCall::mClipRange
	Vector4_t2436299922  ___mClipRange_9;
	// UnityEngine.Vector2 UIDrawCall::mClipSoft
	Vector2_t328513675  ___mClipSoft_10;
	// UnityEngine.Material UIDrawCall::mClippedMat
	Material_t2815264910 * ___mClippedMat_11;
	// UnityEngine.Material UIDrawCall::mDepthMat
	Material_t2815264910 * ___mDepthMat_12;
	// System.Int32[] UIDrawCall::mIndices
	Int32U5BU5D_t1965588061* ___mIndices_13;
	// System.Boolean UIDrawCall::mDepthPass
	bool ___mDepthPass_14;
	// System.Boolean UIDrawCall::mReset
	bool ___mReset_15;
	// System.Boolean UIDrawCall::mEven
	bool ___mEven_16;

public:
	inline static int32_t get_offset_of_mTrans_2() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mTrans_2)); }
	inline Transform_t362059596 * get_mTrans_2() const { return ___mTrans_2; }
	inline Transform_t362059596 ** get_address_of_mTrans_2() { return &___mTrans_2; }
	inline void set_mTrans_2(Transform_t362059596 * value)
	{
		___mTrans_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_2), value);
	}

	inline static int32_t get_offset_of_mSharedMat_3() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mSharedMat_3)); }
	inline Material_t2815264910 * get_mSharedMat_3() const { return ___mSharedMat_3; }
	inline Material_t2815264910 ** get_address_of_mSharedMat_3() { return &___mSharedMat_3; }
	inline void set_mSharedMat_3(Material_t2815264910 * value)
	{
		___mSharedMat_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSharedMat_3), value);
	}

	inline static int32_t get_offset_of_mMesh0_4() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mMesh0_4)); }
	inline Mesh_t4030024733 * get_mMesh0_4() const { return ___mMesh0_4; }
	inline Mesh_t4030024733 ** get_address_of_mMesh0_4() { return &___mMesh0_4; }
	inline void set_mMesh0_4(Mesh_t4030024733 * value)
	{
		___mMesh0_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh0_4), value);
	}

	inline static int32_t get_offset_of_mMesh1_5() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mMesh1_5)); }
	inline Mesh_t4030024733 * get_mMesh1_5() const { return ___mMesh1_5; }
	inline Mesh_t4030024733 ** get_address_of_mMesh1_5() { return &___mMesh1_5; }
	inline void set_mMesh1_5(Mesh_t4030024733 * value)
	{
		___mMesh1_5 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh1_5), value);
	}

	inline static int32_t get_offset_of_mFilter_6() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mFilter_6)); }
	inline MeshFilter_t2059166712 * get_mFilter_6() const { return ___mFilter_6; }
	inline MeshFilter_t2059166712 ** get_address_of_mFilter_6() { return &___mFilter_6; }
	inline void set_mFilter_6(MeshFilter_t2059166712 * value)
	{
		___mFilter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mFilter_6), value);
	}

	inline static int32_t get_offset_of_mRen_7() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mRen_7)); }
	inline MeshRenderer_t2851551746 * get_mRen_7() const { return ___mRen_7; }
	inline MeshRenderer_t2851551746 ** get_address_of_mRen_7() { return &___mRen_7; }
	inline void set_mRen_7(MeshRenderer_t2851551746 * value)
	{
		___mRen_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRen_7), value);
	}

	inline static int32_t get_offset_of_mClipping_8() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mClipping_8)); }
	inline int32_t get_mClipping_8() const { return ___mClipping_8; }
	inline int32_t* get_address_of_mClipping_8() { return &___mClipping_8; }
	inline void set_mClipping_8(int32_t value)
	{
		___mClipping_8 = value;
	}

	inline static int32_t get_offset_of_mClipRange_9() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mClipRange_9)); }
	inline Vector4_t2436299922  get_mClipRange_9() const { return ___mClipRange_9; }
	inline Vector4_t2436299922 * get_address_of_mClipRange_9() { return &___mClipRange_9; }
	inline void set_mClipRange_9(Vector4_t2436299922  value)
	{
		___mClipRange_9 = value;
	}

	inline static int32_t get_offset_of_mClipSoft_10() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mClipSoft_10)); }
	inline Vector2_t328513675  get_mClipSoft_10() const { return ___mClipSoft_10; }
	inline Vector2_t328513675 * get_address_of_mClipSoft_10() { return &___mClipSoft_10; }
	inline void set_mClipSoft_10(Vector2_t328513675  value)
	{
		___mClipSoft_10 = value;
	}

	inline static int32_t get_offset_of_mClippedMat_11() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mClippedMat_11)); }
	inline Material_t2815264910 * get_mClippedMat_11() const { return ___mClippedMat_11; }
	inline Material_t2815264910 ** get_address_of_mClippedMat_11() { return &___mClippedMat_11; }
	inline void set_mClippedMat_11(Material_t2815264910 * value)
	{
		___mClippedMat_11 = value;
		Il2CppCodeGenWriteBarrier((&___mClippedMat_11), value);
	}

	inline static int32_t get_offset_of_mDepthMat_12() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mDepthMat_12)); }
	inline Material_t2815264910 * get_mDepthMat_12() const { return ___mDepthMat_12; }
	inline Material_t2815264910 ** get_address_of_mDepthMat_12() { return &___mDepthMat_12; }
	inline void set_mDepthMat_12(Material_t2815264910 * value)
	{
		___mDepthMat_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDepthMat_12), value);
	}

	inline static int32_t get_offset_of_mIndices_13() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mIndices_13)); }
	inline Int32U5BU5D_t1965588061* get_mIndices_13() const { return ___mIndices_13; }
	inline Int32U5BU5D_t1965588061** get_address_of_mIndices_13() { return &___mIndices_13; }
	inline void set_mIndices_13(Int32U5BU5D_t1965588061* value)
	{
		___mIndices_13 = value;
		Il2CppCodeGenWriteBarrier((&___mIndices_13), value);
	}

	inline static int32_t get_offset_of_mDepthPass_14() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mDepthPass_14)); }
	inline bool get_mDepthPass_14() const { return ___mDepthPass_14; }
	inline bool* get_address_of_mDepthPass_14() { return &___mDepthPass_14; }
	inline void set_mDepthPass_14(bool value)
	{
		___mDepthPass_14 = value;
	}

	inline static int32_t get_offset_of_mReset_15() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mReset_15)); }
	inline bool get_mReset_15() const { return ___mReset_15; }
	inline bool* get_address_of_mReset_15() { return &___mReset_15; }
	inline void set_mReset_15(bool value)
	{
		___mReset_15 = value;
	}

	inline static int32_t get_offset_of_mEven_16() { return static_cast<int32_t>(offsetof(UIDrawCall_t2687921839, ___mEven_16)); }
	inline bool get_mEven_16() const { return ___mEven_16; }
	inline bool* get_address_of_mEven_16() { return &___mEven_16; }
	inline void set_mEven_16(bool value)
	{
		___mEven_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWCALL_T2687921839_H
#ifndef NGUIDEBUG_T3415807361_H
#define NGUIDEBUG_T3415807361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIDebug
struct  NGUIDebug_t3415807361  : public MonoBehaviour_t1618594486
{
public:

public:
};

struct NGUIDebug_t3415807361_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> NGUIDebug::mLines
	List_1_t4069179741 * ___mLines_2;
	// NGUIDebug NGUIDebug::mInstance
	NGUIDebug_t3415807361 * ___mInstance_3;

public:
	inline static int32_t get_offset_of_mLines_2() { return static_cast<int32_t>(offsetof(NGUIDebug_t3415807361_StaticFields, ___mLines_2)); }
	inline List_1_t4069179741 * get_mLines_2() const { return ___mLines_2; }
	inline List_1_t4069179741 ** get_address_of_mLines_2() { return &___mLines_2; }
	inline void set_mLines_2(List_1_t4069179741 * value)
	{
		___mLines_2 = value;
		Il2CppCodeGenWriteBarrier((&___mLines_2), value);
	}

	inline static int32_t get_offset_of_mInstance_3() { return static_cast<int32_t>(offsetof(NGUIDebug_t3415807361_StaticFields, ___mInstance_3)); }
	inline NGUIDebug_t3415807361 * get_mInstance_3() const { return ___mInstance_3; }
	inline NGUIDebug_t3415807361 ** get_address_of_mInstance_3() { return &___mInstance_3; }
	inline void set_mInstance_3(NGUIDebug_t3415807361 * value)
	{
		___mInstance_3 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUIDEBUG_T3415807361_H
#ifndef LOCALIZATION_T1621195128_H
#define LOCALIZATION_T1621195128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization
struct  Localization_t1621195128  : public MonoBehaviour_t1618594486
{
public:
	// System.String Localization::startingLanguage
	String_t* ___startingLanguage_3;
	// UnityEngine.TextAsset[] Localization::languages
	TextAssetU5BU5D_t2688711784* ___languages_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Localization::mDictionary
	Dictionary_2_t1060102516 * ___mDictionary_5;
	// System.String Localization::mLanguage
	String_t* ___mLanguage_6;

public:
	inline static int32_t get_offset_of_startingLanguage_3() { return static_cast<int32_t>(offsetof(Localization_t1621195128, ___startingLanguage_3)); }
	inline String_t* get_startingLanguage_3() const { return ___startingLanguage_3; }
	inline String_t** get_address_of_startingLanguage_3() { return &___startingLanguage_3; }
	inline void set_startingLanguage_3(String_t* value)
	{
		___startingLanguage_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingLanguage_3), value);
	}

	inline static int32_t get_offset_of_languages_4() { return static_cast<int32_t>(offsetof(Localization_t1621195128, ___languages_4)); }
	inline TextAssetU5BU5D_t2688711784* get_languages_4() const { return ___languages_4; }
	inline TextAssetU5BU5D_t2688711784** get_address_of_languages_4() { return &___languages_4; }
	inline void set_languages_4(TextAssetU5BU5D_t2688711784* value)
	{
		___languages_4 = value;
		Il2CppCodeGenWriteBarrier((&___languages_4), value);
	}

	inline static int32_t get_offset_of_mDictionary_5() { return static_cast<int32_t>(offsetof(Localization_t1621195128, ___mDictionary_5)); }
	inline Dictionary_2_t1060102516 * get_mDictionary_5() const { return ___mDictionary_5; }
	inline Dictionary_2_t1060102516 ** get_address_of_mDictionary_5() { return &___mDictionary_5; }
	inline void set_mDictionary_5(Dictionary_2_t1060102516 * value)
	{
		___mDictionary_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDictionary_5), value);
	}

	inline static int32_t get_offset_of_mLanguage_6() { return static_cast<int32_t>(offsetof(Localization_t1621195128, ___mLanguage_6)); }
	inline String_t* get_mLanguage_6() const { return ___mLanguage_6; }
	inline String_t** get_address_of_mLanguage_6() { return &___mLanguage_6; }
	inline void set_mLanguage_6(String_t* value)
	{
		___mLanguage_6 = value;
		Il2CppCodeGenWriteBarrier((&___mLanguage_6), value);
	}
};

struct Localization_t1621195128_StaticFields
{
public:
	// Localization Localization::mInst
	Localization_t1621195128 * ___mInst_2;

public:
	inline static int32_t get_offset_of_mInst_2() { return static_cast<int32_t>(offsetof(Localization_t1621195128_StaticFields, ___mInst_2)); }
	inline Localization_t1621195128 * get_mInst_2() const { return ___mInst_2; }
	inline Localization_t1621195128 ** get_address_of_mInst_2() { return &___mInst_2; }
	inline void set_mInst_2(Localization_t1621195128 * value)
	{
		___mInst_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInst_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATION_T1621195128_H
#ifndef UIINPUT_T1674607152_H
#define UIINPUT_T1674607152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput
struct  UIInput_t1674607152  : public MonoBehaviour_t1618594486
{
public:
	// UILabel UIInput::label
	UILabel_t3478074517 * ___label_3;
	// System.Int32 UIInput::maxChars
	int32_t ___maxChars_4;
	// System.String UIInput::caratChar
	String_t* ___caratChar_5;
	// UIInput/Validator UIInput::validator
	Validator_t1053183372 * ___validator_6;
	// UIInput/KeyboardType UIInput::type
	int32_t ___type_7;
	// System.Boolean UIInput::isPassword
	bool ___isPassword_8;
	// UnityEngine.Color UIInput::activeColor
	Color_t2582018970  ___activeColor_9;
	// UnityEngine.GameObject UIInput::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_10;
	// System.String UIInput::functionName
	String_t* ___functionName_11;
	// UIInput/OnSubmit UIInput::onSubmit
	OnSubmit_t2511512930 * ___onSubmit_12;
	// System.String UIInput::mText
	String_t* ___mText_13;
	// System.String UIInput::mDefaultText
	String_t* ___mDefaultText_14;
	// UnityEngine.Color UIInput::mDefaultColor
	Color_t2582018970  ___mDefaultColor_15;
	// UIWidget/Pivot UIInput::mPivot
	int32_t ___mPivot_16;
	// System.Single UIInput::mPosition
	float ___mPosition_17;
	// UnityEngine.TouchScreenKeyboard UIInput::mKeyboard
	TouchScreenKeyboard_t1357543767 * ___mKeyboard_18;
	// System.Boolean UIInput::mDoInit
	bool ___mDoInit_19;

public:
	inline static int32_t get_offset_of_label_3() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___label_3)); }
	inline UILabel_t3478074517 * get_label_3() const { return ___label_3; }
	inline UILabel_t3478074517 ** get_address_of_label_3() { return &___label_3; }
	inline void set_label_3(UILabel_t3478074517 * value)
	{
		___label_3 = value;
		Il2CppCodeGenWriteBarrier((&___label_3), value);
	}

	inline static int32_t get_offset_of_maxChars_4() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___maxChars_4)); }
	inline int32_t get_maxChars_4() const { return ___maxChars_4; }
	inline int32_t* get_address_of_maxChars_4() { return &___maxChars_4; }
	inline void set_maxChars_4(int32_t value)
	{
		___maxChars_4 = value;
	}

	inline static int32_t get_offset_of_caratChar_5() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___caratChar_5)); }
	inline String_t* get_caratChar_5() const { return ___caratChar_5; }
	inline String_t** get_address_of_caratChar_5() { return &___caratChar_5; }
	inline void set_caratChar_5(String_t* value)
	{
		___caratChar_5 = value;
		Il2CppCodeGenWriteBarrier((&___caratChar_5), value);
	}

	inline static int32_t get_offset_of_validator_6() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___validator_6)); }
	inline Validator_t1053183372 * get_validator_6() const { return ___validator_6; }
	inline Validator_t1053183372 ** get_address_of_validator_6() { return &___validator_6; }
	inline void set_validator_6(Validator_t1053183372 * value)
	{
		___validator_6 = value;
		Il2CppCodeGenWriteBarrier((&___validator_6), value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}

	inline static int32_t get_offset_of_isPassword_8() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___isPassword_8)); }
	inline bool get_isPassword_8() const { return ___isPassword_8; }
	inline bool* get_address_of_isPassword_8() { return &___isPassword_8; }
	inline void set_isPassword_8(bool value)
	{
		___isPassword_8 = value;
	}

	inline static int32_t get_offset_of_activeColor_9() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___activeColor_9)); }
	inline Color_t2582018970  get_activeColor_9() const { return ___activeColor_9; }
	inline Color_t2582018970 * get_address_of_activeColor_9() { return &___activeColor_9; }
	inline void set_activeColor_9(Color_t2582018970  value)
	{
		___activeColor_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_10() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___eventReceiver_10)); }
	inline GameObject_t2557347079 * get_eventReceiver_10() const { return ___eventReceiver_10; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_10() { return &___eventReceiver_10; }
	inline void set_eventReceiver_10(GameObject_t2557347079 * value)
	{
		___eventReceiver_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_10), value);
	}

	inline static int32_t get_offset_of_functionName_11() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___functionName_11)); }
	inline String_t* get_functionName_11() const { return ___functionName_11; }
	inline String_t** get_address_of_functionName_11() { return &___functionName_11; }
	inline void set_functionName_11(String_t* value)
	{
		___functionName_11 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_11), value);
	}

	inline static int32_t get_offset_of_onSubmit_12() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___onSubmit_12)); }
	inline OnSubmit_t2511512930 * get_onSubmit_12() const { return ___onSubmit_12; }
	inline OnSubmit_t2511512930 ** get_address_of_onSubmit_12() { return &___onSubmit_12; }
	inline void set_onSubmit_12(OnSubmit_t2511512930 * value)
	{
		___onSubmit_12 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_12), value);
	}

	inline static int32_t get_offset_of_mText_13() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mText_13)); }
	inline String_t* get_mText_13() const { return ___mText_13; }
	inline String_t** get_address_of_mText_13() { return &___mText_13; }
	inline void set_mText_13(String_t* value)
	{
		___mText_13 = value;
		Il2CppCodeGenWriteBarrier((&___mText_13), value);
	}

	inline static int32_t get_offset_of_mDefaultText_14() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mDefaultText_14)); }
	inline String_t* get_mDefaultText_14() const { return ___mDefaultText_14; }
	inline String_t** get_address_of_mDefaultText_14() { return &___mDefaultText_14; }
	inline void set_mDefaultText_14(String_t* value)
	{
		___mDefaultText_14 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultText_14), value);
	}

	inline static int32_t get_offset_of_mDefaultColor_15() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mDefaultColor_15)); }
	inline Color_t2582018970  get_mDefaultColor_15() const { return ___mDefaultColor_15; }
	inline Color_t2582018970 * get_address_of_mDefaultColor_15() { return &___mDefaultColor_15; }
	inline void set_mDefaultColor_15(Color_t2582018970  value)
	{
		___mDefaultColor_15 = value;
	}

	inline static int32_t get_offset_of_mPivot_16() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mPivot_16)); }
	inline int32_t get_mPivot_16() const { return ___mPivot_16; }
	inline int32_t* get_address_of_mPivot_16() { return &___mPivot_16; }
	inline void set_mPivot_16(int32_t value)
	{
		___mPivot_16 = value;
	}

	inline static int32_t get_offset_of_mPosition_17() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mPosition_17)); }
	inline float get_mPosition_17() const { return ___mPosition_17; }
	inline float* get_address_of_mPosition_17() { return &___mPosition_17; }
	inline void set_mPosition_17(float value)
	{
		___mPosition_17 = value;
	}

	inline static int32_t get_offset_of_mKeyboard_18() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mKeyboard_18)); }
	inline TouchScreenKeyboard_t1357543767 * get_mKeyboard_18() const { return ___mKeyboard_18; }
	inline TouchScreenKeyboard_t1357543767 ** get_address_of_mKeyboard_18() { return &___mKeyboard_18; }
	inline void set_mKeyboard_18(TouchScreenKeyboard_t1357543767 * value)
	{
		___mKeyboard_18 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyboard_18), value);
	}

	inline static int32_t get_offset_of_mDoInit_19() { return static_cast<int32_t>(offsetof(UIInput_t1674607152, ___mDoInit_19)); }
	inline bool get_mDoInit_19() const { return ___mDoInit_19; }
	inline bool* get_address_of_mDoInit_19() { return &___mDoInit_19; }
	inline void set_mDoInit_19(bool value)
	{
		___mDoInit_19 = value;
	}
};

struct UIInput_t1674607152_StaticFields
{
public:
	// UIInput UIInput::current
	UIInput_t1674607152 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIInput_t1674607152_StaticFields, ___current_2)); }
	inline UIInput_t1674607152 * get_current_2() const { return ___current_2; }
	inline UIInput_t1674607152 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIInput_t1674607152 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUT_T1674607152_H
#ifndef IGNORETIMESCALE_T640346767_H
#define IGNORETIMESCALE_T640346767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IgnoreTimeScale
struct  IgnoreTimeScale_t640346767  : public MonoBehaviour_t1618594486
{
public:
	// System.Single IgnoreTimeScale::mTimeStart
	float ___mTimeStart_2;
	// System.Single IgnoreTimeScale::mTimeDelta
	float ___mTimeDelta_3;
	// System.Single IgnoreTimeScale::mActual
	float ___mActual_4;
	// System.Boolean IgnoreTimeScale::mTimeStarted
	bool ___mTimeStarted_5;

public:
	inline static int32_t get_offset_of_mTimeStart_2() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeStart_2)); }
	inline float get_mTimeStart_2() const { return ___mTimeStart_2; }
	inline float* get_address_of_mTimeStart_2() { return &___mTimeStart_2; }
	inline void set_mTimeStart_2(float value)
	{
		___mTimeStart_2 = value;
	}

	inline static int32_t get_offset_of_mTimeDelta_3() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeDelta_3)); }
	inline float get_mTimeDelta_3() const { return ___mTimeDelta_3; }
	inline float* get_address_of_mTimeDelta_3() { return &___mTimeDelta_3; }
	inline void set_mTimeDelta_3(float value)
	{
		___mTimeDelta_3 = value;
	}

	inline static int32_t get_offset_of_mActual_4() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mActual_4)); }
	inline float get_mActual_4() const { return ___mActual_4; }
	inline float* get_address_of_mActual_4() { return &___mActual_4; }
	inline void set_mActual_4(float value)
	{
		___mActual_4 = value;
	}

	inline static int32_t get_offset_of_mTimeStarted_5() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeStarted_5)); }
	inline bool get_mTimeStarted_5() const { return ___mTimeStarted_5; }
	inline bool* get_address_of_mTimeStarted_5() { return &___mTimeStarted_5; }
	inline void set_mTimeStarted_5(bool value)
	{
		___mTimeStarted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORETIMESCALE_T640346767_H
#ifndef UIWIDGET_T1961100141_H
#define UIWIDGET_T1961100141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget
struct  UIWidget_t1961100141  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Material UIWidget::mMat
	Material_t2815264910 * ___mMat_2;
	// UnityEngine.Texture UIWidget::mTex
	Texture_t2119925672 * ___mTex_3;
	// UnityEngine.Color UIWidget::mColor
	Color_t2582018970  ___mColor_4;
	// UIWidget/Pivot UIWidget::mPivot
	int32_t ___mPivot_5;
	// System.Int32 UIWidget::mDepth
	int32_t ___mDepth_6;
	// UnityEngine.Transform UIWidget::mTrans
	Transform_t362059596 * ___mTrans_7;
	// UIPanel UIWidget::mPanel
	UIPanel_t825970685 * ___mPanel_8;
	// System.Boolean UIWidget::mChanged
	bool ___mChanged_9;
	// System.Boolean UIWidget::mPlayMode
	bool ___mPlayMode_10;
	// UnityEngine.Vector3 UIWidget::mDiffPos
	Vector3_t1986933152  ___mDiffPos_11;
	// UnityEngine.Quaternion UIWidget::mDiffRot
	Quaternion_t704191599  ___mDiffRot_12;
	// UnityEngine.Vector3 UIWidget::mDiffScale
	Vector3_t1986933152  ___mDiffScale_13;
	// System.Int32 UIWidget::mVisibleFlag
	int32_t ___mVisibleFlag_14;
	// UIGeometry UIWidget::mGeom
	UIGeometry_t76808639 * ___mGeom_15;

public:
	inline static int32_t get_offset_of_mMat_2() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mMat_2)); }
	inline Material_t2815264910 * get_mMat_2() const { return ___mMat_2; }
	inline Material_t2815264910 ** get_address_of_mMat_2() { return &___mMat_2; }
	inline void set_mMat_2(Material_t2815264910 * value)
	{
		___mMat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_2), value);
	}

	inline static int32_t get_offset_of_mTex_3() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mTex_3)); }
	inline Texture_t2119925672 * get_mTex_3() const { return ___mTex_3; }
	inline Texture_t2119925672 ** get_address_of_mTex_3() { return &___mTex_3; }
	inline void set_mTex_3(Texture_t2119925672 * value)
	{
		___mTex_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTex_3), value);
	}

	inline static int32_t get_offset_of_mColor_4() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mColor_4)); }
	inline Color_t2582018970  get_mColor_4() const { return ___mColor_4; }
	inline Color_t2582018970 * get_address_of_mColor_4() { return &___mColor_4; }
	inline void set_mColor_4(Color_t2582018970  value)
	{
		___mColor_4 = value;
	}

	inline static int32_t get_offset_of_mPivot_5() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mPivot_5)); }
	inline int32_t get_mPivot_5() const { return ___mPivot_5; }
	inline int32_t* get_address_of_mPivot_5() { return &___mPivot_5; }
	inline void set_mPivot_5(int32_t value)
	{
		___mPivot_5 = value;
	}

	inline static int32_t get_offset_of_mDepth_6() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mDepth_6)); }
	inline int32_t get_mDepth_6() const { return ___mDepth_6; }
	inline int32_t* get_address_of_mDepth_6() { return &___mDepth_6; }
	inline void set_mDepth_6(int32_t value)
	{
		___mDepth_6 = value;
	}

	inline static int32_t get_offset_of_mTrans_7() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mTrans_7)); }
	inline Transform_t362059596 * get_mTrans_7() const { return ___mTrans_7; }
	inline Transform_t362059596 ** get_address_of_mTrans_7() { return &___mTrans_7; }
	inline void set_mTrans_7(Transform_t362059596 * value)
	{
		___mTrans_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_7), value);
	}

	inline static int32_t get_offset_of_mPanel_8() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mPanel_8)); }
	inline UIPanel_t825970685 * get_mPanel_8() const { return ___mPanel_8; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_8() { return &___mPanel_8; }
	inline void set_mPanel_8(UIPanel_t825970685 * value)
	{
		___mPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_8), value);
	}

	inline static int32_t get_offset_of_mChanged_9() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mChanged_9)); }
	inline bool get_mChanged_9() const { return ___mChanged_9; }
	inline bool* get_address_of_mChanged_9() { return &___mChanged_9; }
	inline void set_mChanged_9(bool value)
	{
		___mChanged_9 = value;
	}

	inline static int32_t get_offset_of_mPlayMode_10() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mPlayMode_10)); }
	inline bool get_mPlayMode_10() const { return ___mPlayMode_10; }
	inline bool* get_address_of_mPlayMode_10() { return &___mPlayMode_10; }
	inline void set_mPlayMode_10(bool value)
	{
		___mPlayMode_10 = value;
	}

	inline static int32_t get_offset_of_mDiffPos_11() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mDiffPos_11)); }
	inline Vector3_t1986933152  get_mDiffPos_11() const { return ___mDiffPos_11; }
	inline Vector3_t1986933152 * get_address_of_mDiffPos_11() { return &___mDiffPos_11; }
	inline void set_mDiffPos_11(Vector3_t1986933152  value)
	{
		___mDiffPos_11 = value;
	}

	inline static int32_t get_offset_of_mDiffRot_12() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mDiffRot_12)); }
	inline Quaternion_t704191599  get_mDiffRot_12() const { return ___mDiffRot_12; }
	inline Quaternion_t704191599 * get_address_of_mDiffRot_12() { return &___mDiffRot_12; }
	inline void set_mDiffRot_12(Quaternion_t704191599  value)
	{
		___mDiffRot_12 = value;
	}

	inline static int32_t get_offset_of_mDiffScale_13() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mDiffScale_13)); }
	inline Vector3_t1986933152  get_mDiffScale_13() const { return ___mDiffScale_13; }
	inline Vector3_t1986933152 * get_address_of_mDiffScale_13() { return &___mDiffScale_13; }
	inline void set_mDiffScale_13(Vector3_t1986933152  value)
	{
		___mDiffScale_13 = value;
	}

	inline static int32_t get_offset_of_mVisibleFlag_14() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mVisibleFlag_14)); }
	inline int32_t get_mVisibleFlag_14() const { return ___mVisibleFlag_14; }
	inline int32_t* get_address_of_mVisibleFlag_14() { return &___mVisibleFlag_14; }
	inline void set_mVisibleFlag_14(int32_t value)
	{
		___mVisibleFlag_14 = value;
	}

	inline static int32_t get_offset_of_mGeom_15() { return static_cast<int32_t>(offsetof(UIWidget_t1961100141, ___mGeom_15)); }
	inline UIGeometry_t76808639 * get_mGeom_15() const { return ___mGeom_15; }
	inline UIGeometry_t76808639 ** get_address_of_mGeom_15() { return &___mGeom_15; }
	inline void set_mGeom_15(UIGeometry_t76808639 * value)
	{
		___mGeom_15 = value;
		Il2CppCodeGenWriteBarrier((&___mGeom_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGET_T1961100141_H
#ifndef UIFONT_T2730669065_H
#define UIFONT_T2730669065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFont
struct  UIFont_t2730669065  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Material UIFont::mMat
	Material_t2815264910 * ___mMat_2;
	// UnityEngine.Rect UIFont::mUVRect
	Rect_t3039462994  ___mUVRect_3;
	// BMFont UIFont::mFont
	BMFont_t2958998632 * ___mFont_4;
	// System.Int32 UIFont::mSpacingX
	int32_t ___mSpacingX_5;
	// System.Int32 UIFont::mSpacingY
	int32_t ___mSpacingY_6;
	// UIAtlas UIFont::mAtlas
	UIAtlas_t1815452364 * ___mAtlas_7;
	// UIFont UIFont::mReplacement
	UIFont_t2730669065 * ___mReplacement_8;
	// UIAtlas/Sprite UIFont::mSprite
	Sprite_t3039180268 * ___mSprite_9;
	// System.Boolean UIFont::mSpriteSet
	bool ___mSpriteSet_10;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIFont::mColors
	List_1_t1701645977 * ___mColors_11;

public:
	inline static int32_t get_offset_of_mMat_2() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mMat_2)); }
	inline Material_t2815264910 * get_mMat_2() const { return ___mMat_2; }
	inline Material_t2815264910 ** get_address_of_mMat_2() { return &___mMat_2; }
	inline void set_mMat_2(Material_t2815264910 * value)
	{
		___mMat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_2), value);
	}

	inline static int32_t get_offset_of_mUVRect_3() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mUVRect_3)); }
	inline Rect_t3039462994  get_mUVRect_3() const { return ___mUVRect_3; }
	inline Rect_t3039462994 * get_address_of_mUVRect_3() { return &___mUVRect_3; }
	inline void set_mUVRect_3(Rect_t3039462994  value)
	{
		___mUVRect_3 = value;
	}

	inline static int32_t get_offset_of_mFont_4() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mFont_4)); }
	inline BMFont_t2958998632 * get_mFont_4() const { return ___mFont_4; }
	inline BMFont_t2958998632 ** get_address_of_mFont_4() { return &___mFont_4; }
	inline void set_mFont_4(BMFont_t2958998632 * value)
	{
		___mFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___mFont_4), value);
	}

	inline static int32_t get_offset_of_mSpacingX_5() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mSpacingX_5)); }
	inline int32_t get_mSpacingX_5() const { return ___mSpacingX_5; }
	inline int32_t* get_address_of_mSpacingX_5() { return &___mSpacingX_5; }
	inline void set_mSpacingX_5(int32_t value)
	{
		___mSpacingX_5 = value;
	}

	inline static int32_t get_offset_of_mSpacingY_6() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mSpacingY_6)); }
	inline int32_t get_mSpacingY_6() const { return ___mSpacingY_6; }
	inline int32_t* get_address_of_mSpacingY_6() { return &___mSpacingY_6; }
	inline void set_mSpacingY_6(int32_t value)
	{
		___mSpacingY_6 = value;
	}

	inline static int32_t get_offset_of_mAtlas_7() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mAtlas_7)); }
	inline UIAtlas_t1815452364 * get_mAtlas_7() const { return ___mAtlas_7; }
	inline UIAtlas_t1815452364 ** get_address_of_mAtlas_7() { return &___mAtlas_7; }
	inline void set_mAtlas_7(UIAtlas_t1815452364 * value)
	{
		___mAtlas_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_7), value);
	}

	inline static int32_t get_offset_of_mReplacement_8() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mReplacement_8)); }
	inline UIFont_t2730669065 * get_mReplacement_8() const { return ___mReplacement_8; }
	inline UIFont_t2730669065 ** get_address_of_mReplacement_8() { return &___mReplacement_8; }
	inline void set_mReplacement_8(UIFont_t2730669065 * value)
	{
		___mReplacement_8 = value;
		Il2CppCodeGenWriteBarrier((&___mReplacement_8), value);
	}

	inline static int32_t get_offset_of_mSprite_9() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mSprite_9)); }
	inline Sprite_t3039180268 * get_mSprite_9() const { return ___mSprite_9; }
	inline Sprite_t3039180268 ** get_address_of_mSprite_9() { return &___mSprite_9; }
	inline void set_mSprite_9(Sprite_t3039180268 * value)
	{
		___mSprite_9 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_9), value);
	}

	inline static int32_t get_offset_of_mSpriteSet_10() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mSpriteSet_10)); }
	inline bool get_mSpriteSet_10() const { return ___mSpriteSet_10; }
	inline bool* get_address_of_mSpriteSet_10() { return &___mSpriteSet_10; }
	inline void set_mSpriteSet_10(bool value)
	{
		___mSpriteSet_10 = value;
	}

	inline static int32_t get_offset_of_mColors_11() { return static_cast<int32_t>(offsetof(UIFont_t2730669065, ___mColors_11)); }
	inline List_1_t1701645977 * get_mColors_11() const { return ___mColors_11; }
	inline List_1_t1701645977 ** get_address_of_mColors_11() { return &___mColors_11; }
	inline void set_mColors_11(List_1_t1701645977 * value)
	{
		___mColors_11 = value;
		Il2CppCodeGenWriteBarrier((&___mColors_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFONT_T2730669065_H
#ifndef UPDATEMANAGER_T1291149948_H
#define UPDATEMANAGER_T1291149948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager
struct  UpdateManager_t1291149948  : public MonoBehaviour_t1618594486
{
public:
	// System.Collections.Generic.List`1<UpdateManager/UpdateEntry> UpdateManager::mOnUpdate
	List_1_t1921793510 * ___mOnUpdate_3;
	// System.Collections.Generic.List`1<UpdateManager/UpdateEntry> UpdateManager::mOnLate
	List_1_t1921793510 * ___mOnLate_4;
	// System.Collections.Generic.List`1<UpdateManager/UpdateEntry> UpdateManager::mOnCoro
	List_1_t1921793510 * ___mOnCoro_5;
	// BetterList`1<UpdateManager/DestroyEntry> UpdateManager::mDest
	BetterList_1_t2800363178 * ___mDest_6;
	// System.Single UpdateManager::mTime
	float ___mTime_7;

public:
	inline static int32_t get_offset_of_mOnUpdate_3() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948, ___mOnUpdate_3)); }
	inline List_1_t1921793510 * get_mOnUpdate_3() const { return ___mOnUpdate_3; }
	inline List_1_t1921793510 ** get_address_of_mOnUpdate_3() { return &___mOnUpdate_3; }
	inline void set_mOnUpdate_3(List_1_t1921793510 * value)
	{
		___mOnUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mOnUpdate_3), value);
	}

	inline static int32_t get_offset_of_mOnLate_4() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948, ___mOnLate_4)); }
	inline List_1_t1921793510 * get_mOnLate_4() const { return ___mOnLate_4; }
	inline List_1_t1921793510 ** get_address_of_mOnLate_4() { return &___mOnLate_4; }
	inline void set_mOnLate_4(List_1_t1921793510 * value)
	{
		___mOnLate_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnLate_4), value);
	}

	inline static int32_t get_offset_of_mOnCoro_5() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948, ___mOnCoro_5)); }
	inline List_1_t1921793510 * get_mOnCoro_5() const { return ___mOnCoro_5; }
	inline List_1_t1921793510 ** get_address_of_mOnCoro_5() { return &___mOnCoro_5; }
	inline void set_mOnCoro_5(List_1_t1921793510 * value)
	{
		___mOnCoro_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnCoro_5), value);
	}

	inline static int32_t get_offset_of_mDest_6() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948, ___mDest_6)); }
	inline BetterList_1_t2800363178 * get_mDest_6() const { return ___mDest_6; }
	inline BetterList_1_t2800363178 ** get_address_of_mDest_6() { return &___mDest_6; }
	inline void set_mDest_6(BetterList_1_t2800363178 * value)
	{
		___mDest_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDest_6), value);
	}

	inline static int32_t get_offset_of_mTime_7() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948, ___mTime_7)); }
	inline float get_mTime_7() const { return ___mTime_7; }
	inline float* get_address_of_mTime_7() { return &___mTime_7; }
	inline void set_mTime_7(float value)
	{
		___mTime_7 = value;
	}
};

struct UpdateManager_t1291149948_StaticFields
{
public:
	// UpdateManager UpdateManager::mInst
	UpdateManager_t1291149948 * ___mInst_2;
	// System.Comparison`1<UpdateManager/UpdateEntry> UpdateManager::<>f__mg$cache0
	Comparison_1_t4186224356 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_mInst_2() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948_StaticFields, ___mInst_2)); }
	inline UpdateManager_t1291149948 * get_mInst_2() const { return ___mInst_2; }
	inline UpdateManager_t1291149948 ** get_address_of_mInst_2() { return &___mInst_2; }
	inline void set_mInst_2(UpdateManager_t1291149948 * value)
	{
		___mInst_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInst_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(UpdateManager_t1291149948_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline Comparison_1_t4186224356 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline Comparison_1_t4186224356 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(Comparison_1_t4186224356 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMANAGER_T1291149948_H
#ifndef UICAMERA_T3843949198_H
#define UICAMERA_T3843949198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t3843949198  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UICamera::useMouse
	bool ___useMouse_2;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_3;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_4;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_5;
	// System.Boolean UICamera::useController
	bool ___useController_6;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t246267875  ___eventReceiverMask_7;
	// System.Boolean UICamera::clipRaycasts
	bool ___clipRaycasts_8;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_9;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_10;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_11;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_12;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_13;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_14;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_15;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_16;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_17;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_18;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_19;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_20;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_21;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,UICamera/MouseOrTouch> UICamera::mTouches
	Dictionary_2_t4270017272 * ___mTouches_40;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t2557347079 * ___mTooltip_41;
	// UnityEngine.Camera UICamera::mCam
	Camera_t2839736942 * ___mCam_42;
	// UnityEngine.LayerMask UICamera::mLayerMask
	LayerMask_t246267875  ___mLayerMask_43;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_44;
	// System.Boolean UICamera::mIsEditor
	bool ___mIsEditor_45;

public:
	inline static int32_t get_offset_of_useMouse_2() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___useMouse_2)); }
	inline bool get_useMouse_2() const { return ___useMouse_2; }
	inline bool* get_address_of_useMouse_2() { return &___useMouse_2; }
	inline void set_useMouse_2(bool value)
	{
		___useMouse_2 = value;
	}

	inline static int32_t get_offset_of_useTouch_3() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___useTouch_3)); }
	inline bool get_useTouch_3() const { return ___useTouch_3; }
	inline bool* get_address_of_useTouch_3() { return &___useTouch_3; }
	inline void set_useTouch_3(bool value)
	{
		___useTouch_3 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_4() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___allowMultiTouch_4)); }
	inline bool get_allowMultiTouch_4() const { return ___allowMultiTouch_4; }
	inline bool* get_address_of_allowMultiTouch_4() { return &___allowMultiTouch_4; }
	inline void set_allowMultiTouch_4(bool value)
	{
		___allowMultiTouch_4 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_5() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___useKeyboard_5)); }
	inline bool get_useKeyboard_5() const { return ___useKeyboard_5; }
	inline bool* get_address_of_useKeyboard_5() { return &___useKeyboard_5; }
	inline void set_useKeyboard_5(bool value)
	{
		___useKeyboard_5 = value;
	}

	inline static int32_t get_offset_of_useController_6() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___useController_6)); }
	inline bool get_useController_6() const { return ___useController_6; }
	inline bool* get_address_of_useController_6() { return &___useController_6; }
	inline void set_useController_6(bool value)
	{
		___useController_6 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_7() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___eventReceiverMask_7)); }
	inline LayerMask_t246267875  get_eventReceiverMask_7() const { return ___eventReceiverMask_7; }
	inline LayerMask_t246267875 * get_address_of_eventReceiverMask_7() { return &___eventReceiverMask_7; }
	inline void set_eventReceiverMask_7(LayerMask_t246267875  value)
	{
		___eventReceiverMask_7 = value;
	}

	inline static int32_t get_offset_of_clipRaycasts_8() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___clipRaycasts_8)); }
	inline bool get_clipRaycasts_8() const { return ___clipRaycasts_8; }
	inline bool* get_address_of_clipRaycasts_8() { return &___clipRaycasts_8; }
	inline void set_clipRaycasts_8(bool value)
	{
		___clipRaycasts_8 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_9() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___tooltipDelay_9)); }
	inline float get_tooltipDelay_9() const { return ___tooltipDelay_9; }
	inline float* get_address_of_tooltipDelay_9() { return &___tooltipDelay_9; }
	inline void set_tooltipDelay_9(float value)
	{
		___tooltipDelay_9 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_10() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___stickyTooltip_10)); }
	inline bool get_stickyTooltip_10() const { return ___stickyTooltip_10; }
	inline bool* get_address_of_stickyTooltip_10() { return &___stickyTooltip_10; }
	inline void set_stickyTooltip_10(bool value)
	{
		___stickyTooltip_10 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_11() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mouseDragThreshold_11)); }
	inline float get_mouseDragThreshold_11() const { return ___mouseDragThreshold_11; }
	inline float* get_address_of_mouseDragThreshold_11() { return &___mouseDragThreshold_11; }
	inline void set_mouseDragThreshold_11(float value)
	{
		___mouseDragThreshold_11 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_12() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mouseClickThreshold_12)); }
	inline float get_mouseClickThreshold_12() const { return ___mouseClickThreshold_12; }
	inline float* get_address_of_mouseClickThreshold_12() { return &___mouseClickThreshold_12; }
	inline void set_mouseClickThreshold_12(float value)
	{
		___mouseClickThreshold_12 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_13() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___touchDragThreshold_13)); }
	inline float get_touchDragThreshold_13() const { return ___touchDragThreshold_13; }
	inline float* get_address_of_touchDragThreshold_13() { return &___touchDragThreshold_13; }
	inline void set_touchDragThreshold_13(float value)
	{
		___touchDragThreshold_13 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_14() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___touchClickThreshold_14)); }
	inline float get_touchClickThreshold_14() const { return ___touchClickThreshold_14; }
	inline float* get_address_of_touchClickThreshold_14() { return &___touchClickThreshold_14; }
	inline void set_touchClickThreshold_14(float value)
	{
		___touchClickThreshold_14 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_15() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___rangeDistance_15)); }
	inline float get_rangeDistance_15() const { return ___rangeDistance_15; }
	inline float* get_address_of_rangeDistance_15() { return &___rangeDistance_15; }
	inline void set_rangeDistance_15(float value)
	{
		___rangeDistance_15 = value;
	}

	inline static int32_t get_offset_of_scrollAxisName_16() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___scrollAxisName_16)); }
	inline String_t* get_scrollAxisName_16() const { return ___scrollAxisName_16; }
	inline String_t** get_address_of_scrollAxisName_16() { return &___scrollAxisName_16; }
	inline void set_scrollAxisName_16(String_t* value)
	{
		___scrollAxisName_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollAxisName_16), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_17() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___verticalAxisName_17)); }
	inline String_t* get_verticalAxisName_17() const { return ___verticalAxisName_17; }
	inline String_t** get_address_of_verticalAxisName_17() { return &___verticalAxisName_17; }
	inline void set_verticalAxisName_17(String_t* value)
	{
		___verticalAxisName_17 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_17), value);
	}

	inline static int32_t get_offset_of_horizontalAxisName_18() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___horizontalAxisName_18)); }
	inline String_t* get_horizontalAxisName_18() const { return ___horizontalAxisName_18; }
	inline String_t** get_address_of_horizontalAxisName_18() { return &___horizontalAxisName_18; }
	inline void set_horizontalAxisName_18(String_t* value)
	{
		___horizontalAxisName_18 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_18), value);
	}

	inline static int32_t get_offset_of_submitKey0_19() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___submitKey0_19)); }
	inline int32_t get_submitKey0_19() const { return ___submitKey0_19; }
	inline int32_t* get_address_of_submitKey0_19() { return &___submitKey0_19; }
	inline void set_submitKey0_19(int32_t value)
	{
		___submitKey0_19 = value;
	}

	inline static int32_t get_offset_of_submitKey1_20() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___submitKey1_20)); }
	inline int32_t get_submitKey1_20() const { return ___submitKey1_20; }
	inline int32_t* get_address_of_submitKey1_20() { return &___submitKey1_20; }
	inline void set_submitKey1_20(int32_t value)
	{
		___submitKey1_20 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_21() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___cancelKey0_21)); }
	inline int32_t get_cancelKey0_21() const { return ___cancelKey0_21; }
	inline int32_t* get_address_of_cancelKey0_21() { return &___cancelKey0_21; }
	inline void set_cancelKey0_21(int32_t value)
	{
		___cancelKey0_21 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_22() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___cancelKey1_22)); }
	inline int32_t get_cancelKey1_22() const { return ___cancelKey1_22; }
	inline int32_t* get_address_of_cancelKey1_22() { return &___cancelKey1_22; }
	inline void set_cancelKey1_22(int32_t value)
	{
		___cancelKey1_22 = value;
	}

	inline static int32_t get_offset_of_mTouches_40() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mTouches_40)); }
	inline Dictionary_2_t4270017272 * get_mTouches_40() const { return ___mTouches_40; }
	inline Dictionary_2_t4270017272 ** get_address_of_mTouches_40() { return &___mTouches_40; }
	inline void set_mTouches_40(Dictionary_2_t4270017272 * value)
	{
		___mTouches_40 = value;
		Il2CppCodeGenWriteBarrier((&___mTouches_40), value);
	}

	inline static int32_t get_offset_of_mTooltip_41() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mTooltip_41)); }
	inline GameObject_t2557347079 * get_mTooltip_41() const { return ___mTooltip_41; }
	inline GameObject_t2557347079 ** get_address_of_mTooltip_41() { return &___mTooltip_41; }
	inline void set_mTooltip_41(GameObject_t2557347079 * value)
	{
		___mTooltip_41 = value;
		Il2CppCodeGenWriteBarrier((&___mTooltip_41), value);
	}

	inline static int32_t get_offset_of_mCam_42() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mCam_42)); }
	inline Camera_t2839736942 * get_mCam_42() const { return ___mCam_42; }
	inline Camera_t2839736942 ** get_address_of_mCam_42() { return &___mCam_42; }
	inline void set_mCam_42(Camera_t2839736942 * value)
	{
		___mCam_42 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_42), value);
	}

	inline static int32_t get_offset_of_mLayerMask_43() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mLayerMask_43)); }
	inline LayerMask_t246267875  get_mLayerMask_43() const { return ___mLayerMask_43; }
	inline LayerMask_t246267875 * get_address_of_mLayerMask_43() { return &___mLayerMask_43; }
	inline void set_mLayerMask_43(LayerMask_t246267875  value)
	{
		___mLayerMask_43 = value;
	}

	inline static int32_t get_offset_of_mTooltipTime_44() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mTooltipTime_44)); }
	inline float get_mTooltipTime_44() const { return ___mTooltipTime_44; }
	inline float* get_address_of_mTooltipTime_44() { return &___mTooltipTime_44; }
	inline void set_mTooltipTime_44(float value)
	{
		___mTooltipTime_44 = value;
	}

	inline static int32_t get_offset_of_mIsEditor_45() { return static_cast<int32_t>(offsetof(UICamera_t3843949198, ___mIsEditor_45)); }
	inline bool get_mIsEditor_45() const { return ___mIsEditor_45; }
	inline bool* get_address_of_mIsEditor_45() { return &___mIsEditor_45; }
	inline void set_mIsEditor_45(bool value)
	{
		___mIsEditor_45 = value;
	}
};

struct UICamera_t3843949198_StaticFields
{
public:
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_23;
	// UnityEngine.Vector2 UICamera::lastTouchPosition
	Vector2_t328513675  ___lastTouchPosition_24;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t1706347245  ___lastHit_25;
	// UICamera UICamera::current
	UICamera_t3843949198 * ___current_26;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t2839736942 * ___currentCamera_27;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_28;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t115752944 * ___currentTouch_29;
	// System.Boolean UICamera::inputHasFocus
	bool ___inputHasFocus_30;
	// UnityEngine.GameObject UICamera::genericEventHandler
	GameObject_t2557347079 * ___genericEventHandler_31;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t2557347079 * ___fallThrough_32;
	// System.Collections.Generic.List`1<UICamera> UICamera::mList
	List_1_t2963576205 * ___mList_33;
	// System.Collections.Generic.List`1<UICamera/Highlighted> UICamera::mHighlighted
	List_1_t1157524252 * ___mHighlighted_34;
	// UnityEngine.GameObject UICamera::mSel
	GameObject_t2557347079 * ___mSel_35;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t1271829073* ___mMouse_36;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t2557347079 * ___mHover_37;
	// UICamera/MouseOrTouch UICamera::mController
	MouseOrTouch_t115752944 * ___mController_38;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_39;
	// System.Comparison`1<UnityEngine.RaycastHit> UICamera::<>f__am$cache0
	Comparison_1_t3090405098 * ___U3CU3Ef__amU24cache0_46;
	// System.Comparison`1<UICamera> UICamera::<>f__mg$cache0
	Comparison_1_t933039755 * ___U3CU3Ef__mgU24cache0_47;

public:
	inline static int32_t get_offset_of_showTooltips_23() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___showTooltips_23)); }
	inline bool get_showTooltips_23() const { return ___showTooltips_23; }
	inline bool* get_address_of_showTooltips_23() { return &___showTooltips_23; }
	inline void set_showTooltips_23(bool value)
	{
		___showTooltips_23 = value;
	}

	inline static int32_t get_offset_of_lastTouchPosition_24() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___lastTouchPosition_24)); }
	inline Vector2_t328513675  get_lastTouchPosition_24() const { return ___lastTouchPosition_24; }
	inline Vector2_t328513675 * get_address_of_lastTouchPosition_24() { return &___lastTouchPosition_24; }
	inline void set_lastTouchPosition_24(Vector2_t328513675  value)
	{
		___lastTouchPosition_24 = value;
	}

	inline static int32_t get_offset_of_lastHit_25() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___lastHit_25)); }
	inline RaycastHit_t1706347245  get_lastHit_25() const { return ___lastHit_25; }
	inline RaycastHit_t1706347245 * get_address_of_lastHit_25() { return &___lastHit_25; }
	inline void set_lastHit_25(RaycastHit_t1706347245  value)
	{
		___lastHit_25 = value;
	}

	inline static int32_t get_offset_of_current_26() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___current_26)); }
	inline UICamera_t3843949198 * get_current_26() const { return ___current_26; }
	inline UICamera_t3843949198 ** get_address_of_current_26() { return &___current_26; }
	inline void set_current_26(UICamera_t3843949198 * value)
	{
		___current_26 = value;
		Il2CppCodeGenWriteBarrier((&___current_26), value);
	}

	inline static int32_t get_offset_of_currentCamera_27() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___currentCamera_27)); }
	inline Camera_t2839736942 * get_currentCamera_27() const { return ___currentCamera_27; }
	inline Camera_t2839736942 ** get_address_of_currentCamera_27() { return &___currentCamera_27; }
	inline void set_currentCamera_27(Camera_t2839736942 * value)
	{
		___currentCamera_27 = value;
		Il2CppCodeGenWriteBarrier((&___currentCamera_27), value);
	}

	inline static int32_t get_offset_of_currentTouchID_28() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___currentTouchID_28)); }
	inline int32_t get_currentTouchID_28() const { return ___currentTouchID_28; }
	inline int32_t* get_address_of_currentTouchID_28() { return &___currentTouchID_28; }
	inline void set_currentTouchID_28(int32_t value)
	{
		___currentTouchID_28 = value;
	}

	inline static int32_t get_offset_of_currentTouch_29() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___currentTouch_29)); }
	inline MouseOrTouch_t115752944 * get_currentTouch_29() const { return ___currentTouch_29; }
	inline MouseOrTouch_t115752944 ** get_address_of_currentTouch_29() { return &___currentTouch_29; }
	inline void set_currentTouch_29(MouseOrTouch_t115752944 * value)
	{
		___currentTouch_29 = value;
		Il2CppCodeGenWriteBarrier((&___currentTouch_29), value);
	}

	inline static int32_t get_offset_of_inputHasFocus_30() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___inputHasFocus_30)); }
	inline bool get_inputHasFocus_30() const { return ___inputHasFocus_30; }
	inline bool* get_address_of_inputHasFocus_30() { return &___inputHasFocus_30; }
	inline void set_inputHasFocus_30(bool value)
	{
		___inputHasFocus_30 = value;
	}

	inline static int32_t get_offset_of_genericEventHandler_31() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___genericEventHandler_31)); }
	inline GameObject_t2557347079 * get_genericEventHandler_31() const { return ___genericEventHandler_31; }
	inline GameObject_t2557347079 ** get_address_of_genericEventHandler_31() { return &___genericEventHandler_31; }
	inline void set_genericEventHandler_31(GameObject_t2557347079 * value)
	{
		___genericEventHandler_31 = value;
		Il2CppCodeGenWriteBarrier((&___genericEventHandler_31), value);
	}

	inline static int32_t get_offset_of_fallThrough_32() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___fallThrough_32)); }
	inline GameObject_t2557347079 * get_fallThrough_32() const { return ___fallThrough_32; }
	inline GameObject_t2557347079 ** get_address_of_fallThrough_32() { return &___fallThrough_32; }
	inline void set_fallThrough_32(GameObject_t2557347079 * value)
	{
		___fallThrough_32 = value;
		Il2CppCodeGenWriteBarrier((&___fallThrough_32), value);
	}

	inline static int32_t get_offset_of_mList_33() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mList_33)); }
	inline List_1_t2963576205 * get_mList_33() const { return ___mList_33; }
	inline List_1_t2963576205 ** get_address_of_mList_33() { return &___mList_33; }
	inline void set_mList_33(List_1_t2963576205 * value)
	{
		___mList_33 = value;
		Il2CppCodeGenWriteBarrier((&___mList_33), value);
	}

	inline static int32_t get_offset_of_mHighlighted_34() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mHighlighted_34)); }
	inline List_1_t1157524252 * get_mHighlighted_34() const { return ___mHighlighted_34; }
	inline List_1_t1157524252 ** get_address_of_mHighlighted_34() { return &___mHighlighted_34; }
	inline void set_mHighlighted_34(List_1_t1157524252 * value)
	{
		___mHighlighted_34 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlighted_34), value);
	}

	inline static int32_t get_offset_of_mSel_35() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mSel_35)); }
	inline GameObject_t2557347079 * get_mSel_35() const { return ___mSel_35; }
	inline GameObject_t2557347079 ** get_address_of_mSel_35() { return &___mSel_35; }
	inline void set_mSel_35(GameObject_t2557347079 * value)
	{
		___mSel_35 = value;
		Il2CppCodeGenWriteBarrier((&___mSel_35), value);
	}

	inline static int32_t get_offset_of_mMouse_36() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mMouse_36)); }
	inline MouseOrTouchU5BU5D_t1271829073* get_mMouse_36() const { return ___mMouse_36; }
	inline MouseOrTouchU5BU5D_t1271829073** get_address_of_mMouse_36() { return &___mMouse_36; }
	inline void set_mMouse_36(MouseOrTouchU5BU5D_t1271829073* value)
	{
		___mMouse_36 = value;
		Il2CppCodeGenWriteBarrier((&___mMouse_36), value);
	}

	inline static int32_t get_offset_of_mHover_37() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mHover_37)); }
	inline GameObject_t2557347079 * get_mHover_37() const { return ___mHover_37; }
	inline GameObject_t2557347079 ** get_address_of_mHover_37() { return &___mHover_37; }
	inline void set_mHover_37(GameObject_t2557347079 * value)
	{
		___mHover_37 = value;
		Il2CppCodeGenWriteBarrier((&___mHover_37), value);
	}

	inline static int32_t get_offset_of_mController_38() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mController_38)); }
	inline MouseOrTouch_t115752944 * get_mController_38() const { return ___mController_38; }
	inline MouseOrTouch_t115752944 ** get_address_of_mController_38() { return &___mController_38; }
	inline void set_mController_38(MouseOrTouch_t115752944 * value)
	{
		___mController_38 = value;
		Il2CppCodeGenWriteBarrier((&___mController_38), value);
	}

	inline static int32_t get_offset_of_mNextEvent_39() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___mNextEvent_39)); }
	inline float get_mNextEvent_39() const { return ___mNextEvent_39; }
	inline float* get_address_of_mNextEvent_39() { return &___mNextEvent_39; }
	inline void set_mNextEvent_39(float value)
	{
		___mNextEvent_39 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_46() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___U3CU3Ef__amU24cache0_46)); }
	inline Comparison_1_t3090405098 * get_U3CU3Ef__amU24cache0_46() const { return ___U3CU3Ef__amU24cache0_46; }
	inline Comparison_1_t3090405098 ** get_address_of_U3CU3Ef__amU24cache0_46() { return &___U3CU3Ef__amU24cache0_46; }
	inline void set_U3CU3Ef__amU24cache0_46(Comparison_1_t3090405098 * value)
	{
		___U3CU3Ef__amU24cache0_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_46), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_47() { return static_cast<int32_t>(offsetof(UICamera_t3843949198_StaticFields, ___U3CU3Ef__mgU24cache0_47)); }
	inline Comparison_1_t933039755 * get_U3CU3Ef__mgU24cache0_47() const { return ___U3CU3Ef__mgU24cache0_47; }
	inline Comparison_1_t933039755 ** get_address_of_U3CU3Ef__mgU24cache0_47() { return &___U3CU3Ef__mgU24cache0_47; }
	inline void set_U3CU3Ef__mgU24cache0_47(Comparison_1_t933039755 * value)
	{
		___U3CU3Ef__mgU24cache0_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_47), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICAMERA_T3843949198_H
#ifndef UIROOT_T937406327_H
#define UIROOT_T937406327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRoot
struct  UIRoot_t937406327  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform UIRoot::mTrans
	Transform_t362059596 * ___mTrans_3;
	// System.Boolean UIRoot::automatic
	bool ___automatic_4;
	// System.Int32 UIRoot::manualHeight
	int32_t ___manualHeight_5;
	// System.Int32 UIRoot::minimumHeight
	int32_t ___minimumHeight_6;
	// System.Int32 UIRoot::maximumHeight
	int32_t ___maximumHeight_7;

public:
	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(UIRoot_t937406327, ___mTrans_3)); }
	inline Transform_t362059596 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t362059596 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t362059596 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_automatic_4() { return static_cast<int32_t>(offsetof(UIRoot_t937406327, ___automatic_4)); }
	inline bool get_automatic_4() const { return ___automatic_4; }
	inline bool* get_address_of_automatic_4() { return &___automatic_4; }
	inline void set_automatic_4(bool value)
	{
		___automatic_4 = value;
	}

	inline static int32_t get_offset_of_manualHeight_5() { return static_cast<int32_t>(offsetof(UIRoot_t937406327, ___manualHeight_5)); }
	inline int32_t get_manualHeight_5() const { return ___manualHeight_5; }
	inline int32_t* get_address_of_manualHeight_5() { return &___manualHeight_5; }
	inline void set_manualHeight_5(int32_t value)
	{
		___manualHeight_5 = value;
	}

	inline static int32_t get_offset_of_minimumHeight_6() { return static_cast<int32_t>(offsetof(UIRoot_t937406327, ___minimumHeight_6)); }
	inline int32_t get_minimumHeight_6() const { return ___minimumHeight_6; }
	inline int32_t* get_address_of_minimumHeight_6() { return &___minimumHeight_6; }
	inline void set_minimumHeight_6(int32_t value)
	{
		___minimumHeight_6 = value;
	}

	inline static int32_t get_offset_of_maximumHeight_7() { return static_cast<int32_t>(offsetof(UIRoot_t937406327, ___maximumHeight_7)); }
	inline int32_t get_maximumHeight_7() const { return ___maximumHeight_7; }
	inline int32_t* get_address_of_maximumHeight_7() { return &___maximumHeight_7; }
	inline void set_maximumHeight_7(int32_t value)
	{
		___maximumHeight_7 = value;
	}
};

struct UIRoot_t937406327_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIRoot> UIRoot::mRoots
	List_1_t57033334 * ___mRoots_2;

public:
	inline static int32_t get_offset_of_mRoots_2() { return static_cast<int32_t>(offsetof(UIRoot_t937406327_StaticFields, ___mRoots_2)); }
	inline List_1_t57033334 * get_mRoots_2() const { return ___mRoots_2; }
	inline List_1_t57033334 ** get_address_of_mRoots_2() { return &___mRoots_2; }
	inline void set_mRoots_2(List_1_t57033334 * value)
	{
		___mRoots_2 = value;
		Il2CppCodeGenWriteBarrier((&___mRoots_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIROOT_T937406327_H
#ifndef UITOOLTIP_T1085649404_H
#define UITOOLTIP_T1085649404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITooltip
struct  UITooltip_t1085649404  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera UITooltip::uiCamera
	Camera_t2839736942 * ___uiCamera_3;
	// UILabel UITooltip::text
	UILabel_t3478074517 * ___text_4;
	// UISlicedSprite UITooltip::background
	UISlicedSprite_t3319190482 * ___background_5;
	// System.Single UITooltip::appearSpeed
	float ___appearSpeed_6;
	// System.Boolean UITooltip::scalingTransitions
	bool ___scalingTransitions_7;
	// UnityEngine.Transform UITooltip::mTrans
	Transform_t362059596 * ___mTrans_8;
	// System.Single UITooltip::mTarget
	float ___mTarget_9;
	// System.Single UITooltip::mCurrent
	float ___mCurrent_10;
	// UnityEngine.Vector3 UITooltip::mPos
	Vector3_t1986933152  ___mPos_11;
	// UnityEngine.Vector3 UITooltip::mSize
	Vector3_t1986933152  ___mSize_12;
	// UIWidget[] UITooltip::mWidgets
	UIWidgetU5BU5D_t2376853568* ___mWidgets_13;

public:
	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___uiCamera_3)); }
	inline Camera_t2839736942 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t2839736942 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___text_4)); }
	inline UILabel_t3478074517 * get_text_4() const { return ___text_4; }
	inline UILabel_t3478074517 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(UILabel_t3478074517 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___background_5)); }
	inline UISlicedSprite_t3319190482 * get_background_5() const { return ___background_5; }
	inline UISlicedSprite_t3319190482 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(UISlicedSprite_t3319190482 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}

	inline static int32_t get_offset_of_appearSpeed_6() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___appearSpeed_6)); }
	inline float get_appearSpeed_6() const { return ___appearSpeed_6; }
	inline float* get_address_of_appearSpeed_6() { return &___appearSpeed_6; }
	inline void set_appearSpeed_6(float value)
	{
		___appearSpeed_6 = value;
	}

	inline static int32_t get_offset_of_scalingTransitions_7() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___scalingTransitions_7)); }
	inline bool get_scalingTransitions_7() const { return ___scalingTransitions_7; }
	inline bool* get_address_of_scalingTransitions_7() { return &___scalingTransitions_7; }
	inline void set_scalingTransitions_7(bool value)
	{
		___scalingTransitions_7 = value;
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mTrans_8)); }
	inline Transform_t362059596 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t362059596 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t362059596 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mTarget_9() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mTarget_9)); }
	inline float get_mTarget_9() const { return ___mTarget_9; }
	inline float* get_address_of_mTarget_9() { return &___mTarget_9; }
	inline void set_mTarget_9(float value)
	{
		___mTarget_9 = value;
	}

	inline static int32_t get_offset_of_mCurrent_10() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mCurrent_10)); }
	inline float get_mCurrent_10() const { return ___mCurrent_10; }
	inline float* get_address_of_mCurrent_10() { return &___mCurrent_10; }
	inline void set_mCurrent_10(float value)
	{
		___mCurrent_10 = value;
	}

	inline static int32_t get_offset_of_mPos_11() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mPos_11)); }
	inline Vector3_t1986933152  get_mPos_11() const { return ___mPos_11; }
	inline Vector3_t1986933152 * get_address_of_mPos_11() { return &___mPos_11; }
	inline void set_mPos_11(Vector3_t1986933152  value)
	{
		___mPos_11 = value;
	}

	inline static int32_t get_offset_of_mSize_12() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mSize_12)); }
	inline Vector3_t1986933152  get_mSize_12() const { return ___mSize_12; }
	inline Vector3_t1986933152 * get_address_of_mSize_12() { return &___mSize_12; }
	inline void set_mSize_12(Vector3_t1986933152  value)
	{
		___mSize_12 = value;
	}

	inline static int32_t get_offset_of_mWidgets_13() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404, ___mWidgets_13)); }
	inline UIWidgetU5BU5D_t2376853568* get_mWidgets_13() const { return ___mWidgets_13; }
	inline UIWidgetU5BU5D_t2376853568** get_address_of_mWidgets_13() { return &___mWidgets_13; }
	inline void set_mWidgets_13(UIWidgetU5BU5D_t2376853568* value)
	{
		___mWidgets_13 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_13), value);
	}
};

struct UITooltip_t1085649404_StaticFields
{
public:
	// UITooltip UITooltip::mInstance
	UITooltip_t1085649404 * ___mInstance_2;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(UITooltip_t1085649404_StaticFields, ___mInstance_2)); }
	inline UITooltip_t1085649404 * get_mInstance_2() const { return ___mInstance_2; }
	inline UITooltip_t1085649404 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(UITooltip_t1085649404 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOOLTIP_T1085649404_H
#ifndef UITEXTLIST_T1100452823_H
#define UITEXTLIST_T1100452823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList
struct  UITextList_t1100452823  : public MonoBehaviour_t1618594486
{
public:
	// UITextList/Style UITextList::style
	int32_t ___style_2;
	// UILabel UITextList::textLabel
	UILabel_t3478074517 * ___textLabel_3;
	// System.Single UITextList::maxWidth
	float ___maxWidth_4;
	// System.Single UITextList::maxHeight
	float ___maxHeight_5;
	// System.Int32 UITextList::maxEntries
	int32_t ___maxEntries_6;
	// System.Boolean UITextList::supportScrollWheel
	bool ___supportScrollWheel_7;
	// System.Char[] UITextList::mSeparator
	CharU5BU5D_t3419619864* ___mSeparator_8;
	// System.Collections.Generic.List`1<UITextList/Paragraph> UITextList::mParagraphs
	List_1_t2870954769 * ___mParagraphs_9;
	// System.Single UITextList::mScroll
	float ___mScroll_10;
	// System.Boolean UITextList::mSelected
	bool ___mSelected_11;
	// System.Int32 UITextList::mTotalLines
	int32_t ___mTotalLines_12;

public:
	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___style_2)); }
	inline int32_t get_style_2() const { return ___style_2; }
	inline int32_t* get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(int32_t value)
	{
		___style_2 = value;
	}

	inline static int32_t get_offset_of_textLabel_3() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___textLabel_3)); }
	inline UILabel_t3478074517 * get_textLabel_3() const { return ___textLabel_3; }
	inline UILabel_t3478074517 ** get_address_of_textLabel_3() { return &___textLabel_3; }
	inline void set_textLabel_3(UILabel_t3478074517 * value)
	{
		___textLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___textLabel_3), value);
	}

	inline static int32_t get_offset_of_maxWidth_4() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___maxWidth_4)); }
	inline float get_maxWidth_4() const { return ___maxWidth_4; }
	inline float* get_address_of_maxWidth_4() { return &___maxWidth_4; }
	inline void set_maxWidth_4(float value)
	{
		___maxWidth_4 = value;
	}

	inline static int32_t get_offset_of_maxHeight_5() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___maxHeight_5)); }
	inline float get_maxHeight_5() const { return ___maxHeight_5; }
	inline float* get_address_of_maxHeight_5() { return &___maxHeight_5; }
	inline void set_maxHeight_5(float value)
	{
		___maxHeight_5 = value;
	}

	inline static int32_t get_offset_of_maxEntries_6() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___maxEntries_6)); }
	inline int32_t get_maxEntries_6() const { return ___maxEntries_6; }
	inline int32_t* get_address_of_maxEntries_6() { return &___maxEntries_6; }
	inline void set_maxEntries_6(int32_t value)
	{
		___maxEntries_6 = value;
	}

	inline static int32_t get_offset_of_supportScrollWheel_7() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___supportScrollWheel_7)); }
	inline bool get_supportScrollWheel_7() const { return ___supportScrollWheel_7; }
	inline bool* get_address_of_supportScrollWheel_7() { return &___supportScrollWheel_7; }
	inline void set_supportScrollWheel_7(bool value)
	{
		___supportScrollWheel_7 = value;
	}

	inline static int32_t get_offset_of_mSeparator_8() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___mSeparator_8)); }
	inline CharU5BU5D_t3419619864* get_mSeparator_8() const { return ___mSeparator_8; }
	inline CharU5BU5D_t3419619864** get_address_of_mSeparator_8() { return &___mSeparator_8; }
	inline void set_mSeparator_8(CharU5BU5D_t3419619864* value)
	{
		___mSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((&___mSeparator_8), value);
	}

	inline static int32_t get_offset_of_mParagraphs_9() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___mParagraphs_9)); }
	inline List_1_t2870954769 * get_mParagraphs_9() const { return ___mParagraphs_9; }
	inline List_1_t2870954769 ** get_address_of_mParagraphs_9() { return &___mParagraphs_9; }
	inline void set_mParagraphs_9(List_1_t2870954769 * value)
	{
		___mParagraphs_9 = value;
		Il2CppCodeGenWriteBarrier((&___mParagraphs_9), value);
	}

	inline static int32_t get_offset_of_mScroll_10() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___mScroll_10)); }
	inline float get_mScroll_10() const { return ___mScroll_10; }
	inline float* get_address_of_mScroll_10() { return &___mScroll_10; }
	inline void set_mScroll_10(float value)
	{
		___mScroll_10 = value;
	}

	inline static int32_t get_offset_of_mSelected_11() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___mSelected_11)); }
	inline bool get_mSelected_11() const { return ___mSelected_11; }
	inline bool* get_address_of_mSelected_11() { return &___mSelected_11; }
	inline void set_mSelected_11(bool value)
	{
		___mSelected_11 = value;
	}

	inline static int32_t get_offset_of_mTotalLines_12() { return static_cast<int32_t>(offsetof(UITextList_t1100452823, ___mTotalLines_12)); }
	inline int32_t get_mTotalLines_12() const { return ___mTotalLines_12; }
	inline int32_t* get_address_of_mTotalLines_12() { return &___mTotalLines_12; }
	inline void set_mTotalLines_12(int32_t value)
	{
		___mTotalLines_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTLIST_T1100452823_H
#ifndef UIVIEWPORT_T3844485370_H
#define UIVIEWPORT_T3844485370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIViewport
struct  UIViewport_t3844485370  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera UIViewport::sourceCamera
	Camera_t2839736942 * ___sourceCamera_2;
	// UnityEngine.Transform UIViewport::topLeft
	Transform_t362059596 * ___topLeft_3;
	// UnityEngine.Transform UIViewport::bottomRight
	Transform_t362059596 * ___bottomRight_4;
	// System.Single UIViewport::fullSize
	float ___fullSize_5;
	// UnityEngine.Camera UIViewport::mCam
	Camera_t2839736942 * ___mCam_6;

public:
	inline static int32_t get_offset_of_sourceCamera_2() { return static_cast<int32_t>(offsetof(UIViewport_t3844485370, ___sourceCamera_2)); }
	inline Camera_t2839736942 * get_sourceCamera_2() const { return ___sourceCamera_2; }
	inline Camera_t2839736942 ** get_address_of_sourceCamera_2() { return &___sourceCamera_2; }
	inline void set_sourceCamera_2(Camera_t2839736942 * value)
	{
		___sourceCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceCamera_2), value);
	}

	inline static int32_t get_offset_of_topLeft_3() { return static_cast<int32_t>(offsetof(UIViewport_t3844485370, ___topLeft_3)); }
	inline Transform_t362059596 * get_topLeft_3() const { return ___topLeft_3; }
	inline Transform_t362059596 ** get_address_of_topLeft_3() { return &___topLeft_3; }
	inline void set_topLeft_3(Transform_t362059596 * value)
	{
		___topLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___topLeft_3), value);
	}

	inline static int32_t get_offset_of_bottomRight_4() { return static_cast<int32_t>(offsetof(UIViewport_t3844485370, ___bottomRight_4)); }
	inline Transform_t362059596 * get_bottomRight_4() const { return ___bottomRight_4; }
	inline Transform_t362059596 ** get_address_of_bottomRight_4() { return &___bottomRight_4; }
	inline void set_bottomRight_4(Transform_t362059596 * value)
	{
		___bottomRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomRight_4), value);
	}

	inline static int32_t get_offset_of_fullSize_5() { return static_cast<int32_t>(offsetof(UIViewport_t3844485370, ___fullSize_5)); }
	inline float get_fullSize_5() const { return ___fullSize_5; }
	inline float* get_address_of_fullSize_5() { return &___fullSize_5; }
	inline void set_fullSize_5(float value)
	{
		___fullSize_5 = value;
	}

	inline static int32_t get_offset_of_mCam_6() { return static_cast<int32_t>(offsetof(UIViewport_t3844485370, ___mCam_6)); }
	inline Camera_t2839736942 * get_mCam_6() const { return ___mCam_6; }
	inline Camera_t2839736942 ** get_address_of_mCam_6() { return &___mCam_6; }
	inline void set_mCam_6(Camera_t2839736942 * value)
	{
		___mCam_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWPORT_T3844485370_H
#ifndef OUTLINEOBJECT_T941793957_H
#define OUTLINEOBJECT_T941793957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OutlineObject
struct  OutlineObject_t941793957  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Material OutlineObject::mat
	Material_t2815264910 * ___mat_2;
	// System.Boolean OutlineObject::shaderOn
	bool ___shaderOn_3;
	// System.Boolean OutlineObject::enableOnAwake
	bool ___enableOnAwake_4;

public:
	inline static int32_t get_offset_of_mat_2() { return static_cast<int32_t>(offsetof(OutlineObject_t941793957, ___mat_2)); }
	inline Material_t2815264910 * get_mat_2() const { return ___mat_2; }
	inline Material_t2815264910 ** get_address_of_mat_2() { return &___mat_2; }
	inline void set_mat_2(Material_t2815264910 * value)
	{
		___mat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mat_2), value);
	}

	inline static int32_t get_offset_of_shaderOn_3() { return static_cast<int32_t>(offsetof(OutlineObject_t941793957, ___shaderOn_3)); }
	inline bool get_shaderOn_3() const { return ___shaderOn_3; }
	inline bool* get_address_of_shaderOn_3() { return &___shaderOn_3; }
	inline void set_shaderOn_3(bool value)
	{
		___shaderOn_3 = value;
	}

	inline static int32_t get_offset_of_enableOnAwake_4() { return static_cast<int32_t>(offsetof(OutlineObject_t941793957, ___enableOnAwake_4)); }
	inline bool get_enableOnAwake_4() const { return ___enableOnAwake_4; }
	inline bool* get_address_of_enableOnAwake_4() { return &___enableOnAwake_4; }
	inline void set_enableOnAwake_4(bool value)
	{
		___enableOnAwake_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINEOBJECT_T941793957_H
#ifndef PICKATHINGDIALOG_T1829493947_H
#define PICKATHINGDIALOG_T1829493947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickAThingDialog
struct  PickAThingDialog_t1829493947  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject PickAThingDialog::button
	GameObject_t2557347079 * ___button_2;
	// System.Single PickAThingDialog::buttonGap
	float ___buttonGap_3;
	// System.Single PickAThingDialog::offset
	float ___offset_4;
	// System.Int32 PickAThingDialog::numThings
	int32_t ___numThings_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PickAThingDialog::buttons
	List_1_t1676974086 * ___buttons_6;
	// PickAThingDialog/PickAThingCallback PickAThingDialog::callback
	PickAThingCallback_t4122863115 * ___callback_7;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___button_2)); }
	inline GameObject_t2557347079 * get_button_2() const { return ___button_2; }
	inline GameObject_t2557347079 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(GameObject_t2557347079 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_buttonGap_3() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___buttonGap_3)); }
	inline float get_buttonGap_3() const { return ___buttonGap_3; }
	inline float* get_address_of_buttonGap_3() { return &___buttonGap_3; }
	inline void set_buttonGap_3(float value)
	{
		___buttonGap_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___offset_4)); }
	inline float get_offset_4() const { return ___offset_4; }
	inline float* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(float value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_numThings_5() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___numThings_5)); }
	inline int32_t get_numThings_5() const { return ___numThings_5; }
	inline int32_t* get_address_of_numThings_5() { return &___numThings_5; }
	inline void set_numThings_5(int32_t value)
	{
		___numThings_5 = value;
	}

	inline static int32_t get_offset_of_buttons_6() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___buttons_6)); }
	inline List_1_t1676974086 * get_buttons_6() const { return ___buttons_6; }
	inline List_1_t1676974086 ** get_address_of_buttons_6() { return &___buttons_6; }
	inline void set_buttons_6(List_1_t1676974086 * value)
	{
		___buttons_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_6), value);
	}

	inline static int32_t get_offset_of_callback_7() { return static_cast<int32_t>(offsetof(PickAThingDialog_t1829493947, ___callback_7)); }
	inline PickAThingCallback_t4122863115 * get_callback_7() const { return ___callback_7; }
	inline PickAThingCallback_t4122863115 ** get_address_of_callback_7() { return &___callback_7; }
	inline void set_callback_7(PickAThingCallback_t4122863115 * value)
	{
		___callback_7 = value;
		Il2CppCodeGenWriteBarrier((&___callback_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKATHINGDIALOG_T1829493947_H
#ifndef UIPANEL_T825970685_H
#define UIPANEL_T825970685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel
struct  UIPanel_t825970685  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UIPanel::showInPanelTool
	bool ___showInPanelTool_3;
	// System.Boolean UIPanel::generateNormals
	bool ___generateNormals_4;
	// System.Boolean UIPanel::depthPass
	bool ___depthPass_5;
	// System.Boolean UIPanel::widgetsAreStatic
	bool ___widgetsAreStatic_6;
	// UIPanel/DebugInfo UIPanel::mDebugInfo
	int32_t ___mDebugInfo_7;
	// UIDrawCall/Clipping UIPanel::mClipping
	int32_t ___mClipping_8;
	// UnityEngine.Vector4 UIPanel::mClipRange
	Vector4_t2436299922  ___mClipRange_9;
	// UnityEngine.Vector2 UIPanel::mClipSoftness
	Vector2_t328513675  ___mClipSoftness_10;
	// System.Collections.Specialized.OrderedDictionary UIPanel::mChildren
	OrderedDictionary_t648530623 * ___mChildren_11;
	// BetterList`1<UIWidget> UIPanel::mWidgets
	BetterList_1_t416469230 * ___mWidgets_12;
	// BetterList`1<UnityEngine.Material> UIPanel::mChanged
	BetterList_1_t1270633999 * ___mChanged_13;
	// BetterList`1<UIDrawCall> UIPanel::mDrawCalls
	BetterList_1_t1143290928 * ___mDrawCalls_14;
	// BetterList`1<UnityEngine.Vector3> UIPanel::mVerts
	BetterList_1_t442302241 * ___mVerts_15;
	// BetterList`1<UnityEngine.Vector3> UIPanel::mNorms
	BetterList_1_t442302241 * ___mNorms_16;
	// BetterList`1<UnityEngine.Vector4> UIPanel::mTans
	BetterList_1_t891669011 * ___mTans_17;
	// BetterList`1<UnityEngine.Vector2> UIPanel::mUvs
	BetterList_1_t3078850060 * ___mUvs_18;
	// BetterList`1<UnityEngine.Color32> UIPanel::mCols
	BetterList_1_t1883882744 * ___mCols_19;
	// UnityEngine.Transform UIPanel::mTrans
	Transform_t362059596 * ___mTrans_20;
	// UnityEngine.Camera UIPanel::mCam
	Camera_t2839736942 * ___mCam_21;
	// System.Int32 UIPanel::mLayer
	int32_t ___mLayer_22;
	// System.Boolean UIPanel::mDepthChanged
	bool ___mDepthChanged_23;
	// System.Boolean UIPanel::mRebuildAll
	bool ___mRebuildAll_24;
	// System.Boolean UIPanel::mChangedLastFrame
	bool ___mChangedLastFrame_25;
	// System.Boolean UIPanel::mWidgetsAdded
	bool ___mWidgetsAdded_26;
	// System.Single UIPanel::mMatrixTime
	float ___mMatrixTime_27;
	// UnityEngine.Matrix4x4 UIPanel::mWorldToLocal
	Matrix4x4_t1237934469  ___mWorldToLocal_28;
	// UnityEngine.Vector2 UIPanel::mMin
	Vector2_t328513675  ___mMin_30;
	// UnityEngine.Vector2 UIPanel::mMax
	Vector2_t328513675  ___mMax_31;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UIPanel::mRemoved
	List_1_t3776653899 * ___mRemoved_32;
	// System.Boolean UIPanel::mCheckVisibility
	bool ___mCheckVisibility_33;
	// System.Single UIPanel::mCullTime
	float ___mCullTime_34;
	// System.Boolean UIPanel::mCulled
	bool ___mCulled_35;

public:
	inline static int32_t get_offset_of_showInPanelTool_3() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___showInPanelTool_3)); }
	inline bool get_showInPanelTool_3() const { return ___showInPanelTool_3; }
	inline bool* get_address_of_showInPanelTool_3() { return &___showInPanelTool_3; }
	inline void set_showInPanelTool_3(bool value)
	{
		___showInPanelTool_3 = value;
	}

	inline static int32_t get_offset_of_generateNormals_4() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___generateNormals_4)); }
	inline bool get_generateNormals_4() const { return ___generateNormals_4; }
	inline bool* get_address_of_generateNormals_4() { return &___generateNormals_4; }
	inline void set_generateNormals_4(bool value)
	{
		___generateNormals_4 = value;
	}

	inline static int32_t get_offset_of_depthPass_5() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___depthPass_5)); }
	inline bool get_depthPass_5() const { return ___depthPass_5; }
	inline bool* get_address_of_depthPass_5() { return &___depthPass_5; }
	inline void set_depthPass_5(bool value)
	{
		___depthPass_5 = value;
	}

	inline static int32_t get_offset_of_widgetsAreStatic_6() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___widgetsAreStatic_6)); }
	inline bool get_widgetsAreStatic_6() const { return ___widgetsAreStatic_6; }
	inline bool* get_address_of_widgetsAreStatic_6() { return &___widgetsAreStatic_6; }
	inline void set_widgetsAreStatic_6(bool value)
	{
		___widgetsAreStatic_6 = value;
	}

	inline static int32_t get_offset_of_mDebugInfo_7() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mDebugInfo_7)); }
	inline int32_t get_mDebugInfo_7() const { return ___mDebugInfo_7; }
	inline int32_t* get_address_of_mDebugInfo_7() { return &___mDebugInfo_7; }
	inline void set_mDebugInfo_7(int32_t value)
	{
		___mDebugInfo_7 = value;
	}

	inline static int32_t get_offset_of_mClipping_8() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mClipping_8)); }
	inline int32_t get_mClipping_8() const { return ___mClipping_8; }
	inline int32_t* get_address_of_mClipping_8() { return &___mClipping_8; }
	inline void set_mClipping_8(int32_t value)
	{
		___mClipping_8 = value;
	}

	inline static int32_t get_offset_of_mClipRange_9() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mClipRange_9)); }
	inline Vector4_t2436299922  get_mClipRange_9() const { return ___mClipRange_9; }
	inline Vector4_t2436299922 * get_address_of_mClipRange_9() { return &___mClipRange_9; }
	inline void set_mClipRange_9(Vector4_t2436299922  value)
	{
		___mClipRange_9 = value;
	}

	inline static int32_t get_offset_of_mClipSoftness_10() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mClipSoftness_10)); }
	inline Vector2_t328513675  get_mClipSoftness_10() const { return ___mClipSoftness_10; }
	inline Vector2_t328513675 * get_address_of_mClipSoftness_10() { return &___mClipSoftness_10; }
	inline void set_mClipSoftness_10(Vector2_t328513675  value)
	{
		___mClipSoftness_10 = value;
	}

	inline static int32_t get_offset_of_mChildren_11() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mChildren_11)); }
	inline OrderedDictionary_t648530623 * get_mChildren_11() const { return ___mChildren_11; }
	inline OrderedDictionary_t648530623 ** get_address_of_mChildren_11() { return &___mChildren_11; }
	inline void set_mChildren_11(OrderedDictionary_t648530623 * value)
	{
		___mChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_11), value);
	}

	inline static int32_t get_offset_of_mWidgets_12() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mWidgets_12)); }
	inline BetterList_1_t416469230 * get_mWidgets_12() const { return ___mWidgets_12; }
	inline BetterList_1_t416469230 ** get_address_of_mWidgets_12() { return &___mWidgets_12; }
	inline void set_mWidgets_12(BetterList_1_t416469230 * value)
	{
		___mWidgets_12 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_12), value);
	}

	inline static int32_t get_offset_of_mChanged_13() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mChanged_13)); }
	inline BetterList_1_t1270633999 * get_mChanged_13() const { return ___mChanged_13; }
	inline BetterList_1_t1270633999 ** get_address_of_mChanged_13() { return &___mChanged_13; }
	inline void set_mChanged_13(BetterList_1_t1270633999 * value)
	{
		___mChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&___mChanged_13), value);
	}

	inline static int32_t get_offset_of_mDrawCalls_14() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mDrawCalls_14)); }
	inline BetterList_1_t1143290928 * get_mDrawCalls_14() const { return ___mDrawCalls_14; }
	inline BetterList_1_t1143290928 ** get_address_of_mDrawCalls_14() { return &___mDrawCalls_14; }
	inline void set_mDrawCalls_14(BetterList_1_t1143290928 * value)
	{
		___mDrawCalls_14 = value;
		Il2CppCodeGenWriteBarrier((&___mDrawCalls_14), value);
	}

	inline static int32_t get_offset_of_mVerts_15() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mVerts_15)); }
	inline BetterList_1_t442302241 * get_mVerts_15() const { return ___mVerts_15; }
	inline BetterList_1_t442302241 ** get_address_of_mVerts_15() { return &___mVerts_15; }
	inline void set_mVerts_15(BetterList_1_t442302241 * value)
	{
		___mVerts_15 = value;
		Il2CppCodeGenWriteBarrier((&___mVerts_15), value);
	}

	inline static int32_t get_offset_of_mNorms_16() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mNorms_16)); }
	inline BetterList_1_t442302241 * get_mNorms_16() const { return ___mNorms_16; }
	inline BetterList_1_t442302241 ** get_address_of_mNorms_16() { return &___mNorms_16; }
	inline void set_mNorms_16(BetterList_1_t442302241 * value)
	{
		___mNorms_16 = value;
		Il2CppCodeGenWriteBarrier((&___mNorms_16), value);
	}

	inline static int32_t get_offset_of_mTans_17() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mTans_17)); }
	inline BetterList_1_t891669011 * get_mTans_17() const { return ___mTans_17; }
	inline BetterList_1_t891669011 ** get_address_of_mTans_17() { return &___mTans_17; }
	inline void set_mTans_17(BetterList_1_t891669011 * value)
	{
		___mTans_17 = value;
		Il2CppCodeGenWriteBarrier((&___mTans_17), value);
	}

	inline static int32_t get_offset_of_mUvs_18() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mUvs_18)); }
	inline BetterList_1_t3078850060 * get_mUvs_18() const { return ___mUvs_18; }
	inline BetterList_1_t3078850060 ** get_address_of_mUvs_18() { return &___mUvs_18; }
	inline void set_mUvs_18(BetterList_1_t3078850060 * value)
	{
		___mUvs_18 = value;
		Il2CppCodeGenWriteBarrier((&___mUvs_18), value);
	}

	inline static int32_t get_offset_of_mCols_19() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mCols_19)); }
	inline BetterList_1_t1883882744 * get_mCols_19() const { return ___mCols_19; }
	inline BetterList_1_t1883882744 ** get_address_of_mCols_19() { return &___mCols_19; }
	inline void set_mCols_19(BetterList_1_t1883882744 * value)
	{
		___mCols_19 = value;
		Il2CppCodeGenWriteBarrier((&___mCols_19), value);
	}

	inline static int32_t get_offset_of_mTrans_20() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mTrans_20)); }
	inline Transform_t362059596 * get_mTrans_20() const { return ___mTrans_20; }
	inline Transform_t362059596 ** get_address_of_mTrans_20() { return &___mTrans_20; }
	inline void set_mTrans_20(Transform_t362059596 * value)
	{
		___mTrans_20 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_20), value);
	}

	inline static int32_t get_offset_of_mCam_21() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mCam_21)); }
	inline Camera_t2839736942 * get_mCam_21() const { return ___mCam_21; }
	inline Camera_t2839736942 ** get_address_of_mCam_21() { return &___mCam_21; }
	inline void set_mCam_21(Camera_t2839736942 * value)
	{
		___mCam_21 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_21), value);
	}

	inline static int32_t get_offset_of_mLayer_22() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mLayer_22)); }
	inline int32_t get_mLayer_22() const { return ___mLayer_22; }
	inline int32_t* get_address_of_mLayer_22() { return &___mLayer_22; }
	inline void set_mLayer_22(int32_t value)
	{
		___mLayer_22 = value;
	}

	inline static int32_t get_offset_of_mDepthChanged_23() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mDepthChanged_23)); }
	inline bool get_mDepthChanged_23() const { return ___mDepthChanged_23; }
	inline bool* get_address_of_mDepthChanged_23() { return &___mDepthChanged_23; }
	inline void set_mDepthChanged_23(bool value)
	{
		___mDepthChanged_23 = value;
	}

	inline static int32_t get_offset_of_mRebuildAll_24() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mRebuildAll_24)); }
	inline bool get_mRebuildAll_24() const { return ___mRebuildAll_24; }
	inline bool* get_address_of_mRebuildAll_24() { return &___mRebuildAll_24; }
	inline void set_mRebuildAll_24(bool value)
	{
		___mRebuildAll_24 = value;
	}

	inline static int32_t get_offset_of_mChangedLastFrame_25() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mChangedLastFrame_25)); }
	inline bool get_mChangedLastFrame_25() const { return ___mChangedLastFrame_25; }
	inline bool* get_address_of_mChangedLastFrame_25() { return &___mChangedLastFrame_25; }
	inline void set_mChangedLastFrame_25(bool value)
	{
		___mChangedLastFrame_25 = value;
	}

	inline static int32_t get_offset_of_mWidgetsAdded_26() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mWidgetsAdded_26)); }
	inline bool get_mWidgetsAdded_26() const { return ___mWidgetsAdded_26; }
	inline bool* get_address_of_mWidgetsAdded_26() { return &___mWidgetsAdded_26; }
	inline void set_mWidgetsAdded_26(bool value)
	{
		___mWidgetsAdded_26 = value;
	}

	inline static int32_t get_offset_of_mMatrixTime_27() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mMatrixTime_27)); }
	inline float get_mMatrixTime_27() const { return ___mMatrixTime_27; }
	inline float* get_address_of_mMatrixTime_27() { return &___mMatrixTime_27; }
	inline void set_mMatrixTime_27(float value)
	{
		___mMatrixTime_27 = value;
	}

	inline static int32_t get_offset_of_mWorldToLocal_28() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mWorldToLocal_28)); }
	inline Matrix4x4_t1237934469  get_mWorldToLocal_28() const { return ___mWorldToLocal_28; }
	inline Matrix4x4_t1237934469 * get_address_of_mWorldToLocal_28() { return &___mWorldToLocal_28; }
	inline void set_mWorldToLocal_28(Matrix4x4_t1237934469  value)
	{
		___mWorldToLocal_28 = value;
	}

	inline static int32_t get_offset_of_mMin_30() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mMin_30)); }
	inline Vector2_t328513675  get_mMin_30() const { return ___mMin_30; }
	inline Vector2_t328513675 * get_address_of_mMin_30() { return &___mMin_30; }
	inline void set_mMin_30(Vector2_t328513675  value)
	{
		___mMin_30 = value;
	}

	inline static int32_t get_offset_of_mMax_31() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mMax_31)); }
	inline Vector2_t328513675  get_mMax_31() const { return ___mMax_31; }
	inline Vector2_t328513675 * get_address_of_mMax_31() { return &___mMax_31; }
	inline void set_mMax_31(Vector2_t328513675  value)
	{
		___mMax_31 = value;
	}

	inline static int32_t get_offset_of_mRemoved_32() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mRemoved_32)); }
	inline List_1_t3776653899 * get_mRemoved_32() const { return ___mRemoved_32; }
	inline List_1_t3776653899 ** get_address_of_mRemoved_32() { return &___mRemoved_32; }
	inline void set_mRemoved_32(List_1_t3776653899 * value)
	{
		___mRemoved_32 = value;
		Il2CppCodeGenWriteBarrier((&___mRemoved_32), value);
	}

	inline static int32_t get_offset_of_mCheckVisibility_33() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mCheckVisibility_33)); }
	inline bool get_mCheckVisibility_33() const { return ___mCheckVisibility_33; }
	inline bool* get_address_of_mCheckVisibility_33() { return &___mCheckVisibility_33; }
	inline void set_mCheckVisibility_33(bool value)
	{
		___mCheckVisibility_33 = value;
	}

	inline static int32_t get_offset_of_mCullTime_34() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mCullTime_34)); }
	inline float get_mCullTime_34() const { return ___mCullTime_34; }
	inline float* get_address_of_mCullTime_34() { return &___mCullTime_34; }
	inline void set_mCullTime_34(float value)
	{
		___mCullTime_34 = value;
	}

	inline static int32_t get_offset_of_mCulled_35() { return static_cast<int32_t>(offsetof(UIPanel_t825970685, ___mCulled_35)); }
	inline bool get_mCulled_35() const { return ___mCulled_35; }
	inline bool* get_address_of_mCulled_35() { return &___mCulled_35; }
	inline void set_mCulled_35(bool value)
	{
		___mCulled_35 = value;
	}
};

struct UIPanel_t825970685_StaticFields
{
public:
	// UIPanel UIPanel::instanceRef
	UIPanel_t825970685 * ___instanceRef_2;
	// System.Single[] UIPanel::mTemp
	SingleU5BU5D_t2843050510* ___mTemp_29;
	// BetterList`1<UINode> UIPanel::mHierarchy
	BetterList_1_t3635274674 * ___mHierarchy_36;
	// System.Comparison`1<UIWidget> UIPanel::<>f__mg$cache0
	Comparison_1_t3345157994 * ___U3CU3Ef__mgU24cache0_37;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(UIPanel_t825970685_StaticFields, ___instanceRef_2)); }
	inline UIPanel_t825970685 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline UIPanel_t825970685 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(UIPanel_t825970685 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}

	inline static int32_t get_offset_of_mTemp_29() { return static_cast<int32_t>(offsetof(UIPanel_t825970685_StaticFields, ___mTemp_29)); }
	inline SingleU5BU5D_t2843050510* get_mTemp_29() const { return ___mTemp_29; }
	inline SingleU5BU5D_t2843050510** get_address_of_mTemp_29() { return &___mTemp_29; }
	inline void set_mTemp_29(SingleU5BU5D_t2843050510* value)
	{
		___mTemp_29 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_29), value);
	}

	inline static int32_t get_offset_of_mHierarchy_36() { return static_cast<int32_t>(offsetof(UIPanel_t825970685_StaticFields, ___mHierarchy_36)); }
	inline BetterList_1_t3635274674 * get_mHierarchy_36() const { return ___mHierarchy_36; }
	inline BetterList_1_t3635274674 ** get_address_of_mHierarchy_36() { return &___mHierarchy_36; }
	inline void set_mHierarchy_36(BetterList_1_t3635274674 * value)
	{
		___mHierarchy_36 = value;
		Il2CppCodeGenWriteBarrier((&___mHierarchy_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_37() { return static_cast<int32_t>(offsetof(UIPanel_t825970685_StaticFields, ___U3CU3Ef__mgU24cache0_37)); }
	inline Comparison_1_t3345157994 * get_U3CU3Ef__mgU24cache0_37() const { return ___U3CU3Ef__mgU24cache0_37; }
	inline Comparison_1_t3345157994 ** get_address_of_U3CU3Ef__mgU24cache0_37() { return &___U3CU3Ef__mgU24cache0_37; }
	inline void set_U3CU3Ef__mgU24cache0_37(Comparison_1_t3345157994 * value)
	{
		___U3CU3Ef__mgU24cache0_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPANEL_T825970685_H
#ifndef UIORTHOCAMERA_T3857640393_H
#define UIORTHOCAMERA_T3857640393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIOrthoCamera
struct  UIOrthoCamera_t3857640393  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera UIOrthoCamera::mCam
	Camera_t2839736942 * ___mCam_2;
	// UnityEngine.Transform UIOrthoCamera::mTrans
	Transform_t362059596 * ___mTrans_3;

public:
	inline static int32_t get_offset_of_mCam_2() { return static_cast<int32_t>(offsetof(UIOrthoCamera_t3857640393, ___mCam_2)); }
	inline Camera_t2839736942 * get_mCam_2() const { return ___mCam_2; }
	inline Camera_t2839736942 ** get_address_of_mCam_2() { return &___mCam_2; }
	inline void set_mCam_2(Camera_t2839736942 * value)
	{
		___mCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_2), value);
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(UIOrthoCamera_t3857640393, ___mTrans_3)); }
	inline Transform_t362059596 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t362059596 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t362059596 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIORTHOCAMERA_T3857640393_H
#ifndef UISPRITEANIMATION_T1906024296_H
#define UISPRITEANIMATION_T1906024296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISpriteAnimation
struct  UISpriteAnimation_t1906024296  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 UISpriteAnimation::mFPS
	int32_t ___mFPS_2;
	// System.String UISpriteAnimation::mPrefix
	String_t* ___mPrefix_3;
	// System.Boolean UISpriteAnimation::mLoop
	bool ___mLoop_4;
	// UISprite UISpriteAnimation::mSprite
	UISprite_t279728715 * ___mSprite_5;
	// System.Single UISpriteAnimation::mDelta
	float ___mDelta_6;
	// System.Int32 UISpriteAnimation::mIndex
	int32_t ___mIndex_7;
	// System.Boolean UISpriteAnimation::mActive
	bool ___mActive_8;
	// System.Collections.Generic.List`1<System.String> UISpriteAnimation::mSpriteNames
	List_1_t4069179741 * ___mSpriteNames_9;

public:
	inline static int32_t get_offset_of_mFPS_2() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mFPS_2)); }
	inline int32_t get_mFPS_2() const { return ___mFPS_2; }
	inline int32_t* get_address_of_mFPS_2() { return &___mFPS_2; }
	inline void set_mFPS_2(int32_t value)
	{
		___mFPS_2 = value;
	}

	inline static int32_t get_offset_of_mPrefix_3() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mPrefix_3)); }
	inline String_t* get_mPrefix_3() const { return ___mPrefix_3; }
	inline String_t** get_address_of_mPrefix_3() { return &___mPrefix_3; }
	inline void set_mPrefix_3(String_t* value)
	{
		___mPrefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPrefix_3), value);
	}

	inline static int32_t get_offset_of_mLoop_4() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mLoop_4)); }
	inline bool get_mLoop_4() const { return ___mLoop_4; }
	inline bool* get_address_of_mLoop_4() { return &___mLoop_4; }
	inline void set_mLoop_4(bool value)
	{
		___mLoop_4 = value;
	}

	inline static int32_t get_offset_of_mSprite_5() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mSprite_5)); }
	inline UISprite_t279728715 * get_mSprite_5() const { return ___mSprite_5; }
	inline UISprite_t279728715 ** get_address_of_mSprite_5() { return &___mSprite_5; }
	inline void set_mSprite_5(UISprite_t279728715 * value)
	{
		___mSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_5), value);
	}

	inline static int32_t get_offset_of_mDelta_6() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mDelta_6)); }
	inline float get_mDelta_6() const { return ___mDelta_6; }
	inline float* get_address_of_mDelta_6() { return &___mDelta_6; }
	inline void set_mDelta_6(float value)
	{
		___mDelta_6 = value;
	}

	inline static int32_t get_offset_of_mIndex_7() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mIndex_7)); }
	inline int32_t get_mIndex_7() const { return ___mIndex_7; }
	inline int32_t* get_address_of_mIndex_7() { return &___mIndex_7; }
	inline void set_mIndex_7(int32_t value)
	{
		___mIndex_7 = value;
	}

	inline static int32_t get_offset_of_mActive_8() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mActive_8)); }
	inline bool get_mActive_8() const { return ___mActive_8; }
	inline bool* get_address_of_mActive_8() { return &___mActive_8; }
	inline void set_mActive_8(bool value)
	{
		___mActive_8 = value;
	}

	inline static int32_t get_offset_of_mSpriteNames_9() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1906024296, ___mSpriteNames_9)); }
	inline List_1_t4069179741 * get_mSpriteNames_9() const { return ___mSpriteNames_9; }
	inline List_1_t4069179741 ** get_address_of_mSpriteNames_9() { return &___mSpriteNames_9; }
	inline void set_mSpriteNames_9(List_1_t4069179741 * value)
	{
		___mSpriteNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteNames_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITEANIMATION_T1906024296_H
#ifndef UISTRETCH_T950945497_H
#define UISTRETCH_T950945497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStretch
struct  UIStretch_t950945497  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera UIStretch::uiCamera
	Camera_t2839736942 * ___uiCamera_2;
	// UIWidget UIStretch::widgetContainer
	UIWidget_t1961100141 * ___widgetContainer_3;
	// UIPanel UIStretch::panelContainer
	UIPanel_t825970685 * ___panelContainer_4;
	// UIStretch/Style UIStretch::style
	int32_t ___style_5;
	// UnityEngine.Vector2 UIStretch::relativeSize
	Vector2_t328513675  ___relativeSize_6;
	// UnityEngine.Transform UIStretch::mTrans
	Transform_t362059596 * ___mTrans_7;
	// UIRoot UIStretch::mRoot
	UIRoot_t937406327 * ___mRoot_8;
	// UnityEngine.Animation UIStretch::mAnim
	Animation_t3821138400 * ___mAnim_9;

public:
	inline static int32_t get_offset_of_uiCamera_2() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___uiCamera_2)); }
	inline Camera_t2839736942 * get_uiCamera_2() const { return ___uiCamera_2; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_2() { return &___uiCamera_2; }
	inline void set_uiCamera_2(Camera_t2839736942 * value)
	{
		___uiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_2), value);
	}

	inline static int32_t get_offset_of_widgetContainer_3() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___widgetContainer_3)); }
	inline UIWidget_t1961100141 * get_widgetContainer_3() const { return ___widgetContainer_3; }
	inline UIWidget_t1961100141 ** get_address_of_widgetContainer_3() { return &___widgetContainer_3; }
	inline void set_widgetContainer_3(UIWidget_t1961100141 * value)
	{
		___widgetContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___widgetContainer_3), value);
	}

	inline static int32_t get_offset_of_panelContainer_4() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___panelContainer_4)); }
	inline UIPanel_t825970685 * get_panelContainer_4() const { return ___panelContainer_4; }
	inline UIPanel_t825970685 ** get_address_of_panelContainer_4() { return &___panelContainer_4; }
	inline void set_panelContainer_4(UIPanel_t825970685 * value)
	{
		___panelContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_4), value);
	}

	inline static int32_t get_offset_of_style_5() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___style_5)); }
	inline int32_t get_style_5() const { return ___style_5; }
	inline int32_t* get_address_of_style_5() { return &___style_5; }
	inline void set_style_5(int32_t value)
	{
		___style_5 = value;
	}

	inline static int32_t get_offset_of_relativeSize_6() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___relativeSize_6)); }
	inline Vector2_t328513675  get_relativeSize_6() const { return ___relativeSize_6; }
	inline Vector2_t328513675 * get_address_of_relativeSize_6() { return &___relativeSize_6; }
	inline void set_relativeSize_6(Vector2_t328513675  value)
	{
		___relativeSize_6 = value;
	}

	inline static int32_t get_offset_of_mTrans_7() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___mTrans_7)); }
	inline Transform_t362059596 * get_mTrans_7() const { return ___mTrans_7; }
	inline Transform_t362059596 ** get_address_of_mTrans_7() { return &___mTrans_7; }
	inline void set_mTrans_7(Transform_t362059596 * value)
	{
		___mTrans_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_7), value);
	}

	inline static int32_t get_offset_of_mRoot_8() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___mRoot_8)); }
	inline UIRoot_t937406327 * get_mRoot_8() const { return ___mRoot_8; }
	inline UIRoot_t937406327 ** get_address_of_mRoot_8() { return &___mRoot_8; }
	inline void set_mRoot_8(UIRoot_t937406327 * value)
	{
		___mRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_8), value);
	}

	inline static int32_t get_offset_of_mAnim_9() { return static_cast<int32_t>(offsetof(UIStretch_t950945497, ___mAnim_9)); }
	inline Animation_t3821138400 * get_mAnim_9() const { return ___mAnim_9; }
	inline Animation_t3821138400 ** get_address_of_mAnim_9() { return &___mAnim_9; }
	inline void set_mAnim_9(Animation_t3821138400 * value)
	{
		___mAnim_9 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTRETCH_T950945497_H
#ifndef UIANCHOR_T1933351193_H
#define UIANCHOR_T1933351193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAnchor
struct  UIAnchor_t1933351193  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UIAnchor::mIsWindows
	bool ___mIsWindows_2;
	// UnityEngine.Camera UIAnchor::uiCamera
	Camera_t2839736942 * ___uiCamera_3;
	// UIWidget UIAnchor::widgetContainer
	UIWidget_t1961100141 * ___widgetContainer_4;
	// UIPanel UIAnchor::panelContainer
	UIPanel_t825970685 * ___panelContainer_5;
	// UIAnchor/Side UIAnchor::side
	int32_t ___side_6;
	// System.Boolean UIAnchor::halfPixelOffset
	bool ___halfPixelOffset_7;
	// System.Single UIAnchor::depthOffset
	float ___depthOffset_8;
	// UnityEngine.Vector2 UIAnchor::relativeOffset
	Vector2_t328513675  ___relativeOffset_9;
	// UnityEngine.Animation UIAnchor::mAnim
	Animation_t3821138400 * ___mAnim_10;
	// UIRoot UIAnchor::mRoot
	UIRoot_t937406327 * ___mRoot_11;

public:
	inline static int32_t get_offset_of_mIsWindows_2() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___mIsWindows_2)); }
	inline bool get_mIsWindows_2() const { return ___mIsWindows_2; }
	inline bool* get_address_of_mIsWindows_2() { return &___mIsWindows_2; }
	inline void set_mIsWindows_2(bool value)
	{
		___mIsWindows_2 = value;
	}

	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___uiCamera_3)); }
	inline Camera_t2839736942 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t2839736942 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_widgetContainer_4() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___widgetContainer_4)); }
	inline UIWidget_t1961100141 * get_widgetContainer_4() const { return ___widgetContainer_4; }
	inline UIWidget_t1961100141 ** get_address_of_widgetContainer_4() { return &___widgetContainer_4; }
	inline void set_widgetContainer_4(UIWidget_t1961100141 * value)
	{
		___widgetContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&___widgetContainer_4), value);
	}

	inline static int32_t get_offset_of_panelContainer_5() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___panelContainer_5)); }
	inline UIPanel_t825970685 * get_panelContainer_5() const { return ___panelContainer_5; }
	inline UIPanel_t825970685 ** get_address_of_panelContainer_5() { return &___panelContainer_5; }
	inline void set_panelContainer_5(UIPanel_t825970685 * value)
	{
		___panelContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_5), value);
	}

	inline static int32_t get_offset_of_side_6() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___side_6)); }
	inline int32_t get_side_6() const { return ___side_6; }
	inline int32_t* get_address_of_side_6() { return &___side_6; }
	inline void set_side_6(int32_t value)
	{
		___side_6 = value;
	}

	inline static int32_t get_offset_of_halfPixelOffset_7() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___halfPixelOffset_7)); }
	inline bool get_halfPixelOffset_7() const { return ___halfPixelOffset_7; }
	inline bool* get_address_of_halfPixelOffset_7() { return &___halfPixelOffset_7; }
	inline void set_halfPixelOffset_7(bool value)
	{
		___halfPixelOffset_7 = value;
	}

	inline static int32_t get_offset_of_depthOffset_8() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___depthOffset_8)); }
	inline float get_depthOffset_8() const { return ___depthOffset_8; }
	inline float* get_address_of_depthOffset_8() { return &___depthOffset_8; }
	inline void set_depthOffset_8(float value)
	{
		___depthOffset_8 = value;
	}

	inline static int32_t get_offset_of_relativeOffset_9() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___relativeOffset_9)); }
	inline Vector2_t328513675  get_relativeOffset_9() const { return ___relativeOffset_9; }
	inline Vector2_t328513675 * get_address_of_relativeOffset_9() { return &___relativeOffset_9; }
	inline void set_relativeOffset_9(Vector2_t328513675  value)
	{
		___relativeOffset_9 = value;
	}

	inline static int32_t get_offset_of_mAnim_10() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___mAnim_10)); }
	inline Animation_t3821138400 * get_mAnim_10() const { return ___mAnim_10; }
	inline Animation_t3821138400 ** get_address_of_mAnim_10() { return &___mAnim_10; }
	inline void set_mAnim_10(Animation_t3821138400 * value)
	{
		___mAnim_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_10), value);
	}

	inline static int32_t get_offset_of_mRoot_11() { return static_cast<int32_t>(offsetof(UIAnchor_t1933351193, ___mRoot_11)); }
	inline UIRoot_t937406327 * get_mRoot_11() const { return ___mRoot_11; }
	inline UIRoot_t937406327 ** get_address_of_mRoot_11() { return &___mRoot_11; }
	inline void set_mRoot_11(UIRoot_t937406327 * value)
	{
		___mRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANCHOR_T1933351193_H
#ifndef UILOCALIZE_T3299257004_H
#define UILOCALIZE_T3299257004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILocalize
struct  UILocalize_t3299257004  : public MonoBehaviour_t1618594486
{
public:
	// System.String UILocalize::key
	String_t* ___key_2;
	// System.String UILocalize::mLanguage
	String_t* ___mLanguage_3;
	// System.Boolean UILocalize::mStarted
	bool ___mStarted_4;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(UILocalize_t3299257004, ___key_2)); }
	inline String_t* get_key_2() const { return ___key_2; }
	inline String_t** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(String_t* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_mLanguage_3() { return static_cast<int32_t>(offsetof(UILocalize_t3299257004, ___mLanguage_3)); }
	inline String_t* get_mLanguage_3() const { return ___mLanguage_3; }
	inline String_t** get_address_of_mLanguage_3() { return &___mLanguage_3; }
	inline void set_mLanguage_3(String_t* value)
	{
		___mLanguage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLanguage_3), value);
	}

	inline static int32_t get_offset_of_mStarted_4() { return static_cast<int32_t>(offsetof(UILocalize_t3299257004, ___mStarted_4)); }
	inline bool get_mStarted_4() const { return ___mStarted_4; }
	inline bool* get_address_of_mStarted_4() { return &___mStarted_4; }
	inline void set_mStarted_4(bool value)
	{
		___mStarted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILOCALIZE_T3299257004_H
#ifndef UISPRITE_T279728715_H
#define UISPRITE_T279728715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISprite
struct  UISprite_t279728715  : public UIWidget_t1961100141
{
public:
	// UIAtlas UISprite::mAtlas
	UIAtlas_t1815452364 * ___mAtlas_16;
	// System.String UISprite::mSpriteName
	String_t* ___mSpriteName_17;
	// UIAtlas/Sprite UISprite::mSprite
	Sprite_t3039180268 * ___mSprite_18;
	// UnityEngine.Rect UISprite::mOuter
	Rect_t3039462994  ___mOuter_19;
	// UnityEngine.Rect UISprite::mOuterUV
	Rect_t3039462994  ___mOuterUV_20;
	// System.Boolean UISprite::mSpriteSet
	bool ___mSpriteSet_21;
	// System.String UISprite::mLastName
	String_t* ___mLastName_22;

public:
	inline static int32_t get_offset_of_mAtlas_16() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mAtlas_16)); }
	inline UIAtlas_t1815452364 * get_mAtlas_16() const { return ___mAtlas_16; }
	inline UIAtlas_t1815452364 ** get_address_of_mAtlas_16() { return &___mAtlas_16; }
	inline void set_mAtlas_16(UIAtlas_t1815452364 * value)
	{
		___mAtlas_16 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_16), value);
	}

	inline static int32_t get_offset_of_mSpriteName_17() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mSpriteName_17)); }
	inline String_t* get_mSpriteName_17() const { return ___mSpriteName_17; }
	inline String_t** get_address_of_mSpriteName_17() { return &___mSpriteName_17; }
	inline void set_mSpriteName_17(String_t* value)
	{
		___mSpriteName_17 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_17), value);
	}

	inline static int32_t get_offset_of_mSprite_18() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mSprite_18)); }
	inline Sprite_t3039180268 * get_mSprite_18() const { return ___mSprite_18; }
	inline Sprite_t3039180268 ** get_address_of_mSprite_18() { return &___mSprite_18; }
	inline void set_mSprite_18(Sprite_t3039180268 * value)
	{
		___mSprite_18 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_18), value);
	}

	inline static int32_t get_offset_of_mOuter_19() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mOuter_19)); }
	inline Rect_t3039462994  get_mOuter_19() const { return ___mOuter_19; }
	inline Rect_t3039462994 * get_address_of_mOuter_19() { return &___mOuter_19; }
	inline void set_mOuter_19(Rect_t3039462994  value)
	{
		___mOuter_19 = value;
	}

	inline static int32_t get_offset_of_mOuterUV_20() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mOuterUV_20)); }
	inline Rect_t3039462994  get_mOuterUV_20() const { return ___mOuterUV_20; }
	inline Rect_t3039462994 * get_address_of_mOuterUV_20() { return &___mOuterUV_20; }
	inline void set_mOuterUV_20(Rect_t3039462994  value)
	{
		___mOuterUV_20 = value;
	}

	inline static int32_t get_offset_of_mSpriteSet_21() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mSpriteSet_21)); }
	inline bool get_mSpriteSet_21() const { return ___mSpriteSet_21; }
	inline bool* get_address_of_mSpriteSet_21() { return &___mSpriteSet_21; }
	inline void set_mSpriteSet_21(bool value)
	{
		___mSpriteSet_21 = value;
	}

	inline static int32_t get_offset_of_mLastName_22() { return static_cast<int32_t>(offsetof(UISprite_t279728715, ___mLastName_22)); }
	inline String_t* get_mLastName_22() const { return ___mLastName_22; }
	inline String_t** get_address_of_mLastName_22() { return &___mLastName_22; }
	inline void set_mLastName_22(String_t* value)
	{
		___mLastName_22 = value;
		Il2CppCodeGenWriteBarrier((&___mLastName_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITE_T279728715_H
#ifndef UILABEL_T3478074517_H
#define UILABEL_T3478074517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel
struct  UILabel_t3478074517  : public UIWidget_t1961100141
{
public:
	// UIFont UILabel::mFont
	UIFont_t2730669065 * ___mFont_16;
	// System.String UILabel::mText
	String_t* ___mText_17;
	// System.Int32 UILabel::mMaxLineWidth
	int32_t ___mMaxLineWidth_18;
	// System.Boolean UILabel::mEncoding
	bool ___mEncoding_19;
	// System.Int32 UILabel::mMaxLineCount
	int32_t ___mMaxLineCount_20;
	// System.Boolean UILabel::mPassword
	bool ___mPassword_21;
	// System.Boolean UILabel::mShowLastChar
	bool ___mShowLastChar_22;
	// UILabel/Effect UILabel::mEffectStyle
	int32_t ___mEffectStyle_23;
	// UnityEngine.Color UILabel::mEffectColor
	Color_t2582018970  ___mEffectColor_24;
	// UIFont/SymbolStyle UILabel::mSymbols
	int32_t ___mSymbols_25;
	// UnityEngine.Vector2 UILabel::mEffectDistance
	Vector2_t328513675  ___mEffectDistance_26;
	// System.Single UILabel::mLineWidth
	float ___mLineWidth_27;
	// System.Boolean UILabel::mMultiline
	bool ___mMultiline_28;
	// System.Boolean UILabel::mShouldBeProcessed
	bool ___mShouldBeProcessed_29;
	// System.String UILabel::mProcessedText
	String_t* ___mProcessedText_30;
	// UnityEngine.Vector3 UILabel::mLastScale
	Vector3_t1986933152  ___mLastScale_31;
	// System.String UILabel::mLastText
	String_t* ___mLastText_32;
	// System.Int32 UILabel::mLastWidth
	int32_t ___mLastWidth_33;
	// System.Boolean UILabel::mLastEncoding
	bool ___mLastEncoding_34;
	// System.Int32 UILabel::mLastCount
	int32_t ___mLastCount_35;
	// System.Boolean UILabel::mLastPass
	bool ___mLastPass_36;
	// System.Boolean UILabel::mLastShow
	bool ___mLastShow_37;
	// UILabel/Effect UILabel::mLastEffect
	int32_t ___mLastEffect_38;
	// UnityEngine.Vector3 UILabel::mSize
	Vector3_t1986933152  ___mSize_39;

public:
	inline static int32_t get_offset_of_mFont_16() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mFont_16)); }
	inline UIFont_t2730669065 * get_mFont_16() const { return ___mFont_16; }
	inline UIFont_t2730669065 ** get_address_of_mFont_16() { return &___mFont_16; }
	inline void set_mFont_16(UIFont_t2730669065 * value)
	{
		___mFont_16 = value;
		Il2CppCodeGenWriteBarrier((&___mFont_16), value);
	}

	inline static int32_t get_offset_of_mText_17() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mText_17)); }
	inline String_t* get_mText_17() const { return ___mText_17; }
	inline String_t** get_address_of_mText_17() { return &___mText_17; }
	inline void set_mText_17(String_t* value)
	{
		___mText_17 = value;
		Il2CppCodeGenWriteBarrier((&___mText_17), value);
	}

	inline static int32_t get_offset_of_mMaxLineWidth_18() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mMaxLineWidth_18)); }
	inline int32_t get_mMaxLineWidth_18() const { return ___mMaxLineWidth_18; }
	inline int32_t* get_address_of_mMaxLineWidth_18() { return &___mMaxLineWidth_18; }
	inline void set_mMaxLineWidth_18(int32_t value)
	{
		___mMaxLineWidth_18 = value;
	}

	inline static int32_t get_offset_of_mEncoding_19() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mEncoding_19)); }
	inline bool get_mEncoding_19() const { return ___mEncoding_19; }
	inline bool* get_address_of_mEncoding_19() { return &___mEncoding_19; }
	inline void set_mEncoding_19(bool value)
	{
		___mEncoding_19 = value;
	}

	inline static int32_t get_offset_of_mMaxLineCount_20() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mMaxLineCount_20)); }
	inline int32_t get_mMaxLineCount_20() const { return ___mMaxLineCount_20; }
	inline int32_t* get_address_of_mMaxLineCount_20() { return &___mMaxLineCount_20; }
	inline void set_mMaxLineCount_20(int32_t value)
	{
		___mMaxLineCount_20 = value;
	}

	inline static int32_t get_offset_of_mPassword_21() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mPassword_21)); }
	inline bool get_mPassword_21() const { return ___mPassword_21; }
	inline bool* get_address_of_mPassword_21() { return &___mPassword_21; }
	inline void set_mPassword_21(bool value)
	{
		___mPassword_21 = value;
	}

	inline static int32_t get_offset_of_mShowLastChar_22() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mShowLastChar_22)); }
	inline bool get_mShowLastChar_22() const { return ___mShowLastChar_22; }
	inline bool* get_address_of_mShowLastChar_22() { return &___mShowLastChar_22; }
	inline void set_mShowLastChar_22(bool value)
	{
		___mShowLastChar_22 = value;
	}

	inline static int32_t get_offset_of_mEffectStyle_23() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mEffectStyle_23)); }
	inline int32_t get_mEffectStyle_23() const { return ___mEffectStyle_23; }
	inline int32_t* get_address_of_mEffectStyle_23() { return &___mEffectStyle_23; }
	inline void set_mEffectStyle_23(int32_t value)
	{
		___mEffectStyle_23 = value;
	}

	inline static int32_t get_offset_of_mEffectColor_24() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mEffectColor_24)); }
	inline Color_t2582018970  get_mEffectColor_24() const { return ___mEffectColor_24; }
	inline Color_t2582018970 * get_address_of_mEffectColor_24() { return &___mEffectColor_24; }
	inline void set_mEffectColor_24(Color_t2582018970  value)
	{
		___mEffectColor_24 = value;
	}

	inline static int32_t get_offset_of_mSymbols_25() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mSymbols_25)); }
	inline int32_t get_mSymbols_25() const { return ___mSymbols_25; }
	inline int32_t* get_address_of_mSymbols_25() { return &___mSymbols_25; }
	inline void set_mSymbols_25(int32_t value)
	{
		___mSymbols_25 = value;
	}

	inline static int32_t get_offset_of_mEffectDistance_26() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mEffectDistance_26)); }
	inline Vector2_t328513675  get_mEffectDistance_26() const { return ___mEffectDistance_26; }
	inline Vector2_t328513675 * get_address_of_mEffectDistance_26() { return &___mEffectDistance_26; }
	inline void set_mEffectDistance_26(Vector2_t328513675  value)
	{
		___mEffectDistance_26 = value;
	}

	inline static int32_t get_offset_of_mLineWidth_27() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLineWidth_27)); }
	inline float get_mLineWidth_27() const { return ___mLineWidth_27; }
	inline float* get_address_of_mLineWidth_27() { return &___mLineWidth_27; }
	inline void set_mLineWidth_27(float value)
	{
		___mLineWidth_27 = value;
	}

	inline static int32_t get_offset_of_mMultiline_28() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mMultiline_28)); }
	inline bool get_mMultiline_28() const { return ___mMultiline_28; }
	inline bool* get_address_of_mMultiline_28() { return &___mMultiline_28; }
	inline void set_mMultiline_28(bool value)
	{
		___mMultiline_28 = value;
	}

	inline static int32_t get_offset_of_mShouldBeProcessed_29() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mShouldBeProcessed_29)); }
	inline bool get_mShouldBeProcessed_29() const { return ___mShouldBeProcessed_29; }
	inline bool* get_address_of_mShouldBeProcessed_29() { return &___mShouldBeProcessed_29; }
	inline void set_mShouldBeProcessed_29(bool value)
	{
		___mShouldBeProcessed_29 = value;
	}

	inline static int32_t get_offset_of_mProcessedText_30() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mProcessedText_30)); }
	inline String_t* get_mProcessedText_30() const { return ___mProcessedText_30; }
	inline String_t** get_address_of_mProcessedText_30() { return &___mProcessedText_30; }
	inline void set_mProcessedText_30(String_t* value)
	{
		___mProcessedText_30 = value;
		Il2CppCodeGenWriteBarrier((&___mProcessedText_30), value);
	}

	inline static int32_t get_offset_of_mLastScale_31() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastScale_31)); }
	inline Vector3_t1986933152  get_mLastScale_31() const { return ___mLastScale_31; }
	inline Vector3_t1986933152 * get_address_of_mLastScale_31() { return &___mLastScale_31; }
	inline void set_mLastScale_31(Vector3_t1986933152  value)
	{
		___mLastScale_31 = value;
	}

	inline static int32_t get_offset_of_mLastText_32() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastText_32)); }
	inline String_t* get_mLastText_32() const { return ___mLastText_32; }
	inline String_t** get_address_of_mLastText_32() { return &___mLastText_32; }
	inline void set_mLastText_32(String_t* value)
	{
		___mLastText_32 = value;
		Il2CppCodeGenWriteBarrier((&___mLastText_32), value);
	}

	inline static int32_t get_offset_of_mLastWidth_33() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastWidth_33)); }
	inline int32_t get_mLastWidth_33() const { return ___mLastWidth_33; }
	inline int32_t* get_address_of_mLastWidth_33() { return &___mLastWidth_33; }
	inline void set_mLastWidth_33(int32_t value)
	{
		___mLastWidth_33 = value;
	}

	inline static int32_t get_offset_of_mLastEncoding_34() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastEncoding_34)); }
	inline bool get_mLastEncoding_34() const { return ___mLastEncoding_34; }
	inline bool* get_address_of_mLastEncoding_34() { return &___mLastEncoding_34; }
	inline void set_mLastEncoding_34(bool value)
	{
		___mLastEncoding_34 = value;
	}

	inline static int32_t get_offset_of_mLastCount_35() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastCount_35)); }
	inline int32_t get_mLastCount_35() const { return ___mLastCount_35; }
	inline int32_t* get_address_of_mLastCount_35() { return &___mLastCount_35; }
	inline void set_mLastCount_35(int32_t value)
	{
		___mLastCount_35 = value;
	}

	inline static int32_t get_offset_of_mLastPass_36() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastPass_36)); }
	inline bool get_mLastPass_36() const { return ___mLastPass_36; }
	inline bool* get_address_of_mLastPass_36() { return &___mLastPass_36; }
	inline void set_mLastPass_36(bool value)
	{
		___mLastPass_36 = value;
	}

	inline static int32_t get_offset_of_mLastShow_37() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastShow_37)); }
	inline bool get_mLastShow_37() const { return ___mLastShow_37; }
	inline bool* get_address_of_mLastShow_37() { return &___mLastShow_37; }
	inline void set_mLastShow_37(bool value)
	{
		___mLastShow_37 = value;
	}

	inline static int32_t get_offset_of_mLastEffect_38() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mLastEffect_38)); }
	inline int32_t get_mLastEffect_38() const { return ___mLastEffect_38; }
	inline int32_t* get_address_of_mLastEffect_38() { return &___mLastEffect_38; }
	inline void set_mLastEffect_38(int32_t value)
	{
		___mLastEffect_38 = value;
	}

	inline static int32_t get_offset_of_mSize_39() { return static_cast<int32_t>(offsetof(UILabel_t3478074517, ___mSize_39)); }
	inline Vector3_t1986933152  get_mSize_39() const { return ___mSize_39; }
	inline Vector3_t1986933152 * get_address_of_mSize_39() { return &___mSize_39; }
	inline void set_mSize_39(Vector3_t1986933152  value)
	{
		___mSize_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILABEL_T3478074517_H
#ifndef UIINPUTSAVED_T3772643040_H
#define UIINPUTSAVED_T3772643040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputSaved
struct  UIInputSaved_t3772643040  : public UIInput_t1674607152
{
public:
	// System.String UIInputSaved::playerPrefsField
	String_t* ___playerPrefsField_20;

public:
	inline static int32_t get_offset_of_playerPrefsField_20() { return static_cast<int32_t>(offsetof(UIInputSaved_t3772643040, ___playerPrefsField_20)); }
	inline String_t* get_playerPrefsField_20() const { return ___playerPrefsField_20; }
	inline String_t** get_address_of_playerPrefsField_20() { return &___playerPrefsField_20; }
	inline void set_playerPrefsField_20(String_t* value)
	{
		___playerPrefsField_20 = value;
		Il2CppCodeGenWriteBarrier((&___playerPrefsField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTSAVED_T3772643040_H
#ifndef SPRINGPOSITION_T1843463491_H
#define SPRINGPOSITION_T1843463491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPosition
struct  SpringPosition_t1843463491  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Vector3 SpringPosition::target
	Vector3_t1986933152  ___target_6;
	// System.Single SpringPosition::strength
	float ___strength_7;
	// System.Boolean SpringPosition::worldSpace
	bool ___worldSpace_8;
	// System.Boolean SpringPosition::ignoreTimeScale
	bool ___ignoreTimeScale_9;
	// UnityEngine.GameObject SpringPosition::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_10;
	// System.String SpringPosition::callWhenFinished
	String_t* ___callWhenFinished_11;
	// SpringPosition/OnFinished SpringPosition::onFinished
	OnFinished_t3829816540 * ___onFinished_12;
	// UnityEngine.Transform SpringPosition::mTrans
	Transform_t362059596 * ___mTrans_13;
	// System.Single SpringPosition::mThreshold
	float ___mThreshold_14;

public:
	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___target_6)); }
	inline Vector3_t1986933152  get_target_6() const { return ___target_6; }
	inline Vector3_t1986933152 * get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Vector3_t1986933152  value)
	{
		___target_6 = value;
	}

	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}

	inline static int32_t get_offset_of_worldSpace_8() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___worldSpace_8)); }
	inline bool get_worldSpace_8() const { return ___worldSpace_8; }
	inline bool* get_address_of_worldSpace_8() { return &___worldSpace_8; }
	inline void set_worldSpace_8(bool value)
	{
		___worldSpace_8 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_9() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___ignoreTimeScale_9)); }
	inline bool get_ignoreTimeScale_9() const { return ___ignoreTimeScale_9; }
	inline bool* get_address_of_ignoreTimeScale_9() { return &___ignoreTimeScale_9; }
	inline void set_ignoreTimeScale_9(bool value)
	{
		___ignoreTimeScale_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_10() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___eventReceiver_10)); }
	inline GameObject_t2557347079 * get_eventReceiver_10() const { return ___eventReceiver_10; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_10() { return &___eventReceiver_10; }
	inline void set_eventReceiver_10(GameObject_t2557347079 * value)
	{
		___eventReceiver_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_10), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_11() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___callWhenFinished_11)); }
	inline String_t* get_callWhenFinished_11() const { return ___callWhenFinished_11; }
	inline String_t** get_address_of_callWhenFinished_11() { return &___callWhenFinished_11; }
	inline void set_callWhenFinished_11(String_t* value)
	{
		___callWhenFinished_11 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_11), value);
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___onFinished_12)); }
	inline OnFinished_t3829816540 * get_onFinished_12() const { return ___onFinished_12; }
	inline OnFinished_t3829816540 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(OnFinished_t3829816540 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_mTrans_13() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___mTrans_13)); }
	inline Transform_t362059596 * get_mTrans_13() const { return ___mTrans_13; }
	inline Transform_t362059596 ** get_address_of_mTrans_13() { return &___mTrans_13; }
	inline void set_mTrans_13(Transform_t362059596 * value)
	{
		___mTrans_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_13), value);
	}

	inline static int32_t get_offset_of_mThreshold_14() { return static_cast<int32_t>(offsetof(SpringPosition_t1843463491, ___mThreshold_14)); }
	inline float get_mThreshold_14() const { return ___mThreshold_14; }
	inline float* get_address_of_mThreshold_14() { return &___mThreshold_14; }
	inline void set_mThreshold_14(float value)
	{
		___mThreshold_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGPOSITION_T1843463491_H
#ifndef UITEXTURE_T976444930_H
#define UITEXTURE_T976444930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITexture
struct  UITexture_t976444930  : public UIWidget_t1961100141
{
public:
	// UnityEngine.Rect UITexture::mRect
	Rect_t3039462994  ___mRect_16;
	// UnityEngine.Shader UITexture::mShader
	Shader_t1881769421 * ___mShader_17;
	// UnityEngine.Texture UITexture::mTexture
	Texture_t2119925672 * ___mTexture_18;
	// UnityEngine.Material UITexture::mDynamicMat
	Material_t2815264910 * ___mDynamicMat_19;
	// System.Boolean UITexture::mCreatingMat
	bool ___mCreatingMat_20;

public:
	inline static int32_t get_offset_of_mRect_16() { return static_cast<int32_t>(offsetof(UITexture_t976444930, ___mRect_16)); }
	inline Rect_t3039462994  get_mRect_16() const { return ___mRect_16; }
	inline Rect_t3039462994 * get_address_of_mRect_16() { return &___mRect_16; }
	inline void set_mRect_16(Rect_t3039462994  value)
	{
		___mRect_16 = value;
	}

	inline static int32_t get_offset_of_mShader_17() { return static_cast<int32_t>(offsetof(UITexture_t976444930, ___mShader_17)); }
	inline Shader_t1881769421 * get_mShader_17() const { return ___mShader_17; }
	inline Shader_t1881769421 ** get_address_of_mShader_17() { return &___mShader_17; }
	inline void set_mShader_17(Shader_t1881769421 * value)
	{
		___mShader_17 = value;
		Il2CppCodeGenWriteBarrier((&___mShader_17), value);
	}

	inline static int32_t get_offset_of_mTexture_18() { return static_cast<int32_t>(offsetof(UITexture_t976444930, ___mTexture_18)); }
	inline Texture_t2119925672 * get_mTexture_18() const { return ___mTexture_18; }
	inline Texture_t2119925672 ** get_address_of_mTexture_18() { return &___mTexture_18; }
	inline void set_mTexture_18(Texture_t2119925672 * value)
	{
		___mTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_18), value);
	}

	inline static int32_t get_offset_of_mDynamicMat_19() { return static_cast<int32_t>(offsetof(UITexture_t976444930, ___mDynamicMat_19)); }
	inline Material_t2815264910 * get_mDynamicMat_19() const { return ___mDynamicMat_19; }
	inline Material_t2815264910 ** get_address_of_mDynamicMat_19() { return &___mDynamicMat_19; }
	inline void set_mDynamicMat_19(Material_t2815264910 * value)
	{
		___mDynamicMat_19 = value;
		Il2CppCodeGenWriteBarrier((&___mDynamicMat_19), value);
	}

	inline static int32_t get_offset_of_mCreatingMat_20() { return static_cast<int32_t>(offsetof(UITexture_t976444930, ___mCreatingMat_20)); }
	inline bool get_mCreatingMat_20() const { return ___mCreatingMat_20; }
	inline bool* get_address_of_mCreatingMat_20() { return &___mCreatingMat_20; }
	inline void set_mCreatingMat_20(bool value)
	{
		___mCreatingMat_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTURE_T976444930_H
#ifndef UITWEENER_T1705010025_H
#define UITWEENER_T1705010025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener
struct  UITweener_t1705010025  : public IgnoreTimeScale_t640346767
{
public:
	// UITweener/OnFinished UITweener::onFinished
	OnFinished_t2354188770 * ___onFinished_6;
	// UITweener/Method UITweener::method
	int32_t ___method_7;
	// UITweener/Style UITweener::style
	int32_t ___style_8;
	// System.Boolean UITweener::ignoreTimeScale
	bool ___ignoreTimeScale_9;
	// System.Single UITweener::delay
	float ___delay_10;
	// System.Single UITweener::duration
	float ___duration_11;
	// System.Boolean UITweener::steeperCurves
	bool ___steeperCurves_12;
	// System.Int32 UITweener::tweenGroup
	int32_t ___tweenGroup_13;
	// UnityEngine.GameObject UITweener::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_14;
	// System.String UITweener::callWhenFinished
	String_t* ___callWhenFinished_15;
	// System.Single UITweener::mStartTime
	float ___mStartTime_16;
	// System.Single UITweener::mDuration
	float ___mDuration_17;
	// System.Single UITweener::mAmountPerDelta
	float ___mAmountPerDelta_18;
	// System.Single UITweener::mFactor
	float ___mFactor_19;

public:
	inline static int32_t get_offset_of_onFinished_6() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___onFinished_6)); }
	inline OnFinished_t2354188770 * get_onFinished_6() const { return ___onFinished_6; }
	inline OnFinished_t2354188770 ** get_address_of_onFinished_6() { return &___onFinished_6; }
	inline void set_onFinished_6(OnFinished_t2354188770 * value)
	{
		___onFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_6), value);
	}

	inline static int32_t get_offset_of_method_7() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___method_7)); }
	inline int32_t get_method_7() const { return ___method_7; }
	inline int32_t* get_address_of_method_7() { return &___method_7; }
	inline void set_method_7(int32_t value)
	{
		___method_7 = value;
	}

	inline static int32_t get_offset_of_style_8() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___style_8)); }
	inline int32_t get_style_8() const { return ___style_8; }
	inline int32_t* get_address_of_style_8() { return &___style_8; }
	inline void set_style_8(int32_t value)
	{
		___style_8 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_9() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___ignoreTimeScale_9)); }
	inline bool get_ignoreTimeScale_9() const { return ___ignoreTimeScale_9; }
	inline bool* get_address_of_ignoreTimeScale_9() { return &___ignoreTimeScale_9; }
	inline void set_ignoreTimeScale_9(bool value)
	{
		___ignoreTimeScale_9 = value;
	}

	inline static int32_t get_offset_of_delay_10() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___delay_10)); }
	inline float get_delay_10() const { return ___delay_10; }
	inline float* get_address_of_delay_10() { return &___delay_10; }
	inline void set_delay_10(float value)
	{
		___delay_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_steeperCurves_12() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___steeperCurves_12)); }
	inline bool get_steeperCurves_12() const { return ___steeperCurves_12; }
	inline bool* get_address_of_steeperCurves_12() { return &___steeperCurves_12; }
	inline void set_steeperCurves_12(bool value)
	{
		___steeperCurves_12 = value;
	}

	inline static int32_t get_offset_of_tweenGroup_13() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___tweenGroup_13)); }
	inline int32_t get_tweenGroup_13() const { return ___tweenGroup_13; }
	inline int32_t* get_address_of_tweenGroup_13() { return &___tweenGroup_13; }
	inline void set_tweenGroup_13(int32_t value)
	{
		___tweenGroup_13 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_14() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___eventReceiver_14)); }
	inline GameObject_t2557347079 * get_eventReceiver_14() const { return ___eventReceiver_14; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_14() { return &___eventReceiver_14; }
	inline void set_eventReceiver_14(GameObject_t2557347079 * value)
	{
		___eventReceiver_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_14), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_15() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___callWhenFinished_15)); }
	inline String_t* get_callWhenFinished_15() const { return ___callWhenFinished_15; }
	inline String_t** get_address_of_callWhenFinished_15() { return &___callWhenFinished_15; }
	inline void set_callWhenFinished_15(String_t* value)
	{
		___callWhenFinished_15 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_15), value);
	}

	inline static int32_t get_offset_of_mStartTime_16() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___mStartTime_16)); }
	inline float get_mStartTime_16() const { return ___mStartTime_16; }
	inline float* get_address_of_mStartTime_16() { return &___mStartTime_16; }
	inline void set_mStartTime_16(float value)
	{
		___mStartTime_16 = value;
	}

	inline static int32_t get_offset_of_mDuration_17() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___mDuration_17)); }
	inline float get_mDuration_17() const { return ___mDuration_17; }
	inline float* get_address_of_mDuration_17() { return &___mDuration_17; }
	inline void set_mDuration_17(float value)
	{
		___mDuration_17 = value;
	}

	inline static int32_t get_offset_of_mAmountPerDelta_18() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___mAmountPerDelta_18)); }
	inline float get_mAmountPerDelta_18() const { return ___mAmountPerDelta_18; }
	inline float* get_address_of_mAmountPerDelta_18() { return &___mAmountPerDelta_18; }
	inline void set_mAmountPerDelta_18(float value)
	{
		___mAmountPerDelta_18 = value;
	}

	inline static int32_t get_offset_of_mFactor_19() { return static_cast<int32_t>(offsetof(UITweener_t1705010025, ___mFactor_19)); }
	inline float get_mFactor_19() const { return ___mFactor_19; }
	inline float* get_address_of_mFactor_19() { return &___mFactor_19; }
	inline void set_mFactor_19(float value)
	{
		___mFactor_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWEENER_T1705010025_H
#ifndef SPRINGPANEL_T2180292480_H
#define SPRINGPANEL_T2180292480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPanel
struct  SpringPanel_t2180292480  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Vector3 SpringPanel::target
	Vector3_t1986933152  ___target_6;
	// System.Single SpringPanel::strength
	float ___strength_7;
	// SpringPanel/OnFinished SpringPanel::onFinished
	OnFinished_t2500445478 * ___onFinished_8;
	// UIPanel SpringPanel::mPanel
	UIPanel_t825970685 * ___mPanel_9;
	// UnityEngine.Transform SpringPanel::mTrans
	Transform_t362059596 * ___mTrans_10;
	// System.Single SpringPanel::mThreshold
	float ___mThreshold_11;
	// UIDraggablePanel SpringPanel::mDrag
	UIDraggablePanel_t2580300900 * ___mDrag_12;

public:
	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___target_6)); }
	inline Vector3_t1986933152  get_target_6() const { return ___target_6; }
	inline Vector3_t1986933152 * get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Vector3_t1986933152  value)
	{
		___target_6 = value;
	}

	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}

	inline static int32_t get_offset_of_onFinished_8() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___onFinished_8)); }
	inline OnFinished_t2500445478 * get_onFinished_8() const { return ___onFinished_8; }
	inline OnFinished_t2500445478 ** get_address_of_onFinished_8() { return &___onFinished_8; }
	inline void set_onFinished_8(OnFinished_t2500445478 * value)
	{
		___onFinished_8 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_8), value);
	}

	inline static int32_t get_offset_of_mPanel_9() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___mPanel_9)); }
	inline UIPanel_t825970685 * get_mPanel_9() const { return ___mPanel_9; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_9() { return &___mPanel_9; }
	inline void set_mPanel_9(UIPanel_t825970685 * value)
	{
		___mPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_9), value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___mTrans_10)); }
	inline Transform_t362059596 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t362059596 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t362059596 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_10), value);
	}

	inline static int32_t get_offset_of_mThreshold_11() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___mThreshold_11)); }
	inline float get_mThreshold_11() const { return ___mThreshold_11; }
	inline float* get_address_of_mThreshold_11() { return &___mThreshold_11; }
	inline void set_mThreshold_11(float value)
	{
		___mThreshold_11 = value;
	}

	inline static int32_t get_offset_of_mDrag_12() { return static_cast<int32_t>(offsetof(SpringPanel_t2180292480, ___mDrag_12)); }
	inline UIDraggablePanel_t2580300900 * get_mDrag_12() const { return ___mDrag_12; }
	inline UIDraggablePanel_t2580300900 ** get_address_of_mDrag_12() { return &___mDrag_12; }
	inline void set_mDrag_12(UIDraggablePanel_t2580300900 * value)
	{
		___mDrag_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDrag_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGPANEL_T2180292480_H
#ifndef UIFILLEDSPRITE_T3395723239_H
#define UIFILLEDSPRITE_T3395723239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFilledSprite
struct  UIFilledSprite_t3395723239  : public UISprite_t279728715
{
public:
	// UIFilledSprite/FillDirection UIFilledSprite::mFillDirection
	int32_t ___mFillDirection_23;
	// System.Single UIFilledSprite::mFillAmount
	float ___mFillAmount_24;
	// System.Boolean UIFilledSprite::mInvert
	bool ___mInvert_25;

public:
	inline static int32_t get_offset_of_mFillDirection_23() { return static_cast<int32_t>(offsetof(UIFilledSprite_t3395723239, ___mFillDirection_23)); }
	inline int32_t get_mFillDirection_23() const { return ___mFillDirection_23; }
	inline int32_t* get_address_of_mFillDirection_23() { return &___mFillDirection_23; }
	inline void set_mFillDirection_23(int32_t value)
	{
		___mFillDirection_23 = value;
	}

	inline static int32_t get_offset_of_mFillAmount_24() { return static_cast<int32_t>(offsetof(UIFilledSprite_t3395723239, ___mFillAmount_24)); }
	inline float get_mFillAmount_24() const { return ___mFillAmount_24; }
	inline float* get_address_of_mFillAmount_24() { return &___mFillAmount_24; }
	inline void set_mFillAmount_24(float value)
	{
		___mFillAmount_24 = value;
	}

	inline static int32_t get_offset_of_mInvert_25() { return static_cast<int32_t>(offsetof(UIFilledSprite_t3395723239, ___mInvert_25)); }
	inline bool get_mInvert_25() const { return ___mInvert_25; }
	inline bool* get_address_of_mInvert_25() { return &___mInvert_25; }
	inline void set_mInvert_25(bool value)
	{
		___mInvert_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFILLEDSPRITE_T3395723239_H
#ifndef UISLICEDSPRITE_T3319190482_H
#define UISLICEDSPRITE_T3319190482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlicedSprite
struct  UISlicedSprite_t3319190482  : public UISprite_t279728715
{
public:
	// System.Boolean UISlicedSprite::mFillCenter
	bool ___mFillCenter_23;
	// UnityEngine.Rect UISlicedSprite::mInner
	Rect_t3039462994  ___mInner_24;
	// UnityEngine.Rect UISlicedSprite::mInnerUV
	Rect_t3039462994  ___mInnerUV_25;
	// UnityEngine.Vector3 UISlicedSprite::mScale
	Vector3_t1986933152  ___mScale_26;

public:
	inline static int32_t get_offset_of_mFillCenter_23() { return static_cast<int32_t>(offsetof(UISlicedSprite_t3319190482, ___mFillCenter_23)); }
	inline bool get_mFillCenter_23() const { return ___mFillCenter_23; }
	inline bool* get_address_of_mFillCenter_23() { return &___mFillCenter_23; }
	inline void set_mFillCenter_23(bool value)
	{
		___mFillCenter_23 = value;
	}

	inline static int32_t get_offset_of_mInner_24() { return static_cast<int32_t>(offsetof(UISlicedSprite_t3319190482, ___mInner_24)); }
	inline Rect_t3039462994  get_mInner_24() const { return ___mInner_24; }
	inline Rect_t3039462994 * get_address_of_mInner_24() { return &___mInner_24; }
	inline void set_mInner_24(Rect_t3039462994  value)
	{
		___mInner_24 = value;
	}

	inline static int32_t get_offset_of_mInnerUV_25() { return static_cast<int32_t>(offsetof(UISlicedSprite_t3319190482, ___mInnerUV_25)); }
	inline Rect_t3039462994  get_mInnerUV_25() const { return ___mInnerUV_25; }
	inline Rect_t3039462994 * get_address_of_mInnerUV_25() { return &___mInnerUV_25; }
	inline void set_mInnerUV_25(Rect_t3039462994  value)
	{
		___mInnerUV_25 = value;
	}

	inline static int32_t get_offset_of_mScale_26() { return static_cast<int32_t>(offsetof(UISlicedSprite_t3319190482, ___mScale_26)); }
	inline Vector3_t1986933152  get_mScale_26() const { return ___mScale_26; }
	inline Vector3_t1986933152 * get_address_of_mScale_26() { return &___mScale_26; }
	inline void set_mScale_26(Vector3_t1986933152  value)
	{
		___mScale_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLICEDSPRITE_T3319190482_H
#ifndef TWEENALPHA_T4056082079_H
#define TWEENALPHA_T4056082079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAlpha
struct  TweenAlpha_t4056082079  : public UITweener_t1705010025
{
public:
	// System.Single TweenAlpha::from
	float ___from_20;
	// System.Single TweenAlpha::to
	float ___to_21;
	// UnityEngine.Transform TweenAlpha::mTrans
	Transform_t362059596 * ___mTrans_22;
	// UIWidget TweenAlpha::mWidget
	UIWidget_t1961100141 * ___mWidget_23;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenAlpha_t4056082079, ___from_20)); }
	inline float get_from_20() const { return ___from_20; }
	inline float* get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(float value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenAlpha_t4056082079, ___to_21)); }
	inline float get_to_21() const { return ___to_21; }
	inline float* get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(float value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mTrans_22() { return static_cast<int32_t>(offsetof(TweenAlpha_t4056082079, ___mTrans_22)); }
	inline Transform_t362059596 * get_mTrans_22() const { return ___mTrans_22; }
	inline Transform_t362059596 ** get_address_of_mTrans_22() { return &___mTrans_22; }
	inline void set_mTrans_22(Transform_t362059596 * value)
	{
		___mTrans_22 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_22), value);
	}

	inline static int32_t get_offset_of_mWidget_23() { return static_cast<int32_t>(offsetof(TweenAlpha_t4056082079, ___mWidget_23)); }
	inline UIWidget_t1961100141 * get_mWidget_23() const { return ___mWidget_23; }
	inline UIWidget_t1961100141 ** get_address_of_mWidget_23() { return &___mWidget_23; }
	inline void set_mWidget_23(UIWidget_t1961100141 * value)
	{
		___mWidget_23 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENALPHA_T4056082079_H
#ifndef TWEENVOLUME_T3389378834_H
#define TWEENVOLUME_T3389378834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenVolume
struct  TweenVolume_t3389378834  : public UITweener_t1705010025
{
public:
	// System.Single TweenVolume::from
	float ___from_20;
	// System.Single TweenVolume::to
	float ___to_21;
	// UnityEngine.AudioSource TweenVolume::mSource
	AudioSource_t4025721661 * ___mSource_22;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenVolume_t3389378834, ___from_20)); }
	inline float get_from_20() const { return ___from_20; }
	inline float* get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(float value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenVolume_t3389378834, ___to_21)); }
	inline float get_to_21() const { return ___to_21; }
	inline float* get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(float value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mSource_22() { return static_cast<int32_t>(offsetof(TweenVolume_t3389378834, ___mSource_22)); }
	inline AudioSource_t4025721661 * get_mSource_22() const { return ___mSource_22; }
	inline AudioSource_t4025721661 ** get_address_of_mSource_22() { return &___mSource_22; }
	inline void set_mSource_22(AudioSource_t4025721661 * value)
	{
		___mSource_22 = value;
		Il2CppCodeGenWriteBarrier((&___mSource_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENVOLUME_T3389378834_H
#ifndef TWEENTRANSFORM_T3646711066_H
#define TWEENTRANSFORM_T3646711066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenTransform
struct  TweenTransform_t3646711066  : public UITweener_t1705010025
{
public:
	// UnityEngine.Transform TweenTransform::from
	Transform_t362059596 * ___from_20;
	// UnityEngine.Transform TweenTransform::to
	Transform_t362059596 * ___to_21;
	// System.Boolean TweenTransform::parentWhenFinished
	bool ___parentWhenFinished_22;
	// UnityEngine.Transform TweenTransform::mTrans
	Transform_t362059596 * ___mTrans_23;
	// UnityEngine.Vector3 TweenTransform::mPos
	Vector3_t1986933152  ___mPos_24;
	// UnityEngine.Quaternion TweenTransform::mRot
	Quaternion_t704191599  ___mRot_25;
	// UnityEngine.Vector3 TweenTransform::mScale
	Vector3_t1986933152  ___mScale_26;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___from_20)); }
	inline Transform_t362059596 * get_from_20() const { return ___from_20; }
	inline Transform_t362059596 ** get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Transform_t362059596 * value)
	{
		___from_20 = value;
		Il2CppCodeGenWriteBarrier((&___from_20), value);
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___to_21)); }
	inline Transform_t362059596 * get_to_21() const { return ___to_21; }
	inline Transform_t362059596 ** get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Transform_t362059596 * value)
	{
		___to_21 = value;
		Il2CppCodeGenWriteBarrier((&___to_21), value);
	}

	inline static int32_t get_offset_of_parentWhenFinished_22() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___parentWhenFinished_22)); }
	inline bool get_parentWhenFinished_22() const { return ___parentWhenFinished_22; }
	inline bool* get_address_of_parentWhenFinished_22() { return &___parentWhenFinished_22; }
	inline void set_parentWhenFinished_22(bool value)
	{
		___parentWhenFinished_22 = value;
	}

	inline static int32_t get_offset_of_mTrans_23() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___mTrans_23)); }
	inline Transform_t362059596 * get_mTrans_23() const { return ___mTrans_23; }
	inline Transform_t362059596 ** get_address_of_mTrans_23() { return &___mTrans_23; }
	inline void set_mTrans_23(Transform_t362059596 * value)
	{
		___mTrans_23 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_23), value);
	}

	inline static int32_t get_offset_of_mPos_24() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___mPos_24)); }
	inline Vector3_t1986933152  get_mPos_24() const { return ___mPos_24; }
	inline Vector3_t1986933152 * get_address_of_mPos_24() { return &___mPos_24; }
	inline void set_mPos_24(Vector3_t1986933152  value)
	{
		___mPos_24 = value;
	}

	inline static int32_t get_offset_of_mRot_25() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___mRot_25)); }
	inline Quaternion_t704191599  get_mRot_25() const { return ___mRot_25; }
	inline Quaternion_t704191599 * get_address_of_mRot_25() { return &___mRot_25; }
	inline void set_mRot_25(Quaternion_t704191599  value)
	{
		___mRot_25 = value;
	}

	inline static int32_t get_offset_of_mScale_26() { return static_cast<int32_t>(offsetof(TweenTransform_t3646711066, ___mScale_26)); }
	inline Vector3_t1986933152  get_mScale_26() const { return ___mScale_26; }
	inline Vector3_t1986933152 * get_address_of_mScale_26() { return &___mScale_26; }
	inline void set_mScale_26(Vector3_t1986933152  value)
	{
		___mScale_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTRANSFORM_T3646711066_H
#ifndef TWEENSCALE_T312802622_H
#define TWEENSCALE_T312802622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenScale
struct  TweenScale_t312802622  : public UITweener_t1705010025
{
public:
	// UnityEngine.Vector3 TweenScale::from
	Vector3_t1986933152  ___from_20;
	// UnityEngine.Vector3 TweenScale::to
	Vector3_t1986933152  ___to_21;
	// System.Boolean TweenScale::updateTable
	bool ___updateTable_22;
	// UnityEngine.Transform TweenScale::mTrans
	Transform_t362059596 * ___mTrans_23;
	// UITable TweenScale::mTable
	UITable_t2217632677 * ___mTable_24;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenScale_t312802622, ___from_20)); }
	inline Vector3_t1986933152  get_from_20() const { return ___from_20; }
	inline Vector3_t1986933152 * get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Vector3_t1986933152  value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenScale_t312802622, ___to_21)); }
	inline Vector3_t1986933152  get_to_21() const { return ___to_21; }
	inline Vector3_t1986933152 * get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Vector3_t1986933152  value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_updateTable_22() { return static_cast<int32_t>(offsetof(TweenScale_t312802622, ___updateTable_22)); }
	inline bool get_updateTable_22() const { return ___updateTable_22; }
	inline bool* get_address_of_updateTable_22() { return &___updateTable_22; }
	inline void set_updateTable_22(bool value)
	{
		___updateTable_22 = value;
	}

	inline static int32_t get_offset_of_mTrans_23() { return static_cast<int32_t>(offsetof(TweenScale_t312802622, ___mTrans_23)); }
	inline Transform_t362059596 * get_mTrans_23() const { return ___mTrans_23; }
	inline Transform_t362059596 ** get_address_of_mTrans_23() { return &___mTrans_23; }
	inline void set_mTrans_23(Transform_t362059596 * value)
	{
		___mTrans_23 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_23), value);
	}

	inline static int32_t get_offset_of_mTable_24() { return static_cast<int32_t>(offsetof(TweenScale_t312802622, ___mTable_24)); }
	inline UITable_t2217632677 * get_mTable_24() const { return ___mTable_24; }
	inline UITable_t2217632677 ** get_address_of_mTable_24() { return &___mTable_24; }
	inline void set_mTable_24(UITable_t2217632677 * value)
	{
		___mTable_24 = value;
		Il2CppCodeGenWriteBarrier((&___mTable_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSCALE_T312802622_H
#ifndef TWEENPOSITION_T696968733_H
#define TWEENPOSITION_T696968733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenPosition
struct  TweenPosition_t696968733  : public UITweener_t1705010025
{
public:
	// UnityEngine.Vector3 TweenPosition::from
	Vector3_t1986933152  ___from_20;
	// UnityEngine.Vector3 TweenPosition::to
	Vector3_t1986933152  ___to_21;
	// UnityEngine.Transform TweenPosition::mTrans
	Transform_t362059596 * ___mTrans_22;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenPosition_t696968733, ___from_20)); }
	inline Vector3_t1986933152  get_from_20() const { return ___from_20; }
	inline Vector3_t1986933152 * get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Vector3_t1986933152  value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenPosition_t696968733, ___to_21)); }
	inline Vector3_t1986933152  get_to_21() const { return ___to_21; }
	inline Vector3_t1986933152 * get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Vector3_t1986933152  value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mTrans_22() { return static_cast<int32_t>(offsetof(TweenPosition_t696968733, ___mTrans_22)); }
	inline Transform_t362059596 * get_mTrans_22() const { return ___mTrans_22; }
	inline Transform_t362059596 ** get_address_of_mTrans_22() { return &___mTrans_22; }
	inline void set_mTrans_22(Transform_t362059596 * value)
	{
		___mTrans_22 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENPOSITION_T696968733_H
#ifndef TWEENFOV_T2364281439_H
#define TWEENFOV_T2364281439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenFOV
struct  TweenFOV_t2364281439  : public UITweener_t1705010025
{
public:
	// System.Single TweenFOV::from
	float ___from_20;
	// System.Single TweenFOV::to
	float ___to_21;
	// UnityEngine.Camera TweenFOV::mCam
	Camera_t2839736942 * ___mCam_22;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenFOV_t2364281439, ___from_20)); }
	inline float get_from_20() const { return ___from_20; }
	inline float* get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(float value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenFOV_t2364281439, ___to_21)); }
	inline float get_to_21() const { return ___to_21; }
	inline float* get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(float value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mCam_22() { return static_cast<int32_t>(offsetof(TweenFOV_t2364281439, ___mCam_22)); }
	inline Camera_t2839736942 * get_mCam_22() const { return ___mCam_22; }
	inline Camera_t2839736942 ** get_address_of_mCam_22() { return &___mCam_22; }
	inline void set_mCam_22(Camera_t2839736942 * value)
	{
		___mCam_22 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENFOV_T2364281439_H
#ifndef TWEENCOLOR_T4163898564_H
#define TWEENCOLOR_T4163898564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenColor
struct  TweenColor_t4163898564  : public UITweener_t1705010025
{
public:
	// UnityEngine.Color TweenColor::from
	Color_t2582018970  ___from_20;
	// UnityEngine.Color TweenColor::to
	Color_t2582018970  ___to_21;
	// UnityEngine.Transform TweenColor::mTrans
	Transform_t362059596 * ___mTrans_22;
	// UIWidget TweenColor::mWidget
	UIWidget_t1961100141 * ___mWidget_23;
	// UnityEngine.Material TweenColor::mMat
	Material_t2815264910 * ___mMat_24;
	// UnityEngine.Light TweenColor::mLight
	Light_t1775228881 * ___mLight_25;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___from_20)); }
	inline Color_t2582018970  get_from_20() const { return ___from_20; }
	inline Color_t2582018970 * get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Color_t2582018970  value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___to_21)); }
	inline Color_t2582018970  get_to_21() const { return ___to_21; }
	inline Color_t2582018970 * get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Color_t2582018970  value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mTrans_22() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___mTrans_22)); }
	inline Transform_t362059596 * get_mTrans_22() const { return ___mTrans_22; }
	inline Transform_t362059596 ** get_address_of_mTrans_22() { return &___mTrans_22; }
	inline void set_mTrans_22(Transform_t362059596 * value)
	{
		___mTrans_22 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_22), value);
	}

	inline static int32_t get_offset_of_mWidget_23() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___mWidget_23)); }
	inline UIWidget_t1961100141 * get_mWidget_23() const { return ___mWidget_23; }
	inline UIWidget_t1961100141 ** get_address_of_mWidget_23() { return &___mWidget_23; }
	inline void set_mWidget_23(UIWidget_t1961100141 * value)
	{
		___mWidget_23 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_23), value);
	}

	inline static int32_t get_offset_of_mMat_24() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___mMat_24)); }
	inline Material_t2815264910 * get_mMat_24() const { return ___mMat_24; }
	inline Material_t2815264910 ** get_address_of_mMat_24() { return &___mMat_24; }
	inline void set_mMat_24(Material_t2815264910 * value)
	{
		___mMat_24 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_24), value);
	}

	inline static int32_t get_offset_of_mLight_25() { return static_cast<int32_t>(offsetof(TweenColor_t4163898564, ___mLight_25)); }
	inline Light_t1775228881 * get_mLight_25() const { return ___mLight_25; }
	inline Light_t1775228881 ** get_address_of_mLight_25() { return &___mLight_25; }
	inline void set_mLight_25(Light_t1775228881 * value)
	{
		___mLight_25 = value;
		Il2CppCodeGenWriteBarrier((&___mLight_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCOLOR_T4163898564_H
#ifndef TWEENROTATION_T3175222233_H
#define TWEENROTATION_T3175222233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenRotation
struct  TweenRotation_t3175222233  : public UITweener_t1705010025
{
public:
	// UnityEngine.Vector3 TweenRotation::from
	Vector3_t1986933152  ___from_20;
	// UnityEngine.Vector3 TweenRotation::to
	Vector3_t1986933152  ___to_21;
	// UnityEngine.Transform TweenRotation::mTrans
	Transform_t362059596 * ___mTrans_22;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenRotation_t3175222233, ___from_20)); }
	inline Vector3_t1986933152  get_from_20() const { return ___from_20; }
	inline Vector3_t1986933152 * get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Vector3_t1986933152  value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenRotation_t3175222233, ___to_21)); }
	inline Vector3_t1986933152  get_to_21() const { return ___to_21; }
	inline Vector3_t1986933152 * get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Vector3_t1986933152  value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of_mTrans_22() { return static_cast<int32_t>(offsetof(TweenRotation_t3175222233, ___mTrans_22)); }
	inline Transform_t362059596 * get_mTrans_22() const { return ___mTrans_22; }
	inline Transform_t362059596 ** get_address_of_mTrans_22() { return &___mTrans_22; }
	inline void set_mTrans_22(Transform_t362059596 * value)
	{
		___mTrans_22 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENROTATION_T3175222233_H
#ifndef UITILEDSPRITE_T2195056393_H
#define UITILEDSPRITE_T2195056393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITiledSprite
struct  UITiledSprite_t2195056393  : public UISlicedSprite_t3319190482
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITILEDSPRITE_T2195056393_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Direction_t411333392)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2600[4] = 
{
	Direction_t411333392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (EnableCondition_t266497438)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[3] = 
{
	EnableCondition_t266497438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (DisableCondition_t4056368895)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2602[4] = 
{
	DisableCondition_t4056368895::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (BMFont_t2958998632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[9] = 
{
	BMFont_t2958998632::get_offset_of_mGlyphs_0(),
	BMFont_t2958998632::get_offset_of_mSize_1(),
	BMFont_t2958998632::get_offset_of_mBase_2(),
	BMFont_t2958998632::get_offset_of_mWidth_3(),
	BMFont_t2958998632::get_offset_of_mHeight_4(),
	BMFont_t2958998632::get_offset_of_mSpriteName_5(),
	BMFont_t2958998632::get_offset_of_mSaved_6(),
	BMFont_t2958998632::get_offset_of_mSymbols_7(),
	BMFont_t2958998632::get_offset_of_mDict_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (BMGlyph_t2886766731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[10] = 
{
	BMGlyph_t2886766731::get_offset_of_index_0(),
	BMGlyph_t2886766731::get_offset_of_x_1(),
	BMGlyph_t2886766731::get_offset_of_y_2(),
	BMGlyph_t2886766731::get_offset_of_width_3(),
	BMGlyph_t2886766731::get_offset_of_height_4(),
	BMGlyph_t2886766731::get_offset_of_offsetX_5(),
	BMGlyph_t2886766731::get_offset_of_offsetY_6(),
	BMGlyph_t2886766731::get_offset_of_advance_7(),
	BMGlyph_t2886766731::get_offset_of_channel_8(),
	BMGlyph_t2886766731::get_offset_of_kerning_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (Kerning_t647376370)+ sizeof (RuntimeObject), sizeof(Kerning_t647376370 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2607[2] = 
{
	Kerning_t647376370::get_offset_of_previousChar_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Kerning_t647376370::get_offset_of_amount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (BMSymbol_t1387512129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[6] = 
{
	BMSymbol_t1387512129::get_offset_of_sequence_0(),
	BMSymbol_t1387512129::get_offset_of_x_1(),
	BMSymbol_t1387512129::get_offset_of_y_2(),
	BMSymbol_t1387512129::get_offset_of_width_3(),
	BMSymbol_t1387512129::get_offset_of_height_4(),
	BMSymbol_t1387512129::get_offset_of_mLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (ByteReader_t2713471465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[2] = 
{
	ByteReader_t2713471465::get_offset_of_mBuffer_0(),
	ByteReader_t2713471465::get_offset_of_mOffset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (IgnoreTimeScale_t640346767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[4] = 
{
	IgnoreTimeScale_t640346767::get_offset_of_mTimeStart_2(),
	IgnoreTimeScale_t640346767::get_offset_of_mTimeDelta_3(),
	IgnoreTimeScale_t640346767::get_offset_of_mActual_4(),
	IgnoreTimeScale_t640346767::get_offset_of_mTimeStarted_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (Localization_t1621195128), -1, sizeof(Localization_t1621195128_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2611[5] = 
{
	Localization_t1621195128_StaticFields::get_offset_of_mInst_2(),
	Localization_t1621195128::get_offset_of_startingLanguage_3(),
	Localization_t1621195128::get_offset_of_languages_4(),
	Localization_t1621195128::get_offset_of_mDictionary_5(),
	Localization_t1621195128::get_offset_of_mLanguage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (NGUIDebug_t3415807361), -1, sizeof(NGUIDebug_t3415807361_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2612[2] = 
{
	NGUIDebug_t3415807361_StaticFields::get_offset_of_mLines_2(),
	NGUIDebug_t3415807361_StaticFields::get_offset_of_mInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (NGUIMath_t929756146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (NGUITools_t582569761), -1, sizeof(NGUITools_t582569761_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[3] = 
{
	NGUITools_t582569761_StaticFields::get_offset_of_mListener_0(),
	NGUITools_t582569761_StaticFields::get_offset_of_mLoaded_1(),
	NGUITools_t582569761_StaticFields::get_offset_of_mGlobalVolume_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (SpringPanel_t2180292480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[7] = 
{
	SpringPanel_t2180292480::get_offset_of_target_6(),
	SpringPanel_t2180292480::get_offset_of_strength_7(),
	SpringPanel_t2180292480::get_offset_of_onFinished_8(),
	SpringPanel_t2180292480::get_offset_of_mPanel_9(),
	SpringPanel_t2180292480::get_offset_of_mTrans_10(),
	SpringPanel_t2180292480::get_offset_of_mThreshold_11(),
	SpringPanel_t2180292480::get_offset_of_mDrag_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (OnFinished_t2500445478), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (UIDrawCall_t2687921839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[15] = 
{
	UIDrawCall_t2687921839::get_offset_of_mTrans_2(),
	UIDrawCall_t2687921839::get_offset_of_mSharedMat_3(),
	UIDrawCall_t2687921839::get_offset_of_mMesh0_4(),
	UIDrawCall_t2687921839::get_offset_of_mMesh1_5(),
	UIDrawCall_t2687921839::get_offset_of_mFilter_6(),
	UIDrawCall_t2687921839::get_offset_of_mRen_7(),
	UIDrawCall_t2687921839::get_offset_of_mClipping_8(),
	UIDrawCall_t2687921839::get_offset_of_mClipRange_9(),
	UIDrawCall_t2687921839::get_offset_of_mClipSoft_10(),
	UIDrawCall_t2687921839::get_offset_of_mClippedMat_11(),
	UIDrawCall_t2687921839::get_offset_of_mDepthMat_12(),
	UIDrawCall_t2687921839::get_offset_of_mIndices_13(),
	UIDrawCall_t2687921839::get_offset_of_mDepthPass_14(),
	UIDrawCall_t2687921839::get_offset_of_mReset_15(),
	UIDrawCall_t2687921839::get_offset_of_mEven_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (Clipping_t1987716881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2618[5] = 
{
	Clipping_t1987716881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (UIEventListener_t1748975512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[12] = 
{
	UIEventListener_t1748975512::get_offset_of_parameter_2(),
	UIEventListener_t1748975512::get_offset_of_onSubmit_3(),
	UIEventListener_t1748975512::get_offset_of_onClick_4(),
	UIEventListener_t1748975512::get_offset_of_onDoubleClick_5(),
	UIEventListener_t1748975512::get_offset_of_onHover_6(),
	UIEventListener_t1748975512::get_offset_of_onPress_7(),
	UIEventListener_t1748975512::get_offset_of_onSelect_8(),
	UIEventListener_t1748975512::get_offset_of_onScroll_9(),
	UIEventListener_t1748975512::get_offset_of_onDrag_10(),
	UIEventListener_t1748975512::get_offset_of_onDrop_11(),
	UIEventListener_t1748975512::get_offset_of_onInput_12(),
	UIEventListener_t1748975512::get_offset_of_onKey_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (VoidDelegate_t817628917), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (BoolDelegate_t192583560), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (FloatDelegate_t284650127), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (VectorDelegate_t1991147574), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (StringDelegate_t3231667884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (ObjectDelegate_t2232498032), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (KeyCodeDelegate_t1109435781), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (UIGeometry_t76808639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[6] = 
{
	UIGeometry_t76808639::get_offset_of_verts_0(),
	UIGeometry_t76808639::get_offset_of_uvs_1(),
	UIGeometry_t76808639::get_offset_of_cols_2(),
	UIGeometry_t76808639::get_offset_of_mRtpVerts_3(),
	UIGeometry_t76808639::get_offset_of_mRtpNormal_4(),
	UIGeometry_t76808639::get_offset_of_mRtpTan_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (UINode_t884938289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[9] = 
{
	UINode_t884938289::get_offset_of_mVisibleFlag_0(),
	UINode_t884938289::get_offset_of_trans_1(),
	UINode_t884938289::get_offset_of_widget_2(),
	UINode_t884938289::get_offset_of_lastActive_3(),
	UINode_t884938289::get_offset_of_lastPos_4(),
	UINode_t884938289::get_offset_of_lastRot_5(),
	UINode_t884938289::get_offset_of_lastScale_6(),
	UINode_t884938289::get_offset_of_changeFlag_7(),
	UINode_t884938289::get_offset_of_mGo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (UIWidget_t1961100141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[14] = 
{
	UIWidget_t1961100141::get_offset_of_mMat_2(),
	UIWidget_t1961100141::get_offset_of_mTex_3(),
	UIWidget_t1961100141::get_offset_of_mColor_4(),
	UIWidget_t1961100141::get_offset_of_mPivot_5(),
	UIWidget_t1961100141::get_offset_of_mDepth_6(),
	UIWidget_t1961100141::get_offset_of_mTrans_7(),
	UIWidget_t1961100141::get_offset_of_mPanel_8(),
	UIWidget_t1961100141::get_offset_of_mChanged_9(),
	UIWidget_t1961100141::get_offset_of_mPlayMode_10(),
	UIWidget_t1961100141::get_offset_of_mDiffPos_11(),
	UIWidget_t1961100141::get_offset_of_mDiffRot_12(),
	UIWidget_t1961100141::get_offset_of_mDiffScale_13(),
	UIWidget_t1961100141::get_offset_of_mVisibleFlag_14(),
	UIWidget_t1961100141::get_offset_of_mGeom_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (Pivot_t822355240)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2630[10] = 
{
	Pivot_t822355240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (UpdateManager_t1291149948), -1, sizeof(UpdateManager_t1291149948_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2631[7] = 
{
	UpdateManager_t1291149948_StaticFields::get_offset_of_mInst_2(),
	UpdateManager_t1291149948::get_offset_of_mOnUpdate_3(),
	UpdateManager_t1291149948::get_offset_of_mOnLate_4(),
	UpdateManager_t1291149948::get_offset_of_mOnCoro_5(),
	UpdateManager_t1291149948::get_offset_of_mDest_6(),
	UpdateManager_t1291149948::get_offset_of_mTime_7(),
	UpdateManager_t1291149948_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (OnUpdate_t172587279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (UpdateEntry_t2802166503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[4] = 
{
	UpdateEntry_t2802166503::get_offset_of_index_0(),
	UpdateEntry_t2802166503::get_offset_of_func_1(),
	UpdateEntry_t2802166503::get_offset_of_mb_2(),
	UpdateEntry_t2802166503::get_offset_of_isMonoBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (DestroyEntry_t50026793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[2] = 
{
	DestroyEntry_t50026793::get_offset_of_obj_0(),
	DestroyEntry_t50026793::get_offset_of_time_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (U3CCoroutineFunctionU3Ec__Iterator0_t2169398617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[4] = 
{
	U3CCoroutineFunctionU3Ec__Iterator0_t2169398617::get_offset_of_U24this_0(),
	U3CCoroutineFunctionU3Ec__Iterator0_t2169398617::get_offset_of_U24current_1(),
	U3CCoroutineFunctionU3Ec__Iterator0_t2169398617::get_offset_of_U24disposing_2(),
	U3CCoroutineFunctionU3Ec__Iterator0_t2169398617::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (SpringPosition_t1843463491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[9] = 
{
	SpringPosition_t1843463491::get_offset_of_target_6(),
	SpringPosition_t1843463491::get_offset_of_strength_7(),
	SpringPosition_t1843463491::get_offset_of_worldSpace_8(),
	SpringPosition_t1843463491::get_offset_of_ignoreTimeScale_9(),
	SpringPosition_t1843463491::get_offset_of_eventReceiver_10(),
	SpringPosition_t1843463491::get_offset_of_callWhenFinished_11(),
	SpringPosition_t1843463491::get_offset_of_onFinished_12(),
	SpringPosition_t1843463491::get_offset_of_mTrans_13(),
	SpringPosition_t1843463491::get_offset_of_mThreshold_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (OnFinished_t3829816540), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (TweenAlpha_t4056082079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[4] = 
{
	TweenAlpha_t4056082079::get_offset_of_from_20(),
	TweenAlpha_t4056082079::get_offset_of_to_21(),
	TweenAlpha_t4056082079::get_offset_of_mTrans_22(),
	TweenAlpha_t4056082079::get_offset_of_mWidget_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (TweenColor_t4163898564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[6] = 
{
	TweenColor_t4163898564::get_offset_of_from_20(),
	TweenColor_t4163898564::get_offset_of_to_21(),
	TweenColor_t4163898564::get_offset_of_mTrans_22(),
	TweenColor_t4163898564::get_offset_of_mWidget_23(),
	TweenColor_t4163898564::get_offset_of_mMat_24(),
	TweenColor_t4163898564::get_offset_of_mLight_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (TweenFOV_t2364281439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[3] = 
{
	TweenFOV_t2364281439::get_offset_of_from_20(),
	TweenFOV_t2364281439::get_offset_of_to_21(),
	TweenFOV_t2364281439::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (TweenPosition_t696968733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[3] = 
{
	TweenPosition_t696968733::get_offset_of_from_20(),
	TweenPosition_t696968733::get_offset_of_to_21(),
	TweenPosition_t696968733::get_offset_of_mTrans_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (TweenRotation_t3175222233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[3] = 
{
	TweenRotation_t3175222233::get_offset_of_from_20(),
	TweenRotation_t3175222233::get_offset_of_to_21(),
	TweenRotation_t3175222233::get_offset_of_mTrans_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (TweenScale_t312802622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[5] = 
{
	TweenScale_t312802622::get_offset_of_from_20(),
	TweenScale_t312802622::get_offset_of_to_21(),
	TweenScale_t312802622::get_offset_of_updateTable_22(),
	TweenScale_t312802622::get_offset_of_mTrans_23(),
	TweenScale_t312802622::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (TweenTransform_t3646711066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[7] = 
{
	TweenTransform_t3646711066::get_offset_of_from_20(),
	TweenTransform_t3646711066::get_offset_of_to_21(),
	TweenTransform_t3646711066::get_offset_of_parentWhenFinished_22(),
	TweenTransform_t3646711066::get_offset_of_mTrans_23(),
	TweenTransform_t3646711066::get_offset_of_mPos_24(),
	TweenTransform_t3646711066::get_offset_of_mRot_25(),
	TweenTransform_t3646711066::get_offset_of_mScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (TweenVolume_t3389378834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[3] = 
{
	TweenVolume_t3389378834::get_offset_of_from_20(),
	TweenVolume_t3389378834::get_offset_of_to_21(),
	TweenVolume_t3389378834::get_offset_of_mSource_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (UITweener_t1705010025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[14] = 
{
	UITweener_t1705010025::get_offset_of_onFinished_6(),
	UITweener_t1705010025::get_offset_of_method_7(),
	UITweener_t1705010025::get_offset_of_style_8(),
	UITweener_t1705010025::get_offset_of_ignoreTimeScale_9(),
	UITweener_t1705010025::get_offset_of_delay_10(),
	UITweener_t1705010025::get_offset_of_duration_11(),
	UITweener_t1705010025::get_offset_of_steeperCurves_12(),
	UITweener_t1705010025::get_offset_of_tweenGroup_13(),
	UITweener_t1705010025::get_offset_of_eventReceiver_14(),
	UITweener_t1705010025::get_offset_of_callWhenFinished_15(),
	UITweener_t1705010025::get_offset_of_mStartTime_16(),
	UITweener_t1705010025::get_offset_of_mDuration_17(),
	UITweener_t1705010025::get_offset_of_mAmountPerDelta_18(),
	UITweener_t1705010025::get_offset_of_mFactor_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (Method_t2440318679)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2647[7] = 
{
	Method_t2440318679::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (Style_t187796108)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2648[4] = 
{
	Style_t187796108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (OnFinished_t2354188770), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (UIAnchor_t1933351193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[10] = 
{
	UIAnchor_t1933351193::get_offset_of_mIsWindows_2(),
	UIAnchor_t1933351193::get_offset_of_uiCamera_3(),
	UIAnchor_t1933351193::get_offset_of_widgetContainer_4(),
	UIAnchor_t1933351193::get_offset_of_panelContainer_5(),
	UIAnchor_t1933351193::get_offset_of_side_6(),
	UIAnchor_t1933351193::get_offset_of_halfPixelOffset_7(),
	UIAnchor_t1933351193::get_offset_of_depthOffset_8(),
	UIAnchor_t1933351193::get_offset_of_relativeOffset_9(),
	UIAnchor_t1933351193::get_offset_of_mAnim_10(),
	UIAnchor_t1933351193::get_offset_of_mRoot_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (Side_t3253602166)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2651[10] = 
{
	Side_t3253602166::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (UIAtlas_t1815452364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[5] = 
{
	UIAtlas_t1815452364::get_offset_of_material_2(),
	UIAtlas_t1815452364::get_offset_of_sprites_3(),
	UIAtlas_t1815452364::get_offset_of_mCoordinates_4(),
	UIAtlas_t1815452364::get_offset_of_mPixelSize_5(),
	UIAtlas_t1815452364::get_offset_of_mReplacement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (Sprite_t3039180268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[7] = 
{
	Sprite_t3039180268::get_offset_of_name_0(),
	Sprite_t3039180268::get_offset_of_outer_1(),
	Sprite_t3039180268::get_offset_of_inner_2(),
	Sprite_t3039180268::get_offset_of_paddingLeft_3(),
	Sprite_t3039180268::get_offset_of_paddingRight_4(),
	Sprite_t3039180268::get_offset_of_paddingTop_5(),
	Sprite_t3039180268::get_offset_of_paddingBottom_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (Coordinates_t3113092471)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2654[3] = 
{
	Coordinates_t3113092471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (UICamera_t3843949198), -1, sizeof(UICamera_t3843949198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2655[46] = 
{
	UICamera_t3843949198::get_offset_of_useMouse_2(),
	UICamera_t3843949198::get_offset_of_useTouch_3(),
	UICamera_t3843949198::get_offset_of_allowMultiTouch_4(),
	UICamera_t3843949198::get_offset_of_useKeyboard_5(),
	UICamera_t3843949198::get_offset_of_useController_6(),
	UICamera_t3843949198::get_offset_of_eventReceiverMask_7(),
	UICamera_t3843949198::get_offset_of_clipRaycasts_8(),
	UICamera_t3843949198::get_offset_of_tooltipDelay_9(),
	UICamera_t3843949198::get_offset_of_stickyTooltip_10(),
	UICamera_t3843949198::get_offset_of_mouseDragThreshold_11(),
	UICamera_t3843949198::get_offset_of_mouseClickThreshold_12(),
	UICamera_t3843949198::get_offset_of_touchDragThreshold_13(),
	UICamera_t3843949198::get_offset_of_touchClickThreshold_14(),
	UICamera_t3843949198::get_offset_of_rangeDistance_15(),
	UICamera_t3843949198::get_offset_of_scrollAxisName_16(),
	UICamera_t3843949198::get_offset_of_verticalAxisName_17(),
	UICamera_t3843949198::get_offset_of_horizontalAxisName_18(),
	UICamera_t3843949198::get_offset_of_submitKey0_19(),
	UICamera_t3843949198::get_offset_of_submitKey1_20(),
	UICamera_t3843949198::get_offset_of_cancelKey0_21(),
	UICamera_t3843949198::get_offset_of_cancelKey1_22(),
	UICamera_t3843949198_StaticFields::get_offset_of_showTooltips_23(),
	UICamera_t3843949198_StaticFields::get_offset_of_lastTouchPosition_24(),
	UICamera_t3843949198_StaticFields::get_offset_of_lastHit_25(),
	UICamera_t3843949198_StaticFields::get_offset_of_current_26(),
	UICamera_t3843949198_StaticFields::get_offset_of_currentCamera_27(),
	UICamera_t3843949198_StaticFields::get_offset_of_currentTouchID_28(),
	UICamera_t3843949198_StaticFields::get_offset_of_currentTouch_29(),
	UICamera_t3843949198_StaticFields::get_offset_of_inputHasFocus_30(),
	UICamera_t3843949198_StaticFields::get_offset_of_genericEventHandler_31(),
	UICamera_t3843949198_StaticFields::get_offset_of_fallThrough_32(),
	UICamera_t3843949198_StaticFields::get_offset_of_mList_33(),
	UICamera_t3843949198_StaticFields::get_offset_of_mHighlighted_34(),
	UICamera_t3843949198_StaticFields::get_offset_of_mSel_35(),
	UICamera_t3843949198_StaticFields::get_offset_of_mMouse_36(),
	UICamera_t3843949198_StaticFields::get_offset_of_mHover_37(),
	UICamera_t3843949198_StaticFields::get_offset_of_mController_38(),
	UICamera_t3843949198_StaticFields::get_offset_of_mNextEvent_39(),
	UICamera_t3843949198::get_offset_of_mTouches_40(),
	UICamera_t3843949198::get_offset_of_mTooltip_41(),
	UICamera_t3843949198::get_offset_of_mCam_42(),
	UICamera_t3843949198::get_offset_of_mLayerMask_43(),
	UICamera_t3843949198::get_offset_of_mTooltipTime_44(),
	UICamera_t3843949198::get_offset_of_mIsEditor_45(),
	UICamera_t3843949198_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_46(),
	UICamera_t3843949198_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (ClickNotification_t601501802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[4] = 
{
	ClickNotification_t601501802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (MouseOrTouch_t115752944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[10] = 
{
	MouseOrTouch_t115752944::get_offset_of_pos_0(),
	MouseOrTouch_t115752944::get_offset_of_delta_1(),
	MouseOrTouch_t115752944::get_offset_of_totalDelta_2(),
	MouseOrTouch_t115752944::get_offset_of_pressedCam_3(),
	MouseOrTouch_t115752944::get_offset_of_current_4(),
	MouseOrTouch_t115752944::get_offset_of_pressed_5(),
	MouseOrTouch_t115752944::get_offset_of_clickTime_6(),
	MouseOrTouch_t115752944::get_offset_of_clickNotification_7(),
	MouseOrTouch_t115752944::get_offset_of_touchBegan_8(),
	MouseOrTouch_t115752944::get_offset_of_dragStarted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (Highlighted_t2037897245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[2] = 
{
	Highlighted_t2037897245::get_offset_of_go_0(),
	Highlighted_t2037897245::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (UIFilledSprite_t3395723239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	UIFilledSprite_t3395723239::get_offset_of_mFillDirection_23(),
	UIFilledSprite_t3395723239::get_offset_of_mFillAmount_24(),
	UIFilledSprite_t3395723239::get_offset_of_mInvert_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (FillDirection_t2955200481)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2660[6] = 
{
	FillDirection_t2955200481::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (UIFont_t2730669065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[10] = 
{
	UIFont_t2730669065::get_offset_of_mMat_2(),
	UIFont_t2730669065::get_offset_of_mUVRect_3(),
	UIFont_t2730669065::get_offset_of_mFont_4(),
	UIFont_t2730669065::get_offset_of_mSpacingX_5(),
	UIFont_t2730669065::get_offset_of_mSpacingY_6(),
	UIFont_t2730669065::get_offset_of_mAtlas_7(),
	UIFont_t2730669065::get_offset_of_mReplacement_8(),
	UIFont_t2730669065::get_offset_of_mSprite_9(),
	UIFont_t2730669065::get_offset_of_mSpriteSet_10(),
	UIFont_t2730669065::get_offset_of_mColors_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (Alignment_t93203161)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2662[4] = 
{
	Alignment_t93203161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (SymbolStyle_t3548150665)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[4] = 
{
	SymbolStyle_t3548150665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (UIInput_t1674607152), -1, sizeof(UIInput_t1674607152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[18] = 
{
	UIInput_t1674607152_StaticFields::get_offset_of_current_2(),
	UIInput_t1674607152::get_offset_of_label_3(),
	UIInput_t1674607152::get_offset_of_maxChars_4(),
	UIInput_t1674607152::get_offset_of_caratChar_5(),
	UIInput_t1674607152::get_offset_of_validator_6(),
	UIInput_t1674607152::get_offset_of_type_7(),
	UIInput_t1674607152::get_offset_of_isPassword_8(),
	UIInput_t1674607152::get_offset_of_activeColor_9(),
	UIInput_t1674607152::get_offset_of_eventReceiver_10(),
	UIInput_t1674607152::get_offset_of_functionName_11(),
	UIInput_t1674607152::get_offset_of_onSubmit_12(),
	UIInput_t1674607152::get_offset_of_mText_13(),
	UIInput_t1674607152::get_offset_of_mDefaultText_14(),
	UIInput_t1674607152::get_offset_of_mDefaultColor_15(),
	UIInput_t1674607152::get_offset_of_mPivot_16(),
	UIInput_t1674607152::get_offset_of_mPosition_17(),
	UIInput_t1674607152::get_offset_of_mKeyboard_18(),
	UIInput_t1674607152::get_offset_of_mDoInit_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (Validator_t1053183372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (KeyboardType_t2903380147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2666[9] = 
{
	KeyboardType_t2903380147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (OnSubmit_t2511512930), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (UIInputSaved_t3772643040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[1] = 
{
	UIInputSaved_t3772643040::get_offset_of_playerPrefsField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (UILabel_t3478074517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[24] = 
{
	UILabel_t3478074517::get_offset_of_mFont_16(),
	UILabel_t3478074517::get_offset_of_mText_17(),
	UILabel_t3478074517::get_offset_of_mMaxLineWidth_18(),
	UILabel_t3478074517::get_offset_of_mEncoding_19(),
	UILabel_t3478074517::get_offset_of_mMaxLineCount_20(),
	UILabel_t3478074517::get_offset_of_mPassword_21(),
	UILabel_t3478074517::get_offset_of_mShowLastChar_22(),
	UILabel_t3478074517::get_offset_of_mEffectStyle_23(),
	UILabel_t3478074517::get_offset_of_mEffectColor_24(),
	UILabel_t3478074517::get_offset_of_mSymbols_25(),
	UILabel_t3478074517::get_offset_of_mEffectDistance_26(),
	UILabel_t3478074517::get_offset_of_mLineWidth_27(),
	UILabel_t3478074517::get_offset_of_mMultiline_28(),
	UILabel_t3478074517::get_offset_of_mShouldBeProcessed_29(),
	UILabel_t3478074517::get_offset_of_mProcessedText_30(),
	UILabel_t3478074517::get_offset_of_mLastScale_31(),
	UILabel_t3478074517::get_offset_of_mLastText_32(),
	UILabel_t3478074517::get_offset_of_mLastWidth_33(),
	UILabel_t3478074517::get_offset_of_mLastEncoding_34(),
	UILabel_t3478074517::get_offset_of_mLastCount_35(),
	UILabel_t3478074517::get_offset_of_mLastPass_36(),
	UILabel_t3478074517::get_offset_of_mLastShow_37(),
	UILabel_t3478074517::get_offset_of_mLastEffect_38(),
	UILabel_t3478074517::get_offset_of_mSize_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (Effect_t450438489)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2670[4] = 
{
	Effect_t450438489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (UILocalize_t3299257004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	UILocalize_t3299257004::get_offset_of_key_2(),
	UILocalize_t3299257004::get_offset_of_mLanguage_3(),
	UILocalize_t3299257004::get_offset_of_mStarted_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (UIOrthoCamera_t3857640393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[2] = 
{
	UIOrthoCamera_t3857640393::get_offset_of_mCam_2(),
	UIOrthoCamera_t3857640393::get_offset_of_mTrans_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (UIPanel_t825970685), -1, sizeof(UIPanel_t825970685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2673[36] = 
{
	UIPanel_t825970685_StaticFields::get_offset_of_instanceRef_2(),
	UIPanel_t825970685::get_offset_of_showInPanelTool_3(),
	UIPanel_t825970685::get_offset_of_generateNormals_4(),
	UIPanel_t825970685::get_offset_of_depthPass_5(),
	UIPanel_t825970685::get_offset_of_widgetsAreStatic_6(),
	UIPanel_t825970685::get_offset_of_mDebugInfo_7(),
	UIPanel_t825970685::get_offset_of_mClipping_8(),
	UIPanel_t825970685::get_offset_of_mClipRange_9(),
	UIPanel_t825970685::get_offset_of_mClipSoftness_10(),
	UIPanel_t825970685::get_offset_of_mChildren_11(),
	UIPanel_t825970685::get_offset_of_mWidgets_12(),
	UIPanel_t825970685::get_offset_of_mChanged_13(),
	UIPanel_t825970685::get_offset_of_mDrawCalls_14(),
	UIPanel_t825970685::get_offset_of_mVerts_15(),
	UIPanel_t825970685::get_offset_of_mNorms_16(),
	UIPanel_t825970685::get_offset_of_mTans_17(),
	UIPanel_t825970685::get_offset_of_mUvs_18(),
	UIPanel_t825970685::get_offset_of_mCols_19(),
	UIPanel_t825970685::get_offset_of_mTrans_20(),
	UIPanel_t825970685::get_offset_of_mCam_21(),
	UIPanel_t825970685::get_offset_of_mLayer_22(),
	UIPanel_t825970685::get_offset_of_mDepthChanged_23(),
	UIPanel_t825970685::get_offset_of_mRebuildAll_24(),
	UIPanel_t825970685::get_offset_of_mChangedLastFrame_25(),
	UIPanel_t825970685::get_offset_of_mWidgetsAdded_26(),
	UIPanel_t825970685::get_offset_of_mMatrixTime_27(),
	UIPanel_t825970685::get_offset_of_mWorldToLocal_28(),
	UIPanel_t825970685_StaticFields::get_offset_of_mTemp_29(),
	UIPanel_t825970685::get_offset_of_mMin_30(),
	UIPanel_t825970685::get_offset_of_mMax_31(),
	UIPanel_t825970685::get_offset_of_mRemoved_32(),
	UIPanel_t825970685::get_offset_of_mCheckVisibility_33(),
	UIPanel_t825970685::get_offset_of_mCullTime_34(),
	UIPanel_t825970685::get_offset_of_mCulled_35(),
	UIPanel_t825970685_StaticFields::get_offset_of_mHierarchy_36(),
	UIPanel_t825970685_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (DebugInfo_t3987478991)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	DebugInfo_t3987478991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (UIRoot_t937406327), -1, sizeof(UIRoot_t937406327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2675[6] = 
{
	UIRoot_t937406327_StaticFields::get_offset_of_mRoots_2(),
	UIRoot_t937406327::get_offset_of_mTrans_3(),
	UIRoot_t937406327::get_offset_of_automatic_4(),
	UIRoot_t937406327::get_offset_of_manualHeight_5(),
	UIRoot_t937406327::get_offset_of_minimumHeight_6(),
	UIRoot_t937406327::get_offset_of_maximumHeight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (UISlicedSprite_t3319190482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[4] = 
{
	UISlicedSprite_t3319190482::get_offset_of_mFillCenter_23(),
	UISlicedSprite_t3319190482::get_offset_of_mInner_24(),
	UISlicedSprite_t3319190482::get_offset_of_mInnerUV_25(),
	UISlicedSprite_t3319190482::get_offset_of_mScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (UISprite_t279728715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[7] = 
{
	UISprite_t279728715::get_offset_of_mAtlas_16(),
	UISprite_t279728715::get_offset_of_mSpriteName_17(),
	UISprite_t279728715::get_offset_of_mSprite_18(),
	UISprite_t279728715::get_offset_of_mOuter_19(),
	UISprite_t279728715::get_offset_of_mOuterUV_20(),
	UISprite_t279728715::get_offset_of_mSpriteSet_21(),
	UISprite_t279728715::get_offset_of_mLastName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (UISpriteAnimation_t1906024296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[8] = 
{
	UISpriteAnimation_t1906024296::get_offset_of_mFPS_2(),
	UISpriteAnimation_t1906024296::get_offset_of_mPrefix_3(),
	UISpriteAnimation_t1906024296::get_offset_of_mLoop_4(),
	UISpriteAnimation_t1906024296::get_offset_of_mSprite_5(),
	UISpriteAnimation_t1906024296::get_offset_of_mDelta_6(),
	UISpriteAnimation_t1906024296::get_offset_of_mIndex_7(),
	UISpriteAnimation_t1906024296::get_offset_of_mActive_8(),
	UISpriteAnimation_t1906024296::get_offset_of_mSpriteNames_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (UIStretch_t950945497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[8] = 
{
	UIStretch_t950945497::get_offset_of_uiCamera_2(),
	UIStretch_t950945497::get_offset_of_widgetContainer_3(),
	UIStretch_t950945497::get_offset_of_panelContainer_4(),
	UIStretch_t950945497::get_offset_of_style_5(),
	UIStretch_t950945497::get_offset_of_relativeSize_6(),
	UIStretch_t950945497::get_offset_of_mTrans_7(),
	UIStretch_t950945497::get_offset_of_mRoot_8(),
	UIStretch_t950945497::get_offset_of_mAnim_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (Style_t2353225633)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2680[6] = 
{
	Style_t2353225633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (UITextList_t1100452823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[11] = 
{
	UITextList_t1100452823::get_offset_of_style_2(),
	UITextList_t1100452823::get_offset_of_textLabel_3(),
	UITextList_t1100452823::get_offset_of_maxWidth_4(),
	UITextList_t1100452823::get_offset_of_maxHeight_5(),
	UITextList_t1100452823::get_offset_of_maxEntries_6(),
	UITextList_t1100452823::get_offset_of_supportScrollWheel_7(),
	UITextList_t1100452823::get_offset_of_mSeparator_8(),
	UITextList_t1100452823::get_offset_of_mParagraphs_9(),
	UITextList_t1100452823::get_offset_of_mScroll_10(),
	UITextList_t1100452823::get_offset_of_mSelected_11(),
	UITextList_t1100452823::get_offset_of_mTotalLines_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (Style_t2647733567)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2682[3] = 
{
	Style_t2647733567::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (Paragraph_t3751327762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[2] = 
{
	Paragraph_t3751327762::get_offset_of_text_0(),
	Paragraph_t3751327762::get_offset_of_lines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (UITexture_t976444930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[5] = 
{
	UITexture_t976444930::get_offset_of_mRect_16(),
	UITexture_t976444930::get_offset_of_mShader_17(),
	UITexture_t976444930::get_offset_of_mTexture_18(),
	UITexture_t976444930::get_offset_of_mDynamicMat_19(),
	UITexture_t976444930::get_offset_of_mCreatingMat_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (UITiledSprite_t2195056393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (UITooltip_t1085649404), -1, sizeof(UITooltip_t1085649404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2686[12] = 
{
	UITooltip_t1085649404_StaticFields::get_offset_of_mInstance_2(),
	UITooltip_t1085649404::get_offset_of_uiCamera_3(),
	UITooltip_t1085649404::get_offset_of_text_4(),
	UITooltip_t1085649404::get_offset_of_background_5(),
	UITooltip_t1085649404::get_offset_of_appearSpeed_6(),
	UITooltip_t1085649404::get_offset_of_scalingTransitions_7(),
	UITooltip_t1085649404::get_offset_of_mTrans_8(),
	UITooltip_t1085649404::get_offset_of_mTarget_9(),
	UITooltip_t1085649404::get_offset_of_mCurrent_10(),
	UITooltip_t1085649404::get_offset_of_mPos_11(),
	UITooltip_t1085649404::get_offset_of_mSize_12(),
	UITooltip_t1085649404::get_offset_of_mWidgets_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (UIViewport_t3844485370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[5] = 
{
	UIViewport_t3844485370::get_offset_of_sourceCamera_2(),
	UIViewport_t3844485370::get_offset_of_topLeft_3(),
	UIViewport_t3844485370::get_offset_of_bottomRight_4(),
	UIViewport_t3844485370::get_offset_of_fullSize_5(),
	UIViewport_t3844485370::get_offset_of_mCam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (OutlineObject_t941793957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[3] = 
{
	OutlineObject_t941793957::get_offset_of_mat_2(),
	OutlineObject_t941793957::get_offset_of_shaderOn_3(),
	OutlineObject_t941793957::get_offset_of_enableOnAwake_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (PickAThingDialog_t1829493947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[6] = 
{
	PickAThingDialog_t1829493947::get_offset_of_button_2(),
	PickAThingDialog_t1829493947::get_offset_of_buttonGap_3(),
	PickAThingDialog_t1829493947::get_offset_of_offset_4(),
	PickAThingDialog_t1829493947::get_offset_of_numThings_5(),
	PickAThingDialog_t1829493947::get_offset_of_buttons_6(),
	PickAThingDialog_t1829493947::get_offset_of_callback_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (PickAThingCallback_t4122863115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (GetSetAttribute_t596627455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	GetSetAttribute_t596627455::get_offset_of_name_0(),
	GetSetAttribute_t596627455::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (MinAttribute_t1517261931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[1] = 
{
	MinAttribute_t1517261931::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (TrackballAttribute_t3757329946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[1] = 
{
	TrackballAttribute_t3757329946::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (TrackballGroupAttribute_t2573285421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (AmbientOcclusionComponent_t2853146608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[3] = 
{
	0,
	0,
	AmbientOcclusionComponent_t2853146608::get_offset_of_m_MRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (Uniforms_t3304466817), -1, sizeof(Uniforms_t3304466817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2696[9] = 
{
	Uniforms_t3304466817_StaticFields::get_offset_of__Intensity_0(),
	Uniforms_t3304466817_StaticFields::get_offset_of__Radius_1(),
	Uniforms_t3304466817_StaticFields::get_offset_of__Downsample_2(),
	Uniforms_t3304466817_StaticFields::get_offset_of__SampleCount_3(),
	Uniforms_t3304466817_StaticFields::get_offset_of__OcclusionTexture1_4(),
	Uniforms_t3304466817_StaticFields::get_offset_of__OcclusionTexture2_5(),
	Uniforms_t3304466817_StaticFields::get_offset_of__OcclusionTexture_6(),
	Uniforms_t3304466817_StaticFields::get_offset_of__MainTex_7(),
	Uniforms_t3304466817_StaticFields::get_offset_of__TempRT_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (OcclusionSource_t1463809940)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2697[4] = 
{
	OcclusionSource_t1463809940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (BloomComponent_t3971738366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[3] = 
{
	0,
	BloomComponent_t3971738366::get_offset_of_m_BlurBuffer1_3(),
	BloomComponent_t3971738366::get_offset_of_m_BlurBuffer2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (Uniforms_t477084279), -1, sizeof(Uniforms_t477084279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2699[10] = 
{
	Uniforms_t477084279_StaticFields::get_offset_of__AutoExposure_0(),
	Uniforms_t477084279_StaticFields::get_offset_of__Threshold_1(),
	Uniforms_t477084279_StaticFields::get_offset_of__Curve_2(),
	Uniforms_t477084279_StaticFields::get_offset_of__PrefilterOffs_3(),
	Uniforms_t477084279_StaticFields::get_offset_of__SampleScale_4(),
	Uniforms_t477084279_StaticFields::get_offset_of__BaseTex_5(),
	Uniforms_t477084279_StaticFields::get_offset_of__BloomTex_6(),
	Uniforms_t477084279_StaticFields::get_offset_of__Bloom_Settings_7(),
	Uniforms_t477084279_StaticFields::get_offset_of__Bloom_DirtTex_8(),
	Uniforms_t477084279_StaticFields::get_offset_of__Bloom_DirtIntensity_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
