﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// System.Xml.XmlNode
struct XmlNode_t3729859639;
// System.Collections.ArrayList
struct ArrayList_t4250946984;
// System.Collections.Stack
struct Stack_t535311253;
// System.Xml.XmlParserInput/XmlParserInputSource
struct XmlParserInputSource_t1595232959;
// System.String
struct String_t;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t2320539231;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t190893096;
// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t4178255916;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1947670907;
// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t2823685959;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t596510042;
// System.Xml.XmlDocument
struct XmlDocument_t2823332853;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t4201287521;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t1018678279;
// System.Xml.XmlNamespaceManager/NsScope[]
struct NsScopeU5BU5D_t1449259375;
// System.Xml.XmlNameTable
struct XmlNameTable_t347431366;
// System.IO.TextReader
struct TextReader_t218744201;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Globalization.CultureInfo
struct CultureInfo_t2625678720;
// System.Globalization.CompareInfo
struct CompareInfo_t2505958045;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t1006717423;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Net.ICredentials
struct ICredentials_t3564800082;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t3875484823;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t983112148;
// System.Type
struct Type_t;
// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement>
struct IntervalTree_1_t2495587357;
// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement>
struct List_1_t4284214879;
// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback>
struct List_1_t3724562642;
// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct Dictionary_2_t577450642;
// System.Void
struct Void_t653366341;
// System.Text.Encoding
struct Encoding_t4004889433;
// System.IO.Stream
struct Stream_t4138477179;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.Xml.XmlException
struct XmlException_t3531299539;
// System.Text.Decoder
struct Decoder_t4109873929;
// System.Text.StringBuilder
struct StringBuilder_t622404039;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Xml.XmlImplementation
struct XmlImplementation_t3490314784;
// System.Xml.XmlResolver
struct XmlResolver_t161349657;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t728926030;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t93272820;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t784538573;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t3906219045;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t4038918308;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t3150902482;
// System.Xml.XmlInputStream
struct XmlInputStream_t3210582043;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t3528594990;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t4186695123;
// UnityEngine.Object
struct Object_t692178351;
// System.IO.TextWriter
struct TextWriter_t997444721;
// System.IO.StringWriter
struct StringWriter_t1837008501;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t1354331288;
// System.Xml.XmlTextWriter/XmlNodeInfo[]
struct XmlNodeInfoU5BU5D_t1395238467;
// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct XmlTokenInfo_t2043079912;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct XmlAttributeTokenInfo_t2488827827;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[]
struct XmlAttributeTokenInfoU5BU5D_t827491362;
// Mono.Xml2.XmlTextReader/XmlTokenInfo[]
struct XmlTokenInfoU5BU5D_t3383196281;
// System.Xml.XmlParserContext
struct XmlParserContext_t3802687902;
// Mono.Xml2.XmlTextReader/TagName[]
struct TagNameU5BU5D_t2618800335;
// System.Xml.XmlReaderBinarySupport/CharGetter
struct CharGetter_t1293008504;
// System.Xml.NameTable
struct NameTable_t3507653384;
// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct DtdInputStateStack_t2344161889;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// UnityEngine.AnimationClip
struct AnimationClip_t1145879031;
// System.Xml.XmlReader
struct XmlReader_t1082502433;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerator_1_t3020182143;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding>
struct IEnumerator_1_t1928540238;
// UnityEngine.Timeline.TimelineAsset
struct TimelineAsset_t452681751;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t2510260633;
// System.Xml.XmlNodeChangedEventArgs
struct XmlNodeChangedEventArgs_t2915371093;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_t1943312966;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_t984688574;
// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_t2562619115;
// System.Predicate`1<UnityEngine.Timeline.TrackAsset>
struct Predicate_1_t231338901;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t1461289625;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t1733651323;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t2389961773;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t1609610637;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t3874041483;
// UnityEngine.Timeline.ActivationMixerPlayable
struct ActivationMixerPlayable_t1415536146;

struct Object_t692178351_marshaled_com;



#ifndef U3CMODULEU3E_T1429447285_H
#define U3CMODULEU3E_T1429447285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447285 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447285_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLNAMEDNODEMAP_T4038918308_H
#define XMLNAMEDNODEMAP_T4038918308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t4038918308  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t3729859639 * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t4250946984 * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___parent_1)); }
	inline XmlNode_t3729859639 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t3729859639 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t3729859639 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___nodeList_2)); }
	inline ArrayList_t4250946984 * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t4250946984 ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t4250946984 * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_t4038918308_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T4038918308_H
#ifndef XMLPARSERINPUT_T2512531096_H
#define XMLPARSERINPUT_T2512531096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput
struct  XmlParserInput_t2512531096  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Xml.XmlParserInput::sourceStack
	Stack_t535311253 * ___sourceStack_0;
	// System.Xml.XmlParserInput/XmlParserInputSource System.Xml.XmlParserInput::source
	XmlParserInputSource_t1595232959 * ___source_1;
	// System.Boolean System.Xml.XmlParserInput::has_peek
	bool ___has_peek_2;
	// System.Int32 System.Xml.XmlParserInput::peek_char
	int32_t ___peek_char_3;
	// System.Boolean System.Xml.XmlParserInput::allowTextDecl
	bool ___allowTextDecl_4;

public:
	inline static int32_t get_offset_of_sourceStack_0() { return static_cast<int32_t>(offsetof(XmlParserInput_t2512531096, ___sourceStack_0)); }
	inline Stack_t535311253 * get_sourceStack_0() const { return ___sourceStack_0; }
	inline Stack_t535311253 ** get_address_of_sourceStack_0() { return &___sourceStack_0; }
	inline void set_sourceStack_0(Stack_t535311253 * value)
	{
		___sourceStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceStack_0), value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(XmlParserInput_t2512531096, ___source_1)); }
	inline XmlParserInputSource_t1595232959 * get_source_1() const { return ___source_1; }
	inline XmlParserInputSource_t1595232959 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(XmlParserInputSource_t1595232959 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_has_peek_2() { return static_cast<int32_t>(offsetof(XmlParserInput_t2512531096, ___has_peek_2)); }
	inline bool get_has_peek_2() const { return ___has_peek_2; }
	inline bool* get_address_of_has_peek_2() { return &___has_peek_2; }
	inline void set_has_peek_2(bool value)
	{
		___has_peek_2 = value;
	}

	inline static int32_t get_offset_of_peek_char_3() { return static_cast<int32_t>(offsetof(XmlParserInput_t2512531096, ___peek_char_3)); }
	inline int32_t get_peek_char_3() const { return ___peek_char_3; }
	inline int32_t* get_address_of_peek_char_3() { return &___peek_char_3; }
	inline void set_peek_char_3(int32_t value)
	{
		___peek_char_3 = value;
	}

	inline static int32_t get_offset_of_allowTextDecl_4() { return static_cast<int32_t>(offsetof(XmlParserInput_t2512531096, ___allowTextDecl_4)); }
	inline bool get_allowTextDecl_4() const { return ___allowTextDecl_4; }
	inline bool* get_address_of_allowTextDecl_4() { return &___allowTextDecl_4; }
	inline void set_allowTextDecl_4(bool value)
	{
		___allowTextDecl_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUT_T2512531096_H
#ifndef XMLQUALIFIEDNAME_T740477960_H
#define XMLQUALIFIEDNAME_T740477960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t740477960  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t740477960, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t740477960, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t740477960, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t740477960_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t740477960 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t740477960_StaticFields, ___Empty_0)); }
	inline XmlQualifiedName_t740477960 * get_Empty_0() const { return ___Empty_0; }
	inline XmlQualifiedName_t740477960 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(XmlQualifiedName_t740477960 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLQUALIFIEDNAME_T740477960_H
#ifndef PLACEHOLDER_T146813599_H
#define PLACEHOLDER_T146813599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.Placeholder
struct  Placeholder_t146813599  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEHOLDER_T146813599_H
#ifndef EDITORSETTINGS_T2562619115_H
#define EDITORSETTINGS_T2562619115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct  EditorSettings_t2562619115  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::fps
	float ___fps_1;

public:
	inline static int32_t get_offset_of_fps_1() { return static_cast<int32_t>(offsetof(EditorSettings_t2562619115, ___fps_1)); }
	inline float get_fps_1() const { return ___fps_1; }
	inline float* get_address_of_fps_1() { return &___fps_1; }
	inline void set_fps_1(float value)
	{
		___fps_1 = value;
	}
};

struct EditorSettings_t2562619115_StaticFields
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kDefaultFPS
	float ___kDefaultFPS_0;

public:
	inline static int32_t get_offset_of_kDefaultFPS_0() { return static_cast<int32_t>(offsetof(EditorSettings_t2562619115_StaticFields, ___kDefaultFPS_0)); }
	inline float get_kDefaultFPS_0() const { return ___kDefaultFPS_0; }
	inline float* get_address_of_kDefaultFPS_0() { return &___kDefaultFPS_0; }
	inline void set_kDefaultFPS_0(float value)
	{
		___kDefaultFPS_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORSETTINGS_T2562619115_H
#ifndef XMLREADER_T1082502433_H
#define XMLREADER_T1082502433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t1082502433  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t2320539231 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t190893096 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t1082502433, ___binary_0)); }
	inline XmlReaderBinarySupport_t2320539231 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t2320539231 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t2320539231 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t1082502433, ___settings_1)); }
	inline XmlReaderSettings_t190893096 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t190893096 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t190893096 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T1082502433_H
#ifndef ENUMERATOR_T412118962_H
#define ENUMERATOR_T412118962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren/Enumerator
struct  Enumerator_t412118962  : public RuntimeObject
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren/Enumerator::parent
	RuntimeObject* ___parent_0;
	// System.Xml.XmlLinkedNode System.Xml.XmlNodeListChildren/Enumerator::currentChild
	XmlLinkedNode_t1947670907 * ___currentChild_1;
	// System.Boolean System.Xml.XmlNodeListChildren/Enumerator::passedLastNode
	bool ___passedLastNode_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t412118962, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_currentChild_1() { return static_cast<int32_t>(offsetof(Enumerator_t412118962, ___currentChild_1)); }
	inline XmlLinkedNode_t1947670907 * get_currentChild_1() const { return ___currentChild_1; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_currentChild_1() { return &___currentChild_1; }
	inline void set_currentChild_1(XmlLinkedNode_t1947670907 * value)
	{
		___currentChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChild_1), value);
	}

	inline static int32_t get_offset_of_passedLastNode_2() { return static_cast<int32_t>(offsetof(Enumerator_t412118962, ___passedLastNode_2)); }
	inline bool get_passedLastNode_2() const { return ___passedLastNode_2; }
	inline bool* get_address_of_passedLastNode_2() { return &___passedLastNode_2; }
	inline void set_passedLastNode_2(bool value)
	{
		___passedLastNode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T412118962_H
#ifndef XMLNODELIST_T3849781475_H
#define XMLNODELIST_T3849781475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_t3849781475  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_T3849781475_H
#ifndef DTDINPUTSTATESTACK_T2344161889_H
#define DTDINPUTSTATESTACK_T2344161889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct  DtdInputStateStack_t2344161889  : public RuntimeObject
{
public:
	// System.Collections.Stack Mono.Xml2.XmlTextReader/DtdInputStateStack::intern
	Stack_t535311253 * ___intern_0;

public:
	inline static int32_t get_offset_of_intern_0() { return static_cast<int32_t>(offsetof(DtdInputStateStack_t2344161889, ___intern_0)); }
	inline Stack_t535311253 * get_intern_0() const { return ___intern_0; }
	inline Stack_t535311253 ** get_address_of_intern_0() { return &___intern_0; }
	inline void set_intern_0(Stack_t535311253 * value)
	{
		___intern_0 = value;
		Il2CppCodeGenWriteBarrier((&___intern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATESTACK_T2344161889_H
#ifndef TIMELINEMARKER_T3685410445_H
#define TIMELINEMARKER_T3685410445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineMarker
struct  TimelineMarker_t3685410445  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineMarker::m_Key
	String_t* ___m_Key_0;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineMarker::m_ParentTrack
	TrackAsset_t2823685959 * ___m_ParentTrack_1;
	// System.Double UnityEngine.Timeline.TimelineMarker::m_Time
	double ___m_Time_2;
	// System.Boolean UnityEngine.Timeline.TimelineMarker::m_Selected
	bool ___m_Selected_3;

public:
	inline static int32_t get_offset_of_m_Key_0() { return static_cast<int32_t>(offsetof(TimelineMarker_t3685410445, ___m_Key_0)); }
	inline String_t* get_m_Key_0() const { return ___m_Key_0; }
	inline String_t** get_address_of_m_Key_0() { return &___m_Key_0; }
	inline void set_m_Key_0(String_t* value)
	{
		___m_Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_0), value);
	}

	inline static int32_t get_offset_of_m_ParentTrack_1() { return static_cast<int32_t>(offsetof(TimelineMarker_t3685410445, ___m_ParentTrack_1)); }
	inline TrackAsset_t2823685959 * get_m_ParentTrack_1() const { return ___m_ParentTrack_1; }
	inline TrackAsset_t2823685959 ** get_address_of_m_ParentTrack_1() { return &___m_ParentTrack_1; }
	inline void set_m_ParentTrack_1(TrackAsset_t2823685959 * value)
	{
		___m_ParentTrack_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTrack_1), value);
	}

	inline static int32_t get_offset_of_m_Time_2() { return static_cast<int32_t>(offsetof(TimelineMarker_t3685410445, ___m_Time_2)); }
	inline double get_m_Time_2() const { return ___m_Time_2; }
	inline double* get_address_of_m_Time_2() { return &___m_Time_2; }
	inline void set_m_Time_2(double value)
	{
		___m_Time_2 = value;
	}

	inline static int32_t get_offset_of_m_Selected_3() { return static_cast<int32_t>(offsetof(TimelineMarker_t3685410445, ___m_Selected_3)); }
	inline bool get_m_Selected_3() const { return ___m_Selected_3; }
	inline bool* get_address_of_m_Selected_3() { return &___m_Selected_3; }
	inline void set_m_Selected_3(bool value)
	{
		___m_Selected_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEMARKER_T3685410445_H
#ifndef XMLNODE_T3729859639_H
#define XMLNODE_T3729859639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3729859639  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t2823332853 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3729859639 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t4201287521 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___ownerDocument_1)); }
	inline XmlDocument_t2823332853 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t2823332853 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t2823332853 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___parentNode_2)); }
	inline XmlNode_t3729859639 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t3729859639 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t3729859639 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___childNodes_3)); }
	inline XmlNodeListChildren_t4201287521 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t4201287521 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t4201287521 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t3729859639_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t596510042 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map44_4;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t596510042 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t596510042 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t596510042 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3729859639_H
#ifndef XMLNAMESPACEMANAGER_T1354331288_H
#define XMLNAMESPACEMANAGER_T1354331288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t1354331288  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_t1018678279* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.Xml.XmlNamespaceManager/NsScope[] System.Xml.XmlNamespaceManager::scopes
	NsScopeU5BU5D_t1449259375* ___scopes_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopePos
	int32_t ___scopePos_3;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_4;
	// System.Int32 System.Xml.XmlNamespaceManager::count
	int32_t ___count_5;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t347431366 * ___nameTable_6;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_7;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___decls_0)); }
	inline NsDeclU5BU5D_t1018678279* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_t1018678279** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_t1018678279* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_scopes_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___scopes_2)); }
	inline NsScopeU5BU5D_t1449259375* get_scopes_2() const { return ___scopes_2; }
	inline NsScopeU5BU5D_t1449259375** get_address_of_scopes_2() { return &___scopes_2; }
	inline void set_scopes_2(NsScopeU5BU5D_t1449259375* value)
	{
		___scopes_2 = value;
		Il2CppCodeGenWriteBarrier((&___scopes_2), value);
	}

	inline static int32_t get_offset_of_scopePos_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___scopePos_3)); }
	inline int32_t get_scopePos_3() const { return ___scopePos_3; }
	inline int32_t* get_address_of_scopePos_3() { return &___scopePos_3; }
	inline void set_scopePos_3(int32_t value)
	{
		___scopePos_3 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___defaultNamespace_4)); }
	inline String_t* get_defaultNamespace_4() const { return ___defaultNamespace_4; }
	inline String_t** get_address_of_defaultNamespace_4() { return &___defaultNamespace_4; }
	inline void set_defaultNamespace_4(String_t* value)
	{
		___defaultNamespace_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_nameTable_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___nameTable_6)); }
	inline XmlNameTable_t347431366 * get_nameTable_6() const { return ___nameTable_6; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_6() { return &___nameTable_6; }
	inline void set_nameTable_6(XmlNameTable_t347431366 * value)
	{
		___nameTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_6), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288, ___internalAtomizedNames_7)); }
	inline bool get_internalAtomizedNames_7() const { return ___internalAtomizedNames_7; }
	inline bool* get_address_of_internalAtomizedNames_7() { return &___internalAtomizedNames_7; }
	inline void set_internalAtomizedNames_7(bool value)
	{
		___internalAtomizedNames_7 = value;
	}
};

struct XmlNamespaceManager_t1354331288_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switch$map28
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map28_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_8() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t1354331288_StaticFields, ___U3CU3Ef__switchU24map28_8)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map28_8() const { return ___U3CU3Ef__switchU24map28_8; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map28_8() { return &___U3CU3Ef__switchU24map28_8; }
	inline void set_U3CU3Ef__switchU24map28_8(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map28_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map28_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T1354331288_H
#ifndef XMLPARSERINPUTSOURCE_T1595232959_H
#define XMLPARSERINPUTSOURCE_T1595232959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput/XmlParserInputSource
struct  XmlParserInputSource_t1595232959  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserInput/XmlParserInputSource::BaseURI
	String_t* ___BaseURI_0;
	// System.IO.TextReader System.Xml.XmlParserInput/XmlParserInputSource::reader
	TextReader_t218744201 * ___reader_1;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlParserInput/XmlParserInputSource::isPE
	bool ___isPE_3;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::line
	int32_t ___line_4;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::column
	int32_t ___column_5;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___reader_1)); }
	inline TextReader_t218744201 * get_reader_1() const { return ___reader_1; }
	inline TextReader_t218744201 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(TextReader_t218744201 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_isPE_3() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___isPE_3)); }
	inline bool get_isPE_3() const { return ___isPE_3; }
	inline bool* get_address_of_isPE_3() { return &___isPE_3; }
	inline void set_isPE_3(bool value)
	{
		___isPE_3 = value;
	}

	inline static int32_t get_offset_of_line_4() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___line_4)); }
	inline int32_t get_line_4() const { return ___line_4; }
	inline int32_t* get_address_of_line_4() { return &___line_4; }
	inline void set_line_4(int32_t value)
	{
		___line_4 = value;
	}

	inline static int32_t get_offset_of_column_5() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t1595232959, ___column_5)); }
	inline int32_t get_column_5() const { return ___column_5; }
	inline int32_t* get_address_of_column_5() { return &___column_5; }
	inline void set_column_5(int32_t value)
	{
		___column_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUTSOURCE_T1595232959_H
#ifndef XMLNAMETABLE_T347431366_H
#define XMLNAMETABLE_T347431366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t347431366  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T347431366_H
#ifndef TIMELINECLIPCAPSEXTENSIONS_T1024877177_H
#define TIMELINECLIPCAPSEXTENSIONS_T1024877177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClipCapsExtensions
struct  TimelineClipCapsExtensions_t1024877177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIPCAPSEXTENSIONS_T1024877177_H
#ifndef XMLNAMEENTRY_T1006717423_H
#define XMLNAMEENTRY_T1006717423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntry
struct  XmlNameEntry_t1006717423  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlNameEntry::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNameEntry::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlNameEntry::NS
	String_t* ___NS_2;
	// System.Int32 System.Xml.XmlNameEntry::Hash
	int32_t ___Hash_3;
	// System.String System.Xml.XmlNameEntry::prefixed_name_cache
	String_t* ___prefixed_name_cache_4;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1006717423, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1006717423, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1006717423, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Hash_3() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1006717423, ___Hash_3)); }
	inline int32_t get_Hash_3() const { return ___Hash_3; }
	inline int32_t* get_address_of_Hash_3() { return &___Hash_3; }
	inline void set_Hash_3(int32_t value)
	{
		___Hash_3 = value;
	}

	inline static int32_t get_offset_of_prefixed_name_cache_4() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1006717423, ___prefixed_name_cache_4)); }
	inline String_t* get_prefixed_name_cache_4() const { return ___prefixed_name_cache_4; }
	inline String_t** get_address_of_prefixed_name_cache_4() { return &___prefixed_name_cache_4; }
	inline void set_prefixed_name_cache_4(String_t* value)
	{
		___prefixed_name_cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefixed_name_cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRY_T1006717423_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef PLAYABLEBEHAVIOUR_T3096602576_H
#define PLAYABLEBEHAVIOUR_T3096602576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t3096602576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T3096602576_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef STRINGUTIL_T1470244759_H
#define STRINGUTIL_T1470244759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/StringUtil
struct  StringUtil_t1470244759  : public RuntimeObject
{
public:

public:
};

struct StringUtil_t1470244759_StaticFields
{
public:
	// System.Globalization.CultureInfo System.Xml.XmlTextWriter/StringUtil::cul
	CultureInfo_t2625678720 * ___cul_0;
	// System.Globalization.CompareInfo System.Xml.XmlTextWriter/StringUtil::cmp
	CompareInfo_t2505958045 * ___cmp_1;

public:
	inline static int32_t get_offset_of_cul_0() { return static_cast<int32_t>(offsetof(StringUtil_t1470244759_StaticFields, ___cul_0)); }
	inline CultureInfo_t2625678720 * get_cul_0() const { return ___cul_0; }
	inline CultureInfo_t2625678720 ** get_address_of_cul_0() { return &___cul_0; }
	inline void set_cul_0(CultureInfo_t2625678720 * value)
	{
		___cul_0 = value;
		Il2CppCodeGenWriteBarrier((&___cul_0), value);
	}

	inline static int32_t get_offset_of_cmp_1() { return static_cast<int32_t>(offsetof(StringUtil_t1470244759_StaticFields, ___cmp_1)); }
	inline CompareInfo_t2505958045 * get_cmp_1() const { return ___cmp_1; }
	inline CompareInfo_t2505958045 ** get_address_of_cmp_1() { return &___cmp_1; }
	inline void set_cmp_1(CompareInfo_t2505958045 * value)
	{
		___cmp_1 = value;
		Il2CppCodeGenWriteBarrier((&___cmp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTIL_T1470244759_H
#ifndef EVENTARGS_T2659587769_H
#define EVENTARGS_T2659587769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t2659587769  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t2659587769_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t2659587769 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t2659587769_StaticFields, ___Empty_0)); }
	inline EventArgs_t2659587769 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t2659587769 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t2659587769 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T2659587769_H
#ifndef XMLNAMEENTRYCACHE_T728926030_H
#define XMLNAMEENTRYCACHE_T728926030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntryCache
struct  XmlNameEntryCache_t728926030  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlNameEntryCache::table
	Hashtable_t2354558714 * ___table_0;
	// System.Xml.XmlNameTable System.Xml.XmlNameEntryCache::nameTable
	XmlNameTable_t347431366 * ___nameTable_1;
	// System.Xml.XmlNameEntry System.Xml.XmlNameEntryCache::dummy
	XmlNameEntry_t1006717423 * ___dummy_2;
	// System.Char[] System.Xml.XmlNameEntryCache::cacheBuffer
	CharU5BU5D_t3419619864* ___cacheBuffer_3;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t728926030, ___table_0)); }
	inline Hashtable_t2354558714 * get_table_0() const { return ___table_0; }
	inline Hashtable_t2354558714 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_t2354558714 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t728926030, ___nameTable_1)); }
	inline XmlNameTable_t347431366 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t347431366 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_dummy_2() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t728926030, ___dummy_2)); }
	inline XmlNameEntry_t1006717423 * get_dummy_2() const { return ___dummy_2; }
	inline XmlNameEntry_t1006717423 ** get_address_of_dummy_2() { return &___dummy_2; }
	inline void set_dummy_2(XmlNameEntry_t1006717423 * value)
	{
		___dummy_2 = value;
		Il2CppCodeGenWriteBarrier((&___dummy_2), value);
	}

	inline static int32_t get_offset_of_cacheBuffer_3() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t728926030, ___cacheBuffer_3)); }
	inline CharU5BU5D_t3419619864* get_cacheBuffer_3() const { return ___cacheBuffer_3; }
	inline CharU5BU5D_t3419619864** get_address_of_cacheBuffer_3() { return &___cacheBuffer_3; }
	inline void set_cacheBuffer_3(CharU5BU5D_t3419619864* value)
	{
		___cacheBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___cacheBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRYCACHE_T728926030_H
#ifndef XMLWRITER_T1771783396_H
#define XMLWRITER_T1771783396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t1771783396  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T1771783396_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef STREAM_T4138477179_H
#define STREAM_T4138477179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t4138477179  : public RuntimeObject
{
public:

public:
};

struct Stream_t4138477179_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t4138477179 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t4138477179_StaticFields, ___Null_0)); }
	inline Stream_t4138477179 * get_Null_0() const { return ___Null_0; }
	inline Stream_t4138477179 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t4138477179 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T4138477179_H
#ifndef TEXTREADER_T218744201_H
#define TEXTREADER_T218744201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t218744201  : public RuntimeObject
{
public:

public:
};

struct TextReader_t218744201_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t218744201 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t218744201_StaticFields, ___Null_0)); }
	inline TextReader_t218744201 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t218744201 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t218744201 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T218744201_H
#ifndef XMLRESOLVER_T161349657_H
#define XMLRESOLVER_T161349657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlResolver
struct  XmlResolver_t161349657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESOLVER_T161349657_H
#ifndef XMLIMPLEMENTATION_T3490314784_H
#define XMLIMPLEMENTATION_T3490314784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t3490314784  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_t347431366 * ___InternalNameTable_0;

public:
	inline static int32_t get_offset_of_InternalNameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t3490314784, ___InternalNameTable_0)); }
	inline XmlNameTable_t347431366 * get_InternalNameTable_0() const { return ___InternalNameTable_0; }
	inline XmlNameTable_t347431366 ** get_address_of_InternalNameTable_0() { return &___InternalNameTable_0; }
	inline void set_InternalNameTable_0(XmlNameTable_t347431366 * value)
	{
		___InternalNameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___InternalNameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T3490314784_H
#ifndef U24ARRAYTYPEU241280_T3767486320_H
#define U24ARRAYTYPEU241280_T3767486320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1280
struct  U24ArrayTypeU241280_t3767486320 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241280_t3767486320__padding[1280];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241280_T3767486320_H
#ifndef U24ARRAYTYPEU24256_T1491899399_H
#define U24ARRAYTYPEU24256_T1491899399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t1491899399 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t1491899399__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T1491899399_H
#ifndef XMLURLRESOLVER_T1529314874_H
#define XMLURLRESOLVER_T1529314874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t1529314874  : public XmlResolver_t161349657
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::credential
	RuntimeObject* ___credential_0;

public:
	inline static int32_t get_offset_of_credential_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t1529314874, ___credential_0)); }
	inline RuntimeObject* get_credential_0() const { return ___credential_0; }
	inline RuntimeObject** get_address_of_credential_0() { return &___credential_0; }
	inline void set_credential_0(RuntimeObject* value)
	{
		___credential_0 = value;
		Il2CppCodeGenWriteBarrier((&___credential_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLURLRESOLVER_T1529314874_H
#ifndef U24ARRAYTYPEU2412_T1096561007_H
#define U24ARRAYTYPEU2412_T1096561007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$12
struct  U24ArrayTypeU2412_t1096561007 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t1096561007__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T1096561007_H
#ifndef XMLTEXTREADER_T2210588564_H
#define XMLTEXTREADER_T2210588564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t2210588564  : public XmlReader_t1082502433
{
public:
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t2210588564 * ___entity_2;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_t3875484823 * ___source_3;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_5;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t983112148 * ___entityNameStack_6;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(XmlTextReader_t2210588564, ___entity_2)); }
	inline XmlTextReader_t2210588564 * get_entity_2() const { return ___entity_2; }
	inline XmlTextReader_t2210588564 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(XmlTextReader_t2210588564 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t2210588564, ___source_3)); }
	inline XmlTextReader_t3875484823 * get_source_3() const { return ___source_3; }
	inline XmlTextReader_t3875484823 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlTextReader_t3875484823 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t2210588564, ___entityInsideAttribute_4)); }
	inline bool get_entityInsideAttribute_4() const { return ___entityInsideAttribute_4; }
	inline bool* get_address_of_entityInsideAttribute_4() { return &___entityInsideAttribute_4; }
	inline void set_entityInsideAttribute_4(bool value)
	{
		___entityInsideAttribute_4 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t2210588564, ___insideAttribute_5)); }
	inline bool get_insideAttribute_5() const { return ___insideAttribute_5; }
	inline bool* get_address_of_insideAttribute_5() { return &___insideAttribute_5; }
	inline void set_insideAttribute_5(bool value)
	{
		___insideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_entityNameStack_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t2210588564, ___entityNameStack_6)); }
	inline Stack_1_t983112148 * get_entityNameStack_6() const { return ___entityNameStack_6; }
	inline Stack_1_t983112148 ** get_address_of_entityNameStack_6() { return &___entityNameStack_6; }
	inline void set_entityNameStack_6(Stack_1_t983112148 * value)
	{
		___entityNameStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityNameStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T2210588564_H
#ifndef U24ARRAYTYPEU248_T4092320982_H
#define U24ARRAYTYPEU248_T4092320982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$8
struct  U24ArrayTypeU248_t4092320982 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU248_t4092320982__padding[8];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU248_T4092320982_H
#ifndef SUPPORTSCHILDTRACKSATTRIBUTE_T3475482393_H
#define SUPPORTSCHILDTRACKSATTRIBUTE_T3475482393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.SupportsChildTracksAttribute
struct  SupportsChildTracksAttribute_t3475482393  : public Attribute_t1924466020
{
public:
	// System.Type UnityEngine.Timeline.SupportsChildTracksAttribute::childType
	Type_t * ___childType_0;
	// System.Int32 UnityEngine.Timeline.SupportsChildTracksAttribute::levels
	int32_t ___levels_1;

public:
	inline static int32_t get_offset_of_childType_0() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_t3475482393, ___childType_0)); }
	inline Type_t * get_childType_0() const { return ___childType_0; }
	inline Type_t ** get_address_of_childType_0() { return &___childType_0; }
	inline void set_childType_0(Type_t * value)
	{
		___childType_0 = value;
		Il2CppCodeGenWriteBarrier((&___childType_0), value);
	}

	inline static int32_t get_offset_of_levels_1() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_t3475482393, ___levels_1)); }
	inline int32_t get_levels_1() const { return ___levels_1; }
	inline int32_t* get_address_of_levels_1() { return &___levels_1; }
	inline void set_levels_1(int32_t value)
	{
		___levels_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTSCHILDTRACKSATTRIBUTE_T3475482393_H
#ifndef TRACKCLIPTYPEATTRIBUTE_T1551440451_H
#define TRACKCLIPTYPEATTRIBUTE_T1551440451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackClipTypeAttribute
struct  TrackClipTypeAttribute_t1551440451  : public Attribute_t1924466020
{
public:
	// System.Type UnityEngine.Timeline.TrackClipTypeAttribute::inspectedType
	Type_t * ___inspectedType_0;

public:
	inline static int32_t get_offset_of_inspectedType_0() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t1551440451, ___inspectedType_0)); }
	inline Type_t * get_inspectedType_0() const { return ___inspectedType_0; }
	inline Type_t ** get_address_of_inspectedType_0() { return &___inspectedType_0; }
	inline void set_inspectedType_0(Type_t * value)
	{
		___inspectedType_0 = value;
		Il2CppCodeGenWriteBarrier((&___inspectedType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCLIPTYPEATTRIBUTE_T1551440451_H
#ifndef NOTKEYABLEATTRIBUTE_T2161895226_H
#define NOTKEYABLEATTRIBUTE_T2161895226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.NotKeyableAttribute
struct  NotKeyableAttribute_t2161895226  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTKEYABLEATTRIBUTE_T2161895226_H
#ifndef TRACKBINDINGTYPEATTRIBUTE_T2510260633_H
#define TRACKBINDINGTYPEATTRIBUTE_T2510260633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackBindingTypeAttribute
struct  TrackBindingTypeAttribute_t2510260633  : public Attribute_t1924466020
{
public:
	// System.Type UnityEngine.Timeline.TrackBindingTypeAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t2510260633, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBINDINGTYPEATTRIBUTE_T2510260633_H
#ifndef IGNOREONPLAYABLETRACKATTRIBUTE_T1048616002_H
#define IGNOREONPLAYABLETRACKATTRIBUTE_T1048616002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute
struct  IgnoreOnPlayableTrackAttribute_t1048616002  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREONPLAYABLETRACKATTRIBUTE_T1048616002_H
#ifndef TIMELINEPLAYABLE_T1837138455_H
#define TIMELINEPLAYABLE_T1837138455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable
struct  TimelinePlayable_t1837138455  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_IntervalTree
	IntervalTree_1_t2495587357 * ___m_IntervalTree_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_ActiveClips
	List_1_t4284214879 * ___m_ActiveClips_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_CurrentListOfActiveClips
	List_1_t4284214879 * ___m_CurrentListOfActiveClips_2;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable::m_ActiveBit
	int32_t ___m_ActiveBit_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback> UnityEngine.Timeline.TimelinePlayable::m_EvaluateCallbacks
	List_1_t3724562642 * ___m_EvaluateCallbacks_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Timeline.TimelinePlayable/ConnectionCache> UnityEngine.Timeline.TimelinePlayable::m_PlayableCache
	Dictionary_2_t577450642 * ___m_PlayableCache_5;

public:
	inline static int32_t get_offset_of_m_IntervalTree_0() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_IntervalTree_0)); }
	inline IntervalTree_1_t2495587357 * get_m_IntervalTree_0() const { return ___m_IntervalTree_0; }
	inline IntervalTree_1_t2495587357 ** get_address_of_m_IntervalTree_0() { return &___m_IntervalTree_0; }
	inline void set_m_IntervalTree_0(IntervalTree_1_t2495587357 * value)
	{
		___m_IntervalTree_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntervalTree_0), value);
	}

	inline static int32_t get_offset_of_m_ActiveClips_1() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_ActiveClips_1)); }
	inline List_1_t4284214879 * get_m_ActiveClips_1() const { return ___m_ActiveClips_1; }
	inline List_1_t4284214879 ** get_address_of_m_ActiveClips_1() { return &___m_ActiveClips_1; }
	inline void set_m_ActiveClips_1(List_1_t4284214879 * value)
	{
		___m_ActiveClips_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveClips_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentListOfActiveClips_2() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_CurrentListOfActiveClips_2)); }
	inline List_1_t4284214879 * get_m_CurrentListOfActiveClips_2() const { return ___m_CurrentListOfActiveClips_2; }
	inline List_1_t4284214879 ** get_address_of_m_CurrentListOfActiveClips_2() { return &___m_CurrentListOfActiveClips_2; }
	inline void set_m_CurrentListOfActiveClips_2(List_1_t4284214879 * value)
	{
		___m_CurrentListOfActiveClips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentListOfActiveClips_2), value);
	}

	inline static int32_t get_offset_of_m_ActiveBit_3() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_ActiveBit_3)); }
	inline int32_t get_m_ActiveBit_3() const { return ___m_ActiveBit_3; }
	inline int32_t* get_address_of_m_ActiveBit_3() { return &___m_ActiveBit_3; }
	inline void set_m_ActiveBit_3(int32_t value)
	{
		___m_ActiveBit_3 = value;
	}

	inline static int32_t get_offset_of_m_EvaluateCallbacks_4() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_EvaluateCallbacks_4)); }
	inline List_1_t3724562642 * get_m_EvaluateCallbacks_4() const { return ___m_EvaluateCallbacks_4; }
	inline List_1_t3724562642 ** get_address_of_m_EvaluateCallbacks_4() { return &___m_EvaluateCallbacks_4; }
	inline void set_m_EvaluateCallbacks_4(List_1_t3724562642 * value)
	{
		___m_EvaluateCallbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvaluateCallbacks_4), value);
	}

	inline static int32_t get_offset_of_m_PlayableCache_5() { return static_cast<int32_t>(offsetof(TimelinePlayable_t1837138455, ___m_PlayableCache_5)); }
	inline Dictionary_2_t577450642 * get_m_PlayableCache_5() const { return ___m_PlayableCache_5; }
	inline Dictionary_2_t577450642 ** get_address_of_m_PlayableCache_5() { return &___m_PlayableCache_5; }
	inline void set_m_PlayableCache_5(Dictionary_2_t577450642 * value)
	{
		___m_PlayableCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayableCache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEPLAYABLE_T1837138455_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef PROPERTYATTRIBUTE_T381499487_H
#define PROPERTYATTRIBUTE_T381499487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t381499487  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T381499487_H
#ifndef DISCRETETIME_T1558908690_H
#define DISCRETETIME_T1558908690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t1558908690 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t1558908690_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t1558908690  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t1558908690  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t1558908690 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t1558908690  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T1558908690_H
#ifndef TAGNAME_T2918394058_H
#define TAGNAME_T2918394058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/TagName
struct  TagName_t2918394058 
{
public:
	// System.String Mono.Xml2.XmlTextReader/TagName::Name
	String_t* ___Name_0;
	// System.String Mono.Xml2.XmlTextReader/TagName::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml2.XmlTextReader/TagName::Prefix
	String_t* ___Prefix_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TagName_t2918394058, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(TagName_t2918394058, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_Prefix_2() { return static_cast<int32_t>(offsetof(TagName_t2918394058, ___Prefix_2)); }
	inline String_t* get_Prefix_2() const { return ___Prefix_2; }
	inline String_t** get_address_of_Prefix_2() { return &___Prefix_2; }
	inline void set_Prefix_2(String_t* value)
	{
		___Prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_t2918394058_marshaled_pinvoke
{
	char* ___Name_0;
	char* ___LocalName_1;
	char* ___Prefix_2;
};
// Native definition for COM marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_t2918394058_marshaled_com
{
	Il2CppChar* ___Name_0;
	Il2CppChar* ___LocalName_1;
	Il2CppChar* ___Prefix_2;
};
#endif // TAGNAME_T2918394058_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NSSCOPE_T1100031146_H
#define NSSCOPE_T1100031146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsScope
struct  NsScope_t1100031146 
{
public:
	// System.Int32 System.Xml.XmlNamespaceManager/NsScope::DeclCount
	int32_t ___DeclCount_0;
	// System.String System.Xml.XmlNamespaceManager/NsScope::DefaultNamespace
	String_t* ___DefaultNamespace_1;

public:
	inline static int32_t get_offset_of_DeclCount_0() { return static_cast<int32_t>(offsetof(NsScope_t1100031146, ___DeclCount_0)); }
	inline int32_t get_DeclCount_0() const { return ___DeclCount_0; }
	inline int32_t* get_address_of_DeclCount_0() { return &___DeclCount_0; }
	inline void set_DeclCount_0(int32_t value)
	{
		___DeclCount_0 = value;
	}

	inline static int32_t get_offset_of_DefaultNamespace_1() { return static_cast<int32_t>(offsetof(NsScope_t1100031146, ___DefaultNamespace_1)); }
	inline String_t* get_DefaultNamespace_1() const { return ___DefaultNamespace_1; }
	inline String_t** get_address_of_DefaultNamespace_1() { return &___DefaultNamespace_1; }
	inline void set_DefaultNamespace_1(String_t* value)
	{
		___DefaultNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultNamespace_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t1100031146_marshaled_pinvoke
{
	int32_t ___DeclCount_0;
	char* ___DefaultNamespace_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t1100031146_marshaled_com
{
	int32_t ___DeclCount_0;
	Il2CppChar* ___DefaultNamespace_1;
};
#endif // NSSCOPE_T1100031146_H
#ifndef XMLNODELISTCHILDREN_T4201287521_H
#define XMLNODELISTCHILDREN_T4201287521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren
struct  XmlNodeListChildren_t4201287521  : public XmlNodeList_t3849781475
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren::parent
	RuntimeObject* ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNodeListChildren_t4201287521, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELISTCHILDREN_T4201287521_H
#ifndef EMPTYNODELIST_T596510042_H
#define EMPTYNODELIST_T596510042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode/EmptyNodeList
struct  EmptyNodeList_t596510042  : public XmlNodeList_t3849781475
{
public:

public:
};

struct EmptyNodeList_t596510042_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNode/EmptyNodeList::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(EmptyNodeList_t596510042_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYNODELIST_T596510042_H
#ifndef XMLNOTATION_T3101001639_H
#define XMLNOTATION_T3101001639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t3101001639  : public XmlNode_t3729859639
{
public:
	// System.String System.Xml.XmlNotation::localName
	String_t* ___localName_5;
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_6;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_7;
	// System.String System.Xml.XmlNotation::prefix
	String_t* ___prefix_8;

public:
	inline static int32_t get_offset_of_localName_5() { return static_cast<int32_t>(offsetof(XmlNotation_t3101001639, ___localName_5)); }
	inline String_t* get_localName_5() const { return ___localName_5; }
	inline String_t** get_address_of_localName_5() { return &___localName_5; }
	inline void set_localName_5(String_t* value)
	{
		___localName_5 = value;
		Il2CppCodeGenWriteBarrier((&___localName_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(XmlNotation_t3101001639, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(XmlNotation_t3101001639, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(XmlNotation_t3101001639, ___prefix_8)); }
	inline String_t* get_prefix_8() const { return ___prefix_8; }
	inline String_t** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(String_t* value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNOTATION_T3101001639_H
#ifndef NSDECL_T1301437426_H
#define NSDECL_T1301437426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsDecl
struct  NsDecl_t1301437426 
{
public:
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Uri
	String_t* ___Uri_1;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(NsDecl_t1301437426, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_Uri_1() { return static_cast<int32_t>(offsetof(NsDecl_t1301437426, ___Uri_1)); }
	inline String_t* get_Uri_1() const { return ___Uri_1; }
	inline String_t** get_address_of_Uri_1() { return &___Uri_1; }
	inline void set_Uri_1(String_t* value)
	{
		___Uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t1301437426_marshaled_pinvoke
{
	char* ___Prefix_0;
	char* ___Uri_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t1301437426_marshaled_com
{
	Il2CppChar* ___Prefix_0;
	Il2CppChar* ___Uri_1;
};
#endif // NSDECL_T1301437426_H
#ifndef XMLLINKEDNODE_T1947670907_H
#define XMLLINKEDNODE_T1947670907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1947670907  : public XmlNode_t3729859639
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1947670907 * ___nextSibling_5;

public:
	inline static int32_t get_offset_of_nextSibling_5() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1947670907, ___nextSibling_5)); }
	inline XmlLinkedNode_t1947670907 * get_nextSibling_5() const { return ___nextSibling_5; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_nextSibling_5() { return &___nextSibling_5; }
	inline void set_nextSibling_5(XmlLinkedNode_t1947670907 * value)
	{
		___nextSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1947670907_H
#ifndef XMLINPUTSTREAM_T3210582043_H
#define XMLINPUTSTREAM_T3210582043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlInputStream
struct  XmlInputStream_t3210582043  : public Stream_t4138477179
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t4004889433 * ___enc_2;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_t4138477179 * ___stream_3;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t434619169* ___buffer_4;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_5;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_6;

public:
	inline static int32_t get_offset_of_enc_2() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043, ___enc_2)); }
	inline Encoding_t4004889433 * get_enc_2() const { return ___enc_2; }
	inline Encoding_t4004889433 ** get_address_of_enc_2() { return &___enc_2; }
	inline void set_enc_2(Encoding_t4004889433 * value)
	{
		___enc_2 = value;
		Il2CppCodeGenWriteBarrier((&___enc_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043, ___stream_3)); }
	inline Stream_t4138477179 * get_stream_3() const { return ___stream_3; }
	inline Stream_t4138477179 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t4138477179 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043, ___buffer_4)); }
	inline ByteU5BU5D_t434619169* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_t434619169** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_t434619169* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_bufLength_5() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043, ___bufLength_5)); }
	inline int32_t get_bufLength_5() const { return ___bufLength_5; }
	inline int32_t* get_address_of_bufLength_5() { return &___bufLength_5; }
	inline void set_bufLength_5(int32_t value)
	{
		___bufLength_5 = value;
	}

	inline static int32_t get_offset_of_bufPos_6() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043, ___bufPos_6)); }
	inline int32_t get_bufPos_6() const { return ___bufPos_6; }
	inline int32_t* get_address_of_bufPos_6() { return &___bufPos_6; }
	inline void set_bufPos_6(int32_t value)
	{
		___bufPos_6 = value;
	}
};

struct XmlInputStream_t3210582043_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t4004889433 * ___StrictUTF8_1;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_t3531299539 * ___encodingException_7;

public:
	inline static int32_t get_offset_of_StrictUTF8_1() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043_StaticFields, ___StrictUTF8_1)); }
	inline Encoding_t4004889433 * get_StrictUTF8_1() const { return ___StrictUTF8_1; }
	inline Encoding_t4004889433 ** get_address_of_StrictUTF8_1() { return &___StrictUTF8_1; }
	inline void set_StrictUTF8_1(Encoding_t4004889433 * value)
	{
		___StrictUTF8_1 = value;
		Il2CppCodeGenWriteBarrier((&___StrictUTF8_1), value);
	}

	inline static int32_t get_offset_of_encodingException_7() { return static_cast<int32_t>(offsetof(XmlInputStream_t3210582043_StaticFields, ___encodingException_7)); }
	inline XmlException_t3531299539 * get_encodingException_7() const { return ___encodingException_7; }
	inline XmlException_t3531299539 ** get_address_of_encodingException_7() { return &___encodingException_7; }
	inline void set_encodingException_7(XmlException_t3531299539 * value)
	{
		___encodingException_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodingException_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINPUTSTREAM_T3210582043_H
#ifndef NONBLOCKINGSTREAMREADER_T466767770_H
#define NONBLOCKINGSTREAMREADER_T466767770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NonBlockingStreamReader
struct  NonBlockingStreamReader_t466767770  : public TextReader_t218744201
{
public:
	// System.Byte[] System.Xml.NonBlockingStreamReader::input_buffer
	ByteU5BU5D_t434619169* ___input_buffer_1;
	// System.Char[] System.Xml.NonBlockingStreamReader::decoded_buffer
	CharU5BU5D_t3419619864* ___decoded_buffer_2;
	// System.Int32 System.Xml.NonBlockingStreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.Xml.NonBlockingStreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.Xml.NonBlockingStreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Text.Encoding System.Xml.NonBlockingStreamReader::encoding
	Encoding_t4004889433 * ___encoding_6;
	// System.Text.Decoder System.Xml.NonBlockingStreamReader::decoder
	Decoder_t4109873929 * ___decoder_7;
	// System.IO.Stream System.Xml.NonBlockingStreamReader::base_stream
	Stream_t4138477179 * ___base_stream_8;
	// System.Boolean System.Xml.NonBlockingStreamReader::mayBlock
	bool ___mayBlock_9;
	// System.Text.StringBuilder System.Xml.NonBlockingStreamReader::line_builder
	StringBuilder_t622404039 * ___line_builder_10;
	// System.Boolean System.Xml.NonBlockingStreamReader::foundCR
	bool ___foundCR_11;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___input_buffer_1)); }
	inline ByteU5BU5D_t434619169* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t434619169** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t434619169* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___decoded_buffer_2)); }
	inline CharU5BU5D_t3419619864* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t3419619864** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t3419619864* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___encoding_6)); }
	inline Encoding_t4004889433 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t4004889433 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t4004889433 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_decoder_7() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___decoder_7)); }
	inline Decoder_t4109873929 * get_decoder_7() const { return ___decoder_7; }
	inline Decoder_t4109873929 ** get_address_of_decoder_7() { return &___decoder_7; }
	inline void set_decoder_7(Decoder_t4109873929 * value)
	{
		___decoder_7 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_7), value);
	}

	inline static int32_t get_offset_of_base_stream_8() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___base_stream_8)); }
	inline Stream_t4138477179 * get_base_stream_8() const { return ___base_stream_8; }
	inline Stream_t4138477179 ** get_address_of_base_stream_8() { return &___base_stream_8; }
	inline void set_base_stream_8(Stream_t4138477179 * value)
	{
		___base_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_8), value);
	}

	inline static int32_t get_offset_of_mayBlock_9() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___mayBlock_9)); }
	inline bool get_mayBlock_9() const { return ___mayBlock_9; }
	inline bool* get_address_of_mayBlock_9() { return &___mayBlock_9; }
	inline void set_mayBlock_9(bool value)
	{
		___mayBlock_9 = value;
	}

	inline static int32_t get_offset_of_line_builder_10() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___line_builder_10)); }
	inline StringBuilder_t622404039 * get_line_builder_10() const { return ___line_builder_10; }
	inline StringBuilder_t622404039 ** get_address_of_line_builder_10() { return &___line_builder_10; }
	inline void set_line_builder_10(StringBuilder_t622404039 * value)
	{
		___line_builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_10), value);
	}

	inline static int32_t get_offset_of_foundCR_11() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t466767770, ___foundCR_11)); }
	inline bool get_foundCR_11() const { return ___foundCR_11; }
	inline bool* get_address_of_foundCR_11() { return &___foundCR_11; }
	inline void set_foundCR_11(bool value)
	{
		___foundCR_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBLOCKINGSTREAMREADER_T466767770_H
#ifndef XMLENTITY_T2212524324_H
#define XMLENTITY_T2212524324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t2212524324  : public XmlNode_t3729859639
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_5;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_6;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_7;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_8;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_9;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_10;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_NDATA_6() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___NDATA_6)); }
	inline String_t* get_NDATA_6() const { return ___NDATA_6; }
	inline String_t** get_address_of_NDATA_6() { return &___NDATA_6; }
	inline void set_NDATA_6(String_t* value)
	{
		___NDATA_6 = value;
		Il2CppCodeGenWriteBarrier((&___NDATA_6), value);
	}

	inline static int32_t get_offset_of_publicId_7() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___publicId_7)); }
	inline String_t* get_publicId_7() const { return ___publicId_7; }
	inline String_t** get_address_of_publicId_7() { return &___publicId_7; }
	inline void set_publicId_7(String_t* value)
	{
		___publicId_7 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_7), value);
	}

	inline static int32_t get_offset_of_systemId_8() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___systemId_8)); }
	inline String_t* get_systemId_8() const { return ___systemId_8; }
	inline String_t** get_address_of_systemId_8() { return &___systemId_8; }
	inline void set_systemId_8(String_t* value)
	{
		___systemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_8), value);
	}

	inline static int32_t get_offset_of_baseUri_9() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___baseUri_9)); }
	inline String_t* get_baseUri_9() const { return ___baseUri_9; }
	inline String_t** get_address_of_baseUri_9() { return &___baseUri_9; }
	inline void set_baseUri_9(String_t* value)
	{
		___baseUri_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_9), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_10() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___lastLinkedChild_10)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_10() const { return ___lastLinkedChild_10; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_10() { return &___lastLinkedChild_10; }
	inline void set_lastLinkedChild_10(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_10), value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_11() { return static_cast<int32_t>(offsetof(XmlEntity_t2212524324, ___contentAlreadySet_11)); }
	inline bool get_contentAlreadySet_11() const { return ___contentAlreadySet_11; }
	inline bool* get_address_of_contentAlreadySet_11() { return &___contentAlreadySet_11; }
	inline void set_contentAlreadySet_11(bool value)
	{
		___contentAlreadySet_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T2212524324_H
#ifndef XMLNODEARRAYLIST_T4205225702_H
#define XMLNODEARRAYLIST_T4205225702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeArrayList
struct  XmlNodeArrayList_t4205225702  : public XmlNodeList_t3849781475
{
public:
	// System.Collections.ArrayList System.Xml.XmlNodeArrayList::_rgNodes
	ArrayList_t4250946984 * ____rgNodes_0;

public:
	inline static int32_t get_offset_of__rgNodes_0() { return static_cast<int32_t>(offsetof(XmlNodeArrayList_t4205225702, ____rgNodes_0)); }
	inline ArrayList_t4250946984 * get__rgNodes_0() const { return ____rgNodes_0; }
	inline ArrayList_t4250946984 ** get_address_of__rgNodes_0() { return &____rgNodes_0; }
	inline void set__rgNodes_0(ArrayList_t4250946984 * value)
	{
		____rgNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&____rgNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEARRAYLIST_T4205225702_H
#ifndef XMLDOCUMENT_T2823332853_H
#define XMLDOCUMENT_T2823332853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t2823332853  : public XmlNode_t3729859639
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_6;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_7;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_t347431366 * ___nameTable_8;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_9;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t3490314784 * ___implementation_10;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_11;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t161349657 * ___resolver_12;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_t2354558714 * ___idTable_13;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t728926030 * ___nameCache_14;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_15;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_t93272820 * ___schemas_16;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	RuntimeObject* ___schemaInfo_17;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_18;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_t3906219045 * ___NodeChanged_19;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_t3906219045 * ___NodeChanging_20;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_t3906219045 * ___NodeInserted_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_t3906219045 * ___NodeInserting_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_t3906219045 * ___NodeRemoved_23;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_t3906219045 * ___NodeRemoving_24;

public:
	inline static int32_t get_offset_of_optimal_create_element_6() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___optimal_create_element_6)); }
	inline bool get_optimal_create_element_6() const { return ___optimal_create_element_6; }
	inline bool* get_address_of_optimal_create_element_6() { return &___optimal_create_element_6; }
	inline void set_optimal_create_element_6(bool value)
	{
		___optimal_create_element_6 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_7() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___optimal_create_attribute_7)); }
	inline bool get_optimal_create_attribute_7() const { return ___optimal_create_attribute_7; }
	inline bool* get_address_of_optimal_create_attribute_7() { return &___optimal_create_attribute_7; }
	inline void set_optimal_create_attribute_7(bool value)
	{
		___optimal_create_attribute_7 = value;
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___nameTable_8)); }
	inline XmlNameTable_t347431366 * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_t347431366 * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_baseURI_9() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___baseURI_9)); }
	inline String_t* get_baseURI_9() const { return ___baseURI_9; }
	inline String_t** get_address_of_baseURI_9() { return &___baseURI_9; }
	inline void set_baseURI_9(String_t* value)
	{
		___baseURI_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_9), value);
	}

	inline static int32_t get_offset_of_implementation_10() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___implementation_10)); }
	inline XmlImplementation_t3490314784 * get_implementation_10() const { return ___implementation_10; }
	inline XmlImplementation_t3490314784 ** get_address_of_implementation_10() { return &___implementation_10; }
	inline void set_implementation_10(XmlImplementation_t3490314784 * value)
	{
		___implementation_10 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_10), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_11() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___preserveWhitespace_11)); }
	inline bool get_preserveWhitespace_11() const { return ___preserveWhitespace_11; }
	inline bool* get_address_of_preserveWhitespace_11() { return &___preserveWhitespace_11; }
	inline void set_preserveWhitespace_11(bool value)
	{
		___preserveWhitespace_11 = value;
	}

	inline static int32_t get_offset_of_resolver_12() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___resolver_12)); }
	inline XmlResolver_t161349657 * get_resolver_12() const { return ___resolver_12; }
	inline XmlResolver_t161349657 ** get_address_of_resolver_12() { return &___resolver_12; }
	inline void set_resolver_12(XmlResolver_t161349657 * value)
	{
		___resolver_12 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_12), value);
	}

	inline static int32_t get_offset_of_idTable_13() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___idTable_13)); }
	inline Hashtable_t2354558714 * get_idTable_13() const { return ___idTable_13; }
	inline Hashtable_t2354558714 ** get_address_of_idTable_13() { return &___idTable_13; }
	inline void set_idTable_13(Hashtable_t2354558714 * value)
	{
		___idTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___idTable_13), value);
	}

	inline static int32_t get_offset_of_nameCache_14() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___nameCache_14)); }
	inline XmlNameEntryCache_t728926030 * get_nameCache_14() const { return ___nameCache_14; }
	inline XmlNameEntryCache_t728926030 ** get_address_of_nameCache_14() { return &___nameCache_14; }
	inline void set_nameCache_14(XmlNameEntryCache_t728926030 * value)
	{
		___nameCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_14), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_15() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___lastLinkedChild_15)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_15() const { return ___lastLinkedChild_15; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_15() { return &___lastLinkedChild_15; }
	inline void set_lastLinkedChild_15(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_15 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_15), value);
	}

	inline static int32_t get_offset_of_schemas_16() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___schemas_16)); }
	inline XmlSchemaSet_t93272820 * get_schemas_16() const { return ___schemas_16; }
	inline XmlSchemaSet_t93272820 ** get_address_of_schemas_16() { return &___schemas_16; }
	inline void set_schemas_16(XmlSchemaSet_t93272820 * value)
	{
		___schemas_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_16), value);
	}

	inline static int32_t get_offset_of_schemaInfo_17() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___schemaInfo_17)); }
	inline RuntimeObject* get_schemaInfo_17() const { return ___schemaInfo_17; }
	inline RuntimeObject** get_address_of_schemaInfo_17() { return &___schemaInfo_17; }
	inline void set_schemaInfo_17(RuntimeObject* value)
	{
		___schemaInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_17), value);
	}

	inline static int32_t get_offset_of_loadMode_18() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___loadMode_18)); }
	inline bool get_loadMode_18() const { return ___loadMode_18; }
	inline bool* get_address_of_loadMode_18() { return &___loadMode_18; }
	inline void set_loadMode_18(bool value)
	{
		___loadMode_18 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_19() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeChanged_19)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeChanged_19() const { return ___NodeChanged_19; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeChanged_19() { return &___NodeChanged_19; }
	inline void set_NodeChanged_19(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanged_19), value);
	}

	inline static int32_t get_offset_of_NodeChanging_20() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeChanging_20)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeChanging_20() const { return ___NodeChanging_20; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeChanging_20() { return &___NodeChanging_20; }
	inline void set_NodeChanging_20(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeChanging_20 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanging_20), value);
	}

	inline static int32_t get_offset_of_NodeInserted_21() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeInserted_21)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeInserted_21() const { return ___NodeInserted_21; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeInserted_21() { return &___NodeInserted_21; }
	inline void set_NodeInserted_21(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeInserted_21 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserted_21), value);
	}

	inline static int32_t get_offset_of_NodeInserting_22() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeInserting_22)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeInserting_22() const { return ___NodeInserting_22; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeInserting_22() { return &___NodeInserting_22; }
	inline void set_NodeInserting_22(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeInserting_22 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserting_22), value);
	}

	inline static int32_t get_offset_of_NodeRemoved_23() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeRemoved_23)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeRemoved_23() const { return ___NodeRemoved_23; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeRemoved_23() { return &___NodeRemoved_23; }
	inline void set_NodeRemoved_23(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeRemoved_23 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoved_23), value);
	}

	inline static int32_t get_offset_of_NodeRemoving_24() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853, ___NodeRemoving_24)); }
	inline XmlNodeChangedEventHandler_t3906219045 * get_NodeRemoving_24() const { return ___NodeRemoving_24; }
	inline XmlNodeChangedEventHandler_t3906219045 ** get_address_of_NodeRemoving_24() { return &___NodeRemoving_24; }
	inline void set_NodeRemoving_24(XmlNodeChangedEventHandler_t3906219045 * value)
	{
		___NodeRemoving_24 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoving_24), value);
	}
};

struct XmlDocument_t2823332853_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_t1985992169* ___optimal_create_types_5;

public:
	inline static int32_t get_offset_of_optimal_create_types_5() { return static_cast<int32_t>(offsetof(XmlDocument_t2823332853_StaticFields, ___optimal_create_types_5)); }
	inline TypeU5BU5D_t1985992169* get_optimal_create_types_5() const { return ___optimal_create_types_5; }
	inline TypeU5BU5D_t1985992169** get_address_of_optimal_create_types_5() { return &___optimal_create_types_5; }
	inline void set_optimal_create_types_5(TypeU5BU5D_t1985992169* value)
	{
		___optimal_create_types_5 = value;
		Il2CppCodeGenWriteBarrier((&___optimal_create_types_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T2823332853_H
#ifndef XMLDOCUMENTFRAGMENT_T460802731_H
#define XMLDOCUMENTFRAGMENT_T460802731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t460802731  : public XmlNode_t3729859639
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_5;

public:
	inline static int32_t get_offset_of_lastLinkedChild_5() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t460802731, ___lastLinkedChild_5)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_5() const { return ___lastLinkedChild_5; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_5() { return &___lastLinkedChild_5; }
	inline void set_lastLinkedChild_5(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T460802731_H
#ifndef WRITESTATE_T899266867_H
#define WRITESTATE_T899266867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t899266867 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t899266867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T899266867_H
#ifndef CLIPEXTRAPOLATION_T513923237_H
#define CLIPEXTRAPOLATION_T513923237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/ClipExtrapolation
struct  ClipExtrapolation_t513923237 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/ClipExtrapolation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipExtrapolation_t513923237, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPEXTRAPOLATION_T513923237_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef BLENDCURVEMODE_T286953288_H
#define BLENDCURVEMODE_T286953288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/BlendCurveMode
struct  BlendCurveMode_t286953288 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/BlendCurveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendCurveMode_t286953288, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDCURVEMODE_T286953288_H
#ifndef DATASTREAMTYPE_T325920430_H
#define DATASTREAMTYPE_T325920430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t325920430 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t325920430, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T325920430_H
#ifndef POSTPLAYBACKSTATE_T3554111860_H
#define POSTPLAYBACKSTATE_T3554111860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack/PostPlaybackState
struct  PostPlaybackState_t3554111860 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationTrack/PostPlaybackState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PostPlaybackState_t3554111860, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T3554111860_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef XMLDECLARATION_T1715995802_H
#define XMLDECLARATION_T1715995802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t1715995802  : public XmlLinkedNode_t1947670907
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_6;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_7;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_8;

public:
	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1715995802, ___encoding_6)); }
	inline String_t* get_encoding_6() const { return ___encoding_6; }
	inline String_t** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(String_t* value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_standalone_7() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1715995802, ___standalone_7)); }
	inline String_t* get_standalone_7() const { return ___standalone_7; }
	inline String_t** get_address_of_standalone_7() { return &___standalone_7; }
	inline void set_standalone_7(String_t* value)
	{
		___standalone_7 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1715995802, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}
};

struct XmlDeclaration_t1715995802_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map4A_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1715995802_StaticFields, ___U3CU3Ef__switchU24map4A_9)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map4A_9() const { return ___U3CU3Ef__switchU24map4A_9; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map4A_9() { return &___U3CU3Ef__switchU24map4A_9; }
	inline void set_U3CU3Ef__switchU24map4A_9(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map4A_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4A_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T1715995802_H
#ifndef DATETIMESTYLES_T2048082641_H
#define DATETIMESTYLES_T2048082641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t2048082641 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t2048082641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T2048082641_H
#ifndef XMLDOCUMENTTYPE_T1189407184_H
#define XMLDOCUMENTTYPE_T1189407184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t1189407184  : public XmlLinkedNode_t1947670907
{
public:
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t4038918308 * ___entities_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t4038918308 * ___notations_7;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t3150902482 * ___dtd_8;

public:
	inline static int32_t get_offset_of_entities_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t1189407184, ___entities_6)); }
	inline XmlNamedNodeMap_t4038918308 * get_entities_6() const { return ___entities_6; }
	inline XmlNamedNodeMap_t4038918308 ** get_address_of_entities_6() { return &___entities_6; }
	inline void set_entities_6(XmlNamedNodeMap_t4038918308 * value)
	{
		___entities_6 = value;
		Il2CppCodeGenWriteBarrier((&___entities_6), value);
	}

	inline static int32_t get_offset_of_notations_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t1189407184, ___notations_7)); }
	inline XmlNamedNodeMap_t4038918308 * get_notations_7() const { return ___notations_7; }
	inline XmlNamedNodeMap_t4038918308 ** get_address_of_notations_7() { return &___notations_7; }
	inline void set_notations_7(XmlNamedNodeMap_t4038918308 * value)
	{
		___notations_7 = value;
		Il2CppCodeGenWriteBarrier((&___notations_7), value);
	}

	inline static int32_t get_offset_of_dtd_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t1189407184, ___dtd_8)); }
	inline DTDObjectModel_t3150902482 * get_dtd_8() const { return ___dtd_8; }
	inline DTDObjectModel_t3150902482 ** get_address_of_dtd_8() { return &___dtd_8; }
	inline void set_dtd_8(DTDObjectModel_t3150902482 * value)
	{
		___dtd_8 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T1189407184_H
#ifndef XMLSTREAMREADER_T1193375781_H
#define XMLSTREAMREADER_T1193375781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t1193375781  : public NonBlockingStreamReader_t466767770
{
public:
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_t3210582043 * ___input_12;

public:
	inline static int32_t get_offset_of_input_12() { return static_cast<int32_t>(offsetof(XmlStreamReader_t1193375781, ___input_12)); }
	inline XmlInputStream_t3210582043 * get_input_12() const { return ___input_12; }
	inline XmlInputStream_t3210582043 ** get_address_of_input_12() { return &___input_12; }
	inline void set_input_12(XmlInputStream_t3210582043 * value)
	{
		___input_12 = value;
		Il2CppCodeGenWriteBarrier((&___input_12), value);
	}
};

struct XmlStreamReader_t1193375781_StaticFields
{
public:
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_t3531299539 * ___invalidDataException_13;

public:
	inline static int32_t get_offset_of_invalidDataException_13() { return static_cast<int32_t>(offsetof(XmlStreamReader_t1193375781_StaticFields, ___invalidDataException_13)); }
	inline XmlException_t3531299539 * get_invalidDataException_13() const { return ___invalidDataException_13; }
	inline XmlException_t3531299539 ** get_address_of_invalidDataException_13() { return &___invalidDataException_13; }
	inline void set_invalidDataException_13(XmlException_t3531299539 * value)
	{
		___invalidDataException_13 = value;
		Il2CppCodeGenWriteBarrier((&___invalidDataException_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTREAMREADER_T1193375781_H
#ifndef XMLEXCEPTION_T3531299539_H
#define XMLEXCEPTION_T3531299539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t3531299539  : public SystemException_t2062748594
{
public:
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.XmlException::res
	String_t* ___res_14;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t2511808107* ___messages_15;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XmlException_t3531299539, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XmlException_t3531299539, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XmlException_t3531299539, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_13), value);
	}

	inline static int32_t get_offset_of_res_14() { return static_cast<int32_t>(offsetof(XmlException_t3531299539, ___res_14)); }
	inline String_t* get_res_14() const { return ___res_14; }
	inline String_t** get_address_of_res_14() { return &___res_14; }
	inline void set_res_14(String_t* value)
	{
		___res_14 = value;
		Il2CppCodeGenWriteBarrier((&___res_14), value);
	}

	inline static int32_t get_offset_of_messages_15() { return static_cast<int32_t>(offsetof(XmlException_t3531299539, ___messages_15)); }
	inline StringU5BU5D_t2511808107* get_messages_15() const { return ___messages_15; }
	inline StringU5BU5D_t2511808107** get_address_of_messages_15() { return &___messages_15; }
	inline void set_messages_15(StringU5BU5D_t2511808107* value)
	{
		___messages_15 = value;
		Il2CppCodeGenWriteBarrier((&___messages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_T3531299539_H
#ifndef XMLENTITYREFERENCE_T3237868160_H
#define XMLENTITYREFERENCE_T3237868160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t3237868160  : public XmlLinkedNode_t1947670907
{
public:
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_7;

public:
	inline static int32_t get_offset_of_entityName_6() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3237868160, ___entityName_6)); }
	inline String_t* get_entityName_6() const { return ___entityName_6; }
	inline String_t** get_address_of_entityName_6() { return &___entityName_6; }
	inline void set_entityName_6(String_t* value)
	{
		___entityName_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_6), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3237868160, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T3237868160_H
#ifndef XMLELEMENT_T687149941_H
#define XMLELEMENT_T687149941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t687149941  : public XmlLinkedNode_t1947670907
{
public:
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t3528594990 * ___attributes_6;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t1006717423 * ___name_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_8;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_9;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	RuntimeObject* ___schemaInfo_10;

public:
	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(XmlElement_t687149941, ___attributes_6)); }
	inline XmlAttributeCollection_t3528594990 * get_attributes_6() const { return ___attributes_6; }
	inline XmlAttributeCollection_t3528594990 ** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(XmlAttributeCollection_t3528594990 * value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(XmlElement_t687149941, ___name_7)); }
	inline XmlNameEntry_t1006717423 * get_name_7() const { return ___name_7; }
	inline XmlNameEntry_t1006717423 ** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(XmlNameEntry_t1006717423 * value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlElement_t687149941, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}

	inline static int32_t get_offset_of_isNotEmpty_9() { return static_cast<int32_t>(offsetof(XmlElement_t687149941, ___isNotEmpty_9)); }
	inline bool get_isNotEmpty_9() const { return ___isNotEmpty_9; }
	inline bool* get_address_of_isNotEmpty_9() { return &___isNotEmpty_9; }
	inline void set_isNotEmpty_9(bool value)
	{
		___isNotEmpty_9 = value;
	}

	inline static int32_t get_offset_of_schemaInfo_10() { return static_cast<int32_t>(offsetof(XmlElement_t687149941, ___schemaInfo_10)); }
	inline RuntimeObject* get_schemaInfo_10() const { return ___schemaInfo_10; }
	inline RuntimeObject** get_address_of_schemaInfo_10() { return &___schemaInfo_10; }
	inline void set_schemaInfo_10(RuntimeObject* value)
	{
		___schemaInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T687149941_H
#ifndef TIMEFIELDATTRIBUTE_T2900476623_H
#define TIMEFIELDATTRIBUTE_T2900476623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeFieldAttribute
struct  TimeFieldAttribute_t2900476623  : public PropertyAttribute_t381499487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEFIELDATTRIBUTE_T2900476623_H
#ifndef XMLCHARACTERDATA_T4217070933_H
#define XMLCHARACTERDATA_T4217070933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t4217070933  : public XmlLinkedNode_t1947670907
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(XmlCharacterData_t4217070933, ___data_6)); }
	inline String_t* get_data_6() const { return ___data_6; }
	inline String_t** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(String_t* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T4217070933_H
#ifndef READSTATE_T3065260582_H
#define READSTATE_T3065260582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t3065260582 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t3065260582, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T3065260582_H
#ifndef WHITESPACEHANDLING_T2116368771_H
#define WHITESPACEHANDLING_T2116368771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t2116368771 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t2116368771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T2116368771_H
#ifndef ENTITYHANDLING_T3541905618_H
#define ENTITYHANDLING_T3541905618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t3541905618 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t3541905618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T3541905618_H
#ifndef NEWLINEHANDLING_T3095395276_H
#define NEWLINEHANDLING_T3095395276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t3095395276 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t3095395276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T3095395276_H
#ifndef NAMESPACEHANDLING_T2739939700_H
#define NAMESPACEHANDLING_T2739939700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t2739939700 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t2739939700, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T2739939700_H
#ifndef CONFORMANCELEVEL_T2038759208_H
#define CONFORMANCELEVEL_T2038759208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t2038759208 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t2038759208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T2038759208_H
#ifndef DTDINPUTSTATE_T2597333765_H
#define DTDINPUTSTATE_T2597333765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/DtdInputState
struct  DtdInputState_t2597333765 
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader/DtdInputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DtdInputState_t2597333765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATE_T2597333765_H
#ifndef PLAYABLEHANDLE_T743382320_H
#define PLAYABLEHANDLE_T743382320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t743382320 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T743382320_H
#ifndef XMLNODECHANGEDACTION_T3523064990_H
#define XMLNODECHANGEDACTION_T3523064990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_t3523064990 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_t3523064990, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_T3523064990_H
#ifndef XMLNODETYPE_T4278070251_H
#define XMLNODETYPE_T4278070251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t4278070251 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t4278070251, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T4278070251_H
#ifndef CLIPCAPS_T319800182_H
#define CLIPCAPS_T319800182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ClipCaps
struct  ClipCaps_t319800182 
{
public:
	// System.Int32 UnityEngine.Timeline.ClipCaps::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipCaps_t319800182, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPCAPS_T319800182_H
#ifndef XMLPROCESSINGINSTRUCTION_T2473598605_H
#define XMLPROCESSINGINSTRUCTION_T2473598605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_t2473598605  : public XmlLinkedNode_t1947670907
{
public:
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_6;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_7;

public:
	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t2473598605, ___target_6)); }
	inline String_t* get_target_6() const { return ___target_6; }
	inline String_t** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(String_t* value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t2473598605, ___data_7)); }
	inline String_t* get_data_7() const { return ___data_7; }
	inline String_t** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(String_t* value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPROCESSINGINSTRUCTION_T2473598605_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089821_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2655089821  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-23
	U24ArrayTypeU248_t4092320982  ___U24U24fieldU2D23_0;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU248_t4092320982  ___U24U24fieldU2D26_1;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24256_t1491899399  ___U24U24fieldU2D27_2;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU24256_t1491899399  ___U24U24fieldU2D28_3;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU241280_t3767486320  ___U24U24fieldU2D29_4;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-43
	U24ArrayTypeU2412_t1096561007  ___U24U24fieldU2D43_5;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-44
	U24ArrayTypeU2412_t1096561007  ___U24U24fieldU2D44_6;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D23_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D23_0)); }
	inline U24ArrayTypeU248_t4092320982  get_U24U24fieldU2D23_0() const { return ___U24U24fieldU2D23_0; }
	inline U24ArrayTypeU248_t4092320982 * get_address_of_U24U24fieldU2D23_0() { return &___U24U24fieldU2D23_0; }
	inline void set_U24U24fieldU2D23_0(U24ArrayTypeU248_t4092320982  value)
	{
		___U24U24fieldU2D23_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D26_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D26_1)); }
	inline U24ArrayTypeU248_t4092320982  get_U24U24fieldU2D26_1() const { return ___U24U24fieldU2D26_1; }
	inline U24ArrayTypeU248_t4092320982 * get_address_of_U24U24fieldU2D26_1() { return &___U24U24fieldU2D26_1; }
	inline void set_U24U24fieldU2D26_1(U24ArrayTypeU248_t4092320982  value)
	{
		___U24U24fieldU2D26_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D27_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D27_2)); }
	inline U24ArrayTypeU24256_t1491899399  get_U24U24fieldU2D27_2() const { return ___U24U24fieldU2D27_2; }
	inline U24ArrayTypeU24256_t1491899399 * get_address_of_U24U24fieldU2D27_2() { return &___U24U24fieldU2D27_2; }
	inline void set_U24U24fieldU2D27_2(U24ArrayTypeU24256_t1491899399  value)
	{
		___U24U24fieldU2D27_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D28_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D28_3)); }
	inline U24ArrayTypeU24256_t1491899399  get_U24U24fieldU2D28_3() const { return ___U24U24fieldU2D28_3; }
	inline U24ArrayTypeU24256_t1491899399 * get_address_of_U24U24fieldU2D28_3() { return &___U24U24fieldU2D28_3; }
	inline void set_U24U24fieldU2D28_3(U24ArrayTypeU24256_t1491899399  value)
	{
		___U24U24fieldU2D28_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D29_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D29_4)); }
	inline U24ArrayTypeU241280_t3767486320  get_U24U24fieldU2D29_4() const { return ___U24U24fieldU2D29_4; }
	inline U24ArrayTypeU241280_t3767486320 * get_address_of_U24U24fieldU2D29_4() { return &___U24U24fieldU2D29_4; }
	inline void set_U24U24fieldU2D29_4(U24ArrayTypeU241280_t3767486320  value)
	{
		___U24U24fieldU2D29_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D43_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D43_5)); }
	inline U24ArrayTypeU2412_t1096561007  get_U24U24fieldU2D43_5() const { return ___U24U24fieldU2D43_5; }
	inline U24ArrayTypeU2412_t1096561007 * get_address_of_U24U24fieldU2D43_5() { return &___U24U24fieldU2D43_5; }
	inline void set_U24U24fieldU2D43_5(U24ArrayTypeU2412_t1096561007  value)
	{
		___U24U24fieldU2D43_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D44_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields, ___U24U24fieldU2D44_6)); }
	inline U24ArrayTypeU2412_t1096561007  get_U24U24fieldU2D44_6() const { return ___U24U24fieldU2D44_6; }
	inline U24ArrayTypeU2412_t1096561007 * get_address_of_U24U24fieldU2D44_6() { return &___U24U24fieldU2D44_6; }
	inline void set_U24U24fieldU2D44_6(U24ArrayTypeU2412_t1096561007  value)
	{
		___U24U24fieldU2D44_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089821_H
#ifndef XMLDECLSTATE_T4043840337_H
#define XMLDECLSTATE_T4043840337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/XmlDeclState
struct  XmlDeclState_t4043840337 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/XmlDeclState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDeclState_t4043840337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLSTATE_T4043840337_H
#ifndef XMLTOKENIZEDTYPE_T3218469591_H
#define XMLTOKENIZEDTYPE_T3218469591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTokenizedType
struct  XmlTokenizedType_t3218469591 
{
public:
	// System.Int32 System.Xml.XmlTokenizedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTokenizedType_t3218469591, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENIZEDTYPE_T3218469591_H
#ifndef COMMANDSTATE_T2322905070_H
#define COMMANDSTATE_T2322905070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport/CommandState
struct  CommandState_t2322905070 
{
public:
	// System.Int32 System.Xml.XmlReaderBinarySupport/CommandState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommandState_t2322905070, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDSTATE_T2322905070_H
#ifndef DURATIONMODE_T3686826482_H
#define DURATIONMODE_T3686826482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/DurationMode
struct  DurationMode_t3686826482 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/DurationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DurationMode_t3686826482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONMODE_T3686826482_H
#ifndef MEDIATYPE_T673317257_H
#define MEDIATYPE_T673317257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/MediaType
struct  MediaType_t673317257 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/MediaType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MediaType_t673317257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPE_T673317257_H
#ifndef XMLSPACE_T1832342193_H
#define XMLSPACE_T1832342193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t1832342193 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t1832342193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T1832342193_H
#ifndef XMLREADERSETTINGS_T190893096_H
#define XMLREADERSETTINGS_T190893096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_t190893096  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_0;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformance
	int32_t ___conformance_1;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::schemas
	XmlSchemaSet_t93272820 * ___schemas_2;
	// System.Boolean System.Xml.XmlReaderSettings::schemasNeedsInitialization
	bool ___schemasNeedsInitialization_3;

public:
	inline static int32_t get_offset_of_checkCharacters_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t190893096, ___checkCharacters_0)); }
	inline bool get_checkCharacters_0() const { return ___checkCharacters_0; }
	inline bool* get_address_of_checkCharacters_0() { return &___checkCharacters_0; }
	inline void set_checkCharacters_0(bool value)
	{
		___checkCharacters_0 = value;
	}

	inline static int32_t get_offset_of_conformance_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t190893096, ___conformance_1)); }
	inline int32_t get_conformance_1() const { return ___conformance_1; }
	inline int32_t* get_address_of_conformance_1() { return &___conformance_1; }
	inline void set_conformance_1(int32_t value)
	{
		___conformance_1 = value;
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t190893096, ___schemas_2)); }
	inline XmlSchemaSet_t93272820 * get_schemas_2() const { return ___schemas_2; }
	inline XmlSchemaSet_t93272820 ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(XmlSchemaSet_t93272820 * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_schemasNeedsInitialization_3() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t190893096, ___schemasNeedsInitialization_3)); }
	inline bool get_schemasNeedsInitialization_3() const { return ___schemasNeedsInitialization_3; }
	inline bool* get_address_of_schemasNeedsInitialization_3() { return &___schemasNeedsInitialization_3; }
	inline void set_schemasNeedsInitialization_3(bool value)
	{
		___schemasNeedsInitialization_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSETTINGS_T190893096_H
#ifndef XMLSIGNIFICANTWHITESPACE_T2546368137_H
#define XMLSIGNIFICANTWHITESPACE_T2546368137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSignificantWhitespace
struct  XmlSignificantWhitespace_t2546368137  : public XmlCharacterData_t4217070933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSIGNIFICANTWHITESPACE_T2546368137_H
#ifndef XMLWHITESPACE_T547184992_H
#define XMLWHITESPACE_T547184992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWhitespace
struct  XmlWhitespace_t547184992  : public XmlCharacterData_t4217070933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWHITESPACE_T547184992_H
#ifndef PLAYABLEBINDING_T1732044054_H
#define PLAYABLEBINDING_T1732044054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t1732044054 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t692178351 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t692178351 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t692178351 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t692178351 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t1732044054_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t4186695123* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t4186695123* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t4186695123** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t4186695123* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t1732044054_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t692178351_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t1732044054_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t692178351_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};
};
#endif // PLAYABLEBINDING_T1732044054_H
#ifndef PLAYABLE_T3296292090_H
#define PLAYABLE_T3296292090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t3296292090 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t3296292090, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t3296292090_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t3296292090  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t3296292090_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t3296292090  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t3296292090 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t3296292090  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T3296292090_H
#ifndef XMLNODEINFO_T406731110_H
#define XMLNODEINFO_T406731110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/XmlNodeInfo
struct  XmlNodeInfo_t406731110  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::NS
	String_t* ___NS_2;
	// System.Boolean System.Xml.XmlTextWriter/XmlNodeInfo::HasSimple
	bool ___HasSimple_3;
	// System.Boolean System.Xml.XmlTextWriter/XmlNodeInfo::HasElements
	bool ___HasElements_4;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::XmlLang
	String_t* ___XmlLang_5;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter/XmlNodeInfo::XmlSpace
	int32_t ___XmlSpace_6;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_HasSimple_3() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___HasSimple_3)); }
	inline bool get_HasSimple_3() const { return ___HasSimple_3; }
	inline bool* get_address_of_HasSimple_3() { return &___HasSimple_3; }
	inline void set_HasSimple_3(bool value)
	{
		___HasSimple_3 = value;
	}

	inline static int32_t get_offset_of_HasElements_4() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___HasElements_4)); }
	inline bool get_HasElements_4() const { return ___HasElements_4; }
	inline bool* get_address_of_HasElements_4() { return &___HasElements_4; }
	inline void set_HasElements_4(bool value)
	{
		___HasElements_4 = value;
	}

	inline static int32_t get_offset_of_XmlLang_5() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___XmlLang_5)); }
	inline String_t* get_XmlLang_5() const { return ___XmlLang_5; }
	inline String_t** get_address_of_XmlLang_5() { return &___XmlLang_5; }
	inline void set_XmlLang_5(String_t* value)
	{
		___XmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_5), value);
	}

	inline static int32_t get_offset_of_XmlSpace_6() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t406731110, ___XmlSpace_6)); }
	inline int32_t get_XmlSpace_6() const { return ___XmlSpace_6; }
	inline int32_t* get_address_of_XmlSpace_6() { return &___XmlSpace_6; }
	inline void set_XmlSpace_6(int32_t value)
	{
		___XmlSpace_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEINFO_T406731110_H
#ifndef XMLTEXTWRITER_T3070435925_H
#define XMLTEXTWRITER_T3070435925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_t3070435925  : public XmlWriter_t1771783396
{
public:
	// System.IO.Stream System.Xml.XmlTextWriter::base_stream
	Stream_t4138477179 * ___base_stream_3;
	// System.IO.TextWriter System.Xml.XmlTextWriter::source
	TextWriter_t997444721 * ___source_4;
	// System.IO.TextWriter System.Xml.XmlTextWriter::writer
	TextWriter_t997444721 * ___writer_5;
	// System.IO.StringWriter System.Xml.XmlTextWriter::preserver
	StringWriter_t1837008501 * ___preserver_6;
	// System.String System.Xml.XmlTextWriter::preserved_name
	String_t* ___preserved_name_7;
	// System.Boolean System.Xml.XmlTextWriter::is_preserved_xmlns
	bool ___is_preserved_xmlns_8;
	// System.Boolean System.Xml.XmlTextWriter::allow_doc_fragment
	bool ___allow_doc_fragment_9;
	// System.Boolean System.Xml.XmlTextWriter::close_output_stream
	bool ___close_output_stream_10;
	// System.Boolean System.Xml.XmlTextWriter::ignore_encoding
	bool ___ignore_encoding_11;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_12;
	// System.Xml.XmlTextWriter/XmlDeclState System.Xml.XmlTextWriter::xmldecl_state
	int32_t ___xmldecl_state_13;
	// System.Boolean System.Xml.XmlTextWriter::check_character_validity
	bool ___check_character_validity_14;
	// System.Xml.NewLineHandling System.Xml.XmlTextWriter::newline_handling
	int32_t ___newline_handling_15;
	// System.Boolean System.Xml.XmlTextWriter::is_document_entity
	bool ___is_document_entity_16;
	// System.Xml.WriteState System.Xml.XmlTextWriter::state
	int32_t ___state_17;
	// System.Xml.XmlNodeType System.Xml.XmlTextWriter::node_state
	int32_t ___node_state_18;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextWriter::nsmanager
	XmlNamespaceManager_t1354331288 * ___nsmanager_19;
	// System.Int32 System.Xml.XmlTextWriter::open_count
	int32_t ___open_count_20;
	// System.Xml.XmlTextWriter/XmlNodeInfo[] System.Xml.XmlTextWriter::elements
	XmlNodeInfoU5BU5D_t1395238467* ___elements_21;
	// System.Collections.Stack System.Xml.XmlTextWriter::new_local_namespaces
	Stack_t535311253 * ___new_local_namespaces_22;
	// System.Collections.ArrayList System.Xml.XmlTextWriter::explicit_nsdecls
	ArrayList_t4250946984 * ___explicit_nsdecls_23;
	// System.Xml.NamespaceHandling System.Xml.XmlTextWriter::namespace_handling
	int32_t ___namespace_handling_24;
	// System.Boolean System.Xml.XmlTextWriter::indent
	bool ___indent_25;
	// System.Int32 System.Xml.XmlTextWriter::indent_count
	int32_t ___indent_count_26;
	// System.Char System.Xml.XmlTextWriter::indent_char
	Il2CppChar ___indent_char_27;
	// System.String System.Xml.XmlTextWriter::indent_string
	String_t* ___indent_string_28;
	// System.String System.Xml.XmlTextWriter::newline
	String_t* ___newline_29;
	// System.Boolean System.Xml.XmlTextWriter::indent_attributes
	bool ___indent_attributes_30;
	// System.Char System.Xml.XmlTextWriter::quote_char
	Il2CppChar ___quote_char_31;
	// System.Boolean System.Xml.XmlTextWriter::v2
	bool ___v2_32;

public:
	inline static int32_t get_offset_of_base_stream_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___base_stream_3)); }
	inline Stream_t4138477179 * get_base_stream_3() const { return ___base_stream_3; }
	inline Stream_t4138477179 ** get_address_of_base_stream_3() { return &___base_stream_3; }
	inline void set_base_stream_3(Stream_t4138477179 * value)
	{
		___base_stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___source_4)); }
	inline TextWriter_t997444721 * get_source_4() const { return ___source_4; }
	inline TextWriter_t997444721 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(TextWriter_t997444721 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_writer_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___writer_5)); }
	inline TextWriter_t997444721 * get_writer_5() const { return ___writer_5; }
	inline TextWriter_t997444721 ** get_address_of_writer_5() { return &___writer_5; }
	inline void set_writer_5(TextWriter_t997444721 * value)
	{
		___writer_5 = value;
		Il2CppCodeGenWriteBarrier((&___writer_5), value);
	}

	inline static int32_t get_offset_of_preserver_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___preserver_6)); }
	inline StringWriter_t1837008501 * get_preserver_6() const { return ___preserver_6; }
	inline StringWriter_t1837008501 ** get_address_of_preserver_6() { return &___preserver_6; }
	inline void set_preserver_6(StringWriter_t1837008501 * value)
	{
		___preserver_6 = value;
		Il2CppCodeGenWriteBarrier((&___preserver_6), value);
	}

	inline static int32_t get_offset_of_preserved_name_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___preserved_name_7)); }
	inline String_t* get_preserved_name_7() const { return ___preserved_name_7; }
	inline String_t** get_address_of_preserved_name_7() { return &___preserved_name_7; }
	inline void set_preserved_name_7(String_t* value)
	{
		___preserved_name_7 = value;
		Il2CppCodeGenWriteBarrier((&___preserved_name_7), value);
	}

	inline static int32_t get_offset_of_is_preserved_xmlns_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___is_preserved_xmlns_8)); }
	inline bool get_is_preserved_xmlns_8() const { return ___is_preserved_xmlns_8; }
	inline bool* get_address_of_is_preserved_xmlns_8() { return &___is_preserved_xmlns_8; }
	inline void set_is_preserved_xmlns_8(bool value)
	{
		___is_preserved_xmlns_8 = value;
	}

	inline static int32_t get_offset_of_allow_doc_fragment_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___allow_doc_fragment_9)); }
	inline bool get_allow_doc_fragment_9() const { return ___allow_doc_fragment_9; }
	inline bool* get_address_of_allow_doc_fragment_9() { return &___allow_doc_fragment_9; }
	inline void set_allow_doc_fragment_9(bool value)
	{
		___allow_doc_fragment_9 = value;
	}

	inline static int32_t get_offset_of_close_output_stream_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___close_output_stream_10)); }
	inline bool get_close_output_stream_10() const { return ___close_output_stream_10; }
	inline bool* get_address_of_close_output_stream_10() { return &___close_output_stream_10; }
	inline void set_close_output_stream_10(bool value)
	{
		___close_output_stream_10 = value;
	}

	inline static int32_t get_offset_of_ignore_encoding_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___ignore_encoding_11)); }
	inline bool get_ignore_encoding_11() const { return ___ignore_encoding_11; }
	inline bool* get_address_of_ignore_encoding_11() { return &___ignore_encoding_11; }
	inline void set_ignore_encoding_11(bool value)
	{
		___ignore_encoding_11 = value;
	}

	inline static int32_t get_offset_of_namespaces_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___namespaces_12)); }
	inline bool get_namespaces_12() const { return ___namespaces_12; }
	inline bool* get_address_of_namespaces_12() { return &___namespaces_12; }
	inline void set_namespaces_12(bool value)
	{
		___namespaces_12 = value;
	}

	inline static int32_t get_offset_of_xmldecl_state_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___xmldecl_state_13)); }
	inline int32_t get_xmldecl_state_13() const { return ___xmldecl_state_13; }
	inline int32_t* get_address_of_xmldecl_state_13() { return &___xmldecl_state_13; }
	inline void set_xmldecl_state_13(int32_t value)
	{
		___xmldecl_state_13 = value;
	}

	inline static int32_t get_offset_of_check_character_validity_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___check_character_validity_14)); }
	inline bool get_check_character_validity_14() const { return ___check_character_validity_14; }
	inline bool* get_address_of_check_character_validity_14() { return &___check_character_validity_14; }
	inline void set_check_character_validity_14(bool value)
	{
		___check_character_validity_14 = value;
	}

	inline static int32_t get_offset_of_newline_handling_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___newline_handling_15)); }
	inline int32_t get_newline_handling_15() const { return ___newline_handling_15; }
	inline int32_t* get_address_of_newline_handling_15() { return &___newline_handling_15; }
	inline void set_newline_handling_15(int32_t value)
	{
		___newline_handling_15 = value;
	}

	inline static int32_t get_offset_of_is_document_entity_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___is_document_entity_16)); }
	inline bool get_is_document_entity_16() const { return ___is_document_entity_16; }
	inline bool* get_address_of_is_document_entity_16() { return &___is_document_entity_16; }
	inline void set_is_document_entity_16(bool value)
	{
		___is_document_entity_16 = value;
	}

	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___state_17)); }
	inline int32_t get_state_17() const { return ___state_17; }
	inline int32_t* get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(int32_t value)
	{
		___state_17 = value;
	}

	inline static int32_t get_offset_of_node_state_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___node_state_18)); }
	inline int32_t get_node_state_18() const { return ___node_state_18; }
	inline int32_t* get_address_of_node_state_18() { return &___node_state_18; }
	inline void set_node_state_18(int32_t value)
	{
		___node_state_18 = value;
	}

	inline static int32_t get_offset_of_nsmanager_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___nsmanager_19)); }
	inline XmlNamespaceManager_t1354331288 * get_nsmanager_19() const { return ___nsmanager_19; }
	inline XmlNamespaceManager_t1354331288 ** get_address_of_nsmanager_19() { return &___nsmanager_19; }
	inline void set_nsmanager_19(XmlNamespaceManager_t1354331288 * value)
	{
		___nsmanager_19 = value;
		Il2CppCodeGenWriteBarrier((&___nsmanager_19), value);
	}

	inline static int32_t get_offset_of_open_count_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___open_count_20)); }
	inline int32_t get_open_count_20() const { return ___open_count_20; }
	inline int32_t* get_address_of_open_count_20() { return &___open_count_20; }
	inline void set_open_count_20(int32_t value)
	{
		___open_count_20 = value;
	}

	inline static int32_t get_offset_of_elements_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___elements_21)); }
	inline XmlNodeInfoU5BU5D_t1395238467* get_elements_21() const { return ___elements_21; }
	inline XmlNodeInfoU5BU5D_t1395238467** get_address_of_elements_21() { return &___elements_21; }
	inline void set_elements_21(XmlNodeInfoU5BU5D_t1395238467* value)
	{
		___elements_21 = value;
		Il2CppCodeGenWriteBarrier((&___elements_21), value);
	}

	inline static int32_t get_offset_of_new_local_namespaces_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___new_local_namespaces_22)); }
	inline Stack_t535311253 * get_new_local_namespaces_22() const { return ___new_local_namespaces_22; }
	inline Stack_t535311253 ** get_address_of_new_local_namespaces_22() { return &___new_local_namespaces_22; }
	inline void set_new_local_namespaces_22(Stack_t535311253 * value)
	{
		___new_local_namespaces_22 = value;
		Il2CppCodeGenWriteBarrier((&___new_local_namespaces_22), value);
	}

	inline static int32_t get_offset_of_explicit_nsdecls_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___explicit_nsdecls_23)); }
	inline ArrayList_t4250946984 * get_explicit_nsdecls_23() const { return ___explicit_nsdecls_23; }
	inline ArrayList_t4250946984 ** get_address_of_explicit_nsdecls_23() { return &___explicit_nsdecls_23; }
	inline void set_explicit_nsdecls_23(ArrayList_t4250946984 * value)
	{
		___explicit_nsdecls_23 = value;
		Il2CppCodeGenWriteBarrier((&___explicit_nsdecls_23), value);
	}

	inline static int32_t get_offset_of_namespace_handling_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___namespace_handling_24)); }
	inline int32_t get_namespace_handling_24() const { return ___namespace_handling_24; }
	inline int32_t* get_address_of_namespace_handling_24() { return &___namespace_handling_24; }
	inline void set_namespace_handling_24(int32_t value)
	{
		___namespace_handling_24 = value;
	}

	inline static int32_t get_offset_of_indent_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___indent_25)); }
	inline bool get_indent_25() const { return ___indent_25; }
	inline bool* get_address_of_indent_25() { return &___indent_25; }
	inline void set_indent_25(bool value)
	{
		___indent_25 = value;
	}

	inline static int32_t get_offset_of_indent_count_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___indent_count_26)); }
	inline int32_t get_indent_count_26() const { return ___indent_count_26; }
	inline int32_t* get_address_of_indent_count_26() { return &___indent_count_26; }
	inline void set_indent_count_26(int32_t value)
	{
		___indent_count_26 = value;
	}

	inline static int32_t get_offset_of_indent_char_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___indent_char_27)); }
	inline Il2CppChar get_indent_char_27() const { return ___indent_char_27; }
	inline Il2CppChar* get_address_of_indent_char_27() { return &___indent_char_27; }
	inline void set_indent_char_27(Il2CppChar value)
	{
		___indent_char_27 = value;
	}

	inline static int32_t get_offset_of_indent_string_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___indent_string_28)); }
	inline String_t* get_indent_string_28() const { return ___indent_string_28; }
	inline String_t** get_address_of_indent_string_28() { return &___indent_string_28; }
	inline void set_indent_string_28(String_t* value)
	{
		___indent_string_28 = value;
		Il2CppCodeGenWriteBarrier((&___indent_string_28), value);
	}

	inline static int32_t get_offset_of_newline_29() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___newline_29)); }
	inline String_t* get_newline_29() const { return ___newline_29; }
	inline String_t** get_address_of_newline_29() { return &___newline_29; }
	inline void set_newline_29(String_t* value)
	{
		___newline_29 = value;
		Il2CppCodeGenWriteBarrier((&___newline_29), value);
	}

	inline static int32_t get_offset_of_indent_attributes_30() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___indent_attributes_30)); }
	inline bool get_indent_attributes_30() const { return ___indent_attributes_30; }
	inline bool* get_address_of_indent_attributes_30() { return &___indent_attributes_30; }
	inline void set_indent_attributes_30(bool value)
	{
		___indent_attributes_30 = value;
	}

	inline static int32_t get_offset_of_quote_char_31() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___quote_char_31)); }
	inline Il2CppChar get_quote_char_31() const { return ___quote_char_31; }
	inline Il2CppChar* get_address_of_quote_char_31() { return &___quote_char_31; }
	inline void set_quote_char_31(Il2CppChar value)
	{
		___quote_char_31 = value;
	}

	inline static int32_t get_offset_of_v2_32() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925, ___v2_32)); }
	inline bool get_v2_32() const { return ___v2_32; }
	inline bool* get_address_of_v2_32() { return &___v2_32; }
	inline void set_v2_32(bool value)
	{
		___v2_32 = value;
	}
};

struct XmlTextWriter_t3070435925_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlTextWriter::unmarked_utf8encoding
	Encoding_t4004889433 * ___unmarked_utf8encoding_0;
	// System.Char[] System.Xml.XmlTextWriter::escaped_text_chars
	CharU5BU5D_t3419619864* ___escaped_text_chars_1;
	// System.Char[] System.Xml.XmlTextWriter::escaped_attr_chars
	CharU5BU5D_t3419619864* ___escaped_attr_chars_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switch$map53
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map53_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switch$map54
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map54_34;

public:
	inline static int32_t get_offset_of_unmarked_utf8encoding_0() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925_StaticFields, ___unmarked_utf8encoding_0)); }
	inline Encoding_t4004889433 * get_unmarked_utf8encoding_0() const { return ___unmarked_utf8encoding_0; }
	inline Encoding_t4004889433 ** get_address_of_unmarked_utf8encoding_0() { return &___unmarked_utf8encoding_0; }
	inline void set_unmarked_utf8encoding_0(Encoding_t4004889433 * value)
	{
		___unmarked_utf8encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___unmarked_utf8encoding_0), value);
	}

	inline static int32_t get_offset_of_escaped_text_chars_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925_StaticFields, ___escaped_text_chars_1)); }
	inline CharU5BU5D_t3419619864* get_escaped_text_chars_1() const { return ___escaped_text_chars_1; }
	inline CharU5BU5D_t3419619864** get_address_of_escaped_text_chars_1() { return &___escaped_text_chars_1; }
	inline void set_escaped_text_chars_1(CharU5BU5D_t3419619864* value)
	{
		___escaped_text_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_text_chars_1), value);
	}

	inline static int32_t get_offset_of_escaped_attr_chars_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925_StaticFields, ___escaped_attr_chars_2)); }
	inline CharU5BU5D_t3419619864* get_escaped_attr_chars_2() const { return ___escaped_attr_chars_2; }
	inline CharU5BU5D_t3419619864** get_address_of_escaped_attr_chars_2() { return &___escaped_attr_chars_2; }
	inline void set_escaped_attr_chars_2(CharU5BU5D_t3419619864* value)
	{
		___escaped_attr_chars_2 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_attr_chars_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map53_33() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925_StaticFields, ___U3CU3Ef__switchU24map53_33)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map53_33() const { return ___U3CU3Ef__switchU24map53_33; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map53_33() { return &___U3CU3Ef__switchU24map53_33; }
	inline void set_U3CU3Ef__switchU24map53_33(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map53_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map53_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map54_34() { return static_cast<int32_t>(offsetof(XmlTextWriter_t3070435925_StaticFields, ___U3CU3Ef__switchU24map54_34)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map54_34() const { return ___U3CU3Ef__switchU24map54_34; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map54_34() { return &___U3CU3Ef__switchU24map54_34; }
	inline void set_U3CU3Ef__switchU24map54_34(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map54_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map54_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITER_T3070435925_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef XMLTEXTREADER_T3875484823_H
#define XMLTEXTREADER_T3875484823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader
struct  XmlTextReader_t3875484823  : public XmlReader_t1082502433
{
public:
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::cursorToken
	XmlTokenInfo_t2043079912 * ___cursorToken_2;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::currentToken
	XmlTokenInfo_t2043079912 * ___currentToken_3;
	// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo Mono.Xml2.XmlTextReader::currentAttributeToken
	XmlAttributeTokenInfo_t2488827827 * ___currentAttributeToken_4;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::currentAttributeValueToken
	XmlTokenInfo_t2043079912 * ___currentAttributeValueToken_5;
	// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[] Mono.Xml2.XmlTextReader::attributeTokens
	XmlAttributeTokenInfoU5BU5D_t827491362* ___attributeTokens_6;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo[] Mono.Xml2.XmlTextReader::attributeValueTokens
	XmlTokenInfoU5BU5D_t3383196281* ___attributeValueTokens_7;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttribute
	int32_t ___currentAttribute_8;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttributeValue
	int32_t ___currentAttributeValue_9;
	// System.Int32 Mono.Xml2.XmlTextReader::attributeCount
	int32_t ___attributeCount_10;
	// System.Xml.XmlParserContext Mono.Xml2.XmlTextReader::parserContext
	XmlParserContext_t3802687902 * ___parserContext_11;
	// System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::nameTable
	XmlNameTable_t347431366 * ___nameTable_12;
	// System.Xml.XmlNamespaceManager Mono.Xml2.XmlTextReader::nsmgr
	XmlNamespaceManager_t1354331288 * ___nsmgr_13;
	// System.Xml.ReadState Mono.Xml2.XmlTextReader::readState
	int32_t ___readState_14;
	// System.Boolean Mono.Xml2.XmlTextReader::disallowReset
	bool ___disallowReset_15;
	// System.Int32 Mono.Xml2.XmlTextReader::depth
	int32_t ___depth_16;
	// System.Int32 Mono.Xml2.XmlTextReader::elementDepth
	int32_t ___elementDepth_17;
	// System.Boolean Mono.Xml2.XmlTextReader::depthUp
	bool ___depthUp_18;
	// System.Boolean Mono.Xml2.XmlTextReader::popScope
	bool ___popScope_19;
	// Mono.Xml2.XmlTextReader/TagName[] Mono.Xml2.XmlTextReader::elementNames
	TagNameU5BU5D_t2618800335* ___elementNames_20;
	// System.Int32 Mono.Xml2.XmlTextReader::elementNameStackPos
	int32_t ___elementNameStackPos_21;
	// System.Boolean Mono.Xml2.XmlTextReader::allowMultipleRoot
	bool ___allowMultipleRoot_22;
	// System.Boolean Mono.Xml2.XmlTextReader::isStandalone
	bool ___isStandalone_23;
	// System.Boolean Mono.Xml2.XmlTextReader::returnEntityReference
	bool ___returnEntityReference_24;
	// System.String Mono.Xml2.XmlTextReader::entityReferenceName
	String_t* ___entityReferenceName_25;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader::valueBuffer
	StringBuilder_t622404039 * ___valueBuffer_26;
	// System.IO.TextReader Mono.Xml2.XmlTextReader::reader
	TextReader_t218744201 * ___reader_27;
	// System.Char[] Mono.Xml2.XmlTextReader::peekChars
	CharU5BU5D_t3419619864* ___peekChars_28;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsIndex
	int32_t ___peekCharsIndex_29;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsLength
	int32_t ___peekCharsLength_30;
	// System.Int32 Mono.Xml2.XmlTextReader::curNodePeekIndex
	int32_t ___curNodePeekIndex_31;
	// System.Boolean Mono.Xml2.XmlTextReader::preserveCurrentTag
	bool ___preserveCurrentTag_32;
	// System.Int32 Mono.Xml2.XmlTextReader::line
	int32_t ___line_33;
	// System.Int32 Mono.Xml2.XmlTextReader::column
	int32_t ___column_34;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_35;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_36;
	// System.Boolean Mono.Xml2.XmlTextReader::useProceedingLineInfo
	bool ___useProceedingLineInfo_37;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::startNodeType
	int32_t ___startNodeType_38;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::currentState
	int32_t ___currentState_39;
	// System.Int32 Mono.Xml2.XmlTextReader::nestLevel
	int32_t ___nestLevel_40;
	// System.Boolean Mono.Xml2.XmlTextReader::readCharsInProgress
	bool ___readCharsInProgress_41;
	// System.Xml.XmlReaderBinarySupport/CharGetter Mono.Xml2.XmlTextReader::binaryCharGetter
	CharGetter_t1293008504 * ___binaryCharGetter_42;
	// System.Boolean Mono.Xml2.XmlTextReader::namespaces
	bool ___namespaces_43;
	// System.Xml.WhitespaceHandling Mono.Xml2.XmlTextReader::whitespaceHandling
	int32_t ___whitespaceHandling_44;
	// System.Xml.XmlResolver Mono.Xml2.XmlTextReader::resolver
	XmlResolver_t161349657 * ___resolver_45;
	// System.Boolean Mono.Xml2.XmlTextReader::normalization
	bool ___normalization_46;
	// System.Boolean Mono.Xml2.XmlTextReader::checkCharacters
	bool ___checkCharacters_47;
	// System.Boolean Mono.Xml2.XmlTextReader::prohibitDtd
	bool ___prohibitDtd_48;
	// System.Boolean Mono.Xml2.XmlTextReader::closeInput
	bool ___closeInput_49;
	// System.Xml.EntityHandling Mono.Xml2.XmlTextReader::entityHandling
	int32_t ___entityHandling_50;
	// System.Xml.NameTable Mono.Xml2.XmlTextReader::whitespacePool
	NameTable_t3507653384 * ___whitespacePool_51;
	// System.Char[] Mono.Xml2.XmlTextReader::whitespaceCache
	CharU5BU5D_t3419619864* ___whitespaceCache_52;
	// Mono.Xml2.XmlTextReader/DtdInputStateStack Mono.Xml2.XmlTextReader::stateStack
	DtdInputStateStack_t2344161889 * ___stateStack_53;

public:
	inline static int32_t get_offset_of_cursorToken_2() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___cursorToken_2)); }
	inline XmlTokenInfo_t2043079912 * get_cursorToken_2() const { return ___cursorToken_2; }
	inline XmlTokenInfo_t2043079912 ** get_address_of_cursorToken_2() { return &___cursorToken_2; }
	inline void set_cursorToken_2(XmlTokenInfo_t2043079912 * value)
	{
		___cursorToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___cursorToken_2), value);
	}

	inline static int32_t get_offset_of_currentToken_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentToken_3)); }
	inline XmlTokenInfo_t2043079912 * get_currentToken_3() const { return ___currentToken_3; }
	inline XmlTokenInfo_t2043079912 ** get_address_of_currentToken_3() { return &___currentToken_3; }
	inline void set_currentToken_3(XmlTokenInfo_t2043079912 * value)
	{
		___currentToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentToken_3), value);
	}

	inline static int32_t get_offset_of_currentAttributeToken_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentAttributeToken_4)); }
	inline XmlAttributeTokenInfo_t2488827827 * get_currentAttributeToken_4() const { return ___currentAttributeToken_4; }
	inline XmlAttributeTokenInfo_t2488827827 ** get_address_of_currentAttributeToken_4() { return &___currentAttributeToken_4; }
	inline void set_currentAttributeToken_4(XmlAttributeTokenInfo_t2488827827 * value)
	{
		___currentAttributeToken_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeToken_4), value);
	}

	inline static int32_t get_offset_of_currentAttributeValueToken_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentAttributeValueToken_5)); }
	inline XmlTokenInfo_t2043079912 * get_currentAttributeValueToken_5() const { return ___currentAttributeValueToken_5; }
	inline XmlTokenInfo_t2043079912 ** get_address_of_currentAttributeValueToken_5() { return &___currentAttributeValueToken_5; }
	inline void set_currentAttributeValueToken_5(XmlTokenInfo_t2043079912 * value)
	{
		___currentAttributeValueToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeValueToken_5), value);
	}

	inline static int32_t get_offset_of_attributeTokens_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___attributeTokens_6)); }
	inline XmlAttributeTokenInfoU5BU5D_t827491362* get_attributeTokens_6() const { return ___attributeTokens_6; }
	inline XmlAttributeTokenInfoU5BU5D_t827491362** get_address_of_attributeTokens_6() { return &___attributeTokens_6; }
	inline void set_attributeTokens_6(XmlAttributeTokenInfoU5BU5D_t827491362* value)
	{
		___attributeTokens_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTokens_6), value);
	}

	inline static int32_t get_offset_of_attributeValueTokens_7() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___attributeValueTokens_7)); }
	inline XmlTokenInfoU5BU5D_t3383196281* get_attributeValueTokens_7() const { return ___attributeValueTokens_7; }
	inline XmlTokenInfoU5BU5D_t3383196281** get_address_of_attributeValueTokens_7() { return &___attributeValueTokens_7; }
	inline void set_attributeValueTokens_7(XmlTokenInfoU5BU5D_t3383196281* value)
	{
		___attributeValueTokens_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueTokens_7), value);
	}

	inline static int32_t get_offset_of_currentAttribute_8() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentAttribute_8)); }
	inline int32_t get_currentAttribute_8() const { return ___currentAttribute_8; }
	inline int32_t* get_address_of_currentAttribute_8() { return &___currentAttribute_8; }
	inline void set_currentAttribute_8(int32_t value)
	{
		___currentAttribute_8 = value;
	}

	inline static int32_t get_offset_of_currentAttributeValue_9() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentAttributeValue_9)); }
	inline int32_t get_currentAttributeValue_9() const { return ___currentAttributeValue_9; }
	inline int32_t* get_address_of_currentAttributeValue_9() { return &___currentAttributeValue_9; }
	inline void set_currentAttributeValue_9(int32_t value)
	{
		___currentAttributeValue_9 = value;
	}

	inline static int32_t get_offset_of_attributeCount_10() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___attributeCount_10)); }
	inline int32_t get_attributeCount_10() const { return ___attributeCount_10; }
	inline int32_t* get_address_of_attributeCount_10() { return &___attributeCount_10; }
	inline void set_attributeCount_10(int32_t value)
	{
		___attributeCount_10 = value;
	}

	inline static int32_t get_offset_of_parserContext_11() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___parserContext_11)); }
	inline XmlParserContext_t3802687902 * get_parserContext_11() const { return ___parserContext_11; }
	inline XmlParserContext_t3802687902 ** get_address_of_parserContext_11() { return &___parserContext_11; }
	inline void set_parserContext_11(XmlParserContext_t3802687902 * value)
	{
		___parserContext_11 = value;
		Il2CppCodeGenWriteBarrier((&___parserContext_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___nameTable_12)); }
	inline XmlNameTable_t347431366 * get_nameTable_12() const { return ___nameTable_12; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(XmlNameTable_t347431366 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}

	inline static int32_t get_offset_of_nsmgr_13() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___nsmgr_13)); }
	inline XmlNamespaceManager_t1354331288 * get_nsmgr_13() const { return ___nsmgr_13; }
	inline XmlNamespaceManager_t1354331288 ** get_address_of_nsmgr_13() { return &___nsmgr_13; }
	inline void set_nsmgr_13(XmlNamespaceManager_t1354331288 * value)
	{
		___nsmgr_13 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_13), value);
	}

	inline static int32_t get_offset_of_readState_14() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___readState_14)); }
	inline int32_t get_readState_14() const { return ___readState_14; }
	inline int32_t* get_address_of_readState_14() { return &___readState_14; }
	inline void set_readState_14(int32_t value)
	{
		___readState_14 = value;
	}

	inline static int32_t get_offset_of_disallowReset_15() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___disallowReset_15)); }
	inline bool get_disallowReset_15() const { return ___disallowReset_15; }
	inline bool* get_address_of_disallowReset_15() { return &___disallowReset_15; }
	inline void set_disallowReset_15(bool value)
	{
		___disallowReset_15 = value;
	}

	inline static int32_t get_offset_of_depth_16() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___depth_16)); }
	inline int32_t get_depth_16() const { return ___depth_16; }
	inline int32_t* get_address_of_depth_16() { return &___depth_16; }
	inline void set_depth_16(int32_t value)
	{
		___depth_16 = value;
	}

	inline static int32_t get_offset_of_elementDepth_17() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___elementDepth_17)); }
	inline int32_t get_elementDepth_17() const { return ___elementDepth_17; }
	inline int32_t* get_address_of_elementDepth_17() { return &___elementDepth_17; }
	inline void set_elementDepth_17(int32_t value)
	{
		___elementDepth_17 = value;
	}

	inline static int32_t get_offset_of_depthUp_18() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___depthUp_18)); }
	inline bool get_depthUp_18() const { return ___depthUp_18; }
	inline bool* get_address_of_depthUp_18() { return &___depthUp_18; }
	inline void set_depthUp_18(bool value)
	{
		___depthUp_18 = value;
	}

	inline static int32_t get_offset_of_popScope_19() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___popScope_19)); }
	inline bool get_popScope_19() const { return ___popScope_19; }
	inline bool* get_address_of_popScope_19() { return &___popScope_19; }
	inline void set_popScope_19(bool value)
	{
		___popScope_19 = value;
	}

	inline static int32_t get_offset_of_elementNames_20() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___elementNames_20)); }
	inline TagNameU5BU5D_t2618800335* get_elementNames_20() const { return ___elementNames_20; }
	inline TagNameU5BU5D_t2618800335** get_address_of_elementNames_20() { return &___elementNames_20; }
	inline void set_elementNames_20(TagNameU5BU5D_t2618800335* value)
	{
		___elementNames_20 = value;
		Il2CppCodeGenWriteBarrier((&___elementNames_20), value);
	}

	inline static int32_t get_offset_of_elementNameStackPos_21() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___elementNameStackPos_21)); }
	inline int32_t get_elementNameStackPos_21() const { return ___elementNameStackPos_21; }
	inline int32_t* get_address_of_elementNameStackPos_21() { return &___elementNameStackPos_21; }
	inline void set_elementNameStackPos_21(int32_t value)
	{
		___elementNameStackPos_21 = value;
	}

	inline static int32_t get_offset_of_allowMultipleRoot_22() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___allowMultipleRoot_22)); }
	inline bool get_allowMultipleRoot_22() const { return ___allowMultipleRoot_22; }
	inline bool* get_address_of_allowMultipleRoot_22() { return &___allowMultipleRoot_22; }
	inline void set_allowMultipleRoot_22(bool value)
	{
		___allowMultipleRoot_22 = value;
	}

	inline static int32_t get_offset_of_isStandalone_23() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___isStandalone_23)); }
	inline bool get_isStandalone_23() const { return ___isStandalone_23; }
	inline bool* get_address_of_isStandalone_23() { return &___isStandalone_23; }
	inline void set_isStandalone_23(bool value)
	{
		___isStandalone_23 = value;
	}

	inline static int32_t get_offset_of_returnEntityReference_24() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___returnEntityReference_24)); }
	inline bool get_returnEntityReference_24() const { return ___returnEntityReference_24; }
	inline bool* get_address_of_returnEntityReference_24() { return &___returnEntityReference_24; }
	inline void set_returnEntityReference_24(bool value)
	{
		___returnEntityReference_24 = value;
	}

	inline static int32_t get_offset_of_entityReferenceName_25() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___entityReferenceName_25)); }
	inline String_t* get_entityReferenceName_25() const { return ___entityReferenceName_25; }
	inline String_t** get_address_of_entityReferenceName_25() { return &___entityReferenceName_25; }
	inline void set_entityReferenceName_25(String_t* value)
	{
		___entityReferenceName_25 = value;
		Il2CppCodeGenWriteBarrier((&___entityReferenceName_25), value);
	}

	inline static int32_t get_offset_of_valueBuffer_26() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___valueBuffer_26)); }
	inline StringBuilder_t622404039 * get_valueBuffer_26() const { return ___valueBuffer_26; }
	inline StringBuilder_t622404039 ** get_address_of_valueBuffer_26() { return &___valueBuffer_26; }
	inline void set_valueBuffer_26(StringBuilder_t622404039 * value)
	{
		___valueBuffer_26 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_26), value);
	}

	inline static int32_t get_offset_of_reader_27() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___reader_27)); }
	inline TextReader_t218744201 * get_reader_27() const { return ___reader_27; }
	inline TextReader_t218744201 ** get_address_of_reader_27() { return &___reader_27; }
	inline void set_reader_27(TextReader_t218744201 * value)
	{
		___reader_27 = value;
		Il2CppCodeGenWriteBarrier((&___reader_27), value);
	}

	inline static int32_t get_offset_of_peekChars_28() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___peekChars_28)); }
	inline CharU5BU5D_t3419619864* get_peekChars_28() const { return ___peekChars_28; }
	inline CharU5BU5D_t3419619864** get_address_of_peekChars_28() { return &___peekChars_28; }
	inline void set_peekChars_28(CharU5BU5D_t3419619864* value)
	{
		___peekChars_28 = value;
		Il2CppCodeGenWriteBarrier((&___peekChars_28), value);
	}

	inline static int32_t get_offset_of_peekCharsIndex_29() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___peekCharsIndex_29)); }
	inline int32_t get_peekCharsIndex_29() const { return ___peekCharsIndex_29; }
	inline int32_t* get_address_of_peekCharsIndex_29() { return &___peekCharsIndex_29; }
	inline void set_peekCharsIndex_29(int32_t value)
	{
		___peekCharsIndex_29 = value;
	}

	inline static int32_t get_offset_of_peekCharsLength_30() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___peekCharsLength_30)); }
	inline int32_t get_peekCharsLength_30() const { return ___peekCharsLength_30; }
	inline int32_t* get_address_of_peekCharsLength_30() { return &___peekCharsLength_30; }
	inline void set_peekCharsLength_30(int32_t value)
	{
		___peekCharsLength_30 = value;
	}

	inline static int32_t get_offset_of_curNodePeekIndex_31() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___curNodePeekIndex_31)); }
	inline int32_t get_curNodePeekIndex_31() const { return ___curNodePeekIndex_31; }
	inline int32_t* get_address_of_curNodePeekIndex_31() { return &___curNodePeekIndex_31; }
	inline void set_curNodePeekIndex_31(int32_t value)
	{
		___curNodePeekIndex_31 = value;
	}

	inline static int32_t get_offset_of_preserveCurrentTag_32() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___preserveCurrentTag_32)); }
	inline bool get_preserveCurrentTag_32() const { return ___preserveCurrentTag_32; }
	inline bool* get_address_of_preserveCurrentTag_32() { return &___preserveCurrentTag_32; }
	inline void set_preserveCurrentTag_32(bool value)
	{
		___preserveCurrentTag_32 = value;
	}

	inline static int32_t get_offset_of_line_33() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___line_33)); }
	inline int32_t get_line_33() const { return ___line_33; }
	inline int32_t* get_address_of_line_33() { return &___line_33; }
	inline void set_line_33(int32_t value)
	{
		___line_33 = value;
	}

	inline static int32_t get_offset_of_column_34() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___column_34)); }
	inline int32_t get_column_34() const { return ___column_34; }
	inline int32_t* get_address_of_column_34() { return &___column_34; }
	inline void set_column_34(int32_t value)
	{
		___column_34 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_35() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentLinkedNodeLineNumber_35)); }
	inline int32_t get_currentLinkedNodeLineNumber_35() const { return ___currentLinkedNodeLineNumber_35; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_35() { return &___currentLinkedNodeLineNumber_35; }
	inline void set_currentLinkedNodeLineNumber_35(int32_t value)
	{
		___currentLinkedNodeLineNumber_35 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_36() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentLinkedNodeLinePosition_36)); }
	inline int32_t get_currentLinkedNodeLinePosition_36() const { return ___currentLinkedNodeLinePosition_36; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_36() { return &___currentLinkedNodeLinePosition_36; }
	inline void set_currentLinkedNodeLinePosition_36(int32_t value)
	{
		___currentLinkedNodeLinePosition_36 = value;
	}

	inline static int32_t get_offset_of_useProceedingLineInfo_37() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___useProceedingLineInfo_37)); }
	inline bool get_useProceedingLineInfo_37() const { return ___useProceedingLineInfo_37; }
	inline bool* get_address_of_useProceedingLineInfo_37() { return &___useProceedingLineInfo_37; }
	inline void set_useProceedingLineInfo_37(bool value)
	{
		___useProceedingLineInfo_37 = value;
	}

	inline static int32_t get_offset_of_startNodeType_38() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___startNodeType_38)); }
	inline int32_t get_startNodeType_38() const { return ___startNodeType_38; }
	inline int32_t* get_address_of_startNodeType_38() { return &___startNodeType_38; }
	inline void set_startNodeType_38(int32_t value)
	{
		___startNodeType_38 = value;
	}

	inline static int32_t get_offset_of_currentState_39() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___currentState_39)); }
	inline int32_t get_currentState_39() const { return ___currentState_39; }
	inline int32_t* get_address_of_currentState_39() { return &___currentState_39; }
	inline void set_currentState_39(int32_t value)
	{
		___currentState_39 = value;
	}

	inline static int32_t get_offset_of_nestLevel_40() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___nestLevel_40)); }
	inline int32_t get_nestLevel_40() const { return ___nestLevel_40; }
	inline int32_t* get_address_of_nestLevel_40() { return &___nestLevel_40; }
	inline void set_nestLevel_40(int32_t value)
	{
		___nestLevel_40 = value;
	}

	inline static int32_t get_offset_of_readCharsInProgress_41() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___readCharsInProgress_41)); }
	inline bool get_readCharsInProgress_41() const { return ___readCharsInProgress_41; }
	inline bool* get_address_of_readCharsInProgress_41() { return &___readCharsInProgress_41; }
	inline void set_readCharsInProgress_41(bool value)
	{
		___readCharsInProgress_41 = value;
	}

	inline static int32_t get_offset_of_binaryCharGetter_42() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___binaryCharGetter_42)); }
	inline CharGetter_t1293008504 * get_binaryCharGetter_42() const { return ___binaryCharGetter_42; }
	inline CharGetter_t1293008504 ** get_address_of_binaryCharGetter_42() { return &___binaryCharGetter_42; }
	inline void set_binaryCharGetter_42(CharGetter_t1293008504 * value)
	{
		___binaryCharGetter_42 = value;
		Il2CppCodeGenWriteBarrier((&___binaryCharGetter_42), value);
	}

	inline static int32_t get_offset_of_namespaces_43() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___namespaces_43)); }
	inline bool get_namespaces_43() const { return ___namespaces_43; }
	inline bool* get_address_of_namespaces_43() { return &___namespaces_43; }
	inline void set_namespaces_43(bool value)
	{
		___namespaces_43 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_44() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___whitespaceHandling_44)); }
	inline int32_t get_whitespaceHandling_44() const { return ___whitespaceHandling_44; }
	inline int32_t* get_address_of_whitespaceHandling_44() { return &___whitespaceHandling_44; }
	inline void set_whitespaceHandling_44(int32_t value)
	{
		___whitespaceHandling_44 = value;
	}

	inline static int32_t get_offset_of_resolver_45() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___resolver_45)); }
	inline XmlResolver_t161349657 * get_resolver_45() const { return ___resolver_45; }
	inline XmlResolver_t161349657 ** get_address_of_resolver_45() { return &___resolver_45; }
	inline void set_resolver_45(XmlResolver_t161349657 * value)
	{
		___resolver_45 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_45), value);
	}

	inline static int32_t get_offset_of_normalization_46() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___normalization_46)); }
	inline bool get_normalization_46() const { return ___normalization_46; }
	inline bool* get_address_of_normalization_46() { return &___normalization_46; }
	inline void set_normalization_46(bool value)
	{
		___normalization_46 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_47() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___checkCharacters_47)); }
	inline bool get_checkCharacters_47() const { return ___checkCharacters_47; }
	inline bool* get_address_of_checkCharacters_47() { return &___checkCharacters_47; }
	inline void set_checkCharacters_47(bool value)
	{
		___checkCharacters_47 = value;
	}

	inline static int32_t get_offset_of_prohibitDtd_48() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___prohibitDtd_48)); }
	inline bool get_prohibitDtd_48() const { return ___prohibitDtd_48; }
	inline bool* get_address_of_prohibitDtd_48() { return &___prohibitDtd_48; }
	inline void set_prohibitDtd_48(bool value)
	{
		___prohibitDtd_48 = value;
	}

	inline static int32_t get_offset_of_closeInput_49() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___closeInput_49)); }
	inline bool get_closeInput_49() const { return ___closeInput_49; }
	inline bool* get_address_of_closeInput_49() { return &___closeInput_49; }
	inline void set_closeInput_49(bool value)
	{
		___closeInput_49 = value;
	}

	inline static int32_t get_offset_of_entityHandling_50() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___entityHandling_50)); }
	inline int32_t get_entityHandling_50() const { return ___entityHandling_50; }
	inline int32_t* get_address_of_entityHandling_50() { return &___entityHandling_50; }
	inline void set_entityHandling_50(int32_t value)
	{
		___entityHandling_50 = value;
	}

	inline static int32_t get_offset_of_whitespacePool_51() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___whitespacePool_51)); }
	inline NameTable_t3507653384 * get_whitespacePool_51() const { return ___whitespacePool_51; }
	inline NameTable_t3507653384 ** get_address_of_whitespacePool_51() { return &___whitespacePool_51; }
	inline void set_whitespacePool_51(NameTable_t3507653384 * value)
	{
		___whitespacePool_51 = value;
		Il2CppCodeGenWriteBarrier((&___whitespacePool_51), value);
	}

	inline static int32_t get_offset_of_whitespaceCache_52() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___whitespaceCache_52)); }
	inline CharU5BU5D_t3419619864* get_whitespaceCache_52() const { return ___whitespaceCache_52; }
	inline CharU5BU5D_t3419619864** get_address_of_whitespaceCache_52() { return &___whitespaceCache_52; }
	inline void set_whitespaceCache_52(CharU5BU5D_t3419619864* value)
	{
		___whitespaceCache_52 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceCache_52), value);
	}

	inline static int32_t get_offset_of_stateStack_53() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823, ___stateStack_53)); }
	inline DtdInputStateStack_t2344161889 * get_stateStack_53() const { return ___stateStack_53; }
	inline DtdInputStateStack_t2344161889 ** get_address_of_stateStack_53() { return &___stateStack_53; }
	inline void set_stateStack_53(DtdInputStateStack_t2344161889 * value)
	{
		___stateStack_53 = value;
		Il2CppCodeGenWriteBarrier((&___stateStack_53), value);
	}
};

struct XmlTextReader_t3875484823_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switch$map51
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map51_54;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switch$map52
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map52_55;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map51_54() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823_StaticFields, ___U3CU3Ef__switchU24map51_54)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map51_54() const { return ___U3CU3Ef__switchU24map51_54; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map51_54() { return &___U3CU3Ef__switchU24map51_54; }
	inline void set_U3CU3Ef__switchU24map51_54(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map51_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map51_54), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map52_55() { return static_cast<int32_t>(offsetof(XmlTextReader_t3875484823_StaticFields, ___U3CU3Ef__switchU24map52_55)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map52_55() const { return ___U3CU3Ef__switchU24map52_55; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map52_55() { return &___U3CU3Ef__switchU24map52_55; }
	inline void set_U3CU3Ef__switchU24map52_55(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map52_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map52_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T3875484823_H
#ifndef XMLTOKENINFO_T2043079912_H
#define XMLTOKENINFO_T2043079912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct  XmlTokenInfo_t2043079912  : public RuntimeObject
{
public:
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::valueCache
	String_t* ___valueCache_0;
	// Mono.Xml2.XmlTextReader Mono.Xml2.XmlTextReader/XmlTokenInfo::Reader
	XmlTextReader_t3875484823 * ___Reader_1;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::Name
	String_t* ___Name_2;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::LocalName
	String_t* ___LocalName_3;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::Prefix
	String_t* ___Prefix_4;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::NamespaceURI
	String_t* ___NamespaceURI_5;
	// System.Boolean Mono.Xml2.XmlTextReader/XmlTokenInfo::IsEmptyElement
	bool ___IsEmptyElement_6;
	// System.Char Mono.Xml2.XmlTextReader/XmlTokenInfo::QuoteChar
	Il2CppChar ___QuoteChar_7;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::LineNumber
	int32_t ___LineNumber_8;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::LinePosition
	int32_t ___LinePosition_9;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::ValueBufferStart
	int32_t ___ValueBufferStart_10;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::ValueBufferEnd
	int32_t ___ValueBufferEnd_11;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader/XmlTokenInfo::NodeType
	int32_t ___NodeType_12;

public:
	inline static int32_t get_offset_of_valueCache_0() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___valueCache_0)); }
	inline String_t* get_valueCache_0() const { return ___valueCache_0; }
	inline String_t** get_address_of_valueCache_0() { return &___valueCache_0; }
	inline void set_valueCache_0(String_t* value)
	{
		___valueCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_0), value);
	}

	inline static int32_t get_offset_of_Reader_1() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___Reader_1)); }
	inline XmlTextReader_t3875484823 * get_Reader_1() const { return ___Reader_1; }
	inline XmlTextReader_t3875484823 ** get_address_of_Reader_1() { return &___Reader_1; }
	inline void set_Reader_1(XmlTextReader_t3875484823 * value)
	{
		___Reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___Reader_1), value);
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_LocalName_3() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___LocalName_3)); }
	inline String_t* get_LocalName_3() const { return ___LocalName_3; }
	inline String_t** get_address_of_LocalName_3() { return &___LocalName_3; }
	inline void set_LocalName_3(String_t* value)
	{
		___LocalName_3 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_3), value);
	}

	inline static int32_t get_offset_of_Prefix_4() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___Prefix_4)); }
	inline String_t* get_Prefix_4() const { return ___Prefix_4; }
	inline String_t** get_address_of_Prefix_4() { return &___Prefix_4; }
	inline void set_Prefix_4(String_t* value)
	{
		___Prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_4), value);
	}

	inline static int32_t get_offset_of_NamespaceURI_5() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___NamespaceURI_5)); }
	inline String_t* get_NamespaceURI_5() const { return ___NamespaceURI_5; }
	inline String_t** get_address_of_NamespaceURI_5() { return &___NamespaceURI_5; }
	inline void set_NamespaceURI_5(String_t* value)
	{
		___NamespaceURI_5 = value;
		Il2CppCodeGenWriteBarrier((&___NamespaceURI_5), value);
	}

	inline static int32_t get_offset_of_IsEmptyElement_6() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___IsEmptyElement_6)); }
	inline bool get_IsEmptyElement_6() const { return ___IsEmptyElement_6; }
	inline bool* get_address_of_IsEmptyElement_6() { return &___IsEmptyElement_6; }
	inline void set_IsEmptyElement_6(bool value)
	{
		___IsEmptyElement_6 = value;
	}

	inline static int32_t get_offset_of_QuoteChar_7() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___QuoteChar_7)); }
	inline Il2CppChar get_QuoteChar_7() const { return ___QuoteChar_7; }
	inline Il2CppChar* get_address_of_QuoteChar_7() { return &___QuoteChar_7; }
	inline void set_QuoteChar_7(Il2CppChar value)
	{
		___QuoteChar_7 = value;
	}

	inline static int32_t get_offset_of_LineNumber_8() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___LineNumber_8)); }
	inline int32_t get_LineNumber_8() const { return ___LineNumber_8; }
	inline int32_t* get_address_of_LineNumber_8() { return &___LineNumber_8; }
	inline void set_LineNumber_8(int32_t value)
	{
		___LineNumber_8 = value;
	}

	inline static int32_t get_offset_of_LinePosition_9() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___LinePosition_9)); }
	inline int32_t get_LinePosition_9() const { return ___LinePosition_9; }
	inline int32_t* get_address_of_LinePosition_9() { return &___LinePosition_9; }
	inline void set_LinePosition_9(int32_t value)
	{
		___LinePosition_9 = value;
	}

	inline static int32_t get_offset_of_ValueBufferStart_10() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___ValueBufferStart_10)); }
	inline int32_t get_ValueBufferStart_10() const { return ___ValueBufferStart_10; }
	inline int32_t* get_address_of_ValueBufferStart_10() { return &___ValueBufferStart_10; }
	inline void set_ValueBufferStart_10(int32_t value)
	{
		___ValueBufferStart_10 = value;
	}

	inline static int32_t get_offset_of_ValueBufferEnd_11() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___ValueBufferEnd_11)); }
	inline int32_t get_ValueBufferEnd_11() const { return ___ValueBufferEnd_11; }
	inline int32_t* get_address_of_ValueBufferEnd_11() { return &___ValueBufferEnd_11; }
	inline void set_ValueBufferEnd_11(int32_t value)
	{
		___ValueBufferEnd_11 = value;
	}

	inline static int32_t get_offset_of_NodeType_12() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2043079912, ___NodeType_12)); }
	inline int32_t get_NodeType_12() const { return ___NodeType_12; }
	inline int32_t* get_address_of_NodeType_12() { return &___NodeType_12; }
	inline void set_NodeType_12(int32_t value)
	{
		___NodeType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENINFO_T2043079912_H
#ifndef XMLTEXT_T1994551155_H
#define XMLTEXT_T1994551155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlText
struct  XmlText_t1994551155  : public XmlCharacterData_t4217070933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXT_T1994551155_H
#ifndef TIMELINECLIP_T2489983630_H
#define TIMELINECLIP_T2489983630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip
struct  TimelineClip_t2489983630  : public RuntimeObject
{
public:
	// System.Double UnityEngine.Timeline.TimelineClip::m_Start
	double ___m_Start_6;
	// System.Double UnityEngine.Timeline.TimelineClip::m_ClipIn
	double ___m_ClipIn_7;
	// UnityEngine.Object UnityEngine.Timeline.TimelineClip::m_Asset
	Object_t692178351 * ___m_Asset_8;
	// UnityEngine.Object UnityEngine.Timeline.TimelineClip::m_UnderlyingAsset
	Object_t692178351 * ___m_UnderlyingAsset_9;
	// System.Double UnityEngine.Timeline.TimelineClip::m_Duration
	double ___m_Duration_10;
	// System.Double UnityEngine.Timeline.TimelineClip::m_TimeScale
	double ___m_TimeScale_11;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineClip::m_ParentTrack
	TrackAsset_t2823685959 * ___m_ParentTrack_12;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseInDuration
	double ___m_EaseInDuration_13;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseOutDuration
	double ___m_EaseOutDuration_14;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendInDuration
	double ___m_BlendInDuration_15;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendOutDuration
	double ___m_BlendOutDuration_16;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixInCurve
	AnimationCurve_t2757045165 * ___m_MixInCurve_17;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixOutCurve
	AnimationCurve_t2757045165 * ___m_MixOutCurve_18;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendInCurveMode
	int32_t ___m_BlendInCurveMode_19;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendOutCurveMode
	int32_t ___m_BlendOutCurveMode_20;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Timeline.TimelineClip::m_ExposedParameterNames
	List_1_t4069179741 * ___m_ExposedParameterNames_21;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TimelineClip::m_AnimationCurves
	AnimationClip_t1145879031 * ___m_AnimationCurves_22;
	// System.Boolean UnityEngine.Timeline.TimelineClip::m_Recordable
	bool ___m_Recordable_23;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PostExtrapolationMode
	int32_t ___m_PostExtrapolationMode_24;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PreExtrapolationMode
	int32_t ___m_PreExtrapolationMode_25;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PostExtrapolationTime
	double ___m_PostExtrapolationTime_26;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PreExtrapolationTime
	double ___m_PreExtrapolationTime_27;
	// System.String UnityEngine.Timeline.TimelineClip::m_DisplayName
	String_t* ___m_DisplayName_28;
	// System.Int32 UnityEngine.Timeline.TimelineClip::<dirtyHash>k__BackingField
	int32_t ___U3CdirtyHashU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_m_Start_6() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_Start_6)); }
	inline double get_m_Start_6() const { return ___m_Start_6; }
	inline double* get_address_of_m_Start_6() { return &___m_Start_6; }
	inline void set_m_Start_6(double value)
	{
		___m_Start_6 = value;
	}

	inline static int32_t get_offset_of_m_ClipIn_7() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_ClipIn_7)); }
	inline double get_m_ClipIn_7() const { return ___m_ClipIn_7; }
	inline double* get_address_of_m_ClipIn_7() { return &___m_ClipIn_7; }
	inline void set_m_ClipIn_7(double value)
	{
		___m_ClipIn_7 = value;
	}

	inline static int32_t get_offset_of_m_Asset_8() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_Asset_8)); }
	inline Object_t692178351 * get_m_Asset_8() const { return ___m_Asset_8; }
	inline Object_t692178351 ** get_address_of_m_Asset_8() { return &___m_Asset_8; }
	inline void set_m_Asset_8(Object_t692178351 * value)
	{
		___m_Asset_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Asset_8), value);
	}

	inline static int32_t get_offset_of_m_UnderlyingAsset_9() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_UnderlyingAsset_9)); }
	inline Object_t692178351 * get_m_UnderlyingAsset_9() const { return ___m_UnderlyingAsset_9; }
	inline Object_t692178351 ** get_address_of_m_UnderlyingAsset_9() { return &___m_UnderlyingAsset_9; }
	inline void set_m_UnderlyingAsset_9(Object_t692178351 * value)
	{
		___m_UnderlyingAsset_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnderlyingAsset_9), value);
	}

	inline static int32_t get_offset_of_m_Duration_10() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_Duration_10)); }
	inline double get_m_Duration_10() const { return ___m_Duration_10; }
	inline double* get_address_of_m_Duration_10() { return &___m_Duration_10; }
	inline void set_m_Duration_10(double value)
	{
		___m_Duration_10 = value;
	}

	inline static int32_t get_offset_of_m_TimeScale_11() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_TimeScale_11)); }
	inline double get_m_TimeScale_11() const { return ___m_TimeScale_11; }
	inline double* get_address_of_m_TimeScale_11() { return &___m_TimeScale_11; }
	inline void set_m_TimeScale_11(double value)
	{
		___m_TimeScale_11 = value;
	}

	inline static int32_t get_offset_of_m_ParentTrack_12() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_ParentTrack_12)); }
	inline TrackAsset_t2823685959 * get_m_ParentTrack_12() const { return ___m_ParentTrack_12; }
	inline TrackAsset_t2823685959 ** get_address_of_m_ParentTrack_12() { return &___m_ParentTrack_12; }
	inline void set_m_ParentTrack_12(TrackAsset_t2823685959 * value)
	{
		___m_ParentTrack_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTrack_12), value);
	}

	inline static int32_t get_offset_of_m_EaseInDuration_13() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_EaseInDuration_13)); }
	inline double get_m_EaseInDuration_13() const { return ___m_EaseInDuration_13; }
	inline double* get_address_of_m_EaseInDuration_13() { return &___m_EaseInDuration_13; }
	inline void set_m_EaseInDuration_13(double value)
	{
		___m_EaseInDuration_13 = value;
	}

	inline static int32_t get_offset_of_m_EaseOutDuration_14() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_EaseOutDuration_14)); }
	inline double get_m_EaseOutDuration_14() const { return ___m_EaseOutDuration_14; }
	inline double* get_address_of_m_EaseOutDuration_14() { return &___m_EaseOutDuration_14; }
	inline void set_m_EaseOutDuration_14(double value)
	{
		___m_EaseOutDuration_14 = value;
	}

	inline static int32_t get_offset_of_m_BlendInDuration_15() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_BlendInDuration_15)); }
	inline double get_m_BlendInDuration_15() const { return ___m_BlendInDuration_15; }
	inline double* get_address_of_m_BlendInDuration_15() { return &___m_BlendInDuration_15; }
	inline void set_m_BlendInDuration_15(double value)
	{
		___m_BlendInDuration_15 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutDuration_16() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_BlendOutDuration_16)); }
	inline double get_m_BlendOutDuration_16() const { return ___m_BlendOutDuration_16; }
	inline double* get_address_of_m_BlendOutDuration_16() { return &___m_BlendOutDuration_16; }
	inline void set_m_BlendOutDuration_16(double value)
	{
		___m_BlendOutDuration_16 = value;
	}

	inline static int32_t get_offset_of_m_MixInCurve_17() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_MixInCurve_17)); }
	inline AnimationCurve_t2757045165 * get_m_MixInCurve_17() const { return ___m_MixInCurve_17; }
	inline AnimationCurve_t2757045165 ** get_address_of_m_MixInCurve_17() { return &___m_MixInCurve_17; }
	inline void set_m_MixInCurve_17(AnimationCurve_t2757045165 * value)
	{
		___m_MixInCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixInCurve_17), value);
	}

	inline static int32_t get_offset_of_m_MixOutCurve_18() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_MixOutCurve_18)); }
	inline AnimationCurve_t2757045165 * get_m_MixOutCurve_18() const { return ___m_MixOutCurve_18; }
	inline AnimationCurve_t2757045165 ** get_address_of_m_MixOutCurve_18() { return &___m_MixOutCurve_18; }
	inline void set_m_MixOutCurve_18(AnimationCurve_t2757045165 * value)
	{
		___m_MixOutCurve_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixOutCurve_18), value);
	}

	inline static int32_t get_offset_of_m_BlendInCurveMode_19() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_BlendInCurveMode_19)); }
	inline int32_t get_m_BlendInCurveMode_19() const { return ___m_BlendInCurveMode_19; }
	inline int32_t* get_address_of_m_BlendInCurveMode_19() { return &___m_BlendInCurveMode_19; }
	inline void set_m_BlendInCurveMode_19(int32_t value)
	{
		___m_BlendInCurveMode_19 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutCurveMode_20() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_BlendOutCurveMode_20)); }
	inline int32_t get_m_BlendOutCurveMode_20() const { return ___m_BlendOutCurveMode_20; }
	inline int32_t* get_address_of_m_BlendOutCurveMode_20() { return &___m_BlendOutCurveMode_20; }
	inline void set_m_BlendOutCurveMode_20(int32_t value)
	{
		___m_BlendOutCurveMode_20 = value;
	}

	inline static int32_t get_offset_of_m_ExposedParameterNames_21() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_ExposedParameterNames_21)); }
	inline List_1_t4069179741 * get_m_ExposedParameterNames_21() const { return ___m_ExposedParameterNames_21; }
	inline List_1_t4069179741 ** get_address_of_m_ExposedParameterNames_21() { return &___m_ExposedParameterNames_21; }
	inline void set_m_ExposedParameterNames_21(List_1_t4069179741 * value)
	{
		___m_ExposedParameterNames_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExposedParameterNames_21), value);
	}

	inline static int32_t get_offset_of_m_AnimationCurves_22() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_AnimationCurves_22)); }
	inline AnimationClip_t1145879031 * get_m_AnimationCurves_22() const { return ___m_AnimationCurves_22; }
	inline AnimationClip_t1145879031 ** get_address_of_m_AnimationCurves_22() { return &___m_AnimationCurves_22; }
	inline void set_m_AnimationCurves_22(AnimationClip_t1145879031 * value)
	{
		___m_AnimationCurves_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationCurves_22), value);
	}

	inline static int32_t get_offset_of_m_Recordable_23() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_Recordable_23)); }
	inline bool get_m_Recordable_23() const { return ___m_Recordable_23; }
	inline bool* get_address_of_m_Recordable_23() { return &___m_Recordable_23; }
	inline void set_m_Recordable_23(bool value)
	{
		___m_Recordable_23 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationMode_24() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_PostExtrapolationMode_24)); }
	inline int32_t get_m_PostExtrapolationMode_24() const { return ___m_PostExtrapolationMode_24; }
	inline int32_t* get_address_of_m_PostExtrapolationMode_24() { return &___m_PostExtrapolationMode_24; }
	inline void set_m_PostExtrapolationMode_24(int32_t value)
	{
		___m_PostExtrapolationMode_24 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationMode_25() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_PreExtrapolationMode_25)); }
	inline int32_t get_m_PreExtrapolationMode_25() const { return ___m_PreExtrapolationMode_25; }
	inline int32_t* get_address_of_m_PreExtrapolationMode_25() { return &___m_PreExtrapolationMode_25; }
	inline void set_m_PreExtrapolationMode_25(int32_t value)
	{
		___m_PreExtrapolationMode_25 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationTime_26() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_PostExtrapolationTime_26)); }
	inline double get_m_PostExtrapolationTime_26() const { return ___m_PostExtrapolationTime_26; }
	inline double* get_address_of_m_PostExtrapolationTime_26() { return &___m_PostExtrapolationTime_26; }
	inline void set_m_PostExtrapolationTime_26(double value)
	{
		___m_PostExtrapolationTime_26 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationTime_27() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_PreExtrapolationTime_27)); }
	inline double get_m_PreExtrapolationTime_27() const { return ___m_PreExtrapolationTime_27; }
	inline double* get_address_of_m_PreExtrapolationTime_27() { return &___m_PreExtrapolationTime_27; }
	inline void set_m_PreExtrapolationTime_27(double value)
	{
		___m_PreExtrapolationTime_27 = value;
	}

	inline static int32_t get_offset_of_m_DisplayName_28() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___m_DisplayName_28)); }
	inline String_t* get_m_DisplayName_28() const { return ___m_DisplayName_28; }
	inline String_t** get_address_of_m_DisplayName_28() { return &___m_DisplayName_28; }
	inline void set_m_DisplayName_28(String_t* value)
	{
		___m_DisplayName_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisplayName_28), value);
	}

	inline static int32_t get_offset_of_U3CdirtyHashU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630, ___U3CdirtyHashU3Ek__BackingField_29)); }
	inline int32_t get_U3CdirtyHashU3Ek__BackingField_29() const { return ___U3CdirtyHashU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CdirtyHashU3Ek__BackingField_29() { return &___U3CdirtyHashU3Ek__BackingField_29; }
	inline void set_U3CdirtyHashU3Ek__BackingField_29(int32_t value)
	{
		___U3CdirtyHashU3Ek__BackingField_29 = value;
	}
};

struct TimelineClip_t2489983630_StaticFields
{
public:
	// UnityEngine.Timeline.ClipCaps UnityEngine.Timeline.TimelineClip::kDefaultClipCaps
	int32_t ___kDefaultClipCaps_0;
	// System.Single UnityEngine.Timeline.TimelineClip::kDefaultClipDurationInSeconds
	float ___kDefaultClipDurationInSeconds_1;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMin
	double ___kTimeScaleMin_2;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMax
	double ___kTimeScaleMax_3;
	// System.Double UnityEngine.Timeline.TimelineClip::kMinDuration
	double ___kMinDuration_4;
	// System.Double UnityEngine.Timeline.TimelineClip::kMaxTimeValue
	double ___kMaxTimeValue_5;

public:
	inline static int32_t get_offset_of_kDefaultClipCaps_0() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kDefaultClipCaps_0)); }
	inline int32_t get_kDefaultClipCaps_0() const { return ___kDefaultClipCaps_0; }
	inline int32_t* get_address_of_kDefaultClipCaps_0() { return &___kDefaultClipCaps_0; }
	inline void set_kDefaultClipCaps_0(int32_t value)
	{
		___kDefaultClipCaps_0 = value;
	}

	inline static int32_t get_offset_of_kDefaultClipDurationInSeconds_1() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kDefaultClipDurationInSeconds_1)); }
	inline float get_kDefaultClipDurationInSeconds_1() const { return ___kDefaultClipDurationInSeconds_1; }
	inline float* get_address_of_kDefaultClipDurationInSeconds_1() { return &___kDefaultClipDurationInSeconds_1; }
	inline void set_kDefaultClipDurationInSeconds_1(float value)
	{
		___kDefaultClipDurationInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMin_2() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kTimeScaleMin_2)); }
	inline double get_kTimeScaleMin_2() const { return ___kTimeScaleMin_2; }
	inline double* get_address_of_kTimeScaleMin_2() { return &___kTimeScaleMin_2; }
	inline void set_kTimeScaleMin_2(double value)
	{
		___kTimeScaleMin_2 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMax_3() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kTimeScaleMax_3)); }
	inline double get_kTimeScaleMax_3() const { return ___kTimeScaleMax_3; }
	inline double* get_address_of_kTimeScaleMax_3() { return &___kTimeScaleMax_3; }
	inline void set_kTimeScaleMax_3(double value)
	{
		___kTimeScaleMax_3 = value;
	}

	inline static int32_t get_offset_of_kMinDuration_4() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kMinDuration_4)); }
	inline double get_kMinDuration_4() const { return ___kMinDuration_4; }
	inline double* get_address_of_kMinDuration_4() { return &___kMinDuration_4; }
	inline void set_kMinDuration_4(double value)
	{
		___kMinDuration_4 = value;
	}

	inline static int32_t get_offset_of_kMaxTimeValue_5() { return static_cast<int32_t>(offsetof(TimelineClip_t2489983630_StaticFields, ___kMaxTimeValue_5)); }
	inline double get_kMaxTimeValue_5() const { return ___kMaxTimeValue_5; }
	inline double* get_address_of_kMaxTimeValue_5() { return &___kMaxTimeValue_5; }
	inline void set_kMaxTimeValue_5(double value)
	{
		___kMaxTimeValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIP_T2489983630_H
#ifndef XMLREADERBINARYSUPPORT_T2320539231_H
#define XMLREADERBINARYSUPPORT_T2320539231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport
struct  XmlReaderBinarySupport_t2320539231  : public RuntimeObject
{
public:
	// System.Xml.XmlReader System.Xml.XmlReaderBinarySupport::reader
	XmlReader_t1082502433 * ___reader_0;
	// System.Int32 System.Xml.XmlReaderBinarySupport::base64CacheStartsAt
	int32_t ___base64CacheStartsAt_1;
	// System.Xml.XmlReaderBinarySupport/CommandState System.Xml.XmlReaderBinarySupport::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlReaderBinarySupport::hasCache
	bool ___hasCache_3;
	// System.Boolean System.Xml.XmlReaderBinarySupport::dontReset
	bool ___dontReset_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t2320539231, ___reader_0)); }
	inline XmlReader_t1082502433 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t1082502433 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t1082502433 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_base64CacheStartsAt_1() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t2320539231, ___base64CacheStartsAt_1)); }
	inline int32_t get_base64CacheStartsAt_1() const { return ___base64CacheStartsAt_1; }
	inline int32_t* get_address_of_base64CacheStartsAt_1() { return &___base64CacheStartsAt_1; }
	inline void set_base64CacheStartsAt_1(int32_t value)
	{
		___base64CacheStartsAt_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t2320539231, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_hasCache_3() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t2320539231, ___hasCache_3)); }
	inline bool get_hasCache_3() const { return ___hasCache_3; }
	inline bool* get_address_of_hasCache_3() { return &___hasCache_3; }
	inline void set_hasCache_3(bool value)
	{
		___hasCache_3 = value;
	}

	inline static int32_t get_offset_of_dontReset_4() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t2320539231, ___dontReset_4)); }
	inline bool get_dontReset_4() const { return ___dontReset_4; }
	inline bool* get_address_of_dontReset_4() { return &___dontReset_4; }
	inline void set_dontReset_4(bool value)
	{
		___dontReset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERBINARYSUPPORT_T2320539231_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef XMLNODECHANGEDEVENTARGS_T2915371093_H
#define XMLNODECHANGEDEVENTARGS_T2915371093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t2915371093  : public EventArgs_t2659587769
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_oldParent
	XmlNode_t3729859639 * ____oldParent_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_newParent
	XmlNode_t3729859639 * ____newParent_2;
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::_action
	int32_t ____action_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_node
	XmlNode_t3729859639 * ____node_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::_oldValue
	String_t* ____oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::_newValue
	String_t* ____newValue_6;

public:
	inline static int32_t get_offset_of__oldParent_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____oldParent_1)); }
	inline XmlNode_t3729859639 * get__oldParent_1() const { return ____oldParent_1; }
	inline XmlNode_t3729859639 ** get_address_of__oldParent_1() { return &____oldParent_1; }
	inline void set__oldParent_1(XmlNode_t3729859639 * value)
	{
		____oldParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____oldParent_1), value);
	}

	inline static int32_t get_offset_of__newParent_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____newParent_2)); }
	inline XmlNode_t3729859639 * get__newParent_2() const { return ____newParent_2; }
	inline XmlNode_t3729859639 ** get_address_of__newParent_2() { return &____newParent_2; }
	inline void set__newParent_2(XmlNode_t3729859639 * value)
	{
		____newParent_2 = value;
		Il2CppCodeGenWriteBarrier((&____newParent_2), value);
	}

	inline static int32_t get_offset_of__action_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____action_3)); }
	inline int32_t get__action_3() const { return ____action_3; }
	inline int32_t* get_address_of__action_3() { return &____action_3; }
	inline void set__action_3(int32_t value)
	{
		____action_3 = value;
	}

	inline static int32_t get_offset_of__node_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____node_4)); }
	inline XmlNode_t3729859639 * get__node_4() const { return ____node_4; }
	inline XmlNode_t3729859639 ** get_address_of__node_4() { return &____node_4; }
	inline void set__node_4(XmlNode_t3729859639 * value)
	{
		____node_4 = value;
		Il2CppCodeGenWriteBarrier((&____node_4), value);
	}

	inline static int32_t get_offset_of__oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____oldValue_5)); }
	inline String_t* get__oldValue_5() const { return ____oldValue_5; }
	inline String_t** get_address_of__oldValue_5() { return &____oldValue_5; }
	inline void set__oldValue_5(String_t* value)
	{
		____oldValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____oldValue_5), value);
	}

	inline static int32_t get_offset_of__newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2915371093, ____newValue_6)); }
	inline String_t* get__newValue_6() const { return ____newValue_6; }
	inline String_t** get_address_of__newValue_6() { return &____newValue_6; }
	inline void set__newValue_6(String_t* value)
	{
		____newValue_6 = value;
		Il2CppCodeGenWriteBarrier((&____newValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTARGS_T2915371093_H
#ifndef TRACKMEDIATYPE_T224885088_H
#define TRACKMEDIATYPE_T224885088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackMediaType
struct  TrackMediaType_t224885088  : public Attribute_t1924466020
{
public:
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackMediaType::m_MediaType
	int32_t ___m_MediaType_0;

public:
	inline static int32_t get_offset_of_m_MediaType_0() { return static_cast<int32_t>(offsetof(TrackMediaType_t224885088, ___m_MediaType_0)); }
	inline int32_t get_m_MediaType_0() const { return ___m_MediaType_0; }
	inline int32_t* get_address_of_m_MediaType_0() { return &___m_MediaType_0; }
	inline void set_m_MediaType_0(int32_t value)
	{
		___m_MediaType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKMEDIATYPE_T224885088_H
#ifndef ACTIVATIONMIXERPLAYABLE_T1415536146_H
#define ACTIVATIONMIXERPLAYABLE_T1415536146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationMixerPlayable
struct  ActivationMixerPlayable_t1415536146  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationMixerPlayable::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_0;
	// System.Boolean UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObjectInitialStateIsActive
	bool ___m_BoundGameObjectInitialStateIsActive_1;
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObject
	GameObject_t2557347079 * ___m_BoundGameObject_2;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_0() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t1415536146, ___m_PostPlaybackState_0)); }
	inline int32_t get_m_PostPlaybackState_0() const { return ___m_PostPlaybackState_0; }
	inline int32_t* get_address_of_m_PostPlaybackState_0() { return &___m_PostPlaybackState_0; }
	inline void set_m_PostPlaybackState_0(int32_t value)
	{
		___m_PostPlaybackState_0 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObjectInitialStateIsActive_1() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t1415536146, ___m_BoundGameObjectInitialStateIsActive_1)); }
	inline bool get_m_BoundGameObjectInitialStateIsActive_1() const { return ___m_BoundGameObjectInitialStateIsActive_1; }
	inline bool* get_address_of_m_BoundGameObjectInitialStateIsActive_1() { return &___m_BoundGameObjectInitialStateIsActive_1; }
	inline void set_m_BoundGameObjectInitialStateIsActive_1(bool value)
	{
		___m_BoundGameObjectInitialStateIsActive_1 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObject_2() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t1415536146, ___m_BoundGameObject_2)); }
	inline GameObject_t2557347079 * get_m_BoundGameObject_2() const { return ___m_BoundGameObject_2; }
	inline GameObject_t2557347079 ** get_address_of_m_BoundGameObject_2() { return &___m_BoundGameObject_2; }
	inline void set_m_BoundGameObject_2(GameObject_t2557347079 * value)
	{
		___m_BoundGameObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundGameObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONMIXERPLAYABLE_T1415536146_H
#ifndef XMLCONVERT_T1639932650_H
#define XMLCONVERT_T1639932650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1639932650  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1639932650_StaticFields
{
public:
	// System.String[] System.Xml.XmlConvert::datetimeFormats
	StringU5BU5D_t2511808107* ___datetimeFormats_0;
	// System.String[] System.Xml.XmlConvert::defaultDateTimeFormats
	StringU5BU5D_t2511808107* ___defaultDateTimeFormats_1;
	// System.String[] System.Xml.XmlConvert::roundtripDateTimeFormats
	StringU5BU5D_t2511808107* ___roundtripDateTimeFormats_2;
	// System.String[] System.Xml.XmlConvert::localDateTimeFormats
	StringU5BU5D_t2511808107* ___localDateTimeFormats_3;
	// System.String[] System.Xml.XmlConvert::utcDateTimeFormats
	StringU5BU5D_t2511808107* ___utcDateTimeFormats_4;
	// System.String[] System.Xml.XmlConvert::unspecifiedDateTimeFormats
	StringU5BU5D_t2511808107* ___unspecifiedDateTimeFormats_5;
	// System.Globalization.DateTimeStyles System.Xml.XmlConvert::_defaultStyle
	int32_t ____defaultStyle_6;

public:
	inline static int32_t get_offset_of_datetimeFormats_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___datetimeFormats_0)); }
	inline StringU5BU5D_t2511808107* get_datetimeFormats_0() const { return ___datetimeFormats_0; }
	inline StringU5BU5D_t2511808107** get_address_of_datetimeFormats_0() { return &___datetimeFormats_0; }
	inline void set_datetimeFormats_0(StringU5BU5D_t2511808107* value)
	{
		___datetimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___datetimeFormats_0), value);
	}

	inline static int32_t get_offset_of_defaultDateTimeFormats_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___defaultDateTimeFormats_1)); }
	inline StringU5BU5D_t2511808107* get_defaultDateTimeFormats_1() const { return ___defaultDateTimeFormats_1; }
	inline StringU5BU5D_t2511808107** get_address_of_defaultDateTimeFormats_1() { return &___defaultDateTimeFormats_1; }
	inline void set_defaultDateTimeFormats_1(StringU5BU5D_t2511808107* value)
	{
		___defaultDateTimeFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDateTimeFormats_1), value);
	}

	inline static int32_t get_offset_of_roundtripDateTimeFormats_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___roundtripDateTimeFormats_2)); }
	inline StringU5BU5D_t2511808107* get_roundtripDateTimeFormats_2() const { return ___roundtripDateTimeFormats_2; }
	inline StringU5BU5D_t2511808107** get_address_of_roundtripDateTimeFormats_2() { return &___roundtripDateTimeFormats_2; }
	inline void set_roundtripDateTimeFormats_2(StringU5BU5D_t2511808107* value)
	{
		___roundtripDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundtripDateTimeFormats_2), value);
	}

	inline static int32_t get_offset_of_localDateTimeFormats_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___localDateTimeFormats_3)); }
	inline StringU5BU5D_t2511808107* get_localDateTimeFormats_3() const { return ___localDateTimeFormats_3; }
	inline StringU5BU5D_t2511808107** get_address_of_localDateTimeFormats_3() { return &___localDateTimeFormats_3; }
	inline void set_localDateTimeFormats_3(StringU5BU5D_t2511808107* value)
	{
		___localDateTimeFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___localDateTimeFormats_3), value);
	}

	inline static int32_t get_offset_of_utcDateTimeFormats_4() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___utcDateTimeFormats_4)); }
	inline StringU5BU5D_t2511808107* get_utcDateTimeFormats_4() const { return ___utcDateTimeFormats_4; }
	inline StringU5BU5D_t2511808107** get_address_of_utcDateTimeFormats_4() { return &___utcDateTimeFormats_4; }
	inline void set_utcDateTimeFormats_4(StringU5BU5D_t2511808107* value)
	{
		___utcDateTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___utcDateTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_unspecifiedDateTimeFormats_5() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ___unspecifiedDateTimeFormats_5)); }
	inline StringU5BU5D_t2511808107* get_unspecifiedDateTimeFormats_5() const { return ___unspecifiedDateTimeFormats_5; }
	inline StringU5BU5D_t2511808107** get_address_of_unspecifiedDateTimeFormats_5() { return &___unspecifiedDateTimeFormats_5; }
	inline void set_unspecifiedDateTimeFormats_5(StringU5BU5D_t2511808107* value)
	{
		___unspecifiedDateTimeFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedDateTimeFormats_5), value);
	}

	inline static int32_t get_offset_of__defaultStyle_6() { return static_cast<int32_t>(offsetof(XmlConvert_t1639932650_StaticFields, ____defaultStyle_6)); }
	inline int32_t get__defaultStyle_6() const { return ____defaultStyle_6; }
	inline int32_t* get_address_of__defaultStyle_6() { return &____defaultStyle_6; }
	inline void set__defaultStyle_6(int32_t value)
	{
		____defaultStyle_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1639932650_H
#ifndef CONTEXTITEM_T1914052158_H
#define CONTEXTITEM_T1914052158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext/ContextItem
struct  ContextItem_t1914052158  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext/ContextItem::BaseURI
	String_t* ___BaseURI_0;
	// System.String System.Xml.XmlParserContext/ContextItem::XmlLang
	String_t* ___XmlLang_1;
	// System.Xml.XmlSpace System.Xml.XmlParserContext/ContextItem::XmlSpace
	int32_t ___XmlSpace_2;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(ContextItem_t1914052158, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_XmlLang_1() { return static_cast<int32_t>(offsetof(ContextItem_t1914052158, ___XmlLang_1)); }
	inline String_t* get_XmlLang_1() const { return ___XmlLang_1; }
	inline String_t** get_address_of_XmlLang_1() { return &___XmlLang_1; }
	inline void set_XmlLang_1(String_t* value)
	{
		___XmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_1), value);
	}

	inline static int32_t get_offset_of_XmlSpace_2() { return static_cast<int32_t>(offsetof(ContextItem_t1914052158, ___XmlSpace_2)); }
	inline int32_t get_XmlSpace_2() const { return ___XmlSpace_2; }
	inline int32_t* get_address_of_XmlSpace_2() { return &___XmlSpace_2; }
	inline void set_XmlSpace_2(int32_t value)
	{
		___XmlSpace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTITEM_T1914052158_H
#ifndef XMLPARSERCONTEXT_T3802687902_H
#define XMLPARSERCONTEXT_T3802687902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_t3802687902  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext::baseURI
	String_t* ___baseURI_0;
	// System.String System.Xml.XmlParserContext::docTypeName
	String_t* ___docTypeName_1;
	// System.Text.Encoding System.Xml.XmlParserContext::encoding
	Encoding_t4004889433 * ___encoding_2;
	// System.String System.Xml.XmlParserContext::internalSubset
	String_t* ___internalSubset_3;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::namespaceManager
	XmlNamespaceManager_t1354331288 * ___namespaceManager_4;
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::nameTable
	XmlNameTable_t347431366 * ___nameTable_5;
	// System.String System.Xml.XmlParserContext::publicID
	String_t* ___publicID_6;
	// System.String System.Xml.XmlParserContext::systemID
	String_t* ___systemID_7;
	// System.String System.Xml.XmlParserContext::xmlLang
	String_t* ___xmlLang_8;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::xmlSpace
	int32_t ___xmlSpace_9;
	// System.Collections.ArrayList System.Xml.XmlParserContext::contextItems
	ArrayList_t4250946984 * ___contextItems_10;
	// System.Int32 System.Xml.XmlParserContext::contextItemCount
	int32_t ___contextItemCount_11;
	// Mono.Xml.DTDObjectModel System.Xml.XmlParserContext::dtd
	DTDObjectModel_t3150902482 * ___dtd_12;

public:
	inline static int32_t get_offset_of_baseURI_0() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___baseURI_0)); }
	inline String_t* get_baseURI_0() const { return ___baseURI_0; }
	inline String_t** get_address_of_baseURI_0() { return &___baseURI_0; }
	inline void set_baseURI_0(String_t* value)
	{
		___baseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_0), value);
	}

	inline static int32_t get_offset_of_docTypeName_1() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___docTypeName_1)); }
	inline String_t* get_docTypeName_1() const { return ___docTypeName_1; }
	inline String_t** get_address_of_docTypeName_1() { return &___docTypeName_1; }
	inline void set_docTypeName_1(String_t* value)
	{
		___docTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeName_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___encoding_2)); }
	inline Encoding_t4004889433 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t4004889433 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t4004889433 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_internalSubset_3() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___internalSubset_3)); }
	inline String_t* get_internalSubset_3() const { return ___internalSubset_3; }
	inline String_t** get_address_of_internalSubset_3() { return &___internalSubset_3; }
	inline void set_internalSubset_3(String_t* value)
	{
		___internalSubset_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t1354331288 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t1354331288 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t1354331288 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___nameTable_5)); }
	inline XmlNameTable_t347431366 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t347431366 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_publicID_6() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___publicID_6)); }
	inline String_t* get_publicID_6() const { return ___publicID_6; }
	inline String_t** get_address_of_publicID_6() { return &___publicID_6; }
	inline void set_publicID_6(String_t* value)
	{
		___publicID_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicID_6), value);
	}

	inline static int32_t get_offset_of_systemID_7() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___systemID_7)); }
	inline String_t* get_systemID_7() const { return ___systemID_7; }
	inline String_t** get_address_of_systemID_7() { return &___systemID_7; }
	inline void set_systemID_7(String_t* value)
	{
		___systemID_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemID_7), value);
	}

	inline static int32_t get_offset_of_xmlLang_8() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___xmlLang_8)); }
	inline String_t* get_xmlLang_8() const { return ___xmlLang_8; }
	inline String_t** get_address_of_xmlLang_8() { return &___xmlLang_8; }
	inline void set_xmlLang_8(String_t* value)
	{
		___xmlLang_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_8), value);
	}

	inline static int32_t get_offset_of_xmlSpace_9() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___xmlSpace_9)); }
	inline int32_t get_xmlSpace_9() const { return ___xmlSpace_9; }
	inline int32_t* get_address_of_xmlSpace_9() { return &___xmlSpace_9; }
	inline void set_xmlSpace_9(int32_t value)
	{
		___xmlSpace_9 = value;
	}

	inline static int32_t get_offset_of_contextItems_10() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___contextItems_10)); }
	inline ArrayList_t4250946984 * get_contextItems_10() const { return ___contextItems_10; }
	inline ArrayList_t4250946984 ** get_address_of_contextItems_10() { return &___contextItems_10; }
	inline void set_contextItems_10(ArrayList_t4250946984 * value)
	{
		___contextItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___contextItems_10), value);
	}

	inline static int32_t get_offset_of_contextItemCount_11() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___contextItemCount_11)); }
	inline int32_t get_contextItemCount_11() const { return ___contextItemCount_11; }
	inline int32_t* get_address_of_contextItemCount_11() { return &___contextItemCount_11; }
	inline void set_contextItemCount_11(int32_t value)
	{
		___contextItemCount_11 = value;
	}

	inline static int32_t get_offset_of_dtd_12() { return static_cast<int32_t>(offsetof(XmlParserContext_t3802687902, ___dtd_12)); }
	inline DTDObjectModel_t3150902482 * get_dtd_12() const { return ___dtd_12; }
	inline DTDObjectModel_t3150902482 ** get_address_of_dtd_12() { return &___dtd_12; }
	inline void set_dtd_12(DTDObjectModel_t3150902482 * value)
	{
		___dtd_12 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_T3802687902_H
#ifndef PLAYABLEASSET_T1461289625_H
#define PLAYABLEASSET_T1461289625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t1461289625  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T1461289625_H
#ifndef U3CU3EC__ITERATOR0_T3775250355_H
#define U3CU3EC__ITERATOR0_T3775250355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t3775250355  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<outputTracks>__1
	TrackAsset_t2823685959 * ___U3CoutputTracksU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<output>__2
	PlayableBinding_t1732044054  ___U3CoutputU3E__2_3;
	// UnityEngine.Timeline.TimelineAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$this
	TimelineAsset_t452681751 * ___U24this_4;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_5;
	// System.Boolean UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputTracksU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U3CoutputTracksU3E__1_1)); }
	inline TrackAsset_t2823685959 * get_U3CoutputTracksU3E__1_1() const { return ___U3CoutputTracksU3E__1_1; }
	inline TrackAsset_t2823685959 ** get_address_of_U3CoutputTracksU3E__1_1() { return &___U3CoutputTracksU3E__1_1; }
	inline void set_U3CoutputTracksU3E__1_1(TrackAsset_t2823685959 * value)
	{
		___U3CoutputTracksU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputTracksU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3E__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U3CoutputU3E__2_3)); }
	inline PlayableBinding_t1732044054  get_U3CoutputU3E__2_3() const { return ___U3CoutputU3E__2_3; }
	inline PlayableBinding_t1732044054 * get_address_of_U3CoutputU3E__2_3() { return &___U3CoutputU3E__2_3; }
	inline void set_U3CoutputU3E__2_3(PlayableBinding_t1732044054  value)
	{
		___U3CoutputU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24this_4)); }
	inline TimelineAsset_t452681751 * get_U24this_4() const { return ___U24this_4; }
	inline TimelineAsset_t452681751 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TimelineAsset_t452681751 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24current_5)); }
	inline PlayableBinding_t1732044054  get_U24current_5() const { return ___U24current_5; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(PlayableBinding_t1732044054  value)
	{
		___U24current_5 = value;
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3775250355, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T3775250355_H
#ifndef CHARGETTER_T1293008504_H
#define CHARGETTER_T1293008504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport/CharGetter
struct  CharGetter_t1293008504  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGETTER_T1293008504_H
#ifndef U3CU3EC__ITERATOR0_T1996919277_H
#define U3CU3EC__ITERATOR0_T1996919277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1996919277  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.TrackBindingTypeAttribute UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<attribute>__0
	TrackBindingTypeAttribute_t2510260633 * ___U3CattributeU3E__0_0;
	// System.Type UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<trackBindingType>__0
	Type_t * ___U3CtrackBindingTypeU3E__0_1;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$this
	TrackAsset_t2823685959 * ___U24this_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_3;
	// System.Boolean UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CattributeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U3CattributeU3E__0_0)); }
	inline TrackBindingTypeAttribute_t2510260633 * get_U3CattributeU3E__0_0() const { return ___U3CattributeU3E__0_0; }
	inline TrackBindingTypeAttribute_t2510260633 ** get_address_of_U3CattributeU3E__0_0() { return &___U3CattributeU3E__0_0; }
	inline void set_U3CattributeU3E__0_0(TrackBindingTypeAttribute_t2510260633 * value)
	{
		___U3CattributeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CtrackBindingTypeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U3CtrackBindingTypeU3E__0_1)); }
	inline Type_t * get_U3CtrackBindingTypeU3E__0_1() const { return ___U3CtrackBindingTypeU3E__0_1; }
	inline Type_t ** get_address_of_U3CtrackBindingTypeU3E__0_1() { return &___U3CtrackBindingTypeU3E__0_1; }
	inline void set_U3CtrackBindingTypeU3E__0_1(Type_t * value)
	{
		___U3CtrackBindingTypeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrackBindingTypeU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U24this_2)); }
	inline TrackAsset_t2823685959 * get_U24this_2() const { return ___U24this_2; }
	inline TrackAsset_t2823685959 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrackAsset_t2823685959 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U24current_3)); }
	inline PlayableBinding_t1732044054  get_U24current_3() const { return ___U24current_3; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(PlayableBinding_t1732044054  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1996919277, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1996919277_H
#ifndef CONNECTIONCACHE_T1699053097_H
#define CONNECTIONCACHE_T1699053097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable/ConnectionCache
struct  ConnectionCache_t1699053097 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::playable
	Playable_t3296292090  ___playable_0;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable/ConnectionCache::port
	int32_t ___port_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::parent
	Playable_t3296292090  ___parent_2;
	// System.Single UnityEngine.Timeline.TimelinePlayable/ConnectionCache::evalWeight
	float ___evalWeight_3;

public:
	inline static int32_t get_offset_of_playable_0() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___playable_0)); }
	inline Playable_t3296292090  get_playable_0() const { return ___playable_0; }
	inline Playable_t3296292090 * get_address_of_playable_0() { return &___playable_0; }
	inline void set_playable_0(Playable_t3296292090  value)
	{
		___playable_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___parent_2)); }
	inline Playable_t3296292090  get_parent_2() const { return ___parent_2; }
	inline Playable_t3296292090 * get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Playable_t3296292090  value)
	{
		___parent_2 = value;
	}

	inline static int32_t get_offset_of_evalWeight_3() { return static_cast<int32_t>(offsetof(ConnectionCache_t1699053097, ___evalWeight_3)); }
	inline float get_evalWeight_3() const { return ___evalWeight_3; }
	inline float* get_address_of_evalWeight_3() { return &___evalWeight_3; }
	inline void set_evalWeight_3(float value)
	{
		___evalWeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCACHE_T1699053097_H
#ifndef XMLATTRIBUTETOKENINFO_T2488827827_H
#define XMLATTRIBUTETOKENINFO_T2488827827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct  XmlAttributeTokenInfo_t2488827827  : public XmlTokenInfo_t2043079912
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::ValueTokenStartIndex
	int32_t ___ValueTokenStartIndex_13;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::ValueTokenEndIndex
	int32_t ___ValueTokenEndIndex_14;
	// System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::valueCache
	String_t* ___valueCache_15;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::tmpBuilder
	StringBuilder_t622404039 * ___tmpBuilder_16;

public:
	inline static int32_t get_offset_of_ValueTokenStartIndex_13() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t2488827827, ___ValueTokenStartIndex_13)); }
	inline int32_t get_ValueTokenStartIndex_13() const { return ___ValueTokenStartIndex_13; }
	inline int32_t* get_address_of_ValueTokenStartIndex_13() { return &___ValueTokenStartIndex_13; }
	inline void set_ValueTokenStartIndex_13(int32_t value)
	{
		___ValueTokenStartIndex_13 = value;
	}

	inline static int32_t get_offset_of_ValueTokenEndIndex_14() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t2488827827, ___ValueTokenEndIndex_14)); }
	inline int32_t get_ValueTokenEndIndex_14() const { return ___ValueTokenEndIndex_14; }
	inline int32_t* get_address_of_ValueTokenEndIndex_14() { return &___ValueTokenEndIndex_14; }
	inline void set_ValueTokenEndIndex_14(int32_t value)
	{
		___ValueTokenEndIndex_14 = value;
	}

	inline static int32_t get_offset_of_valueCache_15() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t2488827827, ___valueCache_15)); }
	inline String_t* get_valueCache_15() const { return ___valueCache_15; }
	inline String_t** get_address_of_valueCache_15() { return &___valueCache_15; }
	inline void set_valueCache_15(String_t* value)
	{
		___valueCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_15), value);
	}

	inline static int32_t get_offset_of_tmpBuilder_16() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t2488827827, ___tmpBuilder_16)); }
	inline StringBuilder_t622404039 * get_tmpBuilder_16() const { return ___tmpBuilder_16; }
	inline StringBuilder_t622404039 ** get_address_of_tmpBuilder_16() { return &___tmpBuilder_16; }
	inline void set_tmpBuilder_16(StringBuilder_t622404039 * value)
	{
		___tmpBuilder_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuilder_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTETOKENINFO_T2488827827_H
#ifndef XMLNODECHANGEDEVENTHANDLER_T3906219045_H
#define XMLNODECHANGEDEVENTHANDLER_T3906219045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventHandler
struct  XmlNodeChangedEventHandler_t3906219045  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTHANDLER_T3906219045_H
#ifndef TIMELINEASSET_T452681751_H
#define TIMELINEASSET_T452681751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset
struct  TimelineAsset_t452681751  : public PlayableAsset_t1461289625
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset::m_NextId
	int32_t ___m_NextId_2;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_Tracks
	List_1_t1943312966 * ___m_Tracks_3;
	// System.Double UnityEngine.Timeline.TimelineAsset::m_FixedDuration
	double ___m_FixedDuration_4;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TimelineAsset::m_CacheOutputTracks
	TrackAssetU5BU5D_t984688574* ___m_CacheOutputTracks_5;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheFlattenedTracks
	List_1_t1943312966 * ___m_CacheFlattenedTracks_6;
	// UnityEngine.Timeline.TimelineAsset/EditorSettings UnityEngine.Timeline.TimelineAsset::m_EditorSettings
	EditorSettings_t2562619115 * ___m_EditorSettings_7;
	// UnityEngine.Timeline.TimelineAsset/DurationMode UnityEngine.Timeline.TimelineAsset::m_DurationMode
	int32_t ___m_DurationMode_8;

public:
	inline static int32_t get_offset_of_m_NextId_2() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_NextId_2)); }
	inline int32_t get_m_NextId_2() const { return ___m_NextId_2; }
	inline int32_t* get_address_of_m_NextId_2() { return &___m_NextId_2; }
	inline void set_m_NextId_2(int32_t value)
	{
		___m_NextId_2 = value;
	}

	inline static int32_t get_offset_of_m_Tracks_3() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_Tracks_3)); }
	inline List_1_t1943312966 * get_m_Tracks_3() const { return ___m_Tracks_3; }
	inline List_1_t1943312966 ** get_address_of_m_Tracks_3() { return &___m_Tracks_3; }
	inline void set_m_Tracks_3(List_1_t1943312966 * value)
	{
		___m_Tracks_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tracks_3), value);
	}

	inline static int32_t get_offset_of_m_FixedDuration_4() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_FixedDuration_4)); }
	inline double get_m_FixedDuration_4() const { return ___m_FixedDuration_4; }
	inline double* get_address_of_m_FixedDuration_4() { return &___m_FixedDuration_4; }
	inline void set_m_FixedDuration_4(double value)
	{
		___m_FixedDuration_4 = value;
	}

	inline static int32_t get_offset_of_m_CacheOutputTracks_5() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_CacheOutputTracks_5)); }
	inline TrackAssetU5BU5D_t984688574* get_m_CacheOutputTracks_5() const { return ___m_CacheOutputTracks_5; }
	inline TrackAssetU5BU5D_t984688574** get_address_of_m_CacheOutputTracks_5() { return &___m_CacheOutputTracks_5; }
	inline void set_m_CacheOutputTracks_5(TrackAssetU5BU5D_t984688574* value)
	{
		___m_CacheOutputTracks_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheOutputTracks_5), value);
	}

	inline static int32_t get_offset_of_m_CacheFlattenedTracks_6() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_CacheFlattenedTracks_6)); }
	inline List_1_t1943312966 * get_m_CacheFlattenedTracks_6() const { return ___m_CacheFlattenedTracks_6; }
	inline List_1_t1943312966 ** get_address_of_m_CacheFlattenedTracks_6() { return &___m_CacheFlattenedTracks_6; }
	inline void set_m_CacheFlattenedTracks_6(List_1_t1943312966 * value)
	{
		___m_CacheFlattenedTracks_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheFlattenedTracks_6), value);
	}

	inline static int32_t get_offset_of_m_EditorSettings_7() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_EditorSettings_7)); }
	inline EditorSettings_t2562619115 * get_m_EditorSettings_7() const { return ___m_EditorSettings_7; }
	inline EditorSettings_t2562619115 ** get_address_of_m_EditorSettings_7() { return &___m_EditorSettings_7; }
	inline void set_m_EditorSettings_7(EditorSettings_t2562619115 * value)
	{
		___m_EditorSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_EditorSettings_7), value);
	}

	inline static int32_t get_offset_of_m_DurationMode_8() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751, ___m_DurationMode_8)); }
	inline int32_t get_m_DurationMode_8() const { return ___m_DurationMode_8; }
	inline int32_t* get_address_of_m_DurationMode_8() { return &___m_DurationMode_8; }
	inline void set_m_DurationMode_8(int32_t value)
	{
		___m_DurationMode_8 = value;
	}
};

struct TimelineAsset_t452681751_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::<>f__am$cache0
	Predicate_1_t231338901 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(TimelineAsset_t452681751_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Predicate_1_t231338901 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Predicate_1_t231338901 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Predicate_1_t231338901 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEASSET_T452681751_H
#ifndef TRACKASSET_T2823685959_H
#define TRACKASSET_T2823685959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2823685959  : public PlayableAsset_t1461289625
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_2;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_3;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_4;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t1145879031 * ___m_AnimClip_5;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t1461289625 * ___m_Parent_6;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t1943312966 * ___m_Children_7;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_8;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t1733651323* ___m_ClipsCache_9;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t1558908690  ___m_Start_10;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t1558908690  ___m_End_11;
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackAsset::m_MediaType
	int32_t ___m_MediaType_12;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t1609610637 * ___m_Clips_14;

public:
	inline static int32_t get_offset_of_m_Locked_2() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Locked_2)); }
	inline bool get_m_Locked_2() const { return ___m_Locked_2; }
	inline bool* get_address_of_m_Locked_2() { return &___m_Locked_2; }
	inline void set_m_Locked_2(bool value)
	{
		___m_Locked_2 = value;
	}

	inline static int32_t get_offset_of_m_Muted_3() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Muted_3)); }
	inline bool get_m_Muted_3() const { return ___m_Muted_3; }
	inline bool* get_address_of_m_Muted_3() { return &___m_Muted_3; }
	inline void set_m_Muted_3(bool value)
	{
		___m_Muted_3 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_CustomPlayableFullTypename_4)); }
	inline String_t* get_m_CustomPlayableFullTypename_4() const { return ___m_CustomPlayableFullTypename_4; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_4() { return &___m_CustomPlayableFullTypename_4; }
	inline void set_m_CustomPlayableFullTypename_4(String_t* value)
	{
		___m_CustomPlayableFullTypename_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_4), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_AnimClip_5)); }
	inline AnimationClip_t1145879031 * get_m_AnimClip_5() const { return ___m_AnimClip_5; }
	inline AnimationClip_t1145879031 ** get_address_of_m_AnimClip_5() { return &___m_AnimClip_5; }
	inline void set_m_AnimClip_5(AnimationClip_t1145879031 * value)
	{
		___m_AnimClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_5), value);
	}

	inline static int32_t get_offset_of_m_Parent_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Parent_6)); }
	inline PlayableAsset_t1461289625 * get_m_Parent_6() const { return ___m_Parent_6; }
	inline PlayableAsset_t1461289625 ** get_address_of_m_Parent_6() { return &___m_Parent_6; }
	inline void set_m_Parent_6(PlayableAsset_t1461289625 * value)
	{
		___m_Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_6), value);
	}

	inline static int32_t get_offset_of_m_Children_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Children_7)); }
	inline List_1_t1943312966 * get_m_Children_7() const { return ___m_Children_7; }
	inline List_1_t1943312966 ** get_address_of_m_Children_7() { return &___m_Children_7; }
	inline void set_m_Children_7(List_1_t1943312966 * value)
	{
		___m_Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_7), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ItemsHash_8)); }
	inline int32_t get_m_ItemsHash_8() const { return ___m_ItemsHash_8; }
	inline int32_t* get_address_of_m_ItemsHash_8() { return &___m_ItemsHash_8; }
	inline void set_m_ItemsHash_8(int32_t value)
	{
		___m_ItemsHash_8 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ClipsCache_9)); }
	inline TimelineClipU5BU5D_t1733651323* get_m_ClipsCache_9() const { return ___m_ClipsCache_9; }
	inline TimelineClipU5BU5D_t1733651323** get_address_of_m_ClipsCache_9() { return &___m_ClipsCache_9; }
	inline void set_m_ClipsCache_9(TimelineClipU5BU5D_t1733651323* value)
	{
		___m_ClipsCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_9), value);
	}

	inline static int32_t get_offset_of_m_Start_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Start_10)); }
	inline DiscreteTime_t1558908690  get_m_Start_10() const { return ___m_Start_10; }
	inline DiscreteTime_t1558908690 * get_address_of_m_Start_10() { return &___m_Start_10; }
	inline void set_m_Start_10(DiscreteTime_t1558908690  value)
	{
		___m_Start_10 = value;
	}

	inline static int32_t get_offset_of_m_End_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_End_11)); }
	inline DiscreteTime_t1558908690  get_m_End_11() const { return ___m_End_11; }
	inline DiscreteTime_t1558908690 * get_address_of_m_End_11() { return &___m_End_11; }
	inline void set_m_End_11(DiscreteTime_t1558908690  value)
	{
		___m_End_11 = value;
	}

	inline static int32_t get_offset_of_m_MediaType_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_MediaType_12)); }
	inline int32_t get_m_MediaType_12() const { return ___m_MediaType_12; }
	inline int32_t* get_address_of_m_MediaType_12() { return &___m_MediaType_12; }
	inline void set_m_MediaType_12(int32_t value)
	{
		___m_MediaType_12 = value;
	}

	inline static int32_t get_offset_of_m_Clips_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Clips_14)); }
	inline List_1_t1609610637 * get_m_Clips_14() const { return ___m_Clips_14; }
	inline List_1_t1609610637 ** get_address_of_m_Clips_14() { return &___m_Clips_14; }
	inline void set_m_Clips_14(List_1_t1609610637 * value)
	{
		___m_Clips_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_14), value);
	}
};

struct TrackAsset_t2823685959_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t2389961773 * ___s_TrackBindingTypeAttributeCache_13;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t3874041483 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___s_TrackBindingTypeAttributeCache_13)); }
	inline Dictionary_2_t2389961773 * get_s_TrackBindingTypeAttributeCache_13() const { return ___s_TrackBindingTypeAttributeCache_13; }
	inline Dictionary_2_t2389961773 ** get_address_of_s_TrackBindingTypeAttributeCache_13() { return &___s_TrackBindingTypeAttributeCache_13; }
	inline void set_s_TrackBindingTypeAttributeCache_13(Dictionary_2_t2389961773 * value)
	{
		___s_TrackBindingTypeAttributeCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Comparison_1_t3874041483 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Comparison_1_t3874041483 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Comparison_1_t3874041483 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2823685959_H
#ifndef ACTIVATIONPLAYABLEASSET_T3202337516_H
#define ACTIVATIONPLAYABLEASSET_T3202337516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationPlayableAsset
struct  ActivationPlayableAsset_t3202337516  : public PlayableAsset_t1461289625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONPLAYABLEASSET_T3202337516_H
#ifndef ACTIVATIONTRACK_T785765815_H
#define ACTIVATIONTRACK_T785765815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack
struct  ActivationTrack_t785765815  : public TrackAsset_t2823685959
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationTrack::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_16;
	// UnityEngine.Timeline.ActivationMixerPlayable UnityEngine.Timeline.ActivationTrack::m_ActivationMixer
	ActivationMixerPlayable_t1415536146 * ___m_ActivationMixer_17;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_16() { return static_cast<int32_t>(offsetof(ActivationTrack_t785765815, ___m_PostPlaybackState_16)); }
	inline int32_t get_m_PostPlaybackState_16() const { return ___m_PostPlaybackState_16; }
	inline int32_t* get_address_of_m_PostPlaybackState_16() { return &___m_PostPlaybackState_16; }
	inline void set_m_PostPlaybackState_16(int32_t value)
	{
		___m_PostPlaybackState_16 = value;
	}

	inline static int32_t get_offset_of_m_ActivationMixer_17() { return static_cast<int32_t>(offsetof(ActivationTrack_t785765815, ___m_ActivationMixer_17)); }
	inline ActivationMixerPlayable_t1415536146 * get_m_ActivationMixer_17() const { return ___m_ActivationMixer_17; }
	inline ActivationMixerPlayable_t1415536146 ** get_address_of_m_ActivationMixer_17() { return &___m_ActivationMixer_17; }
	inline void set_m_ActivationMixer_17(ActivationMixerPlayable_t1415536146 * value)
	{
		___m_ActivationMixer_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivationMixer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONTRACK_T785765815_H
#ifndef GROUPTRACK_T844122244_H
#define GROUPTRACK_T844122244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.GroupTrack
struct  GroupTrack_t844122244  : public TrackAsset_t2823685959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPTRACK_T844122244_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (XmlConvert_t1639932650), -1, sizeof(XmlConvert_t1639932650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2100[7] = 
{
	XmlConvert_t1639932650_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1639932650_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1639932650_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1639932650_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1639932650_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1639932650_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1639932650_StaticFields::get_offset_of__defaultStyle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (XmlDeclaration_t1715995802), -1, sizeof(XmlDeclaration_t1715995802_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2101[4] = 
{
	XmlDeclaration_t1715995802::get_offset_of_encoding_6(),
	XmlDeclaration_t1715995802::get_offset_of_standalone_7(),
	XmlDeclaration_t1715995802::get_offset_of_version_8(),
	XmlDeclaration_t1715995802_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (XmlDocument_t2823332853), -1, sizeof(XmlDocument_t2823332853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2102[20] = 
{
	XmlDocument_t2823332853_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t2823332853::get_offset_of_optimal_create_element_6(),
	XmlDocument_t2823332853::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t2823332853::get_offset_of_nameTable_8(),
	XmlDocument_t2823332853::get_offset_of_baseURI_9(),
	XmlDocument_t2823332853::get_offset_of_implementation_10(),
	XmlDocument_t2823332853::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t2823332853::get_offset_of_resolver_12(),
	XmlDocument_t2823332853::get_offset_of_idTable_13(),
	XmlDocument_t2823332853::get_offset_of_nameCache_14(),
	XmlDocument_t2823332853::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t2823332853::get_offset_of_schemas_16(),
	XmlDocument_t2823332853::get_offset_of_schemaInfo_17(),
	XmlDocument_t2823332853::get_offset_of_loadMode_18(),
	XmlDocument_t2823332853::get_offset_of_NodeChanged_19(),
	XmlDocument_t2823332853::get_offset_of_NodeChanging_20(),
	XmlDocument_t2823332853::get_offset_of_NodeInserted_21(),
	XmlDocument_t2823332853::get_offset_of_NodeInserting_22(),
	XmlDocument_t2823332853::get_offset_of_NodeRemoved_23(),
	XmlDocument_t2823332853::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (XmlDocumentFragment_t460802731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	XmlDocumentFragment_t460802731::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (XmlDocumentType_t1189407184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[3] = 
{
	XmlDocumentType_t1189407184::get_offset_of_entities_6(),
	XmlDocumentType_t1189407184::get_offset_of_notations_7(),
	XmlDocumentType_t1189407184::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (XmlElement_t687149941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[5] = 
{
	XmlElement_t687149941::get_offset_of_attributes_6(),
	XmlElement_t687149941::get_offset_of_name_7(),
	XmlElement_t687149941::get_offset_of_lastLinkedChild_8(),
	XmlElement_t687149941::get_offset_of_isNotEmpty_9(),
	XmlElement_t687149941::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (XmlEntity_t2212524324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[7] = 
{
	XmlEntity_t2212524324::get_offset_of_name_5(),
	XmlEntity_t2212524324::get_offset_of_NDATA_6(),
	XmlEntity_t2212524324::get_offset_of_publicId_7(),
	XmlEntity_t2212524324::get_offset_of_systemId_8(),
	XmlEntity_t2212524324::get_offset_of_baseUri_9(),
	XmlEntity_t2212524324::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t2212524324::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (XmlEntityReference_t3237868160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	XmlEntityReference_t3237868160::get_offset_of_entityName_6(),
	XmlEntityReference_t3237868160::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (XmlException_t3531299539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	XmlException_t3531299539::get_offset_of_lineNumber_11(),
	XmlException_t3531299539::get_offset_of_linePosition_12(),
	XmlException_t3531299539::get_offset_of_sourceUri_13(),
	XmlException_t3531299539::get_offset_of_res_14(),
	XmlException_t3531299539::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (XmlImplementation_t3490314784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[1] = 
{
	XmlImplementation_t3490314784::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (XmlStreamReader_t1193375781), -1, sizeof(XmlStreamReader_t1193375781_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[2] = 
{
	XmlStreamReader_t1193375781::get_offset_of_input_12(),
	XmlStreamReader_t1193375781_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (NonBlockingStreamReader_t466767770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[11] = 
{
	NonBlockingStreamReader_t466767770::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t466767770::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t466767770::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t466767770::get_offset_of_pos_4(),
	NonBlockingStreamReader_t466767770::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t466767770::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t466767770::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t466767770::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t466767770::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t466767770::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t466767770::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (XmlInputStream_t3210582043), -1, sizeof(XmlInputStream_t3210582043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[7] = 
{
	XmlInputStream_t3210582043_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t3210582043::get_offset_of_enc_2(),
	XmlInputStream_t3210582043::get_offset_of_stream_3(),
	XmlInputStream_t3210582043::get_offset_of_buffer_4(),
	XmlInputStream_t3210582043::get_offset_of_bufLength_5(),
	XmlInputStream_t3210582043::get_offset_of_bufPos_6(),
	XmlInputStream_t3210582043_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (XmlLinkedNode_t1947670907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	XmlLinkedNode_t1947670907::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (XmlNameEntry_t1006717423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[5] = 
{
	XmlNameEntry_t1006717423::get_offset_of_Prefix_0(),
	XmlNameEntry_t1006717423::get_offset_of_LocalName_1(),
	XmlNameEntry_t1006717423::get_offset_of_NS_2(),
	XmlNameEntry_t1006717423::get_offset_of_Hash_3(),
	XmlNameEntry_t1006717423::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (XmlNameEntryCache_t728926030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[4] = 
{
	XmlNameEntryCache_t728926030::get_offset_of_table_0(),
	XmlNameEntryCache_t728926030::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t728926030::get_offset_of_dummy_2(),
	XmlNameEntryCache_t728926030::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (XmlNameTable_t347431366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (XmlNamedNodeMap_t4038918308), -1, sizeof(XmlNamedNodeMap_t4038918308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[4] = 
{
	XmlNamedNodeMap_t4038918308_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t4038918308::get_offset_of_parent_1(),
	XmlNamedNodeMap_t4038918308::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t4038918308::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (XmlNamespaceManager_t1354331288), -1, sizeof(XmlNamespaceManager_t1354331288_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[9] = 
{
	XmlNamespaceManager_t1354331288::get_offset_of_decls_0(),
	XmlNamespaceManager_t1354331288::get_offset_of_declPos_1(),
	XmlNamespaceManager_t1354331288::get_offset_of_scopes_2(),
	XmlNamespaceManager_t1354331288::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t1354331288::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t1354331288::get_offset_of_count_5(),
	XmlNamespaceManager_t1354331288::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t1354331288::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t1354331288_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (NsDecl_t1301437426)+ sizeof (RuntimeObject), sizeof(NsDecl_t1301437426_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[2] = 
{
	NsDecl_t1301437426::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsDecl_t1301437426::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (NsScope_t1100031146)+ sizeof (RuntimeObject), sizeof(NsScope_t1100031146_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2120[2] = 
{
	NsScope_t1100031146::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsScope_t1100031146::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (XmlNode_t3729859639), -1, sizeof(XmlNode_t3729859639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[5] = 
{
	XmlNode_t3729859639_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t3729859639::get_offset_of_ownerDocument_1(),
	XmlNode_t3729859639::get_offset_of_parentNode_2(),
	XmlNode_t3729859639::get_offset_of_childNodes_3(),
	XmlNode_t3729859639_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (EmptyNodeList_t596510042), -1, sizeof(EmptyNodeList_t596510042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	EmptyNodeList_t596510042_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (XmlNodeArrayList_t4205225702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	XmlNodeArrayList_t4205225702::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (XmlNodeChangedAction_t3523064990)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	XmlNodeChangedAction_t3523064990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (XmlNodeChangedEventArgs_t2915371093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[6] = 
{
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t2915371093::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (XmlNodeList_t3849781475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (XmlNodeListChildren_t4201287521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	XmlNodeListChildren_t4201287521::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (Enumerator_t412118962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[3] = 
{
	Enumerator_t412118962::get_offset_of_parent_0(),
	Enumerator_t412118962::get_offset_of_currentChild_1(),
	Enumerator_t412118962::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (XmlNodeType_t4278070251)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[19] = 
{
	XmlNodeType_t4278070251::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (XmlNotation_t3101001639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[4] = 
{
	XmlNotation_t3101001639::get_offset_of_localName_5(),
	XmlNotation_t3101001639::get_offset_of_publicId_6(),
	XmlNotation_t3101001639::get_offset_of_systemId_7(),
	XmlNotation_t3101001639::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (XmlParserContext_t3802687902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[13] = 
{
	XmlParserContext_t3802687902::get_offset_of_baseURI_0(),
	XmlParserContext_t3802687902::get_offset_of_docTypeName_1(),
	XmlParserContext_t3802687902::get_offset_of_encoding_2(),
	XmlParserContext_t3802687902::get_offset_of_internalSubset_3(),
	XmlParserContext_t3802687902::get_offset_of_namespaceManager_4(),
	XmlParserContext_t3802687902::get_offset_of_nameTable_5(),
	XmlParserContext_t3802687902::get_offset_of_publicID_6(),
	XmlParserContext_t3802687902::get_offset_of_systemID_7(),
	XmlParserContext_t3802687902::get_offset_of_xmlLang_8(),
	XmlParserContext_t3802687902::get_offset_of_xmlSpace_9(),
	XmlParserContext_t3802687902::get_offset_of_contextItems_10(),
	XmlParserContext_t3802687902::get_offset_of_contextItemCount_11(),
	XmlParserContext_t3802687902::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (ContextItem_t1914052158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[3] = 
{
	ContextItem_t1914052158::get_offset_of_BaseURI_0(),
	ContextItem_t1914052158::get_offset_of_XmlLang_1(),
	ContextItem_t1914052158::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (XmlParserInput_t2512531096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	XmlParserInput_t2512531096::get_offset_of_sourceStack_0(),
	XmlParserInput_t2512531096::get_offset_of_source_1(),
	XmlParserInput_t2512531096::get_offset_of_has_peek_2(),
	XmlParserInput_t2512531096::get_offset_of_peek_char_3(),
	XmlParserInput_t2512531096::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (XmlParserInputSource_t1595232959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[6] = 
{
	XmlParserInputSource_t1595232959::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t1595232959::get_offset_of_reader_1(),
	XmlParserInputSource_t1595232959::get_offset_of_state_2(),
	XmlParserInputSource_t1595232959::get_offset_of_isPE_3(),
	XmlParserInputSource_t1595232959::get_offset_of_line_4(),
	XmlParserInputSource_t1595232959::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (XmlProcessingInstruction_t2473598605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	XmlProcessingInstruction_t2473598605::get_offset_of_target_6(),
	XmlProcessingInstruction_t2473598605::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (XmlQualifiedName_t740477960), -1, sizeof(XmlQualifiedName_t740477960_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2136[4] = 
{
	XmlQualifiedName_t740477960_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t740477960::get_offset_of_name_1(),
	XmlQualifiedName_t740477960::get_offset_of_ns_2(),
	XmlQualifiedName_t740477960::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (XmlReader_t1082502433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[2] = 
{
	XmlReader_t1082502433::get_offset_of_binary_0(),
	XmlReader_t1082502433::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (XmlReaderBinarySupport_t2320539231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[5] = 
{
	XmlReaderBinarySupport_t2320539231::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t2320539231::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t2320539231::get_offset_of_state_2(),
	XmlReaderBinarySupport_t2320539231::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t2320539231::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (CommandState_t2322905070)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2139[6] = 
{
	CommandState_t2322905070::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (CharGetter_t1293008504), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (XmlReaderSettings_t190893096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	XmlReaderSettings_t190893096::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t190893096::get_offset_of_conformance_1(),
	XmlReaderSettings_t190893096::get_offset_of_schemas_2(),
	XmlReaderSettings_t190893096::get_offset_of_schemasNeedsInitialization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (XmlResolver_t161349657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (XmlSignificantWhitespace_t2546368137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (XmlSpace_t1832342193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[4] = 
{
	XmlSpace_t1832342193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (XmlText_t1994551155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (XmlTextReader_t3875484823), -1, sizeof(XmlTextReader_t3875484823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2146[54] = 
{
	XmlTextReader_t3875484823::get_offset_of_cursorToken_2(),
	XmlTextReader_t3875484823::get_offset_of_currentToken_3(),
	XmlTextReader_t3875484823::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_t3875484823::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_t3875484823::get_offset_of_attributeTokens_6(),
	XmlTextReader_t3875484823::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_t3875484823::get_offset_of_currentAttribute_8(),
	XmlTextReader_t3875484823::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_t3875484823::get_offset_of_attributeCount_10(),
	XmlTextReader_t3875484823::get_offset_of_parserContext_11(),
	XmlTextReader_t3875484823::get_offset_of_nameTable_12(),
	XmlTextReader_t3875484823::get_offset_of_nsmgr_13(),
	XmlTextReader_t3875484823::get_offset_of_readState_14(),
	XmlTextReader_t3875484823::get_offset_of_disallowReset_15(),
	XmlTextReader_t3875484823::get_offset_of_depth_16(),
	XmlTextReader_t3875484823::get_offset_of_elementDepth_17(),
	XmlTextReader_t3875484823::get_offset_of_depthUp_18(),
	XmlTextReader_t3875484823::get_offset_of_popScope_19(),
	XmlTextReader_t3875484823::get_offset_of_elementNames_20(),
	XmlTextReader_t3875484823::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_t3875484823::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_t3875484823::get_offset_of_isStandalone_23(),
	XmlTextReader_t3875484823::get_offset_of_returnEntityReference_24(),
	XmlTextReader_t3875484823::get_offset_of_entityReferenceName_25(),
	XmlTextReader_t3875484823::get_offset_of_valueBuffer_26(),
	XmlTextReader_t3875484823::get_offset_of_reader_27(),
	XmlTextReader_t3875484823::get_offset_of_peekChars_28(),
	XmlTextReader_t3875484823::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_t3875484823::get_offset_of_peekCharsLength_30(),
	XmlTextReader_t3875484823::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_t3875484823::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_t3875484823::get_offset_of_line_33(),
	XmlTextReader_t3875484823::get_offset_of_column_34(),
	XmlTextReader_t3875484823::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_t3875484823::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_t3875484823::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_t3875484823::get_offset_of_startNodeType_38(),
	XmlTextReader_t3875484823::get_offset_of_currentState_39(),
	XmlTextReader_t3875484823::get_offset_of_nestLevel_40(),
	XmlTextReader_t3875484823::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_t3875484823::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_t3875484823::get_offset_of_namespaces_43(),
	XmlTextReader_t3875484823::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_t3875484823::get_offset_of_resolver_45(),
	XmlTextReader_t3875484823::get_offset_of_normalization_46(),
	XmlTextReader_t3875484823::get_offset_of_checkCharacters_47(),
	XmlTextReader_t3875484823::get_offset_of_prohibitDtd_48(),
	XmlTextReader_t3875484823::get_offset_of_closeInput_49(),
	XmlTextReader_t3875484823::get_offset_of_entityHandling_50(),
	XmlTextReader_t3875484823::get_offset_of_whitespacePool_51(),
	XmlTextReader_t3875484823::get_offset_of_whitespaceCache_52(),
	XmlTextReader_t3875484823::get_offset_of_stateStack_53(),
	XmlTextReader_t3875484823_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_54(),
	XmlTextReader_t3875484823_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (XmlTokenInfo_t2043079912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[13] = 
{
	XmlTokenInfo_t2043079912::get_offset_of_valueCache_0(),
	XmlTokenInfo_t2043079912::get_offset_of_Reader_1(),
	XmlTokenInfo_t2043079912::get_offset_of_Name_2(),
	XmlTokenInfo_t2043079912::get_offset_of_LocalName_3(),
	XmlTokenInfo_t2043079912::get_offset_of_Prefix_4(),
	XmlTokenInfo_t2043079912::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t2043079912::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t2043079912::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t2043079912::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t2043079912::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t2043079912::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t2043079912::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t2043079912::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (XmlAttributeTokenInfo_t2488827827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[4] = 
{
	XmlAttributeTokenInfo_t2488827827::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t2488827827::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t2488827827::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t2488827827::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (TagName_t2918394058)+ sizeof (RuntimeObject), sizeof(TagName_t2918394058_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	TagName_t2918394058::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_t2918394058::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_t2918394058::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (DtdInputState_t2597333765)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[10] = 
{
	DtdInputState_t2597333765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (DtdInputStateStack_t2344161889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[1] = 
{
	DtdInputStateStack_t2344161889::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (XmlTextReader_t2210588564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[5] = 
{
	XmlTextReader_t2210588564::get_offset_of_entity_2(),
	XmlTextReader_t2210588564::get_offset_of_source_3(),
	XmlTextReader_t2210588564::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t2210588564::get_offset_of_insideAttribute_5(),
	XmlTextReader_t2210588564::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (XmlTextWriter_t3070435925), -1, sizeof(XmlTextWriter_t3070435925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2153[35] = 
{
	XmlTextWriter_t3070435925_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t3070435925_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t3070435925_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t3070435925::get_offset_of_base_stream_3(),
	XmlTextWriter_t3070435925::get_offset_of_source_4(),
	XmlTextWriter_t3070435925::get_offset_of_writer_5(),
	XmlTextWriter_t3070435925::get_offset_of_preserver_6(),
	XmlTextWriter_t3070435925::get_offset_of_preserved_name_7(),
	XmlTextWriter_t3070435925::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t3070435925::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t3070435925::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t3070435925::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t3070435925::get_offset_of_namespaces_12(),
	XmlTextWriter_t3070435925::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t3070435925::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t3070435925::get_offset_of_newline_handling_15(),
	XmlTextWriter_t3070435925::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t3070435925::get_offset_of_state_17(),
	XmlTextWriter_t3070435925::get_offset_of_node_state_18(),
	XmlTextWriter_t3070435925::get_offset_of_nsmanager_19(),
	XmlTextWriter_t3070435925::get_offset_of_open_count_20(),
	XmlTextWriter_t3070435925::get_offset_of_elements_21(),
	XmlTextWriter_t3070435925::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t3070435925::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t3070435925::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t3070435925::get_offset_of_indent_25(),
	XmlTextWriter_t3070435925::get_offset_of_indent_count_26(),
	XmlTextWriter_t3070435925::get_offset_of_indent_char_27(),
	XmlTextWriter_t3070435925::get_offset_of_indent_string_28(),
	XmlTextWriter_t3070435925::get_offset_of_newline_29(),
	XmlTextWriter_t3070435925::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t3070435925::get_offset_of_quote_char_31(),
	XmlTextWriter_t3070435925::get_offset_of_v2_32(),
	XmlTextWriter_t3070435925_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_33(),
	XmlTextWriter_t3070435925_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (XmlNodeInfo_t406731110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[7] = 
{
	XmlNodeInfo_t406731110::get_offset_of_Prefix_0(),
	XmlNodeInfo_t406731110::get_offset_of_LocalName_1(),
	XmlNodeInfo_t406731110::get_offset_of_NS_2(),
	XmlNodeInfo_t406731110::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t406731110::get_offset_of_HasElements_4(),
	XmlNodeInfo_t406731110::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t406731110::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (StringUtil_t1470244759), -1, sizeof(StringUtil_t1470244759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[2] = 
{
	StringUtil_t1470244759_StaticFields::get_offset_of_cul_0(),
	StringUtil_t1470244759_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (XmlDeclState_t4043840337)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	XmlDeclState_t4043840337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (XmlTokenizedType_t3218469591)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[14] = 
{
	XmlTokenizedType_t3218469591::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (XmlUrlResolver_t1529314874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[1] = 
{
	XmlUrlResolver_t1529314874::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (XmlWhitespace_t547184992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (XmlWriter_t1771783396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (XmlNodeChangedEventHandler_t3906219045), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (U3CPrivateImplementationDetailsU3E_t2655089821), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2162[7] = 
{
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D26_1(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D27_2(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D28_3(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D29_4(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D43_5(),
	U3CPrivateImplementationDetailsU3E_t2655089821_StaticFields::get_offset_of_U24U24fieldU2D44_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (U24ArrayTypeU2412_t1096561007)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2412_t1096561007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U24ArrayTypeU248_t4092320982)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU248_t4092320982 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U24ArrayTypeU24256_t1491899399)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24256_t1491899399 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (U24ArrayTypeU241280_t3767486320)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU241280_t3767486320 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (U3CModuleU3E_t1429447285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (ClipCaps_t319800182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[8] = 
{
	ClipCaps_t319800182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (TimelineClipCapsExtensions_t1024877177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (DiscreteTime_t1558908690)+ sizeof (RuntimeObject), sizeof(DiscreteTime_t1558908690 ), sizeof(DiscreteTime_t1558908690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2170[3] = 
{
	0,
	DiscreteTime_t1558908690_StaticFields::get_offset_of_kMaxTime_1(),
	DiscreteTime_t1558908690::get_offset_of_m_DiscreteTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (GroupTrack_t844122244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (Placeholder_t146813599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (TimelineAsset_t452681751), -1, sizeof(TimelineAsset_t452681751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2173[8] = 
{
	TimelineAsset_t452681751::get_offset_of_m_NextId_2(),
	TimelineAsset_t452681751::get_offset_of_m_Tracks_3(),
	TimelineAsset_t452681751::get_offset_of_m_FixedDuration_4(),
	TimelineAsset_t452681751::get_offset_of_m_CacheOutputTracks_5(),
	TimelineAsset_t452681751::get_offset_of_m_CacheFlattenedTracks_6(),
	TimelineAsset_t452681751::get_offset_of_m_EditorSettings_7(),
	TimelineAsset_t452681751::get_offset_of_m_DurationMode_8(),
	TimelineAsset_t452681751_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (MediaType_t673317257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[7] = 
{
	MediaType_t673317257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (DurationMode_t3686826482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[3] = 
{
	DurationMode_t3686826482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (EditorSettings_t2562619115), -1, sizeof(EditorSettings_t2562619115_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2176[2] = 
{
	EditorSettings_t2562619115_StaticFields::get_offset_of_kDefaultFPS_0(),
	EditorSettings_t2562619115::get_offset_of_fps_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (U3CU3Ec__Iterator0_t3775250355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[8] = 
{
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U3CoutputTracksU3E__1_1(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U3CoutputU3E__2_3(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t3775250355::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (TrackClipTypeAttribute_t1551440451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[1] = 
{
	TrackClipTypeAttribute_t1551440451::get_offset_of_inspectedType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (TrackMediaType_t224885088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[1] = 
{
	TrackMediaType_t224885088::get_offset_of_m_MediaType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (NotKeyableAttribute_t2161895226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (TrackBindingTypeAttribute_t2510260633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[1] = 
{
	TrackBindingTypeAttribute_t2510260633::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (SupportsChildTracksAttribute_t3475482393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[2] = 
{
	SupportsChildTracksAttribute_t3475482393::get_offset_of_childType_0(),
	SupportsChildTracksAttribute_t3475482393::get_offset_of_levels_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (IgnoreOnPlayableTrackAttribute_t1048616002), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (TimeFieldAttribute_t2900476623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (TimelineClip_t2489983630), -1, sizeof(TimelineClip_t2489983630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2187[30] = 
{
	TimelineClip_t2489983630_StaticFields::get_offset_of_kDefaultClipCaps_0(),
	TimelineClip_t2489983630_StaticFields::get_offset_of_kDefaultClipDurationInSeconds_1(),
	TimelineClip_t2489983630_StaticFields::get_offset_of_kTimeScaleMin_2(),
	TimelineClip_t2489983630_StaticFields::get_offset_of_kTimeScaleMax_3(),
	TimelineClip_t2489983630_StaticFields::get_offset_of_kMinDuration_4(),
	TimelineClip_t2489983630_StaticFields::get_offset_of_kMaxTimeValue_5(),
	TimelineClip_t2489983630::get_offset_of_m_Start_6(),
	TimelineClip_t2489983630::get_offset_of_m_ClipIn_7(),
	TimelineClip_t2489983630::get_offset_of_m_Asset_8(),
	TimelineClip_t2489983630::get_offset_of_m_UnderlyingAsset_9(),
	TimelineClip_t2489983630::get_offset_of_m_Duration_10(),
	TimelineClip_t2489983630::get_offset_of_m_TimeScale_11(),
	TimelineClip_t2489983630::get_offset_of_m_ParentTrack_12(),
	TimelineClip_t2489983630::get_offset_of_m_EaseInDuration_13(),
	TimelineClip_t2489983630::get_offset_of_m_EaseOutDuration_14(),
	TimelineClip_t2489983630::get_offset_of_m_BlendInDuration_15(),
	TimelineClip_t2489983630::get_offset_of_m_BlendOutDuration_16(),
	TimelineClip_t2489983630::get_offset_of_m_MixInCurve_17(),
	TimelineClip_t2489983630::get_offset_of_m_MixOutCurve_18(),
	TimelineClip_t2489983630::get_offset_of_m_BlendInCurveMode_19(),
	TimelineClip_t2489983630::get_offset_of_m_BlendOutCurveMode_20(),
	TimelineClip_t2489983630::get_offset_of_m_ExposedParameterNames_21(),
	TimelineClip_t2489983630::get_offset_of_m_AnimationCurves_22(),
	TimelineClip_t2489983630::get_offset_of_m_Recordable_23(),
	TimelineClip_t2489983630::get_offset_of_m_PostExtrapolationMode_24(),
	TimelineClip_t2489983630::get_offset_of_m_PreExtrapolationMode_25(),
	TimelineClip_t2489983630::get_offset_of_m_PostExtrapolationTime_26(),
	TimelineClip_t2489983630::get_offset_of_m_PreExtrapolationTime_27(),
	TimelineClip_t2489983630::get_offset_of_m_DisplayName_28(),
	TimelineClip_t2489983630::get_offset_of_U3CdirtyHashU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ClipExtrapolation_t513923237)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2188[6] = 
{
	ClipExtrapolation_t513923237::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (BlendCurveMode_t286953288)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	BlendCurveMode_t286953288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (TimelineMarker_t3685410445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[4] = 
{
	TimelineMarker_t3685410445::get_offset_of_m_Key_0(),
	TimelineMarker_t3685410445::get_offset_of_m_ParentTrack_1(),
	TimelineMarker_t3685410445::get_offset_of_m_Time_2(),
	TimelineMarker_t3685410445::get_offset_of_m_Selected_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (TimelinePlayable_t1837138455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[6] = 
{
	TimelinePlayable_t1837138455::get_offset_of_m_IntervalTree_0(),
	TimelinePlayable_t1837138455::get_offset_of_m_ActiveClips_1(),
	TimelinePlayable_t1837138455::get_offset_of_m_CurrentListOfActiveClips_2(),
	TimelinePlayable_t1837138455::get_offset_of_m_ActiveBit_3(),
	TimelinePlayable_t1837138455::get_offset_of_m_EvaluateCallbacks_4(),
	TimelinePlayable_t1837138455::get_offset_of_m_PlayableCache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (ConnectionCache_t1699053097)+ sizeof (RuntimeObject), sizeof(ConnectionCache_t1699053097 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[4] = 
{
	ConnectionCache_t1699053097::get_offset_of_playable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1699053097::get_offset_of_port_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1699053097::get_offset_of_parent_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1699053097::get_offset_of_evalWeight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (TrackAsset_t2823685959), -1, sizeof(TrackAsset_t2823685959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[14] = 
{
	TrackAsset_t2823685959::get_offset_of_m_Locked_2(),
	TrackAsset_t2823685959::get_offset_of_m_Muted_3(),
	TrackAsset_t2823685959::get_offset_of_m_CustomPlayableFullTypename_4(),
	TrackAsset_t2823685959::get_offset_of_m_AnimClip_5(),
	TrackAsset_t2823685959::get_offset_of_m_Parent_6(),
	TrackAsset_t2823685959::get_offset_of_m_Children_7(),
	TrackAsset_t2823685959::get_offset_of_m_ItemsHash_8(),
	TrackAsset_t2823685959::get_offset_of_m_ClipsCache_9(),
	TrackAsset_t2823685959::get_offset_of_m_Start_10(),
	TrackAsset_t2823685959::get_offset_of_m_End_11(),
	TrackAsset_t2823685959::get_offset_of_m_MediaType_12(),
	TrackAsset_t2823685959_StaticFields::get_offset_of_s_TrackBindingTypeAttributeCache_13(),
	TrackAsset_t2823685959::get_offset_of_m_Clips_14(),
	TrackAsset_t2823685959_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (U3CU3Ec__Iterator0_t1996919277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[6] = 
{
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U3CattributeU3E__0_0(),
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U3CtrackBindingTypeU3E__0_1(),
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t1996919277::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ActivationMixerPlayable_t1415536146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[3] = 
{
	ActivationMixerPlayable_t1415536146::get_offset_of_m_PostPlaybackState_0(),
	ActivationMixerPlayable_t1415536146::get_offset_of_m_BoundGameObjectInitialStateIsActive_1(),
	ActivationMixerPlayable_t1415536146::get_offset_of_m_BoundGameObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ActivationPlayableAsset_t3202337516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ActivationTrack_t785765815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	ActivationTrack_t785765815::get_offset_of_m_PostPlaybackState_16(),
	ActivationTrack_t785765815::get_offset_of_m_ActivationMixer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (PostPlaybackState_t3554111860)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[5] = 
{
	PostPlaybackState_t3554111860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
