﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Whiteout/FadeFinishDelegate
struct FadeFinishDelegate_t1266332645;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// WindowAutoYaw
struct WindowAutoYaw_t2079382413;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1618594486;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.Component
struct Component_t1632713610;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.Camera
struct Camera_t2839736942;
// UpdateManager/OnUpdate
struct OnUpdate_t172587279;
// WindowDragTilt
struct WindowDragTilt_t561804078;
// WordSearchPuzzle
struct WordSearchPuzzle_t817895283;
// System.String
struct String_t;
// UnityEngine.GUIText
struct GUIText_t3141904915;
// UnityEngine.Material
struct Material_t2815264910;
// PhraseScript
struct PhraseScript_t112947168;
// UnityEngine.GUITexture
struct GUITexture_t2618579966;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.GUISkin
struct GUISkin_t2122630221;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2736631256;
// UnityEngine.GUIStyle
struct GUIStyle_t1840606133;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// System.Void
struct Void_t653366341;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// UnityEngine.GUIStyleState
struct GUIStyleState_t2396521423;
// UnityEngine.RectOffset
struct RectOffset_t1566141465;
// UnityEngine.Font
struct Font_t4006162695;
// UnityEngine.GUISettings
struct GUISettings_t1039510989;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t2246123211;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t3871172268;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t657915608;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;

extern RuntimeClass* FadeState_t852633535_il2cpp_TypeInfo_var;
extern const uint32_t FadeFinishDelegate_BeginInvoke_m551730742_MetadataUsageId;
extern RuntimeClass* Quaternion_t704191599_il2cpp_TypeInfo_var;
extern const uint32_t WindowAutoYaw_OnDisable_m2738187930_MetadataUsageId;
extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern RuntimeClass* NGUITools_t582569761_il2cpp_TypeInfo_var;
extern RuntimeClass* OnUpdate_t172587279_il2cpp_TypeInfo_var;
extern const RuntimeMethod* WindowAutoYaw_CoroutineUpdate_m4013244025_RuntimeMethod_var;
extern const uint32_t WindowAutoYaw_Start_m3582948898_MetadataUsageId;
extern const uint32_t WindowAutoYaw_CoroutineUpdate_m4013244025_MetadataUsageId;
extern const RuntimeMethod* WindowDragTilt_CoroutineUpdate_m515415181_RuntimeMethod_var;
extern const uint32_t WindowDragTilt_Start_m1016492564_MetadataUsageId;
extern RuntimeClass* Vector3_t1986933152_il2cpp_TypeInfo_var;
extern const uint32_t WindowDragTilt_CoroutineUpdate_m515415181_MetadataUsageId;
extern RuntimeClass* BooleanU5BU5D_t698278498_il2cpp_TypeInfo_var;
extern const uint32_t WordSearchPuzzle__ctor_m3849930460_MetadataUsageId;
extern RuntimeClass* UInt32_t1721902794_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGUIText_t3141904915_m874595492_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2517895954;
extern const uint32_t WordSearchPuzzle_Start_m3933208015_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGUITexture_t2618579966_m753041165_RuntimeMethod_var;
extern const uint32_t WordSearchPuzzle_Update_m149845575_MetadataUsageId;
extern RuntimeClass* GUI_t500573050_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t972567508_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t2839736942_m2970667333_RuntimeMethod_var;
extern const uint32_t WordSearchPuzzle_OnGUI_m808235534_MetadataUsageId;
extern const uint32_t WordSearchPuzzle_Awake_m2896928918_MetadataUsageId;
struct GUIStyleState_t2396521423_marshaled_pinvoke;
struct GUIStyleState_t2396521423_marshaled_com;
struct RectOffset_t1566141465_marshaled_com;

struct BooleanU5BU5D_t698278498;
struct GUIStyleU5BU5D_t2736631256;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef UINT32_T1721902794_H
#define UINT32_T1721902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1721902794 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1721902794, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1721902794_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef RECTOFFSET_T1566141465_H
#define RECTOFFSET_T1566141465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1566141465  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1566141465, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1566141465, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1566141465_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1566141465_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1566141465_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef FADESTATE_T852633535_H
#define FADESTATE_T852633535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Whiteout/FadeState
struct  FadeState_t852633535 
{
public:
	// System.Int32 Whiteout/FadeState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FadeState_t852633535, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESTATE_T852633535_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef GAMEOBJECT_T2557347079_H
#define GAMEOBJECT_T2557347079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2557347079  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2557347079_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef MATERIAL_T2815264910_H
#define MATERIAL_T2815264910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2815264910  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2815264910_H
#ifndef TEXTURE_T2119925672_H
#define TEXTURE_T2119925672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2119925672  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2119925672_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef GUISTYLE_T1840606133_H
#define GUISTYLE_T1840606133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t1840606133  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t2396521423 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t2396521423 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t2396521423 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t2396521423 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t2396521423 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t2396521423 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t2396521423 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t2396521423 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1566141465 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1566141465 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1566141465 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1566141465 * ___m_Overflow_12;
	// UnityEngine.Font UnityEngine.GUIStyle::m_FontInternal
	Font_t4006162695 * ___m_FontInternal_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Normal_1)); }
	inline GUIStyleState_t2396521423 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t2396521423 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Hover_2)); }
	inline GUIStyleState_t2396521423 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t2396521423 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Active_3)); }
	inline GUIStyleState_t2396521423 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t2396521423 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Focused_4)); }
	inline GUIStyleState_t2396521423 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t2396521423 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_OnNormal_5)); }
	inline GUIStyleState_t2396521423 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t2396521423 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_OnHover_6)); }
	inline GUIStyleState_t2396521423 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t2396521423 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_OnActive_7)); }
	inline GUIStyleState_t2396521423 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t2396521423 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_OnFocused_8)); }
	inline GUIStyleState_t2396521423 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t2396521423 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t2396521423 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Border_9)); }
	inline RectOffset_t1566141465 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1566141465 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1566141465 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Padding_10)); }
	inline RectOffset_t1566141465 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1566141465 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1566141465 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Margin_11)); }
	inline RectOffset_t1566141465 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1566141465 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1566141465 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_Overflow_12)); }
	inline RectOffset_t1566141465 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1566141465 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1566141465 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}

	inline static int32_t get_offset_of_m_FontInternal_13() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133, ___m_FontInternal_13)); }
	inline Font_t4006162695 * get_m_FontInternal_13() const { return ___m_FontInternal_13; }
	inline Font_t4006162695 ** get_address_of_m_FontInternal_13() { return &___m_FontInternal_13; }
	inline void set_m_FontInternal_13(Font_t4006162695 * value)
	{
		___m_FontInternal_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontInternal_13), value);
	}
};

struct GUIStyle_t1840606133_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t1840606133 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t1840606133_StaticFields, ___s_None_15)); }
	inline GUIStyle_t1840606133 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t1840606133 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t1840606133 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t1840606133_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t2396521423_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1566141465_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1566141465_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1566141465_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1566141465_marshaled_pinvoke ___m_Overflow_12;
	Font_t4006162695 * ___m_FontInternal_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t1840606133_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2396521423_marshaled_com* ___m_Normal_1;
	GUIStyleState_t2396521423_marshaled_com* ___m_Hover_2;
	GUIStyleState_t2396521423_marshaled_com* ___m_Active_3;
	GUIStyleState_t2396521423_marshaled_com* ___m_Focused_4;
	GUIStyleState_t2396521423_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t2396521423_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t2396521423_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t2396521423_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1566141465_marshaled_com* ___m_Border_9;
	RectOffset_t1566141465_marshaled_com* ___m_Padding_10;
	RectOffset_t1566141465_marshaled_com* ___m_Margin_11;
	RectOffset_t1566141465_marshaled_com* ___m_Overflow_12;
	Font_t4006162695 * ___m_FontInternal_13;
};
#endif // GUISTYLE_T1840606133_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef FADEFINISHDELEGATE_T1266332645_H
#define FADEFINISHDELEGATE_T1266332645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Whiteout/FadeFinishDelegate
struct  FadeFinishDelegate_t1266332645  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEFINISHDELEGATE_T1266332645_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef ASYNCCALLBACK_T3561663063_H
#define ASYNCCALLBACK_T3561663063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3561663063  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3561663063_H
#ifndef GUISKIN_T2122630221_H
#define GUISKIN_T2122630221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t2122630221  : public ScriptableObject_t1804531341
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t4006162695 * ___m_Font_2;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t1840606133 * ___m_box_3;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t1840606133 * ___m_button_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t1840606133 * ___m_toggle_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t1840606133 * ___m_label_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t1840606133 * ___m_textField_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t1840606133 * ___m_textArea_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t1840606133 * ___m_window_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t1840606133 * ___m_horizontalSlider_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t1840606133 * ___m_horizontalSliderThumb_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t1840606133 * ___m_verticalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t1840606133 * ___m_verticalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t1840606133 * ___m_horizontalScrollbar_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t1840606133 * ___m_horizontalScrollbarThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t1840606133 * ___m_horizontalScrollbarLeftButton_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t1840606133 * ___m_horizontalScrollbarRightButton_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t1840606133 * ___m_verticalScrollbar_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t1840606133 * ___m_verticalScrollbarThumb_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t1840606133 * ___m_verticalScrollbarUpButton_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t1840606133 * ___m_verticalScrollbarDownButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t1840606133 * ___m_ScrollView_22;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2736631256* ___m_CustomStyles_23;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1039510989 * ___m_Settings_24;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t2246123211 * ___m_Styles_26;

public:
	inline static int32_t get_offset_of_m_Font_2() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_Font_2)); }
	inline Font_t4006162695 * get_m_Font_2() const { return ___m_Font_2; }
	inline Font_t4006162695 ** get_address_of_m_Font_2() { return &___m_Font_2; }
	inline void set_m_Font_2(Font_t4006162695 * value)
	{
		___m_Font_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_2), value);
	}

	inline static int32_t get_offset_of_m_box_3() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_box_3)); }
	inline GUIStyle_t1840606133 * get_m_box_3() const { return ___m_box_3; }
	inline GUIStyle_t1840606133 ** get_address_of_m_box_3() { return &___m_box_3; }
	inline void set_m_box_3(GUIStyle_t1840606133 * value)
	{
		___m_box_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_3), value);
	}

	inline static int32_t get_offset_of_m_button_4() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_button_4)); }
	inline GUIStyle_t1840606133 * get_m_button_4() const { return ___m_button_4; }
	inline GUIStyle_t1840606133 ** get_address_of_m_button_4() { return &___m_button_4; }
	inline void set_m_button_4(GUIStyle_t1840606133 * value)
	{
		___m_button_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_4), value);
	}

	inline static int32_t get_offset_of_m_toggle_5() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_toggle_5)); }
	inline GUIStyle_t1840606133 * get_m_toggle_5() const { return ___m_toggle_5; }
	inline GUIStyle_t1840606133 ** get_address_of_m_toggle_5() { return &___m_toggle_5; }
	inline void set_m_toggle_5(GUIStyle_t1840606133 * value)
	{
		___m_toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_5), value);
	}

	inline static int32_t get_offset_of_m_label_6() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_label_6)); }
	inline GUIStyle_t1840606133 * get_m_label_6() const { return ___m_label_6; }
	inline GUIStyle_t1840606133 ** get_address_of_m_label_6() { return &___m_label_6; }
	inline void set_m_label_6(GUIStyle_t1840606133 * value)
	{
		___m_label_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_6), value);
	}

	inline static int32_t get_offset_of_m_textField_7() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_textField_7)); }
	inline GUIStyle_t1840606133 * get_m_textField_7() const { return ___m_textField_7; }
	inline GUIStyle_t1840606133 ** get_address_of_m_textField_7() { return &___m_textField_7; }
	inline void set_m_textField_7(GUIStyle_t1840606133 * value)
	{
		___m_textField_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_7), value);
	}

	inline static int32_t get_offset_of_m_textArea_8() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_textArea_8)); }
	inline GUIStyle_t1840606133 * get_m_textArea_8() const { return ___m_textArea_8; }
	inline GUIStyle_t1840606133 ** get_address_of_m_textArea_8() { return &___m_textArea_8; }
	inline void set_m_textArea_8(GUIStyle_t1840606133 * value)
	{
		___m_textArea_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_8), value);
	}

	inline static int32_t get_offset_of_m_window_9() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_window_9)); }
	inline GUIStyle_t1840606133 * get_m_window_9() const { return ___m_window_9; }
	inline GUIStyle_t1840606133 ** get_address_of_m_window_9() { return &___m_window_9; }
	inline void set_m_window_9(GUIStyle_t1840606133 * value)
	{
		___m_window_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_9), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_10() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalSlider_10)); }
	inline GUIStyle_t1840606133 * get_m_horizontalSlider_10() const { return ___m_horizontalSlider_10; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalSlider_10() { return &___m_horizontalSlider_10; }
	inline void set_m_horizontalSlider_10(GUIStyle_t1840606133 * value)
	{
		___m_horizontalSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_10), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_11() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalSliderThumb_11)); }
	inline GUIStyle_t1840606133 * get_m_horizontalSliderThumb_11() const { return ___m_horizontalSliderThumb_11; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalSliderThumb_11() { return &___m_horizontalSliderThumb_11; }
	inline void set_m_horizontalSliderThumb_11(GUIStyle_t1840606133 * value)
	{
		___m_horizontalSliderThumb_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_11), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalSlider_12)); }
	inline GUIStyle_t1840606133 * get_m_verticalSlider_12() const { return ___m_verticalSlider_12; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalSlider_12() { return &___m_verticalSlider_12; }
	inline void set_m_verticalSlider_12(GUIStyle_t1840606133 * value)
	{
		___m_verticalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalSliderThumb_13)); }
	inline GUIStyle_t1840606133 * get_m_verticalSliderThumb_13() const { return ___m_verticalSliderThumb_13; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalSliderThumb_13() { return &___m_verticalSliderThumb_13; }
	inline void set_m_verticalSliderThumb_13(GUIStyle_t1840606133 * value)
	{
		___m_verticalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_14() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalScrollbar_14)); }
	inline GUIStyle_t1840606133 * get_m_horizontalScrollbar_14() const { return ___m_horizontalScrollbar_14; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalScrollbar_14() { return &___m_horizontalScrollbar_14; }
	inline void set_m_horizontalScrollbar_14(GUIStyle_t1840606133 * value)
	{
		___m_horizontalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalScrollbarThumb_15)); }
	inline GUIStyle_t1840606133 * get_m_horizontalScrollbarThumb_15() const { return ___m_horizontalScrollbarThumb_15; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalScrollbarThumb_15() { return &___m_horizontalScrollbarThumb_15; }
	inline void set_m_horizontalScrollbarThumb_15(GUIStyle_t1840606133 * value)
	{
		___m_horizontalScrollbarThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_16() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalScrollbarLeftButton_16)); }
	inline GUIStyle_t1840606133 * get_m_horizontalScrollbarLeftButton_16() const { return ___m_horizontalScrollbarLeftButton_16; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalScrollbarLeftButton_16() { return &___m_horizontalScrollbarLeftButton_16; }
	inline void set_m_horizontalScrollbarLeftButton_16(GUIStyle_t1840606133 * value)
	{
		___m_horizontalScrollbarLeftButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_17() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_horizontalScrollbarRightButton_17)); }
	inline GUIStyle_t1840606133 * get_m_horizontalScrollbarRightButton_17() const { return ___m_horizontalScrollbarRightButton_17; }
	inline GUIStyle_t1840606133 ** get_address_of_m_horizontalScrollbarRightButton_17() { return &___m_horizontalScrollbarRightButton_17; }
	inline void set_m_horizontalScrollbarRightButton_17(GUIStyle_t1840606133 * value)
	{
		___m_horizontalScrollbarRightButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_17), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_18() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalScrollbar_18)); }
	inline GUIStyle_t1840606133 * get_m_verticalScrollbar_18() const { return ___m_verticalScrollbar_18; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalScrollbar_18() { return &___m_verticalScrollbar_18; }
	inline void set_m_verticalScrollbar_18(GUIStyle_t1840606133 * value)
	{
		___m_verticalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_18), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_19() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalScrollbarThumb_19)); }
	inline GUIStyle_t1840606133 * get_m_verticalScrollbarThumb_19() const { return ___m_verticalScrollbarThumb_19; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalScrollbarThumb_19() { return &___m_verticalScrollbarThumb_19; }
	inline void set_m_verticalScrollbarThumb_19(GUIStyle_t1840606133 * value)
	{
		___m_verticalScrollbarThumb_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_20() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalScrollbarUpButton_20)); }
	inline GUIStyle_t1840606133 * get_m_verticalScrollbarUpButton_20() const { return ___m_verticalScrollbarUpButton_20; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalScrollbarUpButton_20() { return &___m_verticalScrollbarUpButton_20; }
	inline void set_m_verticalScrollbarUpButton_20(GUIStyle_t1840606133 * value)
	{
		___m_verticalScrollbarUpButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_21() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_verticalScrollbarDownButton_21)); }
	inline GUIStyle_t1840606133 * get_m_verticalScrollbarDownButton_21() const { return ___m_verticalScrollbarDownButton_21; }
	inline GUIStyle_t1840606133 ** get_address_of_m_verticalScrollbarDownButton_21() { return &___m_verticalScrollbarDownButton_21; }
	inline void set_m_verticalScrollbarDownButton_21(GUIStyle_t1840606133 * value)
	{
		___m_verticalScrollbarDownButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_21), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_22() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_ScrollView_22)); }
	inline GUIStyle_t1840606133 * get_m_ScrollView_22() const { return ___m_ScrollView_22; }
	inline GUIStyle_t1840606133 ** get_address_of_m_ScrollView_22() { return &___m_ScrollView_22; }
	inline void set_m_ScrollView_22(GUIStyle_t1840606133 * value)
	{
		___m_ScrollView_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_22), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_23() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_CustomStyles_23)); }
	inline GUIStyleU5BU5D_t2736631256* get_m_CustomStyles_23() const { return ___m_CustomStyles_23; }
	inline GUIStyleU5BU5D_t2736631256** get_address_of_m_CustomStyles_23() { return &___m_CustomStyles_23; }
	inline void set_m_CustomStyles_23(GUIStyleU5BU5D_t2736631256* value)
	{
		___m_CustomStyles_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_23), value);
	}

	inline static int32_t get_offset_of_m_Settings_24() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_Settings_24)); }
	inline GUISettings_t1039510989 * get_m_Settings_24() const { return ___m_Settings_24; }
	inline GUISettings_t1039510989 ** get_address_of_m_Settings_24() { return &___m_Settings_24; }
	inline void set_m_Settings_24(GUISettings_t1039510989 * value)
	{
		___m_Settings_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_24), value);
	}

	inline static int32_t get_offset_of_m_Styles_26() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221, ___m_Styles_26)); }
	inline Dictionary_2_t2246123211 * get_m_Styles_26() const { return ___m_Styles_26; }
	inline Dictionary_2_t2246123211 ** get_address_of_m_Styles_26() { return &___m_Styles_26; }
	inline void set_m_Styles_26(Dictionary_2_t2246123211 * value)
	{
		___m_Styles_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_26), value);
	}
};

struct GUISkin_t2122630221_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t1840606133 * ___ms_Error_25;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t3871172268 * ___m_SkinChanged_27;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t2122630221 * ___current_28;

public:
	inline static int32_t get_offset_of_ms_Error_25() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221_StaticFields, ___ms_Error_25)); }
	inline GUIStyle_t1840606133 * get_ms_Error_25() const { return ___ms_Error_25; }
	inline GUIStyle_t1840606133 ** get_address_of_ms_Error_25() { return &___ms_Error_25; }
	inline void set_ms_Error_25(GUIStyle_t1840606133 * value)
	{
		___ms_Error_25 = value;
		Il2CppCodeGenWriteBarrier((&___ms_Error_25), value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_27() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221_StaticFields, ___m_SkinChanged_27)); }
	inline SkinChangedDelegate_t3871172268 * get_m_SkinChanged_27() const { return ___m_SkinChanged_27; }
	inline SkinChangedDelegate_t3871172268 ** get_address_of_m_SkinChanged_27() { return &___m_SkinChanged_27; }
	inline void set_m_SkinChanged_27(SkinChangedDelegate_t3871172268 * value)
	{
		___m_SkinChanged_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_27), value);
	}

	inline static int32_t get_offset_of_current_28() { return static_cast<int32_t>(offsetof(GUISkin_t2122630221_StaticFields, ___current_28)); }
	inline GUISkin_t2122630221 * get_current_28() const { return ___current_28; }
	inline GUISkin_t2122630221 ** get_address_of_current_28() { return &___current_28; }
	inline void set_current_28(GUISkin_t2122630221 * value)
	{
		___current_28 = value;
		Il2CppCodeGenWriteBarrier((&___current_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T2122630221_H
#ifndef TRANSFORM_T362059596_H
#define TRANSFORM_T362059596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t362059596  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T362059596_H
#ifndef ONUPDATE_T172587279_H
#define ONUPDATE_T172587279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateManager/OnUpdate
struct  OnUpdate_t172587279  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPDATE_T172587279_H
#ifndef TEXTURE2D_T3063074017_H
#define TEXTURE2D_T3063074017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3063074017  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3063074017_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef CAMERA_T2839736942_H
#define CAMERA_T2839736942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t2839736942  : public Behaviour_t2850977393
{
public:

public:
};

struct Camera_t2839736942_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t657915608 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t657915608 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t657915608 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t657915608 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t657915608 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t657915608 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t657915608 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t657915608 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t657915608 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t657915608 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t657915608 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t657915608 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T2839736942_H
#ifndef GUIELEMENT_T2946029536_H
#define GUIELEMENT_T2946029536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t2946029536  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T2946029536_H
#ifndef WINDOWAUTOYAW_T2079382413_H
#define WINDOWAUTOYAW_T2079382413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowAutoYaw
struct  WindowAutoYaw_t2079382413  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 WindowAutoYaw::updateOrder
	int32_t ___updateOrder_2;
	// UnityEngine.Camera WindowAutoYaw::uiCamera
	Camera_t2839736942 * ___uiCamera_3;
	// System.Single WindowAutoYaw::yawAmount
	float ___yawAmount_4;
	// UnityEngine.Transform WindowAutoYaw::mTrans
	Transform_t362059596 * ___mTrans_5;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___uiCamera_3)); }
	inline Camera_t2839736942 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t2839736942 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_yawAmount_4() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___yawAmount_4)); }
	inline float get_yawAmount_4() const { return ___yawAmount_4; }
	inline float* get_address_of_yawAmount_4() { return &___yawAmount_4; }
	inline void set_yawAmount_4(float value)
	{
		___yawAmount_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWAUTOYAW_T2079382413_H
#ifndef GUITEXTURE_T2618579966_H
#define GUITEXTURE_T2618579966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUITexture
struct  GUITexture_t2618579966  : public GUIElement_t2946029536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXTURE_T2618579966_H
#ifndef PHRASESCRIPT_T112947168_H
#define PHRASESCRIPT_T112947168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhraseScript
struct  PhraseScript_t112947168  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32[] PhraseScript::CorrectLetterIndex
	Int32U5BU5D_t1965588061* ___CorrectLetterIndex_2;
	// System.Boolean[] PhraseScript::LettersCovered
	BooleanU5BU5D_t698278498* ___LettersCovered_3;
	// System.Boolean PhraseScript::m_bClear
	bool ___m_bClear_4;

public:
	inline static int32_t get_offset_of_CorrectLetterIndex_2() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___CorrectLetterIndex_2)); }
	inline Int32U5BU5D_t1965588061* get_CorrectLetterIndex_2() const { return ___CorrectLetterIndex_2; }
	inline Int32U5BU5D_t1965588061** get_address_of_CorrectLetterIndex_2() { return &___CorrectLetterIndex_2; }
	inline void set_CorrectLetterIndex_2(Int32U5BU5D_t1965588061* value)
	{
		___CorrectLetterIndex_2 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectLetterIndex_2), value);
	}

	inline static int32_t get_offset_of_LettersCovered_3() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___LettersCovered_3)); }
	inline BooleanU5BU5D_t698278498* get_LettersCovered_3() const { return ___LettersCovered_3; }
	inline BooleanU5BU5D_t698278498** get_address_of_LettersCovered_3() { return &___LettersCovered_3; }
	inline void set_LettersCovered_3(BooleanU5BU5D_t698278498* value)
	{
		___LettersCovered_3 = value;
		Il2CppCodeGenWriteBarrier((&___LettersCovered_3), value);
	}

	inline static int32_t get_offset_of_m_bClear_4() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___m_bClear_4)); }
	inline bool get_m_bClear_4() const { return ___m_bClear_4; }
	inline bool* get_address_of_m_bClear_4() { return &___m_bClear_4; }
	inline void set_m_bClear_4(bool value)
	{
		___m_bClear_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHRASESCRIPT_T112947168_H
#ifndef GUITEXT_T3141904915_H
#define GUITEXT_T3141904915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIText
struct  GUIText_t3141904915  : public GUIElement_t2946029536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T3141904915_H
#ifndef WINDOWDRAGTILT_T561804078_H
#define WINDOWDRAGTILT_T561804078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowDragTilt
struct  WindowDragTilt_t561804078  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 WindowDragTilt::updateOrder
	int32_t ___updateOrder_2;
	// System.Single WindowDragTilt::degrees
	float ___degrees_3;
	// UnityEngine.Vector3 WindowDragTilt::mLastPos
	Vector3_t1986933152  ___mLastPos_4;
	// UnityEngine.Transform WindowDragTilt::mTrans
	Transform_t362059596 * ___mTrans_5;
	// System.Single WindowDragTilt::mAngle
	float ___mAngle_6;
	// System.Boolean WindowDragTilt::mInit
	bool ___mInit_7;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_degrees_3() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___degrees_3)); }
	inline float get_degrees_3() const { return ___degrees_3; }
	inline float* get_address_of_degrees_3() { return &___degrees_3; }
	inline void set_degrees_3(float value)
	{
		___degrees_3 = value;
	}

	inline static int32_t get_offset_of_mLastPos_4() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mLastPos_4)); }
	inline Vector3_t1986933152  get_mLastPos_4() const { return ___mLastPos_4; }
	inline Vector3_t1986933152 * get_address_of_mLastPos_4() { return &___mLastPos_4; }
	inline void set_mLastPos_4(Vector3_t1986933152  value)
	{
		___mLastPos_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mAngle_6() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mAngle_6)); }
	inline float get_mAngle_6() const { return ___mAngle_6; }
	inline float* get_address_of_mAngle_6() { return &___mAngle_6; }
	inline void set_mAngle_6(float value)
	{
		___mAngle_6 = value;
	}

	inline static int32_t get_offset_of_mInit_7() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mInit_7)); }
	inline bool get_mInit_7() const { return ___mInit_7; }
	inline bool* get_address_of_mInit_7() { return &___mInit_7; }
	inline void set_mInit_7(bool value)
	{
		___mInit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWDRAGTILT_T561804078_H
#ifndef WORDSEARCHPUZZLE_T817895283_H
#define WORDSEARCHPUZZLE_T817895283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WordSearchPuzzle
struct  WordSearchPuzzle_t817895283  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject WordSearchPuzzle::Particles
	GameObject_t2557347079 * ___Particles_2;
	// System.String WordSearchPuzzle::Grid
	String_t* ___Grid_3;
	// System.Int32 WordSearchPuzzle::GridWidth
	int32_t ___GridWidth_4;
	// System.Int32 WordSearchPuzzle::GridHeight
	int32_t ___GridHeight_5;
	// System.String WordSearchPuzzle::GoalString
	String_t* ___GoalString_6;
	// System.Int32 WordSearchPuzzle::NumOfPhrase
	int32_t ___NumOfPhrase_7;
	// System.Single WordSearchPuzzle::BlockLeft
	float ___BlockLeft_8;
	// System.Single WordSearchPuzzle::BlockTop
	float ___BlockTop_9;
	// System.Single WordSearchPuzzle::BlockWidth
	float ___BlockWidth_10;
	// System.Single WordSearchPuzzle::BlockHeight
	float ___BlockHeight_11;
	// UnityEngine.GUISkin WordSearchPuzzle::Skin
	GUISkin_t2122630221 * ___Skin_12;
	// System.Single WordSearchPuzzle::m_fEndTimer
	float ___m_fEndTimer_13;
	// System.Boolean[] WordSearchPuzzle::ClearedLetters
	BooleanU5BU5D_t698278498* ___ClearedLetters_14;
	// System.Boolean WordSearchPuzzle::allclear
	bool ___allclear_15;
	// UnityEngine.Texture2D WordSearchPuzzle::m_5Action2Texture
	Texture2D_t3063074017 * ___m_5Action2Texture_16;

public:
	inline static int32_t get_offset_of_Particles_2() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Particles_2)); }
	inline GameObject_t2557347079 * get_Particles_2() const { return ___Particles_2; }
	inline GameObject_t2557347079 ** get_address_of_Particles_2() { return &___Particles_2; }
	inline void set_Particles_2(GameObject_t2557347079 * value)
	{
		___Particles_2 = value;
		Il2CppCodeGenWriteBarrier((&___Particles_2), value);
	}

	inline static int32_t get_offset_of_Grid_3() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Grid_3)); }
	inline String_t* get_Grid_3() const { return ___Grid_3; }
	inline String_t** get_address_of_Grid_3() { return &___Grid_3; }
	inline void set_Grid_3(String_t* value)
	{
		___Grid_3 = value;
		Il2CppCodeGenWriteBarrier((&___Grid_3), value);
	}

	inline static int32_t get_offset_of_GridWidth_4() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GridWidth_4)); }
	inline int32_t get_GridWidth_4() const { return ___GridWidth_4; }
	inline int32_t* get_address_of_GridWidth_4() { return &___GridWidth_4; }
	inline void set_GridWidth_4(int32_t value)
	{
		___GridWidth_4 = value;
	}

	inline static int32_t get_offset_of_GridHeight_5() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GridHeight_5)); }
	inline int32_t get_GridHeight_5() const { return ___GridHeight_5; }
	inline int32_t* get_address_of_GridHeight_5() { return &___GridHeight_5; }
	inline void set_GridHeight_5(int32_t value)
	{
		___GridHeight_5 = value;
	}

	inline static int32_t get_offset_of_GoalString_6() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GoalString_6)); }
	inline String_t* get_GoalString_6() const { return ___GoalString_6; }
	inline String_t** get_address_of_GoalString_6() { return &___GoalString_6; }
	inline void set_GoalString_6(String_t* value)
	{
		___GoalString_6 = value;
		Il2CppCodeGenWriteBarrier((&___GoalString_6), value);
	}

	inline static int32_t get_offset_of_NumOfPhrase_7() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___NumOfPhrase_7)); }
	inline int32_t get_NumOfPhrase_7() const { return ___NumOfPhrase_7; }
	inline int32_t* get_address_of_NumOfPhrase_7() { return &___NumOfPhrase_7; }
	inline void set_NumOfPhrase_7(int32_t value)
	{
		___NumOfPhrase_7 = value;
	}

	inline static int32_t get_offset_of_BlockLeft_8() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockLeft_8)); }
	inline float get_BlockLeft_8() const { return ___BlockLeft_8; }
	inline float* get_address_of_BlockLeft_8() { return &___BlockLeft_8; }
	inline void set_BlockLeft_8(float value)
	{
		___BlockLeft_8 = value;
	}

	inline static int32_t get_offset_of_BlockTop_9() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockTop_9)); }
	inline float get_BlockTop_9() const { return ___BlockTop_9; }
	inline float* get_address_of_BlockTop_9() { return &___BlockTop_9; }
	inline void set_BlockTop_9(float value)
	{
		___BlockTop_9 = value;
	}

	inline static int32_t get_offset_of_BlockWidth_10() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockWidth_10)); }
	inline float get_BlockWidth_10() const { return ___BlockWidth_10; }
	inline float* get_address_of_BlockWidth_10() { return &___BlockWidth_10; }
	inline void set_BlockWidth_10(float value)
	{
		___BlockWidth_10 = value;
	}

	inline static int32_t get_offset_of_BlockHeight_11() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockHeight_11)); }
	inline float get_BlockHeight_11() const { return ___BlockHeight_11; }
	inline float* get_address_of_BlockHeight_11() { return &___BlockHeight_11; }
	inline void set_BlockHeight_11(float value)
	{
		___BlockHeight_11 = value;
	}

	inline static int32_t get_offset_of_Skin_12() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Skin_12)); }
	inline GUISkin_t2122630221 * get_Skin_12() const { return ___Skin_12; }
	inline GUISkin_t2122630221 ** get_address_of_Skin_12() { return &___Skin_12; }
	inline void set_Skin_12(GUISkin_t2122630221 * value)
	{
		___Skin_12 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_12), value);
	}

	inline static int32_t get_offset_of_m_fEndTimer_13() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___m_fEndTimer_13)); }
	inline float get_m_fEndTimer_13() const { return ___m_fEndTimer_13; }
	inline float* get_address_of_m_fEndTimer_13() { return &___m_fEndTimer_13; }
	inline void set_m_fEndTimer_13(float value)
	{
		___m_fEndTimer_13 = value;
	}

	inline static int32_t get_offset_of_ClearedLetters_14() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___ClearedLetters_14)); }
	inline BooleanU5BU5D_t698278498* get_ClearedLetters_14() const { return ___ClearedLetters_14; }
	inline BooleanU5BU5D_t698278498** get_address_of_ClearedLetters_14() { return &___ClearedLetters_14; }
	inline void set_ClearedLetters_14(BooleanU5BU5D_t698278498* value)
	{
		___ClearedLetters_14 = value;
		Il2CppCodeGenWriteBarrier((&___ClearedLetters_14), value);
	}

	inline static int32_t get_offset_of_allclear_15() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___allclear_15)); }
	inline bool get_allclear_15() const { return ___allclear_15; }
	inline bool* get_address_of_allclear_15() { return &___allclear_15; }
	inline void set_allclear_15(bool value)
	{
		___allclear_15 = value;
	}

	inline static int32_t get_offset_of_m_5Action2Texture_16() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___m_5Action2Texture_16)); }
	inline Texture2D_t3063074017 * get_m_5Action2Texture_16() const { return ___m_5Action2Texture_16; }
	inline Texture2D_t3063074017 ** get_address_of_m_5Action2Texture_16() { return &___m_5Action2Texture_16; }
	inline void set_m_5Action2Texture_16(Texture2D_t3063074017 * value)
	{
		___m_5Action2Texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_5Action2Texture_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSEARCHPUZZLE_T817895283_H
// System.Boolean[]
struct BooleanU5BU5D_t698278498  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2736631256  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUIStyle_t1840606133 * m_Items[1];

public:
	inline GUIStyle_t1840606133 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUIStyle_t1840606133 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUIStyle_t1840606133 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUIStyle_t1840606133 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUIStyle_t1840606133 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUIStyle_t1840606133 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m1099312071_gshared (GameObject_t2557347079 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2360518480_gshared (Component_t1632713610 * __this, const RuntimeMethod* method);

// System.Void Whiteout/FadeFinishDelegate::Invoke(Whiteout/FadeState)
extern "C"  void FadeFinishDelegate_Invoke_m321251408 (FadeFinishDelegate_t1266332645 * __this, int32_t ___state0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2850514966 (MonoBehaviour_t1618594486 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t704191599  Quaternion_get_identity_m430028946 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m2730169435 (Transform_t362059596 * __this, Quaternion_t704191599  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2557347079 * Component_get_gameObject_m1032427207 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m1777929869 (GameObject_t2557347079 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera NGUITools::FindCameraForLayer(System.Int32)
extern "C"  Camera_t2839736942 * NGUITools_FindCameraForLayer_m433344575 (RuntimeObject * __this /* static, unused */, int32_t ___layer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t362059596 * Component_get_transform_m520192871 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateManager/OnUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnUpdate__ctor_m963314661 (OnUpdate_t172587279 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateManager::AddCoroutine(UnityEngine.MonoBehaviour,System.Int32,UpdateManager/OnUpdate)
extern "C"  void UpdateManager_AddCoroutine_m2755294966 (RuntimeObject * __this /* static, unused */, MonoBehaviour_t1618594486 * ___mb0, int32_t ___updateOrder1, OnUpdate_t172587279 * ___func2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1032523438 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t1986933152  Transform_get_position_m3101184820 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_WorldToViewportPoint_m781876753 (Camera_t2839736942 * __this, Vector3_t1986933152  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t704191599  Quaternion_Euler_m1199696711 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Subtraction_m1182848491 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  p0, Vector3_t1986933152  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIMath::SpringLerp(System.Single,System.Single,System.Single,System.Single)
extern "C"  float NGUIMath_SpringLerp_m2540361455 (RuntimeObject * __this /* static, unused */, float ___from0, float ___to1, float ___strength2, float ___deltaTime3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m4001439959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1430506261 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m2433954808 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t2557347079 * GameObject_Find_m1280780905 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUIText>()
#define GameObject_GetComponent_TisGUIText_t3141904915_m874595492(__this, method) ((  GUIText_t3141904915 * (*) (GameObject_t2557347079 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m1099312071_gshared)(__this, method)
// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C"  Material_t2815264910 * GUIText_get_material_m4252371176 (GUIText_t3141904915 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2237324961 (Color_t2582018970 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m2603757805 (Material_t2815264910 * __this, Color_t2582018970  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PhraseScript>()
#define GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462(__this, method) ((  PhraseScript_t112947168 * (*) (GameObject_t2557347079 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m1099312071_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUITexture>()
#define GameObject_GetComponent_TisGUITexture_t2618579966_m753041165(__this, method) ((  GUITexture_t2618579966 * (*) (GameObject_t2557347079 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m1099312071_gshared)(__this, method)
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C"  void GUITexture_set_texture_m1541504762 (GUITexture_t2618579966 * __this, Texture_t2119925672 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m1955003375 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m944706439 (GameObject_t2557347079 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3818835225 (Rect_t3039462994 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C"  GUIStyleU5BU5D_t2736631256* GUISkin_get_customStyles_m2862744984 (GUISkin_t2122630221 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4268332576 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m910852106 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  bool GUI_Button_m497084878 (RuntimeObject * __this /* static, unused */, Rect_t3039462994  p0, String_t* p1, GUIStyle_t1840606133 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhraseScript::CheckLetter(System.Int32)
extern "C"  bool PhraseScript_CheckLetter_m3562956615 (PhraseScript_t112947168 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t2839736942 * Camera_get_main_m1102209922 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2839736942_m2970667333(__this, method) ((  Camera_t2839736942 * (*) (Component_t1632713610 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2360518480_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t328513675  Rect_get_center_m1694125794 (Rect_t3039462994 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3282197930 (Vector3_t1986933152 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_ScreenToWorldPoint_m2205839903 (Camera_t2839736942 * __this, Vector3_t1986933152  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3123183460 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_FadeFinishDelegate_t1266332645 (FadeFinishDelegate_t1266332645 * __this, int32_t ___state0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___state0);

}
// System.Void Whiteout/FadeFinishDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FadeFinishDelegate__ctor_m871291316 (FadeFinishDelegate_t1266332645 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Whiteout/FadeFinishDelegate::Invoke(Whiteout/FadeState)
extern "C"  void FadeFinishDelegate_Invoke_m321251408 (FadeFinishDelegate_t1266332645 * __this, int32_t ___state0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FadeFinishDelegate_Invoke_m321251408((FadeFinishDelegate_t1266332645 *)__this->get_prev_9(),___state0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, int32_t ___state0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___state0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___state0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___state0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult Whiteout/FadeFinishDelegate::BeginInvoke(Whiteout/FadeState,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* FadeFinishDelegate_BeginInvoke_m551730742 (FadeFinishDelegate_t1266332645 * __this, int32_t ___state0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FadeFinishDelegate_BeginInvoke_m551730742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FadeState_t852633535_il2cpp_TypeInfo_var, &___state0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Whiteout/FadeFinishDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FadeFinishDelegate_EndInvoke_m1401648050 (FadeFinishDelegate_t1266332645 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void WindowAutoYaw::.ctor()
extern "C"  void WindowAutoYaw__ctor_m2166860490 (WindowAutoYaw_t2079382413 * __this, const RuntimeMethod* method)
{
	{
		__this->set_yawAmount_4((20.0f));
		MonoBehaviour__ctor_m2850514966(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowAutoYaw::OnDisable()
extern "C"  void WindowAutoYaw_OnDisable_m2738187930 (WindowAutoYaw_t2079382413 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowAutoYaw_OnDisable_m2738187930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t362059596 * L_0 = __this->get_mTrans_5();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_1 = Quaternion_get_identity_m430028946(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localRotation_m2730169435(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowAutoYaw::Start()
extern "C"  void WindowAutoYaw_Start_m3582948898 (WindowAutoYaw_t2079382413 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowAutoYaw_Start_m3582948898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t2839736942 * L_0 = __this->get_uiCamera_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		GameObject_t2557347079 * L_2 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = GameObject_get_layer_m1777929869(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t582569761_il2cpp_TypeInfo_var);
		Camera_t2839736942 * L_4 = NGUITools_FindCameraForLayer_m433344575(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_uiCamera_3(L_4);
	}

IL_0027:
	{
		Transform_t362059596 * L_5 = Component_get_transform_m520192871(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_5);
		int32_t L_6 = __this->get_updateOrder_2();
		intptr_t L_7 = (intptr_t)WindowAutoYaw_CoroutineUpdate_m4013244025_RuntimeMethod_var;
		OnUpdate_t172587279 * L_8 = (OnUpdate_t172587279 *)il2cpp_codegen_object_new(OnUpdate_t172587279_il2cpp_TypeInfo_var);
		OnUpdate__ctor_m963314661(L_8, __this, L_7, /*hidden argument*/NULL);
		UpdateManager_AddCoroutine_m2755294966(NULL /*static, unused*/, __this, L_6, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowAutoYaw::CoroutineUpdate(System.Single)
extern "C"  void WindowAutoYaw_CoroutineUpdate_m4013244025 (WindowAutoYaw_t2079382413 * __this, float ___delta0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowAutoYaw_CoroutineUpdate_m4013244025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2839736942 * L_0 = __this->get_uiCamera_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		Camera_t2839736942 * L_2 = __this->get_uiCamera_3();
		Transform_t362059596 * L_3 = __this->get_mTrans_5();
		NullCheck(L_3);
		Vector3_t1986933152  L_4 = Transform_get_position_m3101184820(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t1986933152  L_5 = Camera_WorldToViewportPoint_m781876753(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t362059596 * L_6 = __this->get_mTrans_5();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = __this->get_yawAmount_4();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_9 = Quaternion_Euler_m1199696711(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)L_7*(float)(2.0f)))-(float)(1.0f)))*(float)L_8)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localRotation_m2730169435(L_6, L_9, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void WindowDragTilt::.ctor()
extern "C"  void WindowDragTilt__ctor_m3902029347 (WindowDragTilt_t561804078 * __this, const RuntimeMethod* method)
{
	{
		__this->set_degrees_3((30.0f));
		__this->set_mInit_7((bool)1);
		MonoBehaviour__ctor_m2850514966(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowDragTilt::Start()
extern "C"  void WindowDragTilt_Start_m1016492564 (WindowDragTilt_t561804078 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowDragTilt_Start_m1016492564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_updateOrder_2();
		intptr_t L_1 = (intptr_t)WindowDragTilt_CoroutineUpdate_m515415181_RuntimeMethod_var;
		OnUpdate_t172587279 * L_2 = (OnUpdate_t172587279 *)il2cpp_codegen_object_new(OnUpdate_t172587279_il2cpp_TypeInfo_var);
		OnUpdate__ctor_m963314661(L_2, __this, L_1, /*hidden argument*/NULL);
		UpdateManager_AddCoroutine_m2755294966(NULL /*static, unused*/, __this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowDragTilt::OnEnable()
extern "C"  void WindowDragTilt_OnEnable_m2480426073 (WindowDragTilt_t561804078 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mInit_7((bool)1);
		return;
	}
}
// System.Void WindowDragTilt::CoroutineUpdate(System.Single)
extern "C"  void WindowDragTilt_CoroutineUpdate_m515415181 (WindowDragTilt_t561804078 * __this, float ___delta0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowDragTilt_CoroutineUpdate_m515415181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_mInit_7();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		__this->set_mInit_7((bool)0);
		Transform_t362059596 * L_1 = Component_get_transform_m520192871(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_1);
		Transform_t362059596 * L_2 = __this->get_mTrans_5();
		NullCheck(L_2);
		Vector3_t1986933152  L_3 = Transform_get_position_m3101184820(L_2, /*hidden argument*/NULL);
		__this->set_mLastPos_4(L_3);
	}

IL_002f:
	{
		Transform_t362059596 * L_4 = __this->get_mTrans_5();
		NullCheck(L_4);
		Vector3_t1986933152  L_5 = Transform_get_position_m3101184820(L_4, /*hidden argument*/NULL);
		Vector3_t1986933152  L_6 = __this->get_mLastPos_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_7 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Transform_t362059596 * L_8 = __this->get_mTrans_5();
		NullCheck(L_8);
		Vector3_t1986933152  L_9 = Transform_get_position_m3101184820(L_8, /*hidden argument*/NULL);
		__this->set_mLastPos_4(L_9);
		float L_10 = __this->get_mAngle_6();
		float L_11 = (&V_0)->get_x_1();
		float L_12 = __this->get_degrees_3();
		__this->set_mAngle_6(((float)((float)L_10+(float)((float)((float)L_11*(float)L_12)))));
		float L_13 = __this->get_mAngle_6();
		float L_14 = ___delta0;
		float L_15 = NGUIMath_SpringLerp_m2540361455(NULL /*static, unused*/, L_13, (0.0f), (20.0f), L_14, /*hidden argument*/NULL);
		__this->set_mAngle_6(L_15);
		Transform_t362059596 * L_16 = __this->get_mTrans_5();
		float L_17 = __this->get_mAngle_6();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_18 = Quaternion_Euler_m1199696711(NULL /*static, unused*/, (0.0f), (0.0f), ((-L_17)), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localRotation_m2730169435(L_16, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WordSearchPuzzle::.ctor()
extern "C"  void WordSearchPuzzle__ctor_m3849930460 (WordSearchPuzzle_t817895283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSearchPuzzle__ctor_m3849930460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_GridWidth_4(6);
		__this->set_GridHeight_5(6);
		__this->set_NumOfPhrase_7(3);
		int32_t L_0 = Screen_get_width_m4001439959(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_BlockLeft_8(((float)((float)(((float)((float)L_0)))*(float)(0.3135f))));
		int32_t L_1 = Screen_get_height_m1430506261(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_BlockTop_9(((float)((float)(((float)((float)L_1)))*(float)(0.175f))));
		int32_t L_2 = Screen_get_width_m4001439959(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_BlockWidth_10(((float)((float)(((float)((float)L_2)))*(float)(0.429f))));
		int32_t L_3 = Screen_get_height_m1430506261(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_BlockHeight_11(((float)((float)(((float)((float)L_3)))*(float)(0.67f))));
		__this->set_m_fEndTimer_13((2.0f));
		__this->set_ClearedLetters_14(((BooleanU5BU5D_t698278498*)SZArrayNew(BooleanU5BU5D_t698278498_il2cpp_TypeInfo_var, (uint32_t)((int32_t)36))));
		MonoBehaviour__ctor_m2850514966(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WordSearchPuzzle::Start()
extern "C"  void WordSearchPuzzle_Start_m3933208015 (WordSearchPuzzle_t817895283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSearchPuzzle_Start_m3933208015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004a;
	}

IL_0007:
	{
		uint32_t L_0 = V_0;
		uint32_t L_1 = ((uint32_t)((int32_t)((int32_t)L_0+(int32_t)1)));
		RuntimeObject * L_2 = Box(UInt32_t1721902794_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2433954808(NULL /*static, unused*/, _stringLiteral2517895954, L_2, /*hidden argument*/NULL);
		GameObject_t2557347079 * L_4 = GameObject_Find_m1280780905(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIText_t3141904915 * L_5 = GameObject_GetComponent_TisGUIText_t3141904915_m874595492(L_4, /*hidden argument*/GameObject_GetComponent_TisGUIText_t3141904915_m874595492_RuntimeMethod_var);
		NullCheck(L_5);
		Material_t2815264910 * L_6 = GUIText_get_material_m4252371176(L_5, /*hidden argument*/NULL);
		Color_t2582018970  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2237324961((&L_7), (0.44f), (0.44f), (0.44f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m2603757805(L_6, L_7, /*hidden argument*/NULL);
		uint32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_004a:
	{
		uint32_t L_9 = V_0;
		int32_t L_10 = __this->get_NumOfPhrase_7();
		if ((((int64_t)(((int64_t)((uint64_t)L_9)))) < ((int64_t)(((int64_t)((int64_t)L_10))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WordSearchPuzzle::Update()
extern "C"  void WordSearchPuzzle_Update_m149845575 (WordSearchPuzzle_t817895283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSearchPuzzle_Update_m149845575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		__this->set_allclear_15((bool)1);
		V_0 = 0;
		goto IL_003f;
	}

IL_000e:
	{
		uint32_t L_0 = V_0;
		uint32_t L_1 = ((uint32_t)((int32_t)((int32_t)L_0+(int32_t)1)));
		RuntimeObject * L_2 = Box(UInt32_t1721902794_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2433954808(NULL /*static, unused*/, _stringLiteral2517895954, L_2, /*hidden argument*/NULL);
		GameObject_t2557347079 * L_4 = GameObject_Find_m1280780905(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		PhraseScript_t112947168 * L_5 = GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462(L_4, /*hidden argument*/GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462_RuntimeMethod_var);
		NullCheck(L_5);
		bool L_6 = L_5->get_m_bClear_4();
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		__this->set_allclear_15((bool)0);
	}

IL_003b:
	{
		uint32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003f:
	{
		uint32_t L_8 = V_0;
		int32_t L_9 = __this->get_NumOfPhrase_7();
		if ((((int64_t)(((int64_t)((uint64_t)L_8)))) < ((int64_t)(((int64_t)((int64_t)L_9))))))
		{
			goto IL_000e;
		}
	}
	{
		bool L_10 = __this->get_allclear_15();
		if (!L_10)
		{
			goto IL_009c;
		}
	}
	{
		GameObject_t2557347079 * L_11 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUITexture_t2618579966 * L_12 = GameObject_GetComponent_TisGUITexture_t2618579966_m753041165(L_11, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t2618579966_m753041165_RuntimeMethod_var);
		Texture2D_t3063074017 * L_13 = __this->get_m_5Action2Texture_16();
		NullCheck(L_12);
		GUITexture_set_texture_m1541504762(L_12, L_13, /*hidden argument*/NULL);
		float L_14 = __this->get_m_fEndTimer_13();
		float L_15 = Time_get_deltaTime_m1955003375(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_fEndTimer_13(((float)((float)L_14-(float)L_15)));
		float L_16 = __this->get_m_fEndTimer_13();
		if ((!(((float)L_16) <= ((float)(0.0f)))))
		{
			goto IL_009c;
		}
	}
	{
		GameObject_t2557347079 * L_17 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_SetActive_m944706439(L_17, (bool)0, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void WordSearchPuzzle::OnGUI()
extern "C"  void WordSearchPuzzle_OnGUI_m808235534 (WordSearchPuzzle_t817895283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSearchPuzzle_OnGUI_m808235534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Rect_t3039462994  V_5;
	memset(&V_5, 0, sizeof(V_5));
	GUIStyle_t1840606133 * V_6 = NULL;
	Il2CppChar V_7 = 0x0;
	int32_t V_8 = 0;
	bool V_9 = false;
	Vector3_t1986933152  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t328513675  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t328513675  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		V_0 = 0;
		float L_0 = __this->get_BlockWidth_10();
		V_1 = ((float)((float)L_0/(float)(6.0f)));
		float L_1 = __this->get_BlockHeight_11();
		V_2 = ((float)((float)L_1/(float)(6.0f)));
		V_3 = 0;
		goto IL_0155;
	}

IL_0023:
	{
		V_4 = 0;
		goto IL_0144;
	}

IL_002b:
	{
		float L_2 = __this->get_BlockLeft_8();
		int32_t L_3 = V_4;
		float L_4 = V_1;
		float L_5 = __this->get_BlockTop_9();
		int32_t L_6 = V_3;
		float L_7 = V_2;
		float L_8 = V_1;
		float L_9 = V_2;
		Rect__ctor_m3818835225((&V_5), ((float)((float)L_2+(float)((float)((float)(((float)((float)L_3)))*(float)L_4)))), ((float)((float)L_5+(float)((float)((float)(((float)((float)L_6)))*(float)L_7)))), L_8, L_9, /*hidden argument*/NULL);
		GUISkin_t2122630221 * L_10 = __this->get_Skin_12();
		NullCheck(L_10);
		GUIStyleU5BU5D_t2736631256* L_11 = GUISkin_get_customStyles_m2862744984(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = 6;
		GUIStyle_t1840606133 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_6 = L_13;
		BooleanU5BU5D_t698278498* L_14 = __this->get_ClearedLetters_14();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if (!L_17)
		{
			goto IL_0077;
		}
	}
	{
		GUISkin_t2122630221 * L_18 = __this->get_Skin_12();
		NullCheck(L_18);
		GUIStyleU5BU5D_t2736631256* L_19 = GUISkin_get_customStyles_m2862744984(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = ((int32_t)37);
		GUIStyle_t1840606133 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_6 = L_21;
	}

IL_0077:
	{
		Rect_t3039462994  L_22 = V_5;
		String_t* L_23 = __this->get_Grid_3();
		int32_t L_24 = V_0;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m4268332576(L_23, L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		String_t* L_26 = Char_ToString_m910852106((&V_7), /*hidden argument*/NULL);
		GUIStyle_t1840606133 * L_27 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t500573050_il2cpp_TypeInfo_var);
		bool L_28 = GUI_Button_m497084878(NULL /*static, unused*/, L_22, L_26, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_013a;
		}
	}
	{
		V_8 = 0;
		goto IL_012d;
	}

IL_00a8:
	{
		int32_t L_29 = V_8;
		int32_t L_30 = ((int32_t)((int32_t)L_29+(int32_t)1));
		RuntimeObject * L_31 = Box(Int32_t972567508_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2433954808(NULL /*static, unused*/, _stringLiteral2517895954, L_31, /*hidden argument*/NULL);
		GameObject_t2557347079 * L_33 = GameObject_Find_m1280780905(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		PhraseScript_t112947168 * L_34 = GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462(L_33, /*hidden argument*/GameObject_GetComponent_TisPhraseScript_t112947168_m3640949462_RuntimeMethod_var);
		int32_t L_35 = V_0;
		NullCheck(L_34);
		bool L_36 = PhraseScript_CheckLetter_m3562956615(L_34, L_35, /*hidden argument*/NULL);
		V_9 = L_36;
		bool L_37 = V_9;
		if (!L_37)
		{
			goto IL_0127;
		}
	}
	{
		BooleanU5BU5D_t698278498* L_38 = __this->get_ClearedLetters_14();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (bool)1);
		Camera_t2839736942 * L_40 = Camera_get_main_m1102209922(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		Camera_t2839736942 * L_41 = Component_GetComponent_TisCamera_t2839736942_m2970667333(L_40, /*hidden argument*/Component_GetComponent_TisCamera_t2839736942_m2970667333_RuntimeMethod_var);
		Vector2_t328513675  L_42 = Rect_get_center_m1694125794((&V_5), /*hidden argument*/NULL);
		V_11 = L_42;
		float L_43 = (&V_11)->get_x_0();
		Vector2_t328513675  L_44 = Rect_get_center_m1694125794((&V_5), /*hidden argument*/NULL);
		V_12 = L_44;
		float L_45 = (&V_12)->get_y_1();
		Vector3_t1986933152  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector3__ctor_m3282197930((&L_46), L_43, L_45, (20.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t1986933152  L_47 = Camera_ScreenToWorldPoint_m2205839903(L_41, L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		float L_48 = (&V_10)->get_y_2();
		(&V_10)->set_y_2(((-L_48)));
	}

IL_0127:
	{
		int32_t L_49 = V_8;
		V_8 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_012d:
	{
		int32_t L_50 = V_8;
		int32_t L_51 = __this->get_NumOfPhrase_7();
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_00a8;
		}
	}

IL_013a:
	{
		int32_t L_52 = V_0;
		V_0 = ((int32_t)((int32_t)L_52+(int32_t)1));
		int32_t L_53 = V_4;
		V_4 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_0144:
	{
		int32_t L_54 = V_4;
		int32_t L_55 = __this->get_GridWidth_4();
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_56 = V_3;
		V_3 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_0155:
	{
		int32_t L_57 = V_3;
		int32_t L_58 = __this->get_GridHeight_5();
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
// System.Void WordSearchPuzzle::Awake()
extern "C"  void WordSearchPuzzle_Awake_m2896928918 (WordSearchPuzzle_t817895283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSearchPuzzle_Awake_m2896928918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t2557347079 * L_0 = __this->get_Particles_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3123183460(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t2557347079 * L_2 = __this->get_Particles_2();
		NullCheck(L_2);
		GameObject_SetActive_m944706439(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
