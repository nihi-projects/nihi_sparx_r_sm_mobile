﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// MagicLightFlicker
struct MagicLightFlicker_t4078483488;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1618594486;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t1632713610;
// UnityEngine.Light
struct Light_t1775228881;
// System.String
struct String_t;
// Moving Texture
struct MovingU20Texture_t723204342;
// UnityEngine.Renderer
struct Renderer_t1418648713;
// UnityEngine.Material
struct Material_t2815264910;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Void
struct Void_t653366341;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Reflection.MemberFilter
struct MemberFilter_t2357198184;

extern Il2CppCodeGenString* _stringLiteral3055645696;
extern const uint32_t MagicLightFlicker__ctor_m3163289107_MetadataUsageId;
extern const RuntimeType* Light_t1775228881_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Light_t1775228881_il2cpp_TypeInfo_var;
extern const uint32_t MagicLightFlicker_Start_m468926281_MetadataUsageId;
extern const uint32_t MagicLightFlicker_Update_m445955335_MetadataUsageId;
extern RuntimeClass* Single_t485236535_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t2228218122_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3961547005;
extern Il2CppCodeGenString* _stringLiteral86727331;
extern Il2CppCodeGenString* _stringLiteral3084319294;
extern Il2CppCodeGenString* _stringLiteral4257629191;
extern Il2CppCodeGenString* _stringLiteral2013446388;
extern const uint32_t MagicLightFlicker_EvalWave_m3494744430_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t1418648713_m1897737104_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1665458476;
extern const uint32_t MovingU20Texture_FixedUpdate_m4203207332_MetadataUsageId;



#ifndef U3CMODULEU3E_T1429447288_H
#define U3CMODULEU3E_T1429447288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447288 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447288_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef RUNTIMETYPEHANDLE_T3069131627_H
#define RUNTIMETYPEHANDLE_T3069131627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3069131627 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3069131627, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3069131627_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef MATERIAL_T2815264910_H
#define MATERIAL_T2815264910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2815264910  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2815264910_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3069131627  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3069131627  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3069131627 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3069131627  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1985992169* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2357198184 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2357198184 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2357198184 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1985992169* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1985992169* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2357198184 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2357198184 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2357198184 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2357198184 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2357198184 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2357198184 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef RENDERER_T1418648713_H
#define RENDERER_T1418648713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t1418648713  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T1418648713_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef LIGHT_T1775228881_H
#define LIGHT_T1775228881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Light
struct  Light_t1775228881  : public Behaviour_t2850977393
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_2;

public:
	inline static int32_t get_offset_of_m_BakedIndex_2() { return static_cast<int32_t>(offsetof(Light_t1775228881, ___m_BakedIndex_2)); }
	inline int32_t get_m_BakedIndex_2() const { return ___m_BakedIndex_2; }
	inline int32_t* get_address_of_m_BakedIndex_2() { return &___m_BakedIndex_2; }
	inline void set_m_BakedIndex_2(int32_t value)
	{
		___m_BakedIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHT_T1775228881_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef MOVINGU20TEXTURE_T723204342_H
#define MOVINGU20TEXTURE_T723204342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moving Texture
struct  MovingU20Texture_t723204342  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Moving Texture::m_fScrollSpeed1
	float ___m_fScrollSpeed1_2;
	// System.Single Moving Texture::m_fScrollSpeed2
	float ___m_fScrollSpeed2_3;

public:
	inline static int32_t get_offset_of_m_fScrollSpeed1_2() { return static_cast<int32_t>(offsetof(MovingU20Texture_t723204342, ___m_fScrollSpeed1_2)); }
	inline float get_m_fScrollSpeed1_2() const { return ___m_fScrollSpeed1_2; }
	inline float* get_address_of_m_fScrollSpeed1_2() { return &___m_fScrollSpeed1_2; }
	inline void set_m_fScrollSpeed1_2(float value)
	{
		___m_fScrollSpeed1_2 = value;
	}

	inline static int32_t get_offset_of_m_fScrollSpeed2_3() { return static_cast<int32_t>(offsetof(MovingU20Texture_t723204342, ___m_fScrollSpeed2_3)); }
	inline float get_m_fScrollSpeed2_3() const { return ___m_fScrollSpeed2_3; }
	inline float* get_address_of_m_fScrollSpeed2_3() { return &___m_fScrollSpeed2_3; }
	inline void set_m_fScrollSpeed2_3(float value)
	{
		___m_fScrollSpeed2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVINGU20TEXTURE_T723204342_H
#ifndef MAGICLIGHTFLICKER_T4078483488_H
#define MAGICLIGHTFLICKER_T4078483488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicLightFlicker
struct  MagicLightFlicker_t4078483488  : public MonoBehaviour_t1618594486
{
public:
	// System.String MagicLightFlicker::waveFunction
	String_t* ___waveFunction_2;
	// System.Single MagicLightFlicker::base
	float ___base_3;
	// System.Single MagicLightFlicker::amplitude
	float ___amplitude_4;
	// System.Single MagicLightFlicker::phase
	float ___phase_5;
	// System.Single MagicLightFlicker::frequency
	float ___frequency_6;
	// UnityEngine.Color MagicLightFlicker::originalColor
	Color_t2582018970  ___originalColor_7;

public:
	inline static int32_t get_offset_of_waveFunction_2() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___waveFunction_2)); }
	inline String_t* get_waveFunction_2() const { return ___waveFunction_2; }
	inline String_t** get_address_of_waveFunction_2() { return &___waveFunction_2; }
	inline void set_waveFunction_2(String_t* value)
	{
		___waveFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___waveFunction_2), value);
	}

	inline static int32_t get_offset_of_base_3() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___base_3)); }
	inline float get_base_3() const { return ___base_3; }
	inline float* get_address_of_base_3() { return &___base_3; }
	inline void set_base_3(float value)
	{
		___base_3 = value;
	}

	inline static int32_t get_offset_of_amplitude_4() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___amplitude_4)); }
	inline float get_amplitude_4() const { return ___amplitude_4; }
	inline float* get_address_of_amplitude_4() { return &___amplitude_4; }
	inline void set_amplitude_4(float value)
	{
		___amplitude_4 = value;
	}

	inline static int32_t get_offset_of_phase_5() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___phase_5)); }
	inline float get_phase_5() const { return ___phase_5; }
	inline float* get_address_of_phase_5() { return &___phase_5; }
	inline void set_phase_5(float value)
	{
		___phase_5 = value;
	}

	inline static int32_t get_offset_of_frequency_6() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___frequency_6)); }
	inline float get_frequency_6() const { return ___frequency_6; }
	inline float* get_address_of_frequency_6() { return &___frequency_6; }
	inline void set_frequency_6(float value)
	{
		___frequency_6 = value;
	}

	inline static int32_t get_offset_of_originalColor_7() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___originalColor_7)); }
	inline Color_t2582018970  get_originalColor_7() const { return ___originalColor_7; }
	inline Color_t2582018970 * get_address_of_originalColor_7() { return &___originalColor_7; }
	inline void set_originalColor_7(Color_t2582018970  value)
	{
		___originalColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICLIGHTFLICKER_T4078483488_H


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2360518480_gshared (Component_t1632713610 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2850514966 (MonoBehaviour_t1618594486 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m125071402 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3069131627  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1632713610 * Component_GetComponent_m2808223086 (Component_t1632713610 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Light::get_color()
extern "C"  Color_t2582018970  Light_get_color_m1977739623 (Light_t1775228881 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t2582018970  Color_op_Multiply_m722506647 (RuntimeObject * __this /* static, unused */, Color_t2582018970  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C"  void Light_set_color_m4219441485 (Light_t1775228881 * __this, Color_t2582018970  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m3605797736 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1683246493 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m555213051 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t1418648713_m1897737104(__this, method) ((  Renderer_t1418648713 * (*) (Component_t1632713610 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2360518480_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t2815264910 * Renderer_get_material_m633794551 (Renderer_t1418648713 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m22874897 (Vector2_t328513675 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern "C"  void Material_SetTextureOffset_m236122088 (Material_t2815264910 * __this, String_t* p0, Vector2_t328513675  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MagicLightFlicker::.ctor()
extern "C"  void MagicLightFlicker__ctor_m3163289107 (MagicLightFlicker_t4078483488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MagicLightFlicker__ctor_m3163289107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m2850514966(__this, /*hidden argument*/NULL);
		__this->set_waveFunction_2(_stringLiteral3055645696);
		__this->set_amplitude_4((1.0f));
		__this->set_frequency_6((0.5f));
		return;
	}
}
// System.Void MagicLightFlicker::Start()
extern "C"  void MagicLightFlicker_Start_m468926281 (MagicLightFlicker_t4078483488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MagicLightFlicker_Start_m468926281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Light_t1775228881_0_0_0_var), /*hidden argument*/NULL);
		Component_t1632713610 * L_1 = Component_GetComponent_m2808223086(__this, L_0, /*hidden argument*/NULL);
		NullCheck(((Light_t1775228881 *)CastclassSealed((RuntimeObject*)L_1, Light_t1775228881_il2cpp_TypeInfo_var)));
		Color_t2582018970  L_2 = Light_get_color_m1977739623(((Light_t1775228881 *)CastclassSealed((RuntimeObject*)L_1, Light_t1775228881_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_originalColor_7(L_2);
		return;
	}
}
// System.Void MagicLightFlicker::Update()
extern "C"  void MagicLightFlicker_Update_m445955335 (MagicLightFlicker_t4078483488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MagicLightFlicker_Update_m445955335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Light_t1775228881 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Light_t1775228881_0_0_0_var), /*hidden argument*/NULL);
		Component_t1632713610 * L_1 = Component_GetComponent_m2808223086(__this, L_0, /*hidden argument*/NULL);
		V_0 = ((Light_t1775228881 *)CastclassSealed((RuntimeObject*)L_1, Light_t1775228881_il2cpp_TypeInfo_var));
		Light_t1775228881 * L_2 = V_0;
		Color_t2582018970  L_3 = __this->get_originalColor_7();
		float L_4 = VirtFuncInvoker0< float >::Invoke(6 /* System.Single MagicLightFlicker::EvalWave() */, __this);
		Color_t2582018970  L_5 = Color_op_Multiply_m722506647(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Light_set_color_m4219441485(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Single MagicLightFlicker::EvalWave()
extern "C"  float MagicLightFlicker_EvalWave_m3494744430 (MagicLightFlicker_t4078483488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MagicLightFlicker_EvalWave_m3494744430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_time_m3605797736(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_phase_5();
		float L_2 = __this->get_frequency_6();
		V_0 = ((float)((float)((float)((float)L_0+(float)L_1))*(float)L_2));
		Initobj (Single_t485236535_il2cpp_TypeInfo_var, (&V_1));
		float L_3 = V_0;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_5 = floorf(L_4);
		V_0 = ((float)((float)L_3-(float)L_5));
		String_t* L_6 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_6, _stringLiteral3055645696, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004f;
		}
	}
	{
		float L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_9 = sinf(((float)((float)((float)((float)L_8*(float)(((float)((float)2)))))*(float)(3.14159274f))));
		V_1 = L_9;
		goto IL_0135;
	}

IL_004f:
	{
		String_t* L_10 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_10, _stringLiteral3961547005, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0095;
		}
	}
	{
		float L_12 = V_0;
		if ((((float)L_12) >= ((float)(0.5f))))
		{
			goto IL_0082;
		}
	}
	{
		float L_13 = V_0;
		V_1 = ((float)((float)((float)((float)(4.0f)*(float)L_13))-(float)(1.0f)));
		goto IL_0090;
	}

IL_0082:
	{
		float L_14 = V_0;
		V_1 = ((float)((float)((float)((float)(-4.0f)*(float)L_14))+(float)(3.0f)));
	}

IL_0090:
	{
		goto IL_0135;
	}

IL_0095:
	{
		String_t* L_15 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_15, _stringLiteral86727331, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00cb;
		}
	}
	{
		float L_17 = V_0;
		if ((((float)L_17) >= ((float)(0.5f))))
		{
			goto IL_00c0;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_00c6;
	}

IL_00c0:
	{
		V_1 = (-1.0f);
	}

IL_00c6:
	{
		goto IL_0135;
	}

IL_00cb:
	{
		String_t* L_18 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_18, _stringLiteral3084319294, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e7;
		}
	}
	{
		float L_20 = V_0;
		V_1 = L_20;
		goto IL_0135;
	}

IL_00e7:
	{
		String_t* L_21 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_21, _stringLiteral4257629191, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0109;
		}
	}
	{
		float L_23 = V_0;
		V_1 = ((float)((float)(1.0f)-(float)L_23));
		goto IL_0135;
	}

IL_0109:
	{
		String_t* L_24 = __this->get_waveFunction_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_24, _stringLiteral2013446388, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_012f;
		}
	}
	{
		float L_26 = Random_get_value_m555213051(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)1)))-(float)((float)((float)L_26*(float)(((float)((float)2)))))));
		goto IL_0135;
	}

IL_012f:
	{
		V_1 = (1.0f);
	}

IL_0135:
	{
		float L_27 = V_1;
		float L_28 = __this->get_amplitude_4();
		float L_29 = __this->get_base_3();
		return ((float)((float)((float)((float)L_27*(float)L_28))+(float)L_29));
	}
}
// System.Void MagicLightFlicker::Main()
extern "C"  void MagicLightFlicker_Main_m3082608059 (MagicLightFlicker_t4078483488 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Moving Texture::.ctor()
extern "C"  void MovingU20Texture__ctor_m659098475 (MovingU20Texture_t723204342 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2850514966(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Moving Texture::FixedUpdate()
extern "C"  void MovingU20Texture_FixedUpdate_m4203207332 (MovingU20Texture_t723204342 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingU20Texture_FixedUpdate_m4203207332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_time_m3605797736(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_m_fScrollSpeed1_2();
		V_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = Time_get_time_m3605797736(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_m_fScrollSpeed2_3();
		V_1 = ((float)((float)L_2*(float)L_3));
		Renderer_t1418648713 * L_4 = Component_GetComponent_TisRenderer_t1418648713_m1897737104(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1418648713_m1897737104_RuntimeMethod_var);
		NullCheck(L_4);
		Material_t2815264910 * L_5 = Renderer_get_material_m633794551(L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		float L_7 = V_1;
		Vector2_t328513675  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m22874897((&L_8), L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetTextureOffset_m236122088(L_5, _stringLiteral1665458476, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Moving Texture::Main()
extern "C"  void MovingU20Texture_Main_m2469471422 (MovingU20Texture_t723204342 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
