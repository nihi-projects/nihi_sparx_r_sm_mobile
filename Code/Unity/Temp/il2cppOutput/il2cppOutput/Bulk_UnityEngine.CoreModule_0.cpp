﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t2898687950;
// System.String
struct String_t;
// System.Attribute
struct Attribute_t1924466020;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t2754673280;
// UnityEngine.Application/LowMemoryCallback
struct LowMemoryCallback_t3854352520;
// System.Globalization.CultureInfo
struct CultureInfo_t2625678720;
// System.IFormatProvider
struct IFormatProvider_t1109252446;
// System.Text.StringBuilder
struct StringBuilder_t622404039;
// System.Object[]
struct ObjectU5BU5D_t2737604620;
// UnityEngine.Application/LogCallback
struct LogCallback_t204719788;
// UnityEngine.Events.UnityAction
struct UnityAction_t992331987;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t3739209734;
// System.Action`1<System.Object>
struct Action_1_t4119957313;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t2196794798;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2177427975;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t987895095;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t968528272;
// UnityEngine.RequireComponent
struct RequireComponent_t2180947615;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t334925532;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t2744062746;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t869940717;
// UnityEngine.Behaviour
struct Behaviour_t2850977393;
// UnityEngine.Component
struct Component_t1632713610;
// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t1614977885;
// UnityEngine.Camera
struct Camera_t2839736942;
// UnityEngine.RenderTexture
struct RenderTexture_t971269558;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t32224225;
// System.ArgumentNullException
struct ArgumentNullException_t3857807348;
// System.ArgumentException
struct ArgumentException_t1812645948;
// UnityEngine.Camera[]
struct CameraU5BU5D_t4045078555;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t657915608;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1939304854;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t2909334802;
// UnityEngine.ColorUsageAttribute
struct ColorUsageAttribute_t1858490149;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t381499487;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.Transform
struct Transform_t362059596;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t752340617;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t2358285523;
// UnityEngine.ComputeShader
struct ComputeShader_t532478176;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.ContextMenu
struct ContextMenu_t2541329559;
// UnityEngine.Coroutine
struct Coroutine_t2294981130;
// UnityEngine.CSSLayout.CSSMeasureFunc
struct CSSMeasureFunc_t2094042647;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct Dictionary_2_t3106649840;
// System.WeakReference
struct WeakReference_t3660816289;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t1294734816;
// UnityEngine.CullingGroup
struct CullingGroup_t616951155;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t3981248240;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t2273300952;
// UnityEngine.ILogger
struct ILogger_t1339277430;
// System.Exception
struct Exception_t4086964929;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t2880387906;
// UnityEngine.Logger
struct Logger_t952924384;
// UnityEngine.ILogHandler
struct ILogHandler_t293590501;
// UnityEngine.Display
struct Display_t173707952;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t962499819;
// UnityEngine.RectTransform
struct RectTransform_t859616204;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t288530198;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t27726116;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t3882343965;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t2697346187;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1048652390;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t3442320419;
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t1730346354;
// System.Predicate`1<System.Object>
struct Predicate_1_t3551521503;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t3859867356;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1386075209;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t709177109;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t124089244;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t3587825671;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t4075156644;
// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t3757174574;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t656523105;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2432241475;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3123539581;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t4133256657;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t4123771412;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2603596161;
// System.Reflection.Binder
struct Binder_t1695596754;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t2531273172;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3943516840;
// UnityEngine.Experimental.Rendering.IRenderPipeline
struct IRenderPipeline_t3426467477;
// UnityEngine.Experimental.Rendering.IRenderPipelineAsset
struct IRenderPipelineAsset_t3600099924;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// UnityEngine.Gradient
struct Gradient_t3129154337;
// UnityEngine.Mesh
struct Mesh_t4030024733;
// UnityEngine.Material
struct Material_t2815264910;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t846911691;
// System.ObjectDisposedException
struct ObjectDisposedException_t1428820261;
// UnityEngine.GUIElement
struct GUIElement_t2946029536;
// UnityEngine.GUILayer
struct GUILayer_t478658086;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1708198297;
// UnityEngine.GUITexture
struct GUITexture_t2618579966;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t3937329216;
// UnityEngine.HideInInspector
struct HideInInspector_t3393216074;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t542619783;
// UnityEngine.ImageEffectAllowedInSceneView
struct ImageEffectAllowedInSceneView_t1605044925;
// UnityEngine.ImageEffectOpaque
struct ImageEffectOpaque_t3353908156;
// UnityEngine.ImageEffectTransformsToLDR
struct ImageEffectTransformsToLDR_t1398224731;
// UnityEngine.Touch[]
struct TouchU5BU5D_t170615360;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t3676342093;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t427370092;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t1085454254;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t3274515193;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3840885519;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3030196201;
// System.WeakReference[]
struct WeakReferenceU5BU5D_t3563548860;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t1345636409;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t259346668;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.WeakReference,System.Collections.DictionaryEntry>
struct Transform_1_t1231163164;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t3488379341;
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t4108307263;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t3696554944;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t2692239366;
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t930910456;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3090140724;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3458774959;
// UnityEngine.Gyroscope
struct Gyroscope_t2906904238;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t674921444;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2586599646;
// System.Globalization.TextInfo
struct TextInfo_t441345186;
// System.Globalization.CompareInfo
struct CompareInfo_t2505958045;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1804543921;
// System.Globalization.Calendar
struct Calendar_t1225199120;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// System.Int32
struct Int32_t972567508;
// System.Void
struct Void_t653366341;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t2942892998;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3430223971;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t3112241901;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t1787308802;
// UnityEngine.Display[]
struct DisplayU5BU5D_t2888752273;
// System.DelegateData
struct DelegateData_t1549710636;
// System.Reflection.MemberFilter
struct MemberFilter_t2357198184;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t2394797874;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2209604263;

extern RuntimeClass* KeyframeU5BU5D_t2754673280_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationCurve_t2757045165_il2cpp_TypeInfo_var;
extern const uint32_t AnimationCurve_Linear_m199798804_MetadataUsageId;
extern const uint32_t AnimationCurve_EaseInOut_m1307641671_MetadataUsageId;
extern RuntimeClass* Application_t2283285644_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLowMemory_m839499845_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3714759797_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t972567508_il2cpp_TypeInfo_var;
extern RuntimeClass* Int16_t3196095470_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32_t1721902794_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt16_t14172355_il2cpp_TypeInfo_var;
extern RuntimeClass* Byte_t131097440_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t485236535_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t2625678720_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t3752657471_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_t2204265861_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t622404039_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_t3761276469_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral593551445;
extern Il2CppCodeGenString* _stringLiteral2574419025;
extern Il2CppCodeGenString* _stringLiteral2755344190;
extern Il2CppCodeGenString* _stringLiteral2596912899;
extern Il2CppCodeGenString* _stringLiteral2699167349;
extern Il2CppCodeGenString* _stringLiteral415700502;
extern Il2CppCodeGenString* _stringLiteral2653896445;
extern Il2CppCodeGenString* _stringLiteral2140564651;
extern Il2CppCodeGenString* _stringLiteral2179767692;
extern Il2CppCodeGenString* _stringLiteral750513360;
extern Il2CppCodeGenString* _stringLiteral4185710657;
extern Il2CppCodeGenString* _stringLiteral565947527;
extern Il2CppCodeGenString* _stringLiteral1604226420;
extern Il2CppCodeGenString* _stringLiteral1366464700;
extern Il2CppCodeGenString* _stringLiteral364412306;
extern Il2CppCodeGenString* _stringLiteral591250;
extern Il2CppCodeGenString* _stringLiteral2908764213;
extern const uint32_t Application_ObjectToJSString_m508226240_MetadataUsageId;
extern const uint32_t Application_BuildInvocationForArguments_m4046074230_MetadataUsageId;
extern const uint32_t Application_CallLogCallback_m2569287725_MetadataUsageId;
extern const uint32_t Application_InvokeOnBeforeRender_m2592458376_MetadataUsageId;
extern RuntimeClass* LogType_t3717748719_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m3005294547_MetadataUsageId;
extern RuntimeClass* Action_1_t3739209734_il2cpp_TypeInfo_var;
extern const uint32_t AssetBundleCreateRequest_t1159347791_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleCreateRequest_t1159347791_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t230831548_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t230831548_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t1468153686_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t1468153686_com_FromNativeMethodDefinition_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m14832607_RuntimeMethod_var;
extern const uint32_t AsyncOperation_InvokeCompletionEvent_m2542318802_MetadataUsageId;
extern const RuntimeType* MonoBehaviour_t1618594486_0_0_0_var;
extern const RuntimeType* DisallowMultipleComponent_t2744062746_0_0_0_var;
extern RuntimeClass* Stack_1_t2196794798_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Stack_1__ctor_m4283643689_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m382399138_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m1148751044_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m3176329963_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m22273868_MetadataUsageId;
extern const RuntimeType* RequireComponent_t2180947615_0_0_0_var;
extern RuntimeClass* RequireComponentU5BU5D_t2692239366_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t987895095_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1591575689_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2945177012_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m1572029861_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m4242609707_MetadataUsageId;
extern const RuntimeType* ExecuteInEditMode_t869940717_0_0_0_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m1500407198_MetadataUsageId;
extern RuntimeClass* AttributeHelperEngine_t4266785651_il2cpp_TypeInfo_var;
extern const RuntimeMethod* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t334925532_m3709888332_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m2644510067_MetadataUsageId;
extern RuntimeClass* DisallowMultipleComponentU5BU5D_t4108307263_il2cpp_TypeInfo_var;
extern RuntimeClass* ExecuteInEditModeU5BU5D_t3696554944_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m4186283514_MetadataUsageId;
extern RuntimeClass* Vector3_t1986933152_il2cpp_TypeInfo_var;
extern const uint32_t Bounds__ctor_m1878822430_MetadataUsageId;
extern RuntimeClass* Bounds_t3570137099_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m796471542_MetadataUsageId;
extern const uint32_t Bounds_get_size_m3322399915_MetadataUsageId;
extern const uint32_t Bounds_set_size_m2783247878_MetadataUsageId;
extern const uint32_t Bounds_get_min_m3060588205_MetadataUsageId;
extern const uint32_t Bounds_get_max_m2926624215_MetadataUsageId;
extern const uint32_t Bounds_op_Equality_m3484373173_MetadataUsageId;
extern const uint32_t Bounds_SetMinMax_m1440205454_MetadataUsageId;
extern const uint32_t Bounds_Encapsulate_m608594558_MetadataUsageId;
extern const uint32_t Bounds_Encapsulate_m3380017899_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral656019446;
extern const uint32_t Bounds_ToString_m2675624681_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t3857807348_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t1812645948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2356465786;
extern Il2CppCodeGenString* _stringLiteral330691706;
extern const uint32_t Camera_CalculateFrustumCorners_m1147008066_MetadataUsageId;
extern RuntimeClass* Camera_t2839736942_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m3877489917_MetadataUsageId;
extern const uint32_t Camera_FireOnPreRender_m3318235530_MetadataUsageId;
extern const uint32_t Camera_FireOnPostRender_m3483601918_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4171082218;
extern const uint32_t Color_ToString_m2932792595_MetadataUsageId;
extern RuntimeClass* Color_t2582018970_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m3159899910_MetadataUsageId;
extern RuntimeClass* Vector4_t2436299922_il2cpp_TypeInfo_var;
extern const uint32_t Color_op_Equality_m1988218143_MetadataUsageId;
extern RuntimeClass* Mathf_t2228218122_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m615672158_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3205361995;
extern const uint32_t Color_set_Item_m51493835_MetadataUsageId;
extern const uint32_t Color32_op_Implicit_m1748363328_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3495395157;
extern const uint32_t Color32_ToString_m2415660705_MetadataUsageId;
extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m1184908066_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1719531609;
extern Il2CppCodeGenString* _stringLiteral736989972;
extern Il2CppCodeGenString* _stringLiteral2938520293;
extern Il2CppCodeGenString* _stringLiteral912724562;
extern const uint32_t ComputeBuffer__ctor_m421042894_MetadataUsageId;
extern RuntimeClass* Debug_t3419099923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3043668361;
extern const uint32_t ComputeBuffer_Dispose_m1759304253_MetadataUsageId;
extern RuntimeClass* Marshal_t2040402858_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4132129454;
extern const uint32_t ComputeBuffer_SetData_m1640696821_MetadataUsageId;
extern RuntimeClass* CSSMeasureMode_t4281271123_il2cpp_TypeInfo_var;
extern const uint32_t CSSMeasureFunc_BeginInvoke_m2560653280_MetadataUsageId;
extern RuntimeClass* Native_t4280565868_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureFunc_t2094042647_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1943609909_RuntimeMethod_var;
extern const uint32_t Native_CSSNodeGetMeasureFunc_m1704122301_MetadataUsageId;
extern const uint32_t Native_CSSNodeMeasureInvoke_m55174108_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3106649840_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m83878655_RuntimeMethod_var;
extern const uint32_t Native__cctor_m3749058547_MetadataUsageId;
extern RuntimeClass* StateChanged_t3981248240_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t616951155_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_t616951155_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_Finalize_m4059858500_MetadataUsageId;
extern RuntimeClass* CullingGroupEvent_t655523856_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_SendEvents_m4239256517_MetadataUsageId;
extern const uint32_t StateChanged_BeginInvoke_m3881756820_MetadataUsageId;
extern const uint32_t Debug_get_unityLogger_m1827429516_MetadataUsageId;
extern const uint32_t Debug_DrawLine_m1640476408_MetadataUsageId;
extern const uint32_t Debug_DrawLine_m2699851371_MetadataUsageId;
extern const uint32_t Debug_DrawRay_m4146618718_MetadataUsageId;
extern const uint32_t Debug_DrawRay_m1243172723_MetadataUsageId;
extern const uint32_t Debug_DrawRay_m2541735374_MetadataUsageId;
extern RuntimeClass* ILogger_t1339277430_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m3513156148_MetadataUsageId;
extern const uint32_t Debug_LogError_m3935077967_MetadataUsageId;
extern const uint32_t Debug_LogError_m2374009668_MetadataUsageId;
extern RuntimeClass* ILogHandler_t293590501_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogErrorFormat_m789741991_MetadataUsageId;
extern const uint32_t Debug_LogException_m1994504921_MetadataUsageId;
extern const uint32_t Debug_LogException_m600284527_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m3275468740_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m1775422903_MetadataUsageId;
extern const uint32_t Debug_LogWarningFormat_m1662540287_MetadataUsageId;
extern const uint32_t Debug_LogWarningFormat_m1435679414_MetadataUsageId;
extern RuntimeClass* DebugLogHandler_t2880387906_il2cpp_TypeInfo_var;
extern RuntimeClass* Logger_t952924384_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m1526533645_MetadataUsageId;
extern const uint32_t DebugLogHandler_LogFormat_m366728963_MetadataUsageId;
extern RuntimeClass* Display_t173707952_il2cpp_TypeInfo_var;
extern const uint32_t Display_get_renderingWidth_m730855742_MetadataUsageId;
extern const uint32_t Display_get_renderingHeight_m3547182575_MetadataUsageId;
extern const uint32_t Display_get_systemWidth_m4127972072_MetadataUsageId;
extern const uint32_t Display_get_systemHeight_m3689264282_MetadataUsageId;
extern const uint32_t Display_RelativeMouseAt_m2557534896_MetadataUsageId;
extern RuntimeClass* DisplayU5BU5D_t2888752273_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m165196363_MetadataUsageId;
extern const uint32_t Display_FireDisplaysUpdated_m1779014840_MetadataUsageId;
extern const uint32_t Display__cctor_m4007441132_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral80671380;
extern Il2CppCodeGenString* _stringLiteral1823110067;
extern Il2CppCodeGenString* _stringLiteral2695145057;
extern Il2CppCodeGenString* _stringLiteral809285935;
extern Il2CppCodeGenString* _stringLiteral3069375572;
extern Il2CppCodeGenString* _stringLiteral3912127903;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m1355543587_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2365739746;
extern Il2CppCodeGenString* _stringLiteral1060590976;
extern const uint32_t BaseInvokableCall__ctor_m2058189347_MetadataUsageId;
extern const uint32_t BaseInvokableCall_AllowInvoke_m880256453_MetadataUsageId;
extern const RuntimeType* UnityAction_t992331987_0_0_0_var;
extern RuntimeClass* UnityAction_t992331987_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m4040846306_MetadataUsageId;
extern const uint32_t InvokableCall_add_Delegate_m275363113_MetadataUsageId;
extern const uint32_t InvokableCall_remove_Delegate_m476237524_MetadataUsageId;
extern RuntimeClass* List_1_t3442320419_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3740896317_RuntimeMethod_var;
extern const uint32_t InvokableCallList__ctor_m819252217_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m604383575_RuntimeMethod_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m3955084824_MetadataUsageId;
extern const uint32_t InvokableCallList_AddListener_m1698095211_MetadataUsageId;
extern RuntimeClass* Predicate_1_t1730346354_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m1584988565_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m79931508_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m1617167722_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m422560267_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m127110729_RuntimeMethod_var;
extern const uint32_t InvokableCallList_RemoveListener_m1879627315_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m1306609920_RuntimeMethod_var;
extern const uint32_t InvokableCallList_ClearPersistent_m4038663428_MetadataUsageId;
extern const RuntimeMethod* List_1_AddRange_m3743613219_RuntimeMethod_var;
extern const uint32_t InvokableCallList_Invoke_m371882796_MetadataUsageId;
extern RuntimeClass* ArgumentCache_t288530198_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m3531969768_MetadataUsageId;
extern const uint32_t PersistentCall_IsValid_m1012652439_MetadataUsageId;
extern RuntimeClass* CachedInvokableCall_1_t3587825671_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t4075156644_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t3757174574_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t2432241475_il2cpp_TypeInfo_var;
extern RuntimeClass* InvokableCall_t2697346187_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2264889547_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m4025676652_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2075360316_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2035028495_RuntimeMethod_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m28487261_MetadataUsageId;
extern const RuntimeType* Object_t692178351_0_0_0_var;
extern const RuntimeType* CachedInvokableCall_1_t3314080835_0_0_0_var;
extern const RuntimeType* MethodInfo_t_0_0_0_var;
extern RuntimeClass* BaseInvokableCall_t27726116_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m3632195579_MetadataUsageId;
extern RuntimeClass* List_1_t4123771412_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1780907418_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup__ctor_m741824572_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m3404249592_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3572990374_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1705891082_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m737346653_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup_Initialize_m718681909_MetadataUsageId;
extern const uint32_t UnityEvent__ctor_m661443625_MetadataUsageId;
extern const uint32_t UnityEvent_FindMethod_Impl_m1361289740_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m1183579176_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m2995672099_MetadataUsageId;
extern RuntimeClass* InvokableCallList_t1048652390_il2cpp_TypeInfo_var;
extern RuntimeClass* PersistentCallGroup_t4133256657_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m4162961704_MetadataUsageId;
extern const uint32_t UnityEventBase_FindMethod_m193475207_MetadataUsageId;
extern const RuntimeType* Single_t485236535_0_0_0_var;
extern const RuntimeType* Int32_t972567508_0_0_0_var;
extern const RuntimeType* Boolean_t3624619635_0_0_0_var;
extern const RuntimeType* String_t_0_0_0_var;
extern const uint32_t UnityEventBase_FindMethod_m1074674127_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral434838489;
extern const uint32_t UnityEventBase_ToString_m704360343_MetadataUsageId;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const uint32_t UnityEventBase_GetValidMethodInfo_m435691116_MetadataUsageId;
extern RuntimeClass* RenderPipelineManager_t470120599_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_get_currentPipeline_m3160514901_MetadataUsageId;
extern const uint32_t RenderPipelineManager_set_currentPipeline_m2451449986_MetadataUsageId;
extern RuntimeClass* IRenderPipelineAsset_t3600099924_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_CleanupRenderPipeline_m1052386618_MetadataUsageId;
extern RuntimeClass* IRenderPipeline_t3426467477_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_DoRenderLoop_Internal_m2213944881_MetadataUsageId;
extern const uint32_t RenderPipelineManager_PrepareRenderPipeline_m2025082530_MetadataUsageId;
extern const uint32_t GameObject__ctor_m2888171346_MetadataUsageId;
extern const uint32_t GameObject__ctor_m1313108088_MetadataUsageId;
extern const uint32_t GameObject__ctor_m2577022056_MetadataUsageId;
extern RuntimeClass* Graphics_t3054375344_il2cpp_TypeInfo_var;
extern const uint32_t Graphics_Internal_DrawMeshNow2_m2814279584_MetadataUsageId;
extern const uint32_t Graphics_Blit_m4208839543_MetadataUsageId;
extern const uint32_t Graphics_Blit_m932569440_MetadataUsageId;
extern const uint32_t Graphics_Internal_BlitMaterial_m1646902093_MetadataUsageId;
extern const uint32_t Graphics_BlitMultiTap_m3323122277_MetadataUsageId;
extern const uint32_t Graphics_SetRandomWriteTarget_m2049819467_MetadataUsageId;
extern RuntimeClass* ObjectDisposedException_t1428820261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3980266961;
extern const uint32_t Graphics_SetRandomWriteTarget_m3349738363_MetadataUsageId;
extern const uint32_t Graphics_SetRenderTargetImpl_m1495557259_MetadataUsageId;
extern const uint32_t Graphics_SetRenderTargetImpl_m4294744146_MetadataUsageId;
extern const uint32_t Graphics_SetRenderTargetImpl_m3156622254_MetadataUsageId;
extern const uint32_t Graphics_SetRenderTarget_m3487633971_MetadataUsageId;
extern const uint32_t Graphics_SetRenderTarget_m1420051683_MetadataUsageId;
extern const uint32_t Graphics_DrawMeshNow_m2655627097_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3196149666;
extern const uint32_t Graphics_DrawMeshNow_m46634237_MetadataUsageId;
extern const uint32_t Graphics__cctor_m1827492786_MetadataUsageId;
extern RuntimeClass* GUIStateObjects_t2115760413_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3631383453_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1490102977_RuntimeMethod_var;
extern const uint32_t GUIStateObjects_GetStateObject_m1946903358_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1708198297_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2905489384_RuntimeMethod_var;
extern const uint32_t GUIStateObjects__cctor_m2702348782_MetadataUsageId;
extern RuntimeClass* Input_t1735245659_il2cpp_TypeInfo_var;
extern const uint32_t Input_GetKey_m260282928_MetadataUsageId;
extern const uint32_t Input_GetKeyDown_m2906836562_MetadataUsageId;
extern const uint32_t Input_GetKeyDown_m3096149643_MetadataUsageId;
extern const uint32_t Input_GetKeyUp_m618320100_MetadataUsageId;
extern const uint32_t Input_get_mousePosition_m4034714020_MetadataUsageId;
extern const uint32_t Input_get_mouseScrollDelta_m1084063468_MetadataUsageId;
extern const uint32_t Input_get_acceleration_m850151000_MetadataUsageId;
extern RuntimeClass* TouchU5BU5D_t170615360_il2cpp_TypeInfo_var;
extern const uint32_t Input_get_touches_m3317929777_MetadataUsageId;
extern const uint32_t Input_GetTouch_m3048670206_MetadataUsageId;
extern const uint32_t Input_get_compositionCursorPos_m1395749328_MetadataUsageId;
extern const uint32_t Input_set_compositionCursorPos_m626063828_MetadataUsageId;
extern const uint32_t Input__cctor_m2833959137_MetadataUsageId;
extern RuntimeClass* DefaultValueAttribute_t3676342093_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m3839951568_MetadataUsageId;
extern RuntimeClass* LocalNotification_t1085454254_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m41159022_MetadataUsageId;

struct KeyframeU5BU5D_t2754673280;
struct ObjectU5BU5D_t2737604620;
struct TypeU5BU5D_t1985992169;
struct RequireComponentU5BU5D_t2692239366;
struct DisallowMultipleComponentU5BU5D_t4108307263;
struct ExecuteInEditModeU5BU5D_t3696554944;
struct Vector3U5BU5D_t32224225;
struct CameraU5BU5D_t4045078555;
struct IntPtrU5BU5D_t1552124580;
struct DisplayU5BU5D_t2888752273;
struct ParameterInfoU5BU5D_t3316460985;
struct ParameterModifierU5BU5D_t2531273172;
struct GameObjectU5BU5D_t2988620542;
struct Vector2U5BU5D_t1220531434;
struct RenderBufferU5BU5D_t846911691;
struct TouchU5BU5D_t170615360;


#ifndef U3CMODULEU3E_T1429447266_H
#define U3CMODULEU3E_T1429447266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447266 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447266_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STACK_1_T2196794798_H
#define STACK_1_T2196794798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Type>
struct  Stack_1_t2196794798  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	TypeU5BU5D_t1985992169* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t2196794798, ____array_0)); }
	inline TypeU5BU5D_t1985992169* get__array_0() const { return ____array_0; }
	inline TypeU5BU5D_t1985992169** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(TypeU5BU5D_t1985992169* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t2196794798, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t2196794798, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T2196794798_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef LIST_1_T987895095_H
#define LIST_1_T987895095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Type>
struct  List_1_t987895095  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeU5BU5D_t1985992169* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t987895095, ____items_1)); }
	inline TypeU5BU5D_t1985992169* get__items_1() const { return ____items_1; }
	inline TypeU5BU5D_t1985992169** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeU5BU5D_t1985992169* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t987895095, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t987895095, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t987895095_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TypeU5BU5D_t1985992169* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t987895095_StaticFields, ___EmptyArray_4)); }
	inline TypeU5BU5D_t1985992169* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TypeU5BU5D_t1985992169* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T987895095_H
#ifndef CLASSLIBRARYINITIALIZER_T3967592590_H
#define CLASSLIBRARYINITIALIZER_T3967592590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ClassLibraryInitializer
struct  ClassLibraryInitializer_t3967592590  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSLIBRARYINITIALIZER_T3967592590_H
#ifndef LIST_1_T752340617_H
#define LIST_1_T752340617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_t752340617  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ComponentU5BU5D_t3840885519* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t752340617, ____items_1)); }
	inline ComponentU5BU5D_t3840885519* get__items_1() const { return ____items_1; }
	inline ComponentU5BU5D_t3840885519** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ComponentU5BU5D_t3840885519* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t752340617, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t752340617, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t752340617_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ComponentU5BU5D_t3840885519* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t752340617_StaticFields, ___EmptyArray_4)); }
	inline ComponentU5BU5D_t3840885519* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ComponentU5BU5D_t3840885519** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ComponentU5BU5D_t3840885519* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T752340617_H
#ifndef DICTIONARY_2_T3106649840_H
#define DICTIONARY_2_T3106649840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct  Dictionary_2_t3106649840  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t1965588061* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3030196201* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	IntPtrU5BU5D_t1552124580* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	WeakReferenceU5BU5D_t3563548860* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t259346668 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___table_4)); }
	inline Int32U5BU5D_t1965588061* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t1965588061** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t1965588061* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___linkSlots_5)); }
	inline LinkU5BU5D_t3030196201* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3030196201** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3030196201* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___keySlots_6)); }
	inline IntPtrU5BU5D_t1552124580* get_keySlots_6() const { return ___keySlots_6; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(IntPtrU5BU5D_t1552124580* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___valueSlots_7)); }
	inline WeakReferenceU5BU5D_t3563548860* get_valueSlots_7() const { return ___valueSlots_7; }
	inline WeakReferenceU5BU5D_t3563548860** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(WeakReferenceU5BU5D_t3563548860* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___serialization_info_13)); }
	inline SerializationInfo_t259346668 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t259346668 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t259346668 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3106649840_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1231163164 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3106649840_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1231163164 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1231163164 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1231163164 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3106649840_H
#ifndef CURSOR_T335652096_H
#define CURSOR_T335652096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Cursor
struct  Cursor_t335652096  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSOR_T335652096_H
#ifndef CUSTOMYIELDINSTRUCTION_T2273300952_H
#define CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t2273300952  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifndef DEBUG_T3419099923_H
#define DEBUG_T3419099923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Debug
struct  Debug_t3419099923  : public RuntimeObject
{
public:

public:
};

struct Debug_t3419099923_StaticFields
{
public:
	// UnityEngine.ILogger UnityEngine.Debug::s_Logger
	RuntimeObject* ___s_Logger_0;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Debug_t3419099923_StaticFields, ___s_Logger_0)); }
	inline RuntimeObject* get_s_Logger_0() const { return ___s_Logger_0; }
	inline RuntimeObject** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(RuntimeObject* value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T3419099923_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef DEBUGLOGHANDLER_T2880387906_H
#define DEBUGLOGHANDLER_T2880387906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DebugLogHandler
struct  DebugLogHandler_t2880387906  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGHANDLER_T2880387906_H
#ifndef ARGUMENTCACHE_T288530198_H
#define ARGUMENTCACHE_T288530198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.ArgumentCache
struct  ArgumentCache_t288530198  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.ArgumentCache::m_ObjectArgument
	Object_t692178351 * ___m_ObjectArgument_0;
	// System.String UnityEngine.Events.ArgumentCache::m_ObjectArgumentAssemblyTypeName
	String_t* ___m_ObjectArgumentAssemblyTypeName_1;
	// System.Int32 UnityEngine.Events.ArgumentCache::m_IntArgument
	int32_t ___m_IntArgument_2;
	// System.Single UnityEngine.Events.ArgumentCache::m_FloatArgument
	float ___m_FloatArgument_3;
	// System.String UnityEngine.Events.ArgumentCache::m_StringArgument
	String_t* ___m_StringArgument_4;
	// System.Boolean UnityEngine.Events.ArgumentCache::m_BoolArgument
	bool ___m_BoolArgument_5;

public:
	inline static int32_t get_offset_of_m_ObjectArgument_0() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_ObjectArgument_0)); }
	inline Object_t692178351 * get_m_ObjectArgument_0() const { return ___m_ObjectArgument_0; }
	inline Object_t692178351 ** get_address_of_m_ObjectArgument_0() { return &___m_ObjectArgument_0; }
	inline void set_m_ObjectArgument_0(Object_t692178351 * value)
	{
		___m_ObjectArgument_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgument_0), value);
	}

	inline static int32_t get_offset_of_m_ObjectArgumentAssemblyTypeName_1() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_ObjectArgumentAssemblyTypeName_1)); }
	inline String_t* get_m_ObjectArgumentAssemblyTypeName_1() const { return ___m_ObjectArgumentAssemblyTypeName_1; }
	inline String_t** get_address_of_m_ObjectArgumentAssemblyTypeName_1() { return &___m_ObjectArgumentAssemblyTypeName_1; }
	inline void set_m_ObjectArgumentAssemblyTypeName_1(String_t* value)
	{
		___m_ObjectArgumentAssemblyTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgumentAssemblyTypeName_1), value);
	}

	inline static int32_t get_offset_of_m_IntArgument_2() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_IntArgument_2)); }
	inline int32_t get_m_IntArgument_2() const { return ___m_IntArgument_2; }
	inline int32_t* get_address_of_m_IntArgument_2() { return &___m_IntArgument_2; }
	inline void set_m_IntArgument_2(int32_t value)
	{
		___m_IntArgument_2 = value;
	}

	inline static int32_t get_offset_of_m_FloatArgument_3() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_FloatArgument_3)); }
	inline float get_m_FloatArgument_3() const { return ___m_FloatArgument_3; }
	inline float* get_address_of_m_FloatArgument_3() { return &___m_FloatArgument_3; }
	inline void set_m_FloatArgument_3(float value)
	{
		___m_FloatArgument_3 = value;
	}

	inline static int32_t get_offset_of_m_StringArgument_4() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_StringArgument_4)); }
	inline String_t* get_m_StringArgument_4() const { return ___m_StringArgument_4; }
	inline String_t** get_address_of_m_StringArgument_4() { return &___m_StringArgument_4; }
	inline void set_m_StringArgument_4(String_t* value)
	{
		___m_StringArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringArgument_4), value);
	}

	inline static int32_t get_offset_of_m_BoolArgument_5() { return static_cast<int32_t>(offsetof(ArgumentCache_t288530198, ___m_BoolArgument_5)); }
	inline bool get_m_BoolArgument_5() const { return ___m_BoolArgument_5; }
	inline bool* get_address_of_m_BoolArgument_5() { return &___m_BoolArgument_5; }
	inline void set_m_BoolArgument_5(bool value)
	{
		___m_BoolArgument_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTCACHE_T288530198_H
#ifndef BASEINVOKABLECALL_T27726116_H
#define BASEINVOKABLECALL_T27726116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.BaseInvokableCall
struct  BaseInvokableCall_t27726116  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINVOKABLECALL_T27726116_H
#ifndef LIST_1_T3442320419_H
#define LIST_1_T3442320419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct  List_1_t3442320419  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BaseInvokableCallU5BU5D_t3488379341* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3442320419, ____items_1)); }
	inline BaseInvokableCallU5BU5D_t3488379341* get__items_1() const { return ____items_1; }
	inline BaseInvokableCallU5BU5D_t3488379341** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BaseInvokableCallU5BU5D_t3488379341* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3442320419, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3442320419, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3442320419_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	BaseInvokableCallU5BU5D_t3488379341* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3442320419_StaticFields, ___EmptyArray_4)); }
	inline BaseInvokableCallU5BU5D_t3488379341* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline BaseInvokableCallU5BU5D_t3488379341** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(BaseInvokableCallU5BU5D_t3488379341* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3442320419_H
#ifndef ATTRIBUTEHELPERENGINE_T4266785651_H
#define ATTRIBUTEHELPERENGINE_T4266785651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t4266785651  : public RuntimeObject
{
public:

public:
};

struct AttributeHelperEngine_t4266785651_StaticFields
{
public:
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t4108307263* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t3696554944* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t2692239366* ____requireComponentArray_2;

public:
	inline static int32_t get_offset_of__disallowMultipleComponentArray_0() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t4266785651_StaticFields, ____disallowMultipleComponentArray_0)); }
	inline DisallowMultipleComponentU5BU5D_t4108307263* get__disallowMultipleComponentArray_0() const { return ____disallowMultipleComponentArray_0; }
	inline DisallowMultipleComponentU5BU5D_t4108307263** get_address_of__disallowMultipleComponentArray_0() { return &____disallowMultipleComponentArray_0; }
	inline void set__disallowMultipleComponentArray_0(DisallowMultipleComponentU5BU5D_t4108307263* value)
	{
		____disallowMultipleComponentArray_0 = value;
		Il2CppCodeGenWriteBarrier((&____disallowMultipleComponentArray_0), value);
	}

	inline static int32_t get_offset_of__executeInEditModeArray_1() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t4266785651_StaticFields, ____executeInEditModeArray_1)); }
	inline ExecuteInEditModeU5BU5D_t3696554944* get__executeInEditModeArray_1() const { return ____executeInEditModeArray_1; }
	inline ExecuteInEditModeU5BU5D_t3696554944** get_address_of__executeInEditModeArray_1() { return &____executeInEditModeArray_1; }
	inline void set__executeInEditModeArray_1(ExecuteInEditModeU5BU5D_t3696554944* value)
	{
		____executeInEditModeArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeInEditModeArray_1), value);
	}

	inline static int32_t get_offset_of__requireComponentArray_2() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t4266785651_StaticFields, ____requireComponentArray_2)); }
	inline RequireComponentU5BU5D_t2692239366* get__requireComponentArray_2() const { return ____requireComponentArray_2; }
	inline RequireComponentU5BU5D_t2692239366** get_address_of__requireComponentArray_2() { return &____requireComponentArray_2; }
	inline void set__requireComponentArray_2(RequireComponentU5BU5D_t2692239366* value)
	{
		____requireComponentArray_2 = value;
		Il2CppCodeGenWriteBarrier((&____requireComponentArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEHELPERENGINE_T4266785651_H
#ifndef UNITYEVENTBASE_T124089244_H
#define UNITYEVENTBASE_T124089244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t124089244  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t1048652390 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t4133256657 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t124089244, ___m_Calls_0)); }
	inline InvokableCallList_t1048652390 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t1048652390 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t1048652390 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t124089244, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t4133256657 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t4133256657 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t4133256657 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t124089244, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t124089244, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T124089244_H
#ifndef PERSISTENTCALLGROUP_T4133256657_H
#define PERSISTENTCALLGROUP_T4133256657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCallGroup
struct  PersistentCallGroup_t4133256657  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall> UnityEngine.Events.PersistentCallGroup::m_Calls
	List_1_t4123771412 * ___m_Calls_0;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(PersistentCallGroup_t4133256657, ___m_Calls_0)); }
	inline List_1_t4123771412 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline List_1_t4123771412 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(List_1_t4123771412 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALLGROUP_T4133256657_H
#ifndef LIST_1_T4123771412_H
#define LIST_1_T4123771412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct  List_1_t4123771412  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PersistentCallU5BU5D_t930910456* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4123771412, ____items_1)); }
	inline PersistentCallU5BU5D_t930910456* get__items_1() const { return ____items_1; }
	inline PersistentCallU5BU5D_t930910456** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PersistentCallU5BU5D_t930910456* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4123771412, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4123771412, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4123771412_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PersistentCallU5BU5D_t930910456* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4123771412_StaticFields, ___EmptyArray_4)); }
	inline PersistentCallU5BU5D_t930910456* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PersistentCallU5BU5D_t930910456** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PersistentCallU5BU5D_t930910456* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4123771412_H
#ifndef BINDER_T1695596754_H
#define BINDER_T1695596754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t1695596754  : public RuntimeObject
{
public:

public:
};

struct Binder_t1695596754_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t1695596754 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t1695596754_StaticFields, ___default_binder_0)); }
	inline Binder_t1695596754 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t1695596754 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t1695596754 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T1695596754_H
#ifndef RENDERPIPELINEMANAGER_T470120599_H
#define RENDERPIPELINEMANAGER_T470120599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineManager
struct  RenderPipelineManager_t470120599  : public RuntimeObject
{
public:

public:
};

struct RenderPipelineManager_t470120599_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.IRenderPipelineAsset UnityEngine.Experimental.Rendering.RenderPipelineManager::s_CurrentPipelineAsset
	RuntimeObject* ___s_CurrentPipelineAsset_0;
	// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::<currentPipeline>k__BackingField
	RuntimeObject* ___U3CcurrentPipelineU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_s_CurrentPipelineAsset_0() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t470120599_StaticFields, ___s_CurrentPipelineAsset_0)); }
	inline RuntimeObject* get_s_CurrentPipelineAsset_0() const { return ___s_CurrentPipelineAsset_0; }
	inline RuntimeObject** get_address_of_s_CurrentPipelineAsset_0() { return &___s_CurrentPipelineAsset_0; }
	inline void set_s_CurrentPipelineAsset_0(RuntimeObject* value)
	{
		___s_CurrentPipelineAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentPipelineAsset_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t470120599_StaticFields, ___U3CcurrentPipelineU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CcurrentPipelineU3Ek__BackingField_1() const { return ___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CcurrentPipelineU3Ek__BackingField_1() { return &___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline void set_U3CcurrentPipelineU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CcurrentPipelineU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPipelineU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEMANAGER_T470120599_H
#ifndef GL_T882106759_H
#define GL_T882106759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GL
struct  GL_t882106759  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GL_T882106759_H
#ifndef GRAPHICS_T3054375344_H
#define GRAPHICS_T3054375344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Graphics
struct  Graphics_t3054375344  : public RuntimeObject
{
public:

public:
};

struct Graphics_t3054375344_StaticFields
{
public:
	// System.Int32 UnityEngine.Graphics::kMaxDrawMeshInstanceCount
	int32_t ___kMaxDrawMeshInstanceCount_0;

public:
	inline static int32_t get_offset_of_kMaxDrawMeshInstanceCount_0() { return static_cast<int32_t>(offsetof(Graphics_t3054375344_StaticFields, ___kMaxDrawMeshInstanceCount_0)); }
	inline int32_t get_kMaxDrawMeshInstanceCount_0() const { return ___kMaxDrawMeshInstanceCount_0; }
	inline int32_t* get_address_of_kMaxDrawMeshInstanceCount_0() { return &___kMaxDrawMeshInstanceCount_0; }
	inline void set_kMaxDrawMeshInstanceCount_0(int32_t value)
	{
		___kMaxDrawMeshInstanceCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICS_T3054375344_H
#ifndef GUISTATEOBJECTS_T2115760413_H
#define GUISTATEOBJECTS_T2115760413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStateObjects
struct  GUIStateObjects_t2115760413  : public RuntimeObject
{
public:

public:
};

struct GUIStateObjects_t2115760413_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> UnityEngine.GUIStateObjects::s_StateCache
	Dictionary_2_t1708198297 * ___s_StateCache_0;

public:
	inline static int32_t get_offset_of_s_StateCache_0() { return static_cast<int32_t>(offsetof(GUIStateObjects_t2115760413_StaticFields, ___s_StateCache_0)); }
	inline Dictionary_2_t1708198297 * get_s_StateCache_0() const { return ___s_StateCache_0; }
	inline Dictionary_2_t1708198297 ** get_address_of_s_StateCache_0() { return &___s_StateCache_0; }
	inline void set_s_StateCache_0(Dictionary_2_t1708198297 * value)
	{
		___s_StateCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_StateCache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTATEOBJECTS_T2115760413_H
#ifndef DICTIONARY_2_T1708198297_H
#define DICTIONARY_2_T1708198297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct  Dictionary_2_t1708198297  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t1965588061* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3030196201* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t1965588061* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2737604620* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t259346668 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___table_4)); }
	inline Int32U5BU5D_t1965588061* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t1965588061** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t1965588061* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___linkSlots_5)); }
	inline LinkU5BU5D_t3030196201* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3030196201** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3030196201* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___keySlots_6)); }
	inline Int32U5BU5D_t1965588061* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t1965588061** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t1965588061* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2737604620* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2737604620** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2737604620* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___serialization_info_13)); }
	inline SerializationInfo_t259346668 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t259346668 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t259346668 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1708198297_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3458774959 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1708198297_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3458774959 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3458774959 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3458774959 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1708198297_H
#ifndef GYROSCOPE_T2906904238_H
#define GYROSCOPE_T2906904238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gyroscope
struct  Gyroscope_t2906904238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPE_T2906904238_H
#ifndef INPUT_T1735245659_H
#define INPUT_T1735245659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Input
struct  Input_t1735245659  : public RuntimeObject
{
public:

public:
};

struct Input_t1735245659_StaticFields
{
public:
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t2906904238 * ___m_MainGyro_0;

public:
	inline static int32_t get_offset_of_m_MainGyro_0() { return static_cast<int32_t>(offsetof(Input_t1735245659_StaticFields, ___m_MainGyro_0)); }
	inline Gyroscope_t2906904238 * get_m_MainGyro_0() const { return ___m_MainGyro_0; }
	inline Gyroscope_t2906904238 ** get_address_of_m_MainGyro_0() { return &___m_MainGyro_0; }
	inline void set_m_MainGyro_0(Gyroscope_t2906904238 * value)
	{
		___m_MainGyro_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainGyro_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T1735245659_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef INVOKABLECALLLIST_T1048652390_H
#define INVOKABLECALLLIST_T1048652390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCallList
struct  InvokableCallList_t1048652390  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_PersistentCalls
	List_1_t3442320419 * ___m_PersistentCalls_0;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_RuntimeCalls
	List_1_t3442320419 * ___m_RuntimeCalls_1;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_ExecutingCalls
	List_1_t3442320419 * ___m_ExecutingCalls_2;
	// System.Boolean UnityEngine.Events.InvokableCallList::m_NeedsUpdate
	bool ___m_NeedsUpdate_3;

public:
	inline static int32_t get_offset_of_m_PersistentCalls_0() { return static_cast<int32_t>(offsetof(InvokableCallList_t1048652390, ___m_PersistentCalls_0)); }
	inline List_1_t3442320419 * get_m_PersistentCalls_0() const { return ___m_PersistentCalls_0; }
	inline List_1_t3442320419 ** get_address_of_m_PersistentCalls_0() { return &___m_PersistentCalls_0; }
	inline void set_m_PersistentCalls_0(List_1_t3442320419 * value)
	{
		___m_PersistentCalls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_0), value);
	}

	inline static int32_t get_offset_of_m_RuntimeCalls_1() { return static_cast<int32_t>(offsetof(InvokableCallList_t1048652390, ___m_RuntimeCalls_1)); }
	inline List_1_t3442320419 * get_m_RuntimeCalls_1() const { return ___m_RuntimeCalls_1; }
	inline List_1_t3442320419 ** get_address_of_m_RuntimeCalls_1() { return &___m_RuntimeCalls_1; }
	inline void set_m_RuntimeCalls_1(List_1_t3442320419 * value)
	{
		___m_RuntimeCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_RuntimeCalls_1), value);
	}

	inline static int32_t get_offset_of_m_ExecutingCalls_2() { return static_cast<int32_t>(offsetof(InvokableCallList_t1048652390, ___m_ExecutingCalls_2)); }
	inline List_1_t3442320419 * get_m_ExecutingCalls_2() const { return ___m_ExecutingCalls_2; }
	inline List_1_t3442320419 ** get_address_of_m_ExecutingCalls_2() { return &___m_ExecutingCalls_2; }
	inline void set_m_ExecutingCalls_2(List_1_t3442320419 * value)
	{
		___m_ExecutingCalls_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutingCalls_2), value);
	}

	inline static int32_t get_offset_of_m_NeedsUpdate_3() { return static_cast<int32_t>(offsetof(InvokableCallList_t1048652390, ___m_NeedsUpdate_3)); }
	inline bool get_m_NeedsUpdate_3() const { return ___m_NeedsUpdate_3; }
	inline bool* get_address_of_m_NeedsUpdate_3() { return &___m_NeedsUpdate_3; }
	inline void set_m_NeedsUpdate_3(bool value)
	{
		___m_NeedsUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALLLIST_T1048652390_H
#ifndef YIELDINSTRUCTION_T3270995273_H
#define YIELDINSTRUCTION_T3270995273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3270995273  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3270995273_H
#ifndef NATIVE_T4280565868_H
#define NATIVE_T4280565868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.Native
struct  Native_t4280565868  : public RuntimeObject
{
public:

public:
};

struct Native_t4280565868_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference> UnityEngine.CSSLayout.Native::s_MeasureFunctions
	Dictionary_2_t3106649840 * ___s_MeasureFunctions_0;

public:
	inline static int32_t get_offset_of_s_MeasureFunctions_0() { return static_cast<int32_t>(offsetof(Native_t4280565868_StaticFields, ___s_MeasureFunctions_0)); }
	inline Dictionary_2_t3106649840 * get_s_MeasureFunctions_0() const { return ___s_MeasureFunctions_0; }
	inline Dictionary_2_t3106649840 ** get_address_of_s_MeasureFunctions_0() { return &___s_MeasureFunctions_0; }
	inline void set_s_MeasureFunctions_0(Dictionary_2_t3106649840 * value)
	{
		___s_MeasureFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MeasureFunctions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVE_T4280565868_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef NUMBERFORMATINFO_T674921444_H
#define NUMBERFORMATINFO_T674921444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberFormatInfo
struct  NumberFormatInfo_t674921444  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.NumberFormatInfo::isReadOnly
	bool ___isReadOnly_0;
	// System.String System.Globalization.NumberFormatInfo::decimalFormats
	String_t* ___decimalFormats_1;
	// System.String System.Globalization.NumberFormatInfo::currencyFormats
	String_t* ___currencyFormats_2;
	// System.String System.Globalization.NumberFormatInfo::percentFormats
	String_t* ___percentFormats_3;
	// System.String System.Globalization.NumberFormatInfo::digitPattern
	String_t* ___digitPattern_4;
	// System.String System.Globalization.NumberFormatInfo::zeroPattern
	String_t* ___zeroPattern_5;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyDecimalDigits
	int32_t ___currencyDecimalDigits_6;
	// System.String System.Globalization.NumberFormatInfo::currencyDecimalSeparator
	String_t* ___currencyDecimalSeparator_7;
	// System.String System.Globalization.NumberFormatInfo::currencyGroupSeparator
	String_t* ___currencyGroupSeparator_8;
	// System.Int32[] System.Globalization.NumberFormatInfo::currencyGroupSizes
	Int32U5BU5D_t1965588061* ___currencyGroupSizes_9;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyNegativePattern
	int32_t ___currencyNegativePattern_10;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyPositivePattern
	int32_t ___currencyPositivePattern_11;
	// System.String System.Globalization.NumberFormatInfo::currencySymbol
	String_t* ___currencySymbol_12;
	// System.String System.Globalization.NumberFormatInfo::nanSymbol
	String_t* ___nanSymbol_13;
	// System.String System.Globalization.NumberFormatInfo::negativeInfinitySymbol
	String_t* ___negativeInfinitySymbol_14;
	// System.String System.Globalization.NumberFormatInfo::negativeSign
	String_t* ___negativeSign_15;
	// System.Int32 System.Globalization.NumberFormatInfo::numberDecimalDigits
	int32_t ___numberDecimalDigits_16;
	// System.String System.Globalization.NumberFormatInfo::numberDecimalSeparator
	String_t* ___numberDecimalSeparator_17;
	// System.String System.Globalization.NumberFormatInfo::numberGroupSeparator
	String_t* ___numberGroupSeparator_18;
	// System.Int32[] System.Globalization.NumberFormatInfo::numberGroupSizes
	Int32U5BU5D_t1965588061* ___numberGroupSizes_19;
	// System.Int32 System.Globalization.NumberFormatInfo::numberNegativePattern
	int32_t ___numberNegativePattern_20;
	// System.Int32 System.Globalization.NumberFormatInfo::percentDecimalDigits
	int32_t ___percentDecimalDigits_21;
	// System.String System.Globalization.NumberFormatInfo::percentDecimalSeparator
	String_t* ___percentDecimalSeparator_22;
	// System.String System.Globalization.NumberFormatInfo::percentGroupSeparator
	String_t* ___percentGroupSeparator_23;
	// System.Int32[] System.Globalization.NumberFormatInfo::percentGroupSizes
	Int32U5BU5D_t1965588061* ___percentGroupSizes_24;
	// System.Int32 System.Globalization.NumberFormatInfo::percentNegativePattern
	int32_t ___percentNegativePattern_25;
	// System.Int32 System.Globalization.NumberFormatInfo::percentPositivePattern
	int32_t ___percentPositivePattern_26;
	// System.String System.Globalization.NumberFormatInfo::percentSymbol
	String_t* ___percentSymbol_27;
	// System.String System.Globalization.NumberFormatInfo::perMilleSymbol
	String_t* ___perMilleSymbol_28;
	// System.String System.Globalization.NumberFormatInfo::positiveInfinitySymbol
	String_t* ___positiveInfinitySymbol_29;
	// System.String System.Globalization.NumberFormatInfo::positiveSign
	String_t* ___positiveSign_30;
	// System.String System.Globalization.NumberFormatInfo::ansiCurrencySymbol
	String_t* ___ansiCurrencySymbol_31;
	// System.Int32 System.Globalization.NumberFormatInfo::m_dataItem
	int32_t ___m_dataItem_32;
	// System.Boolean System.Globalization.NumberFormatInfo::m_useUserOverride
	bool ___m_useUserOverride_33;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsNumber
	bool ___validForParseAsNumber_34;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsCurrency
	bool ___validForParseAsCurrency_35;
	// System.String[] System.Globalization.NumberFormatInfo::nativeDigits
	StringU5BU5D_t2511808107* ___nativeDigits_36;
	// System.Int32 System.Globalization.NumberFormatInfo::digitSubstitution
	int32_t ___digitSubstitution_37;

public:
	inline static int32_t get_offset_of_isReadOnly_0() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___isReadOnly_0)); }
	inline bool get_isReadOnly_0() const { return ___isReadOnly_0; }
	inline bool* get_address_of_isReadOnly_0() { return &___isReadOnly_0; }
	inline void set_isReadOnly_0(bool value)
	{
		___isReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_decimalFormats_1() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___decimalFormats_1)); }
	inline String_t* get_decimalFormats_1() const { return ___decimalFormats_1; }
	inline String_t** get_address_of_decimalFormats_1() { return &___decimalFormats_1; }
	inline void set_decimalFormats_1(String_t* value)
	{
		___decimalFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___decimalFormats_1), value);
	}

	inline static int32_t get_offset_of_currencyFormats_2() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyFormats_2)); }
	inline String_t* get_currencyFormats_2() const { return ___currencyFormats_2; }
	inline String_t** get_address_of_currencyFormats_2() { return &___currencyFormats_2; }
	inline void set_currencyFormats_2(String_t* value)
	{
		___currencyFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___currencyFormats_2), value);
	}

	inline static int32_t get_offset_of_percentFormats_3() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentFormats_3)); }
	inline String_t* get_percentFormats_3() const { return ___percentFormats_3; }
	inline String_t** get_address_of_percentFormats_3() { return &___percentFormats_3; }
	inline void set_percentFormats_3(String_t* value)
	{
		___percentFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___percentFormats_3), value);
	}

	inline static int32_t get_offset_of_digitPattern_4() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___digitPattern_4)); }
	inline String_t* get_digitPattern_4() const { return ___digitPattern_4; }
	inline String_t** get_address_of_digitPattern_4() { return &___digitPattern_4; }
	inline void set_digitPattern_4(String_t* value)
	{
		___digitPattern_4 = value;
		Il2CppCodeGenWriteBarrier((&___digitPattern_4), value);
	}

	inline static int32_t get_offset_of_zeroPattern_5() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___zeroPattern_5)); }
	inline String_t* get_zeroPattern_5() const { return ___zeroPattern_5; }
	inline String_t** get_address_of_zeroPattern_5() { return &___zeroPattern_5; }
	inline void set_zeroPattern_5(String_t* value)
	{
		___zeroPattern_5 = value;
		Il2CppCodeGenWriteBarrier((&___zeroPattern_5), value);
	}

	inline static int32_t get_offset_of_currencyDecimalDigits_6() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyDecimalDigits_6)); }
	inline int32_t get_currencyDecimalDigits_6() const { return ___currencyDecimalDigits_6; }
	inline int32_t* get_address_of_currencyDecimalDigits_6() { return &___currencyDecimalDigits_6; }
	inline void set_currencyDecimalDigits_6(int32_t value)
	{
		___currencyDecimalDigits_6 = value;
	}

	inline static int32_t get_offset_of_currencyDecimalSeparator_7() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyDecimalSeparator_7)); }
	inline String_t* get_currencyDecimalSeparator_7() const { return ___currencyDecimalSeparator_7; }
	inline String_t** get_address_of_currencyDecimalSeparator_7() { return &___currencyDecimalSeparator_7; }
	inline void set_currencyDecimalSeparator_7(String_t* value)
	{
		___currencyDecimalSeparator_7 = value;
		Il2CppCodeGenWriteBarrier((&___currencyDecimalSeparator_7), value);
	}

	inline static int32_t get_offset_of_currencyGroupSeparator_8() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyGroupSeparator_8)); }
	inline String_t* get_currencyGroupSeparator_8() const { return ___currencyGroupSeparator_8; }
	inline String_t** get_address_of_currencyGroupSeparator_8() { return &___currencyGroupSeparator_8; }
	inline void set_currencyGroupSeparator_8(String_t* value)
	{
		___currencyGroupSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSeparator_8), value);
	}

	inline static int32_t get_offset_of_currencyGroupSizes_9() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyGroupSizes_9)); }
	inline Int32U5BU5D_t1965588061* get_currencyGroupSizes_9() const { return ___currencyGroupSizes_9; }
	inline Int32U5BU5D_t1965588061** get_address_of_currencyGroupSizes_9() { return &___currencyGroupSizes_9; }
	inline void set_currencyGroupSizes_9(Int32U5BU5D_t1965588061* value)
	{
		___currencyGroupSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSizes_9), value);
	}

	inline static int32_t get_offset_of_currencyNegativePattern_10() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyNegativePattern_10)); }
	inline int32_t get_currencyNegativePattern_10() const { return ___currencyNegativePattern_10; }
	inline int32_t* get_address_of_currencyNegativePattern_10() { return &___currencyNegativePattern_10; }
	inline void set_currencyNegativePattern_10(int32_t value)
	{
		___currencyNegativePattern_10 = value;
	}

	inline static int32_t get_offset_of_currencyPositivePattern_11() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencyPositivePattern_11)); }
	inline int32_t get_currencyPositivePattern_11() const { return ___currencyPositivePattern_11; }
	inline int32_t* get_address_of_currencyPositivePattern_11() { return &___currencyPositivePattern_11; }
	inline void set_currencyPositivePattern_11(int32_t value)
	{
		___currencyPositivePattern_11 = value;
	}

	inline static int32_t get_offset_of_currencySymbol_12() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___currencySymbol_12)); }
	inline String_t* get_currencySymbol_12() const { return ___currencySymbol_12; }
	inline String_t** get_address_of_currencySymbol_12() { return &___currencySymbol_12; }
	inline void set_currencySymbol_12(String_t* value)
	{
		___currencySymbol_12 = value;
		Il2CppCodeGenWriteBarrier((&___currencySymbol_12), value);
	}

	inline static int32_t get_offset_of_nanSymbol_13() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___nanSymbol_13)); }
	inline String_t* get_nanSymbol_13() const { return ___nanSymbol_13; }
	inline String_t** get_address_of_nanSymbol_13() { return &___nanSymbol_13; }
	inline void set_nanSymbol_13(String_t* value)
	{
		___nanSymbol_13 = value;
		Il2CppCodeGenWriteBarrier((&___nanSymbol_13), value);
	}

	inline static int32_t get_offset_of_negativeInfinitySymbol_14() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___negativeInfinitySymbol_14)); }
	inline String_t* get_negativeInfinitySymbol_14() const { return ___negativeInfinitySymbol_14; }
	inline String_t** get_address_of_negativeInfinitySymbol_14() { return &___negativeInfinitySymbol_14; }
	inline void set_negativeInfinitySymbol_14(String_t* value)
	{
		___negativeInfinitySymbol_14 = value;
		Il2CppCodeGenWriteBarrier((&___negativeInfinitySymbol_14), value);
	}

	inline static int32_t get_offset_of_negativeSign_15() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___negativeSign_15)); }
	inline String_t* get_negativeSign_15() const { return ___negativeSign_15; }
	inline String_t** get_address_of_negativeSign_15() { return &___negativeSign_15; }
	inline void set_negativeSign_15(String_t* value)
	{
		___negativeSign_15 = value;
		Il2CppCodeGenWriteBarrier((&___negativeSign_15), value);
	}

	inline static int32_t get_offset_of_numberDecimalDigits_16() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___numberDecimalDigits_16)); }
	inline int32_t get_numberDecimalDigits_16() const { return ___numberDecimalDigits_16; }
	inline int32_t* get_address_of_numberDecimalDigits_16() { return &___numberDecimalDigits_16; }
	inline void set_numberDecimalDigits_16(int32_t value)
	{
		___numberDecimalDigits_16 = value;
	}

	inline static int32_t get_offset_of_numberDecimalSeparator_17() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___numberDecimalSeparator_17)); }
	inline String_t* get_numberDecimalSeparator_17() const { return ___numberDecimalSeparator_17; }
	inline String_t** get_address_of_numberDecimalSeparator_17() { return &___numberDecimalSeparator_17; }
	inline void set_numberDecimalSeparator_17(String_t* value)
	{
		___numberDecimalSeparator_17 = value;
		Il2CppCodeGenWriteBarrier((&___numberDecimalSeparator_17), value);
	}

	inline static int32_t get_offset_of_numberGroupSeparator_18() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___numberGroupSeparator_18)); }
	inline String_t* get_numberGroupSeparator_18() const { return ___numberGroupSeparator_18; }
	inline String_t** get_address_of_numberGroupSeparator_18() { return &___numberGroupSeparator_18; }
	inline void set_numberGroupSeparator_18(String_t* value)
	{
		___numberGroupSeparator_18 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSeparator_18), value);
	}

	inline static int32_t get_offset_of_numberGroupSizes_19() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___numberGroupSizes_19)); }
	inline Int32U5BU5D_t1965588061* get_numberGroupSizes_19() const { return ___numberGroupSizes_19; }
	inline Int32U5BU5D_t1965588061** get_address_of_numberGroupSizes_19() { return &___numberGroupSizes_19; }
	inline void set_numberGroupSizes_19(Int32U5BU5D_t1965588061* value)
	{
		___numberGroupSizes_19 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSizes_19), value);
	}

	inline static int32_t get_offset_of_numberNegativePattern_20() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___numberNegativePattern_20)); }
	inline int32_t get_numberNegativePattern_20() const { return ___numberNegativePattern_20; }
	inline int32_t* get_address_of_numberNegativePattern_20() { return &___numberNegativePattern_20; }
	inline void set_numberNegativePattern_20(int32_t value)
	{
		___numberNegativePattern_20 = value;
	}

	inline static int32_t get_offset_of_percentDecimalDigits_21() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentDecimalDigits_21)); }
	inline int32_t get_percentDecimalDigits_21() const { return ___percentDecimalDigits_21; }
	inline int32_t* get_address_of_percentDecimalDigits_21() { return &___percentDecimalDigits_21; }
	inline void set_percentDecimalDigits_21(int32_t value)
	{
		___percentDecimalDigits_21 = value;
	}

	inline static int32_t get_offset_of_percentDecimalSeparator_22() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentDecimalSeparator_22)); }
	inline String_t* get_percentDecimalSeparator_22() const { return ___percentDecimalSeparator_22; }
	inline String_t** get_address_of_percentDecimalSeparator_22() { return &___percentDecimalSeparator_22; }
	inline void set_percentDecimalSeparator_22(String_t* value)
	{
		___percentDecimalSeparator_22 = value;
		Il2CppCodeGenWriteBarrier((&___percentDecimalSeparator_22), value);
	}

	inline static int32_t get_offset_of_percentGroupSeparator_23() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentGroupSeparator_23)); }
	inline String_t* get_percentGroupSeparator_23() const { return ___percentGroupSeparator_23; }
	inline String_t** get_address_of_percentGroupSeparator_23() { return &___percentGroupSeparator_23; }
	inline void set_percentGroupSeparator_23(String_t* value)
	{
		___percentGroupSeparator_23 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSeparator_23), value);
	}

	inline static int32_t get_offset_of_percentGroupSizes_24() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentGroupSizes_24)); }
	inline Int32U5BU5D_t1965588061* get_percentGroupSizes_24() const { return ___percentGroupSizes_24; }
	inline Int32U5BU5D_t1965588061** get_address_of_percentGroupSizes_24() { return &___percentGroupSizes_24; }
	inline void set_percentGroupSizes_24(Int32U5BU5D_t1965588061* value)
	{
		___percentGroupSizes_24 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSizes_24), value);
	}

	inline static int32_t get_offset_of_percentNegativePattern_25() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentNegativePattern_25)); }
	inline int32_t get_percentNegativePattern_25() const { return ___percentNegativePattern_25; }
	inline int32_t* get_address_of_percentNegativePattern_25() { return &___percentNegativePattern_25; }
	inline void set_percentNegativePattern_25(int32_t value)
	{
		___percentNegativePattern_25 = value;
	}

	inline static int32_t get_offset_of_percentPositivePattern_26() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentPositivePattern_26)); }
	inline int32_t get_percentPositivePattern_26() const { return ___percentPositivePattern_26; }
	inline int32_t* get_address_of_percentPositivePattern_26() { return &___percentPositivePattern_26; }
	inline void set_percentPositivePattern_26(int32_t value)
	{
		___percentPositivePattern_26 = value;
	}

	inline static int32_t get_offset_of_percentSymbol_27() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___percentSymbol_27)); }
	inline String_t* get_percentSymbol_27() const { return ___percentSymbol_27; }
	inline String_t** get_address_of_percentSymbol_27() { return &___percentSymbol_27; }
	inline void set_percentSymbol_27(String_t* value)
	{
		___percentSymbol_27 = value;
		Il2CppCodeGenWriteBarrier((&___percentSymbol_27), value);
	}

	inline static int32_t get_offset_of_perMilleSymbol_28() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___perMilleSymbol_28)); }
	inline String_t* get_perMilleSymbol_28() const { return ___perMilleSymbol_28; }
	inline String_t** get_address_of_perMilleSymbol_28() { return &___perMilleSymbol_28; }
	inline void set_perMilleSymbol_28(String_t* value)
	{
		___perMilleSymbol_28 = value;
		Il2CppCodeGenWriteBarrier((&___perMilleSymbol_28), value);
	}

	inline static int32_t get_offset_of_positiveInfinitySymbol_29() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___positiveInfinitySymbol_29)); }
	inline String_t* get_positiveInfinitySymbol_29() const { return ___positiveInfinitySymbol_29; }
	inline String_t** get_address_of_positiveInfinitySymbol_29() { return &___positiveInfinitySymbol_29; }
	inline void set_positiveInfinitySymbol_29(String_t* value)
	{
		___positiveInfinitySymbol_29 = value;
		Il2CppCodeGenWriteBarrier((&___positiveInfinitySymbol_29), value);
	}

	inline static int32_t get_offset_of_positiveSign_30() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___positiveSign_30)); }
	inline String_t* get_positiveSign_30() const { return ___positiveSign_30; }
	inline String_t** get_address_of_positiveSign_30() { return &___positiveSign_30; }
	inline void set_positiveSign_30(String_t* value)
	{
		___positiveSign_30 = value;
		Il2CppCodeGenWriteBarrier((&___positiveSign_30), value);
	}

	inline static int32_t get_offset_of_ansiCurrencySymbol_31() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___ansiCurrencySymbol_31)); }
	inline String_t* get_ansiCurrencySymbol_31() const { return ___ansiCurrencySymbol_31; }
	inline String_t** get_address_of_ansiCurrencySymbol_31() { return &___ansiCurrencySymbol_31; }
	inline void set_ansiCurrencySymbol_31(String_t* value)
	{
		___ansiCurrencySymbol_31 = value;
		Il2CppCodeGenWriteBarrier((&___ansiCurrencySymbol_31), value);
	}

	inline static int32_t get_offset_of_m_dataItem_32() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___m_dataItem_32)); }
	inline int32_t get_m_dataItem_32() const { return ___m_dataItem_32; }
	inline int32_t* get_address_of_m_dataItem_32() { return &___m_dataItem_32; }
	inline void set_m_dataItem_32(int32_t value)
	{
		___m_dataItem_32 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_33() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___m_useUserOverride_33)); }
	inline bool get_m_useUserOverride_33() const { return ___m_useUserOverride_33; }
	inline bool* get_address_of_m_useUserOverride_33() { return &___m_useUserOverride_33; }
	inline void set_m_useUserOverride_33(bool value)
	{
		___m_useUserOverride_33 = value;
	}

	inline static int32_t get_offset_of_validForParseAsNumber_34() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___validForParseAsNumber_34)); }
	inline bool get_validForParseAsNumber_34() const { return ___validForParseAsNumber_34; }
	inline bool* get_address_of_validForParseAsNumber_34() { return &___validForParseAsNumber_34; }
	inline void set_validForParseAsNumber_34(bool value)
	{
		___validForParseAsNumber_34 = value;
	}

	inline static int32_t get_offset_of_validForParseAsCurrency_35() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___validForParseAsCurrency_35)); }
	inline bool get_validForParseAsCurrency_35() const { return ___validForParseAsCurrency_35; }
	inline bool* get_address_of_validForParseAsCurrency_35() { return &___validForParseAsCurrency_35; }
	inline void set_validForParseAsCurrency_35(bool value)
	{
		___validForParseAsCurrency_35 = value;
	}

	inline static int32_t get_offset_of_nativeDigits_36() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___nativeDigits_36)); }
	inline StringU5BU5D_t2511808107* get_nativeDigits_36() const { return ___nativeDigits_36; }
	inline StringU5BU5D_t2511808107** get_address_of_nativeDigits_36() { return &___nativeDigits_36; }
	inline void set_nativeDigits_36(StringU5BU5D_t2511808107* value)
	{
		___nativeDigits_36 = value;
		Il2CppCodeGenWriteBarrier((&___nativeDigits_36), value);
	}

	inline static int32_t get_offset_of_digitSubstitution_37() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444, ___digitSubstitution_37)); }
	inline int32_t get_digitSubstitution_37() const { return ___digitSubstitution_37; }
	inline int32_t* get_address_of_digitSubstitution_37() { return &___digitSubstitution_37; }
	inline void set_digitSubstitution_37(int32_t value)
	{
		___digitSubstitution_37 = value;
	}
};

struct NumberFormatInfo_t674921444_StaticFields
{
public:
	// System.String[] System.Globalization.NumberFormatInfo::invariantNativeDigits
	StringU5BU5D_t2511808107* ___invariantNativeDigits_38;

public:
	inline static int32_t get_offset_of_invariantNativeDigits_38() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t674921444_StaticFields, ___invariantNativeDigits_38)); }
	inline StringU5BU5D_t2511808107* get_invariantNativeDigits_38() const { return ___invariantNativeDigits_38; }
	inline StringU5BU5D_t2511808107** get_address_of_invariantNativeDigits_38() { return &___invariantNativeDigits_38; }
	inline void set_invariantNativeDigits_38(StringU5BU5D_t2511808107* value)
	{
		___invariantNativeDigits_38 = value;
		Il2CppCodeGenWriteBarrier((&___invariantNativeDigits_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERFORMATINFO_T674921444_H
#ifndef STRINGBUILDER_T622404039_H
#define STRINGBUILDER_T622404039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t622404039  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T622404039_H
#ifndef APPLICATION_T2283285644_H
#define APPLICATION_T2283285644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application
struct  Application_t2283285644  : public RuntimeObject
{
public:

public:
};

struct Application_t2283285644_StaticFields
{
public:
	// UnityEngine.Application/LowMemoryCallback UnityEngine.Application::lowMemory
	LowMemoryCallback_t3854352520 * ___lowMemory_0;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandler
	LogCallback_t204719788 * ___s_LogCallbackHandler_1;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandlerThreaded
	LogCallback_t204719788 * ___s_LogCallbackHandlerThreaded_2;
	// UnityEngine.Events.UnityAction UnityEngine.Application::onBeforeRender
	UnityAction_t992331987 * ___onBeforeRender_3;

public:
	inline static int32_t get_offset_of_lowMemory_0() { return static_cast<int32_t>(offsetof(Application_t2283285644_StaticFields, ___lowMemory_0)); }
	inline LowMemoryCallback_t3854352520 * get_lowMemory_0() const { return ___lowMemory_0; }
	inline LowMemoryCallback_t3854352520 ** get_address_of_lowMemory_0() { return &___lowMemory_0; }
	inline void set_lowMemory_0(LowMemoryCallback_t3854352520 * value)
	{
		___lowMemory_0 = value;
		Il2CppCodeGenWriteBarrier((&___lowMemory_0), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandler_1() { return static_cast<int32_t>(offsetof(Application_t2283285644_StaticFields, ___s_LogCallbackHandler_1)); }
	inline LogCallback_t204719788 * get_s_LogCallbackHandler_1() const { return ___s_LogCallbackHandler_1; }
	inline LogCallback_t204719788 ** get_address_of_s_LogCallbackHandler_1() { return &___s_LogCallbackHandler_1; }
	inline void set_s_LogCallbackHandler_1(LogCallback_t204719788 * value)
	{
		___s_LogCallbackHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandler_1), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandlerThreaded_2() { return static_cast<int32_t>(offsetof(Application_t2283285644_StaticFields, ___s_LogCallbackHandlerThreaded_2)); }
	inline LogCallback_t204719788 * get_s_LogCallbackHandlerThreaded_2() const { return ___s_LogCallbackHandlerThreaded_2; }
	inline LogCallback_t204719788 ** get_address_of_s_LogCallbackHandlerThreaded_2() { return &___s_LogCallbackHandlerThreaded_2; }
	inline void set_s_LogCallbackHandlerThreaded_2(LogCallback_t204719788 * value)
	{
		___s_LogCallbackHandlerThreaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandlerThreaded_2), value);
	}

	inline static int32_t get_offset_of_onBeforeRender_3() { return static_cast<int32_t>(offsetof(Application_t2283285644_StaticFields, ___onBeforeRender_3)); }
	inline UnityAction_t992331987 * get_onBeforeRender_3() const { return ___onBeforeRender_3; }
	inline UnityAction_t992331987 ** get_address_of_onBeforeRender_3() { return &___onBeforeRender_3; }
	inline void set_onBeforeRender_3(UnityAction_t992331987 * value)
	{
		___onBeforeRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeRender_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATION_T2283285644_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef CULTUREINFO_T2625678720_H
#define CULTUREINFO_T2625678720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t2625678720  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t674921444 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2586599646 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t441345186 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t2505958045 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t1804543921* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t2625678720 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1225199120 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t434619169* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___numInfo_14)); }
	inline NumberFormatInfo_t674921444 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t674921444 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t674921444 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t2586599646 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t2586599646 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t2586599646 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___textInfo_16)); }
	inline TextInfo_t441345186 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t441345186 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t441345186 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___compareInfo_26)); }
	inline CompareInfo_t2505958045 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t2505958045 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t2505958045 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t1804543921* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t1804543921** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t1804543921* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___parent_culture_30)); }
	inline CultureInfo_t2625678720 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t2625678720 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t2625678720 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___calendar_32)); }
	inline Calendar_t1225199120 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t1225199120 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t1225199120 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t434619169* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t434619169** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t434619169* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t2625678720_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t2625678720 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t2354558714 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t2354558714 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t2625678720 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t2625678720 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t2625678720 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t2354558714 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t2354558714 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t2354558714 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t2354558714 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t2354558714 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t2354558714 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T2625678720_H
#ifndef KEYFRAME_T3310169645_H
#define KEYFRAME_T3310169645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t3310169645 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t3310169645, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t3310169645, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t3310169645, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t3310169645, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T3310169645_H
#ifndef PROPERTYATTRIBUTE_T381499487_H
#define PROPERTYATTRIBUTE_T381499487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t381499487  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T381499487_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef COLOR32_T3428513655_H
#define COLOR32_T3428513655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t3428513655 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t3428513655, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T3428513655_H
#ifndef TIMESPAN_T2821448670_H
#define TIMESPAN_T2821448670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2821448670 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2821448670_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2821448670  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2821448670  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2821448670  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2821448670  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2821448670 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2821448670  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2821448670  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2821448670 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2821448670  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2821448670  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2821448670 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2821448670  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2821448670_H
#ifndef CONTEXTMENU_T2541329559_H
#define CONTEXTMENU_T2541329559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContextMenu
struct  ContextMenu_t2541329559  : public Attribute_t1924466020
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_t2541329559, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___menuItem_0), value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_t2541329559, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_t2541329559, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTMENU_T2541329559_H
#ifndef INVOKABLECALL_T2697346187_H
#define INVOKABLECALL_T2697346187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall
struct  InvokableCall_t2697346187  : public BaseInvokableCall_t27726116
{
public:
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t992331987 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_t2697346187, ___Delegate_0)); }
	inline UnityAction_t992331987 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_t992331987 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_t992331987 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_T2697346187_H
#ifndef WRITEONLYATTRIBUTE_T2142990926_H
#define WRITEONLYATTRIBUTE_T2142990926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.WriteOnlyAttribute
struct  WriteOnlyAttribute_t2142990926  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEONLYATTRIBUTE_T2142990926_H
#ifndef READWRITEATTRIBUTE_T2094481391_H
#define READWRITEATTRIBUTE_T2094481391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadWriteAttribute
struct  ReadWriteAttribute_t2094481391  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READWRITEATTRIBUTE_T2094481391_H
#ifndef READONLYATTRIBUTE_T309474353_H
#define READONLYATTRIBUTE_T309474353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadOnlyAttribute
struct  ReadOnlyAttribute_t309474353  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T309474353_H
#ifndef NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1692634895_H
#define NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1692634895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute
struct  NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1692634895  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1692634895_H
#ifndef NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3194329370_H
#define NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3194329370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute
struct  NativeContainerSupportsAtomicWriteAttribute_t3194329370  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3194329370_H
#ifndef VECTOR4_T2436299922_H
#define VECTOR4_T2436299922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2436299922 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2436299922_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2436299922  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2436299922  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2436299922  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2436299922  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2436299922  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2436299922 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2436299922  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___oneVector_6)); }
	inline Vector4_t2436299922  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2436299922 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2436299922  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2436299922  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2436299922 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2436299922  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2436299922  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2436299922 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2436299922  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2436299922_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef CSSSIZE_T2672652373_H
#define CSSSIZE_T2672652373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t2672652373 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t2672652373, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t2672652373, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T2672652373_H
#ifndef PARAMETERMODIFIER_T999651945_H
#define PARAMETERMODIFIER_T999651945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t999651945 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t698278498* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t999651945, ____byref_0)); }
	inline BooleanU5BU5D_t698278498* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t698278498** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t698278498* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t999651945_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t999651945_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T999651945_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2554077623_H
#define DRIVENRECTTRANSFORMTRACKER_T2554077623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2554077623 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2554077623_H
#ifndef ASSEMBLYISEDITORASSEMBLY_T3325326876_H
#define ASSEMBLYISEDITORASSEMBLY_T3325326876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssemblyIsEditorAssembly
struct  AssemblyIsEditorAssembly_t3325326876  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYISEDITORASSEMBLY_T3325326876_H
#ifndef INT16_T3196095470_H
#define INT16_T3196095470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t3196095470 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t3196095470, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T3196095470_H
#ifndef UINT32_T1721902794_H
#define UINT32_T1721902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1721902794 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1721902794, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1721902794_H
#ifndef UINT16_T14172355_H
#define UINT16_T14172355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t14172355 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt16_t14172355, ___m_value_2)); }
	inline uint16_t get_m_value_2() const { return ___m_value_2; }
	inline uint16_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint16_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T14172355_H
#ifndef METHODBASE_T2927642150_H
#define METHODBASE_T2927642150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t2927642150  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T2927642150_H
#ifndef BYTE_T131097440_H
#define BYTE_T131097440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t131097440 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t131097440, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T131097440_H
#ifndef CULLINGGROUPEVENT_T655523856_H
#define CULLINGGROUPEVENT_T655523856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroupEvent
struct  CullingGroupEvent_t655523856 
{
public:
	// System.Int32 UnityEngine.CullingGroupEvent::m_Index
	int32_t ___m_Index_0;
	// System.Byte UnityEngine.CullingGroupEvent::m_PrevState
	uint8_t ___m_PrevState_1;
	// System.Byte UnityEngine.CullingGroupEvent::m_ThisState
	uint8_t ___m_ThisState_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t655523856, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_1() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t655523856, ___m_PrevState_1)); }
	inline uint8_t get_m_PrevState_1() const { return ___m_PrevState_1; }
	inline uint8_t* get_address_of_m_PrevState_1() { return &___m_PrevState_1; }
	inline void set_m_PrevState_1(uint8_t value)
	{
		___m_PrevState_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisState_2() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t655523856, ___m_ThisState_2)); }
	inline uint8_t get_m_ThisState_2() const { return ___m_ThisState_2; }
	inline uint8_t* get_address_of_m_ThisState_2() { return &___m_ThisState_2; }
	inline void set_m_ThisState_2(uint8_t value)
	{
		___m_ThisState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGGROUPEVENT_T655523856_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef ENUMERATOR_T3899639936_H
#define ENUMERATOR_T3899639936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct  Enumerator_t3899639936 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4123771412 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PersistentCall_t709177109 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3899639936, ___l_0)); }
	inline List_1_t4123771412 * get_l_0() const { return ___l_0; }
	inline List_1_t4123771412 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4123771412 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3899639936, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3899639936, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3899639936, ___current_3)); }
	inline PersistentCall_t709177109 * get_current_3() const { return ___current_3; }
	inline PersistentCall_t709177109 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PersistentCall_t709177109 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3899639936_H
#ifndef ENUMERATOR_T744396796_H
#define ENUMERATOR_T744396796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t744396796 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t968528272 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___l_0)); }
	inline List_1_t968528272 * get_l_0() const { return ___l_0; }
	inline List_1_t968528272 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t968528272 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T744396796_H
#ifndef UNITYEVENT_T2603596161_H
#define UNITYEVENT_T2603596161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2603596161  : public UnityEventBase_t124089244
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2737604620* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2603596161, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2737604620* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2737604620** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2737604620* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2603596161_H
#ifndef NATIVECONTAINERATTRIBUTE_T3321054552_H
#define NATIVECONTAINERATTRIBUTE_T3321054552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerAttribute
struct  NativeContainerAttribute_t3321054552  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERATTRIBUTE_T3321054552_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2934055589_H
#define DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2934055589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.DeallocateOnJobCompletionAttribute
struct  DeallocateOnJobCompletionAttribute_t2934055589  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2934055589_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef ADDCOMPONENTMENU_T2898687950_H
#define ADDCOMPONENTMENU_T2898687950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t2898687950  : public Attribute_t1924466020
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t2898687950, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AddComponentMenu_0), value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t2898687950, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENTMENU_T2898687950_H
#ifndef UNMARSHALLEDATTRIBUTE_T1614977885_H
#define UNMARSHALLEDATTRIBUTE_T1614977885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bindings.UnmarshalledAttribute
struct  UnmarshalledAttribute_t1614977885  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEDATTRIBUTE_T1614977885_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef EXECUTEINEDITMODE_T869940717_H
#define EXECUTEINEDITMODE_T869940717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExecuteInEditMode
struct  ExecuteInEditMode_t869940717  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEINEDITMODE_T869940717_H
#ifndef DISALLOWMULTIPLECOMPONENT_T2744062746_H
#define DISALLOWMULTIPLECOMPONENT_T2744062746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DisallowMultipleComponent
struct  DisallowMultipleComponent_t2744062746  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISALLOWMULTIPLECOMPONENT_T2744062746_H
#ifndef DEFAULTEXECUTIONORDER_T334925532_H
#define DEFAULTEXECUTIONORDER_T334925532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DefaultExecutionOrder
struct  DefaultExecutionOrder_t334925532  : public Attribute_t1924466020
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t334925532, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXECUTIONORDER_T334925532_H
#ifndef REQUIRECOMPONENT_T2180947615_H
#define REQUIRECOMPONENT_T2180947615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t2180947615  : public Attribute_t1924466020
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t2180947615, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t2180947615, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t2180947615, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T2180947615_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef SCENE_T965641407_H
#define SCENE_T965641407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t965641407 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t965641407, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T965641407_H
#ifndef GCHANDLE_T4151872729_H
#define GCHANDLE_T4151872729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t4151872729 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t4151872729, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T4151872729_H
#ifndef INVOKABLECALL_1_T2794311104_H
#define INVOKABLECALL_1_T2794311104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t2794311104  : public BaseInvokableCall_t27726116
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2942892998 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2794311104, ___Delegate_0)); }
	inline UnityAction_1_t2942892998 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2942892998 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2942892998 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2794311104_H
#ifndef INVOKABLECALL_1_T3281642077_H
#define INVOKABLECALL_1_T3281642077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t3281642077  : public BaseInvokableCall_t27726116
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t3430223971 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t3281642077, ___Delegate_0)); }
	inline UnityAction_1_t3430223971 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t3430223971 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t3430223971 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T3281642077_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INVOKABLECALL_1_T2963660007_H
#define INVOKABLECALL_1_T2963660007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t2963660007  : public BaseInvokableCall_t27726116
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t3112241901 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2963660007, ___Delegate_0)); }
	inline UnityAction_1_t3112241901 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t3112241901 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t3112241901 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2963660007_H
#ifndef INVOKABLECALL_1_T1638726908_H
#define INVOKABLECALL_1_T1638726908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct  InvokableCall_1_t1638726908  : public BaseInvokableCall_t27726116
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t1787308802 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t1638726908, ___Delegate_0)); }
	inline UnityAction_1_t1787308802 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t1787308802 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t1787308802 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T1638726908_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef INT64_T1541278839_H
#define INT64_T1541278839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t1541278839 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t1541278839, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T1541278839_H
#ifndef DOUBLE_T3752657471_H
#define DOUBLE_T3752657471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3752657471 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3752657471, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3752657471_H
#ifndef HIDEININSPECTOR_T3393216074_H
#define HIDEININSPECTOR_T3393216074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideInInspector
struct  HideInInspector_t3393216074  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEININSPECTOR_T3393216074_H
#ifndef IMAGEEFFECTOPAQUE_T3353908156_H
#define IMAGEEFFECTOPAQUE_T3353908156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ImageEffectOpaque
struct  ImageEffectOpaque_t3353908156  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTOPAQUE_T3353908156_H
#ifndef IMAGEEFFECTALLOWEDINSCENEVIEW_T1605044925_H
#define IMAGEEFFECTALLOWEDINSCENEVIEW_T1605044925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ImageEffectAllowedInSceneView
struct  ImageEffectAllowedInSceneView_t1605044925  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTALLOWEDINSCENEVIEW_T1605044925_H
#ifndef DEFAULTVALUEATTRIBUTE_T3676342093_H
#define DEFAULTVALUEATTRIBUTE_T3676342093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t3676342093  : public Attribute_t1924466020
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t3676342093, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T3676342093_H
#ifndef EXCLUDEFROMDOCSATTRIBUTE_T427370092_H
#define EXCLUDEFROMDOCSATTRIBUTE_T427370092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct  ExcludeFromDocsAttribute_t427370092  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMDOCSATTRIBUTE_T427370092_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef IL2CPPSTRUCTALIGNMENTATTRIBUTE_T542619783_H
#define IL2CPPSTRUCTALIGNMENTATTRIBUTE_T542619783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IL2CPPStructAlignmentAttribute
struct  IL2CPPStructAlignmentAttribute_t542619783  : public Attribute_t1924466020
{
public:
	// System.Int32 UnityEngine.IL2CPPStructAlignmentAttribute::Align
	int32_t ___Align_0;

public:
	inline static int32_t get_offset_of_Align_0() { return static_cast<int32_t>(offsetof(IL2CPPStructAlignmentAttribute_t542619783, ___Align_0)); }
	inline int32_t get_Align_0() const { return ___Align_0; }
	inline int32_t* get_address_of_Align_0() { return &___Align_0; }
	inline void set_Align_0(int32_t value)
	{
		___Align_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPSTRUCTALIGNMENTATTRIBUTE_T542619783_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef IMAGEEFFECTTRANSFORMSTOLDR_T1398224731_H
#define IMAGEEFFECTTRANSFORMSTOLDR_T1398224731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ImageEffectTransformsToLDR
struct  ImageEffectTransformsToLDR_t1398224731  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTTRANSFORMSTOLDR_T1398224731_H
#ifndef HIDEFLAGS_T2989707239_H
#define HIDEFLAGS_T2989707239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t2989707239 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t2989707239, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T2989707239_H
#ifndef CACHEDINVOKABLECALL_1_T4075156644_H
#define CACHEDINVOKABLECALL_1_T4075156644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t4075156644  : public InvokableCall_1_t3281642077
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t2737604620* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t4075156644, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t2737604620* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t2737604620** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t2737604620* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T4075156644_H
#ifndef RUNTIMEPLATFORM_T2843441307_H
#define RUNTIMEPLATFORM_T2843441307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t2843441307 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t2843441307, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T2843441307_H
#ifndef UNITYEVENTCALLSTATE_T2476497633_H
#define UNITYEVENTCALLSTATE_T2476497633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventCallState
struct  UnityEventCallState_t2476497633 
{
public:
	// System.Int32 UnityEngine.Events.UnityEventCallState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityEventCallState_t2476497633, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTCALLSTATE_T2476497633_H
#ifndef PARAMETERATTRIBUTES_T2006579846_H
#define PARAMETERATTRIBUTES_T2006579846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t2006579846 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t2006579846, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T2006579846_H
#ifndef INVALIDOPERATIONEXCEPTION_T3358289486_H
#define INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t3358289486  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifndef PERSISTENTLISTENERMODE_T2019451321_H
#define PERSISTENTLISTENERMODE_T2019451321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentListenerMode
struct  PersistentListenerMode_t2019451321 
{
public:
	// System.Int32 UnityEngine.Events.PersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PersistentListenerMode_t2019451321, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTLISTENERMODE_T2019451321_H
#ifndef TOUCHPHASE_T2806880854_H
#define TOUCHPHASE_T2806880854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2806880854 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2806880854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2806880854_H
#ifndef CACHEDINVOKABLECALL_1_T3587825671_H
#define CACHEDINVOKABLECALL_1_T3587825671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t3587825671  : public InvokableCall_1_t2794311104
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t2737604620* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3587825671, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t2737604620* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t2737604620** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t2737604620* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3587825671_H
#ifndef FOGMODE_T2148492286_H
#define FOGMODE_T2148492286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FogMode
struct  FogMode_t2148492286 
{
public:
	// System.Int32 UnityEngine.FogMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FogMode_t2148492286, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGMODE_T2148492286_H
#ifndef IMECOMPOSITIONMODE_T735113699_H
#define IMECOMPOSITIONMODE_T735113699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IMECompositionMode
struct  IMECompositionMode_t735113699 
{
public:
	// System.Int32 UnityEngine.IMECompositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IMECompositionMode_t735113699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMECOMPOSITIONMODE_T735113699_H
#ifndef CACHEDINVOKABLECALL_1_T2432241475_H
#define CACHEDINVOKABLECALL_1_T2432241475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct  CachedInvokableCall_1_t2432241475  : public InvokableCall_1_t1638726908
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t2737604620* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t2432241475, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t2737604620* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t2737604620** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t2737604620* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T2432241475_H
#ifndef CONSTRUCTORINFO_T3123539581_H
#define CONSTRUCTORINFO_T3123539581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t3123539581  : public MethodBase_t2927642150
{
public:

public:
};

struct ConstructorInfo_t3123539581_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3123539581_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3123539581_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T3123539581_H
#ifndef KEYCODE_T1957070546_H
#define KEYCODE_T1957070546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t1957070546 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t1957070546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T1957070546_H
#ifndef RENDERBUFFER_T4121715582_H
#define RENDERBUFFER_T4121715582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderBuffer
struct  RenderBuffer_t4121715582 
{
public:
	// System.Int32 UnityEngine.RenderBuffer::m_RenderTextureInstanceID
	int32_t ___m_RenderTextureInstanceID_0;
	// System.IntPtr UnityEngine.RenderBuffer::m_BufferPtr
	intptr_t ___m_BufferPtr_1;

public:
	inline static int32_t get_offset_of_m_RenderTextureInstanceID_0() { return static_cast<int32_t>(offsetof(RenderBuffer_t4121715582, ___m_RenderTextureInstanceID_0)); }
	inline int32_t get_m_RenderTextureInstanceID_0() const { return ___m_RenderTextureInstanceID_0; }
	inline int32_t* get_address_of_m_RenderTextureInstanceID_0() { return &___m_RenderTextureInstanceID_0; }
	inline void set_m_RenderTextureInstanceID_0(int32_t value)
	{
		___m_RenderTextureInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_BufferPtr_1() { return static_cast<int32_t>(offsetof(RenderBuffer_t4121715582, ___m_BufferPtr_1)); }
	inline intptr_t get_m_BufferPtr_1() const { return ___m_BufferPtr_1; }
	inline intptr_t* get_address_of_m_BufferPtr_1() { return &___m_BufferPtr_1; }
	inline void set_m_BufferPtr_1(intptr_t value)
	{
		___m_BufferPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERBUFFER_T4121715582_H
#ifndef LOCALNOTIFICATION_T1085454254_H
#define LOCALNOTIFICATION_T1085454254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.LocalNotification
struct  LocalNotification_t1085454254  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.LocalNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(LocalNotification_t1085454254, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

struct LocalNotification_t1085454254_StaticFields
{
public:
	// System.Int64 UnityEngine.iOS.LocalNotification::m_NSReferenceDateTicks
	int64_t ___m_NSReferenceDateTicks_1;

public:
	inline static int32_t get_offset_of_m_NSReferenceDateTicks_1() { return static_cast<int32_t>(offsetof(LocalNotification_t1085454254_StaticFields, ___m_NSReferenceDateTicks_1)); }
	inline int64_t get_m_NSReferenceDateTicks_1() const { return ___m_NSReferenceDateTicks_1; }
	inline int64_t* get_address_of_m_NSReferenceDateTicks_1() { return &___m_NSReferenceDateTicks_1; }
	inline void set_m_NSReferenceDateTicks_1(int64_t value)
	{
		___m_NSReferenceDateTicks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALNOTIFICATION_T1085454254_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef INTERNAL_DRAWTEXTUREARGUMENTS_T642817784_H
#define INTERNAL_DRAWTEXTUREARGUMENTS_T642817784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal_DrawTextureArguments
struct  Internal_DrawTextureArguments_t642817784 
{
public:
	// UnityEngine.Rect UnityEngine.Internal_DrawTextureArguments::screenRect
	Rect_t3039462994  ___screenRect_0;
	// UnityEngine.Rect UnityEngine.Internal_DrawTextureArguments::sourceRect
	Rect_t3039462994  ___sourceRect_1;
	// System.Int32 UnityEngine.Internal_DrawTextureArguments::leftBorder
	int32_t ___leftBorder_2;
	// System.Int32 UnityEngine.Internal_DrawTextureArguments::rightBorder
	int32_t ___rightBorder_3;
	// System.Int32 UnityEngine.Internal_DrawTextureArguments::topBorder
	int32_t ___topBorder_4;
	// System.Int32 UnityEngine.Internal_DrawTextureArguments::bottomBorder
	int32_t ___bottomBorder_5;
	// UnityEngine.Color32 UnityEngine.Internal_DrawTextureArguments::color
	Color32_t3428513655  ___color_6;
	// UnityEngine.Vector4 UnityEngine.Internal_DrawTextureArguments::borderWidths
	Vector4_t2436299922  ___borderWidths_7;
	// System.Single UnityEngine.Internal_DrawTextureArguments::cornerRadius
	float ___cornerRadius_8;
	// System.Int32 UnityEngine.Internal_DrawTextureArguments::pass
	int32_t ___pass_9;
	// UnityEngine.Texture UnityEngine.Internal_DrawTextureArguments::texture
	Texture_t2119925672 * ___texture_10;
	// UnityEngine.Material UnityEngine.Internal_DrawTextureArguments::mat
	Material_t2815264910 * ___mat_11;

public:
	inline static int32_t get_offset_of_screenRect_0() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___screenRect_0)); }
	inline Rect_t3039462994  get_screenRect_0() const { return ___screenRect_0; }
	inline Rect_t3039462994 * get_address_of_screenRect_0() { return &___screenRect_0; }
	inline void set_screenRect_0(Rect_t3039462994  value)
	{
		___screenRect_0 = value;
	}

	inline static int32_t get_offset_of_sourceRect_1() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___sourceRect_1)); }
	inline Rect_t3039462994  get_sourceRect_1() const { return ___sourceRect_1; }
	inline Rect_t3039462994 * get_address_of_sourceRect_1() { return &___sourceRect_1; }
	inline void set_sourceRect_1(Rect_t3039462994  value)
	{
		___sourceRect_1 = value;
	}

	inline static int32_t get_offset_of_leftBorder_2() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___leftBorder_2)); }
	inline int32_t get_leftBorder_2() const { return ___leftBorder_2; }
	inline int32_t* get_address_of_leftBorder_2() { return &___leftBorder_2; }
	inline void set_leftBorder_2(int32_t value)
	{
		___leftBorder_2 = value;
	}

	inline static int32_t get_offset_of_rightBorder_3() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___rightBorder_3)); }
	inline int32_t get_rightBorder_3() const { return ___rightBorder_3; }
	inline int32_t* get_address_of_rightBorder_3() { return &___rightBorder_3; }
	inline void set_rightBorder_3(int32_t value)
	{
		___rightBorder_3 = value;
	}

	inline static int32_t get_offset_of_topBorder_4() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___topBorder_4)); }
	inline int32_t get_topBorder_4() const { return ___topBorder_4; }
	inline int32_t* get_address_of_topBorder_4() { return &___topBorder_4; }
	inline void set_topBorder_4(int32_t value)
	{
		___topBorder_4 = value;
	}

	inline static int32_t get_offset_of_bottomBorder_5() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___bottomBorder_5)); }
	inline int32_t get_bottomBorder_5() const { return ___bottomBorder_5; }
	inline int32_t* get_address_of_bottomBorder_5() { return &___bottomBorder_5; }
	inline void set_bottomBorder_5(int32_t value)
	{
		___bottomBorder_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___color_6)); }
	inline Color32_t3428513655  get_color_6() const { return ___color_6; }
	inline Color32_t3428513655 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color32_t3428513655  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_borderWidths_7() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___borderWidths_7)); }
	inline Vector4_t2436299922  get_borderWidths_7() const { return ___borderWidths_7; }
	inline Vector4_t2436299922 * get_address_of_borderWidths_7() { return &___borderWidths_7; }
	inline void set_borderWidths_7(Vector4_t2436299922  value)
	{
		___borderWidths_7 = value;
	}

	inline static int32_t get_offset_of_cornerRadius_8() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___cornerRadius_8)); }
	inline float get_cornerRadius_8() const { return ___cornerRadius_8; }
	inline float* get_address_of_cornerRadius_8() { return &___cornerRadius_8; }
	inline void set_cornerRadius_8(float value)
	{
		___cornerRadius_8 = value;
	}

	inline static int32_t get_offset_of_pass_9() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___pass_9)); }
	inline int32_t get_pass_9() const { return ___pass_9; }
	inline int32_t* get_address_of_pass_9() { return &___pass_9; }
	inline void set_pass_9(int32_t value)
	{
		___pass_9 = value;
	}

	inline static int32_t get_offset_of_texture_10() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___texture_10)); }
	inline Texture_t2119925672 * get_texture_10() const { return ___texture_10; }
	inline Texture_t2119925672 ** get_address_of_texture_10() { return &___texture_10; }
	inline void set_texture_10(Texture_t2119925672 * value)
	{
		___texture_10 = value;
		Il2CppCodeGenWriteBarrier((&___texture_10), value);
	}

	inline static int32_t get_offset_of_mat_11() { return static_cast<int32_t>(offsetof(Internal_DrawTextureArguments_t642817784, ___mat_11)); }
	inline Material_t2815264910 * get_mat_11() const { return ___mat_11; }
	inline Material_t2815264910 ** get_address_of_mat_11() { return &___mat_11; }
	inline void set_mat_11(Material_t2815264910 * value)
	{
		___mat_11 = value;
		Il2CppCodeGenWriteBarrier((&___mat_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Internal_DrawTextureArguments
struct Internal_DrawTextureArguments_t642817784_marshaled_pinvoke
{
	Rect_t3039462994  ___screenRect_0;
	Rect_t3039462994  ___sourceRect_1;
	int32_t ___leftBorder_2;
	int32_t ___rightBorder_3;
	int32_t ___topBorder_4;
	int32_t ___bottomBorder_5;
	Color32_t3428513655  ___color_6;
	Vector4_t2436299922  ___borderWidths_7;
	float ___cornerRadius_8;
	int32_t ___pass_9;
	Texture_t2119925672 * ___texture_10;
	Material_t2815264910 * ___mat_11;
};
// Native definition for COM marshalling of UnityEngine.Internal_DrawTextureArguments
struct Internal_DrawTextureArguments_t642817784_marshaled_com
{
	Rect_t3039462994  ___screenRect_0;
	Rect_t3039462994  ___sourceRect_1;
	int32_t ___leftBorder_2;
	int32_t ___rightBorder_3;
	int32_t ___topBorder_4;
	int32_t ___bottomBorder_5;
	Color32_t3428513655  ___color_6;
	Vector4_t2436299922  ___borderWidths_7;
	float ___cornerRadius_8;
	int32_t ___pass_9;
	Texture_t2119925672 * ___texture_10;
	Material_t2815264910 * ___mat_11;
};
#endif // INTERNAL_DRAWTEXTUREARGUMENTS_T642817784_H
#ifndef MESHTOPOLOGY_T2725410172_H
#define MESHTOPOLOGY_T2725410172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshTopology
struct  MeshTopology_t2725410172 
{
public:
	// System.Int32 UnityEngine.MeshTopology::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MeshTopology_t2725410172, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHTOPOLOGY_T2725410172_H
#ifndef REMOTENOTIFICATION_T3274515193_H
#define REMOTENOTIFICATION_T3274515193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.RemoteNotification
struct  RemoteNotification_t3274515193  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.RemoteNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(RemoteNotification_t3274515193, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTENOTIFICATION_T3274515193_H
#ifndef ANIMATIONCURVE_T2757045165_H
#define ANIMATIONCURVE_T2757045165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t2757045165  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2757045165, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T2757045165_H
#ifndef GRADIENT_T3129154337_H
#define GRADIENT_T3129154337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t3129154337  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t3129154337, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t3129154337_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t3129154337_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T3129154337_H
#ifndef DATETIMEKIND_T1636762993_H
#define DATETIMEKIND_T1636762993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t1636762993 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t1636762993, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T1636762993_H
#ifndef SCRIPTABLERENDERCONTEXT_T2070520004_H
#define SCRIPTABLERENDERCONTEXT_T2070520004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ScriptableRenderContext
struct  ScriptableRenderContext_t2070520004 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ScriptableRenderContext_t2070520004, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERCONTEXT_T2070520004_H
#ifndef FILTERMODE_T2934613156_H
#define FILTERMODE_T2934613156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t2934613156 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t2934613156, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T2934613156_H
#ifndef CACHEDINVOKABLECALL_1_T3757174574_H
#define CACHEDINVOKABLECALL_1_T3757174574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct  CachedInvokableCall_1_t3757174574  : public InvokableCall_1_t2963660007
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t2737604620* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3757174574, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t2737604620* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t2737604620** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t2737604620* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3757174574_H
#ifndef HEADERATTRIBUTE_T3937329216_H
#define HEADERATTRIBUTE_T3937329216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t3937329216  : public PropertyAttribute_t381499487
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t3937329216, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERATTRIBUTE_T3937329216_H
#ifndef DISPLAY_T173707952_H
#define DISPLAY_T173707952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display
struct  Display_t173707952  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Display::nativeDisplay
	intptr_t ___nativeDisplay_0;

public:
	inline static int32_t get_offset_of_nativeDisplay_0() { return static_cast<int32_t>(offsetof(Display_t173707952, ___nativeDisplay_0)); }
	inline intptr_t get_nativeDisplay_0() const { return ___nativeDisplay_0; }
	inline intptr_t* get_address_of_nativeDisplay_0() { return &___nativeDisplay_0; }
	inline void set_nativeDisplay_0(intptr_t value)
	{
		___nativeDisplay_0 = value;
	}
};

struct Display_t173707952_StaticFields
{
public:
	// UnityEngine.Display[] UnityEngine.Display::displays
	DisplayU5BU5D_t2888752273* ___displays_1;
	// UnityEngine.Display UnityEngine.Display::_mainDisplay
	Display_t173707952 * ____mainDisplay_2;
	// UnityEngine.Display/DisplaysUpdatedDelegate UnityEngine.Display::onDisplaysUpdated
	DisplaysUpdatedDelegate_t962499819 * ___onDisplaysUpdated_3;

public:
	inline static int32_t get_offset_of_displays_1() { return static_cast<int32_t>(offsetof(Display_t173707952_StaticFields, ___displays_1)); }
	inline DisplayU5BU5D_t2888752273* get_displays_1() const { return ___displays_1; }
	inline DisplayU5BU5D_t2888752273** get_address_of_displays_1() { return &___displays_1; }
	inline void set_displays_1(DisplayU5BU5D_t2888752273* value)
	{
		___displays_1 = value;
		Il2CppCodeGenWriteBarrier((&___displays_1), value);
	}

	inline static int32_t get_offset_of__mainDisplay_2() { return static_cast<int32_t>(offsetof(Display_t173707952_StaticFields, ____mainDisplay_2)); }
	inline Display_t173707952 * get__mainDisplay_2() const { return ____mainDisplay_2; }
	inline Display_t173707952 ** get_address_of__mainDisplay_2() { return &____mainDisplay_2; }
	inline void set__mainDisplay_2(Display_t173707952 * value)
	{
		____mainDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&____mainDisplay_2), value);
	}

	inline static int32_t get_offset_of_onDisplaysUpdated_3() { return static_cast<int32_t>(offsetof(Display_t173707952_StaticFields, ___onDisplaysUpdated_3)); }
	inline DisplaysUpdatedDelegate_t962499819 * get_onDisplaysUpdated_3() const { return ___onDisplaysUpdated_3; }
	inline DisplaysUpdatedDelegate_t962499819 ** get_address_of_onDisplaysUpdated_3() { return &___onDisplaysUpdated_3; }
	inline void set_onDisplaysUpdated_3(DisplaysUpdatedDelegate_t962499819 * value)
	{
		___onDisplaysUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDisplaysUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAY_T173707952_H
#ifndef BOUNDS_T3570137099_H
#define BOUNDS_T3570137099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3570137099 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t1986933152  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t1986933152  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Center_0)); }
	inline Vector3_t1986933152  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t1986933152 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t1986933152  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Extents_1)); }
	inline Vector3_t1986933152  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t1986933152 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t1986933152  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3570137099_H
#ifndef RAY_T249333749_H
#define RAY_T249333749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t249333749 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t1986933152  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t1986933152  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t249333749, ___m_Origin_0)); }
	inline Vector3_t1986933152  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t1986933152 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t1986933152  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t249333749, ___m_Direction_1)); }
	inline Vector3_t1986933152  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t1986933152 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t1986933152  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T249333749_H
#ifndef COMPUTEBUFFERTYPE_T427724206_H
#define COMPUTEBUFFERTYPE_T427724206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ComputeBufferType
struct  ComputeBufferType_t427724206 
{
public:
	// System.Int32 UnityEngine.ComputeBufferType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ComputeBufferType_t427724206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTEBUFFERTYPE_T427724206_H
#ifndef COMPUTEBUFFER_T2358285523_H
#define COMPUTEBUFFER_T2358285523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ComputeBuffer
struct  ComputeBuffer_t2358285523  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.ComputeBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ComputeBuffer_t2358285523, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTEBUFFER_T2358285523_H
#ifndef SENDMESSAGEOPTIONS_T878493381_H
#define SENDMESSAGEOPTIONS_T878493381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t878493381 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t878493381, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T878493381_H
#ifndef RUNTIMETYPEHANDLE_T3069131627_H
#define RUNTIMETYPEHANDLE_T3069131627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3069131627 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3069131627, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3069131627_H
#ifndef RENDERINGPATH_T34923463_H
#define RENDERINGPATH_T34923463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderingPath
struct  RenderingPath_t34923463 
{
public:
	// System.Int32 UnityEngine.RenderingPath::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderingPath_t34923463, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERINGPATH_T34923463_H
#ifndef LOADSCENEMODE_T149001190_H
#define LOADSCENEMODE_T149001190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t149001190 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t149001190, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T149001190_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef DEPTHTEXTUREMODE_T4236227730_H
#define DEPTHTEXTUREMODE_T4236227730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t4236227730 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DepthTextureMode_t4236227730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T4236227730_H
#ifndef COLORUSAGEATTRIBUTE_T1858490149_H
#define COLORUSAGEATTRIBUTE_T1858490149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorUsageAttribute
struct  ColorUsageAttribute_t1858490149  : public PropertyAttribute_t381499487
{
public:
	// System.Boolean UnityEngine.ColorUsageAttribute::showAlpha
	bool ___showAlpha_0;
	// System.Boolean UnityEngine.ColorUsageAttribute::hdr
	bool ___hdr_1;
	// System.Single UnityEngine.ColorUsageAttribute::minBrightness
	float ___minBrightness_2;
	// System.Single UnityEngine.ColorUsageAttribute::maxBrightness
	float ___maxBrightness_3;
	// System.Single UnityEngine.ColorUsageAttribute::minExposureValue
	float ___minExposureValue_4;
	// System.Single UnityEngine.ColorUsageAttribute::maxExposureValue
	float ___maxExposureValue_5;

public:
	inline static int32_t get_offset_of_showAlpha_0() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___showAlpha_0)); }
	inline bool get_showAlpha_0() const { return ___showAlpha_0; }
	inline bool* get_address_of_showAlpha_0() { return &___showAlpha_0; }
	inline void set_showAlpha_0(bool value)
	{
		___showAlpha_0 = value;
	}

	inline static int32_t get_offset_of_hdr_1() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___hdr_1)); }
	inline bool get_hdr_1() const { return ___hdr_1; }
	inline bool* get_address_of_hdr_1() { return &___hdr_1; }
	inline void set_hdr_1(bool value)
	{
		___hdr_1 = value;
	}

	inline static int32_t get_offset_of_minBrightness_2() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___minBrightness_2)); }
	inline float get_minBrightness_2() const { return ___minBrightness_2; }
	inline float* get_address_of_minBrightness_2() { return &___minBrightness_2; }
	inline void set_minBrightness_2(float value)
	{
		___minBrightness_2 = value;
	}

	inline static int32_t get_offset_of_maxBrightness_3() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___maxBrightness_3)); }
	inline float get_maxBrightness_3() const { return ___maxBrightness_3; }
	inline float* get_address_of_maxBrightness_3() { return &___maxBrightness_3; }
	inline void set_maxBrightness_3(float value)
	{
		___maxBrightness_3 = value;
	}

	inline static int32_t get_offset_of_minExposureValue_4() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___minExposureValue_4)); }
	inline float get_minExposureValue_4() const { return ___minExposureValue_4; }
	inline float* get_address_of_minExposureValue_4() { return &___minExposureValue_4; }
	inline void set_minExposureValue_4(float value)
	{
		___minExposureValue_4 = value;
	}

	inline static int32_t get_offset_of_maxExposureValue_5() { return static_cast<int32_t>(offsetof(ColorUsageAttribute_t1858490149, ___maxExposureValue_5)); }
	inline float get_maxExposureValue_5() const { return ___maxExposureValue_5; }
	inline float* get_address_of_maxExposureValue_5() { return &___maxExposureValue_5; }
	inline void set_maxExposureValue_5(float value)
	{
		___maxExposureValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUSAGEATTRIBUTE_T1858490149_H
#ifndef CAMERAEVENT_T549426807_H
#define CAMERAEVENT_T549426807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t549426807 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t549426807, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T549426807_H
#ifndef COMMANDBUFFER_T1939304854_H
#define COMMANDBUFFER_T1939304854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t1939304854  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t1939304854, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T1939304854_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef COLORSPACE_T3002167351_H
#define COLORSPACE_T3002167351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorSpace
struct  ColorSpace_t3002167351 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t3002167351, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T3002167351_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#define INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t2909334802  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#ifndef COROUTINE_T2294981130_H
#define COROUTINE_T2294981130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t2294981130  : public YieldInstruction_t3270995273
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t2294981130, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t2294981130_marshaled_pinvoke : public YieldInstruction_t3270995273_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t2294981130_marshaled_com : public YieldInstruction_t3270995273_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T2294981130_H
#ifndef CSSMEASUREMODE_T4281271123_H
#define CSSMEASUREMODE_T4281271123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureMode
struct  CSSMeasureMode_t4281271123 
{
public:
	// System.Int32 UnityEngine.CSSLayout.CSSMeasureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CSSMeasureMode_t4281271123, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREMODE_T4281271123_H
#ifndef ARGUMENTEXCEPTION_T1812645948_H
#define ARGUMENTEXCEPTION_T1812645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1812645948  : public SystemException_t2062748594
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1812645948, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1812645948_H
#ifndef NETWORKREACHABILITY_T838440426_H
#define NETWORKREACHABILITY_T838440426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkReachability
struct  NetworkReachability_t838440426 
{
public:
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkReachability_t838440426, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKREACHABILITY_T838440426_H
#ifndef TOUCHTYPE_T1113074055_H
#define TOUCHTYPE_T1113074055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t1113074055 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t1113074055, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T1113074055_H
#ifndef WEAKREFERENCE_T3660816289_H
#define WEAKREFERENCE_T3660816289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WeakReference
struct  WeakReference_t3660816289  : public RuntimeObject
{
public:
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_t4151872729  ___gcHandle_1;

public:
	inline static int32_t get_offset_of_isLongReference_0() { return static_cast<int32_t>(offsetof(WeakReference_t3660816289, ___isLongReference_0)); }
	inline bool get_isLongReference_0() const { return ___isLongReference_0; }
	inline bool* get_address_of_isLongReference_0() { return &___isLongReference_0; }
	inline void set_isLongReference_0(bool value)
	{
		___isLongReference_0 = value;
	}

	inline static int32_t get_offset_of_gcHandle_1() { return static_cast<int32_t>(offsetof(WeakReference_t3660816289, ___gcHandle_1)); }
	inline GCHandle_t4151872729  get_gcHandle_1() const { return ___gcHandle_1; }
	inline GCHandle_t4151872729 * get_address_of_gcHandle_1() { return &___gcHandle_1; }
	inline void set_gcHandle_1(GCHandle_t4151872729  value)
	{
		___gcHandle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKREFERENCE_T3660816289_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t2927642150
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef LOGTYPE_T3717748719_H
#define LOGTYPE_T3717748719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t3717748719 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t3717748719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T3717748719_H
#ifndef CUBEMAPFACE_T3832471454_H
#define CUBEMAPFACE_T3832471454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t3832471454 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t3832471454, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T3832471454_H
#ifndef CULLINGGROUP_T616951155_H
#define CULLINGGROUP_T616951155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup
struct  CullingGroup_t616951155  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.CullingGroup::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.CullingGroup/StateChanged UnityEngine.CullingGroup::m_OnStateChanged
	StateChanged_t3981248240 * ___m_OnStateChanged_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CullingGroup_t616951155, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_OnStateChanged_1() { return static_cast<int32_t>(offsetof(CullingGroup_t616951155, ___m_OnStateChanged_1)); }
	inline StateChanged_t3981248240 * get_m_OnStateChanged_1() const { return ___m_OnStateChanged_1; }
	inline StateChanged_t3981248240 ** get_address_of_m_OnStateChanged_1() { return &___m_OnStateChanged_1; }
	inline void set_m_OnStateChanged_1(StateChanged_t3981248240 * value)
	{
		___m_OnStateChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnStateChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CullingGroup
struct CullingGroup_t616951155_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
// Native definition for COM marshalling of UnityEngine.CullingGroup
struct CullingGroup_t616951155_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
#endif // CULLINGGROUP_T616951155_H
#ifndef MONOORSTEREOSCOPICEYE_T359336667_H
#define MONOORSTEREOSCOPICEYE_T359336667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/MonoOrStereoscopicEye
struct  MonoOrStereoscopicEye_t359336667 
{
public:
	// System.Int32 UnityEngine.Camera/MonoOrStereoscopicEye::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MonoOrStereoscopicEye_t359336667, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOORSTEREOSCOPICEYE_T359336667_H
#ifndef ASYNCOPERATION_T1468153686_H
#define ASYNCOPERATION_T1468153686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1468153686  : public YieldInstruction_t3270995273
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t3739209734 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1468153686, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1468153686, ___m_completeCallback_1)); }
	inline Action_1_t3739209734 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t3739209734 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t3739209734 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686_marshaled_pinvoke : public YieldInstruction_t3270995273_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686_marshaled_com : public YieldInstruction_t3270995273_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1468153686_H
#ifndef DRIVENTRANSFORMPROPERTIES_T4078603187_H
#define DRIVENTRANSFORMPROPERTIES_T4078603187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t4078603187 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t4078603187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T4078603187_H
#ifndef CURSORMODE_T1407240675_H
#define CURSORMODE_T1407240675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorMode
struct  CursorMode_t1407240675 
{
public:
	// System.Int32 UnityEngine.CursorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorMode_t1407240675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORMODE_T1407240675_H
#ifndef CURSORLOCKMODE_T3686483944_H
#define CURSORLOCKMODE_T3686483944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_t3686483944 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_t3686483944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_T3686483944_H
#ifndef STEREOSCOPICEYE_T3664179786_H
#define STEREOSCOPICEYE_T3664179786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/StereoscopicEye
struct  StereoscopicEye_t3664179786 
{
public:
	// System.Int32 UnityEngine.Camera/StereoscopicEye::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoscopicEye_t3664179786, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOSCOPICEYE_T3664179786_H
#ifndef CAMERACLEARFLAGS_T3338048873_H
#define CAMERACLEARFLAGS_T3338048873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t3338048873 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t3338048873, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T3338048873_H
#ifndef DATETIME_T359870098_H
#define DATETIME_T359870098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t359870098 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2821448670  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___ticks_0)); }
	inline TimeSpan_t2821448670  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2821448670 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2821448670  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t359870098_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t359870098  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t359870098  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t2511808107* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t2511808107* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t2511808107* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t2511808107* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t2511808107* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t2511808107* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t2511808107* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t1965588061* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t1965588061* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MaxValue_2)); }
	inline DateTime_t359870098  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t359870098 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t359870098  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MinValue_3)); }
	inline DateTime_t359870098  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t359870098 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t359870098  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t2511808107* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t2511808107* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t2511808107* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t2511808107* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t2511808107* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t2511808107* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t2511808107* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t2511808107* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t2511808107* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t2511808107* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t2511808107* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t2511808107** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t2511808107* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t2511808107* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t2511808107** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t2511808107* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t1965588061* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t1965588061* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t1965588061* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t1965588061* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T359870098_H
#ifndef TOUCH_T1048744301_H
#define TOUCH_T1048744301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1048744301 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t328513675  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t328513675  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t328513675  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Position_1)); }
	inline Vector2_t328513675  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t328513675 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t328513675  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_RawPosition_2)); }
	inline Vector2_t328513675  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t328513675 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t328513675  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_PositionDelta_3)); }
	inline Vector2_t328513675  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t328513675 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t328513675  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1048744301_H
#ifndef ARGUMENTNULLEXCEPTION_T3857807348_H
#define ARGUMENTNULLEXCEPTION_T3857807348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t3857807348  : public ArgumentException_t1812645948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T3857807348_H
#ifndef SHADER_T1881769421_H
#define SHADER_T1881769421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t1881769421  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T1881769421_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef GAMEOBJECT_T2557347079_H
#define GAMEOBJECT_T2557347079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2557347079  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2557347079_H
#ifndef MESH_T4030024733_H
#define MESH_T4030024733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t4030024733  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T4030024733_H
#ifndef OBJECTDISPOSEDEXCEPTION_T1428820261_H
#define OBJECTDISPOSEDEXCEPTION_T1428820261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t1428820261  : public InvalidOperationException_t3358289486
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t1428820261, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t1428820261, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T1428820261_H
#ifndef MATERIAL_T2815264910_H
#define MATERIAL_T2815264910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2815264910  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2815264910_H
#ifndef COMPUTESHADER_T532478176_H
#define COMPUTESHADER_T532478176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ComputeShader
struct  ComputeShader_t532478176  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTESHADER_T532478176_H
#ifndef FAILEDTOLOADSCRIPTOBJECT_T652497987_H
#define FAILEDTOLOADSCRIPTOBJECT_T652497987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FailedToLoadScriptObject
struct  FailedToLoadScriptObject_t652497987  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t652497987_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t652497987_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // FAILEDTOLOADSCRIPTOBJECT_T652497987_H
#ifndef TEXTURE_T2119925672_H
#define TEXTURE_T2119925672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2119925672  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2119925672_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3069131627  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3069131627  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3069131627 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3069131627  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1985992169* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2357198184 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2357198184 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2357198184 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1985992169* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1985992169* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2357198184 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2357198184 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2357198184 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2357198184 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2357198184 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2357198184 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef PARAMETERINFO_T3943516840_H
#define PARAMETERINFO_T3943516840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3943516840  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t2394797874 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___marshalAs_6)); }
	inline UnmanagedMarshal_t2394797874 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t2394797874 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t2394797874 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3943516840_H
#ifndef ASSETBUNDLEREQUEST_T230831548_H
#define ASSETBUNDLEREQUEST_T230831548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleRequest
struct  AssetBundleRequest_t230831548  : public AsyncOperation_t1468153686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t230831548_marshaled_pinvoke : public AsyncOperation_t1468153686_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t230831548_marshaled_com : public AsyncOperation_t1468153686_marshaled_com
{
};
#endif // ASSETBUNDLEREQUEST_T230831548_H
#ifndef LOGGER_T952924384_H
#define LOGGER_T952924384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Logger
struct  Logger_t952924384  : public RuntimeObject
{
public:
	// UnityEngine.ILogHandler UnityEngine.Logger::<logHandler>k__BackingField
	RuntimeObject* ___U3ClogHandlerU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Logger::<logEnabled>k__BackingField
	bool ___U3ClogEnabledU3Ek__BackingField_1;
	// UnityEngine.LogType UnityEngine.Logger::<filterLogType>k__BackingField
	int32_t ___U3CfilterLogTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClogHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Logger_t952924384, ___U3ClogHandlerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3ClogHandlerU3Ek__BackingField_0() const { return ___U3ClogHandlerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3ClogHandlerU3Ek__BackingField_0() { return &___U3ClogHandlerU3Ek__BackingField_0; }
	inline void set_U3ClogHandlerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3ClogHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClogHandlerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClogEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Logger_t952924384, ___U3ClogEnabledU3Ek__BackingField_1)); }
	inline bool get_U3ClogEnabledU3Ek__BackingField_1() const { return ___U3ClogEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3ClogEnabledU3Ek__BackingField_1() { return &___U3ClogEnabledU3Ek__BackingField_1; }
	inline void set_U3ClogEnabledU3Ek__BackingField_1(bool value)
	{
		___U3ClogEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t952924384, ___U3CfilterLogTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CfilterLogTypeU3Ek__BackingField_2() const { return ___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfilterLogTypeU3Ek__BackingField_2() { return &___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline void set_U3CfilterLogTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CfilterLogTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T952924384_H
#ifndef ASSETBUNDLECREATEREQUEST_T1159347791_H
#define ASSETBUNDLECREATEREQUEST_T1159347791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleCreateRequest
struct  AssetBundleCreateRequest_t1159347791  : public AsyncOperation_t1468153686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1159347791_marshaled_pinvoke : public AsyncOperation_t1468153686_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1159347791_marshaled_com : public AsyncOperation_t1468153686_marshaled_com
{
};
#endif // ASSETBUNDLECREATEREQUEST_T1159347791_H
#ifndef PERSISTENTCALL_T709177109_H
#define PERSISTENTCALL_T709177109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCall
struct  PersistentCall_t709177109  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.PersistentCall::m_Target
	Object_t692178351 * ___m_Target_0;
	// System.String UnityEngine.Events.PersistentCall::m_MethodName
	String_t* ___m_MethodName_1;
	// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::m_Mode
	int32_t ___m_Mode_2;
	// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::m_Arguments
	ArgumentCache_t288530198 * ___m_Arguments_3;
	// UnityEngine.Events.UnityEventCallState UnityEngine.Events.PersistentCall::m_CallState
	int32_t ___m_CallState_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(PersistentCall_t709177109, ___m_Target_0)); }
	inline Object_t692178351 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t692178351 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t692178351 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodName_1() { return static_cast<int32_t>(offsetof(PersistentCall_t709177109, ___m_MethodName_1)); }
	inline String_t* get_m_MethodName_1() const { return ___m_MethodName_1; }
	inline String_t** get_address_of_m_MethodName_1() { return &___m_MethodName_1; }
	inline void set_m_MethodName_1(String_t* value)
	{
		___m_MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodName_1), value);
	}

	inline static int32_t get_offset_of_m_Mode_2() { return static_cast<int32_t>(offsetof(PersistentCall_t709177109, ___m_Mode_2)); }
	inline int32_t get_m_Mode_2() const { return ___m_Mode_2; }
	inline int32_t* get_address_of_m_Mode_2() { return &___m_Mode_2; }
	inline void set_m_Mode_2(int32_t value)
	{
		___m_Mode_2 = value;
	}

	inline static int32_t get_offset_of_m_Arguments_3() { return static_cast<int32_t>(offsetof(PersistentCall_t709177109, ___m_Arguments_3)); }
	inline ArgumentCache_t288530198 * get_m_Arguments_3() const { return ___m_Arguments_3; }
	inline ArgumentCache_t288530198 ** get_address_of_m_Arguments_3() { return &___m_Arguments_3; }
	inline void set_m_Arguments_3(ArgumentCache_t288530198 * value)
	{
		___m_Arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arguments_3), value);
	}

	inline static int32_t get_offset_of_m_CallState_4() { return static_cast<int32_t>(offsetof(PersistentCall_t709177109, ___m_CallState_4)); }
	inline int32_t get_m_CallState_4() const { return ___m_CallState_4; }
	inline int32_t* get_address_of_m_CallState_4() { return &___m_CallState_4; }
	inline void set_m_CallState_4(int32_t value)
	{
		___m_CallState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALL_T709177109_H
#ifndef ASSETBUNDLE_T3848723172_H
#define ASSETBUNDLE_T3848723172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundle
struct  AssetBundle_t3848723172  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLE_T3848723172_H
#ifndef UNITYACTION_T992331987_H
#define UNITYACTION_T992331987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t992331987  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T992331987_H
#ifndef ASYNCCALLBACK_T3561663063_H
#define ASYNCCALLBACK_T3561663063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3561663063  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3561663063_H
#ifndef ACTION_1_T3739209734_H
#define ACTION_1_T3739209734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t3739209734  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3739209734_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef PREDICATE_1_T1730346354_H
#define PREDICATE_1_T1730346354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct  Predicate_1_t1730346354  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1730346354_H
#ifndef RENDERTEXTURE_T971269558_H
#define RENDERTEXTURE_T971269558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t971269558  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T971269558_H
#ifndef CAMERACALLBACK_T657915608_H
#define CAMERACALLBACK_T657915608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t657915608  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACALLBACK_T657915608_H
#ifndef TRANSFORM_T362059596_H
#define TRANSFORM_T362059596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t362059596  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T362059596_H
#ifndef LOGCALLBACK_T204719788_H
#define LOGCALLBACK_T204719788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LogCallback
struct  LogCallback_t204719788  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T204719788_H
#ifndef CSSMEASUREFUNC_T2094042647_H
#define CSSMEASUREFUNC_T2094042647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureFunc
struct  CSSMeasureFunc_t2094042647  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREFUNC_T2094042647_H
#ifndef LOWMEMORYCALLBACK_T3854352520_H
#define LOWMEMORYCALLBACK_T3854352520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LowMemoryCallback
struct  LowMemoryCallback_t3854352520  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWMEMORYCALLBACK_T3854352520_H
#ifndef STATECHANGED_T3981248240_H
#define STATECHANGED_T3981248240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup/StateChanged
struct  StateChanged_t3981248240  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATECHANGED_T3981248240_H
#ifndef TEXTURE2D_T3063074017_H
#define TEXTURE2D_T3063074017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3063074017  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3063074017_H
#ifndef DISPLAYSUPDATEDDELEGATE_T962499819_H
#define DISPLAYSUPDATEDDELEGATE_T962499819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display/DisplaysUpdatedDelegate
struct  DisplaysUpdatedDelegate_t962499819  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYSUPDATEDDELEGATE_T962499819_H
#ifndef GUIELEMENT_T2946029536_H
#define GUIELEMENT_T2946029536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t2946029536  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T2946029536_H
#ifndef RECTTRANSFORM_T859616204_H
#define RECTTRANSFORM_T859616204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t859616204  : public Transform_t362059596
{
public:

public:
};

struct RectTransform_t859616204_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t2209604263 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t859616204_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t2209604263 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t2209604263 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t2209604263 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T859616204_H
#ifndef CAMERA_T2839736942_H
#define CAMERA_T2839736942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t2839736942  : public Behaviour_t2850977393
{
public:

public:
};

struct Camera_t2839736942_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t657915608 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t657915608 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t657915608 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t657915608 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t657915608 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t657915608 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t657915608 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t657915608 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t657915608 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t657915608 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t657915608 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t657915608 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T2839736942_H
#ifndef GUILAYER_T478658086_H
#define GUILAYER_T478658086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t478658086  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T478658086_H
#ifndef GUITEXTURE_T2618579966_H
#define GUITEXTURE_T2618579966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUITexture
struct  GUITexture_t2618579966  : public GUIElement_t2946029536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXTURE_T2618579966_H
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t2754673280  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t3310169645  m_Items[1];

public:
	inline Keyframe_t3310169645  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t3310169645 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t3310169645  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t3310169645  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t3310169645 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t3310169645  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2737604620  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t1985992169  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t2692239366  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t2180947615 * m_Items[1];

public:
	inline RequireComponent_t2180947615 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RequireComponent_t2180947615 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t2180947615 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RequireComponent_t2180947615 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RequireComponent_t2180947615 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RequireComponent_t2180947615 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t4108307263  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t2744062746 * m_Items[1];

public:
	inline DisallowMultipleComponent_t2744062746 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t2744062746 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t2744062746 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisallowMultipleComponent_t2744062746 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t2744062746 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisallowMultipleComponent_t2744062746 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t3696554944  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t869940717 * m_Items[1];

public:
	inline ExecuteInEditMode_t869940717 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExecuteInEditMode_t869940717 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t869940717 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExecuteInEditMode_t869940717 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExecuteInEditMode_t869940717 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExecuteInEditMode_t869940717 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t32224225  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t1986933152  m_Items[1];

public:
	inline Vector3_t1986933152  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t1986933152 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t1986933152  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t1986933152  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t1986933152 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t1986933152  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t4045078555  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t2839736942 * m_Items[1];

public:
	inline Camera_t2839736942 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t2839736942 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t2839736942 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t2839736942 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t2839736942 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t2839736942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) intptr_t m_Items[1];

public:
	inline intptr_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline intptr_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, intptr_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline intptr_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline intptr_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, intptr_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t2888752273  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Display_t173707952 * m_Items[1];

public:
	inline Display_t173707952 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Display_t173707952 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Display_t173707952 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Display_t173707952 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Display_t173707952 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Display_t173707952 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t3316460985  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3943516840 * m_Items[1];

public:
	inline ParameterInfo_t3943516840 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3943516840 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3943516840 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3943516840 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3943516840 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3943516840 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t2531273172  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t999651945  m_Items[1];

public:
	inline ParameterModifier_t999651945  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t999651945 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t999651945  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t999651945  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t999651945 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t999651945  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t2557347079 * m_Items[1];

public:
	inline GameObject_t2557347079 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t2557347079 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t2557347079 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t2557347079 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t2557347079 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t2557347079 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t328513675  m_Items[1];

public:
	inline Vector2_t328513675  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t328513675 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t328513675  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t328513675  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t328513675 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t328513675  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t846911691  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RenderBuffer_t4121715582  m_Items[1];

public:
	inline RenderBuffer_t4121715582  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RenderBuffer_t4121715582 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RenderBuffer_t4121715582  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RenderBuffer_t4121715582  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RenderBuffer_t4121715582 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RenderBuffer_t4121715582  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t170615360  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Touch_t1048744301  m_Items[1];

public:
	inline Touch_t1048744301  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Touch_t1048744301 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Touch_t1048744301  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Touch_t1048744301  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Touch_t1048744301 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Touch_t1048744301  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m1075389167_gshared (Action_1_t4119957313 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m1433301027_gshared (Stack_1_t2177427975 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m2236993847_gshared (Stack_1_t2177427975 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m778682499_gshared (Stack_1_t2177427975 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m546753711_gshared (Stack_1_t2177427975 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2666688348_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m491990267_gshared (List_1_t968528272 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2737604620* List_1_ToArray_m654788613_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  RuntimeObject * AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3188523325_gshared (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m230551938_gshared (Dictionary_2_t1294734816 * __this, intptr_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3779226859_gshared (Dictionary_2_t1294734816 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m1273050053_gshared (List_1_t968528272 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2987169932_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1903355775_gshared (Predicate_1_t3551521503 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C"  int32_t List_1_RemoveAll_m468551334_gshared (List_1_t968528272 * __this, Predicate_1_t3551521503 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m1387425582_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m1447358304_gshared (List_1_t968528272 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2264889547_gshared (CachedInvokableCall_1_t3587825671 * __this, Object_t692178351 * p0, MethodInfo_t * p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m4025676652_gshared (CachedInvokableCall_1_t4075156644 * __this, Object_t692178351 * p0, MethodInfo_t * p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1436007573_gshared (CachedInvokableCall_1_t656523105 * __this, Object_t692178351 * p0, MethodInfo_t * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2035028495_gshared (CachedInvokableCall_1_t2432241475 * __this, Object_t692178351 * p0, MethodInfo_t * p1, bool p2, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t744396796  List_1_GetEnumerator_m1837174046_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m335951430_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1328309074_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3763436353_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3631383453_gshared (Dictionary_2_t1708198297 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1490102977_gshared (Dictionary_2_t1708198297 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2905489384_gshared (Dictionary_2_t1708198297 * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m2479467358 (Attribute_t1924466020 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2968844594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m22632116 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m485238445 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m350649604 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t2754673280* AnimationCurve_GetKeys_m3274694562 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m1572898885 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationCurve::AddKey_Internal(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_Internal_m3646094092 (AnimationCurve_t2757045165 * __this, Keyframe_t3310169645  ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
extern "C"  int32_t AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367 (RuntimeObject * __this /* static, unused */, AnimationCurve_t2757045165 * ___self0, Keyframe_t3310169645 * ___key1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey_Internal(System.Int32)
extern "C"  Keyframe_t3310169645  AnimationCurve_GetKey_Internal_m4240133260 (AnimationCurve_t2757045165 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
extern "C"  void AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819 (RuntimeObject * __this /* static, unused */, AnimationCurve_t2757045165 * ___self0, int32_t ___index1, Keyframe_t3310169645 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Keyframe__ctor_m3149517464 (Keyframe_t3310169645 * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m3733989894 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3092601788 (LowMemoryCallback_t3854352520 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m4080113311 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m2628855948 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m712489286 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t2625678720 * CultureInfo_get_InvariantCulture_m1870357956 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.IFormatProvider)
extern "C"  String_t* Single_ToString_m1052142587 (float* __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Double::ToString(System.IFormatProvider)
extern "C"  String_t* Double_ToString_m1506453147 (double* __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m443874444 (StringBuilder_t622404039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t622404039 * StringBuilder_Append_m3980566461 (StringBuilder_t622404039 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::ObjectToJSString(System.Object)
extern "C"  String_t* Application_ObjectToJSString_m508226240 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::BuildInvocationForArguments(System.String,System.Object[])
extern "C"  String_t* Application_BuildInvocationForArguments_m4046074230 (RuntimeObject * __this /* static, unused */, String_t* ___functionName0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Internal_ExternalCall(System.String)
extern "C"  void Application_Internal_ExternalCall_m1484991240 (RuntimeObject * __this /* static, unused */, String_t* ___script0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t622404039 * StringBuilder_Append_m2167980178 (StringBuilder_t622404039 * __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m61920202 (LogCallback_t204719788 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m2407906560 (UnityAction_t992331987 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t965641407  SceneManager_GetActiveScene_m943937788 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m422286238 (Scene_t965641407 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m2002147576 (Scene_t965641407 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m3548089684 (RuntimeObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m1705576468 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m874787853 (YieldInstruction_t3270995273 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m1729956680 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.AsyncOperation>::Invoke(!0)
#define Action_1_Invoke_m14832607(__this, p0, method) ((  void (*) (Action_1_t3739209734 *, AsyncOperation_t1468153686 *, const RuntimeMethod*))Action_1_Invoke_m1075389167_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m4283643689(__this, method) ((  void (*) (Stack_1_t2196794798 *, const RuntimeMethod*))Stack_1__ctor_m1433301027_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
#define Stack_1_Push_m382399138(__this, p0, method) ((  void (*) (Stack_1_t2196794798 *, Type_t *, const RuntimeMethod*))Stack_1_Push_m2236993847_gshared)(__this, p0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m125071402 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3069131627  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m1148751044(__this, method) ((  Type_t * (*) (Stack_1_t2196794798 *, const RuntimeMethod*))Stack_1_Pop_m778682499_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m3176329963(__this, method) ((  int32_t (*) (Stack_1_t2196794798 *, const RuntimeMethod*))Stack_1_get_Count_m546753711_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
#define List_1__ctor_m1591575689(__this, method) ((  void (*) (List_1_t987895095 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
#define List_1_Add_m2945177012(__this, p0, method) ((  void (*) (List_1_t987895095 *, Type_t *, const RuntimeMethod*))List_1_Add_m491990267_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m1572029861(__this, method) ((  TypeU5BU5D_t1985992169* (*) (List_1_t987895095 *, const RuntimeMethod*))List_1_ToArray_m654788613_gshared)(__this, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t334925532_m3709888332(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t334925532 * (*) (RuntimeObject * /* static, unused */, Type_t *, const RuntimeMethod*))AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3188523325_gshared)(__this /* static, unused */, ___klass0, method)
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3197979243 (DefaultExecutionOrder_t334925532 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m1184908066 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_op_Multiply_m952283854 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m1878822430 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___center0, Vector3_t1986933152  ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C"  bool Bounds_INTERNAL_CALL_Internal_Contains_m1305042420 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099 * ___m0, Vector3_t1986933152 * ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C"  bool Bounds_Internal_Contains_m2180258632 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099  ___m0, Vector3_t1986933152  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C"  bool Bounds_Contains_m1117219008 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t1986933152  Bounds_get_center_m292426572 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m4139137898 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t1986933152  Bounds_get_extents_m1383416966 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m247994267 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m3593875679 (Vector3_t1986933152 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m796471542 (Bounds_t3570137099 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1174052405 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t1986933152  Bounds_get_size_m3322399915 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m2783247878 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2896293588 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Subtraction_m1182848491 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t1986933152  Bounds_get_min_m3060588205 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t1986933152  Bounds_get_max_m2926624215 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1440205454 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___min0, Vector3_t1986933152  ___max1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C"  void Bounds_set_min_m3390395710 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Addition_m1125374618 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C"  void Bounds_set_max_m627008328 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m511918437 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3484373173 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099  ___lhs0, Bounds_t3570137099  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Min_m3122780938 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Max_m664546017 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m608594558 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C"  void Bounds_Encapsulate_m3380017899 (Bounds_t3570137099 * __this, Bounds_t3570137099  ___bounds0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3950186024 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m2675624681 (Bounds_t3570137099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2653597291 (Behaviour_t2850977393 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m417459086 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m4137046919 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_rect_m3547426077 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_set_rect_m639478796 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m4233702303 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_worldToCameraMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_worldToCameraMatrix_m4166556156 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_projectionMatrix_m2936427189 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m2733397033 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)
extern "C"  void Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_GetStereoViewMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, int32_t ___eye1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_GetStereoProjectionMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, int32_t ___eye1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1941537752 (ArgumentNullException_t3857807348 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m4009562449 (ArgumentException_t1812645948 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::CalculateFrustumCornersInternal(UnityEngine.Rect,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])
extern "C"  void Camera_CalculateFrustumCornersInternal_m3163453171 (Camera_t2839736942 * __this, Rect_t3039462994  ___viewport0, float ___z1, int32_t ___eye2, Vector3U5BU5D_t32224225* ___outCorners3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_CalculateFrustumCornersInternal(UnityEngine.Camera,UnityEngine.Rect&,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])
extern "C"  void Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Rect_t3039462994 * ___viewport1, float ___z2, int32_t ___eye3, Vector3U5BU5D_t32224225* ___outCorners4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Ray_t249333749 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m1277705143 (CameraCallback_t657915608 * __this, Camera_t2839736942 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_INTERNAL_CALL_RaycastTry_m2381035361 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Ray_t249333749 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_INTERNAL_CALL_RaycastTry2D_m152944706 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Ray_t249333749 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m532212392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2237324961 (Color_t2582018970 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3027879795 (Color_t2582018970 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m2932792595 (Color_t2582018970 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t2436299922  Color_op_Implicit_m2059778741 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3459216764 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m4075930393 (Color_t2582018970 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m2960811275 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m3159899910 (Color_t2582018970 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m1687602531 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___lhs0, Vector4_t2436299922  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2193645629 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1296751250 (Vector4_t2436299922 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m1303321160 (IndexOutOfRangeException_t2909334802 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::set_Item(System.Int32,System.Single)
extern "C"  void Color_set_Item_m51493835 (Color_t2582018970 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m1812196269 (Color32_t3428513655 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m2415660705 (Color32_t3428513655 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1725140046 (PropertyAttribute_t381499487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m4147771927 (Object_t692178351 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2557347079 * Component_get_gameObject_m1032427207 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t1632713610 * GameObject_GetComponent_m469693343 (GameObject_t2557347079 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1632713610 * GameObject_GetComponentInChildren_m1977537235 (GameObject_t2557347079 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t1632713610 * GameObject_GetComponentInParent_m955227985 (GameObject_t2557347079 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m3322214901 (Component_t1632713610 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m1254570625 (Component_t1632713610 * __this, String_t* ___methodName0, RuntimeObject * ___parameter1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32,UnityEngine.ComputeBufferType,System.Int32)
extern "C"  void ComputeBuffer__ctor_m421042894 (ComputeBuffer_t2358285523 * __this, int32_t ___count0, int32_t ___stride1, int32_t ___type2, int32_t ___stackDepth3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)
extern "C"  void ComputeBuffer_InitBuffer_m4240887542 (RuntimeObject * __this /* static, unused */, ComputeBuffer_t2358285523 * ___buf0, int32_t ___count1, int32_t ___stride2, int32_t ___type3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::Dispose(System.Boolean)
extern "C"  void ComputeBuffer_Dispose_m1759304253 (ComputeBuffer_t2358285523 * __this, bool ___disposing0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m3739586421 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
extern "C"  void ComputeBuffer_DestroyBuffer_m22290331 (RuntimeObject * __this /* static, unused */, ComputeBuffer_t2358285523 * ___buf0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m2291805156 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3275468740 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::Dispose()
extern "C"  void ComputeBuffer_Dispose_m2324970370 (ComputeBuffer_t2358285523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m2853335497 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m2972540513 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
extern "C"  int32_t Marshal_SizeOf_m2163745240 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void ComputeBuffer_InternalSetData_m3847349839 (ComputeBuffer_t2358285523 * __this, RuntimeArray * ___data0, int32_t ___managedBufferStartIndex1, int32_t ___computeBufferStartIndex2, int32_t ___count3, int32_t ___elemSize4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m1579628175 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeShader::SetVector(System.Int32,UnityEngine.Vector4)
extern "C"  void ComputeShader_SetVector_m91341944 (ComputeShader_t532478176 * __this, int32_t ___nameID0, Vector4_t2436299922  ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeShader::INTERNAL_CALL_SetVector(UnityEngine.ComputeShader,System.Int32,UnityEngine.Vector4&)
extern "C"  void ComputeShader_INTERNAL_CALL_SetVector_m3065059501 (RuntimeObject * __this /* static, unused */, ComputeShader_t532478176 * ___self0, int32_t ___nameID1, Vector4_t2436299922 * ___val2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.Int32,UnityEngine.Texture)
extern "C"  void ComputeShader_SetTexture_m3962244874 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, int32_t ___nameID1, Texture_t2119925672 * ___texture2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.Int32,UnityEngine.ComputeBuffer)
extern "C"  void ComputeShader_SetBuffer_m173270919 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, int32_t ___nameID1, ComputeBuffer_t2358285523 * ___buffer2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern "C"  void ContextMenu__ctor_m2944493690 (ContextMenu_t2541329559 * __this, String_t* ___itemName0, bool ___isValidateFunction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern "C"  void ContextMenu__ctor_m3411406348 (ContextMenu_t2541329559 * __this, String_t* ___itemName0, bool ___isValidateFunction1, int32_t ___priority2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m2401623703 (Coroutine_t2294981130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t2672652373  CSSMeasureFunc_Invoke_m2866500810 (CSSMeasureFunc_t2094042647 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1943609909(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t3106649840 *, intptr_t, WeakReference_t3660816289 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m230551938_gshared)(__this, p0, p1, method)
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2094042647 * Native_CSSNodeGetMeasureFunc_m1704122301 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m803950198 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::.ctor()
#define Dictionary_2__ctor_m83878655(__this, method) ((  void (*) (Dictionary_2_t3106649840 *, const RuntimeMethod*))Dictionary_2__ctor_m3779226859_gshared)(__this, method)
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m1588373077 (CullingGroup_t616951155 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m1408049937 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m1290116875 (StateChanged_t3981248240 * __this, CullingGroupEvent_t655523856  ___sphere0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::INTERNAL_CALL_SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)
extern "C"  void Cursor_INTERNAL_CALL_SetCursor_m3622843465 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Vector2_t328513675 * ___hotspot1, int32_t ___cursorMode2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C"  void Debug_INTERNAL_CALL_DrawLine_m3943959890 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___start0, Vector3_t1986933152 * ___end1, Color_t2582018970 * ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void Debug_DrawRay_m2541735374 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___dir1, Color_t2582018970  ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2582018970  Color_get_white_m2500342551 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void Debug_DrawLine_m1640476408 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___end1, Color_t2582018970  ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m1827429516 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m141086873 (DebugLogHandler_t2880387906 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m844445376 (Logger_t952924384 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m29320051 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2737604620* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m424405779 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t692178351 * ___obj2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m3622619738 (RuntimeObject * __this /* static, unused */, Exception_t4086964929 * ___exception0, Object_t692178351 * ___obj1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int32)
extern "C"  void IntPtr__ctor_m2782994112 (intptr_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m3140247240 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m2705345354 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m369378025 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m646575176 (Display_t173707952 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m652453720 (DisplaysUpdatedDelegate_t962499819 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m190271719 (Display_t173707952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m4244486561 (DrivenRectTransformTracker_t2554077623 * __this, Object_t692178351 * ___driver0, RectTransform_t859616204 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m2683757757 (DrivenRectTransformTracker_t2554077623 * __this, bool ___revertValues0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2163207444 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m3326695972 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m2084528576 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1881321258 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m3111453544 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2395435812 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1355543587 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m3738249125 (Delegate_t3882343965 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m4090170938 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1032523438 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___x0, Object_t692178351 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2058189347 (BaseInvokableCall_t27726116 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t3882343965 * NetFxCoreExtensions_CreateDelegate_m1322929972 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m275363113 (InvokableCall_t2697346187 * __this, UnityAction_t992331987 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m2710559406 (BaseInvokableCall_t27726116 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3882343965 * Delegate_Combine_m912662445 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * p0, Delegate_t3882343965 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3882343965 * Delegate_Remove_m3357168269 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * p0, Delegate_t3882343965 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m880256453 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * ___delegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m3023103954 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m3740896317(__this, method) ((  void (*) (List_1_t3442320419 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0)
#define List_1_Add_m604383575(__this, p0, method) ((  void (*) (List_1_t3442320419 *, BaseInvokableCall_t27726116 *, const RuntimeMethod*))List_1_Add_m491990267_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m1584988565(__this, p0, method) ((  BaseInvokableCall_t27726116 * (*) (List_1_t3442320419 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1273050053_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m79931508(__this, method) ((  int32_t (*) (List_1_t3442320419 *, const RuntimeMethod*))List_1_get_Count_m2987169932_gshared)(__this, method)
// System.Void System.Predicate`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m422560267(__this, p0, p1, method) ((  void (*) (Predicate_1_t1730346354 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m1903355775_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<!0>)
#define List_1_RemoveAll_m127110729(__this, p0, method) ((  int32_t (*) (List_1_t3442320419 *, Predicate_1_t1730346354 *, const RuntimeMethod*))List_1_RemoveAll_m468551334_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m1306609920(__this, method) ((  void (*) (List_1_t3442320419 *, const RuntimeMethod*))List_1_Clear_m1387425582_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m3743613219(__this, p0, method) ((  void (*) (List_1_t3442320419 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m1447358304_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m4160331614 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t692178351 * PersistentCall_get_target_m520259316 (PersistentCall_t709177109 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m1071443131 (PersistentCall_t709177109 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m193475207 (UnityEventBase_t124089244 * __this, PersistentCall_t709177109 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t27726116 * PersistentCall_GetObjectCall_m3632195579 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t288530198 * ___arguments2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m2917341558 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2264889547(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3587825671 *, Object_t692178351 *, MethodInfo_t *, float, const RuntimeMethod*))CachedInvokableCall_1__ctor_m2264889547_gshared)(__this, p0, p1, p2, method)
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2164087651 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m4025676652(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t4075156644 *, Object_t692178351 *, MethodInfo_t *, int32_t, const RuntimeMethod*))CachedInvokableCall_1__ctor_m4025676652_gshared)(__this, p0, p1, p2, method)
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m3199145890 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2075360316(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3757174574 *, Object_t692178351 *, MethodInfo_t *, String_t*, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1436007573_gshared)(__this, p0, p1, p2, method)
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m4208597231 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2035028495(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t2432241475 *, Object_t692178351 *, MethodInfo_t *, bool, const RuntimeMethod*))CachedInvokableCall_1__ctor_m2035028495_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m4040846306 (InvokableCall_t2697346187 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m3217370930 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C"  ConstructorInfo_t3123539581 * Type_GetConstructor_m816984919 (Type_t * __this, TypeU5BU5D_t1985992169* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t692178351 * ArgumentCache_get_unityObjectArgument_m1826210923 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C"  RuntimeObject * ConstructorInfo_Invoke_m1235543152 (ConstructorInfo_t3123539581 * __this, ObjectU5BU5D_t2737604620* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m1780907418(__this, method) ((  void (*) (List_1_t4123771412 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m3404249592(__this, method) ((  Enumerator_t3899639936  (*) (List_1_t4123771412 *, const RuntimeMethod*))List_1_GetEnumerator_m1837174046_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m3572990374(__this, method) ((  PersistentCall_t709177109 * (*) (Enumerator_t3899639936 *, const RuntimeMethod*))Enumerator_get_Current_m335951430_gshared)(__this, method)
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m1012652439 (PersistentCall_t709177109 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t27726116 * PersistentCall_GetRuntimeCall_m28487261 (PersistentCall_t709177109 * __this, UnityEventBase_t124089244 * ___theEvent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m3955084824 (InvokableCallList_t1048652390 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m1705891082(__this, method) ((  bool (*) (Enumerator_t3899639936 *, const RuntimeMethod*))Enumerator_MoveNext_m1328309074_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m737346653(__this, method) ((  void (*) (Enumerator_t3899639936 *, const RuntimeMethod*))Enumerator_Dispose_m3763436353_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m4162961704 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t27726116 * UnityEvent_GetDelegate_m2995672099 (RuntimeObject * __this /* static, unused */, UnityAction_t992331987 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m504892285 (UnityEventBase_t124089244 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m638354830 (UnityEventBase_t124089244 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m435691116 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1985992169* ___argumentTypes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m3617193550 (InvokableCall_t2697346187 * __this, UnityAction_t992331987 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3701760002 (UnityEventBase_t124089244 * __this, ObjectU5BU5D_t2737604620* ___parameters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m819252217 (InvokableCallList_t1048652390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m741824572 (PersistentCallGroup_t4133256657 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m223105007 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t288530198 * PersistentCall_get_arguments_m3899402300 (PersistentCall_t709177109 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m26565073 (PersistentCall_t709177109 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1074674127 (UnityEventBase_t124089244 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m4038663428 (InvokableCallList_t1048652390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m718681909 (PersistentCallGroup_t4133256657 * __this, InvokableCallList_t1048652390 * ___invokableList0, UnityEventBase_t124089244 * ___unityEventBase1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m1698095211 (InvokableCallList_t1048652390 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m1879627315 (InvokableCallList_t1048652390 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m3827262675 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern "C"  void InvokableCallList_Invoke_m371882796 (InvokableCallList_t1048652390 * __this, ObjectU5BU5D_t2737604620* ___parameters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Object::ToString()
extern "C"  String_t* Object_ToString_m2733393030 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1542845994 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * Type_GetMethod_m1328023905 (Type_t * __this, String_t* p0, int32_t p1, Binder_t1695596754 * p2, TypeU5BU5D_t1985992169* p3, ParameterModifierU5BU5D_t2531273172* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m2293585663 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m2451449986 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m2025082530 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m3160514901 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m3779715225 (ScriptableRenderContext_t2070520004 * __this, intptr_t ___ptr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m1052386618 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m3037547037 (RuntimeObject * __this /* static, unused */, GameObject_t2557347079 * ___mono0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1632713610 * GameObject_AddComponent_m1961781512 (GameObject_t2557347079 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m814261268 (GameObject_t2557347079 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t1632713610 * GameObject_Internal_AddComponentWithType_m3924090706 (GameObject_t2557347079 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::BeginInternal(System.Int32)
extern "C"  void GL_BeginInternal_m1457071361 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_LoadProjectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469 * ___mat0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_GetGPUProjectionMatrix(UnityEngine.Matrix4x4&,System.Boolean,UnityEngine.Matrix4x4&)
extern "C"  void GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469 * ___proj0, bool ___renderIntoTexture1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C"  void GL_Clear_m1308517048 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970  ___backgroundColor2, float ___depth3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C"  void GL_Internal_Clear_m842555462 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970  ___backgroundColor2, float ___depth3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C"  void GL_INTERNAL_CALL_Internal_Clear_m2389845563 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970 * ___backgroundColor2, float ___depth3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m3450230103 (Gradient_t3129154337 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m164069323 (Gradient_t3129154337 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, int32_t ___subsetIndex1, Matrix4x4_t1237934469 * ___matrix2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m932569440 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m22874897 (Vector2_t328513675 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Graphics_Internal_BlitMaterial_m1646902093 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, bool ___setRT4, Vector2_t328513675  ___scale5, Vector2_t328513675  ___offset6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, bool ___setRT4, Vector2_t328513675 * ___scale5, Vector2_t328513675 * ___offset6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
extern "C"  void Graphics_Internal_BlitMultiTap_m1266876306 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, Vector2U5BU5D_t1220531434* ___offsets3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRandomWriteTarget(System.Int32,UnityEngine.ComputeBuffer,System.Boolean)
extern "C"  void Graphics_SetRandomWriteTarget_m3349738363 (RuntimeObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t2358285523 * ___uav1, bool ___preserveCounterValue2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1348886383 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m2302049460 (ObjectDisposedException_t1428820261 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Boolean)
extern "C"  void Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712 (RuntimeObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t2358285523 * ___uav1, bool ___preserveCounterValue2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_Internal_SetRTSimple_m1383567032 (RuntimeObject * __this /* static, unused */, RenderBuffer_t4121715582 * ___color0, RenderBuffer_t4121715582 * ___depth1, int32_t ___mip2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3123183460 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t4121715582  RenderTexture_get_colorBuffer_m425377552 (RenderTexture_t971269558 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t4121715582  RenderTexture_get_depthBuffer_m3164082685 (RenderTexture_t971269558 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m1495557259 (RuntimeObject * __this /* static, unused */, RenderBuffer_t4121715582  ___colorBuffer0, RenderBuffer_t4121715582  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetNullRT()
extern "C"  void Graphics_Internal_SetNullRT_m1010808975 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_Internal_SetMRTSimple_m2432612892 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t846911691* ___colorSA0, RenderBuffer_t4121715582 * ___depth1, int32_t ___mip2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderTexture,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m4294744146 (RuntimeObject * __this /* static, unused */, RenderTexture_t971269558 * ___rt0, int32_t ___mipLevel1, int32_t ___face2, int32_t ___depthSlice3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m3156622254 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t846911691* ___colorBuffers0, RenderBuffer_t4121715582  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4,System.Int32)
extern "C"  void Graphics_DrawMeshNow_m46634237 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, Matrix4x4_t1237934469  ___matrix1, int32_t ___materialIndex2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___x0, Object_t692178351 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_DrawMeshNow2(UnityEngine.Mesh,System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Graphics_Internal_DrawMeshNow2_m2814279584 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, int32_t ___subsetIndex1, Matrix4x4_t1237934469  ___matrix2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
extern "C"  int32_t Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t2946029536 * GUILayer_INTERNAL_CALL_HitTest_m911935273 (RuntimeObject * __this /* static, unused */, GUILayer_t478658086 * ___self0, Vector3_t1986933152 * ___screenPosition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3631383453(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1708198297 *, int32_t, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3631383453_gshared)(__this, p0, p1, method)
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  RuntimeObject * Activator_CreateInstance_m2717577512 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1490102977(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1708198297 *, int32_t, RuntimeObject *, const RuntimeMethod*))Dictionary_2_set_Item_m1490102977_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
#define Dictionary_2__ctor_m2905489384(__this, method) ((  void (*) (Dictionary_2_t1708198297 *, const RuntimeMethod*))Dictionary_2__ctor_m2905489384_gshared)(__this, method)
// System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
extern "C"  void GUITexture_INTERNAL_set_pixelInset_m2310779897 (GUITexture_t2618579966 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
extern "C"  bool Input_GetKeyInt_m3595410015 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
extern "C"  bool Input_GetKeyDownString_m4293950650 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
extern "C"  bool Input_GetKeyDownInt_m626813250 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
extern "C"  bool Input_GetKeyUpInt_m3068478983 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m2679056273 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m1491210786 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_acceleration(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_acceleration_m2630130133 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m2576465830 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1048744301  Input_GetTouch_m3048670206 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m695198429 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t1048744301 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m3585646261 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m1587641303 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m436001336 (DefaultValueAttribute_t3676342093 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Attribute::GetHashCode()
extern "C"  int32_t Attribute_GetHashCode_m2169372130 (Attribute_t1924466020 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m3002643069 (LocalNotification_t1085454254 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.DateTimeKind)
extern "C"  void DateTime__ctor_m4148578998 (DateTime_t359870098 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m916901216 (DateTime_t359870098 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m1964956793 (RemoteNotification_t3274515193 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m3730138296 (AddComponentMenu_t2898687950 * __this, String_t* ___menuName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m572990946 (AddComponentMenu_t2898687950 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke(const AnimationCurve_t2757045165& unmarshaled, AnimationCurve_t2757045165_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke_back(const AnimationCurve_t2757045165_marshaled_pinvoke& marshaled, AnimationCurve_t2757045165& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke_cleanup(AnimationCurve_t2757045165_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2757045165_marshal_com(const AnimationCurve_t2757045165& unmarshaled, AnimationCurve_t2757045165_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2757045165_marshal_com_back(const AnimationCurve_t2757045165_marshaled_com& marshaled, AnimationCurve_t2757045165& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2757045165_marshal_com_cleanup(AnimationCurve_t2757045165_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m3733989894 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t2754673280* L_0 = ___keys0;
		AnimationCurve_Init_m22632116(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m2019935277 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m22632116(__this, (KeyframeU5BU5D_t2754673280*)(KeyframeU5BU5D_t2754673280*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m485238445 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Cleanup_m485238445_ftn) (AnimationCurve_t2757045165 *);
	static AnimationCurve_Cleanup_m485238445_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m485238445_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m571148880 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m485238445(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m821909242 (AnimationCurve_t2757045165 * __this, float ___time0, const RuntimeMethod* method)
{
	typedef float (*AnimationCurve_Evaluate_m821909242_ftn) (AnimationCurve_t2757045165 *, float);
	static AnimationCurve_Evaluate_m821909242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Evaluate_m821909242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Evaluate(System.Single)");
	float retVal = _il2cpp_icall_func(__this, ___time0);
	return retVal;
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern "C"  KeyframeU5BU5D_t2754673280* AnimationCurve_get_keys_m1132786825 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	KeyframeU5BU5D_t2754673280* V_0 = NULL;
	{
		KeyframeU5BU5D_t2754673280* L_0 = AnimationCurve_GetKeys_m3274694562(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		KeyframeU5BU5D_t2754673280* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationCurve::set_keys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_set_keys_m2918718825 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___value0, const RuntimeMethod* method)
{
	{
		KeyframeU5BU5D_t2754673280* L_0 = ___value0;
		AnimationCurve_SetKeys_m1572898885(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.AnimationCurve::AddKey(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_m2974475574 (AnimationCurve_t2757045165 * __this, Keyframe_t3310169645  ___key0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Keyframe_t3310169645  L_0 = ___key0;
		int32_t L_1 = AnimationCurve_AddKey_Internal_m3646094092(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AnimationCurve::AddKey_Internal(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_Internal_m3646094092 (AnimationCurve_t2757045165 * __this, Keyframe_t3310169645  ___key0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367(NULL /*static, unused*/, __this, (&___key0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
extern "C"  int32_t AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367 (RuntimeObject * __this /* static, unused */, AnimationCurve_t2757045165 * ___self0, Keyframe_t3310169645 * ___key1, const RuntimeMethod* method)
{
	typedef int32_t (*AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367_ftn) (AnimationCurve_t2757045165 *, Keyframe_t3310169645 *);
	static AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_INTERNAL_CALL_AddKey_Internal_m1693162367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)");
	int32_t retVal = _il2cpp_icall_func(___self0, ___key1);
	return retVal;
}
// UnityEngine.Keyframe UnityEngine.AnimationCurve::get_Item(System.Int32)
extern "C"  Keyframe_t3310169645  AnimationCurve_get_Item_m110897883 (AnimationCurve_t2757045165 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Keyframe_t3310169645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		Keyframe_t3310169645  L_1 = AnimationCurve_GetKey_Internal_m4240133260(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Keyframe_t3310169645  L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AnimationCurve::get_length()
extern "C"  int32_t AnimationCurve_get_length_m2989711521 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AnimationCurve_get_length_m2989711521_ftn) (AnimationCurve_t2757045165 *);
	static AnimationCurve_get_length_m2989711521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_length_m2989711521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_length()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m1572898885 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_SetKeys_m1572898885_ftn) (AnimationCurve_t2757045165 *, KeyframeU5BU5D_t2754673280*);
	static AnimationCurve_SetKeys_m1572898885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_SetKeys_m1572898885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey_Internal(System.Int32)
extern "C"  Keyframe_t3310169645  AnimationCurve_GetKey_Internal_m4240133260 (AnimationCurve_t2757045165 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Keyframe_t3310169645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Keyframe_t3310169645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Keyframe_t3310169645  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Keyframe_t3310169645  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
extern "C"  void AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819 (RuntimeObject * __this /* static, unused */, AnimationCurve_t2757045165 * ___self0, int32_t ___index1, Keyframe_t3310169645 * ___value2, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819_ftn) (AnimationCurve_t2757045165 *, int32_t, Keyframe_t3310169645 *);
	static AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_INTERNAL_CALL_GetKey_Internal_m2391853819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)");
	_il2cpp_icall_func(___self0, ___index1, ___value2);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t2754673280* AnimationCurve_GetKeys_m3274694562 (AnimationCurve_t2757045165 * __this, const RuntimeMethod* method)
{
	typedef KeyframeU5BU5D_t2754673280* (*AnimationCurve_GetKeys_m3274694562_ftn) (AnimationCurve_t2757045165 *);
	static AnimationCurve_GetKeys_m3274694562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_GetKeys_m3274694562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::GetKeys()");
	KeyframeU5BU5D_t2754673280* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.AnimationCurve UnityEngine.AnimationCurve::Linear(System.Single,System.Single,System.Single,System.Single)
extern "C"  AnimationCurve_t2757045165 * AnimationCurve_Linear_m199798804 (RuntimeObject * __this /* static, unused */, float ___timeStart0, float ___valueStart1, float ___timeEnd2, float ___valueEnd3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurve_Linear_m199798804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	KeyframeU5BU5D_t2754673280* V_1 = NULL;
	AnimationCurve_t2757045165 * V_2 = NULL;
	{
		float L_0 = ___valueEnd3;
		float L_1 = ___valueStart1;
		float L_2 = ___timeEnd2;
		float L_3 = ___timeStart0;
		V_0 = ((float)((float)((float)((float)L_0-(float)L_1))/(float)((float)((float)L_2-(float)L_3))));
		KeyframeU5BU5D_t2754673280* L_4 = ((KeyframeU5BU5D_t2754673280*)SZArrayNew(KeyframeU5BU5D_t2754673280_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_4);
		float L_5 = ___timeStart0;
		float L_6 = ___valueStart1;
		float L_7 = V_0;
		Keyframe_t3310169645  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Keyframe__ctor_m3149517464((&L_8), L_5, L_6, (0.0f), L_7, /*hidden argument*/NULL);
		*(Keyframe_t3310169645 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_8;
		KeyframeU5BU5D_t2754673280* L_9 = L_4;
		NullCheck(L_9);
		float L_10 = ___timeEnd2;
		float L_11 = ___valueEnd3;
		float L_12 = V_0;
		Keyframe_t3310169645  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Keyframe__ctor_m3149517464((&L_13), L_10, L_11, L_12, (0.0f), /*hidden argument*/NULL);
		*(Keyframe_t3310169645 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_13;
		V_1 = L_9;
		KeyframeU5BU5D_t2754673280* L_14 = V_1;
		AnimationCurve_t2757045165 * L_15 = (AnimationCurve_t2757045165 *)il2cpp_codegen_object_new(AnimationCurve_t2757045165_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m3733989894(L_15, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		goto IL_004e;
	}

IL_004e:
	{
		AnimationCurve_t2757045165 * L_16 = V_2;
		return L_16;
	}
}
// UnityEngine.AnimationCurve UnityEngine.AnimationCurve::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern "C"  AnimationCurve_t2757045165 * AnimationCurve_EaseInOut_m1307641671 (RuntimeObject * __this /* static, unused */, float ___timeStart0, float ___valueStart1, float ___timeEnd2, float ___valueEnd3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurve_EaseInOut_m1307641671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyframeU5BU5D_t2754673280* V_0 = NULL;
	AnimationCurve_t2757045165 * V_1 = NULL;
	{
		KeyframeU5BU5D_t2754673280* L_0 = ((KeyframeU5BU5D_t2754673280*)SZArrayNew(KeyframeU5BU5D_t2754673280_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		float L_1 = ___timeStart0;
		float L_2 = ___valueStart1;
		Keyframe_t3310169645  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m3149517464((&L_3), L_1, L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Keyframe_t3310169645 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_3;
		KeyframeU5BU5D_t2754673280* L_4 = L_0;
		NullCheck(L_4);
		float L_5 = ___timeEnd2;
		float L_6 = ___valueEnd3;
		Keyframe_t3310169645  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Keyframe__ctor_m3149517464((&L_7), L_5, L_6, (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Keyframe_t3310169645 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_7;
		V_0 = L_4;
		KeyframeU5BU5D_t2754673280* L_8 = V_0;
		AnimationCurve_t2757045165 * L_9 = (AnimationCurve_t2757045165 *)il2cpp_codegen_object_new(AnimationCurve_t2757045165_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m3733989894(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_004e;
	}

IL_004e:
	{
		AnimationCurve_t2757045165 * L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m22632116 (AnimationCurve_t2757045165 * __this, KeyframeU5BU5D_t2754673280* ___keys0, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Init_m22632116_ftn) (AnimationCurve_t2757045165 *, KeyframeU5BU5D_t2754673280*);
	static AnimationCurve_Init_m22632116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m22632116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// System.Void UnityEngine.Application::CallLowMemory()
extern "C"  void Application_CallLowMemory_m839499845 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLowMemory_m839499845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LowMemoryCallback_t3854352520 * V_0 = NULL;
	{
		LowMemoryCallback_t3854352520 * L_0 = ((Application_t2283285644_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2283285644_il2cpp_TypeInfo_var))->get_lowMemory_0();
		V_0 = L_0;
		LowMemoryCallback_t3854352520 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		LowMemoryCallback_t3854352520 * L_2 = V_0;
		NullCheck(L_2);
		LowMemoryCallback_Invoke_m3092601788(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m996499884 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*Application_Quit_m996499884_ftn) ();
	static Application_Quit_m996499884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m996499884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m3714460646 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isPlaying_m3714460646_ftn) ();
	static Application_get_isPlaying_m3714460646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m3714460646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m3505036892 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isEditor_m3505036892_ftn) ();
	static Application_get_isEditor_m3505036892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m3505036892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m4080113311 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Application_get_platform_m4080113311_ftn) ();
	static Application_get_platform_m4080113311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m4080113311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Application::get_isMobilePlatform()
extern "C"  bool Application_get_isMobilePlatform_m2334826029 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = Application_get_platform_m4080113311(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))))
		{
			case 0:
			{
				goto IL_0045;
			}
			case 1:
			{
				goto IL_0045;
			}
			case 2:
			{
				goto IL_0045;
			}
			case 3:
			{
				goto IL_0028;
			}
			case 4:
			{
				goto IL_0028;
			}
			case 5:
			{
				goto IL_0045;
			}
		}
	}

IL_0028:
	{
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0045;
			}
			case 1:
			{
				goto IL_004c;
			}
			case 2:
			{
				goto IL_004c;
			}
			case 3:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_0045:
	{
		V_1 = (bool)1;
		goto IL_0053;
	}

IL_004c:
	{
		V_1 = (bool)0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m1603997769 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Application_set_runInBackground_m1603997769_ftn) (bool);
	static Application_set_runInBackground_m1603997769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m1603997769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m100508681 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m100508681_ftn) ();
	static Application_get_persistentDataPath_m100508681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m100508681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// System.String UnityEngine.Application::ObjectToJSString(System.Object)
extern "C"  String_t* Application_ObjectToJSString_m508226240 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_ObjectToJSString_m508226240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	NumberFormatInfo_t674921444 * V_2 = NULL;
	float V_3 = 0.0f;
	NumberFormatInfo_t674921444 * V_4 = NULL;
	double V_5 = 0.0;
	RuntimeObject* V_6 = NULL;
	StringBuilder_t622404039 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		RuntimeObject * L_0 = ___o0;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = _stringLiteral593551445;
		goto IL_0237;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___o0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_1, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00b5;
		}
	}
	{
		RuntimeObject * L_2 = ___o0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_3);
		String_t* L_4 = String_Replace_m2628855948(L_3, _stringLiteral2574419025, _stringLiteral2755344190, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m2628855948(L_5, _stringLiteral2596912899, _stringLiteral2699167349, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = String_Replace_m2628855948(L_7, _stringLiteral415700502, _stringLiteral2653896445, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		NullCheck(L_9);
		String_t* L_10 = String_Replace_m2628855948(L_9, _stringLiteral2140564651, _stringLiteral2179767692, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		NullCheck(L_11);
		String_t* L_12 = String_Replace_m2628855948(L_11, _stringLiteral750513360, _stringLiteral4185710657, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = String_Replace_m2628855948(L_13, _stringLiteral565947527, _stringLiteral4185710657, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = String_Replace_m2628855948(L_15, _stringLiteral1604226420, _stringLiteral4185710657, /*hidden argument*/NULL);
		V_1 = L_16;
		Il2CppChar L_17 = ((Il2CppChar)((int32_t)34));
		RuntimeObject * L_18 = Box(Char_t3714759797_il2cpp_TypeInfo_var, &L_17);
		String_t* L_19 = V_1;
		Il2CppChar L_20 = ((Il2CppChar)((int32_t)34));
		RuntimeObject * L_21 = Box(Char_t3714759797_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m712489286(NULL /*static, unused*/, L_18, L_19, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_0237;
	}

IL_00b5:
	{
		RuntimeObject * L_23 = ___o0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_23, Int32_t972567508_il2cpp_TypeInfo_var)))
		{
			goto IL_00ec;
		}
	}
	{
		RuntimeObject * L_24 = ___o0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_24, Int16_t3196095470_il2cpp_TypeInfo_var)))
		{
			goto IL_00ec;
		}
	}
	{
		RuntimeObject * L_25 = ___o0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_25, UInt32_t1721902794_il2cpp_TypeInfo_var)))
		{
			goto IL_00ec;
		}
	}
	{
		RuntimeObject * L_26 = ___o0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_26, UInt16_t14172355_il2cpp_TypeInfo_var)))
		{
			goto IL_00ec;
		}
	}
	{
		RuntimeObject * L_27 = ___o0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_27, Byte_t131097440_il2cpp_TypeInfo_var)))
		{
			goto IL_00f9;
		}
	}

IL_00ec:
	{
		RuntimeObject * L_28 = ___o0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
		V_0 = L_29;
		goto IL_0237;
	}

IL_00f9:
	{
		RuntimeObject * L_30 = ___o0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_30, Single_t485236535_il2cpp_TypeInfo_var)))
		{
			goto IL_0125;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t2625678720_il2cpp_TypeInfo_var);
		CultureInfo_t2625678720 * L_31 = CultureInfo_get_InvariantCulture_m1870357956(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		NumberFormatInfo_t674921444 * L_32 = VirtFuncInvoker0< NumberFormatInfo_t674921444 * >::Invoke(13 /* System.Globalization.NumberFormatInfo System.Globalization.CultureInfo::get_NumberFormat() */, L_31);
		V_2 = L_32;
		RuntimeObject * L_33 = ___o0;
		V_3 = ((*(float*)((float*)UnBox(L_33, Single_t485236535_il2cpp_TypeInfo_var))));
		NumberFormatInfo_t674921444 * L_34 = V_2;
		String_t* L_35 = Single_ToString_m1052142587((&V_3), L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		goto IL_0237;
	}

IL_0125:
	{
		RuntimeObject * L_36 = ___o0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_36, Double_t3752657471_il2cpp_TypeInfo_var)))
		{
			goto IL_0154;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t2625678720_il2cpp_TypeInfo_var);
		CultureInfo_t2625678720 * L_37 = CultureInfo_get_InvariantCulture_m1870357956(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		NumberFormatInfo_t674921444 * L_38 = VirtFuncInvoker0< NumberFormatInfo_t674921444 * >::Invoke(13 /* System.Globalization.NumberFormatInfo System.Globalization.CultureInfo::get_NumberFormat() */, L_37);
		V_4 = L_38;
		RuntimeObject * L_39 = ___o0;
		V_5 = ((*(double*)((double*)UnBox(L_39, Double_t3752657471_il2cpp_TypeInfo_var))));
		NumberFormatInfo_t674921444 * L_40 = V_4;
		String_t* L_41 = Double_ToString_m1506453147((&V_5), L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		goto IL_0237;
	}

IL_0154:
	{
		RuntimeObject * L_42 = ___o0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_42, Char_t3714759797_il2cpp_TypeInfo_var)))
		{
			goto IL_0197;
		}
	}
	{
		RuntimeObject * L_43 = ___o0;
		if ((!(((uint32_t)((*(Il2CppChar*)((Il2CppChar*)UnBox(L_43, Char_t3714759797_il2cpp_TypeInfo_var))))) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0178;
		}
	}
	{
		V_0 = _stringLiteral1366464700;
		goto IL_0237;
	}

IL_0178:
	{
		Il2CppChar L_44 = ((Il2CppChar)((int32_t)34));
		RuntimeObject * L_45 = Box(Char_t3714759797_il2cpp_TypeInfo_var, &L_44);
		RuntimeObject * L_46 = ___o0;
		NullCheck(L_46);
		String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_46);
		Il2CppChar L_48 = ((Il2CppChar)((int32_t)34));
		RuntimeObject * L_49 = Box(Char_t3714759797_il2cpp_TypeInfo_var, &L_48);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m712489286(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		V_0 = L_50;
		goto IL_0237;
	}

IL_0197:
	{
		RuntimeObject * L_51 = ___o0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_51, IList_t2204265861_il2cpp_TypeInfo_var)))
		{
			goto IL_0225;
		}
	}
	{
		RuntimeObject * L_52 = ___o0;
		V_6 = ((RuntimeObject*)Castclass((RuntimeObject*)L_52, IList_t2204265861_il2cpp_TypeInfo_var));
		StringBuilder_t622404039 * L_53 = (StringBuilder_t622404039 *)il2cpp_codegen_object_new(StringBuilder_t622404039_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m443874444(L_53, /*hidden argument*/NULL);
		V_7 = L_53;
		StringBuilder_t622404039 * L_54 = V_7;
		NullCheck(L_54);
		StringBuilder_Append_m3980566461(L_54, _stringLiteral364412306, /*hidden argument*/NULL);
		RuntimeObject* L_55 = V_6;
		NullCheck(L_55);
		int32_t L_56 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761276469_il2cpp_TypeInfo_var, L_55);
		V_8 = L_56;
		V_9 = 0;
		goto IL_0202;
	}

IL_01d0:
	{
		int32_t L_57 = V_9;
		if (!L_57)
		{
			goto IL_01e5;
		}
	}
	{
		StringBuilder_t622404039 * L_58 = V_7;
		NullCheck(L_58);
		StringBuilder_Append_m3980566461(L_58, _stringLiteral591250, /*hidden argument*/NULL);
	}

IL_01e5:
	{
		StringBuilder_t622404039 * L_59 = V_7;
		RuntimeObject* L_60 = V_6;
		int32_t L_61 = V_9;
		NullCheck(L_60);
		RuntimeObject * L_62 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t2204265861_il2cpp_TypeInfo_var, L_60, L_61);
		String_t* L_63 = Application_ObjectToJSString_m508226240(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		NullCheck(L_59);
		StringBuilder_Append_m3980566461(L_59, L_63, /*hidden argument*/NULL);
		int32_t L_64 = V_9;
		V_9 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_0202:
	{
		int32_t L_65 = V_9;
		int32_t L_66 = V_8;
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_01d0;
		}
	}
	{
		StringBuilder_t622404039 * L_67 = V_7;
		NullCheck(L_67);
		StringBuilder_Append_m3980566461(L_67, _stringLiteral2908764213, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_68 = V_7;
		NullCheck(L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_68);
		V_0 = L_69;
		goto IL_0237;
	}

IL_0225:
	{
		RuntimeObject * L_70 = ___o0;
		NullCheck(L_70);
		String_t* L_71 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		String_t* L_72 = Application_ObjectToJSString_m508226240(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		V_0 = L_72;
		goto IL_0237;
	}

IL_0237:
	{
		String_t* L_73 = V_0;
		return L_73;
	}
}
// System.Void UnityEngine.Application::ExternalCall(System.String,System.Object[])
extern "C"  void Application_ExternalCall_m3449264445 (RuntimeObject * __this /* static, unused */, String_t* ___functionName0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___functionName0;
		ObjectU5BU5D_t2737604620* L_1 = ___args1;
		String_t* L_2 = Application_BuildInvocationForArguments_m4046074230(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Application_Internal_ExternalCall_m1484991240(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Application::BuildInvocationForArguments(System.String,System.Object[])
extern "C"  String_t* Application_BuildInvocationForArguments_m4046074230 (RuntimeObject * __this /* static, unused */, String_t* ___functionName0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_BuildInvocationForArguments_m4046074230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t622404039 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		StringBuilder_t622404039 * L_0 = (StringBuilder_t622404039 *)il2cpp_codegen_object_new(StringBuilder_t622404039_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m443874444(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t622404039 * L_1 = V_0;
		String_t* L_2 = ___functionName0;
		NullCheck(L_1);
		StringBuilder_Append_m3980566461(L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_Append_m2167980178(L_3, ((int32_t)40), /*hidden argument*/NULL);
		ObjectU5BU5D_t2737604620* L_4 = ___args1;
		NullCheck(L_4);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))));
		V_2 = 0;
		goto IL_004a;
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		StringBuilder_t622404039 * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Append_m3980566461(L_6, _stringLiteral591250, /*hidden argument*/NULL);
	}

IL_0036:
	{
		StringBuilder_t622404039 * L_7 = V_0;
		ObjectU5BU5D_t2737604620* L_8 = ___args1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		String_t* L_12 = Application_ObjectToJSString_m508226240(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m3980566461(L_7, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_1;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0023;
		}
	}
	{
		StringBuilder_t622404039 * L_16 = V_0;
		NullCheck(L_16);
		StringBuilder_Append_m2167980178(L_16, ((int32_t)41), /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m2167980178(L_17, ((int32_t)59), /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		V_3 = L_19;
		goto IL_006f;
	}

IL_006f:
	{
		String_t* L_20 = V_3;
		return L_20;
	}
}
// System.Void UnityEngine.Application::Internal_ExternalCall(System.String)
extern "C"  void Application_Internal_ExternalCall_m1484991240 (RuntimeObject * __this /* static, unused */, String_t* ___script0, const RuntimeMethod* method)
{
	typedef void (*Application_Internal_ExternalCall_m1484991240_ftn) (String_t*);
	static Application_Internal_ExternalCall_m1484991240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Internal_ExternalCall_m1484991240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Internal_ExternalCall(System.String)");
	_il2cpp_icall_func(___script0);
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C"  void Application_OpenURL_m3919164356 (RuntimeObject * __this /* static, unused */, String_t* ___url0, const RuntimeMethod* method)
{
	typedef void (*Application_OpenURL_m3919164356_ftn) (String_t*);
	static Application_OpenURL_m3919164356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m3919164356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url0);
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m2784979836 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Application_set_targetFrameRate_m2784979836_ftn) (int32_t);
	static Application_set_targetFrameRate_m2784979836_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m2784979836_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern "C"  void Application_CallLogCallback_m2569287725 (RuntimeObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m2569287725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t204719788 * V_0 = NULL;
	LogCallback_t204719788 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t204719788 * L_1 = ((Application_t2283285644_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2283285644_il2cpp_TypeInfo_var))->get_s_LogCallbackHandler_1();
		V_0 = L_1;
		LogCallback_t204719788 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t204719788 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m61920202(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t204719788 * L_7 = ((Application_t2283285644_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2283285644_il2cpp_TypeInfo_var))->get_s_LogCallbackHandlerThreaded_2();
		V_1 = L_7;
		LogCallback_t204719788 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t204719788 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m61920202(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C"  int32_t Application_get_internetReachability_m4029678503 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Application_get_internetReachability_m4029678503_ftn) ();
	static Application_get_internetReachability_m4029678503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_internetReachability_m4029678503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_internetReachability()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern "C"  void Application_InvokeOnBeforeRender_m2592458376 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_InvokeOnBeforeRender_m2592458376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t992331987 * V_0 = NULL;
	{
		UnityAction_t992331987 * L_0 = ((Application_t2283285644_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2283285644_il2cpp_TypeInfo_var))->get_onBeforeRender_3();
		V_0 = L_0;
		UnityAction_t992331987 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UnityAction_t992331987 * L_2 = V_0;
		NullCheck(L_2);
		UnityAction_Invoke_m2407906560(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Int32 UnityEngine.Application::get_loadedLevel()
extern "C"  int32_t Application_get_loadedLevel_m1562173006 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Scene_t965641407  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Scene_t965641407  L_0 = SceneManager_GetActiveScene_m943937788(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m422286238((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
// System.String UnityEngine.Application::get_loadedLevelName()
extern "C"  String_t* Application_get_loadedLevelName_m3316448540 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Scene_t965641407  V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	{
		Scene_t965641407  L_0 = SceneManager_GetActiveScene_m943937788(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m2002147576((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		String_t* L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C"  void Application_LoadLevel_m1393862355 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		SceneManager_LoadScene_m3548089684(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C"  void Application_LoadLevel_m1938940922 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		SceneManager_LoadScene_m1705576468(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t204719788 (LogCallback_t204719788 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m1480033891 (LogCallback_t204719788 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m61920202 (LogCallback_t204719788 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m61920202((LogCallback_t204719788 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LogCallback_BeginInvoke_m3005294547 (LogCallback_t204719788 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t3561663063 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m3005294547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t3717748719_il2cpp_TypeInfo_var, &___type2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m407027289 (LogCallback_t204719788 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_LowMemoryCallback_t3854352520 (LowMemoryCallback_t3854352520 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LowMemoryCallback__ctor_m1754883153 (LowMemoryCallback_t3854352520 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3092601788 (LowMemoryCallback_t3854352520 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LowMemoryCallback_Invoke_m3092601788((LowMemoryCallback_t3854352520 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LowMemoryCallback_BeginInvoke_m1016856967 (LowMemoryCallback_t3854352520 * __this, AsyncCallback_t3561663063 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LowMemoryCallback_EndInvoke_m1728990203 (LowMemoryCallback_t3854352520 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke(const AssetBundleCreateRequest_t1159347791& unmarshaled, AssetBundleCreateRequest_t1159347791_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke_back(const AssetBundleCreateRequest_t1159347791_marshaled_pinvoke& marshaled, AssetBundleCreateRequest_t1159347791& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t1159347791_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke_cleanup(AssetBundleCreateRequest_t1159347791_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_com(const AssetBundleCreateRequest_t1159347791& unmarshaled, AssetBundleCreateRequest_t1159347791_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_com_back(const AssetBundleCreateRequest_t1159347791_marshaled_com& marshaled, AssetBundleCreateRequest_t1159347791& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t1159347791_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_com_cleanup(AssetBundleCreateRequest_t1159347791_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke(const AssetBundleRequest_t230831548& unmarshaled, AssetBundleRequest_t230831548_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke_back(const AssetBundleRequest_t230831548_marshaled_pinvoke& marshaled, AssetBundleRequest_t230831548& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t230831548_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke_cleanup(AssetBundleRequest_t230831548_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t230831548_marshal_com(const AssetBundleRequest_t230831548& unmarshaled, AssetBundleRequest_t230831548_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t230831548_marshal_com_back(const AssetBundleRequest_t230831548_marshaled_com& marshaled, AssetBundleRequest_t230831548& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t230831548_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t230831548_marshal_com_cleanup(AssetBundleRequest_t230831548_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke(const AsyncOperation_t1468153686& unmarshaled, AsyncOperation_t1468153686_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke_back(const AsyncOperation_t1468153686_marshaled_pinvoke& marshaled, AsyncOperation_t1468153686& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t1468153686_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke_cleanup(AsyncOperation_t1468153686_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1468153686_marshal_com(const AsyncOperation_t1468153686& unmarshaled, AsyncOperation_t1468153686_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t1468153686_marshal_com_back(const AsyncOperation_t1468153686_marshaled_com& marshaled, AsyncOperation_t1468153686& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t1468153686_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3739209734>(marshaled.___m_completeCallback_1, Action_1_t3739209734_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1468153686_marshal_com_cleanup(AsyncOperation_t1468153686_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2453508036 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m874787853(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m1729956680 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m1729956680_ftn) (AsyncOperation_t1468153686 *);
	static AsyncOperation_InternalDestroy_m1729956680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m1729956680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m4210132748 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m1729956680(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern "C"  bool AsyncOperation_get_isDone_m2902463919 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method)
{
	typedef bool (*AsyncOperation_get_isDone_m2902463919_ftn) (AsyncOperation_t1468153686 *);
	static AsyncOperation_get_isDone_m2902463919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_isDone_m2902463919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_isDone()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern "C"  void AsyncOperation_InvokeCompletionEvent_m2542318802 (AsyncOperation_t1468153686 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_InvokeCompletionEvent_m2542318802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3739209734 * L_0 = __this->get_m_completeCallback_1();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t3739209734 * L_1 = __this->get_m_completeCallback_1();
		NullCheck(L_1);
		Action_1_Invoke_m14832607(L_1, __this, /*hidden argument*/Action_1_Invoke_m14832607_RuntimeMethod_var);
		__this->set_m_completeCallback_1((Action_1_t3739209734 *)NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m22273868 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m22273868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t2196794798 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t2737604620* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t2196794798 * L_0 = (Stack_1_t2196794798 *)il2cpp_codegen_object_new(Stack_1_t2196794798_il2cpp_TypeInfo_var);
		Stack_1__ctor_m4283643689(L_0, /*hidden argument*/Stack_1__ctor_m4283643689_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t2196794798 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m382399138(L_1, L_2, /*hidden argument*/Stack_1_Push_m382399138_RuntimeMethod_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1618594486_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_6) == ((RuntimeObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t2196794798 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m1148751044(L_8, /*hidden argument*/Stack_1_Pop_m1148751044_RuntimeMethod_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t2744062746_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t2737604620* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t2737604620*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t2737604620* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t2196794798 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m3176329963(L_16, /*hidden argument*/Stack_1_get_Count_m3176329963_RuntimeMethod_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern "C"  TypeU5BU5D_t1985992169* AttributeHelperEngine_GetRequiredComponents_m4242609707 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m4242609707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t987895095 * V_0 = NULL;
	RequireComponentU5BU5D_t2692239366* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t2180947615 * V_3 = NULL;
	RequireComponentU5BU5D_t2692239366* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t1985992169* V_6 = NULL;
	TypeU5BU5D_t1985992169* V_7 = NULL;
	{
		V_0 = (List_1_t987895095 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t2180947615_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t2737604620* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2737604620*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t2692239366*)Castclass((RuntimeObject*)L_2, RequireComponentU5BU5D_t2692239366_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t2692239366* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t2692239366* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t2180947615 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t987895095 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t2692239366* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1618594486_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_12) == ((RuntimeObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t1985992169* L_14 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t2180947615 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1985992169* L_17 = L_14;
		RequireComponent_t2180947615 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t1985992169* L_20 = L_17;
		RequireComponent_t2180947615 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t1985992169* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t987895095 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t987895095 * L_25 = (List_1_t987895095 *)il2cpp_codegen_object_new(List_1_t987895095_il2cpp_TypeInfo_var);
		List_1__ctor_m1591575689(L_25, /*hidden argument*/List_1__ctor_m1591575689_RuntimeMethod_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t2180947615 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t987895095 * L_28 = V_0;
		RequireComponent_t2180947615 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m2945177012(L_28, L_30, /*hidden argument*/List_1_Add_m2945177012_RuntimeMethod_var);
	}

IL_00aa:
	{
		RequireComponent_t2180947615 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t987895095 * L_33 = V_0;
		RequireComponent_t2180947615 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m2945177012(L_33, L_35, /*hidden argument*/List_1_Add_m2945177012_RuntimeMethod_var);
	}

IL_00c1:
	{
		RequireComponent_t2180947615 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t987895095 * L_38 = V_0;
		RequireComponent_t2180947615 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m2945177012(L_38, L_40, /*hidden argument*/List_1_Add_m2945177012_RuntimeMethod_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t2692239366* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1618594486_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_46) == ((RuntimeObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t987895095 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t1985992169*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t987895095 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t1985992169* L_50 = List_1_ToArray_m1572029861(L_49, /*hidden argument*/List_1_ToArray_m1572029861_RuntimeMethod_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t1985992169* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m1500407198 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m1500407198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2737604620* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t869940717_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t2737604620* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2737604620*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t2737604620* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1618594486_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m2644510067 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m2644510067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t334925532 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t4266785651_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t334925532 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t334925532_m3709888332(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t334925532_m3709888332_RuntimeMethod_var);
		V_0 = L_1;
		DefaultExecutionOrder_t334925532 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t334925532 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m3197979243(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern "C"  void AttributeHelperEngine__cctor_m4186283514 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m4186283514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t4266785651_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t4266785651_il2cpp_TypeInfo_var))->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t4108307263*)SZArrayNew(DisallowMultipleComponentU5BU5D_t4108307263_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t4266785651_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t4266785651_il2cpp_TypeInfo_var))->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t3696554944*)SZArrayNew(ExecuteInEditModeU5BU5D_t3696554944_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t4266785651_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t4266785651_il2cpp_TypeInfo_var))->set__requireComponentArray_2(((RequireComponentU5BU5D_t2692239366*)SZArrayNew(RequireComponentU5BU5D_t2692239366_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2653597291 (Behaviour_t2850977393 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m1184908066(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m3996741937 (Behaviour_t2850977393 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_enabled_m3996741937_ftn) (Behaviour_t2850977393 *);
	static Behaviour_get_enabled_m3996741937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m3996741937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m2694832873 (Behaviour_t2850977393 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Behaviour_set_enabled_m2694832873_ftn) (Behaviour_t2850977393 *, bool);
	static Behaviour_set_enabled_m2694832873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m2694832873_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m288851602 (Behaviour_t2850977393 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m288851602_ftn) (Behaviour_t2850977393 *);
	static Behaviour_get_isActiveAndEnabled_m288851602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m288851602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Bindings.UnmarshalledAttribute::.ctor()
extern "C"  void UnmarshalledAttribute__ctor_m2147911925 (UnmarshalledAttribute_t1614977885 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m1878822430 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___center0, Vector3_t1986933152  ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds__ctor_m1878822430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t1986933152  L_1 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Multiply_m952283854(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m1878822430_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___center0, Vector3_t1986933152  ___size1, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds__ctor_m1878822430(_thisAdjusted, ___center0, ___size1, method);
}
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C"  bool Bounds_Internal_Contains_m2180258632 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099  ___m0, Vector3_t1986933152  ___point1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = Bounds_INTERNAL_CALL_Internal_Contains_m1305042420(NULL /*static, unused*/, (&___m0), (&___point1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C"  bool Bounds_INTERNAL_CALL_Internal_Contains_m1305042420 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099 * ___m0, Vector3_t1986933152 * ___point1, const RuntimeMethod* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_Contains_m1305042420_ftn) (Bounds_t3570137099 *, Vector3_t1986933152 *);
	static Bounds_INTERNAL_CALL_Internal_Contains_m1305042420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_Contains_m1305042420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	bool retVal = _il2cpp_icall_func(___m0, ___point1);
	return retVal;
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C"  bool Bounds_Contains_m1117219008 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t1986933152  L_0 = ___point0;
		bool L_1 = Bounds_Internal_Contains_m2180258632(NULL /*static, unused*/, (*(Bounds_t3570137099 *)__this), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool Bounds_Contains_m1117219008_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_Contains_m1117219008(_thisAdjusted, ___point0, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m247994267 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Vector3_t1986933152  L_0 = Bounds_get_center_m292426572(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m4139137898((&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_2 = Bounds_get_extents_m1383416966(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m4139137898((&V_1), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_0032;
	}

IL_0032:
	{
		int32_t L_4 = V_2;
		return L_4;
	}
}
extern "C"  int32_t Bounds_GetHashCode_m247994267_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_GetHashCode_m247994267(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m796471542 (Bounds_t3570137099 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m796471542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Bounds_t3570137099  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t1986933152  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t1986933152  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Bounds_t3570137099_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0068;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Bounds_t3570137099 *)((Bounds_t3570137099 *)UnBox(L_1, Bounds_t3570137099_il2cpp_TypeInfo_var))));
		Vector3_t1986933152  L_2 = Bounds_get_center_m292426572(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t1986933152  L_3 = Bounds_get_center_m292426572((&V_1), /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector3_t1986933152_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m3593875679((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t1986933152  L_7 = Bounds_get_extents_m1383416966(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector3_t1986933152  L_8 = Bounds_get_extents_m1383416966((&V_1), /*hidden argument*/NULL);
		Vector3_t1986933152  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector3_t1986933152_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m3593875679((&V_3), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0062;
	}

IL_0061:
	{
		G_B5_0 = 0;
	}

IL_0062:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0068;
	}

IL_0068:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
extern "C"  bool Bounds_Equals_m796471542_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_Equals_m796471542(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t1986933152  Bounds_get_center_m292426572 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t1986933152  L_0 = __this->get_m_Center_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t1986933152  Bounds_get_center_m292426572_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_get_center_m292426572(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1174052405 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t1986933152  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m1174052405_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_set_center_m1174052405(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t1986933152  Bounds_get_size_m3322399915 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_size_m3322399915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t1986933152  L_0 = __this->get_m_Extents_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = Vector3_op_Multiply_m952283854(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t1986933152  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t1986933152  Bounds_get_size_m3322399915_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_get_size_m3322399915(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m2783247878 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_set_size_m2783247878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = Vector3_op_Multiply_m952283854(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m2783247878_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_set_size_m2783247878(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t1986933152  Bounds_get_extents_m1383416966 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t1986933152  L_0 = __this->get_m_Extents_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t1986933152  Bounds_get_extents_m1383416966_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_get_extents_m1383416966(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2896293588 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t1986933152  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m2896293588_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_set_extents_m2896293588(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t1986933152  Bounds_get_min_m3060588205 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_min_m3060588205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t1986933152  L_0 = Bounds_get_center_m292426572(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = Bounds_get_extents_m1383416966(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t1986933152  Bounds_get_min_m3060588205_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_get_min_m3060588205(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C"  void Bounds_set_min_m3390395710 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t1986933152  L_0 = ___value0;
		Vector3_t1986933152  L_1 = Bounds_get_max_m2926624215(__this, /*hidden argument*/NULL);
		Bounds_SetMinMax_m1440205454(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_set_min_m3390395710_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_set_min_m3390395710(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t1986933152  Bounds_get_max_m2926624215 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_max_m2926624215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t1986933152  L_0 = Bounds_get_center_m292426572(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = Bounds_get_extents_m1383416966(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t1986933152  Bounds_get_max_m2926624215_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_get_max_m2926624215(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C"  void Bounds_set_max_m627008328 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t1986933152  L_0 = Bounds_get_min_m3060588205(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = ___value0;
		Bounds_SetMinMax_m1440205454(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_set_max_m627008328_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_set_max_m627008328(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3484373173 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099  ___lhs0, Bounds_t3570137099  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_op_Equality_m3484373173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Vector3_t1986933152  L_0 = Bounds_get_center_m292426572((&___lhs0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = Bounds_get_center_m292426572((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		bool L_2 = Vector3_op_Equality_m511918437(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Vector3_t1986933152  L_3 = Bounds_get_extents_m1383416966((&___lhs0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = Bounds_get_extents_m1383416966((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		bool L_5 = Vector3_op_Equality_m511918437(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m2293880770 (RuntimeObject * __this /* static, unused */, Bounds_t3570137099  ___lhs0, Bounds_t3570137099  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Bounds_t3570137099  L_0 = ___lhs0;
		Bounds_t3570137099  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m3484373173(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1440205454 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___min0, Vector3_t1986933152  ___max1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_SetMinMax_m1440205454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___max1;
		Vector3_t1986933152  L_1 = ___min0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t1986933152  L_3 = Vector3_op_Multiply_m952283854(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m2896293588(__this, L_3, /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = ___min0;
		Vector3_t1986933152  L_5 = Bounds_get_extents_m1383416966(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_6 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m1174052405(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m1440205454_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___min0, Vector3_t1986933152  ___max1, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_SetMinMax_m1440205454(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m608594558 (Bounds_t3570137099 * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Encapsulate_m608594558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = Bounds_get_min_m3060588205(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = ___point0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_Min_m3122780938(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t1986933152  L_3 = Bounds_get_max_m2926624215(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = ___point0;
		Vector3_t1986933152  L_5 = Vector3_Max_m664546017(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m1440205454(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m608594558_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_Encapsulate_m608594558(_thisAdjusted, ___point0, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C"  void Bounds_Encapsulate_m3380017899 (Bounds_t3570137099 * __this, Bounds_t3570137099  ___bounds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Encapsulate_m3380017899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = Bounds_get_center_m292426572((&___bounds0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = Bounds_get_extents_m1383416966((&___bounds0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Bounds_Encapsulate_m608594558(__this, L_2, /*hidden argument*/NULL);
		Vector3_t1986933152  L_3 = Bounds_get_center_m292426572((&___bounds0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = Bounds_get_extents_m1383416966((&___bounds0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_5 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_Encapsulate_m608594558(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m3380017899_AdjustorThunk (RuntimeObject * __this, Bounds_t3570137099  ___bounds0, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	Bounds_Encapsulate_m3380017899(_thisAdjusted, ___bounds0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m2675624681 (Bounds_t3570137099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m2675624681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t1986933152  L_1 = __this->get_m_Center_0();
		Vector3_t1986933152  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t1986933152_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		Vector3_t1986933152  L_5 = __this->get_m_Extents_1();
		Vector3_t1986933152  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t1986933152_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral656019446, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Bounds_ToString_m2675624681_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3570137099 * _thisAdjusted = reinterpret_cast<Bounds_t3570137099 *>(__this + 1);
	return Bounds_ToString_m2675624681(_thisAdjusted, method);
}
// System.Void UnityEngine.Camera::.ctor()
extern "C"  void Camera__ctor_m2826207447 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2653597291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Camera::get_fov()
extern "C"  float Camera_get_fov_m217120987 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Camera_get_fieldOfView_m417459086(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::set_fov(System.Single)
extern "C"  void Camera_set_fov_m1751807717 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Camera_set_fieldOfView_m4137046919(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m417459086 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_fieldOfView_m417459086_ftn) (Camera_t2839736942 *);
	static Camera_get_fieldOfView_m417459086_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_fieldOfView_m417459086_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_fieldOfView()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m4137046919 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_fieldOfView_m4137046919_ftn) (Camera_t2839736942 *, float);
	static Camera_set_fieldOfView_m4137046919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_fieldOfView_m4137046919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m249543630 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_nearClipPlane_m249543630_ftn) (Camera_t2839736942 *);
	static Camera_get_nearClipPlane_m249543630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m249543630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m1181508651 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_nearClipPlane_m1181508651_ftn) (Camera_t2839736942 *, float);
	static Camera_set_nearClipPlane_m1181508651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_nearClipPlane_m1181508651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_nearClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m713887456 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_farClipPlane_m713887456_ftn) (Camera_t2839736942 *);
	static Camera_get_farClipPlane_m713887456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m713887456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m2682628201 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_farClipPlane_m2682628201_ftn) (Camera_t2839736942 *, float);
	static Camera_set_farClipPlane_m2682628201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_farClipPlane_m2682628201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_farClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
extern "C"  int32_t Camera_get_actualRenderingPath_m3237251681 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_actualRenderingPath_m3237251681_ftn) (Camera_t2839736942 *);
	static Camera_get_actualRenderingPath_m3237251681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_actualRenderingPath_m3237251681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_actualRenderingPath()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Camera::get_allowHDR()
extern "C"  bool Camera_get_allowHDR_m1189377244 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef bool (*Camera_get_allowHDR_m1189377244_ftn) (Camera_t2839736942 *);
	static Camera_get_allowHDR_m1189377244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allowHDR_m1189377244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allowHDR()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Camera::get_hdr()
extern "C"  bool Camera_get_hdr_m3710992627 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef bool (*Camera_get_hdr_m3710992627_ftn) (Camera_t2839736942 *);
	static Camera_get_hdr_m3710992627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_hdr_m3710992627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_hdr()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m1626593557 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_orthographicSize_m1626593557_ftn) (Camera_t2839736942 *);
	static Camera_get_orthographicSize_m1626593557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographicSize_m1626593557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographicSize()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m2482809139 (Camera_t2839736942 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_orthographicSize_m2482809139_ftn) (Camera_t2839736942 *, float);
	static Camera_set_orthographicSize_m2482809139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m2482809139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m612865215 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef bool (*Camera_get_orthographic_m612865215_ftn) (Camera_t2839736942 *);
	static Camera_get_orthographic_m612865215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographic_m612865215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographic()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m1450410369 (Camera_t2839736942 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_orthographic_m1450410369_ftn) (Camera_t2839736942 *, bool);
	static Camera_set_orthographic_m1450410369_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m1450410369_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m3938870720 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_depth_m3938870720_ftn) (Camera_t2839736942 *);
	static Camera_get_depth_m3938870720_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m3938870720_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m3451967663 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_aspect_m3451967663_ftn) (Camera_t2839736942 *);
	static Camera_get_aspect_m3451967663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_aspect_m3451967663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_aspect()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m3464126921 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_cullingMask_m3464126921_ftn) (Camera_t2839736942 *);
	static Camera_get_cullingMask_m3464126921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m3464126921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m3619635914 (Camera_t2839736942 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_cullingMask_m3619635914_ftn) (Camera_t2839736942 *, int32_t);
	static Camera_set_cullingMask_m3619635914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m3619635914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3749265454 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_eventMask_m3749265454_ftn) (Camera_t2839736942 *);
	static Camera_get_eventMask_m3749265454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m3749265454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C"  Rect_t3039462994  Camera_get_rect_m3729642807 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	Rect_t3039462994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3039462994  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_rect_m3547426077(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3039462994  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3039462994  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C"  void Camera_set_rect_m3059066447 (Camera_t2839736942 * __this, Rect_t3039462994  ___value0, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_set_rect_m639478796(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_rect_m3547426077 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_rect_m3547426077_ftn) (Camera_t2839736942 *, Rect_t3039462994 *);
	static Camera_INTERNAL_get_rect_m3547426077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_rect_m3547426077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_set_rect_m639478796 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m639478796_ftn) (Camera_t2839736942 *, Rect_t3039462994 *);
	static Camera_INTERNAL_set_rect_m639478796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m639478796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3039462994  Camera_get_pixelRect_m3077452777 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	Rect_t3039462994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3039462994  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m4233702303(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3039462994  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3039462994  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m4233702303 (Camera_t2839736942 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m4233702303_ftn) (Camera_t2839736942 *, Rect_t3039462994 *);
	static Camera_INTERNAL_get_pixelRect_m4233702303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m4233702303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t971269558 * Camera_get_targetTexture_m478696062 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef RenderTexture_t971269558 * (*Camera_get_targetTexture_m478696062_ftn) (Camera_t2839736942 *);
	static Camera_get_targetTexture_m478696062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m478696062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	RenderTexture_t971269558 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m264579770 (Camera_t2839736942 * __this, RenderTexture_t971269558 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_targetTexture_m264579770_ftn) (Camera_t2839736942 *, RenderTexture_t971269558 *);
	static Camera_set_targetTexture_m264579770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_targetTexture_m264579770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m3873813721 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_pixelWidth_m3873813721_ftn) (Camera_t2839736942 *);
	static Camera_get_pixelWidth_m3873813721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_pixelWidth_m3873813721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_pixelWidth()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m981527012 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_pixelHeight_m981527012_ftn) (Camera_t2839736942 *);
	static Camera_get_pixelHeight_m981527012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_pixelHeight_m981527012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_pixelHeight()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t1237934469  Camera_get_worldToCameraMatrix_m276423131 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_worldToCameraMatrix_m4166556156(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t1237934469  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_worldToCameraMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_worldToCameraMatrix_m4166556156 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_worldToCameraMatrix_m4166556156_ftn) (Camera_t2839736942 *, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_get_worldToCameraMatrix_m4166556156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_worldToCameraMatrix_m4166556156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_worldToCameraMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1237934469  Camera_get_projectionMatrix_m3003477762 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_projectionMatrix_m2936427189(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t1237934469  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m3469231914 (Camera_t2839736942 * __this, Matrix4x4_t1237934469  ___value0, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m2733397033(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_projectionMatrix_m2936427189 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_projectionMatrix_m2936427189_ftn) (Camera_t2839736942 *, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_get_projectionMatrix_m2936427189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_projectionMatrix_m2936427189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m2733397033 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m2733397033_ftn) (Camera_t2839736942 *, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_set_projectionMatrix_m2733397033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m2733397033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_nonJitteredProjectionMatrix_m1943499683 (Camera_t2839736942 * __this, Matrix4x4_t1237934469  ___value0, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705 (Camera_t2839736942 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705_ftn) (Camera_t2839736942 *, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_nonJitteredProjectionMatrix_m1069932705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::set_useJitteredProjectionMatrixForTransparentRendering(System.Boolean)
extern "C"  void Camera_set_useJitteredProjectionMatrixForTransparentRendering_m183099962 (Camera_t2839736942 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_useJitteredProjectionMatrixForTransparentRendering_m183099962_ftn) (Camera_t2839736942 *, bool);
	static Camera_set_useJitteredProjectionMatrixForTransparentRendering_m183099962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_useJitteredProjectionMatrixForTransparentRendering_m183099962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_useJitteredProjectionMatrixForTransparentRendering(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::ResetProjectionMatrix()
extern "C"  void Camera_ResetProjectionMatrix_m1631649794 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)
extern "C"  void Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092_ftn) (Camera_t2839736942 *);
	static Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ResetProjectionMatrix_m1626840092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m950950364 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_clearFlags_m950950364_ftn) (Camera_t2839736942 *);
	static Camera_get_clearFlags_m950950364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m950950364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C"  void Camera_set_clearFlags_m2088707821 (Camera_t2839736942 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_clearFlags_m2088707821_ftn) (Camera_t2839736942 *, int32_t);
	static Camera_set_clearFlags_m2088707821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_clearFlags_m2088707821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Camera::get_stereoEnabled()
extern "C"  bool Camera_get_stereoEnabled_m715884953 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef bool (*Camera_get_stereoEnabled_m715884953_ftn) (Camera_t2839736942 *);
	static Camera_get_stereoEnabled_m715884953_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_stereoEnabled_m715884953_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_stereoEnabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::GetStereoViewMatrix(UnityEngine.Camera/StereoscopicEye)
extern "C"  Matrix4x4_t1237934469  Camera_GetStereoViewMatrix_m2013752869 (Camera_t2839736942 * __this, int32_t ___eye0, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___eye0;
		Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Matrix4x4_t1237934469  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_GetStereoViewMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, int32_t ___eye1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774_ftn) (Camera_t2839736942 *, int32_t, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_GetStereoViewMatrix_m225000774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_GetStereoViewMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___self0, ___eye1, ___value2);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::GetStereoProjectionMatrix(UnityEngine.Camera/StereoscopicEye)
extern "C"  Matrix4x4_t1237934469  Camera_GetStereoProjectionMatrix_m777841534 (Camera_t2839736942 * __this, int32_t ___eye0, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___eye0;
		Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Matrix4x4_t1237934469  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_GetStereoProjectionMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, int32_t ___eye1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157_ftn) (Camera_t2839736942 *, int32_t, Matrix4x4_t1237934469 *);
	static Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_GetStereoProjectionMatrix_m116919157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_GetStereoProjectionMatrix(UnityEngine.Camera,UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___self0, ___eye1, ___value2);
}
// UnityEngine.Camera/MonoOrStereoscopicEye UnityEngine.Camera::get_stereoActiveEye()
extern "C"  int32_t Camera_get_stereoActiveEye_m4227403777 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_stereoActiveEye_m4227403777_ftn) (Camera_t2839736942 *);
	static Camera_get_stereoActiveEye_m4227403777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_stereoActiveEye_m4227403777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_stereoActiveEye()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::CalculateFrustumCorners(UnityEngine.Rect,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])
extern "C"  void Camera_CalculateFrustumCorners_m1147008066 (Camera_t2839736942 * __this, Rect_t3039462994  ___viewport0, float ___z1, int32_t ___eye2, Vector3U5BU5D_t32224225* ___outCorners3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_CalculateFrustumCorners_m1147008066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3U5BU5D_t32224225* L_0 = ___outCorners3;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, _stringLiteral2356465786, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Vector3U5BU5D_t32224225* L_2 = ___outCorners3;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		ArgumentException_t1812645948 * L_3 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_3, _stringLiteral330691706, _stringLiteral2356465786, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002d:
	{
		Rect_t3039462994  L_4 = ___viewport0;
		float L_5 = ___z1;
		int32_t L_6 = ___eye2;
		Vector3U5BU5D_t32224225* L_7 = ___outCorners3;
		Camera_CalculateFrustumCornersInternal_m3163453171(__this, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::CalculateFrustumCornersInternal(UnityEngine.Rect,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])
extern "C"  void Camera_CalculateFrustumCornersInternal_m3163453171 (Camera_t2839736942 * __this, Rect_t3039462994  ___viewport0, float ___z1, int32_t ___eye2, Vector3U5BU5D_t32224225* ___outCorners3, const RuntimeMethod* method)
{
	{
		float L_0 = ___z1;
		int32_t L_1 = ___eye2;
		Vector3U5BU5D_t32224225* L_2 = ___outCorners3;
		Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332(NULL /*static, unused*/, __this, (&___viewport0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_CalculateFrustumCornersInternal(UnityEngine.Camera,UnityEngine.Rect&,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])
extern "C"  void Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Rect_t3039462994 * ___viewport1, float ___z2, int32_t ___eye3, Vector3U5BU5D_t32224225* ___outCorners4, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332_ftn) (Camera_t2839736942 *, Rect_t3039462994 *, float, int32_t, Vector3U5BU5D_t32224225*);
	static Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_CalculateFrustumCornersInternal_m1213139332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_CalculateFrustumCornersInternal(UnityEngine.Camera,UnityEngine.Rect&,System.Single,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3[])");
	_il2cpp_icall_func(___self0, ___viewport1, ___z2, ___eye3, ___outCorners4);
}
// System.Int32 UnityEngine.Camera::get_targetDisplay()
extern "C"  int32_t Camera_get_targetDisplay_m3159368927 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_targetDisplay_m3159368927_ftn) (Camera_t2839736942 *);
	static Camera_get_targetDisplay_m3159368927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetDisplay_m3159368927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetDisplay()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_WorldToScreenPoint_m2466514990 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m2588471566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_WorldToViewportPoint_m781876753 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToViewportPoint_m4147633_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_ViewportToWorldPoint_m1557520116 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ViewportToWorldPoint_m2052081641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_ScreenToWorldPoint_m2205839903 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m2031988988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Camera_ScreenToViewportPoint_m1101638903 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m2738878788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t249333749  Camera_ScreenPointToRay_m1212959324 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Ray_t249333749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t249333749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t249333749  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t249333749  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Vector3_t1986933152 * ___position1, Ray_t249333749 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832_ftn) (Camera_t2839736942 *, Vector3_t1986933152 *, Ray_t249333749 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m2134941832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t2839736942 * Camera_get_main_m1102209922 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Camera_t2839736942 * (*Camera_get_main_m1102209922_ftn) ();
	static Camera_get_main_m1102209922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m1102209922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	Camera_t2839736942 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1216147617 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m1216147617_ftn) ();
	static Camera_get_allCamerasCount_m1216147617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m1216147617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m3041928650 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t4045078555* ___cameras0, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_GetAllCameras_m3041928650_ftn) (CameraU5BU5D_t4045078555*);
	static Camera_GetAllCameras_m3041928650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m3041928650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	int32_t retVal = _il2cpp_icall_func(___cameras0);
	return retVal;
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreCull_m3877489917 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m3877489917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t657915608 * L_0 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t657915608 * L_1 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPreCull_2();
		Camera_t2839736942 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1277705143(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreRender_m3318235530 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m3318235530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t657915608 * L_0 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t657915608 * L_1 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPreRender_3();
		Camera_t2839736942 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1277705143(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPostRender_m3483601918 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m3483601918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t657915608 * L_0 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t657915608 * L_1 = ((Camera_t2839736942_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t2839736942_il2cpp_TypeInfo_var))->get_onPostRender_4();
		Camera_t2839736942 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1277705143(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::RenderWithShader(UnityEngine.Shader,System.String)
extern "C"  void Camera_RenderWithShader_m573551075 (Camera_t2839736942 * __this, Shader_t1881769421 * ___shader0, String_t* ___replacementTag1, const RuntimeMethod* method)
{
	typedef void (*Camera_RenderWithShader_m573551075_ftn) (Camera_t2839736942 *, Shader_t1881769421 *, String_t*);
	static Camera_RenderWithShader_m573551075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RenderWithShader_m573551075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RenderWithShader(UnityEngine.Shader,System.String)");
	_il2cpp_icall_func(__this, ___shader0, ___replacementTag1);
}
// System.Void UnityEngine.Camera::CopyFrom(UnityEngine.Camera)
extern "C"  void Camera_CopyFrom_m2254969364 (Camera_t2839736942 * __this, Camera_t2839736942 * ___other0, const RuntimeMethod* method)
{
	typedef void (*Camera_CopyFrom_m2254969364_ftn) (Camera_t2839736942 *, Camera_t2839736942 *);
	static Camera_CopyFrom_m2254969364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_CopyFrom_m2254969364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::CopyFrom(UnityEngine.Camera)");
	_il2cpp_icall_func(__this, ___other0);
}
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m3812497418 (Camera_t2839736942 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_depthTextureMode_m3812497418_ftn) (Camera_t2839736942 *);
	static Camera_get_depthTextureMode_m3812497418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depthTextureMode_m3812497418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depthTextureMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m61897735 (Camera_t2839736942 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_set_depthTextureMode_m61897735_ftn) (Camera_t2839736942 *, int32_t);
	static Camera_set_depthTextureMode_m61897735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_depthTextureMode_m61897735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m3007491267 (Camera_t2839736942 * __this, int32_t ___evt0, CommandBuffer_t1939304854 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_AddCommandBuffer_m3007491267_ftn) (Camera_t2839736942 *, int32_t, CommandBuffer_t1939304854 *);
	static Camera_AddCommandBuffer_m3007491267_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_AddCommandBuffer_m3007491267_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m4227654437 (Camera_t2839736942 * __this, int32_t ___evt0, CommandBuffer_t1939304854 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_RemoveCommandBuffer_m4227654437_ftn) (Camera_t2839736942 *, int32_t, CommandBuffer_t1939304854 *);
	static Camera_RemoveCommandBuffer_m4227654437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RemoveCommandBuffer_m4227654437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_RaycastTry_m736311948 (Camera_t2839736942 * __this, Ray_t249333749  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t2557347079 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t2557347079 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m2381035361(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t2557347079 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_INTERNAL_CALL_RaycastTry_m2381035361 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Ray_t249333749 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t2557347079 * (*Camera_INTERNAL_CALL_RaycastTry_m2381035361_ftn) (Camera_t2839736942 *, Ray_t249333749 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m2381035361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m2381035361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t2557347079 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_RaycastTry2D_m3310148661 (Camera_t2839736942 * __this, Ray_t249333749  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t2557347079 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t2557347079 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m152944706(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t2557347079 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_INTERNAL_CALL_RaycastTry2D_m152944706 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___self0, Ray_t249333749 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t2557347079 * (*Camera_INTERNAL_CALL_RaycastTry2D_m152944706_ftn) (Camera_t2839736942 *, Ray_t249333749 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m152944706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m152944706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t2557347079 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m4050512374 (CameraCallback_t657915608 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m1277705143 (CameraCallback_t657915608 * __this, Camera_t2839736942 * ___cam0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m1277705143((CameraCallback_t657915608 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Camera_t2839736942 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t2839736942 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CameraCallback_BeginInvoke_m1792822160 (CameraCallback_t657915608 * __this, Camera_t2839736942 * ___cam0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m2208400768 (CameraCallback_t657915608 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.ClassLibraryInitializer::Init()
extern "C"  void ClassLibraryInitializer_Init_m1868767943 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		UnityLogWriter_Init_m532212392(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2237324961 (Color_t2582018970 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m2237324961_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	Color__ctor_m2237324961(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3027879795 (Color_t2582018970 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m3027879795_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	Color__ctor_m3027879795(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m2932792595 (Color_t2582018970 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m2932792595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2737604620* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2737604620* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral4171082218, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color_ToString_m2932792595_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	return Color_ToString_m2932792595(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m4075930393 (Color_t2582018970 * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector4_t2436299922  L_0 = Color_op_Implicit_m2059778741(NULL /*static, unused*/, (*(Color_t2582018970 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3459216764((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0020;
	}

IL_0020:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t Color_GetHashCode_m4075930393_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	return Color_GetHashCode_m4075930393(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m3159899910 (Color_t2582018970 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m3159899910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Color_t2582018970  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Color_t2582018970_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Color_t2582018970 *)((Color_t2582018970 *)UnBox(L_1, Color_t2582018970_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_1)->get_r_0();
		bool L_4 = Single_Equals_m2960811275(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_1)->get_g_1();
		bool L_7 = Single_Equals_m2960811275(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_1)->get_b_2();
		bool L_10 = Single_Equals_m2960811275(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_1)->get_a_3();
		bool L_13 = Single_Equals_m2960811275(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Color_Equals_m3159899910_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	return Color_Equals_m3159899910(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t2582018970  Color_op_Multiply_m722506647 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___a0, float ___b1, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t2582018970  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2237324961((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Color_t2582018970  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(System.Single,UnityEngine.Color)
extern "C"  Color_t2582018970  Color_op_Multiply_m2178997031 (RuntimeObject * __this /* static, unused */, float ___b0, Color_t2582018970  ___a1, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a1)->get_r_0();
		float L_1 = ___b0;
		float L_2 = (&___a1)->get_g_1();
		float L_3 = ___b0;
		float L_4 = (&___a1)->get_b_2();
		float L_5 = ___b0;
		float L_6 = (&___a1)->get_a_3();
		float L_7 = ___b0;
		Color_t2582018970  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2237324961((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Color_t2582018970  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m1988218143 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___lhs0, Color_t2582018970  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_op_Equality_m1988218143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Color_t2582018970  L_0 = ___lhs0;
		Vector4_t2436299922  L_1 = Color_op_Implicit_m2059778741(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t2582018970  L_2 = ___rhs1;
		Vector4_t2436299922  L_3 = Color_op_Implicit_m2059778741(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		bool L_4 = Vector4_op_Equality_m1687602531(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2582018970  Color_Lerp_m615672158 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___a0, Color_t2582018970  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m615672158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t2582018970  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m2237324961((&L_18), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_0078;
	}

IL_0078:
	{
		Color_t2582018970  L_19 = V_0;
		return L_19;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2582018970  Color_get_red_m78881714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2582018970  Color_get_green_m4040542567 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C"  Color_t2582018970  Color_get_blue_m3937245342 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2582018970  Color_get_white_m2500342551 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2582018970  Color_get_black_m3247529257 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2582018970  Color_get_yellow_m3137166995 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_cyan()
extern "C"  Color_t2582018970  Color_get_cyan_m1891517974 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_magenta()
extern "C"  Color_t2582018970  Color_get_magenta_m4155925577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (1.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C"  Color_t2582018970  Color_get_gray_m2746971647 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2582018970  Color_get_grey_m4147229709 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2582018970  Color_get_clear_m2522444181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2582018970  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2237324961((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2582018970  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t2436299922  Color_op_Implicit_m2059778741 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___c0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t2436299922  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m1296751250((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		Vector4_t2436299922  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Color::set_Item(System.Int32,System.Single)
extern "C"  void Color_set_Item_m51493835 (Color_t2582018970 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_set_Item_m51493835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0034;
			}
			case 3:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_001c:
	{
		float L_1 = ___value1;
		__this->set_r_0(L_1);
		goto IL_0057;
	}

IL_0028:
	{
		float L_2 = ___value1;
		__this->set_g_1(L_2);
		goto IL_0057;
	}

IL_0034:
	{
		float L_3 = ___value1;
		__this->set_b_2(L_3);
		goto IL_0057;
	}

IL_0040:
	{
		float L_4 = ___value1;
		__this->set_a_3(L_4);
		goto IL_0057;
	}

IL_004c:
	{
		IndexOutOfRangeException_t2909334802 * L_5 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_5, _stringLiteral3205361995, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0057:
	{
		return;
	}
}
extern "C"  void Color_set_Item_m51493835_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Color_t2582018970 * _thisAdjusted = reinterpret_cast<Color_t2582018970 *>(__this + 1);
	Color_set_Item_m51493835(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m1812196269 (Color32_t3428513655 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m1812196269_AdjustorThunk (RuntimeObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	Color32_t3428513655 * _thisAdjusted = reinterpret_cast<Color32_t3428513655 *>(__this + 1);
	Color32__ctor_m1812196269(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t3428513655  Color32_op_Implicit_m1748363328 (RuntimeObject * __this /* static, unused */, Color_t2582018970  ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m1748363328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t3428513655  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t3428513655  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m1812196269((&L_8), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0058;
	}

IL_0058:
	{
		Color32_t3428513655  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t2582018970  Color32_op_Implicit_m4217851924 (RuntimeObject * __this /* static, unused */, Color32_t3428513655  ___c0, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t2582018970  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m2237324961((&L_4), ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0044;
	}

IL_0044:
	{
		Color_t2582018970  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m2415660705 (Color32_t3428513655 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m2415660705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Byte_t131097440_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Byte_t131097440_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2737604620* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Byte_t131097440_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2737604620* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Byte_t131097440_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral3495395157, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color32_ToString_m2415660705_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color32_t3428513655 * _thisAdjusted = reinterpret_cast<Color32_t3428513655 *>(__this + 1);
	return Color32_ToString_m2415660705(_thisAdjusted, method);
}
// System.Void UnityEngine.ColorUsageAttribute::.ctor(System.Boolean)
extern "C"  void ColorUsageAttribute__ctor_m2041796422 (ColorUsageAttribute_t1858490149 * __this, bool ___showAlpha0, const RuntimeMethod* method)
{
	{
		__this->set_showAlpha_0((bool)1);
		__this->set_hdr_1((bool)0);
		__this->set_minBrightness_2((0.0f));
		__this->set_maxBrightness_3((8.0f));
		__this->set_minExposureValue_4((0.125f));
		__this->set_maxExposureValue_5((3.0f));
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		bool L_0 = ___showAlpha0;
		__this->set_showAlpha_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m1184908066 (Component_t1632713610 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m1184908066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t362059596 * Component_get_transform_m520192871 (Component_t1632713610 * __this, const RuntimeMethod* method)
{
	typedef Transform_t362059596 * (*Component_get_transform_m520192871_ftn) (Component_t1632713610 *);
	static Component_get_transform_m520192871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m520192871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	Transform_t362059596 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2557347079 * Component_get_gameObject_m1032427207 (Component_t1632713610 * __this, const RuntimeMethod* method)
{
	typedef GameObject_t2557347079 * (*Component_get_gameObject_m1032427207_ftn) (Component_t1632713610 *);
	static Component_get_gameObject_m1032427207_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m1032427207_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	GameObject_t2557347079 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1632713610 * Component_GetComponent_m2808223086 (Component_t1632713610 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	Component_t1632713610 * V_0 = NULL;
	{
		GameObject_t2557347079 * L_0 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t1632713610 * L_2 = GameObject_GetComponent_m469693343(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t1632713610 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m2636512973 (Component_t1632713610 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentFastPath_m2636512973_ftn) (Component_t1632713610 *, Type_t *, intptr_t);
	static Component_GetComponentFastPath_m2636512973_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m2636512973_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1632713610 * Component_GetComponentInChildren_m2999727513 (Component_t1632713610 * __this, Type_t * ___t0, bool ___includeInactive1, const RuntimeMethod* method)
{
	Component_t1632713610 * V_0 = NULL;
	{
		GameObject_t2557347079 * L_0 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t1632713610 * L_3 = GameObject_GetComponentInChildren_m1977537235(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Component_t1632713610 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t1632713610 * Component_GetComponentInParent_m1409804302 (Component_t1632713610 * __this, Type_t * ___t0, const RuntimeMethod* method)
{
	Component_t1632713610 * V_0 = NULL;
	{
		GameObject_t2557347079 * L_0 = Component_get_gameObject_m1032427207(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t1632713610 * L_2 = GameObject_GetComponentInParent_m955227985(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t1632713610 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m3322214901 (Component_t1632713610 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentsForListInternal_m3322214901_ftn) (Component_t1632713610 *, Type_t *, RuntimeObject *);
	static Component_GetComponentsForListInternal_m3322214901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m3322214901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m501425980 (Component_t1632713610 * __this, Type_t * ___type0, List_1_t752340617 * ___results1, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t752340617 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m3322214901(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m398105859 (Component_t1632713610 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*Component_SendMessage_m398105859_ftn) (Component_t1632713610 *, String_t*, RuntimeObject *, int32_t);
	static Component_SendMessage_m398105859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m398105859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m1254570625 (Component_t1632713610 * __this, String_t* ___methodName0, RuntimeObject * ___parameter1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*Component_BroadcastMessage_m1254570625_ftn) (Component_t1632713610 *, String_t*, RuntimeObject *, int32_t);
	static Component_BroadcastMessage_m1254570625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_BroadcastMessage_m1254570625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___parameter1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m4059855442 (Component_t1632713610 * __this, String_t* ___methodName0, int32_t ___options1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_BroadcastMessage_m1254570625(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32)
extern "C"  void ComputeBuffer__ctor_m57309873 (ComputeBuffer_t2358285523 * __this, int32_t ___count0, int32_t ___stride1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count0;
		int32_t L_1 = ___stride1;
		ComputeBuffer__ctor_m421042894(__this, L_0, L_1, 0, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32,UnityEngine.ComputeBufferType)
extern "C"  void ComputeBuffer__ctor_m1448904836 (ComputeBuffer_t2358285523 * __this, int32_t ___count0, int32_t ___stride1, int32_t ___type2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count0;
		int32_t L_1 = ___stride1;
		int32_t L_2 = ___type2;
		ComputeBuffer__ctor_m421042894(__this, L_0, L_1, L_2, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32,UnityEngine.ComputeBufferType,System.Int32)
extern "C"  void ComputeBuffer__ctor_m421042894 (ComputeBuffer_t2358285523 * __this, int32_t ___count0, int32_t ___stride1, int32_t ___type2, int32_t ___stackDepth3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComputeBuffer__ctor_m421042894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		ArgumentException_t1812645948 * L_1 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_1, _stringLiteral1719531609, _stringLiteral736989972, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001f:
	{
		int32_t L_2 = ___stride1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		ArgumentException_t1812645948 * L_3 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_3, _stringLiteral2938520293, _stringLiteral912724562, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0037:
	{
		__this->set_m_Ptr_0((intptr_t)(0));
		int32_t L_4 = ___count0;
		int32_t L_5 = ___stride1;
		int32_t L_6 = ___type2;
		ComputeBuffer_InitBuffer_m4240887542(NULL /*static, unused*/, __this, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::Finalize()
extern "C"  void ComputeBuffer_Finalize_m4033380879 (ComputeBuffer_t2358285523 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		ComputeBuffer_Dispose_m1759304253(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000d;
	}

FINALLY_000d:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(13)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(13)
	{
		IL2CPP_JUMP_TBL(0x14, IL_0014)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::Dispose()
extern "C"  void ComputeBuffer_Dispose_m2324970370 (ComputeBuffer_t2358285523 * __this, const RuntimeMethod* method)
{
	{
		ComputeBuffer_Dispose_m1759304253(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m3739586421(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::Dispose(System.Boolean)
extern "C"  void ComputeBuffer_Dispose_m1759304253 (ComputeBuffer_t2358285523 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComputeBuffer_Dispose_m1759304253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		ComputeBuffer_DestroyBuffer_m22290331(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_0014:
	{
		intptr_t L_1 = __this->get_m_Ptr_0();
		bool L_2 = IntPtr_op_Inequality_m2291805156(NULL /*static, unused*/, L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3275468740(NULL /*static, unused*/, _stringLiteral3043668361, /*hidden argument*/NULL);
	}

IL_0035:
	{
		__this->set_m_Ptr_0((intptr_t)(0));
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)
extern "C"  void ComputeBuffer_InitBuffer_m4240887542 (RuntimeObject * __this /* static, unused */, ComputeBuffer_t2358285523 * ___buf0, int32_t ___count1, int32_t ___stride2, int32_t ___type3, const RuntimeMethod* method)
{
	typedef void (*ComputeBuffer_InitBuffer_m4240887542_ftn) (ComputeBuffer_t2358285523 *, int32_t, int32_t, int32_t);
	static ComputeBuffer_InitBuffer_m4240887542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeBuffer_InitBuffer_m4240887542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)");
	_il2cpp_icall_func(___buf0, ___count1, ___stride2, ___type3);
}
// System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
extern "C"  void ComputeBuffer_DestroyBuffer_m22290331 (RuntimeObject * __this /* static, unused */, ComputeBuffer_t2358285523 * ___buf0, const RuntimeMethod* method)
{
	typedef void (*ComputeBuffer_DestroyBuffer_m22290331_ftn) (ComputeBuffer_t2358285523 *);
	static ComputeBuffer_DestroyBuffer_m22290331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeBuffer_DestroyBuffer_m22290331_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)");
	_il2cpp_icall_func(___buf0);
}
// System.Void UnityEngine.ComputeBuffer::Release()
extern "C"  void ComputeBuffer_Release_m3713276049 (ComputeBuffer_t2358285523 * __this, const RuntimeMethod* method)
{
	{
		ComputeBuffer_Dispose_m2324970370(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::SetData(System.Array)
extern "C"  void ComputeBuffer_SetData_m1640696821 (ComputeBuffer_t2358285523 * __this, RuntimeArray * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComputeBuffer_SetData_m1640696821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeArray * L_0 = ___data0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, _stringLiteral4132129454, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeArray * L_2 = ___data0;
		RuntimeArray * L_3 = ___data0;
		NullCheck(L_3);
		int32_t L_4 = Array_get_Length_m2853335497(L_3, /*hidden argument*/NULL);
		RuntimeArray * L_5 = ___data0;
		NullCheck(L_5);
		Type_t * L_6 = Object_GetType_m2972540513(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Type_t * L_7 = VirtFuncInvoker0< Type_t * >::Invoke(42 /* System.Type System.Type::GetElementType() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t2040402858_il2cpp_TypeInfo_var);
		int32_t L_8 = Marshal_SizeOf_m2163745240(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		ComputeBuffer_InternalSetData_m3847349839(__this, L_2, 0, 0, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void ComputeBuffer_InternalSetData_m3847349839 (ComputeBuffer_t2358285523 * __this, RuntimeArray * ___data0, int32_t ___managedBufferStartIndex1, int32_t ___computeBufferStartIndex2, int32_t ___count3, int32_t ___elemSize4, const RuntimeMethod* method)
{
	typedef void (*ComputeBuffer_InternalSetData_m3847349839_ftn) (ComputeBuffer_t2358285523 *, RuntimeArray *, int32_t, int32_t, int32_t, int32_t);
	static ComputeBuffer_InternalSetData_m3847349839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeBuffer_InternalSetData_m3847349839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___data0, ___managedBufferStartIndex1, ___computeBufferStartIndex2, ___count3, ___elemSize4);
}
// System.Void UnityEngine.ComputeBuffer::CopyCount(UnityEngine.ComputeBuffer,UnityEngine.ComputeBuffer,System.Int32)
extern "C"  void ComputeBuffer_CopyCount_m1152926379 (RuntimeObject * __this /* static, unused */, ComputeBuffer_t2358285523 * ___src0, ComputeBuffer_t2358285523 * ___dst1, int32_t ___dstOffsetBytes2, const RuntimeMethod* method)
{
	typedef void (*ComputeBuffer_CopyCount_m1152926379_ftn) (ComputeBuffer_t2358285523 *, ComputeBuffer_t2358285523 *, int32_t);
	static ComputeBuffer_CopyCount_m1152926379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeBuffer_CopyCount_m1152926379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeBuffer::CopyCount(UnityEngine.ComputeBuffer,UnityEngine.ComputeBuffer,System.Int32)");
	_il2cpp_icall_func(___src0, ___dst1, ___dstOffsetBytes2);
}
// System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
extern "C"  int32_t ComputeShader_FindKernel_m142359282 (ComputeShader_t532478176 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*ComputeShader_FindKernel_m142359282_ftn) (ComputeShader_t532478176 *, String_t*);
	static ComputeShader_FindKernel_m142359282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeShader_FindKernel_m142359282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeShader::FindKernel(System.String)");
	int32_t retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// System.Void UnityEngine.ComputeShader::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void ComputeShader_SetVector_m1694484888 (ComputeShader_t532478176 * __this, String_t* ___name0, Vector4_t2436299922  ___val1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m1579628175(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Vector4_t2436299922  L_2 = ___val1;
		ComputeShader_SetVector_m91341944(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeShader::SetVector(System.Int32,UnityEngine.Vector4)
extern "C"  void ComputeShader_SetVector_m91341944 (ComputeShader_t532478176 * __this, int32_t ___nameID0, Vector4_t2436299922  ___val1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		ComputeShader_INTERNAL_CALL_SetVector_m3065059501(NULL /*static, unused*/, __this, L_0, (&___val1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeShader::INTERNAL_CALL_SetVector(UnityEngine.ComputeShader,System.Int32,UnityEngine.Vector4&)
extern "C"  void ComputeShader_INTERNAL_CALL_SetVector_m3065059501 (RuntimeObject * __this /* static, unused */, ComputeShader_t532478176 * ___self0, int32_t ___nameID1, Vector4_t2436299922 * ___val2, const RuntimeMethod* method)
{
	typedef void (*ComputeShader_INTERNAL_CALL_SetVector_m3065059501_ftn) (ComputeShader_t532478176 *, int32_t, Vector4_t2436299922 *);
	static ComputeShader_INTERNAL_CALL_SetVector_m3065059501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeShader_INTERNAL_CALL_SetVector_m3065059501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeShader::INTERNAL_CALL_SetVector(UnityEngine.ComputeShader,System.Int32,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___val2);
}
// System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.String,UnityEngine.Texture)
extern "C"  void ComputeShader_SetTexture_m2465178720 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, String_t* ___name1, Texture_t2119925672 * ___texture2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___kernelIndex0;
		String_t* L_1 = ___name1;
		int32_t L_2 = Shader_PropertyToID_m1579628175(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Texture_t2119925672 * L_3 = ___texture2;
		ComputeShader_SetTexture_m3962244874(__this, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.Int32,UnityEngine.Texture)
extern "C"  void ComputeShader_SetTexture_m3962244874 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, int32_t ___nameID1, Texture_t2119925672 * ___texture2, const RuntimeMethod* method)
{
	typedef void (*ComputeShader_SetTexture_m3962244874_ftn) (ComputeShader_t532478176 *, int32_t, int32_t, Texture_t2119925672 *);
	static ComputeShader_SetTexture_m3962244874_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeShader_SetTexture_m3962244874_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeShader::SetTexture(System.Int32,System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___kernelIndex0, ___nameID1, ___texture2);
}
// System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.String,UnityEngine.ComputeBuffer)
extern "C"  void ComputeShader_SetBuffer_m2421995637 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, String_t* ___name1, ComputeBuffer_t2358285523 * ___buffer2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___kernelIndex0;
		String_t* L_1 = ___name1;
		int32_t L_2 = Shader_PropertyToID_m1579628175(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ComputeBuffer_t2358285523 * L_3 = ___buffer2;
		ComputeShader_SetBuffer_m173270919(__this, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.Int32,UnityEngine.ComputeBuffer)
extern "C"  void ComputeShader_SetBuffer_m173270919 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, int32_t ___nameID1, ComputeBuffer_t2358285523 * ___buffer2, const RuntimeMethod* method)
{
	typedef void (*ComputeShader_SetBuffer_m173270919_ftn) (ComputeShader_t532478176 *, int32_t, int32_t, ComputeBuffer_t2358285523 *);
	static ComputeShader_SetBuffer_m173270919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeShader_SetBuffer_m173270919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeShader::SetBuffer(System.Int32,System.Int32,UnityEngine.ComputeBuffer)");
	_il2cpp_icall_func(__this, ___kernelIndex0, ___nameID1, ___buffer2);
}
// System.Void UnityEngine.ComputeShader::Dispatch(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void ComputeShader_Dispatch_m1908591482 (ComputeShader_t532478176 * __this, int32_t ___kernelIndex0, int32_t ___threadGroupsX1, int32_t ___threadGroupsY2, int32_t ___threadGroupsZ3, const RuntimeMethod* method)
{
	typedef void (*ComputeShader_Dispatch_m1908591482_ftn) (ComputeShader_t532478176 *, int32_t, int32_t, int32_t, int32_t);
	static ComputeShader_Dispatch_m1908591482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ComputeShader_Dispatch_m1908591482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ComputeShader::Dispatch(System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___kernelIndex0, ___threadGroupsX1, ___threadGroupsY2, ___threadGroupsZ3);
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
extern "C"  void ContextMenu__ctor_m3806697780 (ContextMenu_t2541329559 * __this, String_t* ___itemName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___itemName0;
		ContextMenu__ctor_m2944493690(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern "C"  void ContextMenu__ctor_m2944493690 (ContextMenu_t2541329559 * __this, String_t* ___itemName0, bool ___isValidateFunction1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___itemName0;
		bool L_1 = ___isValidateFunction1;
		ContextMenu__ctor_m3411406348(__this, L_0, L_1, ((int32_t)1000000), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern "C"  void ContextMenu__ctor_m3411406348 (ContextMenu_t2541329559 * __this, String_t* ___itemName0, bool ___isValidateFunction1, int32_t ___priority2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___itemName0;
		__this->set_menuItem_0(L_0);
		bool L_1 = ___isValidateFunction1;
		__this->set_validate_1(L_1);
		int32_t L_2 = ___priority2;
		__this->set_priority_2(L_2);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2294981130_marshal_pinvoke(const Coroutine_t2294981130& unmarshaled, Coroutine_t2294981130_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t2294981130_marshal_pinvoke_back(const Coroutine_t2294981130_marshaled_pinvoke& marshaled, Coroutine_t2294981130& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2294981130_marshal_pinvoke_cleanup(Coroutine_t2294981130_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2294981130_marshal_com(const Coroutine_t2294981130& unmarshaled, Coroutine_t2294981130_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t2294981130_marshal_com_back(const Coroutine_t2294981130_marshaled_com& marshaled, Coroutine_t2294981130& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2294981130_marshal_com_cleanup(Coroutine_t2294981130_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m352213490 (Coroutine_t2294981130 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m874787853(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m2401623703 (Coroutine_t2294981130 * __this, const RuntimeMethod* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m2401623703_ftn) (Coroutine_t2294981130 *);
	static Coroutine_ReleaseCoroutine_m2401623703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m2401623703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m1872117466 (Coroutine_t2294981130 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m2401623703(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
extern "C"  CSSSize_t2672652373  DelegatePInvokeWrapper_CSSMeasureFunc_t2094042647 (CSSMeasureFunc_t2094042647 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	typedef CSSSize_t2672652373  (STDCALL *PInvokeFunc)(intptr_t, float, int32_t, float, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	CSSSize_t2672652373  returnValue = il2cppPInvokeFunc(___node0, ___width1, ___widthMode2, ___height3, ___heightMode4);

	return returnValue;
}
// System.Void UnityEngine.CSSLayout.CSSMeasureFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void CSSMeasureFunc__ctor_m2125014652 (CSSMeasureFunc_t2094042647 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t2672652373  CSSMeasureFunc_Invoke_m2866500810 (CSSMeasureFunc_t2094042647 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CSSMeasureFunc_Invoke_m2866500810((CSSMeasureFunc_t2094042647 *)__this->get_prev_9(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef CSSSize_t2672652373  (*FunctionPointerType) (RuntimeObject *, void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef CSSSize_t2672652373  (*FunctionPointerType) (void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CSSLayout.CSSMeasureFunc::BeginInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CSSMeasureFunc_BeginInvoke_m2560653280 (CSSMeasureFunc_t2094042647 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, AsyncCallback_t3561663063 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSSMeasureFunc_BeginInvoke_m2560653280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___node0);
	__d_args[1] = Box(Single_t485236535_il2cpp_TypeInfo_var, &___width1);
	__d_args[2] = Box(CSSMeasureMode_t4281271123_il2cpp_TypeInfo_var, &___widthMode2);
	__d_args[3] = Box(Single_t485236535_il2cpp_TypeInfo_var, &___height3);
	__d_args[4] = Box(CSSMeasureMode_t4281271123_il2cpp_TypeInfo_var, &___heightMode4);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::EndInvoke(System.IAsyncResult)
extern "C"  CSSSize_t2672652373  CSSMeasureFunc_EndInvoke_m375928596 (CSSMeasureFunc_t2094042647 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(CSSSize_t2672652373 *)UnBox ((RuntimeObject*)__result);
}
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2094042647 * Native_CSSNodeGetMeasureFunc_m1704122301 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeGetMeasureFunc_m1704122301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WeakReference_t3660816289 * V_0 = NULL;
	CSSMeasureFunc_t2094042647 * V_1 = NULL;
	{
		V_0 = (WeakReference_t3660816289 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4280565868_il2cpp_TypeInfo_var);
		Dictionary_2_t3106649840 * L_0 = ((Native_t4280565868_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4280565868_il2cpp_TypeInfo_var))->get_s_MeasureFunctions_0();
		intptr_t L_1 = ___node0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1943609909(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1943609909_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t3660816289 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean System.WeakReference::get_IsAlive() */, L_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t3660816289 * L_5 = V_0;
		NullCheck(L_5);
		RuntimeObject * L_6 = VirtFuncInvoker0< RuntimeObject * >::Invoke(5 /* System.Object System.WeakReference::get_Target() */, L_5);
		V_1 = ((CSSMeasureFunc_t2094042647 *)IsInstSealed((RuntimeObject*)L_6, CSSMeasureFunc_t2094042647_il2cpp_TypeInfo_var));
		goto IL_003a;
	}

IL_0032:
	{
		V_1 = (CSSMeasureFunc_t2094042647 *)NULL;
		goto IL_003a;
	}

IL_003a:
	{
		CSSMeasureFunc_t2094042647 * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.CSSLayout.Native::CSSNodeMeasureInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.IntPtr)
extern "C"  void Native_CSSNodeMeasureInvoke_m55174108 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, intptr_t ___returnValueAddress5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeMeasureInvoke_m55174108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CSSMeasureFunc_t2094042647 * V_0 = NULL;
	{
		intptr_t L_0 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4280565868_il2cpp_TypeInfo_var);
		CSSMeasureFunc_t2094042647 * L_1 = Native_CSSNodeGetMeasureFunc_m1704122301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CSSMeasureFunc_t2094042647 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		intptr_t L_3 = ___returnValueAddress5;
		void* L_4 = IntPtr_op_Explicit_m803950198(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		CSSMeasureFunc_t2094042647 * L_5 = V_0;
		intptr_t L_6 = ___node0;
		float L_7 = ___width1;
		int32_t L_8 = ___widthMode2;
		float L_9 = ___height3;
		int32_t L_10 = ___heightMode4;
		NullCheck(L_5);
		CSSSize_t2672652373  L_11 = CSSMeasureFunc_Invoke_m2866500810(L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		*(CSSSize_t2672652373 *)L_4 = L_11;
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.CSSLayout.Native::.cctor()
extern "C"  void Native__cctor_m3749058547 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native__cctor_m3749058547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3106649840 * L_0 = (Dictionary_2_t3106649840 *)il2cpp_codegen_object_new(Dictionary_2_t3106649840_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m83878655(L_0, /*hidden argument*/Dictionary_2__ctor_m83878655_RuntimeMethod_var);
		((Native_t4280565868_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4280565868_il2cpp_TypeInfo_var))->set_s_MeasureFunctions_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t616951155_marshal_pinvoke(const CullingGroup_t616951155& unmarshaled, CullingGroup_t616951155_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t616951155_marshal_pinvoke_back(const CullingGroup_t616951155_marshaled_pinvoke& marshaled, CullingGroup_t616951155& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t616951155_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t3981248240>(marshaled.___m_OnStateChanged_1, StateChanged_t3981248240_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t616951155_marshal_pinvoke_cleanup(CullingGroup_t616951155_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t616951155_marshal_com(const CullingGroup_t616951155& unmarshaled, CullingGroup_t616951155_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t616951155_marshal_com_back(const CullingGroup_t616951155_marshaled_com& marshaled, CullingGroup_t616951155& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t616951155_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t3981248240>(marshaled.___m_OnStateChanged_1, StateChanged_t3981248240_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t616951155_marshal_com_cleanup(CullingGroup_t616951155_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern "C"  void CullingGroup_Finalize_m4059858500 (CullingGroup_t616951155 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m4059858500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			intptr_t L_0 = __this->get_m_Ptr_0();
			bool L_1 = IntPtr_op_Inequality_m2291805156(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m1588373077(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m3581328205 (CullingGroup_t616951155 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_Dispose_m3581328205_ftn) (CullingGroup_t616951155 *);
	static CullingGroup_Dispose_m3581328205_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m3581328205_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m4239256517 (RuntimeObject * __this /* static, unused */, CullingGroup_t616951155 * ___cullingGroup0, intptr_t ___eventsPtr1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_SendEvents_m4239256517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CullingGroupEvent_t655523856 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1408049937((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t655523856 *)L_0;
		CullingGroup_t616951155 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t3981248240 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t616951155 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t3981248240 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t655523856 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = il2cpp_codegen_sizeof(CullingGroupEvent_t655523856_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StateChanged_Invoke_m1290116875(L_4, (*(CullingGroupEvent_t655523856 *)((CullingGroupEvent_t655523856 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m1588373077 (CullingGroup_t616951155 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m1588373077_ftn) (CullingGroup_t616951155 *);
	static CullingGroup_FinalizerFailure_m1588373077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m1588373077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t3981248240 (StateChanged_t3981248240 * __this, CullingGroupEvent_t655523856  ___sphere0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t655523856 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m2638652860 (StateChanged_t3981248240 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m1290116875 (StateChanged_t3981248240 * __this, CullingGroupEvent_t655523856  ___sphere0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m1290116875((StateChanged_t3981248240 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, CullingGroupEvent_t655523856  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t655523856  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* StateChanged_BeginInvoke_m3881756820 (StateChanged_t3981248240 * __this, CullingGroupEvent_t655523856  ___sphere0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m3881756820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t655523856_il2cpp_TypeInfo_var, &___sphere0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m4007773420 (StateChanged_t3981248240 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Cursor::SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2,UnityEngine.CursorMode)
extern "C"  void Cursor_SetCursor_m562056876 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Vector2_t328513675  ___hotspot1, int32_t ___cursorMode2, const RuntimeMethod* method)
{
	{
		Texture2D_t3063074017 * L_0 = ___texture0;
		int32_t L_1 = ___cursorMode2;
		Cursor_INTERNAL_CALL_SetCursor_m3622843465(NULL /*static, unused*/, L_0, (&___hotspot1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Cursor::INTERNAL_CALL_SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)
extern "C"  void Cursor_INTERNAL_CALL_SetCursor_m3622843465 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Vector2_t328513675 * ___hotspot1, int32_t ___cursorMode2, const RuntimeMethod* method)
{
	typedef void (*Cursor_INTERNAL_CALL_SetCursor_m3622843465_ftn) (Texture2D_t3063074017 *, Vector2_t328513675 *, int32_t);
	static Cursor_INTERNAL_CALL_SetCursor_m3622843465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_INTERNAL_CALL_SetCursor_m3622843465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::INTERNAL_CALL_SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)");
	_il2cpp_icall_func(___texture0, ___hotspot1, ___cursorMode2);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m1036811635 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Cursor_get_lockState_m1036811635_ftn) ();
	static Cursor_get_lockState_m1036811635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m1036811635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m900305041 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  RuntimeObject * CustomYieldInstruction_get_Current_m1979938261 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		V_0 = NULL;
		goto IL_0008;
	}

IL_0008:
	{
		RuntimeObject * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m3999109302 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m883297459 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m1827429516 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_unityLogger_m1827429516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Debug_t3419099923_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t3419099923_il2cpp_TypeInfo_var))->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void Debug_DrawLine_m1640476408 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___end1, Color_t2582018970  ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m1640476408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___duration3;
		bool L_1 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m3943959890(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C"  void Debug_DrawLine_m2699851371 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___end1, Color_t2582018970  ___color2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m2699851371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		float L_0 = V_1;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m3943959890(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C"  void Debug_INTERNAL_CALL_DrawLine_m3943959890 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___start0, Vector3_t1986933152 * ___end1, Color_t2582018970 * ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m3943959890_ftn) (Vector3_t1986933152 *, Vector3_t1986933152 *, Color_t2582018970 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m3943959890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m3943959890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start0, ___end1, ___color2, ___duration3, ___depthTest4);
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C"  void Debug_DrawRay_m4146618718 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___dir1, Color_t2582018970  ___color2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m4146618718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		Vector3_t1986933152  L_0 = ___start0;
		Vector3_t1986933152  L_1 = ___dir1;
		Color_t2582018970  L_2 = ___color2;
		float L_3 = V_1;
		bool L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_DrawRay_m2541735374(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Debug_DrawRay_m1243172723 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___dir1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m1243172723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	Color_t2582018970  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		Color_t2582018970  L_0 = Color_get_white_m2500342551(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_0;
		Vector3_t1986933152  L_1 = ___start0;
		Vector3_t1986933152  L_2 = ___dir1;
		Color_t2582018970  L_3 = V_2;
		float L_4 = V_1;
		bool L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_DrawRay_m2541735374(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void Debug_DrawRay_m2541735374 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___start0, Vector3_t1986933152  ___dir1, Color_t2582018970  ___color2, float ___duration3, bool ___depthTest4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m2541735374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___start0;
		Vector3_t1986933152  L_1 = ___start0;
		Vector3_t1986933152  L_2 = ___dir1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_3 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Color_t2582018970  L_4 = ___color2;
		float L_5 = ___duration3;
		bool L_6 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_DrawLine_m1640476408(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m3513156148 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m3513156148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3935077967 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m3935077967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m2374009668 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t692178351 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m2374009668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t692178351 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t692178351 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogErrorFormat_m789741991 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___context0, String_t* ___format1, ObjectU5BU5D_t2737604620* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogErrorFormat_m789741991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t692178351 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t2737604620* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t692178351 *, String_t*, ObjectU5BU5D_t2737604620* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t293590501_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1994504921 (RuntimeObject * __this /* static, unused */, Exception_t4086964929 * ___exception0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m1994504921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t4086964929 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t4086964929 *, Object_t692178351 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t293590501_il2cpp_TypeInfo_var, L_0, L_1, (Object_t692178351 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Debug_LogException_m600284527 (RuntimeObject * __this /* static, unused */, Exception_t4086964929 * ___exception0, Object_t692178351 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m600284527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t4086964929 * L_1 = ___exception0;
		Object_t692178351 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t4086964929 *, Object_t692178351 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t293590501_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3275468740 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m3275468740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m1775422903 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t692178351 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m1775422903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t692178351 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t692178351 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m1662540287 (RuntimeObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m1662540287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___format0;
		ObjectU5BU5D_t2737604620* L_2 = ___args1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, String_t*, ObjectU5BU5D_t2737604620* >::Invoke(2 /* System.Void UnityEngine.ILogger::LogFormat(UnityEngine.LogType,System.String,System.Object[]) */, ILogger_t1339277430_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m1435679414 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___context0, String_t* ___format1, ObjectU5BU5D_t2737604620* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m1435679414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1827429516(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t692178351 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t2737604620* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t692178351 *, String_t*, ObjectU5BU5D_t2737604620* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t293590501_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2, L_3);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C"  bool Debug_get_isDebugBuild_m2721814523 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Debug_get_isDebugBuild_m2721814523_ftn) ();
	static Debug_get_isDebugBuild_m2721814523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m2721814523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Debug::.cctor()
extern "C"  void Debug__cctor_m1526533645 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m1526533645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t2880387906 * L_0 = (DebugLogHandler_t2880387906 *)il2cpp_codegen_object_new(DebugLogHandler_t2880387906_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m141086873(L_0, /*hidden argument*/NULL);
		Logger_t952924384 * L_1 = (Logger_t952924384 *)il2cpp_codegen_object_new(Logger_t952924384_il2cpp_TypeInfo_var);
		Logger__ctor_m844445376(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t3419099923_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t3419099923_il2cpp_TypeInfo_var))->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m141086873 (DebugLogHandler_t2880387906 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m424405779 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t692178351 * ___obj2, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m424405779_ftn) (int32_t, String_t*, Object_t692178351 *);
	static DebugLogHandler_Internal_Log_m424405779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m424405779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m3622619738 (RuntimeObject * __this /* static, unused */, Exception_t4086964929 * ___exception0, Object_t692178351 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m3622619738_ftn) (Exception_t4086964929 *, Object_t692178351 *);
	static DebugLogHandler_Internal_LogException_m3622619738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m3622619738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void DebugLogHandler_LogFormat_m366728963 (DebugLogHandler_t2880387906 * __this, int32_t ___logType0, Object_t692178351 * ___context1, String_t* ___format2, ObjectU5BU5D_t2737604620* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m366728963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t2737604620* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m29320051(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t692178351 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m424405779(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m1458406449 (DebugLogHandler_t2880387906 * __this, Exception_t4086964929 * ___exception0, Object_t692178351 * ___context1, const RuntimeMethod* method)
{
	{
		Exception_t4086964929 * L_0 = ___exception0;
		Object_t692178351 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m3622619738(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3197979243 (DefaultExecutionOrder_t334925532 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m4130170663 (DisallowMultipleComponent_t2744062746 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m190271719 (Display_t173707952 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		intptr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m2782994112((&L_0), 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m646575176 (Display_t173707952 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		intptr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern "C"  int32_t Display_get_renderingWidth_m730855742 (Display_t173707952 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingWidth_m730855742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m3140247240(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern "C"  int32_t Display_get_renderingHeight_m3547182575 (Display_t173707952 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingHeight_m3547182575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m3140247240(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern "C"  int32_t Display_get_systemWidth_m4127972072 (Display_t173707952 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemWidth_m4127972072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m2705345354(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern "C"  int32_t Display_get_systemHeight_m3689264282 (Display_t173707952 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemHeight_m3689264282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m2705345354(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Display_RelativeMouseAt_m2557534896 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___inputMouseCoordinates0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RelativeMouseAt_m2557534896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t1986933152  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = (&___inputMouseCoordinates0)->get_x_1();
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&___inputMouseCoordinates0)->get_y_2();
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m369378025(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->set_z_3((((float)((float)L_4))));
		int32_t L_5 = V_1;
		(&V_0)->set_x_1((((float)((float)L_5))));
		int32_t L_6 = V_2;
		(&V_0)->set_y_2((((float)((float)L_6))));
		Vector3_t1986933152  L_7 = V_0;
		V_5 = L_7;
		goto IL_0046;
	}

IL_0046:
	{
		Vector3_t1986933152  L_8 = V_5;
		return L_8;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern "C"  void Display_RecreateDisplayList_m165196363 (RuntimeObject * __this /* static, unused */, IntPtrU5BU5D_t1552124580* ___nativeDisplay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m165196363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t1552124580* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->set_displays_1(((DisplayU5BU5D_t2888752273*)SZArrayNew(DisplayU5BU5D_t2888752273_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t2888752273* L_1 = ((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t1552124580* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		intptr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t173707952 * L_7 = (Display_t173707952 *)il2cpp_codegen_object_new(Display_t173707952_il2cpp_TypeInfo_var);
		Display__ctor_m646575176(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t173707952 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t1552124580* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t2888752273* L_11 = ((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t173707952 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern "C"  void Display_FireDisplaysUpdated_m1779014840 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m1779014840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t962499819 * L_0 = ((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t173707952_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t962499819 * L_1 = ((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m652453720(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m2705345354 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetSystemExtImpl_m2705345354_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m2705345354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m2705345354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m3140247240 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetRenderingExtImpl_m3140247240_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m3140247240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m3140247240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m369378025 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m369378025_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m369378025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m369378025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	int32_t retVal = _il2cpp_icall_func(___x0, ___y1, ___rx2, ___ry3);
	return retVal;
}
// System.Void UnityEngine.Display::.cctor()
extern "C"  void Display__cctor_m4007441132 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m4007441132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t2888752273* L_0 = ((DisplayU5BU5D_t2888752273*)SZArrayNew(DisplayU5BU5D_t2888752273_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t173707952 * L_1 = (Display_t173707952 *)il2cpp_codegen_object_new(Display_t173707952_il2cpp_TypeInfo_var);
		Display__ctor_m190271719(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t173707952 *)L_1);
		((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->set_displays_1(L_0);
		DisplayU5BU5D_t2888752273* L_2 = ((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t173707952 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_4);
		((Display_t173707952_StaticFields*)il2cpp_codegen_static_fields_for(Display_t173707952_il2cpp_TypeInfo_var))->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t962499819 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t962499819 (DisplaysUpdatedDelegate_t962499819 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m1189411790 (DisplaysUpdatedDelegate_t962499819 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m652453720 (DisplaysUpdatedDelegate_t962499819 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m652453720((DisplaysUpdatedDelegate_t962499819 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* DisplaysUpdatedDelegate_BeginInvoke_m2251948067 (DisplaysUpdatedDelegate_t962499819 * __this, AsyncCallback_t3561663063 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3399071873 (DisplaysUpdatedDelegate_t962499819 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m4244486561 (DrivenRectTransformTracker_t2554077623 * __this, Object_t692178351 * ___driver0, RectTransform_t859616204 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m4244486561_AdjustorThunk (RuntimeObject * __this, Object_t692178351 * ___driver0, RectTransform_t859616204 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t2554077623 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t2554077623 *>(__this + 1);
	DrivenRectTransformTracker_Add_m4244486561(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m2683757757 (DrivenRectTransformTracker_t2554077623 * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m2683757757_AdjustorThunk (RuntimeObject * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t2554077623 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t2554077623 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m2683757757(_thisAdjusted, ___revertValues0, method);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m4160331614 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t692178351 * ArgumentCache_get_unityObjectArgument_m1826210923 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	Object_t692178351 * V_0 = NULL;
	{
		Object_t692178351 * L_0 = __this->get_m_ObjectArgument_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t692178351 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2164087651 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m2917341558 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatArgument_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m3199145890 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m4208597231 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1355543587 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m1355543587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2163207444(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00e4;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m3326695972(L_2, _stringLiteral80671380, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m2084528576(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m3326695972(L_8, _stringLiteral1823110067, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m2084528576(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m3326695972(L_14, _stringLiteral2695145057, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m2084528576(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1881321258(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
	}

IL_009a:
	{
		String_t* L_24 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_24);
		int32_t L_25 = String_IndexOf_m3326695972(L_24, _stringLiteral809285935, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) == ((int32_t)(-1))))
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_27 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_27);
		bool L_28 = String_EndsWith_m3111453544(L_27, _stringLiteral3069375572, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_29 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = String_Substring_m1881321258(L_29, 0, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2395435812(NULL /*static, unused*/, L_31, _stringLiteral3912127903, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_32);
	}

IL_00e4:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m4074156020 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1355543587(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m2640961328 (ArgumentCache_t288530198 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1355543587(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m2710559406 (BaseInvokableCall_t27726116 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2058189347 (BaseInvokableCall_t27726116 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m2058189347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, _stringLiteral2365739746, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_3 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_3, _stringLiteral1060590976, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m880256453 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * ___delegate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m880256453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Object_t692178351 * V_2 = NULL;
	{
		Delegate_t3882343965 * L_0 = ___delegate0;
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m3738249125(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0015:
	{
		RuntimeObject * L_3 = V_0;
		V_2 = ((Object_t692178351 *)IsInstClass((RuntimeObject*)L_3, Object_t692178351_il2cpp_TypeInfo_var));
		Object_t692178351 * L_4 = V_2;
		bool L_5 = Object_ReferenceEquals_m4090170938(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Object_t692178351 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_6, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m4040846306 (InvokableCall_t2697346187 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m4040846306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m2058189347(__this, L_0, L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(UnityAction_t992331987_0_0_0_var), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t3882343965 * L_5 = NetFxCoreExtensions_CreateDelegate_m1322929972(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		InvokableCall_add_Delegate_m275363113(__this, ((UnityAction_t992331987 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t992331987_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m3617193550 (InvokableCall_t2697346187 * __this, UnityAction_t992331987 * ___action0, const RuntimeMethod* method)
{
	{
		BaseInvokableCall__ctor_m2710559406(__this, /*hidden argument*/NULL);
		UnityAction_t992331987 * L_0 = ___action0;
		InvokableCall_add_Delegate_m275363113(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m275363113 (InvokableCall_t2697346187 * __this, UnityAction_t992331987 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_add_Delegate_m275363113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t992331987 * V_0 = NULL;
	UnityAction_t992331987 * V_1 = NULL;
	{
		UnityAction_t992331987 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t992331987 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t992331987 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t992331987 * L_3 = V_1;
		UnityAction_t992331987 * L_4 = ___value0;
		Delegate_t3882343965 * L_5 = Delegate_Combine_m912662445(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t992331987 * L_6 = V_0;
		UnityAction_t992331987 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t992331987 *>(L_2, ((UnityAction_t992331987 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t992331987_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t992331987 * L_8 = V_0;
		UnityAction_t992331987 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t992331987 *)L_8) == ((RuntimeObject*)(UnityAction_t992331987 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_remove_Delegate_m476237524 (InvokableCall_t2697346187 * __this, UnityAction_t992331987 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_remove_Delegate_m476237524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t992331987 * V_0 = NULL;
	UnityAction_t992331987 * V_1 = NULL;
	{
		UnityAction_t992331987 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t992331987 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t992331987 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t992331987 * L_3 = V_1;
		UnityAction_t992331987 * L_4 = ___value0;
		Delegate_t3882343965 * L_5 = Delegate_Remove_m3357168269(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t992331987 * L_6 = V_0;
		UnityAction_t992331987 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t992331987 *>(L_2, ((UnityAction_t992331987 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t992331987_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t992331987 * L_8 = V_0;
		UnityAction_t992331987 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t992331987 *)L_8) == ((RuntimeObject*)(UnityAction_t992331987 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m2275965302 (InvokableCall_t2697346187 * __this, ObjectU5BU5D_t2737604620* ___args0, const RuntimeMethod* method)
{
	{
		UnityAction_t992331987 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m880256453(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t992331987 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m2407906560(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m1207259153 (InvokableCall_t2697346187 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_t992331987 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m3738249125(L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_t992331987 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3023103954(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m819252217 (InvokableCallList_t1048652390 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m819252217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3442320419 * L_0 = (List_1_t3442320419 *)il2cpp_codegen_object_new(List_1_t3442320419_il2cpp_TypeInfo_var);
		List_1__ctor_m3740896317(L_0, /*hidden argument*/List_1__ctor_m3740896317_RuntimeMethod_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t3442320419 * L_1 = (List_1_t3442320419 *)il2cpp_codegen_object_new(List_1_t3442320419_il2cpp_TypeInfo_var);
		List_1__ctor_m3740896317(L_1, /*hidden argument*/List_1__ctor_m3740896317_RuntimeMethod_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t3442320419 * L_2 = (List_1_t3442320419 *)il2cpp_codegen_object_new(List_1_t3442320419_il2cpp_TypeInfo_var);
		List_1__ctor_m3740896317(L_2, /*hidden argument*/List_1__ctor_m3740896317_RuntimeMethod_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m3955084824 (InvokableCallList_t1048652390 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m3955084824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3442320419 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t27726116 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m604383575(L_0, L_1, /*hidden argument*/List_1_Add_m604383575_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m1698095211 (InvokableCallList_t1048652390 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m1698095211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3442320419 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t27726116 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m604383575(L_0, L_1, /*hidden argument*/List_1_Add_m604383575_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m1879627315 (InvokableCallList_t1048652390 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m1879627315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3442320419 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t3442320419 * L_0 = (List_1_t3442320419 *)il2cpp_codegen_object_new(List_1_t3442320419_il2cpp_TypeInfo_var);
		List_1__ctor_m3740896317(L_0, /*hidden argument*/List_1__ctor_m3740896317_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003e;
	}

IL_000e:
	{
		List_1_t3442320419 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t27726116 * L_3 = List_1_get_Item_m1584988565(L_1, L_2, /*hidden argument*/List_1_get_Item_m1584988565_RuntimeMethod_var);
		RuntimeObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, RuntimeObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		List_1_t3442320419 * L_7 = V_0;
		List_1_t3442320419 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t27726116 * L_10 = List_1_get_Item_m1584988565(L_8, L_9, /*hidden argument*/List_1_get_Item_m1584988565_RuntimeMethod_var);
		NullCheck(L_7);
		List_1_Add_m604383575(L_7, L_10, /*hidden argument*/List_1_Add_m604383575_RuntimeMethod_var);
	}

IL_0039:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		List_1_t3442320419 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m79931508(L_13, /*hidden argument*/List_1_get_Count_m79931508_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t3442320419 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t3442320419 * L_16 = V_0;
		intptr_t L_17 = (intptr_t)List_1_Contains_m1617167722_RuntimeMethod_var;
		Predicate_1_t1730346354 * L_18 = (Predicate_1_t1730346354 *)il2cpp_codegen_object_new(Predicate_1_t1730346354_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m422560267(L_18, L_16, L_17, /*hidden argument*/Predicate_1__ctor_m422560267_RuntimeMethod_var);
		NullCheck(L_15);
		List_1_RemoveAll_m127110729(L_15, L_18, /*hidden argument*/List_1_RemoveAll_m127110729_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m4038663428 (InvokableCallList_t1048652390 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m4038663428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3442320419 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m1306609920(L_0, /*hidden argument*/List_1_Clear_m1306609920_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern "C"  void InvokableCallList_Invoke_m371882796 (InvokableCallList_t1048652390 * __this, ObjectU5BU5D_t2737604620* ___parameters0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_Invoke_m371882796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t3442320419 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m1306609920(L_1, /*hidden argument*/List_1_Clear_m1306609920_RuntimeMethod_var);
		List_1_t3442320419 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t3442320419 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m3743613219(L_2, L_3, /*hidden argument*/List_1_AddRange_m3743613219_RuntimeMethod_var);
		List_1_t3442320419 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t3442320419 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m3743613219(L_4, L_5, /*hidden argument*/List_1_AddRange_m3743613219_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_0042:
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0049:
	{
		List_1_t3442320419 * L_6 = __this->get_m_ExecutingCalls_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BaseInvokableCall_t27726116 * L_8 = List_1_get_Item_m1584988565(L_6, L_7, /*hidden argument*/List_1_get_Item_m1584988565_RuntimeMethod_var);
		ObjectU5BU5D_t2737604620* L_9 = ___parameters0;
		NullCheck(L_8);
		VirtActionInvoker1< ObjectU5BU5D_t2737604620* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_11 = V_0;
		List_1_t3442320419 * L_12 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m79931508(L_12, /*hidden argument*/List_1_get_Count_m79931508_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0049;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern "C"  void PersistentCall__ctor_m3531969768 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m3531969768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_2(0);
		ArgumentCache_t288530198 * L_0 = (ArgumentCache_t288530198 *)il2cpp_codegen_object_new(ArgumentCache_t288530198_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m4160331614(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t692178351 * PersistentCall_get_target_m520259316 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	Object_t692178351 * V_0 = NULL;
	{
		Object_t692178351 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t692178351 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m1071443131 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m26565073 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Mode_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t288530198 * PersistentCall_get_arguments_m3899402300 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	ArgumentCache_t288530198 * V_0 = NULL;
	{
		ArgumentCache_t288530198 * L_0 = __this->get_m_Arguments_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ArgumentCache_t288530198 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m1012652439 (PersistentCall_t709177109 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m1012652439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Object_t692178351 * L_0 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m1071443131(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2163207444(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t27726116 * PersistentCall_GetRuntimeCall_m28487261 (PersistentCall_t709177109 * __this, UnityEventBase_t124089244 * ___theEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m28487261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t27726116 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		UnityEventBase_t124089244 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0012:
	{
		V_0 = (BaseInvokableCall_t27726116 *)NULL;
		goto IL_0114;
	}

IL_0019:
	{
		UnityEventBase_t124089244 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m193475207(L_2, __this, /*hidden argument*/NULL);
		V_1 = L_3;
		MethodInfo_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (BaseInvokableCall_t27726116 *)NULL;
		goto IL_0114;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_2 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_005c;
			}
			case 1:
			{
				goto IL_00fb;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_00a4;
			}
			case 4:
			{
				goto IL_0087;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00de;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_005c:
	{
		UnityEventBase_t124089244 * L_7 = ___theEvent0;
		Object_t692178351 * L_8 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_1;
		NullCheck(L_7);
		BaseInvokableCall_t27726116 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t27726116 *, RuntimeObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		V_0 = L_10;
		goto IL_0114;
	}

IL_006f:
	{
		Object_t692178351 * L_11 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_1;
		ArgumentCache_t288530198 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t27726116 * L_14 = PersistentCall_GetObjectCall_m3632195579(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0114;
	}

IL_0087:
	{
		Object_t692178351 * L_15 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_1;
		ArgumentCache_t288530198 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m2917341558(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3587825671 * L_19 = (CachedInvokableCall_1_t3587825671 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3587825671_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2264889547(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m2264889547_RuntimeMethod_var);
		V_0 = L_19;
		goto IL_0114;
	}

IL_00a4:
	{
		Object_t692178351 * L_20 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_1;
		ArgumentCache_t288530198 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m2164087651(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t4075156644 * L_24 = (CachedInvokableCall_1_t4075156644 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t4075156644_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m4025676652(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m4025676652_RuntimeMethod_var);
		V_0 = L_24;
		goto IL_0114;
	}

IL_00c1:
	{
		Object_t692178351 * L_25 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_1;
		ArgumentCache_t288530198 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m3199145890(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3757174574 * L_29 = (CachedInvokableCall_1_t3757174574 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3757174574_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2075360316(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m2075360316_RuntimeMethod_var);
		V_0 = L_29;
		goto IL_0114;
	}

IL_00de:
	{
		Object_t692178351 * L_30 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_1;
		ArgumentCache_t288530198 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m4208597231(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t2432241475 * L_34 = (CachedInvokableCall_1_t2432241475 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t2432241475_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2035028495(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m2035028495_RuntimeMethod_var);
		V_0 = L_34;
		goto IL_0114;
	}

IL_00fb:
	{
		Object_t692178351 * L_35 = PersistentCall_get_target_m520259316(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_1;
		InvokableCall_t2697346187 * L_37 = (InvokableCall_t2697346187 *)il2cpp_codegen_object_new(InvokableCall_t2697346187_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m4040846306(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0114;
	}

IL_010d:
	{
		V_0 = (BaseInvokableCall_t27726116 *)NULL;
		goto IL_0114;
	}

IL_0114:
	{
		BaseInvokableCall_t27726116 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t27726116 * PersistentCall_GetObjectCall_m3632195579 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t288530198 * ___arguments2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m3632195579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t3123539581 * V_3 = NULL;
	Object_t692178351 * V_4 = NULL;
	BaseInvokableCall_t27726116 * V_5 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t288530198 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2163207444(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentCache_t288530198 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3217370930, L_5, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0039:
	{
		V_0 = G_B3_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t3314080835_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1985992169* L_11 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1985992169* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1985992169* L_15 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1985992169* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t1985992169* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t3123539581 * L_21 = Type_GetConstructor_m816984919(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t288530198 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t692178351 * L_23 = ArgumentCache_get_unityObjectArgument_m1826210923(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t692178351 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_24, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t692178351 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m2972540513(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00ab;
		}
	}
	{
		V_4 = (Object_t692178351 *)NULL;
	}

IL_00ab:
	{
		ConstructorInfo_t3123539581 * L_30 = V_3;
		ObjectU5BU5D_t2737604620* L_31 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t692178351 * L_32 = ___target0;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_32);
		ObjectU5BU5D_t2737604620* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_34);
		ObjectU5BU5D_t2737604620* L_35 = L_33;
		Object_t692178351 * L_36 = V_4;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_36);
		NullCheck(L_30);
		RuntimeObject * L_37 = ConstructorInfo_Invoke_m1235543152(L_30, L_35, /*hidden argument*/NULL);
		V_5 = ((BaseInvokableCall_t27726116 *)IsInstClass((RuntimeObject*)L_37, BaseInvokableCall_t27726116_il2cpp_TypeInfo_var));
		goto IL_00d0;
	}

IL_00d0:
	{
		BaseInvokableCall_t27726116 * L_38 = V_5;
		return L_38;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m741824572 (PersistentCallGroup_t4133256657 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m741824572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		List_1_t4123771412 * L_0 = (List_1_t4123771412 *)il2cpp_codegen_object_new(List_1_t4123771412_il2cpp_TypeInfo_var);
		List_1__ctor_m1780907418(L_0, /*hidden argument*/List_1__ctor_m1780907418_RuntimeMethod_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m718681909 (PersistentCallGroup_t4133256657 * __this, InvokableCallList_t1048652390 * ___invokableList0, UnityEventBase_t124089244 * ___unityEventBase1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m718681909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentCall_t709177109 * V_0 = NULL;
	Enumerator_t3899639936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t27726116 * V_2 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t4123771412 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t3899639936  L_1 = List_1_GetEnumerator_m3404249592(L_0, /*hidden argument*/List_1_GetEnumerator_m3404249592_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			PersistentCall_t709177109 * L_2 = Enumerator_get_Current_m3572990374((&V_1), /*hidden argument*/Enumerator_get_Current_m3572990374_RuntimeMethod_var);
			V_0 = L_2;
			PersistentCall_t709177109 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m1012652439(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			goto IL_0042;
		}

IL_002c:
		{
			PersistentCall_t709177109 * L_5 = V_0;
			UnityEventBase_t124089244 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t27726116 * L_7 = PersistentCall_GetRuntimeCall_m28487261(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t27726116 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			InvokableCallList_t1048652390 * L_9 = ___invokableList0;
			BaseInvokableCall_t27726116 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m3955084824(L_9, L_10, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m1705891082((&V_1), /*hidden argument*/Enumerator_MoveNext_m1705891082_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m737346653((&V_1), /*hidden argument*/Enumerator_Dispose_m737346653_RuntimeMethod_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t992331987 (UnityAction_t992331987 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m771528289 (UnityAction_t992331987 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m2407906560 (UnityAction_t992331987 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m2407906560((UnityAction_t992331987 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_BeginInvoke_m1460643791 (UnityAction_t992331987 * __this, AsyncCallback_t3561663063 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m3721673864 (UnityAction_t992331987 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m661443625 (UnityEvent_t2603596161 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent__ctor_m661443625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)0)));
		UnityEventBase__ctor_m4162961704(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m756843201 (UnityEvent_t2603596161 * __this, UnityAction_t992331987 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_t992331987 * L_0 = ___call0;
		BaseInvokableCall_t27726116 * L_1 = UnityEvent_GetDelegate_m2995672099(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m504892285(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_RemoveListener_m1474079325 (UnityEvent_t2603596161 * __this, UnityAction_t992331987 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_t992331987 * L_0 = ___call0;
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m3738249125(L_0, /*hidden argument*/NULL);
		UnityAction_t992331987 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3023103954(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		UnityEventBase_RemoveListener_m638354830(__this, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m1361289740 (UnityEvent_t2603596161 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m1361289740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		MethodInfo_t * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t27726116 * UnityEvent_GetDelegate_m1183579176 (UnityEvent_t2603596161 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m1183579176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t27726116 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t2697346187 * L_2 = (InvokableCall_t2697346187 *)il2cpp_codegen_object_new(InvokableCall_t2697346187_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m4040846306(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t27726116 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t27726116 * UnityEvent_GetDelegate_m2995672099 (RuntimeObject * __this /* static, unused */, UnityAction_t992331987 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m2995672099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t27726116 * V_0 = NULL;
	{
		UnityAction_t992331987 * L_0 = ___action0;
		InvokableCall_t2697346187 * L_1 = (InvokableCall_t2697346187 *)il2cpp_codegen_object_new(InvokableCall_t2697346187_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m3617193550(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t27726116 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m2571842351 (UnityEvent_t2603596161 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t2737604620* L_0 = __this->get_m_InvokeArray_4();
		UnityEventBase_Invoke_m3701760002(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m4162961704 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m4162961704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		InvokableCallList_t1048652390 * L_0 = (InvokableCallList_t1048652390 *)il2cpp_codegen_object_new(InvokableCallList_t1048652390_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m819252217(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t4133256657 * L_1 = (PersistentCallGroup_t4133256657 *)il2cpp_codegen_object_new(PersistentCallGroup_t4133256657_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m741824572(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m2972540513(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3671357335 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m855811580 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m223105007(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m2972540513(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m193475207 (UnityEventBase_t124089244 * __this, PersistentCall_t709177109 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m193475207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t709177109 * L_1 = ___call0;
		NullCheck(L_1);
		ArgumentCache_t288530198 * L_2 = PersistentCall_get_arguments_m3899402300(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2163207444(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		PersistentCall_t709177109 * L_5 = ___call0;
		NullCheck(L_5);
		ArgumentCache_t288530198 * L_6 = PersistentCall_get_arguments_m3899402300(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2025831920(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3217370930, L_7, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B3_0;
	}

IL_0044:
	{
		PersistentCall_t709177109 * L_11 = ___call0;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m1071443131(L_11, /*hidden argument*/NULL);
		PersistentCall_t709177109 * L_13 = ___call0;
		NullCheck(L_13);
		Object_t692178351 * L_14 = PersistentCall_get_target_m520259316(L_13, /*hidden argument*/NULL);
		PersistentCall_t709177109 * L_15 = ___call0;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m26565073(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m1074674127(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		MethodInfo_t * L_19 = V_1;
		return L_19;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1074674127 (UnityEventBase_t124089244 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m1074674127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t1985992169* G_B10_2 = NULL;
	TypeU5BU5D_t1985992169* G_B10_3 = NULL;
	String_t* G_B10_4 = NULL;
	RuntimeObject * G_B10_5 = NULL;
	Type_t * G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t1985992169* G_B9_2 = NULL;
	TypeU5BU5D_t1985992169* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	RuntimeObject * G_B9_5 = NULL;
	{
		int32_t L_0 = ___mode2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_00c9;
			}
			case 3:
			{
				goto IL_0069;
			}
			case 4:
			{
				goto IL_0049;
			}
			case 5:
			{
				goto IL_00a9;
			}
			case 6:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_00f2;
	}

IL_0028:
	{
		String_t* L_1 = ___name0;
		RuntimeObject * L_2 = ___listener1;
		MethodInfo_t * L_3 = VirtFuncInvoker2< MethodInfo_t *, String_t*, RuntimeObject * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_1, L_2);
		V_0 = L_3;
		goto IL_00f9;
	}

IL_0036:
	{
		RuntimeObject * L_4 = ___listener1;
		String_t* L_5 = ___name0;
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_4, L_5, ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_00f9;
	}

IL_0049:
	{
		RuntimeObject * L_7 = ___listener1;
		String_t* L_8 = ___name0;
		TypeU5BU5D_t1985992169* L_9 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Single_t485236535_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		MethodInfo_t * L_11 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_00f9;
	}

IL_0069:
	{
		RuntimeObject * L_12 = ___listener1;
		String_t* L_13 = ___name0;
		TypeU5BU5D_t1985992169* L_14 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Int32_t972567508_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_15);
		MethodInfo_t * L_16 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00f9;
	}

IL_0089:
	{
		RuntimeObject * L_17 = ___listener1;
		String_t* L_18 = ___name0;
		TypeU5BU5D_t1985992169* L_19 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Boolean_t3624619635_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_20);
		MethodInfo_t * L_21 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00f9;
	}

IL_00a9:
	{
		RuntimeObject * L_22 = ___listener1;
		String_t* L_23 = ___name0;
		TypeU5BU5D_t1985992169* L_24 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_25);
		MethodInfo_t * L_26 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00f9;
	}

IL_00c9:
	{
		RuntimeObject * L_27 = ___listener1;
		String_t* L_28 = ___name0;
		TypeU5BU5D_t1985992169* L_29 = ((TypeU5BU5D_t1985992169*)SZArrayNew(TypeU5BU5D_t1985992169_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_30 = ___argumentType3;
		Type_t * L_31 = L_30;
		G_B9_0 = L_31;
		G_B9_1 = 0;
		G_B9_2 = L_29;
		G_B9_3 = L_29;
		G_B9_4 = L_28;
		G_B9_5 = L_27;
		if (L_31)
		{
			G_B10_0 = L_31;
			G_B10_1 = 0;
			G_B10_2 = L_29;
			G_B10_3 = L_29;
			G_B10_4 = L_28;
			G_B10_5 = L_27;
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(Object_t692178351_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00e6:
	{
		NullCheck(G_B10_2);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (Type_t *)G_B10_0);
		MethodInfo_t * L_33 = UnityEventBase_GetValidMethodInfo_m435691116(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00f9;
	}

IL_00f2:
	{
		V_0 = (MethodInfo_t *)NULL;
		goto IL_00f9;
	}

IL_00f9:
	{
		MethodInfo_t * L_34 = V_0;
		return L_34;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m223105007 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1048652390 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m4038663428(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m3827262675 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_CallsDirty_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		PersistentCallGroup_t4133256657 * L_1 = __this->get_m_PersistentCalls_1();
		InvokableCallList_t1048652390 * L_2 = __this->get_m_Calls_0();
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m718681909(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)0);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m504892285 (UnityEventBase_t124089244 * __this, BaseInvokableCall_t27726116 * ___call0, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1048652390 * L_0 = __this->get_m_Calls_0();
		BaseInvokableCall_t27726116 * L_1 = ___call0;
		NullCheck(L_0);
		InvokableCallList_AddListener_m1698095211(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m638354830 (UnityEventBase_t124089244 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1048652390 * L_0 = __this->get_m_Calls_0();
		RuntimeObject * L_1 = ___targetObj0;
		MethodInfo_t * L_2 = ___method1;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m1879627315(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3701760002 (UnityEventBase_t124089244 * __this, ObjectU5BU5D_t2737604620* ___parameters0, const RuntimeMethod* method)
{
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m3827262675(__this, /*hidden argument*/NULL);
		InvokableCallList_t1048652390 * L_0 = __this->get_m_Calls_0();
		ObjectU5BU5D_t2737604620* L_1 = ___parameters0;
		NullCheck(L_0);
		InvokableCallList_Invoke_m371882796(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern "C"  String_t* UnityEventBase_ToString_m704360343 (UnityEventBase_t124089244 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m704360343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m2733393030(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m2972540513(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1542845994(NULL /*static, unused*/, L_0, _stringLiteral434838489, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m435691116 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1985992169* ___argumentTypes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_GetValidMethodInfo_m435691116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	ParameterInfoU5BU5D_t3316460985* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t3943516840 * V_5 = NULL;
	ParameterInfoU5BU5D_t3316460985* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	MethodInfo_t * V_10 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2972540513(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_009c;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName1;
		TypeU5BU5D_t1985992169* L_4 = ___argumentTypes2;
		NullCheck(L_2);
		MethodInfo_t * L_5 = Type_GetMethod_m1328023905(L_2, L_3, ((int32_t)52), (Binder_t1695596754 *)NULL, L_4, (ParameterModifierU5BU5D_t2531273172*)(ParameterModifierU5BU5D_t2531273172*)NULL, /*hidden argument*/NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t3316460985* L_8 = VirtFuncInvoker0< ParameterInfoU5BU5D_t3316460985* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = (bool)1;
		V_4 = 0;
		ParameterInfoU5BU5D_t3316460985* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_007a;
	}

IL_003a:
	{
		ParameterInfoU5BU5D_t3316460985* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t3943516840 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		TypeU5BU5D_t1985992169* L_14 = ___argumentTypes2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_8 = L_17;
		ParameterInfo_t3943516840 * L_18 = V_5;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		V_9 = L_19;
		Type_t * L_20 = V_8;
		NullCheck(L_20);
		bool L_21 = Type_get_IsPrimitive_m2293585663(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = V_9;
		NullCheck(L_22);
		bool L_23 = Type_get_IsPrimitive_m2293585663(L_22, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_21) == ((int32_t)L_23))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0085;
	}

IL_006d:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_7;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_7;
		ParameterInfoU5BU5D_t3316460985* L_28 = V_6;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0085:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_0093;
		}
	}
	{
		MethodInfo_t * L_30 = V_1;
		V_10 = L_30;
		goto IL_00ba;
	}

IL_0093:
	{
	}

IL_0094:
	{
		Type_t * L_31 = V_0;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_31);
		V_0 = L_32;
	}

IL_009c:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m125071402(NULL /*static, unused*/, LoadTypeToken(RuntimeObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_33) == ((RuntimeObject*)(Type_t *)L_34)))
		{
			goto IL_00b2;
		}
	}
	{
		Type_t * L_35 = V_0;
		if (L_35)
		{
			goto IL_000d;
		}
	}

IL_00b2:
	{
		V_10 = (MethodInfo_t *)NULL;
		goto IL_00ba;
	}

IL_00ba:
	{
		MethodInfo_t * L_36 = V_10;
		return L_36;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C"  void ExecuteInEditMode__ctor_m704648856 (ExecuteInEditMode_t869940717 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m3160514901 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_get_currentPipeline_m3160514901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_U3CcurrentPipelineU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m2451449986 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_set_currentPipeline_m2451449986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___value0;
		((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->set_U3CcurrentPipelineU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m1052386618 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_CleanupRenderPipeline_m1052386618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject* L_1 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipelineAsset::DestroyCreatedInstances() */, IRenderPipelineAsset_t3600099924_il2cpp_TypeInfo_var, L_1);
	}

IL_0015:
	{
		((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0((RuntimeObject*)NULL);
		RenderPipelineManager_set_currentPipeline_m2451449986(NULL /*static, unused*/, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Experimental.Rendering.IRenderPipelineAsset,UnityEngine.Camera[],System.IntPtr)
extern "C"  void RenderPipelineManager_DoRenderLoop_Internal_m2213944881 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, CameraU5BU5D_t4045078555* ___cameras1, intptr_t ___loopPtr2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_DoRenderLoop_Internal_m2213944881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScriptableRenderContext_t2070520004  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = ___pipe0;
		RenderPipelineManager_PrepareRenderPipeline_m2025082530(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RuntimeObject* L_1 = RenderPipelineManager_get_currentPipeline_m3160514901(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_002a;
	}

IL_0016:
	{
		intptr_t L_2 = ___loopPtr2;
		ScriptableRenderContext__ctor_m3779715225((&V_0), L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = RenderPipelineManager_get_currentPipeline_m3160514901(NULL /*static, unused*/, /*hidden argument*/NULL);
		ScriptableRenderContext_t2070520004  L_4 = V_0;
		CameraU5BU5D_t4045078555* L_5 = ___cameras1;
		NullCheck(L_3);
		InterfaceActionInvoker2< ScriptableRenderContext_t2070520004 , CameraU5BU5D_t4045078555* >::Invoke(1 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipeline::Render(UnityEngine.Experimental.Rendering.ScriptableRenderContext,UnityEngine.Camera[]) */, IRenderPipeline_t3426467477_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m2025082530 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_PrepareRenderPipeline_m2025082530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		RuntimeObject* L_1 = ___pipe0;
		if ((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject*)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		RuntimeObject* L_2 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RenderPipelineManager_CleanupRenderPipeline_m1052386618(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		RuntimeObject* L_3 = ___pipe0;
		((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0(L_3);
	}

IL_0025:
	{
		RuntimeObject* L_4 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_5 = RenderPipelineManager_get_currentPipeline_m3160514901(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		RuntimeObject* L_6 = RenderPipelineManager_get_currentPipeline_m3160514901(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.Experimental.Rendering.IRenderPipeline::get_disposed() */, IRenderPipeline_t3426467477_il2cpp_TypeInfo_var, L_6);
		if (!L_7)
		{
			goto IL_0057;
		}
	}

IL_0048:
	{
		RuntimeObject* L_8 = ((RenderPipelineManager_t470120599_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t470120599_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.IRenderPipelineAsset::CreatePipeline() */, IRenderPipelineAsset_t3600099924_il2cpp_TypeInfo_var, L_8);
		RenderPipelineManager_set_currentPipeline_m2451449986(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m3779715225 (ScriptableRenderContext_t2070520004 * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___ptr0;
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
extern "C"  void ScriptableRenderContext__ctor_m3779715225_AdjustorThunk (RuntimeObject * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	ScriptableRenderContext_t2070520004 * _thisAdjusted = reinterpret_cast<ScriptableRenderContext_t2070520004 *>(__this + 1);
	ScriptableRenderContext__ctor_m3779715225(_thisAdjusted, ___ptr0, method);
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke(const FailedToLoadScriptObject_t652497987& unmarshaled, FailedToLoadScriptObject_t652497987_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke_back(const FailedToLoadScriptObject_t652497987_marshaled_pinvoke& marshaled, FailedToLoadScriptObject_t652497987& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke_cleanup(FailedToLoadScriptObject_t652497987_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t652497987_marshal_com(const FailedToLoadScriptObject_t652497987& unmarshaled, FailedToLoadScriptObject_t652497987_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t652497987_marshal_com_back(const FailedToLoadScriptObject_t652497987_marshaled_com& marshaled, FailedToLoadScriptObject_t652497987& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t652497987_marshal_com_cleanup(FailedToLoadScriptObject_t652497987_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2888171346 (GameObject_t2557347079 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m2888171346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m3037547037(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m1313108088 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m1313108088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m3037547037(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m2577022056 (GameObject_t2557347079 * __this, String_t* ___name0, TypeU5BU5D_t1985992169* ___components1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m2577022056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t1985992169* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m3037547037(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t1985992169* L_1 = ___components1;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0028;
	}

IL_0018:
	{
		TypeU5BU5D_t1985992169* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Type_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		Type_t * L_6 = V_0;
		GameObject_AddComponent_m1961781512(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_8 = V_2;
		TypeU5BU5D_t1985992169* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t1632713610 * GameObject_GetComponent_m469693343 (GameObject_t2557347079 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t1632713610 * (*GameObject_GetComponent_m469693343_ftn) (GameObject_t2557347079 *, Type_t *);
	static GameObject_GetComponent_m469693343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m469693343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	Component_t1632713610 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void GameObject_GetComponentFastPath_m3506133306 (GameObject_t2557347079 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*GameObject_GetComponentFastPath_m3506133306_ftn) (GameObject_t2557347079 *, Type_t *, intptr_t);
	static GameObject_GetComponentFastPath_m3506133306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m3506133306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1632713610 * GameObject_GetComponentInChildren_m1977537235 (GameObject_t2557347079 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method)
{
	typedef Component_t1632713610 * (*GameObject_GetComponentInChildren_m1977537235_ftn) (GameObject_t2557347079 *, Type_t *, bool);
	static GameObject_GetComponentInChildren_m1977537235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInChildren_m1977537235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)");
	Component_t1632713610 * retVal = _il2cpp_icall_func(__this, ___type0, ___includeInactive1);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t1632713610 * GameObject_GetComponentInParent_m955227985 (GameObject_t2557347079 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t1632713610 * (*GameObject_GetComponentInParent_m955227985_ftn) (GameObject_t2557347079 *, Type_t *);
	static GameObject_GetComponentInParent_m955227985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInParent_m955227985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInParent(System.Type)");
	Component_t1632713610 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C"  RuntimeArray * GameObject_GetComponentsInternal_m503734632 (GameObject_t2557347079 * __this, Type_t * ___type0, bool ___useSearchTypeAsArrayReturnType1, bool ___recursive2, bool ___includeInactive3, bool ___reverse4, RuntimeObject * ___resultList5, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*GameObject_GetComponentsInternal_m503734632_ftn) (GameObject_t2557347079 *, Type_t *, bool, bool, bool, bool, RuntimeObject *);
	static GameObject_GetComponentsInternal_m503734632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m503734632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	RuntimeArray * retVal = _il2cpp_icall_func(__this, ___type0, ___useSearchTypeAsArrayReturnType1, ___recursive2, ___includeInactive3, ___reverse4, ___resultList5);
	return retVal;
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t362059596 * GameObject_get_transform_m1570105226 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef Transform_t362059596 * (*GameObject_get_transform_m1570105226_ftn) (GameObject_t2557347079 *);
	static GameObject_get_transform_m1570105226_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m1570105226_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	Transform_t362059596 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m1777929869 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*GameObject_get_layer_m1777929869_ftn) (GameObject_t2557347079 *);
	static GameObject_get_layer_m1777929869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m1777929869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m2777858213 (GameObject_t2557347079 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_set_layer_m2777858213_ftn) (GameObject_t2557347079 *, int32_t);
	static GameObject_set_layer_m2777858213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m2777858213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.GameObject::get_active()
extern "C"  bool GameObject_get_active_m967287830 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_active_m967287830_ftn) (GameObject_t2557347079 *);
	static GameObject_get_active_m967287830_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_active_m967287830_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_active()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m944706439 (GameObject_t2557347079 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_SetActive_m944706439_ftn) (GameObject_t2557347079 *, bool);
	static GameObject_SetActive_m944706439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m944706439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C"  bool GameObject_get_activeSelf_m1150448021 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeSelf_m1150448021_ftn) (GameObject_t2557347079 *);
	static GameObject_get_activeSelf_m1150448021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeSelf_m1150448021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeSelf()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m1396882093 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m1396882093_ftn) (GameObject_t2557347079 *);
	static GameObject_get_activeInHierarchy_m1396882093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m1396882093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3906497588 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*GameObject_get_tag_m3906497588_ftn) (GameObject_t2557347079 *);
	static GameObject_get_tag_m3906497588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_tag_m3906497588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_tag()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t2988620542* GameObject_FindGameObjectsWithTag_m714911306 (RuntimeObject * __this /* static, unused */, String_t* ___tag0, const RuntimeMethod* method)
{
	typedef GameObjectU5BU5D_t2988620542* (*GameObject_FindGameObjectsWithTag_m714911306_ftn) (String_t*);
	static GameObject_FindGameObjectsWithTag_m714911306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_FindGameObjectsWithTag_m714911306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::FindGameObjectsWithTag(System.String)");
	GameObjectU5BU5D_t2988620542* retVal = _il2cpp_icall_func(___tag0);
	return retVal;
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m814261268 (GameObject_t2557347079 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*GameObject_SendMessage_m814261268_ftn) (GameObject_t2557347079 *, String_t*, RuntimeObject *, int32_t);
	static GameObject_SendMessage_m814261268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m814261268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m830843999 (GameObject_t2557347079 * __this, String_t* ___methodName0, int32_t ___options1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		GameObject_SendMessage_m814261268(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t1632713610 * GameObject_Internal_AddComponentWithType_m3924090706 (GameObject_t2557347079 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	typedef Component_t1632713610 * (*GameObject_Internal_AddComponentWithType_m3924090706_ftn) (GameObject_t2557347079 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m3924090706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m3924090706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	Component_t1632713610 * retVal = _il2cpp_icall_func(__this, ___componentType0);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1632713610 * GameObject_AddComponent_m1961781512 (GameObject_t2557347079 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	Component_t1632713610 * V_0 = NULL;
	{
		Type_t * L_0 = ___componentType0;
		Component_t1632713610 * L_1 = GameObject_Internal_AddComponentWithType_m3924090706(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Component_t1632713610 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m3037547037 (RuntimeObject * __this /* static, unused */, GameObject_t2557347079 * ___mono0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m3037547037_ftn) (GameObject_t2557347079 *, String_t*);
	static GameObject_Internal_CreateGameObject_m3037547037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m3037547037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono0, ___name1);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t2557347079 * GameObject_Find_m1280780905 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef GameObject_t2557347079 * (*GameObject_Find_m1280780905_ftn) (String_t*);
	static GameObject_Find_m1280780905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m1280780905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	GameObject_t2557347079 * retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern "C"  GameObject_t2557347079 * GameObject_get_gameObject_m537411087 (GameObject_t2557347079 * __this, const RuntimeMethod* method)
{
	GameObject_t2557347079 * V_0 = NULL;
	{
		V_0 = __this;
		goto IL_0008;
	}

IL_0008:
	{
		GameObject_t2557347079 * L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
extern "C"  void GL_Vertex3_m3924456566 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	typedef void (*GL_Vertex3_m3924456566_ftn) (float, float, float);
	static GL_Vertex3_m3924456566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_Vertex3_m3924456566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(___x0, ___y1, ___z2);
}
// System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
extern "C"  void GL_TexCoord2_m582470906 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, const RuntimeMethod* method)
{
	typedef void (*GL_TexCoord2_m582470906_ftn) (float, float);
	static GL_TexCoord2_m582470906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_TexCoord2_m582470906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::TexCoord2(System.Single,System.Single)");
	_il2cpp_icall_func(___x0, ___y1);
}
// System.Void UnityEngine.GL::MultiTexCoord2(System.Int32,System.Single,System.Single)
extern "C"  void GL_MultiTexCoord2_m2132342722 (RuntimeObject * __this /* static, unused */, int32_t ___unit0, float ___x1, float ___y2, const RuntimeMethod* method)
{
	typedef void (*GL_MultiTexCoord2_m2132342722_ftn) (int32_t, float, float);
	static GL_MultiTexCoord2_m2132342722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_MultiTexCoord2_m2132342722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::MultiTexCoord2(System.Int32,System.Single,System.Single)");
	_il2cpp_icall_func(___unit0, ___x1, ___y2);
}
// System.Void UnityEngine.GL::BeginInternal(System.Int32)
extern "C"  void GL_BeginInternal_m1457071361 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef void (*GL_BeginInternal_m1457071361_ftn) (int32_t);
	static GL_BeginInternal_m1457071361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_BeginInternal_m1457071361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::BeginInternal(System.Int32)");
	_il2cpp_icall_func(___mode0);
}
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m3571756615 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		GL_BeginInternal_m1457071361(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m265359348 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*GL_End_m265359348_ftn) ();
	static GL_End_m265359348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_End_m265359348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::End()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m1088671265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*GL_PushMatrix_m1088671265_ftn) ();
	static GL_PushMatrix_m1088671265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PushMatrix_m1088671265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PushMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m903936617 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*GL_PopMatrix_m903936617_ftn) ();
	static GL_PopMatrix_m903936617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PopMatrix_m903936617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PopMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::LoadIdentity()
extern "C"  void GL_LoadIdentity_m1490631333 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*GL_LoadIdentity_m1490631333_ftn) ();
	static GL_LoadIdentity_m1490631333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_LoadIdentity_m1490631333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::LoadIdentity()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::LoadOrtho()
extern "C"  void GL_LoadOrtho_m421181128 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*GL_LoadOrtho_m421181128_ftn) ();
	static GL_LoadOrtho_m421181128_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_LoadOrtho_m421181128_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::LoadOrtho()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::LoadProjectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void GL_LoadProjectionMatrix_m4252242195 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469  ___mat0, const RuntimeMethod* method)
{
	{
		GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549(NULL /*static, unused*/, (&___mat0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_LoadProjectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469 * ___mat0, const RuntimeMethod* method)
{
	typedef void (*GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549_ftn) (Matrix4x4_t1237934469 *);
	static GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_LoadProjectionMatrix_m467440549_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_LoadProjectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___mat0);
}
// UnityEngine.Matrix4x4 UnityEngine.GL::GetGPUProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
extern "C"  Matrix4x4_t1237934469  GL_GetGPUProjectionMatrix_m4127843242 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469  ___proj0, bool ___renderIntoTexture1, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = ___renderIntoTexture1;
		GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201(NULL /*static, unused*/, (&___proj0), L_0, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Matrix4x4_t1237934469  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_GetGPUProjectionMatrix(UnityEngine.Matrix4x4&,System.Boolean,UnityEngine.Matrix4x4&)
extern "C"  void GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1237934469 * ___proj0, bool ___renderIntoTexture1, Matrix4x4_t1237934469 * ___value2, const RuntimeMethod* method)
{
	typedef void (*GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201_ftn) (Matrix4x4_t1237934469 *, bool, Matrix4x4_t1237934469 *);
	static GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3062086201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_GetGPUProjectionMatrix(UnityEngine.Matrix4x4&,System.Boolean,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___proj0, ___renderIntoTexture1, ___value2);
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
extern "C"  void GL_Clear_m3507282052 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970  ___backgroundColor2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		bool L_0 = ___clearDepth0;
		bool L_1 = ___clearColor1;
		Color_t2582018970  L_2 = ___backgroundColor2;
		float L_3 = V_0;
		GL_Clear_m1308517048(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C"  void GL_Clear_m1308517048 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970  ___backgroundColor2, float ___depth3, const RuntimeMethod* method)
{
	{
		bool L_0 = ___clearDepth0;
		bool L_1 = ___clearColor1;
		Color_t2582018970  L_2 = ___backgroundColor2;
		float L_3 = ___depth3;
		GL_Internal_Clear_m842555462(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C"  void GL_Internal_Clear_m842555462 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970  ___backgroundColor2, float ___depth3, const RuntimeMethod* method)
{
	{
		bool L_0 = ___clearDepth0;
		bool L_1 = ___clearColor1;
		float L_2 = ___depth3;
		GL_INTERNAL_CALL_Internal_Clear_m2389845563(NULL /*static, unused*/, L_0, L_1, (&___backgroundColor2), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C"  void GL_INTERNAL_CALL_Internal_Clear_m2389845563 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, bool ___clearColor1, Color_t2582018970 * ___backgroundColor2, float ___depth3, const RuntimeMethod* method)
{
	typedef void (*GL_INTERNAL_CALL_Internal_Clear_m2389845563_ftn) (bool, bool, Color_t2582018970 *, float);
	static GL_INTERNAL_CALL_Internal_Clear_m2389845563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_Internal_Clear_m2389845563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)");
	_il2cpp_icall_func(___clearDepth0, ___clearColor1, ___backgroundColor2, ___depth3);
}
// System.Void UnityEngine.GL::ClearWithSkybox(System.Boolean,UnityEngine.Camera)
extern "C"  void GL_ClearWithSkybox_m3044942806 (RuntimeObject * __this /* static, unused */, bool ___clearDepth0, Camera_t2839736942 * ___camera1, const RuntimeMethod* method)
{
	typedef void (*GL_ClearWithSkybox_m3044942806_ftn) (bool, Camera_t2839736942 *);
	static GL_ClearWithSkybox_m3044942806_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_ClearWithSkybox_m3044942806_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::ClearWithSkybox(System.Boolean,UnityEngine.Camera)");
	_il2cpp_icall_func(___clearDepth0, ___camera1);
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3129154337_marshal_pinvoke(const Gradient_t3129154337& unmarshaled, Gradient_t3129154337_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t3129154337_marshal_pinvoke_back(const Gradient_t3129154337_marshaled_pinvoke& marshaled, Gradient_t3129154337& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3129154337_marshal_pinvoke_cleanup(Gradient_t3129154337_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3129154337_marshal_com(const Gradient_t3129154337& unmarshaled, Gradient_t3129154337_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t3129154337_marshal_com_back(const Gradient_t3129154337_marshaled_com& marshaled, Gradient_t3129154337& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3129154337_marshal_com_cleanup(Gradient_t3129154337_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m699625441 (Gradient_t3129154337 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		Gradient_Init_m3450230103(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m3450230103 (Gradient_t3129154337 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Init_m3450230103_ftn) (Gradient_t3129154337 *);
	static Gradient_Init_m3450230103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m3450230103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m164069323 (Gradient_t3129154337 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Cleanup_m164069323_ftn) (Gradient_t3129154337 *);
	static Gradient_Cleanup_m164069323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m164069323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C"  void Gradient_Finalize_m801161646 (Gradient_t3129154337 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m164069323(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Graphics::Internal_DrawMeshNow2(UnityEngine.Mesh,System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Graphics_Internal_DrawMeshNow2_m2814279584 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, int32_t ___subsetIndex1, Matrix4x4_t1237934469  ___matrix2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_Internal_DrawMeshNow2_m2814279584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t4030024733 * L_0 = ___mesh0;
		int32_t L_1 = ___subsetIndex1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613(NULL /*static, unused*/, L_0, L_1, (&___matrix2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, int32_t ___subsetIndex1, Matrix4x4_t1237934469 * ___matrix2, const RuntimeMethod* method)
{
	typedef void (*Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613_ftn) (Mesh_t4030024733 *, int32_t, Matrix4x4_t1237934469 *);
	static Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m2507585613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___mesh0, ___subsetIndex1, ___matrix2);
}
// System.Void UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer,System.Int32)
extern "C"  void Graphics_DrawProceduralIndirect_m642747402 (RuntimeObject * __this /* static, unused */, int32_t ___topology0, ComputeBuffer_t2358285523 * ___bufferWithArgs1, int32_t ___argsOffset2, const RuntimeMethod* method)
{
	typedef void (*Graphics_DrawProceduralIndirect_m642747402_ftn) (int32_t, ComputeBuffer_t2358285523 *, int32_t);
	static Graphics_DrawProceduralIndirect_m642747402_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_DrawProceduralIndirect_m642747402_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer,System.Int32)");
	_il2cpp_icall_func(___topology0, ___bufferWithArgs1, ___argsOffset2);
}
// System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
extern "C"  int32_t Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549_ftn) ();
	static Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
extern "C"  void Graphics_Internal_DrawTexture_m2483719006 (RuntimeObject * __this /* static, unused */, Internal_DrawTextureArguments_t642817784 * ___args0, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_DrawTexture_m2483719006_ftn) (Internal_DrawTextureArguments_t642817784 *);
	static Graphics_Internal_DrawTexture_m2483719006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_DrawTexture_m2483719006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)");
	_il2cpp_icall_func(___args0);
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C"  void Graphics_Blit_m407956217 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, const RuntimeMethod* method)
{
	typedef void (*Graphics_Blit_m407956217_ftn) (Texture_t2119925672 *, RenderTexture_t971269558 *);
	static Graphics_Blit_m407956217_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Blit_m407956217_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___source0, ___dest1);
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void Graphics_Blit_m4208839543 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_Blit_m4208839543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		Texture_t2119925672 * L_0 = ___source0;
		RenderTexture_t971269558 * L_1 = ___dest1;
		Material_t2815264910 * L_2 = ___mat2;
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Blit_m932569440(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m932569440 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_Blit_m932569440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2119925672 * L_0 = ___source0;
		RenderTexture_t971269558 * L_1 = ___dest1;
		Material_t2815264910 * L_2 = ___mat2;
		int32_t L_3 = ___pass3;
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_t328513675  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m22874897((&L_5), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_BlitMaterial_m1646902093(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (bool)1, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Graphics_Internal_BlitMaterial_m1646902093 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, bool ___setRT4, Vector2_t328513675  ___scale5, Vector2_t328513675  ___offset6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_Internal_BlitMaterial_m1646902093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2119925672 * L_0 = ___source0;
		RenderTexture_t971269558 * L_1 = ___dest1;
		Material_t2815264910 * L_2 = ___mat2;
		int32_t L_3 = ___pass3;
		bool L_4 = ___setRT4;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (&___scale5), (&___offset6), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, int32_t ___pass3, bool ___setRT4, Vector2_t328513675 * ___scale5, Vector2_t328513675 * ___offset6, const RuntimeMethod* method)
{
	typedef void (*Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574_ftn) (Texture_t2119925672 *, RenderTexture_t971269558 *, Material_t2815264910 *, int32_t, bool, Vector2_t328513675 *, Vector2_t328513675 *);
	static Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_INTERNAL_CALL_Internal_BlitMaterial_m4294947574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::INTERNAL_CALL_Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___source0, ___dest1, ___mat2, ___pass3, ___setRT4, ___scale5, ___offset6);
}
// System.Void UnityEngine.Graphics::BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
extern "C"  void Graphics_BlitMultiTap_m3323122277 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, Vector2U5BU5D_t1220531434* ___offsets3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_BlitMultiTap_m3323122277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2119925672 * L_0 = ___source0;
		RenderTexture_t971269558 * L_1 = ___dest1;
		Material_t2815264910 * L_2 = ___mat2;
		Vector2U5BU5D_t1220531434* L_3 = ___offsets3;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_BlitMultiTap_m1266876306(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
extern "C"  void Graphics_Internal_BlitMultiTap_m1266876306 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___source0, RenderTexture_t971269558 * ___dest1, Material_t2815264910 * ___mat2, Vector2U5BU5D_t1220531434* ___offsets3, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_BlitMultiTap_m1266876306_ftn) (Texture_t2119925672 *, RenderTexture_t971269558 *, Material_t2815264910 *, Vector2U5BU5D_t1220531434*);
	static Graphics_Internal_BlitMultiTap_m1266876306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_BlitMultiTap_m1266876306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])");
	_il2cpp_icall_func(___source0, ___dest1, ___mat2, ___offsets3);
}
// System.Void UnityEngine.Graphics::Internal_SetNullRT()
extern "C"  void Graphics_Internal_SetNullRT_m1010808975 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_SetNullRT_m1010808975_ftn) ();
	static Graphics_Internal_SetNullRT_m1010808975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_SetNullRT_m1010808975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_SetNullRT()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_Internal_SetRTSimple_m1383567032 (RuntimeObject * __this /* static, unused */, RenderBuffer_t4121715582 * ___color0, RenderBuffer_t4121715582 * ___depth1, int32_t ___mip2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_SetRTSimple_m1383567032_ftn) (RenderBuffer_t4121715582 *, RenderBuffer_t4121715582 *, int32_t, int32_t, int32_t);
	static Graphics_Internal_SetRTSimple_m1383567032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_SetRTSimple_m1383567032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)");
	_il2cpp_icall_func(___color0, ___depth1, ___mip2, ___face3, ___depthSlice4);
}
// System.Void UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_Internal_SetMRTSimple_m2432612892 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t846911691* ___colorSA0, RenderBuffer_t4121715582 * ___depth1, int32_t ___mip2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_SetMRTSimple_m2432612892_ftn) (RenderBufferU5BU5D_t846911691*, RenderBuffer_t4121715582 *, int32_t, int32_t, int32_t);
	static Graphics_Internal_SetMRTSimple_m2432612892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_SetMRTSimple_m2432612892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)");
	_il2cpp_icall_func(___colorSA0, ___depth1, ___mip2, ___face3, ___depthSlice4);
}
// System.Void UnityEngine.Graphics::SetRandomWriteTarget(System.Int32,UnityEngine.ComputeBuffer)
extern "C"  void Graphics_SetRandomWriteTarget_m2049819467 (RuntimeObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t2358285523 * ___uav1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRandomWriteTarget_m2049819467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = ___index0;
		ComputeBuffer_t2358285523 * L_1 = ___uav1;
		bool L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_SetRandomWriteTarget_m3349738363(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::SetRandomWriteTarget(System.Int32,UnityEngine.ComputeBuffer,System.Boolean)
extern "C"  void Graphics_SetRandomWriteTarget_m3349738363 (RuntimeObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t2358285523 * ___uav1, bool ___preserveCounterValue2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRandomWriteTarget_m3349738363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComputeBuffer_t2358285523 * L_0 = ___uav1;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_1 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_1, _stringLiteral3980266961, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		ComputeBuffer_t2358285523 * L_2 = ___uav1;
		NullCheck(L_2);
		intptr_t L_3 = L_2->get_m_Ptr_0();
		bool L_4 = IntPtr_op_Equality_m1348886383(NULL /*static, unused*/, L_3, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		ObjectDisposedException_t1428820261 * L_5 = (ObjectDisposedException_t1428820261 *)il2cpp_codegen_object_new(ObjectDisposedException_t1428820261_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m2302049460(L_5, _stringLiteral3980266961, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		int32_t L_6 = ___index0;
		ComputeBuffer_t2358285523 * L_7 = ___uav1;
		bool L_8 = ___preserveCounterValue2;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::ClearRandomWriteTargets()
extern "C"  void Graphics_ClearRandomWriteTargets_m3389582505 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*Graphics_ClearRandomWriteTargets_m3389582505_ftn) ();
	static Graphics_ClearRandomWriteTargets_m3389582505_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_ClearRandomWriteTargets_m3389582505_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::ClearRandomWriteTargets()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Boolean)
extern "C"  void Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712 (RuntimeObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t2358285523 * ___uav1, bool ___preserveCounterValue2, const RuntimeMethod* method)
{
	typedef void (*Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712_ftn) (int32_t, ComputeBuffer_t2358285523 *, bool);
	static Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_SetRandomWriteTargetBuffer_m1165436712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Boolean)");
	_il2cpp_icall_func(___index0, ___uav1, ___preserveCounterValue2);
}
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m1495557259 (RuntimeObject * __this /* static, unused */, RenderBuffer_t4121715582  ___colorBuffer0, RenderBuffer_t4121715582  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRenderTargetImpl_m1495557259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderBuffer_t4121715582  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t4121715582  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderBuffer_t4121715582  L_0 = ___colorBuffer0;
		V_0 = L_0;
		RenderBuffer_t4121715582  L_1 = ___depthBuffer1;
		V_1 = L_1;
		int32_t L_2 = ___mipLevel2;
		int32_t L_3 = ___face3;
		int32_t L_4 = ___depthSlice4;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_SetRTSimple_m1383567032(NULL /*static, unused*/, (&V_0), (&V_1), L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderTexture,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m4294744146 (RuntimeObject * __this /* static, unused */, RenderTexture_t971269558 * ___rt0, int32_t ___mipLevel1, int32_t ___face2, int32_t ___depthSlice3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRenderTargetImpl_m4294744146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t971269558 * L_0 = ___rt0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3123183460(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		RenderTexture_t971269558 * L_2 = ___rt0;
		NullCheck(L_2);
		RenderBuffer_t4121715582  L_3 = RenderTexture_get_colorBuffer_m425377552(L_2, /*hidden argument*/NULL);
		RenderTexture_t971269558 * L_4 = ___rt0;
		NullCheck(L_4);
		RenderBuffer_t4121715582  L_5 = RenderTexture_get_depthBuffer_m3164082685(L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___mipLevel1;
		int32_t L_7 = ___face2;
		int32_t L_8 = ___depthSlice3;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_SetRenderTargetImpl_m1495557259(NULL /*static, unused*/, L_3, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_SetNullRT_m1010808975(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void Graphics_SetRenderTargetImpl_m3156622254 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t846911691* ___colorBuffers0, RenderBuffer_t4121715582  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, int32_t ___depthSlice4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRenderTargetImpl_m3156622254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderBuffer_t4121715582  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderBuffer_t4121715582  L_0 = ___depthBuffer1;
		V_0 = L_0;
		RenderBufferU5BU5D_t846911691* L_1 = ___colorBuffers0;
		int32_t L_2 = ___mipLevel2;
		int32_t L_3 = ___face3;
		int32_t L_4 = ___depthSlice4;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_SetMRTSimple_m2432612892(NULL /*static, unused*/, L_1, (&V_0), L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTexture)
extern "C"  void Graphics_SetRenderTarget_m3487633971 (RuntimeObject * __this /* static, unused */, RenderTexture_t971269558 * ___rt0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRenderTarget_m3487633971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t971269558 * L_0 = ___rt0;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_SetRenderTargetImpl_m4294744146(NULL /*static, unused*/, L_0, 0, (-1), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer)
extern "C"  void Graphics_SetRenderTarget_m1420051683 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t846911691* ___colorBuffers0, RenderBuffer_t4121715582  ___depthBuffer1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_SetRenderTarget_m1420051683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderBufferU5BU5D_t846911691* L_0 = ___colorBuffers0;
		RenderBuffer_t4121715582  L_1 = ___depthBuffer1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_SetRenderTargetImpl_m3156622254(NULL /*static, unused*/, L_0, L_1, 0, (-1), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4)
extern "C"  void Graphics_DrawMeshNow_m2655627097 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, Matrix4x4_t1237934469  ___matrix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_DrawMeshNow_m2655627097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t4030024733 * L_0 = ___mesh0;
		Matrix4x4_t1237934469  L_1 = ___matrix1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_DrawMeshNow_m46634237(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4,System.Int32)
extern "C"  void Graphics_DrawMeshNow_m46634237 (RuntimeObject * __this /* static, unused */, Mesh_t4030024733 * ___mesh0, Matrix4x4_t1237934469  ___matrix1, int32_t ___materialIndex2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics_DrawMeshNow_m46634237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t4030024733 * L_0 = ___mesh0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_2 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_2, _stringLiteral3196149666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		Mesh_t4030024733 * L_3 = ___mesh0;
		int32_t L_4 = ___materialIndex2;
		Matrix4x4_t1237934469  L_5 = ___matrix1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t3054375344_il2cpp_TypeInfo_var);
		Graphics_Internal_DrawMeshNow2_m2814279584(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::.cctor()
extern "C"  void Graphics__cctor_m1827492786 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Graphics__cctor_m1827492786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Graphics_Internal_GetMaxDrawMeshInstanceCount_m2244659549(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Graphics_t3054375344_StaticFields*)il2cpp_codegen_static_fields_for(Graphics_t3054375344_il2cpp_TypeInfo_var))->set_kMaxDrawMeshInstanceCount_0(L_0);
		return;
	}
}
// System.Void UnityEngine.GUIElement::.ctor()
extern "C"  void GUIElement__ctor_m493013554 (GUIElement_t2946029536 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2653597291(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t2946029536 * GUILayer_HitTest_m2110564057 (GUILayer_t478658086 * __this, Vector3_t1986933152  ___screenPosition0, const RuntimeMethod* method)
{
	GUIElement_t2946029536 * V_0 = NULL;
	{
		GUIElement_t2946029536 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m911935273(NULL /*static, unused*/, __this, (&___screenPosition0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		GUIElement_t2946029536 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t2946029536 * GUILayer_INTERNAL_CALL_HitTest_m911935273 (RuntimeObject * __this /* static, unused */, GUILayer_t478658086 * ___self0, Vector3_t1986933152 * ___screenPosition1, const RuntimeMethod* method)
{
	typedef GUIElement_t2946029536 * (*GUILayer_INTERNAL_CALL_HitTest_m911935273_ftn) (GUILayer_t478658086 *, Vector3_t1986933152 *);
	static GUILayer_INTERNAL_CALL_HitTest_m911935273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m911935273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	GUIElement_t2946029536 * retVal = _il2cpp_icall_func(___self0, ___screenPosition1);
	return retVal;
}
// System.Object UnityEngine.GUIStateObjects::GetStateObject(System.Type,System.Int32)
extern "C"  RuntimeObject * GUIStateObjects_GetStateObject_m1946903358 (RuntimeObject * __this /* static, unused */, Type_t * ___t0, int32_t ___controlID1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GUIStateObjects_GetStateObject_m1946903358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t2115760413_il2cpp_TypeInfo_var);
		Dictionary_2_t1708198297 * L_0 = ((GUIStateObjects_t2115760413_StaticFields*)il2cpp_codegen_static_fields_for(GUIStateObjects_t2115760413_il2cpp_TypeInfo_var))->get_s_StateCache_0();
		int32_t L_1 = ___controlID1;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m3631383453(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3631383453_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2972540513(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ___t0;
		if ((((RuntimeObject*)(Type_t *)L_4) == ((RuntimeObject*)(Type_t *)L_5)))
		{
			goto IL_0034;
		}
	}

IL_001f:
	{
		Type_t * L_6 = ___t0;
		RuntimeObject * L_7 = Activator_CreateInstance_m2717577512(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t2115760413_il2cpp_TypeInfo_var);
		Dictionary_2_t1708198297 * L_8 = ((GUIStateObjects_t2115760413_StaticFields*)il2cpp_codegen_static_fields_for(GUIStateObjects_t2115760413_il2cpp_TypeInfo_var))->get_s_StateCache_0();
		int32_t L_9 = ___controlID1;
		RuntimeObject * L_10 = V_0;
		NullCheck(L_8);
		Dictionary_2_set_Item_m1490102977(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m1490102977_RuntimeMethod_var);
	}

IL_0034:
	{
		RuntimeObject * L_11 = V_0;
		V_1 = L_11;
		goto IL_003b;
	}

IL_003b:
	{
		RuntimeObject * L_12 = V_1;
		return L_12;
	}
}
// System.Void UnityEngine.GUIStateObjects::.cctor()
extern "C"  void GUIStateObjects__cctor_m2702348782 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GUIStateObjects__cctor_m2702348782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1708198297 * L_0 = (Dictionary_2_t1708198297 *)il2cpp_codegen_object_new(Dictionary_2_t1708198297_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2905489384(L_0, /*hidden argument*/Dictionary_2__ctor_m2905489384_RuntimeMethod_var);
		((GUIStateObjects_t2115760413_StaticFields*)il2cpp_codegen_static_fields_for(GUIStateObjects_t2115760413_il2cpp_TypeInfo_var))->set_s_StateCache_0(L_0);
		return;
	}
}
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C"  void GUITexture_set_texture_m1541504762 (GUITexture_t2618579966 * __this, Texture_t2119925672 * ___value0, const RuntimeMethod* method)
{
	typedef void (*GUITexture_set_texture_m1541504762_ftn) (GUITexture_t2618579966 *, Texture_t2119925672 *);
	static GUITexture_set_texture_m1541504762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_set_texture_m1541504762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::set_texture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.GUITexture::set_pixelInset(UnityEngine.Rect)
extern "C"  void GUITexture_set_pixelInset_m249233138 (GUITexture_t2618579966 * __this, Rect_t3039462994  ___value0, const RuntimeMethod* method)
{
	{
		GUITexture_INTERNAL_set_pixelInset_m2310779897(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
extern "C"  void GUITexture_INTERNAL_set_pixelInset_m2310779897 (GUITexture_t2618579966 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*GUITexture_INTERNAL_set_pixelInset_m2310779897_ftn) (GUITexture_t2618579966 *, Rect_t3039462994 *);
	static GUITexture_INTERNAL_set_pixelInset_m2310779897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_INTERNAL_set_pixelInset_m2310779897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
extern "C"  void HeaderAttribute__ctor_m2488030370 (HeaderAttribute_t3937329216 * __this, String_t* ___header0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___header0;
		__this->set_header_0(L_0);
		return;
	}
}
// System.Void UnityEngine.HideInInspector::.ctor()
extern "C"  void HideInInspector__ctor_m3685610268 (HideInInspector_t3393216074 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C"  void IL2CPPStructAlignmentAttribute__ctor_m954288569 (IL2CPPStructAlignmentAttribute_t542619783 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		__this->set_Align_0(1);
		return;
	}
}
// System.Void UnityEngine.ImageEffectAllowedInSceneView::.ctor()
extern "C"  void ImageEffectAllowedInSceneView__ctor_m2434416305 (ImageEffectAllowedInSceneView_t1605044925 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ImageEffectOpaque::.ctor()
extern "C"  void ImageEffectOpaque__ctor_m2983805027 (ImageEffectOpaque_t3353908156 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ImageEffectTransformsToLDR::.ctor()
extern "C"  void ImageEffectTransformsToLDR__ctor_m1267511082 (ImageEffectTransformsToLDR_t1398224731 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
extern "C"  bool Input_GetKeyInt_m3595410015 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetKeyInt_m3595410015_ftn) (int32_t);
	static Input_GetKeyInt_m3595410015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyInt_m3595410015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyInt(System.Int32)");
	bool retVal = _il2cpp_icall_func(___key0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
extern "C"  bool Input_GetKeyUpInt_m3068478983 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetKeyUpInt_m3068478983_ftn) (int32_t);
	static Input_GetKeyUpInt_m3068478983_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyUpInt_m3068478983_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyUpInt(System.Int32)");
	bool retVal = _il2cpp_icall_func(___key0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
extern "C"  bool Input_GetKeyDownInt_m626813250 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetKeyDownInt_m626813250_ftn) (int32_t);
	static Input_GetKeyDownInt_m626813250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownInt_m626813250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownInt(System.Int32)");
	bool retVal = _il2cpp_icall_func(___key0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
extern "C"  bool Input_GetKeyDownString_m4293950650 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetKeyDownString_m4293950650_ftn) (String_t*);
	static Input_GetKeyDownString_m4293950650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownString_m4293950650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownString(System.String)");
	bool retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m200970563 (RuntimeObject * __this /* static, unused */, String_t* ___axisName0, const RuntimeMethod* method)
{
	typedef float (*Input_GetAxis_m200970563_ftn) (String_t*);
	static Input_GetAxis_m200970563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxis_m200970563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxis(System.String)");
	float retVal = _il2cpp_icall_func(___axisName0);
	return retVal;
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m2020871655 (RuntimeObject * __this /* static, unused */, String_t* ___axisName0, const RuntimeMethod* method)
{
	typedef float (*Input_GetAxisRaw_m2020871655_ftn) (String_t*);
	static Input_GetAxisRaw_m2020871655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m2020871655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	float retVal = _il2cpp_icall_func(___axisName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m984724902 (RuntimeObject * __this /* static, unused */, String_t* ___buttonName0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetButton_m984724902_ftn) (String_t*);
	static Input_GetButton_m984724902_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButton_m984724902_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButton(System.String)");
	bool retVal = _il2cpp_icall_func(___buttonName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m2408025371 (RuntimeObject * __this /* static, unused */, String_t* ___buttonName0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetButtonDown_m2408025371_ftn) (String_t*);
	static Input_GetButtonDown_m2408025371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m2408025371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	bool retVal = _il2cpp_icall_func(___buttonName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetButtonUp(System.String)
extern "C"  bool Input_GetButtonUp_m4017018665 (RuntimeObject * __this /* static, unused */, String_t* ___buttonName0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetButtonUp_m4017018665_ftn) (String_t*);
	static Input_GetButtonUp_m4017018665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonUp_m4017018665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonUp(System.String)");
	bool retVal = _il2cpp_icall_func(___buttonName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m260282928 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetKey_m260282928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyInt_m3595410015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C"  bool Input_GetKeyDown_m2906836562 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetKeyDown_m2906836562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownString_m4293950650(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m3096149643 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetKeyDown_m3096149643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownInt_m626813250(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyUp_m618320100 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetKeyUp_m618320100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUpInt_m3068478983(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m823933278 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButton_m823933278_ftn) (int32_t);
	static Input_GetMouseButton_m823933278_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m823933278_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m1898615268 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonDown_m1898615268_ftn) (int32_t);
	static Input_GetMouseButtonDown_m1898615268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m1898615268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m2653921098 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonUp_m2653921098_ftn) (int32_t);
	static Input_GetMouseButtonUp_m2653921098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m2653921098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t1986933152  Input_get_mousePosition_m4034714020 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mousePosition_m4034714020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m2679056273(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m2679056273 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m2679056273_ftn) (Vector3_t1986933152 *);
	static Input_INTERNAL_get_mousePosition_m2679056273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m2679056273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern "C"  Vector2_t328513675  Input_get_mouseScrollDelta_m1084063468 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mouseScrollDelta_m1084063468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m1491210786(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t328513675  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t328513675  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m1491210786 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m1491210786_ftn) (Vector2_t328513675 *);
	static Input_INTERNAL_get_mouseScrollDelta_m1491210786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m1491210786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C"  bool Input_get_mousePresent_m2865707439 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_mousePresent_m2865707439_ftn) ();
	static Input_get_mousePresent_m2865707439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m2865707439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.String UnityEngine.Input::get_inputString()
extern "C"  String_t* Input_get_inputString_m3124880855 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*Input_get_inputString_m3124880855_ftn) ();
	static Input_get_inputString_m3124880855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_inputString_m3124880855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_inputString()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
extern "C"  Vector3_t1986933152  Input_get_acceleration_m850151000 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_acceleration_m850151000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_acceleration_m2630130133(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_acceleration(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_acceleration_m2630130133 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_acceleration_m2630130133_ftn) (Vector3_t1986933152 *);
	static Input_INTERNAL_get_acceleration_m2630130133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_acceleration_m2630130133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_acceleration(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern "C"  TouchU5BU5D_t170615360* Input_get_touches_m3317929777 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_touches_m3317929777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TouchU5BU5D_t170615360* V_1 = NULL;
	int32_t V_2 = 0;
	TouchU5BU5D_t170615360* V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m2576465830(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = ((TouchU5BU5D_t170615360*)SZArrayNew(TouchU5BU5D_t170615360_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_2 = 0;
		goto IL_002b;
	}

IL_0015:
	{
		TouchU5BU5D_t170615360* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Touch_t1048744301  L_5 = Input_GetTouch_m3048670206(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		*(Touch_t1048744301 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))) = L_5;
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0015;
		}
	}
	{
		TouchU5BU5D_t170615360* L_9 = V_1;
		V_3 = L_9;
		goto IL_0039;
	}

IL_0039:
	{
		TouchU5BU5D_t170615360* L_10 = V_3;
		return L_10;
	}
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1048744301  Input_GetTouch_m3048670206 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetTouch_m3048670206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1048744301  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t1048744301  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_CALL_GetTouch_m695198429(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Touch_t1048744301  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Touch_t1048744301  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m695198429 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t1048744301 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_CALL_GetTouch_m695198429_ftn) (int32_t, Touch_t1048744301 *);
	static Input_INTERNAL_CALL_GetTouch_m695198429_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_CALL_GetTouch_m695198429_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m2576465830 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_touchCount_m2576465830_ftn) ();
	static Input_get_touchCount_m2576465830_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m2576465830_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C"  bool Input_get_touchSupported_m3687555646 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_touchSupported_m3687555646_ftn) ();
	static Input_get_touchSupported_m3687555646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchSupported_m3687555646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchSupported()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
extern "C"  int32_t Input_get_imeCompositionMode_m2151937722 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_imeCompositionMode_m2151937722_ftn) ();
	static Input_get_imeCompositionMode_m2151937722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_imeCompositionMode_m2151937722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_imeCompositionMode()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C"  void Input_set_imeCompositionMode_m1749001127 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_set_imeCompositionMode_m1749001127_ftn) (int32_t);
	static Input_set_imeCompositionMode_m1749001127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m1749001127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C"  String_t* Input_get_compositionString_m1570433118 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*Input_get_compositionString_m1570433118_ftn) ();
	static Input_get_compositionString_m1570433118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m1570433118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Vector2 UnityEngine.Input::get_compositionCursorPos()
extern "C"  Vector2_t328513675  Input_get_compositionCursorPos_m1395749328 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_compositionCursorPos_m1395749328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_compositionCursorPos_m3585646261(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t328513675  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t328513675  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern "C"  void Input_set_compositionCursorPos_m626063828 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_set_compositionCursorPos_m626063828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m1587641303(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m3585646261 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_compositionCursorPos_m3585646261_ftn) (Vector2_t328513675 *);
	static Input_INTERNAL_get_compositionCursorPos_m3585646261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_compositionCursorPos_m3585646261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m1587641303 (RuntimeObject * __this /* static, unused */, Vector2_t328513675 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m1587641303_ftn) (Vector2_t328513675 *);
	static Input_INTERNAL_set_compositionCursorPos_m1587641303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m1587641303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::.cctor()
extern "C"  void Input__cctor_m2833959137 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m2833959137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t1735245659_StaticFields*)il2cpp_codegen_static_fields_for(Input_t1735245659_il2cpp_TypeInfo_var))->set_m_MainGyro_0((Gyroscope_t2906904238 *)NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawTextureArguments
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke(const Internal_DrawTextureArguments_t642817784& unmarshaled, Internal_DrawTextureArguments_t642817784_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___texture_10Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'texture' of type 'Internal_DrawTextureArguments': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___texture_10Exception);
}
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke_back(const Internal_DrawTextureArguments_t642817784_marshaled_pinvoke& marshaled, Internal_DrawTextureArguments_t642817784& unmarshaled)
{
	Il2CppCodeGenException* ___texture_10Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'texture' of type 'Internal_DrawTextureArguments': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___texture_10Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawTextureArguments
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke_cleanup(Internal_DrawTextureArguments_t642817784_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawTextureArguments
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_com(const Internal_DrawTextureArguments_t642817784& unmarshaled, Internal_DrawTextureArguments_t642817784_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___texture_10Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'texture' of type 'Internal_DrawTextureArguments': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___texture_10Exception);
}
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_com_back(const Internal_DrawTextureArguments_t642817784_marshaled_com& marshaled, Internal_DrawTextureArguments_t642817784& unmarshaled)
{
	Il2CppCodeGenException* ___texture_10Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'texture' of type 'Internal_DrawTextureArguments': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___texture_10Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawTextureArguments
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_com_cleanup(Internal_DrawTextureArguments_t642817784_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m1794695850 (DefaultValueAttribute_t3676342093 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m436001336 (DefaultValueAttribute_t3676342093 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern "C"  bool DefaultValueAttribute_Equals_m3839951568 (DefaultValueAttribute_t3676342093 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m3839951568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t3676342093 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t3676342093 *)IsInstClass((RuntimeObject*)L_0, DefaultValueAttribute_t3676342093_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t3676342093 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0046;
	}

IL_0015:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		DefaultValueAttribute_t3676342093 * L_3 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_4 = DefaultValueAttribute_get_Value_m436001336(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((RuntimeObject*)(RuntimeObject *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		goto IL_0046;
	}

IL_002f:
	{
		RuntimeObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t3676342093 * L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_7 = DefaultValueAttribute_get_Value_m436001336(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		V_1 = L_8;
		goto IL_0046;
	}

IL_0046:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m2166576836 (DefaultValueAttribute_t3676342093 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m2169372130(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0029;
	}

IL_0018:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C"  void ExcludeFromDocsAttribute__ctor_m2841806499 (ExcludeFromDocsAttribute_t427370092 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m3002643069 (LocalNotification_t1085454254 * __this, const RuntimeMethod* method)
{
	typedef void (*LocalNotification_Destroy_m3002643069_ftn) (LocalNotification_t1085454254 *);
	static LocalNotification_Destroy_m3002643069_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m3002643069_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m3748430994 (LocalNotification_t1085454254 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m3002643069(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern "C"  void LocalNotification__cctor_m41159022 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m41159022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t359870098  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m4148578998((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m916901216((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t1085454254_StaticFields*)il2cpp_codegen_static_fields_for(LocalNotification_t1085454254_il2cpp_TypeInfo_var))->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m1964956793 (RemoteNotification_t3274515193 * __this, const RuntimeMethod* method)
{
	typedef void (*RemoteNotification_Destroy_m1964956793_ftn) (RemoteNotification_t3274515193 *);
	static RemoteNotification_Destroy_m1964956793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m1964956793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m2268681841 (RemoteNotification_t3274515193 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m1964956793(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
