﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Cinemachine.ICinemachineCamera
struct ICinemachineCamera_t3777721060;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;
// Cinemachine.CinemachineBrain
struct CinemachineBrain_t679733552;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t3088348880;
// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct List_1_t357561476;
// Cinemachine.Utility.GaussianWindow1D_Vector3
struct GaussianWindow1D_Vector3_t1200723002;
// Cinemachine.CinemachineCollider/CompiledCurbFeeler[]
struct CompiledCurbFeelerU5BU5D_t4033313670;
// Cinemachine.CinemachineBlend
struct CinemachineBlend_t2146485918;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t2557508663;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.Transform
struct Transform_t362059596;
// System.Void
struct Void_t653366341;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item[]
struct ItemU5BU5D_t1301757822;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UnityEngine.Camera
struct Camera_t2839736942;
// Cinemachine.CinemachineBlenderSettings/CustomBlend[]
struct CustomBlendU5BU5D_t4089921897;
// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_t2803177733;
// Cinemachine.ICinemachineComponent[]
struct ICinemachineComponentU5BU5D_t3125037657;
// Cinemachine.CinemachineFreeLook
struct CinemachineFreeLook_t411363320;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1934891917;
// System.Action
struct Action_t370180854;
// System.String[]
struct StringU5BU5D_t2511808107;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_t2642749565;
// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate
struct OnPostPipelineStageDelegate_t938695173;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.Material
struct Material_t2815264910;
// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCollider/VcamExtraState>
struct Dictionary_2_t4106222255;
// UnityEngine.Texture
struct Texture_t2119925672;
// Cinemachine.CinemachinePathBase
struct CinemachinePathBase_t3902360541;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// UnityEngine.UI.Text
struct Text_t1790657652;
// UnityEngine.UI.Image
struct Image_t2816987602;
// TokenHolderLegacy
struct TokenHolderLegacy_t1380075429;
// Cinemachine.CinemachineBlenderSettings
struct CinemachineBlenderSettings_t865950197;
// Cinemachine.CinemachineBrain/PostFXHandlerDelegate
struct PostFXHandlerDelegate_t2862114331;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2603596161;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain/OverrideStackFrame>
struct List_1_t1470468345;
// Cinemachine.NoiseSettings
struct NoiseSettings_t2082871546;
// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker
struct HeadingTracker_t2287318816;
// UnityEngine.Rigidbody
struct Rigidbody_t4273256674;
// Cinemachine.CinemachineTargetGroup/Target[]
struct TargetU5BU5D_t3745836252;
// UnityEngine.UI.Button
struct Button_t1293135404;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t1258674085;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.Utility.GaussianWindow1D_Vector3>
struct Dictionary_2_t2995859955;
// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.Utility.GaussianWindow1D_CameraRotation>
struct Dictionary_2_t1885300805;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t1489827645;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t363162864;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t892035751;
// Cinemachine.CinemachineVirtualCameraBase[]
struct CinemachineVirtualCameraBaseU5BU5D_t3901427982;
// System.Comparison`1<Cinemachine.CinemachineClearShot/Pair>
struct Comparison_1_t4217023303;
// UnityEngine.RenderTexture
struct RenderTexture_t971269558;
// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct CreatePipelineDelegate_t4206421982;
// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct DestroyPipelineDelegate_t926775217;
// System.Comparison`1<Cinemachine.ICinemachineComponent>
struct Comparison_1_t639385221;
// Cinemachine.CinemachineVirtualCamera[]
struct CinemachineVirtualCameraU5BU5D_t1136890184;
// Cinemachine.CinemachineOrbitalTransposer[]
struct CinemachineOrbitalTransposerU5BU5D_t1933852220;
// Cinemachine.CinemachineFreeLook/CreateRigDelegate
struct CreateRigDelegate_t931026514;
// Cinemachine.CinemachineFreeLook/DestroyRigDelegate
struct DestroyRigDelegate_t2622870986;
// Cinemachine.CinemachinePath/Appearance
struct Appearance_t955816971;
// Cinemachine.CinemachinePath/Waypoint[]
struct WaypointU5BU5D_t106678226;
// UnityEngine.Animator
struct Animator_t2768715325;
// Cinemachine.CinemachineStateDrivenCamera/Instruction[]
struct InstructionU5BU5D_t455858040;
// Cinemachine.CinemachineStateDrivenCamera/ParentHash[]
struct ParentHashU5BU5D_t4234504058;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t831864540;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T1429447287_H
#define U3CMODULEU3E_T1429447287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447287 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447287_H
#ifndef CINEMACHINEBLEND_T2146485918_H
#define CINEMACHINEBLEND_T2146485918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlend
struct  CinemachineBlend_t2146485918  : public RuntimeObject
{
public:
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBlend::<CamA>k__BackingField
	RuntimeObject* ___U3CCamAU3Ek__BackingField_0;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBlend::<CamB>k__BackingField
	RuntimeObject* ___U3CCamBU3Ek__BackingField_1;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineBlend::<BlendCurve>k__BackingField
	AnimationCurve_t2757045165 * ___U3CBlendCurveU3Ek__BackingField_2;
	// System.Single Cinemachine.CinemachineBlend::<TimeInBlend>k__BackingField
	float ___U3CTimeInBlendU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCamAU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CinemachineBlend_t2146485918, ___U3CCamAU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CCamAU3Ek__BackingField_0() const { return ___U3CCamAU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CCamAU3Ek__BackingField_0() { return &___U3CCamAU3Ek__BackingField_0; }
	inline void set_U3CCamAU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CCamAU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamAU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCamBU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CinemachineBlend_t2146485918, ___U3CCamBU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCamBU3Ek__BackingField_1() const { return ___U3CCamBU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCamBU3Ek__BackingField_1() { return &___U3CCamBU3Ek__BackingField_1; }
	inline void set_U3CCamBU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCamBU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamBU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBlendCurveU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CinemachineBlend_t2146485918, ___U3CBlendCurveU3Ek__BackingField_2)); }
	inline AnimationCurve_t2757045165 * get_U3CBlendCurveU3Ek__BackingField_2() const { return ___U3CBlendCurveU3Ek__BackingField_2; }
	inline AnimationCurve_t2757045165 ** get_address_of_U3CBlendCurveU3Ek__BackingField_2() { return &___U3CBlendCurveU3Ek__BackingField_2; }
	inline void set_U3CBlendCurveU3Ek__BackingField_2(AnimationCurve_t2757045165 * value)
	{
		___U3CBlendCurveU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBlendCurveU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTimeInBlendU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CinemachineBlend_t2146485918, ___U3CTimeInBlendU3Ek__BackingField_3)); }
	inline float get_U3CTimeInBlendU3Ek__BackingField_3() const { return ___U3CTimeInBlendU3Ek__BackingField_3; }
	inline float* get_address_of_U3CTimeInBlendU3Ek__BackingField_3() { return &___U3CTimeInBlendU3Ek__BackingField_3; }
	inline void set_U3CTimeInBlendU3Ek__BackingField_3(float value)
	{
		___U3CTimeInBlendU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLEND_T2146485918_H
#ifndef U3CAFTERPHYSICSU3EC__ITERATOR0_T1567695154_H
#define U3CAFTERPHYSICSU3EC__ITERATOR0_T1567695154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBrain/<AfterPhysics>c__Iterator0
struct  U3CAfterPhysicsU3Ec__Iterator0_t1567695154  : public RuntimeObject
{
public:
	// Cinemachine.CinemachineBrain Cinemachine.CinemachineBrain/<AfterPhysics>c__Iterator0::$this
	CinemachineBrain_t679733552 * ___U24this_0;
	// System.Object Cinemachine.CinemachineBrain/<AfterPhysics>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Cinemachine.CinemachineBrain/<AfterPhysics>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Cinemachine.CinemachineBrain/<AfterPhysics>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAfterPhysicsU3Ec__Iterator0_t1567695154, ___U24this_0)); }
	inline CinemachineBrain_t679733552 * get_U24this_0() const { return ___U24this_0; }
	inline CinemachineBrain_t679733552 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CinemachineBrain_t679733552 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAfterPhysicsU3Ec__Iterator0_t1567695154, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAfterPhysicsU3Ec__Iterator0_t1567695154, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAfterPhysicsU3Ec__Iterator0_t1567695154, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAFTERPHYSICSU3EC__ITERATOR0_T1567695154_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef IMAGEEFFECTS_T1429922186_H
#define IMAGEEFFECTS_T1429922186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffects
struct  ImageEffects_t1429922186  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTS_T1429922186_H
#ifndef QUADS_T3919723195_H
#define QUADS_T3919723195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Quads
struct  Quads_t3919723195  : public RuntimeObject
{
public:

public:
};

struct Quads_t3919723195_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::meshes
	MeshU5BU5D_t3088348880* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Quads::currentQuads
	int32_t ___currentQuads_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Quads_t3919723195_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3088348880* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3088348880** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3088348880* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentQuads_1() { return static_cast<int32_t>(offsetof(Quads_t3919723195_StaticFields, ___currentQuads_1)); }
	inline int32_t get_currentQuads_1() const { return ___currentQuads_1; }
	inline int32_t* get_address_of_currentQuads_1() { return &___currentQuads_1; }
	inline void set_currentQuads_1(int32_t value)
	{
		___currentQuads_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADS_T3919723195_H
#ifndef TRIANGLES_T884492039_H
#define TRIANGLES_T884492039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Triangles
struct  Triangles_t884492039  : public RuntimeObject
{
public:

public:
};

struct Triangles_t884492039_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::meshes
	MeshU5BU5D_t3088348880* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Triangles::currentTris
	int32_t ___currentTris_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Triangles_t884492039_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3088348880* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3088348880** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3088348880* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentTris_1() { return static_cast<int32_t>(offsetof(Triangles_t884492039_StaticFields, ___currentTris_1)); }
	inline int32_t get_currentTris_1() const { return ___currentTris_1; }
	inline int32_t* get_address_of_currentTris_1() { return &___currentTris_1; }
	inline void set_currentTris_1(int32_t value)
	{
		___currentTris_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLES_T884492039_H
#ifndef GUISIZER_T1539903446_H
#define GUISIZER_T1539903446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUISizer
struct  GUISizer_t1539903446  : public RuntimeObject
{
public:

public:
};

struct GUISizer_t1539903446_StaticFields
{
public:
	// System.Single GUISizer::WIDTH
	float ___WIDTH_0;
	// System.Single GUISizer::HEIGHT
	float ___HEIGHT_1;
	// System.Collections.Generic.List`1<UnityEngine.Matrix4x4> GUISizer::stack
	List_1_t357561476 * ___stack_2;

public:
	inline static int32_t get_offset_of_WIDTH_0() { return static_cast<int32_t>(offsetof(GUISizer_t1539903446_StaticFields, ___WIDTH_0)); }
	inline float get_WIDTH_0() const { return ___WIDTH_0; }
	inline float* get_address_of_WIDTH_0() { return &___WIDTH_0; }
	inline void set_WIDTH_0(float value)
	{
		___WIDTH_0 = value;
	}

	inline static int32_t get_offset_of_HEIGHT_1() { return static_cast<int32_t>(offsetof(GUISizer_t1539903446_StaticFields, ___HEIGHT_1)); }
	inline float get_HEIGHT_1() const { return ___HEIGHT_1; }
	inline float* get_address_of_HEIGHT_1() { return &___HEIGHT_1; }
	inline void set_HEIGHT_1(float value)
	{
		___HEIGHT_1 = value;
	}

	inline static int32_t get_offset_of_stack_2() { return static_cast<int32_t>(offsetof(GUISizer_t1539903446_StaticFields, ___stack_2)); }
	inline List_1_t357561476 * get_stack_2() const { return ___stack_2; }
	inline List_1_t357561476 ** get_address_of_stack_2() { return &___stack_2; }
	inline void set_stack_2(List_1_t357561476 * value)
	{
		___stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___stack_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISIZER_T1539903446_H
#ifndef VCAMEXTRASTATE_T818142455_H
#define VCAMEXTRASTATE_T818142455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCollider/VcamExtraState
struct  VcamExtraState_t818142455  : public RuntimeObject
{
public:
	// Cinemachine.Utility.GaussianWindow1D_Vector3 Cinemachine.CinemachineCollider/VcamExtraState::filter
	GaussianWindow1D_Vector3_t1200723002 * ___filter_0;
	// System.Int32 Cinemachine.CinemachineCollider/VcamExtraState::colliderDisplacementDecay
	int32_t ___colliderDisplacementDecay_1;
	// System.Single Cinemachine.CinemachineCollider/VcamExtraState::colliderDisplacement
	float ___colliderDisplacement_2;
	// System.Boolean Cinemachine.CinemachineCollider/VcamExtraState::targetObscured
	bool ___targetObscured_3;
	// System.Single Cinemachine.CinemachineCollider/VcamExtraState::curbResistance
	float ___curbResistance_4;
	// System.Single Cinemachine.CinemachineCollider/VcamExtraState::feelerDistance
	float ___feelerDistance_5;
	// Cinemachine.CinemachineCollider/CompiledCurbFeeler[] Cinemachine.CinemachineCollider/VcamExtraState::curbFeelers
	CompiledCurbFeelerU5BU5D_t4033313670* ___curbFeelers_6;

public:
	inline static int32_t get_offset_of_filter_0() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___filter_0)); }
	inline GaussianWindow1D_Vector3_t1200723002 * get_filter_0() const { return ___filter_0; }
	inline GaussianWindow1D_Vector3_t1200723002 ** get_address_of_filter_0() { return &___filter_0; }
	inline void set_filter_0(GaussianWindow1D_Vector3_t1200723002 * value)
	{
		___filter_0 = value;
		Il2CppCodeGenWriteBarrier((&___filter_0), value);
	}

	inline static int32_t get_offset_of_colliderDisplacementDecay_1() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___colliderDisplacementDecay_1)); }
	inline int32_t get_colliderDisplacementDecay_1() const { return ___colliderDisplacementDecay_1; }
	inline int32_t* get_address_of_colliderDisplacementDecay_1() { return &___colliderDisplacementDecay_1; }
	inline void set_colliderDisplacementDecay_1(int32_t value)
	{
		___colliderDisplacementDecay_1 = value;
	}

	inline static int32_t get_offset_of_colliderDisplacement_2() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___colliderDisplacement_2)); }
	inline float get_colliderDisplacement_2() const { return ___colliderDisplacement_2; }
	inline float* get_address_of_colliderDisplacement_2() { return &___colliderDisplacement_2; }
	inline void set_colliderDisplacement_2(float value)
	{
		___colliderDisplacement_2 = value;
	}

	inline static int32_t get_offset_of_targetObscured_3() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___targetObscured_3)); }
	inline bool get_targetObscured_3() const { return ___targetObscured_3; }
	inline bool* get_address_of_targetObscured_3() { return &___targetObscured_3; }
	inline void set_targetObscured_3(bool value)
	{
		___targetObscured_3 = value;
	}

	inline static int32_t get_offset_of_curbResistance_4() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___curbResistance_4)); }
	inline float get_curbResistance_4() const { return ___curbResistance_4; }
	inline float* get_address_of_curbResistance_4() { return &___curbResistance_4; }
	inline void set_curbResistance_4(float value)
	{
		___curbResistance_4 = value;
	}

	inline static int32_t get_offset_of_feelerDistance_5() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___feelerDistance_5)); }
	inline float get_feelerDistance_5() const { return ___feelerDistance_5; }
	inline float* get_address_of_feelerDistance_5() { return &___feelerDistance_5; }
	inline void set_feelerDistance_5(float value)
	{
		___feelerDistance_5 = value;
	}

	inline static int32_t get_offset_of_curbFeelers_6() { return static_cast<int32_t>(offsetof(VcamExtraState_t818142455, ___curbFeelers_6)); }
	inline CompiledCurbFeelerU5BU5D_t4033313670* get_curbFeelers_6() const { return ___curbFeelers_6; }
	inline CompiledCurbFeelerU5BU5D_t4033313670** get_address_of_curbFeelers_6() { return &___curbFeelers_6; }
	inline void set_curbFeelers_6(CompiledCurbFeelerU5BU5D_t4033313670* value)
	{
		___curbFeelers_6 = value;
		Il2CppCodeGenWriteBarrier((&___curbFeelers_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VCAMEXTRASTATE_T818142455_H
#ifndef OVERRIDESTACKFRAME_T2350841338_H
#define OVERRIDESTACKFRAME_T2350841338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBrain/OverrideStackFrame
struct  OverrideStackFrame_t2350841338  : public RuntimeObject
{
public:
	// System.Int32 Cinemachine.CinemachineBrain/OverrideStackFrame::id
	int32_t ___id_0;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain/OverrideStackFrame::camera
	RuntimeObject* ___camera_1;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineBrain/OverrideStackFrame::blend
	CinemachineBlend_t2146485918 * ___blend_2;
	// System.Single Cinemachine.CinemachineBrain/OverrideStackFrame::deltaTime
	float ___deltaTime_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(OverrideStackFrame_t2350841338, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(OverrideStackFrame_t2350841338, ___camera_1)); }
	inline RuntimeObject* get_camera_1() const { return ___camera_1; }
	inline RuntimeObject** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(RuntimeObject* value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}

	inline static int32_t get_offset_of_blend_2() { return static_cast<int32_t>(offsetof(OverrideStackFrame_t2350841338, ___blend_2)); }
	inline CinemachineBlend_t2146485918 * get_blend_2() const { return ___blend_2; }
	inline CinemachineBlend_t2146485918 ** get_address_of_blend_2() { return &___blend_2; }
	inline void set_blend_2(CinemachineBlend_t2146485918 * value)
	{
		___blend_2 = value;
		Il2CppCodeGenWriteBarrier((&___blend_2), value);
	}

	inline static int32_t get_offset_of_deltaTime_3() { return static_cast<int32_t>(offsetof(OverrideStackFrame_t2350841338, ___deltaTime_3)); }
	inline float get_deltaTime_3() const { return ___deltaTime_3; }
	inline float* get_address_of_deltaTime_3() { return &___deltaTime_3; }
	inline void set_deltaTime_3(float value)
	{
		___deltaTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERRIDESTACKFRAME_T2350841338_H
#ifndef PARENTHASH_T1020415291_H
#define PARENTHASH_T1020415291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera/ParentHash
struct  ParentHash_t1020415291 
{
public:
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/ParentHash::m_Hash
	int32_t ___m_Hash_0;
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/ParentHash::m_ParentHash
	int32_t ___m_ParentHash_1;

public:
	inline static int32_t get_offset_of_m_Hash_0() { return static_cast<int32_t>(offsetof(ParentHash_t1020415291, ___m_Hash_0)); }
	inline int32_t get_m_Hash_0() const { return ___m_Hash_0; }
	inline int32_t* get_address_of_m_Hash_0() { return &___m_Hash_0; }
	inline void set_m_Hash_0(int32_t value)
	{
		___m_Hash_0 = value;
	}

	inline static int32_t get_offset_of_m_ParentHash_1() { return static_cast<int32_t>(offsetof(ParentHash_t1020415291, ___m_ParentHash_1)); }
	inline int32_t get_m_ParentHash_1() const { return ___m_ParentHash_1; }
	inline int32_t* get_address_of_m_ParentHash_1() { return &___m_ParentHash_1; }
	inline void set_m_ParentHash_1(int32_t value)
	{
		___m_ParentHash_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTHASH_T1020415291_H
#ifndef INSTRUCTION_T3855846037_H
#define INSTRUCTION_T3855846037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera/Instruction
struct  Instruction_t3855846037 
{
public:
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/Instruction::m_FullHash
	int32_t ___m_FullHash_0;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineStateDrivenCamera/Instruction::m_VirtualCamera
	CinemachineVirtualCameraBase_t2557508663 * ___m_VirtualCamera_1;
	// System.Single Cinemachine.CinemachineStateDrivenCamera/Instruction::m_ActivateAfter
	float ___m_ActivateAfter_2;
	// System.Single Cinemachine.CinemachineStateDrivenCamera/Instruction::m_MinDuration
	float ___m_MinDuration_3;

public:
	inline static int32_t get_offset_of_m_FullHash_0() { return static_cast<int32_t>(offsetof(Instruction_t3855846037, ___m_FullHash_0)); }
	inline int32_t get_m_FullHash_0() const { return ___m_FullHash_0; }
	inline int32_t* get_address_of_m_FullHash_0() { return &___m_FullHash_0; }
	inline void set_m_FullHash_0(int32_t value)
	{
		___m_FullHash_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualCamera_1() { return static_cast<int32_t>(offsetof(Instruction_t3855846037, ___m_VirtualCamera_1)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_m_VirtualCamera_1() const { return ___m_VirtualCamera_1; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_m_VirtualCamera_1() { return &___m_VirtualCamera_1; }
	inline void set_m_VirtualCamera_1(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___m_VirtualCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualCamera_1), value);
	}

	inline static int32_t get_offset_of_m_ActivateAfter_2() { return static_cast<int32_t>(offsetof(Instruction_t3855846037, ___m_ActivateAfter_2)); }
	inline float get_m_ActivateAfter_2() const { return ___m_ActivateAfter_2; }
	inline float* get_address_of_m_ActivateAfter_2() { return &___m_ActivateAfter_2; }
	inline void set_m_ActivateAfter_2(float value)
	{
		___m_ActivateAfter_2 = value;
	}

	inline static int32_t get_offset_of_m_MinDuration_3() { return static_cast<int32_t>(offsetof(Instruction_t3855846037, ___m_MinDuration_3)); }
	inline float get_m_MinDuration_3() const { return ___m_MinDuration_3; }
	inline float* get_address_of_m_MinDuration_3() { return &___m_MinDuration_3; }
	inline void set_m_MinDuration_3(float value)
	{
		___m_MinDuration_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineStateDrivenCamera/Instruction
struct Instruction_t3855846037_marshaled_pinvoke
{
	int32_t ___m_FullHash_0;
	CinemachineVirtualCameraBase_t2557508663 * ___m_VirtualCamera_1;
	float ___m_ActivateAfter_2;
	float ___m_MinDuration_3;
};
// Native definition for COM marshalling of Cinemachine.CinemachineStateDrivenCamera/Instruction
struct Instruction_t3855846037_marshaled_com
{
	int32_t ___m_FullHash_0;
	CinemachineVirtualCameraBase_t2557508663 * ___m_VirtualCamera_1;
	float ___m_ActivateAfter_2;
	float ___m_MinDuration_3;
};
#endif // INSTRUCTION_T3855846037_H
#ifndef AXISSTATE_T2333558477_H
#define AXISSTATE_T2333558477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/AxisState
struct  AxisState_t2333558477 
{
public:
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::Value
	float ___Value_0;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::m_MaxSpeed
	float ___m_MaxSpeed_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::m_AccelTime
	float ___m_AccelTime_2;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::m_DecelTime
	float ___m_DecelTime_3;
	// System.String Cinemachine.CinemachineOrbitalTransposer/AxisState::m_InputAxisName
	String_t* ___m_InputAxisName_4;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::m_InputAxisValue
	float ___m_InputAxisValue_5;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::mCurrentSpeed
	float ___mCurrentSpeed_6;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::mMinValue
	float ___mMinValue_7;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/AxisState::mMaxValue
	float ___mMaxValue_8;
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer/AxisState::mWrapAround
	bool ___mWrapAround_9;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___Value_0)); }
	inline float get_Value_0() const { return ___Value_0; }
	inline float* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(float value)
	{
		___Value_0 = value;
	}

	inline static int32_t get_offset_of_m_MaxSpeed_1() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___m_MaxSpeed_1)); }
	inline float get_m_MaxSpeed_1() const { return ___m_MaxSpeed_1; }
	inline float* get_address_of_m_MaxSpeed_1() { return &___m_MaxSpeed_1; }
	inline void set_m_MaxSpeed_1(float value)
	{
		___m_MaxSpeed_1 = value;
	}

	inline static int32_t get_offset_of_m_AccelTime_2() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___m_AccelTime_2)); }
	inline float get_m_AccelTime_2() const { return ___m_AccelTime_2; }
	inline float* get_address_of_m_AccelTime_2() { return &___m_AccelTime_2; }
	inline void set_m_AccelTime_2(float value)
	{
		___m_AccelTime_2 = value;
	}

	inline static int32_t get_offset_of_m_DecelTime_3() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___m_DecelTime_3)); }
	inline float get_m_DecelTime_3() const { return ___m_DecelTime_3; }
	inline float* get_address_of_m_DecelTime_3() { return &___m_DecelTime_3; }
	inline void set_m_DecelTime_3(float value)
	{
		___m_DecelTime_3 = value;
	}

	inline static int32_t get_offset_of_m_InputAxisName_4() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___m_InputAxisName_4)); }
	inline String_t* get_m_InputAxisName_4() const { return ___m_InputAxisName_4; }
	inline String_t** get_address_of_m_InputAxisName_4() { return &___m_InputAxisName_4; }
	inline void set_m_InputAxisName_4(String_t* value)
	{
		___m_InputAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputAxisName_4), value);
	}

	inline static int32_t get_offset_of_m_InputAxisValue_5() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___m_InputAxisValue_5)); }
	inline float get_m_InputAxisValue_5() const { return ___m_InputAxisValue_5; }
	inline float* get_address_of_m_InputAxisValue_5() { return &___m_InputAxisValue_5; }
	inline void set_m_InputAxisValue_5(float value)
	{
		___m_InputAxisValue_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentSpeed_6() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___mCurrentSpeed_6)); }
	inline float get_mCurrentSpeed_6() const { return ___mCurrentSpeed_6; }
	inline float* get_address_of_mCurrentSpeed_6() { return &___mCurrentSpeed_6; }
	inline void set_mCurrentSpeed_6(float value)
	{
		___mCurrentSpeed_6 = value;
	}

	inline static int32_t get_offset_of_mMinValue_7() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___mMinValue_7)); }
	inline float get_mMinValue_7() const { return ___mMinValue_7; }
	inline float* get_address_of_mMinValue_7() { return &___mMinValue_7; }
	inline void set_mMinValue_7(float value)
	{
		___mMinValue_7 = value;
	}

	inline static int32_t get_offset_of_mMaxValue_8() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___mMaxValue_8)); }
	inline float get_mMaxValue_8() const { return ___mMaxValue_8; }
	inline float* get_address_of_mMaxValue_8() { return &___mMaxValue_8; }
	inline void set_mMaxValue_8(float value)
	{
		___mMaxValue_8 = value;
	}

	inline static int32_t get_offset_of_mWrapAround_9() { return static_cast<int32_t>(offsetof(AxisState_t2333558477, ___mWrapAround_9)); }
	inline bool get_mWrapAround_9() const { return ___mWrapAround_9; }
	inline bool* get_address_of_mWrapAround_9() { return &___mWrapAround_9; }
	inline void set_mWrapAround_9(bool value)
	{
		___mWrapAround_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineOrbitalTransposer/AxisState
struct AxisState_t2333558477_marshaled_pinvoke
{
	float ___Value_0;
	float ___m_MaxSpeed_1;
	float ___m_AccelTime_2;
	float ___m_DecelTime_3;
	char* ___m_InputAxisName_4;
	float ___m_InputAxisValue_5;
	float ___mCurrentSpeed_6;
	float ___mMinValue_7;
	float ___mMaxValue_8;
	int32_t ___mWrapAround_9;
};
// Native definition for COM marshalling of Cinemachine.CinemachineOrbitalTransposer/AxisState
struct AxisState_t2333558477_marshaled_com
{
	float ___Value_0;
	float ___m_MaxSpeed_1;
	float ___m_AccelTime_2;
	float ___m_DecelTime_3;
	Il2CppChar* ___m_InputAxisName_4;
	float ___m_InputAxisValue_5;
	float ___mCurrentSpeed_6;
	float ___mMinValue_7;
	float ___mMaxValue_8;
	int32_t ___mWrapAround_9;
};
#endif // AXISSTATE_T2333558477_H
#ifndef PAIR_T2832965450_H
#define PAIR_T2832965450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineClearShot/Pair
struct  Pair_t2832965450 
{
public:
	// System.Int32 Cinemachine.CinemachineClearShot/Pair::a
	int32_t ___a_0;
	// System.Single Cinemachine.CinemachineClearShot/Pair::b
	float ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(Pair_t2832965450, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(Pair_t2832965450, ___b_1)); }
	inline float get_b_1() const { return ___b_1; }
	inline float* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(float value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIR_T2832965450_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef TARGET_T2477784577_H
#define TARGET_T2477784577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/Target
struct  Target_t2477784577 
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineTargetGroup/Target::target
	Transform_t362059596 * ___target_0;
	// System.Single Cinemachine.CinemachineTargetGroup/Target::weight
	float ___weight_1;
	// System.Single Cinemachine.CinemachineTargetGroup/Target::radius
	float ___radius_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Target_t2477784577, ___target_0)); }
	inline Transform_t362059596 * get_target_0() const { return ___target_0; }
	inline Transform_t362059596 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t362059596 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Target_t2477784577, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(Target_t2477784577, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineTargetGroup/Target
struct Target_t2477784577_marshaled_pinvoke
{
	Transform_t362059596 * ___target_0;
	float ___weight_1;
	float ___radius_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineTargetGroup/Target
struct Target_t2477784577_marshaled_com
{
	Transform_t362059596 * ___target_0;
	float ___weight_1;
	float ___radius_2;
};
#endif // TARGET_T2477784577_H
#ifndef AUTODOLLY_T756678745_H
#define AUTODOLLY_T756678745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly/AutoDolly
struct  AutoDolly_t756678745 
{
public:
	// System.Boolean Cinemachine.CinemachineTrackedDolly/AutoDolly::m_Enabled
	bool ___m_Enabled_0;
	// System.Int32 Cinemachine.CinemachineTrackedDolly/AutoDolly::m_SearchRadius
	int32_t ___m_SearchRadius_1;
	// System.Int32 Cinemachine.CinemachineTrackedDolly/AutoDolly::m_StepsPerSegment
	int32_t ___m_StepsPerSegment_2;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(AutoDolly_t756678745, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}

	inline static int32_t get_offset_of_m_SearchRadius_1() { return static_cast<int32_t>(offsetof(AutoDolly_t756678745, ___m_SearchRadius_1)); }
	inline int32_t get_m_SearchRadius_1() const { return ___m_SearchRadius_1; }
	inline int32_t* get_address_of_m_SearchRadius_1() { return &___m_SearchRadius_1; }
	inline void set_m_SearchRadius_1(int32_t value)
	{
		___m_SearchRadius_1 = value;
	}

	inline static int32_t get_offset_of_m_StepsPerSegment_2() { return static_cast<int32_t>(offsetof(AutoDolly_t756678745, ___m_StepsPerSegment_2)); }
	inline int32_t get_m_StepsPerSegment_2() const { return ___m_StepsPerSegment_2; }
	inline int32_t* get_address_of_m_StepsPerSegment_2() { return &___m_StepsPerSegment_2; }
	inline void set_m_StepsPerSegment_2(int32_t value)
	{
		___m_StepsPerSegment_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineTrackedDolly/AutoDolly
struct AutoDolly_t756678745_marshaled_pinvoke
{
	int32_t ___m_Enabled_0;
	int32_t ___m_SearchRadius_1;
	int32_t ___m_StepsPerSegment_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineTrackedDolly/AutoDolly
struct AutoDolly_t756678745_marshaled_com
{
	int32_t ___m_Enabled_0;
	int32_t ___m_SearchRadius_1;
	int32_t ___m_StepsPerSegment_2;
};
#endif // AUTODOLLY_T756678745_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef LENSSETTINGS_T1505430577_H
#define LENSSETTINGS_T1505430577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.LensSettings
struct  LensSettings_t1505430577 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// System.Boolean Cinemachine.LensSettings::<Orthographic>k__BackingField
	bool ___U3COrthographicU3Ek__BackingField_6;
	// System.Single Cinemachine.LensSettings::<Aspect>k__BackingField
	float ___U3CAspectU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_U3COrthographicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___U3COrthographicU3Ek__BackingField_6)); }
	inline bool get_U3COrthographicU3Ek__BackingField_6() const { return ___U3COrthographicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3COrthographicU3Ek__BackingField_6() { return &___U3COrthographicU3Ek__BackingField_6; }
	inline void set_U3COrthographicU3Ek__BackingField_6(bool value)
	{
		___U3COrthographicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAspectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577, ___U3CAspectU3Ek__BackingField_7)); }
	inline float get_U3CAspectU3Ek__BackingField_7() const { return ___U3CAspectU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAspectU3Ek__BackingField_7() { return &___U3CAspectU3Ek__BackingField_7; }
	inline void set_U3CAspectU3Ek__BackingField_7(float value)
	{
		___U3CAspectU3Ek__BackingField_7 = value;
	}
};

struct LensSettings_t1505430577_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_t1505430577  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_t1505430577_StaticFields, ___Default_0)); }
	inline LensSettings_t1505430577  get_Default_0() const { return ___Default_0; }
	inline LensSettings_t1505430577 * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_t1505430577  value)
	{
		___Default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_t1505430577_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_t1505430577_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
#endif // LENSSETTINGS_T1505430577_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef LAYERMASK_T246267875_H
#define LAYERMASK_T246267875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t246267875 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t246267875, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T246267875_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef HEADINGDERIVATIONMODE_T2507807219_H
#define HEADINGDERIVATIONMODE_T2507807219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/Recentering/HeadingDerivationMode
struct  HeadingDerivationMode_t2507807219 
{
public:
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Recentering/HeadingDerivationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HeadingDerivationMode_t2507807219, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADINGDERIVATIONMODE_T2507807219_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef COMPILEDCURBFEELER_T1909537055_H
#define COMPILEDCURBFEELER_T1909537055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct  CompiledCurbFeeler_t1909537055 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider/CompiledCurbFeeler::LocalVector
	Vector3_t1986933152  ___LocalVector_0;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::RayDistance
	float ___RayDistance_1;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::DampingConstant
	float ___DampingConstant_2;
	// System.Boolean Cinemachine.CinemachineCollider/CompiledCurbFeeler::IsHit
	bool ___IsHit_3;
	// System.Single Cinemachine.CinemachineCollider/CompiledCurbFeeler::HitDistance
	float ___HitDistance_4;

public:
	inline static int32_t get_offset_of_LocalVector_0() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___LocalVector_0)); }
	inline Vector3_t1986933152  get_LocalVector_0() const { return ___LocalVector_0; }
	inline Vector3_t1986933152 * get_address_of_LocalVector_0() { return &___LocalVector_0; }
	inline void set_LocalVector_0(Vector3_t1986933152  value)
	{
		___LocalVector_0 = value;
	}

	inline static int32_t get_offset_of_RayDistance_1() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___RayDistance_1)); }
	inline float get_RayDistance_1() const { return ___RayDistance_1; }
	inline float* get_address_of_RayDistance_1() { return &___RayDistance_1; }
	inline void set_RayDistance_1(float value)
	{
		___RayDistance_1 = value;
	}

	inline static int32_t get_offset_of_DampingConstant_2() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___DampingConstant_2)); }
	inline float get_DampingConstant_2() const { return ___DampingConstant_2; }
	inline float* get_address_of_DampingConstant_2() { return &___DampingConstant_2; }
	inline void set_DampingConstant_2(float value)
	{
		___DampingConstant_2 = value;
	}

	inline static int32_t get_offset_of_IsHit_3() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___IsHit_3)); }
	inline bool get_IsHit_3() const { return ___IsHit_3; }
	inline bool* get_address_of_IsHit_3() { return &___IsHit_3; }
	inline void set_IsHit_3(bool value)
	{
		___IsHit_3 = value;
	}

	inline static int32_t get_offset_of_HitDistance_4() { return static_cast<int32_t>(offsetof(CompiledCurbFeeler_t1909537055, ___HitDistance_4)); }
	inline float get_HitDistance_4() const { return ___HitDistance_4; }
	inline float* get_address_of_HitDistance_4() { return &___HitDistance_4; }
	inline void set_HitDistance_4(float value)
	{
		___HitDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct CompiledCurbFeeler_t1909537055_marshaled_pinvoke
{
	Vector3_t1986933152  ___LocalVector_0;
	float ___RayDistance_1;
	float ___DampingConstant_2;
	int32_t ___IsHit_3;
	float ___HitDistance_4;
};
// Native definition for COM marshalling of Cinemachine.CinemachineCollider/CompiledCurbFeeler
struct CompiledCurbFeeler_t1909537055_marshaled_com
{
	Vector3_t1986933152  ___LocalVector_0;
	float ___RayDistance_1;
	float ___DampingConstant_2;
	int32_t ___IsHit_3;
	float ___HitDistance_4;
};
#endif // COMPILEDCURBFEELER_T1909537055_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef APPEARANCE_T955816971_H
#define APPEARANCE_T955816971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePath/Appearance
struct  Appearance_t955816971  : public RuntimeObject
{
public:
	// UnityEngine.Color Cinemachine.CinemachinePath/Appearance::pathColor
	Color_t2582018970  ___pathColor_0;
	// UnityEngine.Color Cinemachine.CinemachinePath/Appearance::inactivePathColor
	Color_t2582018970  ___inactivePathColor_1;
	// UnityEngine.Color Cinemachine.CinemachinePath/Appearance::handleColor
	Color_t2582018970  ___handleColor_2;
	// System.Single Cinemachine.CinemachinePath/Appearance::width
	float ___width_3;
	// System.Int32 Cinemachine.CinemachinePath/Appearance::steps
	int32_t ___steps_4;

public:
	inline static int32_t get_offset_of_pathColor_0() { return static_cast<int32_t>(offsetof(Appearance_t955816971, ___pathColor_0)); }
	inline Color_t2582018970  get_pathColor_0() const { return ___pathColor_0; }
	inline Color_t2582018970 * get_address_of_pathColor_0() { return &___pathColor_0; }
	inline void set_pathColor_0(Color_t2582018970  value)
	{
		___pathColor_0 = value;
	}

	inline static int32_t get_offset_of_inactivePathColor_1() { return static_cast<int32_t>(offsetof(Appearance_t955816971, ___inactivePathColor_1)); }
	inline Color_t2582018970  get_inactivePathColor_1() const { return ___inactivePathColor_1; }
	inline Color_t2582018970 * get_address_of_inactivePathColor_1() { return &___inactivePathColor_1; }
	inline void set_inactivePathColor_1(Color_t2582018970  value)
	{
		___inactivePathColor_1 = value;
	}

	inline static int32_t get_offset_of_handleColor_2() { return static_cast<int32_t>(offsetof(Appearance_t955816971, ___handleColor_2)); }
	inline Color_t2582018970  get_handleColor_2() const { return ___handleColor_2; }
	inline Color_t2582018970 * get_address_of_handleColor_2() { return &___handleColor_2; }
	inline void set_handleColor_2(Color_t2582018970  value)
	{
		___handleColor_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(Appearance_t955816971, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_steps_4() { return static_cast<int32_t>(offsetof(Appearance_t955816971, ___steps_4)); }
	inline int32_t get_steps_4() const { return ___steps_4; }
	inline int32_t* get_address_of_steps_4() { return &___steps_4; }
	inline void set_steps_4(int32_t value)
	{
		___steps_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEARANCE_T955816971_H
#ifndef WAYPOINT_T552772035_H
#define WAYPOINT_T552772035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePath/Waypoint
struct  Waypoint_t552772035  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachinePath/Waypoint::position
	Vector3_t1986933152  ___position_0;
	// UnityEngine.Vector3 Cinemachine.CinemachinePath/Waypoint::tangent
	Vector3_t1986933152  ___tangent_1;
	// System.Single Cinemachine.CinemachinePath/Waypoint::roll
	float ___roll_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Waypoint_t552772035, ___position_0)); }
	inline Vector3_t1986933152  get_position_0() const { return ___position_0; }
	inline Vector3_t1986933152 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t1986933152  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_tangent_1() { return static_cast<int32_t>(offsetof(Waypoint_t552772035, ___tangent_1)); }
	inline Vector3_t1986933152  get_tangent_1() const { return ___tangent_1; }
	inline Vector3_t1986933152 * get_address_of_tangent_1() { return &___tangent_1; }
	inline void set_tangent_1(Vector3_t1986933152  value)
	{
		___tangent_1 = value;
	}

	inline static int32_t get_offset_of_roll_2() { return static_cast<int32_t>(offsetof(Waypoint_t552772035, ___roll_2)); }
	inline float get_roll_2() const { return ___roll_2; }
	inline float* get_address_of_roll_2() { return &___roll_2; }
	inline void set_roll_2(float value)
	{
		___roll_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T552772035_H
#ifndef STYLE_T4107124224_H
#define STYLE_T4107124224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition/Style
struct  Style_t4107124224 
{
public:
	// System.Int32 Cinemachine.CinemachineBlendDefinition/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t4107124224, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T4107124224_H
#ifndef FILTERMODE_T2934613156_H
#define FILTERMODE_T2934613156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t2934613156 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t2934613156, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T2934613156_H
#ifndef BOUNDS_T3570137099_H
#define BOUNDS_T3570137099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3570137099 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t1986933152  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t1986933152  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Center_0)); }
	inline Vector3_t1986933152  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t1986933152 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t1986933152  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Extents_1)); }
	inline Vector3_t1986933152  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t1986933152 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t1986933152  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3570137099_H
#ifndef CAMERASTATE_T382403230_H
#define CAMERASTATE_T382403230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CameraState
struct  CameraState_t382403230 
{
public:
	union
	{
		struct
		{
			// Cinemachine.LensSettings Cinemachine.CameraState::<Lens>k__BackingField
			LensSettings_t1505430577  ___U3CLensU3Ek__BackingField_0;
			// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceUp>k__BackingField
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceLookAt>k__BackingField
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			// UnityEngine.Vector3 Cinemachine.CameraState::<RawPosition>k__BackingField
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			// UnityEngine.Quaternion Cinemachine.CameraState::<RawOrientation>k__BackingField
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			// System.Single Cinemachine.CameraState::<ShotQuality>k__BackingField
			float ___U3CShotQualityU3Ek__BackingField_6;
			// UnityEngine.Vector3 Cinemachine.CameraState::<PositionCorrection>k__BackingField
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			// UnityEngine.Quaternion Cinemachine.CameraState::<OrientationCorrection>k__BackingField
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CLensU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CLensU3Ek__BackingField_0)); }
	inline LensSettings_t1505430577  get_U3CLensU3Ek__BackingField_0() const { return ___U3CLensU3Ek__BackingField_0; }
	inline LensSettings_t1505430577 * get_address_of_U3CLensU3Ek__BackingField_0() { return &___U3CLensU3Ek__BackingField_0; }
	inline void set_U3CLensU3Ek__BackingField_0(LensSettings_t1505430577  value)
	{
		___U3CLensU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceUpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CReferenceUpU3Ek__BackingField_1)); }
	inline Vector3_t1986933152  get_U3CReferenceUpU3Ek__BackingField_1() const { return ___U3CReferenceUpU3Ek__BackingField_1; }
	inline Vector3_t1986933152 * get_address_of_U3CReferenceUpU3Ek__BackingField_1() { return &___U3CReferenceUpU3Ek__BackingField_1; }
	inline void set_U3CReferenceUpU3Ek__BackingField_1(Vector3_t1986933152  value)
	{
		___U3CReferenceUpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CReferenceLookAtU3Ek__BackingField_2)); }
	inline Vector3_t1986933152  get_U3CReferenceLookAtU3Ek__BackingField_2() const { return ___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline Vector3_t1986933152 * get_address_of_U3CReferenceLookAtU3Ek__BackingField_2() { return &___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline void set_U3CReferenceLookAtU3Ek__BackingField_2(Vector3_t1986933152  value)
	{
		___U3CReferenceLookAtU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRawPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CRawPositionU3Ek__BackingField_4)); }
	inline Vector3_t1986933152  get_U3CRawPositionU3Ek__BackingField_4() const { return ___U3CRawPositionU3Ek__BackingField_4; }
	inline Vector3_t1986933152 * get_address_of_U3CRawPositionU3Ek__BackingField_4() { return &___U3CRawPositionU3Ek__BackingField_4; }
	inline void set_U3CRawPositionU3Ek__BackingField_4(Vector3_t1986933152  value)
	{
		___U3CRawPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawOrientationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CRawOrientationU3Ek__BackingField_5)); }
	inline Quaternion_t704191599  get_U3CRawOrientationU3Ek__BackingField_5() const { return ___U3CRawOrientationU3Ek__BackingField_5; }
	inline Quaternion_t704191599 * get_address_of_U3CRawOrientationU3Ek__BackingField_5() { return &___U3CRawOrientationU3Ek__BackingField_5; }
	inline void set_U3CRawOrientationU3Ek__BackingField_5(Quaternion_t704191599  value)
	{
		___U3CRawOrientationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CShotQualityU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CShotQualityU3Ek__BackingField_6)); }
	inline float get_U3CShotQualityU3Ek__BackingField_6() const { return ___U3CShotQualityU3Ek__BackingField_6; }
	inline float* get_address_of_U3CShotQualityU3Ek__BackingField_6() { return &___U3CShotQualityU3Ek__BackingField_6; }
	inline void set_U3CShotQualityU3Ek__BackingField_6(float value)
	{
		___U3CShotQualityU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPositionCorrectionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3CPositionCorrectionU3Ek__BackingField_7)); }
	inline Vector3_t1986933152  get_U3CPositionCorrectionU3Ek__BackingField_7() const { return ___U3CPositionCorrectionU3Ek__BackingField_7; }
	inline Vector3_t1986933152 * get_address_of_U3CPositionCorrectionU3Ek__BackingField_7() { return &___U3CPositionCorrectionU3Ek__BackingField_7; }
	inline void set_U3CPositionCorrectionU3Ek__BackingField_7(Vector3_t1986933152  value)
	{
		___U3CPositionCorrectionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3COrientationCorrectionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraState_t382403230, ___U3COrientationCorrectionU3Ek__BackingField_8)); }
	inline Quaternion_t704191599  get_U3COrientationCorrectionU3Ek__BackingField_8() const { return ___U3COrientationCorrectionU3Ek__BackingField_8; }
	inline Quaternion_t704191599 * get_address_of_U3COrientationCorrectionU3Ek__BackingField_8() { return &___U3COrientationCorrectionU3Ek__BackingField_8; }
	inline void set_U3COrientationCorrectionU3Ek__BackingField_8(Quaternion_t704191599  value)
	{
		___U3COrientationCorrectionU3Ek__BackingField_8 = value;
	}
};

struct CameraState_t382403230_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CameraState::kNoPoint
	Vector3_t1986933152  ___kNoPoint_3;

public:
	inline static int32_t get_offset_of_kNoPoint_3() { return static_cast<int32_t>(offsetof(CameraState_t382403230_StaticFields, ___kNoPoint_3)); }
	inline Vector3_t1986933152  get_kNoPoint_3() const { return ___kNoPoint_3; }
	inline Vector3_t1986933152 * get_address_of_kNoPoint_3() { return &___kNoPoint_3; }
	inline void set_kNoPoint_3(Vector3_t1986933152  value)
	{
		___kNoPoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CameraState
struct CameraState_t382403230_marshaled_pinvoke
{
	union
	{
		struct
		{
			LensSettings_t1505430577_marshaled_pinvoke ___U3CLensU3Ek__BackingField_0;
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			float ___U3CShotQualityU3Ek__BackingField_6;
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};
};
// Native definition for COM marshalling of Cinemachine.CameraState
struct CameraState_t382403230_marshaled_com
{
	union
	{
		struct
		{
			LensSettings_t1505430577_marshaled_com ___U3CLensU3Ek__BackingField_0;
			Vector3_t1986933152  ___U3CReferenceUpU3Ek__BackingField_1;
			Vector3_t1986933152  ___U3CReferenceLookAtU3Ek__BackingField_2;
			Vector3_t1986933152  ___U3CRawPositionU3Ek__BackingField_4;
			Quaternion_t704191599  ___U3CRawOrientationU3Ek__BackingField_5;
			float ___U3CShotQualityU3Ek__BackingField_6;
			Vector3_t1986933152  ___U3CPositionCorrectionU3Ek__BackingField_7;
			Quaternion_t704191599  ___U3COrientationCorrectionU3Ek__BackingField_8;
		};
		uint8_t CameraState_t382403230__padding[1];
	};
};
#endif // CAMERASTATE_T382403230_H
#ifndef TRANSPOSEROFFSETTYPE_T2447451693_H
#define TRANSPOSEROFFSETTYPE_T2447451693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTransposer/TransposerOffsetType
struct  TransposerOffsetType_t2447451693 
{
public:
	// System.Int32 Cinemachine.CinemachineTransposer/TransposerOffsetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransposerOffsetType_t2447451693, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPOSEROFFSETTYPE_T2447451693_H
#ifndef POSITIONMODE_T4058225743_H
#define POSITIONMODE_T4058225743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/PositionMode
struct  PositionMode_t4058225743 
{
public:
	// System.Int32 Cinemachine.CinemachineTargetGroup/PositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionMode_t4058225743, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_T4058225743_H
#ifndef ROTATIONMODE_T2604816688_H
#define ROTATIONMODE_T2604816688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/RotationMode
struct  RotationMode_t2604816688 
{
public:
	// System.Int32 Cinemachine.CinemachineTargetGroup/RotationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationMode_t2604816688, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONMODE_T2604816688_H
#ifndef RENDERTEXTUREFORMAT_T2838254077_H
#define RENDERTEXTUREFORMAT_T2838254077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2838254077 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2838254077, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T2838254077_H
#ifndef CAMERAUPMODE_T3416712389_H
#define CAMERAUPMODE_T3416712389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly/CameraUpMode
struct  CameraUpMode_t3416712389 
{
public:
	// System.Int32 Cinemachine.CinemachineTrackedDolly/CameraUpMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraUpMode_t3416712389, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAUPMODE_T3416712389_H
#ifndef ADJUSTMENTMODE_T1416763142_H
#define ADJUSTMENTMODE_T1416763142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer/AdjustmentMode
struct  AdjustmentMode_t1416763142 
{
public:
	// System.Int32 Cinemachine.CinemachineGroupComposer/AdjustmentMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdjustmentMode_t1416763142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTMENTMODE_T1416763142_H
#ifndef DAMPINGSTYLE_T1040084675_H
#define DAMPINGSTYLE_T1040084675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/DampingStyle
struct  DampingStyle_t1040084675 
{
public:
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/DampingStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DampingStyle_t1040084675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMPINGSTYLE_T1040084675_H
#ifndef ITEM_T3005053575_H
#define ITEM_T3005053575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item
struct  Item_t3005053575 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::velocity
	Vector3_t1986933152  ___velocity_0;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::weight
	float ___weight_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::time
	float ___time_2;

public:
	inline static int32_t get_offset_of_velocity_0() { return static_cast<int32_t>(offsetof(Item_t3005053575, ___velocity_0)); }
	inline Vector3_t1986933152  get_velocity_0() const { return ___velocity_0; }
	inline Vector3_t1986933152 * get_address_of_velocity_0() { return &___velocity_0; }
	inline void set_velocity_0(Vector3_t1986933152  value)
	{
		___velocity_0 = value;
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Item_t3005053575, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Item_t3005053575, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T3005053575_H
#ifndef HEADINGTRACKER_T2287318816_H
#define HEADINGTRACKER_T2287318816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker
struct  HeadingTracker_t2287318816  : public RuntimeObject
{
public:
	// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item[] Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mHistory
	ItemU5BU5D_t1301757822* ___mHistory_0;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mTop
	int32_t ___mTop_1;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mBottom
	int32_t ___mBottom_2;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mCount
	int32_t ___mCount_3;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mHeadingSum
	Vector3_t1986933152  ___mHeadingSum_4;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mWeightSum
	float ___mWeightSum_5;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mWeightTime
	float ___mWeightTime_6;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mLastGoodHeading
	Vector3_t1986933152  ___mLastGoodHeading_7;

public:
	inline static int32_t get_offset_of_mHistory_0() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mHistory_0)); }
	inline ItemU5BU5D_t1301757822* get_mHistory_0() const { return ___mHistory_0; }
	inline ItemU5BU5D_t1301757822** get_address_of_mHistory_0() { return &___mHistory_0; }
	inline void set_mHistory_0(ItemU5BU5D_t1301757822* value)
	{
		___mHistory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHistory_0), value);
	}

	inline static int32_t get_offset_of_mTop_1() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mTop_1)); }
	inline int32_t get_mTop_1() const { return ___mTop_1; }
	inline int32_t* get_address_of_mTop_1() { return &___mTop_1; }
	inline void set_mTop_1(int32_t value)
	{
		___mTop_1 = value;
	}

	inline static int32_t get_offset_of_mBottom_2() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mBottom_2)); }
	inline int32_t get_mBottom_2() const { return ___mBottom_2; }
	inline int32_t* get_address_of_mBottom_2() { return &___mBottom_2; }
	inline void set_mBottom_2(int32_t value)
	{
		___mBottom_2 = value;
	}

	inline static int32_t get_offset_of_mCount_3() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mCount_3)); }
	inline int32_t get_mCount_3() const { return ___mCount_3; }
	inline int32_t* get_address_of_mCount_3() { return &___mCount_3; }
	inline void set_mCount_3(int32_t value)
	{
		___mCount_3 = value;
	}

	inline static int32_t get_offset_of_mHeadingSum_4() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mHeadingSum_4)); }
	inline Vector3_t1986933152  get_mHeadingSum_4() const { return ___mHeadingSum_4; }
	inline Vector3_t1986933152 * get_address_of_mHeadingSum_4() { return &___mHeadingSum_4; }
	inline void set_mHeadingSum_4(Vector3_t1986933152  value)
	{
		___mHeadingSum_4 = value;
	}

	inline static int32_t get_offset_of_mWeightSum_5() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mWeightSum_5)); }
	inline float get_mWeightSum_5() const { return ___mWeightSum_5; }
	inline float* get_address_of_mWeightSum_5() { return &___mWeightSum_5; }
	inline void set_mWeightSum_5(float value)
	{
		___mWeightSum_5 = value;
	}

	inline static int32_t get_offset_of_mWeightTime_6() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mWeightTime_6)); }
	inline float get_mWeightTime_6() const { return ___mWeightTime_6; }
	inline float* get_address_of_mWeightTime_6() { return &___mWeightTime_6; }
	inline void set_mWeightTime_6(float value)
	{
		___mWeightTime_6 = value;
	}

	inline static int32_t get_offset_of_mLastGoodHeading_7() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816, ___mLastGoodHeading_7)); }
	inline Vector3_t1986933152  get_mLastGoodHeading_7() const { return ___mLastGoodHeading_7; }
	inline Vector3_t1986933152 * get_address_of_mLastGoodHeading_7() { return &___mLastGoodHeading_7; }
	inline void set_mLastGoodHeading_7(Vector3_t1986933152  value)
	{
		___mLastGoodHeading_7 = value;
	}
};

struct HeadingTracker_t2287318816_StaticFields
{
public:
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mDecayExponent
	float ___mDecayExponent_8;

public:
	inline static int32_t get_offset_of_mDecayExponent_8() { return static_cast<int32_t>(offsetof(HeadingTracker_t2287318816_StaticFields, ___mDecayExponent_8)); }
	inline float get_mDecayExponent_8() const { return ___mDecayExponent_8; }
	inline float* get_address_of_mDecayExponent_8() { return &___mDecayExponent_8; }
	inline void set_mDecayExponent_8(float value)
	{
		___mDecayExponent_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADINGTRACKER_T2287318816_H
#ifndef FRAMINGMODE_T2203084160_H
#define FRAMINGMODE_T2203084160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer/FramingMode
struct  FramingMode_t2203084160 
{
public:
	// System.Int32 Cinemachine.CinemachineGroupComposer/FramingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FramingMode_t2203084160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMINGMODE_T2203084160_H
#ifndef DOFRESOLUTION_T1205495178_H
#define DOFRESOLUTION_T1205495178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
struct  DofResolution_t1205495178 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofResolution_t1205495178, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFRESOLUTION_T1205495178_H
#ifndef ADAPTIVETEXSIZE_T2410714347_H
#define ADAPTIVETEXSIZE_T2410714347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
struct  AdaptiveTexSize_t2410714347 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdaptiveTexSize_t2410714347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVETEXSIZE_T2410714347_H
#ifndef UPDATEMETHOD_T3260873211_H
#define UPDATEMETHOD_T3260873211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBrain/UpdateMethod
struct  UpdateMethod_t3260873211 
{
public:
	// System.Int32 Cinemachine.CinemachineBrain/UpdateMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateMethod_t3260873211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMETHOD_T3260873211_H
#ifndef TILTSHIFTQUALITY_T3606954146_H
#define TILTSHIFTQUALITY_T3606954146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
struct  TiltShiftQuality_t3606954146 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TiltShiftQuality_t3606954146, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFTQUALITY_T3606954146_H
#ifndef TILTSHIFTMODE_T1874920922_H
#define TILTSHIFTMODE_T1874920922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
struct  TiltShiftMode_t1874920922 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TiltShiftMode_t1874920922, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFTMODE_T1874920922_H
#ifndef SHAFTSSCREENBLENDMODE_T1906873445_H
#define SHAFTSSCREENBLENDMODE_T1906873445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
struct  ShaftsScreenBlendMode_t1906873445 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShaftsScreenBlendMode_t1906873445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAFTSSCREENBLENDMODE_T1906873445_H
#ifndef SUNSHAFTSRESOLUTION_T3422901908_H
#define SUNSHAFTSRESOLUTION_T3422901908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
struct  SunShaftsResolution_t3422901908 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SunShaftsResolution_t3422901908, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUNSHAFTSRESOLUTION_T3422901908_H
#ifndef SSAOSAMPLES_T2028001761_H
#define SSAOSAMPLES_T2028001761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
struct  SSAOSamples_t2028001761 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSAOSamples_t2028001761, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSAOSAMPLES_T2028001761_H
#ifndef ABERRATIONMODE_T2592060798_H
#define ABERRATIONMODE_T2592060798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
struct  AberrationMode_t2592060798 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AberrationMode_t2592060798, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABERRATIONMODE_T2592060798_H
#ifndef BODYDATA_T2328741553_H
#define BODYDATA_T2328741553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BodyData
struct  BodyData_t2328741553  : public RuntimeObject
{
public:
	// System.Int32 BodyData::hairStyle
	int32_t ___hairStyle_0;
	// UnityEngine.Color BodyData::hairColour
	Color_t2582018970  ___hairColour_1;
	// UnityEngine.Color BodyData::skinColour
	Color_t2582018970  ___skinColour_2;
	// UnityEngine.Color BodyData::clothColour
	Color_t2582018970  ___clothColour_3;
	// UnityEngine.Color BodyData::eyeColour
	Color_t2582018970  ___eyeColour_4;
	// UnityEngine.Color BodyData::trimColour
	Color_t2582018970  ___trimColour_5;
	// System.Int32 BodyData::backPackNumber
	int32_t ___backPackNumber_6;
	// System.Int32 BodyData::bodyType
	int32_t ___bodyType_7;
	// UnityEngine.Texture2D BodyData::trimTexture
	Texture2D_t3063074017 * ___trimTexture_8;

public:
	inline static int32_t get_offset_of_hairStyle_0() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___hairStyle_0)); }
	inline int32_t get_hairStyle_0() const { return ___hairStyle_0; }
	inline int32_t* get_address_of_hairStyle_0() { return &___hairStyle_0; }
	inline void set_hairStyle_0(int32_t value)
	{
		___hairStyle_0 = value;
	}

	inline static int32_t get_offset_of_hairColour_1() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___hairColour_1)); }
	inline Color_t2582018970  get_hairColour_1() const { return ___hairColour_1; }
	inline Color_t2582018970 * get_address_of_hairColour_1() { return &___hairColour_1; }
	inline void set_hairColour_1(Color_t2582018970  value)
	{
		___hairColour_1 = value;
	}

	inline static int32_t get_offset_of_skinColour_2() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___skinColour_2)); }
	inline Color_t2582018970  get_skinColour_2() const { return ___skinColour_2; }
	inline Color_t2582018970 * get_address_of_skinColour_2() { return &___skinColour_2; }
	inline void set_skinColour_2(Color_t2582018970  value)
	{
		___skinColour_2 = value;
	}

	inline static int32_t get_offset_of_clothColour_3() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___clothColour_3)); }
	inline Color_t2582018970  get_clothColour_3() const { return ___clothColour_3; }
	inline Color_t2582018970 * get_address_of_clothColour_3() { return &___clothColour_3; }
	inline void set_clothColour_3(Color_t2582018970  value)
	{
		___clothColour_3 = value;
	}

	inline static int32_t get_offset_of_eyeColour_4() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___eyeColour_4)); }
	inline Color_t2582018970  get_eyeColour_4() const { return ___eyeColour_4; }
	inline Color_t2582018970 * get_address_of_eyeColour_4() { return &___eyeColour_4; }
	inline void set_eyeColour_4(Color_t2582018970  value)
	{
		___eyeColour_4 = value;
	}

	inline static int32_t get_offset_of_trimColour_5() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___trimColour_5)); }
	inline Color_t2582018970  get_trimColour_5() const { return ___trimColour_5; }
	inline Color_t2582018970 * get_address_of_trimColour_5() { return &___trimColour_5; }
	inline void set_trimColour_5(Color_t2582018970  value)
	{
		___trimColour_5 = value;
	}

	inline static int32_t get_offset_of_backPackNumber_6() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___backPackNumber_6)); }
	inline int32_t get_backPackNumber_6() const { return ___backPackNumber_6; }
	inline int32_t* get_address_of_backPackNumber_6() { return &___backPackNumber_6; }
	inline void set_backPackNumber_6(int32_t value)
	{
		___backPackNumber_6 = value;
	}

	inline static int32_t get_offset_of_bodyType_7() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___bodyType_7)); }
	inline int32_t get_bodyType_7() const { return ___bodyType_7; }
	inline int32_t* get_address_of_bodyType_7() { return &___bodyType_7; }
	inline void set_bodyType_7(int32_t value)
	{
		___bodyType_7 = value;
	}

	inline static int32_t get_offset_of_trimTexture_8() { return static_cast<int32_t>(offsetof(BodyData_t2328741553, ___trimTexture_8)); }
	inline Texture2D_t3063074017 * get_trimTexture_8() const { return ___trimTexture_8; }
	inline Texture2D_t3063074017 ** get_address_of_trimTexture_8() { return &___trimTexture_8; }
	inline void set_trimTexture_8(Texture2D_t3063074017 * value)
	{
		___trimTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___trimTexture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BODYDATA_T2328741553_H
#ifndef OVERLAYBLENDMODE_T3093108320_H
#define OVERLAYBLENDMODE_T3093108320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
struct  OverlayBlendMode_t3093108320 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OverlayBlendMode_t3093108320, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYBLENDMODE_T3093108320_H
#ifndef TONEMAPPERTYPE_T118564868_H
#define TONEMAPPERTYPE_T118564868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
struct  TonemapperType_t118564868 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TonemapperType_t118564868, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPERTYPE_T118564868_H
#ifndef ECUSTOMISATIONBUTTON_T3006556935_H
#define ECUSTOMISATIONBUTTON_T3006556935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ECustomisationButton
struct  ECustomisationButton_t3006556935 
{
public:
	// System.Int32 ECustomisationButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ECustomisationButton_t3006556935, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECUSTOMISATIONBUTTON_T3006556935_H
#ifndef EDGEDETECTMODE_T3688097253_H
#define EDGEDETECTMODE_T3688097253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
struct  EdgeDetectMode_t3688097253 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EdgeDetectMode_t3688097253, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEDETECTMODE_T3688097253_H
#ifndef BOKEHDESTINATION_T2246052814_H
#define BOKEHDESTINATION_T2246052814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
struct  BokehDestination_t2246052814 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BokehDestination_t2246052814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOKEHDESTINATION_T2246052814_H
#ifndef DOFBLURRINESS_T1437194827_H
#define DOFBLURRINESS_T1437194827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
struct  DofBlurriness_t1437194827 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofBlurriness_t1437194827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFBLURRINESS_T1437194827_H
#ifndef CINEMACHINEBLENDDEFINITION_T3981438518_H
#define CINEMACHINEBLENDDEFINITION_T3981438518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition
struct  CinemachineBlendDefinition_t3981438518 
{
public:
	// Cinemachine.CinemachineBlendDefinition/Style Cinemachine.CinemachineBlendDefinition::m_Style
	int32_t ___m_Style_0;
	// System.Single Cinemachine.CinemachineBlendDefinition::m_Time
	float ___m_Time_1;

public:
	inline static int32_t get_offset_of_m_Style_0() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3981438518, ___m_Style_0)); }
	inline int32_t get_m_Style_0() const { return ___m_Style_0; }
	inline int32_t* get_address_of_m_Style_0() { return &___m_Style_0; }
	inline void set_m_Style_0(int32_t value)
	{
		___m_Style_0 = value;
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3981438518, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDDEFINITION_T3981438518_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef BLENDSOURCEVIRTUALCAMERA_T3437615881_H
#define BLENDSOURCEVIRTUALCAMERA_T3437615881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.BlendSourceVirtualCamera
struct  BlendSourceVirtualCamera_t3437615881  : public RuntimeObject
{
public:
	// Cinemachine.CinemachineBlend Cinemachine.BlendSourceVirtualCamera::<Blend>k__BackingField
	CinemachineBlend_t2146485918 * ___U3CBlendU3Ek__BackingField_0;
	// System.Int32 Cinemachine.BlendSourceVirtualCamera::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;
	// UnityEngine.Transform Cinemachine.BlendSourceVirtualCamera::<LookAt>k__BackingField
	Transform_t362059596 * ___U3CLookAtU3Ek__BackingField_2;
	// UnityEngine.Transform Cinemachine.BlendSourceVirtualCamera::<Follow>k__BackingField
	Transform_t362059596 * ___U3CFollowU3Ek__BackingField_3;
	// Cinemachine.CameraState Cinemachine.BlendSourceVirtualCamera::<State>k__BackingField
	CameraState_t382403230  ___U3CStateU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBlendU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BlendSourceVirtualCamera_t3437615881, ___U3CBlendU3Ek__BackingField_0)); }
	inline CinemachineBlend_t2146485918 * get_U3CBlendU3Ek__BackingField_0() const { return ___U3CBlendU3Ek__BackingField_0; }
	inline CinemachineBlend_t2146485918 ** get_address_of_U3CBlendU3Ek__BackingField_0() { return &___U3CBlendU3Ek__BackingField_0; }
	inline void set_U3CBlendU3Ek__BackingField_0(CinemachineBlend_t2146485918 * value)
	{
		___U3CBlendU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBlendU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BlendSourceVirtualCamera_t3437615881, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BlendSourceVirtualCamera_t3437615881, ___U3CLookAtU3Ek__BackingField_2)); }
	inline Transform_t362059596 * get_U3CLookAtU3Ek__BackingField_2() const { return ___U3CLookAtU3Ek__BackingField_2; }
	inline Transform_t362059596 ** get_address_of_U3CLookAtU3Ek__BackingField_2() { return &___U3CLookAtU3Ek__BackingField_2; }
	inline void set_U3CLookAtU3Ek__BackingField_2(Transform_t362059596 * value)
	{
		___U3CLookAtU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLookAtU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BlendSourceVirtualCamera_t3437615881, ___U3CFollowU3Ek__BackingField_3)); }
	inline Transform_t362059596 * get_U3CFollowU3Ek__BackingField_3() const { return ___U3CFollowU3Ek__BackingField_3; }
	inline Transform_t362059596 ** get_address_of_U3CFollowU3Ek__BackingField_3() { return &___U3CFollowU3Ek__BackingField_3; }
	inline void set_U3CFollowU3Ek__BackingField_3(Transform_t362059596 * value)
	{
		___U3CFollowU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFollowU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BlendSourceVirtualCamera_t3437615881, ___U3CStateU3Ek__BackingField_4)); }
	inline CameraState_t382403230  get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline CameraState_t382403230 * get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(CameraState_t382403230  value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSOURCEVIRTUALCAMERA_T3437615881_H
#ifndef STATICPOINTVIRTUALCAMERA_T3747499034_H
#define STATICPOINTVIRTUALCAMERA_T3747499034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.StaticPointVirtualCamera
struct  StaticPointVirtualCamera_t3747499034  : public RuntimeObject
{
public:
	// System.String Cinemachine.StaticPointVirtualCamera::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Cinemachine.StaticPointVirtualCamera::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;
	// UnityEngine.Transform Cinemachine.StaticPointVirtualCamera::<LookAt>k__BackingField
	Transform_t362059596 * ___U3CLookAtU3Ek__BackingField_2;
	// UnityEngine.Transform Cinemachine.StaticPointVirtualCamera::<Follow>k__BackingField
	Transform_t362059596 * ___U3CFollowU3Ek__BackingField_3;
	// Cinemachine.CameraState Cinemachine.StaticPointVirtualCamera::<State>k__BackingField
	CameraState_t382403230  ___U3CStateU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StaticPointVirtualCamera_t3747499034, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StaticPointVirtualCamera_t3747499034, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StaticPointVirtualCamera_t3747499034, ___U3CLookAtU3Ek__BackingField_2)); }
	inline Transform_t362059596 * get_U3CLookAtU3Ek__BackingField_2() const { return ___U3CLookAtU3Ek__BackingField_2; }
	inline Transform_t362059596 ** get_address_of_U3CLookAtU3Ek__BackingField_2() { return &___U3CLookAtU3Ek__BackingField_2; }
	inline void set_U3CLookAtU3Ek__BackingField_2(Transform_t362059596 * value)
	{
		___U3CLookAtU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLookAtU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StaticPointVirtualCamera_t3747499034, ___U3CFollowU3Ek__BackingField_3)); }
	inline Transform_t362059596 * get_U3CFollowU3Ek__BackingField_3() const { return ___U3CFollowU3Ek__BackingField_3; }
	inline Transform_t362059596 ** get_address_of_U3CFollowU3Ek__BackingField_3() { return &___U3CFollowU3Ek__BackingField_3; }
	inline void set_U3CFollowU3Ek__BackingField_3(Transform_t362059596 * value)
	{
		___U3CFollowU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFollowU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StaticPointVirtualCamera_t3747499034, ___U3CStateU3Ek__BackingField_4)); }
	inline CameraState_t382403230  get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline CameraState_t382403230 * get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(CameraState_t382403230  value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICPOINTVIRTUALCAMERA_T3747499034_H
#ifndef RECENTERING_T75128244_H
#define RECENTERING_T75128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/Recentering
struct  Recentering_t75128244 
{
public:
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer/Recentering::m_enabled
	bool ___m_enabled_0;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/Recentering::m_RecenterWaitTime
	float ___m_RecenterWaitTime_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/Recentering::m_RecenteringTime
	float ___m_RecenteringTime_2;
	// Cinemachine.CinemachineOrbitalTransposer/Recentering/HeadingDerivationMode Cinemachine.CinemachineOrbitalTransposer/Recentering::m_HeadingDefinition
	int32_t ___m_HeadingDefinition_3;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Recentering::m_VelocityFilterStrength
	int32_t ___m_VelocityFilterStrength_4;

public:
	inline static int32_t get_offset_of_m_enabled_0() { return static_cast<int32_t>(offsetof(Recentering_t75128244, ___m_enabled_0)); }
	inline bool get_m_enabled_0() const { return ___m_enabled_0; }
	inline bool* get_address_of_m_enabled_0() { return &___m_enabled_0; }
	inline void set_m_enabled_0(bool value)
	{
		___m_enabled_0 = value;
	}

	inline static int32_t get_offset_of_m_RecenterWaitTime_1() { return static_cast<int32_t>(offsetof(Recentering_t75128244, ___m_RecenterWaitTime_1)); }
	inline float get_m_RecenterWaitTime_1() const { return ___m_RecenterWaitTime_1; }
	inline float* get_address_of_m_RecenterWaitTime_1() { return &___m_RecenterWaitTime_1; }
	inline void set_m_RecenterWaitTime_1(float value)
	{
		___m_RecenterWaitTime_1 = value;
	}

	inline static int32_t get_offset_of_m_RecenteringTime_2() { return static_cast<int32_t>(offsetof(Recentering_t75128244, ___m_RecenteringTime_2)); }
	inline float get_m_RecenteringTime_2() const { return ___m_RecenteringTime_2; }
	inline float* get_address_of_m_RecenteringTime_2() { return &___m_RecenteringTime_2; }
	inline void set_m_RecenteringTime_2(float value)
	{
		___m_RecenteringTime_2 = value;
	}

	inline static int32_t get_offset_of_m_HeadingDefinition_3() { return static_cast<int32_t>(offsetof(Recentering_t75128244, ___m_HeadingDefinition_3)); }
	inline int32_t get_m_HeadingDefinition_3() const { return ___m_HeadingDefinition_3; }
	inline int32_t* get_address_of_m_HeadingDefinition_3() { return &___m_HeadingDefinition_3; }
	inline void set_m_HeadingDefinition_3(int32_t value)
	{
		___m_HeadingDefinition_3 = value;
	}

	inline static int32_t get_offset_of_m_VelocityFilterStrength_4() { return static_cast<int32_t>(offsetof(Recentering_t75128244, ___m_VelocityFilterStrength_4)); }
	inline int32_t get_m_VelocityFilterStrength_4() const { return ___m_VelocityFilterStrength_4; }
	inline int32_t* get_address_of_m_VelocityFilterStrength_4() { return &___m_VelocityFilterStrength_4; }
	inline void set_m_VelocityFilterStrength_4(int32_t value)
	{
		___m_VelocityFilterStrength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineOrbitalTransposer/Recentering
struct Recentering_t75128244_marshaled_pinvoke
{
	int32_t ___m_enabled_0;
	float ___m_RecenterWaitTime_1;
	float ___m_RecenteringTime_2;
	int32_t ___m_HeadingDefinition_3;
	int32_t ___m_VelocityFilterStrength_4;
};
// Native definition for COM marshalling of Cinemachine.CinemachineOrbitalTransposer/Recentering
struct Recentering_t75128244_marshaled_com
{
	int32_t ___m_enabled_0;
	float ___m_RecenterWaitTime_1;
	float ___m_RecenteringTime_2;
	int32_t ___m_HeadingDefinition_3;
	int32_t ___m_VelocityFilterStrength_4;
};
#endif // RECENTERING_T75128244_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef DESTROYPIPELINEDELEGATE_T926775217_H
#define DESTROYPIPELINEDELEGATE_T926775217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct  DestroyPipelineDelegate_t926775217  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYPIPELINEDELEGATE_T926775217_H
#ifndef POSTFXHANDLERDELEGATE_T2862114331_H
#define POSTFXHANDLERDELEGATE_T2862114331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBrain/PostFXHandlerDelegate
struct  PostFXHandlerDelegate_t2862114331  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTFXHANDLERDELEGATE_T2862114331_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef CINEMACHINEBLENDERSETTINGS_T865950197_H
#define CINEMACHINEBLENDERSETTINGS_T865950197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlenderSettings
struct  CinemachineBlenderSettings_t865950197  : public ScriptableObject_t1804531341
{
public:
	// Cinemachine.CinemachineBlenderSettings/CustomBlend[] Cinemachine.CinemachineBlenderSettings::m_CustomBlends
	CustomBlendU5BU5D_t4089921897* ___m_CustomBlends_2;

public:
	inline static int32_t get_offset_of_m_CustomBlends_2() { return static_cast<int32_t>(offsetof(CinemachineBlenderSettings_t865950197, ___m_CustomBlends_2)); }
	inline CustomBlendU5BU5D_t4089921897* get_m_CustomBlends_2() const { return ___m_CustomBlends_2; }
	inline CustomBlendU5BU5D_t4089921897** get_address_of_m_CustomBlends_2() { return &___m_CustomBlends_2; }
	inline void set_m_CustomBlends_2(CustomBlendU5BU5D_t4089921897* value)
	{
		___m_CustomBlends_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDERSETTINGS_T865950197_H
#ifndef CREATEPIPELINEDELEGATE_T4206421982_H
#define CREATEPIPELINEDELEGATE_T4206421982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct  CreatePipelineDelegate_t4206421982  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEPIPELINEDELEGATE_T4206421982_H
#ifndef CREATERIGDELEGATE_T931026514_H
#define CREATERIGDELEGATE_T931026514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook/CreateRigDelegate
struct  CreateRigDelegate_t931026514  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATERIGDELEGATE_T931026514_H
#ifndef DESTROYRIGDELEGATE_T2622870986_H
#define DESTROYRIGDELEGATE_T2622870986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook/DestroyRigDelegate
struct  DestroyRigDelegate_t2622870986  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYRIGDELEGATE_T2622870986_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef CINEMACHINEFOLLOWZOOM_T904737256_H
#define CINEMACHINEFOLLOWZOOM_T904737256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFollowZoom
struct  CinemachineFollowZoom_t904737256  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Cinemachine.CinemachineFollowZoom::m_Width
	float ___m_Width_2;
	// System.Single Cinemachine.CinemachineFollowZoom::m_Damping
	float ___m_Damping_3;
	// System.Single Cinemachine.CinemachineFollowZoom::m_MinFOV
	float ___m_MinFOV_4;
	// System.Single Cinemachine.CinemachineFollowZoom::m_MaxFOV
	float ___m_MaxFOV_5;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineFollowZoom::<VirtualCamera>k__BackingField
	CinemachineVirtualCameraBase_t2557508663 * ___U3CVirtualCameraU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t904737256, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Damping_3() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t904737256, ___m_Damping_3)); }
	inline float get_m_Damping_3() const { return ___m_Damping_3; }
	inline float* get_address_of_m_Damping_3() { return &___m_Damping_3; }
	inline void set_m_Damping_3(float value)
	{
		___m_Damping_3 = value;
	}

	inline static int32_t get_offset_of_m_MinFOV_4() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t904737256, ___m_MinFOV_4)); }
	inline float get_m_MinFOV_4() const { return ___m_MinFOV_4; }
	inline float* get_address_of_m_MinFOV_4() { return &___m_MinFOV_4; }
	inline void set_m_MinFOV_4(float value)
	{
		___m_MinFOV_4 = value;
	}

	inline static int32_t get_offset_of_m_MaxFOV_5() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t904737256, ___m_MaxFOV_5)); }
	inline float get_m_MaxFOV_5() const { return ___m_MaxFOV_5; }
	inline float* get_address_of_m_MaxFOV_5() { return &___m_MaxFOV_5; }
	inline void set_m_MaxFOV_5(float value)
	{
		___m_MaxFOV_5 = value;
	}

	inline static int32_t get_offset_of_U3CVirtualCameraU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t904737256, ___U3CVirtualCameraU3Ek__BackingField_6)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_U3CVirtualCameraU3Ek__BackingField_6() const { return ___U3CVirtualCameraU3Ek__BackingField_6; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_U3CVirtualCameraU3Ek__BackingField_6() { return &___U3CVirtualCameraU3Ek__BackingField_6; }
	inline void set_U3CVirtualCameraU3Ek__BackingField_6(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___U3CVirtualCameraU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVirtualCameraU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEFOLLOWZOOM_T904737256_H
#ifndef POSTEFFECTSBASE_T1831076792_H
#define POSTEFFECTSBASE_T1831076792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_t1831076792  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_2;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_3;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_4;
	// System.Collections.Generic.List`1<UnityEngine.Material> UnityStandardAssets.ImageEffects.PostEffectsBase::createdMaterials
	List_1_t1934891917 * ___createdMaterials_5;

public:
	inline static int32_t get_offset_of_supportHDRTextures_2() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___supportHDRTextures_2)); }
	inline bool get_supportHDRTextures_2() const { return ___supportHDRTextures_2; }
	inline bool* get_address_of_supportHDRTextures_2() { return &___supportHDRTextures_2; }
	inline void set_supportHDRTextures_2(bool value)
	{
		___supportHDRTextures_2 = value;
	}

	inline static int32_t get_offset_of_supportDX11_3() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___supportDX11_3)); }
	inline bool get_supportDX11_3() const { return ___supportDX11_3; }
	inline bool* get_address_of_supportDX11_3() { return &___supportDX11_3; }
	inline void set_supportDX11_3(bool value)
	{
		___supportDX11_3 = value;
	}

	inline static int32_t get_offset_of_isSupported_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___isSupported_4)); }
	inline bool get_isSupported_4() const { return ___isSupported_4; }
	inline bool* get_address_of_isSupported_4() { return &___isSupported_4; }
	inline void set_isSupported_4(bool value)
	{
		___isSupported_4 = value;
	}

	inline static int32_t get_offset_of_createdMaterials_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___createdMaterials_5)); }
	inline List_1_t1934891917 * get_createdMaterials_5() const { return ___createdMaterials_5; }
	inline List_1_t1934891917 ** get_address_of_createdMaterials_5() { return &___createdMaterials_5; }
	inline void set_createdMaterials_5(List_1_t1934891917 * value)
	{
		___createdMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_T1831076792_H
#ifndef POSTEFFECTSHELPER_T3737141539_H
#define POSTEFFECTSHELPER_T3737141539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsHelper
struct  PostEffectsHelper_t3737141539  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSHELPER_T3737141539_H
#ifndef CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#define CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCameraBase
struct  CinemachineVirtualCameraBase_t2557508663  : public MonoBehaviour_t1618594486
{
public:
	// System.Action Cinemachine.CinemachineVirtualCameraBase::CinemachineGUIDebuggerCallback
	Action_t370180854 * ___CinemachineGUIDebuggerCallback_2;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_HideHeaderInInspector
	bool ___m_HideHeaderInInspector_3;
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_t2511808107* ___m_ExcludedPropertiesInInspector_4;
	// Cinemachine.CinemachineCore/Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_t2642749565* ___m_LockStageInInspector_5;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_6;
	// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate Cinemachine.CinemachineVirtualCameraBase::OnPostPipelineStage
	OnPostPipelineStageDelegate_t938695173 * ___OnPostPipelineStage_7;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::<PreviousStateInvalid>k__BackingField
	bool ___U3CPreviousStateInvalidU3Ek__BackingField_8;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_9;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_t2557508663 * ___m_parentVcam_10;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_11;

public:
	inline static int32_t get_offset_of_CinemachineGUIDebuggerCallback_2() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___CinemachineGUIDebuggerCallback_2)); }
	inline Action_t370180854 * get_CinemachineGUIDebuggerCallback_2() const { return ___CinemachineGUIDebuggerCallback_2; }
	inline Action_t370180854 ** get_address_of_CinemachineGUIDebuggerCallback_2() { return &___CinemachineGUIDebuggerCallback_2; }
	inline void set_CinemachineGUIDebuggerCallback_2(Action_t370180854 * value)
	{
		___CinemachineGUIDebuggerCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___CinemachineGUIDebuggerCallback_2), value);
	}

	inline static int32_t get_offset_of_m_HideHeaderInInspector_3() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_HideHeaderInInspector_3)); }
	inline bool get_m_HideHeaderInInspector_3() const { return ___m_HideHeaderInInspector_3; }
	inline bool* get_address_of_m_HideHeaderInInspector_3() { return &___m_HideHeaderInInspector_3; }
	inline void set_m_HideHeaderInInspector_3(bool value)
	{
		___m_HideHeaderInInspector_3 = value;
	}

	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_ExcludedPropertiesInInspector_4)); }
	inline StringU5BU5D_t2511808107* get_m_ExcludedPropertiesInInspector_4() const { return ___m_ExcludedPropertiesInInspector_4; }
	inline StringU5BU5D_t2511808107** get_address_of_m_ExcludedPropertiesInInspector_4() { return &___m_ExcludedPropertiesInInspector_4; }
	inline void set_m_ExcludedPropertiesInInspector_4(StringU5BU5D_t2511808107* value)
	{
		___m_ExcludedPropertiesInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExcludedPropertiesInInspector_4), value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_LockStageInInspector_5)); }
	inline StageU5BU5D_t2642749565* get_m_LockStageInInspector_5() const { return ___m_LockStageInInspector_5; }
	inline StageU5BU5D_t2642749565** get_address_of_m_LockStageInInspector_5() { return &___m_LockStageInInspector_5; }
	inline void set_m_LockStageInInspector_5(StageU5BU5D_t2642749565* value)
	{
		___m_LockStageInInspector_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LockStageInInspector_5), value);
	}

	inline static int32_t get_offset_of_m_Priority_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_Priority_6)); }
	inline int32_t get_m_Priority_6() const { return ___m_Priority_6; }
	inline int32_t* get_address_of_m_Priority_6() { return &___m_Priority_6; }
	inline void set_m_Priority_6(int32_t value)
	{
		___m_Priority_6 = value;
	}

	inline static int32_t get_offset_of_OnPostPipelineStage_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___OnPostPipelineStage_7)); }
	inline OnPostPipelineStageDelegate_t938695173 * get_OnPostPipelineStage_7() const { return ___OnPostPipelineStage_7; }
	inline OnPostPipelineStageDelegate_t938695173 ** get_address_of_OnPostPipelineStage_7() { return &___OnPostPipelineStage_7; }
	inline void set_OnPostPipelineStage_7(OnPostPipelineStageDelegate_t938695173 * value)
	{
		___OnPostPipelineStage_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostPipelineStage_7), value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateInvalidU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___U3CPreviousStateInvalidU3Ek__BackingField_8)); }
	inline bool get_U3CPreviousStateInvalidU3Ek__BackingField_8() const { return ___U3CPreviousStateInvalidU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CPreviousStateInvalidU3Ek__BackingField_8() { return &___U3CPreviousStateInvalidU3Ek__BackingField_8; }
	inline void set_U3CPreviousStateInvalidU3Ek__BackingField_8(bool value)
	{
		___U3CPreviousStateInvalidU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___mSlaveStatusUpdated_9)); }
	inline bool get_mSlaveStatusUpdated_9() const { return ___mSlaveStatusUpdated_9; }
	inline bool* get_address_of_mSlaveStatusUpdated_9() { return &___mSlaveStatusUpdated_9; }
	inline void set_mSlaveStatusUpdated_9(bool value)
	{
		___mSlaveStatusUpdated_9 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_parentVcam_10)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_m_parentVcam_10() const { return ___m_parentVcam_10; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_m_parentVcam_10() { return &___m_parentVcam_10; }
	inline void set_m_parentVcam_10(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___m_parentVcam_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentVcam_10), value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t2557508663, ___m_QueuePriority_11)); }
	inline int32_t get_m_QueuePriority_11() const { return ___m_QueuePriority_11; }
	inline int32_t* get_address_of_m_QueuePriority_11() { return &___m_QueuePriority_11; }
	inline void set_m_QueuePriority_11(int32_t value)
	{
		___m_QueuePriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEVIRTUALCAMERABASE_T2557508663_H
#ifndef IMAGEEFFECTBASE_T4133330248_H
#define IMAGEEFFECTBASE_T4133330248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffectBase
struct  ImageEffectBase_t4133330248  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ImageEffectBase::shader
	Shader_t1881769421 * ___shader_2;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::m_Material
	Material_t2815264910 * ___m_Material_3;

public:
	inline static int32_t get_offset_of_shader_2() { return static_cast<int32_t>(offsetof(ImageEffectBase_t4133330248, ___shader_2)); }
	inline Shader_t1881769421 * get_shader_2() const { return ___shader_2; }
	inline Shader_t1881769421 ** get_address_of_shader_2() { return &___shader_2; }
	inline void set_shader_2(Shader_t1881769421 * value)
	{
		___shader_2 = value;
		Il2CppCodeGenWriteBarrier((&___shader_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ImageEffectBase_t4133330248, ___m_Material_3)); }
	inline Material_t2815264910 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t2815264910 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t2815264910 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTBASE_T4133330248_H
#ifndef SCREENSPACEAMBIENTOCCLUSION_T1012925760_H
#define SCREENSPACEAMBIENTOCCLUSION_T1012925760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
struct  ScreenSpaceAmbientOcclusion_t1012925760  : public MonoBehaviour_t1618594486
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Radius
	float ___m_Radius_2;
	// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SampleCount
	int32_t ___m_SampleCount_3;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionIntensity
	float ___m_OcclusionIntensity_4;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Blur
	int32_t ___m_Blur_5;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Downsampling
	int32_t ___m_Downsampling_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionAttenuation
	float ___m_OcclusionAttenuation_7;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_MinZ
	float ___m_MinZ_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOShader
	Shader_t1881769421 * ___m_SSAOShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOMaterial
	Material_t2815264910 * ___m_SSAOMaterial_10;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_RandomTexture
	Texture2D_t3063074017 * ___m_RandomTexture_11;
	// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Supported
	bool ___m_Supported_12;

public:
	inline static int32_t get_offset_of_m_Radius_2() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_Radius_2)); }
	inline float get_m_Radius_2() const { return ___m_Radius_2; }
	inline float* get_address_of_m_Radius_2() { return &___m_Radius_2; }
	inline void set_m_Radius_2(float value)
	{
		___m_Radius_2 = value;
	}

	inline static int32_t get_offset_of_m_SampleCount_3() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_SampleCount_3)); }
	inline int32_t get_m_SampleCount_3() const { return ___m_SampleCount_3; }
	inline int32_t* get_address_of_m_SampleCount_3() { return &___m_SampleCount_3; }
	inline void set_m_SampleCount_3(int32_t value)
	{
		___m_SampleCount_3 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionIntensity_4() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_OcclusionIntensity_4)); }
	inline float get_m_OcclusionIntensity_4() const { return ___m_OcclusionIntensity_4; }
	inline float* get_address_of_m_OcclusionIntensity_4() { return &___m_OcclusionIntensity_4; }
	inline void set_m_OcclusionIntensity_4(float value)
	{
		___m_OcclusionIntensity_4 = value;
	}

	inline static int32_t get_offset_of_m_Blur_5() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_Blur_5)); }
	inline int32_t get_m_Blur_5() const { return ___m_Blur_5; }
	inline int32_t* get_address_of_m_Blur_5() { return &___m_Blur_5; }
	inline void set_m_Blur_5(int32_t value)
	{
		___m_Blur_5 = value;
	}

	inline static int32_t get_offset_of_m_Downsampling_6() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_Downsampling_6)); }
	inline int32_t get_m_Downsampling_6() const { return ___m_Downsampling_6; }
	inline int32_t* get_address_of_m_Downsampling_6() { return &___m_Downsampling_6; }
	inline void set_m_Downsampling_6(int32_t value)
	{
		___m_Downsampling_6 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionAttenuation_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_OcclusionAttenuation_7)); }
	inline float get_m_OcclusionAttenuation_7() const { return ___m_OcclusionAttenuation_7; }
	inline float* get_address_of_m_OcclusionAttenuation_7() { return &___m_OcclusionAttenuation_7; }
	inline void set_m_OcclusionAttenuation_7(float value)
	{
		___m_OcclusionAttenuation_7 = value;
	}

	inline static int32_t get_offset_of_m_MinZ_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_MinZ_8)); }
	inline float get_m_MinZ_8() const { return ___m_MinZ_8; }
	inline float* get_address_of_m_MinZ_8() { return &___m_MinZ_8; }
	inline void set_m_MinZ_8(float value)
	{
		___m_MinZ_8 = value;
	}

	inline static int32_t get_offset_of_m_SSAOShader_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_SSAOShader_9)); }
	inline Shader_t1881769421 * get_m_SSAOShader_9() const { return ___m_SSAOShader_9; }
	inline Shader_t1881769421 ** get_address_of_m_SSAOShader_9() { return &___m_SSAOShader_9; }
	inline void set_m_SSAOShader_9(Shader_t1881769421 * value)
	{
		___m_SSAOShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOShader_9), value);
	}

	inline static int32_t get_offset_of_m_SSAOMaterial_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_SSAOMaterial_10)); }
	inline Material_t2815264910 * get_m_SSAOMaterial_10() const { return ___m_SSAOMaterial_10; }
	inline Material_t2815264910 ** get_address_of_m_SSAOMaterial_10() { return &___m_SSAOMaterial_10; }
	inline void set_m_SSAOMaterial_10(Material_t2815264910 * value)
	{
		___m_SSAOMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOMaterial_10), value);
	}

	inline static int32_t get_offset_of_m_RandomTexture_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_RandomTexture_11)); }
	inline Texture2D_t3063074017 * get_m_RandomTexture_11() const { return ___m_RandomTexture_11; }
	inline Texture2D_t3063074017 ** get_address_of_m_RandomTexture_11() { return &___m_RandomTexture_11; }
	inline void set_m_RandomTexture_11(Texture2D_t3063074017 * value)
	{
		___m_RandomTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RandomTexture_11), value);
	}

	inline static int32_t get_offset_of_m_Supported_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1012925760, ___m_Supported_12)); }
	inline bool get_m_Supported_12() const { return ___m_Supported_12; }
	inline bool* get_address_of_m_Supported_12() { return &___m_Supported_12; }
	inline void set_m_Supported_12(bool value)
	{
		___m_Supported_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOCCLUSION_T1012925760_H
#ifndef CINEMACHINEPATHBASE_T3902360541_H
#define CINEMACHINEPATHBASE_T3902360541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePathBase
struct  CinemachinePathBase_t3902360541  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPATHBASE_T3902360541_H
#ifndef CINEMACHINETRANSPOSER_T1461967720_H
#define CINEMACHINETRANSPOSER_T1461967720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTransposer
struct  CinemachineTransposer_t1461967720  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineTransposer::m_FollowOffset
	Vector3_t1986933152  ___m_FollowOffset_2;
	// System.Single Cinemachine.CinemachineTransposer::m_XDamping
	float ___m_XDamping_3;
	// System.Single Cinemachine.CinemachineTransposer::m_YDamping
	float ___m_YDamping_4;
	// System.Single Cinemachine.CinemachineTransposer::m_ZDamping
	float ___m_ZDamping_5;
	// Cinemachine.CinemachineTransposer/TransposerOffsetType Cinemachine.CinemachineTransposer::m_BindingMode
	int32_t ___m_BindingMode_6;
	// UnityEngine.Quaternion Cinemachine.CinemachineTransposer::m_targetOrientationOnAssign
	Quaternion_t704191599  ___m_targetOrientationOnAssign_8;
	// UnityEngine.Transform Cinemachine.CinemachineTransposer::m_previousTarget
	Transform_t362059596 * ___m_previousTarget_9;

public:
	inline static int32_t get_offset_of_m_FollowOffset_2() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_FollowOffset_2)); }
	inline Vector3_t1986933152  get_m_FollowOffset_2() const { return ___m_FollowOffset_2; }
	inline Vector3_t1986933152 * get_address_of_m_FollowOffset_2() { return &___m_FollowOffset_2; }
	inline void set_m_FollowOffset_2(Vector3_t1986933152  value)
	{
		___m_FollowOffset_2 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_3() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_XDamping_3)); }
	inline float get_m_XDamping_3() const { return ___m_XDamping_3; }
	inline float* get_address_of_m_XDamping_3() { return &___m_XDamping_3; }
	inline void set_m_XDamping_3(float value)
	{
		___m_XDamping_3 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_4() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_YDamping_4)); }
	inline float get_m_YDamping_4() const { return ___m_YDamping_4; }
	inline float* get_address_of_m_YDamping_4() { return &___m_YDamping_4; }
	inline void set_m_YDamping_4(float value)
	{
		___m_YDamping_4 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_5() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_ZDamping_5)); }
	inline float get_m_ZDamping_5() const { return ___m_ZDamping_5; }
	inline float* get_address_of_m_ZDamping_5() { return &___m_ZDamping_5; }
	inline void set_m_ZDamping_5(float value)
	{
		___m_ZDamping_5 = value;
	}

	inline static int32_t get_offset_of_m_BindingMode_6() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_BindingMode_6)); }
	inline int32_t get_m_BindingMode_6() const { return ___m_BindingMode_6; }
	inline int32_t* get_address_of_m_BindingMode_6() { return &___m_BindingMode_6; }
	inline void set_m_BindingMode_6(int32_t value)
	{
		___m_BindingMode_6 = value;
	}

	inline static int32_t get_offset_of_m_targetOrientationOnAssign_8() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_targetOrientationOnAssign_8)); }
	inline Quaternion_t704191599  get_m_targetOrientationOnAssign_8() const { return ___m_targetOrientationOnAssign_8; }
	inline Quaternion_t704191599 * get_address_of_m_targetOrientationOnAssign_8() { return &___m_targetOrientationOnAssign_8; }
	inline void set_m_targetOrientationOnAssign_8(Quaternion_t704191599  value)
	{
		___m_targetOrientationOnAssign_8 = value;
	}

	inline static int32_t get_offset_of_m_previousTarget_9() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t1461967720, ___m_previousTarget_9)); }
	inline Transform_t362059596 * get_m_previousTarget_9() const { return ___m_previousTarget_9; }
	inline Transform_t362059596 ** get_address_of_m_previousTarget_9() { return &___m_previousTarget_9; }
	inline void set_m_previousTarget_9(Transform_t362059596 * value)
	{
		___m_previousTarget_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_previousTarget_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRANSPOSER_T1461967720_H
#ifndef CINEMACHINECOLLIDER_T3890891415_H
#define CINEMACHINECOLLIDER_T3890891415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCollider
struct  CinemachineCollider_t3890891415  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.LayerMask Cinemachine.CinemachineCollider::m_CollideAgainst
	LayerMask_t246267875  ___m_CollideAgainst_2;
	// System.Boolean Cinemachine.CinemachineCollider::m_PreserveLineOfSight
	bool ___m_PreserveLineOfSight_3;
	// System.Single Cinemachine.CinemachineCollider::m_LineOfSightFeelerDistance
	float ___m_LineOfSightFeelerDistance_4;
	// System.Single Cinemachine.CinemachineCollider::m_MinimumDistanceFromTarget
	float ___m_MinimumDistanceFromTarget_5;
	// System.Single Cinemachine.CinemachineCollider::m_MinimumDistanceFromCamera
	float ___m_MinimumDistanceFromCamera_6;
	// System.Boolean Cinemachine.CinemachineCollider::m_UseCurbFeelers
	bool ___m_UseCurbFeelers_7;
	// System.Single Cinemachine.CinemachineCollider::m_CurbFeelerDistance
	float ___m_CurbFeelerDistance_8;
	// System.Single Cinemachine.CinemachineCollider::m_CurbResistance
	float ___m_CurbResistance_9;
	// System.Single Cinemachine.CinemachineCollider::m_PositionSmoothing
	float ___m_PositionSmoothing_10;
	// System.Single Cinemachine.CinemachineCollider::m_OptimalTargetDistance
	float ___m_OptimalTargetDistance_11;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineCollider::<VirtualCamera>k__BackingField
	CinemachineVirtualCameraBase_t2557508663 * ___U3CVirtualCameraU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCollider/VcamExtraState> Cinemachine.CinemachineCollider::mExtraState
	Dictionary_2_t4106222255 * ___mExtraState_18;

public:
	inline static int32_t get_offset_of_m_CollideAgainst_2() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_CollideAgainst_2)); }
	inline LayerMask_t246267875  get_m_CollideAgainst_2() const { return ___m_CollideAgainst_2; }
	inline LayerMask_t246267875 * get_address_of_m_CollideAgainst_2() { return &___m_CollideAgainst_2; }
	inline void set_m_CollideAgainst_2(LayerMask_t246267875  value)
	{
		___m_CollideAgainst_2 = value;
	}

	inline static int32_t get_offset_of_m_PreserveLineOfSight_3() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_PreserveLineOfSight_3)); }
	inline bool get_m_PreserveLineOfSight_3() const { return ___m_PreserveLineOfSight_3; }
	inline bool* get_address_of_m_PreserveLineOfSight_3() { return &___m_PreserveLineOfSight_3; }
	inline void set_m_PreserveLineOfSight_3(bool value)
	{
		___m_PreserveLineOfSight_3 = value;
	}

	inline static int32_t get_offset_of_m_LineOfSightFeelerDistance_4() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_LineOfSightFeelerDistance_4)); }
	inline float get_m_LineOfSightFeelerDistance_4() const { return ___m_LineOfSightFeelerDistance_4; }
	inline float* get_address_of_m_LineOfSightFeelerDistance_4() { return &___m_LineOfSightFeelerDistance_4; }
	inline void set_m_LineOfSightFeelerDistance_4(float value)
	{
		___m_LineOfSightFeelerDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistanceFromTarget_5() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_MinimumDistanceFromTarget_5)); }
	inline float get_m_MinimumDistanceFromTarget_5() const { return ___m_MinimumDistanceFromTarget_5; }
	inline float* get_address_of_m_MinimumDistanceFromTarget_5() { return &___m_MinimumDistanceFromTarget_5; }
	inline void set_m_MinimumDistanceFromTarget_5(float value)
	{
		___m_MinimumDistanceFromTarget_5 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistanceFromCamera_6() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_MinimumDistanceFromCamera_6)); }
	inline float get_m_MinimumDistanceFromCamera_6() const { return ___m_MinimumDistanceFromCamera_6; }
	inline float* get_address_of_m_MinimumDistanceFromCamera_6() { return &___m_MinimumDistanceFromCamera_6; }
	inline void set_m_MinimumDistanceFromCamera_6(float value)
	{
		___m_MinimumDistanceFromCamera_6 = value;
	}

	inline static int32_t get_offset_of_m_UseCurbFeelers_7() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_UseCurbFeelers_7)); }
	inline bool get_m_UseCurbFeelers_7() const { return ___m_UseCurbFeelers_7; }
	inline bool* get_address_of_m_UseCurbFeelers_7() { return &___m_UseCurbFeelers_7; }
	inline void set_m_UseCurbFeelers_7(bool value)
	{
		___m_UseCurbFeelers_7 = value;
	}

	inline static int32_t get_offset_of_m_CurbFeelerDistance_8() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_CurbFeelerDistance_8)); }
	inline float get_m_CurbFeelerDistance_8() const { return ___m_CurbFeelerDistance_8; }
	inline float* get_address_of_m_CurbFeelerDistance_8() { return &___m_CurbFeelerDistance_8; }
	inline void set_m_CurbFeelerDistance_8(float value)
	{
		___m_CurbFeelerDistance_8 = value;
	}

	inline static int32_t get_offset_of_m_CurbResistance_9() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_CurbResistance_9)); }
	inline float get_m_CurbResistance_9() const { return ___m_CurbResistance_9; }
	inline float* get_address_of_m_CurbResistance_9() { return &___m_CurbResistance_9; }
	inline void set_m_CurbResistance_9(float value)
	{
		___m_CurbResistance_9 = value;
	}

	inline static int32_t get_offset_of_m_PositionSmoothing_10() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_PositionSmoothing_10)); }
	inline float get_m_PositionSmoothing_10() const { return ___m_PositionSmoothing_10; }
	inline float* get_address_of_m_PositionSmoothing_10() { return &___m_PositionSmoothing_10; }
	inline void set_m_PositionSmoothing_10(float value)
	{
		___m_PositionSmoothing_10 = value;
	}

	inline static int32_t get_offset_of_m_OptimalTargetDistance_11() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___m_OptimalTargetDistance_11)); }
	inline float get_m_OptimalTargetDistance_11() const { return ___m_OptimalTargetDistance_11; }
	inline float* get_address_of_m_OptimalTargetDistance_11() { return &___m_OptimalTargetDistance_11; }
	inline void set_m_OptimalTargetDistance_11(float value)
	{
		___m_OptimalTargetDistance_11 = value;
	}

	inline static int32_t get_offset_of_U3CVirtualCameraU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___U3CVirtualCameraU3Ek__BackingField_12)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_U3CVirtualCameraU3Ek__BackingField_12() const { return ___U3CVirtualCameraU3Ek__BackingField_12; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_U3CVirtualCameraU3Ek__BackingField_12() { return &___U3CVirtualCameraU3Ek__BackingField_12; }
	inline void set_U3CVirtualCameraU3Ek__BackingField_12(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___U3CVirtualCameraU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVirtualCameraU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_mExtraState_18() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415, ___mExtraState_18)); }
	inline Dictionary_2_t4106222255 * get_mExtraState_18() const { return ___mExtraState_18; }
	inline Dictionary_2_t4106222255 ** get_address_of_mExtraState_18() { return &___mExtraState_18; }
	inline void set_mExtraState_18(Dictionary_2_t4106222255 * value)
	{
		___mExtraState_18 = value;
		Il2CppCodeGenWriteBarrier((&___mExtraState_18), value);
	}
};

struct CinemachineCollider_t3890891415_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider::kLocalUpRight
	Vector3_t1986933152  ___kLocalUpRight_13;
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider::kLocalUpLeft
	Vector3_t1986933152  ___kLocalUpLeft_14;
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider::kLocalDownRight
	Vector3_t1986933152  ___kLocalDownRight_15;
	// UnityEngine.Vector3 Cinemachine.CinemachineCollider::kLocalDownLeft
	Vector3_t1986933152  ___kLocalDownLeft_16;

public:
	inline static int32_t get_offset_of_kLocalUpRight_13() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415_StaticFields, ___kLocalUpRight_13)); }
	inline Vector3_t1986933152  get_kLocalUpRight_13() const { return ___kLocalUpRight_13; }
	inline Vector3_t1986933152 * get_address_of_kLocalUpRight_13() { return &___kLocalUpRight_13; }
	inline void set_kLocalUpRight_13(Vector3_t1986933152  value)
	{
		___kLocalUpRight_13 = value;
	}

	inline static int32_t get_offset_of_kLocalUpLeft_14() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415_StaticFields, ___kLocalUpLeft_14)); }
	inline Vector3_t1986933152  get_kLocalUpLeft_14() const { return ___kLocalUpLeft_14; }
	inline Vector3_t1986933152 * get_address_of_kLocalUpLeft_14() { return &___kLocalUpLeft_14; }
	inline void set_kLocalUpLeft_14(Vector3_t1986933152  value)
	{
		___kLocalUpLeft_14 = value;
	}

	inline static int32_t get_offset_of_kLocalDownRight_15() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415_StaticFields, ___kLocalDownRight_15)); }
	inline Vector3_t1986933152  get_kLocalDownRight_15() const { return ___kLocalDownRight_15; }
	inline Vector3_t1986933152 * get_address_of_kLocalDownRight_15() { return &___kLocalDownRight_15; }
	inline void set_kLocalDownRight_15(Vector3_t1986933152  value)
	{
		___kLocalDownRight_15 = value;
	}

	inline static int32_t get_offset_of_kLocalDownLeft_16() { return static_cast<int32_t>(offsetof(CinemachineCollider_t3890891415_StaticFields, ___kLocalDownLeft_16)); }
	inline Vector3_t1986933152  get_kLocalDownLeft_16() const { return ___kLocalDownLeft_16; }
	inline Vector3_t1986933152 * get_address_of_kLocalDownLeft_16() { return &___kLocalDownLeft_16; }
	inline void set_kLocalDownLeft_16(Vector3_t1986933152  value)
	{
		___kLocalDownLeft_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECOLLIDER_T3890891415_H
#ifndef NOISEANDSCRATCHES_T3793967858_H
#define NOISEANDSCRATCHES_T3793967858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.NoiseAndScratches
struct  NoiseAndScratches_t3793967858  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndScratches::monochrome
	bool ___monochrome_2;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndScratches::rgbFallback
	bool ___rgbFallback_3;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainIntensityMin
	float ___grainIntensityMin_4;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainIntensityMax
	float ___grainIntensityMax_5;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainSize
	float ___grainSize_6;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchIntensityMin
	float ___scratchIntensityMin_7;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchIntensityMax
	float ___scratchIntensityMax_8;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchFPS
	float ___scratchFPS_9;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchJitter
	float ___scratchJitter_10;
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.NoiseAndScratches::grainTexture
	Texture_t2119925672 * ___grainTexture_11;
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchTexture
	Texture_t2119925672 * ___scratchTexture_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndScratches::shaderRGB
	Shader_t1881769421 * ___shaderRGB_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndScratches::shaderYUV
	Shader_t1881769421 * ___shaderYUV_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::m_MaterialRGB
	Material_t2815264910 * ___m_MaterialRGB_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::m_MaterialYUV
	Material_t2815264910 * ___m_MaterialYUV_16;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchTimeLeft
	float ___scratchTimeLeft_17;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchX
	float ___scratchX_18;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchY
	float ___scratchY_19;

public:
	inline static int32_t get_offset_of_monochrome_2() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___monochrome_2)); }
	inline bool get_monochrome_2() const { return ___monochrome_2; }
	inline bool* get_address_of_monochrome_2() { return &___monochrome_2; }
	inline void set_monochrome_2(bool value)
	{
		___monochrome_2 = value;
	}

	inline static int32_t get_offset_of_rgbFallback_3() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___rgbFallback_3)); }
	inline bool get_rgbFallback_3() const { return ___rgbFallback_3; }
	inline bool* get_address_of_rgbFallback_3() { return &___rgbFallback_3; }
	inline void set_rgbFallback_3(bool value)
	{
		___rgbFallback_3 = value;
	}

	inline static int32_t get_offset_of_grainIntensityMin_4() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___grainIntensityMin_4)); }
	inline float get_grainIntensityMin_4() const { return ___grainIntensityMin_4; }
	inline float* get_address_of_grainIntensityMin_4() { return &___grainIntensityMin_4; }
	inline void set_grainIntensityMin_4(float value)
	{
		___grainIntensityMin_4 = value;
	}

	inline static int32_t get_offset_of_grainIntensityMax_5() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___grainIntensityMax_5)); }
	inline float get_grainIntensityMax_5() const { return ___grainIntensityMax_5; }
	inline float* get_address_of_grainIntensityMax_5() { return &___grainIntensityMax_5; }
	inline void set_grainIntensityMax_5(float value)
	{
		___grainIntensityMax_5 = value;
	}

	inline static int32_t get_offset_of_grainSize_6() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___grainSize_6)); }
	inline float get_grainSize_6() const { return ___grainSize_6; }
	inline float* get_address_of_grainSize_6() { return &___grainSize_6; }
	inline void set_grainSize_6(float value)
	{
		___grainSize_6 = value;
	}

	inline static int32_t get_offset_of_scratchIntensityMin_7() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchIntensityMin_7)); }
	inline float get_scratchIntensityMin_7() const { return ___scratchIntensityMin_7; }
	inline float* get_address_of_scratchIntensityMin_7() { return &___scratchIntensityMin_7; }
	inline void set_scratchIntensityMin_7(float value)
	{
		___scratchIntensityMin_7 = value;
	}

	inline static int32_t get_offset_of_scratchIntensityMax_8() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchIntensityMax_8)); }
	inline float get_scratchIntensityMax_8() const { return ___scratchIntensityMax_8; }
	inline float* get_address_of_scratchIntensityMax_8() { return &___scratchIntensityMax_8; }
	inline void set_scratchIntensityMax_8(float value)
	{
		___scratchIntensityMax_8 = value;
	}

	inline static int32_t get_offset_of_scratchFPS_9() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchFPS_9)); }
	inline float get_scratchFPS_9() const { return ___scratchFPS_9; }
	inline float* get_address_of_scratchFPS_9() { return &___scratchFPS_9; }
	inline void set_scratchFPS_9(float value)
	{
		___scratchFPS_9 = value;
	}

	inline static int32_t get_offset_of_scratchJitter_10() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchJitter_10)); }
	inline float get_scratchJitter_10() const { return ___scratchJitter_10; }
	inline float* get_address_of_scratchJitter_10() { return &___scratchJitter_10; }
	inline void set_scratchJitter_10(float value)
	{
		___scratchJitter_10 = value;
	}

	inline static int32_t get_offset_of_grainTexture_11() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___grainTexture_11)); }
	inline Texture_t2119925672 * get_grainTexture_11() const { return ___grainTexture_11; }
	inline Texture_t2119925672 ** get_address_of_grainTexture_11() { return &___grainTexture_11; }
	inline void set_grainTexture_11(Texture_t2119925672 * value)
	{
		___grainTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___grainTexture_11), value);
	}

	inline static int32_t get_offset_of_scratchTexture_12() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchTexture_12)); }
	inline Texture_t2119925672 * get_scratchTexture_12() const { return ___scratchTexture_12; }
	inline Texture_t2119925672 ** get_address_of_scratchTexture_12() { return &___scratchTexture_12; }
	inline void set_scratchTexture_12(Texture_t2119925672 * value)
	{
		___scratchTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___scratchTexture_12), value);
	}

	inline static int32_t get_offset_of_shaderRGB_13() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___shaderRGB_13)); }
	inline Shader_t1881769421 * get_shaderRGB_13() const { return ___shaderRGB_13; }
	inline Shader_t1881769421 ** get_address_of_shaderRGB_13() { return &___shaderRGB_13; }
	inline void set_shaderRGB_13(Shader_t1881769421 * value)
	{
		___shaderRGB_13 = value;
		Il2CppCodeGenWriteBarrier((&___shaderRGB_13), value);
	}

	inline static int32_t get_offset_of_shaderYUV_14() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___shaderYUV_14)); }
	inline Shader_t1881769421 * get_shaderYUV_14() const { return ___shaderYUV_14; }
	inline Shader_t1881769421 ** get_address_of_shaderYUV_14() { return &___shaderYUV_14; }
	inline void set_shaderYUV_14(Shader_t1881769421 * value)
	{
		___shaderYUV_14 = value;
		Il2CppCodeGenWriteBarrier((&___shaderYUV_14), value);
	}

	inline static int32_t get_offset_of_m_MaterialRGB_15() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___m_MaterialRGB_15)); }
	inline Material_t2815264910 * get_m_MaterialRGB_15() const { return ___m_MaterialRGB_15; }
	inline Material_t2815264910 ** get_address_of_m_MaterialRGB_15() { return &___m_MaterialRGB_15; }
	inline void set_m_MaterialRGB_15(Material_t2815264910 * value)
	{
		___m_MaterialRGB_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialRGB_15), value);
	}

	inline static int32_t get_offset_of_m_MaterialYUV_16() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___m_MaterialYUV_16)); }
	inline Material_t2815264910 * get_m_MaterialYUV_16() const { return ___m_MaterialYUV_16; }
	inline Material_t2815264910 ** get_address_of_m_MaterialYUV_16() { return &___m_MaterialYUV_16; }
	inline void set_m_MaterialYUV_16(Material_t2815264910 * value)
	{
		___m_MaterialYUV_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialYUV_16), value);
	}

	inline static int32_t get_offset_of_scratchTimeLeft_17() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchTimeLeft_17)); }
	inline float get_scratchTimeLeft_17() const { return ___scratchTimeLeft_17; }
	inline float* get_address_of_scratchTimeLeft_17() { return &___scratchTimeLeft_17; }
	inline void set_scratchTimeLeft_17(float value)
	{
		___scratchTimeLeft_17 = value;
	}

	inline static int32_t get_offset_of_scratchX_18() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchX_18)); }
	inline float get_scratchX_18() const { return ___scratchX_18; }
	inline float* get_address_of_scratchX_18() { return &___scratchX_18; }
	inline void set_scratchX_18(float value)
	{
		___scratchX_18 = value;
	}

	inline static int32_t get_offset_of_scratchY_19() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t3793967858, ___scratchY_19)); }
	inline float get_scratchY_19() const { return ___scratchY_19; }
	inline float* get_address_of_scratchY_19() { return &___scratchY_19; }
	inline void set_scratchY_19(float value)
	{
		___scratchY_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEANDSCRATCHES_T3793967858_H
#ifndef CINEMACHINETRACKEDDOLLY_T3518348890_H
#define CINEMACHINETRACKEDDOLLY_T3518348890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly
struct  CinemachineTrackedDolly_t3518348890  : public MonoBehaviour_t1618594486
{
public:
	// Cinemachine.CinemachinePathBase Cinemachine.CinemachineTrackedDolly::m_Path
	CinemachinePathBase_t3902360541 * ___m_Path_2;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_PathPosition
	float ___m_PathPosition_3;
	// UnityEngine.Vector3 Cinemachine.CinemachineTrackedDolly::m_PathOffset
	Vector3_t1986933152  ___m_PathOffset_4;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_XDamping
	float ___m_XDamping_5;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_YDamping
	float ___m_YDamping_6;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_ZDamping
	float ___m_ZDamping_7;
	// Cinemachine.CinemachineTrackedDolly/CameraUpMode Cinemachine.CinemachineTrackedDolly::m_CameraUp
	int32_t ___m_CameraUp_8;
	// Cinemachine.CinemachineTrackedDolly/AutoDolly Cinemachine.CinemachineTrackedDolly::m_AutoDolly
	AutoDolly_t756678745  ___m_AutoDolly_9;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_PreviousPathPosition
	float ___m_PreviousPathPosition_11;

public:
	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_Path_2)); }
	inline CinemachinePathBase_t3902360541 * get_m_Path_2() const { return ___m_Path_2; }
	inline CinemachinePathBase_t3902360541 ** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(CinemachinePathBase_t3902360541 * value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_2), value);
	}

	inline static int32_t get_offset_of_m_PathPosition_3() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_PathPosition_3)); }
	inline float get_m_PathPosition_3() const { return ___m_PathPosition_3; }
	inline float* get_address_of_m_PathPosition_3() { return &___m_PathPosition_3; }
	inline void set_m_PathPosition_3(float value)
	{
		___m_PathPosition_3 = value;
	}

	inline static int32_t get_offset_of_m_PathOffset_4() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_PathOffset_4)); }
	inline Vector3_t1986933152  get_m_PathOffset_4() const { return ___m_PathOffset_4; }
	inline Vector3_t1986933152 * get_address_of_m_PathOffset_4() { return &___m_PathOffset_4; }
	inline void set_m_PathOffset_4(Vector3_t1986933152  value)
	{
		___m_PathOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_5() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_XDamping_5)); }
	inline float get_m_XDamping_5() const { return ___m_XDamping_5; }
	inline float* get_address_of_m_XDamping_5() { return &___m_XDamping_5; }
	inline void set_m_XDamping_5(float value)
	{
		___m_XDamping_5 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_6() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_YDamping_6)); }
	inline float get_m_YDamping_6() const { return ___m_YDamping_6; }
	inline float* get_address_of_m_YDamping_6() { return &___m_YDamping_6; }
	inline void set_m_YDamping_6(float value)
	{
		___m_YDamping_6 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_7() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_ZDamping_7)); }
	inline float get_m_ZDamping_7() const { return ___m_ZDamping_7; }
	inline float* get_address_of_m_ZDamping_7() { return &___m_ZDamping_7; }
	inline void set_m_ZDamping_7(float value)
	{
		___m_ZDamping_7 = value;
	}

	inline static int32_t get_offset_of_m_CameraUp_8() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_CameraUp_8)); }
	inline int32_t get_m_CameraUp_8() const { return ___m_CameraUp_8; }
	inline int32_t* get_address_of_m_CameraUp_8() { return &___m_CameraUp_8; }
	inline void set_m_CameraUp_8(int32_t value)
	{
		___m_CameraUp_8 = value;
	}

	inline static int32_t get_offset_of_m_AutoDolly_9() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_AutoDolly_9)); }
	inline AutoDolly_t756678745  get_m_AutoDolly_9() const { return ___m_AutoDolly_9; }
	inline AutoDolly_t756678745 * get_address_of_m_AutoDolly_9() { return &___m_AutoDolly_9; }
	inline void set_m_AutoDolly_9(AutoDolly_t756678745  value)
	{
		___m_AutoDolly_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPathPosition_11() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t3518348890, ___m_PreviousPathPosition_11)); }
	inline float get_m_PreviousPathPosition_11() const { return ___m_PreviousPathPosition_11; }
	inline float* get_address_of_m_PreviousPathPosition_11() { return &___m_PreviousPathPosition_11; }
	inline void set_m_PreviousPathPosition_11(float value)
	{
		___m_PreviousPathPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRACKEDDOLLY_T3518348890_H
#ifndef BONUSSPARK_T759759043_H
#define BONUSSPARK_T759759043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BonusSpark
struct  BonusSpark_t759759043  : public MonoBehaviour_t1618594486
{
public:
	// System.String BonusSpark::myID
	String_t* ___myID_7;
	// TokenHolderLegacy BonusSpark::instanceTHL
	TokenHolderLegacy_t1380075429 * ___instanceTHL_8;

public:
	inline static int32_t get_offset_of_myID_7() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043, ___myID_7)); }
	inline String_t* get_myID_7() const { return ___myID_7; }
	inline String_t** get_address_of_myID_7() { return &___myID_7; }
	inline void set_myID_7(String_t* value)
	{
		___myID_7 = value;
		Il2CppCodeGenWriteBarrier((&___myID_7), value);
	}

	inline static int32_t get_offset_of_instanceTHL_8() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043, ___instanceTHL_8)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceTHL_8() const { return ___instanceTHL_8; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceTHL_8() { return &___instanceTHL_8; }
	inline void set_instanceTHL_8(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceTHL_8 = value;
		Il2CppCodeGenWriteBarrier((&___instanceTHL_8), value);
	}
};

struct BonusSpark_t759759043_StaticFields
{
public:
	// System.Int32[] BonusSpark::bonusSparkNums
	Int32U5BU5D_t1965588061* ___bonusSparkNums_2;
	// System.Int32[] BonusSpark::maxLevelSparks
	Int32U5BU5D_t1965588061* ___maxLevelSparks_3;
	// System.Int32 BonusSpark::numLevelSparx
	int32_t ___numLevelSparx_4;
	// UnityEngine.UI.Text BonusSpark::sparxText
	Text_t1790657652 * ___sparxText_5;
	// UnityEngine.UI.Image BonusSpark::img
	Image_t2816987602 * ___img_6;

public:
	inline static int32_t get_offset_of_bonusSparkNums_2() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043_StaticFields, ___bonusSparkNums_2)); }
	inline Int32U5BU5D_t1965588061* get_bonusSparkNums_2() const { return ___bonusSparkNums_2; }
	inline Int32U5BU5D_t1965588061** get_address_of_bonusSparkNums_2() { return &___bonusSparkNums_2; }
	inline void set_bonusSparkNums_2(Int32U5BU5D_t1965588061* value)
	{
		___bonusSparkNums_2 = value;
		Il2CppCodeGenWriteBarrier((&___bonusSparkNums_2), value);
	}

	inline static int32_t get_offset_of_maxLevelSparks_3() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043_StaticFields, ___maxLevelSparks_3)); }
	inline Int32U5BU5D_t1965588061* get_maxLevelSparks_3() const { return ___maxLevelSparks_3; }
	inline Int32U5BU5D_t1965588061** get_address_of_maxLevelSparks_3() { return &___maxLevelSparks_3; }
	inline void set_maxLevelSparks_3(Int32U5BU5D_t1965588061* value)
	{
		___maxLevelSparks_3 = value;
		Il2CppCodeGenWriteBarrier((&___maxLevelSparks_3), value);
	}

	inline static int32_t get_offset_of_numLevelSparx_4() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043_StaticFields, ___numLevelSparx_4)); }
	inline int32_t get_numLevelSparx_4() const { return ___numLevelSparx_4; }
	inline int32_t* get_address_of_numLevelSparx_4() { return &___numLevelSparx_4; }
	inline void set_numLevelSparx_4(int32_t value)
	{
		___numLevelSparx_4 = value;
	}

	inline static int32_t get_offset_of_sparxText_5() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043_StaticFields, ___sparxText_5)); }
	inline Text_t1790657652 * get_sparxText_5() const { return ___sparxText_5; }
	inline Text_t1790657652 ** get_address_of_sparxText_5() { return &___sparxText_5; }
	inline void set_sparxText_5(Text_t1790657652 * value)
	{
		___sparxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___sparxText_5), value);
	}

	inline static int32_t get_offset_of_img_6() { return static_cast<int32_t>(offsetof(BonusSpark_t759759043_StaticFields, ___img_6)); }
	inline Image_t2816987602 * get_img_6() const { return ___img_6; }
	inline Image_t2816987602 ** get_address_of_img_6() { return &___img_6; }
	inline void set_img_6(Image_t2816987602 * value)
	{
		___img_6 = value;
		Il2CppCodeGenWriteBarrier((&___img_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONUSSPARK_T759759043_H
#ifndef BODYPARTHELPER_T2433202137_H
#define BODYPARTHELPER_T2433202137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BodyPartHelper
struct  BodyPartHelper_t2433202137  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Material BodyPartHelper::bodyMaterial
	Material_t2815264910 * ___bodyMaterial_2;
	// System.Int32 BodyPartHelper::trimMaterialNumber
	int32_t ___trimMaterialNumber_3;
	// System.Int32 BodyPartHelper::hairMaterialNumber
	int32_t ___hairMaterialNumber_4;
	// System.Int32 BodyPartHelper::eyeMaterialNumber
	int32_t ___eyeMaterialNumber_5;
	// UnityEngine.Material BodyPartHelper::trimMaterial
	Material_t2815264910 * ___trimMaterial_6;
	// UnityEngine.Material BodyPartHelper::clothMaterial
	Material_t2815264910 * ___clothMaterial_7;
	// UnityEngine.Material BodyPartHelper::hairMaterial
	Material_t2815264910 * ___hairMaterial_8;
	// UnityEngine.Material BodyPartHelper::eyeMaterial
	Material_t2815264910 * ___eyeMaterial_9;
	// UnityEngine.Transform BodyPartHelper::body
	Transform_t362059596 * ___body_10;
	// UnityEngine.Transform BodyPartHelper::head
	Transform_t362059596 * ___head_11;
	// UnityEngine.Transform BodyPartHelper::hair1
	Transform_t362059596 * ___hair1_12;
	// UnityEngine.Transform BodyPartHelper::bag
	Transform_t362059596 * ___bag_13;
	// System.String BodyPartHelper::characterName
	String_t* ___characterName_14;
	// System.Boolean BodyPartHelper::setBodyDataOnRefresh
	bool ___setBodyDataOnRefresh_16;

public:
	inline static int32_t get_offset_of_bodyMaterial_2() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___bodyMaterial_2)); }
	inline Material_t2815264910 * get_bodyMaterial_2() const { return ___bodyMaterial_2; }
	inline Material_t2815264910 ** get_address_of_bodyMaterial_2() { return &___bodyMaterial_2; }
	inline void set_bodyMaterial_2(Material_t2815264910 * value)
	{
		___bodyMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___bodyMaterial_2), value);
	}

	inline static int32_t get_offset_of_trimMaterialNumber_3() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___trimMaterialNumber_3)); }
	inline int32_t get_trimMaterialNumber_3() const { return ___trimMaterialNumber_3; }
	inline int32_t* get_address_of_trimMaterialNumber_3() { return &___trimMaterialNumber_3; }
	inline void set_trimMaterialNumber_3(int32_t value)
	{
		___trimMaterialNumber_3 = value;
	}

	inline static int32_t get_offset_of_hairMaterialNumber_4() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___hairMaterialNumber_4)); }
	inline int32_t get_hairMaterialNumber_4() const { return ___hairMaterialNumber_4; }
	inline int32_t* get_address_of_hairMaterialNumber_4() { return &___hairMaterialNumber_4; }
	inline void set_hairMaterialNumber_4(int32_t value)
	{
		___hairMaterialNumber_4 = value;
	}

	inline static int32_t get_offset_of_eyeMaterialNumber_5() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___eyeMaterialNumber_5)); }
	inline int32_t get_eyeMaterialNumber_5() const { return ___eyeMaterialNumber_5; }
	inline int32_t* get_address_of_eyeMaterialNumber_5() { return &___eyeMaterialNumber_5; }
	inline void set_eyeMaterialNumber_5(int32_t value)
	{
		___eyeMaterialNumber_5 = value;
	}

	inline static int32_t get_offset_of_trimMaterial_6() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___trimMaterial_6)); }
	inline Material_t2815264910 * get_trimMaterial_6() const { return ___trimMaterial_6; }
	inline Material_t2815264910 ** get_address_of_trimMaterial_6() { return &___trimMaterial_6; }
	inline void set_trimMaterial_6(Material_t2815264910 * value)
	{
		___trimMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___trimMaterial_6), value);
	}

	inline static int32_t get_offset_of_clothMaterial_7() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___clothMaterial_7)); }
	inline Material_t2815264910 * get_clothMaterial_7() const { return ___clothMaterial_7; }
	inline Material_t2815264910 ** get_address_of_clothMaterial_7() { return &___clothMaterial_7; }
	inline void set_clothMaterial_7(Material_t2815264910 * value)
	{
		___clothMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___clothMaterial_7), value);
	}

	inline static int32_t get_offset_of_hairMaterial_8() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___hairMaterial_8)); }
	inline Material_t2815264910 * get_hairMaterial_8() const { return ___hairMaterial_8; }
	inline Material_t2815264910 ** get_address_of_hairMaterial_8() { return &___hairMaterial_8; }
	inline void set_hairMaterial_8(Material_t2815264910 * value)
	{
		___hairMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___hairMaterial_8), value);
	}

	inline static int32_t get_offset_of_eyeMaterial_9() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___eyeMaterial_9)); }
	inline Material_t2815264910 * get_eyeMaterial_9() const { return ___eyeMaterial_9; }
	inline Material_t2815264910 ** get_address_of_eyeMaterial_9() { return &___eyeMaterial_9; }
	inline void set_eyeMaterial_9(Material_t2815264910 * value)
	{
		___eyeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___eyeMaterial_9), value);
	}

	inline static int32_t get_offset_of_body_10() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___body_10)); }
	inline Transform_t362059596 * get_body_10() const { return ___body_10; }
	inline Transform_t362059596 ** get_address_of_body_10() { return &___body_10; }
	inline void set_body_10(Transform_t362059596 * value)
	{
		___body_10 = value;
		Il2CppCodeGenWriteBarrier((&___body_10), value);
	}

	inline static int32_t get_offset_of_head_11() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___head_11)); }
	inline Transform_t362059596 * get_head_11() const { return ___head_11; }
	inline Transform_t362059596 ** get_address_of_head_11() { return &___head_11; }
	inline void set_head_11(Transform_t362059596 * value)
	{
		___head_11 = value;
		Il2CppCodeGenWriteBarrier((&___head_11), value);
	}

	inline static int32_t get_offset_of_hair1_12() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___hair1_12)); }
	inline Transform_t362059596 * get_hair1_12() const { return ___hair1_12; }
	inline Transform_t362059596 ** get_address_of_hair1_12() { return &___hair1_12; }
	inline void set_hair1_12(Transform_t362059596 * value)
	{
		___hair1_12 = value;
		Il2CppCodeGenWriteBarrier((&___hair1_12), value);
	}

	inline static int32_t get_offset_of_bag_13() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___bag_13)); }
	inline Transform_t362059596 * get_bag_13() const { return ___bag_13; }
	inline Transform_t362059596 ** get_address_of_bag_13() { return &___bag_13; }
	inline void set_bag_13(Transform_t362059596 * value)
	{
		___bag_13 = value;
		Il2CppCodeGenWriteBarrier((&___bag_13), value);
	}

	inline static int32_t get_offset_of_characterName_14() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___characterName_14)); }
	inline String_t* get_characterName_14() const { return ___characterName_14; }
	inline String_t** get_address_of_characterName_14() { return &___characterName_14; }
	inline void set_characterName_14(String_t* value)
	{
		___characterName_14 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_14), value);
	}

	inline static int32_t get_offset_of_setBodyDataOnRefresh_16() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137, ___setBodyDataOnRefresh_16)); }
	inline bool get_setBodyDataOnRefresh_16() const { return ___setBodyDataOnRefresh_16; }
	inline bool* get_address_of_setBodyDataOnRefresh_16() { return &___setBodyDataOnRefresh_16; }
	inline void set_setBodyDataOnRefresh_16(bool value)
	{
		___setBodyDataOnRefresh_16 = value;
	}
};

struct BodyPartHelper_t2433202137_StaticFields
{
public:
	// System.String[] BodyPartHelper::bodyObjects
	StringU5BU5D_t2511808107* ___bodyObjects_15;

public:
	inline static int32_t get_offset_of_bodyObjects_15() { return static_cast<int32_t>(offsetof(BodyPartHelper_t2433202137_StaticFields, ___bodyObjects_15)); }
	inline StringU5BU5D_t2511808107* get_bodyObjects_15() const { return ___bodyObjects_15; }
	inline StringU5BU5D_t2511808107** get_address_of_bodyObjects_15() { return &___bodyObjects_15; }
	inline void set_bodyObjects_15(StringU5BU5D_t2511808107* value)
	{
		___bodyObjects_15 = value;
		Il2CppCodeGenWriteBarrier((&___bodyObjects_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BODYPARTHELPER_T2433202137_H
#ifndef CINEMACHINEBRAIN_T679733552_H
#define CINEMACHINEBRAIN_T679733552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBrain
struct  CinemachineBrain_t679733552  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowDebugText
	bool ___m_ShowDebugText_2;
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowCameraFrustum
	bool ___m_ShowCameraFrustum_3;
	// UnityEngine.Transform Cinemachine.CinemachineBrain::m_WorldUpOverride
	Transform_t362059596 * ___m_WorldUpOverride_4;
	// Cinemachine.CinemachineBrain/UpdateMethod Cinemachine.CinemachineBrain::m_UpdateMethod
	int32_t ___m_UpdateMethod_5;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineBrain::m_DefaultBlend
	CinemachineBlendDefinition_t3981438518  ___m_DefaultBlend_6;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineBrain::m_CustomBlends
	CinemachineBlenderSettings_t865950197 * ___m_CustomBlends_7;
	// UnityEngine.Camera Cinemachine.CinemachineBrain::m_OutputCamera
	Camera_t2839736942 * ___m_OutputCamera_8;
	// Cinemachine.CinemachineBrain/PostFXHandlerDelegate Cinemachine.CinemachineBrain::<PostFXHandler>k__BackingField
	PostFXHandlerDelegate_t2862114331 * ___U3CPostFXHandlerU3Ek__BackingField_9;
	// UnityEngine.Events.UnityEvent Cinemachine.CinemachineBrain::m_CameraCutEvent
	UnityEvent_t2603596161 * ___m_CameraCutEvent_10;
	// UnityEngine.Events.UnityEvent Cinemachine.CinemachineBrain::m_CameraActivatedEvent
	UnityEvent_t2603596161 * ___m_CameraActivatedEvent_11;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mActiveCameraPreviousFrame
	RuntimeObject* ___mActiveCameraPreviousFrame_13;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mOutgoingCameraPreviousFrame
	RuntimeObject* ___mOutgoingCameraPreviousFrame_14;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineBrain::mActiveBlend
	CinemachineBlend_t2146485918 * ___mActiveBlend_15;
	// System.Boolean Cinemachine.CinemachineBrain::mPreviousFrameWasOverride
	bool ___mPreviousFrameWasOverride_16;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain/OverrideStackFrame> Cinemachine.CinemachineBrain::mOverrideStack
	List_1_t1470468345 * ___mOverrideStack_17;
	// System.Int32 Cinemachine.CinemachineBrain::mNextOverrideId
	int32_t ___mNextOverrideId_18;
	// System.Boolean Cinemachine.CinemachineBrain::<IsSuspended>k__BackingField
	bool ___U3CIsSuspendedU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_m_ShowDebugText_2() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_ShowDebugText_2)); }
	inline bool get_m_ShowDebugText_2() const { return ___m_ShowDebugText_2; }
	inline bool* get_address_of_m_ShowDebugText_2() { return &___m_ShowDebugText_2; }
	inline void set_m_ShowDebugText_2(bool value)
	{
		___m_ShowDebugText_2 = value;
	}

	inline static int32_t get_offset_of_m_ShowCameraFrustum_3() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_ShowCameraFrustum_3)); }
	inline bool get_m_ShowCameraFrustum_3() const { return ___m_ShowCameraFrustum_3; }
	inline bool* get_address_of_m_ShowCameraFrustum_3() { return &___m_ShowCameraFrustum_3; }
	inline void set_m_ShowCameraFrustum_3(bool value)
	{
		___m_ShowCameraFrustum_3 = value;
	}

	inline static int32_t get_offset_of_m_WorldUpOverride_4() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_WorldUpOverride_4)); }
	inline Transform_t362059596 * get_m_WorldUpOverride_4() const { return ___m_WorldUpOverride_4; }
	inline Transform_t362059596 ** get_address_of_m_WorldUpOverride_4() { return &___m_WorldUpOverride_4; }
	inline void set_m_WorldUpOverride_4(Transform_t362059596 * value)
	{
		___m_WorldUpOverride_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldUpOverride_4), value);
	}

	inline static int32_t get_offset_of_m_UpdateMethod_5() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_UpdateMethod_5)); }
	inline int32_t get_m_UpdateMethod_5() const { return ___m_UpdateMethod_5; }
	inline int32_t* get_address_of_m_UpdateMethod_5() { return &___m_UpdateMethod_5; }
	inline void set_m_UpdateMethod_5(int32_t value)
	{
		___m_UpdateMethod_5 = value;
	}

	inline static int32_t get_offset_of_m_DefaultBlend_6() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_DefaultBlend_6)); }
	inline CinemachineBlendDefinition_t3981438518  get_m_DefaultBlend_6() const { return ___m_DefaultBlend_6; }
	inline CinemachineBlendDefinition_t3981438518 * get_address_of_m_DefaultBlend_6() { return &___m_DefaultBlend_6; }
	inline void set_m_DefaultBlend_6(CinemachineBlendDefinition_t3981438518  value)
	{
		___m_DefaultBlend_6 = value;
	}

	inline static int32_t get_offset_of_m_CustomBlends_7() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_CustomBlends_7)); }
	inline CinemachineBlenderSettings_t865950197 * get_m_CustomBlends_7() const { return ___m_CustomBlends_7; }
	inline CinemachineBlenderSettings_t865950197 ** get_address_of_m_CustomBlends_7() { return &___m_CustomBlends_7; }
	inline void set_m_CustomBlends_7(CinemachineBlenderSettings_t865950197 * value)
	{
		___m_CustomBlends_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_7), value);
	}

	inline static int32_t get_offset_of_m_OutputCamera_8() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_OutputCamera_8)); }
	inline Camera_t2839736942 * get_m_OutputCamera_8() const { return ___m_OutputCamera_8; }
	inline Camera_t2839736942 ** get_address_of_m_OutputCamera_8() { return &___m_OutputCamera_8; }
	inline void set_m_OutputCamera_8(Camera_t2839736942 * value)
	{
		___m_OutputCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OutputCamera_8), value);
	}

	inline static int32_t get_offset_of_U3CPostFXHandlerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___U3CPostFXHandlerU3Ek__BackingField_9)); }
	inline PostFXHandlerDelegate_t2862114331 * get_U3CPostFXHandlerU3Ek__BackingField_9() const { return ___U3CPostFXHandlerU3Ek__BackingField_9; }
	inline PostFXHandlerDelegate_t2862114331 ** get_address_of_U3CPostFXHandlerU3Ek__BackingField_9() { return &___U3CPostFXHandlerU3Ek__BackingField_9; }
	inline void set_U3CPostFXHandlerU3Ek__BackingField_9(PostFXHandlerDelegate_t2862114331 * value)
	{
		___U3CPostFXHandlerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPostFXHandlerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_m_CameraCutEvent_10() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_CameraCutEvent_10)); }
	inline UnityEvent_t2603596161 * get_m_CameraCutEvent_10() const { return ___m_CameraCutEvent_10; }
	inline UnityEvent_t2603596161 ** get_address_of_m_CameraCutEvent_10() { return &___m_CameraCutEvent_10; }
	inline void set_m_CameraCutEvent_10(UnityEvent_t2603596161 * value)
	{
		___m_CameraCutEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraCutEvent_10), value);
	}

	inline static int32_t get_offset_of_m_CameraActivatedEvent_11() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___m_CameraActivatedEvent_11)); }
	inline UnityEvent_t2603596161 * get_m_CameraActivatedEvent_11() const { return ___m_CameraActivatedEvent_11; }
	inline UnityEvent_t2603596161 ** get_address_of_m_CameraActivatedEvent_11() { return &___m_CameraActivatedEvent_11; }
	inline void set_m_CameraActivatedEvent_11(UnityEvent_t2603596161 * value)
	{
		___m_CameraActivatedEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraActivatedEvent_11), value);
	}

	inline static int32_t get_offset_of_mActiveCameraPreviousFrame_13() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mActiveCameraPreviousFrame_13)); }
	inline RuntimeObject* get_mActiveCameraPreviousFrame_13() const { return ___mActiveCameraPreviousFrame_13; }
	inline RuntimeObject** get_address_of_mActiveCameraPreviousFrame_13() { return &___mActiveCameraPreviousFrame_13; }
	inline void set_mActiveCameraPreviousFrame_13(RuntimeObject* value)
	{
		___mActiveCameraPreviousFrame_13 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveCameraPreviousFrame_13), value);
	}

	inline static int32_t get_offset_of_mOutgoingCameraPreviousFrame_14() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mOutgoingCameraPreviousFrame_14)); }
	inline RuntimeObject* get_mOutgoingCameraPreviousFrame_14() const { return ___mOutgoingCameraPreviousFrame_14; }
	inline RuntimeObject** get_address_of_mOutgoingCameraPreviousFrame_14() { return &___mOutgoingCameraPreviousFrame_14; }
	inline void set_mOutgoingCameraPreviousFrame_14(RuntimeObject* value)
	{
		___mOutgoingCameraPreviousFrame_14 = value;
		Il2CppCodeGenWriteBarrier((&___mOutgoingCameraPreviousFrame_14), value);
	}

	inline static int32_t get_offset_of_mActiveBlend_15() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mActiveBlend_15)); }
	inline CinemachineBlend_t2146485918 * get_mActiveBlend_15() const { return ___mActiveBlend_15; }
	inline CinemachineBlend_t2146485918 ** get_address_of_mActiveBlend_15() { return &___mActiveBlend_15; }
	inline void set_mActiveBlend_15(CinemachineBlend_t2146485918 * value)
	{
		___mActiveBlend_15 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBlend_15), value);
	}

	inline static int32_t get_offset_of_mPreviousFrameWasOverride_16() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mPreviousFrameWasOverride_16)); }
	inline bool get_mPreviousFrameWasOverride_16() const { return ___mPreviousFrameWasOverride_16; }
	inline bool* get_address_of_mPreviousFrameWasOverride_16() { return &___mPreviousFrameWasOverride_16; }
	inline void set_mPreviousFrameWasOverride_16(bool value)
	{
		___mPreviousFrameWasOverride_16 = value;
	}

	inline static int32_t get_offset_of_mOverrideStack_17() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mOverrideStack_17)); }
	inline List_1_t1470468345 * get_mOverrideStack_17() const { return ___mOverrideStack_17; }
	inline List_1_t1470468345 ** get_address_of_mOverrideStack_17() { return &___mOverrideStack_17; }
	inline void set_mOverrideStack_17(List_1_t1470468345 * value)
	{
		___mOverrideStack_17 = value;
		Il2CppCodeGenWriteBarrier((&___mOverrideStack_17), value);
	}

	inline static int32_t get_offset_of_mNextOverrideId_18() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___mNextOverrideId_18)); }
	inline int32_t get_mNextOverrideId_18() const { return ___mNextOverrideId_18; }
	inline int32_t* get_address_of_mNextOverrideId_18() { return &___mNextOverrideId_18; }
	inline void set_mNextOverrideId_18(int32_t value)
	{
		___mNextOverrideId_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsSuspendedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552, ___U3CIsSuspendedU3Ek__BackingField_19)); }
	inline bool get_U3CIsSuspendedU3Ek__BackingField_19() const { return ___U3CIsSuspendedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CIsSuspendedU3Ek__BackingField_19() { return &___U3CIsSuspendedU3Ek__BackingField_19; }
	inline void set_U3CIsSuspendedU3Ek__BackingField_19(bool value)
	{
		___U3CIsSuspendedU3Ek__BackingField_19 = value;
	}
};

struct CinemachineBrain_t679733552_StaticFields
{
public:
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::<SoloCamera>k__BackingField
	RuntimeObject* ___U3CSoloCameraU3Ek__BackingField_12;
	// System.Int32 Cinemachine.CinemachineBrain::msCurrentFrame
	int32_t ___msCurrentFrame_20;
	// System.Int32 Cinemachine.CinemachineBrain::msFirstBrainObjectId
	int32_t ___msFirstBrainObjectId_21;
	// System.Int32 Cinemachine.CinemachineBrain::msSubframes
	int32_t ___msSubframes_22;

public:
	inline static int32_t get_offset_of_U3CSoloCameraU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552_StaticFields, ___U3CSoloCameraU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CSoloCameraU3Ek__BackingField_12() const { return ___U3CSoloCameraU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CSoloCameraU3Ek__BackingField_12() { return &___U3CSoloCameraU3Ek__BackingField_12; }
	inline void set_U3CSoloCameraU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CSoloCameraU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoloCameraU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_msCurrentFrame_20() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552_StaticFields, ___msCurrentFrame_20)); }
	inline int32_t get_msCurrentFrame_20() const { return ___msCurrentFrame_20; }
	inline int32_t* get_address_of_msCurrentFrame_20() { return &___msCurrentFrame_20; }
	inline void set_msCurrentFrame_20(int32_t value)
	{
		___msCurrentFrame_20 = value;
	}

	inline static int32_t get_offset_of_msFirstBrainObjectId_21() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552_StaticFields, ___msFirstBrainObjectId_21)); }
	inline int32_t get_msFirstBrainObjectId_21() const { return ___msFirstBrainObjectId_21; }
	inline int32_t* get_address_of_msFirstBrainObjectId_21() { return &___msFirstBrainObjectId_21; }
	inline void set_msFirstBrainObjectId_21(int32_t value)
	{
		___msFirstBrainObjectId_21 = value;
	}

	inline static int32_t get_offset_of_msSubframes_22() { return static_cast<int32_t>(offsetof(CinemachineBrain_t679733552_StaticFields, ___msSubframes_22)); }
	inline int32_t get_msSubframes_22() const { return ___msSubframes_22; }
	inline int32_t* get_address_of_msSubframes_22() { return &___msSubframes_22; }
	inline void set_msSubframes_22(int32_t value)
	{
		___msSubframes_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBRAIN_T679733552_H
#ifndef CINEMACHINEBASICMULTICHANNELPERLIN_T3993756505_H
#define CINEMACHINEBASICMULTICHANNELPERLIN_T3993756505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBasicMultiChannelPerlin
struct  CinemachineBasicMultiChannelPerlin_t3993756505  : public MonoBehaviour_t1618594486
{
public:
	// Cinemachine.NoiseSettings Cinemachine.CinemachineBasicMultiChannelPerlin::m_Definition
	NoiseSettings_t2082871546 * ___m_Definition_2;
	// System.Single Cinemachine.CinemachineBasicMultiChannelPerlin::m_AmplitudeGain
	float ___m_AmplitudeGain_3;
	// System.Single Cinemachine.CinemachineBasicMultiChannelPerlin::m_FrequencyGain
	float ___m_FrequencyGain_4;
	// System.Boolean Cinemachine.CinemachineBasicMultiChannelPerlin::mInitialized
	bool ___mInitialized_5;
	// System.Int32 Cinemachine.CinemachineBasicMultiChannelPerlin::mNoiseTime
	int32_t ___mNoiseTime_6;
	// UnityEngine.Vector3 Cinemachine.CinemachineBasicMultiChannelPerlin::mNoiseOffsets
	Vector3_t1986933152  ___mNoiseOffsets_7;

public:
	inline static int32_t get_offset_of_m_Definition_2() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___m_Definition_2)); }
	inline NoiseSettings_t2082871546 * get_m_Definition_2() const { return ___m_Definition_2; }
	inline NoiseSettings_t2082871546 ** get_address_of_m_Definition_2() { return &___m_Definition_2; }
	inline void set_m_Definition_2(NoiseSettings_t2082871546 * value)
	{
		___m_Definition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Definition_2), value);
	}

	inline static int32_t get_offset_of_m_AmplitudeGain_3() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___m_AmplitudeGain_3)); }
	inline float get_m_AmplitudeGain_3() const { return ___m_AmplitudeGain_3; }
	inline float* get_address_of_m_AmplitudeGain_3() { return &___m_AmplitudeGain_3; }
	inline void set_m_AmplitudeGain_3(float value)
	{
		___m_AmplitudeGain_3 = value;
	}

	inline static int32_t get_offset_of_m_FrequencyGain_4() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___m_FrequencyGain_4)); }
	inline float get_m_FrequencyGain_4() const { return ___m_FrequencyGain_4; }
	inline float* get_address_of_m_FrequencyGain_4() { return &___m_FrequencyGain_4; }
	inline void set_m_FrequencyGain_4(float value)
	{
		___m_FrequencyGain_4 = value;
	}

	inline static int32_t get_offset_of_mInitialized_5() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___mInitialized_5)); }
	inline bool get_mInitialized_5() const { return ___mInitialized_5; }
	inline bool* get_address_of_mInitialized_5() { return &___mInitialized_5; }
	inline void set_mInitialized_5(bool value)
	{
		___mInitialized_5 = value;
	}

	inline static int32_t get_offset_of_mNoiseTime_6() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___mNoiseTime_6)); }
	inline int32_t get_mNoiseTime_6() const { return ___mNoiseTime_6; }
	inline int32_t* get_address_of_mNoiseTime_6() { return &___mNoiseTime_6; }
	inline void set_mNoiseTime_6(int32_t value)
	{
		___mNoiseTime_6 = value;
	}

	inline static int32_t get_offset_of_mNoiseOffsets_7() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t3993756505, ___mNoiseOffsets_7)); }
	inline Vector3_t1986933152  get_mNoiseOffsets_7() const { return ___mNoiseOffsets_7; }
	inline Vector3_t1986933152 * get_address_of_mNoiseOffsets_7() { return &___mNoiseOffsets_7; }
	inline void set_mNoiseOffsets_7(Vector3_t1986933152  value)
	{
		___mNoiseOffsets_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBASICMULTICHANNELPERLIN_T3993756505_H
#ifndef CINEMACHINECOMPOSER_T3213114616_H
#define CINEMACHINECOMPOSER_T3213114616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineComposer
struct  CinemachineComposer_t3213114616  : public MonoBehaviour_t1618594486
{
public:
	// System.Action Cinemachine.CinemachineComposer::OnGUICallback
	Action_t370180854 * ___OnGUICallback_2;
	// System.Boolean Cinemachine.CinemachineComposer::m_ShowGuides
	bool ___m_ShowGuides_3;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_TrackedObjectOffset
	Vector3_t1986933152  ___m_TrackedObjectOffset_4;
	// System.Single Cinemachine.CinemachineComposer::m_HorizontalDamping
	float ___m_HorizontalDamping_5;
	// System.Single Cinemachine.CinemachineComposer::m_VerticalDamping
	float ___m_VerticalDamping_6;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenX
	float ___m_ScreenX_7;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenY
	float ___m_ScreenY_8;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneWidth
	float ___m_DeadZoneWidth_9;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneHeight
	float ___m_DeadZoneHeight_10;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneWidth
	float ___m_SoftZoneWidth_11;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneHeight
	float ___m_SoftZoneHeight_12;
	// System.Single Cinemachine.CinemachineComposer::m_BiasX
	float ___m_BiasX_13;
	// System.Single Cinemachine.CinemachineComposer::m_BiasY
	float ___m_BiasY_14;

public:
	inline static int32_t get_offset_of_OnGUICallback_2() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___OnGUICallback_2)); }
	inline Action_t370180854 * get_OnGUICallback_2() const { return ___OnGUICallback_2; }
	inline Action_t370180854 ** get_address_of_OnGUICallback_2() { return &___OnGUICallback_2; }
	inline void set_OnGUICallback_2(Action_t370180854 * value)
	{
		___OnGUICallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnGUICallback_2), value);
	}

	inline static int32_t get_offset_of_m_ShowGuides_3() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_ShowGuides_3)); }
	inline bool get_m_ShowGuides_3() const { return ___m_ShowGuides_3; }
	inline bool* get_address_of_m_ShowGuides_3() { return &___m_ShowGuides_3; }
	inline void set_m_ShowGuides_3(bool value)
	{
		___m_ShowGuides_3 = value;
	}

	inline static int32_t get_offset_of_m_TrackedObjectOffset_4() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_TrackedObjectOffset_4)); }
	inline Vector3_t1986933152  get_m_TrackedObjectOffset_4() const { return ___m_TrackedObjectOffset_4; }
	inline Vector3_t1986933152 * get_address_of_m_TrackedObjectOffset_4() { return &___m_TrackedObjectOffset_4; }
	inline void set_m_TrackedObjectOffset_4(Vector3_t1986933152  value)
	{
		___m_TrackedObjectOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalDamping_5() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_HorizontalDamping_5)); }
	inline float get_m_HorizontalDamping_5() const { return ___m_HorizontalDamping_5; }
	inline float* get_address_of_m_HorizontalDamping_5() { return &___m_HorizontalDamping_5; }
	inline void set_m_HorizontalDamping_5(float value)
	{
		___m_HorizontalDamping_5 = value;
	}

	inline static int32_t get_offset_of_m_VerticalDamping_6() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_VerticalDamping_6)); }
	inline float get_m_VerticalDamping_6() const { return ___m_VerticalDamping_6; }
	inline float* get_address_of_m_VerticalDamping_6() { return &___m_VerticalDamping_6; }
	inline void set_m_VerticalDamping_6(float value)
	{
		___m_VerticalDamping_6 = value;
	}

	inline static int32_t get_offset_of_m_ScreenX_7() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_ScreenX_7)); }
	inline float get_m_ScreenX_7() const { return ___m_ScreenX_7; }
	inline float* get_address_of_m_ScreenX_7() { return &___m_ScreenX_7; }
	inline void set_m_ScreenX_7(float value)
	{
		___m_ScreenX_7 = value;
	}

	inline static int32_t get_offset_of_m_ScreenY_8() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_ScreenY_8)); }
	inline float get_m_ScreenY_8() const { return ___m_ScreenY_8; }
	inline float* get_address_of_m_ScreenY_8() { return &___m_ScreenY_8; }
	inline void set_m_ScreenY_8(float value)
	{
		___m_ScreenY_8 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneWidth_9() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_DeadZoneWidth_9)); }
	inline float get_m_DeadZoneWidth_9() const { return ___m_DeadZoneWidth_9; }
	inline float* get_address_of_m_DeadZoneWidth_9() { return &___m_DeadZoneWidth_9; }
	inline void set_m_DeadZoneWidth_9(float value)
	{
		___m_DeadZoneWidth_9 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneHeight_10() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_DeadZoneHeight_10)); }
	inline float get_m_DeadZoneHeight_10() const { return ___m_DeadZoneHeight_10; }
	inline float* get_address_of_m_DeadZoneHeight_10() { return &___m_DeadZoneHeight_10; }
	inline void set_m_DeadZoneHeight_10(float value)
	{
		___m_DeadZoneHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneWidth_11() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_SoftZoneWidth_11)); }
	inline float get_m_SoftZoneWidth_11() const { return ___m_SoftZoneWidth_11; }
	inline float* get_address_of_m_SoftZoneWidth_11() { return &___m_SoftZoneWidth_11; }
	inline void set_m_SoftZoneWidth_11(float value)
	{
		___m_SoftZoneWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneHeight_12() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_SoftZoneHeight_12)); }
	inline float get_m_SoftZoneHeight_12() const { return ___m_SoftZoneHeight_12; }
	inline float* get_address_of_m_SoftZoneHeight_12() { return &___m_SoftZoneHeight_12; }
	inline void set_m_SoftZoneHeight_12(float value)
	{
		___m_SoftZoneHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_BiasX_13() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_BiasX_13)); }
	inline float get_m_BiasX_13() const { return ___m_BiasX_13; }
	inline float* get_address_of_m_BiasX_13() { return &___m_BiasX_13; }
	inline void set_m_BiasX_13(float value)
	{
		___m_BiasX_13 = value;
	}

	inline static int32_t get_offset_of_m_BiasY_14() { return static_cast<int32_t>(offsetof(CinemachineComposer_t3213114616, ___m_BiasY_14)); }
	inline float get_m_BiasY_14() const { return ___m_BiasY_14; }
	inline float* get_address_of_m_BiasY_14() { return &___m_BiasY_14; }
	inline void set_m_BiasY_14(float value)
	{
		___m_BiasY_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECOMPOSER_T3213114616_H
#ifndef CHARACTERCUSTOMISEBACKGROUND_T4088931509_H
#define CHARACTERCUSTOMISEBACKGROUND_T4088931509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterCustomiseBackground
struct  CharacterCustomiseBackground_t4088931509  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCUSTOMISEBACKGROUND_T4088931509_H
#ifndef CINEMACHINEORBITALTRANSPOSER_T281264161_H
#define CINEMACHINEORBITALTRANSPOSER_T281264161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer
struct  CinemachineOrbitalTransposer_t281264161  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_Radius
	float ___m_Radius_2;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_HeightOffset
	float ___m_HeightOffset_3;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_XDamping
	float ___m_XDamping_4;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_YDamping
	float ___m_YDamping_5;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_ZDamping
	float ___m_ZDamping_6;
	// Cinemachine.CinemachineOrbitalTransposer/DampingStyle Cinemachine.CinemachineOrbitalTransposer::m_DampingStyle
	int32_t ___m_DampingStyle_7;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_HeadingBias
	float ___m_HeadingBias_8;
	// Cinemachine.CinemachineOrbitalTransposer/AxisState Cinemachine.CinemachineOrbitalTransposer::m_XAxis
	AxisState_t2333558477  ___m_XAxis_9;
	// Cinemachine.CinemachineOrbitalTransposer/Recentering Cinemachine.CinemachineOrbitalTransposer::m_RecenterToTargetHeading
	Recentering_t75128244  ___m_RecenterToTargetHeading_10;
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer::m_HeadingIsSlave
	bool ___m_HeadingIsSlave_11;
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer::<UseOffsetOverride>k__BackingField
	bool ___U3CUseOffsetOverrideU3Ek__BackingField_12;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer::<OffsetOverride>k__BackingField
	Vector3_t1986933152  ___U3COffsetOverrideU3Ek__BackingField_13;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::mLastHeadingAxisInputTime
	float ___mLastHeadingAxisInputTime_15;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::mHeadingRecenteringVelocity
	float ___mHeadingRecenteringVelocity_16;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer::mLastTargetPosition
	Vector3_t1986933152  ___mLastTargetPosition_17;
	// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker Cinemachine.CinemachineOrbitalTransposer::mHeadingTracker
	HeadingTracker_t2287318816 * ___mHeadingTracker_18;
	// UnityEngine.Rigidbody Cinemachine.CinemachineOrbitalTransposer::mTargetRigidBody
	Rigidbody_t4273256674 * ___mTargetRigidBody_19;
	// UnityEngine.Transform Cinemachine.CinemachineOrbitalTransposer::<PreviousTarget>k__BackingField
	Transform_t362059596 * ___U3CPreviousTargetU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Radius_2() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_Radius_2)); }
	inline float get_m_Radius_2() const { return ___m_Radius_2; }
	inline float* get_address_of_m_Radius_2() { return &___m_Radius_2; }
	inline void set_m_Radius_2(float value)
	{
		___m_Radius_2 = value;
	}

	inline static int32_t get_offset_of_m_HeightOffset_3() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_HeightOffset_3)); }
	inline float get_m_HeightOffset_3() const { return ___m_HeightOffset_3; }
	inline float* get_address_of_m_HeightOffset_3() { return &___m_HeightOffset_3; }
	inline void set_m_HeightOffset_3(float value)
	{
		___m_HeightOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_4() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_XDamping_4)); }
	inline float get_m_XDamping_4() const { return ___m_XDamping_4; }
	inline float* get_address_of_m_XDamping_4() { return &___m_XDamping_4; }
	inline void set_m_XDamping_4(float value)
	{
		___m_XDamping_4 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_5() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_YDamping_5)); }
	inline float get_m_YDamping_5() const { return ___m_YDamping_5; }
	inline float* get_address_of_m_YDamping_5() { return &___m_YDamping_5; }
	inline void set_m_YDamping_5(float value)
	{
		___m_YDamping_5 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_6() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_ZDamping_6)); }
	inline float get_m_ZDamping_6() const { return ___m_ZDamping_6; }
	inline float* get_address_of_m_ZDamping_6() { return &___m_ZDamping_6; }
	inline void set_m_ZDamping_6(float value)
	{
		___m_ZDamping_6 = value;
	}

	inline static int32_t get_offset_of_m_DampingStyle_7() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_DampingStyle_7)); }
	inline int32_t get_m_DampingStyle_7() const { return ___m_DampingStyle_7; }
	inline int32_t* get_address_of_m_DampingStyle_7() { return &___m_DampingStyle_7; }
	inline void set_m_DampingStyle_7(int32_t value)
	{
		___m_DampingStyle_7 = value;
	}

	inline static int32_t get_offset_of_m_HeadingBias_8() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_HeadingBias_8)); }
	inline float get_m_HeadingBias_8() const { return ___m_HeadingBias_8; }
	inline float* get_address_of_m_HeadingBias_8() { return &___m_HeadingBias_8; }
	inline void set_m_HeadingBias_8(float value)
	{
		___m_HeadingBias_8 = value;
	}

	inline static int32_t get_offset_of_m_XAxis_9() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_XAxis_9)); }
	inline AxisState_t2333558477  get_m_XAxis_9() const { return ___m_XAxis_9; }
	inline AxisState_t2333558477 * get_address_of_m_XAxis_9() { return &___m_XAxis_9; }
	inline void set_m_XAxis_9(AxisState_t2333558477  value)
	{
		___m_XAxis_9 = value;
	}

	inline static int32_t get_offset_of_m_RecenterToTargetHeading_10() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_RecenterToTargetHeading_10)); }
	inline Recentering_t75128244  get_m_RecenterToTargetHeading_10() const { return ___m_RecenterToTargetHeading_10; }
	inline Recentering_t75128244 * get_address_of_m_RecenterToTargetHeading_10() { return &___m_RecenterToTargetHeading_10; }
	inline void set_m_RecenterToTargetHeading_10(Recentering_t75128244  value)
	{
		___m_RecenterToTargetHeading_10 = value;
	}

	inline static int32_t get_offset_of_m_HeadingIsSlave_11() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___m_HeadingIsSlave_11)); }
	inline bool get_m_HeadingIsSlave_11() const { return ___m_HeadingIsSlave_11; }
	inline bool* get_address_of_m_HeadingIsSlave_11() { return &___m_HeadingIsSlave_11; }
	inline void set_m_HeadingIsSlave_11(bool value)
	{
		___m_HeadingIsSlave_11 = value;
	}

	inline static int32_t get_offset_of_U3CUseOffsetOverrideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___U3CUseOffsetOverrideU3Ek__BackingField_12)); }
	inline bool get_U3CUseOffsetOverrideU3Ek__BackingField_12() const { return ___U3CUseOffsetOverrideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CUseOffsetOverrideU3Ek__BackingField_12() { return &___U3CUseOffsetOverrideU3Ek__BackingField_12; }
	inline void set_U3CUseOffsetOverrideU3Ek__BackingField_12(bool value)
	{
		___U3CUseOffsetOverrideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3COffsetOverrideU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___U3COffsetOverrideU3Ek__BackingField_13)); }
	inline Vector3_t1986933152  get_U3COffsetOverrideU3Ek__BackingField_13() const { return ___U3COffsetOverrideU3Ek__BackingField_13; }
	inline Vector3_t1986933152 * get_address_of_U3COffsetOverrideU3Ek__BackingField_13() { return &___U3COffsetOverrideU3Ek__BackingField_13; }
	inline void set_U3COffsetOverrideU3Ek__BackingField_13(Vector3_t1986933152  value)
	{
		___U3COffsetOverrideU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_mLastHeadingAxisInputTime_15() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___mLastHeadingAxisInputTime_15)); }
	inline float get_mLastHeadingAxisInputTime_15() const { return ___mLastHeadingAxisInputTime_15; }
	inline float* get_address_of_mLastHeadingAxisInputTime_15() { return &___mLastHeadingAxisInputTime_15; }
	inline void set_mLastHeadingAxisInputTime_15(float value)
	{
		___mLastHeadingAxisInputTime_15 = value;
	}

	inline static int32_t get_offset_of_mHeadingRecenteringVelocity_16() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___mHeadingRecenteringVelocity_16)); }
	inline float get_mHeadingRecenteringVelocity_16() const { return ___mHeadingRecenteringVelocity_16; }
	inline float* get_address_of_mHeadingRecenteringVelocity_16() { return &___mHeadingRecenteringVelocity_16; }
	inline void set_mHeadingRecenteringVelocity_16(float value)
	{
		___mHeadingRecenteringVelocity_16 = value;
	}

	inline static int32_t get_offset_of_mLastTargetPosition_17() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___mLastTargetPosition_17)); }
	inline Vector3_t1986933152  get_mLastTargetPosition_17() const { return ___mLastTargetPosition_17; }
	inline Vector3_t1986933152 * get_address_of_mLastTargetPosition_17() { return &___mLastTargetPosition_17; }
	inline void set_mLastTargetPosition_17(Vector3_t1986933152  value)
	{
		___mLastTargetPosition_17 = value;
	}

	inline static int32_t get_offset_of_mHeadingTracker_18() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___mHeadingTracker_18)); }
	inline HeadingTracker_t2287318816 * get_mHeadingTracker_18() const { return ___mHeadingTracker_18; }
	inline HeadingTracker_t2287318816 ** get_address_of_mHeadingTracker_18() { return &___mHeadingTracker_18; }
	inline void set_mHeadingTracker_18(HeadingTracker_t2287318816 * value)
	{
		___mHeadingTracker_18 = value;
		Il2CppCodeGenWriteBarrier((&___mHeadingTracker_18), value);
	}

	inline static int32_t get_offset_of_mTargetRigidBody_19() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___mTargetRigidBody_19)); }
	inline Rigidbody_t4273256674 * get_mTargetRigidBody_19() const { return ___mTargetRigidBody_19; }
	inline Rigidbody_t4273256674 ** get_address_of_mTargetRigidBody_19() { return &___mTargetRigidBody_19; }
	inline void set_mTargetRigidBody_19(Rigidbody_t4273256674 * value)
	{
		___mTargetRigidBody_19 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetRigidBody_19), value);
	}

	inline static int32_t get_offset_of_U3CPreviousTargetU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t281264161, ___U3CPreviousTargetU3Ek__BackingField_20)); }
	inline Transform_t362059596 * get_U3CPreviousTargetU3Ek__BackingField_20() const { return ___U3CPreviousTargetU3Ek__BackingField_20; }
	inline Transform_t362059596 ** get_address_of_U3CPreviousTargetU3Ek__BackingField_20() { return &___U3CPreviousTargetU3Ek__BackingField_20; }
	inline void set_U3CPreviousTargetU3Ek__BackingField_20(Transform_t362059596 * value)
	{
		___U3CPreviousTargetU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousTargetU3Ek__BackingField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEORBITALTRANSPOSER_T281264161_H
#ifndef CINEMACHINETARGETGROUP_T3734833537_H
#define CINEMACHINETARGETGROUP_T3734833537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup
struct  CinemachineTargetGroup_t3734833537  : public MonoBehaviour_t1618594486
{
public:
	// Cinemachine.CinemachineTargetGroup/PositionMode Cinemachine.CinemachineTargetGroup::m_PositionMode
	int32_t ___m_PositionMode_2;
	// Cinemachine.CinemachineTargetGroup/RotationMode Cinemachine.CinemachineTargetGroup::m_RotationMode
	int32_t ___m_RotationMode_3;
	// Cinemachine.CinemachineTargetGroup/Target[] Cinemachine.CinemachineTargetGroup::m_Targets
	TargetU5BU5D_t3745836252* ___m_Targets_4;

public:
	inline static int32_t get_offset_of_m_PositionMode_2() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t3734833537, ___m_PositionMode_2)); }
	inline int32_t get_m_PositionMode_2() const { return ___m_PositionMode_2; }
	inline int32_t* get_address_of_m_PositionMode_2() { return &___m_PositionMode_2; }
	inline void set_m_PositionMode_2(int32_t value)
	{
		___m_PositionMode_2 = value;
	}

	inline static int32_t get_offset_of_m_RotationMode_3() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t3734833537, ___m_RotationMode_3)); }
	inline int32_t get_m_RotationMode_3() const { return ___m_RotationMode_3; }
	inline int32_t* get_address_of_m_RotationMode_3() { return &___m_RotationMode_3; }
	inline void set_m_RotationMode_3(int32_t value)
	{
		___m_RotationMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Targets_4() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t3734833537, ___m_Targets_4)); }
	inline TargetU5BU5D_t3745836252* get_m_Targets_4() const { return ___m_Targets_4; }
	inline TargetU5BU5D_t3745836252** get_address_of_m_Targets_4() { return &___m_Targets_4; }
	inline void set_m_Targets_4(TargetU5BU5D_t3745836252* value)
	{
		___m_Targets_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Targets_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETARGETGROUP_T3734833537_H
#ifndef CHARACTERCREATION_T2214710272_H
#define CHARACTERCREATION_T2214710272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterCreation
struct  CharacterCreation_t2214710272  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Button CharacterCreation::hairStyle
	Button_t1293135404 * ___hairStyle_2;
	// UnityEngine.UI.Button CharacterCreation::hairColour
	Button_t1293135404 * ___hairColour_3;
	// UnityEngine.UI.Button CharacterCreation::skinColour
	Button_t1293135404 * ___skinColour_4;
	// UnityEngine.UI.Button CharacterCreation::eyeColour
	Button_t1293135404 * ___eyeColour_5;
	// UnityEngine.UI.Button CharacterCreation::clothingColour
	Button_t1293135404 * ___clothingColour_6;
	// UnityEngine.UI.Button CharacterCreation::trim
	Button_t1293135404 * ___trim_7;
	// UnityEngine.UI.Button CharacterCreation::trimColour
	Button_t1293135404 * ___trimColour_8;
	// UnityEngine.UI.Button CharacterCreation::backpack
	Button_t1293135404 * ___backpack_9;
	// UnityEngine.UI.Button CharacterCreation::confirm
	Button_t1293135404 * ___confirm_10;
	// UnityEngine.UI.Button CharacterCreation::cancel
	Button_t1293135404 * ___cancel_11;
	// UnityEngine.UI.Button CharacterCreation::leftSpin
	Button_t1293135404 * ___leftSpin_12;
	// UnityEngine.UI.Button CharacterCreation::rightSpin
	Button_t1293135404 * ___rightSpin_13;
	// UnityEngine.UI.Button CharacterCreation::clothing
	Button_t1293135404 * ___clothing_14;
	// UnityEngine.UI.Button[] CharacterCreation::customisationButtons
	ButtonU5BU5D_t1258674085* ___customisationButtons_15;
	// System.Boolean[] CharacterCreation::wasClicked
	BooleanU5BU5D_t698278498* ___wasClicked_16;

public:
	inline static int32_t get_offset_of_hairStyle_2() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___hairStyle_2)); }
	inline Button_t1293135404 * get_hairStyle_2() const { return ___hairStyle_2; }
	inline Button_t1293135404 ** get_address_of_hairStyle_2() { return &___hairStyle_2; }
	inline void set_hairStyle_2(Button_t1293135404 * value)
	{
		___hairStyle_2 = value;
		Il2CppCodeGenWriteBarrier((&___hairStyle_2), value);
	}

	inline static int32_t get_offset_of_hairColour_3() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___hairColour_3)); }
	inline Button_t1293135404 * get_hairColour_3() const { return ___hairColour_3; }
	inline Button_t1293135404 ** get_address_of_hairColour_3() { return &___hairColour_3; }
	inline void set_hairColour_3(Button_t1293135404 * value)
	{
		___hairColour_3 = value;
		Il2CppCodeGenWriteBarrier((&___hairColour_3), value);
	}

	inline static int32_t get_offset_of_skinColour_4() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___skinColour_4)); }
	inline Button_t1293135404 * get_skinColour_4() const { return ___skinColour_4; }
	inline Button_t1293135404 ** get_address_of_skinColour_4() { return &___skinColour_4; }
	inline void set_skinColour_4(Button_t1293135404 * value)
	{
		___skinColour_4 = value;
		Il2CppCodeGenWriteBarrier((&___skinColour_4), value);
	}

	inline static int32_t get_offset_of_eyeColour_5() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___eyeColour_5)); }
	inline Button_t1293135404 * get_eyeColour_5() const { return ___eyeColour_5; }
	inline Button_t1293135404 ** get_address_of_eyeColour_5() { return &___eyeColour_5; }
	inline void set_eyeColour_5(Button_t1293135404 * value)
	{
		___eyeColour_5 = value;
		Il2CppCodeGenWriteBarrier((&___eyeColour_5), value);
	}

	inline static int32_t get_offset_of_clothingColour_6() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___clothingColour_6)); }
	inline Button_t1293135404 * get_clothingColour_6() const { return ___clothingColour_6; }
	inline Button_t1293135404 ** get_address_of_clothingColour_6() { return &___clothingColour_6; }
	inline void set_clothingColour_6(Button_t1293135404 * value)
	{
		___clothingColour_6 = value;
		Il2CppCodeGenWriteBarrier((&___clothingColour_6), value);
	}

	inline static int32_t get_offset_of_trim_7() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___trim_7)); }
	inline Button_t1293135404 * get_trim_7() const { return ___trim_7; }
	inline Button_t1293135404 ** get_address_of_trim_7() { return &___trim_7; }
	inline void set_trim_7(Button_t1293135404 * value)
	{
		___trim_7 = value;
		Il2CppCodeGenWriteBarrier((&___trim_7), value);
	}

	inline static int32_t get_offset_of_trimColour_8() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___trimColour_8)); }
	inline Button_t1293135404 * get_trimColour_8() const { return ___trimColour_8; }
	inline Button_t1293135404 ** get_address_of_trimColour_8() { return &___trimColour_8; }
	inline void set_trimColour_8(Button_t1293135404 * value)
	{
		___trimColour_8 = value;
		Il2CppCodeGenWriteBarrier((&___trimColour_8), value);
	}

	inline static int32_t get_offset_of_backpack_9() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___backpack_9)); }
	inline Button_t1293135404 * get_backpack_9() const { return ___backpack_9; }
	inline Button_t1293135404 ** get_address_of_backpack_9() { return &___backpack_9; }
	inline void set_backpack_9(Button_t1293135404 * value)
	{
		___backpack_9 = value;
		Il2CppCodeGenWriteBarrier((&___backpack_9), value);
	}

	inline static int32_t get_offset_of_confirm_10() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___confirm_10)); }
	inline Button_t1293135404 * get_confirm_10() const { return ___confirm_10; }
	inline Button_t1293135404 ** get_address_of_confirm_10() { return &___confirm_10; }
	inline void set_confirm_10(Button_t1293135404 * value)
	{
		___confirm_10 = value;
		Il2CppCodeGenWriteBarrier((&___confirm_10), value);
	}

	inline static int32_t get_offset_of_cancel_11() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___cancel_11)); }
	inline Button_t1293135404 * get_cancel_11() const { return ___cancel_11; }
	inline Button_t1293135404 ** get_address_of_cancel_11() { return &___cancel_11; }
	inline void set_cancel_11(Button_t1293135404 * value)
	{
		___cancel_11 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_11), value);
	}

	inline static int32_t get_offset_of_leftSpin_12() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___leftSpin_12)); }
	inline Button_t1293135404 * get_leftSpin_12() const { return ___leftSpin_12; }
	inline Button_t1293135404 ** get_address_of_leftSpin_12() { return &___leftSpin_12; }
	inline void set_leftSpin_12(Button_t1293135404 * value)
	{
		___leftSpin_12 = value;
		Il2CppCodeGenWriteBarrier((&___leftSpin_12), value);
	}

	inline static int32_t get_offset_of_rightSpin_13() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___rightSpin_13)); }
	inline Button_t1293135404 * get_rightSpin_13() const { return ___rightSpin_13; }
	inline Button_t1293135404 ** get_address_of_rightSpin_13() { return &___rightSpin_13; }
	inline void set_rightSpin_13(Button_t1293135404 * value)
	{
		___rightSpin_13 = value;
		Il2CppCodeGenWriteBarrier((&___rightSpin_13), value);
	}

	inline static int32_t get_offset_of_clothing_14() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___clothing_14)); }
	inline Button_t1293135404 * get_clothing_14() const { return ___clothing_14; }
	inline Button_t1293135404 ** get_address_of_clothing_14() { return &___clothing_14; }
	inline void set_clothing_14(Button_t1293135404 * value)
	{
		___clothing_14 = value;
		Il2CppCodeGenWriteBarrier((&___clothing_14), value);
	}

	inline static int32_t get_offset_of_customisationButtons_15() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___customisationButtons_15)); }
	inline ButtonU5BU5D_t1258674085* get_customisationButtons_15() const { return ___customisationButtons_15; }
	inline ButtonU5BU5D_t1258674085** get_address_of_customisationButtons_15() { return &___customisationButtons_15; }
	inline void set_customisationButtons_15(ButtonU5BU5D_t1258674085* value)
	{
		___customisationButtons_15 = value;
		Il2CppCodeGenWriteBarrier((&___customisationButtons_15), value);
	}

	inline static int32_t get_offset_of_wasClicked_16() { return static_cast<int32_t>(offsetof(CharacterCreation_t2214710272, ___wasClicked_16)); }
	inline BooleanU5BU5D_t698278498* get_wasClicked_16() const { return ___wasClicked_16; }
	inline BooleanU5BU5D_t698278498** get_address_of_wasClicked_16() { return &___wasClicked_16; }
	inline void set_wasClicked_16(BooleanU5BU5D_t698278498* value)
	{
		___wasClicked_16 = value;
		Il2CppCodeGenWriteBarrier((&___wasClicked_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCREATION_T2214710272_H
#ifndef CINEMACHINESMOOTHER_T3297040134_H
#define CINEMACHINESMOOTHER_T3297040134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineSmoother
struct  CinemachineSmoother_t3297040134  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Cinemachine.CinemachineSmoother::m_PositionSmoothing
	float ___m_PositionSmoothing_2;
	// System.Single Cinemachine.CinemachineSmoother::m_LookAtSmoothing
	float ___m_LookAtSmoothing_3;
	// System.Single Cinemachine.CinemachineSmoother::m_RotationSmoothing
	float ___m_RotationSmoothing_4;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineSmoother::<VirtualCamera>k__BackingField
	CinemachineVirtualCameraBase_t2557508663 * ___U3CVirtualCameraU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.Utility.GaussianWindow1D_Vector3> Cinemachine.CinemachineSmoother::mSmoothingFilter
	Dictionary_2_t2995859955 * ___mSmoothingFilter_6;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.Utility.GaussianWindow1D_Vector3> Cinemachine.CinemachineSmoother::mSmoothingFilterLookAt
	Dictionary_2_t2995859955 * ___mSmoothingFilterLookAt_7;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.Utility.GaussianWindow1D_CameraRotation> Cinemachine.CinemachineSmoother::mSmoothingFilterRotation
	Dictionary_2_t1885300805 * ___mSmoothingFilterRotation_8;

public:
	inline static int32_t get_offset_of_m_PositionSmoothing_2() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___m_PositionSmoothing_2)); }
	inline float get_m_PositionSmoothing_2() const { return ___m_PositionSmoothing_2; }
	inline float* get_address_of_m_PositionSmoothing_2() { return &___m_PositionSmoothing_2; }
	inline void set_m_PositionSmoothing_2(float value)
	{
		___m_PositionSmoothing_2 = value;
	}

	inline static int32_t get_offset_of_m_LookAtSmoothing_3() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___m_LookAtSmoothing_3)); }
	inline float get_m_LookAtSmoothing_3() const { return ___m_LookAtSmoothing_3; }
	inline float* get_address_of_m_LookAtSmoothing_3() { return &___m_LookAtSmoothing_3; }
	inline void set_m_LookAtSmoothing_3(float value)
	{
		___m_LookAtSmoothing_3 = value;
	}

	inline static int32_t get_offset_of_m_RotationSmoothing_4() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___m_RotationSmoothing_4)); }
	inline float get_m_RotationSmoothing_4() const { return ___m_RotationSmoothing_4; }
	inline float* get_address_of_m_RotationSmoothing_4() { return &___m_RotationSmoothing_4; }
	inline void set_m_RotationSmoothing_4(float value)
	{
		___m_RotationSmoothing_4 = value;
	}

	inline static int32_t get_offset_of_U3CVirtualCameraU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___U3CVirtualCameraU3Ek__BackingField_5)); }
	inline CinemachineVirtualCameraBase_t2557508663 * get_U3CVirtualCameraU3Ek__BackingField_5() const { return ___U3CVirtualCameraU3Ek__BackingField_5; }
	inline CinemachineVirtualCameraBase_t2557508663 ** get_address_of_U3CVirtualCameraU3Ek__BackingField_5() { return &___U3CVirtualCameraU3Ek__BackingField_5; }
	inline void set_U3CVirtualCameraU3Ek__BackingField_5(CinemachineVirtualCameraBase_t2557508663 * value)
	{
		___U3CVirtualCameraU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVirtualCameraU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_mSmoothingFilter_6() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___mSmoothingFilter_6)); }
	inline Dictionary_2_t2995859955 * get_mSmoothingFilter_6() const { return ___mSmoothingFilter_6; }
	inline Dictionary_2_t2995859955 ** get_address_of_mSmoothingFilter_6() { return &___mSmoothingFilter_6; }
	inline void set_mSmoothingFilter_6(Dictionary_2_t2995859955 * value)
	{
		___mSmoothingFilter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSmoothingFilter_6), value);
	}

	inline static int32_t get_offset_of_mSmoothingFilterLookAt_7() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___mSmoothingFilterLookAt_7)); }
	inline Dictionary_2_t2995859955 * get_mSmoothingFilterLookAt_7() const { return ___mSmoothingFilterLookAt_7; }
	inline Dictionary_2_t2995859955 ** get_address_of_mSmoothingFilterLookAt_7() { return &___mSmoothingFilterLookAt_7; }
	inline void set_mSmoothingFilterLookAt_7(Dictionary_2_t2995859955 * value)
	{
		___mSmoothingFilterLookAt_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSmoothingFilterLookAt_7), value);
	}

	inline static int32_t get_offset_of_mSmoothingFilterRotation_8() { return static_cast<int32_t>(offsetof(CinemachineSmoother_t3297040134, ___mSmoothingFilterRotation_8)); }
	inline Dictionary_2_t1885300805 * get_mSmoothingFilterRotation_8() const { return ___mSmoothingFilterRotation_8; }
	inline Dictionary_2_t1885300805 ** get_address_of_mSmoothingFilterRotation_8() { return &___mSmoothingFilterRotation_8; }
	inline void set_mSmoothingFilterRotation_8(Dictionary_2_t1885300805 * value)
	{
		___mSmoothingFilterRotation_8 = value;
		Il2CppCodeGenWriteBarrier((&___mSmoothingFilterRotation_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESMOOTHER_T3297040134_H
#ifndef CINEMACHINEPIPELINE_T1082879987_H
#define CINEMACHINEPIPELINE_T1082879987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePipeline
struct  CinemachinePipeline_t1082879987  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPIPELINE_T1082879987_H
#ifndef ALWAYSXZERO_T3043892783_H
#define ALWAYSXZERO_T3043892783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlwaysXZero
struct  AlwaysXZero_t3043892783  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALWAYSXZERO_T3043892783_H
#ifndef CANVASNOTEBOOK_T2500940296_H
#define CANVASNOTEBOOK_T2500940296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasNotebook
struct  CanvasNotebook_t2500940296  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Text[] CanvasNotebook::ChangeableText
	TextU5BU5D_t1489827645* ___ChangeableText_2;
	// UnityEngine.UI.InputField[] CanvasNotebook::fields
	InputFieldU5BU5D_t363162864* ___fields_3;
	// UnityEngine.UI.Image[] CanvasNotebook::images
	ImageU5BU5D_t892035751* ___images_4;

public:
	inline static int32_t get_offset_of_ChangeableText_2() { return static_cast<int32_t>(offsetof(CanvasNotebook_t2500940296, ___ChangeableText_2)); }
	inline TextU5BU5D_t1489827645* get_ChangeableText_2() const { return ___ChangeableText_2; }
	inline TextU5BU5D_t1489827645** get_address_of_ChangeableText_2() { return &___ChangeableText_2; }
	inline void set_ChangeableText_2(TextU5BU5D_t1489827645* value)
	{
		___ChangeableText_2 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeableText_2), value);
	}

	inline static int32_t get_offset_of_fields_3() { return static_cast<int32_t>(offsetof(CanvasNotebook_t2500940296, ___fields_3)); }
	inline InputFieldU5BU5D_t363162864* get_fields_3() const { return ___fields_3; }
	inline InputFieldU5BU5D_t363162864** get_address_of_fields_3() { return &___fields_3; }
	inline void set_fields_3(InputFieldU5BU5D_t363162864* value)
	{
		___fields_3 = value;
		Il2CppCodeGenWriteBarrier((&___fields_3), value);
	}

	inline static int32_t get_offset_of_images_4() { return static_cast<int32_t>(offsetof(CanvasNotebook_t2500940296, ___images_4)); }
	inline ImageU5BU5D_t892035751* get_images_4() const { return ___images_4; }
	inline ImageU5BU5D_t892035751** get_address_of_images_4() { return &___images_4; }
	inline void set_images_4(ImageU5BU5D_t892035751* value)
	{
		___images_4 = value;
		Il2CppCodeGenWriteBarrier((&___images_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASNOTEBOOK_T2500940296_H
#ifndef GLOBALFOG_T4163397306_H
#define GLOBALFOG_T4163397306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.GlobalFog
struct  GlobalFog_t4163397306  : public PostEffectsBase_t1831076792
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::distanceFog
	bool ___distanceFog_6;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::excludeFarPixels
	bool ___excludeFarPixels_7;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::useRadialDistance
	bool ___useRadialDistance_8;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::heightFog
	bool ___heightFog_9;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::height
	float ___height_10;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::heightDensity
	float ___heightDensity_11;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::startDistance
	float ___startDistance_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.GlobalFog::fogShader
	Shader_t1881769421 * ___fogShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.GlobalFog::fogMaterial
	Material_t2815264910 * ___fogMaterial_14;

public:
	inline static int32_t get_offset_of_distanceFog_6() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___distanceFog_6)); }
	inline bool get_distanceFog_6() const { return ___distanceFog_6; }
	inline bool* get_address_of_distanceFog_6() { return &___distanceFog_6; }
	inline void set_distanceFog_6(bool value)
	{
		___distanceFog_6 = value;
	}

	inline static int32_t get_offset_of_excludeFarPixels_7() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___excludeFarPixels_7)); }
	inline bool get_excludeFarPixels_7() const { return ___excludeFarPixels_7; }
	inline bool* get_address_of_excludeFarPixels_7() { return &___excludeFarPixels_7; }
	inline void set_excludeFarPixels_7(bool value)
	{
		___excludeFarPixels_7 = value;
	}

	inline static int32_t get_offset_of_useRadialDistance_8() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___useRadialDistance_8)); }
	inline bool get_useRadialDistance_8() const { return ___useRadialDistance_8; }
	inline bool* get_address_of_useRadialDistance_8() { return &___useRadialDistance_8; }
	inline void set_useRadialDistance_8(bool value)
	{
		___useRadialDistance_8 = value;
	}

	inline static int32_t get_offset_of_heightFog_9() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___heightFog_9)); }
	inline bool get_heightFog_9() const { return ___heightFog_9; }
	inline bool* get_address_of_heightFog_9() { return &___heightFog_9; }
	inline void set_heightFog_9(bool value)
	{
		___heightFog_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_heightDensity_11() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___heightDensity_11)); }
	inline float get_heightDensity_11() const { return ___heightDensity_11; }
	inline float* get_address_of_heightDensity_11() { return &___heightDensity_11; }
	inline void set_heightDensity_11(float value)
	{
		___heightDensity_11 = value;
	}

	inline static int32_t get_offset_of_startDistance_12() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___startDistance_12)); }
	inline float get_startDistance_12() const { return ___startDistance_12; }
	inline float* get_address_of_startDistance_12() { return &___startDistance_12; }
	inline void set_startDistance_12(float value)
	{
		___startDistance_12 = value;
	}

	inline static int32_t get_offset_of_fogShader_13() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___fogShader_13)); }
	inline Shader_t1881769421 * get_fogShader_13() const { return ___fogShader_13; }
	inline Shader_t1881769421 ** get_address_of_fogShader_13() { return &___fogShader_13; }
	inline void set_fogShader_13(Shader_t1881769421 * value)
	{
		___fogShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___fogShader_13), value);
	}

	inline static int32_t get_offset_of_fogMaterial_14() { return static_cast<int32_t>(offsetof(GlobalFog_t4163397306, ___fogMaterial_14)); }
	inline Material_t2815264910 * get_fogMaterial_14() const { return ___fogMaterial_14; }
	inline Material_t2815264910 ** get_address_of_fogMaterial_14() { return &___fogMaterial_14; }
	inline void set_fogMaterial_14(Material_t2815264910 * value)
	{
		___fogMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___fogMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALFOG_T4163397306_H
#ifndef EDGEDETECTION_T2492772135_H
#define EDGEDETECTION_T2492772135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.EdgeDetection
struct  EdgeDetection_t2492772135  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode UnityStandardAssets.ImageEffects.EdgeDetection::mode
	int32_t ___mode_6;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sensitivityDepth
	float ___sensitivityDepth_7;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sensitivityNormals
	float ___sensitivityNormals_8;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::lumThreshold
	float ___lumThreshold_9;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::edgeExp
	float ___edgeExp_10;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sampleDist
	float ___sampleDist_11;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::edgesOnly
	float ___edgesOnly_12;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.EdgeDetection::edgesOnlyBgColor
	Color_t2582018970  ___edgesOnlyBgColor_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.EdgeDetection::edgeDetectShader
	Shader_t1881769421 * ___edgeDetectShader_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.EdgeDetection::edgeDetectMaterial
	Material_t2815264910 * ___edgeDetectMaterial_15;
	// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode UnityStandardAssets.ImageEffects.EdgeDetection::oldMode
	int32_t ___oldMode_16;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_sensitivityDepth_7() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___sensitivityDepth_7)); }
	inline float get_sensitivityDepth_7() const { return ___sensitivityDepth_7; }
	inline float* get_address_of_sensitivityDepth_7() { return &___sensitivityDepth_7; }
	inline void set_sensitivityDepth_7(float value)
	{
		___sensitivityDepth_7 = value;
	}

	inline static int32_t get_offset_of_sensitivityNormals_8() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___sensitivityNormals_8)); }
	inline float get_sensitivityNormals_8() const { return ___sensitivityNormals_8; }
	inline float* get_address_of_sensitivityNormals_8() { return &___sensitivityNormals_8; }
	inline void set_sensitivityNormals_8(float value)
	{
		___sensitivityNormals_8 = value;
	}

	inline static int32_t get_offset_of_lumThreshold_9() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___lumThreshold_9)); }
	inline float get_lumThreshold_9() const { return ___lumThreshold_9; }
	inline float* get_address_of_lumThreshold_9() { return &___lumThreshold_9; }
	inline void set_lumThreshold_9(float value)
	{
		___lumThreshold_9 = value;
	}

	inline static int32_t get_offset_of_edgeExp_10() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___edgeExp_10)); }
	inline float get_edgeExp_10() const { return ___edgeExp_10; }
	inline float* get_address_of_edgeExp_10() { return &___edgeExp_10; }
	inline void set_edgeExp_10(float value)
	{
		___edgeExp_10 = value;
	}

	inline static int32_t get_offset_of_sampleDist_11() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___sampleDist_11)); }
	inline float get_sampleDist_11() const { return ___sampleDist_11; }
	inline float* get_address_of_sampleDist_11() { return &___sampleDist_11; }
	inline void set_sampleDist_11(float value)
	{
		___sampleDist_11 = value;
	}

	inline static int32_t get_offset_of_edgesOnly_12() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___edgesOnly_12)); }
	inline float get_edgesOnly_12() const { return ___edgesOnly_12; }
	inline float* get_address_of_edgesOnly_12() { return &___edgesOnly_12; }
	inline void set_edgesOnly_12(float value)
	{
		___edgesOnly_12 = value;
	}

	inline static int32_t get_offset_of_edgesOnlyBgColor_13() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___edgesOnlyBgColor_13)); }
	inline Color_t2582018970  get_edgesOnlyBgColor_13() const { return ___edgesOnlyBgColor_13; }
	inline Color_t2582018970 * get_address_of_edgesOnlyBgColor_13() { return &___edgesOnlyBgColor_13; }
	inline void set_edgesOnlyBgColor_13(Color_t2582018970  value)
	{
		___edgesOnlyBgColor_13 = value;
	}

	inline static int32_t get_offset_of_edgeDetectShader_14() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___edgeDetectShader_14)); }
	inline Shader_t1881769421 * get_edgeDetectShader_14() const { return ___edgeDetectShader_14; }
	inline Shader_t1881769421 ** get_address_of_edgeDetectShader_14() { return &___edgeDetectShader_14; }
	inline void set_edgeDetectShader_14(Shader_t1881769421 * value)
	{
		___edgeDetectShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___edgeDetectShader_14), value);
	}

	inline static int32_t get_offset_of_edgeDetectMaterial_15() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___edgeDetectMaterial_15)); }
	inline Material_t2815264910 * get_edgeDetectMaterial_15() const { return ___edgeDetectMaterial_15; }
	inline Material_t2815264910 ** get_address_of_edgeDetectMaterial_15() { return &___edgeDetectMaterial_15; }
	inline void set_edgeDetectMaterial_15(Material_t2815264910 * value)
	{
		___edgeDetectMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___edgeDetectMaterial_15), value);
	}

	inline static int32_t get_offset_of_oldMode_16() { return static_cast<int32_t>(offsetof(EdgeDetection_t2492772135, ___oldMode_16)); }
	inline int32_t get_oldMode_16() const { return ___oldMode_16; }
	inline int32_t* get_address_of_oldMode_16() { return &___oldMode_16; }
	inline void set_oldMode_16(int32_t value)
	{
		___oldMode_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEDETECTION_T2492772135_H
#ifndef GRAYSCALE_T1098547321_H
#define GRAYSCALE_T1098547321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Grayscale
struct  Grayscale_t1098547321  : public ImageEffectBase_t4133330248
{
public:
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.Grayscale::textureRamp
	Texture_t2119925672 * ___textureRamp_4;
	// System.Single UnityStandardAssets.ImageEffects.Grayscale::rampOffset
	float ___rampOffset_5;

public:
	inline static int32_t get_offset_of_textureRamp_4() { return static_cast<int32_t>(offsetof(Grayscale_t1098547321, ___textureRamp_4)); }
	inline Texture_t2119925672 * get_textureRamp_4() const { return ___textureRamp_4; }
	inline Texture_t2119925672 ** get_address_of_textureRamp_4() { return &___textureRamp_4; }
	inline void set_textureRamp_4(Texture_t2119925672 * value)
	{
		___textureRamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___textureRamp_4), value);
	}

	inline static int32_t get_offset_of_rampOffset_5() { return static_cast<int32_t>(offsetof(Grayscale_t1098547321, ___rampOffset_5)); }
	inline float get_rampOffset_5() const { return ___rampOffset_5; }
	inline float* get_address_of_rampOffset_5() { return &___rampOffset_5; }
	inline void set_rampOffset_5(float value)
	{
		___rampOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAYSCALE_T1098547321_H
#ifndef CINEMACHINECLEARSHOT_T4289735864_H
#define CINEMACHINECLEARSHOT_T4289735864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineClearShot
struct  CinemachineClearShot_t4289735864  : public CinemachineVirtualCameraBase_t2557508663
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineClearShot::m_LookAt
	Transform_t362059596 * ___m_LookAt_12;
	// UnityEngine.Transform Cinemachine.CinemachineClearShot::m_Follow
	Transform_t362059596 * ___m_Follow_13;
	// System.Boolean Cinemachine.CinemachineClearShot::m_ShowDebugText
	bool ___m_ShowDebugText_14;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineClearShot::m_ChildCameras
	CinemachineVirtualCameraBaseU5BU5D_t3901427982* ___m_ChildCameras_15;
	// System.Single Cinemachine.CinemachineClearShot::m_ActivateAfter
	float ___m_ActivateAfter_16;
	// System.Single Cinemachine.CinemachineClearShot::m_MinDuration
	float ___m_MinDuration_17;
	// System.Boolean Cinemachine.CinemachineClearShot::m_RandomizeChoice
	bool ___m_RandomizeChoice_18;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineClearShot::m_DefaultBlend
	CinemachineBlendDefinition_t3981438518  ___m_DefaultBlend_19;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineClearShot::m_CustomBlends
	CinemachineBlenderSettings_t865950197 * ___m_CustomBlends_20;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineClearShot::<LiveChild>k__BackingField
	RuntimeObject* ___U3CLiveChildU3Ek__BackingField_21;
	// Cinemachine.CameraState Cinemachine.CinemachineClearShot::m_State
	CameraState_t382403230  ___m_State_22;
	// System.Single Cinemachine.CinemachineClearShot::mActivationTime
	float ___mActivationTime_23;
	// System.Single Cinemachine.CinemachineClearShot::mPendingActivationTime
	float ___mPendingActivationTime_24;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineClearShot::mPendingCamera
	RuntimeObject* ___mPendingCamera_25;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineClearShot::mActiveBlend
	CinemachineBlend_t2146485918 * ___mActiveBlend_26;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineClearShot::m_RandomizedChilden
	CinemachineVirtualCameraBaseU5BU5D_t3901427982* ___m_RandomizedChilden_27;

public:
	inline static int32_t get_offset_of_m_LookAt_12() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_LookAt_12)); }
	inline Transform_t362059596 * get_m_LookAt_12() const { return ___m_LookAt_12; }
	inline Transform_t362059596 ** get_address_of_m_LookAt_12() { return &___m_LookAt_12; }
	inline void set_m_LookAt_12(Transform_t362059596 * value)
	{
		___m_LookAt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_12), value);
	}

	inline static int32_t get_offset_of_m_Follow_13() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_Follow_13)); }
	inline Transform_t362059596 * get_m_Follow_13() const { return ___m_Follow_13; }
	inline Transform_t362059596 ** get_address_of_m_Follow_13() { return &___m_Follow_13; }
	inline void set_m_Follow_13(Transform_t362059596 * value)
	{
		___m_Follow_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_13), value);
	}

	inline static int32_t get_offset_of_m_ShowDebugText_14() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_ShowDebugText_14)); }
	inline bool get_m_ShowDebugText_14() const { return ___m_ShowDebugText_14; }
	inline bool* get_address_of_m_ShowDebugText_14() { return &___m_ShowDebugText_14; }
	inline void set_m_ShowDebugText_14(bool value)
	{
		___m_ShowDebugText_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildCameras_15() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_ChildCameras_15)); }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982* get_m_ChildCameras_15() const { return ___m_ChildCameras_15; }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982** get_address_of_m_ChildCameras_15() { return &___m_ChildCameras_15; }
	inline void set_m_ChildCameras_15(CinemachineVirtualCameraBaseU5BU5D_t3901427982* value)
	{
		___m_ChildCameras_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildCameras_15), value);
	}

	inline static int32_t get_offset_of_m_ActivateAfter_16() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_ActivateAfter_16)); }
	inline float get_m_ActivateAfter_16() const { return ___m_ActivateAfter_16; }
	inline float* get_address_of_m_ActivateAfter_16() { return &___m_ActivateAfter_16; }
	inline void set_m_ActivateAfter_16(float value)
	{
		___m_ActivateAfter_16 = value;
	}

	inline static int32_t get_offset_of_m_MinDuration_17() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_MinDuration_17)); }
	inline float get_m_MinDuration_17() const { return ___m_MinDuration_17; }
	inline float* get_address_of_m_MinDuration_17() { return &___m_MinDuration_17; }
	inline void set_m_MinDuration_17(float value)
	{
		___m_MinDuration_17 = value;
	}

	inline static int32_t get_offset_of_m_RandomizeChoice_18() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_RandomizeChoice_18)); }
	inline bool get_m_RandomizeChoice_18() const { return ___m_RandomizeChoice_18; }
	inline bool* get_address_of_m_RandomizeChoice_18() { return &___m_RandomizeChoice_18; }
	inline void set_m_RandomizeChoice_18(bool value)
	{
		___m_RandomizeChoice_18 = value;
	}

	inline static int32_t get_offset_of_m_DefaultBlend_19() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_DefaultBlend_19)); }
	inline CinemachineBlendDefinition_t3981438518  get_m_DefaultBlend_19() const { return ___m_DefaultBlend_19; }
	inline CinemachineBlendDefinition_t3981438518 * get_address_of_m_DefaultBlend_19() { return &___m_DefaultBlend_19; }
	inline void set_m_DefaultBlend_19(CinemachineBlendDefinition_t3981438518  value)
	{
		___m_DefaultBlend_19 = value;
	}

	inline static int32_t get_offset_of_m_CustomBlends_20() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_CustomBlends_20)); }
	inline CinemachineBlenderSettings_t865950197 * get_m_CustomBlends_20() const { return ___m_CustomBlends_20; }
	inline CinemachineBlenderSettings_t865950197 ** get_address_of_m_CustomBlends_20() { return &___m_CustomBlends_20; }
	inline void set_m_CustomBlends_20(CinemachineBlenderSettings_t865950197 * value)
	{
		___m_CustomBlends_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_20), value);
	}

	inline static int32_t get_offset_of_U3CLiveChildU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___U3CLiveChildU3Ek__BackingField_21)); }
	inline RuntimeObject* get_U3CLiveChildU3Ek__BackingField_21() const { return ___U3CLiveChildU3Ek__BackingField_21; }
	inline RuntimeObject** get_address_of_U3CLiveChildU3Ek__BackingField_21() { return &___U3CLiveChildU3Ek__BackingField_21; }
	inline void set_U3CLiveChildU3Ek__BackingField_21(RuntimeObject* value)
	{
		___U3CLiveChildU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLiveChildU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_m_State_22() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_State_22)); }
	inline CameraState_t382403230  get_m_State_22() const { return ___m_State_22; }
	inline CameraState_t382403230 * get_address_of_m_State_22() { return &___m_State_22; }
	inline void set_m_State_22(CameraState_t382403230  value)
	{
		___m_State_22 = value;
	}

	inline static int32_t get_offset_of_mActivationTime_23() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___mActivationTime_23)); }
	inline float get_mActivationTime_23() const { return ___mActivationTime_23; }
	inline float* get_address_of_mActivationTime_23() { return &___mActivationTime_23; }
	inline void set_mActivationTime_23(float value)
	{
		___mActivationTime_23 = value;
	}

	inline static int32_t get_offset_of_mPendingActivationTime_24() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___mPendingActivationTime_24)); }
	inline float get_mPendingActivationTime_24() const { return ___mPendingActivationTime_24; }
	inline float* get_address_of_mPendingActivationTime_24() { return &___mPendingActivationTime_24; }
	inline void set_mPendingActivationTime_24(float value)
	{
		___mPendingActivationTime_24 = value;
	}

	inline static int32_t get_offset_of_mPendingCamera_25() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___mPendingCamera_25)); }
	inline RuntimeObject* get_mPendingCamera_25() const { return ___mPendingCamera_25; }
	inline RuntimeObject** get_address_of_mPendingCamera_25() { return &___mPendingCamera_25; }
	inline void set_mPendingCamera_25(RuntimeObject* value)
	{
		___mPendingCamera_25 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingCamera_25), value);
	}

	inline static int32_t get_offset_of_mActiveBlend_26() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___mActiveBlend_26)); }
	inline CinemachineBlend_t2146485918 * get_mActiveBlend_26() const { return ___mActiveBlend_26; }
	inline CinemachineBlend_t2146485918 ** get_address_of_mActiveBlend_26() { return &___mActiveBlend_26; }
	inline void set_mActiveBlend_26(CinemachineBlend_t2146485918 * value)
	{
		___mActiveBlend_26 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBlend_26), value);
	}

	inline static int32_t get_offset_of_m_RandomizedChilden_27() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864, ___m_RandomizedChilden_27)); }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982* get_m_RandomizedChilden_27() const { return ___m_RandomizedChilden_27; }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982** get_address_of_m_RandomizedChilden_27() { return &___m_RandomizedChilden_27; }
	inline void set_m_RandomizedChilden_27(CinemachineVirtualCameraBaseU5BU5D_t3901427982* value)
	{
		___m_RandomizedChilden_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_RandomizedChilden_27), value);
	}
};

struct CinemachineClearShot_t4289735864_StaticFields
{
public:
	// System.Comparison`1<Cinemachine.CinemachineClearShot/Pair> Cinemachine.CinemachineClearShot::<>f__am$cache0
	Comparison_1_t4217023303 * ___U3CU3Ef__amU24cache0_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_28() { return static_cast<int32_t>(offsetof(CinemachineClearShot_t4289735864_StaticFields, ___U3CU3Ef__amU24cache0_28)); }
	inline Comparison_1_t4217023303 * get_U3CU3Ef__amU24cache0_28() const { return ___U3CU3Ef__amU24cache0_28; }
	inline Comparison_1_t4217023303 ** get_address_of_U3CU3Ef__amU24cache0_28() { return &___U3CU3Ef__amU24cache0_28; }
	inline void set_U3CU3Ef__amU24cache0_28(Comparison_1_t4217023303 * value)
	{
		___U3CU3Ef__amU24cache0_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECLEARSHOT_T4289735864_H
#ifndef MOTIONBLUR_T1132854394_H
#define MOTIONBLUR_T1132854394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.MotionBlur
struct  MotionBlur_t1132854394  : public ImageEffectBase_t4133330248
{
public:
	// System.Single UnityStandardAssets.ImageEffects.MotionBlur::blurAmount
	float ___blurAmount_4;
	// System.Boolean UnityStandardAssets.ImageEffects.MotionBlur::extraBlur
	bool ___extraBlur_5;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.MotionBlur::accumTexture
	RenderTexture_t971269558 * ___accumTexture_6;

public:
	inline static int32_t get_offset_of_blurAmount_4() { return static_cast<int32_t>(offsetof(MotionBlur_t1132854394, ___blurAmount_4)); }
	inline float get_blurAmount_4() const { return ___blurAmount_4; }
	inline float* get_address_of_blurAmount_4() { return &___blurAmount_4; }
	inline void set_blurAmount_4(float value)
	{
		___blurAmount_4 = value;
	}

	inline static int32_t get_offset_of_extraBlur_5() { return static_cast<int32_t>(offsetof(MotionBlur_t1132854394, ___extraBlur_5)); }
	inline bool get_extraBlur_5() const { return ___extraBlur_5; }
	inline bool* get_address_of_extraBlur_5() { return &___extraBlur_5; }
	inline void set_extraBlur_5(bool value)
	{
		___extraBlur_5 = value;
	}

	inline static int32_t get_offset_of_accumTexture_6() { return static_cast<int32_t>(offsetof(MotionBlur_t1132854394, ___accumTexture_6)); }
	inline RenderTexture_t971269558 * get_accumTexture_6() const { return ___accumTexture_6; }
	inline RenderTexture_t971269558 ** get_address_of_accumTexture_6() { return &___accumTexture_6; }
	inline void set_accumTexture_6(RenderTexture_t971269558 * value)
	{
		___accumTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___accumTexture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T1132854394_H
#ifndef FISHEYE_T676218443_H
#define FISHEYE_T676218443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Fisheye
struct  Fisheye_t676218443  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthX
	float ___strengthX_6;
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthY
	float ___strengthY_7;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Fisheye::fishEyeShader
	Shader_t1881769421 * ___fishEyeShader_8;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Fisheye::fisheyeMaterial
	Material_t2815264910 * ___fisheyeMaterial_9;

public:
	inline static int32_t get_offset_of_strengthX_6() { return static_cast<int32_t>(offsetof(Fisheye_t676218443, ___strengthX_6)); }
	inline float get_strengthX_6() const { return ___strengthX_6; }
	inline float* get_address_of_strengthX_6() { return &___strengthX_6; }
	inline void set_strengthX_6(float value)
	{
		___strengthX_6 = value;
	}

	inline static int32_t get_offset_of_strengthY_7() { return static_cast<int32_t>(offsetof(Fisheye_t676218443, ___strengthY_7)); }
	inline float get_strengthY_7() const { return ___strengthY_7; }
	inline float* get_address_of_strengthY_7() { return &___strengthY_7; }
	inline void set_strengthY_7(float value)
	{
		___strengthY_7 = value;
	}

	inline static int32_t get_offset_of_fishEyeShader_8() { return static_cast<int32_t>(offsetof(Fisheye_t676218443, ___fishEyeShader_8)); }
	inline Shader_t1881769421 * get_fishEyeShader_8() const { return ___fishEyeShader_8; }
	inline Shader_t1881769421 ** get_address_of_fishEyeShader_8() { return &___fishEyeShader_8; }
	inline void set_fishEyeShader_8(Shader_t1881769421 * value)
	{
		___fishEyeShader_8 = value;
		Il2CppCodeGenWriteBarrier((&___fishEyeShader_8), value);
	}

	inline static int32_t get_offset_of_fisheyeMaterial_9() { return static_cast<int32_t>(offsetof(Fisheye_t676218443, ___fisheyeMaterial_9)); }
	inline Material_t2815264910 * get_fisheyeMaterial_9() const { return ___fisheyeMaterial_9; }
	inline Material_t2815264910 ** get_address_of_fisheyeMaterial_9() { return &___fisheyeMaterial_9; }
	inline void set_fisheyeMaterial_9(Material_t2815264910 * value)
	{
		___fisheyeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___fisheyeMaterial_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHEYE_T676218443_H
#ifndef VIGNETTEANDCHROMATICABERRATION_T137156492_H
#define VIGNETTEANDCHROMATICABERRATION_T137156492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
struct  VignetteAndChromaticAberration_t137156492  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::mode
	int32_t ___mode_6;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::intensity
	float ___intensity_7;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::chromaticAberration
	float ___chromaticAberration_8;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::axialAberration
	float ___axialAberration_9;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blur
	float ___blur_10;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blurSpread
	float ___blurSpread_11;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::luminanceDependency
	float ___luminanceDependency_12;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blurDistance
	float ___blurDistance_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::vignetteShader
	Shader_t1881769421 * ___vignetteShader_14;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::separableBlurShader
	Shader_t1881769421 * ___separableBlurShader_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::chromAberrationShader
	Shader_t1881769421 * ___chromAberrationShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_VignetteMaterial
	Material_t2815264910 * ___m_VignetteMaterial_17;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_SeparableBlurMaterial
	Material_t2815264910 * ___m_SeparableBlurMaterial_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_ChromAberrationMaterial
	Material_t2815264910 * ___m_ChromAberrationMaterial_19;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_chromaticAberration_8() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___chromaticAberration_8)); }
	inline float get_chromaticAberration_8() const { return ___chromaticAberration_8; }
	inline float* get_address_of_chromaticAberration_8() { return &___chromaticAberration_8; }
	inline void set_chromaticAberration_8(float value)
	{
		___chromaticAberration_8 = value;
	}

	inline static int32_t get_offset_of_axialAberration_9() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___axialAberration_9)); }
	inline float get_axialAberration_9() const { return ___axialAberration_9; }
	inline float* get_address_of_axialAberration_9() { return &___axialAberration_9; }
	inline void set_axialAberration_9(float value)
	{
		___axialAberration_9 = value;
	}

	inline static int32_t get_offset_of_blur_10() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___blur_10)); }
	inline float get_blur_10() const { return ___blur_10; }
	inline float* get_address_of_blur_10() { return &___blur_10; }
	inline void set_blur_10(float value)
	{
		___blur_10 = value;
	}

	inline static int32_t get_offset_of_blurSpread_11() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___blurSpread_11)); }
	inline float get_blurSpread_11() const { return ___blurSpread_11; }
	inline float* get_address_of_blurSpread_11() { return &___blurSpread_11; }
	inline void set_blurSpread_11(float value)
	{
		___blurSpread_11 = value;
	}

	inline static int32_t get_offset_of_luminanceDependency_12() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___luminanceDependency_12)); }
	inline float get_luminanceDependency_12() const { return ___luminanceDependency_12; }
	inline float* get_address_of_luminanceDependency_12() { return &___luminanceDependency_12; }
	inline void set_luminanceDependency_12(float value)
	{
		___luminanceDependency_12 = value;
	}

	inline static int32_t get_offset_of_blurDistance_13() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___blurDistance_13)); }
	inline float get_blurDistance_13() const { return ___blurDistance_13; }
	inline float* get_address_of_blurDistance_13() { return &___blurDistance_13; }
	inline void set_blurDistance_13(float value)
	{
		___blurDistance_13 = value;
	}

	inline static int32_t get_offset_of_vignetteShader_14() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___vignetteShader_14)); }
	inline Shader_t1881769421 * get_vignetteShader_14() const { return ___vignetteShader_14; }
	inline Shader_t1881769421 ** get_address_of_vignetteShader_14() { return &___vignetteShader_14; }
	inline void set_vignetteShader_14(Shader_t1881769421 * value)
	{
		___vignetteShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteShader_14), value);
	}

	inline static int32_t get_offset_of_separableBlurShader_15() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___separableBlurShader_15)); }
	inline Shader_t1881769421 * get_separableBlurShader_15() const { return ___separableBlurShader_15; }
	inline Shader_t1881769421 ** get_address_of_separableBlurShader_15() { return &___separableBlurShader_15; }
	inline void set_separableBlurShader_15(Shader_t1881769421 * value)
	{
		___separableBlurShader_15 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_15), value);
	}

	inline static int32_t get_offset_of_chromAberrationShader_16() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___chromAberrationShader_16)); }
	inline Shader_t1881769421 * get_chromAberrationShader_16() const { return ___chromAberrationShader_16; }
	inline Shader_t1881769421 ** get_address_of_chromAberrationShader_16() { return &___chromAberrationShader_16; }
	inline void set_chromAberrationShader_16(Shader_t1881769421 * value)
	{
		___chromAberrationShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___chromAberrationShader_16), value);
	}

	inline static int32_t get_offset_of_m_VignetteMaterial_17() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___m_VignetteMaterial_17)); }
	inline Material_t2815264910 * get_m_VignetteMaterial_17() const { return ___m_VignetteMaterial_17; }
	inline Material_t2815264910 ** get_address_of_m_VignetteMaterial_17() { return &___m_VignetteMaterial_17; }
	inline void set_m_VignetteMaterial_17(Material_t2815264910 * value)
	{
		___m_VignetteMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_VignetteMaterial_17), value);
	}

	inline static int32_t get_offset_of_m_SeparableBlurMaterial_18() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___m_SeparableBlurMaterial_18)); }
	inline Material_t2815264910 * get_m_SeparableBlurMaterial_18() const { return ___m_SeparableBlurMaterial_18; }
	inline Material_t2815264910 ** get_address_of_m_SeparableBlurMaterial_18() { return &___m_SeparableBlurMaterial_18; }
	inline void set_m_SeparableBlurMaterial_18(Material_t2815264910 * value)
	{
		___m_SeparableBlurMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_SeparableBlurMaterial_18), value);
	}

	inline static int32_t get_offset_of_m_ChromAberrationMaterial_19() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t137156492, ___m_ChromAberrationMaterial_19)); }
	inline Material_t2815264910 * get_m_ChromAberrationMaterial_19() const { return ___m_ChromAberrationMaterial_19; }
	inline Material_t2815264910 ** get_address_of_m_ChromAberrationMaterial_19() { return &___m_ChromAberrationMaterial_19; }
	inline void set_m_ChromAberrationMaterial_19(Material_t2815264910 * value)
	{
		___m_ChromAberrationMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChromAberrationMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEANDCHROMATICABERRATION_T137156492_H
#ifndef SCREENOVERLAY_T662123177_H
#define SCREENOVERLAY_T662123177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay
struct  ScreenOverlay_t662123177  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode UnityStandardAssets.ImageEffects.ScreenOverlay::blendMode
	int32_t ___blendMode_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenOverlay::intensity
	float ___intensity_7;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenOverlay::texture
	Texture2D_t3063074017 * ___texture_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenOverlay::overlayShader
	Shader_t1881769421 * ___overlayShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenOverlay::overlayMaterial
	Material_t2815264910 * ___overlayMaterial_10;

public:
	inline static int32_t get_offset_of_blendMode_6() { return static_cast<int32_t>(offsetof(ScreenOverlay_t662123177, ___blendMode_6)); }
	inline int32_t get_blendMode_6() const { return ___blendMode_6; }
	inline int32_t* get_address_of_blendMode_6() { return &___blendMode_6; }
	inline void set_blendMode_6(int32_t value)
	{
		___blendMode_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(ScreenOverlay_t662123177, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_texture_8() { return static_cast<int32_t>(offsetof(ScreenOverlay_t662123177, ___texture_8)); }
	inline Texture2D_t3063074017 * get_texture_8() const { return ___texture_8; }
	inline Texture2D_t3063074017 ** get_address_of_texture_8() { return &___texture_8; }
	inline void set_texture_8(Texture2D_t3063074017 * value)
	{
		___texture_8 = value;
		Il2CppCodeGenWriteBarrier((&___texture_8), value);
	}

	inline static int32_t get_offset_of_overlayShader_9() { return static_cast<int32_t>(offsetof(ScreenOverlay_t662123177, ___overlayShader_9)); }
	inline Shader_t1881769421 * get_overlayShader_9() const { return ___overlayShader_9; }
	inline Shader_t1881769421 ** get_address_of_overlayShader_9() { return &___overlayShader_9; }
	inline void set_overlayShader_9(Shader_t1881769421 * value)
	{
		___overlayShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___overlayShader_9), value);
	}

	inline static int32_t get_offset_of_overlayMaterial_10() { return static_cast<int32_t>(offsetof(ScreenOverlay_t662123177, ___overlayMaterial_10)); }
	inline Material_t2815264910 * get_overlayMaterial_10() const { return ___overlayMaterial_10; }
	inline Material_t2815264910 ** get_address_of_overlayMaterial_10() { return &___overlayMaterial_10; }
	inline void set_overlayMaterial_10(Material_t2815264910 * value)
	{
		___overlayMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___overlayMaterial_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENOVERLAY_T662123177_H
#ifndef CINEMACHINEEXTERNALCAMERA_T3112248349_H
#define CINEMACHINEEXTERNALCAMERA_T3112248349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineExternalCamera
struct  CinemachineExternalCamera_t3112248349  : public CinemachineVirtualCameraBase_t2557508663
{
public:
	// UnityEngine.Camera Cinemachine.CinemachineExternalCamera::m_Camera
	Camera_t2839736942 * ___m_Camera_12;
	// Cinemachine.CameraState Cinemachine.CinemachineExternalCamera::m_State
	CameraState_t382403230  ___m_State_13;
	// UnityEngine.Transform Cinemachine.CinemachineExternalCamera::<LookAt>k__BackingField
	Transform_t362059596 * ___U3CLookAtU3Ek__BackingField_14;
	// UnityEngine.Transform Cinemachine.CinemachineExternalCamera::<Follow>k__BackingField
	Transform_t362059596 * ___U3CFollowU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_m_Camera_12() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3112248349, ___m_Camera_12)); }
	inline Camera_t2839736942 * get_m_Camera_12() const { return ___m_Camera_12; }
	inline Camera_t2839736942 ** get_address_of_m_Camera_12() { return &___m_Camera_12; }
	inline void set_m_Camera_12(Camera_t2839736942 * value)
	{
		___m_Camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_12), value);
	}

	inline static int32_t get_offset_of_m_State_13() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3112248349, ___m_State_13)); }
	inline CameraState_t382403230  get_m_State_13() const { return ___m_State_13; }
	inline CameraState_t382403230 * get_address_of_m_State_13() { return &___m_State_13; }
	inline void set_m_State_13(CameraState_t382403230  value)
	{
		___m_State_13 = value;
	}

	inline static int32_t get_offset_of_U3CLookAtU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3112248349, ___U3CLookAtU3Ek__BackingField_14)); }
	inline Transform_t362059596 * get_U3CLookAtU3Ek__BackingField_14() const { return ___U3CLookAtU3Ek__BackingField_14; }
	inline Transform_t362059596 ** get_address_of_U3CLookAtU3Ek__BackingField_14() { return &___U3CLookAtU3Ek__BackingField_14; }
	inline void set_U3CLookAtU3Ek__BackingField_14(Transform_t362059596 * value)
	{
		___U3CLookAtU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLookAtU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3112248349, ___U3CFollowU3Ek__BackingField_15)); }
	inline Transform_t362059596 * get_U3CFollowU3Ek__BackingField_15() const { return ___U3CFollowU3Ek__BackingField_15; }
	inline Transform_t362059596 ** get_address_of_U3CFollowU3Ek__BackingField_15() { return &___U3CFollowU3Ek__BackingField_15; }
	inline void set_U3CFollowU3Ek__BackingField_15(Transform_t362059596 * value)
	{
		___U3CFollowU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFollowU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEEXTERNALCAMERA_T3112248349_H
#ifndef SCREENSPACEAMBIENTOBSCURANCE_T3449985455_H
#define SCREENSPACEAMBIENTOBSCURANCE_T3449985455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
struct  ScreenSpaceAmbientObscurance_t3449985455  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::intensity
	float ___intensity_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::radius
	float ___radius_7;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurIterations
	int32_t ___blurIterations_8;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurFilterDistance
	float ___blurFilterDistance_9;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::downsample
	int32_t ___downsample_10;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::rand
	Texture2D_t3063074017 * ___rand_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoShader
	Shader_t1881769421 * ___aoShader_12;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoMaterial
	Material_t2815264910 * ___aoMaterial_13;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___radius_7)); }
	inline float get_radius_7() const { return ___radius_7; }
	inline float* get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(float value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_blurIterations_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___blurIterations_8)); }
	inline int32_t get_blurIterations_8() const { return ___blurIterations_8; }
	inline int32_t* get_address_of_blurIterations_8() { return &___blurIterations_8; }
	inline void set_blurIterations_8(int32_t value)
	{
		___blurIterations_8 = value;
	}

	inline static int32_t get_offset_of_blurFilterDistance_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___blurFilterDistance_9)); }
	inline float get_blurFilterDistance_9() const { return ___blurFilterDistance_9; }
	inline float* get_address_of_blurFilterDistance_9() { return &___blurFilterDistance_9; }
	inline void set_blurFilterDistance_9(float value)
	{
		___blurFilterDistance_9 = value;
	}

	inline static int32_t get_offset_of_downsample_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___downsample_10)); }
	inline int32_t get_downsample_10() const { return ___downsample_10; }
	inline int32_t* get_address_of_downsample_10() { return &___downsample_10; }
	inline void set_downsample_10(int32_t value)
	{
		___downsample_10 = value;
	}

	inline static int32_t get_offset_of_rand_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___rand_11)); }
	inline Texture2D_t3063074017 * get_rand_11() const { return ___rand_11; }
	inline Texture2D_t3063074017 ** get_address_of_rand_11() { return &___rand_11; }
	inline void set_rand_11(Texture2D_t3063074017 * value)
	{
		___rand_11 = value;
		Il2CppCodeGenWriteBarrier((&___rand_11), value);
	}

	inline static int32_t get_offset_of_aoShader_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___aoShader_12)); }
	inline Shader_t1881769421 * get_aoShader_12() const { return ___aoShader_12; }
	inline Shader_t1881769421 ** get_address_of_aoShader_12() { return &___aoShader_12; }
	inline void set_aoShader_12(Shader_t1881769421 * value)
	{
		___aoShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___aoShader_12), value);
	}

	inline static int32_t get_offset_of_aoMaterial_13() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t3449985455, ___aoMaterial_13)); }
	inline Material_t2815264910 * get_aoMaterial_13() const { return ___aoMaterial_13; }
	inline Material_t2815264910 ** get_address_of_aoMaterial_13() { return &___aoMaterial_13; }
	inline void set_aoMaterial_13(Material_t2815264910 * value)
	{
		___aoMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___aoMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOBSCURANCE_T3449985455_H
#ifndef CINEMACHINEVIRTUALCAMERA_T2803177733_H
#define CINEMACHINEVIRTUALCAMERA_T2803177733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera
struct  CinemachineVirtualCamera_t2803177733  : public CinemachineVirtualCameraBase_t2557508663
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_LookAt
	Transform_t362059596 * ___m_LookAt_12;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_Follow
	Transform_t362059596 * ___m_Follow_13;
	// Cinemachine.LensSettings Cinemachine.CinemachineVirtualCamera::m_Lens
	LensSettings_t1505430577  ___m_Lens_14;
	// System.Boolean Cinemachine.CinemachineVirtualCamera::<SuppressOrientationUpdate>k__BackingField
	bool ___U3CSuppressOrientationUpdateU3Ek__BackingField_18;
	// Cinemachine.CameraState Cinemachine.CinemachineVirtualCamera::m_State
	CameraState_t382403230  ___m_State_19;
	// Cinemachine.CameraState Cinemachine.CinemachineVirtualCamera::m_PreviousState
	CameraState_t382403230  ___m_PreviousState_20;
	// Cinemachine.ICinemachineComponent[] Cinemachine.CinemachineVirtualCamera::m_ComponentPipeline
	ICinemachineComponentU5BU5D_t3125037657* ___m_ComponentPipeline_21;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_ComponentOwner
	Transform_t362059596 * ___m_ComponentOwner_22;

public:
	inline static int32_t get_offset_of_m_LookAt_12() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_LookAt_12)); }
	inline Transform_t362059596 * get_m_LookAt_12() const { return ___m_LookAt_12; }
	inline Transform_t362059596 ** get_address_of_m_LookAt_12() { return &___m_LookAt_12; }
	inline void set_m_LookAt_12(Transform_t362059596 * value)
	{
		___m_LookAt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_12), value);
	}

	inline static int32_t get_offset_of_m_Follow_13() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_Follow_13)); }
	inline Transform_t362059596 * get_m_Follow_13() const { return ___m_Follow_13; }
	inline Transform_t362059596 ** get_address_of_m_Follow_13() { return &___m_Follow_13; }
	inline void set_m_Follow_13(Transform_t362059596 * value)
	{
		___m_Follow_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_13), value);
	}

	inline static int32_t get_offset_of_m_Lens_14() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_Lens_14)); }
	inline LensSettings_t1505430577  get_m_Lens_14() const { return ___m_Lens_14; }
	inline LensSettings_t1505430577 * get_address_of_m_Lens_14() { return &___m_Lens_14; }
	inline void set_m_Lens_14(LensSettings_t1505430577  value)
	{
		___m_Lens_14 = value;
	}

	inline static int32_t get_offset_of_U3CSuppressOrientationUpdateU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___U3CSuppressOrientationUpdateU3Ek__BackingField_18)); }
	inline bool get_U3CSuppressOrientationUpdateU3Ek__BackingField_18() const { return ___U3CSuppressOrientationUpdateU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CSuppressOrientationUpdateU3Ek__BackingField_18() { return &___U3CSuppressOrientationUpdateU3Ek__BackingField_18; }
	inline void set_U3CSuppressOrientationUpdateU3Ek__BackingField_18(bool value)
	{
		___U3CSuppressOrientationUpdateU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_State_19() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_State_19)); }
	inline CameraState_t382403230  get_m_State_19() const { return ___m_State_19; }
	inline CameraState_t382403230 * get_address_of_m_State_19() { return &___m_State_19; }
	inline void set_m_State_19(CameraState_t382403230  value)
	{
		___m_State_19 = value;
	}

	inline static int32_t get_offset_of_m_PreviousState_20() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_PreviousState_20)); }
	inline CameraState_t382403230  get_m_PreviousState_20() const { return ___m_PreviousState_20; }
	inline CameraState_t382403230 * get_address_of_m_PreviousState_20() { return &___m_PreviousState_20; }
	inline void set_m_PreviousState_20(CameraState_t382403230  value)
	{
		___m_PreviousState_20 = value;
	}

	inline static int32_t get_offset_of_m_ComponentPipeline_21() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_ComponentPipeline_21)); }
	inline ICinemachineComponentU5BU5D_t3125037657* get_m_ComponentPipeline_21() const { return ___m_ComponentPipeline_21; }
	inline ICinemachineComponentU5BU5D_t3125037657** get_address_of_m_ComponentPipeline_21() { return &___m_ComponentPipeline_21; }
	inline void set_m_ComponentPipeline_21(ICinemachineComponentU5BU5D_t3125037657* value)
	{
		___m_ComponentPipeline_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentPipeline_21), value);
	}

	inline static int32_t get_offset_of_m_ComponentOwner_22() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733, ___m_ComponentOwner_22)); }
	inline Transform_t362059596 * get_m_ComponentOwner_22() const { return ___m_ComponentOwner_22; }
	inline Transform_t362059596 ** get_address_of_m_ComponentOwner_22() { return &___m_ComponentOwner_22; }
	inline void set_m_ComponentOwner_22(Transform_t362059596 * value)
	{
		___m_ComponentOwner_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentOwner_22), value);
	}
};

struct CinemachineVirtualCamera_t2803177733_StaticFields
{
public:
	// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate Cinemachine.CinemachineVirtualCamera::CreatePipelineOverride
	CreatePipelineDelegate_t4206421982 * ___CreatePipelineOverride_16;
	// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate Cinemachine.CinemachineVirtualCamera::DestroyPipelineOverride
	DestroyPipelineDelegate_t926775217 * ___DestroyPipelineOverride_17;
	// System.Comparison`1<Cinemachine.ICinemachineComponent> Cinemachine.CinemachineVirtualCamera::<>f__am$cache0
	Comparison_1_t639385221 * ___U3CU3Ef__amU24cache0_23;

public:
	inline static int32_t get_offset_of_CreatePipelineOverride_16() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733_StaticFields, ___CreatePipelineOverride_16)); }
	inline CreatePipelineDelegate_t4206421982 * get_CreatePipelineOverride_16() const { return ___CreatePipelineOverride_16; }
	inline CreatePipelineDelegate_t4206421982 ** get_address_of_CreatePipelineOverride_16() { return &___CreatePipelineOverride_16; }
	inline void set_CreatePipelineOverride_16(CreatePipelineDelegate_t4206421982 * value)
	{
		___CreatePipelineOverride_16 = value;
		Il2CppCodeGenWriteBarrier((&___CreatePipelineOverride_16), value);
	}

	inline static int32_t get_offset_of_DestroyPipelineOverride_17() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733_StaticFields, ___DestroyPipelineOverride_17)); }
	inline DestroyPipelineDelegate_t926775217 * get_DestroyPipelineOverride_17() const { return ___DestroyPipelineOverride_17; }
	inline DestroyPipelineDelegate_t926775217 ** get_address_of_DestroyPipelineOverride_17() { return &___DestroyPipelineOverride_17; }
	inline void set_DestroyPipelineOverride_17(DestroyPipelineDelegate_t926775217 * value)
	{
		___DestroyPipelineOverride_17 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyPipelineOverride_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_23() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t2803177733_StaticFields, ___U3CU3Ef__amU24cache0_23)); }
	inline Comparison_1_t639385221 * get_U3CU3Ef__amU24cache0_23() const { return ___U3CU3Ef__amU24cache0_23; }
	inline Comparison_1_t639385221 ** get_address_of_U3CU3Ef__amU24cache0_23() { return &___U3CU3Ef__amU24cache0_23; }
	inline void set_U3CU3Ef__amU24cache0_23(Comparison_1_t639385221 * value)
	{
		___U3CU3Ef__amU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEVIRTUALCAMERA_T2803177733_H
#ifndef CINEMACHINEFREELOOK_T411363320_H
#define CINEMACHINEFREELOOK_T411363320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook
struct  CinemachineFreeLook_t411363320  : public CinemachineVirtualCameraBase_t2557508663
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_LookAt
	Transform_t362059596 * ___m_LookAt_12;
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_Follow
	Transform_t362059596 * ___m_Follow_13;
	// Cinemachine.CinemachineOrbitalTransposer/DampingStyle Cinemachine.CinemachineFreeLook::m_DampingStyle
	int32_t ___m_DampingStyle_14;
	// System.Single Cinemachine.CinemachineFreeLook::m_HeadingBias
	float ___m_HeadingBias_15;
	// System.Boolean Cinemachine.CinemachineFreeLook::m_UseCommonLensSetting
	bool ___m_UseCommonLensSetting_16;
	// Cinemachine.LensSettings Cinemachine.CinemachineFreeLook::m_Lens
	LensSettings_t1505430577  ___m_Lens_17;
	// Cinemachine.CinemachineOrbitalTransposer/AxisState Cinemachine.CinemachineFreeLook::m_XAxis
	AxisState_t2333558477  ___m_XAxis_18;
	// Cinemachine.CinemachineOrbitalTransposer/AxisState Cinemachine.CinemachineFreeLook::m_YAxis
	AxisState_t2333558477  ___m_YAxis_19;
	// Cinemachine.CinemachineOrbitalTransposer/Recentering Cinemachine.CinemachineFreeLook::m_RecenterToTargetHeading
	Recentering_t75128244  ___m_RecenterToTargetHeading_20;
	// System.Single Cinemachine.CinemachineFreeLook::m_SplineTension
	float ___m_SplineTension_21;
	// Cinemachine.CameraState Cinemachine.CinemachineFreeLook::m_State
	CameraState_t382403230  ___m_State_22;
	// Cinemachine.CinemachineVirtualCamera[] Cinemachine.CinemachineFreeLook::m_Rigs
	CinemachineVirtualCameraU5BU5D_t1136890184* ___m_Rigs_23;
	// Cinemachine.CinemachineOrbitalTransposer[] Cinemachine.CinemachineFreeLook::mOribitals
	CinemachineOrbitalTransposerU5BU5D_t1933852220* ___mOribitals_24;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendA
	CinemachineBlend_t2146485918 * ___mBlendA_25;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendB
	CinemachineBlend_t2146485918 * ___mBlendB_26;

public:
	inline static int32_t get_offset_of_m_LookAt_12() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_LookAt_12)); }
	inline Transform_t362059596 * get_m_LookAt_12() const { return ___m_LookAt_12; }
	inline Transform_t362059596 ** get_address_of_m_LookAt_12() { return &___m_LookAt_12; }
	inline void set_m_LookAt_12(Transform_t362059596 * value)
	{
		___m_LookAt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_12), value);
	}

	inline static int32_t get_offset_of_m_Follow_13() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_Follow_13)); }
	inline Transform_t362059596 * get_m_Follow_13() const { return ___m_Follow_13; }
	inline Transform_t362059596 ** get_address_of_m_Follow_13() { return &___m_Follow_13; }
	inline void set_m_Follow_13(Transform_t362059596 * value)
	{
		___m_Follow_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_13), value);
	}

	inline static int32_t get_offset_of_m_DampingStyle_14() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_DampingStyle_14)); }
	inline int32_t get_m_DampingStyle_14() const { return ___m_DampingStyle_14; }
	inline int32_t* get_address_of_m_DampingStyle_14() { return &___m_DampingStyle_14; }
	inline void set_m_DampingStyle_14(int32_t value)
	{
		___m_DampingStyle_14 = value;
	}

	inline static int32_t get_offset_of_m_HeadingBias_15() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_HeadingBias_15)); }
	inline float get_m_HeadingBias_15() const { return ___m_HeadingBias_15; }
	inline float* get_address_of_m_HeadingBias_15() { return &___m_HeadingBias_15; }
	inline void set_m_HeadingBias_15(float value)
	{
		___m_HeadingBias_15 = value;
	}

	inline static int32_t get_offset_of_m_UseCommonLensSetting_16() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_UseCommonLensSetting_16)); }
	inline bool get_m_UseCommonLensSetting_16() const { return ___m_UseCommonLensSetting_16; }
	inline bool* get_address_of_m_UseCommonLensSetting_16() { return &___m_UseCommonLensSetting_16; }
	inline void set_m_UseCommonLensSetting_16(bool value)
	{
		___m_UseCommonLensSetting_16 = value;
	}

	inline static int32_t get_offset_of_m_Lens_17() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_Lens_17)); }
	inline LensSettings_t1505430577  get_m_Lens_17() const { return ___m_Lens_17; }
	inline LensSettings_t1505430577 * get_address_of_m_Lens_17() { return &___m_Lens_17; }
	inline void set_m_Lens_17(LensSettings_t1505430577  value)
	{
		___m_Lens_17 = value;
	}

	inline static int32_t get_offset_of_m_XAxis_18() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_XAxis_18)); }
	inline AxisState_t2333558477  get_m_XAxis_18() const { return ___m_XAxis_18; }
	inline AxisState_t2333558477 * get_address_of_m_XAxis_18() { return &___m_XAxis_18; }
	inline void set_m_XAxis_18(AxisState_t2333558477  value)
	{
		___m_XAxis_18 = value;
	}

	inline static int32_t get_offset_of_m_YAxis_19() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_YAxis_19)); }
	inline AxisState_t2333558477  get_m_YAxis_19() const { return ___m_YAxis_19; }
	inline AxisState_t2333558477 * get_address_of_m_YAxis_19() { return &___m_YAxis_19; }
	inline void set_m_YAxis_19(AxisState_t2333558477  value)
	{
		___m_YAxis_19 = value;
	}

	inline static int32_t get_offset_of_m_RecenterToTargetHeading_20() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_RecenterToTargetHeading_20)); }
	inline Recentering_t75128244  get_m_RecenterToTargetHeading_20() const { return ___m_RecenterToTargetHeading_20; }
	inline Recentering_t75128244 * get_address_of_m_RecenterToTargetHeading_20() { return &___m_RecenterToTargetHeading_20; }
	inline void set_m_RecenterToTargetHeading_20(Recentering_t75128244  value)
	{
		___m_RecenterToTargetHeading_20 = value;
	}

	inline static int32_t get_offset_of_m_SplineTension_21() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_SplineTension_21)); }
	inline float get_m_SplineTension_21() const { return ___m_SplineTension_21; }
	inline float* get_address_of_m_SplineTension_21() { return &___m_SplineTension_21; }
	inline void set_m_SplineTension_21(float value)
	{
		___m_SplineTension_21 = value;
	}

	inline static int32_t get_offset_of_m_State_22() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_State_22)); }
	inline CameraState_t382403230  get_m_State_22() const { return ___m_State_22; }
	inline CameraState_t382403230 * get_address_of_m_State_22() { return &___m_State_22; }
	inline void set_m_State_22(CameraState_t382403230  value)
	{
		___m_State_22 = value;
	}

	inline static int32_t get_offset_of_m_Rigs_23() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___m_Rigs_23)); }
	inline CinemachineVirtualCameraU5BU5D_t1136890184* get_m_Rigs_23() const { return ___m_Rigs_23; }
	inline CinemachineVirtualCameraU5BU5D_t1136890184** get_address_of_m_Rigs_23() { return &___m_Rigs_23; }
	inline void set_m_Rigs_23(CinemachineVirtualCameraU5BU5D_t1136890184* value)
	{
		___m_Rigs_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigs_23), value);
	}

	inline static int32_t get_offset_of_mOribitals_24() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___mOribitals_24)); }
	inline CinemachineOrbitalTransposerU5BU5D_t1933852220* get_mOribitals_24() const { return ___mOribitals_24; }
	inline CinemachineOrbitalTransposerU5BU5D_t1933852220** get_address_of_mOribitals_24() { return &___mOribitals_24; }
	inline void set_mOribitals_24(CinemachineOrbitalTransposerU5BU5D_t1933852220* value)
	{
		___mOribitals_24 = value;
		Il2CppCodeGenWriteBarrier((&___mOribitals_24), value);
	}

	inline static int32_t get_offset_of_mBlendA_25() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___mBlendA_25)); }
	inline CinemachineBlend_t2146485918 * get_mBlendA_25() const { return ___mBlendA_25; }
	inline CinemachineBlend_t2146485918 ** get_address_of_mBlendA_25() { return &___mBlendA_25; }
	inline void set_mBlendA_25(CinemachineBlend_t2146485918 * value)
	{
		___mBlendA_25 = value;
		Il2CppCodeGenWriteBarrier((&___mBlendA_25), value);
	}

	inline static int32_t get_offset_of_mBlendB_26() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320, ___mBlendB_26)); }
	inline CinemachineBlend_t2146485918 * get_mBlendB_26() const { return ___mBlendB_26; }
	inline CinemachineBlend_t2146485918 ** get_address_of_mBlendB_26() { return &___mBlendB_26; }
	inline void set_mBlendB_26(CinemachineBlend_t2146485918 * value)
	{
		___mBlendB_26 = value;
		Il2CppCodeGenWriteBarrier((&___mBlendB_26), value);
	}
};

struct CinemachineFreeLook_t411363320_StaticFields
{
public:
	// Cinemachine.CinemachineFreeLook/CreateRigDelegate Cinemachine.CinemachineFreeLook::CreateRigOverride
	CreateRigDelegate_t931026514 * ___CreateRigOverride_27;
	// Cinemachine.CinemachineFreeLook/DestroyRigDelegate Cinemachine.CinemachineFreeLook::DestroyRigOverride
	DestroyRigDelegate_t2622870986 * ___DestroyRigOverride_28;

public:
	inline static int32_t get_offset_of_CreateRigOverride_27() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320_StaticFields, ___CreateRigOverride_27)); }
	inline CreateRigDelegate_t931026514 * get_CreateRigOverride_27() const { return ___CreateRigOverride_27; }
	inline CreateRigDelegate_t931026514 ** get_address_of_CreateRigOverride_27() { return &___CreateRigOverride_27; }
	inline void set_CreateRigOverride_27(CreateRigDelegate_t931026514 * value)
	{
		___CreateRigOverride_27 = value;
		Il2CppCodeGenWriteBarrier((&___CreateRigOverride_27), value);
	}

	inline static int32_t get_offset_of_DestroyRigOverride_28() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t411363320_StaticFields, ___DestroyRigOverride_28)); }
	inline DestroyRigDelegate_t2622870986 * get_DestroyRigOverride_28() const { return ___DestroyRigOverride_28; }
	inline DestroyRigDelegate_t2622870986 ** get_address_of_DestroyRigOverride_28() { return &___DestroyRigOverride_28; }
	inline void set_DestroyRigOverride_28(DestroyRigDelegate_t2622870986 * value)
	{
		___DestroyRigOverride_28 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyRigOverride_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEFREELOOK_T411363320_H
#ifndef SEPIATONE_T4284961859_H
#define SEPIATONE_T4284961859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SepiaTone
struct  SepiaTone_t4284961859  : public ImageEffectBase_t4133330248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEPIATONE_T4284961859_H
#ifndef SUNSHAFTS_T135682244_H
#define SUNSHAFTS_T135682244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts
struct  SunShafts_t135682244  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution UnityStandardAssets.ImageEffects.SunShafts::resolution
	int32_t ___resolution_6;
	// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode UnityStandardAssets.ImageEffects.SunShafts::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.SunShafts::sunTransform
	Transform_t362059596 * ___sunTransform_8;
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts::radialBlurIterations
	int32_t ___radialBlurIterations_9;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.SunShafts::sunColor
	Color_t2582018970  ___sunColor_10;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.SunShafts::sunThreshold
	Color_t2582018970  ___sunThreshold_11;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::sunShaftBlurRadius
	float ___sunShaftBlurRadius_12;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::sunShaftIntensity
	float ___sunShaftIntensity_13;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::maxRadius
	float ___maxRadius_14;
	// System.Boolean UnityStandardAssets.ImageEffects.SunShafts::useDepthTexture
	bool ___useDepthTexture_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.SunShafts::sunShaftsShader
	Shader_t1881769421 * ___sunShaftsShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.SunShafts::sunShaftsMaterial
	Material_t2815264910 * ___sunShaftsMaterial_17;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.SunShafts::simpleClearShader
	Shader_t1881769421 * ___simpleClearShader_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.SunShafts::simpleClearMaterial
	Material_t2815264910 * ___simpleClearMaterial_19;

public:
	inline static int32_t get_offset_of_resolution_6() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___resolution_6)); }
	inline int32_t get_resolution_6() const { return ___resolution_6; }
	inline int32_t* get_address_of_resolution_6() { return &___resolution_6; }
	inline void set_resolution_6(int32_t value)
	{
		___resolution_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_sunTransform_8() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunTransform_8)); }
	inline Transform_t362059596 * get_sunTransform_8() const { return ___sunTransform_8; }
	inline Transform_t362059596 ** get_address_of_sunTransform_8() { return &___sunTransform_8; }
	inline void set_sunTransform_8(Transform_t362059596 * value)
	{
		___sunTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___sunTransform_8), value);
	}

	inline static int32_t get_offset_of_radialBlurIterations_9() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___radialBlurIterations_9)); }
	inline int32_t get_radialBlurIterations_9() const { return ___radialBlurIterations_9; }
	inline int32_t* get_address_of_radialBlurIterations_9() { return &___radialBlurIterations_9; }
	inline void set_radialBlurIterations_9(int32_t value)
	{
		___radialBlurIterations_9 = value;
	}

	inline static int32_t get_offset_of_sunColor_10() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunColor_10)); }
	inline Color_t2582018970  get_sunColor_10() const { return ___sunColor_10; }
	inline Color_t2582018970 * get_address_of_sunColor_10() { return &___sunColor_10; }
	inline void set_sunColor_10(Color_t2582018970  value)
	{
		___sunColor_10 = value;
	}

	inline static int32_t get_offset_of_sunThreshold_11() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunThreshold_11)); }
	inline Color_t2582018970  get_sunThreshold_11() const { return ___sunThreshold_11; }
	inline Color_t2582018970 * get_address_of_sunThreshold_11() { return &___sunThreshold_11; }
	inline void set_sunThreshold_11(Color_t2582018970  value)
	{
		___sunThreshold_11 = value;
	}

	inline static int32_t get_offset_of_sunShaftBlurRadius_12() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunShaftBlurRadius_12)); }
	inline float get_sunShaftBlurRadius_12() const { return ___sunShaftBlurRadius_12; }
	inline float* get_address_of_sunShaftBlurRadius_12() { return &___sunShaftBlurRadius_12; }
	inline void set_sunShaftBlurRadius_12(float value)
	{
		___sunShaftBlurRadius_12 = value;
	}

	inline static int32_t get_offset_of_sunShaftIntensity_13() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunShaftIntensity_13)); }
	inline float get_sunShaftIntensity_13() const { return ___sunShaftIntensity_13; }
	inline float* get_address_of_sunShaftIntensity_13() { return &___sunShaftIntensity_13; }
	inline void set_sunShaftIntensity_13(float value)
	{
		___sunShaftIntensity_13 = value;
	}

	inline static int32_t get_offset_of_maxRadius_14() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___maxRadius_14)); }
	inline float get_maxRadius_14() const { return ___maxRadius_14; }
	inline float* get_address_of_maxRadius_14() { return &___maxRadius_14; }
	inline void set_maxRadius_14(float value)
	{
		___maxRadius_14 = value;
	}

	inline static int32_t get_offset_of_useDepthTexture_15() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___useDepthTexture_15)); }
	inline bool get_useDepthTexture_15() const { return ___useDepthTexture_15; }
	inline bool* get_address_of_useDepthTexture_15() { return &___useDepthTexture_15; }
	inline void set_useDepthTexture_15(bool value)
	{
		___useDepthTexture_15 = value;
	}

	inline static int32_t get_offset_of_sunShaftsShader_16() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunShaftsShader_16)); }
	inline Shader_t1881769421 * get_sunShaftsShader_16() const { return ___sunShaftsShader_16; }
	inline Shader_t1881769421 ** get_address_of_sunShaftsShader_16() { return &___sunShaftsShader_16; }
	inline void set_sunShaftsShader_16(Shader_t1881769421 * value)
	{
		___sunShaftsShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___sunShaftsShader_16), value);
	}

	inline static int32_t get_offset_of_sunShaftsMaterial_17() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___sunShaftsMaterial_17)); }
	inline Material_t2815264910 * get_sunShaftsMaterial_17() const { return ___sunShaftsMaterial_17; }
	inline Material_t2815264910 ** get_address_of_sunShaftsMaterial_17() { return &___sunShaftsMaterial_17; }
	inline void set_sunShaftsMaterial_17(Material_t2815264910 * value)
	{
		___sunShaftsMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___sunShaftsMaterial_17), value);
	}

	inline static int32_t get_offset_of_simpleClearShader_18() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___simpleClearShader_18)); }
	inline Shader_t1881769421 * get_simpleClearShader_18() const { return ___simpleClearShader_18; }
	inline Shader_t1881769421 ** get_address_of_simpleClearShader_18() { return &___simpleClearShader_18; }
	inline void set_simpleClearShader_18(Shader_t1881769421 * value)
	{
		___simpleClearShader_18 = value;
		Il2CppCodeGenWriteBarrier((&___simpleClearShader_18), value);
	}

	inline static int32_t get_offset_of_simpleClearMaterial_19() { return static_cast<int32_t>(offsetof(SunShafts_t135682244, ___simpleClearMaterial_19)); }
	inline Material_t2815264910 * get_simpleClearMaterial_19() const { return ___simpleClearMaterial_19; }
	inline Material_t2815264910 ** get_address_of_simpleClearMaterial_19() { return &___simpleClearMaterial_19; }
	inline void set_simpleClearMaterial_19(Material_t2815264910 * value)
	{
		___simpleClearMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___simpleClearMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUNSHAFTS_T135682244_H
#ifndef CINEMACHINEPATH_T3941522772_H
#define CINEMACHINEPATH_T3941522772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePath
struct  CinemachinePath_t3941522772  : public CinemachinePathBase_t3902360541
{
public:
	// Cinemachine.CinemachinePath/Appearance Cinemachine.CinemachinePath::m_Appearance
	Appearance_t955816971 * ___m_Appearance_2;
	// System.Boolean Cinemachine.CinemachinePath::m_Looped
	bool ___m_Looped_3;
	// Cinemachine.CinemachinePath/Waypoint[] Cinemachine.CinemachinePath::m_Waypoints
	WaypointU5BU5D_t106678226* ___m_Waypoints_4;

public:
	inline static int32_t get_offset_of_m_Appearance_2() { return static_cast<int32_t>(offsetof(CinemachinePath_t3941522772, ___m_Appearance_2)); }
	inline Appearance_t955816971 * get_m_Appearance_2() const { return ___m_Appearance_2; }
	inline Appearance_t955816971 ** get_address_of_m_Appearance_2() { return &___m_Appearance_2; }
	inline void set_m_Appearance_2(Appearance_t955816971 * value)
	{
		___m_Appearance_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Appearance_2), value);
	}

	inline static int32_t get_offset_of_m_Looped_3() { return static_cast<int32_t>(offsetof(CinemachinePath_t3941522772, ___m_Looped_3)); }
	inline bool get_m_Looped_3() const { return ___m_Looped_3; }
	inline bool* get_address_of_m_Looped_3() { return &___m_Looped_3; }
	inline void set_m_Looped_3(bool value)
	{
		___m_Looped_3 = value;
	}

	inline static int32_t get_offset_of_m_Waypoints_4() { return static_cast<int32_t>(offsetof(CinemachinePath_t3941522772, ___m_Waypoints_4)); }
	inline WaypointU5BU5D_t106678226* get_m_Waypoints_4() const { return ___m_Waypoints_4; }
	inline WaypointU5BU5D_t106678226** get_address_of_m_Waypoints_4() { return &___m_Waypoints_4; }
	inline void set_m_Waypoints_4(WaypointU5BU5D_t106678226* value)
	{
		___m_Waypoints_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Waypoints_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPATH_T3941522772_H
#ifndef TILTSHIFT_T4026002340_H
#define TILTSHIFT_T4026002340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift
struct  TiltShift_t4026002340  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode UnityStandardAssets.ImageEffects.TiltShift::mode
	int32_t ___mode_6;
	// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality UnityStandardAssets.ImageEffects.TiltShift::quality
	int32_t ___quality_7;
	// System.Single UnityStandardAssets.ImageEffects.TiltShift::blurArea
	float ___blurArea_8;
	// System.Single UnityStandardAssets.ImageEffects.TiltShift::maxBlurSize
	float ___maxBlurSize_9;
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift::downsample
	int32_t ___downsample_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.TiltShift::tiltShiftShader
	Shader_t1881769421 * ___tiltShiftShader_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.TiltShift::tiltShiftMaterial
	Material_t2815264910 * ___tiltShiftMaterial_12;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_quality_7() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___quality_7)); }
	inline int32_t get_quality_7() const { return ___quality_7; }
	inline int32_t* get_address_of_quality_7() { return &___quality_7; }
	inline void set_quality_7(int32_t value)
	{
		___quality_7 = value;
	}

	inline static int32_t get_offset_of_blurArea_8() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___blurArea_8)); }
	inline float get_blurArea_8() const { return ___blurArea_8; }
	inline float* get_address_of_blurArea_8() { return &___blurArea_8; }
	inline void set_blurArea_8(float value)
	{
		___blurArea_8 = value;
	}

	inline static int32_t get_offset_of_maxBlurSize_9() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___maxBlurSize_9)); }
	inline float get_maxBlurSize_9() const { return ___maxBlurSize_9; }
	inline float* get_address_of_maxBlurSize_9() { return &___maxBlurSize_9; }
	inline void set_maxBlurSize_9(float value)
	{
		___maxBlurSize_9 = value;
	}

	inline static int32_t get_offset_of_downsample_10() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___downsample_10)); }
	inline int32_t get_downsample_10() const { return ___downsample_10; }
	inline int32_t* get_address_of_downsample_10() { return &___downsample_10; }
	inline void set_downsample_10(int32_t value)
	{
		___downsample_10 = value;
	}

	inline static int32_t get_offset_of_tiltShiftShader_11() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___tiltShiftShader_11)); }
	inline Shader_t1881769421 * get_tiltShiftShader_11() const { return ___tiltShiftShader_11; }
	inline Shader_t1881769421 ** get_address_of_tiltShiftShader_11() { return &___tiltShiftShader_11; }
	inline void set_tiltShiftShader_11(Shader_t1881769421 * value)
	{
		___tiltShiftShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___tiltShiftShader_11), value);
	}

	inline static int32_t get_offset_of_tiltShiftMaterial_12() { return static_cast<int32_t>(offsetof(TiltShift_t4026002340, ___tiltShiftMaterial_12)); }
	inline Material_t2815264910 * get_tiltShiftMaterial_12() const { return ___tiltShiftMaterial_12; }
	inline Material_t2815264910 ** get_address_of_tiltShiftMaterial_12() { return &___tiltShiftMaterial_12; }
	inline void set_tiltShiftMaterial_12(Material_t2815264910 * value)
	{
		___tiltShiftMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___tiltShiftMaterial_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFT_T4026002340_H
#ifndef CINEMACHINESTATEDRIVENCAMERA_T2857997874_H
#define CINEMACHINESTATEDRIVENCAMERA_T2857997874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera
struct  CinemachineStateDrivenCamera_t2857997874  : public CinemachineVirtualCameraBase_t2557508663
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineStateDrivenCamera::m_LookAt
	Transform_t362059596 * ___m_LookAt_12;
	// UnityEngine.Transform Cinemachine.CinemachineStateDrivenCamera::m_Follow
	Transform_t362059596 * ___m_Follow_13;
	// System.Boolean Cinemachine.CinemachineStateDrivenCamera::m_ShowDebugText
	bool ___m_ShowDebugText_14;
	// System.Boolean Cinemachine.CinemachineStateDrivenCamera::m_EnableAllChildCameras
	bool ___m_EnableAllChildCameras_15;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineStateDrivenCamera::m_ChildCameras
	CinemachineVirtualCameraBaseU5BU5D_t3901427982* ___m_ChildCameras_16;
	// UnityEngine.Animator Cinemachine.CinemachineStateDrivenCamera::m_AnimatedTarget
	Animator_t2768715325 * ___m_AnimatedTarget_17;
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera::m_LayerIndex
	int32_t ___m_LayerIndex_18;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction[] Cinemachine.CinemachineStateDrivenCamera::m_Instructions
	InstructionU5BU5D_t455858040* ___m_Instructions_19;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineStateDrivenCamera::m_DefaultBlend
	CinemachineBlendDefinition_t3981438518  ___m_DefaultBlend_20;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineStateDrivenCamera::m_CustomBlends
	CinemachineBlenderSettings_t865950197 * ___m_CustomBlends_21;
	// Cinemachine.CinemachineStateDrivenCamera/ParentHash[] Cinemachine.CinemachineStateDrivenCamera::m_ParentHash
	ParentHashU5BU5D_t4234504058* ___m_ParentHash_22;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineStateDrivenCamera::<LiveChild>k__BackingField
	RuntimeObject* ___U3CLiveChildU3Ek__BackingField_23;
	// Cinemachine.CameraState Cinemachine.CinemachineStateDrivenCamera::m_State
	CameraState_t382403230  ___m_State_24;
	// System.Single Cinemachine.CinemachineStateDrivenCamera::mActivationTime
	float ___mActivationTime_25;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction Cinemachine.CinemachineStateDrivenCamera::mActiveInstruction
	Instruction_t3855846037  ___mActiveInstruction_26;
	// System.Single Cinemachine.CinemachineStateDrivenCamera::mPendingActivationTime
	float ___mPendingActivationTime_27;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction Cinemachine.CinemachineStateDrivenCamera::mPendingInstruction
	Instruction_t3855846037  ___mPendingInstruction_28;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineStateDrivenCamera::mActiveBlend
	CinemachineBlend_t2146485918 * ___mActiveBlend_29;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Cinemachine.CinemachineStateDrivenCamera::mInstructionDictionary
	Dictionary_2_t831864540 * ___mInstructionDictionary_30;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Cinemachine.CinemachineStateDrivenCamera::mStateParentLookup
	Dictionary_2_t831864540 * ___mStateParentLookup_31;

public:
	inline static int32_t get_offset_of_m_LookAt_12() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_LookAt_12)); }
	inline Transform_t362059596 * get_m_LookAt_12() const { return ___m_LookAt_12; }
	inline Transform_t362059596 ** get_address_of_m_LookAt_12() { return &___m_LookAt_12; }
	inline void set_m_LookAt_12(Transform_t362059596 * value)
	{
		___m_LookAt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_12), value);
	}

	inline static int32_t get_offset_of_m_Follow_13() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_Follow_13)); }
	inline Transform_t362059596 * get_m_Follow_13() const { return ___m_Follow_13; }
	inline Transform_t362059596 ** get_address_of_m_Follow_13() { return &___m_Follow_13; }
	inline void set_m_Follow_13(Transform_t362059596 * value)
	{
		___m_Follow_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_13), value);
	}

	inline static int32_t get_offset_of_m_ShowDebugText_14() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_ShowDebugText_14)); }
	inline bool get_m_ShowDebugText_14() const { return ___m_ShowDebugText_14; }
	inline bool* get_address_of_m_ShowDebugText_14() { return &___m_ShowDebugText_14; }
	inline void set_m_ShowDebugText_14(bool value)
	{
		___m_ShowDebugText_14 = value;
	}

	inline static int32_t get_offset_of_m_EnableAllChildCameras_15() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_EnableAllChildCameras_15)); }
	inline bool get_m_EnableAllChildCameras_15() const { return ___m_EnableAllChildCameras_15; }
	inline bool* get_address_of_m_EnableAllChildCameras_15() { return &___m_EnableAllChildCameras_15; }
	inline void set_m_EnableAllChildCameras_15(bool value)
	{
		___m_EnableAllChildCameras_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildCameras_16() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_ChildCameras_16)); }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982* get_m_ChildCameras_16() const { return ___m_ChildCameras_16; }
	inline CinemachineVirtualCameraBaseU5BU5D_t3901427982** get_address_of_m_ChildCameras_16() { return &___m_ChildCameras_16; }
	inline void set_m_ChildCameras_16(CinemachineVirtualCameraBaseU5BU5D_t3901427982* value)
	{
		___m_ChildCameras_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildCameras_16), value);
	}

	inline static int32_t get_offset_of_m_AnimatedTarget_17() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_AnimatedTarget_17)); }
	inline Animator_t2768715325 * get_m_AnimatedTarget_17() const { return ___m_AnimatedTarget_17; }
	inline Animator_t2768715325 ** get_address_of_m_AnimatedTarget_17() { return &___m_AnimatedTarget_17; }
	inline void set_m_AnimatedTarget_17(Animator_t2768715325 * value)
	{
		___m_AnimatedTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimatedTarget_17), value);
	}

	inline static int32_t get_offset_of_m_LayerIndex_18() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_LayerIndex_18)); }
	inline int32_t get_m_LayerIndex_18() const { return ___m_LayerIndex_18; }
	inline int32_t* get_address_of_m_LayerIndex_18() { return &___m_LayerIndex_18; }
	inline void set_m_LayerIndex_18(int32_t value)
	{
		___m_LayerIndex_18 = value;
	}

	inline static int32_t get_offset_of_m_Instructions_19() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_Instructions_19)); }
	inline InstructionU5BU5D_t455858040* get_m_Instructions_19() const { return ___m_Instructions_19; }
	inline InstructionU5BU5D_t455858040** get_address_of_m_Instructions_19() { return &___m_Instructions_19; }
	inline void set_m_Instructions_19(InstructionU5BU5D_t455858040* value)
	{
		___m_Instructions_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instructions_19), value);
	}

	inline static int32_t get_offset_of_m_DefaultBlend_20() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_DefaultBlend_20)); }
	inline CinemachineBlendDefinition_t3981438518  get_m_DefaultBlend_20() const { return ___m_DefaultBlend_20; }
	inline CinemachineBlendDefinition_t3981438518 * get_address_of_m_DefaultBlend_20() { return &___m_DefaultBlend_20; }
	inline void set_m_DefaultBlend_20(CinemachineBlendDefinition_t3981438518  value)
	{
		___m_DefaultBlend_20 = value;
	}

	inline static int32_t get_offset_of_m_CustomBlends_21() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_CustomBlends_21)); }
	inline CinemachineBlenderSettings_t865950197 * get_m_CustomBlends_21() const { return ___m_CustomBlends_21; }
	inline CinemachineBlenderSettings_t865950197 ** get_address_of_m_CustomBlends_21() { return &___m_CustomBlends_21; }
	inline void set_m_CustomBlends_21(CinemachineBlenderSettings_t865950197 * value)
	{
		___m_CustomBlends_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_21), value);
	}

	inline static int32_t get_offset_of_m_ParentHash_22() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_ParentHash_22)); }
	inline ParentHashU5BU5D_t4234504058* get_m_ParentHash_22() const { return ___m_ParentHash_22; }
	inline ParentHashU5BU5D_t4234504058** get_address_of_m_ParentHash_22() { return &___m_ParentHash_22; }
	inline void set_m_ParentHash_22(ParentHashU5BU5D_t4234504058* value)
	{
		___m_ParentHash_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentHash_22), value);
	}

	inline static int32_t get_offset_of_U3CLiveChildU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___U3CLiveChildU3Ek__BackingField_23)); }
	inline RuntimeObject* get_U3CLiveChildU3Ek__BackingField_23() const { return ___U3CLiveChildU3Ek__BackingField_23; }
	inline RuntimeObject** get_address_of_U3CLiveChildU3Ek__BackingField_23() { return &___U3CLiveChildU3Ek__BackingField_23; }
	inline void set_U3CLiveChildU3Ek__BackingField_23(RuntimeObject* value)
	{
		___U3CLiveChildU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLiveChildU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_m_State_24() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___m_State_24)); }
	inline CameraState_t382403230  get_m_State_24() const { return ___m_State_24; }
	inline CameraState_t382403230 * get_address_of_m_State_24() { return &___m_State_24; }
	inline void set_m_State_24(CameraState_t382403230  value)
	{
		___m_State_24 = value;
	}

	inline static int32_t get_offset_of_mActivationTime_25() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mActivationTime_25)); }
	inline float get_mActivationTime_25() const { return ___mActivationTime_25; }
	inline float* get_address_of_mActivationTime_25() { return &___mActivationTime_25; }
	inline void set_mActivationTime_25(float value)
	{
		___mActivationTime_25 = value;
	}

	inline static int32_t get_offset_of_mActiveInstruction_26() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mActiveInstruction_26)); }
	inline Instruction_t3855846037  get_mActiveInstruction_26() const { return ___mActiveInstruction_26; }
	inline Instruction_t3855846037 * get_address_of_mActiveInstruction_26() { return &___mActiveInstruction_26; }
	inline void set_mActiveInstruction_26(Instruction_t3855846037  value)
	{
		___mActiveInstruction_26 = value;
	}

	inline static int32_t get_offset_of_mPendingActivationTime_27() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mPendingActivationTime_27)); }
	inline float get_mPendingActivationTime_27() const { return ___mPendingActivationTime_27; }
	inline float* get_address_of_mPendingActivationTime_27() { return &___mPendingActivationTime_27; }
	inline void set_mPendingActivationTime_27(float value)
	{
		___mPendingActivationTime_27 = value;
	}

	inline static int32_t get_offset_of_mPendingInstruction_28() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mPendingInstruction_28)); }
	inline Instruction_t3855846037  get_mPendingInstruction_28() const { return ___mPendingInstruction_28; }
	inline Instruction_t3855846037 * get_address_of_mPendingInstruction_28() { return &___mPendingInstruction_28; }
	inline void set_mPendingInstruction_28(Instruction_t3855846037  value)
	{
		___mPendingInstruction_28 = value;
	}

	inline static int32_t get_offset_of_mActiveBlend_29() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mActiveBlend_29)); }
	inline CinemachineBlend_t2146485918 * get_mActiveBlend_29() const { return ___mActiveBlend_29; }
	inline CinemachineBlend_t2146485918 ** get_address_of_mActiveBlend_29() { return &___mActiveBlend_29; }
	inline void set_mActiveBlend_29(CinemachineBlend_t2146485918 * value)
	{
		___mActiveBlend_29 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBlend_29), value);
	}

	inline static int32_t get_offset_of_mInstructionDictionary_30() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mInstructionDictionary_30)); }
	inline Dictionary_2_t831864540 * get_mInstructionDictionary_30() const { return ___mInstructionDictionary_30; }
	inline Dictionary_2_t831864540 ** get_address_of_mInstructionDictionary_30() { return &___mInstructionDictionary_30; }
	inline void set_mInstructionDictionary_30(Dictionary_2_t831864540 * value)
	{
		___mInstructionDictionary_30 = value;
		Il2CppCodeGenWriteBarrier((&___mInstructionDictionary_30), value);
	}

	inline static int32_t get_offset_of_mStateParentLookup_31() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t2857997874, ___mStateParentLookup_31)); }
	inline Dictionary_2_t831864540 * get_mStateParentLookup_31() const { return ___mStateParentLookup_31; }
	inline Dictionary_2_t831864540 ** get_address_of_mStateParentLookup_31() { return &___mStateParentLookup_31; }
	inline void set_mStateParentLookup_31(Dictionary_2_t831864540 * value)
	{
		___mStateParentLookup_31 = value;
		Il2CppCodeGenWriteBarrier((&___mStateParentLookup_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESTATEDRIVENCAMERA_T2857997874_H
#ifndef TWIRL_T1827184245_H
#define TWIRL_T1827184245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Twirl
struct  Twirl_t1827184245  : public ImageEffectBase_t4133330248
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Twirl::radius
	Vector2_t328513675  ___radius_4;
	// System.Single UnityStandardAssets.ImageEffects.Twirl::angle
	float ___angle_5;
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Twirl::center
	Vector2_t328513675  ___center_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Twirl_t1827184245, ___radius_4)); }
	inline Vector2_t328513675  get_radius_4() const { return ___radius_4; }
	inline Vector2_t328513675 * get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(Vector2_t328513675  value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(Twirl_t1827184245, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_center_6() { return static_cast<int32_t>(offsetof(Twirl_t1827184245, ___center_6)); }
	inline Vector2_t328513675  get_center_6() const { return ___center_6; }
	inline Vector2_t328513675 * get_address_of_center_6() { return &___center_6; }
	inline void set_center_6(Vector2_t328513675  value)
	{
		___center_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWIRL_T1827184245_H
#ifndef CINEMACHINEGROUPCOMPOSER_T2240148537_H
#define CINEMACHINEGROUPCOMPOSER_T2240148537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer
struct  CinemachineGroupComposer_t2240148537  : public CinemachineComposer_t3213114616
{
public:
	// System.Single Cinemachine.CinemachineGroupComposer::m_GroupFramingSize
	float ___m_GroupFramingSize_16;
	// Cinemachine.CinemachineGroupComposer/FramingMode Cinemachine.CinemachineGroupComposer::m_FramingMode
	int32_t ___m_FramingMode_17;
	// System.Single Cinemachine.CinemachineGroupComposer::m_FrameDamping
	float ___m_FrameDamping_18;
	// Cinemachine.CinemachineGroupComposer/AdjustmentMode Cinemachine.CinemachineGroupComposer::m_AdjustmentMode
	int32_t ___m_AdjustmentMode_19;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaxDollyIn
	float ___m_MaxDollyIn_20;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaxDollyOut
	float ___m_MaxDollyOut_21;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MinimumDistance
	float ___m_MinimumDistance_22;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaximumDistance
	float ___m_MaximumDistance_23;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MinimumFOV
	float ___m_MinimumFOV_24;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaximumFOV
	float ___m_MaximumFOV_25;
	// System.Single Cinemachine.CinemachineGroupComposer::m_prevTargetHeight
	float ___m_prevTargetHeight_26;
	// UnityEngine.Bounds Cinemachine.CinemachineGroupComposer::<m_LastBounds>k__BackingField
	Bounds_t3570137099  ___U3Cm_LastBoundsU3Ek__BackingField_27;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineGroupComposer::<m_lastBoundsMatrix>k__BackingField
	Matrix4x4_t1237934469  ___U3Cm_lastBoundsMatrixU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_m_GroupFramingSize_16() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_GroupFramingSize_16)); }
	inline float get_m_GroupFramingSize_16() const { return ___m_GroupFramingSize_16; }
	inline float* get_address_of_m_GroupFramingSize_16() { return &___m_GroupFramingSize_16; }
	inline void set_m_GroupFramingSize_16(float value)
	{
		___m_GroupFramingSize_16 = value;
	}

	inline static int32_t get_offset_of_m_FramingMode_17() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_FramingMode_17)); }
	inline int32_t get_m_FramingMode_17() const { return ___m_FramingMode_17; }
	inline int32_t* get_address_of_m_FramingMode_17() { return &___m_FramingMode_17; }
	inline void set_m_FramingMode_17(int32_t value)
	{
		___m_FramingMode_17 = value;
	}

	inline static int32_t get_offset_of_m_FrameDamping_18() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_FrameDamping_18)); }
	inline float get_m_FrameDamping_18() const { return ___m_FrameDamping_18; }
	inline float* get_address_of_m_FrameDamping_18() { return &___m_FrameDamping_18; }
	inline void set_m_FrameDamping_18(float value)
	{
		___m_FrameDamping_18 = value;
	}

	inline static int32_t get_offset_of_m_AdjustmentMode_19() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_AdjustmentMode_19)); }
	inline int32_t get_m_AdjustmentMode_19() const { return ___m_AdjustmentMode_19; }
	inline int32_t* get_address_of_m_AdjustmentMode_19() { return &___m_AdjustmentMode_19; }
	inline void set_m_AdjustmentMode_19(int32_t value)
	{
		___m_AdjustmentMode_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyIn_20() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MaxDollyIn_20)); }
	inline float get_m_MaxDollyIn_20() const { return ___m_MaxDollyIn_20; }
	inline float* get_address_of_m_MaxDollyIn_20() { return &___m_MaxDollyIn_20; }
	inline void set_m_MaxDollyIn_20(float value)
	{
		___m_MaxDollyIn_20 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyOut_21() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MaxDollyOut_21)); }
	inline float get_m_MaxDollyOut_21() const { return ___m_MaxDollyOut_21; }
	inline float* get_address_of_m_MaxDollyOut_21() { return &___m_MaxDollyOut_21; }
	inline void set_m_MaxDollyOut_21(float value)
	{
		___m_MaxDollyOut_21 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistance_22() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MinimumDistance_22)); }
	inline float get_m_MinimumDistance_22() const { return ___m_MinimumDistance_22; }
	inline float* get_address_of_m_MinimumDistance_22() { return &___m_MinimumDistance_22; }
	inline void set_m_MinimumDistance_22(float value)
	{
		___m_MinimumDistance_22 = value;
	}

	inline static int32_t get_offset_of_m_MaximumDistance_23() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MaximumDistance_23)); }
	inline float get_m_MaximumDistance_23() const { return ___m_MaximumDistance_23; }
	inline float* get_address_of_m_MaximumDistance_23() { return &___m_MaximumDistance_23; }
	inline void set_m_MaximumDistance_23(float value)
	{
		___m_MaximumDistance_23 = value;
	}

	inline static int32_t get_offset_of_m_MinimumFOV_24() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MinimumFOV_24)); }
	inline float get_m_MinimumFOV_24() const { return ___m_MinimumFOV_24; }
	inline float* get_address_of_m_MinimumFOV_24() { return &___m_MinimumFOV_24; }
	inline void set_m_MinimumFOV_24(float value)
	{
		___m_MinimumFOV_24 = value;
	}

	inline static int32_t get_offset_of_m_MaximumFOV_25() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_MaximumFOV_25)); }
	inline float get_m_MaximumFOV_25() const { return ___m_MaximumFOV_25; }
	inline float* get_address_of_m_MaximumFOV_25() { return &___m_MaximumFOV_25; }
	inline void set_m_MaximumFOV_25(float value)
	{
		___m_MaximumFOV_25 = value;
	}

	inline static int32_t get_offset_of_m_prevTargetHeight_26() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___m_prevTargetHeight_26)); }
	inline float get_m_prevTargetHeight_26() const { return ___m_prevTargetHeight_26; }
	inline float* get_address_of_m_prevTargetHeight_26() { return &___m_prevTargetHeight_26; }
	inline void set_m_prevTargetHeight_26(float value)
	{
		___m_prevTargetHeight_26 = value;
	}

	inline static int32_t get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___U3Cm_LastBoundsU3Ek__BackingField_27)); }
	inline Bounds_t3570137099  get_U3Cm_LastBoundsU3Ek__BackingField_27() const { return ___U3Cm_LastBoundsU3Ek__BackingField_27; }
	inline Bounds_t3570137099 * get_address_of_U3Cm_LastBoundsU3Ek__BackingField_27() { return &___U3Cm_LastBoundsU3Ek__BackingField_27; }
	inline void set_U3Cm_LastBoundsU3Ek__BackingField_27(Bounds_t3570137099  value)
	{
		___U3Cm_LastBoundsU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t2240148537, ___U3Cm_lastBoundsMatrixU3Ek__BackingField_28)); }
	inline Matrix4x4_t1237934469  get_U3Cm_lastBoundsMatrixU3Ek__BackingField_28() const { return ___U3Cm_lastBoundsMatrixU3Ek__BackingField_28; }
	inline Matrix4x4_t1237934469 * get_address_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_28() { return &___U3Cm_lastBoundsMatrixU3Ek__BackingField_28; }
	inline void set_U3Cm_lastBoundsMatrixU3Ek__BackingField_28(Matrix4x4_t1237934469  value)
	{
		___U3Cm_lastBoundsMatrixU3Ek__BackingField_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEGROUPCOMPOSER_T2240148537_H
#ifndef VORTEX_T702210748_H
#define VORTEX_T702210748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Vortex
struct  Vortex_t702210748  : public ImageEffectBase_t4133330248
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Vortex::radius
	Vector2_t328513675  ___radius_4;
	// System.Single UnityStandardAssets.ImageEffects.Vortex::angle
	float ___angle_5;
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Vortex::center
	Vector2_t328513675  ___center_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Vortex_t702210748, ___radius_4)); }
	inline Vector2_t328513675  get_radius_4() const { return ___radius_4; }
	inline Vector2_t328513675 * get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(Vector2_t328513675  value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(Vortex_t702210748, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_center_6() { return static_cast<int32_t>(offsetof(Vortex_t702210748, ___center_6)); }
	inline Vector2_t328513675  get_center_6() const { return ___center_6; }
	inline Vector2_t328513675 * get_address_of_center_6() { return &___center_6; }
	inline void set_center_6(Vector2_t328513675  value)
	{
		___center_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VORTEX_T702210748_H
#ifndef NOISEANDGRAIN_T2715078849_H
#define NOISEANDGRAIN_T2715078849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.NoiseAndGrain
struct  NoiseAndGrain_t2715078849  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::intensityMultiplier
	float ___intensityMultiplier_6;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::generalIntensity
	float ___generalIntensity_7;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::blackIntensity
	float ___blackIntensity_8;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::whiteIntensity
	float ___whiteIntensity_9;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::midGrey
	float ___midGrey_10;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11Grain
	bool ___dx11Grain_11;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::softness
	float ___softness_12;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::monochrome
	bool ___monochrome_13;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.NoiseAndGrain::intensities
	Vector3_t1986933152  ___intensities_14;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.NoiseAndGrain::tiling
	Vector3_t1986933152  ___tiling_15;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::monochromeTiling
	float ___monochromeTiling_16;
	// UnityEngine.FilterMode UnityStandardAssets.ImageEffects.NoiseAndGrain::filterMode
	int32_t ___filterMode_17;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseTexture
	Texture2D_t3063074017 * ___noiseTexture_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseShader
	Shader_t1881769421 * ___noiseShader_19;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseMaterial
	Material_t2815264910 * ___noiseMaterial_20;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11NoiseShader
	Shader_t1881769421 * ___dx11NoiseShader_21;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11NoiseMaterial
	Material_t2815264910 * ___dx11NoiseMaterial_22;

public:
	inline static int32_t get_offset_of_intensityMultiplier_6() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___intensityMultiplier_6)); }
	inline float get_intensityMultiplier_6() const { return ___intensityMultiplier_6; }
	inline float* get_address_of_intensityMultiplier_6() { return &___intensityMultiplier_6; }
	inline void set_intensityMultiplier_6(float value)
	{
		___intensityMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_generalIntensity_7() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___generalIntensity_7)); }
	inline float get_generalIntensity_7() const { return ___generalIntensity_7; }
	inline float* get_address_of_generalIntensity_7() { return &___generalIntensity_7; }
	inline void set_generalIntensity_7(float value)
	{
		___generalIntensity_7 = value;
	}

	inline static int32_t get_offset_of_blackIntensity_8() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___blackIntensity_8)); }
	inline float get_blackIntensity_8() const { return ___blackIntensity_8; }
	inline float* get_address_of_blackIntensity_8() { return &___blackIntensity_8; }
	inline void set_blackIntensity_8(float value)
	{
		___blackIntensity_8 = value;
	}

	inline static int32_t get_offset_of_whiteIntensity_9() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___whiteIntensity_9)); }
	inline float get_whiteIntensity_9() const { return ___whiteIntensity_9; }
	inline float* get_address_of_whiteIntensity_9() { return &___whiteIntensity_9; }
	inline void set_whiteIntensity_9(float value)
	{
		___whiteIntensity_9 = value;
	}

	inline static int32_t get_offset_of_midGrey_10() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___midGrey_10)); }
	inline float get_midGrey_10() const { return ___midGrey_10; }
	inline float* get_address_of_midGrey_10() { return &___midGrey_10; }
	inline void set_midGrey_10(float value)
	{
		___midGrey_10 = value;
	}

	inline static int32_t get_offset_of_dx11Grain_11() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___dx11Grain_11)); }
	inline bool get_dx11Grain_11() const { return ___dx11Grain_11; }
	inline bool* get_address_of_dx11Grain_11() { return &___dx11Grain_11; }
	inline void set_dx11Grain_11(bool value)
	{
		___dx11Grain_11 = value;
	}

	inline static int32_t get_offset_of_softness_12() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___softness_12)); }
	inline float get_softness_12() const { return ___softness_12; }
	inline float* get_address_of_softness_12() { return &___softness_12; }
	inline void set_softness_12(float value)
	{
		___softness_12 = value;
	}

	inline static int32_t get_offset_of_monochrome_13() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___monochrome_13)); }
	inline bool get_monochrome_13() const { return ___monochrome_13; }
	inline bool* get_address_of_monochrome_13() { return &___monochrome_13; }
	inline void set_monochrome_13(bool value)
	{
		___monochrome_13 = value;
	}

	inline static int32_t get_offset_of_intensities_14() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___intensities_14)); }
	inline Vector3_t1986933152  get_intensities_14() const { return ___intensities_14; }
	inline Vector3_t1986933152 * get_address_of_intensities_14() { return &___intensities_14; }
	inline void set_intensities_14(Vector3_t1986933152  value)
	{
		___intensities_14 = value;
	}

	inline static int32_t get_offset_of_tiling_15() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___tiling_15)); }
	inline Vector3_t1986933152  get_tiling_15() const { return ___tiling_15; }
	inline Vector3_t1986933152 * get_address_of_tiling_15() { return &___tiling_15; }
	inline void set_tiling_15(Vector3_t1986933152  value)
	{
		___tiling_15 = value;
	}

	inline static int32_t get_offset_of_monochromeTiling_16() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___monochromeTiling_16)); }
	inline float get_monochromeTiling_16() const { return ___monochromeTiling_16; }
	inline float* get_address_of_monochromeTiling_16() { return &___monochromeTiling_16; }
	inline void set_monochromeTiling_16(float value)
	{
		___monochromeTiling_16 = value;
	}

	inline static int32_t get_offset_of_filterMode_17() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___filterMode_17)); }
	inline int32_t get_filterMode_17() const { return ___filterMode_17; }
	inline int32_t* get_address_of_filterMode_17() { return &___filterMode_17; }
	inline void set_filterMode_17(int32_t value)
	{
		___filterMode_17 = value;
	}

	inline static int32_t get_offset_of_noiseTexture_18() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___noiseTexture_18)); }
	inline Texture2D_t3063074017 * get_noiseTexture_18() const { return ___noiseTexture_18; }
	inline Texture2D_t3063074017 ** get_address_of_noiseTexture_18() { return &___noiseTexture_18; }
	inline void set_noiseTexture_18(Texture2D_t3063074017 * value)
	{
		___noiseTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTexture_18), value);
	}

	inline static int32_t get_offset_of_noiseShader_19() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___noiseShader_19)); }
	inline Shader_t1881769421 * get_noiseShader_19() const { return ___noiseShader_19; }
	inline Shader_t1881769421 ** get_address_of_noiseShader_19() { return &___noiseShader_19; }
	inline void set_noiseShader_19(Shader_t1881769421 * value)
	{
		___noiseShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___noiseShader_19), value);
	}

	inline static int32_t get_offset_of_noiseMaterial_20() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___noiseMaterial_20)); }
	inline Material_t2815264910 * get_noiseMaterial_20() const { return ___noiseMaterial_20; }
	inline Material_t2815264910 ** get_address_of_noiseMaterial_20() { return &___noiseMaterial_20; }
	inline void set_noiseMaterial_20(Material_t2815264910 * value)
	{
		___noiseMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___noiseMaterial_20), value);
	}

	inline static int32_t get_offset_of_dx11NoiseShader_21() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___dx11NoiseShader_21)); }
	inline Shader_t1881769421 * get_dx11NoiseShader_21() const { return ___dx11NoiseShader_21; }
	inline Shader_t1881769421 ** get_address_of_dx11NoiseShader_21() { return &___dx11NoiseShader_21; }
	inline void set_dx11NoiseShader_21(Shader_t1881769421 * value)
	{
		___dx11NoiseShader_21 = value;
		Il2CppCodeGenWriteBarrier((&___dx11NoiseShader_21), value);
	}

	inline static int32_t get_offset_of_dx11NoiseMaterial_22() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849, ___dx11NoiseMaterial_22)); }
	inline Material_t2815264910 * get_dx11NoiseMaterial_22() const { return ___dx11NoiseMaterial_22; }
	inline Material_t2815264910 ** get_address_of_dx11NoiseMaterial_22() { return &___dx11NoiseMaterial_22; }
	inline void set_dx11NoiseMaterial_22(Material_t2815264910 * value)
	{
		___dx11NoiseMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___dx11NoiseMaterial_22), value);
	}
};

struct NoiseAndGrain_t2715078849_StaticFields
{
public:
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::TILE_AMOUNT
	float ___TILE_AMOUNT_23;

public:
	inline static int32_t get_offset_of_TILE_AMOUNT_23() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t2715078849_StaticFields, ___TILE_AMOUNT_23)); }
	inline float get_TILE_AMOUNT_23() const { return ___TILE_AMOUNT_23; }
	inline float* get_address_of_TILE_AMOUNT_23() { return &___TILE_AMOUNT_23; }
	inline void set_TILE_AMOUNT_23(float value)
	{
		___TILE_AMOUNT_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEANDGRAIN_T2715078849_H
#ifndef TONEMAPPING_T2013258899_H
#define TONEMAPPING_T2013258899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping
struct  Tonemapping_t2013258899  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType UnityStandardAssets.ImageEffects.Tonemapping::type
	int32_t ___type_6;
	// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize UnityStandardAssets.ImageEffects.Tonemapping::adaptiveTextureSize
	int32_t ___adaptiveTextureSize_7;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.Tonemapping::remapCurve
	AnimationCurve_t2757045165 * ___remapCurve_8;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.Tonemapping::curveTex
	Texture2D_t3063074017 * ___curveTex_9;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::exposureAdjustment
	float ___exposureAdjustment_10;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::middleGrey
	float ___middleGrey_11;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::white
	float ___white_12;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::adaptionSpeed
	float ___adaptionSpeed_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Tonemapping::tonemapper
	Shader_t1881769421 * ___tonemapper_14;
	// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::validRenderTextureFormat
	bool ___validRenderTextureFormat_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Tonemapping::tonemapMaterial
	Material_t2815264910 * ___tonemapMaterial_16;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.Tonemapping::rt
	RenderTexture_t971269558 * ___rt_17;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.ImageEffects.Tonemapping::rtFormat
	int32_t ___rtFormat_18;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_adaptiveTextureSize_7() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___adaptiveTextureSize_7)); }
	inline int32_t get_adaptiveTextureSize_7() const { return ___adaptiveTextureSize_7; }
	inline int32_t* get_address_of_adaptiveTextureSize_7() { return &___adaptiveTextureSize_7; }
	inline void set_adaptiveTextureSize_7(int32_t value)
	{
		___adaptiveTextureSize_7 = value;
	}

	inline static int32_t get_offset_of_remapCurve_8() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___remapCurve_8)); }
	inline AnimationCurve_t2757045165 * get_remapCurve_8() const { return ___remapCurve_8; }
	inline AnimationCurve_t2757045165 ** get_address_of_remapCurve_8() { return &___remapCurve_8; }
	inline void set_remapCurve_8(AnimationCurve_t2757045165 * value)
	{
		___remapCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___remapCurve_8), value);
	}

	inline static int32_t get_offset_of_curveTex_9() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___curveTex_9)); }
	inline Texture2D_t3063074017 * get_curveTex_9() const { return ___curveTex_9; }
	inline Texture2D_t3063074017 ** get_address_of_curveTex_9() { return &___curveTex_9; }
	inline void set_curveTex_9(Texture2D_t3063074017 * value)
	{
		___curveTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___curveTex_9), value);
	}

	inline static int32_t get_offset_of_exposureAdjustment_10() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___exposureAdjustment_10)); }
	inline float get_exposureAdjustment_10() const { return ___exposureAdjustment_10; }
	inline float* get_address_of_exposureAdjustment_10() { return &___exposureAdjustment_10; }
	inline void set_exposureAdjustment_10(float value)
	{
		___exposureAdjustment_10 = value;
	}

	inline static int32_t get_offset_of_middleGrey_11() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___middleGrey_11)); }
	inline float get_middleGrey_11() const { return ___middleGrey_11; }
	inline float* get_address_of_middleGrey_11() { return &___middleGrey_11; }
	inline void set_middleGrey_11(float value)
	{
		___middleGrey_11 = value;
	}

	inline static int32_t get_offset_of_white_12() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___white_12)); }
	inline float get_white_12() const { return ___white_12; }
	inline float* get_address_of_white_12() { return &___white_12; }
	inline void set_white_12(float value)
	{
		___white_12 = value;
	}

	inline static int32_t get_offset_of_adaptionSpeed_13() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___adaptionSpeed_13)); }
	inline float get_adaptionSpeed_13() const { return ___adaptionSpeed_13; }
	inline float* get_address_of_adaptionSpeed_13() { return &___adaptionSpeed_13; }
	inline void set_adaptionSpeed_13(float value)
	{
		___adaptionSpeed_13 = value;
	}

	inline static int32_t get_offset_of_tonemapper_14() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___tonemapper_14)); }
	inline Shader_t1881769421 * get_tonemapper_14() const { return ___tonemapper_14; }
	inline Shader_t1881769421 ** get_address_of_tonemapper_14() { return &___tonemapper_14; }
	inline void set_tonemapper_14(Shader_t1881769421 * value)
	{
		___tonemapper_14 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapper_14), value);
	}

	inline static int32_t get_offset_of_validRenderTextureFormat_15() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___validRenderTextureFormat_15)); }
	inline bool get_validRenderTextureFormat_15() const { return ___validRenderTextureFormat_15; }
	inline bool* get_address_of_validRenderTextureFormat_15() { return &___validRenderTextureFormat_15; }
	inline void set_validRenderTextureFormat_15(bool value)
	{
		___validRenderTextureFormat_15 = value;
	}

	inline static int32_t get_offset_of_tonemapMaterial_16() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___tonemapMaterial_16)); }
	inline Material_t2815264910 * get_tonemapMaterial_16() const { return ___tonemapMaterial_16; }
	inline Material_t2815264910 ** get_address_of_tonemapMaterial_16() { return &___tonemapMaterial_16; }
	inline void set_tonemapMaterial_16(Material_t2815264910 * value)
	{
		___tonemapMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapMaterial_16), value);
	}

	inline static int32_t get_offset_of_rt_17() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___rt_17)); }
	inline RenderTexture_t971269558 * get_rt_17() const { return ___rt_17; }
	inline RenderTexture_t971269558 ** get_address_of_rt_17() { return &___rt_17; }
	inline void set_rt_17(RenderTexture_t971269558 * value)
	{
		___rt_17 = value;
		Il2CppCodeGenWriteBarrier((&___rt_17), value);
	}

	inline static int32_t get_offset_of_rtFormat_18() { return static_cast<int32_t>(offsetof(Tonemapping_t2013258899, ___rtFormat_18)); }
	inline int32_t get_rtFormat_18() const { return ___rtFormat_18; }
	inline int32_t* get_address_of_rtFormat_18() { return &___rtFormat_18; }
	inline void set_rtFormat_18(int32_t value)
	{
		___rtFormat_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPING_T2013258899_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (DofResolution_t1205495178)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	DofResolution_t1205495178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (DofBlurriness_t1437194827)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	DofBlurriness_t1437194827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (BokehDestination_t2246052814)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	BokehDestination_t2246052814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (EdgeDetection_t2492772135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[11] = 
{
	EdgeDetection_t2492772135::get_offset_of_mode_6(),
	EdgeDetection_t2492772135::get_offset_of_sensitivityDepth_7(),
	EdgeDetection_t2492772135::get_offset_of_sensitivityNormals_8(),
	EdgeDetection_t2492772135::get_offset_of_lumThreshold_9(),
	EdgeDetection_t2492772135::get_offset_of_edgeExp_10(),
	EdgeDetection_t2492772135::get_offset_of_sampleDist_11(),
	EdgeDetection_t2492772135::get_offset_of_edgesOnly_12(),
	EdgeDetection_t2492772135::get_offset_of_edgesOnlyBgColor_13(),
	EdgeDetection_t2492772135::get_offset_of_edgeDetectShader_14(),
	EdgeDetection_t2492772135::get_offset_of_edgeDetectMaterial_15(),
	EdgeDetection_t2492772135::get_offset_of_oldMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (EdgeDetectMode_t3688097253)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2304[6] = 
{
	EdgeDetectMode_t3688097253::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (Fisheye_t676218443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[4] = 
{
	Fisheye_t676218443::get_offset_of_strengthX_6(),
	Fisheye_t676218443::get_offset_of_strengthY_7(),
	Fisheye_t676218443::get_offset_of_fishEyeShader_8(),
	Fisheye_t676218443::get_offset_of_fisheyeMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (GlobalFog_t4163397306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[9] = 
{
	GlobalFog_t4163397306::get_offset_of_distanceFog_6(),
	GlobalFog_t4163397306::get_offset_of_excludeFarPixels_7(),
	GlobalFog_t4163397306::get_offset_of_useRadialDistance_8(),
	GlobalFog_t4163397306::get_offset_of_heightFog_9(),
	GlobalFog_t4163397306::get_offset_of_height_10(),
	GlobalFog_t4163397306::get_offset_of_heightDensity_11(),
	GlobalFog_t4163397306::get_offset_of_startDistance_12(),
	GlobalFog_t4163397306::get_offset_of_fogShader_13(),
	GlobalFog_t4163397306::get_offset_of_fogMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (Grayscale_t1098547321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[2] = 
{
	Grayscale_t1098547321::get_offset_of_textureRamp_4(),
	Grayscale_t1098547321::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (ImageEffectBase_t4133330248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[2] = 
{
	ImageEffectBase_t4133330248::get_offset_of_shader_2(),
	ImageEffectBase_t4133330248::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (ImageEffects_t1429922186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (MotionBlur_t1132854394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[3] = 
{
	MotionBlur_t1132854394::get_offset_of_blurAmount_4(),
	MotionBlur_t1132854394::get_offset_of_extraBlur_5(),
	MotionBlur_t1132854394::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (NoiseAndGrain_t2715078849), -1, sizeof(NoiseAndGrain_t2715078849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2311[18] = 
{
	NoiseAndGrain_t2715078849::get_offset_of_intensityMultiplier_6(),
	NoiseAndGrain_t2715078849::get_offset_of_generalIntensity_7(),
	NoiseAndGrain_t2715078849::get_offset_of_blackIntensity_8(),
	NoiseAndGrain_t2715078849::get_offset_of_whiteIntensity_9(),
	NoiseAndGrain_t2715078849::get_offset_of_midGrey_10(),
	NoiseAndGrain_t2715078849::get_offset_of_dx11Grain_11(),
	NoiseAndGrain_t2715078849::get_offset_of_softness_12(),
	NoiseAndGrain_t2715078849::get_offset_of_monochrome_13(),
	NoiseAndGrain_t2715078849::get_offset_of_intensities_14(),
	NoiseAndGrain_t2715078849::get_offset_of_tiling_15(),
	NoiseAndGrain_t2715078849::get_offset_of_monochromeTiling_16(),
	NoiseAndGrain_t2715078849::get_offset_of_filterMode_17(),
	NoiseAndGrain_t2715078849::get_offset_of_noiseTexture_18(),
	NoiseAndGrain_t2715078849::get_offset_of_noiseShader_19(),
	NoiseAndGrain_t2715078849::get_offset_of_noiseMaterial_20(),
	NoiseAndGrain_t2715078849::get_offset_of_dx11NoiseShader_21(),
	NoiseAndGrain_t2715078849::get_offset_of_dx11NoiseMaterial_22(),
	NoiseAndGrain_t2715078849_StaticFields::get_offset_of_TILE_AMOUNT_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (NoiseAndScratches_t3793967858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[18] = 
{
	NoiseAndScratches_t3793967858::get_offset_of_monochrome_2(),
	NoiseAndScratches_t3793967858::get_offset_of_rgbFallback_3(),
	NoiseAndScratches_t3793967858::get_offset_of_grainIntensityMin_4(),
	NoiseAndScratches_t3793967858::get_offset_of_grainIntensityMax_5(),
	NoiseAndScratches_t3793967858::get_offset_of_grainSize_6(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchIntensityMin_7(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchIntensityMax_8(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchFPS_9(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchJitter_10(),
	NoiseAndScratches_t3793967858::get_offset_of_grainTexture_11(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchTexture_12(),
	NoiseAndScratches_t3793967858::get_offset_of_shaderRGB_13(),
	NoiseAndScratches_t3793967858::get_offset_of_shaderYUV_14(),
	NoiseAndScratches_t3793967858::get_offset_of_m_MaterialRGB_15(),
	NoiseAndScratches_t3793967858::get_offset_of_m_MaterialYUV_16(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchTimeLeft_17(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchX_18(),
	NoiseAndScratches_t3793967858::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (PostEffectsBase_t1831076792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	PostEffectsBase_t1831076792::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t1831076792::get_offset_of_supportDX11_3(),
	PostEffectsBase_t1831076792::get_offset_of_isSupported_4(),
	PostEffectsBase_t1831076792::get_offset_of_createdMaterials_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (PostEffectsHelper_t3737141539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (Quads_t3919723195), -1, sizeof(Quads_t3919723195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[2] = 
{
	Quads_t3919723195_StaticFields::get_offset_of_meshes_0(),
	Quads_t3919723195_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ScreenOverlay_t662123177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[5] = 
{
	ScreenOverlay_t662123177::get_offset_of_blendMode_6(),
	ScreenOverlay_t662123177::get_offset_of_intensity_7(),
	ScreenOverlay_t662123177::get_offset_of_texture_8(),
	ScreenOverlay_t662123177::get_offset_of_overlayShader_9(),
	ScreenOverlay_t662123177::get_offset_of_overlayMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (OverlayBlendMode_t3093108320)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2317[6] = 
{
	OverlayBlendMode_t3093108320::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (ScreenSpaceAmbientObscurance_t3449985455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[8] = 
{
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_intensity_6(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_radius_7(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_blurIterations_8(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_blurFilterDistance_9(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_downsample_10(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_rand_11(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_aoShader_12(),
	ScreenSpaceAmbientObscurance_t3449985455::get_offset_of_aoMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ScreenSpaceAmbientOcclusion_t1012925760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[11] = 
{
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_Radius_2(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_SampleCount_3(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_OcclusionIntensity_4(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_Blur_5(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_Downsampling_6(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_OcclusionAttenuation_7(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_MinZ_8(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_SSAOShader_9(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_SSAOMaterial_10(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_RandomTexture_11(),
	ScreenSpaceAmbientOcclusion_t1012925760::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (SSAOSamples_t2028001761)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[4] = 
{
	SSAOSamples_t2028001761::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (SepiaTone_t4284961859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (SunShafts_t135682244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[14] = 
{
	SunShafts_t135682244::get_offset_of_resolution_6(),
	SunShafts_t135682244::get_offset_of_screenBlendMode_7(),
	SunShafts_t135682244::get_offset_of_sunTransform_8(),
	SunShafts_t135682244::get_offset_of_radialBlurIterations_9(),
	SunShafts_t135682244::get_offset_of_sunColor_10(),
	SunShafts_t135682244::get_offset_of_sunThreshold_11(),
	SunShafts_t135682244::get_offset_of_sunShaftBlurRadius_12(),
	SunShafts_t135682244::get_offset_of_sunShaftIntensity_13(),
	SunShafts_t135682244::get_offset_of_maxRadius_14(),
	SunShafts_t135682244::get_offset_of_useDepthTexture_15(),
	SunShafts_t135682244::get_offset_of_sunShaftsShader_16(),
	SunShafts_t135682244::get_offset_of_sunShaftsMaterial_17(),
	SunShafts_t135682244::get_offset_of_simpleClearShader_18(),
	SunShafts_t135682244::get_offset_of_simpleClearMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (SunShaftsResolution_t3422901908)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2323[4] = 
{
	SunShaftsResolution_t3422901908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ShaftsScreenBlendMode_t1906873445)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[3] = 
{
	ShaftsScreenBlendMode_t1906873445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (TiltShift_t4026002340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[7] = 
{
	TiltShift_t4026002340::get_offset_of_mode_6(),
	TiltShift_t4026002340::get_offset_of_quality_7(),
	TiltShift_t4026002340::get_offset_of_blurArea_8(),
	TiltShift_t4026002340::get_offset_of_maxBlurSize_9(),
	TiltShift_t4026002340::get_offset_of_downsample_10(),
	TiltShift_t4026002340::get_offset_of_tiltShiftShader_11(),
	TiltShift_t4026002340::get_offset_of_tiltShiftMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (TiltShiftMode_t1874920922)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[3] = 
{
	TiltShiftMode_t1874920922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (TiltShiftQuality_t3606954146)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2327[5] = 
{
	TiltShiftQuality_t3606954146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (Tonemapping_t2013258899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[13] = 
{
	Tonemapping_t2013258899::get_offset_of_type_6(),
	Tonemapping_t2013258899::get_offset_of_adaptiveTextureSize_7(),
	Tonemapping_t2013258899::get_offset_of_remapCurve_8(),
	Tonemapping_t2013258899::get_offset_of_curveTex_9(),
	Tonemapping_t2013258899::get_offset_of_exposureAdjustment_10(),
	Tonemapping_t2013258899::get_offset_of_middleGrey_11(),
	Tonemapping_t2013258899::get_offset_of_white_12(),
	Tonemapping_t2013258899::get_offset_of_adaptionSpeed_13(),
	Tonemapping_t2013258899::get_offset_of_tonemapper_14(),
	Tonemapping_t2013258899::get_offset_of_validRenderTextureFormat_15(),
	Tonemapping_t2013258899::get_offset_of_tonemapMaterial_16(),
	Tonemapping_t2013258899::get_offset_of_rt_17(),
	Tonemapping_t2013258899::get_offset_of_rtFormat_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (TonemapperType_t118564868)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2329[8] = 
{
	TonemapperType_t118564868::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (AdaptiveTexSize_t2410714347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2330[8] = 
{
	AdaptiveTexSize_t2410714347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (Triangles_t884492039), -1, sizeof(Triangles_t884492039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	Triangles_t884492039_StaticFields::get_offset_of_meshes_0(),
	Triangles_t884492039_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (Twirl_t1827184245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[3] = 
{
	Twirl_t1827184245::get_offset_of_radius_4(),
	Twirl_t1827184245::get_offset_of_angle_5(),
	Twirl_t1827184245::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (VignetteAndChromaticAberration_t137156492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[14] = 
{
	VignetteAndChromaticAberration_t137156492::get_offset_of_mode_6(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_intensity_7(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_chromaticAberration_8(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_axialAberration_9(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_blur_10(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_blurSpread_11(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_luminanceDependency_12(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_blurDistance_13(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_vignetteShader_14(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_separableBlurShader_15(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_chromAberrationShader_16(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_m_VignetteMaterial_17(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_m_SeparableBlurMaterial_18(),
	VignetteAndChromaticAberration_t137156492::get_offset_of_m_ChromAberrationMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (AberrationMode_t2592060798)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	AberrationMode_t2592060798::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (Vortex_t702210748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[3] = 
{
	Vortex_t702210748::get_offset_of_radius_4(),
	Vortex_t702210748::get_offset_of_angle_5(),
	Vortex_t702210748::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (GUISizer_t1539903446), -1, sizeof(GUISizer_t1539903446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	GUISizer_t1539903446_StaticFields::get_offset_of_WIDTH_0(),
	GUISizer_t1539903446_StaticFields::get_offset_of_HEIGHT_1(),
	GUISizer_t1539903446_StaticFields::get_offset_of_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (U3CModuleU3E_t1429447287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (AlwaysXZero_t3043892783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (BodyData_t2328741553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[9] = 
{
	BodyData_t2328741553::get_offset_of_hairStyle_0(),
	BodyData_t2328741553::get_offset_of_hairColour_1(),
	BodyData_t2328741553::get_offset_of_skinColour_2(),
	BodyData_t2328741553::get_offset_of_clothColour_3(),
	BodyData_t2328741553::get_offset_of_eyeColour_4(),
	BodyData_t2328741553::get_offset_of_trimColour_5(),
	BodyData_t2328741553::get_offset_of_backPackNumber_6(),
	BodyData_t2328741553::get_offset_of_bodyType_7(),
	BodyData_t2328741553::get_offset_of_trimTexture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (BodyPartHelper_t2433202137), -1, sizeof(BodyPartHelper_t2433202137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2340[15] = 
{
	BodyPartHelper_t2433202137::get_offset_of_bodyMaterial_2(),
	BodyPartHelper_t2433202137::get_offset_of_trimMaterialNumber_3(),
	BodyPartHelper_t2433202137::get_offset_of_hairMaterialNumber_4(),
	BodyPartHelper_t2433202137::get_offset_of_eyeMaterialNumber_5(),
	BodyPartHelper_t2433202137::get_offset_of_trimMaterial_6(),
	BodyPartHelper_t2433202137::get_offset_of_clothMaterial_7(),
	BodyPartHelper_t2433202137::get_offset_of_hairMaterial_8(),
	BodyPartHelper_t2433202137::get_offset_of_eyeMaterial_9(),
	BodyPartHelper_t2433202137::get_offset_of_body_10(),
	BodyPartHelper_t2433202137::get_offset_of_head_11(),
	BodyPartHelper_t2433202137::get_offset_of_hair1_12(),
	BodyPartHelper_t2433202137::get_offset_of_bag_13(),
	BodyPartHelper_t2433202137::get_offset_of_characterName_14(),
	BodyPartHelper_t2433202137_StaticFields::get_offset_of_bodyObjects_15(),
	BodyPartHelper_t2433202137::get_offset_of_setBodyDataOnRefresh_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (BonusSpark_t759759043), -1, sizeof(BonusSpark_t759759043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2341[7] = 
{
	BonusSpark_t759759043_StaticFields::get_offset_of_bonusSparkNums_2(),
	BonusSpark_t759759043_StaticFields::get_offset_of_maxLevelSparks_3(),
	BonusSpark_t759759043_StaticFields::get_offset_of_numLevelSparx_4(),
	BonusSpark_t759759043_StaticFields::get_offset_of_sparxText_5(),
	BonusSpark_t759759043_StaticFields::get_offset_of_img_6(),
	BonusSpark_t759759043::get_offset_of_myID_7(),
	BonusSpark_t759759043::get_offset_of_instanceTHL_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (CanvasNotebook_t2500940296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[3] = 
{
	CanvasNotebook_t2500940296::get_offset_of_ChangeableText_2(),
	CanvasNotebook_t2500940296::get_offset_of_fields_3(),
	CanvasNotebook_t2500940296::get_offset_of_images_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (ECustomisationButton_t3006556935)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[14] = 
{
	ECustomisationButton_t3006556935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (CharacterCreation_t2214710272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[15] = 
{
	CharacterCreation_t2214710272::get_offset_of_hairStyle_2(),
	CharacterCreation_t2214710272::get_offset_of_hairColour_3(),
	CharacterCreation_t2214710272::get_offset_of_skinColour_4(),
	CharacterCreation_t2214710272::get_offset_of_eyeColour_5(),
	CharacterCreation_t2214710272::get_offset_of_clothingColour_6(),
	CharacterCreation_t2214710272::get_offset_of_trim_7(),
	CharacterCreation_t2214710272::get_offset_of_trimColour_8(),
	CharacterCreation_t2214710272::get_offset_of_backpack_9(),
	CharacterCreation_t2214710272::get_offset_of_confirm_10(),
	CharacterCreation_t2214710272::get_offset_of_cancel_11(),
	CharacterCreation_t2214710272::get_offset_of_leftSpin_12(),
	CharacterCreation_t2214710272::get_offset_of_rightSpin_13(),
	CharacterCreation_t2214710272::get_offset_of_clothing_14(),
	CharacterCreation_t2214710272::get_offset_of_customisationButtons_15(),
	CharacterCreation_t2214710272::get_offset_of_wasClicked_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (CharacterCustomiseBackground_t4088931509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (CinemachineBrain_t679733552), -1, sizeof(CinemachineBrain_t679733552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2346[21] = 
{
	CinemachineBrain_t679733552::get_offset_of_m_ShowDebugText_2(),
	CinemachineBrain_t679733552::get_offset_of_m_ShowCameraFrustum_3(),
	CinemachineBrain_t679733552::get_offset_of_m_WorldUpOverride_4(),
	CinemachineBrain_t679733552::get_offset_of_m_UpdateMethod_5(),
	CinemachineBrain_t679733552::get_offset_of_m_DefaultBlend_6(),
	CinemachineBrain_t679733552::get_offset_of_m_CustomBlends_7(),
	CinemachineBrain_t679733552::get_offset_of_m_OutputCamera_8(),
	CinemachineBrain_t679733552::get_offset_of_U3CPostFXHandlerU3Ek__BackingField_9(),
	CinemachineBrain_t679733552::get_offset_of_m_CameraCutEvent_10(),
	CinemachineBrain_t679733552::get_offset_of_m_CameraActivatedEvent_11(),
	CinemachineBrain_t679733552_StaticFields::get_offset_of_U3CSoloCameraU3Ek__BackingField_12(),
	CinemachineBrain_t679733552::get_offset_of_mActiveCameraPreviousFrame_13(),
	CinemachineBrain_t679733552::get_offset_of_mOutgoingCameraPreviousFrame_14(),
	CinemachineBrain_t679733552::get_offset_of_mActiveBlend_15(),
	CinemachineBrain_t679733552::get_offset_of_mPreviousFrameWasOverride_16(),
	CinemachineBrain_t679733552::get_offset_of_mOverrideStack_17(),
	CinemachineBrain_t679733552::get_offset_of_mNextOverrideId_18(),
	CinemachineBrain_t679733552::get_offset_of_U3CIsSuspendedU3Ek__BackingField_19(),
	CinemachineBrain_t679733552_StaticFields::get_offset_of_msCurrentFrame_20(),
	CinemachineBrain_t679733552_StaticFields::get_offset_of_msFirstBrainObjectId_21(),
	CinemachineBrain_t679733552_StaticFields::get_offset_of_msSubframes_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (UpdateMethod_t3260873211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	UpdateMethod_t3260873211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (PostFXHandlerDelegate_t2862114331), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (OverrideStackFrame_t2350841338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[4] = 
{
	OverrideStackFrame_t2350841338::get_offset_of_id_0(),
	OverrideStackFrame_t2350841338::get_offset_of_camera_1(),
	OverrideStackFrame_t2350841338::get_offset_of_blend_2(),
	OverrideStackFrame_t2350841338::get_offset_of_deltaTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CAfterPhysicsU3Ec__Iterator0_t1567695154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[4] = 
{
	U3CAfterPhysicsU3Ec__Iterator0_t1567695154::get_offset_of_U24this_0(),
	U3CAfterPhysicsU3Ec__Iterator0_t1567695154::get_offset_of_U24current_1(),
	U3CAfterPhysicsU3Ec__Iterator0_t1567695154::get_offset_of_U24disposing_2(),
	U3CAfterPhysicsU3Ec__Iterator0_t1567695154::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (StaticPointVirtualCamera_t3747499034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[5] = 
{
	StaticPointVirtualCamera_t3747499034::get_offset_of_U3CNameU3Ek__BackingField_0(),
	StaticPointVirtualCamera_t3747499034::get_offset_of_U3CPriorityU3Ek__BackingField_1(),
	StaticPointVirtualCamera_t3747499034::get_offset_of_U3CLookAtU3Ek__BackingField_2(),
	StaticPointVirtualCamera_t3747499034::get_offset_of_U3CFollowU3Ek__BackingField_3(),
	StaticPointVirtualCamera_t3747499034::get_offset_of_U3CStateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (BlendSourceVirtualCamera_t3437615881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[5] = 
{
	BlendSourceVirtualCamera_t3437615881::get_offset_of_U3CBlendU3Ek__BackingField_0(),
	BlendSourceVirtualCamera_t3437615881::get_offset_of_U3CPriorityU3Ek__BackingField_1(),
	BlendSourceVirtualCamera_t3437615881::get_offset_of_U3CLookAtU3Ek__BackingField_2(),
	BlendSourceVirtualCamera_t3437615881::get_offset_of_U3CFollowU3Ek__BackingField_3(),
	BlendSourceVirtualCamera_t3437615881::get_offset_of_U3CStateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (CinemachineClearShot_t4289735864), -1, sizeof(CinemachineClearShot_t4289735864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2353[17] = 
{
	CinemachineClearShot_t4289735864::get_offset_of_m_LookAt_12(),
	CinemachineClearShot_t4289735864::get_offset_of_m_Follow_13(),
	CinemachineClearShot_t4289735864::get_offset_of_m_ShowDebugText_14(),
	CinemachineClearShot_t4289735864::get_offset_of_m_ChildCameras_15(),
	CinemachineClearShot_t4289735864::get_offset_of_m_ActivateAfter_16(),
	CinemachineClearShot_t4289735864::get_offset_of_m_MinDuration_17(),
	CinemachineClearShot_t4289735864::get_offset_of_m_RandomizeChoice_18(),
	CinemachineClearShot_t4289735864::get_offset_of_m_DefaultBlend_19(),
	CinemachineClearShot_t4289735864::get_offset_of_m_CustomBlends_20(),
	CinemachineClearShot_t4289735864::get_offset_of_U3CLiveChildU3Ek__BackingField_21(),
	CinemachineClearShot_t4289735864::get_offset_of_m_State_22(),
	CinemachineClearShot_t4289735864::get_offset_of_mActivationTime_23(),
	CinemachineClearShot_t4289735864::get_offset_of_mPendingActivationTime_24(),
	CinemachineClearShot_t4289735864::get_offset_of_mPendingCamera_25(),
	CinemachineClearShot_t4289735864::get_offset_of_mActiveBlend_26(),
	CinemachineClearShot_t4289735864::get_offset_of_m_RandomizedChilden_27(),
	CinemachineClearShot_t4289735864_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (Pair_t2832965450)+ sizeof (RuntimeObject), sizeof(Pair_t2832965450 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	Pair_t2832965450::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Pair_t2832965450::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (CinemachineCollider_t3890891415), -1, sizeof(CinemachineCollider_t3890891415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[17] = 
{
	CinemachineCollider_t3890891415::get_offset_of_m_CollideAgainst_2(),
	CinemachineCollider_t3890891415::get_offset_of_m_PreserveLineOfSight_3(),
	CinemachineCollider_t3890891415::get_offset_of_m_LineOfSightFeelerDistance_4(),
	CinemachineCollider_t3890891415::get_offset_of_m_MinimumDistanceFromTarget_5(),
	CinemachineCollider_t3890891415::get_offset_of_m_MinimumDistanceFromCamera_6(),
	CinemachineCollider_t3890891415::get_offset_of_m_UseCurbFeelers_7(),
	CinemachineCollider_t3890891415::get_offset_of_m_CurbFeelerDistance_8(),
	CinemachineCollider_t3890891415::get_offset_of_m_CurbResistance_9(),
	CinemachineCollider_t3890891415::get_offset_of_m_PositionSmoothing_10(),
	CinemachineCollider_t3890891415::get_offset_of_m_OptimalTargetDistance_11(),
	CinemachineCollider_t3890891415::get_offset_of_U3CVirtualCameraU3Ek__BackingField_12(),
	CinemachineCollider_t3890891415_StaticFields::get_offset_of_kLocalUpRight_13(),
	CinemachineCollider_t3890891415_StaticFields::get_offset_of_kLocalUpLeft_14(),
	CinemachineCollider_t3890891415_StaticFields::get_offset_of_kLocalDownRight_15(),
	CinemachineCollider_t3890891415_StaticFields::get_offset_of_kLocalDownLeft_16(),
	0,
	CinemachineCollider_t3890891415::get_offset_of_mExtraState_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (CompiledCurbFeeler_t1909537055)+ sizeof (RuntimeObject), sizeof(CompiledCurbFeeler_t1909537055_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[5] = 
{
	CompiledCurbFeeler_t1909537055::get_offset_of_LocalVector_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompiledCurbFeeler_t1909537055::get_offset_of_RayDistance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompiledCurbFeeler_t1909537055::get_offset_of_DampingConstant_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompiledCurbFeeler_t1909537055::get_offset_of_IsHit_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompiledCurbFeeler_t1909537055::get_offset_of_HitDistance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (VcamExtraState_t818142455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[7] = 
{
	VcamExtraState_t818142455::get_offset_of_filter_0(),
	VcamExtraState_t818142455::get_offset_of_colliderDisplacementDecay_1(),
	VcamExtraState_t818142455::get_offset_of_colliderDisplacement_2(),
	VcamExtraState_t818142455::get_offset_of_targetObscured_3(),
	VcamExtraState_t818142455::get_offset_of_curbResistance_4(),
	VcamExtraState_t818142455::get_offset_of_feelerDistance_5(),
	VcamExtraState_t818142455::get_offset_of_curbFeelers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (CinemachineExternalCamera_t3112248349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[4] = 
{
	CinemachineExternalCamera_t3112248349::get_offset_of_m_Camera_12(),
	CinemachineExternalCamera_t3112248349::get_offset_of_m_State_13(),
	CinemachineExternalCamera_t3112248349::get_offset_of_U3CLookAtU3Ek__BackingField_14(),
	CinemachineExternalCamera_t3112248349::get_offset_of_U3CFollowU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (CinemachineFollowZoom_t904737256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[6] = 
{
	CinemachineFollowZoom_t904737256::get_offset_of_m_Width_2(),
	CinemachineFollowZoom_t904737256::get_offset_of_m_Damping_3(),
	CinemachineFollowZoom_t904737256::get_offset_of_m_MinFOV_4(),
	CinemachineFollowZoom_t904737256::get_offset_of_m_MaxFOV_5(),
	CinemachineFollowZoom_t904737256::get_offset_of_U3CVirtualCameraU3Ek__BackingField_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (CinemachineFreeLook_t411363320), -1, sizeof(CinemachineFreeLook_t411363320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2360[17] = 
{
	CinemachineFreeLook_t411363320::get_offset_of_m_LookAt_12(),
	CinemachineFreeLook_t411363320::get_offset_of_m_Follow_13(),
	CinemachineFreeLook_t411363320::get_offset_of_m_DampingStyle_14(),
	CinemachineFreeLook_t411363320::get_offset_of_m_HeadingBias_15(),
	CinemachineFreeLook_t411363320::get_offset_of_m_UseCommonLensSetting_16(),
	CinemachineFreeLook_t411363320::get_offset_of_m_Lens_17(),
	CinemachineFreeLook_t411363320::get_offset_of_m_XAxis_18(),
	CinemachineFreeLook_t411363320::get_offset_of_m_YAxis_19(),
	CinemachineFreeLook_t411363320::get_offset_of_m_RecenterToTargetHeading_20(),
	CinemachineFreeLook_t411363320::get_offset_of_m_SplineTension_21(),
	CinemachineFreeLook_t411363320::get_offset_of_m_State_22(),
	CinemachineFreeLook_t411363320::get_offset_of_m_Rigs_23(),
	CinemachineFreeLook_t411363320::get_offset_of_mOribitals_24(),
	CinemachineFreeLook_t411363320::get_offset_of_mBlendA_25(),
	CinemachineFreeLook_t411363320::get_offset_of_mBlendB_26(),
	CinemachineFreeLook_t411363320_StaticFields::get_offset_of_CreateRigOverride_27(),
	CinemachineFreeLook_t411363320_StaticFields::get_offset_of_DestroyRigOverride_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (CreateRigDelegate_t931026514), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (DestroyRigDelegate_t2622870986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (CinemachinePath_t3941522772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[3] = 
{
	CinemachinePath_t3941522772::get_offset_of_m_Appearance_2(),
	CinemachinePath_t3941522772::get_offset_of_m_Looped_3(),
	CinemachinePath_t3941522772::get_offset_of_m_Waypoints_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (Appearance_t955816971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	Appearance_t955816971::get_offset_of_pathColor_0(),
	Appearance_t955816971::get_offset_of_inactivePathColor_1(),
	Appearance_t955816971::get_offset_of_handleColor_2(),
	Appearance_t955816971::get_offset_of_width_3(),
	Appearance_t955816971::get_offset_of_steps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (Waypoint_t552772035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[3] = 
{
	Waypoint_t552772035::get_offset_of_position_0(),
	Waypoint_t552772035::get_offset_of_tangent_1(),
	Waypoint_t552772035::get_offset_of_roll_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (CinemachinePipeline_t1082879987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (CinemachineSmoother_t3297040134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[7] = 
{
	CinemachineSmoother_t3297040134::get_offset_of_m_PositionSmoothing_2(),
	CinemachineSmoother_t3297040134::get_offset_of_m_LookAtSmoothing_3(),
	CinemachineSmoother_t3297040134::get_offset_of_m_RotationSmoothing_4(),
	CinemachineSmoother_t3297040134::get_offset_of_U3CVirtualCameraU3Ek__BackingField_5(),
	CinemachineSmoother_t3297040134::get_offset_of_mSmoothingFilter_6(),
	CinemachineSmoother_t3297040134::get_offset_of_mSmoothingFilterLookAt_7(),
	CinemachineSmoother_t3297040134::get_offset_of_mSmoothingFilterRotation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (CinemachineStateDrivenCamera_t2857997874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[20] = 
{
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_LookAt_12(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_Follow_13(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_ShowDebugText_14(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_EnableAllChildCameras_15(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_ChildCameras_16(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_AnimatedTarget_17(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_LayerIndex_18(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_Instructions_19(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_DefaultBlend_20(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_CustomBlends_21(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_ParentHash_22(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_U3CLiveChildU3Ek__BackingField_23(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_m_State_24(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mActivationTime_25(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mActiveInstruction_26(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mPendingActivationTime_27(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mPendingInstruction_28(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mActiveBlend_29(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mInstructionDictionary_30(),
	CinemachineStateDrivenCamera_t2857997874::get_offset_of_mStateParentLookup_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (Instruction_t3855846037)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	Instruction_t3855846037::get_offset_of_m_FullHash_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t3855846037::get_offset_of_m_VirtualCamera_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t3855846037::get_offset_of_m_ActivateAfter_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t3855846037::get_offset_of_m_MinDuration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ParentHash_t1020415291)+ sizeof (RuntimeObject), sizeof(ParentHash_t1020415291 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[2] = 
{
	ParentHash_t1020415291::get_offset_of_m_Hash_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParentHash_t1020415291::get_offset_of_m_ParentHash_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (CinemachineTargetGroup_t3734833537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[3] = 
{
	CinemachineTargetGroup_t3734833537::get_offset_of_m_PositionMode_2(),
	CinemachineTargetGroup_t3734833537::get_offset_of_m_RotationMode_3(),
	CinemachineTargetGroup_t3734833537::get_offset_of_m_Targets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (Target_t2477784577)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[3] = 
{
	Target_t2477784577::get_offset_of_target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Target_t2477784577::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Target_t2477784577::get_offset_of_radius_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (PositionMode_t4058225743)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	PositionMode_t4058225743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (RotationMode_t2604816688)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2374[3] = 
{
	RotationMode_t2604816688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (CinemachineVirtualCamera_t2803177733), -1, sizeof(CinemachineVirtualCamera_t2803177733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2375[12] = 
{
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_LookAt_12(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_Follow_13(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_Lens_14(),
	0,
	CinemachineVirtualCamera_t2803177733_StaticFields::get_offset_of_CreatePipelineOverride_16(),
	CinemachineVirtualCamera_t2803177733_StaticFields::get_offset_of_DestroyPipelineOverride_17(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_U3CSuppressOrientationUpdateU3Ek__BackingField_18(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_State_19(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_PreviousState_20(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_ComponentPipeline_21(),
	CinemachineVirtualCamera_t2803177733::get_offset_of_m_ComponentOwner_22(),
	CinemachineVirtualCamera_t2803177733_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (CreatePipelineDelegate_t4206421982), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (DestroyPipelineDelegate_t926775217), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (CinemachineBasicMultiChannelPerlin_t3993756505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[6] = 
{
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_m_Definition_2(),
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_m_AmplitudeGain_3(),
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_m_FrequencyGain_4(),
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_mInitialized_5(),
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_mNoiseTime_6(),
	CinemachineBasicMultiChannelPerlin_t3993756505::get_offset_of_mNoiseOffsets_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (CinemachineComposer_t3213114616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[14] = 
{
	CinemachineComposer_t3213114616::get_offset_of_OnGUICallback_2(),
	CinemachineComposer_t3213114616::get_offset_of_m_ShowGuides_3(),
	CinemachineComposer_t3213114616::get_offset_of_m_TrackedObjectOffset_4(),
	CinemachineComposer_t3213114616::get_offset_of_m_HorizontalDamping_5(),
	CinemachineComposer_t3213114616::get_offset_of_m_VerticalDamping_6(),
	CinemachineComposer_t3213114616::get_offset_of_m_ScreenX_7(),
	CinemachineComposer_t3213114616::get_offset_of_m_ScreenY_8(),
	CinemachineComposer_t3213114616::get_offset_of_m_DeadZoneWidth_9(),
	CinemachineComposer_t3213114616::get_offset_of_m_DeadZoneHeight_10(),
	CinemachineComposer_t3213114616::get_offset_of_m_SoftZoneWidth_11(),
	CinemachineComposer_t3213114616::get_offset_of_m_SoftZoneHeight_12(),
	CinemachineComposer_t3213114616::get_offset_of_m_BiasX_13(),
	CinemachineComposer_t3213114616::get_offset_of_m_BiasY_14(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (CinemachineGroupComposer_t2240148537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[13] = 
{
	CinemachineGroupComposer_t2240148537::get_offset_of_m_GroupFramingSize_16(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_FramingMode_17(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_FrameDamping_18(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_AdjustmentMode_19(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MaxDollyIn_20(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MaxDollyOut_21(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MinimumDistance_22(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MaximumDistance_23(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MinimumFOV_24(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_MaximumFOV_25(),
	CinemachineGroupComposer_t2240148537::get_offset_of_m_prevTargetHeight_26(),
	CinemachineGroupComposer_t2240148537::get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_27(),
	CinemachineGroupComposer_t2240148537::get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (FramingMode_t2203084160)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[4] = 
{
	FramingMode_t2203084160::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (AdjustmentMode_t1416763142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2382[4] = 
{
	AdjustmentMode_t1416763142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (CinemachineOrbitalTransposer_t281264161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[19] = 
{
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_Radius_2(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_HeightOffset_3(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_XDamping_4(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_YDamping_5(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_ZDamping_6(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_DampingStyle_7(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_HeadingBias_8(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_XAxis_9(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_RecenterToTargetHeading_10(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_m_HeadingIsSlave_11(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_U3CUseOffsetOverrideU3Ek__BackingField_12(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_U3COffsetOverrideU3Ek__BackingField_13(),
	0,
	CinemachineOrbitalTransposer_t281264161::get_offset_of_mLastHeadingAxisInputTime_15(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_mHeadingRecenteringVelocity_16(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_mLastTargetPosition_17(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_mHeadingTracker_18(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_mTargetRigidBody_19(),
	CinemachineOrbitalTransposer_t281264161::get_offset_of_U3CPreviousTargetU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (DampingStyle_t1040084675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2384[3] = 
{
	DampingStyle_t1040084675::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (AxisState_t2333558477)+ sizeof (RuntimeObject), sizeof(AxisState_t2333558477_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2385[10] = 
{
	AxisState_t2333558477::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_m_MaxSpeed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_m_AccelTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_m_DecelTime_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_m_InputAxisName_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_m_InputAxisValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_mCurrentSpeed_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_mMinValue_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_mMaxValue_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t2333558477::get_offset_of_mWrapAround_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (Recentering_t75128244)+ sizeof (RuntimeObject), sizeof(Recentering_t75128244_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2386[5] = 
{
	Recentering_t75128244::get_offset_of_m_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t75128244::get_offset_of_m_RecenterWaitTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t75128244::get_offset_of_m_RecenteringTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t75128244::get_offset_of_m_HeadingDefinition_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t75128244::get_offset_of_m_VelocityFilterStrength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (HeadingDerivationMode_t2507807219)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2387[5] = 
{
	HeadingDerivationMode_t2507807219::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (HeadingTracker_t2287318816), -1, sizeof(HeadingTracker_t2287318816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2388[9] = 
{
	HeadingTracker_t2287318816::get_offset_of_mHistory_0(),
	HeadingTracker_t2287318816::get_offset_of_mTop_1(),
	HeadingTracker_t2287318816::get_offset_of_mBottom_2(),
	HeadingTracker_t2287318816::get_offset_of_mCount_3(),
	HeadingTracker_t2287318816::get_offset_of_mHeadingSum_4(),
	HeadingTracker_t2287318816::get_offset_of_mWeightSum_5(),
	HeadingTracker_t2287318816::get_offset_of_mWeightTime_6(),
	HeadingTracker_t2287318816::get_offset_of_mLastGoodHeading_7(),
	HeadingTracker_t2287318816_StaticFields::get_offset_of_mDecayExponent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (Item_t3005053575)+ sizeof (RuntimeObject), sizeof(Item_t3005053575 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2389[3] = 
{
	Item_t3005053575::get_offset_of_velocity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t3005053575::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t3005053575::get_offset_of_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (CinemachineTrackedDolly_t3518348890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[10] = 
{
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_Path_2(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_PathPosition_3(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_PathOffset_4(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_XDamping_5(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_YDamping_6(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_ZDamping_7(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_CameraUp_8(),
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_AutoDolly_9(),
	0,
	CinemachineTrackedDolly_t3518348890::get_offset_of_m_PreviousPathPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (CameraUpMode_t3416712389)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2391[6] = 
{
	CameraUpMode_t3416712389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (AutoDolly_t756678745)+ sizeof (RuntimeObject), sizeof(AutoDolly_t756678745_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2392[3] = 
{
	AutoDolly_t756678745::get_offset_of_m_Enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoDolly_t756678745::get_offset_of_m_SearchRadius_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoDolly_t756678745::get_offset_of_m_StepsPerSegment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (CinemachineTransposer_t1461967720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[8] = 
{
	CinemachineTransposer_t1461967720::get_offset_of_m_FollowOffset_2(),
	CinemachineTransposer_t1461967720::get_offset_of_m_XDamping_3(),
	CinemachineTransposer_t1461967720::get_offset_of_m_YDamping_4(),
	CinemachineTransposer_t1461967720::get_offset_of_m_ZDamping_5(),
	CinemachineTransposer_t1461967720::get_offset_of_m_BindingMode_6(),
	0,
	CinemachineTransposer_t1461967720::get_offset_of_m_targetOrientationOnAssign_8(),
	CinemachineTransposer_t1461967720::get_offset_of_m_previousTarget_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (TransposerOffsetType_t2447451693)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[6] = 
{
	TransposerOffsetType_t2447451693::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (CameraState_t382403230)+ sizeof (RuntimeObject), sizeof(CameraState_t382403230_marshaled_pinvoke), sizeof(CameraState_t382403230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2395[9] = 
{
	CameraState_t382403230::get_offset_of_U3CLensU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3CReferenceUpU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230_StaticFields::get_offset_of_kNoPoint_3(),
	CameraState_t382403230::get_offset_of_U3CRawPositionU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3CRawOrientationU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3CShotQualityU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3CPositionCorrectionU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t382403230::get_offset_of_U3COrientationCorrectionU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (CinemachineBlend_t2146485918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[4] = 
{
	CinemachineBlend_t2146485918::get_offset_of_U3CCamAU3Ek__BackingField_0(),
	CinemachineBlend_t2146485918::get_offset_of_U3CCamBU3Ek__BackingField_1(),
	CinemachineBlend_t2146485918::get_offset_of_U3CBlendCurveU3Ek__BackingField_2(),
	CinemachineBlend_t2146485918::get_offset_of_U3CTimeInBlendU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (CinemachineBlendDefinition_t3981438518)+ sizeof (RuntimeObject), sizeof(CinemachineBlendDefinition_t3981438518 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	CinemachineBlendDefinition_t3981438518::get_offset_of_m_Style_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CinemachineBlendDefinition_t3981438518::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Style_t4107124224)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2398[8] = 
{
	Style_t4107124224::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (CinemachineBlenderSettings_t865950197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[2] = 
{
	CinemachineBlenderSettings_t865950197::get_offset_of_m_CustomBlends_2(),
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
