﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityEngine.WWW
struct WWW_t3599262362;
// DownloadTexture
struct DownloadTexture_t1340095241;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Void
struct Void_t653366341;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// System.Collections.Generic.List`1<InvStat>
struct List_1_t4155999403;
// UIAtlas
struct UIAtlas_t1815452364;
// InvBaseItem
struct InvBaseItem_t82225019;
// System.Comparison`1<InvStat>
struct Comparison_1_t2125462953;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UIScrollBar
struct UIScrollBar_t3221813871;
// ActiveAnimation
struct ActiveAnimation_t1706595508;
// UISprite
struct UISprite_t279728715;
// UnityEngine.Camera
struct Camera_t2839736942;
// UIScrollBar/OnScrollBarChange
struct OnScrollBarChange_t2155012818;
// UITable/OnReposition
struct OnReposition_t2742626599;
// UIPanel
struct UIPanel_t825970685;
// UIDraggablePanel
struct UIDraggablePanel_t2580300900;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t3776653899;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t1746117449;
// UISlider
struct UISlider_t1813774206;
// UIPopupList
struct UIPopupList_t2907709271;
// UICheckbox
struct UICheckbox_t1205955448;
// UnityEngine.Color[]
struct ColorU5BU5D_t247935167;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t1976598158;
// UIWidget[]
struct UIWidgetU5BU5D_t2376853568;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UIFont
struct UIFont_t2730669065;
// UILabel
struct UILabel_t3478074517;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// UIPopupList/OnSelectionChange
struct OnSelectionChange_t705636838;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t2597701524;
// SpringPanel/OnFinished
struct OnFinished_t2500445478;
// UnityEngine.Material
struct Material_t2815264910;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// UITextList
struct UITextList_t1100452823;
// UIInput
struct UIInput_t1674607152;
// InvGameItem[]
struct InvGameItemU5BU5D_t1090198953;
// InvAttachmentPoint[]
struct InvAttachmentPointU5BU5D_t579766857;
// InvDatabase[]
struct InvDatabaseU5BU5D_t1364645520;
// System.Collections.Generic.List`1<InvBaseItem>
struct List_1_t3496819322;
// UIWidget
struct UIWidget_t1961100141;
// System.Collections.Generic.List`1<InvGameItem>
struct List_1_t740300919;
// UnityEngine.AudioClip
struct AudioClip_t1419814565;
// InvGameItem
struct InvGameItem_t1620673912;
// InvEquipment
struct InvEquipment_t2308977056;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// UnityEngine.UI.Button
struct Button_t1293135404;
// UnityEngine.UI.Text
struct Text_t1790657652;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t1489827645;
// UnityEngine.UI.Image
struct Image_t2816987602;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t602728325;
// System.Collections.Generic.List`1<DragableInformation>
struct List_1_t1390258121;
// System.String[]
struct StringU5BU5D_t2511808107;
// DragObject[]
struct DragObjectU5BU5D_t3683517183;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1618594486;
// Province[]
struct ProvinceU5BU5D_t4026361422;
// UnityEngine.Animation
struct Animation_t3821138400;
// UnityEngine.AnimationClip
struct AnimationClip_t1145879031;
// System.Collections.Generic.List`1<UnityEngine.AnimationClip>
struct List_1_t265506038;
// ActiveAnimation/OnFinished
struct OnFinished_t1828027647;
// UITweener/OnFinished
struct OnFinished_t2354188770;
// UITweener[]
struct UITweenerU5BU5D_t4289166548;
// UICheckbox/OnStateChange
struct OnStateChange_t2569002187;
// UnityEngine.Rigidbody
struct Rigidbody_t4273256674;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// UIDraggablePanel/OnDragFinished
struct OnDragFinished_t81282452;
// UIRoot
struct UIRoot_t937406327;
// UIItemStorage
struct UIItemStorage_t1447122887;
// UISlider/OnValueChange
struct OnValueChange_t3441845912;
// UnityEngine.BoxCollider
struct BoxCollider_t1717529639;
// UIFilledSprite
struct UIFilledSprite_t3395723239;
// UIDraggableCamera
struct UIDraggableCamera_t1133134831;
// UnityEngine.Component
struct Component_t1632713610;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DRAGABLEINFORMATION_T2270631114_H
#define DRAGABLEINFORMATION_T2270631114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragableInformation
struct  DragableInformation_t2270631114  : public RuntimeObject
{
public:
	// UnityEngine.GameObject DragableInformation::obj
	GameObject_t2557347079 * ___obj_0;
	// UnityEngine.Transform DragableInformation::targetTransform
	Transform_t362059596 * ___targetTransform_1;
	// System.Int32 DragableInformation::correctAnswer
	int32_t ___correctAnswer_2;
	// System.Boolean DragableInformation::placedCorrectly
	bool ___placedCorrectly_3;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(DragableInformation_t2270631114, ___obj_0)); }
	inline GameObject_t2557347079 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t2557347079 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t2557347079 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_targetTransform_1() { return static_cast<int32_t>(offsetof(DragableInformation_t2270631114, ___targetTransform_1)); }
	inline Transform_t362059596 * get_targetTransform_1() const { return ___targetTransform_1; }
	inline Transform_t362059596 ** get_address_of_targetTransform_1() { return &___targetTransform_1; }
	inline void set_targetTransform_1(Transform_t362059596 * value)
	{
		___targetTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_1), value);
	}

	inline static int32_t get_offset_of_correctAnswer_2() { return static_cast<int32_t>(offsetof(DragableInformation_t2270631114, ___correctAnswer_2)); }
	inline int32_t get_correctAnswer_2() const { return ___correctAnswer_2; }
	inline int32_t* get_address_of_correctAnswer_2() { return &___correctAnswer_2; }
	inline void set_correctAnswer_2(int32_t value)
	{
		___correctAnswer_2 = value;
	}

	inline static int32_t get_offset_of_placedCorrectly_3() { return static_cast<int32_t>(offsetof(DragableInformation_t2270631114, ___placedCorrectly_3)); }
	inline bool get_placedCorrectly_3() const { return ___placedCorrectly_3; }
	inline bool* get_address_of_placedCorrectly_3() { return &___placedCorrectly_3; }
	inline void set_placedCorrectly_3(bool value)
	{
		___placedCorrectly_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGABLEINFORMATION_T2270631114_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2951566387_H
#define U3CSTARTU3EC__ITERATOR0_T2951566387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadTexture/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2951566387  : public RuntimeObject
{
public:
	// UnityEngine.WWW DownloadTexture/<Start>c__Iterator0::<www>__0
	WWW_t3599262362 * ___U3CwwwU3E__0_0;
	// DownloadTexture DownloadTexture/<Start>c__Iterator0::$this
	DownloadTexture_t1340095241 * ___U24this_1;
	// System.Object DownloadTexture/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean DownloadTexture/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 DownloadTexture/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2951566387, ___U3CwwwU3E__0_0)); }
	inline WWW_t3599262362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3599262362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3599262362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2951566387, ___U24this_1)); }
	inline DownloadTexture_t1340095241 * get_U24this_1() const { return ___U24this_1; }
	inline DownloadTexture_t1340095241 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DownloadTexture_t1340095241 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2951566387, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2951566387, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2951566387, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2951566387_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef DIRECTION_T1015163810_H
#define DIRECTION_T1015163810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable/Direction
struct  Direction_t1015163810 
{
public:
	// System.Int32 UITable/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1015163810, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1015163810_H
#ifndef DIRECTION_T598398482_H
#define DIRECTION_T598398482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlider/Direction
struct  Direction_t598398482 
{
public:
	// System.Int32 UISlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t598398482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T598398482_H
#ifndef ARRANGEMENT_T680747878_H
#define ARRANGEMENT_T680747878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/Arrangement
struct  Arrangement_t680747878 
{
public:
	// System.Int32 UIGrid/Arrangement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Arrangement_t680747878, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRANGEMENT_T680747878_H
#ifndef POSITION_T1317090401_H
#define POSITION_T1317090401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/Position
struct  Position_t1317090401 
{
public:
	// System.Int32 UIPopupList/Position::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t1317090401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T1317090401_H
#ifndef VALIDATION_T2746088341_H
#define VALIDATION_T2746088341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputValidator/Validation
struct  Validation_t2746088341 
{
public:
	// System.Int32 UIInputValidator/Validation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Validation_t2746088341, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATION_T2746088341_H
#ifndef TRIGGER_T2084149677_H
#define TRIGGER_T2084149677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonMessage/Trigger
struct  Trigger_t2084149677 
{
public:
	// System.Int32 UIButtonMessage/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t2084149677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T2084149677_H
#ifndef DRAGEFFECT_T1559318904_H
#define DRAGEFFECT_T1559318904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragObject/DragEffect
struct  DragEffect_t1559318904 
{
public:
	// System.Int32 UIDragObject/DragEffect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DragEffect_t1559318904, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEFFECT_T1559318904_H
#ifndef TRIGGER_T338680093_H
#define TRIGGER_T338680093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonSound/Trigger
struct  Trigger_t338680093 
{
public:
	// System.Int32 UIButtonSound/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t338680093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T338680093_H
#ifndef SHOWCONDITION_T1844407723_H
#define SHOWCONDITION_T1844407723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggablePanel/ShowCondition
struct  ShowCondition_t1844407723 
{
public:
	// System.Int32 UIDraggablePanel/ShowCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShowCondition_t1844407723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWCONDITION_T1844407723_H
#ifndef DRAGEFFECT_T196040209_H
#define DRAGEFFECT_T196040209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggablePanel/DragEffect
struct  DragEffect_t196040209 
{
public:
	// System.Int32 UIDraggablePanel/DragEffect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DragEffect_t196040209, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEFFECT_T196040209_H
#ifndef DIRECTION_T1844548834_H
#define DIRECTION_T1844548834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollBar/Direction
struct  Direction_t1844548834 
{
public:
	// System.Int32 UIScrollBar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1844548834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1844548834_H
#ifndef MODIFIER_T140207204_H
#define MODIFIER_T140207204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvStat/Modifier
struct  Modifier_t140207204 
{
public:
	// System.Int32 InvStat/Modifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Modifier_t140207204, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIER_T140207204_H
#ifndef TRIGGER_T3684321850_H
#define TRIGGER_T3684321850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Trigger
struct  Trigger_t3684321850 
{
public:
	// System.Int32 AnimationOrTween.Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t3684321850, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T3684321850_H
#ifndef EMULTICHOICE_T2282345180_H
#define EMULTICHOICE_T2282345180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EMultiChoice
struct  EMultiChoice_t2282345180 
{
public:
	// System.Int32 EMultiChoice::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EMultiChoice_t2282345180, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULTICHOICE_T2282345180_H
#ifndef ENABLECONDITION_T266497438_H
#define ENABLECONDITION_T266497438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.EnableCondition
struct  EnableCondition_t266497438 
{
public:
	// System.Int32 AnimationOrTween.EnableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnableCondition_t266497438, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECONDITION_T266497438_H
#ifndef DISABLECONDITION_T4056368895_H
#define DISABLECONDITION_T4056368895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.DisableCondition
struct  DisableCondition_t4056368895 
{
public:
	// System.Int32 AnimationOrTween.DisableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisableCondition_t4056368895, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONDITION_T4056368895_H
#ifndef BOUNDS_T3570137099_H
#define BOUNDS_T3570137099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3570137099 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t1986933152  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t1986933152  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Center_0)); }
	inline Vector3_t1986933152  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t1986933152 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t1986933152  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3570137099, ___m_Extents_1)); }
	inline Vector3_t1986933152  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t1986933152 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t1986933152  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3570137099_H
#ifndef PLANE_T3184513474_H
#define PLANE_T3184513474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t3184513474 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t1986933152  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t3184513474, ___m_Normal_0)); }
	inline Vector3_t1986933152  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t1986933152 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t1986933152  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t3184513474, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T3184513474_H
#ifndef DIRECTION_T411333392_H
#define DIRECTION_T411333392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Direction
struct  Direction_t411333392 
{
public:
	// System.Int32 AnimationOrTween.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t411333392, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T411333392_H
#ifndef IDENTIFIER_T1167502831_H
#define IDENTIFIER_T1167502831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvStat/Identifier
struct  Identifier_t1167502831 
{
public:
	// System.Int32 InvStat/Identifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Identifier_t1167502831, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFIER_T1167502831_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef SLOT_T2525937994_H
#define SLOT_T2525937994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvBaseItem/Slot
struct  Slot_t2525937994 
{
public:
	// System.Int32 InvBaseItem/Slot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Slot_t2525937994, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T2525937994_H
#ifndef QUALITY_T2755226396_H
#define QUALITY_T2755226396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvGameItem/Quality
struct  Quality_t2755226396 
{
public:
	// System.Int32 InvGameItem/Quality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Quality_t2755226396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITY_T2755226396_H
#ifndef INVBASEITEM_T82225019_H
#define INVBASEITEM_T82225019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvBaseItem
struct  InvBaseItem_t82225019  : public RuntimeObject
{
public:
	// System.Int32 InvBaseItem::id16
	int32_t ___id16_0;
	// System.String InvBaseItem::name
	String_t* ___name_1;
	// System.String InvBaseItem::description
	String_t* ___description_2;
	// InvBaseItem/Slot InvBaseItem::slot
	int32_t ___slot_3;
	// System.Int32 InvBaseItem::minItemLevel
	int32_t ___minItemLevel_4;
	// System.Int32 InvBaseItem::maxItemLevel
	int32_t ___maxItemLevel_5;
	// System.Collections.Generic.List`1<InvStat> InvBaseItem::stats
	List_1_t4155999403 * ___stats_6;
	// UnityEngine.GameObject InvBaseItem::attachment
	GameObject_t2557347079 * ___attachment_7;
	// UnityEngine.Color InvBaseItem::color
	Color_t2582018970  ___color_8;
	// UIAtlas InvBaseItem::iconAtlas
	UIAtlas_t1815452364 * ___iconAtlas_9;
	// System.String InvBaseItem::iconName
	String_t* ___iconName_10;

public:
	inline static int32_t get_offset_of_id16_0() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___id16_0)); }
	inline int32_t get_id16_0() const { return ___id16_0; }
	inline int32_t* get_address_of_id16_0() { return &___id16_0; }
	inline void set_id16_0(int32_t value)
	{
		___id16_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_slot_3() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___slot_3)); }
	inline int32_t get_slot_3() const { return ___slot_3; }
	inline int32_t* get_address_of_slot_3() { return &___slot_3; }
	inline void set_slot_3(int32_t value)
	{
		___slot_3 = value;
	}

	inline static int32_t get_offset_of_minItemLevel_4() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___minItemLevel_4)); }
	inline int32_t get_minItemLevel_4() const { return ___minItemLevel_4; }
	inline int32_t* get_address_of_minItemLevel_4() { return &___minItemLevel_4; }
	inline void set_minItemLevel_4(int32_t value)
	{
		___minItemLevel_4 = value;
	}

	inline static int32_t get_offset_of_maxItemLevel_5() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___maxItemLevel_5)); }
	inline int32_t get_maxItemLevel_5() const { return ___maxItemLevel_5; }
	inline int32_t* get_address_of_maxItemLevel_5() { return &___maxItemLevel_5; }
	inline void set_maxItemLevel_5(int32_t value)
	{
		___maxItemLevel_5 = value;
	}

	inline static int32_t get_offset_of_stats_6() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___stats_6)); }
	inline List_1_t4155999403 * get_stats_6() const { return ___stats_6; }
	inline List_1_t4155999403 ** get_address_of_stats_6() { return &___stats_6; }
	inline void set_stats_6(List_1_t4155999403 * value)
	{
		___stats_6 = value;
		Il2CppCodeGenWriteBarrier((&___stats_6), value);
	}

	inline static int32_t get_offset_of_attachment_7() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___attachment_7)); }
	inline GameObject_t2557347079 * get_attachment_7() const { return ___attachment_7; }
	inline GameObject_t2557347079 ** get_address_of_attachment_7() { return &___attachment_7; }
	inline void set_attachment_7(GameObject_t2557347079 * value)
	{
		___attachment_7 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_7), value);
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___color_8)); }
	inline Color_t2582018970  get_color_8() const { return ___color_8; }
	inline Color_t2582018970 * get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(Color_t2582018970  value)
	{
		___color_8 = value;
	}

	inline static int32_t get_offset_of_iconAtlas_9() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___iconAtlas_9)); }
	inline UIAtlas_t1815452364 * get_iconAtlas_9() const { return ___iconAtlas_9; }
	inline UIAtlas_t1815452364 ** get_address_of_iconAtlas_9() { return &___iconAtlas_9; }
	inline void set_iconAtlas_9(UIAtlas_t1815452364 * value)
	{
		___iconAtlas_9 = value;
		Il2CppCodeGenWriteBarrier((&___iconAtlas_9), value);
	}

	inline static int32_t get_offset_of_iconName_10() { return static_cast<int32_t>(offsetof(InvBaseItem_t82225019, ___iconName_10)); }
	inline String_t* get_iconName_10() const { return ___iconName_10; }
	inline String_t** get_address_of_iconName_10() { return &___iconName_10; }
	inline void set_iconName_10(String_t* value)
	{
		___iconName_10 = value;
		Il2CppCodeGenWriteBarrier((&___iconName_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVBASEITEM_T82225019_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef INVGAMEITEM_T1620673912_H
#define INVGAMEITEM_T1620673912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvGameItem
struct  InvGameItem_t1620673912  : public RuntimeObject
{
public:
	// System.Int32 InvGameItem::mBaseItemID
	int32_t ___mBaseItemID_0;
	// InvGameItem/Quality InvGameItem::quality
	int32_t ___quality_1;
	// System.Int32 InvGameItem::itemLevel
	int32_t ___itemLevel_2;
	// InvBaseItem InvGameItem::mBaseItem
	InvBaseItem_t82225019 * ___mBaseItem_3;

public:
	inline static int32_t get_offset_of_mBaseItemID_0() { return static_cast<int32_t>(offsetof(InvGameItem_t1620673912, ___mBaseItemID_0)); }
	inline int32_t get_mBaseItemID_0() const { return ___mBaseItemID_0; }
	inline int32_t* get_address_of_mBaseItemID_0() { return &___mBaseItemID_0; }
	inline void set_mBaseItemID_0(int32_t value)
	{
		___mBaseItemID_0 = value;
	}

	inline static int32_t get_offset_of_quality_1() { return static_cast<int32_t>(offsetof(InvGameItem_t1620673912, ___quality_1)); }
	inline int32_t get_quality_1() const { return ___quality_1; }
	inline int32_t* get_address_of_quality_1() { return &___quality_1; }
	inline void set_quality_1(int32_t value)
	{
		___quality_1 = value;
	}

	inline static int32_t get_offset_of_itemLevel_2() { return static_cast<int32_t>(offsetof(InvGameItem_t1620673912, ___itemLevel_2)); }
	inline int32_t get_itemLevel_2() const { return ___itemLevel_2; }
	inline int32_t* get_address_of_itemLevel_2() { return &___itemLevel_2; }
	inline void set_itemLevel_2(int32_t value)
	{
		___itemLevel_2 = value;
	}

	inline static int32_t get_offset_of_mBaseItem_3() { return static_cast<int32_t>(offsetof(InvGameItem_t1620673912, ___mBaseItem_3)); }
	inline InvBaseItem_t82225019 * get_mBaseItem_3() const { return ___mBaseItem_3; }
	inline InvBaseItem_t82225019 ** get_address_of_mBaseItem_3() { return &___mBaseItem_3; }
	inline void set_mBaseItem_3(InvBaseItem_t82225019 * value)
	{
		___mBaseItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___mBaseItem_3), value);
	}
};

struct InvGameItem_t1620673912_StaticFields
{
public:
	// System.Comparison`1<InvStat> InvGameItem::<>f__mg$cache0
	Comparison_1_t2125462953 * ___U3CU3Ef__mgU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(InvGameItem_t1620673912_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Comparison_1_t2125462953 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Comparison_1_t2125462953 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Comparison_1_t2125462953 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVGAMEITEM_T1620673912_H
#ifndef INVSTAT_T741405100_H
#define INVSTAT_T741405100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvStat
struct  InvStat_t741405100  : public RuntimeObject
{
public:
	// InvStat/Identifier InvStat::id
	int32_t ___id_0;
	// InvStat/Modifier InvStat::modifier
	int32_t ___modifier_1;
	// System.Int32 InvStat::amount
	int32_t ___amount_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(InvStat_t741405100, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_modifier_1() { return static_cast<int32_t>(offsetof(InvStat_t741405100, ___modifier_1)); }
	inline int32_t get_modifier_1() const { return ___modifier_1; }
	inline int32_t* get_address_of_modifier_1() { return &___modifier_1; }
	inline void set_modifier_1(int32_t value)
	{
		___modifier_1 = value;
	}

	inline static int32_t get_offset_of_amount_2() { return static_cast<int32_t>(offsetof(InvStat_t741405100, ___amount_2)); }
	inline int32_t get_amount_2() const { return ___amount_2; }
	inline int32_t* get_address_of_amount_2() { return &___amount_2; }
	inline void set_amount_2(int32_t value)
	{
		___amount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVSTAT_T741405100_H
#ifndef ONDRAGFINISHED_T81282452_H
#define ONDRAGFINISHED_T81282452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggablePanel/OnDragFinished
struct  OnDragFinished_t81282452  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGFINISHED_T81282452_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef ONSELECTIONCHANGE_T705636838_H
#define ONSELECTIONCHANGE_T705636838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/OnSelectionChange
struct  OnSelectionChange_t705636838  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSELECTIONCHANGE_T705636838_H
#ifndef ONSCROLLBARCHANGE_T2155012818_H
#define ONSCROLLBARCHANGE_T2155012818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollBar/OnScrollBarChange
struct  OnScrollBarChange_t2155012818  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCROLLBARCHANGE_T2155012818_H
#ifndef ONSTATECHANGE_T2569002187_H
#define ONSTATECHANGE_T2569002187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICheckbox/OnStateChange
struct  OnStateChange_t2569002187  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGE_T2569002187_H
#ifndef ONFINISHED_T1828027647_H
#define ONFINISHED_T1828027647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveAnimation/OnFinished
struct  OnFinished_t1828027647  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T1828027647_H
#ifndef ONREPOSITION_T2742626599_H
#define ONREPOSITION_T2742626599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable/OnReposition
struct  OnReposition_t2742626599  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREPOSITION_T2742626599_H
#ifndef ONVALUECHANGE_T3441845912_H
#define ONVALUECHANGE_T3441845912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlider/OnValueChange
struct  OnValueChange_t3441845912  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALUECHANGE_T3441845912_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef UISCROLLBAR_T3221813871_H
#define UISCROLLBAR_T3221813871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollBar
struct  UIScrollBar_t3221813871  : public MonoBehaviour_t1618594486
{
public:
	// UISprite UIScrollBar::mBG
	UISprite_t279728715 * ___mBG_2;
	// UISprite UIScrollBar::mFG
	UISprite_t279728715 * ___mFG_3;
	// UIScrollBar/Direction UIScrollBar::mDir
	int32_t ___mDir_4;
	// System.Boolean UIScrollBar::mInverted
	bool ___mInverted_5;
	// System.Single UIScrollBar::mScroll
	float ___mScroll_6;
	// System.Single UIScrollBar::mSize
	float ___mSize_7;
	// UnityEngine.Transform UIScrollBar::mTrans
	Transform_t362059596 * ___mTrans_8;
	// System.Boolean UIScrollBar::mIsDirty
	bool ___mIsDirty_9;
	// UnityEngine.Camera UIScrollBar::mCam
	Camera_t2839736942 * ___mCam_10;
	// UnityEngine.Vector2 UIScrollBar::mScreenPos
	Vector2_t328513675  ___mScreenPos_11;
	// UIScrollBar/OnScrollBarChange UIScrollBar::onChange
	OnScrollBarChange_t2155012818 * ___onChange_12;

public:
	inline static int32_t get_offset_of_mBG_2() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mBG_2)); }
	inline UISprite_t279728715 * get_mBG_2() const { return ___mBG_2; }
	inline UISprite_t279728715 ** get_address_of_mBG_2() { return &___mBG_2; }
	inline void set_mBG_2(UISprite_t279728715 * value)
	{
		___mBG_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBG_2), value);
	}

	inline static int32_t get_offset_of_mFG_3() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mFG_3)); }
	inline UISprite_t279728715 * get_mFG_3() const { return ___mFG_3; }
	inline UISprite_t279728715 ** get_address_of_mFG_3() { return &___mFG_3; }
	inline void set_mFG_3(UISprite_t279728715 * value)
	{
		___mFG_3 = value;
		Il2CppCodeGenWriteBarrier((&___mFG_3), value);
	}

	inline static int32_t get_offset_of_mDir_4() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mDir_4)); }
	inline int32_t get_mDir_4() const { return ___mDir_4; }
	inline int32_t* get_address_of_mDir_4() { return &___mDir_4; }
	inline void set_mDir_4(int32_t value)
	{
		___mDir_4 = value;
	}

	inline static int32_t get_offset_of_mInverted_5() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mInverted_5)); }
	inline bool get_mInverted_5() const { return ___mInverted_5; }
	inline bool* get_address_of_mInverted_5() { return &___mInverted_5; }
	inline void set_mInverted_5(bool value)
	{
		___mInverted_5 = value;
	}

	inline static int32_t get_offset_of_mScroll_6() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mScroll_6)); }
	inline float get_mScroll_6() const { return ___mScroll_6; }
	inline float* get_address_of_mScroll_6() { return &___mScroll_6; }
	inline void set_mScroll_6(float value)
	{
		___mScroll_6 = value;
	}

	inline static int32_t get_offset_of_mSize_7() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mSize_7)); }
	inline float get_mSize_7() const { return ___mSize_7; }
	inline float* get_address_of_mSize_7() { return &___mSize_7; }
	inline void set_mSize_7(float value)
	{
		___mSize_7 = value;
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mTrans_8)); }
	inline Transform_t362059596 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t362059596 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t362059596 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mIsDirty_9() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mIsDirty_9)); }
	inline bool get_mIsDirty_9() const { return ___mIsDirty_9; }
	inline bool* get_address_of_mIsDirty_9() { return &___mIsDirty_9; }
	inline void set_mIsDirty_9(bool value)
	{
		___mIsDirty_9 = value;
	}

	inline static int32_t get_offset_of_mCam_10() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mCam_10)); }
	inline Camera_t2839736942 * get_mCam_10() const { return ___mCam_10; }
	inline Camera_t2839736942 ** get_address_of_mCam_10() { return &___mCam_10; }
	inline void set_mCam_10(Camera_t2839736942 * value)
	{
		___mCam_10 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_10), value);
	}

	inline static int32_t get_offset_of_mScreenPos_11() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___mScreenPos_11)); }
	inline Vector2_t328513675  get_mScreenPos_11() const { return ___mScreenPos_11; }
	inline Vector2_t328513675 * get_address_of_mScreenPos_11() { return &___mScreenPos_11; }
	inline void set_mScreenPos_11(Vector2_t328513675  value)
	{
		___mScreenPos_11 = value;
	}

	inline static int32_t get_offset_of_onChange_12() { return static_cast<int32_t>(offsetof(UIScrollBar_t3221813871, ___onChange_12)); }
	inline OnScrollBarChange_t2155012818 * get_onChange_12() const { return ___onChange_12; }
	inline OnScrollBarChange_t2155012818 ** get_address_of_onChange_12() { return &___onChange_12; }
	inline void set_onChange_12(OnScrollBarChange_t2155012818 * value)
	{
		___onChange_12 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBAR_T3221813871_H
#ifndef UITABLE_T2217632677_H
#define UITABLE_T2217632677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable
struct  UITable_t2217632677  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 UITable::columns
	int32_t ___columns_2;
	// UITable/Direction UITable::direction
	int32_t ___direction_3;
	// UnityEngine.Vector2 UITable::padding
	Vector2_t328513675  ___padding_4;
	// System.Boolean UITable::sorted
	bool ___sorted_5;
	// System.Boolean UITable::hideInactive
	bool ___hideInactive_6;
	// System.Boolean UITable::repositionNow
	bool ___repositionNow_7;
	// System.Boolean UITable::keepWithinPanel
	bool ___keepWithinPanel_8;
	// UITable/OnReposition UITable::onReposition
	OnReposition_t2742626599 * ___onReposition_9;
	// UIPanel UITable::mPanel
	UIPanel_t825970685 * ___mPanel_10;
	// UIDraggablePanel UITable::mDrag
	UIDraggablePanel_t2580300900 * ___mDrag_11;
	// System.Boolean UITable::mStarted
	bool ___mStarted_12;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UITable::mChildren
	List_1_t3776653899 * ___mChildren_13;

public:
	inline static int32_t get_offset_of_columns_2() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___columns_2)); }
	inline int32_t get_columns_2() const { return ___columns_2; }
	inline int32_t* get_address_of_columns_2() { return &___columns_2; }
	inline void set_columns_2(int32_t value)
	{
		___columns_2 = value;
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___padding_4)); }
	inline Vector2_t328513675  get_padding_4() const { return ___padding_4; }
	inline Vector2_t328513675 * get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(Vector2_t328513675  value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_sorted_5() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___sorted_5)); }
	inline bool get_sorted_5() const { return ___sorted_5; }
	inline bool* get_address_of_sorted_5() { return &___sorted_5; }
	inline void set_sorted_5(bool value)
	{
		___sorted_5 = value;
	}

	inline static int32_t get_offset_of_hideInactive_6() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___hideInactive_6)); }
	inline bool get_hideInactive_6() const { return ___hideInactive_6; }
	inline bool* get_address_of_hideInactive_6() { return &___hideInactive_6; }
	inline void set_hideInactive_6(bool value)
	{
		___hideInactive_6 = value;
	}

	inline static int32_t get_offset_of_repositionNow_7() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___repositionNow_7)); }
	inline bool get_repositionNow_7() const { return ___repositionNow_7; }
	inline bool* get_address_of_repositionNow_7() { return &___repositionNow_7; }
	inline void set_repositionNow_7(bool value)
	{
		___repositionNow_7 = value;
	}

	inline static int32_t get_offset_of_keepWithinPanel_8() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___keepWithinPanel_8)); }
	inline bool get_keepWithinPanel_8() const { return ___keepWithinPanel_8; }
	inline bool* get_address_of_keepWithinPanel_8() { return &___keepWithinPanel_8; }
	inline void set_keepWithinPanel_8(bool value)
	{
		___keepWithinPanel_8 = value;
	}

	inline static int32_t get_offset_of_onReposition_9() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___onReposition_9)); }
	inline OnReposition_t2742626599 * get_onReposition_9() const { return ___onReposition_9; }
	inline OnReposition_t2742626599 ** get_address_of_onReposition_9() { return &___onReposition_9; }
	inline void set_onReposition_9(OnReposition_t2742626599 * value)
	{
		___onReposition_9 = value;
		Il2CppCodeGenWriteBarrier((&___onReposition_9), value);
	}

	inline static int32_t get_offset_of_mPanel_10() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___mPanel_10)); }
	inline UIPanel_t825970685 * get_mPanel_10() const { return ___mPanel_10; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_10() { return &___mPanel_10; }
	inline void set_mPanel_10(UIPanel_t825970685 * value)
	{
		___mPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_10), value);
	}

	inline static int32_t get_offset_of_mDrag_11() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___mDrag_11)); }
	inline UIDraggablePanel_t2580300900 * get_mDrag_11() const { return ___mDrag_11; }
	inline UIDraggablePanel_t2580300900 ** get_address_of_mDrag_11() { return &___mDrag_11; }
	inline void set_mDrag_11(UIDraggablePanel_t2580300900 * value)
	{
		___mDrag_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDrag_11), value);
	}

	inline static int32_t get_offset_of_mStarted_12() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___mStarted_12)); }
	inline bool get_mStarted_12() const { return ___mStarted_12; }
	inline bool* get_address_of_mStarted_12() { return &___mStarted_12; }
	inline void set_mStarted_12(bool value)
	{
		___mStarted_12 = value;
	}

	inline static int32_t get_offset_of_mChildren_13() { return static_cast<int32_t>(offsetof(UITable_t2217632677, ___mChildren_13)); }
	inline List_1_t3776653899 * get_mChildren_13() const { return ___mChildren_13; }
	inline List_1_t3776653899 ** get_address_of_mChildren_13() { return &___mChildren_13; }
	inline void set_mChildren_13(List_1_t3776653899 * value)
	{
		___mChildren_13 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_13), value);
	}
};

struct UITable_t2217632677_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UITable::<>f__mg$cache0
	Comparison_1_t1746117449 * ___U3CU3Ef__mgU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(UITable_t2217632677_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline Comparison_1_t1746117449 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline Comparison_1_t1746117449 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(Comparison_1_t1746117449 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITABLE_T2217632677_H
#ifndef UISOUNDVOLUME_T390417661_H
#define UISOUNDVOLUME_T390417661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISoundVolume
struct  UISoundVolume_t390417661  : public MonoBehaviour_t1618594486
{
public:
	// UISlider UISoundVolume::mSlider
	UISlider_t1813774206 * ___mSlider_2;

public:
	inline static int32_t get_offset_of_mSlider_2() { return static_cast<int32_t>(offsetof(UISoundVolume_t390417661, ___mSlider_2)); }
	inline UISlider_t1813774206 * get_mSlider_2() const { return ___mSlider_2; }
	inline UISlider_t1813774206 ** get_address_of_mSlider_2() { return &___mSlider_2; }
	inline void set_mSlider_2(UISlider_t1813774206 * value)
	{
		___mSlider_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSlider_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISOUNDVOLUME_T390417661_H
#ifndef UIDRAGPANELCONTENTS_T194675207_H
#define UIDRAGPANELCONTENTS_T194675207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragPanelContents
struct  UIDragPanelContents_t194675207  : public MonoBehaviour_t1618594486
{
public:
	// UIDraggablePanel UIDragPanelContents::draggablePanel
	UIDraggablePanel_t2580300900 * ___draggablePanel_2;
	// UIPanel UIDragPanelContents::panel
	UIPanel_t825970685 * ___panel_3;

public:
	inline static int32_t get_offset_of_draggablePanel_2() { return static_cast<int32_t>(offsetof(UIDragPanelContents_t194675207, ___draggablePanel_2)); }
	inline UIDraggablePanel_t2580300900 * get_draggablePanel_2() const { return ___draggablePanel_2; }
	inline UIDraggablePanel_t2580300900 ** get_address_of_draggablePanel_2() { return &___draggablePanel_2; }
	inline void set_draggablePanel_2(UIDraggablePanel_t2580300900 * value)
	{
		___draggablePanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___draggablePanel_2), value);
	}

	inline static int32_t get_offset_of_panel_3() { return static_cast<int32_t>(offsetof(UIDragPanelContents_t194675207, ___panel_3)); }
	inline UIPanel_t825970685 * get_panel_3() const { return ___panel_3; }
	inline UIPanel_t825970685 ** get_address_of_panel_3() { return &___panel_3; }
	inline void set_panel_3(UIPanel_t825970685 * value)
	{
		___panel_3 = value;
		Il2CppCodeGenWriteBarrier((&___panel_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGPANELCONTENTS_T194675207_H
#ifndef UIGRID_T3202738573_H
#define UIGRID_T3202738573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid
struct  UIGrid_t3202738573  : public MonoBehaviour_t1618594486
{
public:
	// UIGrid/Arrangement UIGrid::arrangement
	int32_t ___arrangement_2;
	// System.Int32 UIGrid::maxPerLine
	int32_t ___maxPerLine_3;
	// System.Single UIGrid::cellWidth
	float ___cellWidth_4;
	// System.Single UIGrid::cellHeight
	float ___cellHeight_5;
	// System.Boolean UIGrid::repositionNow
	bool ___repositionNow_6;
	// System.Boolean UIGrid::sorted
	bool ___sorted_7;
	// System.Boolean UIGrid::hideInactive
	bool ___hideInactive_8;
	// System.Boolean UIGrid::mStarted
	bool ___mStarted_9;

public:
	inline static int32_t get_offset_of_arrangement_2() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___arrangement_2)); }
	inline int32_t get_arrangement_2() const { return ___arrangement_2; }
	inline int32_t* get_address_of_arrangement_2() { return &___arrangement_2; }
	inline void set_arrangement_2(int32_t value)
	{
		___arrangement_2 = value;
	}

	inline static int32_t get_offset_of_maxPerLine_3() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___maxPerLine_3)); }
	inline int32_t get_maxPerLine_3() const { return ___maxPerLine_3; }
	inline int32_t* get_address_of_maxPerLine_3() { return &___maxPerLine_3; }
	inline void set_maxPerLine_3(int32_t value)
	{
		___maxPerLine_3 = value;
	}

	inline static int32_t get_offset_of_cellWidth_4() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___cellWidth_4)); }
	inline float get_cellWidth_4() const { return ___cellWidth_4; }
	inline float* get_address_of_cellWidth_4() { return &___cellWidth_4; }
	inline void set_cellWidth_4(float value)
	{
		___cellWidth_4 = value;
	}

	inline static int32_t get_offset_of_cellHeight_5() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___cellHeight_5)); }
	inline float get_cellHeight_5() const { return ___cellHeight_5; }
	inline float* get_address_of_cellHeight_5() { return &___cellHeight_5; }
	inline void set_cellHeight_5(float value)
	{
		___cellHeight_5 = value;
	}

	inline static int32_t get_offset_of_repositionNow_6() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___repositionNow_6)); }
	inline bool get_repositionNow_6() const { return ___repositionNow_6; }
	inline bool* get_address_of_repositionNow_6() { return &___repositionNow_6; }
	inline void set_repositionNow_6(bool value)
	{
		___repositionNow_6 = value;
	}

	inline static int32_t get_offset_of_sorted_7() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___sorted_7)); }
	inline bool get_sorted_7() const { return ___sorted_7; }
	inline bool* get_address_of_sorted_7() { return &___sorted_7; }
	inline void set_sorted_7(bool value)
	{
		___sorted_7 = value;
	}

	inline static int32_t get_offset_of_hideInactive_8() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___hideInactive_8)); }
	inline bool get_hideInactive_8() const { return ___hideInactive_8; }
	inline bool* get_address_of_hideInactive_8() { return &___hideInactive_8; }
	inline void set_hideInactive_8(bool value)
	{
		___hideInactive_8 = value;
	}

	inline static int32_t get_offset_of_mStarted_9() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573, ___mStarted_9)); }
	inline bool get_mStarted_9() const { return ___mStarted_9; }
	inline bool* get_address_of_mStarted_9() { return &___mStarted_9; }
	inline void set_mStarted_9(bool value)
	{
		___mStarted_9 = value;
	}
};

struct UIGrid_t3202738573_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache0
	Comparison_1_t1746117449 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(UIGrid_t3202738573_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline Comparison_1_t1746117449 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline Comparison_1_t1746117449 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(Comparison_1_t1746117449 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRID_T3202738573_H
#ifndef UISAVEDOPTION_T870911898_H
#define UISAVEDOPTION_T870911898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISavedOption
struct  UISavedOption_t870911898  : public MonoBehaviour_t1618594486
{
public:
	// System.String UISavedOption::keyName
	String_t* ___keyName_2;
	// UIPopupList UISavedOption::mList
	UIPopupList_t2907709271 * ___mList_3;
	// UICheckbox UISavedOption::mCheck
	UICheckbox_t1205955448 * ___mCheck_4;

public:
	inline static int32_t get_offset_of_keyName_2() { return static_cast<int32_t>(offsetof(UISavedOption_t870911898, ___keyName_2)); }
	inline String_t* get_keyName_2() const { return ___keyName_2; }
	inline String_t** get_address_of_keyName_2() { return &___keyName_2; }
	inline void set_keyName_2(String_t* value)
	{
		___keyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyName_2), value);
	}

	inline static int32_t get_offset_of_mList_3() { return static_cast<int32_t>(offsetof(UISavedOption_t870911898, ___mList_3)); }
	inline UIPopupList_t2907709271 * get_mList_3() const { return ___mList_3; }
	inline UIPopupList_t2907709271 ** get_address_of_mList_3() { return &___mList_3; }
	inline void set_mList_3(UIPopupList_t2907709271 * value)
	{
		___mList_3 = value;
		Il2CppCodeGenWriteBarrier((&___mList_3), value);
	}

	inline static int32_t get_offset_of_mCheck_4() { return static_cast<int32_t>(offsetof(UISavedOption_t870911898, ___mCheck_4)); }
	inline UICheckbox_t1205955448 * get_mCheck_4() const { return ___mCheck_4; }
	inline UICheckbox_t1205955448 ** get_address_of_mCheck_4() { return &___mCheck_4; }
	inline void set_mCheck_4(UICheckbox_t1205955448 * value)
	{
		___mCheck_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCheck_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISAVEDOPTION_T870911898_H
#ifndef UISLIDERCOLORS_T1281262261_H
#define UISLIDERCOLORS_T1281262261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISliderColors
struct  UISliderColors_t1281262261  : public MonoBehaviour_t1618594486
{
public:
	// UISprite UISliderColors::sprite
	UISprite_t279728715 * ___sprite_2;
	// UnityEngine.Color[] UISliderColors::colors
	ColorU5BU5D_t247935167* ___colors_3;
	// UISlider UISliderColors::mSlider
	UISlider_t1813774206 * ___mSlider_4;

public:
	inline static int32_t get_offset_of_sprite_2() { return static_cast<int32_t>(offsetof(UISliderColors_t1281262261, ___sprite_2)); }
	inline UISprite_t279728715 * get_sprite_2() const { return ___sprite_2; }
	inline UISprite_t279728715 ** get_address_of_sprite_2() { return &___sprite_2; }
	inline void set_sprite_2(UISprite_t279728715 * value)
	{
		___sprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_2), value);
	}

	inline static int32_t get_offset_of_colors_3() { return static_cast<int32_t>(offsetof(UISliderColors_t1281262261, ___colors_3)); }
	inline ColorU5BU5D_t247935167* get_colors_3() const { return ___colors_3; }
	inline ColorU5BU5D_t247935167** get_address_of_colors_3() { return &___colors_3; }
	inline void set_colors_3(ColorU5BU5D_t247935167* value)
	{
		___colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___colors_3), value);
	}

	inline static int32_t get_offset_of_mSlider_4() { return static_cast<int32_t>(offsetof(UISliderColors_t1281262261, ___mSlider_4)); }
	inline UISlider_t1813774206 * get_mSlider_4() const { return ___mSlider_4; }
	inline UISlider_t1813774206 ** get_address_of_mSlider_4() { return &___mSlider_4; }
	inline void set_mSlider_4(UISlider_t1813774206 * value)
	{
		___mSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSlider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERCOLORS_T1281262261_H
#ifndef UIIMAGEBUTTON_T1024317081_H
#define UIIMAGEBUTTON_T1024317081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIImageButton
struct  UIImageButton_t1024317081  : public MonoBehaviour_t1618594486
{
public:
	// UISprite UIImageButton::target
	UISprite_t279728715 * ___target_2;
	// System.String UIImageButton::normalSprite
	String_t* ___normalSprite_3;
	// System.String UIImageButton::hoverSprite
	String_t* ___hoverSprite_4;
	// System.String UIImageButton::pressedSprite
	String_t* ___pressedSprite_5;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIImageButton_t1024317081, ___target_2)); }
	inline UISprite_t279728715 * get_target_2() const { return ___target_2; }
	inline UISprite_t279728715 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(UISprite_t279728715 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_normalSprite_3() { return static_cast<int32_t>(offsetof(UIImageButton_t1024317081, ___normalSprite_3)); }
	inline String_t* get_normalSprite_3() const { return ___normalSprite_3; }
	inline String_t** get_address_of_normalSprite_3() { return &___normalSprite_3; }
	inline void set_normalSprite_3(String_t* value)
	{
		___normalSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_3), value);
	}

	inline static int32_t get_offset_of_hoverSprite_4() { return static_cast<int32_t>(offsetof(UIImageButton_t1024317081, ___hoverSprite_4)); }
	inline String_t* get_hoverSprite_4() const { return ___hoverSprite_4; }
	inline String_t** get_address_of_hoverSprite_4() { return &___hoverSprite_4; }
	inline void set_hoverSprite_4(String_t* value)
	{
		___hoverSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___hoverSprite_4), value);
	}

	inline static int32_t get_offset_of_pressedSprite_5() { return static_cast<int32_t>(offsetof(UIImageButton_t1024317081, ___pressedSprite_5)); }
	inline String_t* get_pressedSprite_5() const { return ___pressedSprite_5; }
	inline String_t** get_address_of_pressedSprite_5() { return &___pressedSprite_5; }
	inline void set_pressedSprite_5(String_t* value)
	{
		___pressedSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGEBUTTON_T1024317081_H
#ifndef UIINPUTVALIDATOR_T2914548063_H
#define UIINPUTVALIDATOR_T2914548063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputValidator
struct  UIInputValidator_t2914548063  : public MonoBehaviour_t1618594486
{
public:
	// UIInputValidator/Validation UIInputValidator::logic
	int32_t ___logic_2;

public:
	inline static int32_t get_offset_of_logic_2() { return static_cast<int32_t>(offsetof(UIInputValidator_t2914548063, ___logic_2)); }
	inline int32_t get_logic_2() const { return ___logic_2; }
	inline int32_t* get_address_of_logic_2() { return &___logic_2; }
	inline void set_logic_2(int32_t value)
	{
		___logic_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTVALIDATOR_T2914548063_H
#ifndef IGNORETIMESCALE_T640346767_H
#define IGNORETIMESCALE_T640346767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IgnoreTimeScale
struct  IgnoreTimeScale_t640346767  : public MonoBehaviour_t1618594486
{
public:
	// System.Single IgnoreTimeScale::mTimeStart
	float ___mTimeStart_2;
	// System.Single IgnoreTimeScale::mTimeDelta
	float ___mTimeDelta_3;
	// System.Single IgnoreTimeScale::mActual
	float ___mActual_4;
	// System.Boolean IgnoreTimeScale::mTimeStarted
	bool ___mTimeStarted_5;

public:
	inline static int32_t get_offset_of_mTimeStart_2() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeStart_2)); }
	inline float get_mTimeStart_2() const { return ___mTimeStart_2; }
	inline float* get_address_of_mTimeStart_2() { return &___mTimeStart_2; }
	inline void set_mTimeStart_2(float value)
	{
		___mTimeStart_2 = value;
	}

	inline static int32_t get_offset_of_mTimeDelta_3() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeDelta_3)); }
	inline float get_mTimeDelta_3() const { return ___mTimeDelta_3; }
	inline float* get_address_of_mTimeDelta_3() { return &___mTimeDelta_3; }
	inline void set_mTimeDelta_3(float value)
	{
		___mTimeDelta_3 = value;
	}

	inline static int32_t get_offset_of_mActual_4() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mActual_4)); }
	inline float get_mActual_4() const { return ___mActual_4; }
	inline float* get_address_of_mActual_4() { return &___mActual_4; }
	inline void set_mActual_4(float value)
	{
		___mActual_4 = value;
	}

	inline static int32_t get_offset_of_mTimeStarted_5() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t640346767, ___mTimeStarted_5)); }
	inline bool get_mTimeStarted_5() const { return ___mTimeStarted_5; }
	inline bool* get_address_of_mTimeStarted_5() { return &___mTimeStarted_5; }
	inline void set_mTimeStarted_5(bool value)
	{
		___mTimeStarted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORETIMESCALE_T640346767_H
#ifndef UIPANELALPHA_T3764451740_H
#define UIPANELALPHA_T3764451740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanelAlpha
struct  UIPanelAlpha_t3764451740  : public MonoBehaviour_t1618594486
{
public:
	// System.Single UIPanelAlpha::alpha
	float ___alpha_2;
	// UnityEngine.Collider[] UIPanelAlpha::mColliders
	ColliderU5BU5D_t1976598158* ___mColliders_3;
	// UIWidget[] UIPanelAlpha::mWidgets
	UIWidgetU5BU5D_t2376853568* ___mWidgets_4;
	// System.Single[] UIPanelAlpha::mAlpha
	SingleU5BU5D_t2843050510* ___mAlpha_5;
	// System.Single UIPanelAlpha::mLastAlpha
	float ___mLastAlpha_6;
	// System.Int32 UIPanelAlpha::mLevel
	int32_t ___mLevel_7;

public:
	inline static int32_t get_offset_of_alpha_2() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___alpha_2)); }
	inline float get_alpha_2() const { return ___alpha_2; }
	inline float* get_address_of_alpha_2() { return &___alpha_2; }
	inline void set_alpha_2(float value)
	{
		___alpha_2 = value;
	}

	inline static int32_t get_offset_of_mColliders_3() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___mColliders_3)); }
	inline ColliderU5BU5D_t1976598158* get_mColliders_3() const { return ___mColliders_3; }
	inline ColliderU5BU5D_t1976598158** get_address_of_mColliders_3() { return &___mColliders_3; }
	inline void set_mColliders_3(ColliderU5BU5D_t1976598158* value)
	{
		___mColliders_3 = value;
		Il2CppCodeGenWriteBarrier((&___mColliders_3), value);
	}

	inline static int32_t get_offset_of_mWidgets_4() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___mWidgets_4)); }
	inline UIWidgetU5BU5D_t2376853568* get_mWidgets_4() const { return ___mWidgets_4; }
	inline UIWidgetU5BU5D_t2376853568** get_address_of_mWidgets_4() { return &___mWidgets_4; }
	inline void set_mWidgets_4(UIWidgetU5BU5D_t2376853568* value)
	{
		___mWidgets_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_4), value);
	}

	inline static int32_t get_offset_of_mAlpha_5() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___mAlpha_5)); }
	inline SingleU5BU5D_t2843050510* get_mAlpha_5() const { return ___mAlpha_5; }
	inline SingleU5BU5D_t2843050510** get_address_of_mAlpha_5() { return &___mAlpha_5; }
	inline void set_mAlpha_5(SingleU5BU5D_t2843050510* value)
	{
		___mAlpha_5 = value;
		Il2CppCodeGenWriteBarrier((&___mAlpha_5), value);
	}

	inline static int32_t get_offset_of_mLastAlpha_6() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___mLastAlpha_6)); }
	inline float get_mLastAlpha_6() const { return ___mLastAlpha_6; }
	inline float* get_address_of_mLastAlpha_6() { return &___mLastAlpha_6; }
	inline void set_mLastAlpha_6(float value)
	{
		___mLastAlpha_6 = value;
	}

	inline static int32_t get_offset_of_mLevel_7() { return static_cast<int32_t>(offsetof(UIPanelAlpha_t3764451740, ___mLevel_7)); }
	inline int32_t get_mLevel_7() const { return ___mLevel_7; }
	inline int32_t* get_address_of_mLevel_7() { return &___mLevel_7; }
	inline void set_mLevel_7(int32_t value)
	{
		___mLevel_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPANELALPHA_T3764451740_H
#ifndef UIPOPUPLIST_T2907709271_H
#define UIPOPUPLIST_T2907709271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList
struct  UIPopupList_t2907709271  : public MonoBehaviour_t1618594486
{
public:
	// UIAtlas UIPopupList::atlas
	UIAtlas_t1815452364 * ___atlas_4;
	// UIFont UIPopupList::font
	UIFont_t2730669065 * ___font_5;
	// UILabel UIPopupList::textLabel
	UILabel_t3478074517 * ___textLabel_6;
	// System.String UIPopupList::backgroundSprite
	String_t* ___backgroundSprite_7;
	// System.String UIPopupList::highlightSprite
	String_t* ___highlightSprite_8;
	// UIPopupList/Position UIPopupList::position
	int32_t ___position_9;
	// System.Collections.Generic.List`1<System.String> UIPopupList::items
	List_1_t4069179741 * ___items_10;
	// UnityEngine.Vector2 UIPopupList::padding
	Vector2_t328513675  ___padding_11;
	// System.Single UIPopupList::textScale
	float ___textScale_12;
	// UnityEngine.Color UIPopupList::textColor
	Color_t2582018970  ___textColor_13;
	// UnityEngine.Color UIPopupList::backgroundColor
	Color_t2582018970  ___backgroundColor_14;
	// UnityEngine.Color UIPopupList::highlightColor
	Color_t2582018970  ___highlightColor_15;
	// System.Boolean UIPopupList::isAnimated
	bool ___isAnimated_16;
	// System.Boolean UIPopupList::isLocalized
	bool ___isLocalized_17;
	// UnityEngine.GameObject UIPopupList::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_18;
	// System.String UIPopupList::functionName
	String_t* ___functionName_19;
	// UIPopupList/OnSelectionChange UIPopupList::onSelectionChange
	OnSelectionChange_t705636838 * ___onSelectionChange_20;
	// System.String UIPopupList::mSelectedItem
	String_t* ___mSelectedItem_21;
	// UIPanel UIPopupList::mPanel
	UIPanel_t825970685 * ___mPanel_22;
	// UnityEngine.GameObject UIPopupList::mChild
	GameObject_t2557347079 * ___mChild_23;
	// UISprite UIPopupList::mBackground
	UISprite_t279728715 * ___mBackground_24;
	// UISprite UIPopupList::mHighlight
	UISprite_t279728715 * ___mHighlight_25;
	// UILabel UIPopupList::mHighlightedLabel
	UILabel_t3478074517 * ___mHighlightedLabel_26;
	// System.Collections.Generic.List`1<UILabel> UIPopupList::mLabelList
	List_1_t2597701524 * ___mLabelList_27;
	// System.Single UIPopupList::mBgBorder
	float ___mBgBorder_28;

public:
	inline static int32_t get_offset_of_atlas_4() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___atlas_4)); }
	inline UIAtlas_t1815452364 * get_atlas_4() const { return ___atlas_4; }
	inline UIAtlas_t1815452364 ** get_address_of_atlas_4() { return &___atlas_4; }
	inline void set_atlas_4(UIAtlas_t1815452364 * value)
	{
		___atlas_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_4), value);
	}

	inline static int32_t get_offset_of_font_5() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___font_5)); }
	inline UIFont_t2730669065 * get_font_5() const { return ___font_5; }
	inline UIFont_t2730669065 ** get_address_of_font_5() { return &___font_5; }
	inline void set_font_5(UIFont_t2730669065 * value)
	{
		___font_5 = value;
		Il2CppCodeGenWriteBarrier((&___font_5), value);
	}

	inline static int32_t get_offset_of_textLabel_6() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___textLabel_6)); }
	inline UILabel_t3478074517 * get_textLabel_6() const { return ___textLabel_6; }
	inline UILabel_t3478074517 ** get_address_of_textLabel_6() { return &___textLabel_6; }
	inline void set_textLabel_6(UILabel_t3478074517 * value)
	{
		___textLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___textLabel_6), value);
	}

	inline static int32_t get_offset_of_backgroundSprite_7() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___backgroundSprite_7)); }
	inline String_t* get_backgroundSprite_7() const { return ___backgroundSprite_7; }
	inline String_t** get_address_of_backgroundSprite_7() { return &___backgroundSprite_7; }
	inline void set_backgroundSprite_7(String_t* value)
	{
		___backgroundSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSprite_7), value);
	}

	inline static int32_t get_offset_of_highlightSprite_8() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___highlightSprite_8)); }
	inline String_t* get_highlightSprite_8() const { return ___highlightSprite_8; }
	inline String_t** get_address_of_highlightSprite_8() { return &___highlightSprite_8; }
	inline void set_highlightSprite_8(String_t* value)
	{
		___highlightSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___highlightSprite_8), value);
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___position_9)); }
	inline int32_t get_position_9() const { return ___position_9; }
	inline int32_t* get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(int32_t value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_items_10() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___items_10)); }
	inline List_1_t4069179741 * get_items_10() const { return ___items_10; }
	inline List_1_t4069179741 ** get_address_of_items_10() { return &___items_10; }
	inline void set_items_10(List_1_t4069179741 * value)
	{
		___items_10 = value;
		Il2CppCodeGenWriteBarrier((&___items_10), value);
	}

	inline static int32_t get_offset_of_padding_11() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___padding_11)); }
	inline Vector2_t328513675  get_padding_11() const { return ___padding_11; }
	inline Vector2_t328513675 * get_address_of_padding_11() { return &___padding_11; }
	inline void set_padding_11(Vector2_t328513675  value)
	{
		___padding_11 = value;
	}

	inline static int32_t get_offset_of_textScale_12() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___textScale_12)); }
	inline float get_textScale_12() const { return ___textScale_12; }
	inline float* get_address_of_textScale_12() { return &___textScale_12; }
	inline void set_textScale_12(float value)
	{
		___textScale_12 = value;
	}

	inline static int32_t get_offset_of_textColor_13() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___textColor_13)); }
	inline Color_t2582018970  get_textColor_13() const { return ___textColor_13; }
	inline Color_t2582018970 * get_address_of_textColor_13() { return &___textColor_13; }
	inline void set_textColor_13(Color_t2582018970  value)
	{
		___textColor_13 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_14() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___backgroundColor_14)); }
	inline Color_t2582018970  get_backgroundColor_14() const { return ___backgroundColor_14; }
	inline Color_t2582018970 * get_address_of_backgroundColor_14() { return &___backgroundColor_14; }
	inline void set_backgroundColor_14(Color_t2582018970  value)
	{
		___backgroundColor_14 = value;
	}

	inline static int32_t get_offset_of_highlightColor_15() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___highlightColor_15)); }
	inline Color_t2582018970  get_highlightColor_15() const { return ___highlightColor_15; }
	inline Color_t2582018970 * get_address_of_highlightColor_15() { return &___highlightColor_15; }
	inline void set_highlightColor_15(Color_t2582018970  value)
	{
		___highlightColor_15 = value;
	}

	inline static int32_t get_offset_of_isAnimated_16() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___isAnimated_16)); }
	inline bool get_isAnimated_16() const { return ___isAnimated_16; }
	inline bool* get_address_of_isAnimated_16() { return &___isAnimated_16; }
	inline void set_isAnimated_16(bool value)
	{
		___isAnimated_16 = value;
	}

	inline static int32_t get_offset_of_isLocalized_17() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___isLocalized_17)); }
	inline bool get_isLocalized_17() const { return ___isLocalized_17; }
	inline bool* get_address_of_isLocalized_17() { return &___isLocalized_17; }
	inline void set_isLocalized_17(bool value)
	{
		___isLocalized_17 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_18() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___eventReceiver_18)); }
	inline GameObject_t2557347079 * get_eventReceiver_18() const { return ___eventReceiver_18; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_18() { return &___eventReceiver_18; }
	inline void set_eventReceiver_18(GameObject_t2557347079 * value)
	{
		___eventReceiver_18 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_18), value);
	}

	inline static int32_t get_offset_of_functionName_19() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___functionName_19)); }
	inline String_t* get_functionName_19() const { return ___functionName_19; }
	inline String_t** get_address_of_functionName_19() { return &___functionName_19; }
	inline void set_functionName_19(String_t* value)
	{
		___functionName_19 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_19), value);
	}

	inline static int32_t get_offset_of_onSelectionChange_20() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___onSelectionChange_20)); }
	inline OnSelectionChange_t705636838 * get_onSelectionChange_20() const { return ___onSelectionChange_20; }
	inline OnSelectionChange_t705636838 ** get_address_of_onSelectionChange_20() { return &___onSelectionChange_20; }
	inline void set_onSelectionChange_20(OnSelectionChange_t705636838 * value)
	{
		___onSelectionChange_20 = value;
		Il2CppCodeGenWriteBarrier((&___onSelectionChange_20), value);
	}

	inline static int32_t get_offset_of_mSelectedItem_21() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mSelectedItem_21)); }
	inline String_t* get_mSelectedItem_21() const { return ___mSelectedItem_21; }
	inline String_t** get_address_of_mSelectedItem_21() { return &___mSelectedItem_21; }
	inline void set_mSelectedItem_21(String_t* value)
	{
		___mSelectedItem_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedItem_21), value);
	}

	inline static int32_t get_offset_of_mPanel_22() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mPanel_22)); }
	inline UIPanel_t825970685 * get_mPanel_22() const { return ___mPanel_22; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_22() { return &___mPanel_22; }
	inline void set_mPanel_22(UIPanel_t825970685 * value)
	{
		___mPanel_22 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_22), value);
	}

	inline static int32_t get_offset_of_mChild_23() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mChild_23)); }
	inline GameObject_t2557347079 * get_mChild_23() const { return ___mChild_23; }
	inline GameObject_t2557347079 ** get_address_of_mChild_23() { return &___mChild_23; }
	inline void set_mChild_23(GameObject_t2557347079 * value)
	{
		___mChild_23 = value;
		Il2CppCodeGenWriteBarrier((&___mChild_23), value);
	}

	inline static int32_t get_offset_of_mBackground_24() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mBackground_24)); }
	inline UISprite_t279728715 * get_mBackground_24() const { return ___mBackground_24; }
	inline UISprite_t279728715 ** get_address_of_mBackground_24() { return &___mBackground_24; }
	inline void set_mBackground_24(UISprite_t279728715 * value)
	{
		___mBackground_24 = value;
		Il2CppCodeGenWriteBarrier((&___mBackground_24), value);
	}

	inline static int32_t get_offset_of_mHighlight_25() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mHighlight_25)); }
	inline UISprite_t279728715 * get_mHighlight_25() const { return ___mHighlight_25; }
	inline UISprite_t279728715 ** get_address_of_mHighlight_25() { return &___mHighlight_25; }
	inline void set_mHighlight_25(UISprite_t279728715 * value)
	{
		___mHighlight_25 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlight_25), value);
	}

	inline static int32_t get_offset_of_mHighlightedLabel_26() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mHighlightedLabel_26)); }
	inline UILabel_t3478074517 * get_mHighlightedLabel_26() const { return ___mHighlightedLabel_26; }
	inline UILabel_t3478074517 ** get_address_of_mHighlightedLabel_26() { return &___mHighlightedLabel_26; }
	inline void set_mHighlightedLabel_26(UILabel_t3478074517 * value)
	{
		___mHighlightedLabel_26 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlightedLabel_26), value);
	}

	inline static int32_t get_offset_of_mLabelList_27() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mLabelList_27)); }
	inline List_1_t2597701524 * get_mLabelList_27() const { return ___mLabelList_27; }
	inline List_1_t2597701524 ** get_address_of_mLabelList_27() { return &___mLabelList_27; }
	inline void set_mLabelList_27(List_1_t2597701524 * value)
	{
		___mLabelList_27 = value;
		Il2CppCodeGenWriteBarrier((&___mLabelList_27), value);
	}

	inline static int32_t get_offset_of_mBgBorder_28() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271, ___mBgBorder_28)); }
	inline float get_mBgBorder_28() const { return ___mBgBorder_28; }
	inline float* get_address_of_mBgBorder_28() { return &___mBgBorder_28; }
	inline void set_mBgBorder_28(float value)
	{
		___mBgBorder_28 = value;
	}
};

struct UIPopupList_t2907709271_StaticFields
{
public:
	// UIPopupList UIPopupList::current
	UIPopupList_t2907709271 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIPopupList_t2907709271_StaticFields, ___current_2)); }
	inline UIPopupList_t2907709271 * get_current_2() const { return ___current_2; }
	inline UIPopupList_t2907709271 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIPopupList_t2907709271 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPLIST_T2907709271_H
#ifndef UIFORWARDEVENTS_T3697131846_H
#define UIFORWARDEVENTS_T3697131846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIForwardEvents
struct  UIForwardEvents_t3697131846  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UIForwardEvents::target
	GameObject_t2557347079 * ___target_2;
	// System.Boolean UIForwardEvents::onHover
	bool ___onHover_3;
	// System.Boolean UIForwardEvents::onPress
	bool ___onPress_4;
	// System.Boolean UIForwardEvents::onClick
	bool ___onClick_5;
	// System.Boolean UIForwardEvents::onDoubleClick
	bool ___onDoubleClick_6;
	// System.Boolean UIForwardEvents::onSelect
	bool ___onSelect_7;
	// System.Boolean UIForwardEvents::onDrag
	bool ___onDrag_8;
	// System.Boolean UIForwardEvents::onDrop
	bool ___onDrop_9;
	// System.Boolean UIForwardEvents::onInput
	bool ___onInput_10;
	// System.Boolean UIForwardEvents::onSubmit
	bool ___onSubmit_11;
	// System.Boolean UIForwardEvents::onScroll
	bool ___onScroll_12;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___target_2)); }
	inline GameObject_t2557347079 * get_target_2() const { return ___target_2; }
	inline GameObject_t2557347079 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t2557347079 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_onHover_3() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onHover_3)); }
	inline bool get_onHover_3() const { return ___onHover_3; }
	inline bool* get_address_of_onHover_3() { return &___onHover_3; }
	inline void set_onHover_3(bool value)
	{
		___onHover_3 = value;
	}

	inline static int32_t get_offset_of_onPress_4() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onPress_4)); }
	inline bool get_onPress_4() const { return ___onPress_4; }
	inline bool* get_address_of_onPress_4() { return &___onPress_4; }
	inline void set_onPress_4(bool value)
	{
		___onPress_4 = value;
	}

	inline static int32_t get_offset_of_onClick_5() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onClick_5)); }
	inline bool get_onClick_5() const { return ___onClick_5; }
	inline bool* get_address_of_onClick_5() { return &___onClick_5; }
	inline void set_onClick_5(bool value)
	{
		___onClick_5 = value;
	}

	inline static int32_t get_offset_of_onDoubleClick_6() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onDoubleClick_6)); }
	inline bool get_onDoubleClick_6() const { return ___onDoubleClick_6; }
	inline bool* get_address_of_onDoubleClick_6() { return &___onDoubleClick_6; }
	inline void set_onDoubleClick_6(bool value)
	{
		___onDoubleClick_6 = value;
	}

	inline static int32_t get_offset_of_onSelect_7() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onSelect_7)); }
	inline bool get_onSelect_7() const { return ___onSelect_7; }
	inline bool* get_address_of_onSelect_7() { return &___onSelect_7; }
	inline void set_onSelect_7(bool value)
	{
		___onSelect_7 = value;
	}

	inline static int32_t get_offset_of_onDrag_8() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onDrag_8)); }
	inline bool get_onDrag_8() const { return ___onDrag_8; }
	inline bool* get_address_of_onDrag_8() { return &___onDrag_8; }
	inline void set_onDrag_8(bool value)
	{
		___onDrag_8 = value;
	}

	inline static int32_t get_offset_of_onDrop_9() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onDrop_9)); }
	inline bool get_onDrop_9() const { return ___onDrop_9; }
	inline bool* get_address_of_onDrop_9() { return &___onDrop_9; }
	inline void set_onDrop_9(bool value)
	{
		___onDrop_9 = value;
	}

	inline static int32_t get_offset_of_onInput_10() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onInput_10)); }
	inline bool get_onInput_10() const { return ___onInput_10; }
	inline bool* get_address_of_onInput_10() { return &___onInput_10; }
	inline void set_onInput_10(bool value)
	{
		___onInput_10 = value;
	}

	inline static int32_t get_offset_of_onSubmit_11() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onSubmit_11)); }
	inline bool get_onSubmit_11() const { return ___onSubmit_11; }
	inline bool* get_address_of_onSubmit_11() { return &___onSubmit_11; }
	inline void set_onSubmit_11(bool value)
	{
		___onSubmit_11 = value;
	}

	inline static int32_t get_offset_of_onScroll_12() { return static_cast<int32_t>(offsetof(UIForwardEvents_t3697131846, ___onScroll_12)); }
	inline bool get_onScroll_12() const { return ___onScroll_12; }
	inline bool* get_address_of_onScroll_12() { return &___onScroll_12; }
	inline void set_onScroll_12(bool value)
	{
		___onScroll_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFORWARDEVENTS_T3697131846_H
#ifndef UICHECKBOXCONTROLLEDOBJECT_T218782148_H
#define UICHECKBOXCONTROLLEDOBJECT_T218782148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICheckboxControlledObject
struct  UICheckboxControlledObject_t218782148  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UICheckboxControlledObject::target
	GameObject_t2557347079 * ___target_2;
	// System.Boolean UICheckboxControlledObject::inverse
	bool ___inverse_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UICheckboxControlledObject_t218782148, ___target_2)); }
	inline GameObject_t2557347079 * get_target_2() const { return ___target_2; }
	inline GameObject_t2557347079 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t2557347079 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_inverse_3() { return static_cast<int32_t>(offsetof(UICheckboxControlledObject_t218782148, ___inverse_3)); }
	inline bool get_inverse_3() const { return ___inverse_3; }
	inline bool* get_address_of_inverse_3() { return &___inverse_3; }
	inline void set_inverse_3(bool value)
	{
		___inverse_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHECKBOXCONTROLLEDOBJECT_T218782148_H
#ifndef UICENTERONCHILD_T1552049414_H
#define UICENTERONCHILD_T1552049414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICenterOnChild
struct  UICenterOnChild_t1552049414  : public MonoBehaviour_t1618594486
{
public:
	// SpringPanel/OnFinished UICenterOnChild::onFinished
	OnFinished_t2500445478 * ___onFinished_2;
	// UIDraggablePanel UICenterOnChild::mDrag
	UIDraggablePanel_t2580300900 * ___mDrag_3;
	// UnityEngine.GameObject UICenterOnChild::mCenteredObject
	GameObject_t2557347079 * ___mCenteredObject_4;

public:
	inline static int32_t get_offset_of_onFinished_2() { return static_cast<int32_t>(offsetof(UICenterOnChild_t1552049414, ___onFinished_2)); }
	inline OnFinished_t2500445478 * get_onFinished_2() const { return ___onFinished_2; }
	inline OnFinished_t2500445478 ** get_address_of_onFinished_2() { return &___onFinished_2; }
	inline void set_onFinished_2(OnFinished_t2500445478 * value)
	{
		___onFinished_2 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_2), value);
	}

	inline static int32_t get_offset_of_mDrag_3() { return static_cast<int32_t>(offsetof(UICenterOnChild_t1552049414, ___mDrag_3)); }
	inline UIDraggablePanel_t2580300900 * get_mDrag_3() const { return ___mDrag_3; }
	inline UIDraggablePanel_t2580300900 ** get_address_of_mDrag_3() { return &___mDrag_3; }
	inline void set_mDrag_3(UIDraggablePanel_t2580300900 * value)
	{
		___mDrag_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDrag_3), value);
	}

	inline static int32_t get_offset_of_mCenteredObject_4() { return static_cast<int32_t>(offsetof(UICenterOnChild_t1552049414, ___mCenteredObject_4)); }
	inline GameObject_t2557347079 * get_mCenteredObject_4() const { return ___mCenteredObject_4; }
	inline GameObject_t2557347079 ** get_address_of_mCenteredObject_4() { return &___mCenteredObject_4; }
	inline void set_mCenteredObject_4(GameObject_t2557347079 * value)
	{
		___mCenteredObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCenteredObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICENTERONCHILD_T1552049414_H
#ifndef LOADLEVELONCLICK_T1268630639_H
#define LOADLEVELONCLICK_T1268630639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLevelOnClick
struct  LoadLevelOnClick_t1268630639  : public MonoBehaviour_t1618594486
{
public:
	// System.String LoadLevelOnClick::levelName
	String_t* ___levelName_2;

public:
	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(LoadLevelOnClick_t1268630639, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLEVELONCLICK_T1268630639_H
#ifndef LAGROTATION_T1815997116_H
#define LAGROTATION_T1815997116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LagRotation
struct  LagRotation_t1815997116  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 LagRotation::updateOrder
	int32_t ___updateOrder_2;
	// System.Single LagRotation::speed
	float ___speed_3;
	// System.Boolean LagRotation::ignoreTimeScale
	bool ___ignoreTimeScale_4;
	// UnityEngine.Transform LagRotation::mTrans
	Transform_t362059596 * ___mTrans_5;
	// UnityEngine.Quaternion LagRotation::mRelative
	Quaternion_t704191599  ___mRelative_6;
	// UnityEngine.Quaternion LagRotation::mAbsolute
	Quaternion_t704191599  ___mAbsolute_7;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_4() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___ignoreTimeScale_4)); }
	inline bool get_ignoreTimeScale_4() const { return ___ignoreTimeScale_4; }
	inline bool* get_address_of_ignoreTimeScale_4() { return &___ignoreTimeScale_4; }
	inline void set_ignoreTimeScale_4(bool value)
	{
		___ignoreTimeScale_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mRelative_6() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___mRelative_6)); }
	inline Quaternion_t704191599  get_mRelative_6() const { return ___mRelative_6; }
	inline Quaternion_t704191599 * get_address_of_mRelative_6() { return &___mRelative_6; }
	inline void set_mRelative_6(Quaternion_t704191599  value)
	{
		___mRelative_6 = value;
	}

	inline static int32_t get_offset_of_mAbsolute_7() { return static_cast<int32_t>(offsetof(LagRotation_t1815997116, ___mAbsolute_7)); }
	inline Quaternion_t704191599  get_mAbsolute_7() const { return ___mAbsolute_7; }
	inline Quaternion_t704191599 * get_address_of_mAbsolute_7() { return &___mAbsolute_7; }
	inline void set_mAbsolute_7(Quaternion_t704191599  value)
	{
		___mAbsolute_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAGROTATION_T1815997116_H
#ifndef LAGPOSITION_T1504341716_H
#define LAGPOSITION_T1504341716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LagPosition
struct  LagPosition_t1504341716  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 LagPosition::updateOrder
	int32_t ___updateOrder_2;
	// UnityEngine.Vector3 LagPosition::speed
	Vector3_t1986933152  ___speed_3;
	// System.Boolean LagPosition::ignoreTimeScale
	bool ___ignoreTimeScale_4;
	// UnityEngine.Transform LagPosition::mTrans
	Transform_t362059596 * ___mTrans_5;
	// UnityEngine.Vector3 LagPosition::mRelative
	Vector3_t1986933152  ___mRelative_6;
	// UnityEngine.Vector3 LagPosition::mAbsolute
	Vector3_t1986933152  ___mAbsolute_7;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___speed_3)); }
	inline Vector3_t1986933152  get_speed_3() const { return ___speed_3; }
	inline Vector3_t1986933152 * get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(Vector3_t1986933152  value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_4() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___ignoreTimeScale_4)); }
	inline bool get_ignoreTimeScale_4() const { return ___ignoreTimeScale_4; }
	inline bool* get_address_of_ignoreTimeScale_4() { return &___ignoreTimeScale_4; }
	inline void set_ignoreTimeScale_4(bool value)
	{
		___ignoreTimeScale_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mRelative_6() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___mRelative_6)); }
	inline Vector3_t1986933152  get_mRelative_6() const { return ___mRelative_6; }
	inline Vector3_t1986933152 * get_address_of_mRelative_6() { return &___mRelative_6; }
	inline void set_mRelative_6(Vector3_t1986933152  value)
	{
		___mRelative_6 = value;
	}

	inline static int32_t get_offset_of_mAbsolute_7() { return static_cast<int32_t>(offsetof(LagPosition_t1504341716, ___mAbsolute_7)); }
	inline Vector3_t1986933152  get_mAbsolute_7() const { return ___mAbsolute_7; }
	inline Vector3_t1986933152 * get_address_of_mAbsolute_7() { return &___mAbsolute_7; }
	inline void set_mAbsolute_7(Vector3_t1986933152  value)
	{
		___mAbsolute_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAGPOSITION_T1504341716_H
#ifndef DRAGDROPSURFACE_T3929611577_H
#define DRAGDROPSURFACE_T3929611577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragDropSurface
struct  DragDropSurface_t3929611577  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean DragDropSurface::rotatePlacedObject
	bool ___rotatePlacedObject_2;

public:
	inline static int32_t get_offset_of_rotatePlacedObject_2() { return static_cast<int32_t>(offsetof(DragDropSurface_t3929611577, ___rotatePlacedObject_2)); }
	inline bool get_rotatePlacedObject_2() const { return ___rotatePlacedObject_2; }
	inline bool* get_address_of_rotatePlacedObject_2() { return &___rotatePlacedObject_2; }
	inline void set_rotatePlacedObject_2(bool value)
	{
		___rotatePlacedObject_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGDROPSURFACE_T3929611577_H
#ifndef DRAGDROPROOT_T2133709399_H
#define DRAGDROPROOT_T2133709399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragDropRoot
struct  DragDropRoot_t2133709399  : public MonoBehaviour_t1618594486
{
public:

public:
};

struct DragDropRoot_t2133709399_StaticFields
{
public:
	// UnityEngine.Transform DragDropRoot::root
	Transform_t362059596 * ___root_2;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(DragDropRoot_t2133709399_StaticFields, ___root_2)); }
	inline Transform_t362059596 * get_root_2() const { return ___root_2; }
	inline Transform_t362059596 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(Transform_t362059596 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier((&___root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGDROPROOT_T2133709399_H
#ifndef DRAGDROPITEM_T1159484558_H
#define DRAGDROPITEM_T1159484558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragDropItem
struct  DragDropItem_t1159484558  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject DragDropItem::prefab
	GameObject_t2557347079 * ___prefab_2;
	// UnityEngine.Transform DragDropItem::mTrans
	Transform_t362059596 * ___mTrans_3;
	// System.Boolean DragDropItem::mIsDragging
	bool ___mIsDragging_4;
	// UnityEngine.Transform DragDropItem::mParent
	Transform_t362059596 * ___mParent_5;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(DragDropItem_t1159484558, ___prefab_2)); }
	inline GameObject_t2557347079 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t2557347079 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t2557347079 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_2), value);
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(DragDropItem_t1159484558, ___mTrans_3)); }
	inline Transform_t362059596 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t362059596 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t362059596 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mIsDragging_4() { return static_cast<int32_t>(offsetof(DragDropItem_t1159484558, ___mIsDragging_4)); }
	inline bool get_mIsDragging_4() const { return ___mIsDragging_4; }
	inline bool* get_address_of_mIsDragging_4() { return &___mIsDragging_4; }
	inline void set_mIsDragging_4(bool value)
	{
		___mIsDragging_4 = value;
	}

	inline static int32_t get_offset_of_mParent_5() { return static_cast<int32_t>(offsetof(DragDropItem_t1159484558, ___mParent_5)); }
	inline Transform_t362059596 * get_mParent_5() const { return ___mParent_5; }
	inline Transform_t362059596 ** get_address_of_mParent_5() { return &___mParent_5; }
	inline void set_mParent_5(Transform_t362059596 * value)
	{
		___mParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGDROPITEM_T1159484558_H
#ifndef DRAGDROPCONTAINER_T946801301_H
#define DRAGDROPCONTAINER_T946801301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragDropContainer
struct  DragDropContainer_t946801301  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGDROPCONTAINER_T946801301_H
#ifndef DOWNLOADTEXTURE_T1340095241_H
#define DOWNLOADTEXTURE_T1340095241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadTexture
struct  DownloadTexture_t1340095241  : public MonoBehaviour_t1618594486
{
public:
	// System.String DownloadTexture::url
	String_t* ___url_2;
	// UnityEngine.Material DownloadTexture::mMat
	Material_t2815264910 * ___mMat_3;
	// UnityEngine.Texture2D DownloadTexture::mTex
	Texture2D_t3063074017 * ___mTex_4;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(DownloadTexture_t1340095241, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_mMat_3() { return static_cast<int32_t>(offsetof(DownloadTexture_t1340095241, ___mMat_3)); }
	inline Material_t2815264910 * get_mMat_3() const { return ___mMat_3; }
	inline Material_t2815264910 ** get_address_of_mMat_3() { return &___mMat_3; }
	inline void set_mMat_3(Material_t2815264910 * value)
	{
		___mMat_3 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_3), value);
	}

	inline static int32_t get_offset_of_mTex_4() { return static_cast<int32_t>(offsetof(DownloadTexture_t1340095241, ___mTex_4)); }
	inline Texture2D_t3063074017 * get_mTex_4() const { return ___mTex_4; }
	inline Texture2D_t3063074017 ** get_address_of_mTex_4() { return &___mTex_4; }
	inline void set_mTex_4(Texture2D_t3063074017 * value)
	{
		___mTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTex_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADTEXTURE_T1340095241_H
#ifndef CHATINPUT_T3069896720_H
#define CHATINPUT_T3069896720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatInput
struct  ChatInput_t3069896720  : public MonoBehaviour_t1618594486
{
public:
	// UITextList ChatInput::textList
	UITextList_t1100452823 * ___textList_2;
	// System.Boolean ChatInput::fillWithDummyData
	bool ___fillWithDummyData_3;
	// UIInput ChatInput::mInput
	UIInput_t1674607152 * ___mInput_4;
	// System.Boolean ChatInput::mIgnoreNextEnter
	bool ___mIgnoreNextEnter_5;

public:
	inline static int32_t get_offset_of_textList_2() { return static_cast<int32_t>(offsetof(ChatInput_t3069896720, ___textList_2)); }
	inline UITextList_t1100452823 * get_textList_2() const { return ___textList_2; }
	inline UITextList_t1100452823 ** get_address_of_textList_2() { return &___textList_2; }
	inline void set_textList_2(UITextList_t1100452823 * value)
	{
		___textList_2 = value;
		Il2CppCodeGenWriteBarrier((&___textList_2), value);
	}

	inline static int32_t get_offset_of_fillWithDummyData_3() { return static_cast<int32_t>(offsetof(ChatInput_t3069896720, ___fillWithDummyData_3)); }
	inline bool get_fillWithDummyData_3() const { return ___fillWithDummyData_3; }
	inline bool* get_address_of_fillWithDummyData_3() { return &___fillWithDummyData_3; }
	inline void set_fillWithDummyData_3(bool value)
	{
		___fillWithDummyData_3 = value;
	}

	inline static int32_t get_offset_of_mInput_4() { return static_cast<int32_t>(offsetof(ChatInput_t3069896720, ___mInput_4)); }
	inline UIInput_t1674607152 * get_mInput_4() const { return ___mInput_4; }
	inline UIInput_t1674607152 ** get_address_of_mInput_4() { return &___mInput_4; }
	inline void set_mInput_4(UIInput_t1674607152 * value)
	{
		___mInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInput_4), value);
	}

	inline static int32_t get_offset_of_mIgnoreNextEnter_5() { return static_cast<int32_t>(offsetof(ChatInput_t3069896720, ___mIgnoreNextEnter_5)); }
	inline bool get_mIgnoreNextEnter_5() const { return ___mIgnoreNextEnter_5; }
	inline bool* get_address_of_mIgnoreNextEnter_5() { return &___mIgnoreNextEnter_5; }
	inline void set_mIgnoreNextEnter_5(bool value)
	{
		___mIgnoreNextEnter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATINPUT_T3069896720_H
#ifndef INVEQUIPMENT_T2308977056_H
#define INVEQUIPMENT_T2308977056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvEquipment
struct  InvEquipment_t2308977056  : public MonoBehaviour_t1618594486
{
public:
	// InvGameItem[] InvEquipment::mItems
	InvGameItemU5BU5D_t1090198953* ___mItems_2;
	// InvAttachmentPoint[] InvEquipment::mAttachments
	InvAttachmentPointU5BU5D_t579766857* ___mAttachments_3;

public:
	inline static int32_t get_offset_of_mItems_2() { return static_cast<int32_t>(offsetof(InvEquipment_t2308977056, ___mItems_2)); }
	inline InvGameItemU5BU5D_t1090198953* get_mItems_2() const { return ___mItems_2; }
	inline InvGameItemU5BU5D_t1090198953** get_address_of_mItems_2() { return &___mItems_2; }
	inline void set_mItems_2(InvGameItemU5BU5D_t1090198953* value)
	{
		___mItems_2 = value;
		Il2CppCodeGenWriteBarrier((&___mItems_2), value);
	}

	inline static int32_t get_offset_of_mAttachments_3() { return static_cast<int32_t>(offsetof(InvEquipment_t2308977056, ___mAttachments_3)); }
	inline InvAttachmentPointU5BU5D_t579766857* get_mAttachments_3() const { return ___mAttachments_3; }
	inline InvAttachmentPointU5BU5D_t579766857** get_address_of_mAttachments_3() { return &___mAttachments_3; }
	inline void set_mAttachments_3(InvAttachmentPointU5BU5D_t579766857* value)
	{
		___mAttachments_3 = value;
		Il2CppCodeGenWriteBarrier((&___mAttachments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVEQUIPMENT_T2308977056_H
#ifndef INVDATABASE_T2105022301_H
#define INVDATABASE_T2105022301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvDatabase
struct  InvDatabase_t2105022301  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 InvDatabase::databaseID
	int32_t ___databaseID_4;
	// System.Collections.Generic.List`1<InvBaseItem> InvDatabase::items
	List_1_t3496819322 * ___items_5;
	// UIAtlas InvDatabase::iconAtlas
	UIAtlas_t1815452364 * ___iconAtlas_6;

public:
	inline static int32_t get_offset_of_databaseID_4() { return static_cast<int32_t>(offsetof(InvDatabase_t2105022301, ___databaseID_4)); }
	inline int32_t get_databaseID_4() const { return ___databaseID_4; }
	inline int32_t* get_address_of_databaseID_4() { return &___databaseID_4; }
	inline void set_databaseID_4(int32_t value)
	{
		___databaseID_4 = value;
	}

	inline static int32_t get_offset_of_items_5() { return static_cast<int32_t>(offsetof(InvDatabase_t2105022301, ___items_5)); }
	inline List_1_t3496819322 * get_items_5() const { return ___items_5; }
	inline List_1_t3496819322 ** get_address_of_items_5() { return &___items_5; }
	inline void set_items_5(List_1_t3496819322 * value)
	{
		___items_5 = value;
		Il2CppCodeGenWriteBarrier((&___items_5), value);
	}

	inline static int32_t get_offset_of_iconAtlas_6() { return static_cast<int32_t>(offsetof(InvDatabase_t2105022301, ___iconAtlas_6)); }
	inline UIAtlas_t1815452364 * get_iconAtlas_6() const { return ___iconAtlas_6; }
	inline UIAtlas_t1815452364 ** get_address_of_iconAtlas_6() { return &___iconAtlas_6; }
	inline void set_iconAtlas_6(UIAtlas_t1815452364 * value)
	{
		___iconAtlas_6 = value;
		Il2CppCodeGenWriteBarrier((&___iconAtlas_6), value);
	}
};

struct InvDatabase_t2105022301_StaticFields
{
public:
	// InvDatabase[] InvDatabase::mList
	InvDatabaseU5BU5D_t1364645520* ___mList_2;
	// System.Boolean InvDatabase::mIsDirty
	bool ___mIsDirty_3;

public:
	inline static int32_t get_offset_of_mList_2() { return static_cast<int32_t>(offsetof(InvDatabase_t2105022301_StaticFields, ___mList_2)); }
	inline InvDatabaseU5BU5D_t1364645520* get_mList_2() const { return ___mList_2; }
	inline InvDatabaseU5BU5D_t1364645520** get_address_of_mList_2() { return &___mList_2; }
	inline void set_mList_2(InvDatabaseU5BU5D_t1364645520* value)
	{
		___mList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mList_2), value);
	}

	inline static int32_t get_offset_of_mIsDirty_3() { return static_cast<int32_t>(offsetof(InvDatabase_t2105022301_StaticFields, ___mIsDirty_3)); }
	inline bool get_mIsDirty_3() const { return ___mIsDirty_3; }
	inline bool* get_address_of_mIsDirty_3() { return &___mIsDirty_3; }
	inline void set_mIsDirty_3(bool value)
	{
		___mIsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVDATABASE_T2105022301_H
#ifndef INVATTACHMENTPOINT_T3318177112_H
#define INVATTACHMENTPOINT_T3318177112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvAttachmentPoint
struct  InvAttachmentPoint_t3318177112  : public MonoBehaviour_t1618594486
{
public:
	// InvBaseItem/Slot InvAttachmentPoint::slot
	int32_t ___slot_2;
	// UnityEngine.GameObject InvAttachmentPoint::mPrefab
	GameObject_t2557347079 * ___mPrefab_3;
	// UnityEngine.GameObject InvAttachmentPoint::mChild
	GameObject_t2557347079 * ___mChild_4;

public:
	inline static int32_t get_offset_of_slot_2() { return static_cast<int32_t>(offsetof(InvAttachmentPoint_t3318177112, ___slot_2)); }
	inline int32_t get_slot_2() const { return ___slot_2; }
	inline int32_t* get_address_of_slot_2() { return &___slot_2; }
	inline void set_slot_2(int32_t value)
	{
		___slot_2 = value;
	}

	inline static int32_t get_offset_of_mPrefab_3() { return static_cast<int32_t>(offsetof(InvAttachmentPoint_t3318177112, ___mPrefab_3)); }
	inline GameObject_t2557347079 * get_mPrefab_3() const { return ___mPrefab_3; }
	inline GameObject_t2557347079 ** get_address_of_mPrefab_3() { return &___mPrefab_3; }
	inline void set_mPrefab_3(GameObject_t2557347079 * value)
	{
		___mPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPrefab_3), value);
	}

	inline static int32_t get_offset_of_mChild_4() { return static_cast<int32_t>(offsetof(InvAttachmentPoint_t3318177112, ___mChild_4)); }
	inline GameObject_t2557347079 * get_mChild_4() const { return ___mChild_4; }
	inline GameObject_t2557347079 ** get_address_of_mChild_4() { return &___mChild_4; }
	inline void set_mChild_4(GameObject_t2557347079 * value)
	{
		___mChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___mChild_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVATTACHMENTPOINT_T3318177112_H
#ifndef UIITEMSTORAGE_T1447122887_H
#define UIITEMSTORAGE_T1447122887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItemStorage
struct  UIItemStorage_t1447122887  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 UIItemStorage::maxItemCount
	int32_t ___maxItemCount_2;
	// System.Int32 UIItemStorage::maxRows
	int32_t ___maxRows_3;
	// System.Int32 UIItemStorage::maxColumns
	int32_t ___maxColumns_4;
	// UnityEngine.GameObject UIItemStorage::template
	GameObject_t2557347079 * ___template_5;
	// UIWidget UIItemStorage::background
	UIWidget_t1961100141 * ___background_6;
	// System.Int32 UIItemStorage::spacing
	int32_t ___spacing_7;
	// System.Int32 UIItemStorage::padding
	int32_t ___padding_8;
	// System.Collections.Generic.List`1<InvGameItem> UIItemStorage::mItems
	List_1_t740300919 * ___mItems_9;

public:
	inline static int32_t get_offset_of_maxItemCount_2() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___maxItemCount_2)); }
	inline int32_t get_maxItemCount_2() const { return ___maxItemCount_2; }
	inline int32_t* get_address_of_maxItemCount_2() { return &___maxItemCount_2; }
	inline void set_maxItemCount_2(int32_t value)
	{
		___maxItemCount_2 = value;
	}

	inline static int32_t get_offset_of_maxRows_3() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___maxRows_3)); }
	inline int32_t get_maxRows_3() const { return ___maxRows_3; }
	inline int32_t* get_address_of_maxRows_3() { return &___maxRows_3; }
	inline void set_maxRows_3(int32_t value)
	{
		___maxRows_3 = value;
	}

	inline static int32_t get_offset_of_maxColumns_4() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___maxColumns_4)); }
	inline int32_t get_maxColumns_4() const { return ___maxColumns_4; }
	inline int32_t* get_address_of_maxColumns_4() { return &___maxColumns_4; }
	inline void set_maxColumns_4(int32_t value)
	{
		___maxColumns_4 = value;
	}

	inline static int32_t get_offset_of_template_5() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___template_5)); }
	inline GameObject_t2557347079 * get_template_5() const { return ___template_5; }
	inline GameObject_t2557347079 ** get_address_of_template_5() { return &___template_5; }
	inline void set_template_5(GameObject_t2557347079 * value)
	{
		___template_5 = value;
		Il2CppCodeGenWriteBarrier((&___template_5), value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___background_6)); }
	inline UIWidget_t1961100141 * get_background_6() const { return ___background_6; }
	inline UIWidget_t1961100141 ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(UIWidget_t1961100141 * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier((&___background_6), value);
	}

	inline static int32_t get_offset_of_spacing_7() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___spacing_7)); }
	inline int32_t get_spacing_7() const { return ___spacing_7; }
	inline int32_t* get_address_of_spacing_7() { return &___spacing_7; }
	inline void set_spacing_7(int32_t value)
	{
		___spacing_7 = value;
	}

	inline static int32_t get_offset_of_padding_8() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___padding_8)); }
	inline int32_t get_padding_8() const { return ___padding_8; }
	inline int32_t* get_address_of_padding_8() { return &___padding_8; }
	inline void set_padding_8(int32_t value)
	{
		___padding_8 = value;
	}

	inline static int32_t get_offset_of_mItems_9() { return static_cast<int32_t>(offsetof(UIItemStorage_t1447122887, ___mItems_9)); }
	inline List_1_t740300919 * get_mItems_9() const { return ___mItems_9; }
	inline List_1_t740300919 ** get_address_of_mItems_9() { return &___mItems_9; }
	inline void set_mItems_9(List_1_t740300919 * value)
	{
		___mItems_9 = value;
		Il2CppCodeGenWriteBarrier((&___mItems_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEMSTORAGE_T1447122887_H
#ifndef UIITEMSLOT_T3241389696_H
#define UIITEMSLOT_T3241389696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItemSlot
struct  UIItemSlot_t3241389696  : public MonoBehaviour_t1618594486
{
public:
	// UISprite UIItemSlot::icon
	UISprite_t279728715 * ___icon_2;
	// UIWidget UIItemSlot::background
	UIWidget_t1961100141 * ___background_3;
	// UILabel UIItemSlot::label
	UILabel_t3478074517 * ___label_4;
	// UnityEngine.AudioClip UIItemSlot::grabSound
	AudioClip_t1419814565 * ___grabSound_5;
	// UnityEngine.AudioClip UIItemSlot::placeSound
	AudioClip_t1419814565 * ___placeSound_6;
	// UnityEngine.AudioClip UIItemSlot::errorSound
	AudioClip_t1419814565 * ___errorSound_7;
	// InvGameItem UIItemSlot::mItem
	InvGameItem_t1620673912 * ___mItem_8;
	// System.String UIItemSlot::mText
	String_t* ___mText_9;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___icon_2)); }
	inline UISprite_t279728715 * get_icon_2() const { return ___icon_2; }
	inline UISprite_t279728715 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(UISprite_t279728715 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((&___icon_2), value);
	}

	inline static int32_t get_offset_of_background_3() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___background_3)); }
	inline UIWidget_t1961100141 * get_background_3() const { return ___background_3; }
	inline UIWidget_t1961100141 ** get_address_of_background_3() { return &___background_3; }
	inline void set_background_3(UIWidget_t1961100141 * value)
	{
		___background_3 = value;
		Il2CppCodeGenWriteBarrier((&___background_3), value);
	}

	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___label_4)); }
	inline UILabel_t3478074517 * get_label_4() const { return ___label_4; }
	inline UILabel_t3478074517 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(UILabel_t3478074517 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier((&___label_4), value);
	}

	inline static int32_t get_offset_of_grabSound_5() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___grabSound_5)); }
	inline AudioClip_t1419814565 * get_grabSound_5() const { return ___grabSound_5; }
	inline AudioClip_t1419814565 ** get_address_of_grabSound_5() { return &___grabSound_5; }
	inline void set_grabSound_5(AudioClip_t1419814565 * value)
	{
		___grabSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___grabSound_5), value);
	}

	inline static int32_t get_offset_of_placeSound_6() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___placeSound_6)); }
	inline AudioClip_t1419814565 * get_placeSound_6() const { return ___placeSound_6; }
	inline AudioClip_t1419814565 ** get_address_of_placeSound_6() { return &___placeSound_6; }
	inline void set_placeSound_6(AudioClip_t1419814565 * value)
	{
		___placeSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___placeSound_6), value);
	}

	inline static int32_t get_offset_of_errorSound_7() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___errorSound_7)); }
	inline AudioClip_t1419814565 * get_errorSound_7() const { return ___errorSound_7; }
	inline AudioClip_t1419814565 ** get_address_of_errorSound_7() { return &___errorSound_7; }
	inline void set_errorSound_7(AudioClip_t1419814565 * value)
	{
		___errorSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___errorSound_7), value);
	}

	inline static int32_t get_offset_of_mItem_8() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___mItem_8)); }
	inline InvGameItem_t1620673912 * get_mItem_8() const { return ___mItem_8; }
	inline InvGameItem_t1620673912 ** get_address_of_mItem_8() { return &___mItem_8; }
	inline void set_mItem_8(InvGameItem_t1620673912 * value)
	{
		___mItem_8 = value;
		Il2CppCodeGenWriteBarrier((&___mItem_8), value);
	}

	inline static int32_t get_offset_of_mText_9() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696, ___mText_9)); }
	inline String_t* get_mText_9() const { return ___mText_9; }
	inline String_t** get_address_of_mText_9() { return &___mText_9; }
	inline void set_mText_9(String_t* value)
	{
		___mText_9 = value;
		Il2CppCodeGenWriteBarrier((&___mText_9), value);
	}
};

struct UIItemSlot_t3241389696_StaticFields
{
public:
	// InvGameItem UIItemSlot::mDraggedItem
	InvGameItem_t1620673912 * ___mDraggedItem_10;

public:
	inline static int32_t get_offset_of_mDraggedItem_10() { return static_cast<int32_t>(offsetof(UIItemSlot_t3241389696_StaticFields, ___mDraggedItem_10)); }
	inline InvGameItem_t1620673912 * get_mDraggedItem_10() const { return ___mDraggedItem_10; }
	inline InvGameItem_t1620673912 ** get_address_of_mDraggedItem_10() { return &___mDraggedItem_10; }
	inline void set_mDraggedItem_10(InvGameItem_t1620673912 * value)
	{
		___mDraggedItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDraggedItem_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEMSLOT_T3241389696_H
#ifndef UICURSOR_T3371638195_H
#define UICURSOR_T3371638195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICursor
struct  UICursor_t3371638195  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Camera UICursor::uiCamera
	Camera_t2839736942 * ___uiCamera_3;
	// UnityEngine.Transform UICursor::mTrans
	Transform_t362059596 * ___mTrans_4;
	// UISprite UICursor::mSprite
	UISprite_t279728715 * ___mSprite_5;
	// UIAtlas UICursor::mAtlas
	UIAtlas_t1815452364 * ___mAtlas_6;
	// System.String UICursor::mSpriteName
	String_t* ___mSpriteName_7;

public:
	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(UICursor_t3371638195, ___uiCamera_3)); }
	inline Camera_t2839736942 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t2839736942 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_mTrans_4() { return static_cast<int32_t>(offsetof(UICursor_t3371638195, ___mTrans_4)); }
	inline Transform_t362059596 * get_mTrans_4() const { return ___mTrans_4; }
	inline Transform_t362059596 ** get_address_of_mTrans_4() { return &___mTrans_4; }
	inline void set_mTrans_4(Transform_t362059596 * value)
	{
		___mTrans_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_4), value);
	}

	inline static int32_t get_offset_of_mSprite_5() { return static_cast<int32_t>(offsetof(UICursor_t3371638195, ___mSprite_5)); }
	inline UISprite_t279728715 * get_mSprite_5() const { return ___mSprite_5; }
	inline UISprite_t279728715 ** get_address_of_mSprite_5() { return &___mSprite_5; }
	inline void set_mSprite_5(UISprite_t279728715 * value)
	{
		___mSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_5), value);
	}

	inline static int32_t get_offset_of_mAtlas_6() { return static_cast<int32_t>(offsetof(UICursor_t3371638195, ___mAtlas_6)); }
	inline UIAtlas_t1815452364 * get_mAtlas_6() const { return ___mAtlas_6; }
	inline UIAtlas_t1815452364 ** get_address_of_mAtlas_6() { return &___mAtlas_6; }
	inline void set_mAtlas_6(UIAtlas_t1815452364 * value)
	{
		___mAtlas_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_6), value);
	}

	inline static int32_t get_offset_of_mSpriteName_7() { return static_cast<int32_t>(offsetof(UICursor_t3371638195, ___mSpriteName_7)); }
	inline String_t* get_mSpriteName_7() const { return ___mSpriteName_7; }
	inline String_t** get_address_of_mSpriteName_7() { return &___mSpriteName_7; }
	inline void set_mSpriteName_7(String_t* value)
	{
		___mSpriteName_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_7), value);
	}
};

struct UICursor_t3371638195_StaticFields
{
public:
	// UICursor UICursor::mInstance
	UICursor_t3371638195 * ___mInstance_2;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(UICursor_t3371638195_StaticFields, ___mInstance_2)); }
	inline UICursor_t3371638195 * get_mInstance_2() const { return ___mInstance_2; }
	inline UICursor_t3371638195 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(UICursor_t3371638195 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICURSOR_T3371638195_H
#ifndef EQUIPRANDOMITEM_T1811729489_H
#define EQUIPRANDOMITEM_T1811729489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EquipRandomItem
struct  EquipRandomItem_t1811729489  : public MonoBehaviour_t1618594486
{
public:
	// InvEquipment EquipRandomItem::equipment
	InvEquipment_t2308977056 * ___equipment_2;

public:
	inline static int32_t get_offset_of_equipment_2() { return static_cast<int32_t>(offsetof(EquipRandomItem_t1811729489, ___equipment_2)); }
	inline InvEquipment_t2308977056 * get_equipment_2() const { return ___equipment_2; }
	inline InvEquipment_t2308977056 ** get_address_of_equipment_2() { return &___equipment_2; }
	inline void set_equipment_2(InvEquipment_t2308977056 * value)
	{
		___equipment_2 = value;
		Il2CppCodeGenWriteBarrier((&___equipment_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUIPRANDOMITEM_T1811729489_H
#ifndef EQUIPITEMS_T650956023_H
#define EQUIPITEMS_T650956023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EquipItems
struct  EquipItems_t650956023  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32[] EquipItems::itemIDs
	Int32U5BU5D_t1965588061* ___itemIDs_2;

public:
	inline static int32_t get_offset_of_itemIDs_2() { return static_cast<int32_t>(offsetof(EquipItems_t650956023, ___itemIDs_2)); }
	inline Int32U5BU5D_t1965588061* get_itemIDs_2() const { return ___itemIDs_2; }
	inline Int32U5BU5D_t1965588061** get_address_of_itemIDs_2() { return &___itemIDs_2; }
	inline void set_itemIDs_2(Int32U5BU5D_t1965588061* value)
	{
		___itemIDs_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemIDs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUIPITEMS_T650956023_H
#ifndef NEXTSCREEN_T2084349028_H
#define NEXTSCREEN_T2084349028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NextScreen
struct  NextScreen_t2084349028  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean NextScreen::nextClicked
	bool ___nextClicked_2;

public:
	inline static int32_t get_offset_of_nextClicked_2() { return static_cast<int32_t>(offsetof(NextScreen_t2084349028, ___nextClicked_2)); }
	inline bool get_nextClicked_2() const { return ___nextClicked_2; }
	inline bool* get_address_of_nextClicked_2() { return &___nextClicked_2; }
	inline void set_nextClicked_2(bool value)
	{
		___nextClicked_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEXTSCREEN_T2084349028_H
#ifndef MULTICHOICEBOX_T1681099465_H
#define MULTICHOICEBOX_T1681099465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiChoiceBox
struct  MultiChoiceBox_t1681099465  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Button MultiChoiceBox::nextButton
	Button_t1293135404 * ___nextButton_2;
	// UnityEngine.UI.Button MultiChoiceBox::previousButton
	Button_t1293135404 * ___previousButton_3;
	// UnityEngine.UI.Text MultiChoiceBox::characterName
	Text_t1790657652 * ___characterName_4;
	// UnityEngine.UI.Text MultiChoiceBox::dialogueText
	Text_t1790657652 * ___dialogueText_5;
	// UnityEngine.UI.Text[] MultiChoiceBox::choices
	TextU5BU5D_t1489827645* ___choices_6;
	// UnityEngine.Color MultiChoiceBox::selectedColour
	Color_t2582018970  ___selectedColour_7;
	// UnityEngine.UI.Image MultiChoiceBox::dialogueImage
	Image_t2816987602 * ___dialogueImage_8;
	// System.Single MultiChoiceBox::choiceOffest
	float ___choiceOffest_9;
	// System.Boolean MultiChoiceBox::overrideSelfHide
	bool ___overrideSelfHide_10;
	// System.Boolean MultiChoiceBox::nextClicked
	bool ___nextClicked_11;
	// System.Boolean MultiChoiceBox::previousClicked
	bool ___previousClicked_12;
	// System.Boolean[] MultiChoiceBox::selectedChoices
	BooleanU5BU5D_t698278498* ___selectedChoices_13;
	// UnityEngine.Color MultiChoiceBox::normalChoiceColour
	Color_t2582018970  ___normalChoiceColour_14;
	// System.Int32 MultiChoiceBox::lastChosen
	int32_t ___lastChosen_15;

public:
	inline static int32_t get_offset_of_nextButton_2() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___nextButton_2)); }
	inline Button_t1293135404 * get_nextButton_2() const { return ___nextButton_2; }
	inline Button_t1293135404 ** get_address_of_nextButton_2() { return &___nextButton_2; }
	inline void set_nextButton_2(Button_t1293135404 * value)
	{
		___nextButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_2), value);
	}

	inline static int32_t get_offset_of_previousButton_3() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___previousButton_3)); }
	inline Button_t1293135404 * get_previousButton_3() const { return ___previousButton_3; }
	inline Button_t1293135404 ** get_address_of_previousButton_3() { return &___previousButton_3; }
	inline void set_previousButton_3(Button_t1293135404 * value)
	{
		___previousButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___previousButton_3), value);
	}

	inline static int32_t get_offset_of_characterName_4() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___characterName_4)); }
	inline Text_t1790657652 * get_characterName_4() const { return ___characterName_4; }
	inline Text_t1790657652 ** get_address_of_characterName_4() { return &___characterName_4; }
	inline void set_characterName_4(Text_t1790657652 * value)
	{
		___characterName_4 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_4), value);
	}

	inline static int32_t get_offset_of_dialogueText_5() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___dialogueText_5)); }
	inline Text_t1790657652 * get_dialogueText_5() const { return ___dialogueText_5; }
	inline Text_t1790657652 ** get_address_of_dialogueText_5() { return &___dialogueText_5; }
	inline void set_dialogueText_5(Text_t1790657652 * value)
	{
		___dialogueText_5 = value;
		Il2CppCodeGenWriteBarrier((&___dialogueText_5), value);
	}

	inline static int32_t get_offset_of_choices_6() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___choices_6)); }
	inline TextU5BU5D_t1489827645* get_choices_6() const { return ___choices_6; }
	inline TextU5BU5D_t1489827645** get_address_of_choices_6() { return &___choices_6; }
	inline void set_choices_6(TextU5BU5D_t1489827645* value)
	{
		___choices_6 = value;
		Il2CppCodeGenWriteBarrier((&___choices_6), value);
	}

	inline static int32_t get_offset_of_selectedColour_7() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___selectedColour_7)); }
	inline Color_t2582018970  get_selectedColour_7() const { return ___selectedColour_7; }
	inline Color_t2582018970 * get_address_of_selectedColour_7() { return &___selectedColour_7; }
	inline void set_selectedColour_7(Color_t2582018970  value)
	{
		___selectedColour_7 = value;
	}

	inline static int32_t get_offset_of_dialogueImage_8() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___dialogueImage_8)); }
	inline Image_t2816987602 * get_dialogueImage_8() const { return ___dialogueImage_8; }
	inline Image_t2816987602 ** get_address_of_dialogueImage_8() { return &___dialogueImage_8; }
	inline void set_dialogueImage_8(Image_t2816987602 * value)
	{
		___dialogueImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___dialogueImage_8), value);
	}

	inline static int32_t get_offset_of_choiceOffest_9() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___choiceOffest_9)); }
	inline float get_choiceOffest_9() const { return ___choiceOffest_9; }
	inline float* get_address_of_choiceOffest_9() { return &___choiceOffest_9; }
	inline void set_choiceOffest_9(float value)
	{
		___choiceOffest_9 = value;
	}

	inline static int32_t get_offset_of_overrideSelfHide_10() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___overrideSelfHide_10)); }
	inline bool get_overrideSelfHide_10() const { return ___overrideSelfHide_10; }
	inline bool* get_address_of_overrideSelfHide_10() { return &___overrideSelfHide_10; }
	inline void set_overrideSelfHide_10(bool value)
	{
		___overrideSelfHide_10 = value;
	}

	inline static int32_t get_offset_of_nextClicked_11() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___nextClicked_11)); }
	inline bool get_nextClicked_11() const { return ___nextClicked_11; }
	inline bool* get_address_of_nextClicked_11() { return &___nextClicked_11; }
	inline void set_nextClicked_11(bool value)
	{
		___nextClicked_11 = value;
	}

	inline static int32_t get_offset_of_previousClicked_12() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___previousClicked_12)); }
	inline bool get_previousClicked_12() const { return ___previousClicked_12; }
	inline bool* get_address_of_previousClicked_12() { return &___previousClicked_12; }
	inline void set_previousClicked_12(bool value)
	{
		___previousClicked_12 = value;
	}

	inline static int32_t get_offset_of_selectedChoices_13() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___selectedChoices_13)); }
	inline BooleanU5BU5D_t698278498* get_selectedChoices_13() const { return ___selectedChoices_13; }
	inline BooleanU5BU5D_t698278498** get_address_of_selectedChoices_13() { return &___selectedChoices_13; }
	inline void set_selectedChoices_13(BooleanU5BU5D_t698278498* value)
	{
		___selectedChoices_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectedChoices_13), value);
	}

	inline static int32_t get_offset_of_normalChoiceColour_14() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___normalChoiceColour_14)); }
	inline Color_t2582018970  get_normalChoiceColour_14() const { return ___normalChoiceColour_14; }
	inline Color_t2582018970 * get_address_of_normalChoiceColour_14() { return &___normalChoiceColour_14; }
	inline void set_normalChoiceColour_14(Color_t2582018970  value)
	{
		___normalChoiceColour_14 = value;
	}

	inline static int32_t get_offset_of_lastChosen_15() { return static_cast<int32_t>(offsetof(MultiChoiceBox_t1681099465, ___lastChosen_15)); }
	inline int32_t get_lastChosen_15() const { return ___lastChosen_15; }
	inline int32_t* get_address_of_lastChosen_15() { return &___lastChosen_15; }
	inline void set_lastChosen_15(int32_t value)
	{
		___lastChosen_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICHOICEBOX_T1681099465_H
#ifndef MOODGRAPH_T2484840849_H
#define MOODGRAPH_T2484840849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoodGraph
struct  MoodGraph_t2484840849  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.RectTransform[] MoodGraph::bars
	RectTransformU5BU5D_t602728325* ___bars_2;

public:
	inline static int32_t get_offset_of_bars_2() { return static_cast<int32_t>(offsetof(MoodGraph_t2484840849, ___bars_2)); }
	inline RectTransformU5BU5D_t602728325* get_bars_2() const { return ___bars_2; }
	inline RectTransformU5BU5D_t602728325** get_address_of_bars_2() { return &___bars_2; }
	inline void set_bars_2(RectTransformU5BU5D_t602728325* value)
	{
		___bars_2 = value;
		Il2CppCodeGenWriteBarrier((&___bars_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOODGRAPH_T2484840849_H
#ifndef MOBILECONTROLENABLER_T4087415719_H
#define MOBILECONTROLENABLER_T4087415719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileControlEnabler
struct  MobileControlEnabler_t4087415719  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Image MobileControlEnabler::moveImage
	Image_t2816987602 * ___moveImage_2;
	// UnityEngine.UI.Image MobileControlEnabler::jumpImage
	Image_t2816987602 * ___jumpImage_3;

public:
	inline static int32_t get_offset_of_moveImage_2() { return static_cast<int32_t>(offsetof(MobileControlEnabler_t4087415719, ___moveImage_2)); }
	inline Image_t2816987602 * get_moveImage_2() const { return ___moveImage_2; }
	inline Image_t2816987602 ** get_address_of_moveImage_2() { return &___moveImage_2; }
	inline void set_moveImage_2(Image_t2816987602 * value)
	{
		___moveImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveImage_2), value);
	}

	inline static int32_t get_offset_of_jumpImage_3() { return static_cast<int32_t>(offsetof(MobileControlEnabler_t4087415719, ___jumpImage_3)); }
	inline Image_t2816987602 * get_jumpImage_3() const { return ___jumpImage_3; }
	inline Image_t2816987602 ** get_address_of_jumpImage_3() { return &___jumpImage_3; }
	inline void set_jumpImage_3(Image_t2816987602 * value)
	{
		___jumpImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___jumpImage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLENABLER_T4087415719_H
#ifndef MATCHMULTIPLELESSONS_T1564823048_H
#define MATCHMULTIPLELESSONS_T1564823048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchMultipleLessons
struct  MatchMultipleLessons_t1564823048  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject MatchMultipleLessons::sentencePrefab
	GameObject_t2557347079 * ___sentencePrefab_2;
	// System.Single MatchMultipleLessons::gap
	float ___gap_3;
	// UnityEngine.Transform MatchMultipleLessons::leftSpawner
	Transform_t362059596 * ___leftSpawner_4;
	// UnityEngine.Transform MatchMultipleLessons::rightSpawner
	Transform_t362059596 * ___rightSpawner_5;
	// UnityEngine.Transform MatchMultipleLessons::howDoesItRelate
	Transform_t362059596 * ___howDoesItRelate_6;
	// UnityEngine.Transform MatchMultipleLessons::whatsTheMessage
	Transform_t362059596 * ___whatsTheMessage_7;
	// UnityEngine.UI.Text MatchMultipleLessons::questionText
	Text_t1790657652 * ___questionText_8;
	// UnityEngine.UI.Button MatchMultipleLessons::nextButton
	Button_t1293135404 * ___nextButton_9;
	// System.Collections.Generic.List`1<DragableInformation> MatchMultipleLessons::draggables
	List_1_t1390258121 * ___draggables_10;
	// System.String[] MatchMultipleLessons::questions
	StringU5BU5D_t2511808107* ___questions_11;
	// System.Single MatchMultipleLessons::snapDistance
	float ___snapDistance_12;
	// System.Boolean MatchMultipleLessons::bFinished
	bool ___bFinished_13;
	// System.Int32 MatchMultipleLessons::numAnswersIndex
	int32_t ___numAnswersIndex_14;
	// System.Int32 MatchMultipleLessons::currentAnswerIndex
	int32_t ___currentAnswerIndex_15;
	// System.Boolean MatchMultipleLessons::overrideDone
	bool ___overrideDone_16;

public:
	inline static int32_t get_offset_of_sentencePrefab_2() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___sentencePrefab_2)); }
	inline GameObject_t2557347079 * get_sentencePrefab_2() const { return ___sentencePrefab_2; }
	inline GameObject_t2557347079 ** get_address_of_sentencePrefab_2() { return &___sentencePrefab_2; }
	inline void set_sentencePrefab_2(GameObject_t2557347079 * value)
	{
		___sentencePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___sentencePrefab_2), value);
	}

	inline static int32_t get_offset_of_gap_3() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___gap_3)); }
	inline float get_gap_3() const { return ___gap_3; }
	inline float* get_address_of_gap_3() { return &___gap_3; }
	inline void set_gap_3(float value)
	{
		___gap_3 = value;
	}

	inline static int32_t get_offset_of_leftSpawner_4() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___leftSpawner_4)); }
	inline Transform_t362059596 * get_leftSpawner_4() const { return ___leftSpawner_4; }
	inline Transform_t362059596 ** get_address_of_leftSpawner_4() { return &___leftSpawner_4; }
	inline void set_leftSpawner_4(Transform_t362059596 * value)
	{
		___leftSpawner_4 = value;
		Il2CppCodeGenWriteBarrier((&___leftSpawner_4), value);
	}

	inline static int32_t get_offset_of_rightSpawner_5() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___rightSpawner_5)); }
	inline Transform_t362059596 * get_rightSpawner_5() const { return ___rightSpawner_5; }
	inline Transform_t362059596 ** get_address_of_rightSpawner_5() { return &___rightSpawner_5; }
	inline void set_rightSpawner_5(Transform_t362059596 * value)
	{
		___rightSpawner_5 = value;
		Il2CppCodeGenWriteBarrier((&___rightSpawner_5), value);
	}

	inline static int32_t get_offset_of_howDoesItRelate_6() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___howDoesItRelate_6)); }
	inline Transform_t362059596 * get_howDoesItRelate_6() const { return ___howDoesItRelate_6; }
	inline Transform_t362059596 ** get_address_of_howDoesItRelate_6() { return &___howDoesItRelate_6; }
	inline void set_howDoesItRelate_6(Transform_t362059596 * value)
	{
		___howDoesItRelate_6 = value;
		Il2CppCodeGenWriteBarrier((&___howDoesItRelate_6), value);
	}

	inline static int32_t get_offset_of_whatsTheMessage_7() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___whatsTheMessage_7)); }
	inline Transform_t362059596 * get_whatsTheMessage_7() const { return ___whatsTheMessage_7; }
	inline Transform_t362059596 ** get_address_of_whatsTheMessage_7() { return &___whatsTheMessage_7; }
	inline void set_whatsTheMessage_7(Transform_t362059596 * value)
	{
		___whatsTheMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___whatsTheMessage_7), value);
	}

	inline static int32_t get_offset_of_questionText_8() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___questionText_8)); }
	inline Text_t1790657652 * get_questionText_8() const { return ___questionText_8; }
	inline Text_t1790657652 ** get_address_of_questionText_8() { return &___questionText_8; }
	inline void set_questionText_8(Text_t1790657652 * value)
	{
		___questionText_8 = value;
		Il2CppCodeGenWriteBarrier((&___questionText_8), value);
	}

	inline static int32_t get_offset_of_nextButton_9() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___nextButton_9)); }
	inline Button_t1293135404 * get_nextButton_9() const { return ___nextButton_9; }
	inline Button_t1293135404 ** get_address_of_nextButton_9() { return &___nextButton_9; }
	inline void set_nextButton_9(Button_t1293135404 * value)
	{
		___nextButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_9), value);
	}

	inline static int32_t get_offset_of_draggables_10() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___draggables_10)); }
	inline List_1_t1390258121 * get_draggables_10() const { return ___draggables_10; }
	inline List_1_t1390258121 ** get_address_of_draggables_10() { return &___draggables_10; }
	inline void set_draggables_10(List_1_t1390258121 * value)
	{
		___draggables_10 = value;
		Il2CppCodeGenWriteBarrier((&___draggables_10), value);
	}

	inline static int32_t get_offset_of_questions_11() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___questions_11)); }
	inline StringU5BU5D_t2511808107* get_questions_11() const { return ___questions_11; }
	inline StringU5BU5D_t2511808107** get_address_of_questions_11() { return &___questions_11; }
	inline void set_questions_11(StringU5BU5D_t2511808107* value)
	{
		___questions_11 = value;
		Il2CppCodeGenWriteBarrier((&___questions_11), value);
	}

	inline static int32_t get_offset_of_snapDistance_12() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___snapDistance_12)); }
	inline float get_snapDistance_12() const { return ___snapDistance_12; }
	inline float* get_address_of_snapDistance_12() { return &___snapDistance_12; }
	inline void set_snapDistance_12(float value)
	{
		___snapDistance_12 = value;
	}

	inline static int32_t get_offset_of_bFinished_13() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___bFinished_13)); }
	inline bool get_bFinished_13() const { return ___bFinished_13; }
	inline bool* get_address_of_bFinished_13() { return &___bFinished_13; }
	inline void set_bFinished_13(bool value)
	{
		___bFinished_13 = value;
	}

	inline static int32_t get_offset_of_numAnswersIndex_14() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___numAnswersIndex_14)); }
	inline int32_t get_numAnswersIndex_14() const { return ___numAnswersIndex_14; }
	inline int32_t* get_address_of_numAnswersIndex_14() { return &___numAnswersIndex_14; }
	inline void set_numAnswersIndex_14(int32_t value)
	{
		___numAnswersIndex_14 = value;
	}

	inline static int32_t get_offset_of_currentAnswerIndex_15() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___currentAnswerIndex_15)); }
	inline int32_t get_currentAnswerIndex_15() const { return ___currentAnswerIndex_15; }
	inline int32_t* get_address_of_currentAnswerIndex_15() { return &___currentAnswerIndex_15; }
	inline void set_currentAnswerIndex_15(int32_t value)
	{
		___currentAnswerIndex_15 = value;
	}

	inline static int32_t get_offset_of_overrideDone_16() { return static_cast<int32_t>(offsetof(MatchMultipleLessons_t1564823048, ___overrideDone_16)); }
	inline bool get_overrideDone_16() const { return ___overrideDone_16; }
	inline bool* get_address_of_overrideDone_16() { return &___overrideDone_16; }
	inline void set_overrideDone_16(bool value)
	{
		___overrideDone_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHMULTIPLELESSONS_T1564823048_H
#ifndef MATCHGNATSSCREEN_T2040488255_H
#define MATCHGNATSSCREEN_T2040488255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchGnatsScreen
struct  MatchGnatsScreen_t2040488255  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.RectTransform[] MatchGnatsScreen::snapPoints
	RectTransformU5BU5D_t602728325* ___snapPoints_2;
	// DragObject[] MatchGnatsScreen::dragAndDrops
	DragObjectU5BU5D_t3683517183* ___dragAndDrops_3;
	// UnityEngine.UI.Text[] MatchGnatsScreen::descriptions
	TextU5BU5D_t1489827645* ___descriptions_4;
	// UnityEngine.UI.Button MatchGnatsScreen::nextButton
	Button_t1293135404 * ___nextButton_5;
	// System.Single MatchGnatsScreen::snapDistance
	float ___snapDistance_6;
	// System.Boolean MatchGnatsScreen::isFinished
	bool ___isFinished_7;
	// System.Boolean MatchGnatsScreen::overrideFinished
	bool ___overrideFinished_8;

public:
	inline static int32_t get_offset_of_snapPoints_2() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___snapPoints_2)); }
	inline RectTransformU5BU5D_t602728325* get_snapPoints_2() const { return ___snapPoints_2; }
	inline RectTransformU5BU5D_t602728325** get_address_of_snapPoints_2() { return &___snapPoints_2; }
	inline void set_snapPoints_2(RectTransformU5BU5D_t602728325* value)
	{
		___snapPoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___snapPoints_2), value);
	}

	inline static int32_t get_offset_of_dragAndDrops_3() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___dragAndDrops_3)); }
	inline DragObjectU5BU5D_t3683517183* get_dragAndDrops_3() const { return ___dragAndDrops_3; }
	inline DragObjectU5BU5D_t3683517183** get_address_of_dragAndDrops_3() { return &___dragAndDrops_3; }
	inline void set_dragAndDrops_3(DragObjectU5BU5D_t3683517183* value)
	{
		___dragAndDrops_3 = value;
		Il2CppCodeGenWriteBarrier((&___dragAndDrops_3), value);
	}

	inline static int32_t get_offset_of_descriptions_4() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___descriptions_4)); }
	inline TextU5BU5D_t1489827645* get_descriptions_4() const { return ___descriptions_4; }
	inline TextU5BU5D_t1489827645** get_address_of_descriptions_4() { return &___descriptions_4; }
	inline void set_descriptions_4(TextU5BU5D_t1489827645* value)
	{
		___descriptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_4), value);
	}

	inline static int32_t get_offset_of_nextButton_5() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___nextButton_5)); }
	inline Button_t1293135404 * get_nextButton_5() const { return ___nextButton_5; }
	inline Button_t1293135404 ** get_address_of_nextButton_5() { return &___nextButton_5; }
	inline void set_nextButton_5(Button_t1293135404 * value)
	{
		___nextButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_5), value);
	}

	inline static int32_t get_offset_of_snapDistance_6() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___snapDistance_6)); }
	inline float get_snapDistance_6() const { return ___snapDistance_6; }
	inline float* get_address_of_snapDistance_6() { return &___snapDistance_6; }
	inline void set_snapDistance_6(float value)
	{
		___snapDistance_6 = value;
	}

	inline static int32_t get_offset_of_isFinished_7() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___isFinished_7)); }
	inline bool get_isFinished_7() const { return ___isFinished_7; }
	inline bool* get_address_of_isFinished_7() { return &___isFinished_7; }
	inline void set_isFinished_7(bool value)
	{
		___isFinished_7 = value;
	}

	inline static int32_t get_offset_of_overrideFinished_8() { return static_cast<int32_t>(offsetof(MatchGnatsScreen_t2040488255, ___overrideFinished_8)); }
	inline bool get_overrideFinished_8() const { return ___overrideFinished_8; }
	inline bool* get_address_of_overrideFinished_8() { return &___overrideFinished_8; }
	inline void set_overrideFinished_8(bool value)
	{
		___overrideFinished_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHGNATSSCREEN_T2040488255_H
#ifndef MAPCANVAS_T3396692108_H
#define MAPCANVAS_T3396692108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCanvas
struct  MapCanvas_t3396692108  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject MapCanvas::map
	GameObject_t2557347079 * ___map_2;
	// UnityEngine.UI.Button MapCanvas::mapButton
	Button_t1293135404 * ___mapButton_3;
	// UnityEngine.GameObject[] MapCanvas::hideWhenCantMove
	GameObjectU5BU5D_t2988620542* ___hideWhenCantMove_4;

public:
	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(MapCanvas_t3396692108, ___map_2)); }
	inline GameObject_t2557347079 * get_map_2() const { return ___map_2; }
	inline GameObject_t2557347079 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(GameObject_t2557347079 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_mapButton_3() { return static_cast<int32_t>(offsetof(MapCanvas_t3396692108, ___mapButton_3)); }
	inline Button_t1293135404 * get_mapButton_3() const { return ___mapButton_3; }
	inline Button_t1293135404 ** get_address_of_mapButton_3() { return &___mapButton_3; }
	inline void set_mapButton_3(Button_t1293135404 * value)
	{
		___mapButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___mapButton_3), value);
	}

	inline static int32_t get_offset_of_hideWhenCantMove_4() { return static_cast<int32_t>(offsetof(MapCanvas_t3396692108, ___hideWhenCantMove_4)); }
	inline GameObjectU5BU5D_t2988620542* get_hideWhenCantMove_4() const { return ___hideWhenCantMove_4; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_hideWhenCantMove_4() { return &___hideWhenCantMove_4; }
	inline void set_hideWhenCantMove_4(GameObjectU5BU5D_t2988620542* value)
	{
		___hideWhenCantMove_4 = value;
		Il2CppCodeGenWriteBarrier((&___hideWhenCantMove_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPCANVAS_T3396692108_H
#ifndef UICHECKBOXCONTROLLEDCOMPONENT_T2988583793_H
#define UICHECKBOXCONTROLLEDCOMPONENT_T2988583793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICheckboxControlledComponent
struct  UICheckboxControlledComponent_t2988583793  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.MonoBehaviour UICheckboxControlledComponent::target
	MonoBehaviour_t1618594486 * ___target_2;
	// System.Boolean UICheckboxControlledComponent::inverse
	bool ___inverse_3;
	// System.Boolean UICheckboxControlledComponent::mUsingDelegates
	bool ___mUsingDelegates_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UICheckboxControlledComponent_t2988583793, ___target_2)); }
	inline MonoBehaviour_t1618594486 * get_target_2() const { return ___target_2; }
	inline MonoBehaviour_t1618594486 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(MonoBehaviour_t1618594486 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_inverse_3() { return static_cast<int32_t>(offsetof(UICheckboxControlledComponent_t2988583793, ___inverse_3)); }
	inline bool get_inverse_3() const { return ___inverse_3; }
	inline bool* get_address_of_inverse_3() { return &___inverse_3; }
	inline void set_inverse_3(bool value)
	{
		___inverse_3 = value;
	}

	inline static int32_t get_offset_of_mUsingDelegates_4() { return static_cast<int32_t>(offsetof(UICheckboxControlledComponent_t2988583793, ___mUsingDelegates_4)); }
	inline bool get_mUsingDelegates_4() const { return ___mUsingDelegates_4; }
	inline bool* get_address_of_mUsingDelegates_4() { return &___mUsingDelegates_4; }
	inline void set_mUsingDelegates_4(bool value)
	{
		___mUsingDelegates_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHECKBOXCONTROLLEDCOMPONENT_T2988583793_H
#ifndef LOOKATTARGET_T3176889829_H
#define LOOKATTARGET_T3176889829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LookAtTarget
struct  LookAtTarget_t3176889829  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 LookAtTarget::level
	int32_t ___level_2;
	// UnityEngine.Transform LookAtTarget::target
	Transform_t362059596 * ___target_3;
	// System.Single LookAtTarget::speed
	float ___speed_4;
	// UnityEngine.Transform LookAtTarget::mTrans
	Transform_t362059596 * ___mTrans_5;

public:
	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(LookAtTarget_t3176889829, ___level_2)); }
	inline int32_t get_level_2() const { return ___level_2; }
	inline int32_t* get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(int32_t value)
	{
		___level_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(LookAtTarget_t3176889829, ___target_3)); }
	inline Transform_t362059596 * get_target_3() const { return ___target_3; }
	inline Transform_t362059596 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t362059596 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(LookAtTarget_t3176889829, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(LookAtTarget_t3176889829, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKATTARGET_T3176889829_H
#ifndef MAP_T3311971743_H
#define MAP_T3311971743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Map
struct  Map_t3311971743  : public MonoBehaviour_t1618594486
{
public:
	// Province[] Map::provinces
	ProvinceU5BU5D_t4026361422* ___provinces_2;
	// System.Single Map::lockedAlpha
	float ___lockedAlpha_3;
	// UnityEngine.GameObject Map::controls
	GameObject_t2557347079 * ___controls_4;

public:
	inline static int32_t get_offset_of_provinces_2() { return static_cast<int32_t>(offsetof(Map_t3311971743, ___provinces_2)); }
	inline ProvinceU5BU5D_t4026361422* get_provinces_2() const { return ___provinces_2; }
	inline ProvinceU5BU5D_t4026361422** get_address_of_provinces_2() { return &___provinces_2; }
	inline void set_provinces_2(ProvinceU5BU5D_t4026361422* value)
	{
		___provinces_2 = value;
		Il2CppCodeGenWriteBarrier((&___provinces_2), value);
	}

	inline static int32_t get_offset_of_lockedAlpha_3() { return static_cast<int32_t>(offsetof(Map_t3311971743, ___lockedAlpha_3)); }
	inline float get_lockedAlpha_3() const { return ___lockedAlpha_3; }
	inline float* get_address_of_lockedAlpha_3() { return &___lockedAlpha_3; }
	inline void set_lockedAlpha_3(float value)
	{
		___lockedAlpha_3 = value;
	}

	inline static int32_t get_offset_of_controls_4() { return static_cast<int32_t>(offsetof(Map_t3311971743, ___controls_4)); }
	inline GameObject_t2557347079 * get_controls_4() const { return ___controls_4; }
	inline GameObject_t2557347079 ** get_address_of_controls_4() { return &___controls_4; }
	inline void set_controls_4(GameObject_t2557347079 * value)
	{
		___controls_4 = value;
		Il2CppCodeGenWriteBarrier((&___controls_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAP_T3311971743_H
#ifndef PLAYIDLEANIMATIONS_T667263987_H
#define PLAYIDLEANIMATIONS_T667263987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayIdleAnimations
struct  PlayIdleAnimations_t667263987  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Animation PlayIdleAnimations::mAnim
	Animation_t3821138400 * ___mAnim_2;
	// UnityEngine.AnimationClip PlayIdleAnimations::mIdle
	AnimationClip_t1145879031 * ___mIdle_3;
	// System.Collections.Generic.List`1<UnityEngine.AnimationClip> PlayIdleAnimations::mBreaks
	List_1_t265506038 * ___mBreaks_4;
	// System.Single PlayIdleAnimations::mNextBreak
	float ___mNextBreak_5;
	// System.Int32 PlayIdleAnimations::mLastIndex
	int32_t ___mLastIndex_6;

public:
	inline static int32_t get_offset_of_mAnim_2() { return static_cast<int32_t>(offsetof(PlayIdleAnimations_t667263987, ___mAnim_2)); }
	inline Animation_t3821138400 * get_mAnim_2() const { return ___mAnim_2; }
	inline Animation_t3821138400 ** get_address_of_mAnim_2() { return &___mAnim_2; }
	inline void set_mAnim_2(Animation_t3821138400 * value)
	{
		___mAnim_2 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_2), value);
	}

	inline static int32_t get_offset_of_mIdle_3() { return static_cast<int32_t>(offsetof(PlayIdleAnimations_t667263987, ___mIdle_3)); }
	inline AnimationClip_t1145879031 * get_mIdle_3() const { return ___mIdle_3; }
	inline AnimationClip_t1145879031 ** get_address_of_mIdle_3() { return &___mIdle_3; }
	inline void set_mIdle_3(AnimationClip_t1145879031 * value)
	{
		___mIdle_3 = value;
		Il2CppCodeGenWriteBarrier((&___mIdle_3), value);
	}

	inline static int32_t get_offset_of_mBreaks_4() { return static_cast<int32_t>(offsetof(PlayIdleAnimations_t667263987, ___mBreaks_4)); }
	inline List_1_t265506038 * get_mBreaks_4() const { return ___mBreaks_4; }
	inline List_1_t265506038 ** get_address_of_mBreaks_4() { return &___mBreaks_4; }
	inline void set_mBreaks_4(List_1_t265506038 * value)
	{
		___mBreaks_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBreaks_4), value);
	}

	inline static int32_t get_offset_of_mNextBreak_5() { return static_cast<int32_t>(offsetof(PlayIdleAnimations_t667263987, ___mNextBreak_5)); }
	inline float get_mNextBreak_5() const { return ___mNextBreak_5; }
	inline float* get_address_of_mNextBreak_5() { return &___mNextBreak_5; }
	inline void set_mNextBreak_5(float value)
	{
		___mNextBreak_5 = value;
	}

	inline static int32_t get_offset_of_mLastIndex_6() { return static_cast<int32_t>(offsetof(PlayIdleAnimations_t667263987, ___mLastIndex_6)); }
	inline int32_t get_mLastIndex_6() const { return ___mLastIndex_6; }
	inline int32_t* get_address_of_mLastIndex_6() { return &___mLastIndex_6; }
	inline void set_mLastIndex_6(int32_t value)
	{
		___mLastIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYIDLEANIMATIONS_T667263987_H
#ifndef UIBUTTONPLAYANIMATION_T2818799863_H
#define UIBUTTONPLAYANIMATION_T2818799863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonPlayAnimation
struct  UIButtonPlayAnimation_t2818799863  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Animation UIButtonPlayAnimation::target
	Animation_t3821138400 * ___target_2;
	// System.String UIButtonPlayAnimation::clipName
	String_t* ___clipName_3;
	// AnimationOrTween.Trigger UIButtonPlayAnimation::trigger
	int32_t ___trigger_4;
	// AnimationOrTween.Direction UIButtonPlayAnimation::playDirection
	int32_t ___playDirection_5;
	// System.Boolean UIButtonPlayAnimation::resetOnPlay
	bool ___resetOnPlay_6;
	// System.Boolean UIButtonPlayAnimation::clearSelection
	bool ___clearSelection_7;
	// AnimationOrTween.EnableCondition UIButtonPlayAnimation::ifDisabledOnPlay
	int32_t ___ifDisabledOnPlay_8;
	// AnimationOrTween.DisableCondition UIButtonPlayAnimation::disableWhenFinished
	int32_t ___disableWhenFinished_9;
	// UnityEngine.GameObject UIButtonPlayAnimation::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_10;
	// System.String UIButtonPlayAnimation::callWhenFinished
	String_t* ___callWhenFinished_11;
	// ActiveAnimation/OnFinished UIButtonPlayAnimation::onFinished
	OnFinished_t1828027647 * ___onFinished_12;
	// System.Boolean UIButtonPlayAnimation::mStarted
	bool ___mStarted_13;
	// System.Boolean UIButtonPlayAnimation::mHighlighted
	bool ___mHighlighted_14;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___target_2)); }
	inline Animation_t3821138400 * get_target_2() const { return ___target_2; }
	inline Animation_t3821138400 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Animation_t3821138400 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_clipName_3() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___clipName_3)); }
	inline String_t* get_clipName_3() const { return ___clipName_3; }
	inline String_t** get_address_of_clipName_3() { return &___clipName_3; }
	inline void set_clipName_3(String_t* value)
	{
		___clipName_3 = value;
		Il2CppCodeGenWriteBarrier((&___clipName_3), value);
	}

	inline static int32_t get_offset_of_trigger_4() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___trigger_4)); }
	inline int32_t get_trigger_4() const { return ___trigger_4; }
	inline int32_t* get_address_of_trigger_4() { return &___trigger_4; }
	inline void set_trigger_4(int32_t value)
	{
		___trigger_4 = value;
	}

	inline static int32_t get_offset_of_playDirection_5() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___playDirection_5)); }
	inline int32_t get_playDirection_5() const { return ___playDirection_5; }
	inline int32_t* get_address_of_playDirection_5() { return &___playDirection_5; }
	inline void set_playDirection_5(int32_t value)
	{
		___playDirection_5 = value;
	}

	inline static int32_t get_offset_of_resetOnPlay_6() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___resetOnPlay_6)); }
	inline bool get_resetOnPlay_6() const { return ___resetOnPlay_6; }
	inline bool* get_address_of_resetOnPlay_6() { return &___resetOnPlay_6; }
	inline void set_resetOnPlay_6(bool value)
	{
		___resetOnPlay_6 = value;
	}

	inline static int32_t get_offset_of_clearSelection_7() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___clearSelection_7)); }
	inline bool get_clearSelection_7() const { return ___clearSelection_7; }
	inline bool* get_address_of_clearSelection_7() { return &___clearSelection_7; }
	inline void set_clearSelection_7(bool value)
	{
		___clearSelection_7 = value;
	}

	inline static int32_t get_offset_of_ifDisabledOnPlay_8() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___ifDisabledOnPlay_8)); }
	inline int32_t get_ifDisabledOnPlay_8() const { return ___ifDisabledOnPlay_8; }
	inline int32_t* get_address_of_ifDisabledOnPlay_8() { return &___ifDisabledOnPlay_8; }
	inline void set_ifDisabledOnPlay_8(int32_t value)
	{
		___ifDisabledOnPlay_8 = value;
	}

	inline static int32_t get_offset_of_disableWhenFinished_9() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___disableWhenFinished_9)); }
	inline int32_t get_disableWhenFinished_9() const { return ___disableWhenFinished_9; }
	inline int32_t* get_address_of_disableWhenFinished_9() { return &___disableWhenFinished_9; }
	inline void set_disableWhenFinished_9(int32_t value)
	{
		___disableWhenFinished_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_10() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___eventReceiver_10)); }
	inline GameObject_t2557347079 * get_eventReceiver_10() const { return ___eventReceiver_10; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_10() { return &___eventReceiver_10; }
	inline void set_eventReceiver_10(GameObject_t2557347079 * value)
	{
		___eventReceiver_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_10), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_11() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___callWhenFinished_11)); }
	inline String_t* get_callWhenFinished_11() const { return ___callWhenFinished_11; }
	inline String_t** get_address_of_callWhenFinished_11() { return &___callWhenFinished_11; }
	inline void set_callWhenFinished_11(String_t* value)
	{
		___callWhenFinished_11 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_11), value);
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___onFinished_12)); }
	inline OnFinished_t1828027647 * get_onFinished_12() const { return ___onFinished_12; }
	inline OnFinished_t1828027647 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(OnFinished_t1828027647 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_mStarted_13() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___mStarted_13)); }
	inline bool get_mStarted_13() const { return ___mStarted_13; }
	inline bool* get_address_of_mStarted_13() { return &___mStarted_13; }
	inline void set_mStarted_13(bool value)
	{
		___mStarted_13 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_14() { return static_cast<int32_t>(offsetof(UIButtonPlayAnimation_t2818799863, ___mHighlighted_14)); }
	inline bool get_mHighlighted_14() const { return ___mHighlighted_14; }
	inline bool* get_address_of_mHighlighted_14() { return &___mHighlighted_14; }
	inline void set_mHighlighted_14(bool value)
	{
		___mHighlighted_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONPLAYANIMATION_T2818799863_H
#ifndef UIBUTTONOFFSET_T584960962_H
#define UIBUTTONOFFSET_T584960962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonOffset
struct  UIButtonOffset_t584960962  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform UIButtonOffset::tweenTarget
	Transform_t362059596 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonOffset::hover
	Vector3_t1986933152  ___hover_3;
	// UnityEngine.Vector3 UIButtonOffset::pressed
	Vector3_t1986933152  ___pressed_4;
	// System.Single UIButtonOffset::duration
	float ___duration_5;
	// UnityEngine.Vector3 UIButtonOffset::mPos
	Vector3_t1986933152  ___mPos_6;
	// System.Boolean UIButtonOffset::mInitDone
	bool ___mInitDone_7;
	// System.Boolean UIButtonOffset::mStarted
	bool ___mStarted_8;
	// System.Boolean UIButtonOffset::mHighlighted
	bool ___mHighlighted_9;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___tweenTarget_2)); }
	inline Transform_t362059596 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t362059596 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t362059596 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___hover_3)); }
	inline Vector3_t1986933152  get_hover_3() const { return ___hover_3; }
	inline Vector3_t1986933152 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t1986933152  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___pressed_4)); }
	inline Vector3_t1986933152  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t1986933152 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t1986933152  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mPos_6() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___mPos_6)); }
	inline Vector3_t1986933152  get_mPos_6() const { return ___mPos_6; }
	inline Vector3_t1986933152 * get_address_of_mPos_6() { return &___mPos_6; }
	inline void set_mPos_6(Vector3_t1986933152  value)
	{
		___mPos_6 = value;
	}

	inline static int32_t get_offset_of_mInitDone_7() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___mInitDone_7)); }
	inline bool get_mInitDone_7() const { return ___mInitDone_7; }
	inline bool* get_address_of_mInitDone_7() { return &___mInitDone_7; }
	inline void set_mInitDone_7(bool value)
	{
		___mInitDone_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_9() { return static_cast<int32_t>(offsetof(UIButtonOffset_t584960962, ___mHighlighted_9)); }
	inline bool get_mHighlighted_9() const { return ___mHighlighted_9; }
	inline bool* get_address_of_mHighlighted_9() { return &___mHighlighted_9; }
	inline void set_mHighlighted_9(bool value)
	{
		___mHighlighted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONOFFSET_T584960962_H
#ifndef UIBUTTONSOUND_T3002556785_H
#define UIBUTTONSOUND_T3002556785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonSound
struct  UIButtonSound_t3002556785  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.AudioClip UIButtonSound::audioClip
	AudioClip_t1419814565 * ___audioClip_2;
	// UIButtonSound/Trigger UIButtonSound::trigger
	int32_t ___trigger_3;
	// System.Single UIButtonSound::volume
	float ___volume_4;
	// System.Single UIButtonSound::pitch
	float ___pitch_5;

public:
	inline static int32_t get_offset_of_audioClip_2() { return static_cast<int32_t>(offsetof(UIButtonSound_t3002556785, ___audioClip_2)); }
	inline AudioClip_t1419814565 * get_audioClip_2() const { return ___audioClip_2; }
	inline AudioClip_t1419814565 ** get_address_of_audioClip_2() { return &___audioClip_2; }
	inline void set_audioClip_2(AudioClip_t1419814565 * value)
	{
		___audioClip_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_2), value);
	}

	inline static int32_t get_offset_of_trigger_3() { return static_cast<int32_t>(offsetof(UIButtonSound_t3002556785, ___trigger_3)); }
	inline int32_t get_trigger_3() const { return ___trigger_3; }
	inline int32_t* get_address_of_trigger_3() { return &___trigger_3; }
	inline void set_trigger_3(int32_t value)
	{
		___trigger_3 = value;
	}

	inline static int32_t get_offset_of_volume_4() { return static_cast<int32_t>(offsetof(UIButtonSound_t3002556785, ___volume_4)); }
	inline float get_volume_4() const { return ___volume_4; }
	inline float* get_address_of_volume_4() { return &___volume_4; }
	inline void set_volume_4(float value)
	{
		___volume_4 = value;
	}

	inline static int32_t get_offset_of_pitch_5() { return static_cast<int32_t>(offsetof(UIButtonSound_t3002556785, ___pitch_5)); }
	inline float get_pitch_5() const { return ___pitch_5; }
	inline float* get_address_of_pitch_5() { return &___pitch_5; }
	inline void set_pitch_5(float value)
	{
		___pitch_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONSOUND_T3002556785_H
#ifndef UIBUTTONMESSAGE_T897303282_H
#define UIBUTTONMESSAGE_T897303282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonMessage
struct  UIButtonMessage_t897303282  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UIButtonMessage::target
	GameObject_t2557347079 * ___target_2;
	// System.String UIButtonMessage::functionName
	String_t* ___functionName_3;
	// UIButtonMessage/Trigger UIButtonMessage::trigger
	int32_t ___trigger_4;
	// System.Boolean UIButtonMessage::includeChildren
	bool ___includeChildren_5;
	// System.Boolean UIButtonMessage::mStarted
	bool ___mStarted_6;
	// System.Boolean UIButtonMessage::mHighlighted
	bool ___mHighlighted_7;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___target_2)); }
	inline GameObject_t2557347079 * get_target_2() const { return ___target_2; }
	inline GameObject_t2557347079 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t2557347079 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_functionName_3() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___functionName_3)); }
	inline String_t* get_functionName_3() const { return ___functionName_3; }
	inline String_t** get_address_of_functionName_3() { return &___functionName_3; }
	inline void set_functionName_3(String_t* value)
	{
		___functionName_3 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_3), value);
	}

	inline static int32_t get_offset_of_trigger_4() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___trigger_4)); }
	inline int32_t get_trigger_4() const { return ___trigger_4; }
	inline int32_t* get_address_of_trigger_4() { return &___trigger_4; }
	inline void set_trigger_4(int32_t value)
	{
		___trigger_4 = value;
	}

	inline static int32_t get_offset_of_includeChildren_5() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___includeChildren_5)); }
	inline bool get_includeChildren_5() const { return ___includeChildren_5; }
	inline bool* get_address_of_includeChildren_5() { return &___includeChildren_5; }
	inline void set_includeChildren_5(bool value)
	{
		___includeChildren_5 = value;
	}

	inline static int32_t get_offset_of_mStarted_6() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___mStarted_6)); }
	inline bool get_mStarted_6() const { return ___mStarted_6; }
	inline bool* get_address_of_mStarted_6() { return &___mStarted_6; }
	inline void set_mStarted_6(bool value)
	{
		___mStarted_6 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_7() { return static_cast<int32_t>(offsetof(UIButtonMessage_t897303282, ___mHighlighted_7)); }
	inline bool get_mHighlighted_7() const { return ___mHighlighted_7; }
	inline bool* get_address_of_mHighlighted_7() { return &___mHighlighted_7; }
	inline void set_mHighlighted_7(bool value)
	{
		___mHighlighted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONMESSAGE_T897303282_H
#ifndef UIBUTTONKEYS_T2636625883_H
#define UIBUTTONKEYS_T2636625883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonKeys
struct  UIButtonKeys_t2636625883  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UIButtonKeys::startsSelected
	bool ___startsSelected_2;
	// UIButtonKeys UIButtonKeys::selectOnClick
	UIButtonKeys_t2636625883 * ___selectOnClick_3;
	// UIButtonKeys UIButtonKeys::selectOnUp
	UIButtonKeys_t2636625883 * ___selectOnUp_4;
	// UIButtonKeys UIButtonKeys::selectOnDown
	UIButtonKeys_t2636625883 * ___selectOnDown_5;
	// UIButtonKeys UIButtonKeys::selectOnLeft
	UIButtonKeys_t2636625883 * ___selectOnLeft_6;
	// UIButtonKeys UIButtonKeys::selectOnRight
	UIButtonKeys_t2636625883 * ___selectOnRight_7;

public:
	inline static int32_t get_offset_of_startsSelected_2() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___startsSelected_2)); }
	inline bool get_startsSelected_2() const { return ___startsSelected_2; }
	inline bool* get_address_of_startsSelected_2() { return &___startsSelected_2; }
	inline void set_startsSelected_2(bool value)
	{
		___startsSelected_2 = value;
	}

	inline static int32_t get_offset_of_selectOnClick_3() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___selectOnClick_3)); }
	inline UIButtonKeys_t2636625883 * get_selectOnClick_3() const { return ___selectOnClick_3; }
	inline UIButtonKeys_t2636625883 ** get_address_of_selectOnClick_3() { return &___selectOnClick_3; }
	inline void set_selectOnClick_3(UIButtonKeys_t2636625883 * value)
	{
		___selectOnClick_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnClick_3), value);
	}

	inline static int32_t get_offset_of_selectOnUp_4() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___selectOnUp_4)); }
	inline UIButtonKeys_t2636625883 * get_selectOnUp_4() const { return ___selectOnUp_4; }
	inline UIButtonKeys_t2636625883 ** get_address_of_selectOnUp_4() { return &___selectOnUp_4; }
	inline void set_selectOnUp_4(UIButtonKeys_t2636625883 * value)
	{
		___selectOnUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnUp_4), value);
	}

	inline static int32_t get_offset_of_selectOnDown_5() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___selectOnDown_5)); }
	inline UIButtonKeys_t2636625883 * get_selectOnDown_5() const { return ___selectOnDown_5; }
	inline UIButtonKeys_t2636625883 ** get_address_of_selectOnDown_5() { return &___selectOnDown_5; }
	inline void set_selectOnDown_5(UIButtonKeys_t2636625883 * value)
	{
		___selectOnDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnDown_5), value);
	}

	inline static int32_t get_offset_of_selectOnLeft_6() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___selectOnLeft_6)); }
	inline UIButtonKeys_t2636625883 * get_selectOnLeft_6() const { return ___selectOnLeft_6; }
	inline UIButtonKeys_t2636625883 ** get_address_of_selectOnLeft_6() { return &___selectOnLeft_6; }
	inline void set_selectOnLeft_6(UIButtonKeys_t2636625883 * value)
	{
		___selectOnLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnLeft_6), value);
	}

	inline static int32_t get_offset_of_selectOnRight_7() { return static_cast<int32_t>(offsetof(UIButtonKeys_t2636625883, ___selectOnRight_7)); }
	inline UIButtonKeys_t2636625883 * get_selectOnRight_7() const { return ___selectOnRight_7; }
	inline UIButtonKeys_t2636625883 ** get_address_of_selectOnRight_7() { return &___selectOnRight_7; }
	inline void set_selectOnRight_7(UIButtonKeys_t2636625883 * value)
	{
		___selectOnRight_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnRight_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONKEYS_T2636625883_H
#ifndef UIBUTTONTWEEN_T3240537385_H
#define UIBUTTONTWEEN_T3240537385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonTween
struct  UIButtonTween_t3240537385  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UIButtonTween::tweenTarget
	GameObject_t2557347079 * ___tweenTarget_2;
	// System.Int32 UIButtonTween::tweenGroup
	int32_t ___tweenGroup_3;
	// AnimationOrTween.Trigger UIButtonTween::trigger
	int32_t ___trigger_4;
	// AnimationOrTween.Direction UIButtonTween::playDirection
	int32_t ___playDirection_5;
	// System.Boolean UIButtonTween::resetOnPlay
	bool ___resetOnPlay_6;
	// AnimationOrTween.EnableCondition UIButtonTween::ifDisabledOnPlay
	int32_t ___ifDisabledOnPlay_7;
	// AnimationOrTween.DisableCondition UIButtonTween::disableWhenFinished
	int32_t ___disableWhenFinished_8;
	// System.Boolean UIButtonTween::includeChildren
	bool ___includeChildren_9;
	// UnityEngine.GameObject UIButtonTween::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_10;
	// System.String UIButtonTween::callWhenFinished
	String_t* ___callWhenFinished_11;
	// UITweener/OnFinished UIButtonTween::onFinished
	OnFinished_t2354188770 * ___onFinished_12;
	// UITweener[] UIButtonTween::mTweens
	UITweenerU5BU5D_t4289166548* ___mTweens_13;
	// System.Boolean UIButtonTween::mStarted
	bool ___mStarted_14;
	// System.Boolean UIButtonTween::mHighlighted
	bool ___mHighlighted_15;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___tweenTarget_2)); }
	inline GameObject_t2557347079 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline GameObject_t2557347079 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(GameObject_t2557347079 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_tweenGroup_3() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___tweenGroup_3)); }
	inline int32_t get_tweenGroup_3() const { return ___tweenGroup_3; }
	inline int32_t* get_address_of_tweenGroup_3() { return &___tweenGroup_3; }
	inline void set_tweenGroup_3(int32_t value)
	{
		___tweenGroup_3 = value;
	}

	inline static int32_t get_offset_of_trigger_4() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___trigger_4)); }
	inline int32_t get_trigger_4() const { return ___trigger_4; }
	inline int32_t* get_address_of_trigger_4() { return &___trigger_4; }
	inline void set_trigger_4(int32_t value)
	{
		___trigger_4 = value;
	}

	inline static int32_t get_offset_of_playDirection_5() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___playDirection_5)); }
	inline int32_t get_playDirection_5() const { return ___playDirection_5; }
	inline int32_t* get_address_of_playDirection_5() { return &___playDirection_5; }
	inline void set_playDirection_5(int32_t value)
	{
		___playDirection_5 = value;
	}

	inline static int32_t get_offset_of_resetOnPlay_6() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___resetOnPlay_6)); }
	inline bool get_resetOnPlay_6() const { return ___resetOnPlay_6; }
	inline bool* get_address_of_resetOnPlay_6() { return &___resetOnPlay_6; }
	inline void set_resetOnPlay_6(bool value)
	{
		___resetOnPlay_6 = value;
	}

	inline static int32_t get_offset_of_ifDisabledOnPlay_7() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___ifDisabledOnPlay_7)); }
	inline int32_t get_ifDisabledOnPlay_7() const { return ___ifDisabledOnPlay_7; }
	inline int32_t* get_address_of_ifDisabledOnPlay_7() { return &___ifDisabledOnPlay_7; }
	inline void set_ifDisabledOnPlay_7(int32_t value)
	{
		___ifDisabledOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_disableWhenFinished_8() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___disableWhenFinished_8)); }
	inline int32_t get_disableWhenFinished_8() const { return ___disableWhenFinished_8; }
	inline int32_t* get_address_of_disableWhenFinished_8() { return &___disableWhenFinished_8; }
	inline void set_disableWhenFinished_8(int32_t value)
	{
		___disableWhenFinished_8 = value;
	}

	inline static int32_t get_offset_of_includeChildren_9() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___includeChildren_9)); }
	inline bool get_includeChildren_9() const { return ___includeChildren_9; }
	inline bool* get_address_of_includeChildren_9() { return &___includeChildren_9; }
	inline void set_includeChildren_9(bool value)
	{
		___includeChildren_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_10() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___eventReceiver_10)); }
	inline GameObject_t2557347079 * get_eventReceiver_10() const { return ___eventReceiver_10; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_10() { return &___eventReceiver_10; }
	inline void set_eventReceiver_10(GameObject_t2557347079 * value)
	{
		___eventReceiver_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_10), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_11() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___callWhenFinished_11)); }
	inline String_t* get_callWhenFinished_11() const { return ___callWhenFinished_11; }
	inline String_t** get_address_of_callWhenFinished_11() { return &___callWhenFinished_11; }
	inline void set_callWhenFinished_11(String_t* value)
	{
		___callWhenFinished_11 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_11), value);
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___onFinished_12)); }
	inline OnFinished_t2354188770 * get_onFinished_12() const { return ___onFinished_12; }
	inline OnFinished_t2354188770 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(OnFinished_t2354188770 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_mTweens_13() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___mTweens_13)); }
	inline UITweenerU5BU5D_t4289166548* get_mTweens_13() const { return ___mTweens_13; }
	inline UITweenerU5BU5D_t4289166548** get_address_of_mTweens_13() { return &___mTweens_13; }
	inline void set_mTweens_13(UITweenerU5BU5D_t4289166548* value)
	{
		___mTweens_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTweens_13), value);
	}

	inline static int32_t get_offset_of_mStarted_14() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___mStarted_14)); }
	inline bool get_mStarted_14() const { return ___mStarted_14; }
	inline bool* get_address_of_mStarted_14() { return &___mStarted_14; }
	inline void set_mStarted_14(bool value)
	{
		___mStarted_14 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_15() { return static_cast<int32_t>(offsetof(UIButtonTween_t3240537385, ___mHighlighted_15)); }
	inline bool get_mHighlighted_15() const { return ___mHighlighted_15; }
	inline bool* get_address_of_mHighlighted_15() { return &___mHighlighted_15; }
	inline void set_mHighlighted_15(bool value)
	{
		___mHighlighted_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONTWEEN_T3240537385_H
#ifndef UIBUTTONCOLOR_T2620095936_H
#define UIBUTTONCOLOR_T2620095936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonColor
struct  UIButtonColor_t2620095936  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UIButtonColor::tweenTarget
	GameObject_t2557347079 * ___tweenTarget_2;
	// UnityEngine.Color UIButtonColor::hover
	Color_t2582018970  ___hover_3;
	// UnityEngine.Color UIButtonColor::pressed
	Color_t2582018970  ___pressed_4;
	// System.Single UIButtonColor::duration
	float ___duration_5;
	// UnityEngine.Color UIButtonColor::mColor
	Color_t2582018970  ___mColor_6;
	// System.Boolean UIButtonColor::mInitDone
	bool ___mInitDone_7;
	// System.Boolean UIButtonColor::mStarted
	bool ___mStarted_8;
	// System.Boolean UIButtonColor::mHighlighted
	bool ___mHighlighted_9;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___tweenTarget_2)); }
	inline GameObject_t2557347079 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline GameObject_t2557347079 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(GameObject_t2557347079 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___hover_3)); }
	inline Color_t2582018970  get_hover_3() const { return ___hover_3; }
	inline Color_t2582018970 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Color_t2582018970  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___pressed_4)); }
	inline Color_t2582018970  get_pressed_4() const { return ___pressed_4; }
	inline Color_t2582018970 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Color_t2582018970  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mColor_6() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___mColor_6)); }
	inline Color_t2582018970  get_mColor_6() const { return ___mColor_6; }
	inline Color_t2582018970 * get_address_of_mColor_6() { return &___mColor_6; }
	inline void set_mColor_6(Color_t2582018970  value)
	{
		___mColor_6 = value;
	}

	inline static int32_t get_offset_of_mInitDone_7() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___mInitDone_7)); }
	inline bool get_mInitDone_7() const { return ___mInitDone_7; }
	inline bool* get_address_of_mInitDone_7() { return &___mInitDone_7; }
	inline void set_mInitDone_7(bool value)
	{
		___mInitDone_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_9() { return static_cast<int32_t>(offsetof(UIButtonColor_t2620095936, ___mHighlighted_9)); }
	inline bool get_mHighlighted_9() const { return ___mHighlighted_9; }
	inline bool* get_address_of_mHighlighted_9() { return &___mHighlighted_9; }
	inline void set_mHighlighted_9(bool value)
	{
		___mHighlighted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONCOLOR_T2620095936_H
#ifndef UIBUTTONACTIVATE_T301132035_H
#define UIBUTTONACTIVATE_T301132035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonActivate
struct  UIButtonActivate_t301132035  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UIButtonActivate::target
	GameObject_t2557347079 * ___target_2;
	// System.Boolean UIButtonActivate::state
	bool ___state_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIButtonActivate_t301132035, ___target_2)); }
	inline GameObject_t2557347079 * get_target_2() const { return ___target_2; }
	inline GameObject_t2557347079 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t2557347079 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(UIButtonActivate_t301132035, ___state_3)); }
	inline bool get_state_3() const { return ___state_3; }
	inline bool* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(bool value)
	{
		___state_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONACTIVATE_T301132035_H
#ifndef UICHECKBOX_T1205955448_H
#define UICHECKBOX_T1205955448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICheckbox
struct  UICheckbox_t1205955448  : public MonoBehaviour_t1618594486
{
public:
	// UISprite UICheckbox::checkSprite
	UISprite_t279728715 * ___checkSprite_3;
	// UnityEngine.Animation UICheckbox::checkAnimation
	Animation_t3821138400 * ___checkAnimation_4;
	// System.Boolean UICheckbox::startsChecked
	bool ___startsChecked_5;
	// UnityEngine.Transform UICheckbox::radioButtonRoot
	Transform_t362059596 * ___radioButtonRoot_6;
	// System.Boolean UICheckbox::optionCanBeNone
	bool ___optionCanBeNone_7;
	// UnityEngine.GameObject UICheckbox::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_8;
	// System.String UICheckbox::functionName
	String_t* ___functionName_9;
	// UICheckbox/OnStateChange UICheckbox::onStateChange
	OnStateChange_t2569002187 * ___onStateChange_10;
	// System.Boolean UICheckbox::option
	bool ___option_11;
	// System.Boolean UICheckbox::mChecked
	bool ___mChecked_12;
	// System.Boolean UICheckbox::mStarted
	bool ___mStarted_13;
	// UnityEngine.Transform UICheckbox::mTrans
	Transform_t362059596 * ___mTrans_14;

public:
	inline static int32_t get_offset_of_checkSprite_3() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___checkSprite_3)); }
	inline UISprite_t279728715 * get_checkSprite_3() const { return ___checkSprite_3; }
	inline UISprite_t279728715 ** get_address_of_checkSprite_3() { return &___checkSprite_3; }
	inline void set_checkSprite_3(UISprite_t279728715 * value)
	{
		___checkSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___checkSprite_3), value);
	}

	inline static int32_t get_offset_of_checkAnimation_4() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___checkAnimation_4)); }
	inline Animation_t3821138400 * get_checkAnimation_4() const { return ___checkAnimation_4; }
	inline Animation_t3821138400 ** get_address_of_checkAnimation_4() { return &___checkAnimation_4; }
	inline void set_checkAnimation_4(Animation_t3821138400 * value)
	{
		___checkAnimation_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkAnimation_4), value);
	}

	inline static int32_t get_offset_of_startsChecked_5() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___startsChecked_5)); }
	inline bool get_startsChecked_5() const { return ___startsChecked_5; }
	inline bool* get_address_of_startsChecked_5() { return &___startsChecked_5; }
	inline void set_startsChecked_5(bool value)
	{
		___startsChecked_5 = value;
	}

	inline static int32_t get_offset_of_radioButtonRoot_6() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___radioButtonRoot_6)); }
	inline Transform_t362059596 * get_radioButtonRoot_6() const { return ___radioButtonRoot_6; }
	inline Transform_t362059596 ** get_address_of_radioButtonRoot_6() { return &___radioButtonRoot_6; }
	inline void set_radioButtonRoot_6(Transform_t362059596 * value)
	{
		___radioButtonRoot_6 = value;
		Il2CppCodeGenWriteBarrier((&___radioButtonRoot_6), value);
	}

	inline static int32_t get_offset_of_optionCanBeNone_7() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___optionCanBeNone_7)); }
	inline bool get_optionCanBeNone_7() const { return ___optionCanBeNone_7; }
	inline bool* get_address_of_optionCanBeNone_7() { return &___optionCanBeNone_7; }
	inline void set_optionCanBeNone_7(bool value)
	{
		___optionCanBeNone_7 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_8() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___eventReceiver_8)); }
	inline GameObject_t2557347079 * get_eventReceiver_8() const { return ___eventReceiver_8; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_8() { return &___eventReceiver_8; }
	inline void set_eventReceiver_8(GameObject_t2557347079 * value)
	{
		___eventReceiver_8 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_8), value);
	}

	inline static int32_t get_offset_of_functionName_9() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___functionName_9)); }
	inline String_t* get_functionName_9() const { return ___functionName_9; }
	inline String_t** get_address_of_functionName_9() { return &___functionName_9; }
	inline void set_functionName_9(String_t* value)
	{
		___functionName_9 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_9), value);
	}

	inline static int32_t get_offset_of_onStateChange_10() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___onStateChange_10)); }
	inline OnStateChange_t2569002187 * get_onStateChange_10() const { return ___onStateChange_10; }
	inline OnStateChange_t2569002187 ** get_address_of_onStateChange_10() { return &___onStateChange_10; }
	inline void set_onStateChange_10(OnStateChange_t2569002187 * value)
	{
		___onStateChange_10 = value;
		Il2CppCodeGenWriteBarrier((&___onStateChange_10), value);
	}

	inline static int32_t get_offset_of_option_11() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___option_11)); }
	inline bool get_option_11() const { return ___option_11; }
	inline bool* get_address_of_option_11() { return &___option_11; }
	inline void set_option_11(bool value)
	{
		___option_11 = value;
	}

	inline static int32_t get_offset_of_mChecked_12() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___mChecked_12)); }
	inline bool get_mChecked_12() const { return ___mChecked_12; }
	inline bool* get_address_of_mChecked_12() { return &___mChecked_12; }
	inline void set_mChecked_12(bool value)
	{
		___mChecked_12 = value;
	}

	inline static int32_t get_offset_of_mStarted_13() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___mStarted_13)); }
	inline bool get_mStarted_13() const { return ___mStarted_13; }
	inline bool* get_address_of_mStarted_13() { return &___mStarted_13; }
	inline void set_mStarted_13(bool value)
	{
		___mStarted_13 = value;
	}

	inline static int32_t get_offset_of_mTrans_14() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448, ___mTrans_14)); }
	inline Transform_t362059596 * get_mTrans_14() const { return ___mTrans_14; }
	inline Transform_t362059596 ** get_address_of_mTrans_14() { return &___mTrans_14; }
	inline void set_mTrans_14(Transform_t362059596 * value)
	{
		___mTrans_14 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_14), value);
	}
};

struct UICheckbox_t1205955448_StaticFields
{
public:
	// UICheckbox UICheckbox::current
	UICheckbox_t1205955448 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UICheckbox_t1205955448_StaticFields, ___current_2)); }
	inline UICheckbox_t1205955448 * get_current_2() const { return ___current_2; }
	inline UICheckbox_t1205955448 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UICheckbox_t1205955448 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHECKBOX_T1205955448_H
#ifndef LANGUAGESELECTION_T1687987668_H
#define LANGUAGESELECTION_T1687987668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageSelection
struct  LanguageSelection_t1687987668  : public MonoBehaviour_t1618594486
{
public:
	// UIPopupList LanguageSelection::mList
	UIPopupList_t2907709271 * ___mList_2;

public:
	inline static int32_t get_offset_of_mList_2() { return static_cast<int32_t>(offsetof(LanguageSelection_t1687987668, ___mList_2)); }
	inline UIPopupList_t2907709271 * get_mList_2() const { return ___mList_2; }
	inline UIPopupList_t2907709271 ** get_address_of_mList_2() { return &___mList_2; }
	inline void set_mList_2(UIPopupList_t2907709271 * value)
	{
		___mList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGESELECTION_T1687987668_H
#ifndef WINDOWDRAGTILT_T561804078_H
#define WINDOWDRAGTILT_T561804078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowDragTilt
struct  WindowDragTilt_t561804078  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 WindowDragTilt::updateOrder
	int32_t ___updateOrder_2;
	// System.Single WindowDragTilt::degrees
	float ___degrees_3;
	// UnityEngine.Vector3 WindowDragTilt::mLastPos
	Vector3_t1986933152  ___mLastPos_4;
	// UnityEngine.Transform WindowDragTilt::mTrans
	Transform_t362059596 * ___mTrans_5;
	// System.Single WindowDragTilt::mAngle
	float ___mAngle_6;
	// System.Boolean WindowDragTilt::mInit
	bool ___mInit_7;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_degrees_3() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___degrees_3)); }
	inline float get_degrees_3() const { return ___degrees_3; }
	inline float* get_address_of_degrees_3() { return &___degrees_3; }
	inline void set_degrees_3(float value)
	{
		___degrees_3 = value;
	}

	inline static int32_t get_offset_of_mLastPos_4() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mLastPos_4)); }
	inline Vector3_t1986933152  get_mLastPos_4() const { return ___mLastPos_4; }
	inline Vector3_t1986933152 * get_address_of_mLastPos_4() { return &___mLastPos_4; }
	inline void set_mLastPos_4(Vector3_t1986933152  value)
	{
		___mLastPos_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mAngle_6() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mAngle_6)); }
	inline float get_mAngle_6() const { return ___mAngle_6; }
	inline float* get_address_of_mAngle_6() { return &___mAngle_6; }
	inline void set_mAngle_6(float value)
	{
		___mAngle_6 = value;
	}

	inline static int32_t get_offset_of_mInit_7() { return static_cast<int32_t>(offsetof(WindowDragTilt_t561804078, ___mInit_7)); }
	inline bool get_mInit_7() const { return ___mInit_7; }
	inline bool* get_address_of_mInit_7() { return &___mInit_7; }
	inline void set_mInit_7(bool value)
	{
		___mInit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWDRAGTILT_T561804078_H
#ifndef WINDOWAUTOYAW_T2079382413_H
#define WINDOWAUTOYAW_T2079382413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowAutoYaw
struct  WindowAutoYaw_t2079382413  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 WindowAutoYaw::updateOrder
	int32_t ___updateOrder_2;
	// UnityEngine.Camera WindowAutoYaw::uiCamera
	Camera_t2839736942 * ___uiCamera_3;
	// System.Single WindowAutoYaw::yawAmount
	float ___yawAmount_4;
	// UnityEngine.Transform WindowAutoYaw::mTrans
	Transform_t362059596 * ___mTrans_5;

public:
	inline static int32_t get_offset_of_updateOrder_2() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___updateOrder_2)); }
	inline int32_t get_updateOrder_2() const { return ___updateOrder_2; }
	inline int32_t* get_address_of_updateOrder_2() { return &___updateOrder_2; }
	inline void set_updateOrder_2(int32_t value)
	{
		___updateOrder_2 = value;
	}

	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___uiCamera_3)); }
	inline Camera_t2839736942 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t2839736942 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t2839736942 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_yawAmount_4() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___yawAmount_4)); }
	inline float get_yawAmount_4() const { return ___yawAmount_4; }
	inline float* get_address_of_yawAmount_4() { return &___yawAmount_4; }
	inline void set_yawAmount_4(float value)
	{
		___yawAmount_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(WindowAutoYaw_t2079382413, ___mTrans_5)); }
	inline Transform_t362059596 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t362059596 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t362059596 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWAUTOYAW_T2079382413_H
#ifndef TYPEWRITEREFFECT_T1609054849_H
#define TYPEWRITEREFFECT_T1609054849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypewriterEffect
struct  TypewriterEffect_t1609054849  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 TypewriterEffect::charsPerSecond
	int32_t ___charsPerSecond_2;
	// UILabel TypewriterEffect::mLabel
	UILabel_t3478074517 * ___mLabel_3;
	// System.String TypewriterEffect::mText
	String_t* ___mText_4;
	// System.Int32 TypewriterEffect::mOffset
	int32_t ___mOffset_5;
	// System.Single TypewriterEffect::mNextChar
	float ___mNextChar_6;

public:
	inline static int32_t get_offset_of_charsPerSecond_2() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1609054849, ___charsPerSecond_2)); }
	inline int32_t get_charsPerSecond_2() const { return ___charsPerSecond_2; }
	inline int32_t* get_address_of_charsPerSecond_2() { return &___charsPerSecond_2; }
	inline void set_charsPerSecond_2(int32_t value)
	{
		___charsPerSecond_2 = value;
	}

	inline static int32_t get_offset_of_mLabel_3() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1609054849, ___mLabel_3)); }
	inline UILabel_t3478074517 * get_mLabel_3() const { return ___mLabel_3; }
	inline UILabel_t3478074517 ** get_address_of_mLabel_3() { return &___mLabel_3; }
	inline void set_mLabel_3(UILabel_t3478074517 * value)
	{
		___mLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLabel_3), value);
	}

	inline static int32_t get_offset_of_mText_4() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1609054849, ___mText_4)); }
	inline String_t* get_mText_4() const { return ___mText_4; }
	inline String_t** get_address_of_mText_4() { return &___mText_4; }
	inline void set_mText_4(String_t* value)
	{
		___mText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mText_4), value);
	}

	inline static int32_t get_offset_of_mOffset_5() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1609054849, ___mOffset_5)); }
	inline int32_t get_mOffset_5() const { return ___mOffset_5; }
	inline int32_t* get_address_of_mOffset_5() { return &___mOffset_5; }
	inline void set_mOffset_5(int32_t value)
	{
		___mOffset_5 = value;
	}

	inline static int32_t get_offset_of_mNextChar_6() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1609054849, ___mNextChar_6)); }
	inline float get_mNextChar_6() const { return ___mNextChar_6; }
	inline float* get_address_of_mNextChar_6() { return &___mNextChar_6; }
	inline void set_mNextChar_6(float value)
	{
		___mNextChar_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEWRITEREFFECT_T1609054849_H
#ifndef SPINWITHMOUSE_T622040930_H
#define SPINWITHMOUSE_T622040930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpinWithMouse
struct  SpinWithMouse_t622040930  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform SpinWithMouse::target
	Transform_t362059596 * ___target_2;
	// System.Single SpinWithMouse::speed
	float ___speed_3;
	// UnityEngine.Transform SpinWithMouse::mTrans
	Transform_t362059596 * ___mTrans_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SpinWithMouse_t622040930, ___target_2)); }
	inline Transform_t362059596 * get_target_2() const { return ___target_2; }
	inline Transform_t362059596 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t362059596 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(SpinWithMouse_t622040930, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_mTrans_4() { return static_cast<int32_t>(offsetof(SpinWithMouse_t622040930, ___mTrans_4)); }
	inline Transform_t362059596 * get_mTrans_4() const { return ___mTrans_4; }
	inline Transform_t362059596 ** get_address_of_mTrans_4() { return &___mTrans_4; }
	inline void set_mTrans_4(Transform_t362059596 * value)
	{
		___mTrans_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINWITHMOUSE_T622040930_H
#ifndef SPIN_T1543306399_H
#define SPIN_T1543306399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spin
struct  Spin_t1543306399  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 Spin::rotationsPerSecond
	Vector3_t1986933152  ___rotationsPerSecond_2;
	// UnityEngine.Rigidbody Spin::mRb
	Rigidbody_t4273256674 * ___mRb_3;
	// UnityEngine.Transform Spin::mTrans
	Transform_t362059596 * ___mTrans_4;

public:
	inline static int32_t get_offset_of_rotationsPerSecond_2() { return static_cast<int32_t>(offsetof(Spin_t1543306399, ___rotationsPerSecond_2)); }
	inline Vector3_t1986933152  get_rotationsPerSecond_2() const { return ___rotationsPerSecond_2; }
	inline Vector3_t1986933152 * get_address_of_rotationsPerSecond_2() { return &___rotationsPerSecond_2; }
	inline void set_rotationsPerSecond_2(Vector3_t1986933152  value)
	{
		___rotationsPerSecond_2 = value;
	}

	inline static int32_t get_offset_of_mRb_3() { return static_cast<int32_t>(offsetof(Spin_t1543306399, ___mRb_3)); }
	inline Rigidbody_t4273256674 * get_mRb_3() const { return ___mRb_3; }
	inline Rigidbody_t4273256674 ** get_address_of_mRb_3() { return &___mRb_3; }
	inline void set_mRb_3(Rigidbody_t4273256674 * value)
	{
		___mRb_3 = value;
		Il2CppCodeGenWriteBarrier((&___mRb_3), value);
	}

	inline static int32_t get_offset_of_mTrans_4() { return static_cast<int32_t>(offsetof(Spin_t1543306399, ___mTrans_4)); }
	inline Transform_t362059596 * get_mTrans_4() const { return ___mTrans_4; }
	inline Transform_t362059596 ** get_address_of_mTrans_4() { return &___mTrans_4; }
	inline void set_mTrans_4(Transform_t362059596 * value)
	{
		___mTrans_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPIN_T1543306399_H
#ifndef SHADERQUALITY_T2238873719_H
#define SHADERQUALITY_T2238873719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShaderQuality
struct  ShaderQuality_t2238873719  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 ShaderQuality::mCurrent
	int32_t ___mCurrent_2;

public:
	inline static int32_t get_offset_of_mCurrent_2() { return static_cast<int32_t>(offsetof(ShaderQuality_t2238873719, ___mCurrent_2)); }
	inline int32_t get_mCurrent_2() const { return ___mCurrent_2; }
	inline int32_t* get_address_of_mCurrent_2() { return &___mCurrent_2; }
	inline void set_mCurrent_2(int32_t value)
	{
		___mCurrent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERQUALITY_T2238873719_H
#ifndef SETCOLORONSELECTION_T1555214854_H
#define SETCOLORONSELECTION_T1555214854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetColorOnSelection
struct  SetColorOnSelection_t1555214854  : public MonoBehaviour_t1618594486
{
public:
	// UIWidget SetColorOnSelection::mWidget
	UIWidget_t1961100141 * ___mWidget_2;

public:
	inline static int32_t get_offset_of_mWidget_2() { return static_cast<int32_t>(offsetof(SetColorOnSelection_t1555214854, ___mWidget_2)); }
	inline UIWidget_t1961100141 * get_mWidget_2() const { return ___mWidget_2; }
	inline UIWidget_t1961100141 ** get_address_of_mWidget_2() { return &___mWidget_2; }
	inline void set_mWidget_2(UIWidget_t1961100141 * value)
	{
		___mWidget_2 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_2), value);
	}
};

struct SetColorOnSelection_t1555214854_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SetColorOnSelection::<>f__switch$map1
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_3() { return static_cast<int32_t>(offsetof(SetColorOnSelection_t1555214854_StaticFields, ___U3CU3Ef__switchU24map1_3)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map1_3() const { return ___U3CU3Ef__switchU24map1_3; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map1_3() { return &___U3CU3Ef__switchU24map1_3; }
	inline void set_U3CU3Ef__switchU24map1_3(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCOLORONSELECTION_T1555214854_H
#ifndef UIBUTTONROTATION_T3360562254_H
#define UIBUTTONROTATION_T3360562254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonRotation
struct  UIButtonRotation_t3360562254  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform UIButtonRotation::tweenTarget
	Transform_t362059596 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonRotation::hover
	Vector3_t1986933152  ___hover_3;
	// UnityEngine.Vector3 UIButtonRotation::pressed
	Vector3_t1986933152  ___pressed_4;
	// System.Single UIButtonRotation::duration
	float ___duration_5;
	// UnityEngine.Quaternion UIButtonRotation::mRot
	Quaternion_t704191599  ___mRot_6;
	// System.Boolean UIButtonRotation::mInitDone
	bool ___mInitDone_7;
	// System.Boolean UIButtonRotation::mStarted
	bool ___mStarted_8;
	// System.Boolean UIButtonRotation::mHighlighted
	bool ___mHighlighted_9;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___tweenTarget_2)); }
	inline Transform_t362059596 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t362059596 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t362059596 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___hover_3)); }
	inline Vector3_t1986933152  get_hover_3() const { return ___hover_3; }
	inline Vector3_t1986933152 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t1986933152  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___pressed_4)); }
	inline Vector3_t1986933152  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t1986933152 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t1986933152  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mRot_6() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___mRot_6)); }
	inline Quaternion_t704191599  get_mRot_6() const { return ___mRot_6; }
	inline Quaternion_t704191599 * get_address_of_mRot_6() { return &___mRot_6; }
	inline void set_mRot_6(Quaternion_t704191599  value)
	{
		___mRot_6 = value;
	}

	inline static int32_t get_offset_of_mInitDone_7() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___mInitDone_7)); }
	inline bool get_mInitDone_7() const { return ___mInitDone_7; }
	inline bool* get_address_of_mInitDone_7() { return &___mInitDone_7; }
	inline void set_mInitDone_7(bool value)
	{
		___mInitDone_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_9() { return static_cast<int32_t>(offsetof(UIButtonRotation_t3360562254, ___mHighlighted_9)); }
	inline bool get_mHighlighted_9() const { return ___mHighlighted_9; }
	inline bool* get_address_of_mHighlighted_9() { return &___mHighlighted_9; }
	inline void set_mHighlighted_9(bool value)
	{
		___mHighlighted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONROTATION_T3360562254_H
#ifndef UIBUTTONSCALE_T401580026_H
#define UIBUTTONSCALE_T401580026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonScale
struct  UIButtonScale_t401580026  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform UIButtonScale::tweenTarget
	Transform_t362059596 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonScale::hover
	Vector3_t1986933152  ___hover_3;
	// UnityEngine.Vector3 UIButtonScale::pressed
	Vector3_t1986933152  ___pressed_4;
	// System.Single UIButtonScale::duration
	float ___duration_5;
	// UnityEngine.Vector3 UIButtonScale::mScale
	Vector3_t1986933152  ___mScale_6;
	// System.Boolean UIButtonScale::mInitDone
	bool ___mInitDone_7;
	// System.Boolean UIButtonScale::mStarted
	bool ___mStarted_8;
	// System.Boolean UIButtonScale::mHighlighted
	bool ___mHighlighted_9;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___tweenTarget_2)); }
	inline Transform_t362059596 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t362059596 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t362059596 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___hover_3)); }
	inline Vector3_t1986933152  get_hover_3() const { return ___hover_3; }
	inline Vector3_t1986933152 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t1986933152  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___pressed_4)); }
	inline Vector3_t1986933152  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t1986933152 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t1986933152  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mScale_6() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___mScale_6)); }
	inline Vector3_t1986933152  get_mScale_6() const { return ___mScale_6; }
	inline Vector3_t1986933152 * get_address_of_mScale_6() { return &___mScale_6; }
	inline void set_mScale_6(Vector3_t1986933152  value)
	{
		___mScale_6 = value;
	}

	inline static int32_t get_offset_of_mInitDone_7() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___mInitDone_7)); }
	inline bool get_mInitDone_7() const { return ___mInitDone_7; }
	inline bool* get_address_of_mInitDone_7() { return &___mInitDone_7; }
	inline void set_mInitDone_7(bool value)
	{
		___mInitDone_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mHighlighted_9() { return static_cast<int32_t>(offsetof(UIButtonScale_t401580026, ___mHighlighted_9)); }
	inline bool get_mHighlighted_9() const { return ___mHighlighted_9; }
	inline bool* get_address_of_mHighlighted_9() { return &___mHighlighted_9; }
	inline void set_mHighlighted_9(bool value)
	{
		___mHighlighted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONSCALE_T401580026_H
#ifndef UIDRAGGABLEPANEL_T2580300900_H
#define UIDRAGGABLEPANEL_T2580300900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggablePanel
struct  UIDraggablePanel_t2580300900  : public IgnoreTimeScale_t640346767
{
public:
	// System.Boolean UIDraggablePanel::restrictWithinPanel
	bool ___restrictWithinPanel_6;
	// System.Boolean UIDraggablePanel::disableDragIfFits
	bool ___disableDragIfFits_7;
	// UIDraggablePanel/DragEffect UIDraggablePanel::dragEffect
	int32_t ___dragEffect_8;
	// UnityEngine.Vector3 UIDraggablePanel::scale
	Vector3_t1986933152  ___scale_9;
	// System.Single UIDraggablePanel::scrollWheelFactor
	float ___scrollWheelFactor_10;
	// System.Single UIDraggablePanel::momentumAmount
	float ___momentumAmount_11;
	// UnityEngine.Vector2 UIDraggablePanel::relativePositionOnReset
	Vector2_t328513675  ___relativePositionOnReset_12;
	// System.Boolean UIDraggablePanel::repositionClipping
	bool ___repositionClipping_13;
	// UIScrollBar UIDraggablePanel::horizontalScrollBar
	UIScrollBar_t3221813871 * ___horizontalScrollBar_14;
	// UIScrollBar UIDraggablePanel::verticalScrollBar
	UIScrollBar_t3221813871 * ___verticalScrollBar_15;
	// UIDraggablePanel/ShowCondition UIDraggablePanel::showScrollBars
	int32_t ___showScrollBars_16;
	// UIDraggablePanel/OnDragFinished UIDraggablePanel::onDragFinished
	OnDragFinished_t81282452 * ___onDragFinished_17;
	// UnityEngine.Transform UIDraggablePanel::mTrans
	Transform_t362059596 * ___mTrans_18;
	// UIPanel UIDraggablePanel::mPanel
	UIPanel_t825970685 * ___mPanel_19;
	// UnityEngine.Plane UIDraggablePanel::mPlane
	Plane_t3184513474  ___mPlane_20;
	// UnityEngine.Vector3 UIDraggablePanel::mLastPos
	Vector3_t1986933152  ___mLastPos_21;
	// System.Boolean UIDraggablePanel::mPressed
	bool ___mPressed_22;
	// UnityEngine.Vector3 UIDraggablePanel::mMomentum
	Vector3_t1986933152  ___mMomentum_23;
	// System.Single UIDraggablePanel::mScroll
	float ___mScroll_24;
	// UnityEngine.Bounds UIDraggablePanel::mBounds
	Bounds_t3570137099  ___mBounds_25;
	// System.Boolean UIDraggablePanel::mCalculatedBounds
	bool ___mCalculatedBounds_26;
	// System.Boolean UIDraggablePanel::mShouldMove
	bool ___mShouldMove_27;
	// System.Boolean UIDraggablePanel::mIgnoreCallbacks
	bool ___mIgnoreCallbacks_28;
	// System.Int32 UIDraggablePanel::mDragID
	int32_t ___mDragID_29;

public:
	inline static int32_t get_offset_of_restrictWithinPanel_6() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___restrictWithinPanel_6)); }
	inline bool get_restrictWithinPanel_6() const { return ___restrictWithinPanel_6; }
	inline bool* get_address_of_restrictWithinPanel_6() { return &___restrictWithinPanel_6; }
	inline void set_restrictWithinPanel_6(bool value)
	{
		___restrictWithinPanel_6 = value;
	}

	inline static int32_t get_offset_of_disableDragIfFits_7() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___disableDragIfFits_7)); }
	inline bool get_disableDragIfFits_7() const { return ___disableDragIfFits_7; }
	inline bool* get_address_of_disableDragIfFits_7() { return &___disableDragIfFits_7; }
	inline void set_disableDragIfFits_7(bool value)
	{
		___disableDragIfFits_7 = value;
	}

	inline static int32_t get_offset_of_dragEffect_8() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___dragEffect_8)); }
	inline int32_t get_dragEffect_8() const { return ___dragEffect_8; }
	inline int32_t* get_address_of_dragEffect_8() { return &___dragEffect_8; }
	inline void set_dragEffect_8(int32_t value)
	{
		___dragEffect_8 = value;
	}

	inline static int32_t get_offset_of_scale_9() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___scale_9)); }
	inline Vector3_t1986933152  get_scale_9() const { return ___scale_9; }
	inline Vector3_t1986933152 * get_address_of_scale_9() { return &___scale_9; }
	inline void set_scale_9(Vector3_t1986933152  value)
	{
		___scale_9 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_10() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___scrollWheelFactor_10)); }
	inline float get_scrollWheelFactor_10() const { return ___scrollWheelFactor_10; }
	inline float* get_address_of_scrollWheelFactor_10() { return &___scrollWheelFactor_10; }
	inline void set_scrollWheelFactor_10(float value)
	{
		___scrollWheelFactor_10 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_11() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___momentumAmount_11)); }
	inline float get_momentumAmount_11() const { return ___momentumAmount_11; }
	inline float* get_address_of_momentumAmount_11() { return &___momentumAmount_11; }
	inline void set_momentumAmount_11(float value)
	{
		___momentumAmount_11 = value;
	}

	inline static int32_t get_offset_of_relativePositionOnReset_12() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___relativePositionOnReset_12)); }
	inline Vector2_t328513675  get_relativePositionOnReset_12() const { return ___relativePositionOnReset_12; }
	inline Vector2_t328513675 * get_address_of_relativePositionOnReset_12() { return &___relativePositionOnReset_12; }
	inline void set_relativePositionOnReset_12(Vector2_t328513675  value)
	{
		___relativePositionOnReset_12 = value;
	}

	inline static int32_t get_offset_of_repositionClipping_13() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___repositionClipping_13)); }
	inline bool get_repositionClipping_13() const { return ___repositionClipping_13; }
	inline bool* get_address_of_repositionClipping_13() { return &___repositionClipping_13; }
	inline void set_repositionClipping_13(bool value)
	{
		___repositionClipping_13 = value;
	}

	inline static int32_t get_offset_of_horizontalScrollBar_14() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___horizontalScrollBar_14)); }
	inline UIScrollBar_t3221813871 * get_horizontalScrollBar_14() const { return ___horizontalScrollBar_14; }
	inline UIScrollBar_t3221813871 ** get_address_of_horizontalScrollBar_14() { return &___horizontalScrollBar_14; }
	inline void set_horizontalScrollBar_14(UIScrollBar_t3221813871 * value)
	{
		___horizontalScrollBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalScrollBar_14), value);
	}

	inline static int32_t get_offset_of_verticalScrollBar_15() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___verticalScrollBar_15)); }
	inline UIScrollBar_t3221813871 * get_verticalScrollBar_15() const { return ___verticalScrollBar_15; }
	inline UIScrollBar_t3221813871 ** get_address_of_verticalScrollBar_15() { return &___verticalScrollBar_15; }
	inline void set_verticalScrollBar_15(UIScrollBar_t3221813871 * value)
	{
		___verticalScrollBar_15 = value;
		Il2CppCodeGenWriteBarrier((&___verticalScrollBar_15), value);
	}

	inline static int32_t get_offset_of_showScrollBars_16() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___showScrollBars_16)); }
	inline int32_t get_showScrollBars_16() const { return ___showScrollBars_16; }
	inline int32_t* get_address_of_showScrollBars_16() { return &___showScrollBars_16; }
	inline void set_showScrollBars_16(int32_t value)
	{
		___showScrollBars_16 = value;
	}

	inline static int32_t get_offset_of_onDragFinished_17() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___onDragFinished_17)); }
	inline OnDragFinished_t81282452 * get_onDragFinished_17() const { return ___onDragFinished_17; }
	inline OnDragFinished_t81282452 ** get_address_of_onDragFinished_17() { return &___onDragFinished_17; }
	inline void set_onDragFinished_17(OnDragFinished_t81282452 * value)
	{
		___onDragFinished_17 = value;
		Il2CppCodeGenWriteBarrier((&___onDragFinished_17), value);
	}

	inline static int32_t get_offset_of_mTrans_18() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mTrans_18)); }
	inline Transform_t362059596 * get_mTrans_18() const { return ___mTrans_18; }
	inline Transform_t362059596 ** get_address_of_mTrans_18() { return &___mTrans_18; }
	inline void set_mTrans_18(Transform_t362059596 * value)
	{
		___mTrans_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_18), value);
	}

	inline static int32_t get_offset_of_mPanel_19() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mPanel_19)); }
	inline UIPanel_t825970685 * get_mPanel_19() const { return ___mPanel_19; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_19() { return &___mPanel_19; }
	inline void set_mPanel_19(UIPanel_t825970685 * value)
	{
		___mPanel_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_19), value);
	}

	inline static int32_t get_offset_of_mPlane_20() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mPlane_20)); }
	inline Plane_t3184513474  get_mPlane_20() const { return ___mPlane_20; }
	inline Plane_t3184513474 * get_address_of_mPlane_20() { return &___mPlane_20; }
	inline void set_mPlane_20(Plane_t3184513474  value)
	{
		___mPlane_20 = value;
	}

	inline static int32_t get_offset_of_mLastPos_21() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mLastPos_21)); }
	inline Vector3_t1986933152  get_mLastPos_21() const { return ___mLastPos_21; }
	inline Vector3_t1986933152 * get_address_of_mLastPos_21() { return &___mLastPos_21; }
	inline void set_mLastPos_21(Vector3_t1986933152  value)
	{
		___mLastPos_21 = value;
	}

	inline static int32_t get_offset_of_mPressed_22() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mPressed_22)); }
	inline bool get_mPressed_22() const { return ___mPressed_22; }
	inline bool* get_address_of_mPressed_22() { return &___mPressed_22; }
	inline void set_mPressed_22(bool value)
	{
		___mPressed_22 = value;
	}

	inline static int32_t get_offset_of_mMomentum_23() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mMomentum_23)); }
	inline Vector3_t1986933152  get_mMomentum_23() const { return ___mMomentum_23; }
	inline Vector3_t1986933152 * get_address_of_mMomentum_23() { return &___mMomentum_23; }
	inline void set_mMomentum_23(Vector3_t1986933152  value)
	{
		___mMomentum_23 = value;
	}

	inline static int32_t get_offset_of_mScroll_24() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mScroll_24)); }
	inline float get_mScroll_24() const { return ___mScroll_24; }
	inline float* get_address_of_mScroll_24() { return &___mScroll_24; }
	inline void set_mScroll_24(float value)
	{
		___mScroll_24 = value;
	}

	inline static int32_t get_offset_of_mBounds_25() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mBounds_25)); }
	inline Bounds_t3570137099  get_mBounds_25() const { return ___mBounds_25; }
	inline Bounds_t3570137099 * get_address_of_mBounds_25() { return &___mBounds_25; }
	inline void set_mBounds_25(Bounds_t3570137099  value)
	{
		___mBounds_25 = value;
	}

	inline static int32_t get_offset_of_mCalculatedBounds_26() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mCalculatedBounds_26)); }
	inline bool get_mCalculatedBounds_26() const { return ___mCalculatedBounds_26; }
	inline bool* get_address_of_mCalculatedBounds_26() { return &___mCalculatedBounds_26; }
	inline void set_mCalculatedBounds_26(bool value)
	{
		___mCalculatedBounds_26 = value;
	}

	inline static int32_t get_offset_of_mShouldMove_27() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mShouldMove_27)); }
	inline bool get_mShouldMove_27() const { return ___mShouldMove_27; }
	inline bool* get_address_of_mShouldMove_27() { return &___mShouldMove_27; }
	inline void set_mShouldMove_27(bool value)
	{
		___mShouldMove_27 = value;
	}

	inline static int32_t get_offset_of_mIgnoreCallbacks_28() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mIgnoreCallbacks_28)); }
	inline bool get_mIgnoreCallbacks_28() const { return ___mIgnoreCallbacks_28; }
	inline bool* get_address_of_mIgnoreCallbacks_28() { return &___mIgnoreCallbacks_28; }
	inline void set_mIgnoreCallbacks_28(bool value)
	{
		___mIgnoreCallbacks_28 = value;
	}

	inline static int32_t get_offset_of_mDragID_29() { return static_cast<int32_t>(offsetof(UIDraggablePanel_t2580300900, ___mDragID_29)); }
	inline int32_t get_mDragID_29() const { return ___mDragID_29; }
	inline int32_t* get_address_of_mDragID_29() { return &___mDragID_29; }
	inline void set_mDragID_29(int32_t value)
	{
		___mDragID_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGGABLEPANEL_T2580300900_H
#ifndef UIDRAGGABLECAMERA_T1133134831_H
#define UIDRAGGABLECAMERA_T1133134831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggableCamera
struct  UIDraggableCamera_t1133134831  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Transform UIDraggableCamera::rootForBounds
	Transform_t362059596 * ___rootForBounds_6;
	// UnityEngine.Vector2 UIDraggableCamera::scale
	Vector2_t328513675  ___scale_7;
	// System.Single UIDraggableCamera::scrollWheelFactor
	float ___scrollWheelFactor_8;
	// UIDragObject/DragEffect UIDraggableCamera::dragEffect
	int32_t ___dragEffect_9;
	// System.Single UIDraggableCamera::momentumAmount
	float ___momentumAmount_10;
	// UnityEngine.Camera UIDraggableCamera::mCam
	Camera_t2839736942 * ___mCam_11;
	// UnityEngine.Transform UIDraggableCamera::mTrans
	Transform_t362059596 * ___mTrans_12;
	// System.Boolean UIDraggableCamera::mPressed
	bool ___mPressed_13;
	// UnityEngine.Vector2 UIDraggableCamera::mMomentum
	Vector2_t328513675  ___mMomentum_14;
	// UnityEngine.Bounds UIDraggableCamera::mBounds
	Bounds_t3570137099  ___mBounds_15;
	// System.Single UIDraggableCamera::mScroll
	float ___mScroll_16;
	// UIRoot UIDraggableCamera::mRoot
	UIRoot_t937406327 * ___mRoot_17;

public:
	inline static int32_t get_offset_of_rootForBounds_6() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___rootForBounds_6)); }
	inline Transform_t362059596 * get_rootForBounds_6() const { return ___rootForBounds_6; }
	inline Transform_t362059596 ** get_address_of_rootForBounds_6() { return &___rootForBounds_6; }
	inline void set_rootForBounds_6(Transform_t362059596 * value)
	{
		___rootForBounds_6 = value;
		Il2CppCodeGenWriteBarrier((&___rootForBounds_6), value);
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___scale_7)); }
	inline Vector2_t328513675  get_scale_7() const { return ___scale_7; }
	inline Vector2_t328513675 * get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(Vector2_t328513675  value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_8() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___scrollWheelFactor_8)); }
	inline float get_scrollWheelFactor_8() const { return ___scrollWheelFactor_8; }
	inline float* get_address_of_scrollWheelFactor_8() { return &___scrollWheelFactor_8; }
	inline void set_scrollWheelFactor_8(float value)
	{
		___scrollWheelFactor_8 = value;
	}

	inline static int32_t get_offset_of_dragEffect_9() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___dragEffect_9)); }
	inline int32_t get_dragEffect_9() const { return ___dragEffect_9; }
	inline int32_t* get_address_of_dragEffect_9() { return &___dragEffect_9; }
	inline void set_dragEffect_9(int32_t value)
	{
		___dragEffect_9 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_10() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___momentumAmount_10)); }
	inline float get_momentumAmount_10() const { return ___momentumAmount_10; }
	inline float* get_address_of_momentumAmount_10() { return &___momentumAmount_10; }
	inline void set_momentumAmount_10(float value)
	{
		___momentumAmount_10 = value;
	}

	inline static int32_t get_offset_of_mCam_11() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mCam_11)); }
	inline Camera_t2839736942 * get_mCam_11() const { return ___mCam_11; }
	inline Camera_t2839736942 ** get_address_of_mCam_11() { return &___mCam_11; }
	inline void set_mCam_11(Camera_t2839736942 * value)
	{
		___mCam_11 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_11), value);
	}

	inline static int32_t get_offset_of_mTrans_12() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mTrans_12)); }
	inline Transform_t362059596 * get_mTrans_12() const { return ___mTrans_12; }
	inline Transform_t362059596 ** get_address_of_mTrans_12() { return &___mTrans_12; }
	inline void set_mTrans_12(Transform_t362059596 * value)
	{
		___mTrans_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_12), value);
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mMomentum_14() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mMomentum_14)); }
	inline Vector2_t328513675  get_mMomentum_14() const { return ___mMomentum_14; }
	inline Vector2_t328513675 * get_address_of_mMomentum_14() { return &___mMomentum_14; }
	inline void set_mMomentum_14(Vector2_t328513675  value)
	{
		___mMomentum_14 = value;
	}

	inline static int32_t get_offset_of_mBounds_15() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mBounds_15)); }
	inline Bounds_t3570137099  get_mBounds_15() const { return ___mBounds_15; }
	inline Bounds_t3570137099 * get_address_of_mBounds_15() { return &___mBounds_15; }
	inline void set_mBounds_15(Bounds_t3570137099  value)
	{
		___mBounds_15 = value;
	}

	inline static int32_t get_offset_of_mScroll_16() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mScroll_16)); }
	inline float get_mScroll_16() const { return ___mScroll_16; }
	inline float* get_address_of_mScroll_16() { return &___mScroll_16; }
	inline void set_mScroll_16(float value)
	{
		___mScroll_16 = value;
	}

	inline static int32_t get_offset_of_mRoot_17() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1133134831, ___mRoot_17)); }
	inline UIRoot_t937406327 * get_mRoot_17() const { return ___mRoot_17; }
	inline UIRoot_t937406327 ** get_address_of_mRoot_17() { return &___mRoot_17; }
	inline void set_mRoot_17(UIRoot_t937406327 * value)
	{
		___mRoot_17 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGGABLECAMERA_T1133134831_H
#ifndef UISTORAGESLOT_T1081780882_H
#define UISTORAGESLOT_T1081780882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStorageSlot
struct  UIStorageSlot_t1081780882  : public UIItemSlot_t3241389696
{
public:
	// UIItemStorage UIStorageSlot::storage
	UIItemStorage_t1447122887 * ___storage_11;
	// System.Int32 UIStorageSlot::slot
	int32_t ___slot_12;

public:
	inline static int32_t get_offset_of_storage_11() { return static_cast<int32_t>(offsetof(UIStorageSlot_t1081780882, ___storage_11)); }
	inline UIItemStorage_t1447122887 * get_storage_11() const { return ___storage_11; }
	inline UIItemStorage_t1447122887 ** get_address_of_storage_11() { return &___storage_11; }
	inline void set_storage_11(UIItemStorage_t1447122887 * value)
	{
		___storage_11 = value;
		Il2CppCodeGenWriteBarrier((&___storage_11), value);
	}

	inline static int32_t get_offset_of_slot_12() { return static_cast<int32_t>(offsetof(UIStorageSlot_t1081780882, ___slot_12)); }
	inline int32_t get_slot_12() const { return ___slot_12; }
	inline int32_t* get_address_of_slot_12() { return &___slot_12; }
	inline void set_slot_12(int32_t value)
	{
		___slot_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTORAGESLOT_T1081780882_H
#ifndef UIEQUIPMENTSLOT_T365280452_H
#define UIEQUIPMENTSLOT_T365280452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEquipmentSlot
struct  UIEquipmentSlot_t365280452  : public UIItemSlot_t3241389696
{
public:
	// InvEquipment UIEquipmentSlot::equipment
	InvEquipment_t2308977056 * ___equipment_11;
	// InvBaseItem/Slot UIEquipmentSlot::slot
	int32_t ___slot_12;

public:
	inline static int32_t get_offset_of_equipment_11() { return static_cast<int32_t>(offsetof(UIEquipmentSlot_t365280452, ___equipment_11)); }
	inline InvEquipment_t2308977056 * get_equipment_11() const { return ___equipment_11; }
	inline InvEquipment_t2308977056 ** get_address_of_equipment_11() { return &___equipment_11; }
	inline void set_equipment_11(InvEquipment_t2308977056 * value)
	{
		___equipment_11 = value;
		Il2CppCodeGenWriteBarrier((&___equipment_11), value);
	}

	inline static int32_t get_offset_of_slot_12() { return static_cast<int32_t>(offsetof(UIEquipmentSlot_t365280452, ___slot_12)); }
	inline int32_t get_slot_12() const { return ___slot_12; }
	inline int32_t* get_address_of_slot_12() { return &___slot_12; }
	inline void set_slot_12(int32_t value)
	{
		___slot_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEQUIPMENTSLOT_T365280452_H
#ifndef PANWITHMOUSE_T1133239269_H
#define PANWITHMOUSE_T1133239269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanWithMouse
struct  PanWithMouse_t1133239269  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Vector2 PanWithMouse::degrees
	Vector2_t328513675  ___degrees_6;
	// System.Single PanWithMouse::range
	float ___range_7;
	// UnityEngine.Transform PanWithMouse::mTrans
	Transform_t362059596 * ___mTrans_8;
	// UnityEngine.Quaternion PanWithMouse::mStart
	Quaternion_t704191599  ___mStart_9;
	// UnityEngine.Vector2 PanWithMouse::mRot
	Vector2_t328513675  ___mRot_10;

public:
	inline static int32_t get_offset_of_degrees_6() { return static_cast<int32_t>(offsetof(PanWithMouse_t1133239269, ___degrees_6)); }
	inline Vector2_t328513675  get_degrees_6() const { return ___degrees_6; }
	inline Vector2_t328513675 * get_address_of_degrees_6() { return &___degrees_6; }
	inline void set_degrees_6(Vector2_t328513675  value)
	{
		___degrees_6 = value;
	}

	inline static int32_t get_offset_of_range_7() { return static_cast<int32_t>(offsetof(PanWithMouse_t1133239269, ___range_7)); }
	inline float get_range_7() const { return ___range_7; }
	inline float* get_address_of_range_7() { return &___range_7; }
	inline void set_range_7(float value)
	{
		___range_7 = value;
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(PanWithMouse_t1133239269, ___mTrans_8)); }
	inline Transform_t362059596 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t362059596 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t362059596 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mStart_9() { return static_cast<int32_t>(offsetof(PanWithMouse_t1133239269, ___mStart_9)); }
	inline Quaternion_t704191599  get_mStart_9() const { return ___mStart_9; }
	inline Quaternion_t704191599 * get_address_of_mStart_9() { return &___mStart_9; }
	inline void set_mStart_9(Quaternion_t704191599  value)
	{
		___mStart_9 = value;
	}

	inline static int32_t get_offset_of_mRot_10() { return static_cast<int32_t>(offsetof(PanWithMouse_t1133239269, ___mRot_10)); }
	inline Vector2_t328513675  get_mRot_10() const { return ___mRot_10; }
	inline Vector2_t328513675 * get_address_of_mRot_10() { return &___mRot_10; }
	inline void set_mRot_10(Vector2_t328513675  value)
	{
		___mRot_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANWITHMOUSE_T1133239269_H
#ifndef UIBUTTON_T1486492149_H
#define UIBUTTON_T1486492149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButton
struct  UIButton_t1486492149  : public UIButtonColor_t2620095936
{
public:
	// UnityEngine.Color UIButton::disabledColor
	Color_t2582018970  ___disabledColor_10;

public:
	inline static int32_t get_offset_of_disabledColor_10() { return static_cast<int32_t>(offsetof(UIButton_t1486492149, ___disabledColor_10)); }
	inline Color_t2582018970  get_disabledColor_10() const { return ___disabledColor_10; }
	inline Color_t2582018970 * get_address_of_disabledColor_10() { return &___disabledColor_10; }
	inline void set_disabledColor_10(Color_t2582018970  value)
	{
		___disabledColor_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTON_T1486492149_H
#ifndef ACTIVEANIMATION_T1706595508_H
#define ACTIVEANIMATION_T1706595508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveAnimation
struct  ActiveAnimation_t1706595508  : public IgnoreTimeScale_t640346767
{
public:
	// ActiveAnimation/OnFinished ActiveAnimation::onFinished
	OnFinished_t1828027647 * ___onFinished_6;
	// UnityEngine.GameObject ActiveAnimation::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_7;
	// System.String ActiveAnimation::callWhenFinished
	String_t* ___callWhenFinished_8;
	// UnityEngine.Animation ActiveAnimation::mAnim
	Animation_t3821138400 * ___mAnim_9;
	// AnimationOrTween.Direction ActiveAnimation::mLastDirection
	int32_t ___mLastDirection_10;
	// AnimationOrTween.Direction ActiveAnimation::mDisableDirection
	int32_t ___mDisableDirection_11;
	// System.Boolean ActiveAnimation::mNotify
	bool ___mNotify_12;

public:
	inline static int32_t get_offset_of_onFinished_6() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___onFinished_6)); }
	inline OnFinished_t1828027647 * get_onFinished_6() const { return ___onFinished_6; }
	inline OnFinished_t1828027647 ** get_address_of_onFinished_6() { return &___onFinished_6; }
	inline void set_onFinished_6(OnFinished_t1828027647 * value)
	{
		___onFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_6), value);
	}

	inline static int32_t get_offset_of_eventReceiver_7() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___eventReceiver_7)); }
	inline GameObject_t2557347079 * get_eventReceiver_7() const { return ___eventReceiver_7; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_7() { return &___eventReceiver_7; }
	inline void set_eventReceiver_7(GameObject_t2557347079 * value)
	{
		___eventReceiver_7 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_7), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_8() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___callWhenFinished_8)); }
	inline String_t* get_callWhenFinished_8() const { return ___callWhenFinished_8; }
	inline String_t** get_address_of_callWhenFinished_8() { return &___callWhenFinished_8; }
	inline void set_callWhenFinished_8(String_t* value)
	{
		___callWhenFinished_8 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_8), value);
	}

	inline static int32_t get_offset_of_mAnim_9() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___mAnim_9)); }
	inline Animation_t3821138400 * get_mAnim_9() const { return ___mAnim_9; }
	inline Animation_t3821138400 ** get_address_of_mAnim_9() { return &___mAnim_9; }
	inline void set_mAnim_9(Animation_t3821138400 * value)
	{
		___mAnim_9 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_9), value);
	}

	inline static int32_t get_offset_of_mLastDirection_10() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___mLastDirection_10)); }
	inline int32_t get_mLastDirection_10() const { return ___mLastDirection_10; }
	inline int32_t* get_address_of_mLastDirection_10() { return &___mLastDirection_10; }
	inline void set_mLastDirection_10(int32_t value)
	{
		___mLastDirection_10 = value;
	}

	inline static int32_t get_offset_of_mDisableDirection_11() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___mDisableDirection_11)); }
	inline int32_t get_mDisableDirection_11() const { return ___mDisableDirection_11; }
	inline int32_t* get_address_of_mDisableDirection_11() { return &___mDisableDirection_11; }
	inline void set_mDisableDirection_11(int32_t value)
	{
		___mDisableDirection_11 = value;
	}

	inline static int32_t get_offset_of_mNotify_12() { return static_cast<int32_t>(offsetof(ActiveAnimation_t1706595508, ___mNotify_12)); }
	inline bool get_mNotify_12() const { return ___mNotify_12; }
	inline bool* get_address_of_mNotify_12() { return &___mNotify_12; }
	inline void set_mNotify_12(bool value)
	{
		___mNotify_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEANIMATION_T1706595508_H
#ifndef UISLIDER_T1813774206_H
#define UISLIDER_T1813774206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlider
struct  UISlider_t1813774206  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Transform UISlider::foreground
	Transform_t362059596 * ___foreground_7;
	// UnityEngine.Transform UISlider::thumb
	Transform_t362059596 * ___thumb_8;
	// UISlider/Direction UISlider::direction
	int32_t ___direction_9;
	// UnityEngine.Vector2 UISlider::fullSize
	Vector2_t328513675  ___fullSize_10;
	// UnityEngine.GameObject UISlider::eventReceiver
	GameObject_t2557347079 * ___eventReceiver_11;
	// System.String UISlider::functionName
	String_t* ___functionName_12;
	// UISlider/OnValueChange UISlider::onValueChange
	OnValueChange_t3441845912 * ___onValueChange_13;
	// System.Int32 UISlider::numberOfSteps
	int32_t ___numberOfSteps_14;
	// System.Single UISlider::rawValue
	float ___rawValue_15;
	// System.Single UISlider::mStepValue
	float ___mStepValue_16;
	// UnityEngine.BoxCollider UISlider::mCol
	BoxCollider_t1717529639 * ___mCol_17;
	// UnityEngine.Transform UISlider::mTrans
	Transform_t362059596 * ___mTrans_18;
	// UnityEngine.Transform UISlider::mFGTrans
	Transform_t362059596 * ___mFGTrans_19;
	// UIWidget UISlider::mFGWidget
	UIWidget_t1961100141 * ___mFGWidget_20;
	// UIFilledSprite UISlider::mFGFilled
	UIFilledSprite_t3395723239 * ___mFGFilled_21;
	// System.Boolean UISlider::mInitDone
	bool ___mInitDone_22;

public:
	inline static int32_t get_offset_of_foreground_7() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___foreground_7)); }
	inline Transform_t362059596 * get_foreground_7() const { return ___foreground_7; }
	inline Transform_t362059596 ** get_address_of_foreground_7() { return &___foreground_7; }
	inline void set_foreground_7(Transform_t362059596 * value)
	{
		___foreground_7 = value;
		Il2CppCodeGenWriteBarrier((&___foreground_7), value);
	}

	inline static int32_t get_offset_of_thumb_8() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___thumb_8)); }
	inline Transform_t362059596 * get_thumb_8() const { return ___thumb_8; }
	inline Transform_t362059596 ** get_address_of_thumb_8() { return &___thumb_8; }
	inline void set_thumb_8(Transform_t362059596 * value)
	{
		___thumb_8 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_8), value);
	}

	inline static int32_t get_offset_of_direction_9() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___direction_9)); }
	inline int32_t get_direction_9() const { return ___direction_9; }
	inline int32_t* get_address_of_direction_9() { return &___direction_9; }
	inline void set_direction_9(int32_t value)
	{
		___direction_9 = value;
	}

	inline static int32_t get_offset_of_fullSize_10() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___fullSize_10)); }
	inline Vector2_t328513675  get_fullSize_10() const { return ___fullSize_10; }
	inline Vector2_t328513675 * get_address_of_fullSize_10() { return &___fullSize_10; }
	inline void set_fullSize_10(Vector2_t328513675  value)
	{
		___fullSize_10 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_11() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___eventReceiver_11)); }
	inline GameObject_t2557347079 * get_eventReceiver_11() const { return ___eventReceiver_11; }
	inline GameObject_t2557347079 ** get_address_of_eventReceiver_11() { return &___eventReceiver_11; }
	inline void set_eventReceiver_11(GameObject_t2557347079 * value)
	{
		___eventReceiver_11 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_11), value);
	}

	inline static int32_t get_offset_of_functionName_12() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___functionName_12)); }
	inline String_t* get_functionName_12() const { return ___functionName_12; }
	inline String_t** get_address_of_functionName_12() { return &___functionName_12; }
	inline void set_functionName_12(String_t* value)
	{
		___functionName_12 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_12), value);
	}

	inline static int32_t get_offset_of_onValueChange_13() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___onValueChange_13)); }
	inline OnValueChange_t3441845912 * get_onValueChange_13() const { return ___onValueChange_13; }
	inline OnValueChange_t3441845912 ** get_address_of_onValueChange_13() { return &___onValueChange_13; }
	inline void set_onValueChange_13(OnValueChange_t3441845912 * value)
	{
		___onValueChange_13 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChange_13), value);
	}

	inline static int32_t get_offset_of_numberOfSteps_14() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___numberOfSteps_14)); }
	inline int32_t get_numberOfSteps_14() const { return ___numberOfSteps_14; }
	inline int32_t* get_address_of_numberOfSteps_14() { return &___numberOfSteps_14; }
	inline void set_numberOfSteps_14(int32_t value)
	{
		___numberOfSteps_14 = value;
	}

	inline static int32_t get_offset_of_rawValue_15() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___rawValue_15)); }
	inline float get_rawValue_15() const { return ___rawValue_15; }
	inline float* get_address_of_rawValue_15() { return &___rawValue_15; }
	inline void set_rawValue_15(float value)
	{
		___rawValue_15 = value;
	}

	inline static int32_t get_offset_of_mStepValue_16() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mStepValue_16)); }
	inline float get_mStepValue_16() const { return ___mStepValue_16; }
	inline float* get_address_of_mStepValue_16() { return &___mStepValue_16; }
	inline void set_mStepValue_16(float value)
	{
		___mStepValue_16 = value;
	}

	inline static int32_t get_offset_of_mCol_17() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mCol_17)); }
	inline BoxCollider_t1717529639 * get_mCol_17() const { return ___mCol_17; }
	inline BoxCollider_t1717529639 ** get_address_of_mCol_17() { return &___mCol_17; }
	inline void set_mCol_17(BoxCollider_t1717529639 * value)
	{
		___mCol_17 = value;
		Il2CppCodeGenWriteBarrier((&___mCol_17), value);
	}

	inline static int32_t get_offset_of_mTrans_18() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mTrans_18)); }
	inline Transform_t362059596 * get_mTrans_18() const { return ___mTrans_18; }
	inline Transform_t362059596 ** get_address_of_mTrans_18() { return &___mTrans_18; }
	inline void set_mTrans_18(Transform_t362059596 * value)
	{
		___mTrans_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_18), value);
	}

	inline static int32_t get_offset_of_mFGTrans_19() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mFGTrans_19)); }
	inline Transform_t362059596 * get_mFGTrans_19() const { return ___mFGTrans_19; }
	inline Transform_t362059596 ** get_address_of_mFGTrans_19() { return &___mFGTrans_19; }
	inline void set_mFGTrans_19(Transform_t362059596 * value)
	{
		___mFGTrans_19 = value;
		Il2CppCodeGenWriteBarrier((&___mFGTrans_19), value);
	}

	inline static int32_t get_offset_of_mFGWidget_20() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mFGWidget_20)); }
	inline UIWidget_t1961100141 * get_mFGWidget_20() const { return ___mFGWidget_20; }
	inline UIWidget_t1961100141 ** get_address_of_mFGWidget_20() { return &___mFGWidget_20; }
	inline void set_mFGWidget_20(UIWidget_t1961100141 * value)
	{
		___mFGWidget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mFGWidget_20), value);
	}

	inline static int32_t get_offset_of_mFGFilled_21() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mFGFilled_21)); }
	inline UIFilledSprite_t3395723239 * get_mFGFilled_21() const { return ___mFGFilled_21; }
	inline UIFilledSprite_t3395723239 ** get_address_of_mFGFilled_21() { return &___mFGFilled_21; }
	inline void set_mFGFilled_21(UIFilledSprite_t3395723239 * value)
	{
		___mFGFilled_21 = value;
		Il2CppCodeGenWriteBarrier((&___mFGFilled_21), value);
	}

	inline static int32_t get_offset_of_mInitDone_22() { return static_cast<int32_t>(offsetof(UISlider_t1813774206, ___mInitDone_22)); }
	inline bool get_mInitDone_22() const { return ___mInitDone_22; }
	inline bool* get_address_of_mInitDone_22() { return &___mInitDone_22; }
	inline void set_mInitDone_22(bool value)
	{
		___mInitDone_22 = value;
	}
};

struct UISlider_t1813774206_StaticFields
{
public:
	// UISlider UISlider::current
	UISlider_t1813774206 * ___current_6;

public:
	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(UISlider_t1813774206_StaticFields, ___current_6)); }
	inline UISlider_t1813774206 * get_current_6() const { return ___current_6; }
	inline UISlider_t1813774206 ** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(UISlider_t1813774206 * value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((&___current_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDER_T1813774206_H
#ifndef UIDRAGOBJECT_T2695008364_H
#define UIDRAGOBJECT_T2695008364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragObject
struct  UIDragObject_t2695008364  : public IgnoreTimeScale_t640346767
{
public:
	// UnityEngine.Transform UIDragObject::target
	Transform_t362059596 * ___target_6;
	// UnityEngine.Vector3 UIDragObject::scale
	Vector3_t1986933152  ___scale_7;
	// System.Single UIDragObject::scrollWheelFactor
	float ___scrollWheelFactor_8;
	// System.Boolean UIDragObject::restrictWithinPanel
	bool ___restrictWithinPanel_9;
	// UIDragObject/DragEffect UIDragObject::dragEffect
	int32_t ___dragEffect_10;
	// System.Single UIDragObject::momentumAmount
	float ___momentumAmount_11;
	// UnityEngine.Plane UIDragObject::mPlane
	Plane_t3184513474  ___mPlane_12;
	// UnityEngine.Vector3 UIDragObject::mLastPos
	Vector3_t1986933152  ___mLastPos_13;
	// UIPanel UIDragObject::mPanel
	UIPanel_t825970685 * ___mPanel_14;
	// System.Boolean UIDragObject::mPressed
	bool ___mPressed_15;
	// UnityEngine.Vector3 UIDragObject::mMomentum
	Vector3_t1986933152  ___mMomentum_16;
	// System.Single UIDragObject::mScroll
	float ___mScroll_17;
	// UnityEngine.Bounds UIDragObject::mBounds
	Bounds_t3570137099  ___mBounds_18;

public:
	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___target_6)); }
	inline Transform_t362059596 * get_target_6() const { return ___target_6; }
	inline Transform_t362059596 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t362059596 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___scale_7)); }
	inline Vector3_t1986933152  get_scale_7() const { return ___scale_7; }
	inline Vector3_t1986933152 * get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(Vector3_t1986933152  value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_8() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___scrollWheelFactor_8)); }
	inline float get_scrollWheelFactor_8() const { return ___scrollWheelFactor_8; }
	inline float* get_address_of_scrollWheelFactor_8() { return &___scrollWheelFactor_8; }
	inline void set_scrollWheelFactor_8(float value)
	{
		___scrollWheelFactor_8 = value;
	}

	inline static int32_t get_offset_of_restrictWithinPanel_9() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___restrictWithinPanel_9)); }
	inline bool get_restrictWithinPanel_9() const { return ___restrictWithinPanel_9; }
	inline bool* get_address_of_restrictWithinPanel_9() { return &___restrictWithinPanel_9; }
	inline void set_restrictWithinPanel_9(bool value)
	{
		___restrictWithinPanel_9 = value;
	}

	inline static int32_t get_offset_of_dragEffect_10() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___dragEffect_10)); }
	inline int32_t get_dragEffect_10() const { return ___dragEffect_10; }
	inline int32_t* get_address_of_dragEffect_10() { return &___dragEffect_10; }
	inline void set_dragEffect_10(int32_t value)
	{
		___dragEffect_10 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_11() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___momentumAmount_11)); }
	inline float get_momentumAmount_11() const { return ___momentumAmount_11; }
	inline float* get_address_of_momentumAmount_11() { return &___momentumAmount_11; }
	inline void set_momentumAmount_11(float value)
	{
		___momentumAmount_11 = value;
	}

	inline static int32_t get_offset_of_mPlane_12() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mPlane_12)); }
	inline Plane_t3184513474  get_mPlane_12() const { return ___mPlane_12; }
	inline Plane_t3184513474 * get_address_of_mPlane_12() { return &___mPlane_12; }
	inline void set_mPlane_12(Plane_t3184513474  value)
	{
		___mPlane_12 = value;
	}

	inline static int32_t get_offset_of_mLastPos_13() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mLastPos_13)); }
	inline Vector3_t1986933152  get_mLastPos_13() const { return ___mLastPos_13; }
	inline Vector3_t1986933152 * get_address_of_mLastPos_13() { return &___mLastPos_13; }
	inline void set_mLastPos_13(Vector3_t1986933152  value)
	{
		___mLastPos_13 = value;
	}

	inline static int32_t get_offset_of_mPanel_14() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mPanel_14)); }
	inline UIPanel_t825970685 * get_mPanel_14() const { return ___mPanel_14; }
	inline UIPanel_t825970685 ** get_address_of_mPanel_14() { return &___mPanel_14; }
	inline void set_mPanel_14(UIPanel_t825970685 * value)
	{
		___mPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_14), value);
	}

	inline static int32_t get_offset_of_mPressed_15() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mPressed_15)); }
	inline bool get_mPressed_15() const { return ___mPressed_15; }
	inline bool* get_address_of_mPressed_15() { return &___mPressed_15; }
	inline void set_mPressed_15(bool value)
	{
		___mPressed_15 = value;
	}

	inline static int32_t get_offset_of_mMomentum_16() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mMomentum_16)); }
	inline Vector3_t1986933152  get_mMomentum_16() const { return ___mMomentum_16; }
	inline Vector3_t1986933152 * get_address_of_mMomentum_16() { return &___mMomentum_16; }
	inline void set_mMomentum_16(Vector3_t1986933152  value)
	{
		___mMomentum_16 = value;
	}

	inline static int32_t get_offset_of_mScroll_17() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mScroll_17)); }
	inline float get_mScroll_17() const { return ___mScroll_17; }
	inline float* get_address_of_mScroll_17() { return &___mScroll_17; }
	inline void set_mScroll_17(float value)
	{
		___mScroll_17 = value;
	}

	inline static int32_t get_offset_of_mBounds_18() { return static_cast<int32_t>(offsetof(UIDragObject_t2695008364, ___mBounds_18)); }
	inline Bounds_t3570137099  get_mBounds_18() const { return ___mBounds_18; }
	inline Bounds_t3570137099 * get_address_of_mBounds_18() { return &___mBounds_18; }
	inline void set_mBounds_18(Bounds_t3570137099  value)
	{
		___mBounds_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGOBJECT_T2695008364_H
#ifndef UIDRAGCAMERA_T2166085479_H
#define UIDRAGCAMERA_T2166085479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragCamera
struct  UIDragCamera_t2166085479  : public IgnoreTimeScale_t640346767
{
public:
	// UIDraggableCamera UIDragCamera::draggableCamera
	UIDraggableCamera_t1133134831 * ___draggableCamera_6;
	// UnityEngine.Component UIDragCamera::target
	Component_t1632713610 * ___target_7;

public:
	inline static int32_t get_offset_of_draggableCamera_6() { return static_cast<int32_t>(offsetof(UIDragCamera_t2166085479, ___draggableCamera_6)); }
	inline UIDraggableCamera_t1133134831 * get_draggableCamera_6() const { return ___draggableCamera_6; }
	inline UIDraggableCamera_t1133134831 ** get_address_of_draggableCamera_6() { return &___draggableCamera_6; }
	inline void set_draggableCamera_6(UIDraggableCamera_t1133134831 * value)
	{
		___draggableCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___draggableCamera_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(UIDragCamera_t2166085479, ___target_7)); }
	inline Component_t1632713610 * get_target_7() const { return ___target_7; }
	inline Component_t1632713610 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Component_t1632713610 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGCAMERA_T2166085479_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (Map_t3311971743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[3] = 
{
	Map_t3311971743::get_offset_of_provinces_2(),
	Map_t3311971743::get_offset_of_lockedAlpha_3(),
	Map_t3311971743::get_offset_of_controls_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (MapCanvas_t3396692108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[3] = 
{
	MapCanvas_t3396692108::get_offset_of_map_2(),
	MapCanvas_t3396692108::get_offset_of_mapButton_3(),
	MapCanvas_t3396692108::get_offset_of_hideWhenCantMove_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (MatchGnatsScreen_t2040488255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[7] = 
{
	MatchGnatsScreen_t2040488255::get_offset_of_snapPoints_2(),
	MatchGnatsScreen_t2040488255::get_offset_of_dragAndDrops_3(),
	MatchGnatsScreen_t2040488255::get_offset_of_descriptions_4(),
	MatchGnatsScreen_t2040488255::get_offset_of_nextButton_5(),
	MatchGnatsScreen_t2040488255::get_offset_of_snapDistance_6(),
	MatchGnatsScreen_t2040488255::get_offset_of_isFinished_7(),
	MatchGnatsScreen_t2040488255::get_offset_of_overrideFinished_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (DragableInformation_t2270631114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	DragableInformation_t2270631114::get_offset_of_obj_0(),
	DragableInformation_t2270631114::get_offset_of_targetTransform_1(),
	DragableInformation_t2270631114::get_offset_of_correctAnswer_2(),
	DragableInformation_t2270631114::get_offset_of_placedCorrectly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (MatchMultipleLessons_t1564823048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[15] = 
{
	MatchMultipleLessons_t1564823048::get_offset_of_sentencePrefab_2(),
	MatchMultipleLessons_t1564823048::get_offset_of_gap_3(),
	MatchMultipleLessons_t1564823048::get_offset_of_leftSpawner_4(),
	MatchMultipleLessons_t1564823048::get_offset_of_rightSpawner_5(),
	MatchMultipleLessons_t1564823048::get_offset_of_howDoesItRelate_6(),
	MatchMultipleLessons_t1564823048::get_offset_of_whatsTheMessage_7(),
	MatchMultipleLessons_t1564823048::get_offset_of_questionText_8(),
	MatchMultipleLessons_t1564823048::get_offset_of_nextButton_9(),
	MatchMultipleLessons_t1564823048::get_offset_of_draggables_10(),
	MatchMultipleLessons_t1564823048::get_offset_of_questions_11(),
	MatchMultipleLessons_t1564823048::get_offset_of_snapDistance_12(),
	MatchMultipleLessons_t1564823048::get_offset_of_bFinished_13(),
	MatchMultipleLessons_t1564823048::get_offset_of_numAnswersIndex_14(),
	MatchMultipleLessons_t1564823048::get_offset_of_currentAnswerIndex_15(),
	MatchMultipleLessons_t1564823048::get_offset_of_overrideDone_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (MobileControlEnabler_t4087415719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[2] = 
{
	MobileControlEnabler_t4087415719::get_offset_of_moveImage_2(),
	MobileControlEnabler_t4087415719::get_offset_of_jumpImage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (MoodGraph_t2484840849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	MoodGraph_t2484840849::get_offset_of_bars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (EMultiChoice_t2282345180)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[6] = 
{
	EMultiChoice_t2282345180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (MultiChoiceBox_t1681099465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[14] = 
{
	MultiChoiceBox_t1681099465::get_offset_of_nextButton_2(),
	MultiChoiceBox_t1681099465::get_offset_of_previousButton_3(),
	MultiChoiceBox_t1681099465::get_offset_of_characterName_4(),
	MultiChoiceBox_t1681099465::get_offset_of_dialogueText_5(),
	MultiChoiceBox_t1681099465::get_offset_of_choices_6(),
	MultiChoiceBox_t1681099465::get_offset_of_selectedColour_7(),
	MultiChoiceBox_t1681099465::get_offset_of_dialogueImage_8(),
	MultiChoiceBox_t1681099465::get_offset_of_choiceOffest_9(),
	MultiChoiceBox_t1681099465::get_offset_of_overrideSelfHide_10(),
	MultiChoiceBox_t1681099465::get_offset_of_nextClicked_11(),
	MultiChoiceBox_t1681099465::get_offset_of_previousClicked_12(),
	MultiChoiceBox_t1681099465::get_offset_of_selectedChoices_13(),
	MultiChoiceBox_t1681099465::get_offset_of_normalChoiceColour_14(),
	MultiChoiceBox_t1681099465::get_offset_of_lastChosen_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (NextScreen_t2084349028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[1] = 
{
	NextScreen_t2084349028::get_offset_of_nextClicked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (EquipItems_t650956023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	EquipItems_t650956023::get_offset_of_itemIDs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (EquipRandomItem_t1811729489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	EquipRandomItem_t1811729489::get_offset_of_equipment_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (UICursor_t3371638195), -1, sizeof(UICursor_t3371638195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[6] = 
{
	UICursor_t3371638195_StaticFields::get_offset_of_mInstance_2(),
	UICursor_t3371638195::get_offset_of_uiCamera_3(),
	UICursor_t3371638195::get_offset_of_mTrans_4(),
	UICursor_t3371638195::get_offset_of_mSprite_5(),
	UICursor_t3371638195::get_offset_of_mAtlas_6(),
	UICursor_t3371638195::get_offset_of_mSpriteName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (UIEquipmentSlot_t365280452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	UIEquipmentSlot_t365280452::get_offset_of_equipment_11(),
	UIEquipmentSlot_t365280452::get_offset_of_slot_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (UIItemSlot_t3241389696), -1, sizeof(UIItemSlot_t3241389696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[9] = 
{
	UIItemSlot_t3241389696::get_offset_of_icon_2(),
	UIItemSlot_t3241389696::get_offset_of_background_3(),
	UIItemSlot_t3241389696::get_offset_of_label_4(),
	UIItemSlot_t3241389696::get_offset_of_grabSound_5(),
	UIItemSlot_t3241389696::get_offset_of_placeSound_6(),
	UIItemSlot_t3241389696::get_offset_of_errorSound_7(),
	UIItemSlot_t3241389696::get_offset_of_mItem_8(),
	UIItemSlot_t3241389696::get_offset_of_mText_9(),
	UIItemSlot_t3241389696_StaticFields::get_offset_of_mDraggedItem_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (UIItemStorage_t1447122887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[8] = 
{
	UIItemStorage_t1447122887::get_offset_of_maxItemCount_2(),
	UIItemStorage_t1447122887::get_offset_of_maxRows_3(),
	UIItemStorage_t1447122887::get_offset_of_maxColumns_4(),
	UIItemStorage_t1447122887::get_offset_of_template_5(),
	UIItemStorage_t1447122887::get_offset_of_background_6(),
	UIItemStorage_t1447122887::get_offset_of_spacing_7(),
	UIItemStorage_t1447122887::get_offset_of_padding_8(),
	UIItemStorage_t1447122887::get_offset_of_mItems_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (UIStorageSlot_t1081780882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[2] = 
{
	UIStorageSlot_t1081780882::get_offset_of_storage_11(),
	UIStorageSlot_t1081780882::get_offset_of_slot_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (InvAttachmentPoint_t3318177112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	InvAttachmentPoint_t3318177112::get_offset_of_slot_2(),
	InvAttachmentPoint_t3318177112::get_offset_of_mPrefab_3(),
	InvAttachmentPoint_t3318177112::get_offset_of_mChild_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (InvBaseItem_t82225019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[11] = 
{
	InvBaseItem_t82225019::get_offset_of_id16_0(),
	InvBaseItem_t82225019::get_offset_of_name_1(),
	InvBaseItem_t82225019::get_offset_of_description_2(),
	InvBaseItem_t82225019::get_offset_of_slot_3(),
	InvBaseItem_t82225019::get_offset_of_minItemLevel_4(),
	InvBaseItem_t82225019::get_offset_of_maxItemLevel_5(),
	InvBaseItem_t82225019::get_offset_of_stats_6(),
	InvBaseItem_t82225019::get_offset_of_attachment_7(),
	InvBaseItem_t82225019::get_offset_of_color_8(),
	InvBaseItem_t82225019::get_offset_of_iconAtlas_9(),
	InvBaseItem_t82225019::get_offset_of_iconName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Slot_t2525937994)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[10] = 
{
	Slot_t2525937994::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (InvDatabase_t2105022301), -1, sizeof(InvDatabase_t2105022301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[5] = 
{
	InvDatabase_t2105022301_StaticFields::get_offset_of_mList_2(),
	InvDatabase_t2105022301_StaticFields::get_offset_of_mIsDirty_3(),
	InvDatabase_t2105022301::get_offset_of_databaseID_4(),
	InvDatabase_t2105022301::get_offset_of_items_5(),
	InvDatabase_t2105022301::get_offset_of_iconAtlas_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (InvEquipment_t2308977056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[2] = 
{
	InvEquipment_t2308977056::get_offset_of_mItems_2(),
	InvEquipment_t2308977056::get_offset_of_mAttachments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (InvGameItem_t1620673912), -1, sizeof(InvGameItem_t1620673912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2522[5] = 
{
	InvGameItem_t1620673912::get_offset_of_mBaseItemID_0(),
	InvGameItem_t1620673912::get_offset_of_quality_1(),
	InvGameItem_t1620673912::get_offset_of_itemLevel_2(),
	InvGameItem_t1620673912::get_offset_of_mBaseItem_3(),
	InvGameItem_t1620673912_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (Quality_t2755226396)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[14] = 
{
	Quality_t2755226396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (InvStat_t741405100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[3] = 
{
	InvStat_t741405100::get_offset_of_id_0(),
	InvStat_t741405100::get_offset_of_modifier_1(),
	InvStat_t741405100::get_offset_of_amount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (Identifier_t1167502831)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[11] = 
{
	Identifier_t1167502831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Modifier_t140207204)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2526[3] = 
{
	Modifier_t140207204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (ChatInput_t3069896720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[4] = 
{
	ChatInput_t3069896720::get_offset_of_textList_2(),
	ChatInput_t3069896720::get_offset_of_fillWithDummyData_3(),
	ChatInput_t3069896720::get_offset_of_mInput_4(),
	ChatInput_t3069896720::get_offset_of_mIgnoreNextEnter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (DownloadTexture_t1340095241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	DownloadTexture_t1340095241::get_offset_of_url_2(),
	DownloadTexture_t1340095241::get_offset_of_mMat_3(),
	DownloadTexture_t1340095241::get_offset_of_mTex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (U3CStartU3Ec__Iterator0_t2951566387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[5] = 
{
	U3CStartU3Ec__Iterator0_t2951566387::get_offset_of_U3CwwwU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2951566387::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2951566387::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2951566387::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2951566387::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (DragDropContainer_t946801301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (DragDropItem_t1159484558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	DragDropItem_t1159484558::get_offset_of_prefab_2(),
	DragDropItem_t1159484558::get_offset_of_mTrans_3(),
	DragDropItem_t1159484558::get_offset_of_mIsDragging_4(),
	DragDropItem_t1159484558::get_offset_of_mParent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (DragDropRoot_t2133709399), -1, sizeof(DragDropRoot_t2133709399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[1] = 
{
	DragDropRoot_t2133709399_StaticFields::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (DragDropSurface_t3929611577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[1] = 
{
	DragDropSurface_t3929611577::get_offset_of_rotatePlacedObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (LagPosition_t1504341716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[6] = 
{
	LagPosition_t1504341716::get_offset_of_updateOrder_2(),
	LagPosition_t1504341716::get_offset_of_speed_3(),
	LagPosition_t1504341716::get_offset_of_ignoreTimeScale_4(),
	LagPosition_t1504341716::get_offset_of_mTrans_5(),
	LagPosition_t1504341716::get_offset_of_mRelative_6(),
	LagPosition_t1504341716::get_offset_of_mAbsolute_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (LagRotation_t1815997116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[6] = 
{
	LagRotation_t1815997116::get_offset_of_updateOrder_2(),
	LagRotation_t1815997116::get_offset_of_speed_3(),
	LagRotation_t1815997116::get_offset_of_ignoreTimeScale_4(),
	LagRotation_t1815997116::get_offset_of_mTrans_5(),
	LagRotation_t1815997116::get_offset_of_mRelative_6(),
	LagRotation_t1815997116::get_offset_of_mAbsolute_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (LoadLevelOnClick_t1268630639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[1] = 
{
	LoadLevelOnClick_t1268630639::get_offset_of_levelName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (LookAtTarget_t3176889829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	LookAtTarget_t3176889829::get_offset_of_level_2(),
	LookAtTarget_t3176889829::get_offset_of_target_3(),
	LookAtTarget_t3176889829::get_offset_of_speed_4(),
	LookAtTarget_t3176889829::get_offset_of_mTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (PanWithMouse_t1133239269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[5] = 
{
	PanWithMouse_t1133239269::get_offset_of_degrees_6(),
	PanWithMouse_t1133239269::get_offset_of_range_7(),
	PanWithMouse_t1133239269::get_offset_of_mTrans_8(),
	PanWithMouse_t1133239269::get_offset_of_mStart_9(),
	PanWithMouse_t1133239269::get_offset_of_mRot_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (PlayIdleAnimations_t667263987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[5] = 
{
	PlayIdleAnimations_t667263987::get_offset_of_mAnim_2(),
	PlayIdleAnimations_t667263987::get_offset_of_mIdle_3(),
	PlayIdleAnimations_t667263987::get_offset_of_mBreaks_4(),
	PlayIdleAnimations_t667263987::get_offset_of_mNextBreak_5(),
	PlayIdleAnimations_t667263987::get_offset_of_mLastIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (SetColorOnSelection_t1555214854), -1, sizeof(SetColorOnSelection_t1555214854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	SetColorOnSelection_t1555214854::get_offset_of_mWidget_2(),
	SetColorOnSelection_t1555214854_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (ShaderQuality_t2238873719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	ShaderQuality_t2238873719::get_offset_of_mCurrent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (Spin_t1543306399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	Spin_t1543306399::get_offset_of_rotationsPerSecond_2(),
	Spin_t1543306399::get_offset_of_mRb_3(),
	Spin_t1543306399::get_offset_of_mTrans_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (SpinWithMouse_t622040930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[3] = 
{
	SpinWithMouse_t622040930::get_offset_of_target_2(),
	SpinWithMouse_t622040930::get_offset_of_speed_3(),
	SpinWithMouse_t622040930::get_offset_of_mTrans_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (TypewriterEffect_t1609054849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[5] = 
{
	TypewriterEffect_t1609054849::get_offset_of_charsPerSecond_2(),
	TypewriterEffect_t1609054849::get_offset_of_mLabel_3(),
	TypewriterEffect_t1609054849::get_offset_of_mText_4(),
	TypewriterEffect_t1609054849::get_offset_of_mOffset_5(),
	TypewriterEffect_t1609054849::get_offset_of_mNextChar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (WindowAutoYaw_t2079382413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[4] = 
{
	WindowAutoYaw_t2079382413::get_offset_of_updateOrder_2(),
	WindowAutoYaw_t2079382413::get_offset_of_uiCamera_3(),
	WindowAutoYaw_t2079382413::get_offset_of_yawAmount_4(),
	WindowAutoYaw_t2079382413::get_offset_of_mTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (WindowDragTilt_t561804078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[6] = 
{
	WindowDragTilt_t561804078::get_offset_of_updateOrder_2(),
	WindowDragTilt_t561804078::get_offset_of_degrees_3(),
	WindowDragTilt_t561804078::get_offset_of_mLastPos_4(),
	WindowDragTilt_t561804078::get_offset_of_mTrans_5(),
	WindowDragTilt_t561804078::get_offset_of_mAngle_6(),
	WindowDragTilt_t561804078::get_offset_of_mInit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (LanguageSelection_t1687987668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[1] = 
{
	LanguageSelection_t1687987668::get_offset_of_mList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (UIButton_t1486492149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[1] = 
{
	UIButton_t1486492149::get_offset_of_disabledColor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (UIButtonActivate_t301132035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[2] = 
{
	UIButtonActivate_t301132035::get_offset_of_target_2(),
	UIButtonActivate_t301132035::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (UIButtonColor_t2620095936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[8] = 
{
	UIButtonColor_t2620095936::get_offset_of_tweenTarget_2(),
	UIButtonColor_t2620095936::get_offset_of_hover_3(),
	UIButtonColor_t2620095936::get_offset_of_pressed_4(),
	UIButtonColor_t2620095936::get_offset_of_duration_5(),
	UIButtonColor_t2620095936::get_offset_of_mColor_6(),
	UIButtonColor_t2620095936::get_offset_of_mInitDone_7(),
	UIButtonColor_t2620095936::get_offset_of_mStarted_8(),
	UIButtonColor_t2620095936::get_offset_of_mHighlighted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (UIButtonKeys_t2636625883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[6] = 
{
	UIButtonKeys_t2636625883::get_offset_of_startsSelected_2(),
	UIButtonKeys_t2636625883::get_offset_of_selectOnClick_3(),
	UIButtonKeys_t2636625883::get_offset_of_selectOnUp_4(),
	UIButtonKeys_t2636625883::get_offset_of_selectOnDown_5(),
	UIButtonKeys_t2636625883::get_offset_of_selectOnLeft_6(),
	UIButtonKeys_t2636625883::get_offset_of_selectOnRight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (UIButtonMessage_t897303282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[6] = 
{
	UIButtonMessage_t897303282::get_offset_of_target_2(),
	UIButtonMessage_t897303282::get_offset_of_functionName_3(),
	UIButtonMessage_t897303282::get_offset_of_trigger_4(),
	UIButtonMessage_t897303282::get_offset_of_includeChildren_5(),
	UIButtonMessage_t897303282::get_offset_of_mStarted_6(),
	UIButtonMessage_t897303282::get_offset_of_mHighlighted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Trigger_t2084149677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[7] = 
{
	Trigger_t2084149677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (UIButtonOffset_t584960962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[8] = 
{
	UIButtonOffset_t584960962::get_offset_of_tweenTarget_2(),
	UIButtonOffset_t584960962::get_offset_of_hover_3(),
	UIButtonOffset_t584960962::get_offset_of_pressed_4(),
	UIButtonOffset_t584960962::get_offset_of_duration_5(),
	UIButtonOffset_t584960962::get_offset_of_mPos_6(),
	UIButtonOffset_t584960962::get_offset_of_mInitDone_7(),
	UIButtonOffset_t584960962::get_offset_of_mStarted_8(),
	UIButtonOffset_t584960962::get_offset_of_mHighlighted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (UIButtonPlayAnimation_t2818799863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[13] = 
{
	UIButtonPlayAnimation_t2818799863::get_offset_of_target_2(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_clipName_3(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_trigger_4(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_playDirection_5(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_resetOnPlay_6(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_clearSelection_7(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_ifDisabledOnPlay_8(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_disableWhenFinished_9(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_eventReceiver_10(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_callWhenFinished_11(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_onFinished_12(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_mStarted_13(),
	UIButtonPlayAnimation_t2818799863::get_offset_of_mHighlighted_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (UIButtonRotation_t3360562254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[8] = 
{
	UIButtonRotation_t3360562254::get_offset_of_tweenTarget_2(),
	UIButtonRotation_t3360562254::get_offset_of_hover_3(),
	UIButtonRotation_t3360562254::get_offset_of_pressed_4(),
	UIButtonRotation_t3360562254::get_offset_of_duration_5(),
	UIButtonRotation_t3360562254::get_offset_of_mRot_6(),
	UIButtonRotation_t3360562254::get_offset_of_mInitDone_7(),
	UIButtonRotation_t3360562254::get_offset_of_mStarted_8(),
	UIButtonRotation_t3360562254::get_offset_of_mHighlighted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (UIButtonScale_t401580026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[8] = 
{
	UIButtonScale_t401580026::get_offset_of_tweenTarget_2(),
	UIButtonScale_t401580026::get_offset_of_hover_3(),
	UIButtonScale_t401580026::get_offset_of_pressed_4(),
	UIButtonScale_t401580026::get_offset_of_duration_5(),
	UIButtonScale_t401580026::get_offset_of_mScale_6(),
	UIButtonScale_t401580026::get_offset_of_mInitDone_7(),
	UIButtonScale_t401580026::get_offset_of_mStarted_8(),
	UIButtonScale_t401580026::get_offset_of_mHighlighted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (UIButtonSound_t3002556785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[4] = 
{
	UIButtonSound_t3002556785::get_offset_of_audioClip_2(),
	UIButtonSound_t3002556785::get_offset_of_trigger_3(),
	UIButtonSound_t3002556785::get_offset_of_volume_4(),
	UIButtonSound_t3002556785::get_offset_of_pitch_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (Trigger_t338680093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	Trigger_t338680093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (UIButtonTween_t3240537385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[14] = 
{
	UIButtonTween_t3240537385::get_offset_of_tweenTarget_2(),
	UIButtonTween_t3240537385::get_offset_of_tweenGroup_3(),
	UIButtonTween_t3240537385::get_offset_of_trigger_4(),
	UIButtonTween_t3240537385::get_offset_of_playDirection_5(),
	UIButtonTween_t3240537385::get_offset_of_resetOnPlay_6(),
	UIButtonTween_t3240537385::get_offset_of_ifDisabledOnPlay_7(),
	UIButtonTween_t3240537385::get_offset_of_disableWhenFinished_8(),
	UIButtonTween_t3240537385::get_offset_of_includeChildren_9(),
	UIButtonTween_t3240537385::get_offset_of_eventReceiver_10(),
	UIButtonTween_t3240537385::get_offset_of_callWhenFinished_11(),
	UIButtonTween_t3240537385::get_offset_of_onFinished_12(),
	UIButtonTween_t3240537385::get_offset_of_mTweens_13(),
	UIButtonTween_t3240537385::get_offset_of_mStarted_14(),
	UIButtonTween_t3240537385::get_offset_of_mHighlighted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (UICenterOnChild_t1552049414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[3] = 
{
	UICenterOnChild_t1552049414::get_offset_of_onFinished_2(),
	UICenterOnChild_t1552049414::get_offset_of_mDrag_3(),
	UICenterOnChild_t1552049414::get_offset_of_mCenteredObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (UICheckbox_t1205955448), -1, sizeof(UICheckbox_t1205955448_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2562[13] = 
{
	UICheckbox_t1205955448_StaticFields::get_offset_of_current_2(),
	UICheckbox_t1205955448::get_offset_of_checkSprite_3(),
	UICheckbox_t1205955448::get_offset_of_checkAnimation_4(),
	UICheckbox_t1205955448::get_offset_of_startsChecked_5(),
	UICheckbox_t1205955448::get_offset_of_radioButtonRoot_6(),
	UICheckbox_t1205955448::get_offset_of_optionCanBeNone_7(),
	UICheckbox_t1205955448::get_offset_of_eventReceiver_8(),
	UICheckbox_t1205955448::get_offset_of_functionName_9(),
	UICheckbox_t1205955448::get_offset_of_onStateChange_10(),
	UICheckbox_t1205955448::get_offset_of_option_11(),
	UICheckbox_t1205955448::get_offset_of_mChecked_12(),
	UICheckbox_t1205955448::get_offset_of_mStarted_13(),
	UICheckbox_t1205955448::get_offset_of_mTrans_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (OnStateChange_t2569002187), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (UICheckboxControlledComponent_t2988583793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[3] = 
{
	UICheckboxControlledComponent_t2988583793::get_offset_of_target_2(),
	UICheckboxControlledComponent_t2988583793::get_offset_of_inverse_3(),
	UICheckboxControlledComponent_t2988583793::get_offset_of_mUsingDelegates_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (UICheckboxControlledObject_t218782148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	UICheckboxControlledObject_t218782148::get_offset_of_target_2(),
	UICheckboxControlledObject_t218782148::get_offset_of_inverse_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (UIDragCamera_t2166085479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[2] = 
{
	UIDragCamera_t2166085479::get_offset_of_draggableCamera_6(),
	UIDragCamera_t2166085479::get_offset_of_target_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (UIDraggableCamera_t1133134831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[12] = 
{
	UIDraggableCamera_t1133134831::get_offset_of_rootForBounds_6(),
	UIDraggableCamera_t1133134831::get_offset_of_scale_7(),
	UIDraggableCamera_t1133134831::get_offset_of_scrollWheelFactor_8(),
	UIDraggableCamera_t1133134831::get_offset_of_dragEffect_9(),
	UIDraggableCamera_t1133134831::get_offset_of_momentumAmount_10(),
	UIDraggableCamera_t1133134831::get_offset_of_mCam_11(),
	UIDraggableCamera_t1133134831::get_offset_of_mTrans_12(),
	UIDraggableCamera_t1133134831::get_offset_of_mPressed_13(),
	UIDraggableCamera_t1133134831::get_offset_of_mMomentum_14(),
	UIDraggableCamera_t1133134831::get_offset_of_mBounds_15(),
	UIDraggableCamera_t1133134831::get_offset_of_mScroll_16(),
	UIDraggableCamera_t1133134831::get_offset_of_mRoot_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (UIDraggablePanel_t2580300900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[24] = 
{
	UIDraggablePanel_t2580300900::get_offset_of_restrictWithinPanel_6(),
	UIDraggablePanel_t2580300900::get_offset_of_disableDragIfFits_7(),
	UIDraggablePanel_t2580300900::get_offset_of_dragEffect_8(),
	UIDraggablePanel_t2580300900::get_offset_of_scale_9(),
	UIDraggablePanel_t2580300900::get_offset_of_scrollWheelFactor_10(),
	UIDraggablePanel_t2580300900::get_offset_of_momentumAmount_11(),
	UIDraggablePanel_t2580300900::get_offset_of_relativePositionOnReset_12(),
	UIDraggablePanel_t2580300900::get_offset_of_repositionClipping_13(),
	UIDraggablePanel_t2580300900::get_offset_of_horizontalScrollBar_14(),
	UIDraggablePanel_t2580300900::get_offset_of_verticalScrollBar_15(),
	UIDraggablePanel_t2580300900::get_offset_of_showScrollBars_16(),
	UIDraggablePanel_t2580300900::get_offset_of_onDragFinished_17(),
	UIDraggablePanel_t2580300900::get_offset_of_mTrans_18(),
	UIDraggablePanel_t2580300900::get_offset_of_mPanel_19(),
	UIDraggablePanel_t2580300900::get_offset_of_mPlane_20(),
	UIDraggablePanel_t2580300900::get_offset_of_mLastPos_21(),
	UIDraggablePanel_t2580300900::get_offset_of_mPressed_22(),
	UIDraggablePanel_t2580300900::get_offset_of_mMomentum_23(),
	UIDraggablePanel_t2580300900::get_offset_of_mScroll_24(),
	UIDraggablePanel_t2580300900::get_offset_of_mBounds_25(),
	UIDraggablePanel_t2580300900::get_offset_of_mCalculatedBounds_26(),
	UIDraggablePanel_t2580300900::get_offset_of_mShouldMove_27(),
	UIDraggablePanel_t2580300900::get_offset_of_mIgnoreCallbacks_28(),
	UIDraggablePanel_t2580300900::get_offset_of_mDragID_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (DragEffect_t196040209)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[4] = 
{
	DragEffect_t196040209::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ShowCondition_t1844407723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2570[4] = 
{
	ShowCondition_t1844407723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (OnDragFinished_t81282452), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (UIDragObject_t2695008364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[13] = 
{
	UIDragObject_t2695008364::get_offset_of_target_6(),
	UIDragObject_t2695008364::get_offset_of_scale_7(),
	UIDragObject_t2695008364::get_offset_of_scrollWheelFactor_8(),
	UIDragObject_t2695008364::get_offset_of_restrictWithinPanel_9(),
	UIDragObject_t2695008364::get_offset_of_dragEffect_10(),
	UIDragObject_t2695008364::get_offset_of_momentumAmount_11(),
	UIDragObject_t2695008364::get_offset_of_mPlane_12(),
	UIDragObject_t2695008364::get_offset_of_mLastPos_13(),
	UIDragObject_t2695008364::get_offset_of_mPanel_14(),
	UIDragObject_t2695008364::get_offset_of_mPressed_15(),
	UIDragObject_t2695008364::get_offset_of_mMomentum_16(),
	UIDragObject_t2695008364::get_offset_of_mScroll_17(),
	UIDragObject_t2695008364::get_offset_of_mBounds_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (DragEffect_t1559318904)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	DragEffect_t1559318904::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (UIDragPanelContents_t194675207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[2] = 
{
	UIDragPanelContents_t194675207::get_offset_of_draggablePanel_2(),
	UIDragPanelContents_t194675207::get_offset_of_panel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (UIForwardEvents_t3697131846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[11] = 
{
	UIForwardEvents_t3697131846::get_offset_of_target_2(),
	UIForwardEvents_t3697131846::get_offset_of_onHover_3(),
	UIForwardEvents_t3697131846::get_offset_of_onPress_4(),
	UIForwardEvents_t3697131846::get_offset_of_onClick_5(),
	UIForwardEvents_t3697131846::get_offset_of_onDoubleClick_6(),
	UIForwardEvents_t3697131846::get_offset_of_onSelect_7(),
	UIForwardEvents_t3697131846::get_offset_of_onDrag_8(),
	UIForwardEvents_t3697131846::get_offset_of_onDrop_9(),
	UIForwardEvents_t3697131846::get_offset_of_onInput_10(),
	UIForwardEvents_t3697131846::get_offset_of_onSubmit_11(),
	UIForwardEvents_t3697131846::get_offset_of_onScroll_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (UIGrid_t3202738573), -1, sizeof(UIGrid_t3202738573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2576[9] = 
{
	UIGrid_t3202738573::get_offset_of_arrangement_2(),
	UIGrid_t3202738573::get_offset_of_maxPerLine_3(),
	UIGrid_t3202738573::get_offset_of_cellWidth_4(),
	UIGrid_t3202738573::get_offset_of_cellHeight_5(),
	UIGrid_t3202738573::get_offset_of_repositionNow_6(),
	UIGrid_t3202738573::get_offset_of_sorted_7(),
	UIGrid_t3202738573::get_offset_of_hideInactive_8(),
	UIGrid_t3202738573::get_offset_of_mStarted_9(),
	UIGrid_t3202738573_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Arrangement_t680747878)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2577[3] = 
{
	Arrangement_t680747878::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (UIImageButton_t1024317081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[4] = 
{
	UIImageButton_t1024317081::get_offset_of_target_2(),
	UIImageButton_t1024317081::get_offset_of_normalSprite_3(),
	UIImageButton_t1024317081::get_offset_of_hoverSprite_4(),
	UIImageButton_t1024317081::get_offset_of_pressedSprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (UIInputValidator_t2914548063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	UIInputValidator_t2914548063::get_offset_of_logic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (Validation_t2746088341)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[7] = 
{
	Validation_t2746088341::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (UIPanelAlpha_t3764451740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[6] = 
{
	UIPanelAlpha_t3764451740::get_offset_of_alpha_2(),
	UIPanelAlpha_t3764451740::get_offset_of_mColliders_3(),
	UIPanelAlpha_t3764451740::get_offset_of_mWidgets_4(),
	UIPanelAlpha_t3764451740::get_offset_of_mAlpha_5(),
	UIPanelAlpha_t3764451740::get_offset_of_mLastAlpha_6(),
	UIPanelAlpha_t3764451740::get_offset_of_mLevel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (UIPopupList_t2907709271), -1, sizeof(UIPopupList_t2907709271_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2582[27] = 
{
	UIPopupList_t2907709271_StaticFields::get_offset_of_current_2(),
	0,
	UIPopupList_t2907709271::get_offset_of_atlas_4(),
	UIPopupList_t2907709271::get_offset_of_font_5(),
	UIPopupList_t2907709271::get_offset_of_textLabel_6(),
	UIPopupList_t2907709271::get_offset_of_backgroundSprite_7(),
	UIPopupList_t2907709271::get_offset_of_highlightSprite_8(),
	UIPopupList_t2907709271::get_offset_of_position_9(),
	UIPopupList_t2907709271::get_offset_of_items_10(),
	UIPopupList_t2907709271::get_offset_of_padding_11(),
	UIPopupList_t2907709271::get_offset_of_textScale_12(),
	UIPopupList_t2907709271::get_offset_of_textColor_13(),
	UIPopupList_t2907709271::get_offset_of_backgroundColor_14(),
	UIPopupList_t2907709271::get_offset_of_highlightColor_15(),
	UIPopupList_t2907709271::get_offset_of_isAnimated_16(),
	UIPopupList_t2907709271::get_offset_of_isLocalized_17(),
	UIPopupList_t2907709271::get_offset_of_eventReceiver_18(),
	UIPopupList_t2907709271::get_offset_of_functionName_19(),
	UIPopupList_t2907709271::get_offset_of_onSelectionChange_20(),
	UIPopupList_t2907709271::get_offset_of_mSelectedItem_21(),
	UIPopupList_t2907709271::get_offset_of_mPanel_22(),
	UIPopupList_t2907709271::get_offset_of_mChild_23(),
	UIPopupList_t2907709271::get_offset_of_mBackground_24(),
	UIPopupList_t2907709271::get_offset_of_mHighlight_25(),
	UIPopupList_t2907709271::get_offset_of_mHighlightedLabel_26(),
	UIPopupList_t2907709271::get_offset_of_mLabelList_27(),
	UIPopupList_t2907709271::get_offset_of_mBgBorder_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Position_t1317090401)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2583[4] = 
{
	Position_t1317090401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (OnSelectionChange_t705636838), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (UISavedOption_t870911898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[3] = 
{
	UISavedOption_t870911898::get_offset_of_keyName_2(),
	UISavedOption_t870911898::get_offset_of_mList_3(),
	UISavedOption_t870911898::get_offset_of_mCheck_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (UIScrollBar_t3221813871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[11] = 
{
	UIScrollBar_t3221813871::get_offset_of_mBG_2(),
	UIScrollBar_t3221813871::get_offset_of_mFG_3(),
	UIScrollBar_t3221813871::get_offset_of_mDir_4(),
	UIScrollBar_t3221813871::get_offset_of_mInverted_5(),
	UIScrollBar_t3221813871::get_offset_of_mScroll_6(),
	UIScrollBar_t3221813871::get_offset_of_mSize_7(),
	UIScrollBar_t3221813871::get_offset_of_mTrans_8(),
	UIScrollBar_t3221813871::get_offset_of_mIsDirty_9(),
	UIScrollBar_t3221813871::get_offset_of_mCam_10(),
	UIScrollBar_t3221813871::get_offset_of_mScreenPos_11(),
	UIScrollBar_t3221813871::get_offset_of_onChange_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Direction_t1844548834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[3] = 
{
	Direction_t1844548834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (OnScrollBarChange_t2155012818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (UISlider_t1813774206), -1, sizeof(UISlider_t1813774206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[17] = 
{
	UISlider_t1813774206_StaticFields::get_offset_of_current_6(),
	UISlider_t1813774206::get_offset_of_foreground_7(),
	UISlider_t1813774206::get_offset_of_thumb_8(),
	UISlider_t1813774206::get_offset_of_direction_9(),
	UISlider_t1813774206::get_offset_of_fullSize_10(),
	UISlider_t1813774206::get_offset_of_eventReceiver_11(),
	UISlider_t1813774206::get_offset_of_functionName_12(),
	UISlider_t1813774206::get_offset_of_onValueChange_13(),
	UISlider_t1813774206::get_offset_of_numberOfSteps_14(),
	UISlider_t1813774206::get_offset_of_rawValue_15(),
	UISlider_t1813774206::get_offset_of_mStepValue_16(),
	UISlider_t1813774206::get_offset_of_mCol_17(),
	UISlider_t1813774206::get_offset_of_mTrans_18(),
	UISlider_t1813774206::get_offset_of_mFGTrans_19(),
	UISlider_t1813774206::get_offset_of_mFGWidget_20(),
	UISlider_t1813774206::get_offset_of_mFGFilled_21(),
	UISlider_t1813774206::get_offset_of_mInitDone_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (Direction_t598398482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[3] = 
{
	Direction_t598398482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (OnValueChange_t3441845912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (UISliderColors_t1281262261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	UISliderColors_t1281262261::get_offset_of_sprite_2(),
	UISliderColors_t1281262261::get_offset_of_colors_3(),
	UISliderColors_t1281262261::get_offset_of_mSlider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (UISoundVolume_t390417661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	UISoundVolume_t390417661::get_offset_of_mSlider_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (UITable_t2217632677), -1, sizeof(UITable_t2217632677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[13] = 
{
	UITable_t2217632677::get_offset_of_columns_2(),
	UITable_t2217632677::get_offset_of_direction_3(),
	UITable_t2217632677::get_offset_of_padding_4(),
	UITable_t2217632677::get_offset_of_sorted_5(),
	UITable_t2217632677::get_offset_of_hideInactive_6(),
	UITable_t2217632677::get_offset_of_repositionNow_7(),
	UITable_t2217632677::get_offset_of_keepWithinPanel_8(),
	UITable_t2217632677::get_offset_of_onReposition_9(),
	UITable_t2217632677::get_offset_of_mPanel_10(),
	UITable_t2217632677::get_offset_of_mDrag_11(),
	UITable_t2217632677::get_offset_of_mStarted_12(),
	UITable_t2217632677::get_offset_of_mChildren_13(),
	UITable_t2217632677_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (OnReposition_t2742626599), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (Direction_t1015163810)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2596[3] = 
{
	Direction_t1015163810::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (ActiveAnimation_t1706595508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[7] = 
{
	ActiveAnimation_t1706595508::get_offset_of_onFinished_6(),
	ActiveAnimation_t1706595508::get_offset_of_eventReceiver_7(),
	ActiveAnimation_t1706595508::get_offset_of_callWhenFinished_8(),
	ActiveAnimation_t1706595508::get_offset_of_mAnim_9(),
	ActiveAnimation_t1706595508::get_offset_of_mLastDirection_10(),
	ActiveAnimation_t1706595508::get_offset_of_mDisableDirection_11(),
	ActiveAnimation_t1706595508::get_offset_of_mNotify_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (OnFinished_t1828027647), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Trigger_t3684321850)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2599[15] = 
{
	Trigger_t3684321850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
