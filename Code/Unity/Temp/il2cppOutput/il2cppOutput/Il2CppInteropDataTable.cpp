﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t1089269020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1089269020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1089269020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1089269020_0_0_0;
extern "C" void Escape_t1057292663_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t1057292663_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t1057292663_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t1057292663_0_0_0;
extern "C" void PreviousInfo_t964427264_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t964427264_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t964427264_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t964427264_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t2381935213();
extern const RuntimeType AppDomainInitializer_t2381935213_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t1671781364();
extern const RuntimeType Swapper_t1671781364_0_0_0;
extern "C" void DictionaryEntry_t2438591966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t2438591966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t2438591966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t2438591966_0_0_0;
extern "C" void Slot_t3366495841_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3366495841_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3366495841_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3366495841_0_0_0;
extern "C" void Slot_t1995290770_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t1995290770_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t1995290770_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t1995290770_0_0_0;
extern "C" void Enum_t853460045_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t853460045_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t853460045_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t853460045_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2741905888();
extern const RuntimeType ReadDelegate_t2741905888_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4167649055();
extern const RuntimeType WriteDelegate_t4167649055_0_0_0;
extern "C" void MonoIOStat_t921678252_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t921678252_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t921678252_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t921678252_0_0_0;
extern "C" void MonoEnumInfo_t3411119245_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3411119245_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3411119245_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3411119245_0_0_0;
extern "C" void CustomAttributeNamedArgument_t1265726028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t1265726028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t1265726028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t1265726028_0_0_0;
extern "C" void CustomAttributeTypedArgument_t4152925911_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t4152925911_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t4152925911_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t4152925911_0_0_0;
extern "C" void ILTokenInfo_t1855951640_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t1855951640_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t1855951640_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t1855951640_0_0_0;
extern "C" void MonoResource_t109132443_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t109132443_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t109132443_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t109132443_0_0_0;
extern "C" void RefEmitPermissionSet_t453553411_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t453553411_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t453553411_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t453553411_0_0_0;
extern "C" void MonoEventInfo_t81806153_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t81806153_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t81806153_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t81806153_0_0_0;
extern "C" void MonoMethodInfo_t574978670_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t574978670_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t574978670_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t574978670_0_0_0;
extern "C" void MonoPropertyInfo_t1517019720_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t1517019720_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t1517019720_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t1517019720_0_0_0;
extern "C" void ParameterModifier_t999651945_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t999651945_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t999651945_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t999651945_0_0_0;
extern "C" void ResourceCacheItem_t1666212118_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t1666212118_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t1666212118_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t1666212118_0_0_0;
extern "C" void ResourceInfo_t3002469731_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3002469731_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3002469731_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3002469731_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t2457781531();
extern const RuntimeType CrossContextDelegate_t2457781531_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1826147467();
extern const RuntimeType CallbackHandler_t1826147467_0_0_0;
extern "C" void SerializationEntry_t1828222808_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t1828222808_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t1828222808_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t1828222808_0_0_0;
extern "C" void StreamingContext_t648298805_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t648298805_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t648298805_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t648298805_0_0_0;
extern "C" void DSAParameters_t2868793276_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t2868793276_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t2868793276_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t2868793276_0_0_0;
extern "C" void RSAParameters_t3486196793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t3486196793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t3486196793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t3486196793_0_0_0;
extern "C" void SecurityFrame_t3185319045_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t3185319045_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t3185319045_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t3185319045_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1493794728();
extern const RuntimeType ThreadStart_t1493794728_0_0_0;
extern "C" void ValueType_t2988314022_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t2988314022_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t2988314022_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t2988314022_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t2008668336();
extern const RuntimeType ReadMethod_t2008668336_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t3918915493();
extern const RuntimeType UnmanagedReadOrWrite_t3918915493_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1701922830();
extern const RuntimeType WriteMethod_t1701922830_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t87010224();
extern const RuntimeType ReadDelegate_t87010224_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t949668826();
extern const RuntimeType WriteDelegate_t949668826_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t1121706777();
extern const RuntimeType SocketAsyncCall_t1121706777_0_0_0;
extern "C" void SocketAsyncResult_t103255750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t103255750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t103255750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t103255750_0_0_0;
extern "C" void X509ChainStatus_t3602726638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t3602726638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t3602726638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t3602726638_0_0_0;
extern "C" void IntStack_t3049972088_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t3049972088_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t3049972088_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t3049972088_0_0_0;
extern "C" void Interval_t80391396_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t80391396_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t80391396_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t80391396_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t4037042055();
extern const RuntimeType CostDelegate_t4037042055_0_0_0;
extern "C" void UriScheme_t1379670503_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t1379670503_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t1379670503_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t1379670503_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t370180854();
extern const RuntimeType Action_t370180854_0_0_0;
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t2757045165_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t2757045165_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t204719788();
extern const RuntimeType LogCallback_t204719788_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t3854352520();
extern const RuntimeType LowMemoryCallback_t3854352520_0_0_0;
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t1159347791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t1159347791_0_0_0;
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t230831548_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t230831548_0_0_0;
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1468153686_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1468153686_0_0_0;
extern "C" void Coroutine_t2294981130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2294981130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2294981130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t2294981130_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t2094042647();
extern const RuntimeType CSSMeasureFunc_t2094042647_0_0_0;
extern "C" void CullingGroup_t616951155_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t616951155_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t616951155_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t616951155_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t3981248240();
extern const RuntimeType StateChanged_t3981248240_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t962499819();
extern const RuntimeType DisplaysUpdatedDelegate_t962499819_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t992331987();
extern const RuntimeType UnityAction_t992331987_0_0_0;
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t652497987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t652497987_0_0_0;
extern "C" void Gradient_t3129154337_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3129154337_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3129154337_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3129154337_0_0_0;
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawTextureArguments_t642817784_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Internal_DrawTextureArguments_t642817784_0_0_0;
extern "C" void Object_t692178351_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t692178351_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t692178351_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t692178351_0_0_0;
extern "C" void PlayableBinding_t1732044054_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t1732044054_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t1732044054_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t1732044054_0_0_0;
extern "C" void RectOffset_t1566141465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1566141465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1566141465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1566141465_0_0_0;
extern "C" void ResourceRequest_t1767295161_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t1767295161_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t1767295161_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t1767295161_0_0_0;
extern "C" void ScriptableObject_t1804531341_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1804531341_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1804531341_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t1804531341_0_0_0;
extern "C" void HitInfo_t1469516070_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t1469516070_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t1469516070_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t1469516070_0_0_0;
extern "C" void TrackedReference_t2624196464_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t2624196464_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t2624196464_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t2624196464_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t1427750696();
extern const RuntimeType RequestAtlasCallback_t1427750696_0_0_0;
extern "C" void WorkRequest_t1716204206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1716204206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1716204206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1716204206_0_0_0;
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t4272504447_0_0_0;
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t3270995273_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t2245753777();
extern const RuntimeType PCMReaderCallback_t2245753777_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t2684603915();
extern const RuntimeType PCMSetPositionCallback_t2684603915_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2715435404();
extern const RuntimeType AudioConfigurationChangeHandler_t2715435404_0_0_0;
extern "C" void RaycastHit2D_t142485227_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t142485227_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t142485227_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t142485227_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2209694431();
extern const RuntimeType FontTextureRebuildCallback_t2209694431_0_0_0;
extern "C" void TextGenerationSettings_t408295890_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t408295890_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t408295890_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t408295890_0_0_0;
extern "C" void TextGenerator_t146007119_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t146007119_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t146007119_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t146007119_0_0_0;
extern "C" void DownloadHandler_t3833085680_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t3833085680_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t3833085680_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t3833085680_0_0_0;
extern "C" void DownloadHandlerBuffer_t217099739_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t217099739_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t217099739_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t217099739_0_0_0;
extern "C" void UnityWebRequest_t84658793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t84658793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t84658793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t84658793_0_0_0;
extern "C" void UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequestAsyncOperation_t1011823049_0_0_0;
extern "C" void UploadHandler_t200615742_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t200615742_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t200615742_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t200615742_0_0_0;
extern "C" void UploadHandlerRaw_t859941321_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t859941321_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t859941321_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t859941321_0_0_0;
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t3145551608_0_0_0;
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t118268726_0_0_0;
extern "C" void HumanBone_t634326648_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t634326648_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t634326648_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t634326648_0_0_0;
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t1743783883_0_0_0;
extern "C" void GcAchievementData_t1915683925_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t1915683925_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t1915683925_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t1915683925_0_0_0;
extern "C" void GcAchievementDescriptionData_t698908304_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t698908304_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t698908304_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t698908304_0_0_0;
extern "C" void GcLeaderboard_t1632737399_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t1632737399_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t1632737399_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t1632737399_0_0_0;
extern "C" void GcScoreData_t2000871326_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2000871326_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2000871326_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2000871326_0_0_0;
extern "C" void GcUserProfileData_t3962933902_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t3962933902_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t3962933902_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t3962933902_0_0_0;
extern "C" void Event_t1420608527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t1420608527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t1420608527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t1420608527_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2445022493();
extern const RuntimeType WindowFunction_t2445022493_0_0_0;
extern "C" void GUIContent_t2963351833_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t2963351833_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t2963351833_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t2963351833_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3871172268();
extern const RuntimeType SkinChangedDelegate_t3871172268_0_0_0;
extern "C" void GUIStyle_t1840606133_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t1840606133_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t1840606133_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t1840606133_0_0_0;
extern "C" void GUIStyleState_t2396521423_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t2396521423_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t2396521423_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t2396521423_0_0_0;
extern "C" void EmissionModule_t3558523942_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmissionModule_t3558523942_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmissionModule_t3558523942_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmissionModule_t3558523942_0_0_0;
extern "C" void MainModule_t418589237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MainModule_t418589237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MainModule_t418589237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MainModule_t418589237_0_0_0;
extern "C" void Collision_t1409177543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t1409177543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t1409177543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t1409177543_0_0_0;
extern "C" void ControllerColliderHit_t2055739030_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t2055739030_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t2055739030_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t2055739030_0_0_0;
extern "C" void RaycastHit_t1706347245_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1706347245_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1706347245_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t1706347245_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4058323176();
extern const RuntimeType WillRenderCanvases_t4058323176_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t304509084();
extern const RuntimeType SessionStateChanged_t304509084_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1497583125();
extern const RuntimeType UpdatedEventHandler_t1497583125_0_0_0;
extern "C" void RaycastResult_t811913662_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t811913662_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t811913662_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t811913662_0_0_0;
extern "C" void ColorTween_t790005542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t790005542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t790005542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t790005542_0_0_0;
extern "C" void FloatTween_t2676550171_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t2676550171_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t2676550171_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t2676550171_0_0_0;
extern "C" void Resources_t3845317858_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t3845317858_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t3845317858_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t3845317858_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2838732519();
extern const RuntimeType OnValidateInput_t2838732519_0_0_0;
extern "C" void Navigation_t1407358395_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t1407358395_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t1407358395_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t1407358395_0_0_0;
extern "C" void SpriteState_t120936949_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t120936949_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t120936949_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t120936949_0_0_0;
extern "C" void TagName_t2918394058_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagName_t2918394058_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagName_t2918394058_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TagName_t2918394058_0_0_0;
extern "C" void NsDecl_t1301437426_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t1301437426_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t1301437426_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsDecl_t1301437426_0_0_0;
extern "C" void NsScope_t1100031146_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsScope_t1100031146_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsScope_t1100031146_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsScope_t1100031146_0_0_0;
extern "C" void DelegatePInvokeWrapper_CharGetter_t1293008504();
extern const RuntimeType CharGetter_t1293008504_0_0_0;
extern "C" void WeightInfo_t4162100259_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WeightInfo_t4162100259_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WeightInfo_t4162100259_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WeightInfo_t4162100259_0_0_0;
extern "C" void DelegatePInvokeWrapper_ButtonCallbackDelegate_t333906069();
extern const RuntimeType ButtonCallbackDelegate_t333906069_0_0_0;
extern "C" void BlackboardExpression_t4195482571_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BlackboardExpression_t4195482571_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BlackboardExpression_t4195482571_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType BlackboardExpression_t4195482571_0_0_0;
extern "C" void Line_t1381376710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Line_t1381376710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Line_t1381376710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Line_t1381376710_0_0_0;
extern "C" void TargetModifier_t1423884133_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TargetModifier_t1423884133_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TargetModifier_t1423884133_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TargetModifier_t1423884133_0_0_0;
extern "C" void CameraState_t382403230_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraState_t382403230_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraState_t382403230_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CameraState_t382403230_0_0_0;
extern "C" void CustomBlend_t4150975672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomBlend_t4150975672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomBlend_t4150975672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomBlend_t4150975672_0_0_0;
extern "C" void CompiledCurbFeeler_t1909537055_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CompiledCurbFeeler_t1909537055_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CompiledCurbFeeler_t1909537055_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CompiledCurbFeeler_t1909537055_0_0_0;
extern "C" void DelegatePInvokeWrapper_AxisInputDelegate_t2029212819();
extern const RuntimeType AxisInputDelegate_t2029212819_0_0_0;
extern "C" void AxisState_t2333558477_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AxisState_t2333558477_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AxisState_t2333558477_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AxisState_t2333558477_0_0_0;
extern "C" void Recentering_t75128244_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Recentering_t75128244_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Recentering_t75128244_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Recentering_t75128244_0_0_0;
extern "C" void Instruction_t3855846037_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Instruction_t3855846037_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Instruction_t3855846037_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Instruction_t3855846037_0_0_0;
extern "C" void Target_t2477784577_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Target_t2477784577_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Target_t2477784577_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Target_t2477784577_0_0_0;
extern "C" void AutoDolly_t756678745_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AutoDolly_t756678745_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AutoDolly_t756678745_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AutoDolly_t756678745_0_0_0;
extern "C" void LensSettings_t1505430577_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LensSettings_t1505430577_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LensSettings_t1505430577_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LensSettings_t1505430577_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetDescriptionDelegate_t976785689();
extern const RuntimeType GetDescriptionDelegate_t976785689_0_0_0;
extern "C" void DraggableObject_t1812188907_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DraggableObject_t1812188907_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DraggableObject_t1812188907_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DraggableObject_t1812188907_0_0_0;
extern "C" void DelegatePInvokeWrapper_PickAThingCallback_t4122863115();
extern const RuntimeType PickAThingCallback_t4122863115_0_0_0;
extern "C" void DelegatePInvokeWrapper_ShieldCallback_t1004088674();
extern const RuntimeType ShieldCallback_t1004088674_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnFinished_t2500445478();
extern const RuntimeType OnFinished_t2500445478_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnStateChange_t2569002187();
extern const RuntimeType OnStateChange_t2569002187_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDragFinished_t81282452();
extern const RuntimeType OnDragFinished_t81282452_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnSubmit_t2511512930();
extern const RuntimeType OnSubmit_t2511512930_0_0_0;
extern "C" void DelegatePInvokeWrapper_Validator_t1053183372();
extern const RuntimeType Validator_t1053183372_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnSelectionChange_t705636838();
extern const RuntimeType OnSelectionChange_t705636838_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValueChange_t3441845912();
extern const RuntimeType OnValueChange_t3441845912_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnReposition_t2742626599();
extern const RuntimeType OnReposition_t2742626599_0_0_0;
extern "C" void Settings_t261863169_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t261863169_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t261863169_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t261863169_0_0_0;
extern "C" void BloomSettings_t103162495_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BloomSettings_t103162495_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BloomSettings_t103162495_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType BloomSettings_t103162495_0_0_0;
extern "C" void LensDirtSettings_t1291113301_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LensDirtSettings_t1291113301_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LensDirtSettings_t1291113301_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LensDirtSettings_t1291113301_0_0_0;
extern "C" void Settings_t3552172992_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3552172992_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3552172992_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3552172992_0_0_0;
extern "C" void Settings_t3912966942_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3912966942_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3912966942_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3912966942_0_0_0;
extern "C" void CurvesSettings_t606316312_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CurvesSettings_t606316312_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CurvesSettings_t606316312_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CurvesSettings_t606316312_0_0_0;
extern "C" void Settings_t3585872056_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3585872056_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3585872056_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3585872056_0_0_0;
extern "C" void Settings_t3119656574_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3119656574_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3119656574_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3119656574_0_0_0;
extern "C" void Settings_t1843499992_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1843499992_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1843499992_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1843499992_0_0_0;
extern "C" void Settings_t10049459_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t10049459_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t10049459_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t10049459_0_0_0;
extern "C" void Settings_t3862960082_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3862960082_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3862960082_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3862960082_0_0_0;
extern "C" void Frame_t2163579835_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frame_t2163579835_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frame_t2163579835_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Frame_t2163579835_0_0_0;
extern "C" void ReflectionSettings_t3233920285_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ReflectionSettings_t3233920285_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ReflectionSettings_t3233920285_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ReflectionSettings_t3233920285_0_0_0;
extern "C" void Settings_t1057026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1057026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1057026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1057026_0_0_0;
extern "C" void Settings_t3713642975_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3713642975_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3713642975_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3713642975_0_0_0;
extern "C" void Settings_t3696051941_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3696051941_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3696051941_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3696051941_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnUpdate_t172587279();
extern const RuntimeType OnUpdate_t172587279_0_0_0;
extern "C" void DelegatePInvokeWrapper_FadeFinishDelegate_t1266332645();
extern const RuntimeType FadeFinishDelegate_t1266332645_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[165] = 
{
	{ NULL, Context_t1089269020_marshal_pinvoke, Context_t1089269020_marshal_pinvoke_back, Context_t1089269020_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1089269020_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t1057292663_marshal_pinvoke, Escape_t1057292663_marshal_pinvoke_back, Escape_t1057292663_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t1057292663_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t964427264_marshal_pinvoke, PreviousInfo_t964427264_marshal_pinvoke_back, PreviousInfo_t964427264_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t964427264_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t2381935213, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t2381935213_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t1671781364, NULL, NULL, NULL, NULL, NULL, &Swapper_t1671781364_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t2438591966_marshal_pinvoke, DictionaryEntry_t2438591966_marshal_pinvoke_back, DictionaryEntry_t2438591966_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t2438591966_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3366495841_marshal_pinvoke, Slot_t3366495841_marshal_pinvoke_back, Slot_t3366495841_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3366495841_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t1995290770_marshal_pinvoke, Slot_t1995290770_marshal_pinvoke_back, Slot_t1995290770_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t1995290770_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t853460045_marshal_pinvoke, Enum_t853460045_marshal_pinvoke_back, Enum_t853460045_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t853460045_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2741905888, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2741905888_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4167649055, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4167649055_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t921678252_marshal_pinvoke, MonoIOStat_t921678252_marshal_pinvoke_back, MonoIOStat_t921678252_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t921678252_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3411119245_marshal_pinvoke, MonoEnumInfo_t3411119245_marshal_pinvoke_back, MonoEnumInfo_t3411119245_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3411119245_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t1265726028_marshal_pinvoke, CustomAttributeNamedArgument_t1265726028_marshal_pinvoke_back, CustomAttributeNamedArgument_t1265726028_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t1265726028_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t4152925911_marshal_pinvoke, CustomAttributeTypedArgument_t4152925911_marshal_pinvoke_back, CustomAttributeTypedArgument_t4152925911_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t4152925911_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t1855951640_marshal_pinvoke, ILTokenInfo_t1855951640_marshal_pinvoke_back, ILTokenInfo_t1855951640_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t1855951640_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t109132443_marshal_pinvoke, MonoResource_t109132443_marshal_pinvoke_back, MonoResource_t109132443_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t109132443_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, RefEmitPermissionSet_t453553411_marshal_pinvoke, RefEmitPermissionSet_t453553411_marshal_pinvoke_back, RefEmitPermissionSet_t453553411_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t453553411_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t81806153_marshal_pinvoke, MonoEventInfo_t81806153_marshal_pinvoke_back, MonoEventInfo_t81806153_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t81806153_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t574978670_marshal_pinvoke, MonoMethodInfo_t574978670_marshal_pinvoke_back, MonoMethodInfo_t574978670_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t574978670_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t1517019720_marshal_pinvoke, MonoPropertyInfo_t1517019720_marshal_pinvoke_back, MonoPropertyInfo_t1517019720_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t1517019720_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t999651945_marshal_pinvoke, ParameterModifier_t999651945_marshal_pinvoke_back, ParameterModifier_t999651945_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t999651945_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t1666212118_marshal_pinvoke, ResourceCacheItem_t1666212118_marshal_pinvoke_back, ResourceCacheItem_t1666212118_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t1666212118_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3002469731_marshal_pinvoke, ResourceInfo_t3002469731_marshal_pinvoke_back, ResourceInfo_t3002469731_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3002469731_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t2457781531, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t2457781531_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t1826147467, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t1826147467_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t1828222808_marshal_pinvoke, SerializationEntry_t1828222808_marshal_pinvoke_back, SerializationEntry_t1828222808_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t1828222808_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t648298805_marshal_pinvoke, StreamingContext_t648298805_marshal_pinvoke_back, StreamingContext_t648298805_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t648298805_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t2868793276_marshal_pinvoke, DSAParameters_t2868793276_marshal_pinvoke_back, DSAParameters_t2868793276_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t2868793276_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t3486196793_marshal_pinvoke, RSAParameters_t3486196793_marshal_pinvoke_back, RSAParameters_t3486196793_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t3486196793_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t3185319045_marshal_pinvoke, SecurityFrame_t3185319045_marshal_pinvoke_back, SecurityFrame_t3185319045_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t3185319045_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1493794728, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1493794728_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t2988314022_marshal_pinvoke, ValueType_t2988314022_marshal_pinvoke_back, ValueType_t2988314022_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t2988314022_0_0_0 } /* System.ValueType */,
	{ DelegatePInvokeWrapper_ReadMethod_t2008668336, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t2008668336_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t3918915493, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t3918915493_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t1701922830, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t1701922830_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_ReadDelegate_t87010224, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t87010224_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t949668826, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t949668826_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t1121706777, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t1121706777_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t103255750_marshal_pinvoke, SocketAsyncResult_t103255750_marshal_pinvoke_back, SocketAsyncResult_t103255750_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t103255750_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, X509ChainStatus_t3602726638_marshal_pinvoke, X509ChainStatus_t3602726638_marshal_pinvoke_back, X509ChainStatus_t3602726638_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t3602726638_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t3049972088_marshal_pinvoke, IntStack_t3049972088_marshal_pinvoke_back, IntStack_t3049972088_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t3049972088_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t80391396_marshal_pinvoke, Interval_t80391396_marshal_pinvoke_back, Interval_t80391396_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t80391396_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t4037042055, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t4037042055_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t1379670503_marshal_pinvoke, UriScheme_t1379670503_marshal_pinvoke_back, UriScheme_t1379670503_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t1379670503_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t370180854, NULL, NULL, NULL, NULL, NULL, &Action_t370180854_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t2757045165_marshal_pinvoke, AnimationCurve_t2757045165_marshal_pinvoke_back, AnimationCurve_t2757045165_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t2757045165_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t204719788, NULL, NULL, NULL, NULL, NULL, &LogCallback_t204719788_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t3854352520, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t3854352520_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t1159347791_marshal_pinvoke, AssetBundleCreateRequest_t1159347791_marshal_pinvoke_back, AssetBundleCreateRequest_t1159347791_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t1159347791_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t230831548_marshal_pinvoke, AssetBundleRequest_t230831548_marshal_pinvoke_back, AssetBundleRequest_t230831548_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t230831548_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1468153686_marshal_pinvoke, AsyncOperation_t1468153686_marshal_pinvoke_back, AsyncOperation_t1468153686_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1468153686_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, Coroutine_t2294981130_marshal_pinvoke, Coroutine_t2294981130_marshal_pinvoke_back, Coroutine_t2294981130_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2294981130_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t2094042647, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t2094042647_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t616951155_marshal_pinvoke, CullingGroup_t616951155_marshal_pinvoke_back, CullingGroup_t616951155_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t616951155_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t3981248240, NULL, NULL, NULL, NULL, NULL, &StateChanged_t3981248240_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t962499819, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t962499819_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t992331987, NULL, NULL, NULL, NULL, NULL, &UnityAction_t992331987_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t652497987_marshal_pinvoke, FailedToLoadScriptObject_t652497987_marshal_pinvoke_back, FailedToLoadScriptObject_t652497987_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t652497987_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3129154337_marshal_pinvoke, Gradient_t3129154337_marshal_pinvoke_back, Gradient_t3129154337_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3129154337_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Internal_DrawTextureArguments_t642817784_marshal_pinvoke, Internal_DrawTextureArguments_t642817784_marshal_pinvoke_back, Internal_DrawTextureArguments_t642817784_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawTextureArguments_t642817784_0_0_0 } /* UnityEngine.Internal_DrawTextureArguments */,
	{ NULL, Object_t692178351_marshal_pinvoke, Object_t692178351_marshal_pinvoke_back, Object_t692178351_marshal_pinvoke_cleanup, NULL, NULL, &Object_t692178351_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t1732044054_marshal_pinvoke, PlayableBinding_t1732044054_marshal_pinvoke_back, PlayableBinding_t1732044054_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t1732044054_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t1566141465_marshal_pinvoke, RectOffset_t1566141465_marshal_pinvoke_back, RectOffset_t1566141465_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1566141465_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t1767295161_marshal_pinvoke, ResourceRequest_t1767295161_marshal_pinvoke_back, ResourceRequest_t1767295161_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t1767295161_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1804531341_marshal_pinvoke, ScriptableObject_t1804531341_marshal_pinvoke_back, ScriptableObject_t1804531341_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1804531341_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t1469516070_marshal_pinvoke, HitInfo_t1469516070_marshal_pinvoke_back, HitInfo_t1469516070_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t1469516070_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t2624196464_marshal_pinvoke, TrackedReference_t2624196464_marshal_pinvoke_back, TrackedReference_t2624196464_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t2624196464_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t1427750696, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t1427750696_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1716204206_marshal_pinvoke, WorkRequest_t1716204206_marshal_pinvoke_back, WorkRequest_t1716204206_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1716204206_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t4272504447_marshal_pinvoke, WaitForSeconds_t4272504447_marshal_pinvoke_back, WaitForSeconds_t4272504447_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t4272504447_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t3270995273_marshal_pinvoke, YieldInstruction_t3270995273_marshal_pinvoke_back, YieldInstruction_t3270995273_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3270995273_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t2245753777, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t2245753777_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t2684603915, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t2684603915_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2715435404, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2715435404_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ NULL, RaycastHit2D_t142485227_marshal_pinvoke, RaycastHit2D_t142485227_marshal_pinvoke_back, RaycastHit2D_t142485227_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t142485227_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2209694431, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2209694431_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t408295890_marshal_pinvoke, TextGenerationSettings_t408295890_marshal_pinvoke_back, TextGenerationSettings_t408295890_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t408295890_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t146007119_marshal_pinvoke, TextGenerator_t146007119_marshal_pinvoke_back, TextGenerator_t146007119_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t146007119_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, DownloadHandler_t3833085680_marshal_pinvoke, DownloadHandler_t3833085680_marshal_pinvoke_back, DownloadHandler_t3833085680_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t3833085680_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t217099739_marshal_pinvoke, DownloadHandlerBuffer_t217099739_marshal_pinvoke_back, DownloadHandlerBuffer_t217099739_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t217099739_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t84658793_marshal_pinvoke, UnityWebRequest_t84658793_marshal_pinvoke_back, UnityWebRequest_t84658793_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t84658793_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke, UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke_back, UnityWebRequestAsyncOperation_t1011823049_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequestAsyncOperation_t1011823049_0_0_0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation */,
	{ NULL, UploadHandler_t200615742_marshal_pinvoke, UploadHandler_t200615742_marshal_pinvoke_back, UploadHandler_t200615742_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t200615742_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t859941321_marshal_pinvoke, UploadHandlerRaw_t859941321_marshal_pinvoke_back, UploadHandlerRaw_t859941321_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t859941321_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, AnimationEvent_t3145551608_marshal_pinvoke, AnimationEvent_t3145551608_marshal_pinvoke_back, AnimationEvent_t3145551608_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t3145551608_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t118268726_marshal_pinvoke, AnimatorTransitionInfo_t118268726_marshal_pinvoke_back, AnimatorTransitionInfo_t118268726_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t118268726_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t634326648_marshal_pinvoke, HumanBone_t634326648_marshal_pinvoke_back, HumanBone_t634326648_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t634326648_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t1743783883_marshal_pinvoke, SkeletonBone_t1743783883_marshal_pinvoke_back, SkeletonBone_t1743783883_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t1743783883_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t1915683925_marshal_pinvoke, GcAchievementData_t1915683925_marshal_pinvoke_back, GcAchievementData_t1915683925_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t1915683925_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t698908304_marshal_pinvoke, GcAchievementDescriptionData_t698908304_marshal_pinvoke_back, GcAchievementDescriptionData_t698908304_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t698908304_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t1632737399_marshal_pinvoke, GcLeaderboard_t1632737399_marshal_pinvoke_back, GcLeaderboard_t1632737399_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t1632737399_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2000871326_marshal_pinvoke, GcScoreData_t2000871326_marshal_pinvoke_back, GcScoreData_t2000871326_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2000871326_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t3962933902_marshal_pinvoke, GcUserProfileData_t3962933902_marshal_pinvoke_back, GcUserProfileData_t3962933902_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t3962933902_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t1420608527_marshal_pinvoke, Event_t1420608527_marshal_pinvoke_back, Event_t1420608527_marshal_pinvoke_cleanup, NULL, NULL, &Event_t1420608527_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t2445022493, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t2445022493_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t2963351833_marshal_pinvoke, GUIContent_t2963351833_marshal_pinvoke_back, GUIContent_t2963351833_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t2963351833_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3871172268, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3871172268_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t1840606133_marshal_pinvoke, GUIStyle_t1840606133_marshal_pinvoke_back, GUIStyle_t1840606133_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t1840606133_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t2396521423_marshal_pinvoke, GUIStyleState_t2396521423_marshal_pinvoke_back, GUIStyleState_t2396521423_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t2396521423_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, EmissionModule_t3558523942_marshal_pinvoke, EmissionModule_t3558523942_marshal_pinvoke_back, EmissionModule_t3558523942_marshal_pinvoke_cleanup, NULL, NULL, &EmissionModule_t3558523942_0_0_0 } /* UnityEngine.ParticleSystem/EmissionModule */,
	{ NULL, MainModule_t418589237_marshal_pinvoke, MainModule_t418589237_marshal_pinvoke_back, MainModule_t418589237_marshal_pinvoke_cleanup, NULL, NULL, &MainModule_t418589237_0_0_0 } /* UnityEngine.ParticleSystem/MainModule */,
	{ NULL, Collision_t1409177543_marshal_pinvoke, Collision_t1409177543_marshal_pinvoke_back, Collision_t1409177543_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t1409177543_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t2055739030_marshal_pinvoke, ControllerColliderHit_t2055739030_marshal_pinvoke_back, ControllerColliderHit_t2055739030_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t2055739030_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t1706347245_marshal_pinvoke, RaycastHit_t1706347245_marshal_pinvoke_back, RaycastHit_t1706347245_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1706347245_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t4058323176, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t4058323176_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t304509084, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t304509084_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1497583125, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1497583125_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, RaycastResult_t811913662_marshal_pinvoke, RaycastResult_t811913662_marshal_pinvoke_back, RaycastResult_t811913662_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t811913662_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t790005542_marshal_pinvoke, ColorTween_t790005542_marshal_pinvoke_back, ColorTween_t790005542_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t790005542_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t2676550171_marshal_pinvoke, FloatTween_t2676550171_marshal_pinvoke_back, FloatTween_t2676550171_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t2676550171_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t3845317858_marshal_pinvoke, Resources_t3845317858_marshal_pinvoke_back, Resources_t3845317858_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t3845317858_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2838732519, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2838732519_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t1407358395_marshal_pinvoke, Navigation_t1407358395_marshal_pinvoke_back, Navigation_t1407358395_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t1407358395_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t120936949_marshal_pinvoke, SpriteState_t120936949_marshal_pinvoke_back, SpriteState_t120936949_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t120936949_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, TagName_t2918394058_marshal_pinvoke, TagName_t2918394058_marshal_pinvoke_back, TagName_t2918394058_marshal_pinvoke_cleanup, NULL, NULL, &TagName_t2918394058_0_0_0 } /* Mono.Xml2.XmlTextReader/TagName */,
	{ NULL, NsDecl_t1301437426_marshal_pinvoke, NsDecl_t1301437426_marshal_pinvoke_back, NsDecl_t1301437426_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t1301437426_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, NsScope_t1100031146_marshal_pinvoke, NsScope_t1100031146_marshal_pinvoke_back, NsScope_t1100031146_marshal_pinvoke_cleanup, NULL, NULL, &NsScope_t1100031146_0_0_0 } /* System.Xml.XmlNamespaceManager/NsScope */,
	{ DelegatePInvokeWrapper_CharGetter_t1293008504, NULL, NULL, NULL, NULL, NULL, &CharGetter_t1293008504_0_0_0 } /* System.Xml.XmlReaderBinarySupport/CharGetter */,
	{ NULL, WeightInfo_t4162100259_marshal_pinvoke, WeightInfo_t4162100259_marshal_pinvoke_back, WeightInfo_t4162100259_marshal_pinvoke_cleanup, NULL, NULL, &WeightInfo_t4162100259_0_0_0 } /* UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo */,
	{ DelegatePInvokeWrapper_ButtonCallbackDelegate_t333906069, NULL, NULL, NULL, NULL, NULL, &ButtonCallbackDelegate_t333906069_0_0_0 } /* ButtonCallback/ButtonCallbackDelegate */,
	{ NULL, BlackboardExpression_t4195482571_marshal_pinvoke, BlackboardExpression_t4195482571_marshal_pinvoke_back, BlackboardExpression_t4195482571_marshal_pinvoke_cleanup, NULL, NULL, &BlackboardExpression_t4195482571_0_0_0 } /* Cinemachine.Blackboard.Reactor/BlackboardExpression */,
	{ NULL, Line_t1381376710_marshal_pinvoke, Line_t1381376710_marshal_pinvoke_back, Line_t1381376710_marshal_pinvoke_cleanup, NULL, NULL, &Line_t1381376710_0_0_0 } /* Cinemachine.Blackboard.Reactor/BlackboardExpression/Line */,
	{ NULL, TargetModifier_t1423884133_marshal_pinvoke, TargetModifier_t1423884133_marshal_pinvoke_back, TargetModifier_t1423884133_marshal_pinvoke_cleanup, NULL, NULL, &TargetModifier_t1423884133_0_0_0 } /* Cinemachine.Blackboard.Reactor/TargetModifier */,
	{ NULL, CameraState_t382403230_marshal_pinvoke, CameraState_t382403230_marshal_pinvoke_back, CameraState_t382403230_marshal_pinvoke_cleanup, NULL, NULL, &CameraState_t382403230_0_0_0 } /* Cinemachine.CameraState */,
	{ NULL, CustomBlend_t4150975672_marshal_pinvoke, CustomBlend_t4150975672_marshal_pinvoke_back, CustomBlend_t4150975672_marshal_pinvoke_cleanup, NULL, NULL, &CustomBlend_t4150975672_0_0_0 } /* Cinemachine.CinemachineBlenderSettings/CustomBlend */,
	{ NULL, CompiledCurbFeeler_t1909537055_marshal_pinvoke, CompiledCurbFeeler_t1909537055_marshal_pinvoke_back, CompiledCurbFeeler_t1909537055_marshal_pinvoke_cleanup, NULL, NULL, &CompiledCurbFeeler_t1909537055_0_0_0 } /* Cinemachine.CinemachineCollider/CompiledCurbFeeler */,
	{ DelegatePInvokeWrapper_AxisInputDelegate_t2029212819, NULL, NULL, NULL, NULL, NULL, &AxisInputDelegate_t2029212819_0_0_0 } /* Cinemachine.CinemachineCore/AxisInputDelegate */,
	{ NULL, AxisState_t2333558477_marshal_pinvoke, AxisState_t2333558477_marshal_pinvoke_back, AxisState_t2333558477_marshal_pinvoke_cleanup, NULL, NULL, &AxisState_t2333558477_0_0_0 } /* Cinemachine.CinemachineOrbitalTransposer/AxisState */,
	{ NULL, Recentering_t75128244_marshal_pinvoke, Recentering_t75128244_marshal_pinvoke_back, Recentering_t75128244_marshal_pinvoke_cleanup, NULL, NULL, &Recentering_t75128244_0_0_0 } /* Cinemachine.CinemachineOrbitalTransposer/Recentering */,
	{ NULL, Instruction_t3855846037_marshal_pinvoke, Instruction_t3855846037_marshal_pinvoke_back, Instruction_t3855846037_marshal_pinvoke_cleanup, NULL, NULL, &Instruction_t3855846037_0_0_0 } /* Cinemachine.CinemachineStateDrivenCamera/Instruction */,
	{ NULL, Target_t2477784577_marshal_pinvoke, Target_t2477784577_marshal_pinvoke_back, Target_t2477784577_marshal_pinvoke_cleanup, NULL, NULL, &Target_t2477784577_0_0_0 } /* Cinemachine.CinemachineTargetGroup/Target */,
	{ NULL, AutoDolly_t756678745_marshal_pinvoke, AutoDolly_t756678745_marshal_pinvoke_back, AutoDolly_t756678745_marshal_pinvoke_cleanup, NULL, NULL, &AutoDolly_t756678745_0_0_0 } /* Cinemachine.CinemachineTrackedDolly/AutoDolly */,
	{ NULL, LensSettings_t1505430577_marshal_pinvoke, LensSettings_t1505430577_marshal_pinvoke_back, LensSettings_t1505430577_marshal_pinvoke_cleanup, NULL, NULL, &LensSettings_t1505430577_0_0_0 } /* Cinemachine.LensSettings */,
	{ DelegatePInvokeWrapper_GetDescriptionDelegate_t976785689, NULL, NULL, NULL, NULL, NULL, &GetDescriptionDelegate_t976785689_0_0_0 } /* CommonListCode/GetDescriptionDelegate */,
	{ NULL, DraggableObject_t1812188907_marshal_pinvoke, DraggableObject_t1812188907_marshal_pinvoke_back, DraggableObject_t1812188907_marshal_pinvoke_cleanup, NULL, NULL, &DraggableObject_t1812188907_0_0_0 } /* DraggableObject */,
	{ DelegatePInvokeWrapper_PickAThingCallback_t4122863115, NULL, NULL, NULL, NULL, NULL, &PickAThingCallback_t4122863115_0_0_0 } /* PickAThingDialog/PickAThingCallback */,
	{ DelegatePInvokeWrapper_ShieldCallback_t1004088674, NULL, NULL, NULL, NULL, NULL, &ShieldCallback_t1004088674_0_0_0 } /* ShieldMenu/ShieldCallback */,
	{ DelegatePInvokeWrapper_OnFinished_t2500445478, NULL, NULL, NULL, NULL, NULL, &OnFinished_t2500445478_0_0_0 } /* SpringPanel/OnFinished */,
	{ DelegatePInvokeWrapper_OnStateChange_t2569002187, NULL, NULL, NULL, NULL, NULL, &OnStateChange_t2569002187_0_0_0 } /* UICheckbox/OnStateChange */,
	{ DelegatePInvokeWrapper_OnDragFinished_t81282452, NULL, NULL, NULL, NULL, NULL, &OnDragFinished_t81282452_0_0_0 } /* UIDraggablePanel/OnDragFinished */,
	{ DelegatePInvokeWrapper_OnSubmit_t2511512930, NULL, NULL, NULL, NULL, NULL, &OnSubmit_t2511512930_0_0_0 } /* UIInput/OnSubmit */,
	{ DelegatePInvokeWrapper_Validator_t1053183372, NULL, NULL, NULL, NULL, NULL, &Validator_t1053183372_0_0_0 } /* UIInput/Validator */,
	{ DelegatePInvokeWrapper_OnSelectionChange_t705636838, NULL, NULL, NULL, NULL, NULL, &OnSelectionChange_t705636838_0_0_0 } /* UIPopupList/OnSelectionChange */,
	{ DelegatePInvokeWrapper_OnValueChange_t3441845912, NULL, NULL, NULL, NULL, NULL, &OnValueChange_t3441845912_0_0_0 } /* UISlider/OnValueChange */,
	{ DelegatePInvokeWrapper_OnReposition_t2742626599, NULL, NULL, NULL, NULL, NULL, &OnReposition_t2742626599_0_0_0 } /* UITable/OnReposition */,
	{ NULL, Settings_t261863169_marshal_pinvoke, Settings_t261863169_marshal_pinvoke_back, Settings_t261863169_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t261863169_0_0_0 } /* UnityEngine.PostProcessing.AmbientOcclusionModel/Settings */,
	{ NULL, BloomSettings_t103162495_marshal_pinvoke, BloomSettings_t103162495_marshal_pinvoke_back, BloomSettings_t103162495_marshal_pinvoke_cleanup, NULL, NULL, &BloomSettings_t103162495_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/BloomSettings */,
	{ NULL, LensDirtSettings_t1291113301_marshal_pinvoke, LensDirtSettings_t1291113301_marshal_pinvoke_back, LensDirtSettings_t1291113301_marshal_pinvoke_cleanup, NULL, NULL, &LensDirtSettings_t1291113301_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/LensDirtSettings */,
	{ NULL, Settings_t3552172992_marshal_pinvoke, Settings_t3552172992_marshal_pinvoke_back, Settings_t3552172992_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3552172992_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/Settings */,
	{ NULL, Settings_t3912966942_marshal_pinvoke, Settings_t3912966942_marshal_pinvoke_back, Settings_t3912966942_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3912966942_0_0_0 } /* UnityEngine.PostProcessing.ChromaticAberrationModel/Settings */,
	{ NULL, CurvesSettings_t606316312_marshal_pinvoke, CurvesSettings_t606316312_marshal_pinvoke_back, CurvesSettings_t606316312_marshal_pinvoke_cleanup, NULL, NULL, &CurvesSettings_t606316312_0_0_0 } /* UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings */,
	{ NULL, Settings_t3585872056_marshal_pinvoke, Settings_t3585872056_marshal_pinvoke_back, Settings_t3585872056_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3585872056_0_0_0 } /* UnityEngine.PostProcessing.ColorGradingModel/Settings */,
	{ NULL, Settings_t3119656574_marshal_pinvoke, Settings_t3119656574_marshal_pinvoke_back, Settings_t3119656574_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3119656574_0_0_0 } /* UnityEngine.PostProcessing.DepthOfFieldModel/Settings */,
	{ NULL, Settings_t1843499992_marshal_pinvoke, Settings_t1843499992_marshal_pinvoke_back, Settings_t1843499992_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1843499992_0_0_0 } /* UnityEngine.PostProcessing.EyeAdaptationModel/Settings */,
	{ NULL, Settings_t10049459_marshal_pinvoke, Settings_t10049459_marshal_pinvoke_back, Settings_t10049459_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t10049459_0_0_0 } /* UnityEngine.PostProcessing.FogModel/Settings */,
	{ NULL, Settings_t3862960082_marshal_pinvoke, Settings_t3862960082_marshal_pinvoke_back, Settings_t3862960082_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3862960082_0_0_0 } /* UnityEngine.PostProcessing.GrainModel/Settings */,
	{ NULL, Frame_t2163579835_marshal_pinvoke, Frame_t2163579835_marshal_pinvoke_back, Frame_t2163579835_marshal_pinvoke_cleanup, NULL, NULL, &Frame_t2163579835_0_0_0 } /* UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame */,
	{ NULL, ReflectionSettings_t3233920285_marshal_pinvoke, ReflectionSettings_t3233920285_marshal_pinvoke_back, ReflectionSettings_t3233920285_marshal_pinvoke_cleanup, NULL, NULL, &ReflectionSettings_t3233920285_0_0_0 } /* UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings */,
	{ NULL, Settings_t1057026_marshal_pinvoke, Settings_t1057026_marshal_pinvoke_back, Settings_t1057026_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1057026_0_0_0 } /* UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings */,
	{ NULL, Settings_t3713642975_marshal_pinvoke, Settings_t3713642975_marshal_pinvoke_back, Settings_t3713642975_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3713642975_0_0_0 } /* UnityEngine.PostProcessing.UserLutModel/Settings */,
	{ NULL, Settings_t3696051941_marshal_pinvoke, Settings_t3696051941_marshal_pinvoke_back, Settings_t3696051941_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3696051941_0_0_0 } /* UnityEngine.PostProcessing.VignetteModel/Settings */,
	{ DelegatePInvokeWrapper_OnUpdate_t172587279, NULL, NULL, NULL, NULL, NULL, &OnUpdate_t172587279_0_0_0 } /* UpdateManager/OnUpdate */,
	{ DelegatePInvokeWrapper_FadeFinishDelegate_t1266332645, NULL, NULL, NULL, NULL, NULL, &FadeFinishDelegate_t1266332645_0_0_0 } /* Whiteout/FadeFinishDelegate */,
	NULL,
};
