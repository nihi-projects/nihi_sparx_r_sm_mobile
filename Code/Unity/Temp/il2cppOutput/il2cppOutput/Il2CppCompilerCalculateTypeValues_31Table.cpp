﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686;
// GuideEndTransition
struct GuideEndTransition_t2209539835;
// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>
struct IDictionary_2_t1119492713;
// System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>
struct List_1_t2983931971;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t815382151;
// UnityEngine.WWW
struct WWW_t3599262362;
// System.String[]
struct StringU5BU5D_t2511808107;
// UnityEngine.UI.Text
struct Text_t1790657652;
// System.Void
struct Void_t653366341;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t2742677291;
// Boomlagoon.JSON.JSONArray
struct JSONArray_t377107346;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// Whiteout/FadeFinishDelegate
struct FadeFinishDelegate_t1266332645;
// UnityEngine.UI.Image
struct Image_t2816987602;
// HowIFeltFaces[]
struct HowIFeltFacesU5BU5D_t3946832269;
// CapturedDataInput
struct CapturedDataInput_t2616152122;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.GUISkin
struct GUISkin_t2122630221;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// PlayerInfoLoadOnLevelLoad
struct PlayerInfoLoadOnLevelLoad_t3228362066;
// LoadTalkSceneXMLData
struct LoadTalkSceneXMLData_t2381108950;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t1489827645;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// ShieldMenu/ShieldCallback
struct ShieldCallback_t1004088674;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t892035751;
// UnityEngine.UI.Button
struct Button_t1293135404;
// ButtonCallback[]
struct ButtonCallbackU5BU5D_t1838213871;
// StatementBox
struct StatementBox_t3084352480;
// MultiChoiceBox
struct MultiChoiceBox_t1681099465;
// TooltipBox
struct TooltipBox_t3451613534;
// Reel[]
struct ReelU5BU5D_t2983038275;
// OutlineObject
struct OutlineObject_t941793957;
// UnityEngine.Transform
struct Transform_t362059596;




#ifndef U3CMODULEU3E_T1429447288_H
#define U3CMODULEU3E_T1429447288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447288 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447288_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T1429447289_H
#define U3CMODULEU3E_T1429447289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447289 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447289_H
#ifndef UTILS_T2448594833_H
#define UTILS_T2448594833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t2448594833  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T2448594833_H
#ifndef U3CLOADLEVELSU3EC__ITERATOR0_T1826114136_H
#define U3CLOADLEVELSU3EC__ITERATOR0_T1826114136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuideEndTransition/<LoadLevelS>c__Iterator0
struct  U3CLoadLevelSU3Ec__Iterator0_t1826114136  : public RuntimeObject
{
public:
	// System.Single GuideEndTransition/<LoadLevelS>c__Iterator0::<fTimeUntilLoad>__0
	float ___U3CfTimeUntilLoadU3E__0_0;
	// UnityEngine.AsyncOperation GuideEndTransition/<LoadLevelS>c__Iterator0::<op>__0
	AsyncOperation_t1468153686 * ___U3CopU3E__0_1;
	// GuideEndTransition GuideEndTransition/<LoadLevelS>c__Iterator0::$this
	GuideEndTransition_t2209539835 * ___U24this_2;
	// System.Object GuideEndTransition/<LoadLevelS>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GuideEndTransition/<LoadLevelS>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GuideEndTransition/<LoadLevelS>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CfTimeUntilLoadU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U3CfTimeUntilLoadU3E__0_0)); }
	inline float get_U3CfTimeUntilLoadU3E__0_0() const { return ___U3CfTimeUntilLoadU3E__0_0; }
	inline float* get_address_of_U3CfTimeUntilLoadU3E__0_0() { return &___U3CfTimeUntilLoadU3E__0_0; }
	inline void set_U3CfTimeUntilLoadU3E__0_0(float value)
	{
		___U3CfTimeUntilLoadU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CopU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U3CopU3E__0_1)); }
	inline AsyncOperation_t1468153686 * get_U3CopU3E__0_1() const { return ___U3CopU3E__0_1; }
	inline AsyncOperation_t1468153686 ** get_address_of_U3CopU3E__0_1() { return &___U3CopU3E__0_1; }
	inline void set_U3CopU3E__0_1(AsyncOperation_t1468153686 * value)
	{
		___U3CopU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U24this_2)); }
	inline GuideEndTransition_t2209539835 * get_U24this_2() const { return ___U24this_2; }
	inline GuideEndTransition_t2209539835 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GuideEndTransition_t2209539835 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadLevelSU3Ec__Iterator0_t1826114136, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEVELSU3EC__ITERATOR0_T1826114136_H
#ifndef JSONOBJECT_T2742677291_H
#define JSONOBJECT_T2742677291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.JSON.JSONObject
struct  JSONObject_t2742677291  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONObject::values
	RuntimeObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONObject_t2742677291, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T2742677291_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef JSONARRAY_T377107346_H
#define JSONARRAY_T377107346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.JSON.JSONArray
struct  JSONArray_t377107346  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONArray::values
	List_1_t2983931971 * ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONArray_t377107346, ___values_0)); }
	inline List_1_t2983931971 * get_values_0() const { return ___values_0; }
	inline List_1_t2983931971 ** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(List_1_t2983931971 * value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T377107346_H
#ifndef U3CSUBCONNECTU3EC__ITERATOR0_T36647400_H
#define U3CSUBCONNECTU3EC__ITERATOR0_T36647400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfflineFileUploader/<subConnect>c__Iterator0
struct  U3CsubConnectU3Ec__Iterator0_t36647400  : public RuntimeObject
{
public:
	// System.String OfflineFileUploader/<subConnect>c__Iterator0::sJsonString
	String_t* ___sJsonString_0;
	// UnityEngine.WWWForm OfflineFileUploader/<subConnect>c__Iterator0::<form>__0
	WWWForm_t815382151 * ___U3CformU3E__0_1;
	// UnityEngine.WWW OfflineFileUploader/<subConnect>c__Iterator0::<www>__0
	WWW_t3599262362 * ___U3CwwwU3E__0_2;
	// System.Object OfflineFileUploader/<subConnect>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean OfflineFileUploader/<subConnect>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 OfflineFileUploader/<subConnect>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_sJsonString_0() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___sJsonString_0)); }
	inline String_t* get_sJsonString_0() const { return ___sJsonString_0; }
	inline String_t** get_address_of_sJsonString_0() { return &___sJsonString_0; }
	inline void set_sJsonString_0(String_t* value)
	{
		___sJsonString_0 = value;
		Il2CppCodeGenWriteBarrier((&___sJsonString_0), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___U3CformU3E__0_1)); }
	inline WWWForm_t815382151 * get_U3CformU3E__0_1() const { return ___U3CformU3E__0_1; }
	inline WWWForm_t815382151 ** get_address_of_U3CformU3E__0_1() { return &___U3CformU3E__0_1; }
	inline void set_U3CformU3E__0_1(WWWForm_t815382151 * value)
	{
		___U3CformU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___U3CwwwU3E__0_2)); }
	inline WWW_t3599262362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3599262362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3599262362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CsubConnectU3Ec__Iterator0_t36647400, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBCONNECTU3EC__ITERATOR0_T36647400_H
#ifndef OFFLINEFILEUPLOADER_T262227362_H
#define OFFLINEFILEUPLOADER_T262227362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfflineFileUploader
struct  OfflineFileUploader_t262227362  : public RuntimeObject
{
public:
	// System.Int32 OfflineFileUploader::currentLineNum
	int32_t ___currentLineNum_1;
	// System.String[] OfflineFileUploader::allLines
	StringU5BU5D_t2511808107* ___allLines_2;
	// System.String OfflineFileUploader::fileName
	String_t* ___fileName_3;
	// UnityEngine.WWW OfflineFileUploader::connection
	WWW_t3599262362 * ___connection_4;

public:
	inline static int32_t get_offset_of_currentLineNum_1() { return static_cast<int32_t>(offsetof(OfflineFileUploader_t262227362, ___currentLineNum_1)); }
	inline int32_t get_currentLineNum_1() const { return ___currentLineNum_1; }
	inline int32_t* get_address_of_currentLineNum_1() { return &___currentLineNum_1; }
	inline void set_currentLineNum_1(int32_t value)
	{
		___currentLineNum_1 = value;
	}

	inline static int32_t get_offset_of_allLines_2() { return static_cast<int32_t>(offsetof(OfflineFileUploader_t262227362, ___allLines_2)); }
	inline StringU5BU5D_t2511808107* get_allLines_2() const { return ___allLines_2; }
	inline StringU5BU5D_t2511808107** get_address_of_allLines_2() { return &___allLines_2; }
	inline void set_allLines_2(StringU5BU5D_t2511808107* value)
	{
		___allLines_2 = value;
		Il2CppCodeGenWriteBarrier((&___allLines_2), value);
	}

	inline static int32_t get_offset_of_fileName_3() { return static_cast<int32_t>(offsetof(OfflineFileUploader_t262227362, ___fileName_3)); }
	inline String_t* get_fileName_3() const { return ___fileName_3; }
	inline String_t** get_address_of_fileName_3() { return &___fileName_3; }
	inline void set_fileName_3(String_t* value)
	{
		___fileName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_3), value);
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(OfflineFileUploader_t262227362, ___connection_4)); }
	inline WWW_t3599262362 * get_connection_4() const { return ___connection_4; }
	inline WWW_t3599262362 ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(WWW_t3599262362 * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier((&___connection_4), value);
	}
};

struct OfflineFileUploader_t262227362_StaticFields
{
public:
	// OfflineFileUploader OfflineFileUploader::theInstance
	OfflineFileUploader_t262227362 * ___theInstance_0;

public:
	inline static int32_t get_offset_of_theInstance_0() { return static_cast<int32_t>(offsetof(OfflineFileUploader_t262227362_StaticFields, ___theInstance_0)); }
	inline OfflineFileUploader_t262227362 * get_theInstance_0() const { return ___theInstance_0; }
	inline OfflineFileUploader_t262227362 ** get_address_of_theInstance_0() { return &___theInstance_0; }
	inline void set_theInstance_0(OfflineFileUploader_t262227362 * value)
	{
		___theInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___theInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFLINEFILEUPLOADER_T262227362_H
#ifndef REEL_T2885754470_H
#define REEL_T2885754470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reel
struct  Reel_t2885754470  : public RuntimeObject
{
public:
	// System.String[] Reel::words
	StringU5BU5D_t2511808107* ___words_0;
	// System.Int32 Reel::rightWord
	int32_t ___rightWord_1;
	// System.Int32 Reel::startWord
	int32_t ___startWord_2;
	// System.Int32 Reel::currentWord
	int32_t ___currentWord_3;
	// UnityEngine.UI.Text Reel::wordText
	Text_t1790657652 * ___wordText_4;

public:
	inline static int32_t get_offset_of_words_0() { return static_cast<int32_t>(offsetof(Reel_t2885754470, ___words_0)); }
	inline StringU5BU5D_t2511808107* get_words_0() const { return ___words_0; }
	inline StringU5BU5D_t2511808107** get_address_of_words_0() { return &___words_0; }
	inline void set_words_0(StringU5BU5D_t2511808107* value)
	{
		___words_0 = value;
		Il2CppCodeGenWriteBarrier((&___words_0), value);
	}

	inline static int32_t get_offset_of_rightWord_1() { return static_cast<int32_t>(offsetof(Reel_t2885754470, ___rightWord_1)); }
	inline int32_t get_rightWord_1() const { return ___rightWord_1; }
	inline int32_t* get_address_of_rightWord_1() { return &___rightWord_1; }
	inline void set_rightWord_1(int32_t value)
	{
		___rightWord_1 = value;
	}

	inline static int32_t get_offset_of_startWord_2() { return static_cast<int32_t>(offsetof(Reel_t2885754470, ___startWord_2)); }
	inline int32_t get_startWord_2() const { return ___startWord_2; }
	inline int32_t* get_address_of_startWord_2() { return &___startWord_2; }
	inline void set_startWord_2(int32_t value)
	{
		___startWord_2 = value;
	}

	inline static int32_t get_offset_of_currentWord_3() { return static_cast<int32_t>(offsetof(Reel_t2885754470, ___currentWord_3)); }
	inline int32_t get_currentWord_3() const { return ___currentWord_3; }
	inline int32_t* get_address_of_currentWord_3() { return &___currentWord_3; }
	inline void set_currentWord_3(int32_t value)
	{
		___currentWord_3 = value;
	}

	inline static int32_t get_offset_of_wordText_4() { return static_cast<int32_t>(offsetof(Reel_t2885754470, ___wordText_4)); }
	inline Text_t1790657652 * get_wordText_4() const { return ___wordText_4; }
	inline Text_t1790657652 ** get_address_of_wordText_4() { return &___wordText_4; }
	inline void set_wordText_4(Text_t1790657652 * value)
	{
		___wordText_4 = value;
		Il2CppCodeGenWriteBarrier((&___wordText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REEL_T2885754470_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef U24ARRAYTYPEU3D12_T4075530830_H
#define U24ARRAYTYPEU3D12_T4075530830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t4075530830 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t4075530830__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T4075530830_H
#ifndef U24ARRAYTYPEU3D64_T1238761204_H
#define U24ARRAYTYPEU3D64_T1238761204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t1238761204 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t1238761204__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T1238761204_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef U24ARRAYTYPEU3D24_T768527802_H
#define U24ARRAYTYPEU3D24_T768527802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t768527802 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t768527802__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T768527802_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef U24ARRAYTYPEU3D8_T2622132287_H
#define U24ARRAYTYPEU3D8_T2622132287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=8
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D8_t2622132287 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D8_t2622132287__padding[8];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D8_T2622132287_H
#ifndef U24ARRAYTYPEU3D16_T684215895_H
#define U24ARRAYTYPEU3D16_T684215895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t684215895 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t684215895__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T684215895_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089822_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2655089822  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-FA2354A15E68A179407C19817026877B0ECB0254
	U24ArrayTypeU3D12_t4075530830  ___U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-41F65FD5B62627EE31EFA9D3F5DA66347E0F7478
	U24ArrayTypeU3D12_t4075530830  ___U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-78517443912BB49729313EC23065D9970ABC80E3
	U24ArrayTypeU3D12_t4075530830  ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-DB1AF13F41A110B000ECFFF980C9691DEE5900BD
	U24ArrayTypeU3D64_t1238761204  ___U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-51A7A390CD6DE245186881400B18C9D822EFE240
	U24ArrayTypeU3D12_t4075530830  ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-C90F38A020811481753795774EB5AF353F414C59
	U24ArrayTypeU3D24_t768527802  ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-FD7391FFB25CC39FA59979E73C93F6EFE1B302BF
	U24ArrayTypeU3D24_t768527802  ___U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6;
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-C480030255DD5E766C1E24628C4C4078F6C34940
	U24ArrayTypeU3D8_t2622132287  ___U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-CBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D
	U24ArrayTypeU3D24_t768527802  ___U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A
	U24ArrayTypeU3D16_t684215895  ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9;

public:
	inline static int32_t get_offset_of_U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0)); }
	inline U24ArrayTypeU3D12_t4075530830  get_U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0() const { return ___U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0; }
	inline U24ArrayTypeU3D12_t4075530830 * get_address_of_U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0() { return &___U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0; }
	inline void set_U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0(U24ArrayTypeU3D12_t4075530830  value)
	{
		___U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1)); }
	inline U24ArrayTypeU3D12_t4075530830  get_U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1() const { return ___U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1; }
	inline U24ArrayTypeU3D12_t4075530830 * get_address_of_U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1() { return &___U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1; }
	inline void set_U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1(U24ArrayTypeU3D12_t4075530830  value)
	{
		___U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2)); }
	inline U24ArrayTypeU3D12_t4075530830  get_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2() const { return ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2; }
	inline U24ArrayTypeU3D12_t4075530830 * get_address_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2() { return &___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2; }
	inline void set_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2(U24ArrayTypeU3D12_t4075530830  value)
	{
		___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3)); }
	inline U24ArrayTypeU3D64_t1238761204  get_U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3() const { return ___U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3; }
	inline U24ArrayTypeU3D64_t1238761204 * get_address_of_U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3() { return &___U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3; }
	inline void set_U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3(U24ArrayTypeU3D64_t1238761204  value)
	{
		___U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4)); }
	inline U24ArrayTypeU3D12_t4075530830  get_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4() const { return ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4; }
	inline U24ArrayTypeU3D12_t4075530830 * get_address_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4() { return &___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4; }
	inline void set_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4(U24ArrayTypeU3D12_t4075530830  value)
	{
		___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5)); }
	inline U24ArrayTypeU3D24_t768527802  get_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5() const { return ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5; }
	inline U24ArrayTypeU3D24_t768527802 * get_address_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5() { return &___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5; }
	inline void set_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5(U24ArrayTypeU3D24_t768527802  value)
	{
		___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6)); }
	inline U24ArrayTypeU3D24_t768527802  get_U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6() const { return ___U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6; }
	inline U24ArrayTypeU3D24_t768527802 * get_address_of_U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6() { return &___U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6; }
	inline void set_U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6(U24ArrayTypeU3D24_t768527802  value)
	{
		___U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7)); }
	inline U24ArrayTypeU3D8_t2622132287  get_U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7() const { return ___U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7; }
	inline U24ArrayTypeU3D8_t2622132287 * get_address_of_U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7() { return &___U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7; }
	inline void set_U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7(U24ArrayTypeU3D8_t2622132287  value)
	{
		___U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8)); }
	inline U24ArrayTypeU3D24_t768527802  get_U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8() const { return ___U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8; }
	inline U24ArrayTypeU3D24_t768527802 * get_address_of_U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8() { return &___U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8; }
	inline void set_U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8(U24ArrayTypeU3D24_t768527802  value)
	{
		___U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields, ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9)); }
	inline U24ArrayTypeU3D16_t684215895  get_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9() const { return ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9; }
	inline U24ArrayTypeU3D16_t684215895 * get_address_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9() { return &___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9; }
	inline void set_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9(U24ArrayTypeU3D16_t684215895  value)
	{
		___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2655089822_H
#ifndef FADESTATE_T852633535_H
#define FADESTATE_T852633535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Whiteout/FadeState
struct  FadeState_t852633535 
{
public:
	// System.Int32 Whiteout/FadeState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FadeState_t852633535, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESTATE_T852633535_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef JSONPARSINGSTATE_T3264473632_H
#define JSONPARSINGSTATE_T3264473632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.JSON.JSONObject/JSONParsingState
struct  JSONParsingState_t3264473632 
{
public:
	// System.Int32 Boomlagoon.JSON.JSONObject/JSONParsingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JSONParsingState_t3264473632, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSINGSTATE_T3264473632_H
#ifndef JSONVALUETYPE_T2936784338_H
#define JSONVALUETYPE_T2936784338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.JSON.JSONValueType
struct  JSONValueType_t2936784338 
{
public:
	// System.Int32 Boomlagoon.JSON.JSONValueType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JSONValueType_t2936784338, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUETYPE_T2936784338_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef JSONVALUE_T3864304964_H
#define JSONVALUE_T3864304964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.JSON.JSONValue
struct  JSONValue_t3864304964  : public RuntimeObject
{
public:
	// Boomlagoon.JSON.JSONValueType Boomlagoon.JSON.JSONValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String Boomlagoon.JSON.JSONValue::<Str>k__BackingField
	String_t* ___U3CStrU3Ek__BackingField_1;
	// System.Double Boomlagoon.JSON.JSONValue::<Number>k__BackingField
	double ___U3CNumberU3Ek__BackingField_2;
	// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONValue::<Obj>k__BackingField
	JSONObject_t2742677291 * ___U3CObjU3Ek__BackingField_3;
	// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONValue::<Array>k__BackingField
	JSONArray_t377107346 * ___U3CArrayU3Ek__BackingField_4;
	// System.Boolean Boomlagoon.JSON.JSONValue::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_5;
	// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::<Parent>k__BackingField
	JSONValue_t3864304964 * ___U3CParentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStrU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CStrU3Ek__BackingField_1)); }
	inline String_t* get_U3CStrU3Ek__BackingField_1() const { return ___U3CStrU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CStrU3Ek__BackingField_1() { return &___U3CStrU3Ek__BackingField_1; }
	inline void set_U3CStrU3Ek__BackingField_1(String_t* value)
	{
		___U3CStrU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStrU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNumberU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CNumberU3Ek__BackingField_2)); }
	inline double get_U3CNumberU3Ek__BackingField_2() const { return ___U3CNumberU3Ek__BackingField_2; }
	inline double* get_address_of_U3CNumberU3Ek__BackingField_2() { return &___U3CNumberU3Ek__BackingField_2; }
	inline void set_U3CNumberU3Ek__BackingField_2(double value)
	{
		___U3CNumberU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CObjU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CObjU3Ek__BackingField_3)); }
	inline JSONObject_t2742677291 * get_U3CObjU3Ek__BackingField_3() const { return ___U3CObjU3Ek__BackingField_3; }
	inline JSONObject_t2742677291 ** get_address_of_U3CObjU3Ek__BackingField_3() { return &___U3CObjU3Ek__BackingField_3; }
	inline void set_U3CObjU3Ek__BackingField_3(JSONObject_t2742677291 * value)
	{
		___U3CObjU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CArrayU3Ek__BackingField_4)); }
	inline JSONArray_t377107346 * get_U3CArrayU3Ek__BackingField_4() const { return ___U3CArrayU3Ek__BackingField_4; }
	inline JSONArray_t377107346 ** get_address_of_U3CArrayU3Ek__BackingField_4() { return &___U3CArrayU3Ek__BackingField_4; }
	inline void set_U3CArrayU3Ek__BackingField_4(JSONArray_t377107346 * value)
	{
		___U3CArrayU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArrayU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CBooleanU3Ek__BackingField_5)); }
	inline bool get_U3CBooleanU3Ek__BackingField_5() const { return ___U3CBooleanU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_5() { return &___U3CBooleanU3Ek__BackingField_5; }
	inline void set_U3CBooleanU3Ek__BackingField_5(bool value)
	{
		___U3CBooleanU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JSONValue_t3864304964, ___U3CParentU3Ek__BackingField_6)); }
	inline JSONValue_t3864304964 * get_U3CParentU3Ek__BackingField_6() const { return ___U3CParentU3Ek__BackingField_6; }
	inline JSONValue_t3864304964 ** get_address_of_U3CParentU3Ek__BackingField_6() { return &___U3CParentU3Ek__BackingField_6; }
	inline void set_U3CParentU3Ek__BackingField_6(JSONValue_t3864304964 * value)
	{
		___U3CParentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUE_T3864304964_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef SHIELDCALLBACK_T1004088674_H
#define SHIELDCALLBACK_T1004088674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldMenu/ShieldCallback
struct  ShieldCallback_t1004088674  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELDCALLBACK_T1004088674_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef FADEFINISHDELEGATE_T1266332645_H
#define FADEFINISHDELEGATE_T1266332645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Whiteout/FadeFinishDelegate
struct  FadeFinishDelegate_t1266332645  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEFINISHDELEGATE_T1266332645_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef MOVINGU20TEXTURE_T723204342_H
#define MOVINGU20TEXTURE_T723204342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moving Texture
struct  MovingU20Texture_t723204342  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Moving Texture::m_fScrollSpeed1
	float ___m_fScrollSpeed1_2;
	// System.Single Moving Texture::m_fScrollSpeed2
	float ___m_fScrollSpeed2_3;

public:
	inline static int32_t get_offset_of_m_fScrollSpeed1_2() { return static_cast<int32_t>(offsetof(MovingU20Texture_t723204342, ___m_fScrollSpeed1_2)); }
	inline float get_m_fScrollSpeed1_2() const { return ___m_fScrollSpeed1_2; }
	inline float* get_address_of_m_fScrollSpeed1_2() { return &___m_fScrollSpeed1_2; }
	inline void set_m_fScrollSpeed1_2(float value)
	{
		___m_fScrollSpeed1_2 = value;
	}

	inline static int32_t get_offset_of_m_fScrollSpeed2_3() { return static_cast<int32_t>(offsetof(MovingU20Texture_t723204342, ___m_fScrollSpeed2_3)); }
	inline float get_m_fScrollSpeed2_3() const { return ___m_fScrollSpeed2_3; }
	inline float* get_address_of_m_fScrollSpeed2_3() { return &___m_fScrollSpeed2_3; }
	inline void set_m_fScrollSpeed2_3(float value)
	{
		___m_fScrollSpeed2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVINGU20TEXTURE_T723204342_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3765746229_H
#define TIMEDOBJECTDESTRUCTOR_T3765746229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedObjectDestructor
struct  TimedObjectDestructor_t3765746229  : public MonoBehaviour_t1618594486
{
public:
	// System.Single TimedObjectDestructor::timeOut
	float ___timeOut_2;
	// System.Boolean TimedObjectDestructor::detachChildren
	bool ___detachChildren_3;

public:
	inline static int32_t get_offset_of_timeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3765746229, ___timeOut_2)); }
	inline float get_timeOut_2() const { return ___timeOut_2; }
	inline float* get_address_of_timeOut_2() { return &___timeOut_2; }
	inline void set_timeOut_2(float value)
	{
		___timeOut_2 = value;
	}

	inline static int32_t get_offset_of_detachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3765746229, ___detachChildren_3)); }
	inline bool get_detachChildren_3() const { return ___detachChildren_3; }
	inline bool* get_address_of_detachChildren_3() { return &___detachChildren_3; }
	inline void set_detachChildren_3(bool value)
	{
		___detachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3765746229_H
#ifndef WHITEOUT_T3878335910_H
#define WHITEOUT_T3878335910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Whiteout
struct  Whiteout_t3878335910  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Whiteout::fadeSpeed
	float ___fadeSpeed_2;
	// Whiteout/FadeFinishDelegate Whiteout::del
	FadeFinishDelegate_t1266332645 * ___del_3;
	// Whiteout/FadeState Whiteout::fadeState
	int32_t ___fadeState_4;
	// UnityEngine.UI.Image Whiteout::myImage
	Image_t2816987602 * ___myImage_5;
	// System.Single Whiteout::alpha
	float ___alpha_6;

public:
	inline static int32_t get_offset_of_fadeSpeed_2() { return static_cast<int32_t>(offsetof(Whiteout_t3878335910, ___fadeSpeed_2)); }
	inline float get_fadeSpeed_2() const { return ___fadeSpeed_2; }
	inline float* get_address_of_fadeSpeed_2() { return &___fadeSpeed_2; }
	inline void set_fadeSpeed_2(float value)
	{
		___fadeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_del_3() { return static_cast<int32_t>(offsetof(Whiteout_t3878335910, ___del_3)); }
	inline FadeFinishDelegate_t1266332645 * get_del_3() const { return ___del_3; }
	inline FadeFinishDelegate_t1266332645 ** get_address_of_del_3() { return &___del_3; }
	inline void set_del_3(FadeFinishDelegate_t1266332645 * value)
	{
		___del_3 = value;
		Il2CppCodeGenWriteBarrier((&___del_3), value);
	}

	inline static int32_t get_offset_of_fadeState_4() { return static_cast<int32_t>(offsetof(Whiteout_t3878335910, ___fadeState_4)); }
	inline int32_t get_fadeState_4() const { return ___fadeState_4; }
	inline int32_t* get_address_of_fadeState_4() { return &___fadeState_4; }
	inline void set_fadeState_4(int32_t value)
	{
		___fadeState_4 = value;
	}

	inline static int32_t get_offset_of_myImage_5() { return static_cast<int32_t>(offsetof(Whiteout_t3878335910, ___myImage_5)); }
	inline Image_t2816987602 * get_myImage_5() const { return ___myImage_5; }
	inline Image_t2816987602 ** get_address_of_myImage_5() { return &___myImage_5; }
	inline void set_myImage_5(Image_t2816987602 * value)
	{
		___myImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___myImage_5), value);
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(Whiteout_t3878335910, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITEOUT_T3878335910_H
#ifndef MAGICLIGHTFLICKER_T4078483488_H
#define MAGICLIGHTFLICKER_T4078483488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicLightFlicker
struct  MagicLightFlicker_t4078483488  : public MonoBehaviour_t1618594486
{
public:
	// System.String MagicLightFlicker::waveFunction
	String_t* ___waveFunction_2;
	// System.Single MagicLightFlicker::base
	float ___base_3;
	// System.Single MagicLightFlicker::amplitude
	float ___amplitude_4;
	// System.Single MagicLightFlicker::phase
	float ___phase_5;
	// System.Single MagicLightFlicker::frequency
	float ___frequency_6;
	// UnityEngine.Color MagicLightFlicker::originalColor
	Color_t2582018970  ___originalColor_7;

public:
	inline static int32_t get_offset_of_waveFunction_2() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___waveFunction_2)); }
	inline String_t* get_waveFunction_2() const { return ___waveFunction_2; }
	inline String_t** get_address_of_waveFunction_2() { return &___waveFunction_2; }
	inline void set_waveFunction_2(String_t* value)
	{
		___waveFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___waveFunction_2), value);
	}

	inline static int32_t get_offset_of_base_3() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___base_3)); }
	inline float get_base_3() const { return ___base_3; }
	inline float* get_address_of_base_3() { return &___base_3; }
	inline void set_base_3(float value)
	{
		___base_3 = value;
	}

	inline static int32_t get_offset_of_amplitude_4() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___amplitude_4)); }
	inline float get_amplitude_4() const { return ___amplitude_4; }
	inline float* get_address_of_amplitude_4() { return &___amplitude_4; }
	inline void set_amplitude_4(float value)
	{
		___amplitude_4 = value;
	}

	inline static int32_t get_offset_of_phase_5() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___phase_5)); }
	inline float get_phase_5() const { return ___phase_5; }
	inline float* get_address_of_phase_5() { return &___phase_5; }
	inline void set_phase_5(float value)
	{
		___phase_5 = value;
	}

	inline static int32_t get_offset_of_frequency_6() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___frequency_6)); }
	inline float get_frequency_6() const { return ___frequency_6; }
	inline float* get_address_of_frequency_6() { return &___frequency_6; }
	inline void set_frequency_6(float value)
	{
		___frequency_6 = value;
	}

	inline static int32_t get_offset_of_originalColor_7() { return static_cast<int32_t>(offsetof(MagicLightFlicker_t4078483488, ___originalColor_7)); }
	inline Color_t2582018970  get_originalColor_7() const { return ___originalColor_7; }
	inline Color_t2582018970 * get_address_of_originalColor_7() { return &___originalColor_7; }
	inline void set_originalColor_7(Color_t2582018970  value)
	{
		___originalColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICLIGHTFLICKER_T4078483488_H
#ifndef WHATIDIDHOWIFELT_T602585838_H
#define WHATIDIDHOWIFELT_T602585838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WhatIDidHowIFelt
struct  WhatIDidHowIFelt_t602585838  : public MonoBehaviour_t1618594486
{
public:
	// HowIFeltFaces[] WhatIDidHowIFelt::faces
	HowIFeltFacesU5BU5D_t3946832269* ___faces_2;

public:
	inline static int32_t get_offset_of_faces_2() { return static_cast<int32_t>(offsetof(WhatIDidHowIFelt_t602585838, ___faces_2)); }
	inline HowIFeltFacesU5BU5D_t3946832269* get_faces_2() const { return ___faces_2; }
	inline HowIFeltFacesU5BU5D_t3946832269** get_address_of_faces_2() { return &___faces_2; }
	inline void set_faces_2(HowIFeltFacesU5BU5D_t3946832269* value)
	{
		___faces_2 = value;
		Il2CppCodeGenWriteBarrier((&___faces_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHATIDIDHOWIFELT_T602585838_H
#ifndef TESTENABLE_T1673577313_H
#define TESTENABLE_T1673577313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TESTENABLE
struct  TESTENABLE_t1673577313  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTENABLE_T1673577313_H
#ifndef TOOLTIPBOX_T3451613534_H
#define TOOLTIPBOX_T3451613534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TooltipBox
struct  TooltipBox_t3451613534  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Text TooltipBox::toolTipText
	Text_t1790657652 * ___toolTipText_2;
	// System.Boolean TooltipBox::bTextSet
	bool ___bTextSet_3;

public:
	inline static int32_t get_offset_of_toolTipText_2() { return static_cast<int32_t>(offsetof(TooltipBox_t3451613534, ___toolTipText_2)); }
	inline Text_t1790657652 * get_toolTipText_2() const { return ___toolTipText_2; }
	inline Text_t1790657652 ** get_address_of_toolTipText_2() { return &___toolTipText_2; }
	inline void set_toolTipText_2(Text_t1790657652 * value)
	{
		___toolTipText_2 = value;
		Il2CppCodeGenWriteBarrier((&___toolTipText_2), value);
	}

	inline static int32_t get_offset_of_bTextSet_3() { return static_cast<int32_t>(offsetof(TooltipBox_t3451613534, ___bTextSet_3)); }
	inline bool get_bTextSet_3() const { return ___bTextSet_3; }
	inline bool* get_address_of_bTextSet_3() { return &___bTextSet_3; }
	inline void set_bTextSet_3(bool value)
	{
		___bTextSet_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPBOX_T3451613534_H
#ifndef LEVELSAVEONLOAD_T2042969378_H
#define LEVELSAVEONLOAD_T2042969378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// levelSaveOnLoad
struct  levelSaveOnLoad_t2042969378  : public MonoBehaviour_t1618594486
{
public:
	// CapturedDataInput levelSaveOnLoad::instance
	CapturedDataInput_t2616152122 * ___instance_2;
	// System.String levelSaveOnLoad::currentSavePoint
	String_t* ___currentSavePoint_3;
	// System.Boolean levelSaveOnLoad::levelComplete
	bool ___levelComplete_4;
	// System.String levelSaveOnLoad::feedbackData
	String_t* ___feedbackData_5;
	// System.String levelSaveOnLoad::failedFileName
	String_t* ___failedFileName_7;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378, ___instance_2)); }
	inline CapturedDataInput_t2616152122 * get_instance_2() const { return ___instance_2; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CapturedDataInput_t2616152122 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_currentSavePoint_3() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378, ___currentSavePoint_3)); }
	inline String_t* get_currentSavePoint_3() const { return ___currentSavePoint_3; }
	inline String_t** get_address_of_currentSavePoint_3() { return &___currentSavePoint_3; }
	inline void set_currentSavePoint_3(String_t* value)
	{
		___currentSavePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentSavePoint_3), value);
	}

	inline static int32_t get_offset_of_levelComplete_4() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378, ___levelComplete_4)); }
	inline bool get_levelComplete_4() const { return ___levelComplete_4; }
	inline bool* get_address_of_levelComplete_4() { return &___levelComplete_4; }
	inline void set_levelComplete_4(bool value)
	{
		___levelComplete_4 = value;
	}

	inline static int32_t get_offset_of_feedbackData_5() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378, ___feedbackData_5)); }
	inline String_t* get_feedbackData_5() const { return ___feedbackData_5; }
	inline String_t** get_address_of_feedbackData_5() { return &___feedbackData_5; }
	inline void set_feedbackData_5(String_t* value)
	{
		___feedbackData_5 = value;
		Il2CppCodeGenWriteBarrier((&___feedbackData_5), value);
	}

	inline static int32_t get_offset_of_failedFileName_7() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378, ___failedFileName_7)); }
	inline String_t* get_failedFileName_7() const { return ___failedFileName_7; }
	inline String_t** get_address_of_failedFileName_7() { return &___failedFileName_7; }
	inline void set_failedFileName_7(String_t* value)
	{
		___failedFileName_7 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_7), value);
	}
};

struct levelSaveOnLoad_t2042969378_StaticFields
{
public:
	// UnityEngine.GameObject levelSaveOnLoad::saveTriggerToggle
	GameObject_t2557347079 * ___saveTriggerToggle_6;

public:
	inline static int32_t get_offset_of_saveTriggerToggle_6() { return static_cast<int32_t>(offsetof(levelSaveOnLoad_t2042969378_StaticFields, ___saveTriggerToggle_6)); }
	inline GameObject_t2557347079 * get_saveTriggerToggle_6() const { return ___saveTriggerToggle_6; }
	inline GameObject_t2557347079 ** get_address_of_saveTriggerToggle_6() { return &___saveTriggerToggle_6; }
	inline void set_saveTriggerToggle_6(GameObject_t2557347079 * value)
	{
		___saveTriggerToggle_6 = value;
		Il2CppCodeGenWriteBarrier((&___saveTriggerToggle_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSAVEONLOAD_T2042969378_H
#ifndef LEVELSAVETRIGGER_T575094362_H
#define LEVELSAVETRIGGER_T575094362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// levelSaveTrigger
struct  levelSaveTrigger_t575094362  : public MonoBehaviour_t1618594486
{
public:
	// CapturedDataInput levelSaveTrigger::instance
	CapturedDataInput_t2616152122 * ___instance_2;
	// System.String levelSaveTrigger::levelMidSavePoint
	String_t* ___levelMidSavePoint_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(levelSaveTrigger_t575094362, ___instance_2)); }
	inline CapturedDataInput_t2616152122 * get_instance_2() const { return ___instance_2; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CapturedDataInput_t2616152122 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_levelMidSavePoint_3() { return static_cast<int32_t>(offsetof(levelSaveTrigger_t575094362, ___levelMidSavePoint_3)); }
	inline String_t* get_levelMidSavePoint_3() const { return ___levelMidSavePoint_3; }
	inline String_t** get_address_of_levelMidSavePoint_3() { return &___levelMidSavePoint_3; }
	inline void set_levelMidSavePoint_3(String_t* value)
	{
		___levelMidSavePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelMidSavePoint_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSAVETRIGGER_T575094362_H
#ifndef LOADDICTIONARYVARS_T2169303635_H
#define LOADDICTIONARYVARS_T2169303635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// loadDictionaryVars
struct  loadDictionaryVars_t2169303635  : public MonoBehaviour_t1618594486
{
public:

public:
};

struct loadDictionaryVars_t2169303635_StaticFields
{
public:
	// UnityEngine.Vector3 loadDictionaryVars::tempPosition
	Vector3_t1986933152  ___tempPosition_2;

public:
	inline static int32_t get_offset_of_tempPosition_2() { return static_cast<int32_t>(offsetof(loadDictionaryVars_t2169303635_StaticFields, ___tempPosition_2)); }
	inline Vector3_t1986933152  get_tempPosition_2() const { return ___tempPosition_2; }
	inline Vector3_t1986933152 * get_address_of_tempPosition_2() { return &___tempPosition_2; }
	inline void set_tempPosition_2(Vector3_t1986933152  value)
	{
		___tempPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADDICTIONARYVARS_T2169303635_H
#ifndef SAVINGICON_T1459161706_H
#define SAVINGICON_T1459161706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// savingIcon
struct  savingIcon_t1459161706  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Texture savingIcon::saveShieldIcon
	Texture_t2119925672 * ___saveShieldIcon_2;

public:
	inline static int32_t get_offset_of_saveShieldIcon_2() { return static_cast<int32_t>(offsetof(savingIcon_t1459161706, ___saveShieldIcon_2)); }
	inline Texture_t2119925672 * get_saveShieldIcon_2() const { return ___saveShieldIcon_2; }
	inline Texture_t2119925672 ** get_address_of_saveShieldIcon_2() { return &___saveShieldIcon_2; }
	inline void set_saveShieldIcon_2(Texture_t2119925672 * value)
	{
		___saveShieldIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___saveShieldIcon_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVINGICON_T1459161706_H
#ifndef SCENECONTROLLER_T816226515_H
#define SCENECONTROLLER_T816226515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneController
struct  SceneController_t816226515  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTROLLER_T816226515_H
#ifndef SCREENLOGGER_T129510507_H
#define SCREENLOGGER_T129510507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenLogger
struct  ScreenLogger_t129510507  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin ScreenLogger::optionalSkin
	GUISkin_t2122630221 * ___optionalSkin_5;
	// System.Collections.Generic.List`1<System.String> ScreenLogger::screenStrings
	List_1_t4069179741 * ___screenStrings_6;
	// System.Collections.Generic.List`1<System.String> ScreenLogger::singleFrameStrings
	List_1_t4069179741 * ___singleFrameStrings_7;

public:
	inline static int32_t get_offset_of_optionalSkin_5() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507, ___optionalSkin_5)); }
	inline GUISkin_t2122630221 * get_optionalSkin_5() const { return ___optionalSkin_5; }
	inline GUISkin_t2122630221 ** get_address_of_optionalSkin_5() { return &___optionalSkin_5; }
	inline void set_optionalSkin_5(GUISkin_t2122630221 * value)
	{
		___optionalSkin_5 = value;
		Il2CppCodeGenWriteBarrier((&___optionalSkin_5), value);
	}

	inline static int32_t get_offset_of_screenStrings_6() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507, ___screenStrings_6)); }
	inline List_1_t4069179741 * get_screenStrings_6() const { return ___screenStrings_6; }
	inline List_1_t4069179741 ** get_address_of_screenStrings_6() { return &___screenStrings_6; }
	inline void set_screenStrings_6(List_1_t4069179741 * value)
	{
		___screenStrings_6 = value;
		Il2CppCodeGenWriteBarrier((&___screenStrings_6), value);
	}

	inline static int32_t get_offset_of_singleFrameStrings_7() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507, ___singleFrameStrings_7)); }
	inline List_1_t4069179741 * get_singleFrameStrings_7() const { return ___singleFrameStrings_7; }
	inline List_1_t4069179741 ** get_address_of_singleFrameStrings_7() { return &___singleFrameStrings_7; }
	inline void set_singleFrameStrings_7(List_1_t4069179741 * value)
	{
		___singleFrameStrings_7 = value;
		Il2CppCodeGenWriteBarrier((&___singleFrameStrings_7), value);
	}
};

struct ScreenLogger_t129510507_StaticFields
{
public:
	// ScreenLogger ScreenLogger::instance
	ScreenLogger_t129510507 * ___instance_2;
	// System.Int32 ScreenLogger::maxStrings
	int32_t ___maxStrings_3;
	// System.Boolean ScreenLogger::showStrings
	bool ___showStrings_4;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507_StaticFields, ___instance_2)); }
	inline ScreenLogger_t129510507 * get_instance_2() const { return ___instance_2; }
	inline ScreenLogger_t129510507 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ScreenLogger_t129510507 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_maxStrings_3() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507_StaticFields, ___maxStrings_3)); }
	inline int32_t get_maxStrings_3() const { return ___maxStrings_3; }
	inline int32_t* get_address_of_maxStrings_3() { return &___maxStrings_3; }
	inline void set_maxStrings_3(int32_t value)
	{
		___maxStrings_3 = value;
	}

	inline static int32_t get_offset_of_showStrings_4() { return static_cast<int32_t>(offsetof(ScreenLogger_t129510507_StaticFields, ___showStrings_4)); }
	inline bool get_showStrings_4() const { return ___showStrings_4; }
	inline bool* get_address_of_showStrings_4() { return &___showStrings_4; }
	inline void set_showStrings_4(bool value)
	{
		___showStrings_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENLOGGER_T129510507_H
#ifndef TOKENHOLDERLEGACY_T1380075429_H
#define TOKENHOLDERLEGACY_T1380075429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TokenHolderLegacy
struct  TokenHolderLegacy_t1380075429  : public MonoBehaviour_t1618594486
{
public:
	// System.String TokenHolderLegacy::Global_sGameConfigServerURL
	String_t* ___Global_sGameConfigServerURL_3;
	// System.String TokenHolderLegacy::username
	String_t* ___username_4;
	// System.String TokenHolderLegacy::password
	String_t* ___password_5;

public:
	inline static int32_t get_offset_of_Global_sGameConfigServerURL_3() { return static_cast<int32_t>(offsetof(TokenHolderLegacy_t1380075429, ___Global_sGameConfigServerURL_3)); }
	inline String_t* get_Global_sGameConfigServerURL_3() const { return ___Global_sGameConfigServerURL_3; }
	inline String_t** get_address_of_Global_sGameConfigServerURL_3() { return &___Global_sGameConfigServerURL_3; }
	inline void set_Global_sGameConfigServerURL_3(String_t* value)
	{
		___Global_sGameConfigServerURL_3 = value;
		Il2CppCodeGenWriteBarrier((&___Global_sGameConfigServerURL_3), value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(TokenHolderLegacy_t1380075429, ___username_4)); }
	inline String_t* get_username_4() const { return ___username_4; }
	inline String_t** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(String_t* value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier((&___username_4), value);
	}

	inline static int32_t get_offset_of_password_5() { return static_cast<int32_t>(offsetof(TokenHolderLegacy_t1380075429, ___password_5)); }
	inline String_t* get_password_5() const { return ___password_5; }
	inline String_t** get_address_of_password_5() { return &___password_5; }
	inline void set_password_5(String_t* value)
	{
		___password_5 = value;
		Il2CppCodeGenWriteBarrier((&___password_5), value);
	}
};

struct TokenHolderLegacy_t1380075429_StaticFields
{
public:
	// TokenHolderLegacy TokenHolderLegacy::instanceRef
	TokenHolderLegacy_t1380075429 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(TokenHolderLegacy_t1380075429_StaticFields, ___instanceRef_2)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENHOLDERLEGACY_T1380075429_H
#ifndef GUIDEENDTRANSITION_T2209539835_H
#define GUIDEENDTRANSITION_T2209539835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuideEndTransition
struct  GuideEndTransition_t2209539835  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin GuideEndTransition::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Single GuideEndTransition::yPosition
	float ___yPosition_3;
	// System.Single GuideEndTransition::fTime
	float ___fTime_4;
	// PlayerInfoLoadOnLevelLoad GuideEndTransition::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_5;
	// UnityEngine.GameObject GuideEndTransition::whiteout
	GameObject_t2557347079 * ___whiteout_6;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(GuideEndTransition_t2209539835, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_yPosition_3() { return static_cast<int32_t>(offsetof(GuideEndTransition_t2209539835, ___yPosition_3)); }
	inline float get_yPosition_3() const { return ___yPosition_3; }
	inline float* get_address_of_yPosition_3() { return &___yPosition_3; }
	inline void set_yPosition_3(float value)
	{
		___yPosition_3 = value;
	}

	inline static int32_t get_offset_of_fTime_4() { return static_cast<int32_t>(offsetof(GuideEndTransition_t2209539835, ___fTime_4)); }
	inline float get_fTime_4() const { return ___fTime_4; }
	inline float* get_address_of_fTime_4() { return &___fTime_4; }
	inline void set_fTime_4(float value)
	{
		___fTime_4 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_5() { return static_cast<int32_t>(offsetof(GuideEndTransition_t2209539835, ___m_PlayerInfoLevelLoad_5)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_5() const { return ___m_PlayerInfoLevelLoad_5; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_5() { return &___m_PlayerInfoLevelLoad_5; }
	inline void set_m_PlayerInfoLevelLoad_5(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_5), value);
	}

	inline static int32_t get_offset_of_whiteout_6() { return static_cast<int32_t>(offsetof(GuideEndTransition_t2209539835, ___whiteout_6)); }
	inline GameObject_t2557347079 * get_whiteout_6() const { return ___whiteout_6; }
	inline GameObject_t2557347079 ** get_address_of_whiteout_6() { return &___whiteout_6; }
	inline void set_whiteout_6(GameObject_t2557347079 * value)
	{
		___whiteout_6 = value;
		Il2CppCodeGenWriteBarrier((&___whiteout_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEENDTRANSITION_T2209539835_H
#ifndef ICECAVESPASHSCREEN_T2468610820_H
#define ICECAVESPASHSCREEN_T2468610820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceCaveSpashScreen
struct  IceCaveSpashScreen_t2468610820  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean IceCaveSpashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin IceCaveSpashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single IceCaveSpashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single IceCaveSpashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad IceCaveSpashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput IceCaveSpashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(IceCaveSpashScreen_t2468610820, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICECAVESPASHSCREEN_T2468610820_H
#ifndef ISLANDSPLASHSCREEN_T1175684869_H
#define ISLANDSPLASHSCREEN_T1175684869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IslandSplashScreen
struct  IslandSplashScreen_t1175684869  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin IslandSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Single IslandSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_3;
	// System.Single IslandSplashScreen::m_fTime
	float ___m_fTime_4;
	// System.Single IslandSplashScreen::m_fMoveTime
	float ___m_fMoveTime_5;
	// LoadTalkSceneXMLData IslandSplashScreen::m_loadSceneData
	LoadTalkSceneXMLData_t2381108950 * ___m_loadSceneData_6;
	// CapturedDataInput IslandSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;
	// System.Boolean IslandSplashScreen::m_bFadeIn
	bool ___m_bFadeIn_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_3() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_fFadeInTheGuideAlpha_3)); }
	inline float get_m_fFadeInTheGuideAlpha_3() const { return ___m_fFadeInTheGuideAlpha_3; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_3() { return &___m_fFadeInTheGuideAlpha_3; }
	inline void set_m_fFadeInTheGuideAlpha_3(float value)
	{
		___m_fFadeInTheGuideAlpha_3 = value;
	}

	inline static int32_t get_offset_of_m_fTime_4() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_fTime_4)); }
	inline float get_m_fTime_4() const { return ___m_fTime_4; }
	inline float* get_address_of_m_fTime_4() { return &___m_fTime_4; }
	inline void set_m_fTime_4(float value)
	{
		___m_fTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fMoveTime_5() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_fMoveTime_5)); }
	inline float get_m_fMoveTime_5() const { return ___m_fMoveTime_5; }
	inline float* get_address_of_m_fMoveTime_5() { return &___m_fMoveTime_5; }
	inline void set_m_fMoveTime_5(float value)
	{
		___m_fMoveTime_5 = value;
	}

	inline static int32_t get_offset_of_m_loadSceneData_6() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_loadSceneData_6)); }
	inline LoadTalkSceneXMLData_t2381108950 * get_m_loadSceneData_6() const { return ___m_loadSceneData_6; }
	inline LoadTalkSceneXMLData_t2381108950 ** get_address_of_m_loadSceneData_6() { return &___m_loadSceneData_6; }
	inline void set_m_loadSceneData_6(LoadTalkSceneXMLData_t2381108950 * value)
	{
		___m_loadSceneData_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_loadSceneData_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}

	inline static int32_t get_offset_of_m_bFadeIn_8() { return static_cast<int32_t>(offsetof(IslandSplashScreen_t1175684869, ___m_bFadeIn_8)); }
	inline bool get_m_bFadeIn_8() const { return ___m_bFadeIn_8; }
	inline bool* get_address_of_m_bFadeIn_8() { return &___m_bFadeIn_8; }
	inline void set_m_bFadeIn_8(bool value)
	{
		___m_bFadeIn_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISLANDSPLASHSCREEN_T1175684869_H
#ifndef MOUTAINCAVESPLASHSCREEN_T3103079625_H
#define MOUTAINCAVESPLASHSCREEN_T3103079625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoutainCaveSplashScreen
struct  MoutainCaveSplashScreen_t3103079625  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean MoutainCaveSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin MoutainCaveSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single MoutainCaveSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single MoutainCaveSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad MoutainCaveSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput MoutainCaveSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(MoutainCaveSplashScreen_t3103079625, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUTAINCAVESPLASHSCREEN_T3103079625_H
#ifndef THEBRIDGESPLASHSCREEN_T1192841672_H
#define THEBRIDGESPLASHSCREEN_T1192841672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheBridgeSplashScreen
struct  TheBridgeSplashScreen_t1192841672  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TheBridgeSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin TheBridgeSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single TheBridgeSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single TheBridgeSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad TheBridgeSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput TheBridgeSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(TheBridgeSplashScreen_t1192841672, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEBRIDGESPLASHSCREEN_T1192841672_H
#ifndef THECANYONSPLASHSCREEN_T4023409967_H
#define THECANYONSPLASHSCREEN_T4023409967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheCanyonSplashScreen
struct  TheCanyonSplashScreen_t4023409967  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TheCanyonSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin TheCanyonSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single TheCanyonSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single TheCanyonSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad TheCanyonSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput TheCanyonSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(TheCanyonSplashScreen_t4023409967, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THECANYONSPLASHSCREEN_T4023409967_H
#ifndef THECAVESPLASHSCREEN_T265279536_H
#define THECAVESPLASHSCREEN_T265279536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheCaveSplashScreen
struct  TheCaveSplashScreen_t265279536  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TheCaveSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin TheCaveSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single TheCaveSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single TheCaveSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad TheCaveSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput TheCaveSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(TheCaveSplashScreen_t265279536, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THECAVESPLASHSCREEN_T265279536_H
#ifndef THEGUIDESPLASHSCREEN_T2843531037_H
#define THEGUIDESPLASHSCREEN_T2843531037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheGuideSplashScreen
struct  TheGuideSplashScreen_t2843531037  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin TheGuideSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Single TheGuideSplashScreen::m_fFadeInStartTime
	float ___m_fFadeInStartTime_3;
	// System.Single TheGuideSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// LoadTalkSceneXMLData TheGuideSplashScreen::m_loadSceneData
	LoadTalkSceneXMLData_t2381108950 * ___m_loadSceneData_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(TheGuideSplashScreen_t2843531037, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_3() { return static_cast<int32_t>(offsetof(TheGuideSplashScreen_t2843531037, ___m_fFadeInStartTime_3)); }
	inline float get_m_fFadeInStartTime_3() const { return ___m_fFadeInStartTime_3; }
	inline float* get_address_of_m_fFadeInStartTime_3() { return &___m_fFadeInStartTime_3; }
	inline void set_m_fFadeInStartTime_3(float value)
	{
		___m_fFadeInStartTime_3 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(TheGuideSplashScreen_t2843531037, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_loadSceneData_5() { return static_cast<int32_t>(offsetof(TheGuideSplashScreen_t2843531037, ___m_loadSceneData_5)); }
	inline LoadTalkSceneXMLData_t2381108950 * get_m_loadSceneData_5() const { return ___m_loadSceneData_5; }
	inline LoadTalkSceneXMLData_t2381108950 ** get_address_of_m_loadSceneData_5() { return &___m_loadSceneData_5; }
	inline void set_m_loadSceneData_5(LoadTalkSceneXMLData_t2381108950 * value)
	{
		___m_loadSceneData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_loadSceneData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEGUIDESPLASHSCREEN_T2843531037_H
#ifndef THESWAMPSPLASHSCREEN_T3703549283_H
#define THESWAMPSPLASHSCREEN_T3703549283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheSwampSplashScreen
struct  TheSwampSplashScreen_t3703549283  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TheSwampSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin TheSwampSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single TheSwampSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single TheSwampSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad TheSwampSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput TheSwampSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(TheSwampSplashScreen_t3703549283, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THESWAMPSPLASHSCREEN_T3703549283_H
#ifndef VENCANOCAVESPLASHSCREEN_T159642342_H
#define VENCANOCAVESPLASHSCREEN_T159642342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VencanoCaveSplashScreen
struct  VencanoCaveSplashScreen_t159642342  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean VencanoCaveSplashScreen::m_bIsFadeIn
	bool ___m_bIsFadeIn_2;
	// UnityEngine.GUISkin VencanoCaveSplashScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.Single VencanoCaveSplashScreen::m_fFadeInTheGuideAlpha
	float ___m_fFadeInTheGuideAlpha_4;
	// System.Single VencanoCaveSplashScreen::m_fTime
	float ___m_fTime_5;
	// PlayerInfoLoadOnLevelLoad VencanoCaveSplashScreen::m_PlayerInfoLevelLoad
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___m_PlayerInfoLevelLoad_6;
	// CapturedDataInput VencanoCaveSplashScreen::instance
	CapturedDataInput_t2616152122 * ___instance_7;

public:
	inline static int32_t get_offset_of_m_bIsFadeIn_2() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___m_bIsFadeIn_2)); }
	inline bool get_m_bIsFadeIn_2() const { return ___m_bIsFadeIn_2; }
	inline bool* get_address_of_m_bIsFadeIn_2() { return &___m_bIsFadeIn_2; }
	inline void set_m_bIsFadeIn_2(bool value)
	{
		___m_bIsFadeIn_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_fFadeInTheGuideAlpha_4() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___m_fFadeInTheGuideAlpha_4)); }
	inline float get_m_fFadeInTheGuideAlpha_4() const { return ___m_fFadeInTheGuideAlpha_4; }
	inline float* get_address_of_m_fFadeInTheGuideAlpha_4() { return &___m_fFadeInTheGuideAlpha_4; }
	inline void set_m_fFadeInTheGuideAlpha_4(float value)
	{
		___m_fFadeInTheGuideAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}

	inline static int32_t get_offset_of_m_PlayerInfoLevelLoad_6() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___m_PlayerInfoLevelLoad_6)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_m_PlayerInfoLevelLoad_6() const { return ___m_PlayerInfoLevelLoad_6; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_m_PlayerInfoLevelLoad_6() { return &___m_PlayerInfoLevelLoad_6; }
	inline void set_m_PlayerInfoLevelLoad_6(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___m_PlayerInfoLevelLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerInfoLevelLoad_6), value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(VencanoCaveSplashScreen_t159642342, ___instance_7)); }
	inline CapturedDataInput_t2616152122 * get_instance_7() const { return ___instance_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(CapturedDataInput_t2616152122 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VENCANOCAVESPLASHSCREEN_T159642342_H
#ifndef GUIDESCENETRANSITION_T3184135131_H
#define GUIDESCENETRANSITION_T3184135131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuideSceneTransition
struct  GuideSceneTransition_t3184135131  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDESCENETRANSITION_T3184135131_H
#ifndef SHIELDMENU_T3436809847_H
#define SHIELDMENU_T3436809847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldMenu
struct  ShieldMenu_t3436809847  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Text[] ShieldMenu::shieldText
	TextU5BU5D_t1489827645* ___shieldText_2;
	// UnityEngine.GameObject[] ShieldMenu::words
	GameObjectU5BU5D_t2988620542* ___words_3;
	// UnityEngine.GameObject[] ShieldMenu::colouredWords
	GameObjectU5BU5D_t2988620542* ___colouredWords_4;
	// System.Collections.Generic.List`1<System.String> ShieldMenu::descriptions
	List_1_t4069179741 * ___descriptions_5;
	// UnityEngine.GameObject ShieldMenu::descriptionBox
	GameObject_t2557347079 * ___descriptionBox_6;
	// UnityEngine.GameObject ShieldMenu::nextButton
	GameObject_t2557347079 * ___nextButton_7;
	// System.Boolean ShieldMenu::wordsClickable
	bool ___wordsClickable_8;
	// System.Boolean ShieldMenu::nextClicked
	bool ___nextClicked_9;
	// ShieldMenu/ShieldCallback ShieldMenu::callback
	ShieldCallback_t1004088674 * ___callback_10;

public:
	inline static int32_t get_offset_of_shieldText_2() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___shieldText_2)); }
	inline TextU5BU5D_t1489827645* get_shieldText_2() const { return ___shieldText_2; }
	inline TextU5BU5D_t1489827645** get_address_of_shieldText_2() { return &___shieldText_2; }
	inline void set_shieldText_2(TextU5BU5D_t1489827645* value)
	{
		___shieldText_2 = value;
		Il2CppCodeGenWriteBarrier((&___shieldText_2), value);
	}

	inline static int32_t get_offset_of_words_3() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___words_3)); }
	inline GameObjectU5BU5D_t2988620542* get_words_3() const { return ___words_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_words_3() { return &___words_3; }
	inline void set_words_3(GameObjectU5BU5D_t2988620542* value)
	{
		___words_3 = value;
		Il2CppCodeGenWriteBarrier((&___words_3), value);
	}

	inline static int32_t get_offset_of_colouredWords_4() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___colouredWords_4)); }
	inline GameObjectU5BU5D_t2988620542* get_colouredWords_4() const { return ___colouredWords_4; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_colouredWords_4() { return &___colouredWords_4; }
	inline void set_colouredWords_4(GameObjectU5BU5D_t2988620542* value)
	{
		___colouredWords_4 = value;
		Il2CppCodeGenWriteBarrier((&___colouredWords_4), value);
	}

	inline static int32_t get_offset_of_descriptions_5() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___descriptions_5)); }
	inline List_1_t4069179741 * get_descriptions_5() const { return ___descriptions_5; }
	inline List_1_t4069179741 ** get_address_of_descriptions_5() { return &___descriptions_5; }
	inline void set_descriptions_5(List_1_t4069179741 * value)
	{
		___descriptions_5 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_5), value);
	}

	inline static int32_t get_offset_of_descriptionBox_6() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___descriptionBox_6)); }
	inline GameObject_t2557347079 * get_descriptionBox_6() const { return ___descriptionBox_6; }
	inline GameObject_t2557347079 ** get_address_of_descriptionBox_6() { return &___descriptionBox_6; }
	inline void set_descriptionBox_6(GameObject_t2557347079 * value)
	{
		___descriptionBox_6 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionBox_6), value);
	}

	inline static int32_t get_offset_of_nextButton_7() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___nextButton_7)); }
	inline GameObject_t2557347079 * get_nextButton_7() const { return ___nextButton_7; }
	inline GameObject_t2557347079 ** get_address_of_nextButton_7() { return &___nextButton_7; }
	inline void set_nextButton_7(GameObject_t2557347079 * value)
	{
		___nextButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_7), value);
	}

	inline static int32_t get_offset_of_wordsClickable_8() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___wordsClickable_8)); }
	inline bool get_wordsClickable_8() const { return ___wordsClickable_8; }
	inline bool* get_address_of_wordsClickable_8() { return &___wordsClickable_8; }
	inline void set_wordsClickable_8(bool value)
	{
		___wordsClickable_8 = value;
	}

	inline static int32_t get_offset_of_nextClicked_9() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___nextClicked_9)); }
	inline bool get_nextClicked_9() const { return ___nextClicked_9; }
	inline bool* get_address_of_nextClicked_9() { return &___nextClicked_9; }
	inline void set_nextClicked_9(bool value)
	{
		___nextClicked_9 = value;
	}

	inline static int32_t get_offset_of_callback_10() { return static_cast<int32_t>(offsetof(ShieldMenu_t3436809847, ___callback_10)); }
	inline ShieldCallback_t1004088674 * get_callback_10() const { return ___callback_10; }
	inline ShieldCallback_t1004088674 ** get_address_of_callback_10() { return &___callback_10; }
	inline void set_callback_10(ShieldCallback_t1004088674 * value)
	{
		___callback_10 = value;
		Il2CppCodeGenWriteBarrier((&___callback_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELDMENU_T3436809847_H
#ifndef SORTITNEGOTIATEDIALOG_T2102352598_H
#define SORTITNEGOTIATEDIALOG_T2102352598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortItNegotiateDialog
struct  SortItNegotiateDialog_t2102352598  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Image[] SortItNegotiateDialog::images
	ImageU5BU5D_t892035751* ___images_2;
	// System.String[] SortItNegotiateDialog::blincDescriptions
	StringU5BU5D_t2511808107* ___blincDescriptions_3;
	// System.String SortItNegotiateDialog::defaultDescription
	String_t* ___defaultDescription_4;
	// UnityEngine.UI.Text SortItNegotiateDialog::description
	Text_t1790657652 * ___description_5;

public:
	inline static int32_t get_offset_of_images_2() { return static_cast<int32_t>(offsetof(SortItNegotiateDialog_t2102352598, ___images_2)); }
	inline ImageU5BU5D_t892035751* get_images_2() const { return ___images_2; }
	inline ImageU5BU5D_t892035751** get_address_of_images_2() { return &___images_2; }
	inline void set_images_2(ImageU5BU5D_t892035751* value)
	{
		___images_2 = value;
		Il2CppCodeGenWriteBarrier((&___images_2), value);
	}

	inline static int32_t get_offset_of_blincDescriptions_3() { return static_cast<int32_t>(offsetof(SortItNegotiateDialog_t2102352598, ___blincDescriptions_3)); }
	inline StringU5BU5D_t2511808107* get_blincDescriptions_3() const { return ___blincDescriptions_3; }
	inline StringU5BU5D_t2511808107** get_address_of_blincDescriptions_3() { return &___blincDescriptions_3; }
	inline void set_blincDescriptions_3(StringU5BU5D_t2511808107* value)
	{
		___blincDescriptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___blincDescriptions_3), value);
	}

	inline static int32_t get_offset_of_defaultDescription_4() { return static_cast<int32_t>(offsetof(SortItNegotiateDialog_t2102352598, ___defaultDescription_4)); }
	inline String_t* get_defaultDescription_4() const { return ___defaultDescription_4; }
	inline String_t** get_address_of_defaultDescription_4() { return &___defaultDescription_4; }
	inline void set_defaultDescription_4(String_t* value)
	{
		___defaultDescription_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDescription_4), value);
	}

	inline static int32_t get_offset_of_description_5() { return static_cast<int32_t>(offsetof(SortItNegotiateDialog_t2102352598, ___description_5)); }
	inline Text_t1790657652 * get_description_5() const { return ___description_5; }
	inline Text_t1790657652 ** get_address_of_description_5() { return &___description_5; }
	inline void set_description_5(Text_t1790657652 * value)
	{
		___description_5 = value;
		Il2CppCodeGenWriteBarrier((&___description_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTITNEGOTIATEDIALOG_T2102352598_H
#ifndef STARTGAME_T3013009792_H
#define STARTGAME_T3013009792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartGame
struct  StartGame_t3013009792  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTGAME_T3013009792_H
#ifndef STATEMENTBOX_T3084352480_H
#define STATEMENTBOX_T3084352480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatementBox
struct  StatementBox_t3084352480  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Button StatementBox::previousButton
	Button_t1293135404 * ___previousButton_2;
	// UnityEngine.UI.Button StatementBox::nextButton
	Button_t1293135404 * ___nextButton_3;
	// UnityEngine.UI.Text StatementBox::characterName
	Text_t1790657652 * ___characterName_4;
	// UnityEngine.UI.Text StatementBox::dialogueText
	Text_t1790657652 * ___dialogueText_5;
	// System.Boolean StatementBox::previousClicked
	bool ___previousClicked_6;
	// System.Boolean StatementBox::nextClicked
	bool ___nextClicked_7;

public:
	inline static int32_t get_offset_of_previousButton_2() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___previousButton_2)); }
	inline Button_t1293135404 * get_previousButton_2() const { return ___previousButton_2; }
	inline Button_t1293135404 ** get_address_of_previousButton_2() { return &___previousButton_2; }
	inline void set_previousButton_2(Button_t1293135404 * value)
	{
		___previousButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___previousButton_2), value);
	}

	inline static int32_t get_offset_of_nextButton_3() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___nextButton_3)); }
	inline Button_t1293135404 * get_nextButton_3() const { return ___nextButton_3; }
	inline Button_t1293135404 ** get_address_of_nextButton_3() { return &___nextButton_3; }
	inline void set_nextButton_3(Button_t1293135404 * value)
	{
		___nextButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_3), value);
	}

	inline static int32_t get_offset_of_characterName_4() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___characterName_4)); }
	inline Text_t1790657652 * get_characterName_4() const { return ___characterName_4; }
	inline Text_t1790657652 ** get_address_of_characterName_4() { return &___characterName_4; }
	inline void set_characterName_4(Text_t1790657652 * value)
	{
		___characterName_4 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_4), value);
	}

	inline static int32_t get_offset_of_dialogueText_5() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___dialogueText_5)); }
	inline Text_t1790657652 * get_dialogueText_5() const { return ___dialogueText_5; }
	inline Text_t1790657652 ** get_address_of_dialogueText_5() { return &___dialogueText_5; }
	inline void set_dialogueText_5(Text_t1790657652 * value)
	{
		___dialogueText_5 = value;
		Il2CppCodeGenWriteBarrier((&___dialogueText_5), value);
	}

	inline static int32_t get_offset_of_previousClicked_6() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___previousClicked_6)); }
	inline bool get_previousClicked_6() const { return ___previousClicked_6; }
	inline bool* get_address_of_previousClicked_6() { return &___previousClicked_6; }
	inline void set_previousClicked_6(bool value)
	{
		___previousClicked_6 = value;
	}

	inline static int32_t get_offset_of_nextClicked_7() { return static_cast<int32_t>(offsetof(StatementBox_t3084352480, ___nextClicked_7)); }
	inline bool get_nextClicked_7() const { return ___nextClicked_7; }
	inline bool* get_address_of_nextClicked_7() { return &___nextClicked_7; }
	inline void set_nextClicked_7(bool value)
	{
		___nextClicked_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMENTBOX_T3084352480_H
#ifndef TAKEHOMEDIALOG_T743655907_H
#define TAKEHOMEDIALOG_T743655907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TakeHomeDialog
struct  TakeHomeDialog_t743655907  : public MonoBehaviour_t1618594486
{
public:
	// ButtonCallback[] TakeHomeDialog::takeHomeObjects
	ButtonCallbackU5BU5D_t1838213871* ___takeHomeObjects_2;
	// UnityEngine.UI.Button TakeHomeDialog::nextButton
	Button_t1293135404 * ___nextButton_3;
	// UnityEngine.UI.Text TakeHomeDialog::description
	Text_t1790657652 * ___description_4;
	// System.String[] TakeHomeDialog::descriptions
	StringU5BU5D_t2511808107* ___descriptions_5;
	// UnityEngine.GameObject TakeHomeDialog::selected
	GameObject_t2557347079 * ___selected_6;
	// System.Int32 TakeHomeDialog::selectedIndex
	int32_t ___selectedIndex_7;

public:
	inline static int32_t get_offset_of_takeHomeObjects_2() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___takeHomeObjects_2)); }
	inline ButtonCallbackU5BU5D_t1838213871* get_takeHomeObjects_2() const { return ___takeHomeObjects_2; }
	inline ButtonCallbackU5BU5D_t1838213871** get_address_of_takeHomeObjects_2() { return &___takeHomeObjects_2; }
	inline void set_takeHomeObjects_2(ButtonCallbackU5BU5D_t1838213871* value)
	{
		___takeHomeObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___takeHomeObjects_2), value);
	}

	inline static int32_t get_offset_of_nextButton_3() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___nextButton_3)); }
	inline Button_t1293135404 * get_nextButton_3() const { return ___nextButton_3; }
	inline Button_t1293135404 ** get_address_of_nextButton_3() { return &___nextButton_3; }
	inline void set_nextButton_3(Button_t1293135404 * value)
	{
		___nextButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_3), value);
	}

	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___description_4)); }
	inline Text_t1790657652 * get_description_4() const { return ___description_4; }
	inline Text_t1790657652 ** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(Text_t1790657652 * value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_descriptions_5() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___descriptions_5)); }
	inline StringU5BU5D_t2511808107* get_descriptions_5() const { return ___descriptions_5; }
	inline StringU5BU5D_t2511808107** get_address_of_descriptions_5() { return &___descriptions_5; }
	inline void set_descriptions_5(StringU5BU5D_t2511808107* value)
	{
		___descriptions_5 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_5), value);
	}

	inline static int32_t get_offset_of_selected_6() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___selected_6)); }
	inline GameObject_t2557347079 * get_selected_6() const { return ___selected_6; }
	inline GameObject_t2557347079 ** get_address_of_selected_6() { return &___selected_6; }
	inline void set_selected_6(GameObject_t2557347079 * value)
	{
		___selected_6 = value;
		Il2CppCodeGenWriteBarrier((&___selected_6), value);
	}

	inline static int32_t get_offset_of_selectedIndex_7() { return static_cast<int32_t>(offsetof(TakeHomeDialog_t743655907, ___selectedIndex_7)); }
	inline int32_t get_selectedIndex_7() const { return ___selectedIndex_7; }
	inline int32_t* get_address_of_selectedIndex_7() { return &___selectedIndex_7; }
	inline void set_selectedIndex_7(int32_t value)
	{
		___selectedIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAKEHOMEDIALOG_T743655907_H
#ifndef TEXTDISPLAYCANVAS_T2418733069_H
#define TEXTDISPLAYCANVAS_T2418733069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextDisplayCanvas
struct  TextDisplayCanvas_t2418733069  : public MonoBehaviour_t1618594486
{
public:
	// StatementBox TextDisplayCanvas::statementBox
	StatementBox_t3084352480 * ___statementBox_3;
	// StatementBox TextDisplayCanvas::largeStatementBox
	StatementBox_t3084352480 * ___largeStatementBox_4;
	// MultiChoiceBox TextDisplayCanvas::multiChoiceBox
	MultiChoiceBox_t1681099465 * ___multiChoiceBox_5;
	// TooltipBox TextDisplayCanvas::toolTipBox
	TooltipBox_t3451613534 * ___toolTipBox_6;
	// UnityEngine.UI.Text TextDisplayCanvas::title
	Text_t1790657652 * ___title_7;
	// System.Boolean TextDisplayCanvas::textShowingPrivate
	bool ___textShowingPrivate_8;
	// System.String TextDisplayCanvas::lastText
	String_t* ___lastText_9;
	// StatementBox TextDisplayCanvas::box
	StatementBox_t3084352480 * ___box_10;

public:
	inline static int32_t get_offset_of_statementBox_3() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___statementBox_3)); }
	inline StatementBox_t3084352480 * get_statementBox_3() const { return ___statementBox_3; }
	inline StatementBox_t3084352480 ** get_address_of_statementBox_3() { return &___statementBox_3; }
	inline void set_statementBox_3(StatementBox_t3084352480 * value)
	{
		___statementBox_3 = value;
		Il2CppCodeGenWriteBarrier((&___statementBox_3), value);
	}

	inline static int32_t get_offset_of_largeStatementBox_4() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___largeStatementBox_4)); }
	inline StatementBox_t3084352480 * get_largeStatementBox_4() const { return ___largeStatementBox_4; }
	inline StatementBox_t3084352480 ** get_address_of_largeStatementBox_4() { return &___largeStatementBox_4; }
	inline void set_largeStatementBox_4(StatementBox_t3084352480 * value)
	{
		___largeStatementBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___largeStatementBox_4), value);
	}

	inline static int32_t get_offset_of_multiChoiceBox_5() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___multiChoiceBox_5)); }
	inline MultiChoiceBox_t1681099465 * get_multiChoiceBox_5() const { return ___multiChoiceBox_5; }
	inline MultiChoiceBox_t1681099465 ** get_address_of_multiChoiceBox_5() { return &___multiChoiceBox_5; }
	inline void set_multiChoiceBox_5(MultiChoiceBox_t1681099465 * value)
	{
		___multiChoiceBox_5 = value;
		Il2CppCodeGenWriteBarrier((&___multiChoiceBox_5), value);
	}

	inline static int32_t get_offset_of_toolTipBox_6() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___toolTipBox_6)); }
	inline TooltipBox_t3451613534 * get_toolTipBox_6() const { return ___toolTipBox_6; }
	inline TooltipBox_t3451613534 ** get_address_of_toolTipBox_6() { return &___toolTipBox_6; }
	inline void set_toolTipBox_6(TooltipBox_t3451613534 * value)
	{
		___toolTipBox_6 = value;
		Il2CppCodeGenWriteBarrier((&___toolTipBox_6), value);
	}

	inline static int32_t get_offset_of_title_7() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___title_7)); }
	inline Text_t1790657652 * get_title_7() const { return ___title_7; }
	inline Text_t1790657652 ** get_address_of_title_7() { return &___title_7; }
	inline void set_title_7(Text_t1790657652 * value)
	{
		___title_7 = value;
		Il2CppCodeGenWriteBarrier((&___title_7), value);
	}

	inline static int32_t get_offset_of_textShowingPrivate_8() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___textShowingPrivate_8)); }
	inline bool get_textShowingPrivate_8() const { return ___textShowingPrivate_8; }
	inline bool* get_address_of_textShowingPrivate_8() { return &___textShowingPrivate_8; }
	inline void set_textShowingPrivate_8(bool value)
	{
		___textShowingPrivate_8 = value;
	}

	inline static int32_t get_offset_of_lastText_9() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___lastText_9)); }
	inline String_t* get_lastText_9() const { return ___lastText_9; }
	inline String_t** get_address_of_lastText_9() { return &___lastText_9; }
	inline void set_lastText_9(String_t* value)
	{
		___lastText_9 = value;
		Il2CppCodeGenWriteBarrier((&___lastText_9), value);
	}

	inline static int32_t get_offset_of_box_10() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069, ___box_10)); }
	inline StatementBox_t3084352480 * get_box_10() const { return ___box_10; }
	inline StatementBox_t3084352480 ** get_address_of_box_10() { return &___box_10; }
	inline void set_box_10(StatementBox_t3084352480 * value)
	{
		___box_10 = value;
		Il2CppCodeGenWriteBarrier((&___box_10), value);
	}
};

struct TextDisplayCanvas_t2418733069_StaticFields
{
public:
	// TextDisplayCanvas TextDisplayCanvas::instance
	TextDisplayCanvas_t2418733069 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(TextDisplayCanvas_t2418733069_StaticFields, ___instance_2)); }
	inline TextDisplayCanvas_t2418733069 * get_instance_2() const { return ___instance_2; }
	inline TextDisplayCanvas_t2418733069 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TextDisplayCanvas_t2418733069 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTDISPLAYCANVAS_T2418733069_H
#ifndef THREEWORDGAME_T2710613907_H
#define THREEWORDGAME_T2710613907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThreeWordGame
struct  ThreeWordGame_t2710613907  : public MonoBehaviour_t1618594486
{
public:
	// Reel[] ThreeWordGame::reels
	ReelU5BU5D_t2983038275* ___reels_2;
	// System.Single ThreeWordGame::solveLag
	float ___solveLag_3;
	// System.Single ThreeWordGame::solveCounter
	float ___solveCounter_4;
	// System.Boolean ThreeWordGame::animationPlaying
	bool ___animationPlaying_5;
	// System.Boolean ThreeWordGame::animationDone
	bool ___animationDone_6;

public:
	inline static int32_t get_offset_of_reels_2() { return static_cast<int32_t>(offsetof(ThreeWordGame_t2710613907, ___reels_2)); }
	inline ReelU5BU5D_t2983038275* get_reels_2() const { return ___reels_2; }
	inline ReelU5BU5D_t2983038275** get_address_of_reels_2() { return &___reels_2; }
	inline void set_reels_2(ReelU5BU5D_t2983038275* value)
	{
		___reels_2 = value;
		Il2CppCodeGenWriteBarrier((&___reels_2), value);
	}

	inline static int32_t get_offset_of_solveLag_3() { return static_cast<int32_t>(offsetof(ThreeWordGame_t2710613907, ___solveLag_3)); }
	inline float get_solveLag_3() const { return ___solveLag_3; }
	inline float* get_address_of_solveLag_3() { return &___solveLag_3; }
	inline void set_solveLag_3(float value)
	{
		___solveLag_3 = value;
	}

	inline static int32_t get_offset_of_solveCounter_4() { return static_cast<int32_t>(offsetof(ThreeWordGame_t2710613907, ___solveCounter_4)); }
	inline float get_solveCounter_4() const { return ___solveCounter_4; }
	inline float* get_address_of_solveCounter_4() { return &___solveCounter_4; }
	inline void set_solveCounter_4(float value)
	{
		___solveCounter_4 = value;
	}

	inline static int32_t get_offset_of_animationPlaying_5() { return static_cast<int32_t>(offsetof(ThreeWordGame_t2710613907, ___animationPlaying_5)); }
	inline bool get_animationPlaying_5() const { return ___animationPlaying_5; }
	inline bool* get_address_of_animationPlaying_5() { return &___animationPlaying_5; }
	inline void set_animationPlaying_5(bool value)
	{
		___animationPlaying_5 = value;
	}

	inline static int32_t get_offset_of_animationDone_6() { return static_cast<int32_t>(offsetof(ThreeWordGame_t2710613907, ___animationDone_6)); }
	inline bool get_animationDone_6() const { return ___animationDone_6; }
	inline bool* get_address_of_animationDone_6() { return &___animationDone_6; }
	inline void set_animationDone_6(bool value)
	{
		___animationDone_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEWORDGAME_T2710613907_H
#ifndef TRIGGEROUTLINE_T1691305668_H
#define TRIGGEROUTLINE_T1691305668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriggerOutline
struct  TriggerOutline_t1691305668  : public MonoBehaviour_t1618594486
{
public:
	// OutlineObject TriggerOutline::outline
	OutlineObject_t941793957 * ___outline_2;

public:
	inline static int32_t get_offset_of_outline_2() { return static_cast<int32_t>(offsetof(TriggerOutline_t1691305668, ___outline_2)); }
	inline OutlineObject_t941793957 * get_outline_2() const { return ___outline_2; }
	inline OutlineObject_t941793957 ** get_address_of_outline_2() { return &___outline_2; }
	inline void set_outline_2(OutlineObject_t941793957 * value)
	{
		___outline_2 = value;
		Il2CppCodeGenWriteBarrier((&___outline_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROUTLINE_T1691305668_H
#ifndef ICONLOCATION_T4110305816_H
#define ICONLOCATION_T4110305816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iconLocation
struct  iconLocation_t4110305816  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform iconLocation::CameraTransform
	Transform_t362059596 * ___CameraTransform_2;

public:
	inline static int32_t get_offset_of_CameraTransform_2() { return static_cast<int32_t>(offsetof(iconLocation_t4110305816, ___CameraTransform_2)); }
	inline Transform_t362059596 * get_CameraTransform_2() const { return ___CameraTransform_2; }
	inline Transform_t362059596 ** get_address_of_CameraTransform_2() { return &___CameraTransform_2; }
	inline void set_CameraTransform_2(Transform_t362059596 * value)
	{
		___CameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONLOCATION_T4110305816_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (iconLocation_t4110305816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[1] = 
{
	iconLocation_t4110305816::get_offset_of_CameraTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (levelSaveOnLoad_t2042969378), -1, sizeof(levelSaveOnLoad_t2042969378_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3101[6] = 
{
	levelSaveOnLoad_t2042969378::get_offset_of_instance_2(),
	levelSaveOnLoad_t2042969378::get_offset_of_currentSavePoint_3(),
	levelSaveOnLoad_t2042969378::get_offset_of_levelComplete_4(),
	levelSaveOnLoad_t2042969378::get_offset_of_feedbackData_5(),
	levelSaveOnLoad_t2042969378_StaticFields::get_offset_of_saveTriggerToggle_6(),
	levelSaveOnLoad_t2042969378::get_offset_of_failedFileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (levelSaveTrigger_t575094362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[2] = 
{
	levelSaveTrigger_t575094362::get_offset_of_instance_2(),
	levelSaveTrigger_t575094362::get_offset_of_levelMidSavePoint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (loadDictionaryVars_t2169303635), -1, sizeof(loadDictionaryVars_t2169303635_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3103[1] = 
{
	loadDictionaryVars_t2169303635_StaticFields::get_offset_of_tempPosition_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (OfflineFileUploader_t262227362), -1, sizeof(OfflineFileUploader_t262227362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3104[5] = 
{
	OfflineFileUploader_t262227362_StaticFields::get_offset_of_theInstance_0(),
	OfflineFileUploader_t262227362::get_offset_of_currentLineNum_1(),
	OfflineFileUploader_t262227362::get_offset_of_allLines_2(),
	OfflineFileUploader_t262227362::get_offset_of_fileName_3(),
	OfflineFileUploader_t262227362::get_offset_of_connection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (U3CsubConnectU3Ec__Iterator0_t36647400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[6] = 
{
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_sJsonString_0(),
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_U3CformU3E__0_1(),
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_U3CwwwU3E__0_2(),
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_U24current_3(),
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_U24disposing_4(),
	U3CsubConnectU3Ec__Iterator0_t36647400::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (savingIcon_t1459161706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[1] = 
{
	savingIcon_t1459161706::get_offset_of_saveShieldIcon_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (SceneController_t816226515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (ScreenLogger_t129510507), -1, sizeof(ScreenLogger_t129510507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3108[6] = 
{
	ScreenLogger_t129510507_StaticFields::get_offset_of_instance_2(),
	ScreenLogger_t129510507_StaticFields::get_offset_of_maxStrings_3(),
	ScreenLogger_t129510507_StaticFields::get_offset_of_showStrings_4(),
	ScreenLogger_t129510507::get_offset_of_optionalSkin_5(),
	ScreenLogger_t129510507::get_offset_of_screenStrings_6(),
	ScreenLogger_t129510507::get_offset_of_singleFrameStrings_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (JSONValueType_t2936784338)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3109[7] = 
{
	JSONValueType_t2936784338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (JSONValue_t3864304964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[7] = 
{
	JSONValue_t3864304964::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	JSONValue_t3864304964::get_offset_of_U3CStrU3Ek__BackingField_1(),
	JSONValue_t3864304964::get_offset_of_U3CNumberU3Ek__BackingField_2(),
	JSONValue_t3864304964::get_offset_of_U3CObjU3Ek__BackingField_3(),
	JSONValue_t3864304964::get_offset_of_U3CArrayU3Ek__BackingField_4(),
	JSONValue_t3864304964::get_offset_of_U3CBooleanU3Ek__BackingField_5(),
	JSONValue_t3864304964::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (JSONArray_t377107346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[1] = 
{
	JSONArray_t377107346::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (JSONObject_t2742677291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[1] = 
{
	JSONObject_t2742677291::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (JSONParsingState_t3264473632)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3113[13] = 
{
	JSONParsingState_t3264473632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (TokenHolderLegacy_t1380075429), -1, sizeof(TokenHolderLegacy_t1380075429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3114[4] = 
{
	TokenHolderLegacy_t1380075429_StaticFields::get_offset_of_instanceRef_2(),
	TokenHolderLegacy_t1380075429::get_offset_of_Global_sGameConfigServerURL_3(),
	TokenHolderLegacy_t1380075429::get_offset_of_username_4(),
	TokenHolderLegacy_t1380075429::get_offset_of_password_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (GuideEndTransition_t2209539835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[5] = 
{
	GuideEndTransition_t2209539835::get_offset_of_m_skin_2(),
	GuideEndTransition_t2209539835::get_offset_of_yPosition_3(),
	GuideEndTransition_t2209539835::get_offset_of_fTime_4(),
	GuideEndTransition_t2209539835::get_offset_of_m_PlayerInfoLevelLoad_5(),
	GuideEndTransition_t2209539835::get_offset_of_whiteout_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (U3CLoadLevelSU3Ec__Iterator0_t1826114136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[6] = 
{
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U3CfTimeUntilLoadU3E__0_0(),
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U3CopU3E__0_1(),
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U24this_2(),
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U24current_3(),
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U24disposing_4(),
	U3CLoadLevelSU3Ec__Iterator0_t1826114136::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (IceCaveSpashScreen_t2468610820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[6] = 
{
	IceCaveSpashScreen_t2468610820::get_offset_of_m_bIsFadeIn_2(),
	IceCaveSpashScreen_t2468610820::get_offset_of_m_skin_3(),
	IceCaveSpashScreen_t2468610820::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	IceCaveSpashScreen_t2468610820::get_offset_of_m_fTime_5(),
	IceCaveSpashScreen_t2468610820::get_offset_of_m_PlayerInfoLevelLoad_6(),
	IceCaveSpashScreen_t2468610820::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (IslandSplashScreen_t1175684869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[7] = 
{
	IslandSplashScreen_t1175684869::get_offset_of_m_skin_2(),
	IslandSplashScreen_t1175684869::get_offset_of_m_fFadeInTheGuideAlpha_3(),
	IslandSplashScreen_t1175684869::get_offset_of_m_fTime_4(),
	IslandSplashScreen_t1175684869::get_offset_of_m_fMoveTime_5(),
	IslandSplashScreen_t1175684869::get_offset_of_m_loadSceneData_6(),
	IslandSplashScreen_t1175684869::get_offset_of_instance_7(),
	IslandSplashScreen_t1175684869::get_offset_of_m_bFadeIn_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (MoutainCaveSplashScreen_t3103079625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[6] = 
{
	MoutainCaveSplashScreen_t3103079625::get_offset_of_m_bIsFadeIn_2(),
	MoutainCaveSplashScreen_t3103079625::get_offset_of_m_skin_3(),
	MoutainCaveSplashScreen_t3103079625::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	MoutainCaveSplashScreen_t3103079625::get_offset_of_m_fTime_5(),
	MoutainCaveSplashScreen_t3103079625::get_offset_of_m_PlayerInfoLevelLoad_6(),
	MoutainCaveSplashScreen_t3103079625::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (TheBridgeSplashScreen_t1192841672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[6] = 
{
	TheBridgeSplashScreen_t1192841672::get_offset_of_m_bIsFadeIn_2(),
	TheBridgeSplashScreen_t1192841672::get_offset_of_m_skin_3(),
	TheBridgeSplashScreen_t1192841672::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	TheBridgeSplashScreen_t1192841672::get_offset_of_m_fTime_5(),
	TheBridgeSplashScreen_t1192841672::get_offset_of_m_PlayerInfoLevelLoad_6(),
	TheBridgeSplashScreen_t1192841672::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (TheCanyonSplashScreen_t4023409967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[6] = 
{
	TheCanyonSplashScreen_t4023409967::get_offset_of_m_bIsFadeIn_2(),
	TheCanyonSplashScreen_t4023409967::get_offset_of_m_skin_3(),
	TheCanyonSplashScreen_t4023409967::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	TheCanyonSplashScreen_t4023409967::get_offset_of_m_fTime_5(),
	TheCanyonSplashScreen_t4023409967::get_offset_of_m_PlayerInfoLevelLoad_6(),
	TheCanyonSplashScreen_t4023409967::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (TheCaveSplashScreen_t265279536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[6] = 
{
	TheCaveSplashScreen_t265279536::get_offset_of_m_bIsFadeIn_2(),
	TheCaveSplashScreen_t265279536::get_offset_of_m_skin_3(),
	TheCaveSplashScreen_t265279536::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	TheCaveSplashScreen_t265279536::get_offset_of_m_fTime_5(),
	TheCaveSplashScreen_t265279536::get_offset_of_m_PlayerInfoLevelLoad_6(),
	TheCaveSplashScreen_t265279536::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (TheGuideSplashScreen_t2843531037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[4] = 
{
	TheGuideSplashScreen_t2843531037::get_offset_of_m_skin_2(),
	TheGuideSplashScreen_t2843531037::get_offset_of_m_fFadeInStartTime_3(),
	TheGuideSplashScreen_t2843531037::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	TheGuideSplashScreen_t2843531037::get_offset_of_m_loadSceneData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (TheSwampSplashScreen_t3703549283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[6] = 
{
	TheSwampSplashScreen_t3703549283::get_offset_of_m_bIsFadeIn_2(),
	TheSwampSplashScreen_t3703549283::get_offset_of_m_skin_3(),
	TheSwampSplashScreen_t3703549283::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	TheSwampSplashScreen_t3703549283::get_offset_of_m_fTime_5(),
	TheSwampSplashScreen_t3703549283::get_offset_of_m_PlayerInfoLevelLoad_6(),
	TheSwampSplashScreen_t3703549283::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (VencanoCaveSplashScreen_t159642342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[6] = 
{
	VencanoCaveSplashScreen_t159642342::get_offset_of_m_bIsFadeIn_2(),
	VencanoCaveSplashScreen_t159642342::get_offset_of_m_skin_3(),
	VencanoCaveSplashScreen_t159642342::get_offset_of_m_fFadeInTheGuideAlpha_4(),
	VencanoCaveSplashScreen_t159642342::get_offset_of_m_fTime_5(),
	VencanoCaveSplashScreen_t159642342::get_offset_of_m_PlayerInfoLevelLoad_6(),
	VencanoCaveSplashScreen_t159642342::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (GuideSceneTransition_t3184135131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (Utils_t2448594833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (ShieldMenu_t3436809847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[9] = 
{
	ShieldMenu_t3436809847::get_offset_of_shieldText_2(),
	ShieldMenu_t3436809847::get_offset_of_words_3(),
	ShieldMenu_t3436809847::get_offset_of_colouredWords_4(),
	ShieldMenu_t3436809847::get_offset_of_descriptions_5(),
	ShieldMenu_t3436809847::get_offset_of_descriptionBox_6(),
	ShieldMenu_t3436809847::get_offset_of_nextButton_7(),
	ShieldMenu_t3436809847::get_offset_of_wordsClickable_8(),
	ShieldMenu_t3436809847::get_offset_of_nextClicked_9(),
	ShieldMenu_t3436809847::get_offset_of_callback_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (ShieldCallback_t1004088674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (SortItNegotiateDialog_t2102352598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[4] = 
{
	SortItNegotiateDialog_t2102352598::get_offset_of_images_2(),
	SortItNegotiateDialog_t2102352598::get_offset_of_blincDescriptions_3(),
	SortItNegotiateDialog_t2102352598::get_offset_of_defaultDescription_4(),
	SortItNegotiateDialog_t2102352598::get_offset_of_description_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (StartGame_t3013009792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (StatementBox_t3084352480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[6] = 
{
	StatementBox_t3084352480::get_offset_of_previousButton_2(),
	StatementBox_t3084352480::get_offset_of_nextButton_3(),
	StatementBox_t3084352480::get_offset_of_characterName_4(),
	StatementBox_t3084352480::get_offset_of_dialogueText_5(),
	StatementBox_t3084352480::get_offset_of_previousClicked_6(),
	StatementBox_t3084352480::get_offset_of_nextClicked_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (TakeHomeDialog_t743655907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[6] = 
{
	TakeHomeDialog_t743655907::get_offset_of_takeHomeObjects_2(),
	TakeHomeDialog_t743655907::get_offset_of_nextButton_3(),
	TakeHomeDialog_t743655907::get_offset_of_description_4(),
	TakeHomeDialog_t743655907::get_offset_of_descriptions_5(),
	TakeHomeDialog_t743655907::get_offset_of_selected_6(),
	TakeHomeDialog_t743655907::get_offset_of_selectedIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (TESTENABLE_t1673577313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (TextDisplayCanvas_t2418733069), -1, sizeof(TextDisplayCanvas_t2418733069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3135[9] = 
{
	TextDisplayCanvas_t2418733069_StaticFields::get_offset_of_instance_2(),
	TextDisplayCanvas_t2418733069::get_offset_of_statementBox_3(),
	TextDisplayCanvas_t2418733069::get_offset_of_largeStatementBox_4(),
	TextDisplayCanvas_t2418733069::get_offset_of_multiChoiceBox_5(),
	TextDisplayCanvas_t2418733069::get_offset_of_toolTipBox_6(),
	TextDisplayCanvas_t2418733069::get_offset_of_title_7(),
	TextDisplayCanvas_t2418733069::get_offset_of_textShowingPrivate_8(),
	TextDisplayCanvas_t2418733069::get_offset_of_lastText_9(),
	TextDisplayCanvas_t2418733069::get_offset_of_box_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (Reel_t2885754470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[5] = 
{
	Reel_t2885754470::get_offset_of_words_0(),
	Reel_t2885754470::get_offset_of_rightWord_1(),
	Reel_t2885754470::get_offset_of_startWord_2(),
	Reel_t2885754470::get_offset_of_currentWord_3(),
	Reel_t2885754470::get_offset_of_wordText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (ThreeWordGame_t2710613907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[5] = 
{
	ThreeWordGame_t2710613907::get_offset_of_reels_2(),
	ThreeWordGame_t2710613907::get_offset_of_solveLag_3(),
	ThreeWordGame_t2710613907::get_offset_of_solveCounter_4(),
	ThreeWordGame_t2710613907::get_offset_of_animationPlaying_5(),
	ThreeWordGame_t2710613907::get_offset_of_animationDone_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (TooltipBox_t3451613534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[2] = 
{
	TooltipBox_t3451613534::get_offset_of_toolTipText_2(),
	TooltipBox_t3451613534::get_offset_of_bTextSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (TriggerOutline_t1691305668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[1] = 
{
	TriggerOutline_t1691305668::get_offset_of_outline_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (WhatIDidHowIFelt_t602585838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[1] = 
{
	WhatIDidHowIFelt_t602585838::get_offset_of_faces_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (Whiteout_t3878335910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[5] = 
{
	Whiteout_t3878335910::get_offset_of_fadeSpeed_2(),
	Whiteout_t3878335910::get_offset_of_del_3(),
	Whiteout_t3878335910::get_offset_of_fadeState_4(),
	Whiteout_t3878335910::get_offset_of_myImage_5(),
	Whiteout_t3878335910::get_offset_of_alpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (FadeFinishDelegate_t1266332645), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (FadeState_t852633535)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3143[4] = 
{
	FadeState_t852633535::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (U3CPrivateImplementationDetailsU3E_t2655089822), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3144[10] = 
{
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DFA2354A15E68A179407C19817026877B0ECB0254_0(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2D41F65FD5B62627EE31EFA9D3F5DA66347E0F7478_1(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_2(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DDB1AF13F41A110B000ECFFF980C9691DEE5900BD_3(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_4(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_5(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DFD7391FFB25CC39FA59979E73C93F6EFE1B302BF_6(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DC480030255DD5E766C1E24628C4C4078F6C34940_7(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2DCBCAE4AC65AC0FDC5AF0D801D8A9B9654569906D_8(),
	U3CPrivateImplementationDetailsU3E_t2655089822_StaticFields::get_offset_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (U24ArrayTypeU3D12_t4075530830)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t4075530830 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (U24ArrayTypeU3D64_t1238761204)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t1238761204 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (U24ArrayTypeU3D24_t768527802)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t768527802 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (U24ArrayTypeU3D8_t2622132287)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D8_t2622132287 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (U24ArrayTypeU3D16_t684215895)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t684215895 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (U3CModuleU3E_t1429447288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (MagicLightFlicker_t4078483488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[6] = 
{
	MagicLightFlicker_t4078483488::get_offset_of_waveFunction_2(),
	MagicLightFlicker_t4078483488::get_offset_of_base_3(),
	MagicLightFlicker_t4078483488::get_offset_of_amplitude_4(),
	MagicLightFlicker_t4078483488::get_offset_of_phase_5(),
	MagicLightFlicker_t4078483488::get_offset_of_frequency_6(),
	MagicLightFlicker_t4078483488::get_offset_of_originalColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (MovingU20Texture_t723204342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[2] = 
{
	MovingU20Texture_t723204342::get_offset_of_m_fScrollSpeed1_2(),
	MovingU20Texture_t723204342::get_offset_of_m_fScrollSpeed2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (U3CModuleU3E_t1429447289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (TimedObjectDestructor_t3765746229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[2] = 
{
	TimedObjectDestructor_t3765746229::get_offset_of_timeOut_2(),
	TimedObjectDestructor_t3765746229::get_offset_of_detachChildren_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
