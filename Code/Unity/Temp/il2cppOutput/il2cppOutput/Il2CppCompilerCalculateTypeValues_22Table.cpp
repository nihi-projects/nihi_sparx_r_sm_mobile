﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t3874041483;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2016646579;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1618594486;
// System.String
struct String_t;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2652211791;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t556173537;
// UnityEngine.Timeline.ITimeControl
struct ITimeControl_t2452620802;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2248283753;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t2171935445;
// System.Void
struct Void_t653366341;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3772266050;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t1595482203;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t4186695123;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct List_1_t3281727266;
// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t2489983630;
// UnityEngine.Timeline.AudioTrack
struct AudioTrack_t2200009395;
// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_t200707209;
// UnityEngine.AnimationClip
struct AnimationClip_t1145879031;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t1461289625;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_t1943312966;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t1733651323;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t2389961773;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t1609610637;
// UnityEngine.AudioClip
struct AudioClip_t1419814565;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.Material
struct Material_t2815264910;
// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t4235477116;
// UnityEngine.AvatarMask
struct AvatarMask_t2076757458;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1934891917;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t3366748972;
// UnityEngine.RectTransform
struct RectTransform_t859616204;
// UnityEngine.UI.Image
struct Image_t2816987602;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t2276377189;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t1249652403;
// UnityEngine.Texture3D
struct Texture3D_t2628067514;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// UnityEngine.Camera
struct Camera_t2839736942;
// UnityEngine.RenderTexture
struct RenderTexture_t971269558;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1457428168;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t2358285523;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;

struct Object_t692178351_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T1429447286_H
#define U3CMODULEU3E_T1429447286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447286 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447286_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef PLAYABLEBEHAVIOUR_T3096602576_H
#define PLAYABLEBEHAVIOUR_T3096602576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t3096602576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T3096602576_H
#ifndef TRACKASSETEXTENSIONS_T2124117065_H
#define TRACKASSETEXTENSIONS_T2124117065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAssetExtensions
struct  TrackAssetExtensions_t2124117065  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSETEXTENSIONS_T2124117065_H
#ifndef RUNTIMEELEMENT_T869620576_H
#define RUNTIMEELEMENT_T869620576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeElement
struct  RuntimeElement_t869620576  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Timeline.RuntimeElement::<intervalBit>k__BackingField
	int32_t ___U3CintervalBitU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CintervalBitU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RuntimeElement_t869620576, ___U3CintervalBitU3Ek__BackingField_0)); }
	inline int32_t get_U3CintervalBitU3Ek__BackingField_0() const { return ___U3CintervalBitU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CintervalBitU3Ek__BackingField_0() { return &___U3CintervalBitU3Ek__BackingField_0; }
	inline void set_U3CintervalBitU3Ek__BackingField_0(int32_t value)
	{
		___U3CintervalBitU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEELEMENT_T869620576_H
#ifndef EXTRAPOLATION_T2678001198_H
#define EXTRAPOLATION_T2678001198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.Extrapolation
struct  Extrapolation_t2678001198  : public RuntimeObject
{
public:

public:
};

struct Extrapolation_t2678001198_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.Extrapolation::kMinExtrapolationTime
	double ___kMinExtrapolationTime_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.Extrapolation::<>f__am$cache0
	Comparison_1_t3874041483 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_kMinExtrapolationTime_0() { return static_cast<int32_t>(offsetof(Extrapolation_t2678001198_StaticFields, ___kMinExtrapolationTime_0)); }
	inline double get_kMinExtrapolationTime_0() const { return ___kMinExtrapolationTime_0; }
	inline double* get_address_of_kMinExtrapolationTime_0() { return &___kMinExtrapolationTime_0; }
	inline void set_kMinExtrapolationTime_0(double value)
	{
		___kMinExtrapolationTime_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Extrapolation_t2678001198_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Comparison_1_t3874041483 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Comparison_1_t3874041483 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Comparison_1_t3874041483 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRAPOLATION_T2678001198_H
#ifndef HASHUTILITY_T3989591031_H
#define HASHUTILITY_T3989591031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.HashUtility
struct  HashUtility_t3989591031  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHUTILITY_T3989591031_H
#ifndef U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T272318736_H
#define U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T272318736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0
struct  U3CGetControlableScriptsU3Ec__Iterator0_t272318736  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::root
	GameObject_t2557347079 * ___root_0;
	// UnityEngine.MonoBehaviour[] UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar0
	MonoBehaviourU5BU5D_t2016646579* ___U24locvar0_1;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::<script>__1
	MonoBehaviour_t1618594486 * ___U3CscriptU3E__1_3;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$current
	MonoBehaviour_t1618594486 * ___U24current_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___root_0)); }
	inline GameObject_t2557347079 * get_root_0() const { return ___root_0; }
	inline GameObject_t2557347079 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_t2557347079 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U24locvar0_1)); }
	inline MonoBehaviourU5BU5D_t2016646579* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline MonoBehaviourU5BU5D_t2016646579** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(MonoBehaviourU5BU5D_t2016646579* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CscriptU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U3CscriptU3E__1_3)); }
	inline MonoBehaviour_t1618594486 * get_U3CscriptU3E__1_3() const { return ___U3CscriptU3E__1_3; }
	inline MonoBehaviour_t1618594486 ** get_address_of_U3CscriptU3E__1_3() { return &___U3CscriptU3E__1_3; }
	inline void set_U3CscriptU3E__1_3(MonoBehaviour_t1618594486 * value)
	{
		___U3CscriptU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U24current_4)); }
	inline MonoBehaviour_t1618594486 * get_U24current_4() const { return ___U24current_4; }
	inline MonoBehaviour_t1618594486 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MonoBehaviour_t1618594486 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t272318736, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T272318736_H
#ifndef TIMELINECREATEUTILITIES_T707790311_H
#define TIMELINECREATEUTILITIES_T707790311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities
struct  TimelineCreateUtilities_t707790311  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECREATEUTILITIES_T707790311_H
#ifndef TIMELINEUNDO_T4178130704_H
#define TIMELINEUNDO_T4178130704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineUndo
struct  TimelineUndo_t4178130704  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEUNDO_T4178130704_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T3053161571_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T3053161571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::prefix
	String_t* ___prefix_0;
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::newName
	String_t* ___newName_1;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_newName_1() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571, ___newName_1)); }
	inline String_t* get_newName_1() const { return ___newName_1; }
	inline String_t** get_address_of_newName_1() { return &___newName_1; }
	inline void set_newName_1(String_t* value)
	{
		___newName_1 = value;
		Il2CppCodeGenWriteBarrier((&___newName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T3053161571_H
#ifndef WEIGHTUTILITY_T2995380413_H
#define WEIGHTUTILITY_T2995380413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.WeightUtility
struct  WeightUtility_t2995380413  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTUTILITY_T2995380413_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef CROSSPLATFORMINPUTMANAGER_T490741389_H
#define CROSSPLATFORMINPUTMANAGER_T490741389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t490741389  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t490741389_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2652211791 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2652211791 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2652211791 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t490741389_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2652211791 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2652211791 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2652211791 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t490741389_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2652211791 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2652211791 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2652211791 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t490741389_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2652211791 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2652211791 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2652211791 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T490741389_H
#ifndef VIRTUALAXIS_T3366748972_H
#define VIRTUALAXIS_T3366748972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t3366748972  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t3366748972, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t3366748972, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t3366748972, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T3366748972_H
#ifndef VIRTUALBUTTON_T1189965125_H
#define VIRTUALBUTTON_T1189965125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t1189965125  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t1189965125, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t1189965125, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t1189965125, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t1189965125, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t1189965125, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T1189965125_H
#ifndef TIMEUTILITY_T2058592793_H
#define TIMEUTILITY_T2058592793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeUtility
struct  TimeUtility_t2058592793  : public RuntimeObject
{
public:

public:
};

struct TimeUtility_t2058592793_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.TimeUtility::kTimeEpsilon
	double ___kTimeEpsilon_0;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateEpsilon
	double ___kFrameRateEpsilon_1;
	// System.Func`2<System.Char,System.Boolean> UnityEngine.Timeline.TimeUtility::<>f__am$cache0
	Func_2_t556173537 * ___U3CU3Ef__amU24cache0_2;
	// System.Func`2<System.Char,System.Boolean> UnityEngine.Timeline.TimeUtility::<>f__am$cache1
	Func_2_t556173537 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_kTimeEpsilon_0() { return static_cast<int32_t>(offsetof(TimeUtility_t2058592793_StaticFields, ___kTimeEpsilon_0)); }
	inline double get_kTimeEpsilon_0() const { return ___kTimeEpsilon_0; }
	inline double* get_address_of_kTimeEpsilon_0() { return &___kTimeEpsilon_0; }
	inline void set_kTimeEpsilon_0(double value)
	{
		___kTimeEpsilon_0 = value;
	}

	inline static int32_t get_offset_of_kFrameRateEpsilon_1() { return static_cast<int32_t>(offsetof(TimeUtility_t2058592793_StaticFields, ___kFrameRateEpsilon_1)); }
	inline double get_kFrameRateEpsilon_1() const { return ___kFrameRateEpsilon_1; }
	inline double* get_address_of_kFrameRateEpsilon_1() { return &___kFrameRateEpsilon_1; }
	inline void set_kFrameRateEpsilon_1(double value)
	{
		___kFrameRateEpsilon_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(TimeUtility_t2058592793_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t556173537 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t556173537 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t556173537 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(TimeUtility_t2058592793_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t556173537 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t556173537 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t556173537 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILITY_T2058592793_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef TIMECONTROLPLAYABLE_T2938852893_H
#define TIMECONTROLPLAYABLE_T2938852893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeControlPlayable
struct  TimeControlPlayable_t2938852893  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.Timeline.ITimeControl UnityEngine.Timeline.TimeControlPlayable::m_timeControl
	RuntimeObject* ___m_timeControl_0;
	// System.Boolean UnityEngine.Timeline.TimeControlPlayable::m_started
	bool ___m_started_1;

public:
	inline static int32_t get_offset_of_m_timeControl_0() { return static_cast<int32_t>(offsetof(TimeControlPlayable_t2938852893, ___m_timeControl_0)); }
	inline RuntimeObject* get_m_timeControl_0() const { return ___m_timeControl_0; }
	inline RuntimeObject** get_address_of_m_timeControl_0() { return &___m_timeControl_0; }
	inline void set_m_timeControl_0(RuntimeObject* value)
	{
		___m_timeControl_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_timeControl_0), value);
	}

	inline static int32_t get_offset_of_m_started_1() { return static_cast<int32_t>(offsetof(TimeControlPlayable_t2938852893, ___m_started_1)); }
	inline bool get_m_started_1() const { return ___m_started_1; }
	inline bool* get_address_of_m_started_1() { return &___m_started_1; }
	inline void set_m_started_1(bool value)
	{
		___m_started_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECONTROLPLAYABLE_T2938852893_H
#ifndef PREFABCONTROLPLAYABLE_T470232467_H
#define PREFABCONTROLPLAYABLE_T470232467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PrefabControlPlayable
struct  PrefabControlPlayable_t470232467  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.PrefabControlPlayable::m_Instance
	GameObject_t2557347079 * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(PrefabControlPlayable_t470232467, ___m_Instance_0)); }
	inline GameObject_t2557347079 * get_m_Instance_0() const { return ___m_Instance_0; }
	inline GameObject_t2557347079 ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(GameObject_t2557347079 * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABCONTROLPLAYABLE_T470232467_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef PARTICLECONTROLPLAYABLE_T1573458720_H
#define PARTICLECONTROLPLAYABLE_T1573458720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ParticleControlPlayable
struct  ParticleControlPlayable_t1573458720  : public PlayableBehaviour_t3096602576
{
public:
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_LastTime
	float ___m_LastTime_1;
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_LastPsTime
	float ___m_LastPsTime_2;
	// System.UInt32 UnityEngine.Timeline.ParticleControlPlayable::m_RandomSeed
	uint32_t ___m_RandomSeed_3;
	// UnityEngine.ParticleSystem UnityEngine.Timeline.ParticleControlPlayable::m_ParticleSystem
	ParticleSystem_t2248283753 * ___m_ParticleSystem_4;

public:
	inline static int32_t get_offset_of_m_LastTime_1() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t1573458720, ___m_LastTime_1)); }
	inline float get_m_LastTime_1() const { return ___m_LastTime_1; }
	inline float* get_address_of_m_LastTime_1() { return &___m_LastTime_1; }
	inline void set_m_LastTime_1(float value)
	{
		___m_LastTime_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPsTime_2() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t1573458720, ___m_LastPsTime_2)); }
	inline float get_m_LastPsTime_2() const { return ___m_LastPsTime_2; }
	inline float* get_address_of_m_LastPsTime_2() { return &___m_LastPsTime_2; }
	inline void set_m_LastPsTime_2(float value)
	{
		___m_LastPsTime_2 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_3() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t1573458720, ___m_RandomSeed_3)); }
	inline uint32_t get_m_RandomSeed_3() const { return ___m_RandomSeed_3; }
	inline uint32_t* get_address_of_m_RandomSeed_3() { return &___m_RandomSeed_3; }
	inline void set_m_RandomSeed_3(uint32_t value)
	{
		___m_RandomSeed_3 = value;
	}

	inline static int32_t get_offset_of_m_ParticleSystem_4() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t1573458720, ___m_ParticleSystem_4)); }
	inline ParticleSystem_t2248283753 * get_m_ParticleSystem_4() const { return ___m_ParticleSystem_4; }
	inline ParticleSystem_t2248283753 ** get_address_of_m_ParticleSystem_4() { return &___m_ParticleSystem_4; }
	inline void set_m_ParticleSystem_4(ParticleSystem_t2248283753 * value)
	{
		___m_ParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTROLPLAYABLE_T1573458720_H
#ifndef DIRECTORCONTROLPLAYABLE_T2785872831_H
#define DIRECTORCONTROLPLAYABLE_T2785872831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DirectorControlPlayable
struct  DirectorControlPlayable_t2785872831  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.Playables.PlayableDirector UnityEngine.Timeline.DirectorControlPlayable::director
	PlayableDirector_t2171935445 * ___director_0;

public:
	inline static int32_t get_offset_of_director_0() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2785872831, ___director_0)); }
	inline PlayableDirector_t2171935445 * get_director_0() const { return ___director_0; }
	inline PlayableDirector_t2171935445 ** get_address_of_director_0() { return &___director_0; }
	inline void set_director_0(PlayableDirector_t2171935445 * value)
	{
		___director_0 = value;
		Il2CppCodeGenWriteBarrier((&___director_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORCONTROLPLAYABLE_T2785872831_H
#ifndef EVENTPLAYABLE_T1499726131_H
#define EVENTPLAYABLE_T1499726131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.EventPlayable
struct  EventPlayable_t1499726131  : public PlayableBehaviour_t3096602576
{
public:
	// System.Double UnityEngine.Timeline.EventPlayable::<triggerTime>k__BackingField
	double ___U3CtriggerTimeU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Timeline.EventPlayable::m_HasFired
	bool ___m_HasFired_1;
	// System.Double UnityEngine.Timeline.EventPlayable::m_PreviousTime
	double ___m_PreviousTime_2;

public:
	inline static int32_t get_offset_of_U3CtriggerTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventPlayable_t1499726131, ___U3CtriggerTimeU3Ek__BackingField_0)); }
	inline double get_U3CtriggerTimeU3Ek__BackingField_0() const { return ___U3CtriggerTimeU3Ek__BackingField_0; }
	inline double* get_address_of_U3CtriggerTimeU3Ek__BackingField_0() { return &___U3CtriggerTimeU3Ek__BackingField_0; }
	inline void set_U3CtriggerTimeU3Ek__BackingField_0(double value)
	{
		___U3CtriggerTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_HasFired_1() { return static_cast<int32_t>(offsetof(EventPlayable_t1499726131, ___m_HasFired_1)); }
	inline bool get_m_HasFired_1() const { return ___m_HasFired_1; }
	inline bool* get_address_of_m_HasFired_1() { return &___m_HasFired_1; }
	inline void set_m_HasFired_1(bool value)
	{
		___m_HasFired_1 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTime_2() { return static_cast<int32_t>(offsetof(EventPlayable_t1499726131, ___m_PreviousTime_2)); }
	inline double get_m_PreviousTime_2() const { return ___m_PreviousTime_2; }
	inline double* get_address_of_m_PreviousTime_2() { return &___m_PreviousTime_2; }
	inline void set_m_PreviousTime_2(double value)
	{
		___m_PreviousTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTPLAYABLE_T1499726131_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PROPERTYNAME_T65259757_H
#define PROPERTYNAME_T65259757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t65259757 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t65259757, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T65259757_H
#ifndef DISCRETETIME_T1558908690_H
#define DISCRETETIME_T1558908690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t1558908690 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t1558908690_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t1558908690  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t1558908690_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t1558908690  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t1558908690 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t1558908690  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T1558908690_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef LAYERMASK_T246267875_H
#define LAYERMASK_T246267875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t246267875 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t246267875, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T246267875_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef RUNTIMECLIPBASE_T433944096_H
#define RUNTIMECLIPBASE_T433944096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClipBase
struct  RuntimeClipBase_t433944096  : public RuntimeElement_t869620576
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIPBASE_T433944096_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef LENSFLARESTYLE_T2381973294_H
#define LENSFLARESTYLE_T2381973294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
struct  LensFlareStyle_t2381973294 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LensFlareStyle_t2381973294, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSFLARESTYLE_T2381973294_H
#ifndef BLOOMSCREENBLENDMODE_T2757664361_H
#define BLOOMSCREENBLENDMODE_T2757664361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
struct  BloomScreenBlendMode_t2757664361 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomScreenBlendMode_t2757664361, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMSCREENBLENDMODE_T2757664361_H
#ifndef MAPPINGTYPE_T1524902041_H
#define MAPPINGTYPE_T1524902041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t1524902041 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t1524902041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T1524902041_H
#ifndef AXISOPTION_T1354449705_H
#define AXISOPTION_T1354449705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1354449705 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1354449705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1354449705_H
#ifndef CONTROLSTYLE_T611612146_H
#define CONTROLSTYLE_T611612146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t611612146 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t611612146, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T611612146_H
#ifndef COLORCORRECTIONMODE_T4139101073_H
#define COLORCORRECTIONMODE_T4139101073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
struct  ColorCorrectionMode_t4139101073 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorCorrectionMode_t4139101073, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONMODE_T4139101073_H
#ifndef BLOOMQUALITY_T425680226_H
#define BLOOMQUALITY_T425680226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
struct  BloomQuality_t425680226 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomQuality_t425680226, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMQUALITY_T425680226_H
#ifndef BLURTYPE_T3292036609_H
#define BLURTYPE_T3292036609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
struct  BlurType_t3292036609 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfField/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t3292036609, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T3292036609_H
#ifndef VIRTUALINPUT_T2652211791_H
#define VIRTUALINPUT_T2652211791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2652211791  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t1986933152  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3772266050 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t1595482203 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t4069179741 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2652211791, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t1986933152  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t1986933152 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t1986933152  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2652211791, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3772266050 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3772266050 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3772266050 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2652211791, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t1595482203 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t1595482203 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t1595482203 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2652211791, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t4069179741 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t4069179741 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t4069179741 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2652211791_H
#ifndef BLURTYPE_T1352644979_H
#define BLURTYPE_T1352644979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
struct  BlurType_t1352644979 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t1352644979, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T1352644979_H
#ifndef RESOLUTION_T1117279058_H
#define RESOLUTION_T1117279058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
struct  Resolution_t1117279058 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized/Resolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Resolution_t1117279058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T1117279058_H
#ifndef TWEAKMODE_T1384345914_H
#define TWEAKMODE_T1384345914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/TweakMode
struct  TweakMode_t1384345914 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/TweakMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweakMode_t1384345914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE_T1384345914_H
#ifndef DOF34QUALITYSETTING_T1186080811_H
#define DOF34QUALITYSETTING_T1186080811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
struct  Dof34QualitySetting_t1186080811 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Dof34QualitySetting_t1186080811, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOF34QUALITYSETTING_T1186080811_H
#ifndef BLOOMSCREENBLENDMODE_T629878214_H
#define BLOOMSCREENBLENDMODE_T629878214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
struct  BloomScreenBlendMode_t629878214 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomScreenBlendMode_t629878214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMSCREENBLENDMODE_T629878214_H
#ifndef HDRBLOOMMODE_T3656197844_H
#define HDRBLOOMMODE_T3656197844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.HDRBloomMode
struct  HDRBloomMode_t3656197844 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.HDRBloomMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HDRBloomMode_t3656197844, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRBLOOMMODE_T3656197844_H
#ifndef TWEAKMODE34_T2469558863_H
#define TWEAKMODE34_T2469558863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TweakMode34
struct  TweakMode34_t2469558863 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TweakMode34::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweakMode34_t2469558863, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE34_T2469558863_H
#ifndef LENSFLARESTYLE34_T1433830633_H
#define LENSFLARESTYLE34_T1433830633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.LensflareStyle34
struct  LensflareStyle34_t1433830633 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.LensflareStyle34::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LensflareStyle34_t1433830633, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSFLARESTYLE34_T1433830633_H
#ifndef AAMODE_T657424638_H
#define AAMODE_T657424638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.AAMode
struct  AAMode_t657424638 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.AAMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AAMode_t657424638, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AAMODE_T657424638_H
#ifndef BLURSAMPLECOUNT_T3090947702_H
#define BLURSAMPLECOUNT_T3090947702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
struct  BlurSampleCount_t3090947702 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurSampleCount_t3090947702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURSAMPLECOUNT_T3090947702_H
#ifndef HDRBLOOMMODE_T1836510126_H
#define HDRBLOOMMODE_T1836510126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
struct  HDRBloomMode_t1836510126 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HDRBloomMode_t1836510126, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRBLOOMMODE_T1836510126_H
#ifndef POSTPLAYBACKSTATE_T2766824872_H
#define POSTPLAYBACKSTATE_T2766824872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState
struct  PostPlaybackState_t2766824872 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PostPlaybackState_t2766824872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T2766824872_H
#ifndef MEDIATYPE_T673317257_H
#define MEDIATYPE_T673317257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/MediaType
struct  MediaType_t673317257 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/MediaType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MediaType_t673317257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPE_T673317257_H
#ifndef MATCHTARGETFIELDS_T2467835737_H
#define MATCHTARGETFIELDS_T2467835737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFields
struct  MatchTargetFields_t2467835737 
{
public:
	// System.Int32 UnityEngine.Timeline.MatchTargetFields::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchTargetFields_t2467835737, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDS_T2467835737_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef DATASTREAMTYPE_T325920430_H
#define DATASTREAMTYPE_T325920430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t325920430 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t325920430, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T325920430_H
#ifndef PLAYABLEHANDLE_T743382320_H
#define PLAYABLEHANDLE_T743382320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t743382320 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T743382320_H
#ifndef TRACKCOLORATTRIBUTE_T915884494_H
#define TRACKCOLORATTRIBUTE_T915884494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackColorAttribute
struct  TrackColorAttribute_t915884494  : public Attribute_t1924466020
{
public:
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_t2582018970  ___m_Color_0;

public:
	inline static int32_t get_offset_of_m_Color_0() { return static_cast<int32_t>(offsetof(TrackColorAttribute_t915884494, ___m_Color_0)); }
	inline Color_t2582018970  get_m_Color_0() const { return ___m_Color_0; }
	inline Color_t2582018970 * get_address_of_m_Color_0() { return &___m_Color_0; }
	inline void set_m_Color_0(Color_t2582018970  value)
	{
		___m_Color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCOLORATTRIBUTE_T915884494_H
#ifndef PLAYABLEOUTPUTHANDLE_T506089257_H
#define PLAYABLEOUTPUTHANDLE_T506089257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t506089257 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T506089257_H
#ifndef BOKEHDESTINATION_T2246052814_H
#define BOKEHDESTINATION_T2246052814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
struct  BokehDestination_t2246052814 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BokehDestination_t2246052814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOKEHDESTINATION_T2246052814_H
#ifndef DOFBLURRINESS_T1437194827_H
#define DOFBLURRINESS_T1437194827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
struct  DofBlurriness_t1437194827 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofBlurriness_t1437194827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFBLURRINESS_T1437194827_H
#ifndef DOFRESOLUTION_T1205495178_H
#define DOFRESOLUTION_T1205495178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
struct  DofResolution_t1205495178 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofResolution_t1205495178, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFRESOLUTION_T1205495178_H
#ifndef AXISOPTIONS_T2951597766_H
#define AXISOPTIONS_T2951597766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t2951597766 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t2951597766, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T2951597766_H
#ifndef MOTIONBLURFILTER_T746967900_H
#define MOTIONBLURFILTER_T746967900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
struct  MotionBlurFilter_t746967900 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionBlurFilter_t746967900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURFILTER_T746967900_H
#ifndef EXPOSEDREFERENCE_1_T2355854472_H
#define EXPOSEDREFERENCE_1_T2355854472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<UnityEngine.GameObject>
struct  ExposedReference_1_t2355854472 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t65259757  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_t692178351 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_t2355854472, ___exposedName_0)); }
	inline PropertyName_t65259757  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t65259757 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t65259757  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_t2355854472, ___defaultValue_1)); }
	inline Object_t692178351 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_t692178351 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_t692178351 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSEDREFERENCE_1_T2355854472_H
#ifndef BLURTYPE_T56316822_H
#define BLURTYPE_T56316822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
struct  BlurType_t56316822 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t56316822, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T56316822_H
#ifndef CLIPEXTRAPOLATION_T513923237_H
#define CLIPEXTRAPOLATION_T513923237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/ClipExtrapolation
struct  ClipExtrapolation_t513923237 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/ClipExtrapolation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipExtrapolation_t513923237, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPEXTRAPOLATION_T513923237_H
#ifndef ACTIVEINPUTMETHOD_T2454902678_H
#define ACTIVEINPUTMETHOD_T2454902678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t2454902678 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t2454902678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T2454902678_H
#ifndef AXISOPTION_T1081434817_H
#define AXISOPTION_T1081434817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t1081434817 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1081434817, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1081434817_H
#ifndef INITIALSTATE_T2473441553_H
#define INITIALSTATE_T2473441553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/InitialState
struct  InitialState_t2473441553 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/InitialState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitialState_t2473441553, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALSTATE_T2473441553_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef AUDIOCLIPPLAYABLE_T2447722938_H
#define AUDIOCLIPPLAYABLE_T2447722938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioClipPlayable
struct  AudioClipPlayable_t2447722938 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioClipPlayable_t2447722938, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIPPLAYABLE_T2447722938_H
#ifndef ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#define ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t2708046649 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t506089257  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t2708046649, ___m_Handle_0)); }
	inline PlayableOutputHandle_t506089257  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t506089257 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t506089257  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#ifndef PLAYABLEBINDING_T1732044054_H
#define PLAYABLEBINDING_T1732044054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t1732044054 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t692178351 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t692178351 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t692178351 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t692178351 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t1732044054_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t4186695123* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t4186695123* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t4186695123** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t4186695123* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t1732044054_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t1732044054_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t692178351_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t1732044054_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t692178351_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t1732044054__padding[1];
	};
};
#endif // PLAYABLEBINDING_T1732044054_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#define ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t4073556314 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t4073556314, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t4073556314_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t4073556314  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t4073556314_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t4073556314  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t4073556314 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t4073556314  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#ifndef ANIMATIONCLIPPLAYABLE_T2813945836_H
#define ANIMATIONCLIPPLAYABLE_T2813945836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationClipPlayable
struct  AnimationClipPlayable_t2813945836 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationClipPlayable_t2813945836, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPPLAYABLE_T2813945836_H
#ifndef PLAYABLE_T3296292090_H
#define PLAYABLE_T3296292090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t3296292090 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t3296292090, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t3296292090_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t3296292090  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t3296292090_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t3296292090  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t3296292090 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t3296292090  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T3296292090_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef AXISMAPPING_T2276377189_H
#define AXISMAPPING_T2276377189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t2276377189  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t2276377189, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t2276377189, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T2276377189_H
#ifndef MOBILEINPUT_T3573436291_H
#define MOBILEINPUT_T3573436291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t3573436291  : public VirtualInput_t2652211791
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T3573436291_H
#ifndef ACTIVATIONCONTROLPLAYABLE_T3132244818_H
#define ACTIVATIONCONTROLPLAYABLE_T3132244818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable
struct  ActivationControlPlayable_t3132244818  : public PlayableBehaviour_t3096602576
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationControlPlayable::gameObject
	GameObject_t2557347079 * ___gameObject_0;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ActivationControlPlayable::postPlayback
	int32_t ___postPlayback_1;
	// UnityEngine.Timeline.ActivationControlPlayable/InitialState UnityEngine.Timeline.ActivationControlPlayable::m_InitialState
	int32_t ___m_InitialState_2;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t3132244818, ___gameObject_0)); }
	inline GameObject_t2557347079 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t2557347079 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t2557347079 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_postPlayback_1() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t3132244818, ___postPlayback_1)); }
	inline int32_t get_postPlayback_1() const { return ___postPlayback_1; }
	inline int32_t* get_address_of_postPlayback_1() { return &___postPlayback_1; }
	inline void set_postPlayback_1(int32_t value)
	{
		___postPlayback_1 = value;
	}

	inline static int32_t get_offset_of_m_InitialState_2() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t3132244818, ___m_InitialState_2)); }
	inline int32_t get_m_InitialState_2() const { return ___m_InitialState_2; }
	inline int32_t* get_address_of_m_InitialState_2() { return &___m_InitialState_2; }
	inline void set_m_InitialState_2(int32_t value)
	{
		___m_InitialState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONCONTROLPLAYABLE_T3132244818_H
#ifndef MATCHTARGETFIELDCONSTANTS_T1045040963_H
#define MATCHTARGETFIELDCONSTANTS_T1045040963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFieldConstants
struct  MatchTargetFieldConstants_t1045040963  : public RuntimeObject
{
public:

public:
};

struct MatchTargetFieldConstants_t1045040963_StaticFields
{
public:
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::All
	int32_t ___All_0;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::None
	int32_t ___None_1;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Position
	int32_t ___Position_2;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Rotation
	int32_t ___Rotation_3;

public:
	inline static int32_t get_offset_of_All_0() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t1045040963_StaticFields, ___All_0)); }
	inline int32_t get_All_0() const { return ___All_0; }
	inline int32_t* get_address_of_All_0() { return &___All_0; }
	inline void set_All_0(int32_t value)
	{
		___All_0 = value;
	}

	inline static int32_t get_offset_of_None_1() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t1045040963_StaticFields, ___None_1)); }
	inline int32_t get_None_1() const { return ___None_1; }
	inline int32_t* get_address_of_None_1() { return &___None_1; }
	inline void set_None_1(int32_t value)
	{
		___None_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t1045040963_StaticFields, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t1045040963_StaticFields, ___Rotation_3)); }
	inline int32_t get_Rotation_3() const { return ___Rotation_3; }
	inline int32_t* get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(int32_t value)
	{
		___Rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDCONSTANTS_T1045040963_H
#ifndef STANDALONEINPUT_T1224557719_H
#define STANDALONEINPUT_T1224557719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1224557719  : public VirtualInput_t2652211791
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1224557719_H
#ifndef ANIMATIONOUTPUTWEIGHTPROCESSOR_T3652584005_H
#define ANIMATIONOUTPUTWEIGHTPROCESSOR_T3652584005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor
struct  AnimationOutputWeightProcessor_t3652584005  : public RuntimeObject
{
public:
	// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Output
	AnimationPlayableOutput_t2708046649  ___m_Output_0;
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_LayerMixer
	AnimationLayerMixerPlayable_t4073556314  ___m_LayerMixer_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo> UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Mixers
	List_1_t3281727266 * ___m_Mixers_2;

public:
	inline static int32_t get_offset_of_m_Output_0() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t3652584005, ___m_Output_0)); }
	inline AnimationPlayableOutput_t2708046649  get_m_Output_0() const { return ___m_Output_0; }
	inline AnimationPlayableOutput_t2708046649 * get_address_of_m_Output_0() { return &___m_Output_0; }
	inline void set_m_Output_0(AnimationPlayableOutput_t2708046649  value)
	{
		___m_Output_0 = value;
	}

	inline static int32_t get_offset_of_m_LayerMixer_1() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t3652584005, ___m_LayerMixer_1)); }
	inline AnimationLayerMixerPlayable_t4073556314  get_m_LayerMixer_1() const { return ___m_LayerMixer_1; }
	inline AnimationLayerMixerPlayable_t4073556314 * get_address_of_m_LayerMixer_1() { return &___m_LayerMixer_1; }
	inline void set_m_LayerMixer_1(AnimationLayerMixerPlayable_t4073556314  value)
	{
		___m_LayerMixer_1 = value;
	}

	inline static int32_t get_offset_of_m_Mixers_2() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t3652584005, ___m_Mixers_2)); }
	inline List_1_t3281727266 * get_m_Mixers_2() const { return ___m_Mixers_2; }
	inline List_1_t3281727266 ** get_address_of_m_Mixers_2() { return &___m_Mixers_2; }
	inline void set_m_Mixers_2(List_1_t3281727266 * value)
	{
		___m_Mixers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mixers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOUTPUTWEIGHTPROCESSOR_T3652584005_H
#ifndef BASICPLAYABLEBEHAVIOUR_T3342818502_H
#define BASICPLAYABLEBEHAVIOUR_T3342818502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.BasicPlayableBehaviour
struct  BasicPlayableBehaviour_t3342818502  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICPLAYABLEBEHAVIOUR_T3342818502_H
#ifndef RUNTIMECLIP_T3327076603_H
#define RUNTIMECLIP_T3327076603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClip
struct  RuntimeClip_t3327076603  : public RuntimeClipBase_t433944096
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.RuntimeClip::m_Clip
	TimelineClip_t2489983630 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_Playable
	Playable_t3296292090  ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_ParentMixer
	Playable_t3296292090  ___m_ParentMixer_3;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(RuntimeClip_t3327076603, ___m_Clip_1)); }
	inline TimelineClip_t2489983630 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2489983630 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2489983630 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(RuntimeClip_t3327076603, ___m_Playable_2)); }
	inline Playable_t3296292090  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t3296292090 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t3296292090  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_3() { return static_cast<int32_t>(offsetof(RuntimeClip_t3327076603, ___m_ParentMixer_3)); }
	inline Playable_t3296292090  get_m_ParentMixer_3() const { return ___m_ParentMixer_3; }
	inline Playable_t3296292090 * get_address_of_m_ParentMixer_3() { return &___m_ParentMixer_3; }
	inline void set_m_ParentMixer_3(Playable_t3296292090  value)
	{
		___m_ParentMixer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIP_T3327076603_H
#ifndef U3CU3EC__ITERATOR0_T347944413_H
#define U3CU3EC__ITERATOR0_T347944413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t347944413  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioTrack UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$this
	AudioTrack_t2200009395 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t347944413, ___U24this_0)); }
	inline AudioTrack_t2200009395 * get_U24this_0() const { return ___U24this_0; }
	inline AudioTrack_t2200009395 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioTrack_t2200009395 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t347944413, ___U24current_1)); }
	inline PlayableBinding_t1732044054  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t1732044054  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t347944413, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t347944413, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T347944413_H
#ifndef SCHEDULERUNTIMECLIP_T3649402065_H
#define SCHEDULERUNTIMECLIP_T3649402065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ScheduleRuntimeClip
struct  ScheduleRuntimeClip_t3649402065  : public RuntimeClipBase_t433944096
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.ScheduleRuntimeClip::m_Clip
	TimelineClip_t2489983630 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_Playable
	Playable_t3296292090  ___m_Playable_2;
	// UnityEngine.Audio.AudioClipPlayable UnityEngine.Timeline.ScheduleRuntimeClip::m_AudioClipPlayable
	AudioClipPlayable_t2447722938  ___m_AudioClipPlayable_3;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_ParentMixer
	Playable_t3296292090  ___m_ParentMixer_4;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_StartDelay
	double ___m_StartDelay_5;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_FinishTail
	double ___m_FinishTail_6;
	// System.Boolean UnityEngine.Timeline.ScheduleRuntimeClip::m_Started
	bool ___m_Started_7;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_Clip_1)); }
	inline TimelineClip_t2489983630 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2489983630 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2489983630 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_Playable_2)); }
	inline Playable_t3296292090  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t3296292090 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t3296292090  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_AudioClipPlayable_3() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_AudioClipPlayable_3)); }
	inline AudioClipPlayable_t2447722938  get_m_AudioClipPlayable_3() const { return ___m_AudioClipPlayable_3; }
	inline AudioClipPlayable_t2447722938 * get_address_of_m_AudioClipPlayable_3() { return &___m_AudioClipPlayable_3; }
	inline void set_m_AudioClipPlayable_3(AudioClipPlayable_t2447722938  value)
	{
		___m_AudioClipPlayable_3 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_4() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_ParentMixer_4)); }
	inline Playable_t3296292090  get_m_ParentMixer_4() const { return ___m_ParentMixer_4; }
	inline Playable_t3296292090 * get_address_of_m_ParentMixer_4() { return &___m_ParentMixer_4; }
	inline void set_m_ParentMixer_4(Playable_t3296292090  value)
	{
		___m_ParentMixer_4 = value;
	}

	inline static int32_t get_offset_of_m_StartDelay_5() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_StartDelay_5)); }
	inline double get_m_StartDelay_5() const { return ___m_StartDelay_5; }
	inline double* get_address_of_m_StartDelay_5() { return &___m_StartDelay_5; }
	inline void set_m_StartDelay_5(double value)
	{
		___m_StartDelay_5 = value;
	}

	inline static int32_t get_offset_of_m_FinishTail_6() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_FinishTail_6)); }
	inline double get_m_FinishTail_6() const { return ___m_FinishTail_6; }
	inline double* get_address_of_m_FinishTail_6() { return &___m_FinishTail_6; }
	inline void set_m_FinishTail_6(double value)
	{
		___m_FinishTail_6 = value;
	}

	inline static int32_t get_offset_of_m_Started_7() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t3649402065, ___m_Started_7)); }
	inline bool get_m_Started_7() const { return ___m_Started_7; }
	inline bool* get_address_of_m_Started_7() { return &___m_Started_7; }
	inline void set_m_Started_7(bool value)
	{
		___m_Started_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULERUNTIMECLIP_T3649402065_H
#ifndef U3CU3EC__ITERATOR0_T1763991329_H
#define U3CU3EC__ITERATOR0_T1763991329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1763991329  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationTrack UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$this
	AnimationTrack_t200707209 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1763991329, ___U24this_0)); }
	inline AnimationTrack_t200707209 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationTrack_t200707209 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationTrack_t200707209 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1763991329, ___U24current_1)); }
	inline PlayableBinding_t1732044054  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t1732044054  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1763991329, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1763991329, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1763991329_H
#ifndef U3CU3EC__ITERATOR0_T2002482455_H
#define U3CU3EC__ITERATOR0_T2002482455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2002482455  : public RuntimeObject
{
public:
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_0;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2002482455, ___U24current_0)); }
	inline PlayableBinding_t1732044054  get_U24current_0() const { return ___U24current_0; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(PlayableBinding_t1732044054  value)
	{
		___U24current_0 = value;
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2002482455, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2002482455, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2002482455_H
#ifndef WEIGHTINFO_T4162100259_H
#define WEIGHTINFO_T4162100259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct  WeightInfo_t4162100259 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t3296292090  ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t3296292090  ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::modulate
	bool ___modulate_3;

public:
	inline static int32_t get_offset_of_mixer_0() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___mixer_0)); }
	inline Playable_t3296292090  get_mixer_0() const { return ___mixer_0; }
	inline Playable_t3296292090 * get_address_of_mixer_0() { return &___mixer_0; }
	inline void set_mixer_0(Playable_t3296292090  value)
	{
		___mixer_0 = value;
	}

	inline static int32_t get_offset_of_parentMixer_1() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___parentMixer_1)); }
	inline Playable_t3296292090  get_parentMixer_1() const { return ___parentMixer_1; }
	inline Playable_t3296292090 * get_address_of_parentMixer_1() { return &___parentMixer_1; }
	inline void set_parentMixer_1(Playable_t3296292090  value)
	{
		___parentMixer_1 = value;
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_modulate_3() { return static_cast<int32_t>(offsetof(WeightInfo_t4162100259, ___modulate_3)); }
	inline bool get_modulate_3() const { return ___modulate_3; }
	inline bool* get_address_of_modulate_3() { return &___modulate_3; }
	inline void set_modulate_3(bool value)
	{
		___modulate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t4162100259_marshaled_pinvoke
{
	Playable_t3296292090  ___mixer_0;
	Playable_t3296292090  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t4162100259_marshaled_com
{
	Playable_t3296292090  ___mixer_0;
	Playable_t3296292090  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
#endif // WEIGHTINFO_T4162100259_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef U3CU3EC__ITERATOR0_T2455484034_H
#define U3CU3EC__ITERATOR0_T2455484034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2455484034  : public RuntimeObject
{
public:
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t1732044054  ___U24current_0;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2455484034, ___U24current_0)); }
	inline PlayableBinding_t1732044054  get_U24current_0() const { return ___U24current_0; }
	inline PlayableBinding_t1732044054 * get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(PlayableBinding_t1732044054  value)
	{
		___U24current_0 = value;
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2455484034, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2455484034, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2455484034_H
#ifndef PLAYABLEASSET_T1461289625_H
#define PLAYABLEASSET_T1461289625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t1461289625  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T1461289625_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef TRACKASSET_T2823685959_H
#define TRACKASSET_T2823685959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2823685959  : public PlayableAsset_t1461289625
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_2;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_3;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_4;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t1145879031 * ___m_AnimClip_5;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t1461289625 * ___m_Parent_6;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t1943312966 * ___m_Children_7;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_8;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t1733651323* ___m_ClipsCache_9;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t1558908690  ___m_Start_10;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t1558908690  ___m_End_11;
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackAsset::m_MediaType
	int32_t ___m_MediaType_12;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t1609610637 * ___m_Clips_14;

public:
	inline static int32_t get_offset_of_m_Locked_2() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Locked_2)); }
	inline bool get_m_Locked_2() const { return ___m_Locked_2; }
	inline bool* get_address_of_m_Locked_2() { return &___m_Locked_2; }
	inline void set_m_Locked_2(bool value)
	{
		___m_Locked_2 = value;
	}

	inline static int32_t get_offset_of_m_Muted_3() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Muted_3)); }
	inline bool get_m_Muted_3() const { return ___m_Muted_3; }
	inline bool* get_address_of_m_Muted_3() { return &___m_Muted_3; }
	inline void set_m_Muted_3(bool value)
	{
		___m_Muted_3 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_CustomPlayableFullTypename_4)); }
	inline String_t* get_m_CustomPlayableFullTypename_4() const { return ___m_CustomPlayableFullTypename_4; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_4() { return &___m_CustomPlayableFullTypename_4; }
	inline void set_m_CustomPlayableFullTypename_4(String_t* value)
	{
		___m_CustomPlayableFullTypename_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_4), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_AnimClip_5)); }
	inline AnimationClip_t1145879031 * get_m_AnimClip_5() const { return ___m_AnimClip_5; }
	inline AnimationClip_t1145879031 ** get_address_of_m_AnimClip_5() { return &___m_AnimClip_5; }
	inline void set_m_AnimClip_5(AnimationClip_t1145879031 * value)
	{
		___m_AnimClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_5), value);
	}

	inline static int32_t get_offset_of_m_Parent_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Parent_6)); }
	inline PlayableAsset_t1461289625 * get_m_Parent_6() const { return ___m_Parent_6; }
	inline PlayableAsset_t1461289625 ** get_address_of_m_Parent_6() { return &___m_Parent_6; }
	inline void set_m_Parent_6(PlayableAsset_t1461289625 * value)
	{
		___m_Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_6), value);
	}

	inline static int32_t get_offset_of_m_Children_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Children_7)); }
	inline List_1_t1943312966 * get_m_Children_7() const { return ___m_Children_7; }
	inline List_1_t1943312966 ** get_address_of_m_Children_7() { return &___m_Children_7; }
	inline void set_m_Children_7(List_1_t1943312966 * value)
	{
		___m_Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_7), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ItemsHash_8)); }
	inline int32_t get_m_ItemsHash_8() const { return ___m_ItemsHash_8; }
	inline int32_t* get_address_of_m_ItemsHash_8() { return &___m_ItemsHash_8; }
	inline void set_m_ItemsHash_8(int32_t value)
	{
		___m_ItemsHash_8 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_ClipsCache_9)); }
	inline TimelineClipU5BU5D_t1733651323* get_m_ClipsCache_9() const { return ___m_ClipsCache_9; }
	inline TimelineClipU5BU5D_t1733651323** get_address_of_m_ClipsCache_9() { return &___m_ClipsCache_9; }
	inline void set_m_ClipsCache_9(TimelineClipU5BU5D_t1733651323* value)
	{
		___m_ClipsCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_9), value);
	}

	inline static int32_t get_offset_of_m_Start_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Start_10)); }
	inline DiscreteTime_t1558908690  get_m_Start_10() const { return ___m_Start_10; }
	inline DiscreteTime_t1558908690 * get_address_of_m_Start_10() { return &___m_Start_10; }
	inline void set_m_Start_10(DiscreteTime_t1558908690  value)
	{
		___m_Start_10 = value;
	}

	inline static int32_t get_offset_of_m_End_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_End_11)); }
	inline DiscreteTime_t1558908690  get_m_End_11() const { return ___m_End_11; }
	inline DiscreteTime_t1558908690 * get_address_of_m_End_11() { return &___m_End_11; }
	inline void set_m_End_11(DiscreteTime_t1558908690  value)
	{
		___m_End_11 = value;
	}

	inline static int32_t get_offset_of_m_MediaType_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_MediaType_12)); }
	inline int32_t get_m_MediaType_12() const { return ___m_MediaType_12; }
	inline int32_t* get_address_of_m_MediaType_12() { return &___m_MediaType_12; }
	inline void set_m_MediaType_12(int32_t value)
	{
		___m_MediaType_12 = value;
	}

	inline static int32_t get_offset_of_m_Clips_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959, ___m_Clips_14)); }
	inline List_1_t1609610637 * get_m_Clips_14() const { return ___m_Clips_14; }
	inline List_1_t1609610637 ** get_address_of_m_Clips_14() { return &___m_Clips_14; }
	inline void set_m_Clips_14(List_1_t1609610637 * value)
	{
		___m_Clips_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_14), value);
	}
};

struct TrackAsset_t2823685959_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t2389961773 * ___s_TrackBindingTypeAttributeCache_13;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t3874041483 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___s_TrackBindingTypeAttributeCache_13)); }
	inline Dictionary_2_t2389961773 * get_s_TrackBindingTypeAttributeCache_13() const { return ___s_TrackBindingTypeAttributeCache_13; }
	inline Dictionary_2_t2389961773 ** get_address_of_s_TrackBindingTypeAttributeCache_13() { return &___s_TrackBindingTypeAttributeCache_13; }
	inline void set_s_TrackBindingTypeAttributeCache_13(Dictionary_2_t2389961773 * value)
	{
		___s_TrackBindingTypeAttributeCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2823685959_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Comparison_1_t3874041483 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Comparison_1_t3874041483 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Comparison_1_t3874041483 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2823685959_H
#ifndef CONTROLPLAYABLEASSET_T1457549000_H
#define CONTROLPLAYABLEASSET_T1457549000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset
struct  ControlPlayableAsset_t1457549000  : public PlayableAsset_t1461289625
{
public:
	// UnityEngine.ExposedReference`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::sourceGameObject
	ExposedReference_1_t2355854472  ___sourceGameObject_3;
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset::prefabGameObject
	GameObject_t2557347079 * ___prefabGameObject_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateParticle
	bool ___updateParticle_5;
	// System.UInt32 UnityEngine.Timeline.ControlPlayableAsset::particleRandomSeed
	uint32_t ___particleRandomSeed_6;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateDirector
	bool ___updateDirector_7;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateITimeControl
	bool ___updateITimeControl_8;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::searchHierarchy
	bool ___searchHierarchy_9;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::active
	bool ___active_10;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ControlPlayableAsset::postPlayback
	int32_t ___postPlayback_11;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.ControlPlayableAsset::m_ControlDirectorAsset
	PlayableAsset_t1461289625 * ___m_ControlDirectorAsset_12;
	// System.Double UnityEngine.Timeline.ControlPlayableAsset::m_Duration
	double ___m_Duration_13;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::m_SupportLoop
	bool ___m_SupportLoop_14;

public:
	inline static int32_t get_offset_of_sourceGameObject_3() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___sourceGameObject_3)); }
	inline ExposedReference_1_t2355854472  get_sourceGameObject_3() const { return ___sourceGameObject_3; }
	inline ExposedReference_1_t2355854472 * get_address_of_sourceGameObject_3() { return &___sourceGameObject_3; }
	inline void set_sourceGameObject_3(ExposedReference_1_t2355854472  value)
	{
		___sourceGameObject_3 = value;
	}

	inline static int32_t get_offset_of_prefabGameObject_4() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___prefabGameObject_4)); }
	inline GameObject_t2557347079 * get_prefabGameObject_4() const { return ___prefabGameObject_4; }
	inline GameObject_t2557347079 ** get_address_of_prefabGameObject_4() { return &___prefabGameObject_4; }
	inline void set_prefabGameObject_4(GameObject_t2557347079 * value)
	{
		___prefabGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGameObject_4), value);
	}

	inline static int32_t get_offset_of_updateParticle_5() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___updateParticle_5)); }
	inline bool get_updateParticle_5() const { return ___updateParticle_5; }
	inline bool* get_address_of_updateParticle_5() { return &___updateParticle_5; }
	inline void set_updateParticle_5(bool value)
	{
		___updateParticle_5 = value;
	}

	inline static int32_t get_offset_of_particleRandomSeed_6() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___particleRandomSeed_6)); }
	inline uint32_t get_particleRandomSeed_6() const { return ___particleRandomSeed_6; }
	inline uint32_t* get_address_of_particleRandomSeed_6() { return &___particleRandomSeed_6; }
	inline void set_particleRandomSeed_6(uint32_t value)
	{
		___particleRandomSeed_6 = value;
	}

	inline static int32_t get_offset_of_updateDirector_7() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___updateDirector_7)); }
	inline bool get_updateDirector_7() const { return ___updateDirector_7; }
	inline bool* get_address_of_updateDirector_7() { return &___updateDirector_7; }
	inline void set_updateDirector_7(bool value)
	{
		___updateDirector_7 = value;
	}

	inline static int32_t get_offset_of_updateITimeControl_8() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___updateITimeControl_8)); }
	inline bool get_updateITimeControl_8() const { return ___updateITimeControl_8; }
	inline bool* get_address_of_updateITimeControl_8() { return &___updateITimeControl_8; }
	inline void set_updateITimeControl_8(bool value)
	{
		___updateITimeControl_8 = value;
	}

	inline static int32_t get_offset_of_searchHierarchy_9() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___searchHierarchy_9)); }
	inline bool get_searchHierarchy_9() const { return ___searchHierarchy_9; }
	inline bool* get_address_of_searchHierarchy_9() { return &___searchHierarchy_9; }
	inline void set_searchHierarchy_9(bool value)
	{
		___searchHierarchy_9 = value;
	}

	inline static int32_t get_offset_of_active_10() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___active_10)); }
	inline bool get_active_10() const { return ___active_10; }
	inline bool* get_address_of_active_10() { return &___active_10; }
	inline void set_active_10(bool value)
	{
		___active_10 = value;
	}

	inline static int32_t get_offset_of_postPlayback_11() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___postPlayback_11)); }
	inline int32_t get_postPlayback_11() const { return ___postPlayback_11; }
	inline int32_t* get_address_of_postPlayback_11() { return &___postPlayback_11; }
	inline void set_postPlayback_11(int32_t value)
	{
		___postPlayback_11 = value;
	}

	inline static int32_t get_offset_of_m_ControlDirectorAsset_12() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___m_ControlDirectorAsset_12)); }
	inline PlayableAsset_t1461289625 * get_m_ControlDirectorAsset_12() const { return ___m_ControlDirectorAsset_12; }
	inline PlayableAsset_t1461289625 ** get_address_of_m_ControlDirectorAsset_12() { return &___m_ControlDirectorAsset_12; }
	inline void set_m_ControlDirectorAsset_12(PlayableAsset_t1461289625 * value)
	{
		___m_ControlDirectorAsset_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlDirectorAsset_12), value);
	}

	inline static int32_t get_offset_of_m_Duration_13() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___m_Duration_13)); }
	inline double get_m_Duration_13() const { return ___m_Duration_13; }
	inline double* get_address_of_m_Duration_13() { return &___m_Duration_13; }
	inline void set_m_Duration_13(double value)
	{
		___m_Duration_13 = value;
	}

	inline static int32_t get_offset_of_m_SupportLoop_14() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000, ___m_SupportLoop_14)); }
	inline bool get_m_SupportLoop_14() const { return ___m_SupportLoop_14; }
	inline bool* get_address_of_m_SupportLoop_14() { return &___m_SupportLoop_14; }
	inline void set_m_SupportLoop_14(bool value)
	{
		___m_SupportLoop_14 = value;
	}
};

struct ControlPlayableAsset_t1457549000_StaticFields
{
public:
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset::k_MaxRandInt
	int32_t ___k_MaxRandInt_2;

public:
	inline static int32_t get_offset_of_k_MaxRandInt_2() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t1457549000_StaticFields, ___k_MaxRandInt_2)); }
	inline int32_t get_k_MaxRandInt_2() const { return ___k_MaxRandInt_2; }
	inline int32_t* get_address_of_k_MaxRandInt_2() { return &___k_MaxRandInt_2; }
	inline void set_k_MaxRandInt_2(int32_t value)
	{
		___k_MaxRandInt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPLAYABLEASSET_T1457549000_H
#ifndef AUDIOPLAYABLEASSET_T3472402037_H
#define AUDIOPLAYABLEASSET_T3472402037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset
struct  AudioPlayableAsset_t3472402037  : public PlayableAsset_t1461289625
{
public:
	// UnityEngine.AudioClip UnityEngine.Timeline.AudioPlayableAsset::m_Clip
	AudioClip_t1419814565 * ___m_Clip_2;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset::m_Loop
	bool ___m_Loop_3;
	// System.Single UnityEngine.Timeline.AudioPlayableAsset::m_bufferingTime
	float ___m_bufferingTime_4;

public:
	inline static int32_t get_offset_of_m_Clip_2() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t3472402037, ___m_Clip_2)); }
	inline AudioClip_t1419814565 * get_m_Clip_2() const { return ___m_Clip_2; }
	inline AudioClip_t1419814565 ** get_address_of_m_Clip_2() { return &___m_Clip_2; }
	inline void set_m_Clip_2(AudioClip_t1419814565 * value)
	{
		___m_Clip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_2), value);
	}

	inline static int32_t get_offset_of_m_Loop_3() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t3472402037, ___m_Loop_3)); }
	inline bool get_m_Loop_3() const { return ___m_Loop_3; }
	inline bool* get_address_of_m_Loop_3() { return &___m_Loop_3; }
	inline void set_m_Loop_3(bool value)
	{
		___m_Loop_3 = value;
	}

	inline static int32_t get_offset_of_m_bufferingTime_4() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t3472402037, ___m_bufferingTime_4)); }
	inline float get_m_bufferingTime_4() const { return ___m_bufferingTime_4; }
	inline float* get_address_of_m_bufferingTime_4() { return &___m_bufferingTime_4; }
	inline void set_m_bufferingTime_4(float value)
	{
		___m_bufferingTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEASSET_T3472402037_H
#ifndef ANIMATIONPLAYABLEASSET_T4235477116_H
#define ANIMATIONPLAYABLEASSET_T4235477116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset
struct  AnimationPlayableAsset_t4235477116  : public PlayableAsset_t1461289625
{
public:
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationPlayableAsset::m_Clip
	AnimationClip_t1145879031 * ___m_Clip_2;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_Position
	Vector3_t1986933152  ___m_Position_3;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationPlayableAsset::m_Rotation
	Quaternion_t704191599  ___m_Rotation_4;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_UseTrackMatchFields
	bool ___m_UseTrackMatchFields_5;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationPlayableAsset::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_6;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_RemoveStartOffset
	bool ___m_RemoveStartOffset_7;
	// UnityEngine.Animations.AnimationClipPlayable UnityEngine.Timeline.AnimationPlayableAsset::m_AnimationClipPlayable
	AnimationClipPlayable_t2813945836  ___m_AnimationClipPlayable_8;

public:
	inline static int32_t get_offset_of_m_Clip_2() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_Clip_2)); }
	inline AnimationClip_t1145879031 * get_m_Clip_2() const { return ___m_Clip_2; }
	inline AnimationClip_t1145879031 ** get_address_of_m_Clip_2() { return &___m_Clip_2; }
	inline void set_m_Clip_2(AnimationClip_t1145879031 * value)
	{
		___m_Clip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_2), value);
	}

	inline static int32_t get_offset_of_m_Position_3() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_Position_3)); }
	inline Vector3_t1986933152  get_m_Position_3() const { return ___m_Position_3; }
	inline Vector3_t1986933152 * get_address_of_m_Position_3() { return &___m_Position_3; }
	inline void set_m_Position_3(Vector3_t1986933152  value)
	{
		___m_Position_3 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_4() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_Rotation_4)); }
	inline Quaternion_t704191599  get_m_Rotation_4() const { return ___m_Rotation_4; }
	inline Quaternion_t704191599 * get_address_of_m_Rotation_4() { return &___m_Rotation_4; }
	inline void set_m_Rotation_4(Quaternion_t704191599  value)
	{
		___m_Rotation_4 = value;
	}

	inline static int32_t get_offset_of_m_UseTrackMatchFields_5() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_UseTrackMatchFields_5)); }
	inline bool get_m_UseTrackMatchFields_5() const { return ___m_UseTrackMatchFields_5; }
	inline bool* get_address_of_m_UseTrackMatchFields_5() { return &___m_UseTrackMatchFields_5; }
	inline void set_m_UseTrackMatchFields_5(bool value)
	{
		___m_UseTrackMatchFields_5 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_6() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_MatchTargetFields_6)); }
	inline int32_t get_m_MatchTargetFields_6() const { return ___m_MatchTargetFields_6; }
	inline int32_t* get_address_of_m_MatchTargetFields_6() { return &___m_MatchTargetFields_6; }
	inline void set_m_MatchTargetFields_6(int32_t value)
	{
		___m_MatchTargetFields_6 = value;
	}

	inline static int32_t get_offset_of_m_RemoveStartOffset_7() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_RemoveStartOffset_7)); }
	inline bool get_m_RemoveStartOffset_7() const { return ___m_RemoveStartOffset_7; }
	inline bool* get_address_of_m_RemoveStartOffset_7() { return &___m_RemoveStartOffset_7; }
	inline void set_m_RemoveStartOffset_7(bool value)
	{
		___m_RemoveStartOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_AnimationClipPlayable_8() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t4235477116, ___m_AnimationClipPlayable_8)); }
	inline AnimationClipPlayable_t2813945836  get_m_AnimationClipPlayable_8() const { return ___m_AnimationClipPlayable_8; }
	inline AnimationClipPlayable_t2813945836 * get_address_of_m_AnimationClipPlayable_8() { return &___m_AnimationClipPlayable_8; }
	inline void set_m_AnimationClipPlayable_8(AnimationClipPlayable_t2813945836  value)
	{
		___m_AnimationClipPlayable_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSET_T4235477116_H
#ifndef CONTROLTRACK_T3303583055_H
#define CONTROLTRACK_T3303583055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlTrack
struct  ControlTrack_t3303583055  : public TrackAsset_t2823685959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTRACK_T3303583055_H
#ifndef IMAGEEFFECTBASE_T4133330248_H
#define IMAGEEFFECTBASE_T4133330248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffectBase
struct  ImageEffectBase_t4133330248  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ImageEffectBase::shader
	Shader_t1881769421 * ___shader_2;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::m_Material
	Material_t2815264910 * ___m_Material_3;

public:
	inline static int32_t get_offset_of_shader_2() { return static_cast<int32_t>(offsetof(ImageEffectBase_t4133330248, ___shader_2)); }
	inline Shader_t1881769421 * get_shader_2() const { return ___shader_2; }
	inline Shader_t1881769421 ** get_address_of_shader_2() { return &___shader_2; }
	inline void set_shader_2(Shader_t1881769421 * value)
	{
		___shader_2 = value;
		Il2CppCodeGenWriteBarrier((&___shader_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ImageEffectBase_t4133330248, ___m_Material_3)); }
	inline Material_t2815264910 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t2815264910 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t2815264910 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTBASE_T4133330248_H
#ifndef AUDIOTRACK_T2200009395_H
#define AUDIOTRACK_T2200009395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack
struct  AudioTrack_t2200009395  : public TrackAsset_t2823685959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTRACK_T2200009395_H
#ifndef ANIMATIONTRACK_T200707209_H
#define ANIMATIONTRACK_T200707209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack
struct  AnimationTrack_t200707209  : public TrackAsset_t2823685959
{
public:
	// UnityEngine.Timeline.AnimationPlayableAsset UnityEngine.Timeline.AnimationTrack::m_AnimationPlayableAsset
	AnimationPlayableAsset_t4235477116 * ___m_AnimationPlayableAsset_16;
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.AnimationTrack::m_FakeAnimClip
	TimelineClip_t2489983630 * ___m_FakeAnimClip_17;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPreExtrapolation
	int32_t ___m_OpenClipPreExtrapolation_18;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPostExtrapolation
	int32_t ___m_OpenClipPostExtrapolation_19;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetPosition
	Vector3_t1986933152  ___m_OpenClipOffsetPosition_20;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetRotation
	Quaternion_t704191599  ___m_OpenClipOffsetRotation_21;
	// System.Double UnityEngine.Timeline.AnimationTrack::m_OpenClipTimeOffset
	double ___m_OpenClipTimeOffset_22;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationTrack::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_23;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_Position
	Vector3_t1986933152  ___m_Position_24;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_Rotation
	Quaternion_t704191599  ___m_Rotation_25;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyOffsets
	bool ___m_ApplyOffsets_26;
	// UnityEngine.AvatarMask UnityEngine.Timeline.AnimationTrack::m_AvatarMask
	AvatarMask_t2076757458 * ___m_AvatarMask_27;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyAvatarMask
	bool ___m_ApplyAvatarMask_28;

public:
	inline static int32_t get_offset_of_m_AnimationPlayableAsset_16() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_AnimationPlayableAsset_16)); }
	inline AnimationPlayableAsset_t4235477116 * get_m_AnimationPlayableAsset_16() const { return ___m_AnimationPlayableAsset_16; }
	inline AnimationPlayableAsset_t4235477116 ** get_address_of_m_AnimationPlayableAsset_16() { return &___m_AnimationPlayableAsset_16; }
	inline void set_m_AnimationPlayableAsset_16(AnimationPlayableAsset_t4235477116 * value)
	{
		___m_AnimationPlayableAsset_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationPlayableAsset_16), value);
	}

	inline static int32_t get_offset_of_m_FakeAnimClip_17() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_FakeAnimClip_17)); }
	inline TimelineClip_t2489983630 * get_m_FakeAnimClip_17() const { return ___m_FakeAnimClip_17; }
	inline TimelineClip_t2489983630 ** get_address_of_m_FakeAnimClip_17() { return &___m_FakeAnimClip_17; }
	inline void set_m_FakeAnimClip_17(TimelineClip_t2489983630 * value)
	{
		___m_FakeAnimClip_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_FakeAnimClip_17), value);
	}

	inline static int32_t get_offset_of_m_OpenClipPreExtrapolation_18() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_OpenClipPreExtrapolation_18)); }
	inline int32_t get_m_OpenClipPreExtrapolation_18() const { return ___m_OpenClipPreExtrapolation_18; }
	inline int32_t* get_address_of_m_OpenClipPreExtrapolation_18() { return &___m_OpenClipPreExtrapolation_18; }
	inline void set_m_OpenClipPreExtrapolation_18(int32_t value)
	{
		___m_OpenClipPreExtrapolation_18 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipPostExtrapolation_19() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_OpenClipPostExtrapolation_19)); }
	inline int32_t get_m_OpenClipPostExtrapolation_19() const { return ___m_OpenClipPostExtrapolation_19; }
	inline int32_t* get_address_of_m_OpenClipPostExtrapolation_19() { return &___m_OpenClipPostExtrapolation_19; }
	inline void set_m_OpenClipPostExtrapolation_19(int32_t value)
	{
		___m_OpenClipPostExtrapolation_19 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetPosition_20() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_OpenClipOffsetPosition_20)); }
	inline Vector3_t1986933152  get_m_OpenClipOffsetPosition_20() const { return ___m_OpenClipOffsetPosition_20; }
	inline Vector3_t1986933152 * get_address_of_m_OpenClipOffsetPosition_20() { return &___m_OpenClipOffsetPosition_20; }
	inline void set_m_OpenClipOffsetPosition_20(Vector3_t1986933152  value)
	{
		___m_OpenClipOffsetPosition_20 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetRotation_21() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_OpenClipOffsetRotation_21)); }
	inline Quaternion_t704191599  get_m_OpenClipOffsetRotation_21() const { return ___m_OpenClipOffsetRotation_21; }
	inline Quaternion_t704191599 * get_address_of_m_OpenClipOffsetRotation_21() { return &___m_OpenClipOffsetRotation_21; }
	inline void set_m_OpenClipOffsetRotation_21(Quaternion_t704191599  value)
	{
		___m_OpenClipOffsetRotation_21 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipTimeOffset_22() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_OpenClipTimeOffset_22)); }
	inline double get_m_OpenClipTimeOffset_22() const { return ___m_OpenClipTimeOffset_22; }
	inline double* get_address_of_m_OpenClipTimeOffset_22() { return &___m_OpenClipTimeOffset_22; }
	inline void set_m_OpenClipTimeOffset_22(double value)
	{
		___m_OpenClipTimeOffset_22 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_23() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_MatchTargetFields_23)); }
	inline int32_t get_m_MatchTargetFields_23() const { return ___m_MatchTargetFields_23; }
	inline int32_t* get_address_of_m_MatchTargetFields_23() { return &___m_MatchTargetFields_23; }
	inline void set_m_MatchTargetFields_23(int32_t value)
	{
		___m_MatchTargetFields_23 = value;
	}

	inline static int32_t get_offset_of_m_Position_24() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_Position_24)); }
	inline Vector3_t1986933152  get_m_Position_24() const { return ___m_Position_24; }
	inline Vector3_t1986933152 * get_address_of_m_Position_24() { return &___m_Position_24; }
	inline void set_m_Position_24(Vector3_t1986933152  value)
	{
		___m_Position_24 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_25() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_Rotation_25)); }
	inline Quaternion_t704191599  get_m_Rotation_25() const { return ___m_Rotation_25; }
	inline Quaternion_t704191599 * get_address_of_m_Rotation_25() { return &___m_Rotation_25; }
	inline void set_m_Rotation_25(Quaternion_t704191599  value)
	{
		___m_Rotation_25 = value;
	}

	inline static int32_t get_offset_of_m_ApplyOffsets_26() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_ApplyOffsets_26)); }
	inline bool get_m_ApplyOffsets_26() const { return ___m_ApplyOffsets_26; }
	inline bool* get_address_of_m_ApplyOffsets_26() { return &___m_ApplyOffsets_26; }
	inline void set_m_ApplyOffsets_26(bool value)
	{
		___m_ApplyOffsets_26 = value;
	}

	inline static int32_t get_offset_of_m_AvatarMask_27() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_AvatarMask_27)); }
	inline AvatarMask_t2076757458 * get_m_AvatarMask_27() const { return ___m_AvatarMask_27; }
	inline AvatarMask_t2076757458 ** get_address_of_m_AvatarMask_27() { return &___m_AvatarMask_27; }
	inline void set_m_AvatarMask_27(AvatarMask_t2076757458 * value)
	{
		___m_AvatarMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AvatarMask_27), value);
	}

	inline static int32_t get_offset_of_m_ApplyAvatarMask_28() { return static_cast<int32_t>(offsetof(AnimationTrack_t200707209, ___m_ApplyAvatarMask_28)); }
	inline bool get_m_ApplyAvatarMask_28() const { return ___m_ApplyAvatarMask_28; }
	inline bool* get_address_of_m_ApplyAvatarMask_28() { return &___m_ApplyAvatarMask_28; }
	inline void set_m_ApplyAvatarMask_28(bool value)
	{
		___m_ApplyAvatarMask_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACK_T200707209_H
#ifndef POSTEFFECTSBASE_T1831076792_H
#define POSTEFFECTSBASE_T1831076792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_t1831076792  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_2;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_3;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_4;
	// System.Collections.Generic.List`1<UnityEngine.Material> UnityStandardAssets.ImageEffects.PostEffectsBase::createdMaterials
	List_1_t1934891917 * ___createdMaterials_5;

public:
	inline static int32_t get_offset_of_supportHDRTextures_2() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___supportHDRTextures_2)); }
	inline bool get_supportHDRTextures_2() const { return ___supportHDRTextures_2; }
	inline bool* get_address_of_supportHDRTextures_2() { return &___supportHDRTextures_2; }
	inline void set_supportHDRTextures_2(bool value)
	{
		___supportHDRTextures_2 = value;
	}

	inline static int32_t get_offset_of_supportDX11_3() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___supportDX11_3)); }
	inline bool get_supportDX11_3() const { return ___supportDX11_3; }
	inline bool* get_address_of_supportDX11_3() { return &___supportDX11_3; }
	inline void set_supportDX11_3(bool value)
	{
		___supportDX11_3 = value;
	}

	inline static int32_t get_offset_of_isSupported_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___isSupported_4)); }
	inline bool get_isSupported_4() const { return ___isSupported_4; }
	inline bool* get_address_of_isSupported_4() { return &___isSupported_4; }
	inline void set_isSupported_4(bool value)
	{
		___isSupported_4 = value;
	}

	inline static int32_t get_offset_of_createdMaterials_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_t1831076792, ___createdMaterials_5)); }
	inline List_1_t1934891917 * get_createdMaterials_5() const { return ___createdMaterials_5; }
	inline List_1_t1934891917 ** get_address_of_createdMaterials_5() { return &___createdMaterials_5; }
	inline void set_createdMaterials_5(List_1_t1934891917 * value)
	{
		___createdMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_T1831076792_H
#ifndef PLAYABLETRACK_T4089958788_H
#define PLAYABLETRACK_T4089958788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PlayableTrack
struct  PlayableTrack_t4089958788  : public TrackAsset_t2823685959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLETRACK_T4089958788_H
#ifndef CHECKCONTOLACTIVE_T3413127716_H
#define CHECKCONTOLACTIVE_T3413127716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheckContolActive
struct  CheckContolActive_t3413127716  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean CheckContolActive::bValidated
	bool ___bValidated_2;
	// UnityEngine.GameObject CheckContolActive::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_3;

public:
	inline static int32_t get_offset_of_bValidated_2() { return static_cast<int32_t>(offsetof(CheckContolActive_t3413127716, ___bValidated_2)); }
	inline bool get_bValidated_2() const { return ___bValidated_2; }
	inline bool* get_address_of_bValidated_2() { return &___bValidated_2; }
	inline void set_bValidated_2(bool value)
	{
		___bValidated_2 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_3() { return static_cast<int32_t>(offsetof(CheckContolActive_t3413127716, ___m_ObjectMobileController_3)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_3() const { return ___m_ObjectMobileController_3; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_3() { return &___m_ObjectMobileController_3; }
	inline void set_m_ObjectMobileController_3(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKCONTOLACTIVE_T3413127716_H
#ifndef BUTTONHANDLER_T2221835595_H
#define BUTTONHANDLER_T2221835595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t2221835595  : public MonoBehaviour_t1618594486
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(ButtonHandler_t2221835595, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T2221835595_H
#ifndef INPUTAXISSCROLLBAR_T681389185_H
#define INPUTAXISSCROLLBAR_T681389185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t681389185  : public MonoBehaviour_t1618594486
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_2;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t681389185, ___axis_2)); }
	inline String_t* get_axis_2() const { return ___axis_2; }
	inline String_t** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(String_t* value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T681389185_H
#ifndef JOYSTICK_T1263661182_H
#define JOYSTICK_T1263661182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t1263661182  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject UnityStandardAssets.CrossPlatformInput.Joystick::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_2;
	// UnityEngine.GameObject UnityStandardAssets.CrossPlatformInput.Joystick::m_ObjectMobileControllerMove
	GameObject_t2557347079 * ___m_ObjectMobileControllerMove_3;
	// UnityEngine.GameObject UnityStandardAssets.CrossPlatformInput.Joystick::m_ObjectMobileControllerSprint
	GameObject_t2557347079 * ___m_ObjectMobileControllerSprint_4;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.Joystick::m_controlOffset
	Vector2_t328513675  ___m_controlOffset_5;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_6;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_7;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_8;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_9;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t3366748972 * ___m_HorizontalVirtualAxis_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t3366748972 * ___m_VerticalVirtualAxis_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t1986933152  ___m_StartPos_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_14;
	// UnityEngine.RectTransform UnityStandardAssets.CrossPlatformInput.Joystick::rectTrans
	RectTransform_t859616204 * ___rectTrans_15;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::isLeft
	bool ___isLeft_16;

public:
	inline static int32_t get_offset_of_m_ObjectMobileController_2() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_ObjectMobileController_2)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_2() const { return ___m_ObjectMobileController_2; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_2() { return &___m_ObjectMobileController_2; }
	inline void set_m_ObjectMobileController_2(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_2), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerMove_3() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_ObjectMobileControllerMove_3)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerMove_3() const { return ___m_ObjectMobileControllerMove_3; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerMove_3() { return &___m_ObjectMobileControllerMove_3; }
	inline void set_m_ObjectMobileControllerMove_3(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerMove_3), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerSprint_4() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_ObjectMobileControllerSprint_4)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerSprint_4() const { return ___m_ObjectMobileControllerSprint_4; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerSprint_4() { return &___m_ObjectMobileControllerSprint_4; }
	inline void set_m_ObjectMobileControllerSprint_4(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerSprint_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerSprint_4), value);
	}

	inline static int32_t get_offset_of_m_controlOffset_5() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_controlOffset_5)); }
	inline Vector2_t328513675  get_m_controlOffset_5() const { return ___m_controlOffset_5; }
	inline Vector2_t328513675 * get_address_of_m_controlOffset_5() { return &___m_controlOffset_5; }
	inline void set_m_controlOffset_5(Vector2_t328513675  value)
	{
		___m_controlOffset_5 = value;
	}

	inline static int32_t get_offset_of_MovementRange_6() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___MovementRange_6)); }
	inline int32_t get_MovementRange_6() const { return ___MovementRange_6; }
	inline int32_t* get_address_of_MovementRange_6() { return &___MovementRange_6; }
	inline void set_MovementRange_6(int32_t value)
	{
		___MovementRange_6 = value;
	}

	inline static int32_t get_offset_of_axesToUse_7() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___axesToUse_7)); }
	inline int32_t get_axesToUse_7() const { return ___axesToUse_7; }
	inline int32_t* get_address_of_axesToUse_7() { return &___axesToUse_7; }
	inline void set_axesToUse_7(int32_t value)
	{
		___axesToUse_7 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_8() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___horizontalAxisName_8)); }
	inline String_t* get_horizontalAxisName_8() const { return ___horizontalAxisName_8; }
	inline String_t** get_address_of_horizontalAxisName_8() { return &___horizontalAxisName_8; }
	inline void set_horizontalAxisName_8(String_t* value)
	{
		___horizontalAxisName_8 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_8), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_9() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___verticalAxisName_9)); }
	inline String_t* get_verticalAxisName_9() const { return ___verticalAxisName_9; }
	inline String_t** get_address_of_verticalAxisName_9() { return &___verticalAxisName_9; }
	inline void set_verticalAxisName_9(String_t* value)
	{
		___verticalAxisName_9 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_9), value);
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_10() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_HorizontalVirtualAxis_10)); }
	inline VirtualAxis_t3366748972 * get_m_HorizontalVirtualAxis_10() const { return ___m_HorizontalVirtualAxis_10; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_HorizontalVirtualAxis_10() { return &___m_HorizontalVirtualAxis_10; }
	inline void set_m_HorizontalVirtualAxis_10(VirtualAxis_t3366748972 * value)
	{
		___m_HorizontalVirtualAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_10), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_VerticalVirtualAxis_11)); }
	inline VirtualAxis_t3366748972 * get_m_VerticalVirtualAxis_11() const { return ___m_VerticalVirtualAxis_11; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_VerticalVirtualAxis_11() { return &___m_VerticalVirtualAxis_11; }
	inline void set_m_VerticalVirtualAxis_11(VirtualAxis_t3366748972 * value)
	{
		___m_VerticalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_StartPos_12() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_StartPos_12)); }
	inline Vector3_t1986933152  get_m_StartPos_12() const { return ___m_StartPos_12; }
	inline Vector3_t1986933152 * get_address_of_m_StartPos_12() { return &___m_StartPos_12; }
	inline void set_m_StartPos_12(Vector3_t1986933152  value)
	{
		___m_StartPos_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_rectTrans_15() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___rectTrans_15)); }
	inline RectTransform_t859616204 * get_rectTrans_15() const { return ___rectTrans_15; }
	inline RectTransform_t859616204 ** get_address_of_rectTrans_15() { return &___rectTrans_15; }
	inline void set_rectTrans_15(RectTransform_t859616204 * value)
	{
		___rectTrans_15 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_15), value);
	}

	inline static int32_t get_offset_of_isLeft_16() { return static_cast<int32_t>(offsetof(Joystick_t1263661182, ___isLeft_16)); }
	inline bool get_isLeft_16() const { return ___isLeft_16; }
	inline bool* get_address_of_isLeft_16() { return &___isLeft_16; }
	inline void set_isLeft_16(bool value)
	{
		___isLeft_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T1263661182_H
#ifndef MOBILECONTROLRIG_T208031424_H
#define MOBILECONTROLRIG_T208031424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t208031424  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.MobileControlRig::moveImage
	Image_t2816987602 * ___moveImage_2;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.MobileControlRig::jumpImage
	Image_t2816987602 * ___jumpImage_3;
	// UnityEngine.Transform UnityStandardAssets.CrossPlatformInput.MobileControlRig::m_ObjectMobileControllerMove
	Transform_t362059596 * ___m_ObjectMobileControllerMove_4;
	// UnityEngine.Transform UnityStandardAssets.CrossPlatformInput.MobileControlRig::m_ObjectMobileControllerSprint
	Transform_t362059596 * ___m_ObjectMobileControllerSprint_5;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.MobileControlRig::m_ObjectMobileController_LeftPosition
	Vector3_t1986933152  ___m_ObjectMobileController_LeftPosition_6;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.MobileControlRig::m_ObjectMobileController_RightPosition
	Vector3_t1986933152  ___m_ObjectMobileController_RightPosition_7;

public:
	inline static int32_t get_offset_of_moveImage_2() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___moveImage_2)); }
	inline Image_t2816987602 * get_moveImage_2() const { return ___moveImage_2; }
	inline Image_t2816987602 ** get_address_of_moveImage_2() { return &___moveImage_2; }
	inline void set_moveImage_2(Image_t2816987602 * value)
	{
		___moveImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveImage_2), value);
	}

	inline static int32_t get_offset_of_jumpImage_3() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___jumpImage_3)); }
	inline Image_t2816987602 * get_jumpImage_3() const { return ___jumpImage_3; }
	inline Image_t2816987602 ** get_address_of_jumpImage_3() { return &___jumpImage_3; }
	inline void set_jumpImage_3(Image_t2816987602 * value)
	{
		___jumpImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___jumpImage_3), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerMove_4() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___m_ObjectMobileControllerMove_4)); }
	inline Transform_t362059596 * get_m_ObjectMobileControllerMove_4() const { return ___m_ObjectMobileControllerMove_4; }
	inline Transform_t362059596 ** get_address_of_m_ObjectMobileControllerMove_4() { return &___m_ObjectMobileControllerMove_4; }
	inline void set_m_ObjectMobileControllerMove_4(Transform_t362059596 * value)
	{
		___m_ObjectMobileControllerMove_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerMove_4), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerSprint_5() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___m_ObjectMobileControllerSprint_5)); }
	inline Transform_t362059596 * get_m_ObjectMobileControllerSprint_5() const { return ___m_ObjectMobileControllerSprint_5; }
	inline Transform_t362059596 ** get_address_of_m_ObjectMobileControllerSprint_5() { return &___m_ObjectMobileControllerSprint_5; }
	inline void set_m_ObjectMobileControllerSprint_5(Transform_t362059596 * value)
	{
		___m_ObjectMobileControllerSprint_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerSprint_5), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_LeftPosition_6() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___m_ObjectMobileController_LeftPosition_6)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_LeftPosition_6() const { return ___m_ObjectMobileController_LeftPosition_6; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_LeftPosition_6() { return &___m_ObjectMobileController_LeftPosition_6; }
	inline void set_m_ObjectMobileController_LeftPosition_6(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_LeftPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_RightPosition_7() { return static_cast<int32_t>(offsetof(MobileControlRig_t208031424, ___m_ObjectMobileController_RightPosition_7)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_RightPosition_7() const { return ___m_ObjectMobileController_RightPosition_7; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_RightPosition_7() { return &___m_ObjectMobileController_RightPosition_7; }
	inline void set_m_ObjectMobileController_RightPosition_7(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_RightPosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T208031424_H
#ifndef TILTINPUT_T2714299038_H
#define TILTINPUT_T2714299038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t2714299038  : public MonoBehaviour_t1618594486
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t2276377189 * ___mapping_2;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_5;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t3366748972 * ___m_SteerAxis_6;

public:
	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(TiltInput_t2714299038, ___mapping_2)); }
	inline AxisMapping_t2276377189 * get_mapping_2() const { return ___mapping_2; }
	inline AxisMapping_t2276377189 ** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(AxisMapping_t2276377189 * value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_3() { return static_cast<int32_t>(offsetof(TiltInput_t2714299038, ___tiltAroundAxis_3)); }
	inline int32_t get_tiltAroundAxis_3() const { return ___tiltAroundAxis_3; }
	inline int32_t* get_address_of_tiltAroundAxis_3() { return &___tiltAroundAxis_3; }
	inline void set_tiltAroundAxis_3(int32_t value)
	{
		___tiltAroundAxis_3 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_4() { return static_cast<int32_t>(offsetof(TiltInput_t2714299038, ___fullTiltAngle_4)); }
	inline float get_fullTiltAngle_4() const { return ___fullTiltAngle_4; }
	inline float* get_address_of_fullTiltAngle_4() { return &___fullTiltAngle_4; }
	inline void set_fullTiltAngle_4(float value)
	{
		___fullTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_5() { return static_cast<int32_t>(offsetof(TiltInput_t2714299038, ___centreAngleOffset_5)); }
	inline float get_centreAngleOffset_5() const { return ___centreAngleOffset_5; }
	inline float* get_address_of_centreAngleOffset_5() { return &___centreAngleOffset_5; }
	inline void set_centreAngleOffset_5(float value)
	{
		___centreAngleOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_6() { return static_cast<int32_t>(offsetof(TiltInput_t2714299038, ___m_SteerAxis_6)); }
	inline VirtualAxis_t3366748972 * get_m_SteerAxis_6() const { return ___m_SteerAxis_6; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_SteerAxis_6() { return &___m_SteerAxis_6; }
	inline void set_m_SteerAxis_6(VirtualAxis_t3366748972 * value)
	{
		___m_SteerAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T2714299038_H
#ifndef TOUCHPAD_T372495057_H
#define TOUCHPAD_T372495057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t372495057  : public MonoBehaviour_t1618594486
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_2;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_3;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t1986933152  ___m_StartPos_8;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t328513675  ___m_PreviousDelta_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t1986933152  ___m_JoytickOutput_10;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_11;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_12;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t3366748972 * ___m_HorizontalVirtualAxis_13;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t3366748972 * ___m_VerticalVirtualAxis_14;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_15;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_16;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t328513675  ___m_PreviousTouchPos_17;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t1986933152  ___m_Center_18;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2816987602 * ___m_Image_19;

public:
	inline static int32_t get_offset_of_axesToUse_2() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___axesToUse_2)); }
	inline int32_t get_axesToUse_2() const { return ___axesToUse_2; }
	inline int32_t* get_address_of_axesToUse_2() { return &___axesToUse_2; }
	inline void set_axesToUse_2(int32_t value)
	{
		___axesToUse_2 = value;
	}

	inline static int32_t get_offset_of_controlStyle_3() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___controlStyle_3)); }
	inline int32_t get_controlStyle_3() const { return ___controlStyle_3; }
	inline int32_t* get_address_of_controlStyle_3() { return &___controlStyle_3; }
	inline void set_controlStyle_3(int32_t value)
	{
		___controlStyle_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_6() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___Xsensitivity_6)); }
	inline float get_Xsensitivity_6() const { return ___Xsensitivity_6; }
	inline float* get_address_of_Xsensitivity_6() { return &___Xsensitivity_6; }
	inline void set_Xsensitivity_6(float value)
	{
		___Xsensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_7() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___Ysensitivity_7)); }
	inline float get_Ysensitivity_7() const { return ___Ysensitivity_7; }
	inline float* get_address_of_Ysensitivity_7() { return &___Ysensitivity_7; }
	inline void set_Ysensitivity_7(float value)
	{
		___Ysensitivity_7 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_StartPos_8)); }
	inline Vector3_t1986933152  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t1986933152 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t1986933152  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_9() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_PreviousDelta_9)); }
	inline Vector2_t328513675  get_m_PreviousDelta_9() const { return ___m_PreviousDelta_9; }
	inline Vector2_t328513675 * get_address_of_m_PreviousDelta_9() { return &___m_PreviousDelta_9; }
	inline void set_m_PreviousDelta_9(Vector2_t328513675  value)
	{
		___m_PreviousDelta_9 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_10() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_JoytickOutput_10)); }
	inline Vector3_t1986933152  get_m_JoytickOutput_10() const { return ___m_JoytickOutput_10; }
	inline Vector3_t1986933152 * get_address_of_m_JoytickOutput_10() { return &___m_JoytickOutput_10; }
	inline void set_m_JoytickOutput_10(Vector3_t1986933152  value)
	{
		___m_JoytickOutput_10 = value;
	}

	inline static int32_t get_offset_of_m_UseX_11() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_UseX_11)); }
	inline bool get_m_UseX_11() const { return ___m_UseX_11; }
	inline bool* get_address_of_m_UseX_11() { return &___m_UseX_11; }
	inline void set_m_UseX_11(bool value)
	{
		___m_UseX_11 = value;
	}

	inline static int32_t get_offset_of_m_UseY_12() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_UseY_12)); }
	inline bool get_m_UseY_12() const { return ___m_UseY_12; }
	inline bool* get_address_of_m_UseY_12() { return &___m_UseY_12; }
	inline void set_m_UseY_12(bool value)
	{
		___m_UseY_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_13() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_HorizontalVirtualAxis_13)); }
	inline VirtualAxis_t3366748972 * get_m_HorizontalVirtualAxis_13() const { return ___m_HorizontalVirtualAxis_13; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_HorizontalVirtualAxis_13() { return &___m_HorizontalVirtualAxis_13; }
	inline void set_m_HorizontalVirtualAxis_13(VirtualAxis_t3366748972 * value)
	{
		___m_HorizontalVirtualAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_14() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_VerticalVirtualAxis_14)); }
	inline VirtualAxis_t3366748972 * get_m_VerticalVirtualAxis_14() const { return ___m_VerticalVirtualAxis_14; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_VerticalVirtualAxis_14() { return &___m_VerticalVirtualAxis_14; }
	inline void set_m_VerticalVirtualAxis_14(VirtualAxis_t3366748972 * value)
	{
		___m_VerticalVirtualAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_14), value);
	}

	inline static int32_t get_offset_of_m_Dragging_15() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_Dragging_15)); }
	inline bool get_m_Dragging_15() const { return ___m_Dragging_15; }
	inline bool* get_address_of_m_Dragging_15() { return &___m_Dragging_15; }
	inline void set_m_Dragging_15(bool value)
	{
		___m_Dragging_15 = value;
	}

	inline static int32_t get_offset_of_m_Id_16() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_Id_16)); }
	inline int32_t get_m_Id_16() const { return ___m_Id_16; }
	inline int32_t* get_address_of_m_Id_16() { return &___m_Id_16; }
	inline void set_m_Id_16(int32_t value)
	{
		___m_Id_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_17() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_PreviousTouchPos_17)); }
	inline Vector2_t328513675  get_m_PreviousTouchPos_17() const { return ___m_PreviousTouchPos_17; }
	inline Vector2_t328513675 * get_address_of_m_PreviousTouchPos_17() { return &___m_PreviousTouchPos_17; }
	inline void set_m_PreviousTouchPos_17(Vector2_t328513675  value)
	{
		___m_PreviousTouchPos_17 = value;
	}

	inline static int32_t get_offset_of_m_Center_18() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_Center_18)); }
	inline Vector3_t1986933152  get_m_Center_18() const { return ___m_Center_18; }
	inline Vector3_t1986933152 * get_address_of_m_Center_18() { return &___m_Center_18; }
	inline void set_m_Center_18(Vector3_t1986933152  value)
	{
		___m_Center_18 = value;
	}

	inline static int32_t get_offset_of_m_Image_19() { return static_cast<int32_t>(offsetof(TouchPad_t372495057, ___m_Image_19)); }
	inline Image_t2816987602 * get_m_Image_19() const { return ___m_Image_19; }
	inline Image_t2816987602 ** get_address_of_m_Image_19() { return &___m_Image_19; }
	inline void set_m_Image_19(Image_t2816987602 * value)
	{
		___m_Image_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T372495057_H
#ifndef AXISTOUCHBUTTON_T3269831460_H
#define AXISTOUCHBUTTON_T3269831460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3269831460  : public MonoBehaviour_t1618594486
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_2;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_5;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3269831460 * ___m_PairedWith_6;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t3366748972 * ___m_Axis_7;

public:
	inline static int32_t get_offset_of_axisName_2() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___axisName_2)); }
	inline String_t* get_axisName_2() const { return ___axisName_2; }
	inline String_t** get_address_of_axisName_2() { return &___axisName_2; }
	inline void set_axisName_2(String_t* value)
	{
		___axisName_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_2), value);
	}

	inline static int32_t get_offset_of_axisValue_3() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___axisValue_3)); }
	inline float get_axisValue_3() const { return ___axisValue_3; }
	inline float* get_address_of_axisValue_3() { return &___axisValue_3; }
	inline void set_axisValue_3(float value)
	{
		___axisValue_3 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___responseSpeed_4)); }
	inline float get_responseSpeed_4() const { return ___responseSpeed_4; }
	inline float* get_address_of_responseSpeed_4() { return &___responseSpeed_4; }
	inline void set_responseSpeed_4(float value)
	{
		___responseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___returnToCentreSpeed_5)); }
	inline float get_returnToCentreSpeed_5() const { return ___returnToCentreSpeed_5; }
	inline float* get_address_of_returnToCentreSpeed_5() { return &___returnToCentreSpeed_5; }
	inline void set_returnToCentreSpeed_5(float value)
	{
		___returnToCentreSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___m_PairedWith_6)); }
	inline AxisTouchButton_t3269831460 * get_m_PairedWith_6() const { return ___m_PairedWith_6; }
	inline AxisTouchButton_t3269831460 ** get_address_of_m_PairedWith_6() { return &___m_PairedWith_6; }
	inline void set_m_PairedWith_6(AxisTouchButton_t3269831460 * value)
	{
		___m_PairedWith_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_6), value);
	}

	inline static int32_t get_offset_of_m_Axis_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3269831460, ___m_Axis_7)); }
	inline VirtualAxis_t3366748972 * get_m_Axis_7() const { return ___m_Axis_7; }
	inline VirtualAxis_t3366748972 ** get_address_of_m_Axis_7() { return &___m_Axis_7; }
	inline void set_m_Axis_7(VirtualAxis_t3366748972 * value)
	{
		___m_Axis_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3269831460_H
#ifndef CONTRASTSTRETCH_T1477452256_H
#define CONTRASTSTRETCH_T1477452256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ContrastStretch
struct  ContrastStretch_t1477452256  : public MonoBehaviour_t1618594486
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::adaptationSpeed
	float ___adaptationSpeed_2;
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::limitMinimum
	float ___limitMinimum_3;
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::limitMaximum
	float ___limitMaximum_4;
	// UnityEngine.RenderTexture[] UnityStandardAssets.ImageEffects.ContrastStretch::adaptRenderTex
	RenderTextureU5BU5D_t1249652403* ___adaptRenderTex_5;
	// System.Int32 UnityStandardAssets.ImageEffects.ContrastStretch::curAdaptIndex
	int32_t ___curAdaptIndex_6;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderLum
	Shader_t1881769421 * ___shaderLum_7;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialLum
	Material_t2815264910 * ___m_materialLum_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderReduce
	Shader_t1881769421 * ___shaderReduce_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialReduce
	Material_t2815264910 * ___m_materialReduce_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderAdapt
	Shader_t1881769421 * ___shaderAdapt_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialAdapt
	Material_t2815264910 * ___m_materialAdapt_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderApply
	Shader_t1881769421 * ___shaderApply_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialApply
	Material_t2815264910 * ___m_materialApply_14;

public:
	inline static int32_t get_offset_of_adaptationSpeed_2() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___adaptationSpeed_2)); }
	inline float get_adaptationSpeed_2() const { return ___adaptationSpeed_2; }
	inline float* get_address_of_adaptationSpeed_2() { return &___adaptationSpeed_2; }
	inline void set_adaptationSpeed_2(float value)
	{
		___adaptationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_limitMinimum_3() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___limitMinimum_3)); }
	inline float get_limitMinimum_3() const { return ___limitMinimum_3; }
	inline float* get_address_of_limitMinimum_3() { return &___limitMinimum_3; }
	inline void set_limitMinimum_3(float value)
	{
		___limitMinimum_3 = value;
	}

	inline static int32_t get_offset_of_limitMaximum_4() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___limitMaximum_4)); }
	inline float get_limitMaximum_4() const { return ___limitMaximum_4; }
	inline float* get_address_of_limitMaximum_4() { return &___limitMaximum_4; }
	inline void set_limitMaximum_4(float value)
	{
		___limitMaximum_4 = value;
	}

	inline static int32_t get_offset_of_adaptRenderTex_5() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___adaptRenderTex_5)); }
	inline RenderTextureU5BU5D_t1249652403* get_adaptRenderTex_5() const { return ___adaptRenderTex_5; }
	inline RenderTextureU5BU5D_t1249652403** get_address_of_adaptRenderTex_5() { return &___adaptRenderTex_5; }
	inline void set_adaptRenderTex_5(RenderTextureU5BU5D_t1249652403* value)
	{
		___adaptRenderTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___adaptRenderTex_5), value);
	}

	inline static int32_t get_offset_of_curAdaptIndex_6() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___curAdaptIndex_6)); }
	inline int32_t get_curAdaptIndex_6() const { return ___curAdaptIndex_6; }
	inline int32_t* get_address_of_curAdaptIndex_6() { return &___curAdaptIndex_6; }
	inline void set_curAdaptIndex_6(int32_t value)
	{
		___curAdaptIndex_6 = value;
	}

	inline static int32_t get_offset_of_shaderLum_7() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___shaderLum_7)); }
	inline Shader_t1881769421 * get_shaderLum_7() const { return ___shaderLum_7; }
	inline Shader_t1881769421 ** get_address_of_shaderLum_7() { return &___shaderLum_7; }
	inline void set_shaderLum_7(Shader_t1881769421 * value)
	{
		___shaderLum_7 = value;
		Il2CppCodeGenWriteBarrier((&___shaderLum_7), value);
	}

	inline static int32_t get_offset_of_m_materialLum_8() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___m_materialLum_8)); }
	inline Material_t2815264910 * get_m_materialLum_8() const { return ___m_materialLum_8; }
	inline Material_t2815264910 ** get_address_of_m_materialLum_8() { return &___m_materialLum_8; }
	inline void set_m_materialLum_8(Material_t2815264910 * value)
	{
		___m_materialLum_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialLum_8), value);
	}

	inline static int32_t get_offset_of_shaderReduce_9() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___shaderReduce_9)); }
	inline Shader_t1881769421 * get_shaderReduce_9() const { return ___shaderReduce_9; }
	inline Shader_t1881769421 ** get_address_of_shaderReduce_9() { return &___shaderReduce_9; }
	inline void set_shaderReduce_9(Shader_t1881769421 * value)
	{
		___shaderReduce_9 = value;
		Il2CppCodeGenWriteBarrier((&___shaderReduce_9), value);
	}

	inline static int32_t get_offset_of_m_materialReduce_10() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___m_materialReduce_10)); }
	inline Material_t2815264910 * get_m_materialReduce_10() const { return ___m_materialReduce_10; }
	inline Material_t2815264910 ** get_address_of_m_materialReduce_10() { return &___m_materialReduce_10; }
	inline void set_m_materialReduce_10(Material_t2815264910 * value)
	{
		___m_materialReduce_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReduce_10), value);
	}

	inline static int32_t get_offset_of_shaderAdapt_11() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___shaderAdapt_11)); }
	inline Shader_t1881769421 * get_shaderAdapt_11() const { return ___shaderAdapt_11; }
	inline Shader_t1881769421 ** get_address_of_shaderAdapt_11() { return &___shaderAdapt_11; }
	inline void set_shaderAdapt_11(Shader_t1881769421 * value)
	{
		___shaderAdapt_11 = value;
		Il2CppCodeGenWriteBarrier((&___shaderAdapt_11), value);
	}

	inline static int32_t get_offset_of_m_materialAdapt_12() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___m_materialAdapt_12)); }
	inline Material_t2815264910 * get_m_materialAdapt_12() const { return ___m_materialAdapt_12; }
	inline Material_t2815264910 ** get_address_of_m_materialAdapt_12() { return &___m_materialAdapt_12; }
	inline void set_m_materialAdapt_12(Material_t2815264910 * value)
	{
		___m_materialAdapt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialAdapt_12), value);
	}

	inline static int32_t get_offset_of_shaderApply_13() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___shaderApply_13)); }
	inline Shader_t1881769421 * get_shaderApply_13() const { return ___shaderApply_13; }
	inline Shader_t1881769421 ** get_address_of_shaderApply_13() { return &___shaderApply_13; }
	inline void set_shaderApply_13(Shader_t1881769421 * value)
	{
		___shaderApply_13 = value;
		Il2CppCodeGenWriteBarrier((&___shaderApply_13), value);
	}

	inline static int32_t get_offset_of_m_materialApply_14() { return static_cast<int32_t>(offsetof(ContrastStretch_t1477452256, ___m_materialApply_14)); }
	inline Material_t2815264910 * get_m_materialApply_14() const { return ___m_materialApply_14; }
	inline Material_t2815264910 ** get_address_of_m_materialApply_14() { return &___m_materialApply_14; }
	inline void set_m_materialApply_14(Material_t2815264910 * value)
	{
		___m_materialApply_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialApply_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTSTRETCH_T1477452256_H
#ifndef BLUR_T2292599283_H
#define BLUR_T2292599283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Blur
struct  Blur_t2292599283  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Blur::iterations
	int32_t ___iterations_2;
	// System.Single UnityStandardAssets.ImageEffects.Blur::blurSpread
	float ___blurSpread_3;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Blur::blurShader
	Shader_t1881769421 * ___blurShader_4;

public:
	inline static int32_t get_offset_of_iterations_2() { return static_cast<int32_t>(offsetof(Blur_t2292599283, ___iterations_2)); }
	inline int32_t get_iterations_2() const { return ___iterations_2; }
	inline int32_t* get_address_of_iterations_2() { return &___iterations_2; }
	inline void set_iterations_2(int32_t value)
	{
		___iterations_2 = value;
	}

	inline static int32_t get_offset_of_blurSpread_3() { return static_cast<int32_t>(offsetof(Blur_t2292599283, ___blurSpread_3)); }
	inline float get_blurSpread_3() const { return ___blurSpread_3; }
	inline float* get_address_of_blurSpread_3() { return &___blurSpread_3; }
	inline void set_blurSpread_3(float value)
	{
		___blurSpread_3 = value;
	}

	inline static int32_t get_offset_of_blurShader_4() { return static_cast<int32_t>(offsetof(Blur_t2292599283, ___blurShader_4)); }
	inline Shader_t1881769421 * get_blurShader_4() const { return ___blurShader_4; }
	inline Shader_t1881769421 ** get_address_of_blurShader_4() { return &___blurShader_4; }
	inline void set_blurShader_4(Shader_t1881769421 * value)
	{
		___blurShader_4 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_4), value);
	}
};

struct Blur_t2292599283_StaticFields
{
public:
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::m_Material
	Material_t2815264910 * ___m_Material_5;

public:
	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(Blur_t2292599283_StaticFields, ___m_Material_5)); }
	inline Material_t2815264910 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t2815264910 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t2815264910 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUR_T2292599283_H
#ifndef BLUROPTIMIZED_T3587832534_H
#define BLUROPTIMIZED_T3587832534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BlurOptimized
struct  BlurOptimized_t3587832534  : public PostEffectsBase_t1831076792
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized::downsample
	int32_t ___downsample_6;
	// System.Single UnityStandardAssets.ImageEffects.BlurOptimized::blurSize
	float ___blurSize_7;
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized::blurIterations
	int32_t ___blurIterations_8;
	// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType UnityStandardAssets.ImageEffects.BlurOptimized::blurType
	int32_t ___blurType_9;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BlurOptimized::blurShader
	Shader_t1881769421 * ___blurShader_10;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BlurOptimized::blurMaterial
	Material_t2815264910 * ___blurMaterial_11;

public:
	inline static int32_t get_offset_of_downsample_6() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___downsample_6)); }
	inline int32_t get_downsample_6() const { return ___downsample_6; }
	inline int32_t* get_address_of_downsample_6() { return &___downsample_6; }
	inline void set_downsample_6(int32_t value)
	{
		___downsample_6 = value;
	}

	inline static int32_t get_offset_of_blurSize_7() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___blurSize_7)); }
	inline float get_blurSize_7() const { return ___blurSize_7; }
	inline float* get_address_of_blurSize_7() { return &___blurSize_7; }
	inline void set_blurSize_7(float value)
	{
		___blurSize_7 = value;
	}

	inline static int32_t get_offset_of_blurIterations_8() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___blurIterations_8)); }
	inline int32_t get_blurIterations_8() const { return ___blurIterations_8; }
	inline int32_t* get_address_of_blurIterations_8() { return &___blurIterations_8; }
	inline void set_blurIterations_8(int32_t value)
	{
		___blurIterations_8 = value;
	}

	inline static int32_t get_offset_of_blurType_9() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___blurType_9)); }
	inline int32_t get_blurType_9() const { return ___blurType_9; }
	inline int32_t* get_address_of_blurType_9() { return &___blurType_9; }
	inline void set_blurType_9(int32_t value)
	{
		___blurType_9 = value;
	}

	inline static int32_t get_offset_of_blurShader_10() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___blurShader_10)); }
	inline Shader_t1881769421 * get_blurShader_10() const { return ___blurShader_10; }
	inline Shader_t1881769421 ** get_address_of_blurShader_10() { return &___blurShader_10; }
	inline void set_blurShader_10(Shader_t1881769421 * value)
	{
		___blurShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_10), value);
	}

	inline static int32_t get_offset_of_blurMaterial_11() { return static_cast<int32_t>(offsetof(BlurOptimized_t3587832534, ___blurMaterial_11)); }
	inline Material_t2815264910 * get_blurMaterial_11() const { return ___blurMaterial_11; }
	inline Material_t2815264910 ** get_address_of_blurMaterial_11() { return &___blurMaterial_11; }
	inline void set_blurMaterial_11(Material_t2815264910 * value)
	{
		___blurMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___blurMaterial_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUROPTIMIZED_T3587832534_H
#ifndef COLORCORRECTIONLOOKUP_T4097657427_H
#define COLORCORRECTIONLOOKUP_T4097657427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
struct  ColorCorrectionLookup_t4097657427  : public PostEffectsBase_t1831076792
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionLookup::shader
	Shader_t1881769421 * ___shader_6;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionLookup::material
	Material_t2815264910 * ___material_7;
	// UnityEngine.Texture3D UnityStandardAssets.ImageEffects.ColorCorrectionLookup::converted3DLut
	Texture3D_t2628067514 * ___converted3DLut_8;
	// System.String UnityStandardAssets.ImageEffects.ColorCorrectionLookup::basedOnTempTex
	String_t* ___basedOnTempTex_9;

public:
	inline static int32_t get_offset_of_shader_6() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t4097657427, ___shader_6)); }
	inline Shader_t1881769421 * get_shader_6() const { return ___shader_6; }
	inline Shader_t1881769421 ** get_address_of_shader_6() { return &___shader_6; }
	inline void set_shader_6(Shader_t1881769421 * value)
	{
		___shader_6 = value;
		Il2CppCodeGenWriteBarrier((&___shader_6), value);
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t4097657427, ___material_7)); }
	inline Material_t2815264910 * get_material_7() const { return ___material_7; }
	inline Material_t2815264910 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t2815264910 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_converted3DLut_8() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t4097657427, ___converted3DLut_8)); }
	inline Texture3D_t2628067514 * get_converted3DLut_8() const { return ___converted3DLut_8; }
	inline Texture3D_t2628067514 ** get_address_of_converted3DLut_8() { return &___converted3DLut_8; }
	inline void set_converted3DLut_8(Texture3D_t2628067514 * value)
	{
		___converted3DLut_8 = value;
		Il2CppCodeGenWriteBarrier((&___converted3DLut_8), value);
	}

	inline static int32_t get_offset_of_basedOnTempTex_9() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t4097657427, ___basedOnTempTex_9)); }
	inline String_t* get_basedOnTempTex_9() const { return ___basedOnTempTex_9; }
	inline String_t** get_address_of_basedOnTempTex_9() { return &___basedOnTempTex_9; }
	inline void set_basedOnTempTex_9(String_t* value)
	{
		___basedOnTempTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___basedOnTempTex_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONLOOKUP_T4097657427_H
#ifndef COLORCORRECTIONRAMP_T3429778610_H
#define COLORCORRECTIONRAMP_T3429778610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
struct  ColorCorrectionRamp_t3429778610  : public ImageEffectBase_t4133330248
{
public:
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.ColorCorrectionRamp::textureRamp
	Texture_t2119925672 * ___textureRamp_4;

public:
	inline static int32_t get_offset_of_textureRamp_4() { return static_cast<int32_t>(offsetof(ColorCorrectionRamp_t3429778610, ___textureRamp_4)); }
	inline Texture_t2119925672 * get_textureRamp_4() const { return ___textureRamp_4; }
	inline Texture_t2119925672 ** get_address_of_textureRamp_4() { return &___textureRamp_4; }
	inline void set_textureRamp_4(Texture_t2119925672 * value)
	{
		___textureRamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___textureRamp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONRAMP_T3429778610_H
#ifndef CONTRASTENHANCE_T1749987403_H
#define CONTRASTENHANCE_T1749987403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ContrastEnhance
struct  ContrastEnhance_t1749987403  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::intensity
	float ___intensity_6;
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::threshold
	float ___threshold_7;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastEnhance::separableBlurMaterial
	Material_t2815264910 * ___separableBlurMaterial_8;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastEnhance::contrastCompositeMaterial
	Material_t2815264910 * ___contrastCompositeMaterial_9;
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::blurSpread
	float ___blurSpread_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastEnhance::separableBlurShader
	Shader_t1881769421 * ___separableBlurShader_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastEnhance::contrastCompositeShader
	Shader_t1881769421 * ___contrastCompositeShader_12;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_threshold_7() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___threshold_7)); }
	inline float get_threshold_7() const { return ___threshold_7; }
	inline float* get_address_of_threshold_7() { return &___threshold_7; }
	inline void set_threshold_7(float value)
	{
		___threshold_7 = value;
	}

	inline static int32_t get_offset_of_separableBlurMaterial_8() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___separableBlurMaterial_8)); }
	inline Material_t2815264910 * get_separableBlurMaterial_8() const { return ___separableBlurMaterial_8; }
	inline Material_t2815264910 ** get_address_of_separableBlurMaterial_8() { return &___separableBlurMaterial_8; }
	inline void set_separableBlurMaterial_8(Material_t2815264910 * value)
	{
		___separableBlurMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurMaterial_8), value);
	}

	inline static int32_t get_offset_of_contrastCompositeMaterial_9() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___contrastCompositeMaterial_9)); }
	inline Material_t2815264910 * get_contrastCompositeMaterial_9() const { return ___contrastCompositeMaterial_9; }
	inline Material_t2815264910 ** get_address_of_contrastCompositeMaterial_9() { return &___contrastCompositeMaterial_9; }
	inline void set_contrastCompositeMaterial_9(Material_t2815264910 * value)
	{
		___contrastCompositeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___contrastCompositeMaterial_9), value);
	}

	inline static int32_t get_offset_of_blurSpread_10() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___blurSpread_10)); }
	inline float get_blurSpread_10() const { return ___blurSpread_10; }
	inline float* get_address_of_blurSpread_10() { return &___blurSpread_10; }
	inline void set_blurSpread_10(float value)
	{
		___blurSpread_10 = value;
	}

	inline static int32_t get_offset_of_separableBlurShader_11() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___separableBlurShader_11)); }
	inline Shader_t1881769421 * get_separableBlurShader_11() const { return ___separableBlurShader_11; }
	inline Shader_t1881769421 ** get_address_of_separableBlurShader_11() { return &___separableBlurShader_11; }
	inline void set_separableBlurShader_11(Shader_t1881769421 * value)
	{
		___separableBlurShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_11), value);
	}

	inline static int32_t get_offset_of_contrastCompositeShader_12() { return static_cast<int32_t>(offsetof(ContrastEnhance_t1749987403, ___contrastCompositeShader_12)); }
	inline Shader_t1881769421 * get_contrastCompositeShader_12() const { return ___contrastCompositeShader_12; }
	inline Shader_t1881769421 ** get_address_of_contrastCompositeShader_12() { return &___contrastCompositeShader_12; }
	inline void set_contrastCompositeShader_12(Shader_t1881769421 * value)
	{
		___contrastCompositeShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___contrastCompositeShader_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTENHANCE_T1749987403_H
#ifndef CREASESHADING_T3763133077_H
#define CREASESHADING_T3763133077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CreaseShading
struct  CreaseShading_t3763133077  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.CreaseShading::intensity
	float ___intensity_6;
	// System.Int32 UnityStandardAssets.ImageEffects.CreaseShading::softness
	int32_t ___softness_7;
	// System.Single UnityStandardAssets.ImageEffects.CreaseShading::spread
	float ___spread_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::blurShader
	Shader_t1881769421 * ___blurShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::blurMaterial
	Material_t2815264910 * ___blurMaterial_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::depthFetchShader
	Shader_t1881769421 * ___depthFetchShader_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::depthFetchMaterial
	Material_t2815264910 * ___depthFetchMaterial_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::creaseApplyShader
	Shader_t1881769421 * ___creaseApplyShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::creaseApplyMaterial
	Material_t2815264910 * ___creaseApplyMaterial_14;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_softness_7() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___softness_7)); }
	inline int32_t get_softness_7() const { return ___softness_7; }
	inline int32_t* get_address_of_softness_7() { return &___softness_7; }
	inline void set_softness_7(int32_t value)
	{
		___softness_7 = value;
	}

	inline static int32_t get_offset_of_spread_8() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___spread_8)); }
	inline float get_spread_8() const { return ___spread_8; }
	inline float* get_address_of_spread_8() { return &___spread_8; }
	inline void set_spread_8(float value)
	{
		___spread_8 = value;
	}

	inline static int32_t get_offset_of_blurShader_9() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___blurShader_9)); }
	inline Shader_t1881769421 * get_blurShader_9() const { return ___blurShader_9; }
	inline Shader_t1881769421 ** get_address_of_blurShader_9() { return &___blurShader_9; }
	inline void set_blurShader_9(Shader_t1881769421 * value)
	{
		___blurShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_9), value);
	}

	inline static int32_t get_offset_of_blurMaterial_10() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___blurMaterial_10)); }
	inline Material_t2815264910 * get_blurMaterial_10() const { return ___blurMaterial_10; }
	inline Material_t2815264910 ** get_address_of_blurMaterial_10() { return &___blurMaterial_10; }
	inline void set_blurMaterial_10(Material_t2815264910 * value)
	{
		___blurMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___blurMaterial_10), value);
	}

	inline static int32_t get_offset_of_depthFetchShader_11() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___depthFetchShader_11)); }
	inline Shader_t1881769421 * get_depthFetchShader_11() const { return ___depthFetchShader_11; }
	inline Shader_t1881769421 ** get_address_of_depthFetchShader_11() { return &___depthFetchShader_11; }
	inline void set_depthFetchShader_11(Shader_t1881769421 * value)
	{
		___depthFetchShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___depthFetchShader_11), value);
	}

	inline static int32_t get_offset_of_depthFetchMaterial_12() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___depthFetchMaterial_12)); }
	inline Material_t2815264910 * get_depthFetchMaterial_12() const { return ___depthFetchMaterial_12; }
	inline Material_t2815264910 ** get_address_of_depthFetchMaterial_12() { return &___depthFetchMaterial_12; }
	inline void set_depthFetchMaterial_12(Material_t2815264910 * value)
	{
		___depthFetchMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___depthFetchMaterial_12), value);
	}

	inline static int32_t get_offset_of_creaseApplyShader_13() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___creaseApplyShader_13)); }
	inline Shader_t1881769421 * get_creaseApplyShader_13() const { return ___creaseApplyShader_13; }
	inline Shader_t1881769421 ** get_address_of_creaseApplyShader_13() { return &___creaseApplyShader_13; }
	inline void set_creaseApplyShader_13(Shader_t1881769421 * value)
	{
		___creaseApplyShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___creaseApplyShader_13), value);
	}

	inline static int32_t get_offset_of_creaseApplyMaterial_14() { return static_cast<int32_t>(offsetof(CreaseShading_t3763133077, ___creaseApplyMaterial_14)); }
	inline Material_t2815264910 * get_creaseApplyMaterial_14() const { return ___creaseApplyMaterial_14; }
	inline Material_t2815264910 ** get_address_of_creaseApplyMaterial_14() { return &___creaseApplyMaterial_14; }
	inline void set_creaseApplyMaterial_14(Material_t2815264910 * value)
	{
		___creaseApplyMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___creaseApplyMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREASESHADING_T3763133077_H
#ifndef DEPTHOFFIELDDEPRECATED_T4224030657_H
#define DEPTHOFFIELDDEPRECATED_T4224030657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
struct  DepthOfFieldDeprecated_t4224030657  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::quality
	int32_t ___quality_8;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::resolution
	int32_t ___resolution_9;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::simpleTweakMode
	bool ___simpleTweakMode_10;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalPoint
	float ___focalPoint_11;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::smoothness
	float ___smoothness_12;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZDistance
	float ___focalZDistance_13;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZStartCurve
	float ___focalZStartCurve_14;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZEndCurve
	float ___focalZEndCurve_15;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalStartCurve
	float ___focalStartCurve_16;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalEndCurve
	float ___focalEndCurve_17;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalDistance01
	float ___focalDistance01_18;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::objectFocus
	Transform_t362059596 * ___objectFocus_19;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalSize
	float ___focalSize_20;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bluriness
	int32_t ___bluriness_21;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::maxBlurSpread
	float ___maxBlurSpread_22;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::foregroundBlurExtrude
	float ___foregroundBlurExtrude_23;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofBlurShader
	Shader_t1881769421 * ___dofBlurShader_24;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofBlurMaterial
	Material_t2815264910 * ___dofBlurMaterial_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofShader
	Shader_t1881769421 * ___dofShader_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofMaterial
	Material_t2815264910 * ___dofMaterial_27;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::visualize
	bool ___visualize_28;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehDestination
	int32_t ___bokehDestination_29;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::widthOverHeight
	float ___widthOverHeight_30;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::oneOverBaseSize
	float ___oneOverBaseSize_31;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokeh
	bool ___bokeh_32;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSupport
	bool ___bokehSupport_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehShader
	Shader_t1881769421 * ___bokehShader_34;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehTexture
	Texture2D_t3063074017 * ___bokehTexture_35;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehScale
	float ___bokehScale_36;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehIntensity
	float ___bokehIntensity_37;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehThresholdContrast
	float ___bokehThresholdContrast_38;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehThresholdLuminance
	float ___bokehThresholdLuminance_39;
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehDownsample
	int32_t ___bokehDownsample_40;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehMaterial
	Material_t2815264910 * ___bokehMaterial_41;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::_camera
	Camera_t2839736942 * ____camera_42;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::foregroundTexture
	RenderTexture_t971269558 * ___foregroundTexture_43;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::mediumRezWorkTexture
	RenderTexture_t971269558 * ___mediumRezWorkTexture_44;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::finalDefocus
	RenderTexture_t971269558 * ___finalDefocus_45;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::lowRezWorkTexture
	RenderTexture_t971269558 * ___lowRezWorkTexture_46;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSource
	RenderTexture_t971269558 * ___bokehSource_47;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSource2
	RenderTexture_t971269558 * ___bokehSource2_48;

public:
	inline static int32_t get_offset_of_quality_8() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___quality_8)); }
	inline int32_t get_quality_8() const { return ___quality_8; }
	inline int32_t* get_address_of_quality_8() { return &___quality_8; }
	inline void set_quality_8(int32_t value)
	{
		___quality_8 = value;
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___resolution_9)); }
	inline int32_t get_resolution_9() const { return ___resolution_9; }
	inline int32_t* get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(int32_t value)
	{
		___resolution_9 = value;
	}

	inline static int32_t get_offset_of_simpleTweakMode_10() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___simpleTweakMode_10)); }
	inline bool get_simpleTweakMode_10() const { return ___simpleTweakMode_10; }
	inline bool* get_address_of_simpleTweakMode_10() { return &___simpleTweakMode_10; }
	inline void set_simpleTweakMode_10(bool value)
	{
		___simpleTweakMode_10 = value;
	}

	inline static int32_t get_offset_of_focalPoint_11() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalPoint_11)); }
	inline float get_focalPoint_11() const { return ___focalPoint_11; }
	inline float* get_address_of_focalPoint_11() { return &___focalPoint_11; }
	inline void set_focalPoint_11(float value)
	{
		___focalPoint_11 = value;
	}

	inline static int32_t get_offset_of_smoothness_12() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___smoothness_12)); }
	inline float get_smoothness_12() const { return ___smoothness_12; }
	inline float* get_address_of_smoothness_12() { return &___smoothness_12; }
	inline void set_smoothness_12(float value)
	{
		___smoothness_12 = value;
	}

	inline static int32_t get_offset_of_focalZDistance_13() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalZDistance_13)); }
	inline float get_focalZDistance_13() const { return ___focalZDistance_13; }
	inline float* get_address_of_focalZDistance_13() { return &___focalZDistance_13; }
	inline void set_focalZDistance_13(float value)
	{
		___focalZDistance_13 = value;
	}

	inline static int32_t get_offset_of_focalZStartCurve_14() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalZStartCurve_14)); }
	inline float get_focalZStartCurve_14() const { return ___focalZStartCurve_14; }
	inline float* get_address_of_focalZStartCurve_14() { return &___focalZStartCurve_14; }
	inline void set_focalZStartCurve_14(float value)
	{
		___focalZStartCurve_14 = value;
	}

	inline static int32_t get_offset_of_focalZEndCurve_15() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalZEndCurve_15)); }
	inline float get_focalZEndCurve_15() const { return ___focalZEndCurve_15; }
	inline float* get_address_of_focalZEndCurve_15() { return &___focalZEndCurve_15; }
	inline void set_focalZEndCurve_15(float value)
	{
		___focalZEndCurve_15 = value;
	}

	inline static int32_t get_offset_of_focalStartCurve_16() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalStartCurve_16)); }
	inline float get_focalStartCurve_16() const { return ___focalStartCurve_16; }
	inline float* get_address_of_focalStartCurve_16() { return &___focalStartCurve_16; }
	inline void set_focalStartCurve_16(float value)
	{
		___focalStartCurve_16 = value;
	}

	inline static int32_t get_offset_of_focalEndCurve_17() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalEndCurve_17)); }
	inline float get_focalEndCurve_17() const { return ___focalEndCurve_17; }
	inline float* get_address_of_focalEndCurve_17() { return &___focalEndCurve_17; }
	inline void set_focalEndCurve_17(float value)
	{
		___focalEndCurve_17 = value;
	}

	inline static int32_t get_offset_of_focalDistance01_18() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalDistance01_18)); }
	inline float get_focalDistance01_18() const { return ___focalDistance01_18; }
	inline float* get_address_of_focalDistance01_18() { return &___focalDistance01_18; }
	inline void set_focalDistance01_18(float value)
	{
		___focalDistance01_18 = value;
	}

	inline static int32_t get_offset_of_objectFocus_19() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___objectFocus_19)); }
	inline Transform_t362059596 * get_objectFocus_19() const { return ___objectFocus_19; }
	inline Transform_t362059596 ** get_address_of_objectFocus_19() { return &___objectFocus_19; }
	inline void set_objectFocus_19(Transform_t362059596 * value)
	{
		___objectFocus_19 = value;
		Il2CppCodeGenWriteBarrier((&___objectFocus_19), value);
	}

	inline static int32_t get_offset_of_focalSize_20() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___focalSize_20)); }
	inline float get_focalSize_20() const { return ___focalSize_20; }
	inline float* get_address_of_focalSize_20() { return &___focalSize_20; }
	inline void set_focalSize_20(float value)
	{
		___focalSize_20 = value;
	}

	inline static int32_t get_offset_of_bluriness_21() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bluriness_21)); }
	inline int32_t get_bluriness_21() const { return ___bluriness_21; }
	inline int32_t* get_address_of_bluriness_21() { return &___bluriness_21; }
	inline void set_bluriness_21(int32_t value)
	{
		___bluriness_21 = value;
	}

	inline static int32_t get_offset_of_maxBlurSpread_22() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___maxBlurSpread_22)); }
	inline float get_maxBlurSpread_22() const { return ___maxBlurSpread_22; }
	inline float* get_address_of_maxBlurSpread_22() { return &___maxBlurSpread_22; }
	inline void set_maxBlurSpread_22(float value)
	{
		___maxBlurSpread_22 = value;
	}

	inline static int32_t get_offset_of_foregroundBlurExtrude_23() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___foregroundBlurExtrude_23)); }
	inline float get_foregroundBlurExtrude_23() const { return ___foregroundBlurExtrude_23; }
	inline float* get_address_of_foregroundBlurExtrude_23() { return &___foregroundBlurExtrude_23; }
	inline void set_foregroundBlurExtrude_23(float value)
	{
		___foregroundBlurExtrude_23 = value;
	}

	inline static int32_t get_offset_of_dofBlurShader_24() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___dofBlurShader_24)); }
	inline Shader_t1881769421 * get_dofBlurShader_24() const { return ___dofBlurShader_24; }
	inline Shader_t1881769421 ** get_address_of_dofBlurShader_24() { return &___dofBlurShader_24; }
	inline void set_dofBlurShader_24(Shader_t1881769421 * value)
	{
		___dofBlurShader_24 = value;
		Il2CppCodeGenWriteBarrier((&___dofBlurShader_24), value);
	}

	inline static int32_t get_offset_of_dofBlurMaterial_25() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___dofBlurMaterial_25)); }
	inline Material_t2815264910 * get_dofBlurMaterial_25() const { return ___dofBlurMaterial_25; }
	inline Material_t2815264910 ** get_address_of_dofBlurMaterial_25() { return &___dofBlurMaterial_25; }
	inline void set_dofBlurMaterial_25(Material_t2815264910 * value)
	{
		___dofBlurMaterial_25 = value;
		Il2CppCodeGenWriteBarrier((&___dofBlurMaterial_25), value);
	}

	inline static int32_t get_offset_of_dofShader_26() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___dofShader_26)); }
	inline Shader_t1881769421 * get_dofShader_26() const { return ___dofShader_26; }
	inline Shader_t1881769421 ** get_address_of_dofShader_26() { return &___dofShader_26; }
	inline void set_dofShader_26(Shader_t1881769421 * value)
	{
		___dofShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___dofShader_26), value);
	}

	inline static int32_t get_offset_of_dofMaterial_27() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___dofMaterial_27)); }
	inline Material_t2815264910 * get_dofMaterial_27() const { return ___dofMaterial_27; }
	inline Material_t2815264910 ** get_address_of_dofMaterial_27() { return &___dofMaterial_27; }
	inline void set_dofMaterial_27(Material_t2815264910 * value)
	{
		___dofMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((&___dofMaterial_27), value);
	}

	inline static int32_t get_offset_of_visualize_28() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___visualize_28)); }
	inline bool get_visualize_28() const { return ___visualize_28; }
	inline bool* get_address_of_visualize_28() { return &___visualize_28; }
	inline void set_visualize_28(bool value)
	{
		___visualize_28 = value;
	}

	inline static int32_t get_offset_of_bokehDestination_29() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehDestination_29)); }
	inline int32_t get_bokehDestination_29() const { return ___bokehDestination_29; }
	inline int32_t* get_address_of_bokehDestination_29() { return &___bokehDestination_29; }
	inline void set_bokehDestination_29(int32_t value)
	{
		___bokehDestination_29 = value;
	}

	inline static int32_t get_offset_of_widthOverHeight_30() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___widthOverHeight_30)); }
	inline float get_widthOverHeight_30() const { return ___widthOverHeight_30; }
	inline float* get_address_of_widthOverHeight_30() { return &___widthOverHeight_30; }
	inline void set_widthOverHeight_30(float value)
	{
		___widthOverHeight_30 = value;
	}

	inline static int32_t get_offset_of_oneOverBaseSize_31() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___oneOverBaseSize_31)); }
	inline float get_oneOverBaseSize_31() const { return ___oneOverBaseSize_31; }
	inline float* get_address_of_oneOverBaseSize_31() { return &___oneOverBaseSize_31; }
	inline void set_oneOverBaseSize_31(float value)
	{
		___oneOverBaseSize_31 = value;
	}

	inline static int32_t get_offset_of_bokeh_32() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokeh_32)); }
	inline bool get_bokeh_32() const { return ___bokeh_32; }
	inline bool* get_address_of_bokeh_32() { return &___bokeh_32; }
	inline void set_bokeh_32(bool value)
	{
		___bokeh_32 = value;
	}

	inline static int32_t get_offset_of_bokehSupport_33() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehSupport_33)); }
	inline bool get_bokehSupport_33() const { return ___bokehSupport_33; }
	inline bool* get_address_of_bokehSupport_33() { return &___bokehSupport_33; }
	inline void set_bokehSupport_33(bool value)
	{
		___bokehSupport_33 = value;
	}

	inline static int32_t get_offset_of_bokehShader_34() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehShader_34)); }
	inline Shader_t1881769421 * get_bokehShader_34() const { return ___bokehShader_34; }
	inline Shader_t1881769421 ** get_address_of_bokehShader_34() { return &___bokehShader_34; }
	inline void set_bokehShader_34(Shader_t1881769421 * value)
	{
		___bokehShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___bokehShader_34), value);
	}

	inline static int32_t get_offset_of_bokehTexture_35() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehTexture_35)); }
	inline Texture2D_t3063074017 * get_bokehTexture_35() const { return ___bokehTexture_35; }
	inline Texture2D_t3063074017 ** get_address_of_bokehTexture_35() { return &___bokehTexture_35; }
	inline void set_bokehTexture_35(Texture2D_t3063074017 * value)
	{
		___bokehTexture_35 = value;
		Il2CppCodeGenWriteBarrier((&___bokehTexture_35), value);
	}

	inline static int32_t get_offset_of_bokehScale_36() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehScale_36)); }
	inline float get_bokehScale_36() const { return ___bokehScale_36; }
	inline float* get_address_of_bokehScale_36() { return &___bokehScale_36; }
	inline void set_bokehScale_36(float value)
	{
		___bokehScale_36 = value;
	}

	inline static int32_t get_offset_of_bokehIntensity_37() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehIntensity_37)); }
	inline float get_bokehIntensity_37() const { return ___bokehIntensity_37; }
	inline float* get_address_of_bokehIntensity_37() { return &___bokehIntensity_37; }
	inline void set_bokehIntensity_37(float value)
	{
		___bokehIntensity_37 = value;
	}

	inline static int32_t get_offset_of_bokehThresholdContrast_38() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehThresholdContrast_38)); }
	inline float get_bokehThresholdContrast_38() const { return ___bokehThresholdContrast_38; }
	inline float* get_address_of_bokehThresholdContrast_38() { return &___bokehThresholdContrast_38; }
	inline void set_bokehThresholdContrast_38(float value)
	{
		___bokehThresholdContrast_38 = value;
	}

	inline static int32_t get_offset_of_bokehThresholdLuminance_39() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehThresholdLuminance_39)); }
	inline float get_bokehThresholdLuminance_39() const { return ___bokehThresholdLuminance_39; }
	inline float* get_address_of_bokehThresholdLuminance_39() { return &___bokehThresholdLuminance_39; }
	inline void set_bokehThresholdLuminance_39(float value)
	{
		___bokehThresholdLuminance_39 = value;
	}

	inline static int32_t get_offset_of_bokehDownsample_40() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehDownsample_40)); }
	inline int32_t get_bokehDownsample_40() const { return ___bokehDownsample_40; }
	inline int32_t* get_address_of_bokehDownsample_40() { return &___bokehDownsample_40; }
	inline void set_bokehDownsample_40(int32_t value)
	{
		___bokehDownsample_40 = value;
	}

	inline static int32_t get_offset_of_bokehMaterial_41() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehMaterial_41)); }
	inline Material_t2815264910 * get_bokehMaterial_41() const { return ___bokehMaterial_41; }
	inline Material_t2815264910 ** get_address_of_bokehMaterial_41() { return &___bokehMaterial_41; }
	inline void set_bokehMaterial_41(Material_t2815264910 * value)
	{
		___bokehMaterial_41 = value;
		Il2CppCodeGenWriteBarrier((&___bokehMaterial_41), value);
	}

	inline static int32_t get_offset_of__camera_42() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ____camera_42)); }
	inline Camera_t2839736942 * get__camera_42() const { return ____camera_42; }
	inline Camera_t2839736942 ** get_address_of__camera_42() { return &____camera_42; }
	inline void set__camera_42(Camera_t2839736942 * value)
	{
		____camera_42 = value;
		Il2CppCodeGenWriteBarrier((&____camera_42), value);
	}

	inline static int32_t get_offset_of_foregroundTexture_43() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___foregroundTexture_43)); }
	inline RenderTexture_t971269558 * get_foregroundTexture_43() const { return ___foregroundTexture_43; }
	inline RenderTexture_t971269558 ** get_address_of_foregroundTexture_43() { return &___foregroundTexture_43; }
	inline void set_foregroundTexture_43(RenderTexture_t971269558 * value)
	{
		___foregroundTexture_43 = value;
		Il2CppCodeGenWriteBarrier((&___foregroundTexture_43), value);
	}

	inline static int32_t get_offset_of_mediumRezWorkTexture_44() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___mediumRezWorkTexture_44)); }
	inline RenderTexture_t971269558 * get_mediumRezWorkTexture_44() const { return ___mediumRezWorkTexture_44; }
	inline RenderTexture_t971269558 ** get_address_of_mediumRezWorkTexture_44() { return &___mediumRezWorkTexture_44; }
	inline void set_mediumRezWorkTexture_44(RenderTexture_t971269558 * value)
	{
		___mediumRezWorkTexture_44 = value;
		Il2CppCodeGenWriteBarrier((&___mediumRezWorkTexture_44), value);
	}

	inline static int32_t get_offset_of_finalDefocus_45() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___finalDefocus_45)); }
	inline RenderTexture_t971269558 * get_finalDefocus_45() const { return ___finalDefocus_45; }
	inline RenderTexture_t971269558 ** get_address_of_finalDefocus_45() { return &___finalDefocus_45; }
	inline void set_finalDefocus_45(RenderTexture_t971269558 * value)
	{
		___finalDefocus_45 = value;
		Il2CppCodeGenWriteBarrier((&___finalDefocus_45), value);
	}

	inline static int32_t get_offset_of_lowRezWorkTexture_46() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___lowRezWorkTexture_46)); }
	inline RenderTexture_t971269558 * get_lowRezWorkTexture_46() const { return ___lowRezWorkTexture_46; }
	inline RenderTexture_t971269558 ** get_address_of_lowRezWorkTexture_46() { return &___lowRezWorkTexture_46; }
	inline void set_lowRezWorkTexture_46(RenderTexture_t971269558 * value)
	{
		___lowRezWorkTexture_46 = value;
		Il2CppCodeGenWriteBarrier((&___lowRezWorkTexture_46), value);
	}

	inline static int32_t get_offset_of_bokehSource_47() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehSource_47)); }
	inline RenderTexture_t971269558 * get_bokehSource_47() const { return ___bokehSource_47; }
	inline RenderTexture_t971269558 ** get_address_of_bokehSource_47() { return &___bokehSource_47; }
	inline void set_bokehSource_47(RenderTexture_t971269558 * value)
	{
		___bokehSource_47 = value;
		Il2CppCodeGenWriteBarrier((&___bokehSource_47), value);
	}

	inline static int32_t get_offset_of_bokehSource2_48() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657, ___bokehSource2_48)); }
	inline RenderTexture_t971269558 * get_bokehSource2_48() const { return ___bokehSource2_48; }
	inline RenderTexture_t971269558 ** get_address_of_bokehSource2_48() { return &___bokehSource2_48; }
	inline void set_bokehSource2_48(RenderTexture_t971269558 * value)
	{
		___bokehSource2_48 = value;
		Il2CppCodeGenWriteBarrier((&___bokehSource2_48), value);
	}
};

struct DepthOfFieldDeprecated_t4224030657_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::SMOOTH_DOWNSAMPLE_PASS
	int32_t ___SMOOTH_DOWNSAMPLE_PASS_6;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BOKEH_EXTRA_BLUR
	float ___BOKEH_EXTRA_BLUR_7;

public:
	inline static int32_t get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657_StaticFields, ___SMOOTH_DOWNSAMPLE_PASS_6)); }
	inline int32_t get_SMOOTH_DOWNSAMPLE_PASS_6() const { return ___SMOOTH_DOWNSAMPLE_PASS_6; }
	inline int32_t* get_address_of_SMOOTH_DOWNSAMPLE_PASS_6() { return &___SMOOTH_DOWNSAMPLE_PASS_6; }
	inline void set_SMOOTH_DOWNSAMPLE_PASS_6(int32_t value)
	{
		___SMOOTH_DOWNSAMPLE_PASS_6 = value;
	}

	inline static int32_t get_offset_of_BOKEH_EXTRA_BLUR_7() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4224030657_StaticFields, ___BOKEH_EXTRA_BLUR_7)); }
	inline float get_BOKEH_EXTRA_BLUR_7() const { return ___BOKEH_EXTRA_BLUR_7; }
	inline float* get_address_of_BOKEH_EXTRA_BLUR_7() { return &___BOKEH_EXTRA_BLUR_7; }
	inline void set_BOKEH_EXTRA_BLUR_7(float value)
	{
		___BOKEH_EXTRA_BLUR_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDDEPRECATED_T4224030657_H
#ifndef BLOOMOPTIMIZED_T1465546345_H
#define BLOOMOPTIMIZED_T1465546345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized
struct  BloomOptimized_t1465546345  : public PostEffectsBase_t1831076792
{
public:
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::threshold
	float ___threshold_6;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::intensity
	float ___intensity_7;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::blurSize
	float ___blurSize_8;
	// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution UnityStandardAssets.ImageEffects.BloomOptimized::resolution
	int32_t ___resolution_9;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized::blurIterations
	int32_t ___blurIterations_10;
	// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType UnityStandardAssets.ImageEffects.BloomOptimized::blurType
	int32_t ___blurType_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomShader
	Shader_t1881769421 * ___fastBloomShader_12;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomMaterial
	Material_t2815264910 * ___fastBloomMaterial_13;

public:
	inline static int32_t get_offset_of_threshold_6() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___threshold_6)); }
	inline float get_threshold_6() const { return ___threshold_6; }
	inline float* get_address_of_threshold_6() { return &___threshold_6; }
	inline void set_threshold_6(float value)
	{
		___threshold_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_blurSize_8() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___blurSize_8)); }
	inline float get_blurSize_8() const { return ___blurSize_8; }
	inline float* get_address_of_blurSize_8() { return &___blurSize_8; }
	inline void set_blurSize_8(float value)
	{
		___blurSize_8 = value;
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___resolution_9)); }
	inline int32_t get_resolution_9() const { return ___resolution_9; }
	inline int32_t* get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(int32_t value)
	{
		___resolution_9 = value;
	}

	inline static int32_t get_offset_of_blurIterations_10() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___blurIterations_10)); }
	inline int32_t get_blurIterations_10() const { return ___blurIterations_10; }
	inline int32_t* get_address_of_blurIterations_10() { return &___blurIterations_10; }
	inline void set_blurIterations_10(int32_t value)
	{
		___blurIterations_10 = value;
	}

	inline static int32_t get_offset_of_blurType_11() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___blurType_11)); }
	inline int32_t get_blurType_11() const { return ___blurType_11; }
	inline int32_t* get_address_of_blurType_11() { return &___blurType_11; }
	inline void set_blurType_11(int32_t value)
	{
		___blurType_11 = value;
	}

	inline static int32_t get_offset_of_fastBloomShader_12() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___fastBloomShader_12)); }
	inline Shader_t1881769421 * get_fastBloomShader_12() const { return ___fastBloomShader_12; }
	inline Shader_t1881769421 ** get_address_of_fastBloomShader_12() { return &___fastBloomShader_12; }
	inline void set_fastBloomShader_12(Shader_t1881769421 * value)
	{
		___fastBloomShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomShader_12), value);
	}

	inline static int32_t get_offset_of_fastBloomMaterial_13() { return static_cast<int32_t>(offsetof(BloomOptimized_t1465546345, ___fastBloomMaterial_13)); }
	inline Material_t2815264910 * get_fastBloomMaterial_13() const { return ___fastBloomMaterial_13; }
	inline Material_t2815264910 ** get_address_of_fastBloomMaterial_13() { return &___fastBloomMaterial_13; }
	inline void set_fastBloomMaterial_13(Material_t2815264910 * value)
	{
		___fastBloomMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMOPTIMIZED_T1465546345_H
#ifndef BLOOMANDFLARES_T1406318507_H
#define BLOOMANDFLARES_T1406318507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomAndFlares
struct  BloomAndFlares_t1406318507  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.TweakMode34 UnityStandardAssets.ImageEffects.BloomAndFlares::tweakMode
	int32_t ___tweakMode_6;
	// UnityStandardAssets.ImageEffects.BloomScreenBlendMode UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityStandardAssets.ImageEffects.HDRBloomMode UnityStandardAssets.ImageEffects.BloomAndFlares::hdr
	int32_t ___hdr_8;
	// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::doHdr
	bool ___doHdr_9;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::sepBlurSpread
	float ___sepBlurSpread_10;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::useSrcAlphaAsMask
	float ___useSrcAlphaAsMask_11;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::bloomIntensity
	float ___bloomIntensity_12;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::bloomThreshold
	float ___bloomThreshold_13;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomAndFlares::bloomBlurIterations
	int32_t ___bloomBlurIterations_14;
	// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::lensflares
	bool ___lensflares_15;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlareBlurIterations
	int32_t ___hollywoodFlareBlurIterations_16;
	// UnityStandardAssets.ImageEffects.LensflareStyle34 UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareMode
	int32_t ___lensflareMode_17;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::hollyStretchWidth
	float ___hollyStretchWidth_18;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareIntensity
	float ___lensflareIntensity_19;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareThreshold
	float ___lensflareThreshold_20;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorA
	Color_t2582018970  ___flareColorA_21;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorB
	Color_t2582018970  ___flareColorB_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorC
	Color_t2582018970  ___flareColorC_23;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorD
	Color_t2582018970  ___flareColorD_24;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareVignetteMask
	Texture2D_t3063074017 * ___lensFlareVignetteMask_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareShader
	Shader_t1881769421 * ___lensFlareShader_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareMaterial
	Material_t2815264910 * ___lensFlareMaterial_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::vignetteShader
	Shader_t1881769421 * ___vignetteShader_28;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::vignetteMaterial
	Material_t2815264910 * ___vignetteMaterial_29;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::separableBlurShader
	Shader_t1881769421 * ___separableBlurShader_30;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::separableBlurMaterial
	Material_t2815264910 * ___separableBlurMaterial_31;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::addBrightStuffOneOneShader
	Shader_t1881769421 * ___addBrightStuffOneOneShader_32;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::addBrightStuffBlendOneOneMaterial
	Material_t2815264910 * ___addBrightStuffBlendOneOneMaterial_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlendShader
	Shader_t1881769421 * ___screenBlendShader_34;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlend
	Material_t2815264910 * ___screenBlend_35;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlaresShader
	Shader_t1881769421 * ___hollywoodFlaresShader_36;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlaresMaterial
	Material_t2815264910 * ___hollywoodFlaresMaterial_37;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::brightPassFilterShader
	Shader_t1881769421 * ___brightPassFilterShader_38;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::brightPassFilterMaterial
	Material_t2815264910 * ___brightPassFilterMaterial_39;

public:
	inline static int32_t get_offset_of_tweakMode_6() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___tweakMode_6)); }
	inline int32_t get_tweakMode_6() const { return ___tweakMode_6; }
	inline int32_t* get_address_of_tweakMode_6() { return &___tweakMode_6; }
	inline void set_tweakMode_6(int32_t value)
	{
		___tweakMode_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_hdr_8() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___hdr_8)); }
	inline int32_t get_hdr_8() const { return ___hdr_8; }
	inline int32_t* get_address_of_hdr_8() { return &___hdr_8; }
	inline void set_hdr_8(int32_t value)
	{
		___hdr_8 = value;
	}

	inline static int32_t get_offset_of_doHdr_9() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___doHdr_9)); }
	inline bool get_doHdr_9() const { return ___doHdr_9; }
	inline bool* get_address_of_doHdr_9() { return &___doHdr_9; }
	inline void set_doHdr_9(bool value)
	{
		___doHdr_9 = value;
	}

	inline static int32_t get_offset_of_sepBlurSpread_10() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___sepBlurSpread_10)); }
	inline float get_sepBlurSpread_10() const { return ___sepBlurSpread_10; }
	inline float* get_address_of_sepBlurSpread_10() { return &___sepBlurSpread_10; }
	inline void set_sepBlurSpread_10(float value)
	{
		___sepBlurSpread_10 = value;
	}

	inline static int32_t get_offset_of_useSrcAlphaAsMask_11() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___useSrcAlphaAsMask_11)); }
	inline float get_useSrcAlphaAsMask_11() const { return ___useSrcAlphaAsMask_11; }
	inline float* get_address_of_useSrcAlphaAsMask_11() { return &___useSrcAlphaAsMask_11; }
	inline void set_useSrcAlphaAsMask_11(float value)
	{
		___useSrcAlphaAsMask_11 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_12() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___bloomIntensity_12)); }
	inline float get_bloomIntensity_12() const { return ___bloomIntensity_12; }
	inline float* get_address_of_bloomIntensity_12() { return &___bloomIntensity_12; }
	inline void set_bloomIntensity_12(float value)
	{
		___bloomIntensity_12 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_13() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___bloomThreshold_13)); }
	inline float get_bloomThreshold_13() const { return ___bloomThreshold_13; }
	inline float* get_address_of_bloomThreshold_13() { return &___bloomThreshold_13; }
	inline void set_bloomThreshold_13(float value)
	{
		___bloomThreshold_13 = value;
	}

	inline static int32_t get_offset_of_bloomBlurIterations_14() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___bloomBlurIterations_14)); }
	inline int32_t get_bloomBlurIterations_14() const { return ___bloomBlurIterations_14; }
	inline int32_t* get_address_of_bloomBlurIterations_14() { return &___bloomBlurIterations_14; }
	inline void set_bloomBlurIterations_14(int32_t value)
	{
		___bloomBlurIterations_14 = value;
	}

	inline static int32_t get_offset_of_lensflares_15() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensflares_15)); }
	inline bool get_lensflares_15() const { return ___lensflares_15; }
	inline bool* get_address_of_lensflares_15() { return &___lensflares_15; }
	inline void set_lensflares_15(bool value)
	{
		___lensflares_15 = value;
	}

	inline static int32_t get_offset_of_hollywoodFlareBlurIterations_16() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___hollywoodFlareBlurIterations_16)); }
	inline int32_t get_hollywoodFlareBlurIterations_16() const { return ___hollywoodFlareBlurIterations_16; }
	inline int32_t* get_address_of_hollywoodFlareBlurIterations_16() { return &___hollywoodFlareBlurIterations_16; }
	inline void set_hollywoodFlareBlurIterations_16(int32_t value)
	{
		___hollywoodFlareBlurIterations_16 = value;
	}

	inline static int32_t get_offset_of_lensflareMode_17() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensflareMode_17)); }
	inline int32_t get_lensflareMode_17() const { return ___lensflareMode_17; }
	inline int32_t* get_address_of_lensflareMode_17() { return &___lensflareMode_17; }
	inline void set_lensflareMode_17(int32_t value)
	{
		___lensflareMode_17 = value;
	}

	inline static int32_t get_offset_of_hollyStretchWidth_18() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___hollyStretchWidth_18)); }
	inline float get_hollyStretchWidth_18() const { return ___hollyStretchWidth_18; }
	inline float* get_address_of_hollyStretchWidth_18() { return &___hollyStretchWidth_18; }
	inline void set_hollyStretchWidth_18(float value)
	{
		___hollyStretchWidth_18 = value;
	}

	inline static int32_t get_offset_of_lensflareIntensity_19() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensflareIntensity_19)); }
	inline float get_lensflareIntensity_19() const { return ___lensflareIntensity_19; }
	inline float* get_address_of_lensflareIntensity_19() { return &___lensflareIntensity_19; }
	inline void set_lensflareIntensity_19(float value)
	{
		___lensflareIntensity_19 = value;
	}

	inline static int32_t get_offset_of_lensflareThreshold_20() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensflareThreshold_20)); }
	inline float get_lensflareThreshold_20() const { return ___lensflareThreshold_20; }
	inline float* get_address_of_lensflareThreshold_20() { return &___lensflareThreshold_20; }
	inline void set_lensflareThreshold_20(float value)
	{
		___lensflareThreshold_20 = value;
	}

	inline static int32_t get_offset_of_flareColorA_21() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___flareColorA_21)); }
	inline Color_t2582018970  get_flareColorA_21() const { return ___flareColorA_21; }
	inline Color_t2582018970 * get_address_of_flareColorA_21() { return &___flareColorA_21; }
	inline void set_flareColorA_21(Color_t2582018970  value)
	{
		___flareColorA_21 = value;
	}

	inline static int32_t get_offset_of_flareColorB_22() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___flareColorB_22)); }
	inline Color_t2582018970  get_flareColorB_22() const { return ___flareColorB_22; }
	inline Color_t2582018970 * get_address_of_flareColorB_22() { return &___flareColorB_22; }
	inline void set_flareColorB_22(Color_t2582018970  value)
	{
		___flareColorB_22 = value;
	}

	inline static int32_t get_offset_of_flareColorC_23() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___flareColorC_23)); }
	inline Color_t2582018970  get_flareColorC_23() const { return ___flareColorC_23; }
	inline Color_t2582018970 * get_address_of_flareColorC_23() { return &___flareColorC_23; }
	inline void set_flareColorC_23(Color_t2582018970  value)
	{
		___flareColorC_23 = value;
	}

	inline static int32_t get_offset_of_flareColorD_24() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___flareColorD_24)); }
	inline Color_t2582018970  get_flareColorD_24() const { return ___flareColorD_24; }
	inline Color_t2582018970 * get_address_of_flareColorD_24() { return &___flareColorD_24; }
	inline void set_flareColorD_24(Color_t2582018970  value)
	{
		___flareColorD_24 = value;
	}

	inline static int32_t get_offset_of_lensFlareVignetteMask_25() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensFlareVignetteMask_25)); }
	inline Texture2D_t3063074017 * get_lensFlareVignetteMask_25() const { return ___lensFlareVignetteMask_25; }
	inline Texture2D_t3063074017 ** get_address_of_lensFlareVignetteMask_25() { return &___lensFlareVignetteMask_25; }
	inline void set_lensFlareVignetteMask_25(Texture2D_t3063074017 * value)
	{
		___lensFlareVignetteMask_25 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareVignetteMask_25), value);
	}

	inline static int32_t get_offset_of_lensFlareShader_26() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensFlareShader_26)); }
	inline Shader_t1881769421 * get_lensFlareShader_26() const { return ___lensFlareShader_26; }
	inline Shader_t1881769421 ** get_address_of_lensFlareShader_26() { return &___lensFlareShader_26; }
	inline void set_lensFlareShader_26(Shader_t1881769421 * value)
	{
		___lensFlareShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareShader_26), value);
	}

	inline static int32_t get_offset_of_lensFlareMaterial_27() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___lensFlareMaterial_27)); }
	inline Material_t2815264910 * get_lensFlareMaterial_27() const { return ___lensFlareMaterial_27; }
	inline Material_t2815264910 ** get_address_of_lensFlareMaterial_27() { return &___lensFlareMaterial_27; }
	inline void set_lensFlareMaterial_27(Material_t2815264910 * value)
	{
		___lensFlareMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareMaterial_27), value);
	}

	inline static int32_t get_offset_of_vignetteShader_28() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___vignetteShader_28)); }
	inline Shader_t1881769421 * get_vignetteShader_28() const { return ___vignetteShader_28; }
	inline Shader_t1881769421 ** get_address_of_vignetteShader_28() { return &___vignetteShader_28; }
	inline void set_vignetteShader_28(Shader_t1881769421 * value)
	{
		___vignetteShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteShader_28), value);
	}

	inline static int32_t get_offset_of_vignetteMaterial_29() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___vignetteMaterial_29)); }
	inline Material_t2815264910 * get_vignetteMaterial_29() const { return ___vignetteMaterial_29; }
	inline Material_t2815264910 ** get_address_of_vignetteMaterial_29() { return &___vignetteMaterial_29; }
	inline void set_vignetteMaterial_29(Material_t2815264910 * value)
	{
		___vignetteMaterial_29 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteMaterial_29), value);
	}

	inline static int32_t get_offset_of_separableBlurShader_30() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___separableBlurShader_30)); }
	inline Shader_t1881769421 * get_separableBlurShader_30() const { return ___separableBlurShader_30; }
	inline Shader_t1881769421 ** get_address_of_separableBlurShader_30() { return &___separableBlurShader_30; }
	inline void set_separableBlurShader_30(Shader_t1881769421 * value)
	{
		___separableBlurShader_30 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_30), value);
	}

	inline static int32_t get_offset_of_separableBlurMaterial_31() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___separableBlurMaterial_31)); }
	inline Material_t2815264910 * get_separableBlurMaterial_31() const { return ___separableBlurMaterial_31; }
	inline Material_t2815264910 ** get_address_of_separableBlurMaterial_31() { return &___separableBlurMaterial_31; }
	inline void set_separableBlurMaterial_31(Material_t2815264910 * value)
	{
		___separableBlurMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurMaterial_31), value);
	}

	inline static int32_t get_offset_of_addBrightStuffOneOneShader_32() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___addBrightStuffOneOneShader_32)); }
	inline Shader_t1881769421 * get_addBrightStuffOneOneShader_32() const { return ___addBrightStuffOneOneShader_32; }
	inline Shader_t1881769421 ** get_address_of_addBrightStuffOneOneShader_32() { return &___addBrightStuffOneOneShader_32; }
	inline void set_addBrightStuffOneOneShader_32(Shader_t1881769421 * value)
	{
		___addBrightStuffOneOneShader_32 = value;
		Il2CppCodeGenWriteBarrier((&___addBrightStuffOneOneShader_32), value);
	}

	inline static int32_t get_offset_of_addBrightStuffBlendOneOneMaterial_33() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___addBrightStuffBlendOneOneMaterial_33)); }
	inline Material_t2815264910 * get_addBrightStuffBlendOneOneMaterial_33() const { return ___addBrightStuffBlendOneOneMaterial_33; }
	inline Material_t2815264910 ** get_address_of_addBrightStuffBlendOneOneMaterial_33() { return &___addBrightStuffBlendOneOneMaterial_33; }
	inline void set_addBrightStuffBlendOneOneMaterial_33(Material_t2815264910 * value)
	{
		___addBrightStuffBlendOneOneMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___addBrightStuffBlendOneOneMaterial_33), value);
	}

	inline static int32_t get_offset_of_screenBlendShader_34() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___screenBlendShader_34)); }
	inline Shader_t1881769421 * get_screenBlendShader_34() const { return ___screenBlendShader_34; }
	inline Shader_t1881769421 ** get_address_of_screenBlendShader_34() { return &___screenBlendShader_34; }
	inline void set_screenBlendShader_34(Shader_t1881769421 * value)
	{
		___screenBlendShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlendShader_34), value);
	}

	inline static int32_t get_offset_of_screenBlend_35() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___screenBlend_35)); }
	inline Material_t2815264910 * get_screenBlend_35() const { return ___screenBlend_35; }
	inline Material_t2815264910 ** get_address_of_screenBlend_35() { return &___screenBlend_35; }
	inline void set_screenBlend_35(Material_t2815264910 * value)
	{
		___screenBlend_35 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlend_35), value);
	}

	inline static int32_t get_offset_of_hollywoodFlaresShader_36() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___hollywoodFlaresShader_36)); }
	inline Shader_t1881769421 * get_hollywoodFlaresShader_36() const { return ___hollywoodFlaresShader_36; }
	inline Shader_t1881769421 ** get_address_of_hollywoodFlaresShader_36() { return &___hollywoodFlaresShader_36; }
	inline void set_hollywoodFlaresShader_36(Shader_t1881769421 * value)
	{
		___hollywoodFlaresShader_36 = value;
		Il2CppCodeGenWriteBarrier((&___hollywoodFlaresShader_36), value);
	}

	inline static int32_t get_offset_of_hollywoodFlaresMaterial_37() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___hollywoodFlaresMaterial_37)); }
	inline Material_t2815264910 * get_hollywoodFlaresMaterial_37() const { return ___hollywoodFlaresMaterial_37; }
	inline Material_t2815264910 ** get_address_of_hollywoodFlaresMaterial_37() { return &___hollywoodFlaresMaterial_37; }
	inline void set_hollywoodFlaresMaterial_37(Material_t2815264910 * value)
	{
		___hollywoodFlaresMaterial_37 = value;
		Il2CppCodeGenWriteBarrier((&___hollywoodFlaresMaterial_37), value);
	}

	inline static int32_t get_offset_of_brightPassFilterShader_38() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___brightPassFilterShader_38)); }
	inline Shader_t1881769421 * get_brightPassFilterShader_38() const { return ___brightPassFilterShader_38; }
	inline Shader_t1881769421 ** get_address_of_brightPassFilterShader_38() { return &___brightPassFilterShader_38; }
	inline void set_brightPassFilterShader_38(Shader_t1881769421 * value)
	{
		___brightPassFilterShader_38 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterShader_38), value);
	}

	inline static int32_t get_offset_of_brightPassFilterMaterial_39() { return static_cast<int32_t>(offsetof(BloomAndFlares_t1406318507, ___brightPassFilterMaterial_39)); }
	inline Material_t2815264910 * get_brightPassFilterMaterial_39() const { return ___brightPassFilterMaterial_39; }
	inline Material_t2815264910 ** get_address_of_brightPassFilterMaterial_39() { return &___brightPassFilterMaterial_39; }
	inline void set_brightPassFilterMaterial_39(Material_t2815264910 * value)
	{
		___brightPassFilterMaterial_39 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterMaterial_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMANDFLARES_T1406318507_H
#ifndef BLOOM_T3558726986_H
#define BLOOM_T3558726986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom
struct  Bloom_t3558726986  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.Bloom/TweakMode UnityStandardAssets.ImageEffects.Bloom::tweakMode
	int32_t ___tweakMode_6;
	// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode UnityStandardAssets.ImageEffects.Bloom::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode UnityStandardAssets.ImageEffects.Bloom::hdr
	int32_t ___hdr_8;
	// System.Boolean UnityStandardAssets.ImageEffects.Bloom::doHdr
	bool ___doHdr_9;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::sepBlurSpread
	float ___sepBlurSpread_10;
	// UnityStandardAssets.ImageEffects.Bloom/BloomQuality UnityStandardAssets.ImageEffects.Bloom::quality
	int32_t ___quality_11;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomIntensity
	float ___bloomIntensity_12;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomThreshold
	float ___bloomThreshold_13;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::bloomThresholdColor
	Color_t2582018970  ___bloomThresholdColor_14;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::bloomBlurIterations
	int32_t ___bloomBlurIterations_15;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::hollywoodFlareBlurIterations
	int32_t ___hollywoodFlareBlurIterations_16;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::flareRotation
	float ___flareRotation_17;
	// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle UnityStandardAssets.ImageEffects.Bloom::lensflareMode
	int32_t ___lensflareMode_18;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::hollyStretchWidth
	float ___hollyStretchWidth_19;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareIntensity
	float ___lensflareIntensity_20;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareThreshold
	float ___lensflareThreshold_21;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensFlareSaturation
	float ___lensFlareSaturation_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorA
	Color_t2582018970  ___flareColorA_23;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorB
	Color_t2582018970  ___flareColorB_24;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorC
	Color_t2582018970  ___flareColorC_25;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorD
	Color_t2582018970  ___flareColorD_26;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.Bloom::lensFlareVignetteMask
	Texture2D_t3063074017 * ___lensFlareVignetteMask_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::lensFlareShader
	Shader_t1881769421 * ___lensFlareShader_28;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::lensFlareMaterial
	Material_t2815264910 * ___lensFlareMaterial_29;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::screenBlendShader
	Shader_t1881769421 * ___screenBlendShader_30;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::screenBlend
	Material_t2815264910 * ___screenBlend_31;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresShader
	Shader_t1881769421 * ___blurAndFlaresShader_32;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresMaterial
	Material_t2815264910 * ___blurAndFlaresMaterial_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::brightPassFilterShader
	Shader_t1881769421 * ___brightPassFilterShader_34;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::brightPassFilterMaterial
	Material_t2815264910 * ___brightPassFilterMaterial_35;

public:
	inline static int32_t get_offset_of_tweakMode_6() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___tweakMode_6)); }
	inline int32_t get_tweakMode_6() const { return ___tweakMode_6; }
	inline int32_t* get_address_of_tweakMode_6() { return &___tweakMode_6; }
	inline void set_tweakMode_6(int32_t value)
	{
		___tweakMode_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_hdr_8() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___hdr_8)); }
	inline int32_t get_hdr_8() const { return ___hdr_8; }
	inline int32_t* get_address_of_hdr_8() { return &___hdr_8; }
	inline void set_hdr_8(int32_t value)
	{
		___hdr_8 = value;
	}

	inline static int32_t get_offset_of_doHdr_9() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___doHdr_9)); }
	inline bool get_doHdr_9() const { return ___doHdr_9; }
	inline bool* get_address_of_doHdr_9() { return &___doHdr_9; }
	inline void set_doHdr_9(bool value)
	{
		___doHdr_9 = value;
	}

	inline static int32_t get_offset_of_sepBlurSpread_10() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___sepBlurSpread_10)); }
	inline float get_sepBlurSpread_10() const { return ___sepBlurSpread_10; }
	inline float* get_address_of_sepBlurSpread_10() { return &___sepBlurSpread_10; }
	inline void set_sepBlurSpread_10(float value)
	{
		___sepBlurSpread_10 = value;
	}

	inline static int32_t get_offset_of_quality_11() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___quality_11)); }
	inline int32_t get_quality_11() const { return ___quality_11; }
	inline int32_t* get_address_of_quality_11() { return &___quality_11; }
	inline void set_quality_11(int32_t value)
	{
		___quality_11 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_12() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___bloomIntensity_12)); }
	inline float get_bloomIntensity_12() const { return ___bloomIntensity_12; }
	inline float* get_address_of_bloomIntensity_12() { return &___bloomIntensity_12; }
	inline void set_bloomIntensity_12(float value)
	{
		___bloomIntensity_12 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_13() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___bloomThreshold_13)); }
	inline float get_bloomThreshold_13() const { return ___bloomThreshold_13; }
	inline float* get_address_of_bloomThreshold_13() { return &___bloomThreshold_13; }
	inline void set_bloomThreshold_13(float value)
	{
		___bloomThreshold_13 = value;
	}

	inline static int32_t get_offset_of_bloomThresholdColor_14() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___bloomThresholdColor_14)); }
	inline Color_t2582018970  get_bloomThresholdColor_14() const { return ___bloomThresholdColor_14; }
	inline Color_t2582018970 * get_address_of_bloomThresholdColor_14() { return &___bloomThresholdColor_14; }
	inline void set_bloomThresholdColor_14(Color_t2582018970  value)
	{
		___bloomThresholdColor_14 = value;
	}

	inline static int32_t get_offset_of_bloomBlurIterations_15() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___bloomBlurIterations_15)); }
	inline int32_t get_bloomBlurIterations_15() const { return ___bloomBlurIterations_15; }
	inline int32_t* get_address_of_bloomBlurIterations_15() { return &___bloomBlurIterations_15; }
	inline void set_bloomBlurIterations_15(int32_t value)
	{
		___bloomBlurIterations_15 = value;
	}

	inline static int32_t get_offset_of_hollywoodFlareBlurIterations_16() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___hollywoodFlareBlurIterations_16)); }
	inline int32_t get_hollywoodFlareBlurIterations_16() const { return ___hollywoodFlareBlurIterations_16; }
	inline int32_t* get_address_of_hollywoodFlareBlurIterations_16() { return &___hollywoodFlareBlurIterations_16; }
	inline void set_hollywoodFlareBlurIterations_16(int32_t value)
	{
		___hollywoodFlareBlurIterations_16 = value;
	}

	inline static int32_t get_offset_of_flareRotation_17() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___flareRotation_17)); }
	inline float get_flareRotation_17() const { return ___flareRotation_17; }
	inline float* get_address_of_flareRotation_17() { return &___flareRotation_17; }
	inline void set_flareRotation_17(float value)
	{
		___flareRotation_17 = value;
	}

	inline static int32_t get_offset_of_lensflareMode_18() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensflareMode_18)); }
	inline int32_t get_lensflareMode_18() const { return ___lensflareMode_18; }
	inline int32_t* get_address_of_lensflareMode_18() { return &___lensflareMode_18; }
	inline void set_lensflareMode_18(int32_t value)
	{
		___lensflareMode_18 = value;
	}

	inline static int32_t get_offset_of_hollyStretchWidth_19() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___hollyStretchWidth_19)); }
	inline float get_hollyStretchWidth_19() const { return ___hollyStretchWidth_19; }
	inline float* get_address_of_hollyStretchWidth_19() { return &___hollyStretchWidth_19; }
	inline void set_hollyStretchWidth_19(float value)
	{
		___hollyStretchWidth_19 = value;
	}

	inline static int32_t get_offset_of_lensflareIntensity_20() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensflareIntensity_20)); }
	inline float get_lensflareIntensity_20() const { return ___lensflareIntensity_20; }
	inline float* get_address_of_lensflareIntensity_20() { return &___lensflareIntensity_20; }
	inline void set_lensflareIntensity_20(float value)
	{
		___lensflareIntensity_20 = value;
	}

	inline static int32_t get_offset_of_lensflareThreshold_21() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensflareThreshold_21)); }
	inline float get_lensflareThreshold_21() const { return ___lensflareThreshold_21; }
	inline float* get_address_of_lensflareThreshold_21() { return &___lensflareThreshold_21; }
	inline void set_lensflareThreshold_21(float value)
	{
		___lensflareThreshold_21 = value;
	}

	inline static int32_t get_offset_of_lensFlareSaturation_22() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensFlareSaturation_22)); }
	inline float get_lensFlareSaturation_22() const { return ___lensFlareSaturation_22; }
	inline float* get_address_of_lensFlareSaturation_22() { return &___lensFlareSaturation_22; }
	inline void set_lensFlareSaturation_22(float value)
	{
		___lensFlareSaturation_22 = value;
	}

	inline static int32_t get_offset_of_flareColorA_23() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___flareColorA_23)); }
	inline Color_t2582018970  get_flareColorA_23() const { return ___flareColorA_23; }
	inline Color_t2582018970 * get_address_of_flareColorA_23() { return &___flareColorA_23; }
	inline void set_flareColorA_23(Color_t2582018970  value)
	{
		___flareColorA_23 = value;
	}

	inline static int32_t get_offset_of_flareColorB_24() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___flareColorB_24)); }
	inline Color_t2582018970  get_flareColorB_24() const { return ___flareColorB_24; }
	inline Color_t2582018970 * get_address_of_flareColorB_24() { return &___flareColorB_24; }
	inline void set_flareColorB_24(Color_t2582018970  value)
	{
		___flareColorB_24 = value;
	}

	inline static int32_t get_offset_of_flareColorC_25() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___flareColorC_25)); }
	inline Color_t2582018970  get_flareColorC_25() const { return ___flareColorC_25; }
	inline Color_t2582018970 * get_address_of_flareColorC_25() { return &___flareColorC_25; }
	inline void set_flareColorC_25(Color_t2582018970  value)
	{
		___flareColorC_25 = value;
	}

	inline static int32_t get_offset_of_flareColorD_26() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___flareColorD_26)); }
	inline Color_t2582018970  get_flareColorD_26() const { return ___flareColorD_26; }
	inline Color_t2582018970 * get_address_of_flareColorD_26() { return &___flareColorD_26; }
	inline void set_flareColorD_26(Color_t2582018970  value)
	{
		___flareColorD_26 = value;
	}

	inline static int32_t get_offset_of_lensFlareVignetteMask_27() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensFlareVignetteMask_27)); }
	inline Texture2D_t3063074017 * get_lensFlareVignetteMask_27() const { return ___lensFlareVignetteMask_27; }
	inline Texture2D_t3063074017 ** get_address_of_lensFlareVignetteMask_27() { return &___lensFlareVignetteMask_27; }
	inline void set_lensFlareVignetteMask_27(Texture2D_t3063074017 * value)
	{
		___lensFlareVignetteMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareVignetteMask_27), value);
	}

	inline static int32_t get_offset_of_lensFlareShader_28() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensFlareShader_28)); }
	inline Shader_t1881769421 * get_lensFlareShader_28() const { return ___lensFlareShader_28; }
	inline Shader_t1881769421 ** get_address_of_lensFlareShader_28() { return &___lensFlareShader_28; }
	inline void set_lensFlareShader_28(Shader_t1881769421 * value)
	{
		___lensFlareShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareShader_28), value);
	}

	inline static int32_t get_offset_of_lensFlareMaterial_29() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___lensFlareMaterial_29)); }
	inline Material_t2815264910 * get_lensFlareMaterial_29() const { return ___lensFlareMaterial_29; }
	inline Material_t2815264910 ** get_address_of_lensFlareMaterial_29() { return &___lensFlareMaterial_29; }
	inline void set_lensFlareMaterial_29(Material_t2815264910 * value)
	{
		___lensFlareMaterial_29 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareMaterial_29), value);
	}

	inline static int32_t get_offset_of_screenBlendShader_30() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___screenBlendShader_30)); }
	inline Shader_t1881769421 * get_screenBlendShader_30() const { return ___screenBlendShader_30; }
	inline Shader_t1881769421 ** get_address_of_screenBlendShader_30() { return &___screenBlendShader_30; }
	inline void set_screenBlendShader_30(Shader_t1881769421 * value)
	{
		___screenBlendShader_30 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlendShader_30), value);
	}

	inline static int32_t get_offset_of_screenBlend_31() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___screenBlend_31)); }
	inline Material_t2815264910 * get_screenBlend_31() const { return ___screenBlend_31; }
	inline Material_t2815264910 ** get_address_of_screenBlend_31() { return &___screenBlend_31; }
	inline void set_screenBlend_31(Material_t2815264910 * value)
	{
		___screenBlend_31 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlend_31), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresShader_32() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___blurAndFlaresShader_32)); }
	inline Shader_t1881769421 * get_blurAndFlaresShader_32() const { return ___blurAndFlaresShader_32; }
	inline Shader_t1881769421 ** get_address_of_blurAndFlaresShader_32() { return &___blurAndFlaresShader_32; }
	inline void set_blurAndFlaresShader_32(Shader_t1881769421 * value)
	{
		___blurAndFlaresShader_32 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresShader_32), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresMaterial_33() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___blurAndFlaresMaterial_33)); }
	inline Material_t2815264910 * get_blurAndFlaresMaterial_33() const { return ___blurAndFlaresMaterial_33; }
	inline Material_t2815264910 ** get_address_of_blurAndFlaresMaterial_33() { return &___blurAndFlaresMaterial_33; }
	inline void set_blurAndFlaresMaterial_33(Material_t2815264910 * value)
	{
		___blurAndFlaresMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresMaterial_33), value);
	}

	inline static int32_t get_offset_of_brightPassFilterShader_34() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___brightPassFilterShader_34)); }
	inline Shader_t1881769421 * get_brightPassFilterShader_34() const { return ___brightPassFilterShader_34; }
	inline Shader_t1881769421 ** get_address_of_brightPassFilterShader_34() { return &___brightPassFilterShader_34; }
	inline void set_brightPassFilterShader_34(Shader_t1881769421 * value)
	{
		___brightPassFilterShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterShader_34), value);
	}

	inline static int32_t get_offset_of_brightPassFilterMaterial_35() { return static_cast<int32_t>(offsetof(Bloom_t3558726986, ___brightPassFilterMaterial_35)); }
	inline Material_t2815264910 * get_brightPassFilterMaterial_35() const { return ___brightPassFilterMaterial_35; }
	inline Material_t2815264910 ** get_address_of_brightPassFilterMaterial_35() { return &___brightPassFilterMaterial_35; }
	inline void set_brightPassFilterMaterial_35(Material_t2815264910 * value)
	{
		___brightPassFilterMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterMaterial_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_T3558726986_H
#ifndef ANTIALIASING_T1399311265_H
#define ANTIALIASING_T1399311265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Antialiasing
struct  Antialiasing_t1399311265  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.AAMode UnityStandardAssets.ImageEffects.Antialiasing::mode
	int32_t ___mode_6;
	// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::showGeneratedNormals
	bool ___showGeneratedNormals_7;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::offsetScale
	float ___offsetScale_8;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::blurRadius
	float ___blurRadius_9;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeThresholdMin
	float ___edgeThresholdMin_10;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeThreshold
	float ___edgeThreshold_11;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeSharpness
	float ___edgeSharpness_12;
	// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::dlaaSharp
	bool ___dlaaSharp_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::ssaaShader
	Shader_t1881769421 * ___ssaaShader_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::ssaa
	Material_t2815264910 * ___ssaa_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::dlaaShader
	Shader_t1881769421 * ___dlaaShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::dlaa
	Material_t2815264910 * ___dlaa_17;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::nfaaShader
	Shader_t1881769421 * ___nfaaShader_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::nfaa
	Material_t2815264910 * ___nfaa_19;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAPreset2
	Shader_t1881769421 * ___shaderFXAAPreset2_20;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAPreset2
	Material_t2815264910 * ___materialFXAAPreset2_21;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAPreset3
	Shader_t1881769421 * ___shaderFXAAPreset3_22;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAPreset3
	Material_t2815264910 * ___materialFXAAPreset3_23;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAII
	Shader_t1881769421 * ___shaderFXAAII_24;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAII
	Material_t2815264910 * ___materialFXAAII_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAIII
	Shader_t1881769421 * ___shaderFXAAIII_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAIII
	Material_t2815264910 * ___materialFXAAIII_27;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_showGeneratedNormals_7() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___showGeneratedNormals_7)); }
	inline bool get_showGeneratedNormals_7() const { return ___showGeneratedNormals_7; }
	inline bool* get_address_of_showGeneratedNormals_7() { return &___showGeneratedNormals_7; }
	inline void set_showGeneratedNormals_7(bool value)
	{
		___showGeneratedNormals_7 = value;
	}

	inline static int32_t get_offset_of_offsetScale_8() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___offsetScale_8)); }
	inline float get_offsetScale_8() const { return ___offsetScale_8; }
	inline float* get_address_of_offsetScale_8() { return &___offsetScale_8; }
	inline void set_offsetScale_8(float value)
	{
		___offsetScale_8 = value;
	}

	inline static int32_t get_offset_of_blurRadius_9() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___blurRadius_9)); }
	inline float get_blurRadius_9() const { return ___blurRadius_9; }
	inline float* get_address_of_blurRadius_9() { return &___blurRadius_9; }
	inline void set_blurRadius_9(float value)
	{
		___blurRadius_9 = value;
	}

	inline static int32_t get_offset_of_edgeThresholdMin_10() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___edgeThresholdMin_10)); }
	inline float get_edgeThresholdMin_10() const { return ___edgeThresholdMin_10; }
	inline float* get_address_of_edgeThresholdMin_10() { return &___edgeThresholdMin_10; }
	inline void set_edgeThresholdMin_10(float value)
	{
		___edgeThresholdMin_10 = value;
	}

	inline static int32_t get_offset_of_edgeThreshold_11() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___edgeThreshold_11)); }
	inline float get_edgeThreshold_11() const { return ___edgeThreshold_11; }
	inline float* get_address_of_edgeThreshold_11() { return &___edgeThreshold_11; }
	inline void set_edgeThreshold_11(float value)
	{
		___edgeThreshold_11 = value;
	}

	inline static int32_t get_offset_of_edgeSharpness_12() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___edgeSharpness_12)); }
	inline float get_edgeSharpness_12() const { return ___edgeSharpness_12; }
	inline float* get_address_of_edgeSharpness_12() { return &___edgeSharpness_12; }
	inline void set_edgeSharpness_12(float value)
	{
		___edgeSharpness_12 = value;
	}

	inline static int32_t get_offset_of_dlaaSharp_13() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___dlaaSharp_13)); }
	inline bool get_dlaaSharp_13() const { return ___dlaaSharp_13; }
	inline bool* get_address_of_dlaaSharp_13() { return &___dlaaSharp_13; }
	inline void set_dlaaSharp_13(bool value)
	{
		___dlaaSharp_13 = value;
	}

	inline static int32_t get_offset_of_ssaaShader_14() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___ssaaShader_14)); }
	inline Shader_t1881769421 * get_ssaaShader_14() const { return ___ssaaShader_14; }
	inline Shader_t1881769421 ** get_address_of_ssaaShader_14() { return &___ssaaShader_14; }
	inline void set_ssaaShader_14(Shader_t1881769421 * value)
	{
		___ssaaShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___ssaaShader_14), value);
	}

	inline static int32_t get_offset_of_ssaa_15() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___ssaa_15)); }
	inline Material_t2815264910 * get_ssaa_15() const { return ___ssaa_15; }
	inline Material_t2815264910 ** get_address_of_ssaa_15() { return &___ssaa_15; }
	inline void set_ssaa_15(Material_t2815264910 * value)
	{
		___ssaa_15 = value;
		Il2CppCodeGenWriteBarrier((&___ssaa_15), value);
	}

	inline static int32_t get_offset_of_dlaaShader_16() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___dlaaShader_16)); }
	inline Shader_t1881769421 * get_dlaaShader_16() const { return ___dlaaShader_16; }
	inline Shader_t1881769421 ** get_address_of_dlaaShader_16() { return &___dlaaShader_16; }
	inline void set_dlaaShader_16(Shader_t1881769421 * value)
	{
		___dlaaShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___dlaaShader_16), value);
	}

	inline static int32_t get_offset_of_dlaa_17() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___dlaa_17)); }
	inline Material_t2815264910 * get_dlaa_17() const { return ___dlaa_17; }
	inline Material_t2815264910 ** get_address_of_dlaa_17() { return &___dlaa_17; }
	inline void set_dlaa_17(Material_t2815264910 * value)
	{
		___dlaa_17 = value;
		Il2CppCodeGenWriteBarrier((&___dlaa_17), value);
	}

	inline static int32_t get_offset_of_nfaaShader_18() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___nfaaShader_18)); }
	inline Shader_t1881769421 * get_nfaaShader_18() const { return ___nfaaShader_18; }
	inline Shader_t1881769421 ** get_address_of_nfaaShader_18() { return &___nfaaShader_18; }
	inline void set_nfaaShader_18(Shader_t1881769421 * value)
	{
		___nfaaShader_18 = value;
		Il2CppCodeGenWriteBarrier((&___nfaaShader_18), value);
	}

	inline static int32_t get_offset_of_nfaa_19() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___nfaa_19)); }
	inline Material_t2815264910 * get_nfaa_19() const { return ___nfaa_19; }
	inline Material_t2815264910 ** get_address_of_nfaa_19() { return &___nfaa_19; }
	inline void set_nfaa_19(Material_t2815264910 * value)
	{
		___nfaa_19 = value;
		Il2CppCodeGenWriteBarrier((&___nfaa_19), value);
	}

	inline static int32_t get_offset_of_shaderFXAAPreset2_20() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___shaderFXAAPreset2_20)); }
	inline Shader_t1881769421 * get_shaderFXAAPreset2_20() const { return ___shaderFXAAPreset2_20; }
	inline Shader_t1881769421 ** get_address_of_shaderFXAAPreset2_20() { return &___shaderFXAAPreset2_20; }
	inline void set_shaderFXAAPreset2_20(Shader_t1881769421 * value)
	{
		___shaderFXAAPreset2_20 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAPreset2_20), value);
	}

	inline static int32_t get_offset_of_materialFXAAPreset2_21() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___materialFXAAPreset2_21)); }
	inline Material_t2815264910 * get_materialFXAAPreset2_21() const { return ___materialFXAAPreset2_21; }
	inline Material_t2815264910 ** get_address_of_materialFXAAPreset2_21() { return &___materialFXAAPreset2_21; }
	inline void set_materialFXAAPreset2_21(Material_t2815264910 * value)
	{
		___materialFXAAPreset2_21 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAPreset2_21), value);
	}

	inline static int32_t get_offset_of_shaderFXAAPreset3_22() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___shaderFXAAPreset3_22)); }
	inline Shader_t1881769421 * get_shaderFXAAPreset3_22() const { return ___shaderFXAAPreset3_22; }
	inline Shader_t1881769421 ** get_address_of_shaderFXAAPreset3_22() { return &___shaderFXAAPreset3_22; }
	inline void set_shaderFXAAPreset3_22(Shader_t1881769421 * value)
	{
		___shaderFXAAPreset3_22 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAPreset3_22), value);
	}

	inline static int32_t get_offset_of_materialFXAAPreset3_23() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___materialFXAAPreset3_23)); }
	inline Material_t2815264910 * get_materialFXAAPreset3_23() const { return ___materialFXAAPreset3_23; }
	inline Material_t2815264910 ** get_address_of_materialFXAAPreset3_23() { return &___materialFXAAPreset3_23; }
	inline void set_materialFXAAPreset3_23(Material_t2815264910 * value)
	{
		___materialFXAAPreset3_23 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAPreset3_23), value);
	}

	inline static int32_t get_offset_of_shaderFXAAII_24() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___shaderFXAAII_24)); }
	inline Shader_t1881769421 * get_shaderFXAAII_24() const { return ___shaderFXAAII_24; }
	inline Shader_t1881769421 ** get_address_of_shaderFXAAII_24() { return &___shaderFXAAII_24; }
	inline void set_shaderFXAAII_24(Shader_t1881769421 * value)
	{
		___shaderFXAAII_24 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAII_24), value);
	}

	inline static int32_t get_offset_of_materialFXAAII_25() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___materialFXAAII_25)); }
	inline Material_t2815264910 * get_materialFXAAII_25() const { return ___materialFXAAII_25; }
	inline Material_t2815264910 ** get_address_of_materialFXAAII_25() { return &___materialFXAAII_25; }
	inline void set_materialFXAAII_25(Material_t2815264910 * value)
	{
		___materialFXAAII_25 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAII_25), value);
	}

	inline static int32_t get_offset_of_shaderFXAAIII_26() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___shaderFXAAIII_26)); }
	inline Shader_t1881769421 * get_shaderFXAAIII_26() const { return ___shaderFXAAIII_26; }
	inline Shader_t1881769421 ** get_address_of_shaderFXAAIII_26() { return &___shaderFXAAIII_26; }
	inline void set_shaderFXAAIII_26(Shader_t1881769421 * value)
	{
		___shaderFXAAIII_26 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAIII_26), value);
	}

	inline static int32_t get_offset_of_materialFXAAIII_27() { return static_cast<int32_t>(offsetof(Antialiasing_t1399311265, ___materialFXAAIII_27)); }
	inline Material_t2815264910 * get_materialFXAAIII_27() const { return ___materialFXAAIII_27; }
	inline Material_t2815264910 ** get_address_of_materialFXAAIII_27() { return &___materialFXAAIII_27; }
	inline void set_materialFXAAIII_27(Material_t2815264910 * value)
	{
		___materialFXAAIII_27 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAIII_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASING_T1399311265_H
#ifndef CAMERAMOTIONBLUR_T1021540318_H
#define CAMERAMOTIONBLUR_T1021540318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CameraMotionBlur
struct  CameraMotionBlur_t1021540318  : public PostEffectsBase_t1831076792
{
public:
	// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter UnityStandardAssets.ImageEffects.CameraMotionBlur::filterType
	int32_t ___filterType_7;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::preview
	bool ___preview_8;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::previewScale
	Vector3_t1986933152  ___previewScale_9;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::movementScale
	float ___movementScale_10;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::rotationScale
	float ___rotationScale_11;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::maxVelocity
	float ___maxVelocity_12;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::minVelocity
	float ___minVelocity_13;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::velocityScale
	float ___velocityScale_14;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::softZDistance
	float ___softZDistance_15;
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::velocityDownsample
	int32_t ___velocityDownsample_16;
	// UnityEngine.LayerMask UnityStandardAssets.ImageEffects.CameraMotionBlur::excludeLayers
	LayerMask_t246267875  ___excludeLayers_17;
	// UnityEngine.GameObject UnityStandardAssets.ImageEffects.CameraMotionBlur::tmpCam
	GameObject_t2557347079 * ___tmpCam_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::shader
	Shader_t1881769421 * ___shader_19;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::dx11MotionBlurShader
	Shader_t1881769421 * ___dx11MotionBlurShader_20;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::replacementClear
	Shader_t1881769421 * ___replacementClear_21;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CameraMotionBlur::motionBlurMaterial
	Material_t2815264910 * ___motionBlurMaterial_22;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CameraMotionBlur::dx11MotionBlurMaterial
	Material_t2815264910 * ___dx11MotionBlurMaterial_23;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.CameraMotionBlur::noiseTexture
	Texture2D_t3063074017 * ___noiseTexture_24;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::jitter
	float ___jitter_25;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::showVelocity
	bool ___showVelocity_26;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::showVelocityScale
	float ___showVelocityScale_27;
	// UnityEngine.Matrix4x4 UnityStandardAssets.ImageEffects.CameraMotionBlur::currentViewProjMat
	Matrix4x4_t1237934469  ___currentViewProjMat_28;
	// UnityEngine.Matrix4x4[] UnityStandardAssets.ImageEffects.CameraMotionBlur::currentStereoViewProjMat
	Matrix4x4U5BU5D_t1457428168* ___currentStereoViewProjMat_29;
	// UnityEngine.Matrix4x4 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevViewProjMat
	Matrix4x4_t1237934469  ___prevViewProjMat_30;
	// UnityEngine.Matrix4x4[] UnityStandardAssets.ImageEffects.CameraMotionBlur::prevStereoViewProjMat
	Matrix4x4U5BU5D_t1457428168* ___prevStereoViewProjMat_31;
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameCount
	int32_t ___prevFrameCount_32;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::wasActive
	bool ___wasActive_33;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameForward
	Vector3_t1986933152  ___prevFrameForward_34;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameUp
	Vector3_t1986933152  ___prevFrameUp_35;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFramePos
	Vector3_t1986933152  ___prevFramePos_36;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::_camera
	Camera_t2839736942 * ____camera_37;

public:
	inline static int32_t get_offset_of_filterType_7() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___filterType_7)); }
	inline int32_t get_filterType_7() const { return ___filterType_7; }
	inline int32_t* get_address_of_filterType_7() { return &___filterType_7; }
	inline void set_filterType_7(int32_t value)
	{
		___filterType_7 = value;
	}

	inline static int32_t get_offset_of_preview_8() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___preview_8)); }
	inline bool get_preview_8() const { return ___preview_8; }
	inline bool* get_address_of_preview_8() { return &___preview_8; }
	inline void set_preview_8(bool value)
	{
		___preview_8 = value;
	}

	inline static int32_t get_offset_of_previewScale_9() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___previewScale_9)); }
	inline Vector3_t1986933152  get_previewScale_9() const { return ___previewScale_9; }
	inline Vector3_t1986933152 * get_address_of_previewScale_9() { return &___previewScale_9; }
	inline void set_previewScale_9(Vector3_t1986933152  value)
	{
		___previewScale_9 = value;
	}

	inline static int32_t get_offset_of_movementScale_10() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___movementScale_10)); }
	inline float get_movementScale_10() const { return ___movementScale_10; }
	inline float* get_address_of_movementScale_10() { return &___movementScale_10; }
	inline void set_movementScale_10(float value)
	{
		___movementScale_10 = value;
	}

	inline static int32_t get_offset_of_rotationScale_11() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___rotationScale_11)); }
	inline float get_rotationScale_11() const { return ___rotationScale_11; }
	inline float* get_address_of_rotationScale_11() { return &___rotationScale_11; }
	inline void set_rotationScale_11(float value)
	{
		___rotationScale_11 = value;
	}

	inline static int32_t get_offset_of_maxVelocity_12() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___maxVelocity_12)); }
	inline float get_maxVelocity_12() const { return ___maxVelocity_12; }
	inline float* get_address_of_maxVelocity_12() { return &___maxVelocity_12; }
	inline void set_maxVelocity_12(float value)
	{
		___maxVelocity_12 = value;
	}

	inline static int32_t get_offset_of_minVelocity_13() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___minVelocity_13)); }
	inline float get_minVelocity_13() const { return ___minVelocity_13; }
	inline float* get_address_of_minVelocity_13() { return &___minVelocity_13; }
	inline void set_minVelocity_13(float value)
	{
		___minVelocity_13 = value;
	}

	inline static int32_t get_offset_of_velocityScale_14() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___velocityScale_14)); }
	inline float get_velocityScale_14() const { return ___velocityScale_14; }
	inline float* get_address_of_velocityScale_14() { return &___velocityScale_14; }
	inline void set_velocityScale_14(float value)
	{
		___velocityScale_14 = value;
	}

	inline static int32_t get_offset_of_softZDistance_15() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___softZDistance_15)); }
	inline float get_softZDistance_15() const { return ___softZDistance_15; }
	inline float* get_address_of_softZDistance_15() { return &___softZDistance_15; }
	inline void set_softZDistance_15(float value)
	{
		___softZDistance_15 = value;
	}

	inline static int32_t get_offset_of_velocityDownsample_16() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___velocityDownsample_16)); }
	inline int32_t get_velocityDownsample_16() const { return ___velocityDownsample_16; }
	inline int32_t* get_address_of_velocityDownsample_16() { return &___velocityDownsample_16; }
	inline void set_velocityDownsample_16(int32_t value)
	{
		___velocityDownsample_16 = value;
	}

	inline static int32_t get_offset_of_excludeLayers_17() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___excludeLayers_17)); }
	inline LayerMask_t246267875  get_excludeLayers_17() const { return ___excludeLayers_17; }
	inline LayerMask_t246267875 * get_address_of_excludeLayers_17() { return &___excludeLayers_17; }
	inline void set_excludeLayers_17(LayerMask_t246267875  value)
	{
		___excludeLayers_17 = value;
	}

	inline static int32_t get_offset_of_tmpCam_18() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___tmpCam_18)); }
	inline GameObject_t2557347079 * get_tmpCam_18() const { return ___tmpCam_18; }
	inline GameObject_t2557347079 ** get_address_of_tmpCam_18() { return &___tmpCam_18; }
	inline void set_tmpCam_18(GameObject_t2557347079 * value)
	{
		___tmpCam_18 = value;
		Il2CppCodeGenWriteBarrier((&___tmpCam_18), value);
	}

	inline static int32_t get_offset_of_shader_19() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___shader_19)); }
	inline Shader_t1881769421 * get_shader_19() const { return ___shader_19; }
	inline Shader_t1881769421 ** get_address_of_shader_19() { return &___shader_19; }
	inline void set_shader_19(Shader_t1881769421 * value)
	{
		___shader_19 = value;
		Il2CppCodeGenWriteBarrier((&___shader_19), value);
	}

	inline static int32_t get_offset_of_dx11MotionBlurShader_20() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___dx11MotionBlurShader_20)); }
	inline Shader_t1881769421 * get_dx11MotionBlurShader_20() const { return ___dx11MotionBlurShader_20; }
	inline Shader_t1881769421 ** get_address_of_dx11MotionBlurShader_20() { return &___dx11MotionBlurShader_20; }
	inline void set_dx11MotionBlurShader_20(Shader_t1881769421 * value)
	{
		___dx11MotionBlurShader_20 = value;
		Il2CppCodeGenWriteBarrier((&___dx11MotionBlurShader_20), value);
	}

	inline static int32_t get_offset_of_replacementClear_21() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___replacementClear_21)); }
	inline Shader_t1881769421 * get_replacementClear_21() const { return ___replacementClear_21; }
	inline Shader_t1881769421 ** get_address_of_replacementClear_21() { return &___replacementClear_21; }
	inline void set_replacementClear_21(Shader_t1881769421 * value)
	{
		___replacementClear_21 = value;
		Il2CppCodeGenWriteBarrier((&___replacementClear_21), value);
	}

	inline static int32_t get_offset_of_motionBlurMaterial_22() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___motionBlurMaterial_22)); }
	inline Material_t2815264910 * get_motionBlurMaterial_22() const { return ___motionBlurMaterial_22; }
	inline Material_t2815264910 ** get_address_of_motionBlurMaterial_22() { return &___motionBlurMaterial_22; }
	inline void set_motionBlurMaterial_22(Material_t2815264910 * value)
	{
		___motionBlurMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlurMaterial_22), value);
	}

	inline static int32_t get_offset_of_dx11MotionBlurMaterial_23() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___dx11MotionBlurMaterial_23)); }
	inline Material_t2815264910 * get_dx11MotionBlurMaterial_23() const { return ___dx11MotionBlurMaterial_23; }
	inline Material_t2815264910 ** get_address_of_dx11MotionBlurMaterial_23() { return &___dx11MotionBlurMaterial_23; }
	inline void set_dx11MotionBlurMaterial_23(Material_t2815264910 * value)
	{
		___dx11MotionBlurMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___dx11MotionBlurMaterial_23), value);
	}

	inline static int32_t get_offset_of_noiseTexture_24() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___noiseTexture_24)); }
	inline Texture2D_t3063074017 * get_noiseTexture_24() const { return ___noiseTexture_24; }
	inline Texture2D_t3063074017 ** get_address_of_noiseTexture_24() { return &___noiseTexture_24; }
	inline void set_noiseTexture_24(Texture2D_t3063074017 * value)
	{
		___noiseTexture_24 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTexture_24), value);
	}

	inline static int32_t get_offset_of_jitter_25() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___jitter_25)); }
	inline float get_jitter_25() const { return ___jitter_25; }
	inline float* get_address_of_jitter_25() { return &___jitter_25; }
	inline void set_jitter_25(float value)
	{
		___jitter_25 = value;
	}

	inline static int32_t get_offset_of_showVelocity_26() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___showVelocity_26)); }
	inline bool get_showVelocity_26() const { return ___showVelocity_26; }
	inline bool* get_address_of_showVelocity_26() { return &___showVelocity_26; }
	inline void set_showVelocity_26(bool value)
	{
		___showVelocity_26 = value;
	}

	inline static int32_t get_offset_of_showVelocityScale_27() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___showVelocityScale_27)); }
	inline float get_showVelocityScale_27() const { return ___showVelocityScale_27; }
	inline float* get_address_of_showVelocityScale_27() { return &___showVelocityScale_27; }
	inline void set_showVelocityScale_27(float value)
	{
		___showVelocityScale_27 = value;
	}

	inline static int32_t get_offset_of_currentViewProjMat_28() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___currentViewProjMat_28)); }
	inline Matrix4x4_t1237934469  get_currentViewProjMat_28() const { return ___currentViewProjMat_28; }
	inline Matrix4x4_t1237934469 * get_address_of_currentViewProjMat_28() { return &___currentViewProjMat_28; }
	inline void set_currentViewProjMat_28(Matrix4x4_t1237934469  value)
	{
		___currentViewProjMat_28 = value;
	}

	inline static int32_t get_offset_of_currentStereoViewProjMat_29() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___currentStereoViewProjMat_29)); }
	inline Matrix4x4U5BU5D_t1457428168* get_currentStereoViewProjMat_29() const { return ___currentStereoViewProjMat_29; }
	inline Matrix4x4U5BU5D_t1457428168** get_address_of_currentStereoViewProjMat_29() { return &___currentStereoViewProjMat_29; }
	inline void set_currentStereoViewProjMat_29(Matrix4x4U5BU5D_t1457428168* value)
	{
		___currentStereoViewProjMat_29 = value;
		Il2CppCodeGenWriteBarrier((&___currentStereoViewProjMat_29), value);
	}

	inline static int32_t get_offset_of_prevViewProjMat_30() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevViewProjMat_30)); }
	inline Matrix4x4_t1237934469  get_prevViewProjMat_30() const { return ___prevViewProjMat_30; }
	inline Matrix4x4_t1237934469 * get_address_of_prevViewProjMat_30() { return &___prevViewProjMat_30; }
	inline void set_prevViewProjMat_30(Matrix4x4_t1237934469  value)
	{
		___prevViewProjMat_30 = value;
	}

	inline static int32_t get_offset_of_prevStereoViewProjMat_31() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevStereoViewProjMat_31)); }
	inline Matrix4x4U5BU5D_t1457428168* get_prevStereoViewProjMat_31() const { return ___prevStereoViewProjMat_31; }
	inline Matrix4x4U5BU5D_t1457428168** get_address_of_prevStereoViewProjMat_31() { return &___prevStereoViewProjMat_31; }
	inline void set_prevStereoViewProjMat_31(Matrix4x4U5BU5D_t1457428168* value)
	{
		___prevStereoViewProjMat_31 = value;
		Il2CppCodeGenWriteBarrier((&___prevStereoViewProjMat_31), value);
	}

	inline static int32_t get_offset_of_prevFrameCount_32() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevFrameCount_32)); }
	inline int32_t get_prevFrameCount_32() const { return ___prevFrameCount_32; }
	inline int32_t* get_address_of_prevFrameCount_32() { return &___prevFrameCount_32; }
	inline void set_prevFrameCount_32(int32_t value)
	{
		___prevFrameCount_32 = value;
	}

	inline static int32_t get_offset_of_wasActive_33() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___wasActive_33)); }
	inline bool get_wasActive_33() const { return ___wasActive_33; }
	inline bool* get_address_of_wasActive_33() { return &___wasActive_33; }
	inline void set_wasActive_33(bool value)
	{
		___wasActive_33 = value;
	}

	inline static int32_t get_offset_of_prevFrameForward_34() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevFrameForward_34)); }
	inline Vector3_t1986933152  get_prevFrameForward_34() const { return ___prevFrameForward_34; }
	inline Vector3_t1986933152 * get_address_of_prevFrameForward_34() { return &___prevFrameForward_34; }
	inline void set_prevFrameForward_34(Vector3_t1986933152  value)
	{
		___prevFrameForward_34 = value;
	}

	inline static int32_t get_offset_of_prevFrameUp_35() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevFrameUp_35)); }
	inline Vector3_t1986933152  get_prevFrameUp_35() const { return ___prevFrameUp_35; }
	inline Vector3_t1986933152 * get_address_of_prevFrameUp_35() { return &___prevFrameUp_35; }
	inline void set_prevFrameUp_35(Vector3_t1986933152  value)
	{
		___prevFrameUp_35 = value;
	}

	inline static int32_t get_offset_of_prevFramePos_36() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ___prevFramePos_36)); }
	inline Vector3_t1986933152  get_prevFramePos_36() const { return ___prevFramePos_36; }
	inline Vector3_t1986933152 * get_address_of_prevFramePos_36() { return &___prevFramePos_36; }
	inline void set_prevFramePos_36(Vector3_t1986933152  value)
	{
		___prevFramePos_36 = value;
	}

	inline static int32_t get_offset_of__camera_37() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318, ____camera_37)); }
	inline Camera_t2839736942 * get__camera_37() const { return ____camera_37; }
	inline Camera_t2839736942 ** get_address_of__camera_37() { return &____camera_37; }
	inline void set__camera_37(Camera_t2839736942 * value)
	{
		____camera_37 = value;
		Il2CppCodeGenWriteBarrier((&____camera_37), value);
	}
};

struct CameraMotionBlur_t1021540318_StaticFields
{
public:
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::MAX_RADIUS
	float ___MAX_RADIUS_6;

public:
	inline static int32_t get_offset_of_MAX_RADIUS_6() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t1021540318_StaticFields, ___MAX_RADIUS_6)); }
	inline float get_MAX_RADIUS_6() const { return ___MAX_RADIUS_6; }
	inline float* get_address_of_MAX_RADIUS_6() { return &___MAX_RADIUS_6; }
	inline void set_MAX_RADIUS_6(float value)
	{
		___MAX_RADIUS_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMOTIONBLUR_T1021540318_H
#ifndef DEPTHOFFIELD_T195334042_H
#define DEPTHOFFIELD_T195334042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField
struct  DepthOfField_t195334042  : public PostEffectsBase_t1831076792
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::visualizeFocus
	bool ___visualizeFocus_6;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalLength
	float ___focalLength_7;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalSize
	float ___focalSize_8;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::aperture
	float ___aperture_9;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.DepthOfField::focalTransform
	Transform_t362059596 * ___focalTransform_10;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::maxBlurSize
	float ___maxBlurSize_11;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::highResolution
	bool ___highResolution_12;
	// UnityStandardAssets.ImageEffects.DepthOfField/BlurType UnityStandardAssets.ImageEffects.DepthOfField::blurType
	int32_t ___blurType_13;
	// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount UnityStandardAssets.ImageEffects.DepthOfField::blurSampleCount
	int32_t ___blurSampleCount_14;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::nearBlur
	bool ___nearBlur_15;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::foregroundOverlap
	float ___foregroundOverlap_16;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfField::dofHdrShader
	Shader_t1881769421 * ___dofHdrShader_17;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfField::dofHdrMaterial
	Material_t2815264910 * ___dofHdrMaterial_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehShader
	Shader_t1881769421 * ___dx11BokehShader_19;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfField::dx11bokehMaterial
	Material_t2815264910 * ___dx11bokehMaterial_20;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehThreshold
	float ___dx11BokehThreshold_21;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11SpawnHeuristic
	float ___dx11SpawnHeuristic_22;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehTexture
	Texture2D_t3063074017 * ___dx11BokehTexture_23;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehScale
	float ___dx11BokehScale_24;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehIntensity
	float ___dx11BokehIntensity_25;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalDistance01
	float ___focalDistance01_26;
	// UnityEngine.ComputeBuffer UnityStandardAssets.ImageEffects.DepthOfField::cbDrawArgs
	ComputeBuffer_t2358285523 * ___cbDrawArgs_27;
	// UnityEngine.ComputeBuffer UnityStandardAssets.ImageEffects.DepthOfField::cbPoints
	ComputeBuffer_t2358285523 * ___cbPoints_28;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::internalBlurWidth
	float ___internalBlurWidth_29;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.DepthOfField::cachedCamera
	Camera_t2839736942 * ___cachedCamera_30;

public:
	inline static int32_t get_offset_of_visualizeFocus_6() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___visualizeFocus_6)); }
	inline bool get_visualizeFocus_6() const { return ___visualizeFocus_6; }
	inline bool* get_address_of_visualizeFocus_6() { return &___visualizeFocus_6; }
	inline void set_visualizeFocus_6(bool value)
	{
		___visualizeFocus_6 = value;
	}

	inline static int32_t get_offset_of_focalLength_7() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___focalLength_7)); }
	inline float get_focalLength_7() const { return ___focalLength_7; }
	inline float* get_address_of_focalLength_7() { return &___focalLength_7; }
	inline void set_focalLength_7(float value)
	{
		___focalLength_7 = value;
	}

	inline static int32_t get_offset_of_focalSize_8() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___focalSize_8)); }
	inline float get_focalSize_8() const { return ___focalSize_8; }
	inline float* get_address_of_focalSize_8() { return &___focalSize_8; }
	inline void set_focalSize_8(float value)
	{
		___focalSize_8 = value;
	}

	inline static int32_t get_offset_of_aperture_9() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___aperture_9)); }
	inline float get_aperture_9() const { return ___aperture_9; }
	inline float* get_address_of_aperture_9() { return &___aperture_9; }
	inline void set_aperture_9(float value)
	{
		___aperture_9 = value;
	}

	inline static int32_t get_offset_of_focalTransform_10() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___focalTransform_10)); }
	inline Transform_t362059596 * get_focalTransform_10() const { return ___focalTransform_10; }
	inline Transform_t362059596 ** get_address_of_focalTransform_10() { return &___focalTransform_10; }
	inline void set_focalTransform_10(Transform_t362059596 * value)
	{
		___focalTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___focalTransform_10), value);
	}

	inline static int32_t get_offset_of_maxBlurSize_11() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___maxBlurSize_11)); }
	inline float get_maxBlurSize_11() const { return ___maxBlurSize_11; }
	inline float* get_address_of_maxBlurSize_11() { return &___maxBlurSize_11; }
	inline void set_maxBlurSize_11(float value)
	{
		___maxBlurSize_11 = value;
	}

	inline static int32_t get_offset_of_highResolution_12() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___highResolution_12)); }
	inline bool get_highResolution_12() const { return ___highResolution_12; }
	inline bool* get_address_of_highResolution_12() { return &___highResolution_12; }
	inline void set_highResolution_12(bool value)
	{
		___highResolution_12 = value;
	}

	inline static int32_t get_offset_of_blurType_13() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___blurType_13)); }
	inline int32_t get_blurType_13() const { return ___blurType_13; }
	inline int32_t* get_address_of_blurType_13() { return &___blurType_13; }
	inline void set_blurType_13(int32_t value)
	{
		___blurType_13 = value;
	}

	inline static int32_t get_offset_of_blurSampleCount_14() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___blurSampleCount_14)); }
	inline int32_t get_blurSampleCount_14() const { return ___blurSampleCount_14; }
	inline int32_t* get_address_of_blurSampleCount_14() { return &___blurSampleCount_14; }
	inline void set_blurSampleCount_14(int32_t value)
	{
		___blurSampleCount_14 = value;
	}

	inline static int32_t get_offset_of_nearBlur_15() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___nearBlur_15)); }
	inline bool get_nearBlur_15() const { return ___nearBlur_15; }
	inline bool* get_address_of_nearBlur_15() { return &___nearBlur_15; }
	inline void set_nearBlur_15(bool value)
	{
		___nearBlur_15 = value;
	}

	inline static int32_t get_offset_of_foregroundOverlap_16() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___foregroundOverlap_16)); }
	inline float get_foregroundOverlap_16() const { return ___foregroundOverlap_16; }
	inline float* get_address_of_foregroundOverlap_16() { return &___foregroundOverlap_16; }
	inline void set_foregroundOverlap_16(float value)
	{
		___foregroundOverlap_16 = value;
	}

	inline static int32_t get_offset_of_dofHdrShader_17() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dofHdrShader_17)); }
	inline Shader_t1881769421 * get_dofHdrShader_17() const { return ___dofHdrShader_17; }
	inline Shader_t1881769421 ** get_address_of_dofHdrShader_17() { return &___dofHdrShader_17; }
	inline void set_dofHdrShader_17(Shader_t1881769421 * value)
	{
		___dofHdrShader_17 = value;
		Il2CppCodeGenWriteBarrier((&___dofHdrShader_17), value);
	}

	inline static int32_t get_offset_of_dofHdrMaterial_18() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dofHdrMaterial_18)); }
	inline Material_t2815264910 * get_dofHdrMaterial_18() const { return ___dofHdrMaterial_18; }
	inline Material_t2815264910 ** get_address_of_dofHdrMaterial_18() { return &___dofHdrMaterial_18; }
	inline void set_dofHdrMaterial_18(Material_t2815264910 * value)
	{
		___dofHdrMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___dofHdrMaterial_18), value);
	}

	inline static int32_t get_offset_of_dx11BokehShader_19() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11BokehShader_19)); }
	inline Shader_t1881769421 * get_dx11BokehShader_19() const { return ___dx11BokehShader_19; }
	inline Shader_t1881769421 ** get_address_of_dx11BokehShader_19() { return &___dx11BokehShader_19; }
	inline void set_dx11BokehShader_19(Shader_t1881769421 * value)
	{
		___dx11BokehShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___dx11BokehShader_19), value);
	}

	inline static int32_t get_offset_of_dx11bokehMaterial_20() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11bokehMaterial_20)); }
	inline Material_t2815264910 * get_dx11bokehMaterial_20() const { return ___dx11bokehMaterial_20; }
	inline Material_t2815264910 ** get_address_of_dx11bokehMaterial_20() { return &___dx11bokehMaterial_20; }
	inline void set_dx11bokehMaterial_20(Material_t2815264910 * value)
	{
		___dx11bokehMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___dx11bokehMaterial_20), value);
	}

	inline static int32_t get_offset_of_dx11BokehThreshold_21() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11BokehThreshold_21)); }
	inline float get_dx11BokehThreshold_21() const { return ___dx11BokehThreshold_21; }
	inline float* get_address_of_dx11BokehThreshold_21() { return &___dx11BokehThreshold_21; }
	inline void set_dx11BokehThreshold_21(float value)
	{
		___dx11BokehThreshold_21 = value;
	}

	inline static int32_t get_offset_of_dx11SpawnHeuristic_22() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11SpawnHeuristic_22)); }
	inline float get_dx11SpawnHeuristic_22() const { return ___dx11SpawnHeuristic_22; }
	inline float* get_address_of_dx11SpawnHeuristic_22() { return &___dx11SpawnHeuristic_22; }
	inline void set_dx11SpawnHeuristic_22(float value)
	{
		___dx11SpawnHeuristic_22 = value;
	}

	inline static int32_t get_offset_of_dx11BokehTexture_23() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11BokehTexture_23)); }
	inline Texture2D_t3063074017 * get_dx11BokehTexture_23() const { return ___dx11BokehTexture_23; }
	inline Texture2D_t3063074017 ** get_address_of_dx11BokehTexture_23() { return &___dx11BokehTexture_23; }
	inline void set_dx11BokehTexture_23(Texture2D_t3063074017 * value)
	{
		___dx11BokehTexture_23 = value;
		Il2CppCodeGenWriteBarrier((&___dx11BokehTexture_23), value);
	}

	inline static int32_t get_offset_of_dx11BokehScale_24() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11BokehScale_24)); }
	inline float get_dx11BokehScale_24() const { return ___dx11BokehScale_24; }
	inline float* get_address_of_dx11BokehScale_24() { return &___dx11BokehScale_24; }
	inline void set_dx11BokehScale_24(float value)
	{
		___dx11BokehScale_24 = value;
	}

	inline static int32_t get_offset_of_dx11BokehIntensity_25() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___dx11BokehIntensity_25)); }
	inline float get_dx11BokehIntensity_25() const { return ___dx11BokehIntensity_25; }
	inline float* get_address_of_dx11BokehIntensity_25() { return &___dx11BokehIntensity_25; }
	inline void set_dx11BokehIntensity_25(float value)
	{
		___dx11BokehIntensity_25 = value;
	}

	inline static int32_t get_offset_of_focalDistance01_26() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___focalDistance01_26)); }
	inline float get_focalDistance01_26() const { return ___focalDistance01_26; }
	inline float* get_address_of_focalDistance01_26() { return &___focalDistance01_26; }
	inline void set_focalDistance01_26(float value)
	{
		___focalDistance01_26 = value;
	}

	inline static int32_t get_offset_of_cbDrawArgs_27() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___cbDrawArgs_27)); }
	inline ComputeBuffer_t2358285523 * get_cbDrawArgs_27() const { return ___cbDrawArgs_27; }
	inline ComputeBuffer_t2358285523 ** get_address_of_cbDrawArgs_27() { return &___cbDrawArgs_27; }
	inline void set_cbDrawArgs_27(ComputeBuffer_t2358285523 * value)
	{
		___cbDrawArgs_27 = value;
		Il2CppCodeGenWriteBarrier((&___cbDrawArgs_27), value);
	}

	inline static int32_t get_offset_of_cbPoints_28() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___cbPoints_28)); }
	inline ComputeBuffer_t2358285523 * get_cbPoints_28() const { return ___cbPoints_28; }
	inline ComputeBuffer_t2358285523 ** get_address_of_cbPoints_28() { return &___cbPoints_28; }
	inline void set_cbPoints_28(ComputeBuffer_t2358285523 * value)
	{
		___cbPoints_28 = value;
		Il2CppCodeGenWriteBarrier((&___cbPoints_28), value);
	}

	inline static int32_t get_offset_of_internalBlurWidth_29() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___internalBlurWidth_29)); }
	inline float get_internalBlurWidth_29() const { return ___internalBlurWidth_29; }
	inline float* get_address_of_internalBlurWidth_29() { return &___internalBlurWidth_29; }
	inline void set_internalBlurWidth_29(float value)
	{
		___internalBlurWidth_29 = value;
	}

	inline static int32_t get_offset_of_cachedCamera_30() { return static_cast<int32_t>(offsetof(DepthOfField_t195334042, ___cachedCamera_30)); }
	inline Camera_t2839736942 * get_cachedCamera_30() const { return ___cachedCamera_30; }
	inline Camera_t2839736942 ** get_address_of_cachedCamera_30() { return &___cachedCamera_30; }
	inline void set_cachedCamera_30(Camera_t2839736942 * value)
	{
		___cachedCamera_30 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELD_T195334042_H
#ifndef COLORCORRECTIONCURVES_T2809218626_H
#define COLORCORRECTIONCURVES_T2809218626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
struct  ColorCorrectionCurves_t2809218626  : public PostEffectsBase_t1831076792
{
public:
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::redChannel
	AnimationCurve_t2757045165 * ___redChannel_6;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::greenChannel
	AnimationCurve_t2757045165 * ___greenChannel_7;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::blueChannel
	AnimationCurve_t2757045165 * ___blueChannel_8;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::useDepthCorrection
	bool ___useDepthCorrection_9;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::zCurve
	AnimationCurve_t2757045165 * ___zCurve_10;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthRedChannel
	AnimationCurve_t2757045165 * ___depthRedChannel_11;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthGreenChannel
	AnimationCurve_t2757045165 * ___depthGreenChannel_12;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthBlueChannel
	AnimationCurve_t2757045165 * ___depthBlueChannel_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::ccMaterial
	Material_t2815264910 * ___ccMaterial_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::ccDepthMaterial
	Material_t2815264910 * ___ccDepthMaterial_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveCcMaterial
	Material_t2815264910 * ___selectiveCcMaterial_16;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::rgbChannelTex
	Texture2D_t3063074017 * ___rgbChannelTex_17;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::rgbDepthChannelTex
	Texture2D_t3063074017 * ___rgbDepthChannelTex_18;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::zCurveTex
	Texture2D_t3063074017 * ___zCurveTex_19;
	// System.Single UnityStandardAssets.ImageEffects.ColorCorrectionCurves::saturation
	float ___saturation_20;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveCc
	bool ___selectiveCc_21;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveFromColor
	Color_t2582018970  ___selectiveFromColor_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveToColor
	Color_t2582018970  ___selectiveToColor_23;
	// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode UnityStandardAssets.ImageEffects.ColorCorrectionCurves::mode
	int32_t ___mode_24;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::updateTextures
	bool ___updateTextures_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::colorCorrectionCurvesShader
	Shader_t1881769421 * ___colorCorrectionCurvesShader_26;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::simpleColorCorrectionCurvesShader
	Shader_t1881769421 * ___simpleColorCorrectionCurvesShader_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::colorCorrectionSelectiveShader
	Shader_t1881769421 * ___colorCorrectionSelectiveShader_28;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::updateTexturesOnStartup
	bool ___updateTexturesOnStartup_29;

public:
	inline static int32_t get_offset_of_redChannel_6() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___redChannel_6)); }
	inline AnimationCurve_t2757045165 * get_redChannel_6() const { return ___redChannel_6; }
	inline AnimationCurve_t2757045165 ** get_address_of_redChannel_6() { return &___redChannel_6; }
	inline void set_redChannel_6(AnimationCurve_t2757045165 * value)
	{
		___redChannel_6 = value;
		Il2CppCodeGenWriteBarrier((&___redChannel_6), value);
	}

	inline static int32_t get_offset_of_greenChannel_7() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___greenChannel_7)); }
	inline AnimationCurve_t2757045165 * get_greenChannel_7() const { return ___greenChannel_7; }
	inline AnimationCurve_t2757045165 ** get_address_of_greenChannel_7() { return &___greenChannel_7; }
	inline void set_greenChannel_7(AnimationCurve_t2757045165 * value)
	{
		___greenChannel_7 = value;
		Il2CppCodeGenWriteBarrier((&___greenChannel_7), value);
	}

	inline static int32_t get_offset_of_blueChannel_8() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___blueChannel_8)); }
	inline AnimationCurve_t2757045165 * get_blueChannel_8() const { return ___blueChannel_8; }
	inline AnimationCurve_t2757045165 ** get_address_of_blueChannel_8() { return &___blueChannel_8; }
	inline void set_blueChannel_8(AnimationCurve_t2757045165 * value)
	{
		___blueChannel_8 = value;
		Il2CppCodeGenWriteBarrier((&___blueChannel_8), value);
	}

	inline static int32_t get_offset_of_useDepthCorrection_9() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___useDepthCorrection_9)); }
	inline bool get_useDepthCorrection_9() const { return ___useDepthCorrection_9; }
	inline bool* get_address_of_useDepthCorrection_9() { return &___useDepthCorrection_9; }
	inline void set_useDepthCorrection_9(bool value)
	{
		___useDepthCorrection_9 = value;
	}

	inline static int32_t get_offset_of_zCurve_10() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___zCurve_10)); }
	inline AnimationCurve_t2757045165 * get_zCurve_10() const { return ___zCurve_10; }
	inline AnimationCurve_t2757045165 ** get_address_of_zCurve_10() { return &___zCurve_10; }
	inline void set_zCurve_10(AnimationCurve_t2757045165 * value)
	{
		___zCurve_10 = value;
		Il2CppCodeGenWriteBarrier((&___zCurve_10), value);
	}

	inline static int32_t get_offset_of_depthRedChannel_11() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___depthRedChannel_11)); }
	inline AnimationCurve_t2757045165 * get_depthRedChannel_11() const { return ___depthRedChannel_11; }
	inline AnimationCurve_t2757045165 ** get_address_of_depthRedChannel_11() { return &___depthRedChannel_11; }
	inline void set_depthRedChannel_11(AnimationCurve_t2757045165 * value)
	{
		___depthRedChannel_11 = value;
		Il2CppCodeGenWriteBarrier((&___depthRedChannel_11), value);
	}

	inline static int32_t get_offset_of_depthGreenChannel_12() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___depthGreenChannel_12)); }
	inline AnimationCurve_t2757045165 * get_depthGreenChannel_12() const { return ___depthGreenChannel_12; }
	inline AnimationCurve_t2757045165 ** get_address_of_depthGreenChannel_12() { return &___depthGreenChannel_12; }
	inline void set_depthGreenChannel_12(AnimationCurve_t2757045165 * value)
	{
		___depthGreenChannel_12 = value;
		Il2CppCodeGenWriteBarrier((&___depthGreenChannel_12), value);
	}

	inline static int32_t get_offset_of_depthBlueChannel_13() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___depthBlueChannel_13)); }
	inline AnimationCurve_t2757045165 * get_depthBlueChannel_13() const { return ___depthBlueChannel_13; }
	inline AnimationCurve_t2757045165 ** get_address_of_depthBlueChannel_13() { return &___depthBlueChannel_13; }
	inline void set_depthBlueChannel_13(AnimationCurve_t2757045165 * value)
	{
		___depthBlueChannel_13 = value;
		Il2CppCodeGenWriteBarrier((&___depthBlueChannel_13), value);
	}

	inline static int32_t get_offset_of_ccMaterial_14() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___ccMaterial_14)); }
	inline Material_t2815264910 * get_ccMaterial_14() const { return ___ccMaterial_14; }
	inline Material_t2815264910 ** get_address_of_ccMaterial_14() { return &___ccMaterial_14; }
	inline void set_ccMaterial_14(Material_t2815264910 * value)
	{
		___ccMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___ccMaterial_14), value);
	}

	inline static int32_t get_offset_of_ccDepthMaterial_15() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___ccDepthMaterial_15)); }
	inline Material_t2815264910 * get_ccDepthMaterial_15() const { return ___ccDepthMaterial_15; }
	inline Material_t2815264910 ** get_address_of_ccDepthMaterial_15() { return &___ccDepthMaterial_15; }
	inline void set_ccDepthMaterial_15(Material_t2815264910 * value)
	{
		___ccDepthMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___ccDepthMaterial_15), value);
	}

	inline static int32_t get_offset_of_selectiveCcMaterial_16() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___selectiveCcMaterial_16)); }
	inline Material_t2815264910 * get_selectiveCcMaterial_16() const { return ___selectiveCcMaterial_16; }
	inline Material_t2815264910 ** get_address_of_selectiveCcMaterial_16() { return &___selectiveCcMaterial_16; }
	inline void set_selectiveCcMaterial_16(Material_t2815264910 * value)
	{
		___selectiveCcMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectiveCcMaterial_16), value);
	}

	inline static int32_t get_offset_of_rgbChannelTex_17() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___rgbChannelTex_17)); }
	inline Texture2D_t3063074017 * get_rgbChannelTex_17() const { return ___rgbChannelTex_17; }
	inline Texture2D_t3063074017 ** get_address_of_rgbChannelTex_17() { return &___rgbChannelTex_17; }
	inline void set_rgbChannelTex_17(Texture2D_t3063074017 * value)
	{
		___rgbChannelTex_17 = value;
		Il2CppCodeGenWriteBarrier((&___rgbChannelTex_17), value);
	}

	inline static int32_t get_offset_of_rgbDepthChannelTex_18() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___rgbDepthChannelTex_18)); }
	inline Texture2D_t3063074017 * get_rgbDepthChannelTex_18() const { return ___rgbDepthChannelTex_18; }
	inline Texture2D_t3063074017 ** get_address_of_rgbDepthChannelTex_18() { return &___rgbDepthChannelTex_18; }
	inline void set_rgbDepthChannelTex_18(Texture2D_t3063074017 * value)
	{
		___rgbDepthChannelTex_18 = value;
		Il2CppCodeGenWriteBarrier((&___rgbDepthChannelTex_18), value);
	}

	inline static int32_t get_offset_of_zCurveTex_19() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___zCurveTex_19)); }
	inline Texture2D_t3063074017 * get_zCurveTex_19() const { return ___zCurveTex_19; }
	inline Texture2D_t3063074017 ** get_address_of_zCurveTex_19() { return &___zCurveTex_19; }
	inline void set_zCurveTex_19(Texture2D_t3063074017 * value)
	{
		___zCurveTex_19 = value;
		Il2CppCodeGenWriteBarrier((&___zCurveTex_19), value);
	}

	inline static int32_t get_offset_of_saturation_20() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___saturation_20)); }
	inline float get_saturation_20() const { return ___saturation_20; }
	inline float* get_address_of_saturation_20() { return &___saturation_20; }
	inline void set_saturation_20(float value)
	{
		___saturation_20 = value;
	}

	inline static int32_t get_offset_of_selectiveCc_21() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___selectiveCc_21)); }
	inline bool get_selectiveCc_21() const { return ___selectiveCc_21; }
	inline bool* get_address_of_selectiveCc_21() { return &___selectiveCc_21; }
	inline void set_selectiveCc_21(bool value)
	{
		___selectiveCc_21 = value;
	}

	inline static int32_t get_offset_of_selectiveFromColor_22() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___selectiveFromColor_22)); }
	inline Color_t2582018970  get_selectiveFromColor_22() const { return ___selectiveFromColor_22; }
	inline Color_t2582018970 * get_address_of_selectiveFromColor_22() { return &___selectiveFromColor_22; }
	inline void set_selectiveFromColor_22(Color_t2582018970  value)
	{
		___selectiveFromColor_22 = value;
	}

	inline static int32_t get_offset_of_selectiveToColor_23() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___selectiveToColor_23)); }
	inline Color_t2582018970  get_selectiveToColor_23() const { return ___selectiveToColor_23; }
	inline Color_t2582018970 * get_address_of_selectiveToColor_23() { return &___selectiveToColor_23; }
	inline void set_selectiveToColor_23(Color_t2582018970  value)
	{
		___selectiveToColor_23 = value;
	}

	inline static int32_t get_offset_of_mode_24() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___mode_24)); }
	inline int32_t get_mode_24() const { return ___mode_24; }
	inline int32_t* get_address_of_mode_24() { return &___mode_24; }
	inline void set_mode_24(int32_t value)
	{
		___mode_24 = value;
	}

	inline static int32_t get_offset_of_updateTextures_25() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___updateTextures_25)); }
	inline bool get_updateTextures_25() const { return ___updateTextures_25; }
	inline bool* get_address_of_updateTextures_25() { return &___updateTextures_25; }
	inline void set_updateTextures_25(bool value)
	{
		___updateTextures_25 = value;
	}

	inline static int32_t get_offset_of_colorCorrectionCurvesShader_26() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___colorCorrectionCurvesShader_26)); }
	inline Shader_t1881769421 * get_colorCorrectionCurvesShader_26() const { return ___colorCorrectionCurvesShader_26; }
	inline Shader_t1881769421 ** get_address_of_colorCorrectionCurvesShader_26() { return &___colorCorrectionCurvesShader_26; }
	inline void set_colorCorrectionCurvesShader_26(Shader_t1881769421 * value)
	{
		___colorCorrectionCurvesShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___colorCorrectionCurvesShader_26), value);
	}

	inline static int32_t get_offset_of_simpleColorCorrectionCurvesShader_27() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___simpleColorCorrectionCurvesShader_27)); }
	inline Shader_t1881769421 * get_simpleColorCorrectionCurvesShader_27() const { return ___simpleColorCorrectionCurvesShader_27; }
	inline Shader_t1881769421 ** get_address_of_simpleColorCorrectionCurvesShader_27() { return &___simpleColorCorrectionCurvesShader_27; }
	inline void set_simpleColorCorrectionCurvesShader_27(Shader_t1881769421 * value)
	{
		___simpleColorCorrectionCurvesShader_27 = value;
		Il2CppCodeGenWriteBarrier((&___simpleColorCorrectionCurvesShader_27), value);
	}

	inline static int32_t get_offset_of_colorCorrectionSelectiveShader_28() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___colorCorrectionSelectiveShader_28)); }
	inline Shader_t1881769421 * get_colorCorrectionSelectiveShader_28() const { return ___colorCorrectionSelectiveShader_28; }
	inline Shader_t1881769421 ** get_address_of_colorCorrectionSelectiveShader_28() { return &___colorCorrectionSelectiveShader_28; }
	inline void set_colorCorrectionSelectiveShader_28(Shader_t1881769421 * value)
	{
		___colorCorrectionSelectiveShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___colorCorrectionSelectiveShader_28), value);
	}

	inline static int32_t get_offset_of_updateTexturesOnStartup_29() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t2809218626, ___updateTexturesOnStartup_29)); }
	inline bool get_updateTexturesOnStartup_29() const { return ___updateTexturesOnStartup_29; }
	inline bool* get_address_of_updateTexturesOnStartup_29() { return &___updateTexturesOnStartup_29; }
	inline void set_updateTexturesOnStartup_29(bool value)
	{
		___updateTexturesOnStartup_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONCURVES_T2809218626_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (AnimationOutputWeightProcessor_t3652584005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[3] = 
{
	AnimationOutputWeightProcessor_t3652584005::get_offset_of_m_Output_0(),
	AnimationOutputWeightProcessor_t3652584005::get_offset_of_m_LayerMixer_1(),
	AnimationOutputWeightProcessor_t3652584005::get_offset_of_m_Mixers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (WeightInfo_t4162100259)+ sizeof (RuntimeObject), sizeof(WeightInfo_t4162100259_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[4] = 
{
	WeightInfo_t4162100259::get_offset_of_mixer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t4162100259::get_offset_of_parentMixer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t4162100259::get_offset_of_port_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t4162100259::get_offset_of_modulate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (AnimationPlayableAsset_t4235477116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[7] = 
{
	AnimationPlayableAsset_t4235477116::get_offset_of_m_Clip_2(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_Position_3(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_Rotation_4(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_UseTrackMatchFields_5(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_MatchTargetFields_6(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_RemoveStartOffset_7(),
	AnimationPlayableAsset_t4235477116::get_offset_of_m_AnimationClipPlayable_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (U3CU3Ec__Iterator0_t2002482455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	U3CU3Ec__Iterator0_t2002482455::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t2002482455::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t2002482455::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (MatchTargetFields_t2467835737)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[7] = 
{
	MatchTargetFields_t2467835737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (MatchTargetFieldConstants_t1045040963), -1, sizeof(MatchTargetFieldConstants_t1045040963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	MatchTargetFieldConstants_t1045040963_StaticFields::get_offset_of_All_0(),
	MatchTargetFieldConstants_t1045040963_StaticFields::get_offset_of_None_1(),
	MatchTargetFieldConstants_t1045040963_StaticFields::get_offset_of_Position_2(),
	MatchTargetFieldConstants_t1045040963_StaticFields::get_offset_of_Rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (AnimationTrack_t200707209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[13] = 
{
	AnimationTrack_t200707209::get_offset_of_m_AnimationPlayableAsset_16(),
	AnimationTrack_t200707209::get_offset_of_m_FakeAnimClip_17(),
	AnimationTrack_t200707209::get_offset_of_m_OpenClipPreExtrapolation_18(),
	AnimationTrack_t200707209::get_offset_of_m_OpenClipPostExtrapolation_19(),
	AnimationTrack_t200707209::get_offset_of_m_OpenClipOffsetPosition_20(),
	AnimationTrack_t200707209::get_offset_of_m_OpenClipOffsetRotation_21(),
	AnimationTrack_t200707209::get_offset_of_m_OpenClipTimeOffset_22(),
	AnimationTrack_t200707209::get_offset_of_m_MatchTargetFields_23(),
	AnimationTrack_t200707209::get_offset_of_m_Position_24(),
	AnimationTrack_t200707209::get_offset_of_m_Rotation_25(),
	AnimationTrack_t200707209::get_offset_of_m_ApplyOffsets_26(),
	AnimationTrack_t200707209::get_offset_of_m_AvatarMask_27(),
	AnimationTrack_t200707209::get_offset_of_m_ApplyAvatarMask_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CU3Ec__Iterator0_t1763991329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[4] = 
{
	U3CU3Ec__Iterator0_t1763991329::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t1763991329::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t1763991329::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t1763991329::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (TrackColorAttribute_t915884494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[1] = 
{
	TrackColorAttribute_t915884494::get_offset_of_m_Color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (AudioPlayableAsset_t3472402037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[3] = 
{
	AudioPlayableAsset_t3472402037::get_offset_of_m_Clip_2(),
	AudioPlayableAsset_t3472402037::get_offset_of_m_Loop_3(),
	AudioPlayableAsset_t3472402037::get_offset_of_m_bufferingTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (U3CU3Ec__Iterator0_t2455484034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	U3CU3Ec__Iterator0_t2455484034::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t2455484034::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t2455484034::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (AudioTrack_t2200009395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CU3Ec__Iterator0_t347944413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[4] = 
{
	U3CU3Ec__Iterator0_t347944413::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t347944413::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t347944413::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t347944413::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (ControlPlayableAsset_t1457549000), -1, sizeof(ControlPlayableAsset_t1457549000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2213[13] = 
{
	ControlPlayableAsset_t1457549000_StaticFields::get_offset_of_k_MaxRandInt_2(),
	ControlPlayableAsset_t1457549000::get_offset_of_sourceGameObject_3(),
	ControlPlayableAsset_t1457549000::get_offset_of_prefabGameObject_4(),
	ControlPlayableAsset_t1457549000::get_offset_of_updateParticle_5(),
	ControlPlayableAsset_t1457549000::get_offset_of_particleRandomSeed_6(),
	ControlPlayableAsset_t1457549000::get_offset_of_updateDirector_7(),
	ControlPlayableAsset_t1457549000::get_offset_of_updateITimeControl_8(),
	ControlPlayableAsset_t1457549000::get_offset_of_searchHierarchy_9(),
	ControlPlayableAsset_t1457549000::get_offset_of_active_10(),
	ControlPlayableAsset_t1457549000::get_offset_of_postPlayback_11(),
	ControlPlayableAsset_t1457549000::get_offset_of_m_ControlDirectorAsset_12(),
	ControlPlayableAsset_t1457549000::get_offset_of_m_Duration_13(),
	ControlPlayableAsset_t1457549000::get_offset_of_m_SupportLoop_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (U3CGetControlableScriptsU3Ec__Iterator0_t272318736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[7] = 
{
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_root_0(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U24locvar0_1(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U24locvar1_2(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U3CscriptU3E__1_3(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U24current_4(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U24disposing_5(),
	U3CGetControlableScriptsU3Ec__Iterator0_t272318736::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (ControlTrack_t3303583055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (RuntimeClip_t3327076603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	RuntimeClip_t3327076603::get_offset_of_m_Clip_1(),
	RuntimeClip_t3327076603::get_offset_of_m_Playable_2(),
	RuntimeClip_t3327076603::get_offset_of_m_ParentMixer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (RuntimeClipBase_t433944096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (RuntimeElement_t869620576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[1] = 
{
	RuntimeElement_t869620576::get_offset_of_U3CintervalBitU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ScheduleRuntimeClip_t3649402065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[7] = 
{
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_Clip_1(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_Playable_2(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_AudioClipPlayable_3(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_ParentMixer_4(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_StartDelay_5(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_FinishTail_6(),
	ScheduleRuntimeClip_t3649402065::get_offset_of_m_Started_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (TrackAssetExtensions_t2124117065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (ActivationControlPlayable_t3132244818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	ActivationControlPlayable_t3132244818::get_offset_of_gameObject_0(),
	ActivationControlPlayable_t3132244818::get_offset_of_postPlayback_1(),
	ActivationControlPlayable_t3132244818::get_offset_of_m_InitialState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (PostPlaybackState_t2766824872)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	PostPlaybackState_t2766824872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (InitialState_t2473441553)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[4] = 
{
	InitialState_t2473441553::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (BasicPlayableBehaviour_t3342818502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (DirectorControlPlayable_t2785872831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[1] = 
{
	DirectorControlPlayable_t2785872831::get_offset_of_director_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (EventPlayable_t1499726131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	EventPlayable_t1499726131::get_offset_of_U3CtriggerTimeU3Ek__BackingField_0(),
	EventPlayable_t1499726131::get_offset_of_m_HasFired_1(),
	EventPlayable_t1499726131::get_offset_of_m_PreviousTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (ParticleControlPlayable_t1573458720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[5] = 
{
	0,
	ParticleControlPlayable_t1573458720::get_offset_of_m_LastTime_1(),
	ParticleControlPlayable_t1573458720::get_offset_of_m_LastPsTime_2(),
	ParticleControlPlayable_t1573458720::get_offset_of_m_RandomSeed_3(),
	ParticleControlPlayable_t1573458720::get_offset_of_m_ParticleSystem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (PrefabControlPlayable_t470232467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[1] = 
{
	PrefabControlPlayable_t470232467::get_offset_of_m_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (TimeControlPlayable_t2938852893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[2] = 
{
	TimeControlPlayable_t2938852893::get_offset_of_m_timeControl_0(),
	TimeControlPlayable_t2938852893::get_offset_of_m_started_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (PlayableTrack_t4089958788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (Extrapolation_t2678001198), -1, sizeof(Extrapolation_t2678001198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	Extrapolation_t2678001198_StaticFields::get_offset_of_kMinExtrapolationTime_0(),
	Extrapolation_t2678001198_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (HashUtility_t3989591031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (TimelineCreateUtilities_t707790311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571::get_offset_of_prefix_0(),
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t3053161571::get_offset_of_newName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (TimelineUndo_t4178130704), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (TimeUtility_t2058592793), -1, sizeof(TimeUtility_t2058592793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[4] = 
{
	TimeUtility_t2058592793_StaticFields::get_offset_of_kTimeEpsilon_0(),
	TimeUtility_t2058592793_StaticFields::get_offset_of_kFrameRateEpsilon_1(),
	TimeUtility_t2058592793_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	TimeUtility_t2058592793_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (WeightUtility_t2995380413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CModuleU3E_t1429447286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (AxisTouchButton_t3269831460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[6] = 
{
	AxisTouchButton_t3269831460::get_offset_of_axisName_2(),
	AxisTouchButton_t3269831460::get_offset_of_axisValue_3(),
	AxisTouchButton_t3269831460::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3269831460::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3269831460::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3269831460::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (ButtonHandler_t2221835595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[1] = 
{
	ButtonHandler_t2221835595::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (CheckContolActive_t3413127716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[2] = 
{
	CheckContolActive_t3413127716::get_offset_of_bValidated_2(),
	CheckContolActive_t3413127716::get_offset_of_m_ObjectMobileController_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (CrossPlatformInputManager_t490741389), -1, sizeof(CrossPlatformInputManager_t490741389_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2249[3] = 
{
	CrossPlatformInputManager_t490741389_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t490741389_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t490741389_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (ActiveInputMethod_t2454902678)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2250[3] = 
{
	ActiveInputMethod_t2454902678::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (VirtualAxis_t3366748972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[3] = 
{
	VirtualAxis_t3366748972::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t3366748972::get_offset_of_m_Value_1(),
	VirtualAxis_t3366748972::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (VirtualButton_t1189965125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[5] = 
{
	VirtualButton_t1189965125::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t1189965125::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t1189965125::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t1189965125::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t1189965125::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (InputAxisScrollbar_t681389185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	InputAxisScrollbar_t681389185::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (Joystick_t1263661182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[15] = 
{
	Joystick_t1263661182::get_offset_of_m_ObjectMobileController_2(),
	Joystick_t1263661182::get_offset_of_m_ObjectMobileControllerMove_3(),
	Joystick_t1263661182::get_offset_of_m_ObjectMobileControllerSprint_4(),
	Joystick_t1263661182::get_offset_of_m_controlOffset_5(),
	Joystick_t1263661182::get_offset_of_MovementRange_6(),
	Joystick_t1263661182::get_offset_of_axesToUse_7(),
	Joystick_t1263661182::get_offset_of_horizontalAxisName_8(),
	Joystick_t1263661182::get_offset_of_verticalAxisName_9(),
	Joystick_t1263661182::get_offset_of_m_HorizontalVirtualAxis_10(),
	Joystick_t1263661182::get_offset_of_m_VerticalVirtualAxis_11(),
	Joystick_t1263661182::get_offset_of_m_StartPos_12(),
	Joystick_t1263661182::get_offset_of_m_UseX_13(),
	Joystick_t1263661182::get_offset_of_m_UseY_14(),
	Joystick_t1263661182::get_offset_of_rectTrans_15(),
	Joystick_t1263661182::get_offset_of_isLeft_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (AxisOption_t1081434817)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[5] = 
{
	AxisOption_t1081434817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (MobileControlRig_t208031424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[6] = 
{
	MobileControlRig_t208031424::get_offset_of_moveImage_2(),
	MobileControlRig_t208031424::get_offset_of_jumpImage_3(),
	MobileControlRig_t208031424::get_offset_of_m_ObjectMobileControllerMove_4(),
	MobileControlRig_t208031424::get_offset_of_m_ObjectMobileControllerSprint_5(),
	MobileControlRig_t208031424::get_offset_of_m_ObjectMobileController_LeftPosition_6(),
	MobileControlRig_t208031424::get_offset_of_m_ObjectMobileController_RightPosition_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (MobileInput_t3573436291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (StandaloneInput_t1224557719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TiltInput_t2714299038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[5] = 
{
	TiltInput_t2714299038::get_offset_of_mapping_2(),
	TiltInput_t2714299038::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t2714299038::get_offset_of_fullTiltAngle_4(),
	TiltInput_t2714299038::get_offset_of_centreAngleOffset_5(),
	TiltInput_t2714299038::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (AxisOptions_t2951597766)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[3] = 
{
	AxisOptions_t2951597766::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (AxisMapping_t2276377189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	AxisMapping_t2276377189::get_offset_of_type_0(),
	AxisMapping_t2276377189::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (MappingType_t1524902041)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[5] = 
{
	MappingType_t1524902041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (TouchPad_t372495057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[18] = 
{
	TouchPad_t372495057::get_offset_of_axesToUse_2(),
	TouchPad_t372495057::get_offset_of_controlStyle_3(),
	TouchPad_t372495057::get_offset_of_horizontalAxisName_4(),
	TouchPad_t372495057::get_offset_of_verticalAxisName_5(),
	TouchPad_t372495057::get_offset_of_Xsensitivity_6(),
	TouchPad_t372495057::get_offset_of_Ysensitivity_7(),
	TouchPad_t372495057::get_offset_of_m_StartPos_8(),
	TouchPad_t372495057::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t372495057::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t372495057::get_offset_of_m_UseX_11(),
	TouchPad_t372495057::get_offset_of_m_UseY_12(),
	TouchPad_t372495057::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t372495057::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t372495057::get_offset_of_m_Dragging_15(),
	TouchPad_t372495057::get_offset_of_m_Id_16(),
	TouchPad_t372495057::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t372495057::get_offset_of_m_Center_18(),
	TouchPad_t372495057::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (AxisOption_t1354449705)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[4] = 
{
	AxisOption_t1354449705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (ControlStyle_t611612146)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[4] = 
{
	ControlStyle_t611612146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (VirtualInput_t2652211791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[4] = 
{
	VirtualInput_t2652211791::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2652211791::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2652211791::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2652211791::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (AAMode_t657424638)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[8] = 
{
	AAMode_t657424638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (Antialiasing_t1399311265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[22] = 
{
	Antialiasing_t1399311265::get_offset_of_mode_6(),
	Antialiasing_t1399311265::get_offset_of_showGeneratedNormals_7(),
	Antialiasing_t1399311265::get_offset_of_offsetScale_8(),
	Antialiasing_t1399311265::get_offset_of_blurRadius_9(),
	Antialiasing_t1399311265::get_offset_of_edgeThresholdMin_10(),
	Antialiasing_t1399311265::get_offset_of_edgeThreshold_11(),
	Antialiasing_t1399311265::get_offset_of_edgeSharpness_12(),
	Antialiasing_t1399311265::get_offset_of_dlaaSharp_13(),
	Antialiasing_t1399311265::get_offset_of_ssaaShader_14(),
	Antialiasing_t1399311265::get_offset_of_ssaa_15(),
	Antialiasing_t1399311265::get_offset_of_dlaaShader_16(),
	Antialiasing_t1399311265::get_offset_of_dlaa_17(),
	Antialiasing_t1399311265::get_offset_of_nfaaShader_18(),
	Antialiasing_t1399311265::get_offset_of_nfaa_19(),
	Antialiasing_t1399311265::get_offset_of_shaderFXAAPreset2_20(),
	Antialiasing_t1399311265::get_offset_of_materialFXAAPreset2_21(),
	Antialiasing_t1399311265::get_offset_of_shaderFXAAPreset3_22(),
	Antialiasing_t1399311265::get_offset_of_materialFXAAPreset3_23(),
	Antialiasing_t1399311265::get_offset_of_shaderFXAAII_24(),
	Antialiasing_t1399311265::get_offset_of_materialFXAAII_25(),
	Antialiasing_t1399311265::get_offset_of_shaderFXAAIII_26(),
	Antialiasing_t1399311265::get_offset_of_materialFXAAIII_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (Bloom_t3558726986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[30] = 
{
	Bloom_t3558726986::get_offset_of_tweakMode_6(),
	Bloom_t3558726986::get_offset_of_screenBlendMode_7(),
	Bloom_t3558726986::get_offset_of_hdr_8(),
	Bloom_t3558726986::get_offset_of_doHdr_9(),
	Bloom_t3558726986::get_offset_of_sepBlurSpread_10(),
	Bloom_t3558726986::get_offset_of_quality_11(),
	Bloom_t3558726986::get_offset_of_bloomIntensity_12(),
	Bloom_t3558726986::get_offset_of_bloomThreshold_13(),
	Bloom_t3558726986::get_offset_of_bloomThresholdColor_14(),
	Bloom_t3558726986::get_offset_of_bloomBlurIterations_15(),
	Bloom_t3558726986::get_offset_of_hollywoodFlareBlurIterations_16(),
	Bloom_t3558726986::get_offset_of_flareRotation_17(),
	Bloom_t3558726986::get_offset_of_lensflareMode_18(),
	Bloom_t3558726986::get_offset_of_hollyStretchWidth_19(),
	Bloom_t3558726986::get_offset_of_lensflareIntensity_20(),
	Bloom_t3558726986::get_offset_of_lensflareThreshold_21(),
	Bloom_t3558726986::get_offset_of_lensFlareSaturation_22(),
	Bloom_t3558726986::get_offset_of_flareColorA_23(),
	Bloom_t3558726986::get_offset_of_flareColorB_24(),
	Bloom_t3558726986::get_offset_of_flareColorC_25(),
	Bloom_t3558726986::get_offset_of_flareColorD_26(),
	Bloom_t3558726986::get_offset_of_lensFlareVignetteMask_27(),
	Bloom_t3558726986::get_offset_of_lensFlareShader_28(),
	Bloom_t3558726986::get_offset_of_lensFlareMaterial_29(),
	Bloom_t3558726986::get_offset_of_screenBlendShader_30(),
	Bloom_t3558726986::get_offset_of_screenBlend_31(),
	Bloom_t3558726986::get_offset_of_blurAndFlaresShader_32(),
	Bloom_t3558726986::get_offset_of_blurAndFlaresMaterial_33(),
	Bloom_t3558726986::get_offset_of_brightPassFilterShader_34(),
	Bloom_t3558726986::get_offset_of_brightPassFilterMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (LensFlareStyle_t2381973294)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[4] = 
{
	LensFlareStyle_t2381973294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (TweakMode_t1384345914)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[3] = 
{
	TweakMode_t1384345914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (HDRBloomMode_t1836510126)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[4] = 
{
	HDRBloomMode_t1836510126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (BloomScreenBlendMode_t2757664361)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	BloomScreenBlendMode_t2757664361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (BloomQuality_t425680226)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[3] = 
{
	BloomQuality_t425680226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (LensflareStyle34_t1433830633)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2275[4] = 
{
	LensflareStyle34_t1433830633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (TweakMode34_t2469558863)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[3] = 
{
	TweakMode34_t2469558863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (HDRBloomMode_t3656197844)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2277[4] = 
{
	HDRBloomMode_t3656197844::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (BloomScreenBlendMode_t629878214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	BloomScreenBlendMode_t629878214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (BloomAndFlares_t1406318507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[34] = 
{
	BloomAndFlares_t1406318507::get_offset_of_tweakMode_6(),
	BloomAndFlares_t1406318507::get_offset_of_screenBlendMode_7(),
	BloomAndFlares_t1406318507::get_offset_of_hdr_8(),
	BloomAndFlares_t1406318507::get_offset_of_doHdr_9(),
	BloomAndFlares_t1406318507::get_offset_of_sepBlurSpread_10(),
	BloomAndFlares_t1406318507::get_offset_of_useSrcAlphaAsMask_11(),
	BloomAndFlares_t1406318507::get_offset_of_bloomIntensity_12(),
	BloomAndFlares_t1406318507::get_offset_of_bloomThreshold_13(),
	BloomAndFlares_t1406318507::get_offset_of_bloomBlurIterations_14(),
	BloomAndFlares_t1406318507::get_offset_of_lensflares_15(),
	BloomAndFlares_t1406318507::get_offset_of_hollywoodFlareBlurIterations_16(),
	BloomAndFlares_t1406318507::get_offset_of_lensflareMode_17(),
	BloomAndFlares_t1406318507::get_offset_of_hollyStretchWidth_18(),
	BloomAndFlares_t1406318507::get_offset_of_lensflareIntensity_19(),
	BloomAndFlares_t1406318507::get_offset_of_lensflareThreshold_20(),
	BloomAndFlares_t1406318507::get_offset_of_flareColorA_21(),
	BloomAndFlares_t1406318507::get_offset_of_flareColorB_22(),
	BloomAndFlares_t1406318507::get_offset_of_flareColorC_23(),
	BloomAndFlares_t1406318507::get_offset_of_flareColorD_24(),
	BloomAndFlares_t1406318507::get_offset_of_lensFlareVignetteMask_25(),
	BloomAndFlares_t1406318507::get_offset_of_lensFlareShader_26(),
	BloomAndFlares_t1406318507::get_offset_of_lensFlareMaterial_27(),
	BloomAndFlares_t1406318507::get_offset_of_vignetteShader_28(),
	BloomAndFlares_t1406318507::get_offset_of_vignetteMaterial_29(),
	BloomAndFlares_t1406318507::get_offset_of_separableBlurShader_30(),
	BloomAndFlares_t1406318507::get_offset_of_separableBlurMaterial_31(),
	BloomAndFlares_t1406318507::get_offset_of_addBrightStuffOneOneShader_32(),
	BloomAndFlares_t1406318507::get_offset_of_addBrightStuffBlendOneOneMaterial_33(),
	BloomAndFlares_t1406318507::get_offset_of_screenBlendShader_34(),
	BloomAndFlares_t1406318507::get_offset_of_screenBlend_35(),
	BloomAndFlares_t1406318507::get_offset_of_hollywoodFlaresShader_36(),
	BloomAndFlares_t1406318507::get_offset_of_hollywoodFlaresMaterial_37(),
	BloomAndFlares_t1406318507::get_offset_of_brightPassFilterShader_38(),
	BloomAndFlares_t1406318507::get_offset_of_brightPassFilterMaterial_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (BloomOptimized_t1465546345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[8] = 
{
	BloomOptimized_t1465546345::get_offset_of_threshold_6(),
	BloomOptimized_t1465546345::get_offset_of_intensity_7(),
	BloomOptimized_t1465546345::get_offset_of_blurSize_8(),
	BloomOptimized_t1465546345::get_offset_of_resolution_9(),
	BloomOptimized_t1465546345::get_offset_of_blurIterations_10(),
	BloomOptimized_t1465546345::get_offset_of_blurType_11(),
	BloomOptimized_t1465546345::get_offset_of_fastBloomShader_12(),
	BloomOptimized_t1465546345::get_offset_of_fastBloomMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (Resolution_t1117279058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2281[3] = 
{
	Resolution_t1117279058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (BlurType_t1352644979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2282[3] = 
{
	BlurType_t1352644979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (Blur_t2292599283), -1, sizeof(Blur_t2292599283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2283[4] = 
{
	Blur_t2292599283::get_offset_of_iterations_2(),
	Blur_t2292599283::get_offset_of_blurSpread_3(),
	Blur_t2292599283::get_offset_of_blurShader_4(),
	Blur_t2292599283_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (BlurOptimized_t3587832534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[6] = 
{
	BlurOptimized_t3587832534::get_offset_of_downsample_6(),
	BlurOptimized_t3587832534::get_offset_of_blurSize_7(),
	BlurOptimized_t3587832534::get_offset_of_blurIterations_8(),
	BlurOptimized_t3587832534::get_offset_of_blurType_9(),
	BlurOptimized_t3587832534::get_offset_of_blurShader_10(),
	BlurOptimized_t3587832534::get_offset_of_blurMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (BlurType_t56316822)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2285[3] = 
{
	BlurType_t56316822::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (CameraMotionBlur_t1021540318), -1, sizeof(CameraMotionBlur_t1021540318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2286[32] = 
{
	CameraMotionBlur_t1021540318_StaticFields::get_offset_of_MAX_RADIUS_6(),
	CameraMotionBlur_t1021540318::get_offset_of_filterType_7(),
	CameraMotionBlur_t1021540318::get_offset_of_preview_8(),
	CameraMotionBlur_t1021540318::get_offset_of_previewScale_9(),
	CameraMotionBlur_t1021540318::get_offset_of_movementScale_10(),
	CameraMotionBlur_t1021540318::get_offset_of_rotationScale_11(),
	CameraMotionBlur_t1021540318::get_offset_of_maxVelocity_12(),
	CameraMotionBlur_t1021540318::get_offset_of_minVelocity_13(),
	CameraMotionBlur_t1021540318::get_offset_of_velocityScale_14(),
	CameraMotionBlur_t1021540318::get_offset_of_softZDistance_15(),
	CameraMotionBlur_t1021540318::get_offset_of_velocityDownsample_16(),
	CameraMotionBlur_t1021540318::get_offset_of_excludeLayers_17(),
	CameraMotionBlur_t1021540318::get_offset_of_tmpCam_18(),
	CameraMotionBlur_t1021540318::get_offset_of_shader_19(),
	CameraMotionBlur_t1021540318::get_offset_of_dx11MotionBlurShader_20(),
	CameraMotionBlur_t1021540318::get_offset_of_replacementClear_21(),
	CameraMotionBlur_t1021540318::get_offset_of_motionBlurMaterial_22(),
	CameraMotionBlur_t1021540318::get_offset_of_dx11MotionBlurMaterial_23(),
	CameraMotionBlur_t1021540318::get_offset_of_noiseTexture_24(),
	CameraMotionBlur_t1021540318::get_offset_of_jitter_25(),
	CameraMotionBlur_t1021540318::get_offset_of_showVelocity_26(),
	CameraMotionBlur_t1021540318::get_offset_of_showVelocityScale_27(),
	CameraMotionBlur_t1021540318::get_offset_of_currentViewProjMat_28(),
	CameraMotionBlur_t1021540318::get_offset_of_currentStereoViewProjMat_29(),
	CameraMotionBlur_t1021540318::get_offset_of_prevViewProjMat_30(),
	CameraMotionBlur_t1021540318::get_offset_of_prevStereoViewProjMat_31(),
	CameraMotionBlur_t1021540318::get_offset_of_prevFrameCount_32(),
	CameraMotionBlur_t1021540318::get_offset_of_wasActive_33(),
	CameraMotionBlur_t1021540318::get_offset_of_prevFrameForward_34(),
	CameraMotionBlur_t1021540318::get_offset_of_prevFrameUp_35(),
	CameraMotionBlur_t1021540318::get_offset_of_prevFramePos_36(),
	CameraMotionBlur_t1021540318::get_offset_of__camera_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (MotionBlurFilter_t746967900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[6] = 
{
	MotionBlurFilter_t746967900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (ColorCorrectionCurves_t2809218626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[24] = 
{
	ColorCorrectionCurves_t2809218626::get_offset_of_redChannel_6(),
	ColorCorrectionCurves_t2809218626::get_offset_of_greenChannel_7(),
	ColorCorrectionCurves_t2809218626::get_offset_of_blueChannel_8(),
	ColorCorrectionCurves_t2809218626::get_offset_of_useDepthCorrection_9(),
	ColorCorrectionCurves_t2809218626::get_offset_of_zCurve_10(),
	ColorCorrectionCurves_t2809218626::get_offset_of_depthRedChannel_11(),
	ColorCorrectionCurves_t2809218626::get_offset_of_depthGreenChannel_12(),
	ColorCorrectionCurves_t2809218626::get_offset_of_depthBlueChannel_13(),
	ColorCorrectionCurves_t2809218626::get_offset_of_ccMaterial_14(),
	ColorCorrectionCurves_t2809218626::get_offset_of_ccDepthMaterial_15(),
	ColorCorrectionCurves_t2809218626::get_offset_of_selectiveCcMaterial_16(),
	ColorCorrectionCurves_t2809218626::get_offset_of_rgbChannelTex_17(),
	ColorCorrectionCurves_t2809218626::get_offset_of_rgbDepthChannelTex_18(),
	ColorCorrectionCurves_t2809218626::get_offset_of_zCurveTex_19(),
	ColorCorrectionCurves_t2809218626::get_offset_of_saturation_20(),
	ColorCorrectionCurves_t2809218626::get_offset_of_selectiveCc_21(),
	ColorCorrectionCurves_t2809218626::get_offset_of_selectiveFromColor_22(),
	ColorCorrectionCurves_t2809218626::get_offset_of_selectiveToColor_23(),
	ColorCorrectionCurves_t2809218626::get_offset_of_mode_24(),
	ColorCorrectionCurves_t2809218626::get_offset_of_updateTextures_25(),
	ColorCorrectionCurves_t2809218626::get_offset_of_colorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t2809218626::get_offset_of_simpleColorCorrectionCurvesShader_27(),
	ColorCorrectionCurves_t2809218626::get_offset_of_colorCorrectionSelectiveShader_28(),
	ColorCorrectionCurves_t2809218626::get_offset_of_updateTexturesOnStartup_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (ColorCorrectionMode_t4139101073)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	ColorCorrectionMode_t4139101073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (ColorCorrectionLookup_t4097657427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[4] = 
{
	ColorCorrectionLookup_t4097657427::get_offset_of_shader_6(),
	ColorCorrectionLookup_t4097657427::get_offset_of_material_7(),
	ColorCorrectionLookup_t4097657427::get_offset_of_converted3DLut_8(),
	ColorCorrectionLookup_t4097657427::get_offset_of_basedOnTempTex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (ColorCorrectionRamp_t3429778610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[1] = 
{
	ColorCorrectionRamp_t3429778610::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (ContrastEnhance_t1749987403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[7] = 
{
	ContrastEnhance_t1749987403::get_offset_of_intensity_6(),
	ContrastEnhance_t1749987403::get_offset_of_threshold_7(),
	ContrastEnhance_t1749987403::get_offset_of_separableBlurMaterial_8(),
	ContrastEnhance_t1749987403::get_offset_of_contrastCompositeMaterial_9(),
	ContrastEnhance_t1749987403::get_offset_of_blurSpread_10(),
	ContrastEnhance_t1749987403::get_offset_of_separableBlurShader_11(),
	ContrastEnhance_t1749987403::get_offset_of_contrastCompositeShader_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (ContrastStretch_t1477452256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[13] = 
{
	ContrastStretch_t1477452256::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t1477452256::get_offset_of_limitMinimum_3(),
	ContrastStretch_t1477452256::get_offset_of_limitMaximum_4(),
	ContrastStretch_t1477452256::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t1477452256::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t1477452256::get_offset_of_shaderLum_7(),
	ContrastStretch_t1477452256::get_offset_of_m_materialLum_8(),
	ContrastStretch_t1477452256::get_offset_of_shaderReduce_9(),
	ContrastStretch_t1477452256::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t1477452256::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t1477452256::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t1477452256::get_offset_of_shaderApply_13(),
	ContrastStretch_t1477452256::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (CreaseShading_t3763133077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[9] = 
{
	CreaseShading_t3763133077::get_offset_of_intensity_6(),
	CreaseShading_t3763133077::get_offset_of_softness_7(),
	CreaseShading_t3763133077::get_offset_of_spread_8(),
	CreaseShading_t3763133077::get_offset_of_blurShader_9(),
	CreaseShading_t3763133077::get_offset_of_blurMaterial_10(),
	CreaseShading_t3763133077::get_offset_of_depthFetchShader_11(),
	CreaseShading_t3763133077::get_offset_of_depthFetchMaterial_12(),
	CreaseShading_t3763133077::get_offset_of_creaseApplyShader_13(),
	CreaseShading_t3763133077::get_offset_of_creaseApplyMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (DepthOfField_t195334042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[25] = 
{
	DepthOfField_t195334042::get_offset_of_visualizeFocus_6(),
	DepthOfField_t195334042::get_offset_of_focalLength_7(),
	DepthOfField_t195334042::get_offset_of_focalSize_8(),
	DepthOfField_t195334042::get_offset_of_aperture_9(),
	DepthOfField_t195334042::get_offset_of_focalTransform_10(),
	DepthOfField_t195334042::get_offset_of_maxBlurSize_11(),
	DepthOfField_t195334042::get_offset_of_highResolution_12(),
	DepthOfField_t195334042::get_offset_of_blurType_13(),
	DepthOfField_t195334042::get_offset_of_blurSampleCount_14(),
	DepthOfField_t195334042::get_offset_of_nearBlur_15(),
	DepthOfField_t195334042::get_offset_of_foregroundOverlap_16(),
	DepthOfField_t195334042::get_offset_of_dofHdrShader_17(),
	DepthOfField_t195334042::get_offset_of_dofHdrMaterial_18(),
	DepthOfField_t195334042::get_offset_of_dx11BokehShader_19(),
	DepthOfField_t195334042::get_offset_of_dx11bokehMaterial_20(),
	DepthOfField_t195334042::get_offset_of_dx11BokehThreshold_21(),
	DepthOfField_t195334042::get_offset_of_dx11SpawnHeuristic_22(),
	DepthOfField_t195334042::get_offset_of_dx11BokehTexture_23(),
	DepthOfField_t195334042::get_offset_of_dx11BokehScale_24(),
	DepthOfField_t195334042::get_offset_of_dx11BokehIntensity_25(),
	DepthOfField_t195334042::get_offset_of_focalDistance01_26(),
	DepthOfField_t195334042::get_offset_of_cbDrawArgs_27(),
	DepthOfField_t195334042::get_offset_of_cbPoints_28(),
	DepthOfField_t195334042::get_offset_of_internalBlurWidth_29(),
	DepthOfField_t195334042::get_offset_of_cachedCamera_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (BlurType_t3292036609)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2296[3] = 
{
	BlurType_t3292036609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (BlurSampleCount_t3090947702)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2297[4] = 
{
	BlurSampleCount_t3090947702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (DepthOfFieldDeprecated_t4224030657), -1, sizeof(DepthOfFieldDeprecated_t4224030657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[43] = 
{
	DepthOfFieldDeprecated_t4224030657_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6(),
	DepthOfFieldDeprecated_t4224030657_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_7(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_quality_8(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_resolution_9(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_simpleTweakMode_10(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalPoint_11(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_smoothness_12(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalZDistance_13(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalZStartCurve_14(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalZEndCurve_15(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalStartCurve_16(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalEndCurve_17(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalDistance01_18(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_objectFocus_19(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_focalSize_20(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bluriness_21(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_maxBlurSpread_22(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_foregroundBlurExtrude_23(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_dofBlurShader_24(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_dofBlurMaterial_25(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_dofShader_26(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_dofMaterial_27(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_visualize_28(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehDestination_29(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_widthOverHeight_30(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_oneOverBaseSize_31(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokeh_32(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehSupport_33(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehShader_34(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehTexture_35(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehScale_36(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehIntensity_37(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehThresholdContrast_38(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehThresholdLuminance_39(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehDownsample_40(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehMaterial_41(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of__camera_42(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_foregroundTexture_43(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_mediumRezWorkTexture_44(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_finalDefocus_45(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_lowRezWorkTexture_46(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehSource_47(),
	DepthOfFieldDeprecated_t4224030657::get_offset_of_bokehSource2_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (Dof34QualitySetting_t1186080811)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[3] = 
{
	Dof34QualitySetting_t1186080811::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
