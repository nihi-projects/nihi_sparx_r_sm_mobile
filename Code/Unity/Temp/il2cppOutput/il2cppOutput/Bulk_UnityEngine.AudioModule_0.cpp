﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.InvalidCastException
struct InvalidCastException_t3988541634;
// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1419814565;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.AudioSource
struct AudioSource_t4025721661;
// UnityEngine.AudioAmbisonicExtensionDefinition
struct AudioAmbisonicExtensionDefinition_t4140913893;
// UnityEngine.AudioExtensionDefinition
struct AudioExtensionDefinition_t2916689971;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t2245753777;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t2684603915;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// System.Type
struct Type_t;
// System.String[]
struct StringU5BU5D_t2511808107;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t1155560466;
// System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition>
struct List_1_t1165541249;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t968528272;
// UnityEngine.AudioSpatializerExtensionDefinition
struct AudioSpatializerExtensionDefinition_t2045914242;
// System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition>
struct List_1_t3260540900;
// UnityEngine.AudioListenerExtension
struct AudioListenerExtension_t2090751094;
// UnityEngine.AudioListener
struct AudioListener_t3253228504;
// System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>
struct List_1_t275187473;
// UnityEngine.Behaviour
struct Behaviour_t2850977393;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t2715435404;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// UnityEngine.AudioAmbisonicExtensionDefinition[]
struct AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.AudioSpatializerExtensionDefinition[]
struct AudioSpatializerExtensionDefinitionU5BU5D_t1095391031;
// UnityEngine.AudioSourceExtension[]
struct AudioSourceExtensionU5BU5D_t1191882599;
// System.Void
struct Void_t653366341;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Reflection.MemberFilter
struct MemberFilter_t2357198184;

extern RuntimeClass* InvalidCastException_t3988541634_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1694235800;
extern const uint32_t AudioClipPlayable__ctor_m2881294493_MetadataUsageId;
extern RuntimeClass* Double_t3752657471_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3419099923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2549451288;
extern Il2CppCodeGenString* _stringLiteral1790281158;
extern const uint32_t AudioClipPlayable_ValidateStartDelayInternal_m3862081530_MetadataUsageId;
extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349_RuntimeMethod_var;
extern const uint32_t AudioClipPlayable_Create_m2400233086_MetadataUsageId;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2408473799;
extern const uint32_t AudioMixerPlayable__ctor_m342058380_MetadataUsageId;
extern const RuntimeMethod* PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral980058864;
extern const uint32_t AudioPlayableOutput__ctor_m349506094_MetadataUsageId;
extern const uint32_t AudioClip__ctor_m891747244_MetadataUsageId;
extern RuntimeClass* Int32_t972567508_il2cpp_TypeInfo_var;
extern const uint32_t PCMSetPositionCallback_BeginInvoke_m2102179571_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t2511808107_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2957364442;
extern Il2CppCodeGenString* _stringLiteral591250;
extern const uint32_t AudioExtensionDefinition_GetExtensionType_m513008212_MetadataUsageId;
extern RuntimeClass* AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m4108440129_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2378857811_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3997841088_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3350638329_RuntimeMethod_var;
extern const uint32_t AudioExtensionManager_AddSpatializerExtension_m375267302_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m269488467_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3859005127_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1883253745_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2943463037_RuntimeMethod_var;
extern const uint32_t AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134_MetadataUsageId;
extern const uint32_t AudioExtensionManager_WriteExtensionProperties_m1007984039_MetadataUsageId;
extern const uint32_t AudioExtensionManager_AddSpatializerExtension_m837049305_MetadataUsageId;
extern const uint32_t AudioExtensionManager_WriteExtensionProperties_m3816343998_MetadataUsageId;
extern const uint32_t AudioExtensionManager_GetListenerSpatializerExtensionType_m3630031100_MetadataUsageId;
extern const uint32_t AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m422144144_MetadataUsageId;
extern const uint32_t AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m29487416_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m789375335_RuntimeMethod_var;
extern const uint32_t AudioExtensionManager_AddExtensionToManager_m2092709385_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m609089971_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_m2969022274_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m4186794102_RuntimeMethod_var;
extern const uint32_t AudioExtensionManager_RemoveExtensionFromManager_m3149216797_MetadataUsageId;
extern RuntimeClass* AudioListener_t3253228504_il2cpp_TypeInfo_var;
extern const uint32_t AudioExtensionManager_Update_m3577106864_MetadataUsageId;
extern const uint32_t AudioExtensionManager_GetReadyToPlay_m3146133677_MetadataUsageId;
extern RuntimeClass* AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1475799231;
extern Il2CppCodeGenString* _stringLiteral577122778;
extern Il2CppCodeGenString* _stringLiteral1656762229;
extern Il2CppCodeGenString* _stringLiteral1623959416;
extern Il2CppCodeGenString* _stringLiteral1708419827;
extern Il2CppCodeGenString* _stringLiteral3796107019;
extern Il2CppCodeGenString* _stringLiteral873129047;
extern Il2CppCodeGenString* _stringLiteral3572251999;
extern Il2CppCodeGenString* _stringLiteral227735072;
extern Il2CppCodeGenString* _stringLiteral1058676305;
extern const uint32_t AudioExtensionManager_RegisterBuiltinDefinitions_m600067160_MetadataUsageId;
extern RuntimeClass* AudioSpatializerExtensionDefinition_t2045914242_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m2413286470_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2661309874;
extern Il2CppCodeGenString* _stringLiteral3186058801;
extern const uint32_t AudioExtensionManager_RegisterListenerSpatializerDefinition_m440281069_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2133610319;
extern Il2CppCodeGenString* _stringLiteral3429621635;
extern const uint32_t AudioExtensionManager_RegisterSourceSpatializerDefinition_m349058419_MetadataUsageId;
extern RuntimeClass* AudioAmbisonicExtensionDefinition_t4140913893_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m2970273886_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1978627438;
extern Il2CppCodeGenString* _stringLiteral329553197;
extern const uint32_t AudioExtensionManager_RegisterSourceAmbisonicDefinition_m765937035_MetadataUsageId;
extern RuntimeClass* List_1_t1165541249_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3260540900_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t275187473_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m57615730_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1918620480_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m4205201701_RuntimeMethod_var;
extern const uint32_t AudioExtensionManager__cctor_m2972930274_MetadataUsageId;
extern RuntimeClass* AudioListenerExtension_t2090751094_il2cpp_TypeInfo_var;
extern const uint32_t AudioListener_AddExtension_m2595025457_MetadataUsageId;
extern RuntimeClass* AudioSettings_t1374326787_il2cpp_TypeInfo_var;
extern const uint32_t AudioSettings_InvokeOnAudioConfigurationChanged_m2141630212_MetadataUsageId;
extern const uint32_t AudioSettings_InvokeOnAudioManagerUpdate_m2143358649_MetadataUsageId;
extern const uint32_t AudioSettings_InvokeOnAudioSourcePlay_m239283393_MetadataUsageId;
extern RuntimeClass* Boolean_t3624619635_il2cpp_TypeInfo_var;
extern const uint32_t AudioConfigurationChangeHandler_BeginInvoke_m138753585_MetadataUsageId;
extern const uint32_t AudioSource_PlayOneShot_m1662760097_MetadataUsageId;
extern RuntimeClass* AudioSourceExtension_t1155560466_il2cpp_TypeInfo_var;
extern const uint32_t AudioSource_AddSpatializerExtension_m699023343_MetadataUsageId;
extern const uint32_t AudioSource_AddAmbisonicExtension_m1752284328_MetadataUsageId;

struct SingleU5BU5D_t2843050510;
struct StringU5BU5D_t2511808107;


#ifndef U3CMODULEU3E_T1429447267_H
#define U3CMODULEU3E_T1429447267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447267 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447267_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef LIST_1_T3260540900_H
#define LIST_1_T3260540900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition>
struct  List_1_t3260540900  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3260540900, ____items_1)); }
	inline AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* get__items_1() const { return ____items_1; }
	inline AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3260540900, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3260540900, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3260540900_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3260540900_StaticFields, ___EmptyArray_4)); }
	inline AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AudioAmbisonicExtensionDefinitionU5BU5D_t2423163368* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3260540900_H
#ifndef AUDIOPLAYABLEGRAPHEXTENSIONS_T3364587526_H
#define AUDIOPLAYABLEGRAPHEXTENSIONS_T3364587526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.AudioPlayableGraphExtensions
struct  AudioPlayableGraphExtensions_t3364587526  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEGRAPHEXTENSIONS_T3364587526_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef AUDIOSETTINGS_T1374326787_H
#define AUDIOSETTINGS_T1374326787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSettings
struct  AudioSettings_t1374326787  : public RuntimeObject
{
public:

public:
};

struct AudioSettings_t1374326787_StaticFields
{
public:
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t2715435404 * ___OnAudioConfigurationChanged_0;

public:
	inline static int32_t get_offset_of_OnAudioConfigurationChanged_0() { return static_cast<int32_t>(offsetof(AudioSettings_t1374326787_StaticFields, ___OnAudioConfigurationChanged_0)); }
	inline AudioConfigurationChangeHandler_t2715435404 * get_OnAudioConfigurationChanged_0() const { return ___OnAudioConfigurationChanged_0; }
	inline AudioConfigurationChangeHandler_t2715435404 ** get_address_of_OnAudioConfigurationChanged_0() { return &___OnAudioConfigurationChanged_0; }
	inline void set_OnAudioConfigurationChanged_0(AudioConfigurationChangeHandler_t2715435404 * value)
	{
		___OnAudioConfigurationChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAudioConfigurationChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSETTINGS_T1374326787_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef AUDIOEXTENSIONDEFINITION_T2916689971_H
#define AUDIOEXTENSIONDEFINITION_T2916689971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioExtensionDefinition
struct  AudioExtensionDefinition_t2916689971  : public RuntimeObject
{
public:
	// System.String UnityEngine.AudioExtensionDefinition::assemblyName
	String_t* ___assemblyName_0;
	// System.String UnityEngine.AudioExtensionDefinition::extensionNamespace
	String_t* ___extensionNamespace_1;
	// System.String UnityEngine.AudioExtensionDefinition::extensionTypeName
	String_t* ___extensionTypeName_2;
	// System.Type UnityEngine.AudioExtensionDefinition::extensionType
	Type_t * ___extensionType_3;

public:
	inline static int32_t get_offset_of_assemblyName_0() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t2916689971, ___assemblyName_0)); }
	inline String_t* get_assemblyName_0() const { return ___assemblyName_0; }
	inline String_t** get_address_of_assemblyName_0() { return &___assemblyName_0; }
	inline void set_assemblyName_0(String_t* value)
	{
		___assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_0), value);
	}

	inline static int32_t get_offset_of_extensionNamespace_1() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t2916689971, ___extensionNamespace_1)); }
	inline String_t* get_extensionNamespace_1() const { return ___extensionNamespace_1; }
	inline String_t** get_address_of_extensionNamespace_1() { return &___extensionNamespace_1; }
	inline void set_extensionNamespace_1(String_t* value)
	{
		___extensionNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionNamespace_1), value);
	}

	inline static int32_t get_offset_of_extensionTypeName_2() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t2916689971, ___extensionTypeName_2)); }
	inline String_t* get_extensionTypeName_2() const { return ___extensionTypeName_2; }
	inline String_t** get_address_of_extensionTypeName_2() { return &___extensionTypeName_2; }
	inline void set_extensionTypeName_2(String_t* value)
	{
		___extensionTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionTypeName_2), value);
	}

	inline static int32_t get_offset_of_extensionType_3() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t2916689971, ___extensionType_3)); }
	inline Type_t * get_extensionType_3() const { return ___extensionType_3; }
	inline Type_t ** get_address_of_extensionType_3() { return &___extensionType_3; }
	inline void set_extensionType_3(Type_t * value)
	{
		___extensionType_3 = value;
		Il2CppCodeGenWriteBarrier((&___extensionType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEXTENSIONDEFINITION_T2916689971_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T1165541249_H
#define LIST_1_T1165541249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition>
struct  List_1_t1165541249  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1165541249, ____items_1)); }
	inline AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* get__items_1() const { return ____items_1; }
	inline AudioSpatializerExtensionDefinitionU5BU5D_t1095391031** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1165541249, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1165541249, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1165541249_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1165541249_StaticFields, ___EmptyArray_4)); }
	inline AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AudioSpatializerExtensionDefinitionU5BU5D_t1095391031** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AudioSpatializerExtensionDefinitionU5BU5D_t1095391031* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1165541249_H
#ifndef LIST_1_T275187473_H
#define LIST_1_T275187473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>
struct  List_1_t275187473  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AudioSourceExtensionU5BU5D_t1191882599* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t275187473, ____items_1)); }
	inline AudioSourceExtensionU5BU5D_t1191882599* get__items_1() const { return ____items_1; }
	inline AudioSourceExtensionU5BU5D_t1191882599** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AudioSourceExtensionU5BU5D_t1191882599* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t275187473, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t275187473, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t275187473_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AudioSourceExtensionU5BU5D_t1191882599* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t275187473_StaticFields, ___EmptyArray_4)); }
	inline AudioSourceExtensionU5BU5D_t1191882599* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AudioSourceExtensionU5BU5D_t1191882599** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AudioSourceExtensionU5BU5D_t1191882599* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T275187473_H
#ifndef ENUMERATOR_T3036409424_H
#define ENUMERATOR_T3036409424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioAmbisonicExtensionDefinition>
struct  Enumerator_t3036409424 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3260540900 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	AudioAmbisonicExtensionDefinition_t4140913893 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3036409424, ___l_0)); }
	inline List_1_t3260540900 * get_l_0() const { return ___l_0; }
	inline List_1_t3260540900 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3260540900 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3036409424, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3036409424, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3036409424, ___current_3)); }
	inline AudioAmbisonicExtensionDefinition_t4140913893 * get_current_3() const { return ___current_3; }
	inline AudioAmbisonicExtensionDefinition_t4140913893 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AudioAmbisonicExtensionDefinition_t4140913893 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3036409424_H
#ifndef PROPERTYNAME_T65259757_H
#define PROPERTYNAME_T65259757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t65259757 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t65259757, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T65259757_H
#ifndef ENUMERATOR_T744396796_H
#define ENUMERATOR_T744396796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t744396796 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t968528272 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___l_0)); }
	inline List_1_t968528272 * get_l_0() const { return ___l_0; }
	inline List_1_t968528272 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t968528272 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t744396796, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T744396796_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef UINT64_T2561252689_H
#define UINT64_T2561252689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t2561252689 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t2561252689, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T2561252689_H
#ifndef DOUBLE_T3752657471_H
#define DOUBLE_T3752657471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3752657471 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3752657471, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3752657471_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef ENUMERATOR_T941409773_H
#define ENUMERATOR_T941409773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSpatializerExtensionDefinition>
struct  Enumerator_t941409773 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1165541249 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	AudioSpatializerExtensionDefinition_t2045914242 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t941409773, ___l_0)); }
	inline List_1_t1165541249 * get_l_0() const { return ___l_0; }
	inline List_1_t1165541249 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1165541249 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t941409773, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t941409773, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t941409773, ___current_3)); }
	inline AudioSpatializerExtensionDefinition_t2045914242 * get_current_3() const { return ___current_3; }
	inline AudioSpatializerExtensionDefinition_t2045914242 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AudioSpatializerExtensionDefinition_t2045914242 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T941409773_H
#ifndef AUDIOSPATIALIZEREXTENSIONDEFINITION_T2045914242_H
#define AUDIOSPATIALIZEREXTENSIONDEFINITION_T2045914242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSpatializerExtensionDefinition
struct  AudioSpatializerExtensionDefinition_t2045914242  : public RuntimeObject
{
public:
	// UnityEngine.PropertyName UnityEngine.AudioSpatializerExtensionDefinition::spatializerName
	PropertyName_t65259757  ___spatializerName_0;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioSpatializerExtensionDefinition::definition
	AudioExtensionDefinition_t2916689971 * ___definition_1;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioSpatializerExtensionDefinition::editorDefinition
	AudioExtensionDefinition_t2916689971 * ___editorDefinition_2;

public:
	inline static int32_t get_offset_of_spatializerName_0() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t2045914242, ___spatializerName_0)); }
	inline PropertyName_t65259757  get_spatializerName_0() const { return ___spatializerName_0; }
	inline PropertyName_t65259757 * get_address_of_spatializerName_0() { return &___spatializerName_0; }
	inline void set_spatializerName_0(PropertyName_t65259757  value)
	{
		___spatializerName_0 = value;
	}

	inline static int32_t get_offset_of_definition_1() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t2045914242, ___definition_1)); }
	inline AudioExtensionDefinition_t2916689971 * get_definition_1() const { return ___definition_1; }
	inline AudioExtensionDefinition_t2916689971 ** get_address_of_definition_1() { return &___definition_1; }
	inline void set_definition_1(AudioExtensionDefinition_t2916689971 * value)
	{
		___definition_1 = value;
		Il2CppCodeGenWriteBarrier((&___definition_1), value);
	}

	inline static int32_t get_offset_of_editorDefinition_2() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t2045914242, ___editorDefinition_2)); }
	inline AudioExtensionDefinition_t2916689971 * get_editorDefinition_2() const { return ___editorDefinition_2; }
	inline AudioExtensionDefinition_t2916689971 ** get_address_of_editorDefinition_2() { return &___editorDefinition_2; }
	inline void set_editorDefinition_2(AudioExtensionDefinition_t2916689971 * value)
	{
		___editorDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___editorDefinition_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSPATIALIZEREXTENSIONDEFINITION_T2045914242_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef RUNTIMETYPEHANDLE_T3069131627_H
#define RUNTIMETYPEHANDLE_T3069131627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3069131627 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3069131627, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3069131627_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef PLAYSTATE_T2860714712_H
#define PLAYSTATE_T2860714712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayState
struct  PlayState_t2860714712 
{
public:
	// System.Int32 UnityEngine.Playables.PlayState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayState_t2860714712, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSTATE_T2860714712_H
#ifndef PLAYABLEGRAPH_T1490949931_H
#define PLAYABLEGRAPH_T1490949931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1490949931 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1490949931_H
#ifndef AUDIOEXTENSIONMANAGER_T3958046208_H
#define AUDIOEXTENSIONMANAGER_T3958046208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioExtensionManager
struct  AudioExtensionManager_t3958046208  : public RuntimeObject
{
public:

public:
};

struct AudioExtensionManager_t3958046208_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition> UnityEngine.AudioExtensionManager::m_ListenerSpatializerExtensionDefinitions
	List_1_t1165541249 * ___m_ListenerSpatializerExtensionDefinitions_0;
	// System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition> UnityEngine.AudioExtensionManager::m_SourceSpatializerExtensionDefinitions
	List_1_t1165541249 * ___m_SourceSpatializerExtensionDefinitions_1;
	// System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition> UnityEngine.AudioExtensionManager::m_SourceAmbisonicDecoderExtensionDefinitions
	List_1_t3260540900 * ___m_SourceAmbisonicDecoderExtensionDefinitions_2;
	// System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension> UnityEngine.AudioExtensionManager::m_SourceExtensionsToUpdate
	List_1_t275187473 * ___m_SourceExtensionsToUpdate_3;
	// System.Int32 UnityEngine.AudioExtensionManager::m_NextStopIndex
	int32_t ___m_NextStopIndex_4;
	// System.Boolean UnityEngine.AudioExtensionManager::m_BuiltinDefinitionsRegistered
	bool ___m_BuiltinDefinitionsRegistered_5;
	// UnityEngine.PropertyName UnityEngine.AudioExtensionManager::m_SpatializerName
	PropertyName_t65259757  ___m_SpatializerName_6;
	// UnityEngine.PropertyName UnityEngine.AudioExtensionManager::m_SpatializerExtensionName
	PropertyName_t65259757  ___m_SpatializerExtensionName_7;
	// UnityEngine.PropertyName UnityEngine.AudioExtensionManager::m_ListenerSpatializerExtensionName
	PropertyName_t65259757  ___m_ListenerSpatializerExtensionName_8;

public:
	inline static int32_t get_offset_of_m_ListenerSpatializerExtensionDefinitions_0() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_ListenerSpatializerExtensionDefinitions_0)); }
	inline List_1_t1165541249 * get_m_ListenerSpatializerExtensionDefinitions_0() const { return ___m_ListenerSpatializerExtensionDefinitions_0; }
	inline List_1_t1165541249 ** get_address_of_m_ListenerSpatializerExtensionDefinitions_0() { return &___m_ListenerSpatializerExtensionDefinitions_0; }
	inline void set_m_ListenerSpatializerExtensionDefinitions_0(List_1_t1165541249 * value)
	{
		___m_ListenerSpatializerExtensionDefinitions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ListenerSpatializerExtensionDefinitions_0), value);
	}

	inline static int32_t get_offset_of_m_SourceSpatializerExtensionDefinitions_1() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_SourceSpatializerExtensionDefinitions_1)); }
	inline List_1_t1165541249 * get_m_SourceSpatializerExtensionDefinitions_1() const { return ___m_SourceSpatializerExtensionDefinitions_1; }
	inline List_1_t1165541249 ** get_address_of_m_SourceSpatializerExtensionDefinitions_1() { return &___m_SourceSpatializerExtensionDefinitions_1; }
	inline void set_m_SourceSpatializerExtensionDefinitions_1(List_1_t1165541249 * value)
	{
		___m_SourceSpatializerExtensionDefinitions_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceSpatializerExtensionDefinitions_1), value);
	}

	inline static int32_t get_offset_of_m_SourceAmbisonicDecoderExtensionDefinitions_2() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_SourceAmbisonicDecoderExtensionDefinitions_2)); }
	inline List_1_t3260540900 * get_m_SourceAmbisonicDecoderExtensionDefinitions_2() const { return ___m_SourceAmbisonicDecoderExtensionDefinitions_2; }
	inline List_1_t3260540900 ** get_address_of_m_SourceAmbisonicDecoderExtensionDefinitions_2() { return &___m_SourceAmbisonicDecoderExtensionDefinitions_2; }
	inline void set_m_SourceAmbisonicDecoderExtensionDefinitions_2(List_1_t3260540900 * value)
	{
		___m_SourceAmbisonicDecoderExtensionDefinitions_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceAmbisonicDecoderExtensionDefinitions_2), value);
	}

	inline static int32_t get_offset_of_m_SourceExtensionsToUpdate_3() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_SourceExtensionsToUpdate_3)); }
	inline List_1_t275187473 * get_m_SourceExtensionsToUpdate_3() const { return ___m_SourceExtensionsToUpdate_3; }
	inline List_1_t275187473 ** get_address_of_m_SourceExtensionsToUpdate_3() { return &___m_SourceExtensionsToUpdate_3; }
	inline void set_m_SourceExtensionsToUpdate_3(List_1_t275187473 * value)
	{
		___m_SourceExtensionsToUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceExtensionsToUpdate_3), value);
	}

	inline static int32_t get_offset_of_m_NextStopIndex_4() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_NextStopIndex_4)); }
	inline int32_t get_m_NextStopIndex_4() const { return ___m_NextStopIndex_4; }
	inline int32_t* get_address_of_m_NextStopIndex_4() { return &___m_NextStopIndex_4; }
	inline void set_m_NextStopIndex_4(int32_t value)
	{
		___m_NextStopIndex_4 = value;
	}

	inline static int32_t get_offset_of_m_BuiltinDefinitionsRegistered_5() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_BuiltinDefinitionsRegistered_5)); }
	inline bool get_m_BuiltinDefinitionsRegistered_5() const { return ___m_BuiltinDefinitionsRegistered_5; }
	inline bool* get_address_of_m_BuiltinDefinitionsRegistered_5() { return &___m_BuiltinDefinitionsRegistered_5; }
	inline void set_m_BuiltinDefinitionsRegistered_5(bool value)
	{
		___m_BuiltinDefinitionsRegistered_5 = value;
	}

	inline static int32_t get_offset_of_m_SpatializerName_6() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_SpatializerName_6)); }
	inline PropertyName_t65259757  get_m_SpatializerName_6() const { return ___m_SpatializerName_6; }
	inline PropertyName_t65259757 * get_address_of_m_SpatializerName_6() { return &___m_SpatializerName_6; }
	inline void set_m_SpatializerName_6(PropertyName_t65259757  value)
	{
		___m_SpatializerName_6 = value;
	}

	inline static int32_t get_offset_of_m_SpatializerExtensionName_7() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_SpatializerExtensionName_7)); }
	inline PropertyName_t65259757  get_m_SpatializerExtensionName_7() const { return ___m_SpatializerExtensionName_7; }
	inline PropertyName_t65259757 * get_address_of_m_SpatializerExtensionName_7() { return &___m_SpatializerExtensionName_7; }
	inline void set_m_SpatializerExtensionName_7(PropertyName_t65259757  value)
	{
		___m_SpatializerExtensionName_7 = value;
	}

	inline static int32_t get_offset_of_m_ListenerSpatializerExtensionName_8() { return static_cast<int32_t>(offsetof(AudioExtensionManager_t3958046208_StaticFields, ___m_ListenerSpatializerExtensionName_8)); }
	inline PropertyName_t65259757  get_m_ListenerSpatializerExtensionName_8() const { return ___m_ListenerSpatializerExtensionName_8; }
	inline PropertyName_t65259757 * get_address_of_m_ListenerSpatializerExtensionName_8() { return &___m_ListenerSpatializerExtensionName_8; }
	inline void set_m_ListenerSpatializerExtensionName_8(PropertyName_t65259757  value)
	{
		___m_ListenerSpatializerExtensionName_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEXTENSIONMANAGER_T3958046208_H
#ifndef AUDIOAMBISONICEXTENSIONDEFINITION_T4140913893_H
#define AUDIOAMBISONICEXTENSIONDEFINITION_T4140913893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioAmbisonicExtensionDefinition
struct  AudioAmbisonicExtensionDefinition_t4140913893  : public RuntimeObject
{
public:
	// UnityEngine.PropertyName UnityEngine.AudioAmbisonicExtensionDefinition::ambisonicPluginName
	PropertyName_t65259757  ___ambisonicPluginName_0;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioAmbisonicExtensionDefinition::definition
	AudioExtensionDefinition_t2916689971 * ___definition_1;

public:
	inline static int32_t get_offset_of_ambisonicPluginName_0() { return static_cast<int32_t>(offsetof(AudioAmbisonicExtensionDefinition_t4140913893, ___ambisonicPluginName_0)); }
	inline PropertyName_t65259757  get_ambisonicPluginName_0() const { return ___ambisonicPluginName_0; }
	inline PropertyName_t65259757 * get_address_of_ambisonicPluginName_0() { return &___ambisonicPluginName_0; }
	inline void set_ambisonicPluginName_0(PropertyName_t65259757  value)
	{
		___ambisonicPluginName_0 = value;
	}

	inline static int32_t get_offset_of_definition_1() { return static_cast<int32_t>(offsetof(AudioAmbisonicExtensionDefinition_t4140913893, ___definition_1)); }
	inline AudioExtensionDefinition_t2916689971 * get_definition_1() const { return ___definition_1; }
	inline AudioExtensionDefinition_t2916689971 ** get_address_of_definition_1() { return &___definition_1; }
	inline void set_definition_1(AudioExtensionDefinition_t2916689971 * value)
	{
		___definition_1 = value;
		Il2CppCodeGenWriteBarrier((&___definition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOAMBISONICEXTENSIONDEFINITION_T4140913893_H
#ifndef PLAYABLEHANDLE_T743382320_H
#define PLAYABLEHANDLE_T743382320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t743382320 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T743382320_H
#ifndef PLAYABLEOUTPUTHANDLE_T506089257_H
#define PLAYABLEOUTPUTHANDLE_T506089257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t506089257 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T506089257_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef INVALIDCASTEXCEPTION_T3988541634_H
#define INVALIDCASTEXCEPTION_T3988541634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t3988541634  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T3988541634_H
#ifndef PLAYABLE_T3296292090_H
#define PLAYABLE_T3296292090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t3296292090 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t3296292090, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t3296292090_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t3296292090  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t3296292090_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t3296292090  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t3296292090 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t3296292090  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T3296292090_H
#ifndef AUDIOCLIP_T1419814565_H
#define AUDIOCLIP_T1419814565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t1419814565  : public Object_t692178351
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t2245753777 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t2684603915 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t1419814565, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t2245753777 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t2245753777 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t2245753777 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t1419814565, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t2684603915 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t2684603915 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t2684603915 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T1419814565_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef AUDIOPLAYABLEOUTPUT_T4230785345_H
#define AUDIOPLAYABLEOUTPUT_T4230785345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioPlayableOutput
struct  AudioPlayableOutput_t4230785345 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_t506089257  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioPlayableOutput_t4230785345, ___m_Handle_0)); }
	inline PlayableOutputHandle_t506089257  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t506089257 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t506089257  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEOUTPUT_T4230785345_H
#ifndef AUDIOCLIPPLAYABLE_T2447722938_H
#define AUDIOCLIPPLAYABLE_T2447722938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioClipPlayable
struct  AudioClipPlayable_t2447722938 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioClipPlayable_t2447722938, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIPPLAYABLE_T2447722938_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3069131627  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3069131627  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3069131627 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3069131627  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1985992169* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2357198184 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2357198184 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2357198184 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1985992169* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1985992169* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2357198184 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2357198184 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2357198184 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2357198184 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2357198184 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2357198184 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef AUDIOMIXERPLAYABLE_T1969208417_H
#define AUDIOMIXERPLAYABLE_T1969208417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioMixerPlayable
struct  AudioMixerPlayable_t1969208417 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioMixerPlayable_t1969208417, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMIXERPLAYABLE_T1969208417_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef AUDIOSOURCEEXTENSION_T1155560466_H
#define AUDIOSOURCEEXTENSION_T1155560466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSourceExtension
struct  AudioSourceExtension_t1155560466  : public ScriptableObject_t1804531341
{
public:
	// UnityEngine.AudioSource UnityEngine.AudioSourceExtension::m_audioSource
	AudioSource_t4025721661 * ___m_audioSource_2;
	// System.Int32 UnityEngine.AudioSourceExtension::m_ExtensionManagerUpdateIndex
	int32_t ___m_ExtensionManagerUpdateIndex_3;

public:
	inline static int32_t get_offset_of_m_audioSource_2() { return static_cast<int32_t>(offsetof(AudioSourceExtension_t1155560466, ___m_audioSource_2)); }
	inline AudioSource_t4025721661 * get_m_audioSource_2() const { return ___m_audioSource_2; }
	inline AudioSource_t4025721661 ** get_address_of_m_audioSource_2() { return &___m_audioSource_2; }
	inline void set_m_audioSource_2(AudioSource_t4025721661 * value)
	{
		___m_audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioSource_2), value);
	}

	inline static int32_t get_offset_of_m_ExtensionManagerUpdateIndex_3() { return static_cast<int32_t>(offsetof(AudioSourceExtension_t1155560466, ___m_ExtensionManagerUpdateIndex_3)); }
	inline int32_t get_m_ExtensionManagerUpdateIndex_3() const { return ___m_ExtensionManagerUpdateIndex_3; }
	inline int32_t* get_address_of_m_ExtensionManagerUpdateIndex_3() { return &___m_ExtensionManagerUpdateIndex_3; }
	inline void set_m_ExtensionManagerUpdateIndex_3(int32_t value)
	{
		___m_ExtensionManagerUpdateIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCEEXTENSION_T1155560466_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef AUDIOLISTENEREXTENSION_T2090751094_H
#define AUDIOLISTENEREXTENSION_T2090751094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioListenerExtension
struct  AudioListenerExtension_t2090751094  : public ScriptableObject_t1804531341
{
public:
	// UnityEngine.AudioListener UnityEngine.AudioListenerExtension::m_audioListener
	AudioListener_t3253228504 * ___m_audioListener_2;

public:
	inline static int32_t get_offset_of_m_audioListener_2() { return static_cast<int32_t>(offsetof(AudioListenerExtension_t2090751094, ___m_audioListener_2)); }
	inline AudioListener_t3253228504 * get_m_audioListener_2() const { return ___m_audioListener_2; }
	inline AudioListener_t3253228504 ** get_address_of_m_audioListener_2() { return &___m_audioListener_2; }
	inline void set_m_audioListener_2(AudioListener_t3253228504 * value)
	{
		___m_audioListener_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioListener_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLISTENEREXTENSION_T2090751094_H
#ifndef PCMREADERCALLBACK_T2245753777_H
#define PCMREADERCALLBACK_T2245753777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t2245753777  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PCMREADERCALLBACK_T2245753777_H
#ifndef PCMSETPOSITIONCALLBACK_T2684603915_H
#define PCMSETPOSITIONCALLBACK_T2684603915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip/PCMSetPositionCallback
struct  PCMSetPositionCallback_t2684603915  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PCMSETPOSITIONCALLBACK_T2684603915_H
#ifndef AUDIOCONFIGURATIONCHANGEHANDLER_T2715435404_H
#define AUDIOCONFIGURATIONCHANGEHANDLER_T2715435404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct  AudioConfigurationChangeHandler_t2715435404  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONFIGURATIONCHANGEHANDLER_T2715435404_H
#ifndef ASYNCCALLBACK_T3561663063_H
#define ASYNCCALLBACK_T3561663063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3561663063  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3561663063_H
#ifndef AUDIOLISTENER_T3253228504_H
#define AUDIOLISTENER_T3253228504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioListener
struct  AudioListener_t3253228504  : public Behaviour_t2850977393
{
public:
	// UnityEngine.AudioListenerExtension UnityEngine.AudioListener::spatializerExtension
	AudioListenerExtension_t2090751094 * ___spatializerExtension_2;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioListener_t3253228504, ___spatializerExtension_2)); }
	inline AudioListenerExtension_t2090751094 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioListenerExtension_t2090751094 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioListenerExtension_t2090751094 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLISTENER_T3253228504_H
#ifndef AUDIOSOURCE_T4025721661_H
#define AUDIOSOURCE_T4025721661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t4025721661  : public Behaviour_t2850977393
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t1155560466 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t1155560466 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t4025721661, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t1155560466 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t1155560466 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t1155560466 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t4025721661, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t1155560466 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t1155560466 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t1155560466 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T4025721661_H
// System.Single[]
struct SingleU5BU5D_t2843050510  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t2511808107  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Audio.AudioClipPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableExtensions::SetDuration<UnityEngine.Audio.AudioClipPlayable>(!!0,System.Double)
extern "C"  void PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349_gshared (RuntimeObject * __this /* static, unused */, AudioClipPlayable_t2447722938  p0, double p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Audio.AudioMixerPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Audio.AudioPlayableOutput>()
extern "C"  bool PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203_gshared (PlayableOutputHandle_t506089257 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t744396796  List_1_GetEnumerator_m1837174046_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m335951430_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1328309074_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3763436353_gshared (Enumerator_t744396796 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m491990267_gshared (List_1_t968528272 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2987169932_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m1273050053_gshared (List_1_t968528272 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m1647502537_gshared (List_1_t968528272 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3534220313_gshared (List_1_t968528272 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2666688348_gshared (List_1_t968528272 * __this, const RuntimeMethod* method);

// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2628965434 (PlayableHandle_t743382320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Audio.AudioClipPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093_gshared)(__this, method)
// System.Void System.InvalidCastException::.ctor(System.String)
extern "C"  void InvalidCastException__ctor_m1966396325 (InvalidCastException_t3988541634 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AudioClipPlayable__ctor_m2881294493 (AudioClipPlayable_t2447722938 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::GetClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_GetClipInternal_m2268910778 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::GetClip()
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_GetClip_m2798209257 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetLoopedInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_GetLoopedInternal_m1075586015 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetLooped()
extern "C"  bool AudioClipPlayable_GetLooped_m588268711 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetLoopedInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetIsPlayingInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_GetIsPlayingInternal_m683041653 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::IsPlaying()
extern "C"  bool AudioClipPlayable_IsPlaying_m3987091117 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetIsPlayingInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::GetStartDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_GetStartDelayInternal_m93040472 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::GetStartDelay()
extern "C"  double AudioClipPlayable_GetStartDelay_m3310108382 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetStartDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetStartDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::GetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_GetPauseDelayInternal_m2464570216 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::GetPauseDelay()
extern "C"  double AudioClipPlayable_GetPauseDelay_m3382214223 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_InternalCreateAudioClipPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.AudioClip,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::ValidateStartDelayInternal(System.Double)
extern "C"  void AudioClipPlayable_ValidateStartDelayInternal_m3862081530 (AudioClipPlayable_t2447722938 * __this, double ___startDelay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::SetStartDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_SetStartDelayInternal_m2174257542 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetDuration(System.Double)
extern "C"  void PlayableHandle_SetDuration_m2981055392 (PlayableHandle_t743382320 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::SetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_SetPauseDelayInternal_m1554537478 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetTime(System.Double)
extern "C"  void PlayableHandle_SetTime_m1623126370 (PlayableHandle_t743382320 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetPlayState(UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_SetPlayState_m3389381656 (PlayableHandle_t743382320 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioClipPlayable::Seek(System.Double,System.Double,System.Double)
extern "C"  void AudioClipPlayable_Seek_m622849263 (AudioClipPlayable_t2447722938 * __this, double ___startTime0, double ___startDelay1, double ___duration2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m712489286 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3275468740 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AudioClip,System.Boolean)
extern "C"  PlayableHandle_t743382320  AudioClipPlayable_CreateHandle_m3637393686 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1032523438 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioClip::get_length()
extern "C"  float AudioClip_get_length_m2655312187 (AudioClip_t1419814565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableExtensions::SetDuration<UnityEngine.Audio.AudioClipPlayable>(!!0,System.Double)
#define PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349(__this /* static, unused */, p0, p1, method) ((  void (*) (RuntimeObject * /* static, unused */, AudioClipPlayable_t2447722938 , double, const RuntimeMethod*))PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349_gshared)(__this /* static, unused */, p0, p1, method)
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t743382320  PlayableHandle_get_Null_m2780162545 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::InternalCreateAudioClipPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.AudioClip,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_InternalCreateAudioClipPlayable_m3190819499 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AudioClipPlayable_GetHandle_m3169199556 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m765599562 (Playable_t3296292090 * __this, PlayableHandle_t743382320  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t743382320  Playable_GetHandle_m3912652506 (Playable_t3296292090 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2481114358 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320  p0, PlayableHandle_t743382320  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern "C"  bool AudioClipPlayable_Equals_m3631668241 (AudioClipPlayable_t2447722938 * __this, AudioClipPlayable_t2447722938  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Audio.AudioMixerPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807_gshared)(__this, method)
// System.Void UnityEngine.Audio.AudioMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AudioMixerPlayable__ctor_m342058380 (AudioMixerPlayable_t1969208417 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::GetAutoNormalizeInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_GetAutoNormalizeInternal_m3697552406 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::GetAutoNormalizeVolumes()
extern "C"  bool AudioMixerPlayable_GetAutoNormalizeVolumes_m732528428 (AudioMixerPlayable_t1969208417 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_GetAutoNormalizeInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_CreateAudioMixerPlayableInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  PlayableHandle_t743382320  AudioMixerPlayable_CreateHandle_m770923301 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::CreateAudioMixerPlayableInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_CreateAudioMixerPlayableInternal_m1244008640 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AudioMixerPlayable_GetHandle_m3951918077 (AudioMixerPlayable_t1969208417 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern "C"  bool AudioMixerPlayable_Equals_m3889413004 (AudioMixerPlayable_t1969208417 * __this, AudioMixerPlayable_t1969208417  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValid()
extern "C"  bool PlayableOutputHandle_IsValid_m124014148 (PlayableOutputHandle_t506089257 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Audio.AudioPlayableOutput>()
#define PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203(__this, method) ((  bool (*) (PlayableOutputHandle_t506089257 *, const RuntimeMethod*))PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203_gshared)(__this, method)
// System.Void UnityEngine.Audio.AudioPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void AudioPlayableOutput__ctor_m349506094 (AudioPlayableOutput_t4230785345 * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_InternalGetTarget_m1082890279 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::GetTarget()
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_GetTarget_m3071235604 (AudioPlayableOutput_t4230785345 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_InternalSetTarget_m1664251999 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, AudioSource_t4025721661 * ___target1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioPlayableOutput::SetTarget(UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_SetTarget_m622379536 (AudioPlayableOutput_t4230785345 * __this, AudioSource_t4025721661 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, AudioSource_t4025721661 * ___target1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_InternalCreateAudioOutput_m808215400 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Audio.AudioPlayableOutput UnityEngine.Audio.AudioPlayableOutput::get_Null()
extern "C"  AudioPlayableOutput_t4230785345  AudioPlayableOutput_get_Null_m702796338 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t506089257  PlayableOutputHandle_get_Null_m4090468577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t506089257  AudioPlayableOutput_GetHandle_m535534745 (AudioPlayableOutput_t4230785345 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2968844594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.String)
extern "C"  PropertyName_t65259757  PropertyName_op_Implicit_m1456940466 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m4147771927 (Object_t692178351 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m1100627178 (PCMReaderCallback_t2245753777 * __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m3131925813 (PCMSetPositionCallback_t2684603915 * __this, int32_t ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.AudioExtensionDefinition::GetExtensionType()
extern "C"  Type_t * AudioExtensionDefinition_GetExtensionType_m513008212 (AudioExtensionDefinition_t2916689971 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m3123563816 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t2511808107* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String)
extern "C"  Type_t * Type_GetType_m605100414 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_spatialize()
extern "C"  bool AudioSource_get_spatialize_m2425555736 (AudioSource_t4025721661 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::RegisterBuiltinDefinitions()
extern "C"  void AudioExtensionManager_RegisterBuiltinDefinitions_m600067160 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition>::GetEnumerator()
#define List_1_GetEnumerator_m4108440129(__this, method) ((  Enumerator_t941409773  (*) (List_1_t1165541249 *, const RuntimeMethod*))List_1_GetEnumerator_m1837174046_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSpatializerExtensionDefinition>::get_Current()
#define Enumerator_get_Current_m2378857811(__this, method) ((  AudioSpatializerExtensionDefinition_t2045914242 * (*) (Enumerator_t941409773 *, const RuntimeMethod*))Enumerator_get_Current_m335951430_gshared)(__this, method)
// System.String UnityEngine.AudioSettings::GetSpatializerPluginName()
extern "C"  String_t* AudioSettings_GetSpatializerPluginName_m56128157 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m1873673143 (RuntimeObject * __this /* static, unused */, PropertyName_t65259757  p0, PropertyName_t65259757  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::AddSpatializerExtension(System.Type)
extern "C"  AudioSourceExtension_t1155560466 * AudioSource_AddSpatializerExtension_m699023343 (AudioSource_t4025721661 * __this, Type_t * ___extensionType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSourceExtension::set_audioSource(UnityEngine.AudioSource)
extern "C"  void AudioSourceExtension_set_audioSource_m2096082894 (AudioSourceExtension_t1155560466 * __this, AudioSource_t4025721661 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::WriteExtensionProperties(UnityEngine.AudioSourceExtension,System.String)
extern "C"  void AudioExtensionManager_WriteExtensionProperties_m1007984039 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, String_t* ___extensionName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSpatializerExtensionDefinition>::MoveNext()
#define Enumerator_MoveNext_m3997841088(__this, method) ((  bool (*) (Enumerator_t941409773 *, const RuntimeMethod*))Enumerator_MoveNext_m1328309074_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSpatializerExtensionDefinition>::Dispose()
#define Enumerator_Dispose_m3350638329(__this, method) ((  void (*) (Enumerator_t941409773 *, const RuntimeMethod*))Enumerator_Dispose_m3763436353_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition>::GetEnumerator()
#define List_1_GetEnumerator_m269488467(__this, method) ((  Enumerator_t3036409424  (*) (List_1_t3260540900 *, const RuntimeMethod*))List_1_GetEnumerator_m1837174046_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioAmbisonicExtensionDefinition>::get_Current()
#define Enumerator_get_Current_m3859005127(__this, method) ((  AudioAmbisonicExtensionDefinition_t4140913893 * (*) (Enumerator_t3036409424 *, const RuntimeMethod*))Enumerator_get_Current_m335951430_gshared)(__this, method)
// System.String UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()
extern "C"  String_t* AudioSettings_GetAmbisonicDecoderPluginName_m854307786 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::AddAmbisonicExtension(System.Type)
extern "C"  AudioSourceExtension_t1155560466 * AudioSource_AddAmbisonicExtension_m1752284328 (AudioSource_t4025721661 * __this, Type_t * ___extensionType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioAmbisonicExtensionDefinition>::MoveNext()
#define Enumerator_MoveNext_m1883253745(__this, method) ((  bool (*) (Enumerator_t3036409424 *, const RuntimeMethod*))Enumerator_MoveNext_m1328309074_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioAmbisonicExtensionDefinition>::Dispose()
#define Enumerator_Dispose_m2943463037(__this, method) ((  void (*) (Enumerator_t3036409424 *, const RuntimeMethod*))Enumerator_Dispose_m3763436353_gshared)(__this, method)
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.Int32)
extern "C"  PropertyName_t65259757  PropertyName_op_Implicit_m920796712 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.AudioSourceExtension::get_audioSource()
extern "C"  AudioSource_t4025721661 * AudioSourceExtension_get_audioSource_m500534479 (AudioSourceExtension_t1155560466 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.AudioSource::ReadExtensionName(System.Int32)
extern "C"  PropertyName_t65259757  AudioSource_ReadExtensionName_m3482741001 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.AudioSource::ReadExtensionPropertyName(System.Int32)
extern "C"  PropertyName_t65259757  AudioSource_ReadExtensionPropertyName_m3408990507 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)
extern "C"  float AudioSource_ReadExtensionPropertyValue_m3076907217 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioSource::GetNumExtensionProperties()
extern "C"  int32_t AudioSource_GetNumExtensionProperties_m206409171 (AudioSource_t4025721661 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::ClearExtensionProperties(UnityEngine.PropertyName)
extern "C"  void AudioSource_ClearExtensionProperties_m310946178 (AudioSource_t4025721661 * __this, PropertyName_t65259757  ___extensionName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioListenerExtension UnityEngine.AudioListener::AddExtension(System.Type)
extern "C"  AudioListenerExtension_t2090751094 * AudioListener_AddExtension_m2595025457 (AudioListener_t3253228504 * __this, Type_t * ___extensionType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListenerExtension::set_audioListener(UnityEngine.AudioListener)
extern "C"  void AudioListenerExtension_set_audioListener_m2623015159 (AudioListenerExtension_t2090751094 * __this, AudioListener_t3253228504 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::WriteExtensionProperties(UnityEngine.AudioListenerExtension,System.String)
extern "C"  void AudioExtensionManager_WriteExtensionProperties_m3816343998 (RuntimeObject * __this /* static, unused */, AudioListenerExtension_t2090751094 * ___extension0, String_t* ___extensionName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioListener UnityEngine.AudioListenerExtension::get_audioListener()
extern "C"  AudioListener_t3253228504 * AudioListenerExtension_get_audioListener_m3069071264 (AudioListenerExtension_t2090751094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.AudioListener::ReadExtensionName(System.Int32)
extern "C"  PropertyName_t65259757  AudioListener_ReadExtensionName_m5265467 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.AudioListener::ReadExtensionPropertyName(System.Int32)
extern "C"  PropertyName_t65259757  AudioListener_ReadExtensionPropertyName_m298351786 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)
extern "C"  float AudioListener_ReadExtensionPropertyValue_m2875278341 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioListener::GetNumExtensionProperties()
extern "C"  int32_t AudioListener_GetNumExtensionProperties_m2276445238 (AudioListener_t3253228504 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::ClearExtensionProperties(UnityEngine.PropertyName)
extern "C"  void AudioListener_ClearExtensionProperties_m2269379717 (AudioListener_t3253228504 * __this, PropertyName_t65259757  ___extensionName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::Add(!0)
#define List_1_Add_m29487416(__this, p0, method) ((  void (*) (List_1_t275187473 *, AudioSourceExtension_t1155560466 *, const RuntimeMethod*))List_1_Add_m491990267_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::get_Count()
#define List_1_get_Count_m789375335(__this, method) ((  int32_t (*) (List_1_t275187473 *, const RuntimeMethod*))List_1_get_Count_m2987169932_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::get_Item(System.Int32)
#define List_1_get_Item_m609089971(__this, p0, method) ((  AudioSourceExtension_t1155560466 * (*) (List_1_t275187473 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1273050053_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::set_Item(System.Int32,!0)
#define List_1_set_Item_m2969022274(__this, p0, p1, method) ((  void (*) (List_1_t275187473 *, int32_t, AudioSourceExtension_t1155560466 *, const RuntimeMethod*))List_1_set_Item_m1647502537_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m4186794102(__this, p0, method) ((  void (*) (List_1_t275187473 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m3534220313_gshared)(__this, p0, method)
// System.Boolean UnityEngine.PropertyName::op_Inequality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Inequality_m3593548498 (RuntimeObject * __this /* static, unused */, PropertyName_t65259757  p0, PropertyName_t65259757  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.AudioExtensionManager::GetSourceSpatializerExtensionType()
extern "C"  Type_t * AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.AudioExtensionManager::GetListenerSpatializerExtensionEditorType()
extern "C"  Type_t * AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m422144144 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.AudioExtensionManager::GetListenerSpatializerExtensionType()
extern "C"  Type_t * AudioExtensionManager_GetListenerSpatializerExtensionType_m3630031100 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AudioExtensionManager::GetAudioListener()
extern "C"  Object_t692178351 * AudioExtensionManager_GetAudioListener_m1216759061 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioListenerExtension UnityEngine.AudioExtensionManager::AddSpatializerExtension(UnityEngine.AudioListener)
extern "C"  AudioListenerExtension_t2090751094 * AudioExtensionManager_AddSpatializerExtension_m837049305 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___listener0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m3996741937 (Behaviour_t2850977393 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m1553006287 (AudioSource_t4025721661 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::RemoveExtensionFromManager(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_RemoveExtensionFromManager_m3149216797 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::AddExtensionToManager(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_AddExtensionToManager_m2092709385 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1683246493 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionDefinition::.ctor(System.String,System.String,System.String)
extern "C"  void AudioExtensionDefinition__ctor_m3723261159 (AudioExtensionDefinition_t2916689971 * __this, String_t* ___assemblyNameIn0, String_t* ___extensionNamespaceIn1, String_t* ___extensionTypeNameIn2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioExtensionManager::RegisterListenerSpatializerDefinition(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterListenerSpatializerDefinition_m440281069 (RuntimeObject * __this /* static, unused */, String_t* ___spatializerName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, AudioExtensionDefinition_t2916689971 * ___editorDefinition2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioExtensionManager::RegisterSourceSpatializerDefinition(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterSourceSpatializerDefinition_m349058419 (RuntimeObject * __this /* static, unused */, String_t* ___spatializerName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, AudioExtensionDefinition_t2916689971 * ___editorDefinition2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioExtensionManager::RegisterSourceAmbisonicDefinition(System.String,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterSourceAmbisonicDefinition_m765937035 (RuntimeObject * __this /* static, unused */, String_t* ___ambisonicDecoderName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m3513156148 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSpatializerExtensionDefinition::.ctor(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  void AudioSpatializerExtensionDefinition__ctor_m507988252 (AudioSpatializerExtensionDefinition_t2045914242 * __this, String_t* ___spatializerNameIn0, AudioExtensionDefinition_t2916689971 * ___definitionIn1, AudioExtensionDefinition_t2916689971 * ___editorDefinitionIn2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition>::Add(!0)
#define List_1_Add_m2413286470(__this, p0, method) ((  void (*) (List_1_t1165541249 *, AudioSpatializerExtensionDefinition_t2045914242 *, const RuntimeMethod*))List_1_Add_m491990267_gshared)(__this, p0, method)
// System.Void UnityEngine.AudioAmbisonicExtensionDefinition::.ctor(System.String,UnityEngine.AudioExtensionDefinition)
extern "C"  void AudioAmbisonicExtensionDefinition__ctor_m2030192098 (AudioAmbisonicExtensionDefinition_t4140913893 * __this, String_t* ___ambisonicNameIn0, AudioExtensionDefinition_t2916689971 * ___definitionIn1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition>::Add(!0)
#define List_1_Add_m2970273886(__this, p0, method) ((  void (*) (List_1_t3260540900 *, AudioAmbisonicExtensionDefinition_t4140913893 *, const RuntimeMethod*))List_1_Add_m491990267_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSpatializerExtensionDefinition>::.ctor()
#define List_1__ctor_m57615730(__this, method) ((  void (*) (List_1_t1165541249 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioAmbisonicExtensionDefinition>::.ctor()
#define List_1__ctor_m1918620480(__this, method) ((  void (*) (List_1_t3260540900 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSourceExtension>::.ctor()
#define List_1__ctor_m4205201701(__this, method) ((  void (*) (List_1_t275187473 *, const RuntimeMethod*))List_1__ctor_m2666688348_gshared)(__this, method)
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, int32_t ___listenerIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, int32_t ___listenerIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, PropertyName_t65259757 * ___extensionName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t1804531341 * ScriptableObject_CreateInstance_m1486025286 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m2092555472 (AudioConfigurationChangeHandler_t2715435404 * __this, bool ___deviceWasChanged0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::Update()
extern "C"  void AudioExtensionManager_Update_m3577106864 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSourceExtension UnityEngine.AudioExtensionManager::AddSpatializerExtension(UnityEngine.AudioSource)
extern "C"  AudioSourceExtension_t1155560466 * AudioExtensionManager_AddSpatializerExtension_m375267302 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___source0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioExtensionManager::GetReadyToPlay(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_GetReadyToPlay_m3146133677 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t1419814565 * AudioSource_get_clip_m4008781082 (AudioSource_t4025721661 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioClip::get_ambisonic()
extern "C"  bool AudioClip_get_ambisonic_m4289332812 (AudioClip_t1419814565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSourceExtension UnityEngine.AudioExtensionManager::AddAmbisonicDecoderExtension(UnityEngine.AudioSource)
extern "C"  AudioSourceExtension_t1155560466 * AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___source0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2653597291 (Behaviour_t2850977393 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m992094170 (AudioSource_t4025721661 * __this, uint64_t ___delay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShotHelper_m1922705126 (AudioSource_t4025721661 * __this, AudioClip_t1419814565 * ___clip0, float ___volumeScale1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_spatializeInternal()
extern "C"  bool AudioSource_get_spatializeInternal_m3549592781 (AudioSource_t4025721661 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, int32_t ___sourceIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, int32_t ___sourceIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, PropertyName_t65259757 * ___extensionName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Audio.AudioClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AudioClipPlayable__ctor_m2881294493 (AudioClipPlayable_t2447722938 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioClipPlayable__ctor_m2881294493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAudioClipPlayable_t2447722938_m1418432093_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral1694235800, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AudioClipPlayable__ctor_m2881294493_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	AudioClipPlayable__ctor_m2881294493(_thisAdjusted, ___handle0, method);
}
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::GetClip()
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_GetClip_m2798209257 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	AudioClip_t1419814565 * V_0 = NULL;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		AudioClip_t1419814565 * L_1 = AudioClipPlayable_GetClipInternal_m2268910778(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AudioClip_t1419814565 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_GetClip_m2798209257_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_GetClip_m2798209257(_thisAdjusted, method);
}
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::GetClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_GetClipInternal_m2268910778 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	AudioClip_t1419814565 * V_0 = NULL;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		AudioClip_t1419814565 * L_1 = AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		AudioClip_t1419814565 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AudioClip UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AudioClip_t1419814565 * AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef AudioClip_t1419814565 * (*AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731_ftn) (PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_GetClipInternal_m2122299731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetClipInternal(UnityEngine.Playables.PlayableHandle&)");
	AudioClip_t1419814565 * retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetLooped()
extern "C"  bool AudioClipPlayable_GetLooped_m588268711 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AudioClipPlayable_GetLoopedInternal_m1075586015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AudioClipPlayable_GetLooped_m588268711_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_GetLooped_m588268711(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetLoopedInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_GetLoopedInternal_m1075586015 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		bool L_1 = AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetLoopedInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef bool (*AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030_ftn) (PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_GetLoopedInternal_m285701030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetLoopedInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::IsPlaying()
extern "C"  bool AudioClipPlayable_IsPlaying_m3987091117 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AudioClipPlayable_GetIsPlayingInternal_m683041653(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AudioClipPlayable_IsPlaying_m3987091117_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_IsPlaying_m3987091117(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::GetIsPlayingInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_GetIsPlayingInternal_m683041653 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		bool L_1 = AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetIsPlayingInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef bool (*AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841_ftn) (PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_GetIsPlayingInternal_m1472344841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetIsPlayingInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Double UnityEngine.Audio.AudioClipPlayable::GetStartDelay()
extern "C"  double AudioClipPlayable_GetStartDelay_m3310108382 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		double L_1 = AudioClipPlayable_GetStartDelayInternal_m93040472(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		double L_2 = V_0;
		return L_2;
	}
}
extern "C"  double AudioClipPlayable_GetStartDelay_m3310108382_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_GetStartDelay_m3310108382(_thisAdjusted, method);
}
// System.Double UnityEngine.Audio.AudioClipPlayable::GetStartDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_GetStartDelayInternal_m93040472 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		double L_1 = AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		double L_2 = V_0;
		return L_2;
	}
}
// System.Double UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetStartDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef double (*AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308_ftn) (PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_GetStartDelayInternal_m2469523308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetStartDelayInternal(UnityEngine.Playables.PlayableHandle&)");
	double retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Void UnityEngine.Audio.AudioClipPlayable::SetStartDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_SetStartDelayInternal_m2174257542 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		double L_1 = ___delay1;
		AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetStartDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method)
{
	typedef void (*AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457_ftn) (PlayableHandle_t743382320 *, double);
	static AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_SetStartDelayInternal_m750242457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetStartDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)");
	_il2cpp_icall_func(___hdl0, ___delay1);
}
// System.Double UnityEngine.Audio.AudioClipPlayable::GetPauseDelay()
extern "C"  double AudioClipPlayable_GetPauseDelay_m3382214223 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		double L_1 = AudioClipPlayable_GetPauseDelayInternal_m2464570216(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		double L_2 = V_0;
		return L_2;
	}
}
extern "C"  double AudioClipPlayable_GetPauseDelay_m3382214223_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_GetPauseDelay_m3382214223(_thisAdjusted, method);
}
// System.Double UnityEngine.Audio.AudioClipPlayable::GetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_GetPauseDelayInternal_m2464570216 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		double L_1 = AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		double L_2 = V_0;
		return L_2;
	}
}
// System.Double UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  double AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef double (*AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355_ftn) (PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_GetPauseDelayInternal_m1149063355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_GetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&)");
	double retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Void UnityEngine.Audio.AudioClipPlayable::SetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_SetPauseDelayInternal_m1554537478 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		double L_1 = ___delay1;
		AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, double ___delay1, const RuntimeMethod* method)
{
	typedef void (*AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899_ftn) (PlayableHandle_t743382320 *, double);
	static AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_SetPauseDelayInternal_m956460899_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_SetPauseDelayInternal(UnityEngine.Playables.PlayableHandle&,System.Double)");
	_il2cpp_icall_func(___hdl0, ___delay1);
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::InternalCreateAudioClipPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.AudioClip,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_InternalCreateAudioClipPlayable_m3190819499 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		AudioClip_t1419814565 * L_1 = ___clip1;
		bool L_2 = ___looping2;
		PlayableHandle_t743382320 * L_3 = ___handle3;
		bool L_4 = AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_InternalCreateAudioClipPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.AudioClip,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942_ftn) (PlayableGraph_t1490949931 *, AudioClip_t1419814565 *, bool, PlayableHandle_t743382320 *);
	static AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClipPlayable_INTERNAL_CALL_InternalCreateAudioClipPlayable_m1728897942_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioClipPlayable::INTERNAL_CALL_InternalCreateAudioClipPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.AudioClip,System.Boolean,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___clip1, ___looping2, ___handle3);
	return retVal;
}
// System.Void UnityEngine.Audio.AudioClipPlayable::Seek(System.Double,System.Double,System.Double)
extern "C"  void AudioClipPlayable_Seek_m622849263 (AudioClipPlayable_t2447722938 * __this, double ___startTime0, double ___startDelay1, double ___duration2, const RuntimeMethod* method)
{
	{
		double L_0 = ___startDelay1;
		AudioClipPlayable_ValidateStartDelayInternal_m3862081530(__this, L_0, /*hidden argument*/NULL);
		PlayableHandle_t743382320 * L_1 = __this->get_address_of_m_Handle_0();
		double L_2 = ___startDelay1;
		AudioClipPlayable_SetStartDelayInternal_m2174257542(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		double L_3 = ___duration2;
		if ((!(((double)L_3) > ((double)(0.0)))))
		{
			goto IL_0046;
		}
	}
	{
		PlayableHandle_t743382320 * L_4 = __this->get_address_of_m_Handle_0();
		double L_5 = ___duration2;
		double L_6 = ___startTime0;
		PlayableHandle_SetDuration_m2981055392(L_4, ((double)((double)L_5+(double)L_6)), /*hidden argument*/NULL);
		PlayableHandle_t743382320 * L_7 = __this->get_address_of_m_Handle_0();
		double L_8 = ___startDelay1;
		double L_9 = ___duration2;
		AudioClipPlayable_SetPauseDelayInternal_m1554537478(NULL /*static, unused*/, L_7, ((double)((double)L_8+(double)L_9)), /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0046:
	{
		PlayableHandle_t743382320 * L_10 = __this->get_address_of_m_Handle_0();
		PlayableHandle_SetDuration_m2981055392(L_10, (1.7976931348623157E+308), /*hidden argument*/NULL);
		PlayableHandle_t743382320 * L_11 = __this->get_address_of_m_Handle_0();
		AudioClipPlayable_SetPauseDelayInternal_m1554537478(NULL /*static, unused*/, L_11, (0.0), /*hidden argument*/NULL);
	}

IL_0070:
	{
		PlayableHandle_t743382320 * L_12 = __this->get_address_of_m_Handle_0();
		double L_13 = ___startTime0;
		PlayableHandle_SetTime_m1623126370(L_12, L_13, /*hidden argument*/NULL);
		PlayableHandle_t743382320 * L_14 = __this->get_address_of_m_Handle_0();
		PlayableHandle_SetPlayState_m3389381656(L_14, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AudioClipPlayable_Seek_m622849263_AdjustorThunk (RuntimeObject * __this, double ___startTime0, double ___startDelay1, double ___duration2, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	AudioClipPlayable_Seek_m622849263(_thisAdjusted, ___startTime0, ___startDelay1, ___duration2, method);
}
// System.Void UnityEngine.Audio.AudioClipPlayable::ValidateStartDelayInternal(System.Double)
extern "C"  void AudioClipPlayable_ValidateStartDelayInternal_m3862081530 (AudioClipPlayable_t2447722938 * __this, double ___startDelay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioClipPlayable_ValidateStartDelayInternal_m3862081530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		double L_1 = AudioClipPlayable_GetStartDelayInternal_m93040472(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = AudioClipPlayable_IsPlaying_m3987091117(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0061;
		}
	}
	{
		double L_3 = ___startDelay0;
		if ((((double)L_3) < ((double)(0.05))))
		{
			goto IL_0045;
		}
	}
	{
		double L_4 = V_0;
		if ((!(((double)L_4) >= ((double)(1.0E-05)))))
		{
			goto IL_0061;
		}
	}
	{
		double L_5 = V_0;
		if ((!(((double)L_5) < ((double)(0.05)))))
		{
			goto IL_0061;
		}
	}

IL_0045:
	{
		double L_6 = V_0;
		double L_7 = L_6;
		RuntimeObject * L_8 = Box(Double_t3752657471_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m712489286(NULL /*static, unused*/, _stringLiteral2549451288, L_8, _stringLiteral1790281158, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3275468740(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void AudioClipPlayable_ValidateStartDelayInternal_m3862081530_AdjustorThunk (RuntimeObject * __this, double ___startDelay0, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	AudioClipPlayable_ValidateStartDelayInternal_m3862081530(_thisAdjusted, ___startDelay0, method);
}
// UnityEngine.Audio.AudioClipPlayable UnityEngine.Audio.AudioClipPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.AudioClip,System.Boolean)
extern "C"  AudioClipPlayable_t2447722938  AudioClipPlayable_Create_m2400233086 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioClipPlayable_Create_m2400233086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AudioClipPlayable_t2447722938  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AudioClipPlayable_t2447722938  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		AudioClip_t1419814565 * L_1 = ___clip1;
		bool L_2 = ___looping2;
		PlayableHandle_t743382320  L_3 = AudioClipPlayable_CreateHandle_m3637393686(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PlayableHandle_t743382320  L_4 = V_0;
		AudioClipPlayable__ctor_m2881294493((&V_1), L_4, /*hidden argument*/NULL);
		AudioClip_t1419814565 * L_5 = ___clip1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_5, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		AudioClipPlayable_t2447722938  L_7 = V_1;
		AudioClip_t1419814565 * L_8 = ___clip1;
		NullCheck(L_8);
		float L_9 = AudioClip_get_length_m2655312187(L_8, /*hidden argument*/NULL);
		PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349(NULL /*static, unused*/, L_7, (((double)((double)L_9))), /*hidden argument*/PlayableExtensions_SetDuration_TisAudioClipPlayable_t2447722938_m2888280349_RuntimeMethod_var);
	}

IL_002b:
	{
		AudioClipPlayable_t2447722938  L_10 = V_1;
		V_2 = L_10;
		goto IL_0032;
	}

IL_0032:
	{
		AudioClipPlayable_t2447722938  L_11 = V_2;
		return L_11;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AudioClip,System.Boolean)
extern "C"  PlayableHandle_t743382320  AudioClipPlayable_CreateHandle_m3637393686 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AudioClip_t1419814565 * ___clip1, bool ___looping2, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		AudioClip_t1419814565 * L_1 = ___clip1;
		bool L_2 = ___looping2;
		bool L_3 = AudioClipPlayable_InternalCreateAudioClipPlayable_m3190819499(NULL /*static, unused*/, (&___graph0), L_1, L_2, (&V_0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		PlayableHandle_t743382320  L_4 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0029;
	}

IL_0022:
	{
		PlayableHandle_t743382320  L_5 = V_0;
		V_1 = L_5;
		goto IL_0029;
	}

IL_0029:
	{
		PlayableHandle_t743382320  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AudioClipPlayable_GetHandle_m3169199556 (AudioClipPlayable_t2447722938 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AudioClipPlayable_GetHandle_m3169199556_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_GetHandle_m3169199556(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Audio.AudioClipPlayable::op_Implicit(UnityEngine.Audio.AudioClipPlayable)
extern "C"  Playable_t3296292090  AudioClipPlayable_op_Implicit_m1121340892 (RuntimeObject * __this /* static, unused */, AudioClipPlayable_t2447722938  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AudioClipPlayable_GetHandle_m3169199556((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Audio.AudioClipPlayable UnityEngine.Audio.AudioClipPlayable::op_Explicit(UnityEngine.Playables.Playable)
extern "C"  AudioClipPlayable_t2447722938  AudioClipPlayable_op_Explicit_m1985342934 (RuntimeObject * __this /* static, unused */, Playable_t3296292090  ___playable0, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = Playable_GetHandle_m3912652506((&___playable0), /*hidden argument*/NULL);
		AudioClipPlayable_t2447722938  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AudioClipPlayable__ctor_m2881294493((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		AudioClipPlayable_t2447722938  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern "C"  bool AudioClipPlayable_Equals_m3631668241 (AudioClipPlayable_t2447722938 * __this, AudioClipPlayable_t2447722938  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AudioClipPlayable_GetHandle_m3169199556(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AudioClipPlayable_GetHandle_m3169199556((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AudioClipPlayable_Equals_m3631668241_AdjustorThunk (RuntimeObject * __this, AudioClipPlayable_t2447722938  ___other0, const RuntimeMethod* method)
{
	AudioClipPlayable_t2447722938 * _thisAdjusted = reinterpret_cast<AudioClipPlayable_t2447722938 *>(__this + 1);
	return AudioClipPlayable_Equals_m3631668241(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Audio.AudioMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AudioMixerPlayable__ctor_m342058380 (AudioMixerPlayable_t1969208417 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioMixerPlayable__ctor_m342058380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAudioMixerPlayable_t1969208417_m3362174807_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral2408473799, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AudioMixerPlayable__ctor_m342058380_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AudioMixerPlayable_t1969208417 * _thisAdjusted = reinterpret_cast<AudioMixerPlayable_t1969208417 *>(__this + 1);
	AudioMixerPlayable__ctor_m342058380(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::GetAutoNormalizeVolumes()
extern "C"  bool AudioMixerPlayable_GetAutoNormalizeVolumes_m732528428 (AudioMixerPlayable_t1969208417 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AudioMixerPlayable_GetAutoNormalizeInternal_m3697552406(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AudioMixerPlayable_GetAutoNormalizeVolumes_m732528428_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioMixerPlayable_t1969208417 * _thisAdjusted = reinterpret_cast<AudioMixerPlayable_t1969208417 *>(__this + 1);
	return AudioMixerPlayable_GetAutoNormalizeVolumes_m732528428(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::GetAutoNormalizeInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_GetAutoNormalizeInternal_m3697552406 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___hdl0;
		bool L_1 = AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_GetAutoNormalizeInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___hdl0, const RuntimeMethod* method)
{
	typedef bool (*AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214_ftn) (PlayableHandle_t743382320 *);
	static AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioMixerPlayable_INTERNAL_CALL_GetAutoNormalizeInternal_m3278470214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_GetAutoNormalizeInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___hdl0);
	return retVal;
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::CreateAudioMixerPlayableInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_CreateAudioMixerPlayableInternal_m1244008640 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		bool L_2 = ___normalizeInputVolumes2;
		PlayableHandle_t743382320 * L_3 = ___handle3;
		bool L_4 = AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_CreateAudioMixerPlayableInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311_ftn) (PlayableGraph_t1490949931 *, int32_t, bool, PlayableHandle_t743382320 *);
	static AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioMixerPlayable_INTERNAL_CALL_CreateAudioMixerPlayableInternal_m2066367311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioMixerPlayable::INTERNAL_CALL_CreateAudioMixerPlayableInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___inputCount1, ___normalizeInputVolumes2, ___handle3);
	return retVal;
}
// UnityEngine.Audio.AudioMixerPlayable UnityEngine.Audio.AudioMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  AudioMixerPlayable_t1969208417  AudioMixerPlayable_Create_m2326434930 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AudioMixerPlayable_t1969208417  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		bool L_2 = ___normalizeInputVolumes2;
		PlayableHandle_t743382320  L_3 = AudioMixerPlayable_CreateHandle_m770923301(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PlayableHandle_t743382320  L_4 = V_0;
		AudioMixerPlayable_t1969208417  L_5;
		memset(&L_5, 0, sizeof(L_5));
		AudioMixerPlayable__ctor_m342058380((&L_5), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		AudioMixerPlayable_t1969208417  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  PlayableHandle_t743382320  AudioMixerPlayable_CreateHandle_m770923301 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeInputVolumes2, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___inputCount1;
		bool L_2 = ___normalizeInputVolumes2;
		bool L_3 = AudioMixerPlayable_CreateAudioMixerPlayableInternal_m1244008640(NULL /*static, unused*/, (&___graph0), L_1, L_2, (&V_0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		PlayableHandle_t743382320  L_4 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0029;
	}

IL_0022:
	{
		PlayableHandle_t743382320  L_5 = V_0;
		V_1 = L_5;
		goto IL_0029;
	}

IL_0029:
	{
		PlayableHandle_t743382320  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AudioMixerPlayable_GetHandle_m3951918077 (AudioMixerPlayable_t1969208417 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AudioMixerPlayable_GetHandle_m3951918077_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioMixerPlayable_t1969208417 * _thisAdjusted = reinterpret_cast<AudioMixerPlayable_t1969208417 *>(__this + 1);
	return AudioMixerPlayable_GetHandle_m3951918077(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Audio.AudioMixerPlayable::op_Implicit(UnityEngine.Audio.AudioMixerPlayable)
extern "C"  Playable_t3296292090  AudioMixerPlayable_op_Implicit_m604819791 (RuntimeObject * __this /* static, unused */, AudioMixerPlayable_t1969208417  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AudioMixerPlayable_GetHandle_m3951918077((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern "C"  bool AudioMixerPlayable_Equals_m3889413004 (AudioMixerPlayable_t1969208417 * __this, AudioMixerPlayable_t1969208417  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AudioMixerPlayable_GetHandle_m3951918077(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AudioMixerPlayable_GetHandle_m3951918077((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AudioMixerPlayable_Equals_m3889413004_AdjustorThunk (RuntimeObject * __this, AudioMixerPlayable_t1969208417  ___other0, const RuntimeMethod* method)
{
	AudioMixerPlayable_t1969208417 * _thisAdjusted = reinterpret_cast<AudioMixerPlayable_t1969208417 *>(__this + 1);
	return AudioMixerPlayable_Equals_m3889413004(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Audio.AudioPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void AudioPlayableOutput__ctor_m349506094 (AudioPlayableOutput_t4230785345 * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioPlayableOutput__ctor_m349506094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableOutputHandle_IsValid_m124014148((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203((&___handle0), /*hidden argument*/PlayableOutputHandle_IsPlayableOutputOfType_TisAudioPlayableOutput_t4230785345_m482036203_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral980058864, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableOutputHandle_t506089257  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AudioPlayableOutput__ctor_m349506094_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method)
{
	AudioPlayableOutput_t4230785345 * _thisAdjusted = reinterpret_cast<AudioPlayableOutput_t4230785345 *>(__this + 1);
	AudioPlayableOutput__ctor_m349506094(_thisAdjusted, ___handle0, method);
}
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::GetTarget()
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_GetTarget_m3071235604 (AudioPlayableOutput_t4230785345 * __this, const RuntimeMethod* method)
{
	AudioSource_t4025721661 * V_0 = NULL;
	{
		PlayableOutputHandle_t506089257 * L_0 = __this->get_address_of_m_Handle_0();
		AudioSource_t4025721661 * L_1 = AudioPlayableOutput_InternalGetTarget_m1082890279(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AudioSource_t4025721661 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_GetTarget_m3071235604_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioPlayableOutput_t4230785345 * _thisAdjusted = reinterpret_cast<AudioPlayableOutput_t4230785345 *>(__this + 1);
	return AudioPlayableOutput_GetTarget_m3071235604(_thisAdjusted, method);
}
// System.Void UnityEngine.Audio.AudioPlayableOutput::SetTarget(UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_SetTarget_m622379536 (AudioPlayableOutput_t4230785345 * __this, AudioSource_t4025721661 * ___value0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t506089257 * L_0 = __this->get_address_of_m_Handle_0();
		AudioSource_t4025721661 * L_1 = ___value0;
		AudioPlayableOutput_InternalSetTarget_m1664251999(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AudioPlayableOutput_SetTarget_m622379536_AdjustorThunk (RuntimeObject * __this, AudioSource_t4025721661 * ___value0, const RuntimeMethod* method)
{
	AudioPlayableOutput_t4230785345 * _thisAdjusted = reinterpret_cast<AudioPlayableOutput_t4230785345 *>(__this + 1);
	AudioPlayableOutput_SetTarget_m622379536(_thisAdjusted, ___value0, method);
}
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_InternalGetTarget_m1082890279 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, const RuntimeMethod* method)
{
	AudioSource_t4025721661 * V_0 = NULL;
	{
		PlayableOutputHandle_t506089257 * L_0 = ___output0;
		AudioSource_t4025721661 * L_1 = AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		AudioSource_t4025721661 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AudioSource UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  AudioSource_t4025721661 * AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, const RuntimeMethod* method)
{
	typedef AudioSource_t4025721661 * (*AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741_ftn) (PlayableOutputHandle_t506089257 *);
	static AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioPlayableOutput_INTERNAL_CALL_InternalGetTarget_m4236577741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)");
	AudioSource_t4025721661 * retVal = _il2cpp_icall_func(___output0);
	return retVal;
}
// System.Void UnityEngine.Audio.AudioPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_InternalSetTarget_m1664251999 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, AudioSource_t4025721661 * ___target1, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t506089257 * L_0 = ___output0;
		AudioSource_t4025721661 * L_1 = ___target1;
		AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.AudioSource)
extern "C"  void AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___output0, AudioSource_t4025721661 * ___target1, const RuntimeMethod* method)
{
	typedef void (*AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467_ftn) (PlayableOutputHandle_t506089257 *, AudioSource_t4025721661 *);
	static AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioPlayableOutput_INTERNAL_CALL_InternalSetTarget_m102263467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.AudioSource)");
	_il2cpp_icall_func(___output0, ___target1);
}
// UnityEngine.Audio.AudioPlayableOutput UnityEngine.Audio.AudioPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String,UnityEngine.AudioSource)
extern "C"  AudioPlayableOutput_t4230785345  AudioPlayableOutput_Create_m2748526891 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, String_t* ___name1, AudioSource_t4025721661 * ___target2, const RuntimeMethod* method)
{
	PlayableOutputHandle_t506089257  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AudioPlayableOutput_t4230785345  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AudioPlayableOutput_t4230785345  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		String_t* L_0 = ___name1;
		bool L_1 = AudioPlayableGraphExtensions_InternalCreateAudioOutput_m808215400(NULL /*static, unused*/, (&___graph0), L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		AudioPlayableOutput_t4230785345  L_2 = AudioPlayableOutput_get_Null_m702796338(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0032;
	}

IL_001b:
	{
		PlayableOutputHandle_t506089257  L_3 = V_0;
		AudioPlayableOutput__ctor_m349506094((&V_2), L_3, /*hidden argument*/NULL);
		AudioSource_t4025721661 * L_4 = ___target2;
		AudioPlayableOutput_SetTarget_m622379536((&V_2), L_4, /*hidden argument*/NULL);
		AudioPlayableOutput_t4230785345  L_5 = V_2;
		V_1 = L_5;
		goto IL_0032;
	}

IL_0032:
	{
		AudioPlayableOutput_t4230785345  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Audio.AudioPlayableOutput UnityEngine.Audio.AudioPlayableOutput::get_Null()
extern "C"  AudioPlayableOutput_t4230785345  AudioPlayableOutput_get_Null_m702796338 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	AudioPlayableOutput_t4230785345  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t506089257  L_0 = PlayableOutputHandle_get_Null_m4090468577(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioPlayableOutput_t4230785345  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AudioPlayableOutput__ctor_m349506094((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		AudioPlayableOutput_t4230785345  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t506089257  AudioPlayableOutput_GetHandle_m535534745 (AudioPlayableOutput_t4230785345 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t506089257  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t506089257  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t506089257  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t506089257  AudioPlayableOutput_GetHandle_m535534745_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AudioPlayableOutput_t4230785345 * _thisAdjusted = reinterpret_cast<AudioPlayableOutput_t4230785345 *>(__this + 1);
	return AudioPlayableOutput_GetHandle_m535534745(_thisAdjusted, method);
}
// System.Void UnityEngine.AudioAmbisonicExtensionDefinition::.ctor(System.String,UnityEngine.AudioExtensionDefinition)
extern "C"  void AudioAmbisonicExtensionDefinition__ctor_m2030192098 (AudioAmbisonicExtensionDefinition_t4140913893 * __this, String_t* ___ambisonicNameIn0, AudioExtensionDefinition_t2916689971 * ___definitionIn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___ambisonicNameIn0;
		PropertyName_t65259757  L_1 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_ambisonicPluginName_0(L_1);
		AudioExtensionDefinition_t2916689971 * L_2 = ___definitionIn1;
		__this->set_definition_1(L_2);
		return;
	}
}
// System.Void UnityEngine.AudioClip::.ctor()
extern "C"  void AudioClip__ctor_m891747244 (AudioClip_t1419814565 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioClip__ctor_m891747244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_PCMReaderCallback_2((PCMReaderCallback_t2245753777 *)NULL);
		__this->set_m_PCMSetPositionCallback_3((PCMSetPositionCallback_t2684603915 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.AudioClip::get_length()
extern "C"  float AudioClip_get_length_m2655312187 (AudioClip_t1419814565 * __this, const RuntimeMethod* method)
{
	typedef float (*AudioClip_get_length_m2655312187_ftn) (AudioClip_t1419814565 *);
	static AudioClip_get_length_m2655312187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m2655312187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.AudioClip::get_samples()
extern "C"  int32_t AudioClip_get_samples_m4026431183 (AudioClip_t1419814565 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AudioClip_get_samples_m4026431183_ftn) (AudioClip_t1419814565 *);
	static AudioClip_get_samples_m4026431183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_samples_m4026431183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_samples()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.AudioClip::get_frequency()
extern "C"  int32_t AudioClip_get_frequency_m464492074 (AudioClip_t1419814565 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AudioClip_get_frequency_m464492074_ftn) (AudioClip_t1419814565 *);
	static AudioClip_get_frequency_m464492074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_frequency_m464492074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_frequency()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.AudioClip::get_ambisonic()
extern "C"  bool AudioClip_get_ambisonic_m4289332812 (AudioClip_t1419814565 * __this, const RuntimeMethod* method)
{
	typedef bool (*AudioClip_get_ambisonic_m4289332812_ftn) (AudioClip_t1419814565 *);
	static AudioClip_get_ambisonic_m4289332812_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_ambisonic_m4289332812_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_ambisonic()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C"  void AudioClip_InvokePCMReaderCallback_Internal_m2012548836 (AudioClip_t1419814565 * __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method)
{
	{
		PCMReaderCallback_t2245753777 * L_0 = __this->get_m_PCMReaderCallback_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMReaderCallback_t2245753777 * L_1 = __this->get_m_PCMReaderCallback_2();
		SingleU5BU5D_t2843050510* L_2 = ___data0;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m1100627178(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C"  void AudioClip_InvokePCMSetPositionCallback_Internal_m2631009500 (AudioClip_t1419814565 * __this, int32_t ___position0, const RuntimeMethod* method)
{
	{
		PCMSetPositionCallback_t2684603915 * L_0 = __this->get_m_PCMSetPositionCallback_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMSetPositionCallback_t2684603915 * L_1 = __this->get_m_PCMSetPositionCallback_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m3131925813(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMReaderCallback_t2245753777 (PCMReaderCallback_t2245753777 * __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___data0' to native representation
	float* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<float*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMReaderCallback__ctor_m2625355556 (PCMReaderCallback_t2245753777 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m1100627178 (PCMReaderCallback_t2245753777 * __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMReaderCallback_Invoke_m1100627178((PCMReaderCallback_t2245753777 *)__this->get_prev_9(),___data0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___data0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SingleU5BU5D_t2843050510* ___data0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___data0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___data0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* PCMReaderCallback_BeginInvoke_m2996855574 (PCMReaderCallback_t2245753777 * __this, SingleU5BU5D_t2843050510* ___data0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMReaderCallback_EndInvoke_m1602346067 (PCMReaderCallback_t2245753777 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_PCMSetPositionCallback_t2684603915 (PCMSetPositionCallback_t2684603915 * __this, int32_t ___position0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___position0);

}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMSetPositionCallback__ctor_m2497052609 (PCMSetPositionCallback_t2684603915 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m3131925813 (PCMSetPositionCallback_t2684603915 * __this, int32_t ___position0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMSetPositionCallback_Invoke_m3131925813((PCMSetPositionCallback_t2684603915 *)__this->get_prev_9(),___position0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, int32_t ___position0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___position0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___position0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___position0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* PCMSetPositionCallback_BeginInvoke_m2102179571 (PCMSetPositionCallback_t2684603915 * __this, int32_t ___position0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PCMSetPositionCallback_BeginInvoke_m2102179571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t972567508_il2cpp_TypeInfo_var, &___position0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMSetPositionCallback_EndInvoke_m385816721 (PCMSetPositionCallback_t2684603915 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioExtensionDefinition::.ctor(System.String,System.String,System.String)
extern "C"  void AudioExtensionDefinition__ctor_m3723261159 (AudioExtensionDefinition_t2916689971 * __this, String_t* ___assemblyNameIn0, String_t* ___extensionNamespaceIn1, String_t* ___extensionTypeNameIn2, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___assemblyNameIn0;
		__this->set_assemblyName_0(L_0);
		String_t* L_1 = ___extensionNamespaceIn1;
		__this->set_extensionNamespace_1(L_1);
		String_t* L_2 = ___extensionTypeNameIn2;
		__this->set_extensionTypeName_2(L_2);
		Type_t * L_3 = AudioExtensionDefinition_GetExtensionType_m513008212(__this, /*hidden argument*/NULL);
		__this->set_extensionType_3(L_3);
		return;
	}
}
// System.Type UnityEngine.AudioExtensionDefinition::GetExtensionType()
extern "C"  Type_t * AudioExtensionDefinition_GetExtensionType_m513008212 (AudioExtensionDefinition_t2916689971 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionDefinition_GetExtensionType_m513008212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = __this->get_extensionType_3();
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2511808107* L_1 = ((StringU5BU5D_t2511808107*)SZArrayNew(StringU5BU5D_t2511808107_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_2 = __this->get_extensionNamespace_1();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_2);
		StringU5BU5D_t2511808107* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral2957364442);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2957364442);
		StringU5BU5D_t2511808107* L_4 = L_3;
		String_t* L_5 = __this->get_extensionTypeName_2();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_5);
		StringU5BU5D_t2511808107* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral591250);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral591250);
		StringU5BU5D_t2511808107* L_7 = L_6;
		String_t* L_8 = __this->get_assemblyName_0();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3123563816(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m605100414, L_9, "UnityEngine.AudioModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		__this->set_extensionType_3(L_10);
	}

IL_004d:
	{
		Type_t * L_11 = __this->get_extensionType_3();
		V_0 = L_11;
		goto IL_0059;
	}

IL_0059:
	{
		Type_t * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.AudioExtensionManager::GetAudioListener()
extern "C"  Object_t692178351 * AudioExtensionManager_GetAudioListener_m1216759061 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Object_t692178351 * (*AudioExtensionManager_GetAudioListener_m1216759061_ftn) ();
	static AudioExtensionManager_GetAudioListener_m1216759061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioExtensionManager_GetAudioListener_m1216759061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioExtensionManager::GetAudioListener()");
	Object_t692178351 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.AudioSourceExtension UnityEngine.AudioExtensionManager::AddSpatializerExtension(UnityEngine.AudioSource)
extern "C"  AudioSourceExtension_t1155560466 * AudioExtensionManager_AddSpatializerExtension_m375267302 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_AddSpatializerExtension_m375267302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	AudioSpatializerExtensionDefinition_t2045914242 * V_1 = NULL;
	Enumerator_t941409773  V_2;
	memset(&V_2, 0, sizeof(V_2));
	AudioSourceExtension_t1155560466 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AudioSource_t4025721661 * L_0 = ___source0;
		NullCheck(L_0);
		bool L_1 = AudioSource_get_spatialize_m2425555736(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (AudioSourceExtension_t1155560466 *)NULL;
		goto IL_00dc;
	}

IL_0013:
	{
		AudioSource_t4025721661 * L_2 = ___source0;
		NullCheck(L_2);
		AudioSourceExtension_t1155560466 * L_3 = L_2->get_spatializerExtension_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_3, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		AudioSource_t4025721661 * L_5 = ___source0;
		NullCheck(L_5);
		AudioSourceExtension_t1155560466 * L_6 = L_5->get_spatializerExtension_2();
		V_0 = L_6;
		goto IL_00dc;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterBuiltinDefinitions_m600067160(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1165541249 * L_7 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceSpatializerExtensionDefinitions_1();
		NullCheck(L_7);
		Enumerator_t941409773  L_8 = List_1_GetEnumerator_m4108440129(L_7, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_2 = L_8;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b6;
		}

IL_0046:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_9 = Enumerator_get_Current_m2378857811((&V_2), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_1 = L_9;
			String_t* L_10 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_11 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_12 = V_1;
			NullCheck(L_12);
			PropertyName_t65259757  L_13 = L_12->get_spatializerName_0();
			bool L_14 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_00b5;
			}
		}

IL_0069:
		{
			AudioSource_t4025721661 * L_15 = ___source0;
			AudioSpatializerExtensionDefinition_t2045914242 * L_16 = V_1;
			NullCheck(L_16);
			AudioExtensionDefinition_t2916689971 * L_17 = L_16->get_definition_1();
			NullCheck(L_17);
			Type_t * L_18 = AudioExtensionDefinition_GetExtensionType_m513008212(L_17, /*hidden argument*/NULL);
			NullCheck(L_15);
			AudioSourceExtension_t1155560466 * L_19 = AudioSource_AddSpatializerExtension_m699023343(L_15, L_18, /*hidden argument*/NULL);
			V_3 = L_19;
			AudioSourceExtension_t1155560466 * L_20 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
			bool L_21 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_20, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00b4;
			}
		}

IL_0088:
		{
			AudioSourceExtension_t1155560466 * L_22 = V_3;
			AudioSource_t4025721661 * L_23 = ___source0;
			NullCheck(L_22);
			AudioSourceExtension_set_audioSource_m2096082894(L_22, L_23, /*hidden argument*/NULL);
			AudioSource_t4025721661 * L_24 = ___source0;
			AudioSourceExtension_t1155560466 * L_25 = V_3;
			NullCheck(L_24);
			L_24->set_spatializerExtension_2(L_25);
			AudioSourceExtension_t1155560466 * L_26 = V_3;
			AudioSpatializerExtensionDefinition_t2045914242 * L_27 = V_1;
			NullCheck(L_27);
			AudioExtensionDefinition_t2916689971 * L_28 = L_27->get_definition_1();
			NullCheck(L_28);
			Type_t * L_29 = AudioExtensionDefinition_GetExtensionType_m513008212(L_28, /*hidden argument*/NULL);
			NullCheck(L_29);
			String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
			IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
			AudioExtensionManager_WriteExtensionProperties_m1007984039(NULL /*static, unused*/, L_26, L_30, /*hidden argument*/NULL);
			AudioSourceExtension_t1155560466 * L_31 = V_3;
			V_0 = L_31;
			IL2CPP_LEAVE(0xDC, FINALLY_00c7);
		}

IL_00b4:
		{
		}

IL_00b5:
		{
		}

IL_00b6:
		{
			bool L_32 = Enumerator_MoveNext_m3997841088((&V_2), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_32)
			{
				goto IL_0046;
			}
		}

IL_00c2:
		{
			IL2CPP_LEAVE(0xD5, FINALLY_00c7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_00c7;
	}

FINALLY_00c7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_2), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(199)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(199)
	{
		IL2CPP_JUMP_TBL(0xDC, IL_00dc)
		IL2CPP_JUMP_TBL(0xD5, IL_00d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_00d5:
	{
		V_0 = (AudioSourceExtension_t1155560466 *)NULL;
		goto IL_00dc;
	}

IL_00dc:
	{
		AudioSourceExtension_t1155560466 * L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.AudioSourceExtension UnityEngine.AudioExtensionManager::AddAmbisonicDecoderExtension(UnityEngine.AudioSource)
extern "C"  AudioSourceExtension_t1155560466 * AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	AudioAmbisonicExtensionDefinition_t4140913893 * V_1 = NULL;
	Enumerator_t3036409424  V_2;
	memset(&V_2, 0, sizeof(V_2));
	AudioSourceExtension_t1155560466 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AudioSource_t4025721661 * L_0 = ___source0;
		NullCheck(L_0);
		AudioSourceExtension_t1155560466 * L_1 = L_0->get_ambisonicExtension_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_1, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		AudioSource_t4025721661 * L_3 = ___source0;
		NullCheck(L_3);
		AudioSourceExtension_t1155560466 * L_4 = L_3->get_ambisonicExtension_3();
		V_0 = L_4;
		goto IL_00b4;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterBuiltinDefinitions_m600067160(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t3260540900 * L_5 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceAmbisonicDecoderExtensionDefinitions_2();
		NullCheck(L_5);
		Enumerator_t3036409424  L_6 = List_1_GetEnumerator_m269488467(L_5, /*hidden argument*/List_1_GetEnumerator_m269488467_RuntimeMethod_var);
		V_2 = L_6;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008e;
		}

IL_0034:
		{
			AudioAmbisonicExtensionDefinition_t4140913893 * L_7 = Enumerator_get_Current_m3859005127((&V_2), /*hidden argument*/Enumerator_get_Current_m3859005127_RuntimeMethod_var);
			V_1 = L_7;
			String_t* L_8 = AudioSettings_GetAmbisonicDecoderPluginName_m854307786(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_9 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			AudioAmbisonicExtensionDefinition_t4140913893 * L_10 = V_1;
			NullCheck(L_10);
			PropertyName_t65259757  L_11 = L_10->get_ambisonicPluginName_0();
			bool L_12 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_008d;
			}
		}

IL_0057:
		{
			AudioSource_t4025721661 * L_13 = ___source0;
			AudioAmbisonicExtensionDefinition_t4140913893 * L_14 = V_1;
			NullCheck(L_14);
			AudioExtensionDefinition_t2916689971 * L_15 = L_14->get_definition_1();
			NullCheck(L_15);
			Type_t * L_16 = AudioExtensionDefinition_GetExtensionType_m513008212(L_15, /*hidden argument*/NULL);
			NullCheck(L_13);
			AudioSourceExtension_t1155560466 * L_17 = AudioSource_AddAmbisonicExtension_m1752284328(L_13, L_16, /*hidden argument*/NULL);
			V_3 = L_17;
			AudioSourceExtension_t1155560466 * L_18 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
			bool L_19 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_18, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_008c;
			}
		}

IL_0076:
		{
			AudioSourceExtension_t1155560466 * L_20 = V_3;
			AudioSource_t4025721661 * L_21 = ___source0;
			NullCheck(L_20);
			AudioSourceExtension_set_audioSource_m2096082894(L_20, L_21, /*hidden argument*/NULL);
			AudioSource_t4025721661 * L_22 = ___source0;
			AudioSourceExtension_t1155560466 * L_23 = V_3;
			NullCheck(L_22);
			L_22->set_ambisonicExtension_3(L_23);
			AudioSourceExtension_t1155560466 * L_24 = V_3;
			V_0 = L_24;
			IL2CPP_LEAVE(0xB4, FINALLY_009f);
		}

IL_008c:
		{
		}

IL_008d:
		{
		}

IL_008e:
		{
			bool L_25 = Enumerator_MoveNext_m1883253745((&V_2), /*hidden argument*/Enumerator_MoveNext_m1883253745_RuntimeMethod_var);
			if (L_25)
			{
				goto IL_0034;
			}
		}

IL_009a:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_009f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_009f;
	}

FINALLY_009f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2943463037((&V_2), /*hidden argument*/Enumerator_Dispose_m2943463037_RuntimeMethod_var);
		IL2CPP_END_FINALLY(159)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(159)
	{
		IL2CPP_JUMP_TBL(0xB4, IL_00b4)
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_00ad:
	{
		V_0 = (AudioSourceExtension_t1155560466 *)NULL;
		goto IL_00b4;
	}

IL_00b4:
	{
		AudioSourceExtension_t1155560466 * L_26 = V_0;
		return L_26;
	}
}
// System.Void UnityEngine.AudioExtensionManager::WriteExtensionProperties(UnityEngine.AudioSourceExtension,System.String)
extern "C"  void AudioExtensionManager_WriteExtensionProperties_m1007984039 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, String_t* ___extensionName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_WriteExtensionProperties_m1007984039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SpatializerExtensionName_7();
		PropertyName_t65259757  L_1 = PropertyName_op_Implicit_m920796712(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		bool L_2 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___extensionName1;
		PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SpatializerExtensionName_7(L_4);
	}

IL_0021:
	{
		V_0 = 0;
		goto IL_006d;
	}

IL_0028:
	{
		AudioSourceExtension_t1155560466 * L_5 = ___extension0;
		NullCheck(L_5);
		AudioSource_t4025721661 * L_6 = AudioSourceExtension_get_audioSource_m500534479(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		PropertyName_t65259757  L_8 = AudioSource_ReadExtensionName_m3482741001(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_9 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SpatializerExtensionName_7();
		bool L_10 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_11 = ___extension0;
		NullCheck(L_11);
		AudioSource_t4025721661 * L_12 = AudioSourceExtension_get_audioSource_m500534479(L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PropertyName_t65259757  L_14 = AudioSource_ReadExtensionPropertyName_m3408990507(L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		AudioSourceExtension_t1155560466 * L_15 = ___extension0;
		NullCheck(L_15);
		AudioSource_t4025721661 * L_16 = AudioSourceExtension_get_audioSource_m500534479(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		float L_18 = AudioSource_ReadExtensionPropertyValue_m3076907217(L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		AudioSourceExtension_t1155560466 * L_19 = ___extension0;
		PropertyName_t65259757  L_20 = V_1;
		float L_21 = V_2;
		NullCheck(L_19);
		VirtActionInvoker2< PropertyName_t65259757 , float >::Invoke(4 /* System.Void UnityEngine.AudioSourceExtension::WriteExtensionProperty(UnityEngine.PropertyName,System.Single) */, L_19, L_20, L_21);
	}

IL_0068:
	{
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006d:
	{
		int32_t L_23 = V_0;
		AudioSourceExtension_t1155560466 * L_24 = ___extension0;
		NullCheck(L_24);
		AudioSource_t4025721661 * L_25 = AudioSourceExtension_get_audioSource_m500534479(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = AudioSource_GetNumExtensionProperties_m206409171(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_23) < ((int32_t)L_26)))
		{
			goto IL_0028;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_27 = ___extension0;
		NullCheck(L_27);
		AudioSource_t4025721661 * L_28 = AudioSourceExtension_get_audioSource_m500534479(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_29 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SpatializerExtensionName_7();
		NullCheck(L_28);
		AudioSource_ClearExtensionProperties_m310946178(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AudioListenerExtension UnityEngine.AudioExtensionManager::AddSpatializerExtension(UnityEngine.AudioListener)
extern "C"  AudioListenerExtension_t2090751094 * AudioExtensionManager_AddSpatializerExtension_m837049305 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___listener0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_AddSpatializerExtension_m837049305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioListenerExtension_t2090751094 * V_0 = NULL;
	AudioSpatializerExtensionDefinition_t2045914242 * V_1 = NULL;
	Enumerator_t941409773  V_2;
	memset(&V_2, 0, sizeof(V_2));
	AudioListenerExtension_t2090751094 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AudioListener_t3253228504 * L_0 = ___listener0;
		NullCheck(L_0);
		AudioListenerExtension_t2090751094 * L_1 = L_0->get_spatializerExtension_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_1, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		AudioListener_t3253228504 * L_3 = ___listener0;
		NullCheck(L_3);
		AudioListenerExtension_t2090751094 * L_4 = L_3->get_spatializerExtension_2();
		V_0 = L_4;
		goto IL_00e4;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterBuiltinDefinitions_m600067160(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1165541249 * L_5 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionDefinitions_0();
		NullCheck(L_5);
		Enumerator_t941409773  L_6 = List_1_GetEnumerator_m4108440129(L_5, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_2 = L_6;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00be;
		}

IL_0034:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_7 = Enumerator_get_Current_m2378857811((&V_2), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_1 = L_7;
			String_t* L_8 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_9 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_10 = V_1;
			NullCheck(L_10);
			PropertyName_t65259757  L_11 = L_10->get_spatializerName_0();
			bool L_12 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_0071;
			}
		}

IL_0057:
		{
			String_t* L_13 = AudioSettings_GetAmbisonicDecoderPluginName_m854307786(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_14 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_15 = V_1;
			NullCheck(L_15);
			PropertyName_t65259757  L_16 = L_15->get_spatializerName_0();
			bool L_17 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_00bd;
			}
		}

IL_0071:
		{
			AudioListener_t3253228504 * L_18 = ___listener0;
			AudioSpatializerExtensionDefinition_t2045914242 * L_19 = V_1;
			NullCheck(L_19);
			AudioExtensionDefinition_t2916689971 * L_20 = L_19->get_definition_1();
			NullCheck(L_20);
			Type_t * L_21 = AudioExtensionDefinition_GetExtensionType_m513008212(L_20, /*hidden argument*/NULL);
			NullCheck(L_18);
			AudioListenerExtension_t2090751094 * L_22 = AudioListener_AddExtension_m2595025457(L_18, L_21, /*hidden argument*/NULL);
			V_3 = L_22;
			AudioListenerExtension_t2090751094 * L_23 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
			bool L_24 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_23, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00bc;
			}
		}

IL_0090:
		{
			AudioListenerExtension_t2090751094 * L_25 = V_3;
			AudioListener_t3253228504 * L_26 = ___listener0;
			NullCheck(L_25);
			AudioListenerExtension_set_audioListener_m2623015159(L_25, L_26, /*hidden argument*/NULL);
			AudioListener_t3253228504 * L_27 = ___listener0;
			AudioListenerExtension_t2090751094 * L_28 = V_3;
			NullCheck(L_27);
			L_27->set_spatializerExtension_2(L_28);
			AudioListenerExtension_t2090751094 * L_29 = V_3;
			AudioSpatializerExtensionDefinition_t2045914242 * L_30 = V_1;
			NullCheck(L_30);
			AudioExtensionDefinition_t2916689971 * L_31 = L_30->get_definition_1();
			NullCheck(L_31);
			Type_t * L_32 = AudioExtensionDefinition_GetExtensionType_m513008212(L_31, /*hidden argument*/NULL);
			NullCheck(L_32);
			String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_32);
			IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
			AudioExtensionManager_WriteExtensionProperties_m3816343998(NULL /*static, unused*/, L_29, L_33, /*hidden argument*/NULL);
			AudioListenerExtension_t2090751094 * L_34 = V_3;
			V_0 = L_34;
			IL2CPP_LEAVE(0xE4, FINALLY_00cf);
		}

IL_00bc:
		{
		}

IL_00bd:
		{
		}

IL_00be:
		{
			bool L_35 = Enumerator_MoveNext_m3997841088((&V_2), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_35)
			{
				goto IL_0034;
			}
		}

IL_00ca:
		{
			IL2CPP_LEAVE(0xDD, FINALLY_00cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_00cf;
	}

FINALLY_00cf:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_2), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(207)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(207)
	{
		IL2CPP_JUMP_TBL(0xE4, IL_00e4)
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_00dd:
	{
		V_0 = (AudioListenerExtension_t2090751094 *)NULL;
		goto IL_00e4;
	}

IL_00e4:
	{
		AudioListenerExtension_t2090751094 * L_36 = V_0;
		return L_36;
	}
}
// System.Void UnityEngine.AudioExtensionManager::WriteExtensionProperties(UnityEngine.AudioListenerExtension,System.String)
extern "C"  void AudioExtensionManager_WriteExtensionProperties_m3816343998 (RuntimeObject * __this /* static, unused */, AudioListenerExtension_t2090751094 * ___extension0, String_t* ___extensionName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_WriteExtensionProperties_m3816343998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionName_8();
		PropertyName_t65259757  L_1 = PropertyName_op_Implicit_m920796712(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		bool L_2 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___extensionName1;
		PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_ListenerSpatializerExtensionName_8(L_4);
	}

IL_0021:
	{
		V_0 = 0;
		goto IL_006d;
	}

IL_0028:
	{
		AudioListenerExtension_t2090751094 * L_5 = ___extension0;
		NullCheck(L_5);
		AudioListener_t3253228504 * L_6 = AudioListenerExtension_get_audioListener_m3069071264(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		PropertyName_t65259757  L_8 = AudioListener_ReadExtensionName_m5265467(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_9 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionName_8();
		bool L_10 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		AudioListenerExtension_t2090751094 * L_11 = ___extension0;
		NullCheck(L_11);
		AudioListener_t3253228504 * L_12 = AudioListenerExtension_get_audioListener_m3069071264(L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PropertyName_t65259757  L_14 = AudioListener_ReadExtensionPropertyName_m298351786(L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		AudioListenerExtension_t2090751094 * L_15 = ___extension0;
		NullCheck(L_15);
		AudioListener_t3253228504 * L_16 = AudioListenerExtension_get_audioListener_m3069071264(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		float L_18 = AudioListener_ReadExtensionPropertyValue_m2875278341(L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		AudioListenerExtension_t2090751094 * L_19 = ___extension0;
		PropertyName_t65259757  L_20 = V_1;
		float L_21 = V_2;
		NullCheck(L_19);
		VirtActionInvoker2< PropertyName_t65259757 , float >::Invoke(4 /* System.Void UnityEngine.AudioListenerExtension::WriteExtensionProperty(UnityEngine.PropertyName,System.Single) */, L_19, L_20, L_21);
	}

IL_0068:
	{
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006d:
	{
		int32_t L_23 = V_0;
		AudioListenerExtension_t2090751094 * L_24 = ___extension0;
		NullCheck(L_24);
		AudioListener_t3253228504 * L_25 = AudioListenerExtension_get_audioListener_m3069071264(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = AudioListener_GetNumExtensionProperties_m2276445238(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_23) < ((int32_t)L_26)))
		{
			goto IL_0028;
		}
	}
	{
		AudioListenerExtension_t2090751094 * L_27 = ___extension0;
		NullCheck(L_27);
		AudioListener_t3253228504 * L_28 = AudioListenerExtension_get_audioListener_m3069071264(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		PropertyName_t65259757  L_29 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionName_8();
		NullCheck(L_28);
		AudioListener_ClearExtensionProperties_m2269379717(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Type UnityEngine.AudioExtensionManager::GetListenerSpatializerExtensionType()
extern "C"  Type_t * AudioExtensionManager_GetListenerSpatializerExtensionType_m3630031100 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_GetListenerSpatializerExtensionType_m3630031100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSpatializerExtensionDefinition_t2045914242 * V_0 = NULL;
	Enumerator_t941409773  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Type_t * V_2 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionDefinitions_0();
		NullCheck(L_0);
		Enumerator_t941409773  L_1 = List_1_GetEnumerator_m4108440129(L_0, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0012:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_2 = Enumerator_get_Current_m2378857811((&V_1), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_spatializerName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0046;
			}
		}

IL_0035:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_8 = V_0;
			NullCheck(L_8);
			AudioExtensionDefinition_t2916689971 * L_9 = L_8->get_definition_1();
			NullCheck(L_9);
			Type_t * L_10 = AudioExtensionDefinition_GetExtensionType_m513008212(L_9, /*hidden argument*/NULL);
			V_2 = L_10;
			IL2CPP_LEAVE(0x6D, FINALLY_0058);
		}

IL_0046:
		{
		}

IL_0047:
		{
			bool L_11 = Enumerator_MoveNext_m3997841088((&V_1), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_1), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0066:
	{
		V_2 = (Type_t *)NULL;
		goto IL_006d;
	}

IL_006d:
	{
		Type_t * L_12 = V_2;
		return L_12;
	}
}
// System.Type UnityEngine.AudioExtensionManager::GetListenerSpatializerExtensionEditorType()
extern "C"  Type_t * AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m422144144 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m422144144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSpatializerExtensionDefinition_t2045914242 * V_0 = NULL;
	Enumerator_t941409773  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Type_t * V_2 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionDefinitions_0();
		NullCheck(L_0);
		Enumerator_t941409773  L_1 = List_1_GetEnumerator_m4108440129(L_0, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0012:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_2 = Enumerator_get_Current_m2378857811((&V_1), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_spatializerName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0046;
			}
		}

IL_0035:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_8 = V_0;
			NullCheck(L_8);
			AudioExtensionDefinition_t2916689971 * L_9 = L_8->get_editorDefinition_2();
			NullCheck(L_9);
			Type_t * L_10 = AudioExtensionDefinition_GetExtensionType_m513008212(L_9, /*hidden argument*/NULL);
			V_2 = L_10;
			IL2CPP_LEAVE(0x6D, FINALLY_0058);
		}

IL_0046:
		{
		}

IL_0047:
		{
			bool L_11 = Enumerator_MoveNext_m3997841088((&V_1), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_1), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0066:
	{
		V_2 = (Type_t *)NULL;
		goto IL_006d;
	}

IL_006d:
	{
		Type_t * L_12 = V_2;
		return L_12;
	}
}
// System.Type UnityEngine.AudioExtensionManager::GetSourceSpatializerExtensionType()
extern "C"  Type_t * AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSpatializerExtensionDefinition_t2045914242 * V_0 = NULL;
	Enumerator_t941409773  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Type_t * V_2 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceSpatializerExtensionDefinitions_1();
		NullCheck(L_0);
		Enumerator_t941409773  L_1 = List_1_GetEnumerator_m4108440129(L_0, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0012:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_2 = Enumerator_get_Current_m2378857811((&V_1), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_spatializerName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0046;
			}
		}

IL_0035:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_8 = V_0;
			NullCheck(L_8);
			AudioExtensionDefinition_t2916689971 * L_9 = L_8->get_definition_1();
			NullCheck(L_9);
			Type_t * L_10 = AudioExtensionDefinition_GetExtensionType_m513008212(L_9, /*hidden argument*/NULL);
			V_2 = L_10;
			IL2CPP_LEAVE(0x6D, FINALLY_0058);
		}

IL_0046:
		{
		}

IL_0047:
		{
			bool L_11 = Enumerator_MoveNext_m3997841088((&V_1), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_1), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0066:
	{
		V_2 = (Type_t *)NULL;
		goto IL_006d;
	}

IL_006d:
	{
		Type_t * L_12 = V_2;
		return L_12;
	}
}
// System.Void UnityEngine.AudioExtensionManager::AddExtensionToManager(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_AddExtensionToManager_m2092709385 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_AddExtensionToManager_m2092709385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterBuiltinDefinitions_m600067160(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioSourceExtension_t1155560466 * L_0 = ___extension0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_m_ExtensionManagerUpdateIndex_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_2 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		AudioSourceExtension_t1155560466 * L_3 = ___extension0;
		NullCheck(L_2);
		List_1_Add_m29487416(L_2, L_3, /*hidden argument*/List_1_Add_m29487416_RuntimeMethod_var);
		AudioSourceExtension_t1155560466 * L_4 = ___extension0;
		List_1_t275187473 * L_5 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m789375335(L_5, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		NullCheck(L_4);
		L_4->set_m_ExtensionManagerUpdateIndex_3(((int32_t)((int32_t)L_6-(int32_t)1)));
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.AudioExtensionManager::RemoveExtensionFromManager(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_RemoveExtensionFromManager_m3149216797 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_RemoveExtensionFromManager_m3149216797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		AudioSourceExtension_t1155560466 * L_0 = ___extension0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_m_ExtensionManagerUpdateIndex_3();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_4 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m789375335(L_4, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_6 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m789375335(L_6, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		V_1 = ((int32_t)((int32_t)L_7-(int32_t)1));
		List_1_t275187473 * L_8 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_9 = V_0;
		List_1_t275187473 * L_10 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		AudioSourceExtension_t1155560466 * L_12 = List_1_get_Item_m609089971(L_10, L_11, /*hidden argument*/List_1_get_Item_m609089971_RuntimeMethod_var);
		NullCheck(L_8);
		List_1_set_Item_m2969022274(L_8, L_9, L_12, /*hidden argument*/List_1_set_Item_m2969022274_RuntimeMethod_var);
		List_1_t275187473 * L_13 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		AudioSourceExtension_t1155560466 * L_15 = List_1_get_Item_m609089971(L_13, L_14, /*hidden argument*/List_1_get_Item_m609089971_RuntimeMethod_var);
		int32_t L_16 = V_0;
		NullCheck(L_15);
		L_15->set_m_ExtensionManagerUpdateIndex_3(L_16);
		List_1_t275187473 * L_17 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		List_1_RemoveAt_m4186794102(L_17, L_18, /*hidden argument*/List_1_RemoveAt_m4186794102_RuntimeMethod_var);
	}

IL_0060:
	{
		AudioSourceExtension_t1155560466 * L_19 = ___extension0;
		NullCheck(L_19);
		L_19->set_m_ExtensionManagerUpdateIndex_3((-1));
		return;
	}
}
// System.Void UnityEngine.AudioExtensionManager::Update()
extern "C"  void AudioExtensionManager_Update_m3577106864 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_Update_m3577106864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioListener_t3253228504 * V_0 = NULL;
	AudioListenerExtension_t2090751094 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	AudioSourceExtension_t1155560466 * V_5 = NULL;
	int32_t G_B16_0 = 0;
	int32_t G_B19_0 = 0;
	int32_t G_B27_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterBuiltinDefinitions_m600067160(NULL /*static, unused*/, /*hidden argument*/NULL);
		PropertyName_t65259757  L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SpatializerName_6();
		String_t* L_1 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
		PropertyName_t65259757  L_2 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_3 = PropertyName_op_Inequality_m3593548498(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_4 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
		PropertyName_t65259757  L_5 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SpatializerName_6(L_5);
		Type_t * L_6 = AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		Type_t * L_7 = AudioExtensionManager_GetSourceSpatializerExtensionType_m1742627288(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		PropertyName_t65259757  L_9 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SpatializerExtensionName_7(L_9);
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		Type_t * L_10 = AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m422144144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		Type_t * L_11 = AudioExtensionManager_GetListenerSpatializerExtensionType_m3630031100(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_11);
		PropertyName_t65259757  L_13 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_ListenerSpatializerExtensionName_8(L_13);
	}

IL_006b:
	{
	}

IL_006c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		Object_t692178351 * L_14 = AudioExtensionManager_GetAudioListener_m1216759061(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((AudioListener_t3253228504 *)IsInstSealed((RuntimeObject*)L_14, AudioListener_t3253228504_il2cpp_TypeInfo_var));
		AudioListener_t3253228504 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_15, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_009e;
		}
	}
	{
		AudioListener_t3253228504 * L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioListenerExtension_t2090751094 * L_18 = AudioExtensionManager_AddSpatializerExtension_m837049305(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		AudioListenerExtension_t2090751094 * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_19, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_009d;
		}
	}
	{
		AudioListenerExtension_t2090751094 * L_21 = V_1;
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.AudioListenerExtension::ExtensionUpdate() */, L_21);
	}

IL_009d:
	{
	}

IL_009e:
	{
		V_2 = 0;
		goto IL_00b9;
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_22 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		AudioSourceExtension_t1155560466 * L_24 = List_1_get_Item_m609089971(L_22, L_23, /*hidden argument*/List_1_get_Item_m609089971_RuntimeMethod_var);
		NullCheck(L_24);
		VirtActionInvoker0::Invoke(7 /* System.Void UnityEngine.AudioSourceExtension::ExtensionUpdate() */, L_24);
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_27 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m789375335(L_27, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		int32_t L_29 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		List_1_t275187473 * L_30 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m789375335(L_30, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_00e3;
		}
	}
	{
		G_B16_0 = 0;
		goto IL_00e8;
	}

IL_00e3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		int32_t L_32 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		G_B16_0 = L_32;
	}

IL_00e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_NextStopIndex_4(G_B16_0);
		List_1_t275187473 * L_33 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m789375335(L_33, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		if ((((int32_t)L_34) <= ((int32_t)0)))
		{
			goto IL_0110;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_35 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m789375335(L_35, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		G_B19_0 = ((int32_t)((int32_t)1+(int32_t)((int32_t)((int32_t)L_36/(int32_t)8))));
		goto IL_0111;
	}

IL_0110:
	{
		G_B19_0 = 0;
	}

IL_0111:
	{
		V_3 = G_B19_0;
		V_4 = 0;
		goto IL_01ae;
	}

IL_011a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t275187473 * L_37 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		int32_t L_38 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		NullCheck(L_37);
		AudioSourceExtension_t1155560466 * L_39 = List_1_get_Item_m609089971(L_37, L_38, /*hidden argument*/List_1_get_Item_m609089971_RuntimeMethod_var);
		V_5 = L_39;
		AudioSourceExtension_t1155560466 * L_40 = V_5;
		NullCheck(L_40);
		AudioSource_t4025721661 * L_41 = AudioSourceExtension_get_audioSource_m500534479(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_41, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0160;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_43 = V_5;
		NullCheck(L_43);
		AudioSource_t4025721661 * L_44 = AudioSourceExtension_get_audioSource_m500534479(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		bool L_45 = Behaviour_get_enabled_m3996741937(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0160;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_46 = V_5;
		NullCheck(L_46);
		AudioSource_t4025721661 * L_47 = AudioSourceExtension_get_audioSource_m500534479(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		bool L_48 = AudioSource_get_isPlaying_m1553006287(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_0175;
		}
	}

IL_0160:
	{
		AudioSourceExtension_t1155560466 * L_49 = V_5;
		NullCheck(L_49);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.AudioSourceExtension::Stop() */, L_49);
		AudioSourceExtension_t1155560466 * L_50 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RemoveExtensionFromManager_m3149216797(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		goto IL_01a7;
	}

IL_0175:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		int32_t L_51 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_NextStopIndex_4(((int32_t)((int32_t)L_51+(int32_t)1)));
		int32_t L_52 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		List_1_t275187473 * L_53 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceExtensionsToUpdate_3();
		NullCheck(L_53);
		int32_t L_54 = List_1_get_Count_m789375335(L_53, /*hidden argument*/List_1_get_Count_m789375335_RuntimeMethod_var);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_019c;
		}
	}
	{
		G_B27_0 = 0;
		goto IL_01a1;
	}

IL_019c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		int32_t L_55 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_NextStopIndex_4();
		G_B27_0 = L_55;
	}

IL_01a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_NextStopIndex_4(G_B27_0);
	}

IL_01a7:
	{
		int32_t L_56 = V_4;
		V_4 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_01ae:
	{
		int32_t L_57 = V_4;
		int32_t L_58 = V_3;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_011a;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AudioExtensionManager::GetReadyToPlay(UnityEngine.AudioSourceExtension)
extern "C"  void AudioExtensionManager_GetReadyToPlay_m3146133677 (RuntimeObject * __this /* static, unused */, AudioSourceExtension_t1155560466 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_GetReadyToPlay_m3146133677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSourceExtension_t1155560466 * L_0 = ___extension0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_2 = ___extension0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.AudioSourceExtension::Play() */, L_2);
		AudioSourceExtension_t1155560466 * L_3 = ___extension0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_AddExtensionToManager_m2092709385(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityEngine.AudioExtensionManager::RegisterBuiltinDefinitions()
extern "C"  void AudioExtensionManager_RegisterBuiltinDefinitions_m600067160 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_RegisterBuiltinDefinitions_m600067160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	AudioExtensionDefinition_t2916689971 * V_1 = NULL;
	AudioExtensionDefinition_t2916689971 * V_2 = NULL;
	AudioExtensionDefinition_t2916689971 * V_3 = NULL;
	AudioExtensionDefinition_t2916689971 * V_4 = NULL;
	AudioExtensionDefinition_t2916689971 * V_5 = NULL;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		bool L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_BuiltinDefinitionsRegistered_5();
		if (L_0)
		{
			goto IL_00e0;
		}
	}
	{
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_2 = AudioSettings_GetSpatializerPluginName_m56128157(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_2, _stringLiteral1475799231, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_009a;
		}
	}

IL_0028:
	{
		AudioExtensionDefinition_t2916689971 * L_4 = (AudioExtensionDefinition_t2916689971 *)il2cpp_codegen_object_new(AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var);
		AudioExtensionDefinition__ctor_m3723261159(L_4, _stringLiteral577122778, _stringLiteral1656762229, _stringLiteral1623959416, /*hidden argument*/NULL);
		V_1 = L_4;
		AudioExtensionDefinition_t2916689971 * L_5 = (AudioExtensionDefinition_t2916689971 *)il2cpp_codegen_object_new(AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var);
		AudioExtensionDefinition__ctor_m3723261159(L_5, _stringLiteral1708419827, _stringLiteral3796107019, _stringLiteral873129047, /*hidden argument*/NULL);
		V_2 = L_5;
		AudioExtensionDefinition_t2916689971 * L_6 = V_1;
		AudioExtensionDefinition_t2916689971 * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterListenerSpatializerDefinition_m440281069(NULL /*static, unused*/, _stringLiteral1475799231, L_6, L_7, /*hidden argument*/NULL);
		AudioExtensionDefinition_t2916689971 * L_8 = (AudioExtensionDefinition_t2916689971 *)il2cpp_codegen_object_new(AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var);
		AudioExtensionDefinition__ctor_m3723261159(L_8, _stringLiteral577122778, _stringLiteral1656762229, _stringLiteral3572251999, /*hidden argument*/NULL);
		V_3 = L_8;
		AudioExtensionDefinition_t2916689971 * L_9 = (AudioExtensionDefinition_t2916689971 *)il2cpp_codegen_object_new(AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var);
		AudioExtensionDefinition__ctor_m3723261159(L_9, _stringLiteral1708419827, _stringLiteral3796107019, _stringLiteral227735072, /*hidden argument*/NULL);
		V_4 = L_9;
		AudioExtensionDefinition_t2916689971 * L_10 = V_3;
		AudioExtensionDefinition_t2916689971 * L_11 = V_4;
		AudioExtensionManager_RegisterSourceSpatializerDefinition_m349058419(NULL /*static, unused*/, _stringLiteral1475799231, L_10, L_11, /*hidden argument*/NULL);
	}

IL_009a:
	{
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_13 = AudioSettings_GetAmbisonicDecoderPluginName_m854307786(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_13, _stringLiteral1475799231, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d9;
		}
	}

IL_00b4:
	{
		AudioExtensionDefinition_t2916689971 * L_15 = (AudioExtensionDefinition_t2916689971 *)il2cpp_codegen_object_new(AudioExtensionDefinition_t2916689971_il2cpp_TypeInfo_var);
		AudioExtensionDefinition__ctor_m3723261159(L_15, _stringLiteral577122778, _stringLiteral1656762229, _stringLiteral1058676305, /*hidden argument*/NULL);
		V_5 = L_15;
		AudioExtensionDefinition_t2916689971 * L_16 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_RegisterSourceAmbisonicDefinition_m765937035(NULL /*static, unused*/, _stringLiteral1475799231, L_16, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_BuiltinDefinitionsRegistered_5((bool)1);
	}

IL_00e0:
	{
		return;
	}
}
// System.Boolean UnityEngine.AudioExtensionManager::RegisterListenerSpatializerDefinition(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterListenerSpatializerDefinition_m440281069 (RuntimeObject * __this /* static, unused */, String_t* ___spatializerName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, AudioExtensionDefinition_t2916689971 * ___editorDefinition2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_RegisterListenerSpatializerDefinition_m440281069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSpatializerExtensionDefinition_t2045914242 * V_0 = NULL;
	Enumerator_t941409773  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	AudioSpatializerExtensionDefinition_t2045914242 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionDefinitions_0();
		NullCheck(L_0);
		Enumerator_t941409773  L_1 = List_1_GetEnumerator_m4108440129(L_0, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0012:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_2 = Enumerator_get_Current_m2378857811((&V_1), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = ___spatializerName0;
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_spatializerName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0053;
			}
		}

IL_0031:
		{
			AudioExtensionDefinition_t2916689971 * L_8 = ___extensionDefinition1;
			NullCheck(L_8);
			Type_t * L_9 = AudioExtensionDefinition_GetExtensionType_m513008212(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m712489286(NULL /*static, unused*/, _stringLiteral2661309874, L_9, _stringLiteral3186058801, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
			Debug_Log_m3513156148(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x8E, FINALLY_0065);
		}

IL_0053:
		{
		}

IL_0054:
		{
			bool L_11 = Enumerator_MoveNext_m3997841088((&V_1), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_1), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0073:
	{
		String_t* L_12 = ___spatializerName0;
		AudioExtensionDefinition_t2916689971 * L_13 = ___extensionDefinition1;
		AudioExtensionDefinition_t2916689971 * L_14 = ___editorDefinition2;
		AudioSpatializerExtensionDefinition_t2045914242 * L_15 = (AudioSpatializerExtensionDefinition_t2045914242 *)il2cpp_codegen_object_new(AudioSpatializerExtensionDefinition_t2045914242_il2cpp_TypeInfo_var);
		AudioSpatializerExtensionDefinition__ctor_m507988252(L_15, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_16 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_ListenerSpatializerExtensionDefinitions_0();
		AudioSpatializerExtensionDefinition_t2045914242 * L_17 = V_3;
		NullCheck(L_16);
		List_1_Add_m2413286470(L_16, L_17, /*hidden argument*/List_1_Add_m2413286470_RuntimeMethod_var);
		V_2 = (bool)1;
		goto IL_008e;
	}

IL_008e:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Boolean UnityEngine.AudioExtensionManager::RegisterSourceSpatializerDefinition(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterSourceSpatializerDefinition_m349058419 (RuntimeObject * __this /* static, unused */, String_t* ___spatializerName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, AudioExtensionDefinition_t2916689971 * ___editorDefinition2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_RegisterSourceSpatializerDefinition_m349058419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSpatializerExtensionDefinition_t2045914242 * V_0 = NULL;
	Enumerator_t941409773  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	AudioSpatializerExtensionDefinition_t2045914242 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceSpatializerExtensionDefinitions_1();
		NullCheck(L_0);
		Enumerator_t941409773  L_1 = List_1_GetEnumerator_m4108440129(L_0, /*hidden argument*/List_1_GetEnumerator_m4108440129_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0012:
		{
			AudioSpatializerExtensionDefinition_t2045914242 * L_2 = Enumerator_get_Current_m2378857811((&V_1), /*hidden argument*/Enumerator_get_Current_m2378857811_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = ___spatializerName0;
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioSpatializerExtensionDefinition_t2045914242 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_spatializerName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0053;
			}
		}

IL_0031:
		{
			AudioExtensionDefinition_t2916689971 * L_8 = ___extensionDefinition1;
			NullCheck(L_8);
			Type_t * L_9 = AudioExtensionDefinition_GetExtensionType_m513008212(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m712489286(NULL /*static, unused*/, _stringLiteral2133610319, L_9, _stringLiteral3429621635, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
			Debug_Log_m3513156148(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x8E, FINALLY_0065);
		}

IL_0053:
		{
		}

IL_0054:
		{
			bool L_11 = Enumerator_MoveNext_m3997841088((&V_1), /*hidden argument*/Enumerator_MoveNext_m3997841088_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3350638329((&V_1), /*hidden argument*/Enumerator_Dispose_m3350638329_RuntimeMethod_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0073:
	{
		String_t* L_12 = ___spatializerName0;
		AudioExtensionDefinition_t2916689971 * L_13 = ___extensionDefinition1;
		AudioExtensionDefinition_t2916689971 * L_14 = ___editorDefinition2;
		AudioSpatializerExtensionDefinition_t2045914242 * L_15 = (AudioSpatializerExtensionDefinition_t2045914242 *)il2cpp_codegen_object_new(AudioSpatializerExtensionDefinition_t2045914242_il2cpp_TypeInfo_var);
		AudioSpatializerExtensionDefinition__ctor_m507988252(L_15, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t1165541249 * L_16 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceSpatializerExtensionDefinitions_1();
		AudioSpatializerExtensionDefinition_t2045914242 * L_17 = V_3;
		NullCheck(L_16);
		List_1_Add_m2413286470(L_16, L_17, /*hidden argument*/List_1_Add_m2413286470_RuntimeMethod_var);
		V_2 = (bool)1;
		goto IL_008e;
	}

IL_008e:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Boolean UnityEngine.AudioExtensionManager::RegisterSourceAmbisonicDefinition(System.String,UnityEngine.AudioExtensionDefinition)
extern "C"  bool AudioExtensionManager_RegisterSourceAmbisonicDefinition_m765937035 (RuntimeObject * __this /* static, unused */, String_t* ___ambisonicDecoderName0, AudioExtensionDefinition_t2916689971 * ___extensionDefinition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager_RegisterSourceAmbisonicDefinition_m765937035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioAmbisonicExtensionDefinition_t4140913893 * V_0 = NULL;
	Enumerator_t3036409424  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	AudioAmbisonicExtensionDefinition_t4140913893 * V_3 = NULL;
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t3260540900 * L_0 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceAmbisonicDecoderExtensionDefinitions_2();
		NullCheck(L_0);
		Enumerator_t3036409424  L_1 = List_1_GetEnumerator_m269488467(L_0, /*hidden argument*/List_1_GetEnumerator_m269488467_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0012:
		{
			AudioAmbisonicExtensionDefinition_t4140913893 * L_2 = Enumerator_get_Current_m3859005127((&V_1), /*hidden argument*/Enumerator_get_Current_m3859005127_RuntimeMethod_var);
			V_0 = L_2;
			String_t* L_3 = ___ambisonicDecoderName0;
			PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			AudioAmbisonicExtensionDefinition_t4140913893 * L_5 = V_0;
			NullCheck(L_5);
			PropertyName_t65259757  L_6 = L_5->get_ambisonicPluginName_0();
			bool L_7 = PropertyName_op_Equality_m1873673143(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0053;
			}
		}

IL_0031:
		{
			AudioExtensionDefinition_t2916689971 * L_8 = ___extensionDefinition1;
			NullCheck(L_8);
			Type_t * L_9 = AudioExtensionDefinition_GetExtensionType_m513008212(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m712489286(NULL /*static, unused*/, _stringLiteral1978627438, L_9, _stringLiteral329553197, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
			Debug_Log_m3513156148(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x8D, FINALLY_0065);
		}

IL_0053:
		{
		}

IL_0054:
		{
			bool L_11 = Enumerator_MoveNext_m1883253745((&V_1), /*hidden argument*/Enumerator_MoveNext_m1883253745_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2943463037((&V_1), /*hidden argument*/Enumerator_Dispose_m2943463037_RuntimeMethod_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0073:
	{
		String_t* L_12 = ___ambisonicDecoderName0;
		AudioExtensionDefinition_t2916689971 * L_13 = ___extensionDefinition1;
		AudioAmbisonicExtensionDefinition_t4140913893 * L_14 = (AudioAmbisonicExtensionDefinition_t4140913893 *)il2cpp_codegen_object_new(AudioAmbisonicExtensionDefinition_t4140913893_il2cpp_TypeInfo_var);
		AudioAmbisonicExtensionDefinition__ctor_m2030192098(L_14, L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		List_1_t3260540900 * L_15 = ((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->get_m_SourceAmbisonicDecoderExtensionDefinitions_2();
		AudioAmbisonicExtensionDefinition_t4140913893 * L_16 = V_3;
		NullCheck(L_15);
		List_1_Add_m2970273886(L_15, L_16, /*hidden argument*/List_1_Add_m2970273886_RuntimeMethod_var);
		V_2 = (bool)1;
		goto IL_008d;
	}

IL_008d:
	{
		bool L_17 = V_2;
		return L_17;
	}
}
// System.Void UnityEngine.AudioExtensionManager::.cctor()
extern "C"  void AudioExtensionManager__cctor_m2972930274 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioExtensionManager__cctor_m2972930274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1165541249 * L_0 = (List_1_t1165541249 *)il2cpp_codegen_object_new(List_1_t1165541249_il2cpp_TypeInfo_var);
		List_1__ctor_m57615730(L_0, /*hidden argument*/List_1__ctor_m57615730_RuntimeMethod_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_ListenerSpatializerExtensionDefinitions_0(L_0);
		List_1_t1165541249 * L_1 = (List_1_t1165541249 *)il2cpp_codegen_object_new(List_1_t1165541249_il2cpp_TypeInfo_var);
		List_1__ctor_m57615730(L_1, /*hidden argument*/List_1__ctor_m57615730_RuntimeMethod_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SourceSpatializerExtensionDefinitions_1(L_1);
		List_1_t3260540900 * L_2 = (List_1_t3260540900 *)il2cpp_codegen_object_new(List_1_t3260540900_il2cpp_TypeInfo_var);
		List_1__ctor_m1918620480(L_2, /*hidden argument*/List_1__ctor_m1918620480_RuntimeMethod_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SourceAmbisonicDecoderExtensionDefinitions_2(L_2);
		List_1_t275187473 * L_3 = (List_1_t275187473 *)il2cpp_codegen_object_new(List_1_t275187473_il2cpp_TypeInfo_var);
		List_1__ctor_m4205201701(L_3, /*hidden argument*/List_1__ctor_m4205201701_RuntimeMethod_var);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SourceExtensionsToUpdate_3(L_3);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_NextStopIndex_4(0);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_BuiltinDefinitionsRegistered_5((bool)0);
		PropertyName_t65259757  L_4 = PropertyName_op_Implicit_m920796712(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SpatializerName_6(L_4);
		PropertyName_t65259757  L_5 = PropertyName_op_Implicit_m920796712(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_SpatializerExtensionName_7(L_5);
		PropertyName_t65259757  L_6 = PropertyName_op_Implicit_m920796712(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		((AudioExtensionManager_t3958046208_StaticFields*)il2cpp_codegen_static_fields_for(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var))->set_m_ListenerSpatializerExtensionName_8(L_6);
		return;
	}
}
// System.Void UnityEngine.AudioListener::set_pause(System.Boolean)
extern "C"  void AudioListener_set_pause_m777346538 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioListener_set_pause_m777346538_ftn) (bool);
	static AudioListener_set_pause_m777346538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_set_pause_m777346538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::set_pause(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.AudioListener::GetNumExtensionProperties()
extern "C"  int32_t AudioListener_GetNumExtensionProperties_m2276445238 (AudioListener_t3253228504 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AudioListener_GetNumExtensionProperties_m2276445238_ftn) (AudioListener_t3253228504 *);
	static AudioListener_GetNumExtensionProperties_m2276445238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_GetNumExtensionProperties_m2276445238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::GetNumExtensionProperties()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.PropertyName UnityEngine.AudioListener::ReadExtensionName(System.Int32)
extern "C"  PropertyName_t65259757  AudioListener_ReadExtensionName_m5265467 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method)
{
	PropertyName_t65259757  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___listenerIndex0;
		AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t65259757  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		PropertyName_t65259757  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, int32_t ___listenerIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method)
{
	typedef void (*AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872_ftn) (AudioListener_t3253228504 *, int32_t, PropertyName_t65259757 *);
	static AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_INTERNAL_CALL_ReadExtensionName_m2075953872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___listenerIndex1, ___value2);
}
// UnityEngine.PropertyName UnityEngine.AudioListener::ReadExtensionPropertyName(System.Int32)
extern "C"  PropertyName_t65259757  AudioListener_ReadExtensionPropertyName_m298351786 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method)
{
	PropertyName_t65259757  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___listenerIndex0;
		AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t65259757  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		PropertyName_t65259757  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, int32_t ___listenerIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method)
{
	typedef void (*AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180_ftn) (AudioListener_t3253228504 *, int32_t, PropertyName_t65259757 *);
	static AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m992131180_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___listenerIndex1, ___value2);
}
// System.Single UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)
extern "C"  float AudioListener_ReadExtensionPropertyValue_m2875278341 (AudioListener_t3253228504 * __this, int32_t ___listenerIndex0, const RuntimeMethod* method)
{
	typedef float (*AudioListener_ReadExtensionPropertyValue_m2875278341_ftn) (AudioListener_t3253228504 *, int32_t);
	static AudioListener_ReadExtensionPropertyValue_m2875278341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_ReadExtensionPropertyValue_m2875278341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)");
	float retVal = _il2cpp_icall_func(__this, ___listenerIndex0);
	return retVal;
}
// System.Void UnityEngine.AudioListener::ClearExtensionProperties(UnityEngine.PropertyName)
extern "C"  void AudioListener_ClearExtensionProperties_m2269379717 (AudioListener_t3253228504 * __this, PropertyName_t65259757  ___extensionName0, const RuntimeMethod* method)
{
	{
		AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443(NULL /*static, unused*/, __this, (&___extensionName0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)
extern "C"  void AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443 (RuntimeObject * __this /* static, unused */, AudioListener_t3253228504 * ___self0, PropertyName_t65259757 * ___extensionName1, const RuntimeMethod* method)
{
	typedef void (*AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443_ftn) (AudioListener_t3253228504 *, PropertyName_t65259757 *);
	static AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_INTERNAL_CALL_ClearExtensionProperties_m2528742443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___extensionName1);
}
// UnityEngine.AudioListenerExtension UnityEngine.AudioListener::AddExtension(System.Type)
extern "C"  AudioListenerExtension_t2090751094 * AudioListener_AddExtension_m2595025457 (AudioListener_t3253228504 * __this, Type_t * ___extensionType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioListener_AddExtension_m2595025457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioListenerExtension_t2090751094 * V_0 = NULL;
	{
		AudioListenerExtension_t2090751094 * L_0 = __this->get_spatializerExtension_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Type_t * L_2 = ___extensionType0;
		ScriptableObject_t1804531341 * L_3 = ScriptableObject_CreateInstance_m1486025286(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_spatializerExtension_2(((AudioListenerExtension_t2090751094 *)IsInstClass((RuntimeObject*)L_3, AudioListenerExtension_t2090751094_il2cpp_TypeInfo_var)));
	}

IL_0025:
	{
		AudioListenerExtension_t2090751094 * L_4 = __this->get_spatializerExtension_2();
		V_0 = L_4;
		goto IL_0031;
	}

IL_0031:
	{
		AudioListenerExtension_t2090751094 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.AudioListener UnityEngine.AudioListenerExtension::get_audioListener()
extern "C"  AudioListener_t3253228504 * AudioListenerExtension_get_audioListener_m3069071264 (AudioListenerExtension_t2090751094 * __this, const RuntimeMethod* method)
{
	AudioListener_t3253228504 * V_0 = NULL;
	{
		AudioListener_t3253228504 * L_0 = __this->get_m_audioListener_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		AudioListener_t3253228504 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AudioListenerExtension::set_audioListener(UnityEngine.AudioListener)
extern "C"  void AudioListenerExtension_set_audioListener_m2623015159 (AudioListenerExtension_t2090751094 * __this, AudioListener_t3253228504 * ___value0, const RuntimeMethod* method)
{
	{
		AudioListener_t3253228504 * L_0 = ___value0;
		__this->set_m_audioListener_2(L_0);
		return;
	}
}
// System.Void UnityEngine.AudioListenerExtension::WriteExtensionProperty(UnityEngine.PropertyName,System.Single)
extern "C"  void AudioListenerExtension_WriteExtensionProperty_m3970153720 (AudioListenerExtension_t2090751094 * __this, PropertyName_t65259757  ___propertyName0, float ___propertyValue1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AudioListenerExtension::ExtensionUpdate()
extern "C"  void AudioListenerExtension_ExtensionUpdate_m1859691407 (AudioListenerExtension_t2090751094 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.String UnityEngine.AudioSettings::GetSpatializerPluginName()
extern "C"  String_t* AudioSettings_GetSpatializerPluginName_m56128157 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*AudioSettings_GetSpatializerPluginName_m56128157_ftn) ();
	static AudioSettings_GetSpatializerPluginName_m56128157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSettings_GetSpatializerPluginName_m56128157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSettings::GetSpatializerPluginName()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m2141630212 (RuntimeObject * __this /* static, unused */, bool ___deviceWasChanged0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioConfigurationChanged_m2141630212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioConfigurationChangeHandler_t2715435404 * L_0 = ((AudioSettings_t1374326787_StaticFields*)il2cpp_codegen_static_fields_for(AudioSettings_t1374326787_il2cpp_TypeInfo_var))->get_OnAudioConfigurationChanged_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioConfigurationChangeHandler_t2715435404 * L_1 = ((AudioSettings_t1374326787_StaticFields*)il2cpp_codegen_static_fields_for(AudioSettings_t1374326787_il2cpp_TypeInfo_var))->get_OnAudioConfigurationChanged_0();
		bool L_2 = ___deviceWasChanged0;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m2092555472(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioManagerUpdate()
extern "C"  void AudioSettings_InvokeOnAudioManagerUpdate_m2143358649 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioManagerUpdate_m2143358649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_Update_m3577106864(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioSourcePlay(UnityEngine.AudioSource)
extern "C"  void AudioSettings_InvokeOnAudioSourcePlay_m239283393 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioSourcePlay_m239283393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	AudioSourceExtension_t1155560466 * V_1 = NULL;
	{
		AudioSource_t4025721661 * L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioSourceExtension_t1155560466 * L_1 = AudioExtensionManager_AddSpatializerExtension_m375267302(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		AudioSourceExtension_t1155560466 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_2, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_GetReadyToPlay_m3146133677(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001a:
	{
		AudioSource_t4025721661 * L_5 = ___source0;
		NullCheck(L_5);
		AudioClip_t1419814565 * L_6 = AudioSource_get_clip_m4008781082(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_6, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		AudioSource_t4025721661 * L_8 = ___source0;
		NullCheck(L_8);
		AudioClip_t1419814565 * L_9 = AudioSource_get_clip_m4008781082(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = AudioClip_get_ambisonic_m4289332812(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0056;
		}
	}
	{
		AudioSource_t4025721661 * L_11 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioSourceExtension_t1155560466 * L_12 = AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		AudioSourceExtension_t1155560466 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_13, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0055;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_GetReadyToPlay_m3146133677(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0055:
	{
	}

IL_0056:
	{
		return;
	}
}
// System.String UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()
extern "C"  String_t* AudioSettings_GetAmbisonicDecoderPluginName_m854307786 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*AudioSettings_GetAmbisonicDecoderPluginName_m854307786_ftn) ();
	static AudioSettings_GetAmbisonicDecoderPluginName_m854307786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSettings_GetAmbisonicDecoderPluginName_m854307786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
extern "C"  void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2715435404 (AudioConfigurationChangeHandler_t2715435404 * __this, bool ___deviceWasChanged0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___deviceWasChanged0));

}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AudioConfigurationChangeHandler__ctor_m3140887969 (AudioConfigurationChangeHandler_t2715435404 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m2092555472 (AudioConfigurationChangeHandler_t2715435404 * __this, bool ___deviceWasChanged0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m2092555472((AudioConfigurationChangeHandler_t2715435404 *)__this->get_prev_9(),___deviceWasChanged0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___deviceWasChanged0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___deviceWasChanged0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___deviceWasChanged0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___deviceWasChanged0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* AudioConfigurationChangeHandler_BeginInvoke_m138753585 (AudioConfigurationChangeHandler_t2715435404 * __this, bool ___deviceWasChanged0, AsyncCallback_t3561663063 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioConfigurationChangeHandler_BeginInvoke_m138753585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3624619635_il2cpp_TypeInfo_var, &___deviceWasChanged0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AudioConfigurationChangeHandler_EndInvoke_m3919759706 (AudioConfigurationChangeHandler_t2715435404 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioSource::.ctor()
extern "C"  void AudioSource__ctor_m3701253265 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	{
		__this->set_spatializerExtension_2((AudioSourceExtension_t1155560466 *)NULL);
		__this->set_ambisonicExtension_3((AudioSourceExtension_t1155560466 *)NULL);
		Behaviour__ctor_m2653597291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.AudioSource::get_volume()
extern "C"  float AudioSource_get_volume_m1190062269 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef float (*AudioSource_get_volume_m1190062269_ftn) (AudioSource_t4025721661 *);
	static AudioSource_get_volume_m1190062269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m1190062269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m1149360195 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_volume_m1149360195_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_volume_m1149360195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m1149360195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C"  void AudioSource_set_pitch_m332815354 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_pitch_m332815354_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_pitch_m332815354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m332815354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AudioSource::get_time()
extern "C"  float AudioSource_get_time_m3284582203 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef float (*AudioSource_get_time_m3284582203_ftn) (AudioSource_t4025721661 *);
	static AudioSource_get_time_m3284582203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_time_m3284582203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_time()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AudioSource::set_time(System.Single)
extern "C"  void AudioSource_set_time_m2186836409 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_time_m2186836409_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_time_m2186836409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_time_m2186836409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t1419814565 * AudioSource_get_clip_m4008781082 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef AudioClip_t1419814565 * (*AudioSource_get_clip_m4008781082_ftn) (AudioSource_t4025721661 *);
	static AudioSource_get_clip_m4008781082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m4008781082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	AudioClip_t1419814565 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m3908622112 (AudioSource_t4025721661 * __this, AudioClip_t1419814565 * ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_clip_m3908622112_ftn) (AudioSource_t4025721661 *, AudioClip_t1419814565 *);
	static AudioSource_set_clip_m3908622112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m3908622112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m992094170 (AudioSource_t4025721661 * __this, uint64_t ___delay0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_Play_m992094170_ftn) (AudioSource_t4025721661 *, uint64_t);
	static AudioSource_Play_m992094170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m992094170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay0);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m1119848675 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m992094170(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m2757663760 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef void (*AudioSource_Stop_m2757663760_ftn) (AudioSource_t4025721661 *);
	static AudioSource_Stop_m2757663760_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Stop_m2757663760_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Stop()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m1553006287 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef bool (*AudioSource_get_isPlaying_m1553006287_ftn) (AudioSource_t4025721661 *);
	static AudioSource_get_isPlaying_m1553006287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_isPlaying_m1553006287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m1662760097 (AudioSource_t4025721661 * __this, AudioClip_t1419814565 * ___clip0, float ___volumeScale1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_PlayOneShot_m1662760097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	{
		AudioClip_t1419814565 * L_0 = ___clip0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		AudioClip_t1419814565 * L_2 = ___clip0;
		NullCheck(L_2);
		bool L_3 = AudioClip_get_ambisonic_m4289332812(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioSourceExtension_t1155560466 * L_4 = AudioExtensionManager_AddAmbisonicDecoderExtension_m1968281134(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_4;
		AudioSourceExtension_t1155560466 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_5, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		AudioSourceExtension_t1155560466 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AudioExtensionManager_t3958046208_il2cpp_TypeInfo_var);
		AudioExtensionManager_GetReadyToPlay_m3146133677(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
	}

IL_0033:
	{
		AudioClip_t1419814565 * L_8 = ___clip0;
		float L_9 = ___volumeScale1;
		AudioSource_PlayOneShotHelper_m1922705126(__this, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShotHelper_m1922705126 (AudioSource_t4025721661 * __this, AudioClip_t1419814565 * ___clip0, float ___volumeScale1, const RuntimeMethod* method)
{
	typedef void (*AudioSource_PlayOneShotHelper_m1922705126_ftn) (AudioSource_t4025721661 *, AudioClip_t1419814565 *, float);
	static AudioSource_PlayOneShotHelper_m1922705126_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShotHelper_m1922705126_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip0, ___volumeScale1);
}
// System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern "C"  void AudioSource_set_spatialBlend_m987206137 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_spatialBlend_m987206137_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_spatialBlend_m987206137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_spatialBlend_m987206137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_spatialBlend(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AudioSource::get_spatializeInternal()
extern "C"  bool AudioSource_get_spatializeInternal_m3549592781 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef bool (*AudioSource_get_spatializeInternal_m3549592781_ftn) (AudioSource_t4025721661 *);
	static AudioSource_get_spatializeInternal_m3549592781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_spatializeInternal_m3549592781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_spatializeInternal()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.AudioSource::get_spatialize()
extern "C"  bool AudioSource_get_spatialize_m2425555736 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = AudioSource_get_spatializeInternal_m3549592781(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AudioSource::set_dopplerLevel(System.Single)
extern "C"  void AudioSource_set_dopplerLevel_m2483631094 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_dopplerLevel_m2483631094_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_dopplerLevel_m2483631094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_dopplerLevel_m2483631094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_dopplerLevel(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
extern "C"  void AudioSource_set_minDistance_m978296874 (AudioSource_t4025721661 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AudioSource_set_minDistance_m978296874_ftn) (AudioSource_t4025721661 *, float);
	static AudioSource_set_minDistance_m978296874_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_minDistance_m978296874_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_minDistance(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.AudioSource::GetNumExtensionProperties()
extern "C"  int32_t AudioSource_GetNumExtensionProperties_m206409171 (AudioSource_t4025721661 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AudioSource_GetNumExtensionProperties_m206409171_ftn) (AudioSource_t4025721661 *);
	static AudioSource_GetNumExtensionProperties_m206409171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_GetNumExtensionProperties_m206409171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::GetNumExtensionProperties()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.PropertyName UnityEngine.AudioSource::ReadExtensionName(System.Int32)
extern "C"  PropertyName_t65259757  AudioSource_ReadExtensionName_m3482741001 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method)
{
	PropertyName_t65259757  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___sourceIndex0;
		AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t65259757  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		PropertyName_t65259757  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, int32_t ___sourceIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106_ftn) (AudioSource_t4025721661 *, int32_t, PropertyName_t65259757 *);
	static AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_ReadExtensionName_m3752220106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___sourceIndex1, ___value2);
}
// UnityEngine.PropertyName UnityEngine.AudioSource::ReadExtensionPropertyName(System.Int32)
extern "C"  PropertyName_t65259757  AudioSource_ReadExtensionPropertyName_m3408990507 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method)
{
	PropertyName_t65259757  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyName_t65259757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___sourceIndex0;
		AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t65259757  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		PropertyName_t65259757  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, int32_t ___sourceIndex1, PropertyName_t65259757 * ___value2, const RuntimeMethod* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545_ftn) (AudioSource_t4025721661 *, int32_t, PropertyName_t65259757 *);
	static AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m211510545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___sourceIndex1, ___value2);
}
// System.Single UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)
extern "C"  float AudioSource_ReadExtensionPropertyValue_m3076907217 (AudioSource_t4025721661 * __this, int32_t ___sourceIndex0, const RuntimeMethod* method)
{
	typedef float (*AudioSource_ReadExtensionPropertyValue_m3076907217_ftn) (AudioSource_t4025721661 *, int32_t);
	static AudioSource_ReadExtensionPropertyValue_m3076907217_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_ReadExtensionPropertyValue_m3076907217_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)");
	float retVal = _il2cpp_icall_func(__this, ___sourceIndex0);
	return retVal;
}
// System.Void UnityEngine.AudioSource::ClearExtensionProperties(UnityEngine.PropertyName)
extern "C"  void AudioSource_ClearExtensionProperties_m310946178 (AudioSource_t4025721661 * __this, PropertyName_t65259757  ___extensionName0, const RuntimeMethod* method)
{
	{
		AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682(NULL /*static, unused*/, __this, (&___extensionName0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)
extern "C"  void AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682 (RuntimeObject * __this /* static, unused */, AudioSource_t4025721661 * ___self0, PropertyName_t65259757 * ___extensionName1, const RuntimeMethod* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682_ftn) (AudioSource_t4025721661 *, PropertyName_t65259757 *);
	static AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_ClearExtensionProperties_m2465084682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___self0, ___extensionName1);
}
// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::AddSpatializerExtension(System.Type)
extern "C"  AudioSourceExtension_t1155560466 * AudioSource_AddSpatializerExtension_m699023343 (AudioSource_t4025721661 * __this, Type_t * ___extensionType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_AddSpatializerExtension_m699023343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	{
		AudioSourceExtension_t1155560466 * L_0 = __this->get_spatializerExtension_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Type_t * L_2 = ___extensionType0;
		ScriptableObject_t1804531341 * L_3 = ScriptableObject_CreateInstance_m1486025286(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_spatializerExtension_2(((AudioSourceExtension_t1155560466 *)IsInstClass((RuntimeObject*)L_3, AudioSourceExtension_t1155560466_il2cpp_TypeInfo_var)));
	}

IL_0025:
	{
		AudioSourceExtension_t1155560466 * L_4 = __this->get_spatializerExtension_2();
		V_0 = L_4;
		goto IL_0031;
	}

IL_0031:
	{
		AudioSourceExtension_t1155560466 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::AddAmbisonicExtension(System.Type)
extern "C"  AudioSourceExtension_t1155560466 * AudioSource_AddAmbisonicExtension_m1752284328 (AudioSource_t4025721661 * __this, Type_t * ___extensionType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_AddAmbisonicExtension_m1752284328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSourceExtension_t1155560466 * V_0 = NULL;
	{
		AudioSourceExtension_t1155560466 * L_0 = __this->get_ambisonicExtension_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Type_t * L_2 = ___extensionType0;
		ScriptableObject_t1804531341 * L_3 = ScriptableObject_CreateInstance_m1486025286(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_ambisonicExtension_3(((AudioSourceExtension_t1155560466 *)IsInstClass((RuntimeObject*)L_3, AudioSourceExtension_t1155560466_il2cpp_TypeInfo_var)));
	}

IL_0025:
	{
		AudioSourceExtension_t1155560466 * L_4 = __this->get_ambisonicExtension_3();
		V_0 = L_4;
		goto IL_0031;
	}

IL_0031:
	{
		AudioSourceExtension_t1155560466 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.AudioSource UnityEngine.AudioSourceExtension::get_audioSource()
extern "C"  AudioSource_t4025721661 * AudioSourceExtension_get_audioSource_m500534479 (AudioSourceExtension_t1155560466 * __this, const RuntimeMethod* method)
{
	AudioSource_t4025721661 * V_0 = NULL;
	{
		AudioSource_t4025721661 * L_0 = __this->get_m_audioSource_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		AudioSource_t4025721661 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AudioSourceExtension::set_audioSource(UnityEngine.AudioSource)
extern "C"  void AudioSourceExtension_set_audioSource_m2096082894 (AudioSourceExtension_t1155560466 * __this, AudioSource_t4025721661 * ___value0, const RuntimeMethod* method)
{
	{
		AudioSource_t4025721661 * L_0 = ___value0;
		__this->set_m_audioSource_2(L_0);
		return;
	}
}
// System.Void UnityEngine.AudioSourceExtension::WriteExtensionProperty(UnityEngine.PropertyName,System.Single)
extern "C"  void AudioSourceExtension_WriteExtensionProperty_m4100815384 (AudioSourceExtension_t1155560466 * __this, PropertyName_t65259757  ___propertyName0, float ___propertyValue1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AudioSourceExtension::Play()
extern "C"  void AudioSourceExtension_Play_m124598424 (AudioSourceExtension_t1155560466 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AudioSourceExtension::Stop()
extern "C"  void AudioSourceExtension_Stop_m578155681 (AudioSourceExtension_t1155560466 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AudioSourceExtension::ExtensionUpdate()
extern "C"  void AudioSourceExtension_ExtensionUpdate_m2781575154 (AudioSourceExtension_t1155560466 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AudioSpatializerExtensionDefinition::.ctor(System.String,UnityEngine.AudioExtensionDefinition,UnityEngine.AudioExtensionDefinition)
extern "C"  void AudioSpatializerExtensionDefinition__ctor_m507988252 (AudioSpatializerExtensionDefinition_t2045914242 * __this, String_t* ___spatializerNameIn0, AudioExtensionDefinition_t2916689971 * ___definitionIn1, AudioExtensionDefinition_t2916689971 * ___editorDefinitionIn2, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___spatializerNameIn0;
		PropertyName_t65259757  L_1 = PropertyName_op_Implicit_m1456940466(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_spatializerName_0(L_1);
		AudioExtensionDefinition_t2916689971 * L_2 = ___definitionIn1;
		__this->set_definition_1(L_2);
		AudioExtensionDefinition_t2916689971 * L_3 = ___editorDefinitionIn2;
		__this->set_editorDefinition_2(L_3);
		return;
	}
}
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_InternalCreateAudioOutput_m808215400 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		String_t* L_1 = ___name1;
		PlayableOutputHandle_t506089257 * L_2 = ___handle2;
		bool L_3 = AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409_ftn) (PlayableGraph_t1490949931 *, String_t*, PlayableOutputHandle_t506089257 *);
	static AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m2809267409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___name1, ___handle2);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
