﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Animation
struct Animation_t3821138400;
// UnityEngine.AnimationClip
struct AnimationClip_t1145879031;
// System.String
struct String_t;
// UnityEngine.AnimationState
struct AnimationState_t1353060852;
// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// UnityEngine.Animation/Enumerator
struct Enumerator_t3167017746;
// UnityEngine.Motion
struct Motion_t712717323;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.AnimationEvent
struct AnimationEvent_t3145551608;
// System.InvalidCastException
struct InvalidCastException_t3988541634;
// UnityEngine.AvatarMask
struct AvatarMask_t2076757458;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t2335218917;
// System.ArgumentNullException
struct ArgumentNullException_t3857807348;
// UnityEngine.Animator
struct Animator_t2768715325;
// System.InvalidOperationException
struct InvalidOperationException_t3358289486;
// UnityEngine.TrackedReference
struct TrackedReference_t2624196464;
// UnityEngine.Behaviour
struct Behaviour_t2850977393;
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t2373780016;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Void
struct Void_t653366341;

extern RuntimeClass* Enumerator_t3167017746_il2cpp_TypeInfo_var;
extern const uint32_t Animation_GetEnumerator_m1960677199_MetadataUsageId;
struct Object_t692178351_marshaled_pinvoke;
struct Object_t692178351;;
struct Object_t692178351_marshaled_pinvoke;;
struct Object_t692178351_marshaled_com;
struct Object_t692178351_marshaled_com;;
extern Il2CppCodeGenString* _stringLiteral4185710657;
extern const uint32_t AnimationEvent__ctor_m430745688_MetadataUsageId;
extern RuntimeClass* InvalidCastException_t3988541634_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3378384216;
extern const uint32_t AnimationClipPlayable__ctor_m3931264303_MetadataUsageId;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral4113542315;
extern const uint32_t AnimationLayerMixerPlayable__ctor_m3403316352_MetadataUsageId;
extern RuntimeClass* AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var;
extern const uint32_t AnimationLayerMixerPlayable_CreateHandleInternal_m2718066848_MetadataUsageId;
extern RuntimeClass* UInt32_t1721902794_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t972567508_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t3857807348_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2414497163;
extern Il2CppCodeGenString* _stringLiteral2578260669;
extern Il2CppCodeGenString* _stringLiteral2105139031;
extern const uint32_t AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m1500292959_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_get_Null_m3868116653_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_Create_m3450340570_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_CreateHandle_m3929655259_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable__cctor_m3977391353_MetadataUsageId;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral562150037;
extern const uint32_t AnimationMixerPlayable__ctor_m354515751_MetadataUsageId;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1312382514;
extern const uint32_t AnimationOffsetPlayable__ctor_m3498961827_MetadataUsageId;
extern RuntimeClass* AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var;
extern const uint32_t AnimationOffsetPlayable_CreateHandleInternal_m1530857440_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetPosition_m1979386976_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetRotation_m1783363935_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetPositionInternal_m1179161308_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetRotationInternal_m1466851248_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_Create_m1320251422_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_CreateHandle_m1672393200_MetadataUsageId;
extern RuntimeClass* PlayableHandle_t743382320_il2cpp_TypeInfo_var;
extern const uint32_t AnimationOffsetPlayable_Equals_m2331729891_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable__cctor_m1029727379_MetadataUsageId;
extern const RuntimeMethod* PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral223177411;
extern const uint32_t AnimationPlayableOutput__ctor_m2942214619_MetadataUsageId;
extern RuntimeClass* InvalidOperationException_t3358289486_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3254879098;
extern Il2CppCodeGenString* _stringLiteral1638578942;
extern const uint32_t AnimatorControllerPlayable_SetHandle_m4095728861_MetadataUsageId;
extern RuntimeClass* AnimatorControllerPlayable_t4063768904_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorControllerPlayable__cctor_m3436190657_MetadataUsageId;
extern const uint32_t Motion__ctor_m3009723041_MetadataUsageId;

struct AnimatorClipInfoU5BU5D_t2373780016;


#ifndef U3CMODULEU3E_T1429447272_H
#define U3CMODULEU3E_T1429447272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447272 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447272_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3769671934_H
#define ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3769671934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableGraphExtensions
struct  AnimationPlayableGraphExtensions_t3769671934  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3769671934_H
#ifndef ANIMATIONPLAYABLEEXTENSIONS_T2376167741_H
#define ANIMATIONPLAYABLEEXTENSIONS_T2376167741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableExtensions
struct  AnimationPlayableExtensions_t2376167741  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEEXTENSIONS_T2376167741_H
#ifndef ENUMERATOR_T3167017746_H
#define ENUMERATOR_T3167017746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation/Enumerator
struct  Enumerator_t3167017746  : public RuntimeObject
{
public:
	// UnityEngine.Animation UnityEngine.Animation/Enumerator::m_Outer
	Animation_t3821138400 * ___m_Outer_0;
	// System.Int32 UnityEngine.Animation/Enumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_1;

public:
	inline static int32_t get_offset_of_m_Outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t3167017746, ___m_Outer_0)); }
	inline Animation_t3821138400 * get_m_Outer_0() const { return ___m_Outer_0; }
	inline Animation_t3821138400 ** get_address_of_m_Outer_0() { return &___m_Outer_0; }
	inline void set_m_Outer_0(Animation_t3821138400 * value)
	{
		___m_Outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Outer_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t3167017746, ___m_CurrentIndex_1)); }
	inline int32_t get_m_CurrentIndex_1() const { return ___m_CurrentIndex_1; }
	inline int32_t* get_address_of_m_CurrentIndex_1() { return &___m_CurrentIndex_1; }
	inline void set_m_CurrentIndex_1(int32_t value)
	{
		___m_CurrentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3167017746_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef SHAREDBETWEENANIMATORSATTRIBUTE_T4139382706_H
#define SHAREDBETWEENANIMATORSATTRIBUTE_T4139382706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SharedBetweenAnimatorsAttribute
struct  SharedBetweenAnimatorsAttribute_t4139382706  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDBETWEENANIMATORSATTRIBUTE_T4139382706_H
#ifndef ANIMATORTRANSITIONINFO_T118268726_H
#define ANIMATORTRANSITIONINFO_T118268726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorTransitionInfo
struct  AnimatorTransitionInfo_t118268726 
{
public:
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_4;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_5;

public:
	inline static int32_t get_offset_of_m_FullPath_0() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_FullPath_0)); }
	inline int32_t get_m_FullPath_0() const { return ___m_FullPath_0; }
	inline int32_t* get_address_of_m_FullPath_0() { return &___m_FullPath_0; }
	inline void set_m_FullPath_0(int32_t value)
	{
		___m_FullPath_0 = value;
	}

	inline static int32_t get_offset_of_m_UserName_1() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_UserName_1)); }
	inline int32_t get_m_UserName_1() const { return ___m_UserName_1; }
	inline int32_t* get_address_of_m_UserName_1() { return &___m_UserName_1; }
	inline void set_m_UserName_1(int32_t value)
	{
		___m_UserName_1 = value;
	}

	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_Name_2)); }
	inline int32_t get_m_Name_2() const { return ___m_Name_2; }
	inline int32_t* get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(int32_t value)
	{
		___m_Name_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_AnyState_4() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_AnyState_4)); }
	inline bool get_m_AnyState_4() const { return ___m_AnyState_4; }
	inline bool* get_address_of_m_AnyState_4() { return &___m_AnyState_4; }
	inline void set_m_AnyState_4(bool value)
	{
		___m_AnyState_4 = value;
	}

	inline static int32_t get_offset_of_m_TransitionType_5() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t118268726, ___m_TransitionType_5)); }
	inline int32_t get_m_TransitionType_5() const { return ___m_TransitionType_5; }
	inline int32_t* get_address_of_m_TransitionType_5() { return &___m_TransitionType_5; }
	inline void set_m_TransitionType_5(int32_t value)
	{
		___m_TransitionType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t118268726_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	float ___m_NormalizedTime_3;
	int32_t ___m_AnyState_4;
	int32_t ___m_TransitionType_5;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t118268726_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	float ___m_NormalizedTime_3;
	int32_t ___m_AnyState_4;
	int32_t ___m_TransitionType_5;
};
#endif // ANIMATORTRANSITIONINFO_T118268726_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef UINT32_T1721902794_H
#define UINT32_T1721902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1721902794 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1721902794, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1721902794_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef ANIMATORCLIPINFO_T2792414269_H
#define ANIMATORCLIPINFO_T2792414269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorClipInfo
struct  AnimatorClipInfo_t2792414269 
{
public:
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_ClipInstanceID_0() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t2792414269, ___m_ClipInstanceID_0)); }
	inline int32_t get_m_ClipInstanceID_0() const { return ___m_ClipInstanceID_0; }
	inline int32_t* get_address_of_m_ClipInstanceID_0() { return &___m_ClipInstanceID_0; }
	inline void set_m_ClipInstanceID_0(int32_t value)
	{
		___m_ClipInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t2792414269, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCLIPINFO_T2792414269_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef ANIMATORSTATEINFO_T4013846803_H
#define ANIMATORSTATEINFO_T4013846803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t4013846803 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4013846803, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T4013846803_H
#ifndef PLAYABLEHANDLE_T743382320_H
#define PLAYABLEHANDLE_T743382320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t743382320 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t743382320, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T743382320_H
#ifndef HUMANLIMIT_T69269969_H
#define HUMANLIMIT_T69269969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanLimit
struct  HumanLimit_t69269969 
{
public:
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t1986933152  ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t1986933152  ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t1986933152  ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;

public:
	inline static int32_t get_offset_of_m_Min_0() { return static_cast<int32_t>(offsetof(HumanLimit_t69269969, ___m_Min_0)); }
	inline Vector3_t1986933152  get_m_Min_0() const { return ___m_Min_0; }
	inline Vector3_t1986933152 * get_address_of_m_Min_0() { return &___m_Min_0; }
	inline void set_m_Min_0(Vector3_t1986933152  value)
	{
		___m_Min_0 = value;
	}

	inline static int32_t get_offset_of_m_Max_1() { return static_cast<int32_t>(offsetof(HumanLimit_t69269969, ___m_Max_1)); }
	inline Vector3_t1986933152  get_m_Max_1() const { return ___m_Max_1; }
	inline Vector3_t1986933152 * get_address_of_m_Max_1() { return &___m_Max_1; }
	inline void set_m_Max_1(Vector3_t1986933152  value)
	{
		___m_Max_1 = value;
	}

	inline static int32_t get_offset_of_m_Center_2() { return static_cast<int32_t>(offsetof(HumanLimit_t69269969, ___m_Center_2)); }
	inline Vector3_t1986933152  get_m_Center_2() const { return ___m_Center_2; }
	inline Vector3_t1986933152 * get_address_of_m_Center_2() { return &___m_Center_2; }
	inline void set_m_Center_2(Vector3_t1986933152  value)
	{
		___m_Center_2 = value;
	}

	inline static int32_t get_offset_of_m_AxisLength_3() { return static_cast<int32_t>(offsetof(HumanLimit_t69269969, ___m_AxisLength_3)); }
	inline float get_m_AxisLength_3() const { return ___m_AxisLength_3; }
	inline float* get_address_of_m_AxisLength_3() { return &___m_AxisLength_3; }
	inline void set_m_AxisLength_3(float value)
	{
		___m_AxisLength_3 = value;
	}

	inline static int32_t get_offset_of_m_UseDefaultValues_4() { return static_cast<int32_t>(offsetof(HumanLimit_t69269969, ___m_UseDefaultValues_4)); }
	inline int32_t get_m_UseDefaultValues_4() const { return ___m_UseDefaultValues_4; }
	inline int32_t* get_address_of_m_UseDefaultValues_4() { return &___m_UseDefaultValues_4; }
	inline void set_m_UseDefaultValues_4(int32_t value)
	{
		___m_UseDefaultValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUMANLIMIT_T69269969_H
#ifndef PLAYABLEOUTPUTHANDLE_T506089257_H
#define PLAYABLEOUTPUTHANDLE_T506089257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t506089257 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t506089257, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T506089257_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef ANIMATIONEVENTSOURCE_T1980499909_H
#define ANIMATIONEVENTSOURCE_T1980499909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationEventSource
struct  AnimationEventSource_t1980499909 
{
public:
	// System.Int32 UnityEngine.AnimationEventSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationEventSource_t1980499909, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONEVENTSOURCE_T1980499909_H
#ifndef PLAYMODE_T3053504117_H
#define PLAYMODE_T3053504117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayMode
struct  PlayMode_t3053504117 
{
public:
	// System.Int32 UnityEngine.PlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayMode_t3053504117, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_T3053504117_H
#ifndef TRACKEDREFERENCE_T2624196464_H
#define TRACKEDREFERENCE_T2624196464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t2624196464  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t2624196464, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2624196464_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2624196464_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T2624196464_H
#ifndef SKELETONBONE_T1743783883_H
#define SKELETONBONE_T1743783883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SkeletonBone
struct  SkeletonBone_t1743783883 
{
public:
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t1986933152  ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_t704191599  ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t1986933152  ___scale_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonBone_t1743783883, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_parentName_1() { return static_cast<int32_t>(offsetof(SkeletonBone_t1743783883, ___parentName_1)); }
	inline String_t* get_parentName_1() const { return ___parentName_1; }
	inline String_t** get_address_of_parentName_1() { return &___parentName_1; }
	inline void set_parentName_1(String_t* value)
	{
		___parentName_1 = value;
		Il2CppCodeGenWriteBarrier((&___parentName_1), value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(SkeletonBone_t1743783883, ___position_2)); }
	inline Vector3_t1986933152  get_position_2() const { return ___position_2; }
	inline Vector3_t1986933152 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_t1986933152  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(SkeletonBone_t1743783883, ___rotation_3)); }
	inline Quaternion_t704191599  get_rotation_3() const { return ___rotation_3; }
	inline Quaternion_t704191599 * get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(Quaternion_t704191599  value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(SkeletonBone_t1743783883, ___scale_4)); }
	inline Vector3_t1986933152  get_scale_4() const { return ___scale_4; }
	inline Vector3_t1986933152 * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector3_t1986933152  value)
	{
		___scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t1743783883_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t1986933152  ___position_2;
	Quaternion_t704191599  ___rotation_3;
	Vector3_t1986933152  ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t1743783883_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t1986933152  ___position_2;
	Quaternion_t704191599  ___rotation_3;
	Vector3_t1986933152  ___scale_4;
};
#endif // SKELETONBONE_T1743783883_H
#ifndef ARGUMENTEXCEPTION_T1812645948_H
#define ARGUMENTEXCEPTION_T1812645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1812645948  : public SystemException_t2062748594
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1812645948, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1812645948_H
#ifndef PLAYABLEGRAPH_T1490949931_H
#define PLAYABLEGRAPH_T1490949931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1490949931 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1490949931_H
#ifndef INVALIDCASTEXCEPTION_T3988541634_H
#define INVALIDCASTEXCEPTION_T3988541634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t3988541634  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T3988541634_H
#ifndef INVALIDOPERATIONEXCEPTION_T3358289486_H
#define INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t3358289486  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifndef ANIMATIONSTATE_T1353060852_H
#define ANIMATIONSTATE_T1353060852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationState
struct  AnimationState_t1353060852  : public TrackedReference_t2624196464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T1353060852_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef HUMANBONE_T634326648_H
#define HUMANBONE_T634326648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanBone
struct  HumanBone_t634326648 
{
public:
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_t69269969  ___limit_2;

public:
	inline static int32_t get_offset_of_m_BoneName_0() { return static_cast<int32_t>(offsetof(HumanBone_t634326648, ___m_BoneName_0)); }
	inline String_t* get_m_BoneName_0() const { return ___m_BoneName_0; }
	inline String_t** get_address_of_m_BoneName_0() { return &___m_BoneName_0; }
	inline void set_m_BoneName_0(String_t* value)
	{
		___m_BoneName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoneName_0), value);
	}

	inline static int32_t get_offset_of_m_HumanName_1() { return static_cast<int32_t>(offsetof(HumanBone_t634326648, ___m_HumanName_1)); }
	inline String_t* get_m_HumanName_1() const { return ___m_HumanName_1; }
	inline String_t** get_address_of_m_HumanName_1() { return &___m_HumanName_1; }
	inline void set_m_HumanName_1(String_t* value)
	{
		___m_HumanName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_HumanName_1), value);
	}

	inline static int32_t get_offset_of_limit_2() { return static_cast<int32_t>(offsetof(HumanBone_t634326648, ___limit_2)); }
	inline HumanLimit_t69269969  get_limit_2() const { return ___limit_2; }
	inline HumanLimit_t69269969 * get_address_of_limit_2() { return &___limit_2; }
	inline void set_limit_2(HumanLimit_t69269969  value)
	{
		___limit_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t634326648_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_t69269969  ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t634326648_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_t69269969  ___limit_2;
};
#endif // HUMANBONE_T634326648_H
#ifndef ANIMATIONEVENT_T3145551608_H
#define ANIMATIONEVENT_T3145551608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationEvent
struct  AnimationEvent_t3145551608  : public RuntimeObject
{
public:
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_t692178351 * ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_t1353060852 * ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t4013846803  ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t2792414269  ___m_AnimatorClipInfo_10;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_FunctionName_1() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_FunctionName_1)); }
	inline String_t* get_m_FunctionName_1() const { return ___m_FunctionName_1; }
	inline String_t** get_address_of_m_FunctionName_1() { return &___m_FunctionName_1; }
	inline void set_m_FunctionName_1(String_t* value)
	{
		___m_FunctionName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FunctionName_1), value);
	}

	inline static int32_t get_offset_of_m_StringParameter_2() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_StringParameter_2)); }
	inline String_t* get_m_StringParameter_2() const { return ___m_StringParameter_2; }
	inline String_t** get_address_of_m_StringParameter_2() { return &___m_StringParameter_2; }
	inline void set_m_StringParameter_2(String_t* value)
	{
		___m_StringParameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringParameter_2), value);
	}

	inline static int32_t get_offset_of_m_ObjectReferenceParameter_3() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_ObjectReferenceParameter_3)); }
	inline Object_t692178351 * get_m_ObjectReferenceParameter_3() const { return ___m_ObjectReferenceParameter_3; }
	inline Object_t692178351 ** get_address_of_m_ObjectReferenceParameter_3() { return &___m_ObjectReferenceParameter_3; }
	inline void set_m_ObjectReferenceParameter_3(Object_t692178351 * value)
	{
		___m_ObjectReferenceParameter_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectReferenceParameter_3), value);
	}

	inline static int32_t get_offset_of_m_FloatParameter_4() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_FloatParameter_4)); }
	inline float get_m_FloatParameter_4() const { return ___m_FloatParameter_4; }
	inline float* get_address_of_m_FloatParameter_4() { return &___m_FloatParameter_4; }
	inline void set_m_FloatParameter_4(float value)
	{
		___m_FloatParameter_4 = value;
	}

	inline static int32_t get_offset_of_m_IntParameter_5() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_IntParameter_5)); }
	inline int32_t get_m_IntParameter_5() const { return ___m_IntParameter_5; }
	inline int32_t* get_address_of_m_IntParameter_5() { return &___m_IntParameter_5; }
	inline void set_m_IntParameter_5(int32_t value)
	{
		___m_IntParameter_5 = value;
	}

	inline static int32_t get_offset_of_m_MessageOptions_6() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_MessageOptions_6)); }
	inline int32_t get_m_MessageOptions_6() const { return ___m_MessageOptions_6; }
	inline int32_t* get_address_of_m_MessageOptions_6() { return &___m_MessageOptions_6; }
	inline void set_m_MessageOptions_6(int32_t value)
	{
		___m_MessageOptions_6 = value;
	}

	inline static int32_t get_offset_of_m_Source_7() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_Source_7)); }
	inline int32_t get_m_Source_7() const { return ___m_Source_7; }
	inline int32_t* get_address_of_m_Source_7() { return &___m_Source_7; }
	inline void set_m_Source_7(int32_t value)
	{
		___m_Source_7 = value;
	}

	inline static int32_t get_offset_of_m_StateSender_8() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_StateSender_8)); }
	inline AnimationState_t1353060852 * get_m_StateSender_8() const { return ___m_StateSender_8; }
	inline AnimationState_t1353060852 ** get_address_of_m_StateSender_8() { return &___m_StateSender_8; }
	inline void set_m_StateSender_8(AnimationState_t1353060852 * value)
	{
		___m_StateSender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_StateSender_8), value);
	}

	inline static int32_t get_offset_of_m_AnimatorStateInfo_9() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_AnimatorStateInfo_9)); }
	inline AnimatorStateInfo_t4013846803  get_m_AnimatorStateInfo_9() const { return ___m_AnimatorStateInfo_9; }
	inline AnimatorStateInfo_t4013846803 * get_address_of_m_AnimatorStateInfo_9() { return &___m_AnimatorStateInfo_9; }
	inline void set_m_AnimatorStateInfo_9(AnimatorStateInfo_t4013846803  value)
	{
		___m_AnimatorStateInfo_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimatorClipInfo_10() { return static_cast<int32_t>(offsetof(AnimationEvent_t3145551608, ___m_AnimatorClipInfo_10)); }
	inline AnimatorClipInfo_t2792414269  get_m_AnimatorClipInfo_10() const { return ___m_AnimatorClipInfo_10; }
	inline AnimatorClipInfo_t2792414269 * get_address_of_m_AnimatorClipInfo_10() { return &___m_AnimatorClipInfo_10; }
	inline void set_m_AnimatorClipInfo_10(AnimatorClipInfo_t2792414269  value)
	{
		___m_AnimatorClipInfo_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t3145551608_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_t692178351_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_t1353060852 * ___m_StateSender_8;
	AnimatorStateInfo_t4013846803  ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t2792414269  ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t3145551608_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_t692178351_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_t1353060852 * ___m_StateSender_8;
	AnimatorStateInfo_t4013846803  ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t2792414269  ___m_AnimatorClipInfo_10;
};
#endif // ANIMATIONEVENT_T3145551608_H
#ifndef MOTION_T712717323_H
#define MOTION_T712717323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Motion
struct  Motion_t712717323  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTION_T712717323_H
#ifndef ANIMATORCONTROLLERPLAYABLE_T4063768904_H
#define ANIMATORCONTROLLERPLAYABLE_T4063768904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimatorControllerPlayable
struct  AnimatorControllerPlayable_t4063768904 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t4063768904, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimatorControllerPlayable_t4063768904_StaticFields
{
public:
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_t4063768904  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t4063768904_StaticFields, ___m_NullPlayable_1)); }
	inline AnimatorControllerPlayable_t4063768904  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimatorControllerPlayable_t4063768904 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimatorControllerPlayable_t4063768904  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPLAYABLE_T4063768904_H
#ifndef ANIMATIONCLIPPLAYABLE_T2813945836_H
#define ANIMATIONCLIPPLAYABLE_T2813945836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationClipPlayable
struct  AnimationClipPlayable_t2813945836 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationClipPlayable_t2813945836, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPPLAYABLE_T2813945836_H
#ifndef ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#define ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t2708046649 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t506089257  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t2708046649, ___m_Handle_0)); }
	inline PlayableOutputHandle_t506089257  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t506089257 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t506089257  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_T2708046649_H
#ifndef ANIMATIONOFFSETPLAYABLE_T786003479_H
#define ANIMATIONOFFSETPLAYABLE_T786003479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationOffsetPlayable
struct  AnimationOffsetPlayable_t786003479 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t786003479, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationOffsetPlayable_t786003479_StaticFields
{
public:
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t786003479  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t786003479_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationOffsetPlayable_t786003479  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationOffsetPlayable_t786003479 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationOffsetPlayable_t786003479  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOFFSETPLAYABLE_T786003479_H
#ifndef ANIMATIONMIXERPLAYABLE_T2307681866_H
#define ANIMATIONMIXERPLAYABLE_T2307681866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationMixerPlayable
struct  AnimationMixerPlayable_t2307681866 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_t2307681866, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMIXERPLAYABLE_T2307681866_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t2335218917  : public ArgumentException_t1812645948
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t2335218917, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T2335218917_H
#ifndef AVATARMASK_T2076757458_H
#define AVATARMASK_T2076757458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarMask
struct  AvatarMask_t2076757458  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARMASK_T2076757458_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#define ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t4073556314 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t4073556314, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t4073556314_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t4073556314  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t4073556314_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t4073556314  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t4073556314 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t4073556314  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T4073556314_H
#ifndef PLAYABLE_T3296292090_H
#define PLAYABLE_T3296292090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t3296292090 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t743382320  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t3296292090, ___m_Handle_0)); }
	inline PlayableHandle_t743382320  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t743382320 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t743382320  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t3296292090_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t3296292090  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t3296292090_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t3296292090  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t3296292090 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t3296292090  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T3296292090_H
#ifndef ARGUMENTNULLEXCEPTION_T3857807348_H
#define ARGUMENTNULLEXCEPTION_T3857807348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t3857807348  : public ArgumentException_t1812645948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T3857807348_H
#ifndef STATEMACHINEBEHAVIOUR_T1679037885_H
#define STATEMACHINEBEHAVIOUR_T1679037885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t1679037885  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T1679037885_H
#ifndef ANIMATIONCLIP_T1145879031_H
#define ANIMATIONCLIP_T1145879031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationClip
struct  AnimationClip_t1145879031  : public Motion_t712717323
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIP_T1145879031_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef ANIMATOR_T2768715325_H
#define ANIMATOR_T2768715325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t2768715325  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T2768715325_H
#ifndef ANIMATION_T3821138400_H
#define ANIMATION_T3821138400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t3821138400  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T3821138400_H
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t2373780016  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t2792414269  m_Items[1];

public:
	inline AnimatorClipInfo_t2792414269  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimatorClipInfo_t2792414269 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t2792414269  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline AnimatorClipInfo_t2792414269  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimatorClipInfo_t2792414269 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimatorClipInfo_t2792414269  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void Object_t692178351_marshal_pinvoke(const Object_t692178351& unmarshaled, Object_t692178351_marshaled_pinvoke& marshaled);
extern "C" void Object_t692178351_marshal_pinvoke_back(const Object_t692178351_marshaled_pinvoke& marshaled, Object_t692178351& unmarshaled);
extern "C" void Object_t692178351_marshal_pinvoke_cleanup(Object_t692178351_marshaled_pinvoke& marshaled);
extern "C" void Object_t692178351_marshal_com(const Object_t692178351& unmarshaled, Object_t692178351_marshaled_com& marshaled);
extern "C" void Object_t692178351_marshal_com_back(const Object_t692178351_marshaled_com& marshaled, Object_t692178351& unmarshaled);
extern "C" void Object_t692178351_marshal_com_cleanup(Object_t692178351_marshaled_com& marshaled);

// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationClipPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationLayerMixerPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMixerPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationOffsetPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Animations.AnimationPlayableOutput>()
extern "C"  bool PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854_gshared (PlayableOutputHandle_t506089257 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimatorControllerPlayable>()
extern "C"  bool PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385_gshared (PlayableHandle_t743382320 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Stop_m928556781 (RuntimeObject * __this /* static, unused */, Animation_t3821138400 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C"  void Animation_Internal_StopByName_m3428276012 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_Sample(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Sample_m2477203404 (RuntimeObject * __this /* static, unused */, Animation_t3821138400 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C"  AnimationState_t1353060852 * Animation_GetState_m95008089 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m4160369280 (Animation_t3821138400 * __this, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
extern "C"  bool Animation_PlayDefaultAnimation_m2467094813 (Animation_t3821138400 * __this, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m2721141334 (Animation_t3821138400 * __this, String_t* ___animation0, int32_t ___mode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C"  void Animation_CrossFade_m3452504538 (Animation_t3821138400 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C"  void Enumerator__ctor_m3042063048 (Enumerator_t3167017746 * __this, Animation_t3821138400 * ___outer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2968844594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C"  AnimationState_t1353060852 * Animation_GetStateAtIndex_m109571758 (Animation_t3821138400 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C"  int32_t Animation_GetStateCount_m1512371370 (Animation_t3821138400 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Motion::.ctor()
extern "C"  void Motion__ctor_m3009723041 (Motion_t712717323 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m3243057121 (RuntimeObject * __this /* static, unused */, AnimationClip_t1145879031 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2628965434 (PlayableHandle_t743382320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationClipPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306_gshared)(__this, method)
// System.Void System.InvalidCastException::.ctor(System.String)
extern "C"  void InvalidCastException__ctor_m1966396325 (InvalidCastException_t3988541634 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationClipPlayable__ctor_m3931264303 (AnimationClipPlayable_t2813945836 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AnimationClip_t1145879031 * ___clip1, PlayableHandle_t743382320 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_GetAnimationClipInternal_m3492101426 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClip()
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_GetAnimationClip_m612583448 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetApplyFootIKInternal_m1027566677 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIK()
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m4095876580 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffsetInternal_m57204089 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffset()
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m535172577 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffsetInternal_m3545622143 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffset(System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m629646185 (AnimationClipPlayable_t2813945836 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C"  PlayableHandle_t743382320  AnimationClipPlayable_CreateHandle_m4234307555 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AnimationClip_t1145879031 * ___clip1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t743382320  PlayableHandle_get_Null_m2780162545 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_CreateHandleInternal_m4175826051 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AnimationClip_t1145879031 * ___clip1, PlayableHandle_t743382320 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationClipPlayable_GetHandle_m3457548684 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m765599562 (Playable_t3296292090 * __this, PlayableHandle_t743382320  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2481114358 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320  p0, PlayableHandle_t743382320  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::Equals(UnityEngine.Animations.AnimationClipPlayable)
extern "C"  bool AnimationClipPlayable_Equals_m2689488492 (AnimationClipPlayable_t2813945836 * __this, AnimationClipPlayable_t2813945836  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationLayerMixerPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204_gshared)(__this, method)
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationLayerMixerPlayable__ctor_m3403316352 (AnimationLayerMixerPlayable_t4073556314 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCount()
extern "C"  int32_t PlayableHandle_GetInputCount_m3012569931 (PlayableHandle_t743382320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m3571813163 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m2446168321 (ArgumentOutOfRangeException_t2335218917 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1941537752 (ArgumentNullException_t3857807348 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m1500292959 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t2076757458 * ___mask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMask(System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528 (AnimationLayerMixerPlayable_t4073556314 * __this, uint32_t ___layerIndex0, AvatarMask_t2076757458 * ___mask1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t2076757458 * ___mask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C"  PlayableHandle_t743382320  AnimationLayerMixerPlayable_CreateHandle_m3929655259 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationLayerMixerPlayable_CreateHandleInternal_m2718066848 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCount(System.Int32)
extern "C"  void PlayableHandle_SetInputCount_m3197056814 (PlayableHandle_t743382320 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationLayerMixerPlayable_GetHandle_m1521142950 (AnimationLayerMixerPlayable_t4073556314 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t743382320  Playable_GetHandle_m3912652506 (Playable_t3296292090 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::Equals(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C"  bool AnimationLayerMixerPlayable_Equals_m2203675750 (AnimationLayerMixerPlayable_t4073556314 * __this, AnimationLayerMixerPlayable_t4073556314  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMixerPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236_gshared)(__this, method)
// System.Void UnityEngine.Animations.AnimationMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationMixerPlayable__ctor_m354515751 (AnimationMixerPlayable_t2307681866 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  PlayableHandle_t743382320  AnimationMixerPlayable_CreateHandle_m1467265624 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationMixerPlayable_CreateHandleInternal_m3116575894 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationMixerPlayable_GetHandle_m3098381875 (AnimationMixerPlayable_t2307681866 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::Equals(UnityEngine.Animations.AnimationMixerPlayable)
extern "C"  bool AnimationMixerPlayable_Equals_m2111336796 (AnimationMixerPlayable_t2307681866 * __this, AnimationMixerPlayable_t2307681866  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationOffsetPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166_gshared)(__this, method)
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationOffsetPlayable__ctor_m3498961827 (AnimationOffsetPlayable_t786003479 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, Vector3_t1986933152 * ___position1, Quaternion_t704191599 * ___rotation2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  Vector3_t1986933152  AnimationOffsetPlayable_GetPositionInternal_m1179161308 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPosition()
extern "C"  Vector3_t1986933152  AnimationOffsetPlayable_GetPosition_m1979386976 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  Quaternion_t704191599  AnimationOffsetPlayable_GetRotationInternal_m1466851248 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotation()
extern "C"  Quaternion_t704191599  AnimationOffsetPlayable_GetRotation_m1783363935 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetPositionInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)
extern "C"  void AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, Vector3_t1986933152 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetRotationInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)
extern "C"  void AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, Quaternion_t704191599 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  PlayableHandle_t743382320  AnimationOffsetPlayable_CreateHandle_m1672393200 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, Vector3_t1986933152  ___position1, Quaternion_t704191599  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationOffsetPlayable_CreateHandleInternal_m1530857440 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, Vector3_t1986933152  ___position1, Quaternion_t704191599  ___rotation2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationOffsetPlayable_GetHandle_m3588746519 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::Equals(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C"  bool AnimationOffsetPlayable_Equals_m2331729891 (AnimationOffsetPlayable_t786003479 * __this, AnimationOffsetPlayable_t786003479  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationPlayableExtensions::INTERNAL_CALL_SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)
extern "C"  void AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___playable0, AnimationClip_t1145879031 * ___animatedProperties1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_InternalCreateAnimationMotionXToDeltaPlayable_m458782705 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValid()
extern "C"  bool PlayableOutputHandle_IsValid_m124014148 (PlayableOutputHandle_t506089257 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Animations.AnimationPlayableOutput>()
#define PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854(__this, method) ((  bool (*) (PlayableOutputHandle_t506089257 *, const RuntimeMethod*))PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854_gshared)(__this, method)
// System.Void UnityEngine.Animations.AnimationPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void AnimationPlayableOutput__ctor_m2942214619 (AnimationPlayableOutput_t2708046649 * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_InternalGetTarget_m2813996520 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::GetTarget()
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_GetTarget_m2544830582 (AnimationPlayableOutput_t2708046649 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_InternalSetTarget_m3556155859 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, Animator_t2768715325 * ___target1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationPlayableOutput::SetTarget(UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_SetTarget_m1801562013 (AnimationPlayableOutput_t2708046649 * __this, Animator_t2768715325 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, Animator_t2768715325 * ___target1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m3536337750 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::get_Null()
extern "C"  AnimationPlayableOutput_t2708046649  AnimationPlayableOutput_get_Null_m3047134962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t506089257  PlayableOutputHandle_get_Null_m4090468577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t506089257  AnimationPlayableOutput_GetHandle_m1000807631 (AnimationPlayableOutput_t2708046649 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::SetHandle(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimatorControllerPlayable_SetHandle_m4095728861 (AnimatorControllerPlayable_t4063768904 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimatorControllerPlayable__ctor_m39277502 (AnimatorControllerPlayable_t4063768904 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimatorControllerPlayable_GetHandle_m4277404188 (AnimatorControllerPlayable_t4063768904 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2912198563 (InvalidOperationException_t3358289486 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimatorControllerPlayable>()
#define PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385(__this, method) ((  bool (*) (PlayableHandle_t743382320 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385_gshared)(__this, method)
// System.Boolean UnityEngine.Animations.AnimatorControllerPlayable::Equals(UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  bool AnimatorControllerPlayable_Equals_m2390645362 (AnimatorControllerPlayable_t4063768904 * __this, AnimatorControllerPlayable_t4063768904  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TrackedReference::.ctor()
extern "C"  void TrackedReference__ctor_m1051293765 (TrackedReference_t2624196464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2653597291 (Behaviour_t2850977393 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2787488330 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m2785384252 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m1698754883 (Animator_t2768715325 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m2696168653 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m2740437926 (Animator_t2768715325 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::ClipInstanceToScriptingObject(System.Int32)
extern "C"  AnimationClip_t1145879031 * AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::get_clip()
extern "C"  AnimationClip_t1145879031 * AnimatorClipInfo_get_clip_m2688614585 (AnimatorClipInfo_t2792414269 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorClipInfo::get_weight()
extern "C"  float AnimatorClipInfo_get_weight_m1864185950 (AnimatorClipInfo_t2792414269 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m1617659127 (AnimatorStateInfo_t4013846803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m4147771927 (Object_t692178351 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
extern "C"  AnimationClip_t1145879031 * Animation_get_clip_m2526423801 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	typedef AnimationClip_t1145879031 * (*Animation_get_clip_m2526423801_ftn) (Animation_t3821138400 *);
	static Animation_get_clip_m2526423801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_get_clip_m2526423801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::get_clip()");
	AnimationClip_t1145879031 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animation::Stop()
extern "C"  void Animation_Stop_m946069016 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	{
		Animation_INTERNAL_CALL_Stop_m928556781(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Stop_m928556781 (RuntimeObject * __this /* static, unused */, Animation_t3821138400 * ___self0, const RuntimeMethod* method)
{
	typedef void (*Animation_INTERNAL_CALL_Stop_m928556781_ftn) (Animation_t3821138400 *);
	static Animation_INTERNAL_CALL_Stop_m928556781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Stop_m928556781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C"  void Animation_Stop_m645643548 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_StopByName_m3428276012(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C"  void Animation_Internal_StopByName_m3428276012 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animation_Internal_StopByName_m3428276012_ftn) (Animation_t3821138400 *, String_t*);
	static Animation_Internal_StopByName_m3428276012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_StopByName_m3428276012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_StopByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animation::Sample()
extern "C"  void Animation_Sample_m4064291660 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	{
		Animation_INTERNAL_CALL_Sample_m2477203404(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Sample(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Sample_m2477203404 (RuntimeObject * __this /* static, unused */, Animation_t3821138400 * ___self0, const RuntimeMethod* method)
{
	typedef void (*Animation_INTERNAL_CALL_Sample_m2477203404_ftn) (Animation_t3821138400 *);
	static Animation_INTERNAL_CALL_Sample_m2477203404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Sample_m2477203404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Sample(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.Animation::get_isPlaying()
extern "C"  bool Animation_get_isPlaying_m2988970312 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animation_get_isPlaying_m2988970312_ftn) (Animation_t3821138400 *);
	static Animation_get_isPlaying_m2988970312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_get_isPlaying_m2988970312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Animation::IsPlaying(System.String)
extern "C"  bool Animation_IsPlaying_m3752582332 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef bool (*Animation_IsPlaying_m3752582332_ftn) (Animation_t3821138400 *, String_t*);
	static Animation_IsPlaying_m3752582332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_IsPlaying_m3752582332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::IsPlaying(System.String)");
	bool retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t1353060852 * Animation_get_Item_m1994222672 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	AnimationState_t1353060852 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		AnimationState_t1353060852 * L_1 = Animation_GetState_m95008089(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		AnimationState_t1353060852 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation::Play()
extern "C"  bool Animation_Play_m2417797911 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		bool L_1 = Animation_Play_m4160369280(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation::Play(UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m4160369280 (Animation_t3821138400 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___mode0;
		bool L_1 = Animation_PlayDefaultAnimation_m2467094813(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m2721141334 (Animation_t3821138400 * __this, String_t* ___animation0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef bool (*Animation_Play_m2721141334_ftn) (Animation_t3821138400 *, String_t*, int32_t);
	static Animation_Play_m2721141334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Play_m2721141334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)");
	bool retVal = _il2cpp_icall_func(__this, ___animation0, ___mode1);
	return retVal;
}
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m2269094967 (Animation_t3821138400 * __this, String_t* ___animation0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		String_t* L_0 = ___animation0;
		int32_t L_1 = V_0;
		bool L_2 = Animation_Play_m2721141334(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C"  void Animation_CrossFade_m3452504538 (Animation_t3821138400 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Animation_CrossFade_m3452504538_ftn) (Animation_t3821138400 *, String_t*, float, int32_t);
	static Animation_CrossFade_m3452504538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_CrossFade_m3452504538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)");
	_il2cpp_icall_func(__this, ___animation0, ___fadeLength1, ___mode2);
}
// System.Void UnityEngine.Animation::CrossFade(System.String)
extern "C"  void Animation_CrossFade_m1480661329 (Animation_t3821138400 * __this, String_t* ___animation0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.3f);
		String_t* L_0 = ___animation0;
		float L_1 = V_1;
		int32_t L_2 = V_0;
		Animation_CrossFade_m3452504538(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animation::GetClipCount()
extern "C"  int32_t Animation_GetClipCount_m2482106452 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animation_GetClipCount_m2482106452_ftn) (Animation_t3821138400 *);
	static Animation_GetClipCount_m2482106452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetClipCount_m2482106452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetClipCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
extern "C"  bool Animation_PlayDefaultAnimation_m2467094813 (Animation_t3821138400 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef bool (*Animation_PlayDefaultAnimation_m2467094813_ftn) (Animation_t3821138400 *, int32_t);
	static Animation_PlayDefaultAnimation_m2467094813_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_PlayDefaultAnimation_m2467094813_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)");
	bool retVal = _il2cpp_icall_func(__this, ___mode0);
	return retVal;
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern "C"  RuntimeObject* Animation_GetEnumerator_m1960677199 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animation_GetEnumerator_m1960677199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t3167017746 * L_0 = (Enumerator_t3167017746 *)il2cpp_codegen_object_new(Enumerator_t3167017746_il2cpp_TypeInfo_var);
		Enumerator__ctor_m3042063048(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C"  AnimationState_t1353060852 * Animation_GetState_m95008089 (Animation_t3821138400 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef AnimationState_t1353060852 * (*Animation_GetState_m95008089_ftn) (Animation_t3821138400 *, String_t*);
	static Animation_GetState_m95008089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetState_m95008089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetState(System.String)");
	AnimationState_t1353060852 * retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C"  AnimationState_t1353060852 * Animation_GetStateAtIndex_m109571758 (Animation_t3821138400 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef AnimationState_t1353060852 * (*Animation_GetStateAtIndex_m109571758_ftn) (Animation_t3821138400 *, int32_t);
	static Animation_GetStateAtIndex_m109571758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m109571758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	AnimationState_t1353060852 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C"  int32_t Animation_GetStateCount_m1512371370 (Animation_t3821138400 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animation_GetStateCount_m1512371370_ftn) (Animation_t3821138400 *);
	static Animation_GetStateCount_m1512371370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m1512371370_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C"  void Enumerator__ctor_m3042063048 (Enumerator_t3167017746 * __this, Animation_t3821138400 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		Animation_t3821138400 * L_0 = ___outer0;
		__this->set_m_Outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m877660712 (Enumerator_t3167017746 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Animation_t3821138400 * L_0 = __this->get_m_Outer_0();
		int32_t L_1 = __this->get_m_CurrentIndex_1();
		NullCheck(L_0);
		AnimationState_t1353060852 * L_2 = Animation_GetStateAtIndex_m109571758(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1787700375 (Enumerator_t3167017746 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		Animation_t3821138400 * L_0 = __this->get_m_Outer_0();
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m1512371370(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_m_CurrentIndex_1();
		__this->set_m_CurrentIndex_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = __this->get_m_CurrentIndex_1();
		int32_t L_4 = V_0;
		V_1 = (bool)((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
		goto IL_002a;
	}

IL_002a:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m1555866632 (Enumerator_t3167017746 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		return;
	}
}
// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m1706800806 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	{
		Motion__ctor_m3009723041(__this, /*hidden argument*/NULL);
		AnimationClip_Internal_CreateAnimationClip_m3243057121(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m3243057121 (RuntimeObject * __this /* static, unused */, AnimationClip_t1145879031 * ___self0, const RuntimeMethod* method)
{
	typedef void (*AnimationClip_Internal_CreateAnimationClip_m3243057121_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_Internal_CreateAnimationClip_m3243057121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_Internal_CreateAnimationClip_m3243057121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___self0);
}
// System.Single UnityEngine.AnimationClip::get_length()
extern "C"  float AnimationClip_get_length_m160896021 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationClip_get_length_m160896021_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_get_length_m160896021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_length_m160896021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_length()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.AnimationClip::get_frameRate()
extern "C"  float AnimationClip_get_frameRate_m192555437 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationClip_get_frameRate_m192555437_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_get_frameRate_m192555437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_frameRate_m192555437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_frameRate()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.AnimationClip::get_legacy()
extern "C"  bool AnimationClip_get_legacy_m4218621674 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_legacy_m4218621674_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_get_legacy_m4218621674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_legacy_m4218621674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_legacy()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationClip::set_legacy(System.Boolean)
extern "C"  void AnimationClip_set_legacy_m535601416 (AnimationClip_t1145879031 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationClip_set_legacy_m535601416_ftn) (AnimationClip_t1145879031 *, bool);
	static AnimationClip_set_legacy_m535601416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_set_legacy_m535601416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::set_legacy(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AnimationClip::get_empty()
extern "C"  bool AnimationClip_get_empty_m2887819551 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_empty_m2887819551_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_get_empty_m2887819551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_empty_m2887819551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_empty()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.AnimationClip::get_hasRootMotion()
extern "C"  bool AnimationClip_get_hasRootMotion_m1388558917 (AnimationClip_t1145879031 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_hasRootMotion_m1388558917_ftn) (AnimationClip_t1145879031 *);
	static AnimationClip_get_hasRootMotion_m1388558917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_hasRootMotion_m1388558917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_hasRootMotion()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke(const AnimationEvent_t3145551608& unmarshaled, AnimationEvent_t3145551608_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke_back(const AnimationEvent_t3145551608_marshaled_pinvoke& marshaled, AnimationEvent_t3145551608& unmarshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3145551608_marshal_pinvoke_cleanup(AnimationEvent_t3145551608_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3145551608_marshal_com(const AnimationEvent_t3145551608& unmarshaled, AnimationEvent_t3145551608_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
extern "C" void AnimationEvent_t3145551608_marshal_com_back(const AnimationEvent_t3145551608_marshaled_com& marshaled, AnimationEvent_t3145551608& unmarshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3145551608_marshal_com_cleanup(AnimationEvent_t3145551608_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern "C"  void AnimationEvent__ctor_m430745688 (AnimationEvent_t3145551608 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m430745688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		__this->set_m_FunctionName_1(_stringLiteral4185710657);
		__this->set_m_StringParameter_2(_stringLiteral4185710657);
		__this->set_m_ObjectReferenceParameter_3((Object_t692178351 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t1353060852 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationClipPlayable__ctor_m3931264303 (AnimationClipPlayable_t2813945836 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationClipPlayable__ctor_m3931264303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t2813945836_m1137688306_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral3378384216, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationClipPlayable__ctor_m3931264303_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	AnimationClipPlayable__ctor_m3931264303(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_CreateHandleInternal_m4175826051 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AnimationClip_t1145879031 * ___clip1, PlayableHandle_t743382320 * ___handle2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		AnimationClip_t1145879031 * L_0 = ___clip1;
		PlayableHandle_t743382320 * L_1 = ___handle2;
		bool L_2 = AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913(NULL /*static, unused*/, (&___graph0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, AnimationClip_t1145879031 * ___clip1, PlayableHandle_t743382320 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913_ftn) (PlayableGraph_t1490949931 *, AnimationClip_t1145879031 *, PlayableHandle_t743382320 *);
	static AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_CreateHandleInternal_m3096545913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___clip1, ___handle2);
	return retVal;
}
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClip()
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_GetAnimationClip_m612583448 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method)
{
	AnimationClip_t1145879031 * V_0 = NULL;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		AnimationClip_t1145879031 * L_1 = AnimationClipPlayable_GetAnimationClipInternal_m3492101426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AnimationClip_t1145879031 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_GetAnimationClip_m612583448_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	return AnimationClipPlayable_GetAnimationClip_m612583448(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIK()
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m4095876580 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AnimationClipPlayable_GetApplyFootIKInternal_m1027566677(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m4095876580_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	return AnimationClipPlayable_GetApplyFootIK_m4095876580(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffset()
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m535172577 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AnimationClipPlayable_GetRemoveStartOffsetInternal_m57204089(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m535172577_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	return AnimationClipPlayable_GetRemoveStartOffset_m535172577(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffset(System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m629646185 (AnimationClipPlayable_t2813945836 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = ___value0;
		AnimationClipPlayable_SetRemoveStartOffsetInternal_m3545622143(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m629646185_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	AnimationClipPlayable_SetRemoveStartOffset_m629646185(_thisAdjusted, ___value0, method);
}
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_GetAnimationClipInternal_m3492101426 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	AnimationClip_t1145879031 * V_0 = NULL;
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		AnimationClip_t1145879031 * L_1 = AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		AnimationClip_t1145879031 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  AnimationClip_t1145879031 * AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	typedef AnimationClip_t1145879031 * (*AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603_ftn) (PlayableHandle_t743382320 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetAnimationClipInternal_m371826603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)");
	AnimationClip_t1145879031 * retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetApplyFootIKInternal_m1027566677 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		bool L_1 = AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167_ftn) (PlayableHandle_t743382320 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIKInternal_m2909237167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffsetInternal_m57204089 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		bool L_1 = AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717_ftn) (PlayableHandle_t743382320 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffsetInternal_m625732717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffsetInternal_m3545622143 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, bool ___value1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		bool L_1 = ___value1;
		AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277_ftn) (PlayableHandle_t743382320 *, bool);
	static AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffsetInternal_m1486877277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// UnityEngine.Animations.AnimationClipPlayable UnityEngine.Animations.AnimationClipPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C"  AnimationClipPlayable_t2813945836  AnimationClipPlayable_Create_m3141563549 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AnimationClip_t1145879031 * ___clip1, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationClipPlayable_t2813945836  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		AnimationClip_t1145879031 * L_1 = ___clip1;
		PlayableHandle_t743382320  L_2 = AnimationClipPlayable_CreateHandle_m4234307555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PlayableHandle_t743382320  L_3 = V_0;
		AnimationClipPlayable_t2813945836  L_4;
		memset(&L_4, 0, sizeof(L_4));
		AnimationClipPlayable__ctor_m3931264303((&L_4), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		AnimationClipPlayable_t2813945836  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C"  PlayableHandle_t743382320  AnimationClipPlayable_CreateHandle_m4234307555 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, AnimationClip_t1145879031 * ___clip1, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t1490949931  L_1 = ___graph0;
		AnimationClip_t1145879031 * L_2 = ___clip1;
		bool L_3 = AnimationClipPlayable_CreateHandleInternal_m4175826051(NULL /*static, unused*/, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		PlayableHandle_t743382320  L_4 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0027;
	}

IL_0020:
	{
		PlayableHandle_t743382320  L_5 = V_0;
		V_1 = L_5;
		goto IL_0027;
	}

IL_0027:
	{
		PlayableHandle_t743382320  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationClipPlayable_GetHandle_m3457548684 (AnimationClipPlayable_t2813945836 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AnimationClipPlayable_GetHandle_m3457548684_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	return AnimationClipPlayable_GetHandle_m3457548684(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationClipPlayable::op_Implicit(UnityEngine.Animations.AnimationClipPlayable)
extern "C"  Playable_t3296292090  AnimationClipPlayable_op_Implicit_m2484051765 (RuntimeObject * __this /* static, unused */, AnimationClipPlayable_t2813945836  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AnimationClipPlayable_GetHandle_m3457548684((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::Equals(UnityEngine.Animations.AnimationClipPlayable)
extern "C"  bool AnimationClipPlayable_Equals_m2689488492 (AnimationClipPlayable_t2813945836 * __this, AnimationClipPlayable_t2813945836  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AnimationClipPlayable_GetHandle_m3457548684(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AnimationClipPlayable_GetHandle_m3457548684((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationClipPlayable_Equals_m2689488492_AdjustorThunk (RuntimeObject * __this, AnimationClipPlayable_t2813945836  ___other0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t2813945836 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t2813945836 *>(__this + 1);
	return AnimationClipPlayable_Equals_m2689488492(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationLayerMixerPlayable__ctor_m3403316352 (AnimationLayerMixerPlayable_t4073556314 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable__ctor_m3403316352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t4073556314_m2345357204_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral4113542315, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationLayerMixerPlayable__ctor_m3403316352_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t4073556314 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t4073556314 *>(__this + 1);
	AnimationLayerMixerPlayable__ctor_m3403316352(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationLayerMixerPlayable_CreateHandleInternal_m2718066848 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_CreateHandleInternal_m2718066848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___handle1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		bool L_1 = AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135(NULL /*static, unused*/, (&___graph0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method)
{
	typedef bool (*AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135_ftn) (PlayableGraph_t1490949931 *, PlayableHandle_t743382320 *);
	static AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationLayerMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m2898533135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___handle1);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMask(System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528 (AnimationLayerMixerPlayable_t4073556314 * __this, uint32_t ___layerIndex0, AvatarMask_t2076757458 * ___mask1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint32_t L_0 = ___layerIndex0;
		PlayableHandle_t743382320 * L_1 = __this->get_address_of_m_Handle_0();
		int32_t L_2 = PlayableHandle_GetInputCount_m3012569931(L_1, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((uint64_t)L_0)))) < ((int64_t)(((int64_t)((int64_t)L_2))))))
		{
			goto IL_0041;
		}
	}
	{
		uint32_t L_3 = ___layerIndex0;
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_t1721902794_il2cpp_TypeInfo_var, &L_4);
		PlayableHandle_t743382320 * L_6 = __this->get_address_of_m_Handle_0();
		int32_t L_7 = PlayableHandle_GetInputCount_m3012569931(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)((int32_t)L_7-(int32_t)1));
		RuntimeObject * L_9 = Box(Int32_t972567508_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m3571813163(NULL /*static, unused*/, _stringLiteral2578260669, L_5, L_9, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t2335218917 * L_11 = (ArgumentOutOfRangeException_t2335218917 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2335218917_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2446168321(L_11, _stringLiteral2414497163, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0041:
	{
		AvatarMask_t2076757458 * L_12 = ___mask1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_12, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0058;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_14 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_14, _stringLiteral2105139031, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0058:
	{
		PlayableHandle_t743382320 * L_15 = __this->get_address_of_m_Handle_0();
		uint32_t L_16 = ___layerIndex0;
		AvatarMask_t2076757458 * L_17 = ___mask1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m1500292959(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528_AdjustorThunk (RuntimeObject * __this, uint32_t ___layerIndex0, AvatarMask_t2076757458 * ___mask1, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t4073556314 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t4073556314 *>(__this + 1);
	AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m920580528(_thisAdjusted, ___layerIndex0, ___mask1, method);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m1500292959 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t2076757458 * ___mask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m1500292959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		uint32_t L_1 = ___layerIndex1;
		AvatarMask_t2076757458 * L_2 = ___mask2;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C"  void AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t2076757458 * ___mask2, const RuntimeMethod* method)
{
	typedef void (*AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195_ftn) (PlayableHandle_t743382320 *, uint32_t, AvatarMask_t2076757458 *);
	static AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationLayerMixerPlayable_INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal_m80612195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationLayerMixerPlayable::INTERNAL_CALL_SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)");
	_il2cpp_icall_func(___handle0, ___layerIndex1, ___mask2);
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::get_Null()
extern "C"  AnimationLayerMixerPlayable_t4073556314  AnimationLayerMixerPlayable_get_Null_m3868116653 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_get_Null_m3868116653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationLayerMixerPlayable_t4073556314  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		AnimationLayerMixerPlayable_t4073556314  L_0 = ((AnimationLayerMixerPlayable_t4073556314_StaticFields*)il2cpp_codegen_static_fields_for(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		AnimationLayerMixerPlayable_t4073556314  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C"  AnimationLayerMixerPlayable_t4073556314  AnimationLayerMixerPlayable_Create_m3450340570 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_Create_m3450340570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationLayerMixerPlayable_t4073556314  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		PlayableHandle_t743382320  L_2 = AnimationLayerMixerPlayable_CreateHandle_m3929655259(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PlayableHandle_t743382320  L_3 = V_0;
		AnimationLayerMixerPlayable_t4073556314  L_4;
		memset(&L_4, 0, sizeof(L_4));
		AnimationLayerMixerPlayable__ctor_m3403316352((&L_4), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		AnimationLayerMixerPlayable_t4073556314  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C"  PlayableHandle_t743382320  AnimationLayerMixerPlayable_CreateHandle_m3929655259 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_CreateHandle_m3929655259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t1490949931  L_1 = ___graph0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var);
		bool L_2 = AnimationLayerMixerPlayable_CreateHandleInternal_m2718066848(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		PlayableHandle_t743382320  L_3 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_002e;
	}

IL_001f:
	{
		int32_t L_4 = ___inputCount1;
		PlayableHandle_SetInputCount_m3197056814((&V_0), L_4, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_5 = V_0;
		V_1 = L_5;
		goto IL_002e;
	}

IL_002e:
	{
		PlayableHandle_t743382320  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationLayerMixerPlayable_GetHandle_m1521142950 (AnimationLayerMixerPlayable_t4073556314 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AnimationLayerMixerPlayable_GetHandle_m1521142950_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t4073556314 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t4073556314 *>(__this + 1);
	return AnimationLayerMixerPlayable_GetHandle_m1521142950(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationLayerMixerPlayable::op_Implicit(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C"  Playable_t3296292090  AnimationLayerMixerPlayable_op_Implicit_m1041713701 (RuntimeObject * __this /* static, unused */, AnimationLayerMixerPlayable_t4073556314  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AnimationLayerMixerPlayable_GetHandle_m1521142950((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::op_Explicit(UnityEngine.Playables.Playable)
extern "C"  AnimationLayerMixerPlayable_t4073556314  AnimationLayerMixerPlayable_op_Explicit_m395046944 (RuntimeObject * __this /* static, unused */, Playable_t3296292090  ___playable0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t4073556314  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = Playable_GetHandle_m3912652506((&___playable0), /*hidden argument*/NULL);
		AnimationLayerMixerPlayable_t4073556314  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationLayerMixerPlayable__ctor_m3403316352((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		AnimationLayerMixerPlayable_t4073556314  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::Equals(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C"  bool AnimationLayerMixerPlayable_Equals_m2203675750 (AnimationLayerMixerPlayable_t4073556314 * __this, AnimationLayerMixerPlayable_t4073556314  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AnimationLayerMixerPlayable_GetHandle_m1521142950(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AnimationLayerMixerPlayable_GetHandle_m1521142950((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationLayerMixerPlayable_Equals_m2203675750_AdjustorThunk (RuntimeObject * __this, AnimationLayerMixerPlayable_t4073556314  ___other0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t4073556314 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t4073556314 *>(__this + 1);
	return AnimationLayerMixerPlayable_Equals_m2203675750(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.cctor()
extern "C"  void AnimationLayerMixerPlayable__cctor_m3977391353 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable__cctor_m3977391353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationLayerMixerPlayable_t4073556314  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationLayerMixerPlayable__ctor_m3403316352((&L_1), L_0, /*hidden argument*/NULL);
		((AnimationLayerMixerPlayable_t4073556314_StaticFields*)il2cpp_codegen_static_fields_for(AnimationLayerMixerPlayable_t4073556314_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationMixerPlayable__ctor_m354515751 (AnimationMixerPlayable_t2307681866 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationMixerPlayable__ctor_m354515751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t2307681866_m2701287236_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral562150037, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationMixerPlayable__ctor_m354515751_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t2307681866 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t2307681866 *>(__this + 1);
	AnimationMixerPlayable__ctor_m354515751(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationMixerPlayable_CreateHandleInternal_m3116575894 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___inputCount1;
		bool L_1 = ___normalizeWeights2;
		PlayableHandle_t743382320 * L_2 = ___handle3;
		bool L_3 = AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134(NULL /*static, unused*/, (&___graph0), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134_ftn) (PlayableGraph_t1490949931 *, int32_t, bool, PlayableHandle_t743382320 *);
	static AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationMixerPlayable_INTERNAL_CALL_CreateHandleInternal_m719010134_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationMixerPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___inputCount1, ___normalizeWeights2, ___handle3);
	return retVal;
}
// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  AnimationMixerPlayable_t2307681866  AnimationMixerPlayable_Create_m363916422 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationMixerPlayable_t2307681866  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		bool L_2 = ___normalizeWeights2;
		PlayableHandle_t743382320  L_3 = AnimationMixerPlayable_CreateHandle_m1467265624(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PlayableHandle_t743382320  L_4 = V_0;
		AnimationMixerPlayable_t2307681866  L_5;
		memset(&L_5, 0, sizeof(L_5));
		AnimationMixerPlayable__ctor_m354515751((&L_5), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		AnimationMixerPlayable_t2307681866  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C"  PlayableHandle_t743382320  AnimationMixerPlayable_CreateHandle_m1467265624 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t1490949931  L_1 = ___graph0;
		int32_t L_2 = ___inputCount1;
		bool L_3 = ___normalizeWeights2;
		bool L_4 = AnimationMixerPlayable_CreateHandleInternal_m3116575894(NULL /*static, unused*/, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		PlayableHandle_t743382320  L_5 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0030;
	}

IL_0021:
	{
		int32_t L_6 = ___inputCount1;
		PlayableHandle_SetInputCount_m3197056814((&V_0), L_6, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_7 = V_0;
		V_1 = L_7;
		goto IL_0030;
	}

IL_0030:
	{
		PlayableHandle_t743382320  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationMixerPlayable_GetHandle_m3098381875 (AnimationMixerPlayable_t2307681866 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AnimationMixerPlayable_GetHandle_m3098381875_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t2307681866 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t2307681866 *>(__this + 1);
	return AnimationMixerPlayable_GetHandle_m3098381875(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationMixerPlayable::op_Implicit(UnityEngine.Animations.AnimationMixerPlayable)
extern "C"  Playable_t3296292090  AnimationMixerPlayable_op_Implicit_m569401851 (RuntimeObject * __this /* static, unused */, AnimationMixerPlayable_t2307681866  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AnimationMixerPlayable_GetHandle_m3098381875((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::Equals(UnityEngine.Animations.AnimationMixerPlayable)
extern "C"  bool AnimationMixerPlayable_Equals_m2111336796 (AnimationMixerPlayable_t2307681866 * __this, AnimationMixerPlayable_t2307681866  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AnimationMixerPlayable_GetHandle_m3098381875(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AnimationMixerPlayable_GetHandle_m3098381875((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationMixerPlayable_Equals_m2111336796_AdjustorThunk (RuntimeObject * __this, AnimationMixerPlayable_t2307681866  ___other0, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t2307681866 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t2307681866 *>(__this + 1);
	return AnimationMixerPlayable_Equals_m2111336796(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimationOffsetPlayable__ctor_m3498961827 (AnimationOffsetPlayable_t786003479 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable__ctor_m3498961827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t786003479_m2105731166_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral1312382514, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t743382320  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationOffsetPlayable__ctor_m3498961827_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t786003479 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t786003479 *>(__this + 1);
	AnimationOffsetPlayable__ctor_m3498961827(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationOffsetPlayable_CreateHandleInternal_m1530857440 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, Vector3_t1986933152  ___position1, Quaternion_t704191599  ___rotation2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_CreateHandleInternal_m1530857440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		PlayableHandle_t743382320 * L_0 = ___handle3;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		bool L_1 = AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181(NULL /*static, unused*/, (&___graph0), (&___position1), (&___rotation2), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, Vector3_t1986933152 * ___position1, Quaternion_t704191599 * ___rotation2, PlayableHandle_t743382320 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181_ftn) (PlayableGraph_t1490949931 *, Vector3_t1986933152 *, Quaternion_t704191599 *, PlayableHandle_t743382320 *);
	static AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_INTERNAL_CALL_CreateHandleInternal_m1022678181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_CreateHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___position1, ___rotation2, ___handle3);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPosition()
extern "C"  Vector3_t1986933152  AnimationOffsetPlayable_GetPosition_m1979386976 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetPosition_m1979386976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = AnimationOffsetPlayable_GetPositionInternal_m1179161308(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t1986933152  AnimationOffsetPlayable_GetPosition_m1979386976_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t786003479 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t786003479 *>(__this + 1);
	return AnimationOffsetPlayable_GetPosition_m1979386976(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotation()
extern "C"  Quaternion_t704191599  AnimationOffsetPlayable_GetRotation_m1783363935 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetRotation_m1783363935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_1 = AnimationOffsetPlayable_GetRotationInternal_m1466851248(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t704191599  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Quaternion_t704191599  AnimationOffsetPlayable_GetRotation_m1783363935_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t786003479 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t786003479 *>(__this + 1);
	return AnimationOffsetPlayable_GetRotation_m1783363935(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  Vector3_t1986933152  AnimationOffsetPlayable_GetPositionInternal_m1179161308 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetPositionInternal_m1179161308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t1986933152  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetPositionInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)
extern "C"  void AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, Vector3_t1986933152 * ___value1, const RuntimeMethod* method)
{
	typedef void (*AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880_ftn) (PlayableHandle_t743382320 *, Vector3_t1986933152 *);
	static AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_INTERNAL_CALL_GetPositionInternal_m2389352880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetPositionInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  Quaternion_t704191599  AnimationOffsetPlayable_GetRotationInternal_m1466851248 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetRotationInternal_m1466851248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t704191599  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320 * L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t704191599  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t704191599  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetRotationInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)
extern "C"  void AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___handle0, Quaternion_t704191599 * ___value1, const RuntimeMethod* method)
{
	typedef void (*AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155_ftn) (PlayableHandle_t743382320 *, Quaternion_t704191599 *);
	static AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_INTERNAL_CALL_GetRotationInternal_m2389541155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::INTERNAL_CALL_GetRotationInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  AnimationOffsetPlayable_t786003479  AnimationOffsetPlayable_Create_m1320251422 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, Vector3_t1986933152  ___position1, Quaternion_t704191599  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_Create_m1320251422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationOffsetPlayable_t786003479  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t1490949931  L_0 = ___graph0;
		Vector3_t1986933152  L_1 = ___position1;
		Quaternion_t704191599  L_2 = ___rotation2;
		int32_t L_3 = ___inputCount3;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		PlayableHandle_t743382320  L_4 = AnimationOffsetPlayable_CreateHandle_m1672393200(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		PlayableHandle_t743382320  L_5 = V_0;
		AnimationOffsetPlayable_t786003479  L_6;
		memset(&L_6, 0, sizeof(L_6));
		AnimationOffsetPlayable__ctor_m3498961827((&L_6), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		AnimationOffsetPlayable_t786003479  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  PlayableHandle_t743382320  AnimationOffsetPlayable_CreateHandle_m1672393200 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, Vector3_t1986933152  ___position1, Quaternion_t704191599  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_CreateHandle_m1672393200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t1490949931  L_1 = ___graph0;
		Vector3_t1986933152  L_2 = ___position1;
		Quaternion_t704191599  L_3 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var);
		bool L_4 = AnimationOffsetPlayable_CreateHandleInternal_m1530857440(NULL /*static, unused*/, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		PlayableHandle_t743382320  L_5 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0030;
	}

IL_0021:
	{
		int32_t L_6 = ___inputCount3;
		PlayableHandle_SetInputCount_m3197056814((&V_0), L_6, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_7 = V_0;
		V_1 = L_7;
		goto IL_0030;
	}

IL_0030:
	{
		PlayableHandle_t743382320  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimationOffsetPlayable_GetHandle_m3588746519 (AnimationOffsetPlayable_t786003479 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AnimationOffsetPlayable_GetHandle_m3588746519_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t786003479 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t786003479 *>(__this + 1);
	return AnimationOffsetPlayable_GetHandle_m3588746519(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationOffsetPlayable::op_Implicit(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C"  Playable_t3296292090  AnimationOffsetPlayable_op_Implicit_m1036000925 (RuntimeObject * __this /* static, unused */, AnimationOffsetPlayable_t786003479  ___playable0, const RuntimeMethod* method)
{
	Playable_t3296292090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = AnimationOffsetPlayable_GetHandle_m3588746519((&___playable0), /*hidden argument*/NULL);
		Playable_t3296292090  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m765599562((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t3296292090  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::Equals(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C"  bool AnimationOffsetPlayable_Equals_m2331729891 (AnimationOffsetPlayable_t786003479 * __this, AnimationOffsetPlayable_t786003479  ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_Equals_m2331729891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AnimationOffsetPlayable_GetHandle_m3588746519((&___other0), /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = L_0;
		RuntimeObject * L_2 = Box(PlayableHandle_t743382320_il2cpp_TypeInfo_var, &L_1);
		RuntimeObject * L_3 = Box(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var, __this);
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_2);
		*__this = *(AnimationOffsetPlayable_t786003479 *)UnBox(L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
extern "C"  bool AnimationOffsetPlayable_Equals_m2331729891_AdjustorThunk (RuntimeObject * __this, AnimationOffsetPlayable_t786003479  ___other0, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t786003479 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t786003479 *>(__this + 1);
	return AnimationOffsetPlayable_Equals_m2331729891(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.cctor()
extern "C"  void AnimationOffsetPlayable__cctor_m1029727379 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable__cctor_m1029727379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationOffsetPlayable_t786003479  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationOffsetPlayable__ctor_m3498961827((&L_1), L_0, /*hidden argument*/NULL);
		((AnimationOffsetPlayable_t786003479_StaticFields*)il2cpp_codegen_static_fields_for(AnimationOffsetPlayable_t786003479_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationPlayableExtensions::SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)
extern "C"  void AnimationPlayableExtensions_SetAnimatedPropertiesInternal_m1650061229 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___playable0, AnimationClip_t1145879031 * ___animatedProperties1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320 * L_0 = ___playable0;
		AnimationClip_t1145879031 * L_1 = ___animatedProperties1;
		AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationPlayableExtensions::INTERNAL_CALL_SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)
extern "C"  void AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038 (RuntimeObject * __this /* static, unused */, PlayableHandle_t743382320 * ___playable0, AnimationClip_t1145879031 * ___animatedProperties1, const RuntimeMethod* method)
{
	typedef void (*AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038_ftn) (PlayableHandle_t743382320 *, AnimationClip_t1145879031 *);
	static AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableExtensions_INTERNAL_CALL_SetAnimatedPropertiesInternal_m4283627038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableExtensions::INTERNAL_CALL_SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___playable0, ___animatedProperties1);
}
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m3536337750 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		String_t* L_1 = ___name1;
		PlayableOutputHandle_t506089257 * L_2 = ___handle2;
		bool L_3 = AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, String_t* ___name1, PlayableOutputHandle_t506089257 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062_ftn) (PlayableGraph_t1490949931 *, String_t*, PlayableOutputHandle_t506089257 *);
	static AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationOutput_m1797861062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___name1, ___handle2);
	return retVal;
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationPlayableGraphExtensions::CreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph)
extern "C"  PlayableHandle_t743382320  AnimationPlayableGraphExtensions_CreateAnimationMotionXToDeltaPlayable_m1279066392 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t743382320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = AnimationPlayableGraphExtensions_InternalCreateAnimationMotionXToDeltaPlayable_m458782705(NULL /*static, unused*/, (&___graph0), (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		PlayableHandle_t743382320  L_2 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_002f;
	}

IL_0020:
	{
		PlayableHandle_SetInputCount_m3197056814((&V_0), 1, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_3 = V_0;
		V_1 = L_3;
		goto IL_002f;
	}

IL_002f:
	{
		PlayableHandle_t743382320  L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_InternalCreateAnimationMotionXToDeltaPlayable_m458782705 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		PlayableHandle_t743382320 * L_1 = ___handle1;
		bool L_2 = AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931 * ___graph0, PlayableHandle_t743382320 * ___handle1, const RuntimeMethod* method)
{
	typedef bool (*AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759_ftn) (PlayableGraph_t1490949931 *, PlayableHandle_t743382320 *);
	static AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable_m76234759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAnimationMotionXToDeltaPlayable(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___handle1);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void AnimationPlayableOutput__ctor_m2942214619 (AnimationPlayableOutput_t2708046649 * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayableOutput__ctor_m2942214619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableOutputHandle_IsValid_m124014148((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854((&___handle0), /*hidden argument*/PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t2708046649_m3866528854_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3988541634 * L_2 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_2, _stringLiteral223177411, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableOutputHandle_t506089257  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationPlayableOutput__ctor_m2942214619_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t506089257  ___handle0, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t2708046649 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t2708046649 *>(__this + 1);
	AnimationPlayableOutput__ctor_m2942214619(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::GetTarget()
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_GetTarget_m2544830582 (AnimationPlayableOutput_t2708046649 * __this, const RuntimeMethod* method)
{
	Animator_t2768715325 * V_0 = NULL;
	{
		PlayableOutputHandle_t506089257 * L_0 = __this->get_address_of_m_Handle_0();
		Animator_t2768715325 * L_1 = AnimationPlayableOutput_InternalGetTarget_m2813996520(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Animator_t2768715325 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_GetTarget_m2544830582_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t2708046649 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t2708046649 *>(__this + 1);
	return AnimationPlayableOutput_GetTarget_m2544830582(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::SetTarget(UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_SetTarget_m1801562013 (AnimationPlayableOutput_t2708046649 * __this, Animator_t2768715325 * ___value0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t506089257 * L_0 = __this->get_address_of_m_Handle_0();
		Animator_t2768715325 * L_1 = ___value0;
		AnimationPlayableOutput_InternalSetTarget_m3556155859(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationPlayableOutput_SetTarget_m1801562013_AdjustorThunk (RuntimeObject * __this, Animator_t2768715325 * ___value0, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t2708046649 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t2708046649 *>(__this + 1);
	AnimationPlayableOutput_SetTarget_m1801562013(_thisAdjusted, ___value0, method);
}
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_InternalGetTarget_m2813996520 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, const RuntimeMethod* method)
{
	Animator_t2768715325 * V_0 = NULL;
	{
		PlayableOutputHandle_t506089257 * L_0 = ___handle0;
		Animator_t2768715325 * L_1 = AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Animator_t2768715325 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Animator_t2768715325 * AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, const RuntimeMethod* method)
{
	typedef Animator_t2768715325 * (*AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079_ftn) (PlayableOutputHandle_t506089257 *);
	static AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableOutput_INTERNAL_CALL_InternalGetTarget_m1413947079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)");
	Animator_t2768715325 * retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_InternalSetTarget_m3556155859 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, Animator_t2768715325 * ___target1, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t506089257 * L_0 = ___handle0;
		Animator_t2768715325 * L_1 = ___target1;
		AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C"  void AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t506089257 * ___handle0, Animator_t2768715325 * ___target1, const RuntimeMethod* method)
{
	typedef void (*AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087_ftn) (PlayableOutputHandle_t506089257 *, Animator_t2768715325 *);
	static AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableOutput_INTERNAL_CALL_InternalSetTarget_m2488122087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableOutput::INTERNAL_CALL_InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)");
	_il2cpp_icall_func(___handle0, ___target1);
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String,UnityEngine.Animator)
extern "C"  AnimationPlayableOutput_t2708046649  AnimationPlayableOutput_Create_m2484549358 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1490949931  ___graph0, String_t* ___name1, Animator_t2768715325 * ___target2, const RuntimeMethod* method)
{
	PlayableOutputHandle_t506089257  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationPlayableOutput_t2708046649  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AnimationPlayableOutput_t2708046649  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		String_t* L_0 = ___name1;
		bool L_1 = AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m3536337750(NULL /*static, unused*/, (&___graph0), L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		AnimationPlayableOutput_t2708046649  L_2 = AnimationPlayableOutput_get_Null_m3047134962(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0032;
	}

IL_001b:
	{
		PlayableOutputHandle_t506089257  L_3 = V_0;
		AnimationPlayableOutput__ctor_m2942214619((&V_2), L_3, /*hidden argument*/NULL);
		Animator_t2768715325 * L_4 = ___target2;
		AnimationPlayableOutput_SetTarget_m1801562013((&V_2), L_4, /*hidden argument*/NULL);
		AnimationPlayableOutput_t2708046649  L_5 = V_2;
		V_1 = L_5;
		goto IL_0032;
	}

IL_0032:
	{
		AnimationPlayableOutput_t2708046649  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::get_Null()
extern "C"  AnimationPlayableOutput_t2708046649  AnimationPlayableOutput_get_Null_m3047134962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t2708046649  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t506089257  L_0 = PlayableOutputHandle_get_Null_m4090468577(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationPlayableOutput_t2708046649  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationPlayableOutput__ctor_m2942214619((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		AnimationPlayableOutput_t2708046649  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t506089257  AnimationPlayableOutput_GetHandle_m1000807631 (AnimationPlayableOutput_t2708046649 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t506089257  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t506089257  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t506089257  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t506089257  AnimationPlayableOutput_GetHandle_m1000807631_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t2708046649 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t2708046649 *>(__this + 1);
	return AnimationPlayableOutput_GetHandle_m1000807631(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimatorControllerPlayable__ctor_m39277502 (AnimatorControllerPlayable_t4063768904 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Handle_0(L_0);
		PlayableHandle_t743382320  L_1 = ___handle0;
		AnimatorControllerPlayable_SetHandle_m4095728861(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimatorControllerPlayable__ctor_m39277502_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t4063768904 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4063768904 *>(__this + 1);
	AnimatorControllerPlayable__ctor_m39277502(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::GetHandle()
extern "C"  PlayableHandle_t743382320  AnimatorControllerPlayable_GetHandle_m4277404188 (AnimatorControllerPlayable_t4063768904 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t743382320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t743382320  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t743382320  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t743382320  AnimatorControllerPlayable_GetHandle_m4277404188_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t4063768904 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4063768904 *>(__this + 1);
	return AnimatorControllerPlayable_GetHandle_m4277404188(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::SetHandle(UnityEngine.Playables.PlayableHandle)
extern "C"  void AnimatorControllerPlayable_SetHandle_m4095728861 (AnimatorControllerPlayable_t4063768904 * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControllerPlayable_SetHandle_m4095728861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t743382320 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = PlayableHandle_IsValid_m2628965434(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t3358289486 * L_2 = (InvalidOperationException_t3358289486 *)il2cpp_codegen_object_new(InvalidOperationException_t3358289486_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2912198563(L_2, _stringLiteral3254879098, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		bool L_3 = PlayableHandle_IsValid_m2628965434((&___handle0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385((&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t4063768904_m688381385_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		InvalidCastException_t3988541634 * L_5 = (InvalidCastException_t3988541634 *)il2cpp_codegen_object_new(InvalidCastException_t3988541634_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1966396325(L_5, _stringLiteral1638578942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0040:
	{
	}

IL_0041:
	{
		PlayableHandle_t743382320  L_6 = ___handle0;
		__this->set_m_Handle_0(L_6);
		return;
	}
}
extern "C"  void AnimatorControllerPlayable_SetHandle_m4095728861_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t743382320  ___handle0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t4063768904 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4063768904 *>(__this + 1);
	AnimatorControllerPlayable_SetHandle_m4095728861(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimatorControllerPlayable::Equals(UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  bool AnimatorControllerPlayable_Equals_m2390645362 (AnimatorControllerPlayable_t4063768904 * __this, AnimatorControllerPlayable_t4063768904  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t743382320  L_0 = AnimatorControllerPlayable_GetHandle_m4277404188(__this, /*hidden argument*/NULL);
		PlayableHandle_t743382320  L_1 = AnimatorControllerPlayable_GetHandle_m4277404188((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2481114358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorControllerPlayable_Equals_m2390645362_AdjustorThunk (RuntimeObject * __this, AnimatorControllerPlayable_t4063768904  ___other0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t4063768904 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4063768904 *>(__this + 1);
	return AnimatorControllerPlayable_Equals_m2390645362(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.cctor()
extern "C"  void AnimatorControllerPlayable__cctor_m3436190657 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControllerPlayable__cctor_m3436190657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t743382320  L_0 = PlayableHandle_get_Null_m2780162545(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimatorControllerPlayable_t4063768904  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimatorControllerPlayable__ctor_m39277502((&L_1), L_0, /*hidden argument*/NULL);
		((AnimatorControllerPlayable_t4063768904_StaticFields*)il2cpp_codegen_static_fields_for(AnimatorControllerPlayable_t4063768904_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.AnimationState::.ctor()
extern "C"  void AnimationState__ctor_m282136970 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	{
		TrackedReference__ctor_m1051293765(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.AnimationState::get_time()
extern "C"  float AnimationState_get_time_m2870420575 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationState_get_time_m2870420575_ftn) (AnimationState_t1353060852 *);
	static AnimationState_get_time_m2870420575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_time_m2870420575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_time()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C"  void AnimationState_set_time_m184674145 (AnimationState_t1353060852 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_time_m184674145_ftn) (AnimationState_t1353060852 *, float);
	static AnimationState_set_time_m184674145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_time_m184674145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_speed()
extern "C"  float AnimationState_get_speed_m3453848080 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationState_get_speed_m3453848080_ftn) (AnimationState_t1353060852 *);
	static AnimationState_get_speed_m3453848080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_speed_m3453848080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_speed()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m3007111845 (AnimationState_t1353060852 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_speed_m3007111845_ftn) (AnimationState_t1353060852 *, float);
	static AnimationState_set_speed_m3007111845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_speed_m3007111845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_length()
extern "C"  float AnimationState_get_length_m2493970310 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationState_get_length_m2493970310_ftn) (AnimationState_t1353060852 *);
	static AnimationState_get_length_m2493970310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_length_m2493970310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_length()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C"  void AnimationState_set_layer_m1704854967 (AnimationState_t1353060852 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_layer_m1704854967_ftn) (AnimationState_t1353060852 *, int32_t);
	static AnimationState_set_layer_m1704854967_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_layer_m1704854967_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AnimationClip UnityEngine.AnimationState::get_clip()
extern "C"  AnimationClip_t1145879031 * AnimationState_get_clip_m2674062878 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	typedef AnimationClip_t1145879031 * (*AnimationState_get_clip_m2674062878_ftn) (AnimationState_t1353060852 *);
	static AnimationState_get_clip_m2674062878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_clip_m2674062878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_clip()");
	AnimationClip_t1145879031 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.String UnityEngine.AnimationState::get_name()
extern "C"  String_t* AnimationState_get_name_m3791938359 (AnimationState_t1353060852 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*AnimationState_get_name_m3791938359_ftn) (AnimationState_t1353060852 *);
	static AnimationState_get_name_m3791938359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_name_m3791938359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_name()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::.ctor()
extern "C"  void Animator__ctor_m4254275227 (Animator_t2768715325 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2653597291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m1993985023 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_SetTriggerString_m2787488330(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m1513939566 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_ResetTriggerString_m2785384252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m1500237654 (Animator_t2768715325 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_get_layerCount_m1500237654_ftn) (Animator_t2768715325 *);
	static Animator_get_layerCount_m1500237654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layerCount_m1500237654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layerCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t4013846803  Animator_GetCurrentAnimatorStateInfo_m3591795216 (Animator_t2768715325 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	typedef AnimatorStateInfo_t4013846803  (*Animator_GetCurrentAnimatorStateInfo_m3591795216_ftn) (Animator_t2768715325 *, int32_t);
	static Animator_GetCurrentAnimatorStateInfo_m3591795216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorStateInfo_m3591795216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)");
	AnimatorStateInfo_t4013846803  retVal = _il2cpp_icall_func(__this, ___layerIndex0);
	return retVal;
}
// UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)
extern "C"  AnimatorClipInfoU5BU5D_t2373780016* Animator_GetCurrentAnimatorClipInfo_m1831738408 (Animator_t2768715325 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	typedef AnimatorClipInfoU5BU5D_t2373780016* (*Animator_GetCurrentAnimatorClipInfo_m1831738408_ftn) (Animator_t2768715325 *, int32_t);
	static Animator_GetCurrentAnimatorClipInfo_m1831738408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorClipInfo_m1831738408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)");
	AnimatorClipInfoU5BU5D_t2373780016* retVal = _il2cpp_icall_func(__this, ___layerIndex0);
	return retVal;
}
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m3081484038 (Animator_t2768715325 * __this, String_t* ___stateName0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		V_1 = (-1);
		String_t* L_0 = ___stateName0;
		int32_t L_1 = V_1;
		float L_2 = V_0;
		Animator_Play_m1698754883(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m1698754883 (Animator_t2768715325 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m2696168653(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		float L_3 = ___normalizedTime2;
		Animator_Play_m2740437926(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m2740437926 (Animator_t2768715325 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method)
{
	typedef void (*Animator_Play_m2740437926_ftn) (Animator_t2768715325 *, int32_t, int32_t, float);
	static Animator_Play_m2740437926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m2740437926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___layer1, ___normalizedTime2);
}
// System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
extern "C"  bool Animator_get_hasBoundPlayables_m1499390031 (Animator_t2768715325 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_hasBoundPlayables_m1499390031_ftn) (Animator_t2768715325 *);
	static Animator_get_hasBoundPlayables_m1499390031_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_hasBoundPlayables_m1499390031_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_hasBoundPlayables()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m2696168653 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_StringToHash_m2696168653_ftn) (String_t*);
	static Animator_StringToHash_m2696168653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m2696168653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2787488330 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animator_SetTriggerString_m2787488330_ftn) (Animator_t2768715325 *, String_t*);
	static Animator_SetTriggerString_m2787488330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m2787488330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m2785384252 (Animator_t2768715325 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animator_ResetTriggerString_m2785384252_ftn) (Animator_t2768715325 *, String_t*);
	static Animator_ResetTriggerString_m2785384252_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m2785384252_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::get_clip()
extern "C"  AnimationClip_t1145879031 * AnimatorClipInfo_get_clip_m2688614585 (AnimatorClipInfo_t2792414269 * __this, const RuntimeMethod* method)
{
	AnimationClip_t1145879031 * V_0 = NULL;
	AnimationClip_t1145879031 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_m_ClipInstanceID_0();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = __this->get_m_ClipInstanceID_0();
		AnimationClip_t1145879031 * L_2 = AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = ((AnimationClip_t1145879031 *)(NULL));
	}

IL_001d:
	{
		V_0 = G_B3_0;
		goto IL_0023;
	}

IL_0023:
	{
		AnimationClip_t1145879031 * L_3 = V_0;
		return L_3;
	}
}
extern "C"  AnimationClip_t1145879031 * AnimatorClipInfo_get_clip_m2688614585_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorClipInfo_t2792414269 * _thisAdjusted = reinterpret_cast<AnimatorClipInfo_t2792414269 *>(__this + 1);
	return AnimatorClipInfo_get_clip_m2688614585(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorClipInfo::get_weight()
extern "C"  float AnimatorClipInfo_get_weight_m1864185950 (AnimatorClipInfo_t2792414269 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Weight_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorClipInfo_get_weight_m1864185950_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorClipInfo_t2792414269 * _thisAdjusted = reinterpret_cast<AnimatorClipInfo_t2792414269 *>(__this + 1);
	return AnimatorClipInfo_get_weight_m1864185950(_thisAdjusted, method);
}
// UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::ClipInstanceToScriptingObject(System.Int32)
extern "C"  AnimationClip_t1145879031 * AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method)
{
	typedef AnimationClip_t1145879031 * (*AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362_ftn) (int32_t);
	static AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimatorClipInfo_ClipInstanceToScriptingObject_m2715056362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimatorClipInfo::ClipInstanceToScriptingObject(System.Int32)");
	AnimationClip_t1145879031 * retVal = _il2cpp_icall_func(___instanceID0);
	return retVal;
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m1617659127 (AnimatorStateInfo_t4013846803 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m1617659127_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t4013846803 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t4013846803 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m1617659127(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke(const AnimatorTransitionInfo_t118268726& unmarshaled, AnimatorTransitionInfo_t118268726_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke_back(const AnimatorTransitionInfo_t118268726_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t118268726& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t118268726_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t118268726_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t118268726_marshal_com(const AnimatorTransitionInfo_t118268726& unmarshaled, AnimatorTransitionInfo_t118268726_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t118268726_marshal_com_back(const AnimatorTransitionInfo_t118268726_marshaled_com& marshaled, AnimatorTransitionInfo_t118268726& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t118268726_marshal_com_cleanup(AnimatorTransitionInfo_t118268726_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t634326648_marshal_pinvoke(const HumanBone_t634326648& unmarshaled, HumanBone_t634326648_marshaled_pinvoke& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t634326648_marshal_pinvoke_back(const HumanBone_t634326648_marshaled_pinvoke& marshaled, HumanBone_t634326648& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1));
	HumanLimit_t69269969  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t634326648_marshal_pinvoke_cleanup(HumanBone_t634326648_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t634326648_marshal_com(const HumanBone_t634326648& unmarshaled, HumanBone_t634326648_marshaled_com& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t634326648_marshal_com_back(const HumanBone_t634326648_marshaled_com& marshaled, HumanBone_t634326648& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___m_HumanName_1));
	HumanLimit_t69269969  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t634326648_marshal_com_cleanup(HumanBone_t634326648_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Void UnityEngine.Motion::.ctor()
extern "C"  void Motion__ctor_m3009723041 (Motion_t712717323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Motion__ctor_m3009723041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Motion::get_isLooping()
extern "C"  bool Motion_get_isLooping_m520660497 (Motion_t712717323 * __this, const RuntimeMethod* method)
{
	typedef bool (*Motion_get_isLooping_m520660497_ftn) (Motion_t712717323 *);
	static Motion_get_isLooping_m520660497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Motion_get_isLooping_m520660497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Motion::get_isLooping()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke(const SkeletonBone_t1743783883& unmarshaled, SkeletonBone_t1743783883_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke_back(const SkeletonBone_t1743783883_marshaled_pinvoke& marshaled, SkeletonBone_t1743783883& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_string_result(marshaled.___parentName_1));
	Vector3_t1986933152  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t704191599  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t1986933152  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1743783883_marshal_pinvoke_cleanup(SkeletonBone_t1743783883_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1743783883_marshal_com(const SkeletonBone_t1743783883& unmarshaled, SkeletonBone_t1743783883_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t1743783883_marshal_com_back(const SkeletonBone_t1743783883_marshaled_com& marshaled, SkeletonBone_t1743783883& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___parentName_1));
	Vector3_t1986933152  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t704191599  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t1986933152  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1743783883_marshal_com_cleanup(SkeletonBone_t1743783883_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
