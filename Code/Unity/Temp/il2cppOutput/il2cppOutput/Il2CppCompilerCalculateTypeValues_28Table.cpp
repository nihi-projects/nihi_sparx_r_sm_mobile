﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t2757045165;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// UnityEngine.Mesh
struct Mesh_t4030024733;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t3220781988;
// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>
struct HashSet_1_t1152188360;
// UnityEngine.Transform
struct Transform_t362059596;
// BoatTrigger
struct BoatTrigger_t357608549;
// GnatsCombat
struct GnatsCombat_t1950420656;
// Goodbye
struct Goodbye_t2957868680;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Void
struct Void_t653366341;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t427307204;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t449931294;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t2150172183;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t3851579516;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t2630807469;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t1419768887;
// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t1157756927;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t4125836827;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t3872312760;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1893950678;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t2644391680;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t2533961311;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t3424148675;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t3042570574;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t1266169160;
// UnityEngine.GUISkin
struct GUISkin_t2122630221;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// UnityEngine.Color[]
struct ColorU5BU5D_t247935167;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434;
// UnityEngine.Rect[]
struct RectU5BU5D_t141872167;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// UnityEngine.UI.Text
struct Text_t1790657652;
// DraggableObject[]
struct DraggableObjectU5BU5D_t2511386890;
// ShieldMenu
struct ShieldMenu_t3436809847;
// TalkScenes
struct TalkScenes_t1215051795;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t3304433276;
// UnityEngine.Renderer
struct Renderer_t1418648713;
// UnityEngine.Rigidbody
struct Rigidbody_t4273256674;
// CapturedDataInput
struct CapturedDataInput_t2616152122;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t892035751;
// UnityEngine.UI.Button
struct Button_t1293135404;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t2171935445;
// BoatCutSceneManager
struct BoatCutSceneManager_t78220184;
// ButtonCallback/ButtonCallbackDelegate
struct ButtonCallbackDelegate_t333906069;
// UnityEngine.Camera
struct Camera_t2839736942;
// PlayerMovement
struct PlayerMovement_t3557127520;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// DragObject/DragCallback
struct DragCallback_t2341893212;
// UnityEngine.MeshCollider[]
struct MeshColliderU5BU5D_t1888750571;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2248283753;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.Transform[]
struct TransformU5BU5D_t2383644165;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t32224225;
// GnatsAction[]
struct GnatsActionU5BU5D_t1693839301;
// NextScreen
struct NextScreen_t2084349028;
// OutlineObject
struct OutlineObject_t941793957;
// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>
struct List_1_t1370252420;
// CharacterCreation
struct CharacterCreation_t2214710272;
// ObjectMovement
struct ObjectMovement_t1497131487;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef POSTPROCESSINGMODEL_T519727923_H
#define POSTPROCESSINGMODEL_T519727923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t519727923  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t519727923, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T519727923_H
#ifndef COLORGRADINGCURVE_T4179314942_H
#define COLORGRADINGCURVE_T4179314942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingCurve
struct  ColorGradingCurve_t4179314942  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::curve
	AnimationCurve_t2757045165 * ___curve_0;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingCurve::m_Loop
	bool ___m_Loop_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_ZeroValue
	float ___m_ZeroValue_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_Range
	float ___m_Range_3;
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::m_InternalLoopingCurve
	AnimationCurve_t2757045165 * ___m_InternalLoopingCurve_4;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t4179314942, ___curve_0)); }
	inline AnimationCurve_t2757045165 * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_t2757045165 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_t2757045165 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_m_Loop_1() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t4179314942, ___m_Loop_1)); }
	inline bool get_m_Loop_1() const { return ___m_Loop_1; }
	inline bool* get_address_of_m_Loop_1() { return &___m_Loop_1; }
	inline void set_m_Loop_1(bool value)
	{
		___m_Loop_1 = value;
	}

	inline static int32_t get_offset_of_m_ZeroValue_2() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t4179314942, ___m_ZeroValue_2)); }
	inline float get_m_ZeroValue_2() const { return ___m_ZeroValue_2; }
	inline float* get_address_of_m_ZeroValue_2() { return &___m_ZeroValue_2; }
	inline void set_m_ZeroValue_2(float value)
	{
		___m_ZeroValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Range_3() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t4179314942, ___m_Range_3)); }
	inline float get_m_Range_3() const { return ___m_Range_3; }
	inline float* get_address_of_m_Range_3() { return &___m_Range_3; }
	inline void set_m_Range_3(float value)
	{
		___m_Range_3 = value;
	}

	inline static int32_t get_offset_of_m_InternalLoopingCurve_4() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t4179314942, ___m_InternalLoopingCurve_4)); }
	inline AnimationCurve_t2757045165 * get_m_InternalLoopingCurve_4() const { return ___m_InternalLoopingCurve_4; }
	inline AnimationCurve_t2757045165 ** get_address_of_m_InternalLoopingCurve_4() { return &___m_InternalLoopingCurve_4; }
	inline void set_m_InternalLoopingCurve_4(AnimationCurve_t2757045165 * value)
	{
		___m_InternalLoopingCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLoopingCurve_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCURVE_T4179314942_H
#ifndef GRAPHICSUTILS_T2847288477_H
#define GRAPHICSUTILS_T2847288477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GraphicsUtils
struct  GraphicsUtils_t2847288477  : public RuntimeObject
{
public:

public:
};

struct GraphicsUtils_t2847288477_StaticFields
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.GraphicsUtils::s_WhiteTexture
	Texture2D_t3063074017 * ___s_WhiteTexture_0;
	// UnityEngine.Mesh UnityEngine.PostProcessing.GraphicsUtils::s_Quad
	Mesh_t4030024733 * ___s_Quad_1;

public:
	inline static int32_t get_offset_of_s_WhiteTexture_0() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2847288477_StaticFields, ___s_WhiteTexture_0)); }
	inline Texture2D_t3063074017 * get_s_WhiteTexture_0() const { return ___s_WhiteTexture_0; }
	inline Texture2D_t3063074017 ** get_address_of_s_WhiteTexture_0() { return &___s_WhiteTexture_0; }
	inline void set_s_WhiteTexture_0(Texture2D_t3063074017 * value)
	{
		___s_WhiteTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_0), value);
	}

	inline static int32_t get_offset_of_s_Quad_1() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2847288477_StaticFields, ___s_Quad_1)); }
	inline Mesh_t4030024733 * get_s_Quad_1() const { return ___s_Quad_1; }
	inline Mesh_t4030024733 ** get_address_of_s_Quad_1() { return &___s_Quad_1; }
	inline void set_s_Quad_1(Mesh_t4030024733 * value)
	{
		___s_Quad_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Quad_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICSUTILS_T2847288477_H
#ifndef MATERIALFACTORY_T1986441003_H
#define MATERIALFACTORY_T1986441003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MaterialFactory
struct  MaterialFactory_t1986441003  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> UnityEngine.PostProcessing.MaterialFactory::m_Materials
	Dictionary_2_t3220781988 * ___m_Materials_0;

public:
	inline static int32_t get_offset_of_m_Materials_0() { return static_cast<int32_t>(offsetof(MaterialFactory_t1986441003, ___m_Materials_0)); }
	inline Dictionary_2_t3220781988 * get_m_Materials_0() const { return ___m_Materials_0; }
	inline Dictionary_2_t3220781988 ** get_address_of_m_Materials_0() { return &___m_Materials_0; }
	inline void set_m_Materials_0(Dictionary_2_t3220781988 * value)
	{
		___m_Materials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Materials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALFACTORY_T1986441003_H
#ifndef RENDERTEXTUREFACTORY_T1364694010_H
#define RENDERTEXTUREFACTORY_T1364694010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.RenderTextureFactory
struct  RenderTextureFactory_t1364694010  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture> UnityEngine.PostProcessing.RenderTextureFactory::m_TemporaryRTs
	HashSet_1_t1152188360 * ___m_TemporaryRTs_0;

public:
	inline static int32_t get_offset_of_m_TemporaryRTs_0() { return static_cast<int32_t>(offsetof(RenderTextureFactory_t1364694010, ___m_TemporaryRTs_0)); }
	inline HashSet_1_t1152188360 * get_m_TemporaryRTs_0() const { return ___m_TemporaryRTs_0; }
	inline HashSet_1_t1152188360 ** get_address_of_m_TemporaryRTs_0() { return &___m_TemporaryRTs_0; }
	inline void set_m_TemporaryRTs_0(HashSet_1_t1152188360 * value)
	{
		___m_TemporaryRTs_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TemporaryRTs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFACTORY_T1364694010_H
#ifndef UNIFORMS_T4027551681_H
#define UNIFORMS_T4027551681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController/Uniforms
struct  Uniforms_t4027551681  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t4027551681_StaticFields
{
public:
	// System.Int32 ExampleWheelController/Uniforms::_MotionAmount
	int32_t ____MotionAmount_0;

public:
	inline static int32_t get_offset_of__MotionAmount_0() { return static_cast<int32_t>(offsetof(Uniforms_t4027551681_StaticFields, ____MotionAmount_0)); }
	inline int32_t get__MotionAmount_0() const { return ____MotionAmount_0; }
	inline int32_t* get_address_of__MotionAmount_0() { return &____MotionAmount_0; }
	inline void set__MotionAmount_0(int32_t value)
	{
		____MotionAmount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T4027551681_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef U3CTIMETODELAYU3EC__ITERATOR0_T3570146540_H
#define U3CTIMETODELAYU3EC__ITERATOR0_T3570146540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatTrigger/<timeToDelay>c__Iterator0
struct  U3CtimeToDelayU3Ec__Iterator0_t3570146540  : public RuntimeObject
{
public:
	// UnityEngine.Transform BoatTrigger/<timeToDelay>c__Iterator0::teleportPos
	Transform_t362059596 * ___teleportPos_0;
	// BoatTrigger BoatTrigger/<timeToDelay>c__Iterator0::$this
	BoatTrigger_t357608549 * ___U24this_1;
	// System.Object BoatTrigger/<timeToDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BoatTrigger/<timeToDelay>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BoatTrigger/<timeToDelay>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_teleportPos_0() { return static_cast<int32_t>(offsetof(U3CtimeToDelayU3Ec__Iterator0_t3570146540, ___teleportPos_0)); }
	inline Transform_t362059596 * get_teleportPos_0() const { return ___teleportPos_0; }
	inline Transform_t362059596 ** get_address_of_teleportPos_0() { return &___teleportPos_0; }
	inline void set_teleportPos_0(Transform_t362059596 * value)
	{
		___teleportPos_0 = value;
		Il2CppCodeGenWriteBarrier((&___teleportPos_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CtimeToDelayU3Ec__Iterator0_t3570146540, ___U24this_1)); }
	inline BoatTrigger_t357608549 * get_U24this_1() const { return ___U24this_1; }
	inline BoatTrigger_t357608549 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BoatTrigger_t357608549 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CtimeToDelayU3Ec__Iterator0_t3570146540, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CtimeToDelayU3Ec__Iterator0_t3570146540, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CtimeToDelayU3Ec__Iterator0_t3570146540, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETODELAYU3EC__ITERATOR0_T3570146540_H
#ifndef U3CWAITTOREMOVEHELPBANNERU3EC__ITERATOR0_T3780298074_H
#define U3CWAITTOREMOVEHELPBANNERU3EC__ITERATOR0_T3780298074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsCombat/<WaitToRemoveHelpBanner>c__Iterator0
struct  U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074  : public RuntimeObject
{
public:
	// GnatsCombat GnatsCombat/<WaitToRemoveHelpBanner>c__Iterator0::$this
	GnatsCombat_t1950420656 * ___U24this_0;
	// System.Object GnatsCombat/<WaitToRemoveHelpBanner>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GnatsCombat/<WaitToRemoveHelpBanner>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GnatsCombat/<WaitToRemoveHelpBanner>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074, ___U24this_0)); }
	inline GnatsCombat_t1950420656 * get_U24this_0() const { return ___U24this_0; }
	inline GnatsCombat_t1950420656 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GnatsCombat_t1950420656 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTOREMOVEHELPBANNERU3EC__ITERATOR0_T3780298074_H
#ifndef U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4161125173_H
#define U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4161125173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Goodbye/<restartApplication>c__Iterator0
struct  U3CrestartApplicationU3Ec__Iterator0_t4161125173  : public RuntimeObject
{
public:
	// System.Boolean Goodbye/<restartApplication>c__Iterator0::<running>__0
	bool ___U3CrunningU3E__0_0;
	// Goodbye Goodbye/<restartApplication>c__Iterator0::$this
	Goodbye_t2957868680 * ___U24this_1;
	// System.Object Goodbye/<restartApplication>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Goodbye/<restartApplication>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Goodbye/<restartApplication>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrunningU3E__0_0() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4161125173, ___U3CrunningU3E__0_0)); }
	inline bool get_U3CrunningU3E__0_0() const { return ___U3CrunningU3E__0_0; }
	inline bool* get_address_of_U3CrunningU3E__0_0() { return &___U3CrunningU3E__0_0; }
	inline void set_U3CrunningU3E__0_0(bool value)
	{
		___U3CrunningU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4161125173, ___U24this_1)); }
	inline Goodbye_t2957868680 * get_U24this_1() const { return ___U24this_1; }
	inline Goodbye_t2957868680 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Goodbye_t2957868680 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4161125173, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4161125173, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4161125173, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4161125173_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef DRAGGABLEOBJECT_T1812188907_H
#define DRAGGABLEOBJECT_T1812188907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DraggableObject
struct  DraggableObject_t1812188907 
{
public:
	// UnityEngine.Rect DraggableObject::original
	Rect_t3039462994  ___original_0;
	// UnityEngine.Rect DraggableObject::movable
	Rect_t3039462994  ___movable_1;
	// System.String DraggableObject::words
	String_t* ___words_2;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(DraggableObject_t1812188907, ___original_0)); }
	inline Rect_t3039462994  get_original_0() const { return ___original_0; }
	inline Rect_t3039462994 * get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Rect_t3039462994  value)
	{
		___original_0 = value;
	}

	inline static int32_t get_offset_of_movable_1() { return static_cast<int32_t>(offsetof(DraggableObject_t1812188907, ___movable_1)); }
	inline Rect_t3039462994  get_movable_1() const { return ___movable_1; }
	inline Rect_t3039462994 * get_address_of_movable_1() { return &___movable_1; }
	inline void set_movable_1(Rect_t3039462994  value)
	{
		___movable_1 = value;
	}

	inline static int32_t get_offset_of_words_2() { return static_cast<int32_t>(offsetof(DraggableObject_t1812188907, ___words_2)); }
	inline String_t* get_words_2() const { return ___words_2; }
	inline String_t** get_address_of_words_2() { return &___words_2; }
	inline void set_words_2(String_t* value)
	{
		___words_2 = value;
		Il2CppCodeGenWriteBarrier((&___words_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DraggableObject
struct DraggableObject_t1812188907_marshaled_pinvoke
{
	Rect_t3039462994  ___original_0;
	Rect_t3039462994  ___movable_1;
	char* ___words_2;
};
// Native definition for COM marshalling of DraggableObject
struct DraggableObject_t1812188907_marshaled_com
{
	Rect_t3039462994  ___original_0;
	Rect_t3039462994  ___movable_1;
	Il2CppChar* ___words_2;
};
#endif // DRAGGABLEOBJECT_T1812188907_H
#ifndef GNATSACTION_T2534711436_H
#define GNATSACTION_T2534711436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsAction
struct  GnatsAction_t2534711436 
{
public:
	// System.Int32 GnatsAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GnatsAction_t2534711436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSACTION_T2534711436_H
#ifndef BOATPLACEHOLDER_T3308939318_H
#define BOATPLACEHOLDER_T3308939318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatCutSceneManager/boatPlaceholder
struct  boatPlaceholder_t3308939318 
{
public:
	// System.Int32 BoatCutSceneManager/boatPlaceholder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(boatPlaceholder_t3308939318, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOATPLACEHOLDER_T3308939318_H
#ifndef BOATPLAYERSPAWNER_T2586798380_H
#define BOATPLAYERSPAWNER_T2586798380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatCutSceneManager/boatPlayerSpawner
struct  boatPlayerSpawner_t2586798380 
{
public:
	// System.Int32 BoatCutSceneManager/boatPlayerSpawner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(boatPlayerSpawner_t2586798380, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOATPLAYERSPAWNER_T2586798380_H
#ifndef ERELAXSTATE_T75127890_H
#define ERELAXSTATE_T75127890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelaxMuscles/ERelaxState
struct  ERelaxState_t75127890 
{
public:
	// System.Int32 RelaxMuscles/ERelaxState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ERelaxState_t75127890, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERELAXSTATE_T75127890_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef GNATSTATUS_T3561057505_H
#define GNATSTATUS_T3561057505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatStatus
struct  GnatStatus_t3561057505 
{
public:
	// System.Int32 GnatStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GnatStatus_t3561057505, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSTATUS_T3561057505_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef DRAGCALLBACK_T2341893212_H
#define DRAGCALLBACK_T2341893212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragObject/DragCallback
struct  DragCallback_t2341893212  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGCALLBACK_T2341893212_H
#ifndef POSTPROCESSINGPROFILE_T1367512122_H
#define POSTPROCESSINGPROFILE_T1367512122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingProfile
struct  PostProcessingProfile_t1367512122  : public ScriptableObject_t1804531341
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel UnityEngine.PostProcessing.PostProcessingProfile::debugViews
	BuiltinDebugViewsModel_t427307204 * ___debugViews_2;
	// UnityEngine.PostProcessing.FogModel UnityEngine.PostProcessing.PostProcessingProfile::fog
	FogModel_t449931294 * ___fog_3;
	// UnityEngine.PostProcessing.AntialiasingModel UnityEngine.PostProcessing.PostProcessingProfile::antialiasing
	AntialiasingModel_t2150172183 * ___antialiasing_4;
	// UnityEngine.PostProcessing.AmbientOcclusionModel UnityEngine.PostProcessing.PostProcessingProfile::ambientOcclusion
	AmbientOcclusionModel_t3851579516 * ___ambientOcclusion_5;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel UnityEngine.PostProcessing.PostProcessingProfile::screenSpaceReflection
	ScreenSpaceReflectionModel_t2630807469 * ___screenSpaceReflection_6;
	// UnityEngine.PostProcessing.DepthOfFieldModel UnityEngine.PostProcessing.PostProcessingProfile::depthOfField
	DepthOfFieldModel_t1419768887 * ___depthOfField_7;
	// UnityEngine.PostProcessing.MotionBlurModel UnityEngine.PostProcessing.PostProcessingProfile::motionBlur
	MotionBlurModel_t1157756927 * ___motionBlur_8;
	// UnityEngine.PostProcessing.EyeAdaptationModel UnityEngine.PostProcessing.PostProcessingProfile::eyeAdaptation
	EyeAdaptationModel_t4125836827 * ___eyeAdaptation_9;
	// UnityEngine.PostProcessing.BloomModel UnityEngine.PostProcessing.PostProcessingProfile::bloom
	BloomModel_t3872312760 * ___bloom_10;
	// UnityEngine.PostProcessing.ColorGradingModel UnityEngine.PostProcessing.PostProcessingProfile::colorGrading
	ColorGradingModel_t1893950678 * ___colorGrading_11;
	// UnityEngine.PostProcessing.UserLutModel UnityEngine.PostProcessing.PostProcessingProfile::userLut
	UserLutModel_t2644391680 * ___userLut_12;
	// UnityEngine.PostProcessing.ChromaticAberrationModel UnityEngine.PostProcessing.PostProcessingProfile::chromaticAberration
	ChromaticAberrationModel_t2533961311 * ___chromaticAberration_13;
	// UnityEngine.PostProcessing.GrainModel UnityEngine.PostProcessing.PostProcessingProfile::grain
	GrainModel_t3424148675 * ___grain_14;
	// UnityEngine.PostProcessing.VignetteModel UnityEngine.PostProcessing.PostProcessingProfile::vignette
	VignetteModel_t3042570574 * ___vignette_15;
	// UnityEngine.PostProcessing.DitheringModel UnityEngine.PostProcessing.PostProcessingProfile::dithering
	DitheringModel_t1266169160 * ___dithering_16;

public:
	inline static int32_t get_offset_of_debugViews_2() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___debugViews_2)); }
	inline BuiltinDebugViewsModel_t427307204 * get_debugViews_2() const { return ___debugViews_2; }
	inline BuiltinDebugViewsModel_t427307204 ** get_address_of_debugViews_2() { return &___debugViews_2; }
	inline void set_debugViews_2(BuiltinDebugViewsModel_t427307204 * value)
	{
		___debugViews_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugViews_2), value);
	}

	inline static int32_t get_offset_of_fog_3() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___fog_3)); }
	inline FogModel_t449931294 * get_fog_3() const { return ___fog_3; }
	inline FogModel_t449931294 ** get_address_of_fog_3() { return &___fog_3; }
	inline void set_fog_3(FogModel_t449931294 * value)
	{
		___fog_3 = value;
		Il2CppCodeGenWriteBarrier((&___fog_3), value);
	}

	inline static int32_t get_offset_of_antialiasing_4() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___antialiasing_4)); }
	inline AntialiasingModel_t2150172183 * get_antialiasing_4() const { return ___antialiasing_4; }
	inline AntialiasingModel_t2150172183 ** get_address_of_antialiasing_4() { return &___antialiasing_4; }
	inline void set_antialiasing_4(AntialiasingModel_t2150172183 * value)
	{
		___antialiasing_4 = value;
		Il2CppCodeGenWriteBarrier((&___antialiasing_4), value);
	}

	inline static int32_t get_offset_of_ambientOcclusion_5() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___ambientOcclusion_5)); }
	inline AmbientOcclusionModel_t3851579516 * get_ambientOcclusion_5() const { return ___ambientOcclusion_5; }
	inline AmbientOcclusionModel_t3851579516 ** get_address_of_ambientOcclusion_5() { return &___ambientOcclusion_5; }
	inline void set_ambientOcclusion_5(AmbientOcclusionModel_t3851579516 * value)
	{
		___ambientOcclusion_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOcclusion_5), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflection_6() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___screenSpaceReflection_6)); }
	inline ScreenSpaceReflectionModel_t2630807469 * get_screenSpaceReflection_6() const { return ___screenSpaceReflection_6; }
	inline ScreenSpaceReflectionModel_t2630807469 ** get_address_of_screenSpaceReflection_6() { return &___screenSpaceReflection_6; }
	inline void set_screenSpaceReflection_6(ScreenSpaceReflectionModel_t2630807469 * value)
	{
		___screenSpaceReflection_6 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflection_6), value);
	}

	inline static int32_t get_offset_of_depthOfField_7() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___depthOfField_7)); }
	inline DepthOfFieldModel_t1419768887 * get_depthOfField_7() const { return ___depthOfField_7; }
	inline DepthOfFieldModel_t1419768887 ** get_address_of_depthOfField_7() { return &___depthOfField_7; }
	inline void set_depthOfField_7(DepthOfFieldModel_t1419768887 * value)
	{
		___depthOfField_7 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_7), value);
	}

	inline static int32_t get_offset_of_motionBlur_8() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___motionBlur_8)); }
	inline MotionBlurModel_t1157756927 * get_motionBlur_8() const { return ___motionBlur_8; }
	inline MotionBlurModel_t1157756927 ** get_address_of_motionBlur_8() { return &___motionBlur_8; }
	inline void set_motionBlur_8(MotionBlurModel_t1157756927 * value)
	{
		___motionBlur_8 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_8), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_9() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___eyeAdaptation_9)); }
	inline EyeAdaptationModel_t4125836827 * get_eyeAdaptation_9() const { return ___eyeAdaptation_9; }
	inline EyeAdaptationModel_t4125836827 ** get_address_of_eyeAdaptation_9() { return &___eyeAdaptation_9; }
	inline void set_eyeAdaptation_9(EyeAdaptationModel_t4125836827 * value)
	{
		___eyeAdaptation_9 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_9), value);
	}

	inline static int32_t get_offset_of_bloom_10() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___bloom_10)); }
	inline BloomModel_t3872312760 * get_bloom_10() const { return ___bloom_10; }
	inline BloomModel_t3872312760 ** get_address_of_bloom_10() { return &___bloom_10; }
	inline void set_bloom_10(BloomModel_t3872312760 * value)
	{
		___bloom_10 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_10), value);
	}

	inline static int32_t get_offset_of_colorGrading_11() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___colorGrading_11)); }
	inline ColorGradingModel_t1893950678 * get_colorGrading_11() const { return ___colorGrading_11; }
	inline ColorGradingModel_t1893950678 ** get_address_of_colorGrading_11() { return &___colorGrading_11; }
	inline void set_colorGrading_11(ColorGradingModel_t1893950678 * value)
	{
		___colorGrading_11 = value;
		Il2CppCodeGenWriteBarrier((&___colorGrading_11), value);
	}

	inline static int32_t get_offset_of_userLut_12() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___userLut_12)); }
	inline UserLutModel_t2644391680 * get_userLut_12() const { return ___userLut_12; }
	inline UserLutModel_t2644391680 ** get_address_of_userLut_12() { return &___userLut_12; }
	inline void set_userLut_12(UserLutModel_t2644391680 * value)
	{
		___userLut_12 = value;
		Il2CppCodeGenWriteBarrier((&___userLut_12), value);
	}

	inline static int32_t get_offset_of_chromaticAberration_13() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___chromaticAberration_13)); }
	inline ChromaticAberrationModel_t2533961311 * get_chromaticAberration_13() const { return ___chromaticAberration_13; }
	inline ChromaticAberrationModel_t2533961311 ** get_address_of_chromaticAberration_13() { return &___chromaticAberration_13; }
	inline void set_chromaticAberration_13(ChromaticAberrationModel_t2533961311 * value)
	{
		___chromaticAberration_13 = value;
		Il2CppCodeGenWriteBarrier((&___chromaticAberration_13), value);
	}

	inline static int32_t get_offset_of_grain_14() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___grain_14)); }
	inline GrainModel_t3424148675 * get_grain_14() const { return ___grain_14; }
	inline GrainModel_t3424148675 ** get_address_of_grain_14() { return &___grain_14; }
	inline void set_grain_14(GrainModel_t3424148675 * value)
	{
		___grain_14 = value;
		Il2CppCodeGenWriteBarrier((&___grain_14), value);
	}

	inline static int32_t get_offset_of_vignette_15() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___vignette_15)); }
	inline VignetteModel_t3042570574 * get_vignette_15() const { return ___vignette_15; }
	inline VignetteModel_t3042570574 ** get_address_of_vignette_15() { return &___vignette_15; }
	inline void set_vignette_15(VignetteModel_t3042570574 * value)
	{
		___vignette_15 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_15), value);
	}

	inline static int32_t get_offset_of_dithering_16() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t1367512122, ___dithering_16)); }
	inline DitheringModel_t1266169160 * get_dithering_16() const { return ___dithering_16; }
	inline DitheringModel_t1266169160 ** get_address_of_dithering_16() { return &___dithering_16; }
	inline void set_dithering_16(DitheringModel_t1266169160 * value)
	{
		___dithering_16 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGPROFILE_T1367512122_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef BUTTONCALLBACKDELEGATE_T333906069_H
#define BUTTONCALLBACKDELEGATE_T333906069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonCallback/ButtonCallbackDelegate
struct  ButtonCallbackDelegate_t333906069  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCALLBACKDELEGATE_T333906069_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef L3SHIELDAGADEPRESSION_T1653586630_H
#define L3SHIELDAGADEPRESSION_T1653586630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3ShieldAgaDepression
struct  L3ShieldAgaDepression_t1653586630  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L3ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L3ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L3ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L3ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] L3ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] L3ShieldAgaDepression::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// System.Boolean[] L3ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_10;
	// UnityEngine.Color[] L3ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_11;
	// UnityEngine.Texture L3ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_12;
	// UnityEngine.Texture L3ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_13;
	// UnityEngine.GameObject L3ShieldAgaDepression::shieldScreen
	GameObject_t2557347079 * ___shieldScreen_14;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_displayName_10() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___displayName_10)); }
	inline BooleanU5BU5D_t698278498* get_displayName_10() const { return ___displayName_10; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_10() { return &___displayName_10; }
	inline void set_displayName_10(BooleanU5BU5D_t698278498* value)
	{
		___displayName_10 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_10), value);
	}

	inline static int32_t get_offset_of_displayColor_11() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___displayColor_11)); }
	inline ColorU5BU5D_t247935167* get_displayColor_11() const { return ___displayColor_11; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_11() { return &___displayColor_11; }
	inline void set_displayColor_11(ColorU5BU5D_t247935167* value)
	{
		___displayColor_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_11), value);
	}

	inline static int32_t get_offset_of_colourTexture_12() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___colourTexture_12)); }
	inline Texture_t2119925672 * get_colourTexture_12() const { return ___colourTexture_12; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_12() { return &___colourTexture_12; }
	inline void set_colourTexture_12(Texture_t2119925672 * value)
	{
		___colourTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_12), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_13() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___greyScaleTexture_13)); }
	inline Texture_t2119925672 * get_greyScaleTexture_13() const { return ___greyScaleTexture_13; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_13() { return &___greyScaleTexture_13; }
	inline void set_greyScaleTexture_13(Texture_t2119925672 * value)
	{
		___greyScaleTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_13), value);
	}

	inline static int32_t get_offset_of_shieldScreen_14() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630, ___shieldScreen_14)); }
	inline GameObject_t2557347079 * get_shieldScreen_14() const { return ___shieldScreen_14; }
	inline GameObject_t2557347079 ** get_address_of_shieldScreen_14() { return &___shieldScreen_14; }
	inline void set_shieldScreen_14(GameObject_t2557347079 * value)
	{
		___shieldScreen_14 = value;
		Il2CppCodeGenWriteBarrier((&___shieldScreen_14), value);
	}
};

struct L3ShieldAgaDepression_t1653586630_StaticFields
{
public:
	// System.Single L3ShieldAgaDepression::TextWidth
	float ___TextWidth_6;
	// System.Single L3ShieldAgaDepression::TextHeight
	float ___TextHeight_7;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(L3ShieldAgaDepression_t1653586630_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3SHIELDAGADEPRESSION_T1653586630_H
#ifndef L3HOWIFEELMINIGAME_T490880838_H
#define L3HOWIFEELMINIGAME_T490880838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3HowIFeelMiniGame
struct  L3HowIFeelMiniGame_t490880838  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3HowIFeelMiniGame::widhif
	GameObject_t2557347079 * ___widhif_2;

public:
	inline static int32_t get_offset_of_widhif_2() { return static_cast<int32_t>(offsetof(L3HowIFeelMiniGame_t490880838, ___widhif_2)); }
	inline GameObject_t2557347079 * get_widhif_2() const { return ___widhif_2; }
	inline GameObject_t2557347079 ** get_address_of_widhif_2() { return &___widhif_2; }
	inline void set_widhif_2(GameObject_t2557347079 * value)
	{
		___widhif_2 = value;
		Il2CppCodeGenWriteBarrier((&___widhif_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3HOWIFEELMINIGAME_T490880838_H
#ifndef L3DRAGWORDSHOWIREACT_T1761835085_H
#define L3DRAGWORDSHOWIREACT_T1761835085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragWordsHowIReact
struct  L3DragWordsHowIReact_t1761835085  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragWordsHowIReact::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragWordsHowIReact::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.String L3DragWordsHowIReact::m_conversationBoxText
	String_t* ___m_conversationBoxText_4;
	// UnityEngine.Vector2[] L3DragWordsHowIReact::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_7;
	// UnityEngine.Rect[] L3DragWordsHowIReact::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_8;
	// System.String[] L3DragWordsHowIReact::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_9;
	// System.String[] L3DragWordsHowIReact::m_descriptions
	StringU5BU5D_t2511808107* ___m_descriptions_10;
	// UnityEngine.Rect[] L3DragWordsHowIReact::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// System.String[] L3DragWordsHowIReact::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_14;
	// UnityEngine.Rect[] L3DragWordsHowIReact::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragWordsHowIReact::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragWordsHowIReact::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragWordsHowIReact::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragWordsHowIReact::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragWordsHowIReact::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragWordsHowIReact::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragWordsHowIReact::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragWordsHowIReact::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;
	// UnityEngine.GameObject L3DragWordsHowIReact::listScreen
	GameObject_t2557347079 * ___listScreen_24;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_4() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_conversationBoxText_4)); }
	inline String_t* get_m_conversationBoxText_4() const { return ___m_conversationBoxText_4; }
	inline String_t** get_address_of_m_conversationBoxText_4() { return &___m_conversationBoxText_4; }
	inline void set_m_conversationBoxText_4(String_t* value)
	{
		___m_conversationBoxText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_4), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_7() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_KeywordDimensions_7)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_7() const { return ___m_KeywordDimensions_7; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_7() { return &___m_KeywordDimensions_7; }
	inline void set_m_KeywordDimensions_7(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_7), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_8() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_myOriRect_8)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_8() const { return ___m_myOriRect_8; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_8() { return &___m_myOriRect_8; }
	inline void set_m_myOriRect_8(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_8), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_9() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_keyWordText_9)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_9() const { return ___m_keyWordText_9; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_9() { return &___m_keyWordText_9; }
	inline void set_m_keyWordText_9(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_9), value);
	}

	inline static int32_t get_offset_of_m_descriptions_10() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_descriptions_10)); }
	inline StringU5BU5D_t2511808107* get_m_descriptions_10() const { return ___m_descriptions_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_descriptions_10() { return &___m_descriptions_10; }
	inline void set_m_descriptions_10(StringU5BU5D_t2511808107* value)
	{
		___m_descriptions_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_descriptions_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationText_14() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_14() const { return ___m_destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_14() { return &___m_destinationText_14; }
	inline void set_m_destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_14), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}

	inline static int32_t get_offset_of_listScreen_24() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085, ___listScreen_24)); }
	inline GameObject_t2557347079 * get_listScreen_24() const { return ___listScreen_24; }
	inline GameObject_t2557347079 ** get_address_of_listScreen_24() { return &___listScreen_24; }
	inline void set_listScreen_24(GameObject_t2557347079 * value)
	{
		___listScreen_24 = value;
		Il2CppCodeGenWriteBarrier((&___listScreen_24), value);
	}
};

struct L3DragWordsHowIReact_t1761835085_StaticFields
{
public:
	// System.Single L3DragWordsHowIReact::m_keywordWidth
	float ___m_keywordWidth_5;
	// System.Single L3DragWordsHowIReact::m_keywordHeight
	float ___m_keywordHeight_6;
	// System.Single L3DragWordsHowIReact::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragWordsHowIReact::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3DragWordsHowIReact::<>f__switch$map9
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map9_25;

public:
	inline static int32_t get_offset_of_m_keywordWidth_5() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085_StaticFields, ___m_keywordWidth_5)); }
	inline float get_m_keywordWidth_5() const { return ___m_keywordWidth_5; }
	inline float* get_address_of_m_keywordWidth_5() { return &___m_keywordWidth_5; }
	inline void set_m_keywordWidth_5(float value)
	{
		___m_keywordWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_6() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085_StaticFields, ___m_keywordHeight_6)); }
	inline float get_m_keywordHeight_6() const { return ___m_keywordHeight_6; }
	inline float* get_address_of_m_keywordHeight_6() { return &___m_keywordHeight_6; }
	inline void set_m_keywordHeight_6(float value)
	{
		___m_keywordHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_25() { return static_cast<int32_t>(offsetof(L3DragWordsHowIReact_t1761835085_StaticFields, ___U3CU3Ef__switchU24map9_25)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map9_25() const { return ___U3CU3Ef__switchU24map9_25; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map9_25() { return &___U3CU3Ef__switchU24map9_25; }
	inline void set_U3CU3Ef__switchU24map9_25(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map9_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGWORDSHOWIREACT_T1761835085_H
#ifndef L3DRAGDROPWORDS5_T114919130_H
#define L3DRAGDROPWORDS5_T114919130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragDropWords5
struct  L3DragDropWords5_t114919130  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragDropWords5::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragDropWords5::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L3DragDropWords5::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L3DragDropWords5::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L3DragDropWords5::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L3DragDropWords5::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L3DragDropWords5::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L3DragDropWords5::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// UnityEngine.Rect[] L3DragDropWords5::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragDropWords5::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragDropWords5::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragDropWords5::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragDropWords5::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragDropWords5::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragDropWords5::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragDropWords5::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragDropWords5::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L3DragDropWords5_t114919130_StaticFields
{
public:
	// System.Single L3DragDropWords5::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L3DragDropWords5::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L3DragDropWords5::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragDropWords5::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.String[] L3DragDropWords5::m_L3destinationText
	StringU5BU5D_t2511808107* ___m_L3destinationText_14;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_L3destinationText_14() { return static_cast<int32_t>(offsetof(L3DragDropWords5_t114919130_StaticFields, ___m_L3destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_L3destinationText_14() const { return ___m_L3destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L3destinationText_14() { return &___m_L3destinationText_14; }
	inline void set_m_L3destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_L3destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_L3destinationText_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGDROPWORDS5_T114919130_H
#ifndef L3DRAGDROPWORDS4_T2882406281_H
#define L3DRAGDROPWORDS4_T2882406281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragDropWords4
struct  L3DragDropWords4_t2882406281  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragDropWords4::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragDropWords4::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L3DragDropWords4::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L3DragDropWords4::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L3DragDropWords4::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L3DragDropWords4::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L3DragDropWords4::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L3DragDropWords4::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// UnityEngine.Rect[] L3DragDropWords4::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragDropWords4::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragDropWords4::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragDropWords4::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragDropWords4::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragDropWords4::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragDropWords4::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragDropWords4::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragDropWords4::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L3DragDropWords4_t2882406281_StaticFields
{
public:
	// System.Single L3DragDropWords4::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L3DragDropWords4::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L3DragDropWords4::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragDropWords4::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.String[] L3DragDropWords4::m_L3destinationText
	StringU5BU5D_t2511808107* ___m_L3destinationText_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3DragDropWords4::<>f__switch$map8
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map8_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_L3destinationText_14() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___m_L3destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_L3destinationText_14() const { return ___m_L3destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L3destinationText_14() { return &___m_L3destinationText_14; }
	inline void set_m_L3destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_L3destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_L3destinationText_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_24() { return static_cast<int32_t>(offsetof(L3DragDropWords4_t2882406281_StaticFields, ___U3CU3Ef__switchU24map8_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map8_24() const { return ___U3CU3Ef__switchU24map8_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map8_24() { return &___U3CU3Ef__switchU24map8_24; }
	inline void set_U3CU3Ef__switchU24map8_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map8_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGDROPWORDS4_T2882406281_H
#ifndef L3DRAGDROPWORDS3_T375893153_H
#define L3DRAGDROPWORDS3_T375893153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragDropWords3
struct  L3DragDropWords3_t375893153  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragDropWords3::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragDropWords3::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L3DragDropWords3::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L3DragDropWords3::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L3DragDropWords3::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L3DragDropWords3::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L3DragDropWords3::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L3DragDropWords3::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// UnityEngine.Rect[] L3DragDropWords3::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragDropWords3::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragDropWords3::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragDropWords3::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragDropWords3::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragDropWords3::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragDropWords3::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragDropWords3::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragDropWords3::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L3DragDropWords3_t375893153_StaticFields
{
public:
	// System.Single L3DragDropWords3::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L3DragDropWords3::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L3DragDropWords3::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragDropWords3::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.String[] L3DragDropWords3::m_L3destinationText
	StringU5BU5D_t2511808107* ___m_L3destinationText_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3DragDropWords3::<>f__switch$map7
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map7_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_L3destinationText_14() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___m_L3destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_L3destinationText_14() const { return ___m_L3destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L3destinationText_14() { return &___m_L3destinationText_14; }
	inline void set_m_L3destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_L3destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_L3destinationText_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_24() { return static_cast<int32_t>(offsetof(L3DragDropWords3_t375893153_StaticFields, ___U3CU3Ef__switchU24map7_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map7_24() const { return ___U3CU3Ef__switchU24map7_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map7_24() { return &___U3CU3Ef__switchU24map7_24; }
	inline void set_U3CU3Ef__switchU24map7_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map7_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGDROPWORDS3_T375893153_H
#ifndef L3DRAGDROPWORDS2_T3194046937_H
#define L3DRAGDROPWORDS2_T3194046937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragDropWords2
struct  L3DragDropWords2_t3194046937  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragDropWords2::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragDropWords2::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L3DragDropWords2::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L3DragDropWords2::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L3DragDropWords2::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L3DragDropWords2::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L3DragDropWords2::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L3DragDropWords2::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// System.String[] L3DragDropWords2::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_14;
	// UnityEngine.Rect[] L3DragDropWords2::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragDropWords2::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragDropWords2::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragDropWords2::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragDropWords2::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragDropWords2::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragDropWords2::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragDropWords2::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragDropWords2::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationText_14() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_14() const { return ___m_destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_14() { return &___m_destinationText_14; }
	inline void set_m_destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_14), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L3DragDropWords2_t3194046937_StaticFields
{
public:
	// System.Single L3DragDropWords2::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L3DragDropWords2::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L3DragDropWords2::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragDropWords2::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3DragDropWords2::<>f__switch$map6
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map6_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_24() { return static_cast<int32_t>(offsetof(L3DragDropWords2_t3194046937_StaticFields, ___U3CU3Ef__switchU24map6_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map6_24() const { return ___U3CU3Ef__switchU24map6_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map6_24() { return &___U3CU3Ef__switchU24map6_24; }
	inline void set_U3CU3Ef__switchU24map6_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map6_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGDROPWORDS2_T3194046937_H
#ifndef L3DRAGDROPWORDS_T3281039464_H
#define L3DRAGDROPWORDS_T3281039464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragDropWords
struct  L3DragDropWords_t3281039464  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L3DragDropWords::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L3DragDropWords::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L3DragDropWords::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L3DragDropWords::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L3DragDropWords::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L3DragDropWords::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L3DragDropWords::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L3DragDropWords::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// System.String[] L3DragDropWords::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_14;
	// UnityEngine.Rect[] L3DragDropWords::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L3DragDropWords::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L3DragDropWords::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L3DragDropWords::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L3DragDropWords::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L3DragDropWords::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L3DragDropWords::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L3DragDropWords::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L3DragDropWords::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationText_14() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_14() const { return ___m_destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_14() { return &___m_destinationText_14; }
	inline void set_m_destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_14), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L3DragDropWords_t3281039464_StaticFields
{
public:
	// System.Single L3DragDropWords::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L3DragDropWords::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L3DragDropWords::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L3DragDropWords::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3DragDropWords::<>f__switch$map5
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map5_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_24() { return static_cast<int32_t>(offsetof(L3DragDropWords_t3281039464_StaticFields, ___U3CU3Ef__switchU24map5_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map5_24() const { return ___U3CU3Ef__switchU24map5_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map5_24() { return &___U3CU3Ef__switchU24map5_24; }
	inline void set_U3CU3Ef__switchU24map5_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGDROPWORDS_T3281039464_H
#ifndef L3DRAGANDDROPBLINC_ITEM_T2347719852_H
#define L3DRAGANDDROPBLINC_ITEM_T2347719852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragAndDropBlinc_Item
struct  L3DragAndDropBlinc_Item_t2347719852  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3DragAndDropBlinc_Item::oMinigameManager
	GameObject_t2557347079 * ___oMinigameManager_2;

public:
	inline static int32_t get_offset_of_oMinigameManager_2() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_Item_t2347719852, ___oMinigameManager_2)); }
	inline GameObject_t2557347079 * get_oMinigameManager_2() const { return ___oMinigameManager_2; }
	inline GameObject_t2557347079 ** get_address_of_oMinigameManager_2() { return &___oMinigameManager_2; }
	inline void set_oMinigameManager_2(GameObject_t2557347079 * value)
	{
		___oMinigameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___oMinigameManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGANDDROPBLINC_ITEM_T2347719852_H
#ifndef L3DRAGANDDROPBLINC_INVENTORY_T2077180760_H
#define L3DRAGANDDROPBLINC_INVENTORY_T2077180760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragAndDropBlinc_Inventory
struct  L3DragAndDropBlinc_Inventory_t2077180760  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3DragAndDropBlinc_Inventory::oNextButton
	GameObject_t2557347079 * ___oNextButton_2;
	// System.Int32 L3DragAndDropBlinc_Inventory::nItems
	int32_t ___nItems_3;

public:
	inline static int32_t get_offset_of_oNextButton_2() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_Inventory_t2077180760, ___oNextButton_2)); }
	inline GameObject_t2557347079 * get_oNextButton_2() const { return ___oNextButton_2; }
	inline GameObject_t2557347079 ** get_address_of_oNextButton_2() { return &___oNextButton_2; }
	inline void set_oNextButton_2(GameObject_t2557347079 * value)
	{
		___oNextButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___oNextButton_2), value);
	}

	inline static int32_t get_offset_of_nItems_3() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_Inventory_t2077180760, ___nItems_3)); }
	inline int32_t get_nItems_3() const { return ___nItems_3; }
	inline int32_t* get_address_of_nItems_3() { return &___nItems_3; }
	inline void set_nItems_3(int32_t value)
	{
		___nItems_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGANDDROPBLINC_INVENTORY_T2077180760_H
#ifndef L3DRAGANDDROPBLINC_DRAGHANDLER_T2105775540_H
#define L3DRAGANDDROPBLINC_DRAGHANDLER_T2105775540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragAndDropBlinc_DragHandler
struct  L3DragAndDropBlinc_DragHandler_t2105775540  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 L3DragAndDropBlinc_DragHandler::startPosittion
	Vector3_t1986933152  ___startPosittion_3;
	// UnityEngine.UI.Text L3DragAndDropBlinc_DragHandler::oText
	Text_t1790657652 * ___oText_4;
	// UnityEngine.Transform L3DragAndDropBlinc_DragHandler::startParent
	Transform_t362059596 * ___startParent_5;

public:
	inline static int32_t get_offset_of_startPosittion_3() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_DragHandler_t2105775540, ___startPosittion_3)); }
	inline Vector3_t1986933152  get_startPosittion_3() const { return ___startPosittion_3; }
	inline Vector3_t1986933152 * get_address_of_startPosittion_3() { return &___startPosittion_3; }
	inline void set_startPosittion_3(Vector3_t1986933152  value)
	{
		___startPosittion_3 = value;
	}

	inline static int32_t get_offset_of_oText_4() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_DragHandler_t2105775540, ___oText_4)); }
	inline Text_t1790657652 * get_oText_4() const { return ___oText_4; }
	inline Text_t1790657652 ** get_address_of_oText_4() { return &___oText_4; }
	inline void set_oText_4(Text_t1790657652 * value)
	{
		___oText_4 = value;
		Il2CppCodeGenWriteBarrier((&___oText_4), value);
	}

	inline static int32_t get_offset_of_startParent_5() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_DragHandler_t2105775540, ___startParent_5)); }
	inline Transform_t362059596 * get_startParent_5() const { return ___startParent_5; }
	inline Transform_t362059596 ** get_address_of_startParent_5() { return &___startParent_5; }
	inline void set_startParent_5(Transform_t362059596 * value)
	{
		___startParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___startParent_5), value);
	}
};

struct L3DragAndDropBlinc_DragHandler_t2105775540_StaticFields
{
public:
	// UnityEngine.GameObject L3DragAndDropBlinc_DragHandler::itemBeingDregged
	GameObject_t2557347079 * ___itemBeingDregged_2;

public:
	inline static int32_t get_offset_of_itemBeingDregged_2() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_DragHandler_t2105775540_StaticFields, ___itemBeingDregged_2)); }
	inline GameObject_t2557347079 * get_itemBeingDregged_2() const { return ___itemBeingDregged_2; }
	inline GameObject_t2557347079 ** get_address_of_itemBeingDregged_2() { return &___itemBeingDregged_2; }
	inline void set_itemBeingDregged_2(GameObject_t2557347079 * value)
	{
		___itemBeingDregged_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemBeingDregged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGANDDROPBLINC_DRAGHANDLER_T2105775540_H
#ifndef L3DRAGANDDROPBLINC_CANVAS_T4294777750_H
#define L3DRAGANDDROPBLINC_CANVAS_T4294777750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragAndDropBlinc_Canvas
struct  L3DragAndDropBlinc_Canvas_t4294777750  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGANDDROPBLINC_CANVAS_T4294777750_H
#ifndef L3DRAGANDDROPBLINC_T4110504062_H
#define L3DRAGANDDROPBLINC_T4110504062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DragAndDropBlinc
struct  L3DragAndDropBlinc_t4110504062  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L3DragAndDropBlinc::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String L3DragAndDropBlinc::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// DraggableObject[] L3DragAndDropBlinc::m_letterRects
	DraggableObjectU5BU5D_t2511386890* ___m_letterRects_7;
	// System.Boolean[] L3DragAndDropBlinc::m_placed
	BooleanU5BU5D_t698278498* ___m_placed_8;
	// UnityEngine.Rect[] L3DragAndDropBlinc::m_destinations
	RectU5BU5D_t141872167* ___m_destinations_9;
	// UnityEngine.Vector2[] L3DragAndDropBlinc::m_finalLetterPos
	Vector2U5BU5D_t1220531434* ___m_finalLetterPos_10;
	// System.Int32 L3DragAndDropBlinc::m_currentDragIndex
	int32_t ___m_currentDragIndex_11;
	// System.Boolean L3DragAndDropBlinc::m_dragging
	bool ___m_dragging_12;
	// UnityEngine.Texture L3DragAndDropBlinc::m_letterB
	Texture_t2119925672 * ___m_letterB_13;
	// UnityEngine.Texture L3DragAndDropBlinc::m_letterL
	Texture_t2119925672 * ___m_letterL_14;
	// UnityEngine.Texture L3DragAndDropBlinc::m_letterI
	Texture_t2119925672 * ___m_letterI_15;
	// UnityEngine.Texture L3DragAndDropBlinc::m_letterN
	Texture_t2119925672 * ___m_letterN_16;
	// UnityEngine.Texture L3DragAndDropBlinc::m_letterC
	Texture_t2119925672 * ___m_letterC_17;
	// UnityEngine.Texture L3DragAndDropBlinc::m_wordBLINC
	Texture_t2119925672 * ___m_wordBLINC_18;
	// UnityEngine.Texture2D L3DragAndDropBlinc::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_19;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_letterRects_7() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterRects_7)); }
	inline DraggableObjectU5BU5D_t2511386890* get_m_letterRects_7() const { return ___m_letterRects_7; }
	inline DraggableObjectU5BU5D_t2511386890** get_address_of_m_letterRects_7() { return &___m_letterRects_7; }
	inline void set_m_letterRects_7(DraggableObjectU5BU5D_t2511386890* value)
	{
		___m_letterRects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterRects_7), value);
	}

	inline static int32_t get_offset_of_m_placed_8() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_placed_8)); }
	inline BooleanU5BU5D_t698278498* get_m_placed_8() const { return ___m_placed_8; }
	inline BooleanU5BU5D_t698278498** get_address_of_m_placed_8() { return &___m_placed_8; }
	inline void set_m_placed_8(BooleanU5BU5D_t698278498* value)
	{
		___m_placed_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_placed_8), value);
	}

	inline static int32_t get_offset_of_m_destinations_9() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_destinations_9)); }
	inline RectU5BU5D_t141872167* get_m_destinations_9() const { return ___m_destinations_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinations_9() { return &___m_destinations_9; }
	inline void set_m_destinations_9(RectU5BU5D_t141872167* value)
	{
		___m_destinations_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinations_9), value);
	}

	inline static int32_t get_offset_of_m_finalLetterPos_10() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_finalLetterPos_10)); }
	inline Vector2U5BU5D_t1220531434* get_m_finalLetterPos_10() const { return ___m_finalLetterPos_10; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_finalLetterPos_10() { return &___m_finalLetterPos_10; }
	inline void set_m_finalLetterPos_10(Vector2U5BU5D_t1220531434* value)
	{
		___m_finalLetterPos_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_finalLetterPos_10), value);
	}

	inline static int32_t get_offset_of_m_currentDragIndex_11() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_currentDragIndex_11)); }
	inline int32_t get_m_currentDragIndex_11() const { return ___m_currentDragIndex_11; }
	inline int32_t* get_address_of_m_currentDragIndex_11() { return &___m_currentDragIndex_11; }
	inline void set_m_currentDragIndex_11(int32_t value)
	{
		___m_currentDragIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_dragging_12() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_dragging_12)); }
	inline bool get_m_dragging_12() const { return ___m_dragging_12; }
	inline bool* get_address_of_m_dragging_12() { return &___m_dragging_12; }
	inline void set_m_dragging_12(bool value)
	{
		___m_dragging_12 = value;
	}

	inline static int32_t get_offset_of_m_letterB_13() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterB_13)); }
	inline Texture_t2119925672 * get_m_letterB_13() const { return ___m_letterB_13; }
	inline Texture_t2119925672 ** get_address_of_m_letterB_13() { return &___m_letterB_13; }
	inline void set_m_letterB_13(Texture_t2119925672 * value)
	{
		___m_letterB_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterB_13), value);
	}

	inline static int32_t get_offset_of_m_letterL_14() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterL_14)); }
	inline Texture_t2119925672 * get_m_letterL_14() const { return ___m_letterL_14; }
	inline Texture_t2119925672 ** get_address_of_m_letterL_14() { return &___m_letterL_14; }
	inline void set_m_letterL_14(Texture_t2119925672 * value)
	{
		___m_letterL_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterL_14), value);
	}

	inline static int32_t get_offset_of_m_letterI_15() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterI_15)); }
	inline Texture_t2119925672 * get_m_letterI_15() const { return ___m_letterI_15; }
	inline Texture_t2119925672 ** get_address_of_m_letterI_15() { return &___m_letterI_15; }
	inline void set_m_letterI_15(Texture_t2119925672 * value)
	{
		___m_letterI_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterI_15), value);
	}

	inline static int32_t get_offset_of_m_letterN_16() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterN_16)); }
	inline Texture_t2119925672 * get_m_letterN_16() const { return ___m_letterN_16; }
	inline Texture_t2119925672 ** get_address_of_m_letterN_16() { return &___m_letterN_16; }
	inline void set_m_letterN_16(Texture_t2119925672 * value)
	{
		___m_letterN_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterN_16), value);
	}

	inline static int32_t get_offset_of_m_letterC_17() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_letterC_17)); }
	inline Texture_t2119925672 * get_m_letterC_17() const { return ___m_letterC_17; }
	inline Texture_t2119925672 ** get_address_of_m_letterC_17() { return &___m_letterC_17; }
	inline void set_m_letterC_17(Texture_t2119925672 * value)
	{
		___m_letterC_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_letterC_17), value);
	}

	inline static int32_t get_offset_of_m_wordBLINC_18() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_wordBLINC_18)); }
	inline Texture_t2119925672 * get_m_wordBLINC_18() const { return ___m_wordBLINC_18; }
	inline Texture_t2119925672 ** get_address_of_m_wordBLINC_18() { return &___m_wordBLINC_18; }
	inline void set_m_wordBLINC_18(Texture_t2119925672 * value)
	{
		___m_wordBLINC_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_wordBLINC_18), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_19() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062, ___m_tConversationBackground_19)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_19() const { return ___m_tConversationBackground_19; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_19() { return &___m_tConversationBackground_19; }
	inline void set_m_tConversationBackground_19(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_19), value);
	}
};

struct L3DragAndDropBlinc_t4110504062_StaticFields
{
public:
	// System.Int32 L3DragAndDropBlinc::ScreenHeight_Factor
	int32_t ___ScreenHeight_Factor_3;
	// System.Int32 L3DragAndDropBlinc::ScreenWidth_Factor
	int32_t ___ScreenWidth_Factor_4;
	// System.Single L3DragAndDropBlinc::m_collisiondimension
	float ___m_collisiondimension_6;

public:
	inline static int32_t get_offset_of_ScreenHeight_Factor_3() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062_StaticFields, ___ScreenHeight_Factor_3)); }
	inline int32_t get_ScreenHeight_Factor_3() const { return ___ScreenHeight_Factor_3; }
	inline int32_t* get_address_of_ScreenHeight_Factor_3() { return &___ScreenHeight_Factor_3; }
	inline void set_ScreenHeight_Factor_3(int32_t value)
	{
		___ScreenHeight_Factor_3 = value;
	}

	inline static int32_t get_offset_of_ScreenWidth_Factor_4() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062_StaticFields, ___ScreenWidth_Factor_4)); }
	inline int32_t get_ScreenWidth_Factor_4() const { return ___ScreenWidth_Factor_4; }
	inline int32_t* get_address_of_ScreenWidth_Factor_4() { return &___ScreenWidth_Factor_4; }
	inline void set_ScreenWidth_Factor_4(int32_t value)
	{
		___ScreenWidth_Factor_4 = value;
	}

	inline static int32_t get_offset_of_m_collisiondimension_6() { return static_cast<int32_t>(offsetof(L3DragAndDropBlinc_t4110504062_StaticFields, ___m_collisiondimension_6)); }
	inline float get_m_collisiondimension_6() const { return ___m_collisiondimension_6; }
	inline float* get_address_of_m_collisiondimension_6() { return &___m_collisiondimension_6; }
	inline void set_m_collisiondimension_6(float value)
	{
		___m_collisiondimension_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DRAGANDDROPBLINC_T4110504062_H
#ifndef L2SHIELDAGADEPRESSION_T2978038194_H
#define L2SHIELDAGADEPRESSION_T2978038194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2ShieldAgaDepression
struct  L2ShieldAgaDepression_t2978038194  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L2ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L2ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L2ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L2ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// ShieldMenu L2ShieldAgaDepression::menuCanvasObject
	ShieldMenu_t3436809847 * ___menuCanvasObject_6;
	// System.String[] L2ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_9;
	// System.Boolean[] L2ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_10;
	// UnityEngine.Color[] L2ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_11;
	// UnityEngine.Texture L2ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_12;
	// UnityEngine.Texture L2ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_13;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_menuCanvasObject_6() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___menuCanvasObject_6)); }
	inline ShieldMenu_t3436809847 * get_menuCanvasObject_6() const { return ___menuCanvasObject_6; }
	inline ShieldMenu_t3436809847 ** get_address_of_menuCanvasObject_6() { return &___menuCanvasObject_6; }
	inline void set_menuCanvasObject_6(ShieldMenu_t3436809847 * value)
	{
		___menuCanvasObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___menuCanvasObject_6), value);
	}

	inline static int32_t get_offset_of_childObjectNames_9() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___childObjectNames_9)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_9() const { return ___childObjectNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_9() { return &___childObjectNames_9; }
	inline void set_childObjectNames_9(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_9), value);
	}

	inline static int32_t get_offset_of_displayName_10() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___displayName_10)); }
	inline BooleanU5BU5D_t698278498* get_displayName_10() const { return ___displayName_10; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_10() { return &___displayName_10; }
	inline void set_displayName_10(BooleanU5BU5D_t698278498* value)
	{
		___displayName_10 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_10), value);
	}

	inline static int32_t get_offset_of_displayColor_11() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___displayColor_11)); }
	inline ColorU5BU5D_t247935167* get_displayColor_11() const { return ___displayColor_11; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_11() { return &___displayColor_11; }
	inline void set_displayColor_11(ColorU5BU5D_t247935167* value)
	{
		___displayColor_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_11), value);
	}

	inline static int32_t get_offset_of_colourTexture_12() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___colourTexture_12)); }
	inline Texture_t2119925672 * get_colourTexture_12() const { return ___colourTexture_12; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_12() { return &___colourTexture_12; }
	inline void set_colourTexture_12(Texture_t2119925672 * value)
	{
		___colourTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_12), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_13() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194, ___greyScaleTexture_13)); }
	inline Texture_t2119925672 * get_greyScaleTexture_13() const { return ___greyScaleTexture_13; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_13() { return &___greyScaleTexture_13; }
	inline void set_greyScaleTexture_13(Texture_t2119925672 * value)
	{
		___greyScaleTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_13), value);
	}
};

struct L2ShieldAgaDepression_t2978038194_StaticFields
{
public:
	// System.Single L2ShieldAgaDepression::TextWidth
	float ___TextWidth_7;
	// System.Single L2ShieldAgaDepression::TextHeight
	float ___TextHeight_8;

public:
	inline static int32_t get_offset_of_TextWidth_7() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194_StaticFields, ___TextWidth_7)); }
	inline float get_TextWidth_7() const { return ___TextWidth_7; }
	inline float* get_address_of_TextWidth_7() { return &___TextWidth_7; }
	inline void set_TextWidth_7(float value)
	{
		___TextWidth_7 = value;
	}

	inline static int32_t get_offset_of_TextHeight_8() { return static_cast<int32_t>(offsetof(L2ShieldAgaDepression_t2978038194_StaticFields, ___TextHeight_8)); }
	inline float get_TextHeight_8() const { return ___TextHeight_8; }
	inline float* get_address_of_TextHeight_8() { return &___TextHeight_8; }
	inline void set_TextHeight_8(float value)
	{
		___TextHeight_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2SHIELDAGADEPRESSION_T2978038194_H
#ifndef L2RELEXYOURMUSEL_T1858011544_H
#define L2RELEXYOURMUSEL_T1858011544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2RelexYourMusel
struct  L2RelexYourMusel_t1858011544  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L2RelexYourMusel::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D L2RelexYourMusel::m_tBodyTexture0
	Texture2D_t3063074017 * ___m_tBodyTexture0_3;
	// UnityEngine.Texture2D L2RelexYourMusel::m_tArmTexture1
	Texture2D_t3063074017 * ___m_tArmTexture1_4;
	// UnityEngine.Texture2D L2RelexYourMusel::m_tLegsTexture2
	Texture2D_t3063074017 * ___m_tLegsTexture2_5;
	// UnityEngine.Texture2D L2RelexYourMusel::m_tChestTexture3
	Texture2D_t3063074017 * ___m_tChestTexture3_6;
	// TalkScenes L2RelexYourMusel::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_7;
	// UnityEngine.Texture2D L2RelexYourMusel::m_displayedTexture
	Texture2D_t3063074017 * ___m_displayedTexture_8;
	// System.Single L2RelexYourMusel::m_fStartTime
	float ___m_fStartTime_9;
	// System.String L2RelexYourMusel::m_sTitleName
	String_t* ___m_sTitleName_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_tBodyTexture0_3() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_tBodyTexture0_3)); }
	inline Texture2D_t3063074017 * get_m_tBodyTexture0_3() const { return ___m_tBodyTexture0_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBodyTexture0_3() { return &___m_tBodyTexture0_3; }
	inline void set_m_tBodyTexture0_3(Texture2D_t3063074017 * value)
	{
		___m_tBodyTexture0_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBodyTexture0_3), value);
	}

	inline static int32_t get_offset_of_m_tArmTexture1_4() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_tArmTexture1_4)); }
	inline Texture2D_t3063074017 * get_m_tArmTexture1_4() const { return ___m_tArmTexture1_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tArmTexture1_4() { return &___m_tArmTexture1_4; }
	inline void set_m_tArmTexture1_4(Texture2D_t3063074017 * value)
	{
		___m_tArmTexture1_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tArmTexture1_4), value);
	}

	inline static int32_t get_offset_of_m_tLegsTexture2_5() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_tLegsTexture2_5)); }
	inline Texture2D_t3063074017 * get_m_tLegsTexture2_5() const { return ___m_tLegsTexture2_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tLegsTexture2_5() { return &___m_tLegsTexture2_5; }
	inline void set_m_tLegsTexture2_5(Texture2D_t3063074017 * value)
	{
		___m_tLegsTexture2_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tLegsTexture2_5), value);
	}

	inline static int32_t get_offset_of_m_tChestTexture3_6() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_tChestTexture3_6)); }
	inline Texture2D_t3063074017 * get_m_tChestTexture3_6() const { return ___m_tChestTexture3_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_tChestTexture3_6() { return &___m_tChestTexture3_6; }
	inline void set_m_tChestTexture3_6(Texture2D_t3063074017 * value)
	{
		___m_tChestTexture3_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_tChestTexture3_6), value);
	}

	inline static int32_t get_offset_of_m_talkScene_7() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_talkScene_7)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_7() const { return ___m_talkScene_7; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_7() { return &___m_talkScene_7; }
	inline void set_m_talkScene_7(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_7), value);
	}

	inline static int32_t get_offset_of_m_displayedTexture_8() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_displayedTexture_8)); }
	inline Texture2D_t3063074017 * get_m_displayedTexture_8() const { return ___m_displayedTexture_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_displayedTexture_8() { return &___m_displayedTexture_8; }
	inline void set_m_displayedTexture_8(Texture2D_t3063074017 * value)
	{
		___m_displayedTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_displayedTexture_8), value);
	}

	inline static int32_t get_offset_of_m_fStartTime_9() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_fStartTime_9)); }
	inline float get_m_fStartTime_9() const { return ___m_fStartTime_9; }
	inline float* get_address_of_m_fStartTime_9() { return &___m_fStartTime_9; }
	inline void set_m_fStartTime_9(float value)
	{
		___m_fStartTime_9 = value;
	}

	inline static int32_t get_offset_of_m_sTitleName_10() { return static_cast<int32_t>(offsetof(L2RelexYourMusel_t1858011544, ___m_sTitleName_10)); }
	inline String_t* get_m_sTitleName_10() const { return ___m_sTitleName_10; }
	inline String_t** get_address_of_m_sTitleName_10() { return &___m_sTitleName_10; }
	inline void set_m_sTitleName_10(String_t* value)
	{
		___m_sTitleName_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTitleName_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2RELEXYOURMUSEL_T1858011544_H
#ifndef L2RELAXYOURMUSCLES_T1973662748_H
#define L2RELAXYOURMUSCLES_T1973662748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2RelaxYourMuscles
struct  L2RelaxYourMuscles_t1973662748  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L2RelaxYourMuscles::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D[] L2RelaxYourMuscles::m_tMuscles
	Texture2DU5BU5D_t3304433276* ___m_tMuscles_3;
	// System.Single L2RelaxYourMuscles::m_fTimer
	float ___m_fTimer_4;
	// System.String[] L2RelaxYourMuscles::m_strNames
	StringU5BU5D_t2511808107* ___m_strNames_5;
	// System.Int32 L2RelaxYourMuscles::m_iCurrentImage
	int32_t ___m_iCurrentImage_6;
	// UnityEngine.GameObject L2RelaxYourMuscles::menu
	GameObject_t2557347079 * ___menu_7;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_tMuscles_3() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___m_tMuscles_3)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tMuscles_3() const { return ___m_tMuscles_3; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tMuscles_3() { return &___m_tMuscles_3; }
	inline void set_m_tMuscles_3(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tMuscles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tMuscles_3), value);
	}

	inline static int32_t get_offset_of_m_fTimer_4() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___m_fTimer_4)); }
	inline float get_m_fTimer_4() const { return ___m_fTimer_4; }
	inline float* get_address_of_m_fTimer_4() { return &___m_fTimer_4; }
	inline void set_m_fTimer_4(float value)
	{
		___m_fTimer_4 = value;
	}

	inline static int32_t get_offset_of_m_strNames_5() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___m_strNames_5)); }
	inline StringU5BU5D_t2511808107* get_m_strNames_5() const { return ___m_strNames_5; }
	inline StringU5BU5D_t2511808107** get_address_of_m_strNames_5() { return &___m_strNames_5; }
	inline void set_m_strNames_5(StringU5BU5D_t2511808107* value)
	{
		___m_strNames_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_strNames_5), value);
	}

	inline static int32_t get_offset_of_m_iCurrentImage_6() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___m_iCurrentImage_6)); }
	inline int32_t get_m_iCurrentImage_6() const { return ___m_iCurrentImage_6; }
	inline int32_t* get_address_of_m_iCurrentImage_6() { return &___m_iCurrentImage_6; }
	inline void set_m_iCurrentImage_6(int32_t value)
	{
		___m_iCurrentImage_6 = value;
	}

	inline static int32_t get_offset_of_menu_7() { return static_cast<int32_t>(offsetof(L2RelaxYourMuscles_t1973662748, ___menu_7)); }
	inline GameObject_t2557347079 * get_menu_7() const { return ___menu_7; }
	inline GameObject_t2557347079 ** get_address_of_menu_7() { return &___menu_7; }
	inline void set_menu_7(GameObject_t2557347079 * value)
	{
		___menu_7 = value;
		Il2CppCodeGenWriteBarrier((&___menu_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2RELAXYOURMUSCLES_T1973662748_H
#ifndef L3STOPIT_T150832697_H
#define L3STOPIT_T150832697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3StopIt
struct  L3StopIt_t150832697  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3StopIt::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L3StopIt_t150832697, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3STOPIT_T150832697_H
#ifndef L3STOPITWITHTEXT_T4263992057_H
#define L3STOPITWITHTEXT_T4263992057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3StopItWithText
struct  L3StopItWithText_t4263992057  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3StopItWithText::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L3StopItWithText_t4263992057, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3STOPITWITHTEXT_T4263992057_H
#ifndef L3TURNITDOWN_T2719411953_H
#define L3TURNITDOWN_T2719411953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3TurnItDown
struct  L3TurnItDown_t2719411953  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3TurnItDown::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L3TurnItDown_t2719411953, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3TURNITDOWN_T2719411953_H
#ifndef L2DRAGANDDROPWORDS_T1602406363_H
#define L2DRAGANDDROPWORDS_T1602406363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2DragAndDropWords
struct  L2DragAndDropWords_t1602406363  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L2DragAndDropWords::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L2DragAndDropWords::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.String L2DragAndDropWords::m_conversationBoxText
	String_t* ___m_conversationBoxText_4;
	// UnityEngine.Vector2[] L2DragAndDropWords::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_7;
	// UnityEngine.Rect[] L2DragAndDropWords::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_8;
	// System.String[] L2DragAndDropWords::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_9;
	// UnityEngine.Rect[] L2DragAndDropWords::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_12;
	// UnityEngine.Rect[] L2DragAndDropWords::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_14;
	// UnityEngine.Rect[] L2DragAndDropWords::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_15;
	// UnityEngine.Rect L2DragAndDropWords::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_16;
	// System.String L2DragAndDropWords::m_currentClickedString
	String_t* ___m_currentClickedString_17;
	// System.Int32 L2DragAndDropWords::m_currentIndex
	int32_t ___m_currentIndex_18;
	// System.Boolean L2DragAndDropWords::b_dragging
	bool ___b_dragging_19;
	// UnityEngine.Texture2D L2DragAndDropWords::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_20;
	// UnityEngine.Texture2D L2DragAndDropWords::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_21;
	// UnityEngine.Texture2D L2DragAndDropWords::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_22;
	// UnityEngine.GameObject L2DragAndDropWords::menu
	GameObject_t2557347079 * ___menu_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_4() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_conversationBoxText_4)); }
	inline String_t* get_m_conversationBoxText_4() const { return ___m_conversationBoxText_4; }
	inline String_t** get_address_of_m_conversationBoxText_4() { return &___m_conversationBoxText_4; }
	inline void set_m_conversationBoxText_4(String_t* value)
	{
		___m_conversationBoxText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_4), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_7() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_KeywordDimensions_7)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_7() const { return ___m_KeywordDimensions_7; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_7() { return &___m_KeywordDimensions_7; }
	inline void set_m_KeywordDimensions_7(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_7), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_8() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_myOriRect_8)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_8() const { return ___m_myOriRect_8; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_8() { return &___m_myOriRect_8; }
	inline void set_m_myOriRect_8(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_8), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_9() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_keyWordText_9)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_9() const { return ___m_keyWordText_9; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_9() { return &___m_keyWordText_9; }
	inline void set_m_keyWordText_9(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_9), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_12() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_destinationOriKeyWordRect_12)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_12() const { return ___m_destinationOriKeyWordRect_12; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_12() { return &___m_destinationOriKeyWordRect_12; }
	inline void set_m_destinationOriKeyWordRect_12(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_12), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_14() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_destinationRect_14)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_14() const { return ___m_destinationRect_14; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_14() { return &___m_destinationRect_14; }
	inline void set_m_destinationRect_14(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_14), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_15() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_movableStringRect_15)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_15() const { return ___m_movableStringRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_15() { return &___m_movableStringRect_15; }
	inline void set_m_movableStringRect_15(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_15), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_16() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_currentClickedStringRect_16)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_16() const { return ___m_currentClickedStringRect_16; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_16() { return &___m_currentClickedStringRect_16; }
	inline void set_m_currentClickedStringRect_16(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_16 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_17() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_currentClickedString_17)); }
	inline String_t* get_m_currentClickedString_17() const { return ___m_currentClickedString_17; }
	inline String_t** get_address_of_m_currentClickedString_17() { return &___m_currentClickedString_17; }
	inline void set_m_currentClickedString_17(String_t* value)
	{
		___m_currentClickedString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_17), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_18() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_currentIndex_18)); }
	inline int32_t get_m_currentIndex_18() const { return ___m_currentIndex_18; }
	inline int32_t* get_address_of_m_currentIndex_18() { return &___m_currentIndex_18; }
	inline void set_m_currentIndex_18(int32_t value)
	{
		___m_currentIndex_18 = value;
	}

	inline static int32_t get_offset_of_b_dragging_19() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___b_dragging_19)); }
	inline bool get_b_dragging_19() const { return ___b_dragging_19; }
	inline bool* get_address_of_b_dragging_19() { return &___b_dragging_19; }
	inline void set_b_dragging_19(bool value)
	{
		___b_dragging_19 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_20() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_tFillFieldBackground_20)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_20() const { return ___m_tFillFieldBackground_20; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_20() { return &___m_tFillFieldBackground_20; }
	inline void set_m_tFillFieldBackground_20(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_20), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_21() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_tDestinationBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_21() const { return ___m_tDestinationBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_21() { return &___m_tDestinationBackground_21; }
	inline void set_m_tDestinationBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_22() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___m_tConversationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_22() const { return ___m_tConversationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_22() { return &___m_tConversationBackground_22; }
	inline void set_m_tConversationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_22), value);
	}

	inline static int32_t get_offset_of_menu_23() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363, ___menu_23)); }
	inline GameObject_t2557347079 * get_menu_23() const { return ___menu_23; }
	inline GameObject_t2557347079 ** get_address_of_menu_23() { return &___menu_23; }
	inline void set_menu_23(GameObject_t2557347079 * value)
	{
		___menu_23 = value;
		Il2CppCodeGenWriteBarrier((&___menu_23), value);
	}
};

struct L2DragAndDropWords_t1602406363_StaticFields
{
public:
	// System.Single L2DragAndDropWords::m_keywordWidth
	float ___m_keywordWidth_5;
	// System.Single L2DragAndDropWords::m_keywordHeight
	float ___m_keywordHeight_6;
	// System.Single L2DragAndDropWords::m_dextinationRectWidth
	float ___m_dextinationRectWidth_10;
	// System.Single L2DragAndDropWords::m_dextinationRectHeight
	float ___m_dextinationRectHeight_11;
	// System.String[] L2DragAndDropWords::m_L2destinationText
	StringU5BU5D_t2511808107* ___m_L2destinationText_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L2DragAndDropWords::<>f__switch$map4
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map4_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_5() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___m_keywordWidth_5)); }
	inline float get_m_keywordWidth_5() const { return ___m_keywordWidth_5; }
	inline float* get_address_of_m_keywordWidth_5() { return &___m_keywordWidth_5; }
	inline void set_m_keywordWidth_5(float value)
	{
		___m_keywordWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_6() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___m_keywordHeight_6)); }
	inline float get_m_keywordHeight_6() const { return ___m_keywordHeight_6; }
	inline float* get_address_of_m_keywordHeight_6() { return &___m_keywordHeight_6; }
	inline void set_m_keywordHeight_6(float value)
	{
		___m_keywordHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_10() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___m_dextinationRectWidth_10)); }
	inline float get_m_dextinationRectWidth_10() const { return ___m_dextinationRectWidth_10; }
	inline float* get_address_of_m_dextinationRectWidth_10() { return &___m_dextinationRectWidth_10; }
	inline void set_m_dextinationRectWidth_10(float value)
	{
		___m_dextinationRectWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_11() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___m_dextinationRectHeight_11)); }
	inline float get_m_dextinationRectHeight_11() const { return ___m_dextinationRectHeight_11; }
	inline float* get_address_of_m_dextinationRectHeight_11() { return &___m_dextinationRectHeight_11; }
	inline void set_m_dextinationRectHeight_11(float value)
	{
		___m_dextinationRectHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_L2destinationText_13() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___m_L2destinationText_13)); }
	inline StringU5BU5D_t2511808107* get_m_L2destinationText_13() const { return ___m_L2destinationText_13; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L2destinationText_13() { return &___m_L2destinationText_13; }
	inline void set_m_L2destinationText_13(StringU5BU5D_t2511808107* value)
	{
		___m_L2destinationText_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_L2destinationText_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_24() { return static_cast<int32_t>(offsetof(L2DragAndDropWords_t1602406363_StaticFields, ___U3CU3Ef__switchU24map4_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map4_24() const { return ___U3CU3Ef__switchU24map4_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map4_24() { return &___U3CU3Ef__switchU24map4_24; }
	inline void set_U3CU3Ef__switchU24map4_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map4_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2DRAGANDDROPWORDS_T1602406363_H
#ifndef EXAMPLEWHEELCONTROLLER_T1631372755_H
#define EXAMPLEWHEELCONTROLLER_T1631372755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController
struct  ExampleWheelController_t1631372755  : public MonoBehaviour_t1618594486
{
public:
	// System.Single ExampleWheelController::acceleration
	float ___acceleration_2;
	// UnityEngine.Renderer ExampleWheelController::motionVectorRenderer
	Renderer_t1418648713 * ___motionVectorRenderer_3;
	// UnityEngine.Rigidbody ExampleWheelController::m_Rigidbody
	Rigidbody_t4273256674 * ___m_Rigidbody_4;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(ExampleWheelController_t1631372755, ___acceleration_2)); }
	inline float get_acceleration_2() const { return ___acceleration_2; }
	inline float* get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(float value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_motionVectorRenderer_3() { return static_cast<int32_t>(offsetof(ExampleWheelController_t1631372755, ___motionVectorRenderer_3)); }
	inline Renderer_t1418648713 * get_motionVectorRenderer_3() const { return ___motionVectorRenderer_3; }
	inline Renderer_t1418648713 ** get_address_of_motionVectorRenderer_3() { return &___motionVectorRenderer_3; }
	inline void set_motionVectorRenderer_3(Renderer_t1418648713 * value)
	{
		___motionVectorRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___motionVectorRenderer_3), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody_4() { return static_cast<int32_t>(offsetof(ExampleWheelController_t1631372755, ___m_Rigidbody_4)); }
	inline Rigidbody_t4273256674 * get_m_Rigidbody_4() const { return ___m_Rigidbody_4; }
	inline Rigidbody_t4273256674 ** get_address_of_m_Rigidbody_4() { return &___m_Rigidbody_4; }
	inline void set_m_Rigidbody_4(Rigidbody_t4273256674 * value)
	{
		___m_Rigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEWHEELCONTROLLER_T1631372755_H
#ifndef PROVINCESCRIPT_T1528091046_H
#define PROVINCESCRIPT_T1528091046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProvinceScript
struct  ProvinceScript_t1528091046  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject ProvinceScript::pinPrefab
	GameObject_t2557347079 * ___pinPrefab_2;
	// UnityEngine.Vector3 ProvinceScript::pinOffset
	Vector3_t1986933152  ___pinOffset_3;
	// System.String ProvinceScript::level
	String_t* ___level_4;
	// System.Boolean ProvinceScript::canTravelHere
	bool ___canTravelHere_5;
	// System.Int32 ProvinceScript::levelID
	int32_t ___levelID_6;
	// UnityEngine.GameObject ProvinceScript::pin
	GameObject_t2557347079 * ___pin_7;
	// UnityEngine.Vector3 ProvinceScript::pinStartPos
	Vector3_t1986933152  ___pinStartPos_8;
	// System.Boolean ProvinceScript::pinClicked
	bool ___pinClicked_9;
	// CapturedDataInput ProvinceScript::dataInput
	CapturedDataInput_t2616152122 * ___dataInput_10;

public:
	inline static int32_t get_offset_of_pinPrefab_2() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___pinPrefab_2)); }
	inline GameObject_t2557347079 * get_pinPrefab_2() const { return ___pinPrefab_2; }
	inline GameObject_t2557347079 ** get_address_of_pinPrefab_2() { return &___pinPrefab_2; }
	inline void set_pinPrefab_2(GameObject_t2557347079 * value)
	{
		___pinPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pinPrefab_2), value);
	}

	inline static int32_t get_offset_of_pinOffset_3() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___pinOffset_3)); }
	inline Vector3_t1986933152  get_pinOffset_3() const { return ___pinOffset_3; }
	inline Vector3_t1986933152 * get_address_of_pinOffset_3() { return &___pinOffset_3; }
	inline void set_pinOffset_3(Vector3_t1986933152  value)
	{
		___pinOffset_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___level_4)); }
	inline String_t* get_level_4() const { return ___level_4; }
	inline String_t** get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(String_t* value)
	{
		___level_4 = value;
		Il2CppCodeGenWriteBarrier((&___level_4), value);
	}

	inline static int32_t get_offset_of_canTravelHere_5() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___canTravelHere_5)); }
	inline bool get_canTravelHere_5() const { return ___canTravelHere_5; }
	inline bool* get_address_of_canTravelHere_5() { return &___canTravelHere_5; }
	inline void set_canTravelHere_5(bool value)
	{
		___canTravelHere_5 = value;
	}

	inline static int32_t get_offset_of_levelID_6() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___levelID_6)); }
	inline int32_t get_levelID_6() const { return ___levelID_6; }
	inline int32_t* get_address_of_levelID_6() { return &___levelID_6; }
	inline void set_levelID_6(int32_t value)
	{
		___levelID_6 = value;
	}

	inline static int32_t get_offset_of_pin_7() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___pin_7)); }
	inline GameObject_t2557347079 * get_pin_7() const { return ___pin_7; }
	inline GameObject_t2557347079 ** get_address_of_pin_7() { return &___pin_7; }
	inline void set_pin_7(GameObject_t2557347079 * value)
	{
		___pin_7 = value;
		Il2CppCodeGenWriteBarrier((&___pin_7), value);
	}

	inline static int32_t get_offset_of_pinStartPos_8() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___pinStartPos_8)); }
	inline Vector3_t1986933152  get_pinStartPos_8() const { return ___pinStartPos_8; }
	inline Vector3_t1986933152 * get_address_of_pinStartPos_8() { return &___pinStartPos_8; }
	inline void set_pinStartPos_8(Vector3_t1986933152  value)
	{
		___pinStartPos_8 = value;
	}

	inline static int32_t get_offset_of_pinClicked_9() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___pinClicked_9)); }
	inline bool get_pinClicked_9() const { return ___pinClicked_9; }
	inline bool* get_address_of_pinClicked_9() { return &___pinClicked_9; }
	inline void set_pinClicked_9(bool value)
	{
		___pinClicked_9 = value;
	}

	inline static int32_t get_offset_of_dataInput_10() { return static_cast<int32_t>(offsetof(ProvinceScript_t1528091046, ___dataInput_10)); }
	inline CapturedDataInput_t2616152122 * get_dataInput_10() const { return ___dataInput_10; }
	inline CapturedDataInput_t2616152122 ** get_address_of_dataInput_10() { return &___dataInput_10; }
	inline void set_dataInput_10(CapturedDataInput_t2616152122 * value)
	{
		___dataInput_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataInput_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVINCESCRIPT_T1528091046_H
#ifndef RELAXMUSCLES_T2970223765_H
#define RELAXMUSCLES_T2970223765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelaxMuscles
struct  RelaxMuscles_t2970223765  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Image[] RelaxMuscles::images
	ImageU5BU5D_t892035751* ___images_2;
	// System.String[] RelaxMuscles::text
	StringU5BU5D_t2511808107* ___text_3;
	// System.Single RelaxMuscles::fadeRate
	float ___fadeRate_4;
	// UnityEngine.UI.Button RelaxMuscles::nextButton
	Button_t1293135404 * ___nextButton_5;
	// UnityEngine.UI.Text RelaxMuscles::textObject
	Text_t1790657652 * ___textObject_6;
	// System.Single RelaxMuscles::waitTime
	float ___waitTime_7;
	// System.Single RelaxMuscles::currentWaitTime
	float ___currentWaitTime_8;
	// System.Int32 RelaxMuscles::currentImage
	int32_t ___currentImage_9;
	// System.Single RelaxMuscles::currentAlpha
	float ___currentAlpha_10;
	// RelaxMuscles/ERelaxState RelaxMuscles::currentState
	int32_t ___currentState_11;

public:
	inline static int32_t get_offset_of_images_2() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___images_2)); }
	inline ImageU5BU5D_t892035751* get_images_2() const { return ___images_2; }
	inline ImageU5BU5D_t892035751** get_address_of_images_2() { return &___images_2; }
	inline void set_images_2(ImageU5BU5D_t892035751* value)
	{
		___images_2 = value;
		Il2CppCodeGenWriteBarrier((&___images_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___text_3)); }
	inline StringU5BU5D_t2511808107* get_text_3() const { return ___text_3; }
	inline StringU5BU5D_t2511808107** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(StringU5BU5D_t2511808107* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_fadeRate_4() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___fadeRate_4)); }
	inline float get_fadeRate_4() const { return ___fadeRate_4; }
	inline float* get_address_of_fadeRate_4() { return &___fadeRate_4; }
	inline void set_fadeRate_4(float value)
	{
		___fadeRate_4 = value;
	}

	inline static int32_t get_offset_of_nextButton_5() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___nextButton_5)); }
	inline Button_t1293135404 * get_nextButton_5() const { return ___nextButton_5; }
	inline Button_t1293135404 ** get_address_of_nextButton_5() { return &___nextButton_5; }
	inline void set_nextButton_5(Button_t1293135404 * value)
	{
		___nextButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_5), value);
	}

	inline static int32_t get_offset_of_textObject_6() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___textObject_6)); }
	inline Text_t1790657652 * get_textObject_6() const { return ___textObject_6; }
	inline Text_t1790657652 ** get_address_of_textObject_6() { return &___textObject_6; }
	inline void set_textObject_6(Text_t1790657652 * value)
	{
		___textObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___textObject_6), value);
	}

	inline static int32_t get_offset_of_waitTime_7() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___waitTime_7)); }
	inline float get_waitTime_7() const { return ___waitTime_7; }
	inline float* get_address_of_waitTime_7() { return &___waitTime_7; }
	inline void set_waitTime_7(float value)
	{
		___waitTime_7 = value;
	}

	inline static int32_t get_offset_of_currentWaitTime_8() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___currentWaitTime_8)); }
	inline float get_currentWaitTime_8() const { return ___currentWaitTime_8; }
	inline float* get_address_of_currentWaitTime_8() { return &___currentWaitTime_8; }
	inline void set_currentWaitTime_8(float value)
	{
		___currentWaitTime_8 = value;
	}

	inline static int32_t get_offset_of_currentImage_9() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___currentImage_9)); }
	inline int32_t get_currentImage_9() const { return ___currentImage_9; }
	inline int32_t* get_address_of_currentImage_9() { return &___currentImage_9; }
	inline void set_currentImage_9(int32_t value)
	{
		___currentImage_9 = value;
	}

	inline static int32_t get_offset_of_currentAlpha_10() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___currentAlpha_10)); }
	inline float get_currentAlpha_10() const { return ___currentAlpha_10; }
	inline float* get_address_of_currentAlpha_10() { return &___currentAlpha_10; }
	inline void set_currentAlpha_10(float value)
	{
		___currentAlpha_10 = value;
	}

	inline static int32_t get_offset_of_currentState_11() { return static_cast<int32_t>(offsetof(RelaxMuscles_t2970223765, ___currentState_11)); }
	inline int32_t get_currentState_11() const { return ___currentState_11; }
	inline int32_t* get_address_of_currentState_11() { return &___currentState_11; }
	inline void set_currentState_11(int32_t value)
	{
		___currentState_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELAXMUSCLES_T2970223765_H
#ifndef BOATCUTSCENEMANAGER_T78220184_H
#define BOATCUTSCENEMANAGER_T78220184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatCutSceneManager
struct  BoatCutSceneManager_t78220184  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] BoatCutSceneManager::boatsPlaceHolder
	GameObjectU5BU5D_t2988620542* ___boatsPlaceHolder_2;
	// UnityEngine.Playables.PlayableDirector BoatCutSceneManager::boatTimeline
	PlayableDirector_t2171935445 * ___boatTimeline_3;
	// UnityEngine.GameObject BoatCutSceneManager::fireball
	GameObject_t2557347079 * ___fireball_4;
	// System.Boolean BoatCutSceneManager::cinematicTrigger
	bool ___cinematicTrigger_5;

public:
	inline static int32_t get_offset_of_boatsPlaceHolder_2() { return static_cast<int32_t>(offsetof(BoatCutSceneManager_t78220184, ___boatsPlaceHolder_2)); }
	inline GameObjectU5BU5D_t2988620542* get_boatsPlaceHolder_2() const { return ___boatsPlaceHolder_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_boatsPlaceHolder_2() { return &___boatsPlaceHolder_2; }
	inline void set_boatsPlaceHolder_2(GameObjectU5BU5D_t2988620542* value)
	{
		___boatsPlaceHolder_2 = value;
		Il2CppCodeGenWriteBarrier((&___boatsPlaceHolder_2), value);
	}

	inline static int32_t get_offset_of_boatTimeline_3() { return static_cast<int32_t>(offsetof(BoatCutSceneManager_t78220184, ___boatTimeline_3)); }
	inline PlayableDirector_t2171935445 * get_boatTimeline_3() const { return ___boatTimeline_3; }
	inline PlayableDirector_t2171935445 ** get_address_of_boatTimeline_3() { return &___boatTimeline_3; }
	inline void set_boatTimeline_3(PlayableDirector_t2171935445 * value)
	{
		___boatTimeline_3 = value;
		Il2CppCodeGenWriteBarrier((&___boatTimeline_3), value);
	}

	inline static int32_t get_offset_of_fireball_4() { return static_cast<int32_t>(offsetof(BoatCutSceneManager_t78220184, ___fireball_4)); }
	inline GameObject_t2557347079 * get_fireball_4() const { return ___fireball_4; }
	inline GameObject_t2557347079 ** get_address_of_fireball_4() { return &___fireball_4; }
	inline void set_fireball_4(GameObject_t2557347079 * value)
	{
		___fireball_4 = value;
		Il2CppCodeGenWriteBarrier((&___fireball_4), value);
	}

	inline static int32_t get_offset_of_cinematicTrigger_5() { return static_cast<int32_t>(offsetof(BoatCutSceneManager_t78220184, ___cinematicTrigger_5)); }
	inline bool get_cinematicTrigger_5() const { return ___cinematicTrigger_5; }
	inline bool* get_address_of_cinematicTrigger_5() { return &___cinematicTrigger_5; }
	inline void set_cinematicTrigger_5(bool value)
	{
		___cinematicTrigger_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOATCUTSCENEMANAGER_T78220184_H
#ifndef BOATTRIGGER_T357608549_H
#define BOATTRIGGER_T357608549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatTrigger
struct  BoatTrigger_t357608549  : public MonoBehaviour_t1618594486
{
public:
	// BoatCutSceneManager BoatTrigger::_boatCutSceneManagerRef
	BoatCutSceneManager_t78220184 * ____boatCutSceneManagerRef_2;
	// System.Single BoatTrigger::CutSceneInitialTime
	float ___CutSceneInitialTime_3;
	// System.Single BoatTrigger::CutSceneStopTime
	float ___CutSceneStopTime_4;
	// UnityEngine.GameObject BoatTrigger::teleportPos
	GameObject_t2557347079 * ___teleportPos_5;

public:
	inline static int32_t get_offset_of__boatCutSceneManagerRef_2() { return static_cast<int32_t>(offsetof(BoatTrigger_t357608549, ____boatCutSceneManagerRef_2)); }
	inline BoatCutSceneManager_t78220184 * get__boatCutSceneManagerRef_2() const { return ____boatCutSceneManagerRef_2; }
	inline BoatCutSceneManager_t78220184 ** get_address_of__boatCutSceneManagerRef_2() { return &____boatCutSceneManagerRef_2; }
	inline void set__boatCutSceneManagerRef_2(BoatCutSceneManager_t78220184 * value)
	{
		____boatCutSceneManagerRef_2 = value;
		Il2CppCodeGenWriteBarrier((&____boatCutSceneManagerRef_2), value);
	}

	inline static int32_t get_offset_of_CutSceneInitialTime_3() { return static_cast<int32_t>(offsetof(BoatTrigger_t357608549, ___CutSceneInitialTime_3)); }
	inline float get_CutSceneInitialTime_3() const { return ___CutSceneInitialTime_3; }
	inline float* get_address_of_CutSceneInitialTime_3() { return &___CutSceneInitialTime_3; }
	inline void set_CutSceneInitialTime_3(float value)
	{
		___CutSceneInitialTime_3 = value;
	}

	inline static int32_t get_offset_of_CutSceneStopTime_4() { return static_cast<int32_t>(offsetof(BoatTrigger_t357608549, ___CutSceneStopTime_4)); }
	inline float get_CutSceneStopTime_4() const { return ___CutSceneStopTime_4; }
	inline float* get_address_of_CutSceneStopTime_4() { return &___CutSceneStopTime_4; }
	inline void set_CutSceneStopTime_4(float value)
	{
		___CutSceneStopTime_4 = value;
	}

	inline static int32_t get_offset_of_teleportPos_5() { return static_cast<int32_t>(offsetof(BoatTrigger_t357608549, ___teleportPos_5)); }
	inline GameObject_t2557347079 * get_teleportPos_5() const { return ___teleportPos_5; }
	inline GameObject_t2557347079 ** get_address_of_teleportPos_5() { return &___teleportPos_5; }
	inline void set_teleportPos_5(GameObject_t2557347079 * value)
	{
		___teleportPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___teleportPos_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOATTRIGGER_T357608549_H
#ifndef BUTTONCALLBACK_T1280411946_H
#define BUTTONCALLBACK_T1280411946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonCallback
struct  ButtonCallback_t1280411946  : public MonoBehaviour_t1618594486
{
public:
	// ButtonCallback/ButtonCallbackDelegate ButtonCallback::callback
	ButtonCallbackDelegate_t333906069 * ___callback_2;
	// System.String ButtonCallback::param
	String_t* ___param_3;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(ButtonCallback_t1280411946, ___callback_2)); }
	inline ButtonCallbackDelegate_t333906069 * get_callback_2() const { return ___callback_2; }
	inline ButtonCallbackDelegate_t333906069 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(ButtonCallbackDelegate_t333906069 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_param_3() { return static_cast<int32_t>(offsetof(ButtonCallback_t1280411946, ___param_3)); }
	inline String_t* get_param_3() const { return ___param_3; }
	inline String_t** get_address_of_param_3() { return &___param_3; }
	inline void set_param_3(String_t* value)
	{
		___param_3 = value;
		Il2CppCodeGenWriteBarrier((&___param_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCALLBACK_T1280411946_H
#ifndef CMEAGLEDEACTIVATECAM_T2782597658_H
#define CMEAGLEDEACTIVATECAM_T2782597658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMEagleDeactivateCam
struct  CMEagleDeactivateCam_t2782597658  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject CMEagleDeactivateCam::timelineGO
	GameObject_t2557347079 * ___timelineGO_2;

public:
	inline static int32_t get_offset_of_timelineGO_2() { return static_cast<int32_t>(offsetof(CMEagleDeactivateCam_t2782597658, ___timelineGO_2)); }
	inline GameObject_t2557347079 * get_timelineGO_2() const { return ___timelineGO_2; }
	inline GameObject_t2557347079 ** get_address_of_timelineGO_2() { return &___timelineGO_2; }
	inline void set_timelineGO_2(GameObject_t2557347079 * value)
	{
		___timelineGO_2 = value;
		Il2CppCodeGenWriteBarrier((&___timelineGO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMEAGLEDEACTIVATECAM_T2782597658_H
#ifndef L3TRASHIT_T838776596_H
#define L3TRASHIT_T838776596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3TrashIt
struct  L3TrashIt_t838776596  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L3TrashIt::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L3TrashIt_t838776596, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3TRASHIT_T838776596_H
#ifndef CMFOLLOWGEMPLACEMENT_T2444057678_H
#define CMFOLLOWGEMPLACEMENT_T2444057678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMFollowGemPlacement
struct  CMFollowGemPlacement_t2444057678  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject CMFollowGemPlacement::timeline
	GameObject_t2557347079 * ___timeline_2;
	// UnityEngine.Camera CMFollowGemPlacement::camera2
	Camera_t2839736942 * ___camera2_3;
	// PlayerMovement CMFollowGemPlacement::_pMovemenrRef
	PlayerMovement_t3557127520 * ____pMovemenrRef_4;
	// System.Boolean CMFollowGemPlacement::bMovementSet
	bool ___bMovementSet_5;

public:
	inline static int32_t get_offset_of_timeline_2() { return static_cast<int32_t>(offsetof(CMFollowGemPlacement_t2444057678, ___timeline_2)); }
	inline GameObject_t2557347079 * get_timeline_2() const { return ___timeline_2; }
	inline GameObject_t2557347079 ** get_address_of_timeline_2() { return &___timeline_2; }
	inline void set_timeline_2(GameObject_t2557347079 * value)
	{
		___timeline_2 = value;
		Il2CppCodeGenWriteBarrier((&___timeline_2), value);
	}

	inline static int32_t get_offset_of_camera2_3() { return static_cast<int32_t>(offsetof(CMFollowGemPlacement_t2444057678, ___camera2_3)); }
	inline Camera_t2839736942 * get_camera2_3() const { return ___camera2_3; }
	inline Camera_t2839736942 ** get_address_of_camera2_3() { return &___camera2_3; }
	inline void set_camera2_3(Camera_t2839736942 * value)
	{
		___camera2_3 = value;
		Il2CppCodeGenWriteBarrier((&___camera2_3), value);
	}

	inline static int32_t get_offset_of__pMovemenrRef_4() { return static_cast<int32_t>(offsetof(CMFollowGemPlacement_t2444057678, ____pMovemenrRef_4)); }
	inline PlayerMovement_t3557127520 * get__pMovemenrRef_4() const { return ____pMovemenrRef_4; }
	inline PlayerMovement_t3557127520 ** get_address_of__pMovemenrRef_4() { return &____pMovemenrRef_4; }
	inline void set__pMovemenrRef_4(PlayerMovement_t3557127520 * value)
	{
		____pMovemenrRef_4 = value;
		Il2CppCodeGenWriteBarrier((&____pMovemenrRef_4), value);
	}

	inline static int32_t get_offset_of_bMovementSet_5() { return static_cast<int32_t>(offsetof(CMFollowGemPlacement_t2444057678, ___bMovementSet_5)); }
	inline bool get_bMovementSet_5() const { return ___bMovementSet_5; }
	inline bool* get_address_of_bMovementSet_5() { return &___bMovementSet_5; }
	inline void set_bMovementSet_5(bool value)
	{
		___bMovementSet_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMFOLLOWGEMPLACEMENT_T2444057678_H
#ifndef MOUSEORBITIMPROVED_T2709099835_H
#define MOUSEORBITIMPROVED_T2709099835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MouseOrbitImproved
struct  MouseOrbitImproved_t2709099835  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform MouseOrbitImproved::target
	Transform_t362059596 * ___target_2;
	// System.Single MouseOrbitImproved::distance
	float ___distance_3;
	// System.Single MouseOrbitImproved::xSpeed
	float ___xSpeed_4;
	// System.Single MouseOrbitImproved::ySpeed
	float ___ySpeed_5;
	// System.Single MouseOrbitImproved::yMinLimit
	float ___yMinLimit_6;
	// System.Single MouseOrbitImproved::yMaxLimit
	float ___yMaxLimit_7;
	// System.Single MouseOrbitImproved::distanceMin
	float ___distanceMin_8;
	// System.Single MouseOrbitImproved::distanceMax
	float ___distanceMax_9;
	// System.Single MouseOrbitImproved::x
	float ___x_10;
	// System.Single MouseOrbitImproved::y
	float ___y_11;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___target_2)); }
	inline Transform_t362059596 * get_target_2() const { return ___target_2; }
	inline Transform_t362059596 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t362059596 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_xSpeed_4() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___xSpeed_4)); }
	inline float get_xSpeed_4() const { return ___xSpeed_4; }
	inline float* get_address_of_xSpeed_4() { return &___xSpeed_4; }
	inline void set_xSpeed_4(float value)
	{
		___xSpeed_4 = value;
	}

	inline static int32_t get_offset_of_ySpeed_5() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___ySpeed_5)); }
	inline float get_ySpeed_5() const { return ___ySpeed_5; }
	inline float* get_address_of_ySpeed_5() { return &___ySpeed_5; }
	inline void set_ySpeed_5(float value)
	{
		___ySpeed_5 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_6() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___yMinLimit_6)); }
	inline float get_yMinLimit_6() const { return ___yMinLimit_6; }
	inline float* get_address_of_yMinLimit_6() { return &___yMinLimit_6; }
	inline void set_yMinLimit_6(float value)
	{
		___yMinLimit_6 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_7() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___yMaxLimit_7)); }
	inline float get_yMaxLimit_7() const { return ___yMaxLimit_7; }
	inline float* get_address_of_yMaxLimit_7() { return &___yMaxLimit_7; }
	inline void set_yMaxLimit_7(float value)
	{
		___yMaxLimit_7 = value;
	}

	inline static int32_t get_offset_of_distanceMin_8() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___distanceMin_8)); }
	inline float get_distanceMin_8() const { return ___distanceMin_8; }
	inline float* get_address_of_distanceMin_8() { return &___distanceMin_8; }
	inline void set_distanceMin_8(float value)
	{
		___distanceMin_8 = value;
	}

	inline static int32_t get_offset_of_distanceMax_9() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___distanceMax_9)); }
	inline float get_distanceMax_9() const { return ___distanceMax_9; }
	inline float* get_address_of_distanceMax_9() { return &___distanceMax_9; }
	inline void set_distanceMax_9(float value)
	{
		___distanceMax_9 = value;
	}

	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___x_10)); }
	inline float get_x_10() const { return ___x_10; }
	inline float* get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(float value)
	{
		___x_10 = value;
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t2709099835, ___y_11)); }
	inline float get_y_11() const { return ___y_11; }
	inline float* get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(float value)
	{
		___y_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORBITIMPROVED_T2709099835_H
#ifndef L5DRAGDROPWORDS_T3511694991_H
#define L5DRAGDROPWORDS_T3511694991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5DragDropWords
struct  L5DragDropWords_t3511694991  : public MonoBehaviour_t1618594486
{
public:
	// System.String[] L5DragDropWords::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_2;
	// System.String[] L5DragDropWords::draggables
	StringU5BU5D_t2511808107* ___draggables_3;
	// UnityEngine.GameObject L5DragDropWords::menu
	GameObject_t2557347079 * ___menu_4;

public:
	inline static int32_t get_offset_of_m_destinationText_2() { return static_cast<int32_t>(offsetof(L5DragDropWords_t3511694991, ___m_destinationText_2)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_2() const { return ___m_destinationText_2; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_2() { return &___m_destinationText_2; }
	inline void set_m_destinationText_2(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_2), value);
	}

	inline static int32_t get_offset_of_draggables_3() { return static_cast<int32_t>(offsetof(L5DragDropWords_t3511694991, ___draggables_3)); }
	inline StringU5BU5D_t2511808107* get_draggables_3() const { return ___draggables_3; }
	inline StringU5BU5D_t2511808107** get_address_of_draggables_3() { return &___draggables_3; }
	inline void set_draggables_3(StringU5BU5D_t2511808107* value)
	{
		___draggables_3 = value;
		Il2CppCodeGenWriteBarrier((&___draggables_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L5DragDropWords_t3511694991, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5DRAGDROPWORDS_T3511694991_H
#ifndef L5DRAGDROPSHIELD_T3805322197_H
#define L5DRAGDROPSHIELD_T3805322197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5DragDropShield
struct  L5DragDropShield_t3805322197  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L5DragDropShield::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L5DragDropShield::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L5DragDropShield::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L5DragDropShield::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String L5DragDropShield::m_conversationBoxText
	String_t* ___m_conversationBoxText_6;
	// System.String[] L5DragDropShield::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_9;
	// System.String[] L5DragDropShield::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_10;
	// UnityEngine.Rect[] L5DragDropShield::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_11;
	// System.Boolean[] L5DragDropShield::displayName
	BooleanU5BU5D_t698278498* ___displayName_12;
	// UnityEngine.Color[] L5DragDropShield::displayColor
	ColorU5BU5D_t247935167* ___displayColor_13;
	// UnityEngine.GameObject L5DragDropShield::menu
	GameObject_t2557347079 * ___menu_17;
	// DraggableObject[] L5DragDropShield::m_wordsRects
	DraggableObjectU5BU5D_t2511386890* ___m_wordsRects_18;
	// UnityEngine.Rect[] L5DragDropShield::m_destinations
	RectU5BU5D_t141872167* ___m_destinations_20;
	// System.Int32[] L5DragDropShield::m_correctIndex
	Int32U5BU5D_t1965588061* ___m_correctIndex_21;
	// System.String L5DragDropShield::m_currentDragText
	String_t* ___m_currentDragText_22;
	// System.Int32 L5DragDropShield::m_currentDragIndex
	int32_t ___m_currentDragIndex_23;
	// System.Boolean L5DragDropShield::m_dragging
	bool ___m_dragging_24;
	// UnityEngine.Texture L5DragDropShield::colourTexture
	Texture_t2119925672 * ___colourTexture_25;
	// UnityEngine.Texture L5DragDropShield::colourFrameTexture
	Texture_t2119925672 * ___colourFrameTexture_26;
	// UnityEngine.Texture L5DragDropShield::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_27;
	// UnityEngine.Texture L5DragDropShield::m_tConversationBackground
	Texture_t2119925672 * ___m_tConversationBackground_28;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_6() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_conversationBoxText_6)); }
	inline String_t* get_m_conversationBoxText_6() const { return ___m_conversationBoxText_6; }
	inline String_t** get_address_of_m_conversationBoxText_6() { return &___m_conversationBoxText_6; }
	inline void set_m_conversationBoxText_6(String_t* value)
	{
		___m_conversationBoxText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_6), value);
	}

	inline static int32_t get_offset_of_childObjectNames_9() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___childObjectNames_9)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_9() const { return ___childObjectNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_9() { return &___childObjectNames_9; }
	inline void set_childObjectNames_9(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNames_10() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___shieldNames_10)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_10() const { return ___shieldNames_10; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_10() { return &___shieldNames_10; }
	inline void set_shieldNames_10(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_10), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_11() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___shieldNameRects_11)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_11() const { return ___shieldNameRects_11; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_11() { return &___shieldNameRects_11; }
	inline void set_shieldNameRects_11(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_11 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_11), value);
	}

	inline static int32_t get_offset_of_displayName_12() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___displayName_12)); }
	inline BooleanU5BU5D_t698278498* get_displayName_12() const { return ___displayName_12; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_12() { return &___displayName_12; }
	inline void set_displayName_12(BooleanU5BU5D_t698278498* value)
	{
		___displayName_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_12), value);
	}

	inline static int32_t get_offset_of_displayColor_13() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___displayColor_13)); }
	inline ColorU5BU5D_t247935167* get_displayColor_13() const { return ___displayColor_13; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_13() { return &___displayColor_13; }
	inline void set_displayColor_13(ColorU5BU5D_t247935167* value)
	{
		___displayColor_13 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_13), value);
	}

	inline static int32_t get_offset_of_menu_17() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___menu_17)); }
	inline GameObject_t2557347079 * get_menu_17() const { return ___menu_17; }
	inline GameObject_t2557347079 ** get_address_of_menu_17() { return &___menu_17; }
	inline void set_menu_17(GameObject_t2557347079 * value)
	{
		___menu_17 = value;
		Il2CppCodeGenWriteBarrier((&___menu_17), value);
	}

	inline static int32_t get_offset_of_m_wordsRects_18() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_wordsRects_18)); }
	inline DraggableObjectU5BU5D_t2511386890* get_m_wordsRects_18() const { return ___m_wordsRects_18; }
	inline DraggableObjectU5BU5D_t2511386890** get_address_of_m_wordsRects_18() { return &___m_wordsRects_18; }
	inline void set_m_wordsRects_18(DraggableObjectU5BU5D_t2511386890* value)
	{
		___m_wordsRects_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_wordsRects_18), value);
	}

	inline static int32_t get_offset_of_m_destinations_20() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_destinations_20)); }
	inline RectU5BU5D_t141872167* get_m_destinations_20() const { return ___m_destinations_20; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinations_20() { return &___m_destinations_20; }
	inline void set_m_destinations_20(RectU5BU5D_t141872167* value)
	{
		___m_destinations_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinations_20), value);
	}

	inline static int32_t get_offset_of_m_correctIndex_21() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_correctIndex_21)); }
	inline Int32U5BU5D_t1965588061* get_m_correctIndex_21() const { return ___m_correctIndex_21; }
	inline Int32U5BU5D_t1965588061** get_address_of_m_correctIndex_21() { return &___m_correctIndex_21; }
	inline void set_m_correctIndex_21(Int32U5BU5D_t1965588061* value)
	{
		___m_correctIndex_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_correctIndex_21), value);
	}

	inline static int32_t get_offset_of_m_currentDragText_22() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_currentDragText_22)); }
	inline String_t* get_m_currentDragText_22() const { return ___m_currentDragText_22; }
	inline String_t** get_address_of_m_currentDragText_22() { return &___m_currentDragText_22; }
	inline void set_m_currentDragText_22(String_t* value)
	{
		___m_currentDragText_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentDragText_22), value);
	}

	inline static int32_t get_offset_of_m_currentDragIndex_23() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_currentDragIndex_23)); }
	inline int32_t get_m_currentDragIndex_23() const { return ___m_currentDragIndex_23; }
	inline int32_t* get_address_of_m_currentDragIndex_23() { return &___m_currentDragIndex_23; }
	inline void set_m_currentDragIndex_23(int32_t value)
	{
		___m_currentDragIndex_23 = value;
	}

	inline static int32_t get_offset_of_m_dragging_24() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_dragging_24)); }
	inline bool get_m_dragging_24() const { return ___m_dragging_24; }
	inline bool* get_address_of_m_dragging_24() { return &___m_dragging_24; }
	inline void set_m_dragging_24(bool value)
	{
		___m_dragging_24 = value;
	}

	inline static int32_t get_offset_of_colourTexture_25() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___colourTexture_25)); }
	inline Texture_t2119925672 * get_colourTexture_25() const { return ___colourTexture_25; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_25() { return &___colourTexture_25; }
	inline void set_colourTexture_25(Texture_t2119925672 * value)
	{
		___colourTexture_25 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_25), value);
	}

	inline static int32_t get_offset_of_colourFrameTexture_26() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___colourFrameTexture_26)); }
	inline Texture_t2119925672 * get_colourFrameTexture_26() const { return ___colourFrameTexture_26; }
	inline Texture_t2119925672 ** get_address_of_colourFrameTexture_26() { return &___colourFrameTexture_26; }
	inline void set_colourFrameTexture_26(Texture_t2119925672 * value)
	{
		___colourFrameTexture_26 = value;
		Il2CppCodeGenWriteBarrier((&___colourFrameTexture_26), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_27() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___greyScaleTexture_27)); }
	inline Texture_t2119925672 * get_greyScaleTexture_27() const { return ___greyScaleTexture_27; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_27() { return &___greyScaleTexture_27; }
	inline void set_greyScaleTexture_27(Texture_t2119925672 * value)
	{
		___greyScaleTexture_27 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_27), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_28() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197, ___m_tConversationBackground_28)); }
	inline Texture_t2119925672 * get_m_tConversationBackground_28() const { return ___m_tConversationBackground_28; }
	inline Texture_t2119925672 ** get_address_of_m_tConversationBackground_28() { return &___m_tConversationBackground_28; }
	inline void set_m_tConversationBackground_28(Texture_t2119925672 * value)
	{
		___m_tConversationBackground_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_28), value);
	}
};

struct L5DragDropShield_t3805322197_StaticFields
{
public:
	// System.Single L5DragDropShield::TextWidth
	float ___TextWidth_7;
	// System.Single L5DragDropShield::TextHeight
	float ___TextHeight_8;
	// System.Single L5DragDropShield::m_collisiondimension
	float ___m_collisiondimension_14;
	// System.Single L5DragDropShield::m_movingTextwidth
	float ___m_movingTextwidth_15;
	// System.Single L5DragDropShield::m_movingTextHeight
	float ___m_movingTextHeight_16;
	// System.Single L5DragDropShield::m_targetDim
	float ___m_targetDim_19;

public:
	inline static int32_t get_offset_of_TextWidth_7() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___TextWidth_7)); }
	inline float get_TextWidth_7() const { return ___TextWidth_7; }
	inline float* get_address_of_TextWidth_7() { return &___TextWidth_7; }
	inline void set_TextWidth_7(float value)
	{
		___TextWidth_7 = value;
	}

	inline static int32_t get_offset_of_TextHeight_8() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___TextHeight_8)); }
	inline float get_TextHeight_8() const { return ___TextHeight_8; }
	inline float* get_address_of_TextHeight_8() { return &___TextHeight_8; }
	inline void set_TextHeight_8(float value)
	{
		___TextHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_collisiondimension_14() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___m_collisiondimension_14)); }
	inline float get_m_collisiondimension_14() const { return ___m_collisiondimension_14; }
	inline float* get_address_of_m_collisiondimension_14() { return &___m_collisiondimension_14; }
	inline void set_m_collisiondimension_14(float value)
	{
		___m_collisiondimension_14 = value;
	}

	inline static int32_t get_offset_of_m_movingTextwidth_15() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___m_movingTextwidth_15)); }
	inline float get_m_movingTextwidth_15() const { return ___m_movingTextwidth_15; }
	inline float* get_address_of_m_movingTextwidth_15() { return &___m_movingTextwidth_15; }
	inline void set_m_movingTextwidth_15(float value)
	{
		___m_movingTextwidth_15 = value;
	}

	inline static int32_t get_offset_of_m_movingTextHeight_16() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___m_movingTextHeight_16)); }
	inline float get_m_movingTextHeight_16() const { return ___m_movingTextHeight_16; }
	inline float* get_address_of_m_movingTextHeight_16() { return &___m_movingTextHeight_16; }
	inline void set_m_movingTextHeight_16(float value)
	{
		___m_movingTextHeight_16 = value;
	}

	inline static int32_t get_offset_of_m_targetDim_19() { return static_cast<int32_t>(offsetof(L5DragDropShield_t3805322197_StaticFields, ___m_targetDim_19)); }
	inline float get_m_targetDim_19() const { return ___m_targetDim_19; }
	inline float* get_address_of_m_targetDim_19() { return &___m_targetDim_19; }
	inline void set_m_targetDim_19(float value)
	{
		___m_targetDim_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5DRAGDROPSHIELD_T3805322197_H
#ifndef L4SHIELDAGADEPRESSION_T944406110_H
#define L4SHIELDAGADEPRESSION_T944406110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4ShieldAgaDepression
struct  L4ShieldAgaDepression_t944406110  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L4ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L4ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L4ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L4ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] L4ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] L4ShieldAgaDepression::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// UnityEngine.Rect[] L4ShieldAgaDepression::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_10;
	// System.Boolean[] L4ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_11;
	// UnityEngine.Color[] L4ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_12;
	// UnityEngine.Texture L4ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_13;
	// UnityEngine.Texture L4ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_14;
	// UnityEngine.GameObject L4ShieldAgaDepression::menu
	GameObject_t2557347079 * ___menu_15;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_10() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___shieldNameRects_10)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_10() const { return ___shieldNameRects_10; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_10() { return &___shieldNameRects_10; }
	inline void set_shieldNameRects_10(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_10), value);
	}

	inline static int32_t get_offset_of_displayName_11() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___displayName_11)); }
	inline BooleanU5BU5D_t698278498* get_displayName_11() const { return ___displayName_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_11() { return &___displayName_11; }
	inline void set_displayName_11(BooleanU5BU5D_t698278498* value)
	{
		___displayName_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_11), value);
	}

	inline static int32_t get_offset_of_displayColor_12() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___displayColor_12)); }
	inline ColorU5BU5D_t247935167* get_displayColor_12() const { return ___displayColor_12; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_12() { return &___displayColor_12; }
	inline void set_displayColor_12(ColorU5BU5D_t247935167* value)
	{
		___displayColor_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_12), value);
	}

	inline static int32_t get_offset_of_colourTexture_13() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___colourTexture_13)); }
	inline Texture_t2119925672 * get_colourTexture_13() const { return ___colourTexture_13; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_13() { return &___colourTexture_13; }
	inline void set_colourTexture_13(Texture_t2119925672 * value)
	{
		___colourTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_13), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_14() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___greyScaleTexture_14)); }
	inline Texture_t2119925672 * get_greyScaleTexture_14() const { return ___greyScaleTexture_14; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_14() { return &___greyScaleTexture_14; }
	inline void set_greyScaleTexture_14(Texture_t2119925672 * value)
	{
		___greyScaleTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_14), value);
	}

	inline static int32_t get_offset_of_menu_15() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110, ___menu_15)); }
	inline GameObject_t2557347079 * get_menu_15() const { return ___menu_15; }
	inline GameObject_t2557347079 ** get_address_of_menu_15() { return &___menu_15; }
	inline void set_menu_15(GameObject_t2557347079 * value)
	{
		___menu_15 = value;
		Il2CppCodeGenWriteBarrier((&___menu_15), value);
	}
};

struct L4ShieldAgaDepression_t944406110_StaticFields
{
public:
	// System.Single L4ShieldAgaDepression::TextWidth
	float ___TextWidth_6;
	// System.Single L4ShieldAgaDepression::TextHeight
	float ___TextHeight_7;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(L4ShieldAgaDepression_t944406110_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4SHIELDAGADEPRESSION_T944406110_H
#ifndef L4ENTERTEXTS_T4102979614_H
#define L4ENTERTEXTS_T4102979614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4EnterTexts
struct  L4EnterTexts_t4102979614  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L4EnterTexts::menu
	GameObject_t2557347079 * ___menu_4;

public:
	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L4EnterTexts_t4102979614, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}
};

struct L4EnterTexts_t4102979614_StaticFields
{
public:
	// System.Boolean L4EnterTexts::m_bTriggered
	bool ___m_bTriggered_2;
	// System.String L4EnterTexts::m_inputTexts
	String_t* ___m_inputTexts_3;

public:
	inline static int32_t get_offset_of_m_bTriggered_2() { return static_cast<int32_t>(offsetof(L4EnterTexts_t4102979614_StaticFields, ___m_bTriggered_2)); }
	inline bool get_m_bTriggered_2() const { return ___m_bTriggered_2; }
	inline bool* get_address_of_m_bTriggered_2() { return &___m_bTriggered_2; }
	inline void set_m_bTriggered_2(bool value)
	{
		___m_bTriggered_2 = value;
	}

	inline static int32_t get_offset_of_m_inputTexts_3() { return static_cast<int32_t>(offsetof(L4EnterTexts_t4102979614_StaticFields, ___m_inputTexts_3)); }
	inline String_t* get_m_inputTexts_3() const { return ___m_inputTexts_3; }
	inline String_t** get_address_of_m_inputTexts_3() { return &___m_inputTexts_3; }
	inline void set_m_inputTexts_3(String_t* value)
	{
		___m_inputTexts_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputTexts_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4ENTERTEXTS_T4102979614_H
#ifndef L4DRAGDROPWORDS2_T4012073771_H
#define L4DRAGDROPWORDS2_T4012073771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4DragDropWords2
struct  L4DragDropWords2_t4012073771  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L4DragDropWords2::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L4DragDropWords2::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.String L4DragDropWords2::m_conversationBoxText
	String_t* ___m_conversationBoxText_4;
	// UnityEngine.Vector2[] L4DragDropWords2::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_7;
	// UnityEngine.Rect[] L4DragDropWords2::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_8;
	// System.String[] L4DragDropWords2::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_9;
	// UnityEngine.Rect[] L4DragDropWords2::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_12;
	// UnityEngine.Rect[] L4DragDropWords2::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_14;
	// UnityEngine.Rect[] L4DragDropWords2::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_15;
	// UnityEngine.Rect L4DragDropWords2::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_16;
	// System.String L4DragDropWords2::m_currentClickedString
	String_t* ___m_currentClickedString_17;
	// System.Int32 L4DragDropWords2::m_currentIndex
	int32_t ___m_currentIndex_18;
	// System.Boolean L4DragDropWords2::b_dragging
	bool ___b_dragging_19;
	// UnityEngine.Texture2D L4DragDropWords2::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_20;
	// UnityEngine.Texture2D L4DragDropWords2::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_21;
	// UnityEngine.Texture2D L4DragDropWords2::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_22;
	// UnityEngine.GameObject L4DragDropWords2::menu
	GameObject_t2557347079 * ___menu_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_4() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_conversationBoxText_4)); }
	inline String_t* get_m_conversationBoxText_4() const { return ___m_conversationBoxText_4; }
	inline String_t** get_address_of_m_conversationBoxText_4() { return &___m_conversationBoxText_4; }
	inline void set_m_conversationBoxText_4(String_t* value)
	{
		___m_conversationBoxText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_4), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_7() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_KeywordDimensions_7)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_7() const { return ___m_KeywordDimensions_7; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_7() { return &___m_KeywordDimensions_7; }
	inline void set_m_KeywordDimensions_7(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_7), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_8() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_myOriRect_8)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_8() const { return ___m_myOriRect_8; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_8() { return &___m_myOriRect_8; }
	inline void set_m_myOriRect_8(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_8), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_9() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_keyWordText_9)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_9() const { return ___m_keyWordText_9; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_9() { return &___m_keyWordText_9; }
	inline void set_m_keyWordText_9(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_9), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_12() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_destinationOriKeyWordRect_12)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_12() const { return ___m_destinationOriKeyWordRect_12; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_12() { return &___m_destinationOriKeyWordRect_12; }
	inline void set_m_destinationOriKeyWordRect_12(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_12), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_14() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_destinationRect_14)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_14() const { return ___m_destinationRect_14; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_14() { return &___m_destinationRect_14; }
	inline void set_m_destinationRect_14(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_14), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_15() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_movableStringRect_15)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_15() const { return ___m_movableStringRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_15() { return &___m_movableStringRect_15; }
	inline void set_m_movableStringRect_15(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_15), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_16() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_currentClickedStringRect_16)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_16() const { return ___m_currentClickedStringRect_16; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_16() { return &___m_currentClickedStringRect_16; }
	inline void set_m_currentClickedStringRect_16(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_16 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_17() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_currentClickedString_17)); }
	inline String_t* get_m_currentClickedString_17() const { return ___m_currentClickedString_17; }
	inline String_t** get_address_of_m_currentClickedString_17() { return &___m_currentClickedString_17; }
	inline void set_m_currentClickedString_17(String_t* value)
	{
		___m_currentClickedString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_17), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_18() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_currentIndex_18)); }
	inline int32_t get_m_currentIndex_18() const { return ___m_currentIndex_18; }
	inline int32_t* get_address_of_m_currentIndex_18() { return &___m_currentIndex_18; }
	inline void set_m_currentIndex_18(int32_t value)
	{
		___m_currentIndex_18 = value;
	}

	inline static int32_t get_offset_of_b_dragging_19() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___b_dragging_19)); }
	inline bool get_b_dragging_19() const { return ___b_dragging_19; }
	inline bool* get_address_of_b_dragging_19() { return &___b_dragging_19; }
	inline void set_b_dragging_19(bool value)
	{
		___b_dragging_19 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_20() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_tFillFieldBackground_20)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_20() const { return ___m_tFillFieldBackground_20; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_20() { return &___m_tFillFieldBackground_20; }
	inline void set_m_tFillFieldBackground_20(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_20), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_21() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_tDestinationBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_21() const { return ___m_tDestinationBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_21() { return &___m_tDestinationBackground_21; }
	inline void set_m_tDestinationBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_22() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___m_tConversationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_22() const { return ___m_tConversationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_22() { return &___m_tConversationBackground_22; }
	inline void set_m_tConversationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_22), value);
	}

	inline static int32_t get_offset_of_menu_23() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771, ___menu_23)); }
	inline GameObject_t2557347079 * get_menu_23() const { return ___menu_23; }
	inline GameObject_t2557347079 ** get_address_of_menu_23() { return &___menu_23; }
	inline void set_menu_23(GameObject_t2557347079 * value)
	{
		___menu_23 = value;
		Il2CppCodeGenWriteBarrier((&___menu_23), value);
	}
};

struct L4DragDropWords2_t4012073771_StaticFields
{
public:
	// System.Single L4DragDropWords2::m_keywordWidth
	float ___m_keywordWidth_5;
	// System.Single L4DragDropWords2::m_keywordHeight
	float ___m_keywordHeight_6;
	// System.Single L4DragDropWords2::m_dextinationRectWidth
	float ___m_dextinationRectWidth_10;
	// System.Single L4DragDropWords2::m_dextinationRectHeight
	float ___m_dextinationRectHeight_11;
	// System.String[] L4DragDropWords2::m_L4destinationText
	StringU5BU5D_t2511808107* ___m_L4destinationText_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L4DragDropWords2::<>f__switch$mapB
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapB_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_5() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___m_keywordWidth_5)); }
	inline float get_m_keywordWidth_5() const { return ___m_keywordWidth_5; }
	inline float* get_address_of_m_keywordWidth_5() { return &___m_keywordWidth_5; }
	inline void set_m_keywordWidth_5(float value)
	{
		___m_keywordWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_6() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___m_keywordHeight_6)); }
	inline float get_m_keywordHeight_6() const { return ___m_keywordHeight_6; }
	inline float* get_address_of_m_keywordHeight_6() { return &___m_keywordHeight_6; }
	inline void set_m_keywordHeight_6(float value)
	{
		___m_keywordHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_10() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___m_dextinationRectWidth_10)); }
	inline float get_m_dextinationRectWidth_10() const { return ___m_dextinationRectWidth_10; }
	inline float* get_address_of_m_dextinationRectWidth_10() { return &___m_dextinationRectWidth_10; }
	inline void set_m_dextinationRectWidth_10(float value)
	{
		___m_dextinationRectWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_11() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___m_dextinationRectHeight_11)); }
	inline float get_m_dextinationRectHeight_11() const { return ___m_dextinationRectHeight_11; }
	inline float* get_address_of_m_dextinationRectHeight_11() { return &___m_dextinationRectHeight_11; }
	inline void set_m_dextinationRectHeight_11(float value)
	{
		___m_dextinationRectHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_L4destinationText_13() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___m_L4destinationText_13)); }
	inline StringU5BU5D_t2511808107* get_m_L4destinationText_13() const { return ___m_L4destinationText_13; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L4destinationText_13() { return &___m_L4destinationText_13; }
	inline void set_m_L4destinationText_13(StringU5BU5D_t2511808107* value)
	{
		___m_L4destinationText_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_L4destinationText_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_24() { return static_cast<int32_t>(offsetof(L4DragDropWords2_t4012073771_StaticFields, ___U3CU3Ef__switchU24mapB_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapB_24() const { return ___U3CU3Ef__switchU24mapB_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapB_24() { return &___U3CU3Ef__switchU24mapB_24; }
	inline void set_U3CU3Ef__switchU24mapB_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapB_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4DRAGDROPWORDS2_T4012073771_H
#ifndef L4DRAGDROPWORDS_T3565732689_H
#define L4DRAGDROPWORDS_T3565732689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4DragDropWords
struct  L4DragDropWords_t3565732689  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect L4DragDropWords::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin L4DragDropWords::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.GameObject L4DragDropWords::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String L4DragDropWords::m_conversationBoxText
	String_t* ___m_conversationBoxText_5;
	// UnityEngine.Vector2[] L4DragDropWords::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_8;
	// UnityEngine.Rect[] L4DragDropWords::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_9;
	// System.String[] L4DragDropWords::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_10;
	// UnityEngine.Rect[] L4DragDropWords::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_13;
	// UnityEngine.Rect[] L4DragDropWords::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_15;
	// UnityEngine.Rect[] L4DragDropWords::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_16;
	// UnityEngine.Rect L4DragDropWords::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_17;
	// System.String L4DragDropWords::m_currentClickedString
	String_t* ___m_currentClickedString_18;
	// System.Int32 L4DragDropWords::m_currentIndex
	int32_t ___m_currentIndex_19;
	// System.Boolean L4DragDropWords::b_dragging
	bool ___b_dragging_20;
	// UnityEngine.Texture2D L4DragDropWords::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_21;
	// UnityEngine.Texture2D L4DragDropWords::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_22;
	// UnityEngine.Texture2D L4DragDropWords::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_5() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_conversationBoxText_5)); }
	inline String_t* get_m_conversationBoxText_5() const { return ___m_conversationBoxText_5; }
	inline String_t** get_address_of_m_conversationBoxText_5() { return &___m_conversationBoxText_5; }
	inline void set_m_conversationBoxText_5(String_t* value)
	{
		___m_conversationBoxText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_5), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_8() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_KeywordDimensions_8)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_8() const { return ___m_KeywordDimensions_8; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_8() { return &___m_KeywordDimensions_8; }
	inline void set_m_KeywordDimensions_8(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_8), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_9() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_myOriRect_9)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_9() const { return ___m_myOriRect_9; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_9() { return &___m_myOriRect_9; }
	inline void set_m_myOriRect_9(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_9), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_10() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_keyWordText_10)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_10() const { return ___m_keyWordText_10; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_10() { return &___m_keyWordText_10; }
	inline void set_m_keyWordText_10(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_10), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_13() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_destinationOriKeyWordRect_13)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_13() const { return ___m_destinationOriKeyWordRect_13; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_13() { return &___m_destinationOriKeyWordRect_13; }
	inline void set_m_destinationOriKeyWordRect_13(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_13), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_15() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_destinationRect_15)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_15() const { return ___m_destinationRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_15() { return &___m_destinationRect_15; }
	inline void set_m_destinationRect_15(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_15), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_16() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_movableStringRect_16)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_16() const { return ___m_movableStringRect_16; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_16() { return &___m_movableStringRect_16; }
	inline void set_m_movableStringRect_16(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_16), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_17() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_currentClickedStringRect_17)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_17() const { return ___m_currentClickedStringRect_17; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_17() { return &___m_currentClickedStringRect_17; }
	inline void set_m_currentClickedStringRect_17(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_17 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_18() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_currentClickedString_18)); }
	inline String_t* get_m_currentClickedString_18() const { return ___m_currentClickedString_18; }
	inline String_t** get_address_of_m_currentClickedString_18() { return &___m_currentClickedString_18; }
	inline void set_m_currentClickedString_18(String_t* value)
	{
		___m_currentClickedString_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_18), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_19() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_currentIndex_19)); }
	inline int32_t get_m_currentIndex_19() const { return ___m_currentIndex_19; }
	inline int32_t* get_address_of_m_currentIndex_19() { return &___m_currentIndex_19; }
	inline void set_m_currentIndex_19(int32_t value)
	{
		___m_currentIndex_19 = value;
	}

	inline static int32_t get_offset_of_b_dragging_20() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___b_dragging_20)); }
	inline bool get_b_dragging_20() const { return ___b_dragging_20; }
	inline bool* get_address_of_b_dragging_20() { return &___b_dragging_20; }
	inline void set_b_dragging_20(bool value)
	{
		___b_dragging_20 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_21() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_tFillFieldBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_21() const { return ___m_tFillFieldBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_21() { return &___m_tFillFieldBackground_21; }
	inline void set_m_tFillFieldBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_22() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_tDestinationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_22() const { return ___m_tDestinationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_22() { return &___m_tDestinationBackground_22; }
	inline void set_m_tDestinationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_22), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_23() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689, ___m_tConversationBackground_23)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_23() const { return ___m_tConversationBackground_23; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_23() { return &___m_tConversationBackground_23; }
	inline void set_m_tConversationBackground_23(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_23), value);
	}
};

struct L4DragDropWords_t3565732689_StaticFields
{
public:
	// System.Single L4DragDropWords::m_keywordWidth
	float ___m_keywordWidth_6;
	// System.Single L4DragDropWords::m_keywordHeight
	float ___m_keywordHeight_7;
	// System.Single L4DragDropWords::m_dextinationRectWidth
	float ___m_dextinationRectWidth_11;
	// System.Single L4DragDropWords::m_dextinationRectHeight
	float ___m_dextinationRectHeight_12;
	// System.String[] L4DragDropWords::m_L4destinationText
	StringU5BU5D_t2511808107* ___m_L4destinationText_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L4DragDropWords::<>f__switch$mapA
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapA_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_6() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___m_keywordWidth_6)); }
	inline float get_m_keywordWidth_6() const { return ___m_keywordWidth_6; }
	inline float* get_address_of_m_keywordWidth_6() { return &___m_keywordWidth_6; }
	inline void set_m_keywordWidth_6(float value)
	{
		___m_keywordWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_7() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___m_keywordHeight_7)); }
	inline float get_m_keywordHeight_7() const { return ___m_keywordHeight_7; }
	inline float* get_address_of_m_keywordHeight_7() { return &___m_keywordHeight_7; }
	inline void set_m_keywordHeight_7(float value)
	{
		___m_keywordHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_11() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___m_dextinationRectWidth_11)); }
	inline float get_m_dextinationRectWidth_11() const { return ___m_dextinationRectWidth_11; }
	inline float* get_address_of_m_dextinationRectWidth_11() { return &___m_dextinationRectWidth_11; }
	inline void set_m_dextinationRectWidth_11(float value)
	{
		___m_dextinationRectWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_12() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___m_dextinationRectHeight_12)); }
	inline float get_m_dextinationRectHeight_12() const { return ___m_dextinationRectHeight_12; }
	inline float* get_address_of_m_dextinationRectHeight_12() { return &___m_dextinationRectHeight_12; }
	inline void set_m_dextinationRectHeight_12(float value)
	{
		___m_dextinationRectHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_L4destinationText_14() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___m_L4destinationText_14)); }
	inline StringU5BU5D_t2511808107* get_m_L4destinationText_14() const { return ___m_L4destinationText_14; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L4destinationText_14() { return &___m_L4destinationText_14; }
	inline void set_m_L4destinationText_14(StringU5BU5D_t2511808107* value)
	{
		___m_L4destinationText_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_L4destinationText_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_24() { return static_cast<int32_t>(offsetof(L4DragDropWords_t3565732689_StaticFields, ___U3CU3Ef__switchU24mapA_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapA_24() const { return ___U3CU3Ef__switchU24mapA_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapA_24() { return &___U3CU3Ef__switchU24mapA_24; }
	inline void set_U3CU3Ef__switchU24mapA_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapA_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4DRAGDROPWORDS_T3565732689_H
#ifndef L7CAMERAROTATION_T3332317093_H
#define L7CAMERAROTATION_T3332317093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7CameraRotation
struct  L7CameraRotation_t3332317093  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L7CameraRotation::fAngleRotated
	float ___fAngleRotated_2;
	// System.Boolean L7CameraRotation::m_bCameraIsRotating
	bool ___m_bCameraIsRotating_3;
	// System.Boolean L7CameraRotation::m_bCameraCanRotate
	bool ___m_bCameraCanRotate_4;
	// System.Boolean L7CameraRotation::m_bMovingToPoint
	bool ___m_bMovingToPoint_5;
	// UnityEngine.Vector3 L7CameraRotation::m_vStartVector
	Vector3_t1986933152  ___m_vStartVector_6;
	// UnityEngine.Vector3 L7CameraRotation::m_vCurrentVector
	Vector3_t1986933152  ___m_vCurrentVector_7;

public:
	inline static int32_t get_offset_of_fAngleRotated_2() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___fAngleRotated_2)); }
	inline float get_fAngleRotated_2() const { return ___fAngleRotated_2; }
	inline float* get_address_of_fAngleRotated_2() { return &___fAngleRotated_2; }
	inline void set_fAngleRotated_2(float value)
	{
		___fAngleRotated_2 = value;
	}

	inline static int32_t get_offset_of_m_bCameraIsRotating_3() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___m_bCameraIsRotating_3)); }
	inline bool get_m_bCameraIsRotating_3() const { return ___m_bCameraIsRotating_3; }
	inline bool* get_address_of_m_bCameraIsRotating_3() { return &___m_bCameraIsRotating_3; }
	inline void set_m_bCameraIsRotating_3(bool value)
	{
		___m_bCameraIsRotating_3 = value;
	}

	inline static int32_t get_offset_of_m_bCameraCanRotate_4() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___m_bCameraCanRotate_4)); }
	inline bool get_m_bCameraCanRotate_4() const { return ___m_bCameraCanRotate_4; }
	inline bool* get_address_of_m_bCameraCanRotate_4() { return &___m_bCameraCanRotate_4; }
	inline void set_m_bCameraCanRotate_4(bool value)
	{
		___m_bCameraCanRotate_4 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToPoint_5() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___m_bMovingToPoint_5)); }
	inline bool get_m_bMovingToPoint_5() const { return ___m_bMovingToPoint_5; }
	inline bool* get_address_of_m_bMovingToPoint_5() { return &___m_bMovingToPoint_5; }
	inline void set_m_bMovingToPoint_5(bool value)
	{
		___m_bMovingToPoint_5 = value;
	}

	inline static int32_t get_offset_of_m_vStartVector_6() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___m_vStartVector_6)); }
	inline Vector3_t1986933152  get_m_vStartVector_6() const { return ___m_vStartVector_6; }
	inline Vector3_t1986933152 * get_address_of_m_vStartVector_6() { return &___m_vStartVector_6; }
	inline void set_m_vStartVector_6(Vector3_t1986933152  value)
	{
		___m_vStartVector_6 = value;
	}

	inline static int32_t get_offset_of_m_vCurrentVector_7() { return static_cast<int32_t>(offsetof(L7CameraRotation_t3332317093, ___m_vCurrentVector_7)); }
	inline Vector3_t1986933152  get_m_vCurrentVector_7() const { return ___m_vCurrentVector_7; }
	inline Vector3_t1986933152 * get_address_of_m_vCurrentVector_7() { return &___m_vCurrentVector_7; }
	inline void set_m_vCurrentVector_7(Vector3_t1986933152  value)
	{
		___m_vCurrentVector_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7CAMERAROTATION_T3332317093_H
#ifndef DRAGOBJECT_T2975917658_H
#define DRAGOBJECT_T2975917658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragObject
struct  DragObject_t2975917658  : public MonoBehaviour_t1618594486
{
public:
	// DragObject/DragCallback DragObject::callback
	DragCallback_t2341893212 * ___callback_2;
	// System.Boolean DragObject::dragging
	bool ___dragging_3;
	// System.Boolean DragObject::dragable
	bool ___dragable_4;
	// System.Boolean DragObject::snapped
	bool ___snapped_5;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(DragObject_t2975917658, ___callback_2)); }
	inline DragCallback_t2341893212 * get_callback_2() const { return ___callback_2; }
	inline DragCallback_t2341893212 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(DragCallback_t2341893212 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_dragging_3() { return static_cast<int32_t>(offsetof(DragObject_t2975917658, ___dragging_3)); }
	inline bool get_dragging_3() const { return ___dragging_3; }
	inline bool* get_address_of_dragging_3() { return &___dragging_3; }
	inline void set_dragging_3(bool value)
	{
		___dragging_3 = value;
	}

	inline static int32_t get_offset_of_dragable_4() { return static_cast<int32_t>(offsetof(DragObject_t2975917658, ___dragable_4)); }
	inline bool get_dragable_4() const { return ___dragable_4; }
	inline bool* get_address_of_dragable_4() { return &___dragable_4; }
	inline void set_dragable_4(bool value)
	{
		___dragable_4 = value;
	}

	inline static int32_t get_offset_of_snapped_5() { return static_cast<int32_t>(offsetof(DragObject_t2975917658, ___snapped_5)); }
	inline bool get_snapped_5() const { return ___snapped_5; }
	inline bool* get_address_of_snapped_5() { return &___snapped_5; }
	inline void set_snapped_5(bool value)
	{
		___snapped_5 = value;
	}
};

struct DragObject_t2975917658_StaticFields
{
public:
	// System.Boolean DragObject::dragOne
	bool ___dragOne_6;

public:
	inline static int32_t get_offset_of_dragOne_6() { return static_cast<int32_t>(offsetof(DragObject_t2975917658_StaticFields, ___dragOne_6)); }
	inline bool get_dragOne_6() const { return ___dragOne_6; }
	inline bool* get_address_of_dragOne_6() { return &___dragOne_6; }
	inline void set_dragOne_6(bool value)
	{
		___dragOne_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGOBJECT_T2975917658_H
#ifndef KEYBUTTONSCRIPTRAPA_T1321666730_H
#define KEYBUTTONSCRIPTRAPA_T1321666730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyButtonScriptRAPA
struct  KeyButtonScriptRAPA_t1321666730  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin KeyButtonScriptRAPA::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Rect[] KeyButtonScriptRAPA::m_blincRects
	RectU5BU5D_t141872167* ___m_blincRects_3;
	// UnityEngine.GameObject KeyButtonScriptRAPA::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String[] KeyButtonScriptRAPA::m_blinc
	StringU5BU5D_t2511808107* ___m_blinc_5;
	// System.String KeyButtonScriptRAPA::m_conversationBoxText
	String_t* ___m_conversationBoxText_6;
	// UnityEngine.Texture KeyButtonScriptRAPA::m_tKey
	Texture_t2119925672 * ___m_tKey_7;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_blincRects_3() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___m_blincRects_3)); }
	inline RectU5BU5D_t141872167* get_m_blincRects_3() const { return ___m_blincRects_3; }
	inline RectU5BU5D_t141872167** get_address_of_m_blincRects_3() { return &___m_blincRects_3; }
	inline void set_m_blincRects_3(RectU5BU5D_t141872167* value)
	{
		___m_blincRects_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_blincRects_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_m_blinc_5() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___m_blinc_5)); }
	inline StringU5BU5D_t2511808107* get_m_blinc_5() const { return ___m_blinc_5; }
	inline StringU5BU5D_t2511808107** get_address_of_m_blinc_5() { return &___m_blinc_5; }
	inline void set_m_blinc_5(StringU5BU5D_t2511808107* value)
	{
		___m_blinc_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_blinc_5), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_6() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___m_conversationBoxText_6)); }
	inline String_t* get_m_conversationBoxText_6() const { return ___m_conversationBoxText_6; }
	inline String_t** get_address_of_m_conversationBoxText_6() { return &___m_conversationBoxText_6; }
	inline void set_m_conversationBoxText_6(String_t* value)
	{
		___m_conversationBoxText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_6), value);
	}

	inline static int32_t get_offset_of_m_tKey_7() { return static_cast<int32_t>(offsetof(KeyButtonScriptRAPA_t1321666730, ___m_tKey_7)); }
	inline Texture_t2119925672 * get_m_tKey_7() const { return ___m_tKey_7; }
	inline Texture_t2119925672 ** get_address_of_m_tKey_7() { return &___m_tKey_7; }
	inline void set_m_tKey_7(Texture_t2119925672 * value)
	{
		___m_tKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tKey_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUTTONSCRIPTRAPA_T1321666730_H
#ifndef HELPLINE_T2975842282_H
#define HELPLINE_T2975842282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpLine
struct  HelpLine_t2975842282  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject HelpLine::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(HelpLine_t2975842282, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPLINE_T2975842282_H
#ifndef FINDTRIANGLES_T78342577_H
#define FINDTRIANGLES_T78342577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FindTriangles
struct  FindTriangles_t78342577  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.MeshCollider[] FindTriangles::glist
	MeshColliderU5BU5D_t1888750571* ___glist_2;

public:
	inline static int32_t get_offset_of_glist_2() { return static_cast<int32_t>(offsetof(FindTriangles_t78342577, ___glist_2)); }
	inline MeshColliderU5BU5D_t1888750571* get_glist_2() const { return ___glist_2; }
	inline MeshColliderU5BU5D_t1888750571** get_address_of_glist_2() { return &___glist_2; }
	inline void set_glist_2(MeshColliderU5BU5D_t1888750571* value)
	{
		___glist_2 = value;
		Il2CppCodeGenWriteBarrier((&___glist_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDTRIANGLES_T78342577_H
#ifndef GLOBAL_T3489172580_H
#define GLOBAL_T3489172580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Global
struct  Global_t3489172580  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin Global::m_skin
	GUISkin_t2122630221 * ___m_skin_2;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(Global_t3489172580, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}
};

struct Global_t3489172580_StaticFields
{
public:
	// Global Global::instanceRef
	Global_t3489172580 * ___instanceRef_3;
	// System.String Global::privateInteractNPC
	String_t* ___privateInteractNPC_4;
	// System.String Global::m_sCurrentFakeNPC
	String_t* ___m_sCurrentFakeNPC_5;
	// System.String Global::CurrentPlayerName
	String_t* ___CurrentPlayerName_6;
	// System.Boolean Global::GemObtainedForTheLevel
	bool ___GemObtainedForTheLevel_7;
	// System.Int32 Global::m_L6CurrentDoorNum
	int32_t ___m_L6CurrentDoorNum_8;
	// System.String Global::TempTalkCharacter
	String_t* ___TempTalkCharacter_9;
	// System.Boolean Global::m_bGotCurrentLevelGem
	bool ___m_bGotCurrentLevelGem_10;
	// System.Boolean Global::LevelSVisited
	bool ___LevelSVisited_11;
	// System.Int32 Global::currentLevelNumber
	int32_t ___currentLevelNumber_12;
	// System.String Global::PreviousSceneName
	String_t* ___PreviousSceneName_13;
	// System.Boolean Global::m_bGemStartsRaising
	bool ___m_bGemStartsRaising_14;
	// System.Boolean Global::m_bGemIsFullyReleased
	bool ___m_bGemIsFullyReleased_15;
	// UnityEngine.GameObject[] Global::GUIObjects
	GameObjectU5BU5D_t2988620542* ___GUIObjects_16;
	// System.Int32 Global::userCurrentLevelNum
	int32_t ___userCurrentLevelNum_17;
	// System.Int32[] Global::userFeedBackScore
	Int32U5BU5D_t1965588061* ___userFeedBackScore_18;
	// System.Int32 Global::userHairStyleNum
	int32_t ___userHairStyleNum_19;
	// UnityEngine.Color Global::userHairColor
	Color_t2582018970  ___userHairColor_20;
	// UnityEngine.Color Global::userSkinColor
	Color_t2582018970  ___userSkinColor_21;
	// UnityEngine.Color Global::userEyeColor
	Color_t2582018970  ___userEyeColor_22;
	// UnityEngine.Color Global::userClothColor
	Color_t2582018970  ___userClothColor_23;
	// System.Int32 Global::userTrimTextureNum
	int32_t ___userTrimTextureNum_24;
	// UnityEngine.Color Global::userTrimColor
	Color_t2582018970  ___userTrimColor_25;
	// System.Int32 Global::userBackPackNum
	int32_t ___userBackPackNum_26;
	// System.Int32 Global::userBodyType
	int32_t ___userBodyType_27;
	// System.String[] Global::userL1NoteBookDataBlocks
	StringU5BU5D_t2511808107* ___userL1NoteBookDataBlocks_28;
	// System.String[] Global::userL1NoteBookManualBlocks
	StringU5BU5D_t2511808107* ___userL1NoteBookManualBlocks_29;
	// System.String[] Global::userL2NoteBookDataBlocks
	StringU5BU5D_t2511808107* ___userL2NoteBookDataBlocks_30;
	// System.String[] Global::userL2NoteBookManualBlocks
	StringU5BU5D_t2511808107* ___userL2NoteBookManualBlocks_31;
	// System.String[] Global::userL3NoteBookDataBlockOne
	StringU5BU5D_t2511808107* ___userL3NoteBookDataBlockOne_32;
	// System.String[] Global::userL3NoteBookDataBlockTwo
	StringU5BU5D_t2511808107* ___userL3NoteBookDataBlockTwo_33;
	// System.String[] Global::userL3NoteBookDataBlockThree
	StringU5BU5D_t2511808107* ___userL3NoteBookDataBlockThree_34;
	// System.String[] Global::userL4NoteBookDataBlockOne
	StringU5BU5D_t2511808107* ___userL4NoteBookDataBlockOne_35;
	// System.String[] Global::userL4NoteBookDataBlockTwo
	StringU5BU5D_t2511808107* ___userL4NoteBookDataBlockTwo_36;
	// System.String[] Global::userL4NoteBookStepsBlocks
	StringU5BU5D_t2511808107* ___userL4NoteBookStepsBlocks_37;
	// System.String[] Global::userL5NoteBookDataBlocks
	StringU5BU5D_t2511808107* ___userL5NoteBookDataBlocks_38;
	// System.String[] Global::userL5NoteBookManualBlocks
	StringU5BU5D_t2511808107* ___userL5NoteBookManualBlocks_39;
	// System.String[] Global::userL6NoteBookDataBlocks
	StringU5BU5D_t2511808107* ___userL6NoteBookDataBlocks_40;
	// System.String[] Global::userL6NoteBookManualBlocks
	StringU5BU5D_t2511808107* ___userL6NoteBookManualBlocks_41;
	// System.String[] Global::userL6NoteBookRapaBlocks
	StringU5BU5D_t2511808107* ___userL6NoteBookRapaBlocks_42;
	// System.String[] Global::userL7NoteBookDataBlocks
	StringU5BU5D_t2511808107* ___userL7NoteBookDataBlocks_43;
	// System.String Global::lastLevelNoteBookData
	String_t* ___lastLevelNoteBookData_44;
	// System.String Global::m_giveAwayNPCName
	String_t* ___m_giveAwayNPCName_45;
	// System.String Global::m_giveAwayAniName
	String_t* ___m_giveAwayAniName_46;
	// System.String Global::m_giveAwayObjName
	String_t* ___m_giveAwayObjName_47;
	// UnityEngine.Vector3 Global::m_giveAwayObjAppearPos
	Vector3_t1986933152  ___m_giveAwayObjAppearPos_48;
	// System.Boolean Global::m_gObjectStartGivingAway
	bool ___m_gObjectStartGivingAway_49;
	// System.String Global::m_pickNPCName
	String_t* ___m_pickNPCName_50;
	// System.String Global::m_PickAniName
	String_t* ___m_PickAniName_51;
	// System.Boolean Global::m_gObjectTakingStart
	bool ___m_gObjectTakingStart_52;
	// System.Boolean Global::m_ObjectTaken
	bool ___m_ObjectTaken_53;
	// System.String Global::multiChoice
	String_t* ___multiChoice_54;
	// System.String Global::arrowKey
	String_t* ___arrowKey_55;
	// System.Boolean Global::playerReadyCheck
	bool ___playerReadyCheck_56;
	// System.String Global::m_sPrivateSavePoint
	String_t* ___m_sPrivateSavePoint_57;
	// System.String Global::tuiScene
	String_t* ___tuiScene_58;
	// System.Boolean Global::m_bTalkedWithMentor
	bool ___m_bTalkedWithMentor_59;
	// System.Boolean Global::toggleGUI
	bool ___toggleGUI_60;
	// System.Boolean Global::levelComplete
	bool ___levelComplete_61;
	// System.Int32 Global::savePointLoadLevelNumber
	int32_t ___savePointLoadLevelNumber_62;
	// System.String Global::loadedSavePoint
	String_t* ___loadedSavePoint_63;
	// System.Boolean Global::levelStarted
	bool ___levelStarted_64;
	// UnityEngine.Vector3 Global::loadPos
	Vector3_t1986933152  ___loadPos_65;
	// System.Boolean Global::bsavePointToServerSuccess
	bool ___bsavePointToServerSuccess_66;
	// System.String Global::sSavePointToServerURL
	String_t* ___sSavePointToServerURL_67;
	// System.Boolean Global::m_bPlayerAcceptHelp
	bool ___m_bPlayerAcceptHelp_68;
	// System.Boolean Global::m_bPlayerAfterFire
	bool ___m_bPlayerAfterFire_69;
	// System.Boolean Global::TestingOnline
	bool ___TestingOnline_70;
	// System.Int32 Global::TestProgress
	int32_t ___TestProgress_71;
	// System.String Global::TestSavePoint
	String_t* ___TestSavePoint_72;
	// System.String Global::versionNumber
	String_t* ___versionNumber_73;
	// System.Boolean Global::Global_bshowVersionOnScreen
	bool ___Global_bshowVersionOnScreen_74;
	// System.Boolean Global::TestingRetinaRes
	bool ___TestingRetinaRes_75;
	// System.String[] Global::questionName
	StringU5BU5D_t2511808107* ___questionName_76;
	// System.String Global::LastResponseFromServer
	String_t* ___LastResponseFromServer_77;
	// System.Single Global::ScreenHeight_Factor
	float ___ScreenHeight_Factor_78;
	// System.Single Global::ScreenWidth_Factor
	float ___ScreenWidth_Factor_79;
	// System.Single Global::DPI_Factor
	float ___DPI_Factor_80;
	// System.Boolean Global::Level2FireguyInteraction
	bool ___Level2FireguyInteraction_81;
	// System.String[] Global::debugControls
	StringU5BU5D_t2511808107* ___debugControls_82;
	// System.Int32 Global::LogID
	int32_t ___LogID_83;
	// System.String Global::LastEntry
	String_t* ___LastEntry_84;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Global::<>f__switch$map2
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2_85;

public:
	inline static int32_t get_offset_of_instanceRef_3() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___instanceRef_3)); }
	inline Global_t3489172580 * get_instanceRef_3() const { return ___instanceRef_3; }
	inline Global_t3489172580 ** get_address_of_instanceRef_3() { return &___instanceRef_3; }
	inline void set_instanceRef_3(Global_t3489172580 * value)
	{
		___instanceRef_3 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_3), value);
	}

	inline static int32_t get_offset_of_privateInteractNPC_4() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___privateInteractNPC_4)); }
	inline String_t* get_privateInteractNPC_4() const { return ___privateInteractNPC_4; }
	inline String_t** get_address_of_privateInteractNPC_4() { return &___privateInteractNPC_4; }
	inline void set_privateInteractNPC_4(String_t* value)
	{
		___privateInteractNPC_4 = value;
		Il2CppCodeGenWriteBarrier((&___privateInteractNPC_4), value);
	}

	inline static int32_t get_offset_of_m_sCurrentFakeNPC_5() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_sCurrentFakeNPC_5)); }
	inline String_t* get_m_sCurrentFakeNPC_5() const { return ___m_sCurrentFakeNPC_5; }
	inline String_t** get_address_of_m_sCurrentFakeNPC_5() { return &___m_sCurrentFakeNPC_5; }
	inline void set_m_sCurrentFakeNPC_5(String_t* value)
	{
		___m_sCurrentFakeNPC_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sCurrentFakeNPC_5), value);
	}

	inline static int32_t get_offset_of_CurrentPlayerName_6() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___CurrentPlayerName_6)); }
	inline String_t* get_CurrentPlayerName_6() const { return ___CurrentPlayerName_6; }
	inline String_t** get_address_of_CurrentPlayerName_6() { return &___CurrentPlayerName_6; }
	inline void set_CurrentPlayerName_6(String_t* value)
	{
		___CurrentPlayerName_6 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentPlayerName_6), value);
	}

	inline static int32_t get_offset_of_GemObtainedForTheLevel_7() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___GemObtainedForTheLevel_7)); }
	inline bool get_GemObtainedForTheLevel_7() const { return ___GemObtainedForTheLevel_7; }
	inline bool* get_address_of_GemObtainedForTheLevel_7() { return &___GemObtainedForTheLevel_7; }
	inline void set_GemObtainedForTheLevel_7(bool value)
	{
		___GemObtainedForTheLevel_7 = value;
	}

	inline static int32_t get_offset_of_m_L6CurrentDoorNum_8() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_L6CurrentDoorNum_8)); }
	inline int32_t get_m_L6CurrentDoorNum_8() const { return ___m_L6CurrentDoorNum_8; }
	inline int32_t* get_address_of_m_L6CurrentDoorNum_8() { return &___m_L6CurrentDoorNum_8; }
	inline void set_m_L6CurrentDoorNum_8(int32_t value)
	{
		___m_L6CurrentDoorNum_8 = value;
	}

	inline static int32_t get_offset_of_TempTalkCharacter_9() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___TempTalkCharacter_9)); }
	inline String_t* get_TempTalkCharacter_9() const { return ___TempTalkCharacter_9; }
	inline String_t** get_address_of_TempTalkCharacter_9() { return &___TempTalkCharacter_9; }
	inline void set_TempTalkCharacter_9(String_t* value)
	{
		___TempTalkCharacter_9 = value;
		Il2CppCodeGenWriteBarrier((&___TempTalkCharacter_9), value);
	}

	inline static int32_t get_offset_of_m_bGotCurrentLevelGem_10() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bGotCurrentLevelGem_10)); }
	inline bool get_m_bGotCurrentLevelGem_10() const { return ___m_bGotCurrentLevelGem_10; }
	inline bool* get_address_of_m_bGotCurrentLevelGem_10() { return &___m_bGotCurrentLevelGem_10; }
	inline void set_m_bGotCurrentLevelGem_10(bool value)
	{
		___m_bGotCurrentLevelGem_10 = value;
	}

	inline static int32_t get_offset_of_LevelSVisited_11() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___LevelSVisited_11)); }
	inline bool get_LevelSVisited_11() const { return ___LevelSVisited_11; }
	inline bool* get_address_of_LevelSVisited_11() { return &___LevelSVisited_11; }
	inline void set_LevelSVisited_11(bool value)
	{
		___LevelSVisited_11 = value;
	}

	inline static int32_t get_offset_of_currentLevelNumber_12() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___currentLevelNumber_12)); }
	inline int32_t get_currentLevelNumber_12() const { return ___currentLevelNumber_12; }
	inline int32_t* get_address_of_currentLevelNumber_12() { return &___currentLevelNumber_12; }
	inline void set_currentLevelNumber_12(int32_t value)
	{
		___currentLevelNumber_12 = value;
	}

	inline static int32_t get_offset_of_PreviousSceneName_13() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___PreviousSceneName_13)); }
	inline String_t* get_PreviousSceneName_13() const { return ___PreviousSceneName_13; }
	inline String_t** get_address_of_PreviousSceneName_13() { return &___PreviousSceneName_13; }
	inline void set_PreviousSceneName_13(String_t* value)
	{
		___PreviousSceneName_13 = value;
		Il2CppCodeGenWriteBarrier((&___PreviousSceneName_13), value);
	}

	inline static int32_t get_offset_of_m_bGemStartsRaising_14() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bGemStartsRaising_14)); }
	inline bool get_m_bGemStartsRaising_14() const { return ___m_bGemStartsRaising_14; }
	inline bool* get_address_of_m_bGemStartsRaising_14() { return &___m_bGemStartsRaising_14; }
	inline void set_m_bGemStartsRaising_14(bool value)
	{
		___m_bGemStartsRaising_14 = value;
	}

	inline static int32_t get_offset_of_m_bGemIsFullyReleased_15() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bGemIsFullyReleased_15)); }
	inline bool get_m_bGemIsFullyReleased_15() const { return ___m_bGemIsFullyReleased_15; }
	inline bool* get_address_of_m_bGemIsFullyReleased_15() { return &___m_bGemIsFullyReleased_15; }
	inline void set_m_bGemIsFullyReleased_15(bool value)
	{
		___m_bGemIsFullyReleased_15 = value;
	}

	inline static int32_t get_offset_of_GUIObjects_16() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___GUIObjects_16)); }
	inline GameObjectU5BU5D_t2988620542* get_GUIObjects_16() const { return ___GUIObjects_16; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_GUIObjects_16() { return &___GUIObjects_16; }
	inline void set_GUIObjects_16(GameObjectU5BU5D_t2988620542* value)
	{
		___GUIObjects_16 = value;
		Il2CppCodeGenWriteBarrier((&___GUIObjects_16), value);
	}

	inline static int32_t get_offset_of_userCurrentLevelNum_17() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userCurrentLevelNum_17)); }
	inline int32_t get_userCurrentLevelNum_17() const { return ___userCurrentLevelNum_17; }
	inline int32_t* get_address_of_userCurrentLevelNum_17() { return &___userCurrentLevelNum_17; }
	inline void set_userCurrentLevelNum_17(int32_t value)
	{
		___userCurrentLevelNum_17 = value;
	}

	inline static int32_t get_offset_of_userFeedBackScore_18() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userFeedBackScore_18)); }
	inline Int32U5BU5D_t1965588061* get_userFeedBackScore_18() const { return ___userFeedBackScore_18; }
	inline Int32U5BU5D_t1965588061** get_address_of_userFeedBackScore_18() { return &___userFeedBackScore_18; }
	inline void set_userFeedBackScore_18(Int32U5BU5D_t1965588061* value)
	{
		___userFeedBackScore_18 = value;
		Il2CppCodeGenWriteBarrier((&___userFeedBackScore_18), value);
	}

	inline static int32_t get_offset_of_userHairStyleNum_19() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userHairStyleNum_19)); }
	inline int32_t get_userHairStyleNum_19() const { return ___userHairStyleNum_19; }
	inline int32_t* get_address_of_userHairStyleNum_19() { return &___userHairStyleNum_19; }
	inline void set_userHairStyleNum_19(int32_t value)
	{
		___userHairStyleNum_19 = value;
	}

	inline static int32_t get_offset_of_userHairColor_20() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userHairColor_20)); }
	inline Color_t2582018970  get_userHairColor_20() const { return ___userHairColor_20; }
	inline Color_t2582018970 * get_address_of_userHairColor_20() { return &___userHairColor_20; }
	inline void set_userHairColor_20(Color_t2582018970  value)
	{
		___userHairColor_20 = value;
	}

	inline static int32_t get_offset_of_userSkinColor_21() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userSkinColor_21)); }
	inline Color_t2582018970  get_userSkinColor_21() const { return ___userSkinColor_21; }
	inline Color_t2582018970 * get_address_of_userSkinColor_21() { return &___userSkinColor_21; }
	inline void set_userSkinColor_21(Color_t2582018970  value)
	{
		___userSkinColor_21 = value;
	}

	inline static int32_t get_offset_of_userEyeColor_22() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userEyeColor_22)); }
	inline Color_t2582018970  get_userEyeColor_22() const { return ___userEyeColor_22; }
	inline Color_t2582018970 * get_address_of_userEyeColor_22() { return &___userEyeColor_22; }
	inline void set_userEyeColor_22(Color_t2582018970  value)
	{
		___userEyeColor_22 = value;
	}

	inline static int32_t get_offset_of_userClothColor_23() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userClothColor_23)); }
	inline Color_t2582018970  get_userClothColor_23() const { return ___userClothColor_23; }
	inline Color_t2582018970 * get_address_of_userClothColor_23() { return &___userClothColor_23; }
	inline void set_userClothColor_23(Color_t2582018970  value)
	{
		___userClothColor_23 = value;
	}

	inline static int32_t get_offset_of_userTrimTextureNum_24() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userTrimTextureNum_24)); }
	inline int32_t get_userTrimTextureNum_24() const { return ___userTrimTextureNum_24; }
	inline int32_t* get_address_of_userTrimTextureNum_24() { return &___userTrimTextureNum_24; }
	inline void set_userTrimTextureNum_24(int32_t value)
	{
		___userTrimTextureNum_24 = value;
	}

	inline static int32_t get_offset_of_userTrimColor_25() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userTrimColor_25)); }
	inline Color_t2582018970  get_userTrimColor_25() const { return ___userTrimColor_25; }
	inline Color_t2582018970 * get_address_of_userTrimColor_25() { return &___userTrimColor_25; }
	inline void set_userTrimColor_25(Color_t2582018970  value)
	{
		___userTrimColor_25 = value;
	}

	inline static int32_t get_offset_of_userBackPackNum_26() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userBackPackNum_26)); }
	inline int32_t get_userBackPackNum_26() const { return ___userBackPackNum_26; }
	inline int32_t* get_address_of_userBackPackNum_26() { return &___userBackPackNum_26; }
	inline void set_userBackPackNum_26(int32_t value)
	{
		___userBackPackNum_26 = value;
	}

	inline static int32_t get_offset_of_userBodyType_27() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userBodyType_27)); }
	inline int32_t get_userBodyType_27() const { return ___userBodyType_27; }
	inline int32_t* get_address_of_userBodyType_27() { return &___userBodyType_27; }
	inline void set_userBodyType_27(int32_t value)
	{
		___userBodyType_27 = value;
	}

	inline static int32_t get_offset_of_userL1NoteBookDataBlocks_28() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL1NoteBookDataBlocks_28)); }
	inline StringU5BU5D_t2511808107* get_userL1NoteBookDataBlocks_28() const { return ___userL1NoteBookDataBlocks_28; }
	inline StringU5BU5D_t2511808107** get_address_of_userL1NoteBookDataBlocks_28() { return &___userL1NoteBookDataBlocks_28; }
	inline void set_userL1NoteBookDataBlocks_28(StringU5BU5D_t2511808107* value)
	{
		___userL1NoteBookDataBlocks_28 = value;
		Il2CppCodeGenWriteBarrier((&___userL1NoteBookDataBlocks_28), value);
	}

	inline static int32_t get_offset_of_userL1NoteBookManualBlocks_29() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL1NoteBookManualBlocks_29)); }
	inline StringU5BU5D_t2511808107* get_userL1NoteBookManualBlocks_29() const { return ___userL1NoteBookManualBlocks_29; }
	inline StringU5BU5D_t2511808107** get_address_of_userL1NoteBookManualBlocks_29() { return &___userL1NoteBookManualBlocks_29; }
	inline void set_userL1NoteBookManualBlocks_29(StringU5BU5D_t2511808107* value)
	{
		___userL1NoteBookManualBlocks_29 = value;
		Il2CppCodeGenWriteBarrier((&___userL1NoteBookManualBlocks_29), value);
	}

	inline static int32_t get_offset_of_userL2NoteBookDataBlocks_30() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL2NoteBookDataBlocks_30)); }
	inline StringU5BU5D_t2511808107* get_userL2NoteBookDataBlocks_30() const { return ___userL2NoteBookDataBlocks_30; }
	inline StringU5BU5D_t2511808107** get_address_of_userL2NoteBookDataBlocks_30() { return &___userL2NoteBookDataBlocks_30; }
	inline void set_userL2NoteBookDataBlocks_30(StringU5BU5D_t2511808107* value)
	{
		___userL2NoteBookDataBlocks_30 = value;
		Il2CppCodeGenWriteBarrier((&___userL2NoteBookDataBlocks_30), value);
	}

	inline static int32_t get_offset_of_userL2NoteBookManualBlocks_31() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL2NoteBookManualBlocks_31)); }
	inline StringU5BU5D_t2511808107* get_userL2NoteBookManualBlocks_31() const { return ___userL2NoteBookManualBlocks_31; }
	inline StringU5BU5D_t2511808107** get_address_of_userL2NoteBookManualBlocks_31() { return &___userL2NoteBookManualBlocks_31; }
	inline void set_userL2NoteBookManualBlocks_31(StringU5BU5D_t2511808107* value)
	{
		___userL2NoteBookManualBlocks_31 = value;
		Il2CppCodeGenWriteBarrier((&___userL2NoteBookManualBlocks_31), value);
	}

	inline static int32_t get_offset_of_userL3NoteBookDataBlockOne_32() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL3NoteBookDataBlockOne_32)); }
	inline StringU5BU5D_t2511808107* get_userL3NoteBookDataBlockOne_32() const { return ___userL3NoteBookDataBlockOne_32; }
	inline StringU5BU5D_t2511808107** get_address_of_userL3NoteBookDataBlockOne_32() { return &___userL3NoteBookDataBlockOne_32; }
	inline void set_userL3NoteBookDataBlockOne_32(StringU5BU5D_t2511808107* value)
	{
		___userL3NoteBookDataBlockOne_32 = value;
		Il2CppCodeGenWriteBarrier((&___userL3NoteBookDataBlockOne_32), value);
	}

	inline static int32_t get_offset_of_userL3NoteBookDataBlockTwo_33() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL3NoteBookDataBlockTwo_33)); }
	inline StringU5BU5D_t2511808107* get_userL3NoteBookDataBlockTwo_33() const { return ___userL3NoteBookDataBlockTwo_33; }
	inline StringU5BU5D_t2511808107** get_address_of_userL3NoteBookDataBlockTwo_33() { return &___userL3NoteBookDataBlockTwo_33; }
	inline void set_userL3NoteBookDataBlockTwo_33(StringU5BU5D_t2511808107* value)
	{
		___userL3NoteBookDataBlockTwo_33 = value;
		Il2CppCodeGenWriteBarrier((&___userL3NoteBookDataBlockTwo_33), value);
	}

	inline static int32_t get_offset_of_userL3NoteBookDataBlockThree_34() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL3NoteBookDataBlockThree_34)); }
	inline StringU5BU5D_t2511808107* get_userL3NoteBookDataBlockThree_34() const { return ___userL3NoteBookDataBlockThree_34; }
	inline StringU5BU5D_t2511808107** get_address_of_userL3NoteBookDataBlockThree_34() { return &___userL3NoteBookDataBlockThree_34; }
	inline void set_userL3NoteBookDataBlockThree_34(StringU5BU5D_t2511808107* value)
	{
		___userL3NoteBookDataBlockThree_34 = value;
		Il2CppCodeGenWriteBarrier((&___userL3NoteBookDataBlockThree_34), value);
	}

	inline static int32_t get_offset_of_userL4NoteBookDataBlockOne_35() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL4NoteBookDataBlockOne_35)); }
	inline StringU5BU5D_t2511808107* get_userL4NoteBookDataBlockOne_35() const { return ___userL4NoteBookDataBlockOne_35; }
	inline StringU5BU5D_t2511808107** get_address_of_userL4NoteBookDataBlockOne_35() { return &___userL4NoteBookDataBlockOne_35; }
	inline void set_userL4NoteBookDataBlockOne_35(StringU5BU5D_t2511808107* value)
	{
		___userL4NoteBookDataBlockOne_35 = value;
		Il2CppCodeGenWriteBarrier((&___userL4NoteBookDataBlockOne_35), value);
	}

	inline static int32_t get_offset_of_userL4NoteBookDataBlockTwo_36() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL4NoteBookDataBlockTwo_36)); }
	inline StringU5BU5D_t2511808107* get_userL4NoteBookDataBlockTwo_36() const { return ___userL4NoteBookDataBlockTwo_36; }
	inline StringU5BU5D_t2511808107** get_address_of_userL4NoteBookDataBlockTwo_36() { return &___userL4NoteBookDataBlockTwo_36; }
	inline void set_userL4NoteBookDataBlockTwo_36(StringU5BU5D_t2511808107* value)
	{
		___userL4NoteBookDataBlockTwo_36 = value;
		Il2CppCodeGenWriteBarrier((&___userL4NoteBookDataBlockTwo_36), value);
	}

	inline static int32_t get_offset_of_userL4NoteBookStepsBlocks_37() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL4NoteBookStepsBlocks_37)); }
	inline StringU5BU5D_t2511808107* get_userL4NoteBookStepsBlocks_37() const { return ___userL4NoteBookStepsBlocks_37; }
	inline StringU5BU5D_t2511808107** get_address_of_userL4NoteBookStepsBlocks_37() { return &___userL4NoteBookStepsBlocks_37; }
	inline void set_userL4NoteBookStepsBlocks_37(StringU5BU5D_t2511808107* value)
	{
		___userL4NoteBookStepsBlocks_37 = value;
		Il2CppCodeGenWriteBarrier((&___userL4NoteBookStepsBlocks_37), value);
	}

	inline static int32_t get_offset_of_userL5NoteBookDataBlocks_38() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL5NoteBookDataBlocks_38)); }
	inline StringU5BU5D_t2511808107* get_userL5NoteBookDataBlocks_38() const { return ___userL5NoteBookDataBlocks_38; }
	inline StringU5BU5D_t2511808107** get_address_of_userL5NoteBookDataBlocks_38() { return &___userL5NoteBookDataBlocks_38; }
	inline void set_userL5NoteBookDataBlocks_38(StringU5BU5D_t2511808107* value)
	{
		___userL5NoteBookDataBlocks_38 = value;
		Il2CppCodeGenWriteBarrier((&___userL5NoteBookDataBlocks_38), value);
	}

	inline static int32_t get_offset_of_userL5NoteBookManualBlocks_39() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL5NoteBookManualBlocks_39)); }
	inline StringU5BU5D_t2511808107* get_userL5NoteBookManualBlocks_39() const { return ___userL5NoteBookManualBlocks_39; }
	inline StringU5BU5D_t2511808107** get_address_of_userL5NoteBookManualBlocks_39() { return &___userL5NoteBookManualBlocks_39; }
	inline void set_userL5NoteBookManualBlocks_39(StringU5BU5D_t2511808107* value)
	{
		___userL5NoteBookManualBlocks_39 = value;
		Il2CppCodeGenWriteBarrier((&___userL5NoteBookManualBlocks_39), value);
	}

	inline static int32_t get_offset_of_userL6NoteBookDataBlocks_40() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL6NoteBookDataBlocks_40)); }
	inline StringU5BU5D_t2511808107* get_userL6NoteBookDataBlocks_40() const { return ___userL6NoteBookDataBlocks_40; }
	inline StringU5BU5D_t2511808107** get_address_of_userL6NoteBookDataBlocks_40() { return &___userL6NoteBookDataBlocks_40; }
	inline void set_userL6NoteBookDataBlocks_40(StringU5BU5D_t2511808107* value)
	{
		___userL6NoteBookDataBlocks_40 = value;
		Il2CppCodeGenWriteBarrier((&___userL6NoteBookDataBlocks_40), value);
	}

	inline static int32_t get_offset_of_userL6NoteBookManualBlocks_41() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL6NoteBookManualBlocks_41)); }
	inline StringU5BU5D_t2511808107* get_userL6NoteBookManualBlocks_41() const { return ___userL6NoteBookManualBlocks_41; }
	inline StringU5BU5D_t2511808107** get_address_of_userL6NoteBookManualBlocks_41() { return &___userL6NoteBookManualBlocks_41; }
	inline void set_userL6NoteBookManualBlocks_41(StringU5BU5D_t2511808107* value)
	{
		___userL6NoteBookManualBlocks_41 = value;
		Il2CppCodeGenWriteBarrier((&___userL6NoteBookManualBlocks_41), value);
	}

	inline static int32_t get_offset_of_userL6NoteBookRapaBlocks_42() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL6NoteBookRapaBlocks_42)); }
	inline StringU5BU5D_t2511808107* get_userL6NoteBookRapaBlocks_42() const { return ___userL6NoteBookRapaBlocks_42; }
	inline StringU5BU5D_t2511808107** get_address_of_userL6NoteBookRapaBlocks_42() { return &___userL6NoteBookRapaBlocks_42; }
	inline void set_userL6NoteBookRapaBlocks_42(StringU5BU5D_t2511808107* value)
	{
		___userL6NoteBookRapaBlocks_42 = value;
		Il2CppCodeGenWriteBarrier((&___userL6NoteBookRapaBlocks_42), value);
	}

	inline static int32_t get_offset_of_userL7NoteBookDataBlocks_43() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___userL7NoteBookDataBlocks_43)); }
	inline StringU5BU5D_t2511808107* get_userL7NoteBookDataBlocks_43() const { return ___userL7NoteBookDataBlocks_43; }
	inline StringU5BU5D_t2511808107** get_address_of_userL7NoteBookDataBlocks_43() { return &___userL7NoteBookDataBlocks_43; }
	inline void set_userL7NoteBookDataBlocks_43(StringU5BU5D_t2511808107* value)
	{
		___userL7NoteBookDataBlocks_43 = value;
		Il2CppCodeGenWriteBarrier((&___userL7NoteBookDataBlocks_43), value);
	}

	inline static int32_t get_offset_of_lastLevelNoteBookData_44() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___lastLevelNoteBookData_44)); }
	inline String_t* get_lastLevelNoteBookData_44() const { return ___lastLevelNoteBookData_44; }
	inline String_t** get_address_of_lastLevelNoteBookData_44() { return &___lastLevelNoteBookData_44; }
	inline void set_lastLevelNoteBookData_44(String_t* value)
	{
		___lastLevelNoteBookData_44 = value;
		Il2CppCodeGenWriteBarrier((&___lastLevelNoteBookData_44), value);
	}

	inline static int32_t get_offset_of_m_giveAwayNPCName_45() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_giveAwayNPCName_45)); }
	inline String_t* get_m_giveAwayNPCName_45() const { return ___m_giveAwayNPCName_45; }
	inline String_t** get_address_of_m_giveAwayNPCName_45() { return &___m_giveAwayNPCName_45; }
	inline void set_m_giveAwayNPCName_45(String_t* value)
	{
		___m_giveAwayNPCName_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_giveAwayNPCName_45), value);
	}

	inline static int32_t get_offset_of_m_giveAwayAniName_46() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_giveAwayAniName_46)); }
	inline String_t* get_m_giveAwayAniName_46() const { return ___m_giveAwayAniName_46; }
	inline String_t** get_address_of_m_giveAwayAniName_46() { return &___m_giveAwayAniName_46; }
	inline void set_m_giveAwayAniName_46(String_t* value)
	{
		___m_giveAwayAniName_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_giveAwayAniName_46), value);
	}

	inline static int32_t get_offset_of_m_giveAwayObjName_47() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_giveAwayObjName_47)); }
	inline String_t* get_m_giveAwayObjName_47() const { return ___m_giveAwayObjName_47; }
	inline String_t** get_address_of_m_giveAwayObjName_47() { return &___m_giveAwayObjName_47; }
	inline void set_m_giveAwayObjName_47(String_t* value)
	{
		___m_giveAwayObjName_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_giveAwayObjName_47), value);
	}

	inline static int32_t get_offset_of_m_giveAwayObjAppearPos_48() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_giveAwayObjAppearPos_48)); }
	inline Vector3_t1986933152  get_m_giveAwayObjAppearPos_48() const { return ___m_giveAwayObjAppearPos_48; }
	inline Vector3_t1986933152 * get_address_of_m_giveAwayObjAppearPos_48() { return &___m_giveAwayObjAppearPos_48; }
	inline void set_m_giveAwayObjAppearPos_48(Vector3_t1986933152  value)
	{
		___m_giveAwayObjAppearPos_48 = value;
	}

	inline static int32_t get_offset_of_m_gObjectStartGivingAway_49() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_gObjectStartGivingAway_49)); }
	inline bool get_m_gObjectStartGivingAway_49() const { return ___m_gObjectStartGivingAway_49; }
	inline bool* get_address_of_m_gObjectStartGivingAway_49() { return &___m_gObjectStartGivingAway_49; }
	inline void set_m_gObjectStartGivingAway_49(bool value)
	{
		___m_gObjectStartGivingAway_49 = value;
	}

	inline static int32_t get_offset_of_m_pickNPCName_50() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_pickNPCName_50)); }
	inline String_t* get_m_pickNPCName_50() const { return ___m_pickNPCName_50; }
	inline String_t** get_address_of_m_pickNPCName_50() { return &___m_pickNPCName_50; }
	inline void set_m_pickNPCName_50(String_t* value)
	{
		___m_pickNPCName_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_pickNPCName_50), value);
	}

	inline static int32_t get_offset_of_m_PickAniName_51() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_PickAniName_51)); }
	inline String_t* get_m_PickAniName_51() const { return ___m_PickAniName_51; }
	inline String_t** get_address_of_m_PickAniName_51() { return &___m_PickAniName_51; }
	inline void set_m_PickAniName_51(String_t* value)
	{
		___m_PickAniName_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_PickAniName_51), value);
	}

	inline static int32_t get_offset_of_m_gObjectTakingStart_52() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_gObjectTakingStart_52)); }
	inline bool get_m_gObjectTakingStart_52() const { return ___m_gObjectTakingStart_52; }
	inline bool* get_address_of_m_gObjectTakingStart_52() { return &___m_gObjectTakingStart_52; }
	inline void set_m_gObjectTakingStart_52(bool value)
	{
		___m_gObjectTakingStart_52 = value;
	}

	inline static int32_t get_offset_of_m_ObjectTaken_53() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_ObjectTaken_53)); }
	inline bool get_m_ObjectTaken_53() const { return ___m_ObjectTaken_53; }
	inline bool* get_address_of_m_ObjectTaken_53() { return &___m_ObjectTaken_53; }
	inline void set_m_ObjectTaken_53(bool value)
	{
		___m_ObjectTaken_53 = value;
	}

	inline static int32_t get_offset_of_multiChoice_54() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___multiChoice_54)); }
	inline String_t* get_multiChoice_54() const { return ___multiChoice_54; }
	inline String_t** get_address_of_multiChoice_54() { return &___multiChoice_54; }
	inline void set_multiChoice_54(String_t* value)
	{
		___multiChoice_54 = value;
		Il2CppCodeGenWriteBarrier((&___multiChoice_54), value);
	}

	inline static int32_t get_offset_of_arrowKey_55() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___arrowKey_55)); }
	inline String_t* get_arrowKey_55() const { return ___arrowKey_55; }
	inline String_t** get_address_of_arrowKey_55() { return &___arrowKey_55; }
	inline void set_arrowKey_55(String_t* value)
	{
		___arrowKey_55 = value;
		Il2CppCodeGenWriteBarrier((&___arrowKey_55), value);
	}

	inline static int32_t get_offset_of_playerReadyCheck_56() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___playerReadyCheck_56)); }
	inline bool get_playerReadyCheck_56() const { return ___playerReadyCheck_56; }
	inline bool* get_address_of_playerReadyCheck_56() { return &___playerReadyCheck_56; }
	inline void set_playerReadyCheck_56(bool value)
	{
		___playerReadyCheck_56 = value;
	}

	inline static int32_t get_offset_of_m_sPrivateSavePoint_57() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_sPrivateSavePoint_57)); }
	inline String_t* get_m_sPrivateSavePoint_57() const { return ___m_sPrivateSavePoint_57; }
	inline String_t** get_address_of_m_sPrivateSavePoint_57() { return &___m_sPrivateSavePoint_57; }
	inline void set_m_sPrivateSavePoint_57(String_t* value)
	{
		___m_sPrivateSavePoint_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_sPrivateSavePoint_57), value);
	}

	inline static int32_t get_offset_of_tuiScene_58() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___tuiScene_58)); }
	inline String_t* get_tuiScene_58() const { return ___tuiScene_58; }
	inline String_t** get_address_of_tuiScene_58() { return &___tuiScene_58; }
	inline void set_tuiScene_58(String_t* value)
	{
		___tuiScene_58 = value;
		Il2CppCodeGenWriteBarrier((&___tuiScene_58), value);
	}

	inline static int32_t get_offset_of_m_bTalkedWithMentor_59() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bTalkedWithMentor_59)); }
	inline bool get_m_bTalkedWithMentor_59() const { return ___m_bTalkedWithMentor_59; }
	inline bool* get_address_of_m_bTalkedWithMentor_59() { return &___m_bTalkedWithMentor_59; }
	inline void set_m_bTalkedWithMentor_59(bool value)
	{
		___m_bTalkedWithMentor_59 = value;
	}

	inline static int32_t get_offset_of_toggleGUI_60() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___toggleGUI_60)); }
	inline bool get_toggleGUI_60() const { return ___toggleGUI_60; }
	inline bool* get_address_of_toggleGUI_60() { return &___toggleGUI_60; }
	inline void set_toggleGUI_60(bool value)
	{
		___toggleGUI_60 = value;
	}

	inline static int32_t get_offset_of_levelComplete_61() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___levelComplete_61)); }
	inline bool get_levelComplete_61() const { return ___levelComplete_61; }
	inline bool* get_address_of_levelComplete_61() { return &___levelComplete_61; }
	inline void set_levelComplete_61(bool value)
	{
		___levelComplete_61 = value;
	}

	inline static int32_t get_offset_of_savePointLoadLevelNumber_62() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___savePointLoadLevelNumber_62)); }
	inline int32_t get_savePointLoadLevelNumber_62() const { return ___savePointLoadLevelNumber_62; }
	inline int32_t* get_address_of_savePointLoadLevelNumber_62() { return &___savePointLoadLevelNumber_62; }
	inline void set_savePointLoadLevelNumber_62(int32_t value)
	{
		___savePointLoadLevelNumber_62 = value;
	}

	inline static int32_t get_offset_of_loadedSavePoint_63() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___loadedSavePoint_63)); }
	inline String_t* get_loadedSavePoint_63() const { return ___loadedSavePoint_63; }
	inline String_t** get_address_of_loadedSavePoint_63() { return &___loadedSavePoint_63; }
	inline void set_loadedSavePoint_63(String_t* value)
	{
		___loadedSavePoint_63 = value;
		Il2CppCodeGenWriteBarrier((&___loadedSavePoint_63), value);
	}

	inline static int32_t get_offset_of_levelStarted_64() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___levelStarted_64)); }
	inline bool get_levelStarted_64() const { return ___levelStarted_64; }
	inline bool* get_address_of_levelStarted_64() { return &___levelStarted_64; }
	inline void set_levelStarted_64(bool value)
	{
		___levelStarted_64 = value;
	}

	inline static int32_t get_offset_of_loadPos_65() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___loadPos_65)); }
	inline Vector3_t1986933152  get_loadPos_65() const { return ___loadPos_65; }
	inline Vector3_t1986933152 * get_address_of_loadPos_65() { return &___loadPos_65; }
	inline void set_loadPos_65(Vector3_t1986933152  value)
	{
		___loadPos_65 = value;
	}

	inline static int32_t get_offset_of_bsavePointToServerSuccess_66() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___bsavePointToServerSuccess_66)); }
	inline bool get_bsavePointToServerSuccess_66() const { return ___bsavePointToServerSuccess_66; }
	inline bool* get_address_of_bsavePointToServerSuccess_66() { return &___bsavePointToServerSuccess_66; }
	inline void set_bsavePointToServerSuccess_66(bool value)
	{
		___bsavePointToServerSuccess_66 = value;
	}

	inline static int32_t get_offset_of_sSavePointToServerURL_67() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___sSavePointToServerURL_67)); }
	inline String_t* get_sSavePointToServerURL_67() const { return ___sSavePointToServerURL_67; }
	inline String_t** get_address_of_sSavePointToServerURL_67() { return &___sSavePointToServerURL_67; }
	inline void set_sSavePointToServerURL_67(String_t* value)
	{
		___sSavePointToServerURL_67 = value;
		Il2CppCodeGenWriteBarrier((&___sSavePointToServerURL_67), value);
	}

	inline static int32_t get_offset_of_m_bPlayerAcceptHelp_68() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bPlayerAcceptHelp_68)); }
	inline bool get_m_bPlayerAcceptHelp_68() const { return ___m_bPlayerAcceptHelp_68; }
	inline bool* get_address_of_m_bPlayerAcceptHelp_68() { return &___m_bPlayerAcceptHelp_68; }
	inline void set_m_bPlayerAcceptHelp_68(bool value)
	{
		___m_bPlayerAcceptHelp_68 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerAfterFire_69() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___m_bPlayerAfterFire_69)); }
	inline bool get_m_bPlayerAfterFire_69() const { return ___m_bPlayerAfterFire_69; }
	inline bool* get_address_of_m_bPlayerAfterFire_69() { return &___m_bPlayerAfterFire_69; }
	inline void set_m_bPlayerAfterFire_69(bool value)
	{
		___m_bPlayerAfterFire_69 = value;
	}

	inline static int32_t get_offset_of_TestingOnline_70() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___TestingOnline_70)); }
	inline bool get_TestingOnline_70() const { return ___TestingOnline_70; }
	inline bool* get_address_of_TestingOnline_70() { return &___TestingOnline_70; }
	inline void set_TestingOnline_70(bool value)
	{
		___TestingOnline_70 = value;
	}

	inline static int32_t get_offset_of_TestProgress_71() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___TestProgress_71)); }
	inline int32_t get_TestProgress_71() const { return ___TestProgress_71; }
	inline int32_t* get_address_of_TestProgress_71() { return &___TestProgress_71; }
	inline void set_TestProgress_71(int32_t value)
	{
		___TestProgress_71 = value;
	}

	inline static int32_t get_offset_of_TestSavePoint_72() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___TestSavePoint_72)); }
	inline String_t* get_TestSavePoint_72() const { return ___TestSavePoint_72; }
	inline String_t** get_address_of_TestSavePoint_72() { return &___TestSavePoint_72; }
	inline void set_TestSavePoint_72(String_t* value)
	{
		___TestSavePoint_72 = value;
		Il2CppCodeGenWriteBarrier((&___TestSavePoint_72), value);
	}

	inline static int32_t get_offset_of_versionNumber_73() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___versionNumber_73)); }
	inline String_t* get_versionNumber_73() const { return ___versionNumber_73; }
	inline String_t** get_address_of_versionNumber_73() { return &___versionNumber_73; }
	inline void set_versionNumber_73(String_t* value)
	{
		___versionNumber_73 = value;
		Il2CppCodeGenWriteBarrier((&___versionNumber_73), value);
	}

	inline static int32_t get_offset_of_Global_bshowVersionOnScreen_74() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___Global_bshowVersionOnScreen_74)); }
	inline bool get_Global_bshowVersionOnScreen_74() const { return ___Global_bshowVersionOnScreen_74; }
	inline bool* get_address_of_Global_bshowVersionOnScreen_74() { return &___Global_bshowVersionOnScreen_74; }
	inline void set_Global_bshowVersionOnScreen_74(bool value)
	{
		___Global_bshowVersionOnScreen_74 = value;
	}

	inline static int32_t get_offset_of_TestingRetinaRes_75() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___TestingRetinaRes_75)); }
	inline bool get_TestingRetinaRes_75() const { return ___TestingRetinaRes_75; }
	inline bool* get_address_of_TestingRetinaRes_75() { return &___TestingRetinaRes_75; }
	inline void set_TestingRetinaRes_75(bool value)
	{
		___TestingRetinaRes_75 = value;
	}

	inline static int32_t get_offset_of_questionName_76() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___questionName_76)); }
	inline StringU5BU5D_t2511808107* get_questionName_76() const { return ___questionName_76; }
	inline StringU5BU5D_t2511808107** get_address_of_questionName_76() { return &___questionName_76; }
	inline void set_questionName_76(StringU5BU5D_t2511808107* value)
	{
		___questionName_76 = value;
		Il2CppCodeGenWriteBarrier((&___questionName_76), value);
	}

	inline static int32_t get_offset_of_LastResponseFromServer_77() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___LastResponseFromServer_77)); }
	inline String_t* get_LastResponseFromServer_77() const { return ___LastResponseFromServer_77; }
	inline String_t** get_address_of_LastResponseFromServer_77() { return &___LastResponseFromServer_77; }
	inline void set_LastResponseFromServer_77(String_t* value)
	{
		___LastResponseFromServer_77 = value;
		Il2CppCodeGenWriteBarrier((&___LastResponseFromServer_77), value);
	}

	inline static int32_t get_offset_of_ScreenHeight_Factor_78() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___ScreenHeight_Factor_78)); }
	inline float get_ScreenHeight_Factor_78() const { return ___ScreenHeight_Factor_78; }
	inline float* get_address_of_ScreenHeight_Factor_78() { return &___ScreenHeight_Factor_78; }
	inline void set_ScreenHeight_Factor_78(float value)
	{
		___ScreenHeight_Factor_78 = value;
	}

	inline static int32_t get_offset_of_ScreenWidth_Factor_79() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___ScreenWidth_Factor_79)); }
	inline float get_ScreenWidth_Factor_79() const { return ___ScreenWidth_Factor_79; }
	inline float* get_address_of_ScreenWidth_Factor_79() { return &___ScreenWidth_Factor_79; }
	inline void set_ScreenWidth_Factor_79(float value)
	{
		___ScreenWidth_Factor_79 = value;
	}

	inline static int32_t get_offset_of_DPI_Factor_80() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___DPI_Factor_80)); }
	inline float get_DPI_Factor_80() const { return ___DPI_Factor_80; }
	inline float* get_address_of_DPI_Factor_80() { return &___DPI_Factor_80; }
	inline void set_DPI_Factor_80(float value)
	{
		___DPI_Factor_80 = value;
	}

	inline static int32_t get_offset_of_Level2FireguyInteraction_81() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___Level2FireguyInteraction_81)); }
	inline bool get_Level2FireguyInteraction_81() const { return ___Level2FireguyInteraction_81; }
	inline bool* get_address_of_Level2FireguyInteraction_81() { return &___Level2FireguyInteraction_81; }
	inline void set_Level2FireguyInteraction_81(bool value)
	{
		___Level2FireguyInteraction_81 = value;
	}

	inline static int32_t get_offset_of_debugControls_82() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___debugControls_82)); }
	inline StringU5BU5D_t2511808107* get_debugControls_82() const { return ___debugControls_82; }
	inline StringU5BU5D_t2511808107** get_address_of_debugControls_82() { return &___debugControls_82; }
	inline void set_debugControls_82(StringU5BU5D_t2511808107* value)
	{
		___debugControls_82 = value;
		Il2CppCodeGenWriteBarrier((&___debugControls_82), value);
	}

	inline static int32_t get_offset_of_LogID_83() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___LogID_83)); }
	inline int32_t get_LogID_83() const { return ___LogID_83; }
	inline int32_t* get_address_of_LogID_83() { return &___LogID_83; }
	inline void set_LogID_83(int32_t value)
	{
		___LogID_83 = value;
	}

	inline static int32_t get_offset_of_LastEntry_84() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___LastEntry_84)); }
	inline String_t* get_LastEntry_84() const { return ___LastEntry_84; }
	inline String_t** get_address_of_LastEntry_84() { return &___LastEntry_84; }
	inline void set_LastEntry_84(String_t* value)
	{
		___LastEntry_84 = value;
		Il2CppCodeGenWriteBarrier((&___LastEntry_84), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_85() { return static_cast<int32_t>(offsetof(Global_t3489172580_StaticFields, ___U3CU3Ef__switchU24map2_85)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2_85() const { return ___U3CU3Ef__switchU24map2_85; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2_85() { return &___U3CU3Ef__switchU24map2_85; }
	inline void set_U3CU3Ef__switchU24map2_85(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2_85 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_85), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBAL_T3489172580_H
#ifndef SHOWGEM_T26615769_H
#define SHOWGEM_T26615769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowGem
struct  ShowGem_t26615769  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject ShowGem::gem
	GameObject_t2557347079 * ___gem_2;
	// System.Single ShowGem::fTimer
	float ___fTimer_3;

public:
	inline static int32_t get_offset_of_gem_2() { return static_cast<int32_t>(offsetof(ShowGem_t26615769, ___gem_2)); }
	inline GameObject_t2557347079 * get_gem_2() const { return ___gem_2; }
	inline GameObject_t2557347079 ** get_address_of_gem_2() { return &___gem_2; }
	inline void set_gem_2(GameObject_t2557347079 * value)
	{
		___gem_2 = value;
		Il2CppCodeGenWriteBarrier((&___gem_2), value);
	}

	inline static int32_t get_offset_of_fTimer_3() { return static_cast<int32_t>(offsetof(ShowGem_t26615769, ___fTimer_3)); }
	inline float get_fTimer_3() const { return ___fTimer_3; }
	inline float* get_address_of_fTimer_3() { return &___fTimer_3; }
	inline void set_fTimer_3(float value)
	{
		___fTimer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWGEM_T26615769_H
#ifndef RELEASEGEM_T3020869431_H
#define RELEASEGEM_T3020869431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReleaseGem
struct  ReleaseGem_t3020869431  : public MonoBehaviour_t1618594486
{
public:
	// System.Single ReleaseGem::m_bReleaseGemRadius
	float ___m_bReleaseGemRadius_2;
	// System.Boolean ReleaseGem::m_bReadyToPutGem
	bool ___m_bReadyToPutGem_3;
	// System.Boolean ReleaseGem::m_bGemCanRelease
	bool ___m_bGemCanRelease_4;
	// System.Boolean ReleaseGem::m_bGemStartsRaising
	bool ___m_bGemStartsRaising_5;
	// System.Boolean ReleaseGem::m_bGemRaiseCompleted
	bool ___m_bGemRaiseCompleted_7;
	// UnityEngine.GameObject ReleaseGem::outlineTrigger
	GameObject_t2557347079 * ___outlineTrigger_8;
	// UnityEngine.ParticleSystem ReleaseGem::releaseGemEffect
	ParticleSystem_t2248283753 * ___releaseGemEffect_9;
	// UnityEngine.Shader ReleaseGem::shader1
	Shader_t1881769421 * ___shader1_10;
	// UnityEngine.Shader ReleaseGem::shader2
	Shader_t1881769421 * ___shader2_11;
	// UnityEngine.Renderer ReleaseGem::rend
	Renderer_t1418648713 * ___rend_12;
	// System.Single ReleaseGem::waitOver
	float ___waitOver_13;

public:
	inline static int32_t get_offset_of_m_bReleaseGemRadius_2() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___m_bReleaseGemRadius_2)); }
	inline float get_m_bReleaseGemRadius_2() const { return ___m_bReleaseGemRadius_2; }
	inline float* get_address_of_m_bReleaseGemRadius_2() { return &___m_bReleaseGemRadius_2; }
	inline void set_m_bReleaseGemRadius_2(float value)
	{
		___m_bReleaseGemRadius_2 = value;
	}

	inline static int32_t get_offset_of_m_bReadyToPutGem_3() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___m_bReadyToPutGem_3)); }
	inline bool get_m_bReadyToPutGem_3() const { return ___m_bReadyToPutGem_3; }
	inline bool* get_address_of_m_bReadyToPutGem_3() { return &___m_bReadyToPutGem_3; }
	inline void set_m_bReadyToPutGem_3(bool value)
	{
		___m_bReadyToPutGem_3 = value;
	}

	inline static int32_t get_offset_of_m_bGemCanRelease_4() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___m_bGemCanRelease_4)); }
	inline bool get_m_bGemCanRelease_4() const { return ___m_bGemCanRelease_4; }
	inline bool* get_address_of_m_bGemCanRelease_4() { return &___m_bGemCanRelease_4; }
	inline void set_m_bGemCanRelease_4(bool value)
	{
		___m_bGemCanRelease_4 = value;
	}

	inline static int32_t get_offset_of_m_bGemStartsRaising_5() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___m_bGemStartsRaising_5)); }
	inline bool get_m_bGemStartsRaising_5() const { return ___m_bGemStartsRaising_5; }
	inline bool* get_address_of_m_bGemStartsRaising_5() { return &___m_bGemStartsRaising_5; }
	inline void set_m_bGemStartsRaising_5(bool value)
	{
		___m_bGemStartsRaising_5 = value;
	}

	inline static int32_t get_offset_of_m_bGemRaiseCompleted_7() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___m_bGemRaiseCompleted_7)); }
	inline bool get_m_bGemRaiseCompleted_7() const { return ___m_bGemRaiseCompleted_7; }
	inline bool* get_address_of_m_bGemRaiseCompleted_7() { return &___m_bGemRaiseCompleted_7; }
	inline void set_m_bGemRaiseCompleted_7(bool value)
	{
		___m_bGemRaiseCompleted_7 = value;
	}

	inline static int32_t get_offset_of_outlineTrigger_8() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___outlineTrigger_8)); }
	inline GameObject_t2557347079 * get_outlineTrigger_8() const { return ___outlineTrigger_8; }
	inline GameObject_t2557347079 ** get_address_of_outlineTrigger_8() { return &___outlineTrigger_8; }
	inline void set_outlineTrigger_8(GameObject_t2557347079 * value)
	{
		___outlineTrigger_8 = value;
		Il2CppCodeGenWriteBarrier((&___outlineTrigger_8), value);
	}

	inline static int32_t get_offset_of_releaseGemEffect_9() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___releaseGemEffect_9)); }
	inline ParticleSystem_t2248283753 * get_releaseGemEffect_9() const { return ___releaseGemEffect_9; }
	inline ParticleSystem_t2248283753 ** get_address_of_releaseGemEffect_9() { return &___releaseGemEffect_9; }
	inline void set_releaseGemEffect_9(ParticleSystem_t2248283753 * value)
	{
		___releaseGemEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___releaseGemEffect_9), value);
	}

	inline static int32_t get_offset_of_shader1_10() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___shader1_10)); }
	inline Shader_t1881769421 * get_shader1_10() const { return ___shader1_10; }
	inline Shader_t1881769421 ** get_address_of_shader1_10() { return &___shader1_10; }
	inline void set_shader1_10(Shader_t1881769421 * value)
	{
		___shader1_10 = value;
		Il2CppCodeGenWriteBarrier((&___shader1_10), value);
	}

	inline static int32_t get_offset_of_shader2_11() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___shader2_11)); }
	inline Shader_t1881769421 * get_shader2_11() const { return ___shader2_11; }
	inline Shader_t1881769421 ** get_address_of_shader2_11() { return &___shader2_11; }
	inline void set_shader2_11(Shader_t1881769421 * value)
	{
		___shader2_11 = value;
		Il2CppCodeGenWriteBarrier((&___shader2_11), value);
	}

	inline static int32_t get_offset_of_rend_12() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___rend_12)); }
	inline Renderer_t1418648713 * get_rend_12() const { return ___rend_12; }
	inline Renderer_t1418648713 ** get_address_of_rend_12() { return &___rend_12; }
	inline void set_rend_12(Renderer_t1418648713 * value)
	{
		___rend_12 = value;
		Il2CppCodeGenWriteBarrier((&___rend_12), value);
	}

	inline static int32_t get_offset_of_waitOver_13() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431, ___waitOver_13)); }
	inline float get_waitOver_13() const { return ___waitOver_13; }
	inline float* get_address_of_waitOver_13() { return &___waitOver_13; }
	inline void set_waitOver_13(float value)
	{
		___waitOver_13 = value;
	}
};

struct ReleaseGem_t3020869431_StaticFields
{
public:
	// System.Boolean ReleaseGem::bagOff
	bool ___bagOff_6;

public:
	inline static int32_t get_offset_of_bagOff_6() { return static_cast<int32_t>(offsetof(ReleaseGem_t3020869431_StaticFields, ___bagOff_6)); }
	inline bool get_bagOff_6() const { return ___bagOff_6; }
	inline bool* get_address_of_bagOff_6() { return &___bagOff_6; }
	inline void set_bagOff_6(bool value)
	{
		___bagOff_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELEASEGEM_T3020869431_H
#ifndef GETGEM_T2101980823_H
#define GETGEM_T2101980823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetGem
struct  GetGem_t2101980823  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean GetGem::m_bPlayerStartTakingGem
	bool ___m_bPlayerStartTakingGem_2;
	// UnityEngine.Texture2D GetGem::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_3;
	// UnityEngine.Texture2D GetGem::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_4;

public:
	inline static int32_t get_offset_of_m_bPlayerStartTakingGem_2() { return static_cast<int32_t>(offsetof(GetGem_t2101980823, ___m_bPlayerStartTakingGem_2)); }
	inline bool get_m_bPlayerStartTakingGem_2() const { return ___m_bPlayerStartTakingGem_2; }
	inline bool* get_address_of_m_bPlayerStartTakingGem_2() { return &___m_bPlayerStartTakingGem_2; }
	inline void set_m_bPlayerStartTakingGem_2(bool value)
	{
		___m_bPlayerStartTakingGem_2 = value;
	}

	inline static int32_t get_offset_of_MouseOverTexture_3() { return static_cast<int32_t>(offsetof(GetGem_t2101980823, ___MouseOverTexture_3)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_3() const { return ___MouseOverTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_3() { return &___MouseOverTexture_3; }
	inline void set_MouseOverTexture_3(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_3), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_4() { return static_cast<int32_t>(offsetof(GetGem_t2101980823, ___MouseOriTexture_4)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_4() const { return ___MouseOriTexture_4; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_4() { return &___MouseOriTexture_4; }
	inline void set_MouseOriTexture_4(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETGEM_T2101980823_H
#ifndef GAMETIMER_T736613873_H
#define GAMETIMER_T736613873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTimer
struct  GameTimer_t736613873  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMETIMER_T736613873_H
#ifndef FIRESPRITFLYAWAY_T1850743346_H
#define FIRESPRITFLYAWAY_T1850743346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireSpritFlyAway
struct  FireSpritFlyAway_t1850743346  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 FireSpritFlyAway::m_vTargetPosition
	Vector3_t1986933152  ___m_vTargetPosition_2;
	// System.Boolean FireSpritFlyAway::m_bMovingToTarget
	bool ___m_bMovingToTarget_3;
	// UnityEngine.Vector3 FireSpritFlyAway::m_start
	Vector3_t1986933152  ___m_start_4;
	// System.Single FireSpritFlyAway::m_fTime
	float ___m_fTime_5;

public:
	inline static int32_t get_offset_of_m_vTargetPosition_2() { return static_cast<int32_t>(offsetof(FireSpritFlyAway_t1850743346, ___m_vTargetPosition_2)); }
	inline Vector3_t1986933152  get_m_vTargetPosition_2() const { return ___m_vTargetPosition_2; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetPosition_2() { return &___m_vTargetPosition_2; }
	inline void set_m_vTargetPosition_2(Vector3_t1986933152  value)
	{
		___m_vTargetPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToTarget_3() { return static_cast<int32_t>(offsetof(FireSpritFlyAway_t1850743346, ___m_bMovingToTarget_3)); }
	inline bool get_m_bMovingToTarget_3() const { return ___m_bMovingToTarget_3; }
	inline bool* get_address_of_m_bMovingToTarget_3() { return &___m_bMovingToTarget_3; }
	inline void set_m_bMovingToTarget_3(bool value)
	{
		___m_bMovingToTarget_3 = value;
	}

	inline static int32_t get_offset_of_m_start_4() { return static_cast<int32_t>(offsetof(FireSpritFlyAway_t1850743346, ___m_start_4)); }
	inline Vector3_t1986933152  get_m_start_4() const { return ___m_start_4; }
	inline Vector3_t1986933152 * get_address_of_m_start_4() { return &___m_start_4; }
	inline void set_m_start_4(Vector3_t1986933152  value)
	{
		___m_start_4 = value;
	}

	inline static int32_t get_offset_of_m_fTime_5() { return static_cast<int32_t>(offsetof(FireSpritFlyAway_t1850743346, ___m_fTime_5)); }
	inline float get_m_fTime_5() const { return ___m_fTime_5; }
	inline float* get_address_of_m_fTime_5() { return &___m_fTime_5; }
	inline void set_m_fTime_5(float value)
	{
		___m_fTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRESPRITFLYAWAY_T1850743346_H
#ifndef WATEREFFECT_T779416252_H
#define WATEREFFECT_T779416252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterEffect
struct  WaterEffect_t779416252  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector2[] WaterEffect::Offset
	Vector2U5BU5D_t1220531434* ___Offset_2;
	// UnityEngine.Vector2[] WaterEffect::tiling
	Vector2U5BU5D_t1220531434* ___tiling_3;
	// System.Single WaterEffect::AccumulatedTime
	float ___AccumulatedTime_4;
	// System.Single WaterEffect::Speed
	float ___Speed_5;

public:
	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(WaterEffect_t779416252, ___Offset_2)); }
	inline Vector2U5BU5D_t1220531434* get_Offset_2() const { return ___Offset_2; }
	inline Vector2U5BU5D_t1220531434** get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(Vector2U5BU5D_t1220531434* value)
	{
		___Offset_2 = value;
		Il2CppCodeGenWriteBarrier((&___Offset_2), value);
	}

	inline static int32_t get_offset_of_tiling_3() { return static_cast<int32_t>(offsetof(WaterEffect_t779416252, ___tiling_3)); }
	inline Vector2U5BU5D_t1220531434* get_tiling_3() const { return ___tiling_3; }
	inline Vector2U5BU5D_t1220531434** get_address_of_tiling_3() { return &___tiling_3; }
	inline void set_tiling_3(Vector2U5BU5D_t1220531434* value)
	{
		___tiling_3 = value;
		Il2CppCodeGenWriteBarrier((&___tiling_3), value);
	}

	inline static int32_t get_offset_of_AccumulatedTime_4() { return static_cast<int32_t>(offsetof(WaterEffect_t779416252, ___AccumulatedTime_4)); }
	inline float get_AccumulatedTime_4() const { return ___AccumulatedTime_4; }
	inline float* get_address_of_AccumulatedTime_4() { return &___AccumulatedTime_4; }
	inline void set_AccumulatedTime_4(float value)
	{
		___AccumulatedTime_4 = value;
	}

	inline static int32_t get_offset_of_Speed_5() { return static_cast<int32_t>(offsetof(WaterEffect_t779416252, ___Speed_5)); }
	inline float get_Speed_5() const { return ___Speed_5; }
	inline float* get_address_of_Speed_5() { return &___Speed_5; }
	inline void set_Speed_5(float value)
	{
		___Speed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATEREFFECT_T779416252_H
#ifndef GNATSCOMBAT_T1950420656_H
#define GNATSCOMBAT_T1950420656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsCombat
struct  GnatsCombat_t1950420656  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean GnatsCombat::m_bIsTriggered
	bool ___m_bIsTriggered_2;
	// UnityEngine.GameObject[] GnatsCombat::Gnats
	GameObjectU5BU5D_t2988620542* ___Gnats_3;
	// UnityEngine.Transform[] GnatsCombat::CombatTargets
	TransformU5BU5D_t2383644165* ___CombatTargets_4;
	// System.Int32 GnatsCombat::iCurrentAttackingUnit
	int32_t ___iCurrentAttackingUnit_5;
	// System.Boolean[] GnatsCombat::Defeated
	BooleanU5BU5D_t698278498* ___Defeated_6;
	// UnityEngine.Transform[] GnatsCombat::DefeatPos
	TransformU5BU5D_t2383644165* ___DefeatPos_7;
	// System.Boolean GnatsCombat::m_bHitPlayer
	bool ___m_bHitPlayer_8;
	// System.Boolean GnatsCombat::bAttackFirst
	bool ___bAttackFirst_9;
	// System.Boolean GnatsCombat::bSecondGnatAtacck
	bool ___bSecondGnatAtacck_10;
	// System.Boolean GnatsCombat::bCombatEnded
	bool ___bCombatEnded_11;

public:
	inline static int32_t get_offset_of_m_bIsTriggered_2() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___m_bIsTriggered_2)); }
	inline bool get_m_bIsTriggered_2() const { return ___m_bIsTriggered_2; }
	inline bool* get_address_of_m_bIsTriggered_2() { return &___m_bIsTriggered_2; }
	inline void set_m_bIsTriggered_2(bool value)
	{
		___m_bIsTriggered_2 = value;
	}

	inline static int32_t get_offset_of_Gnats_3() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___Gnats_3)); }
	inline GameObjectU5BU5D_t2988620542* get_Gnats_3() const { return ___Gnats_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Gnats_3() { return &___Gnats_3; }
	inline void set_Gnats_3(GameObjectU5BU5D_t2988620542* value)
	{
		___Gnats_3 = value;
		Il2CppCodeGenWriteBarrier((&___Gnats_3), value);
	}

	inline static int32_t get_offset_of_CombatTargets_4() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___CombatTargets_4)); }
	inline TransformU5BU5D_t2383644165* get_CombatTargets_4() const { return ___CombatTargets_4; }
	inline TransformU5BU5D_t2383644165** get_address_of_CombatTargets_4() { return &___CombatTargets_4; }
	inline void set_CombatTargets_4(TransformU5BU5D_t2383644165* value)
	{
		___CombatTargets_4 = value;
		Il2CppCodeGenWriteBarrier((&___CombatTargets_4), value);
	}

	inline static int32_t get_offset_of_iCurrentAttackingUnit_5() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___iCurrentAttackingUnit_5)); }
	inline int32_t get_iCurrentAttackingUnit_5() const { return ___iCurrentAttackingUnit_5; }
	inline int32_t* get_address_of_iCurrentAttackingUnit_5() { return &___iCurrentAttackingUnit_5; }
	inline void set_iCurrentAttackingUnit_5(int32_t value)
	{
		___iCurrentAttackingUnit_5 = value;
	}

	inline static int32_t get_offset_of_Defeated_6() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___Defeated_6)); }
	inline BooleanU5BU5D_t698278498* get_Defeated_6() const { return ___Defeated_6; }
	inline BooleanU5BU5D_t698278498** get_address_of_Defeated_6() { return &___Defeated_6; }
	inline void set_Defeated_6(BooleanU5BU5D_t698278498* value)
	{
		___Defeated_6 = value;
		Il2CppCodeGenWriteBarrier((&___Defeated_6), value);
	}

	inline static int32_t get_offset_of_DefeatPos_7() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___DefeatPos_7)); }
	inline TransformU5BU5D_t2383644165* get_DefeatPos_7() const { return ___DefeatPos_7; }
	inline TransformU5BU5D_t2383644165** get_address_of_DefeatPos_7() { return &___DefeatPos_7; }
	inline void set_DefeatPos_7(TransformU5BU5D_t2383644165* value)
	{
		___DefeatPos_7 = value;
		Il2CppCodeGenWriteBarrier((&___DefeatPos_7), value);
	}

	inline static int32_t get_offset_of_m_bHitPlayer_8() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___m_bHitPlayer_8)); }
	inline bool get_m_bHitPlayer_8() const { return ___m_bHitPlayer_8; }
	inline bool* get_address_of_m_bHitPlayer_8() { return &___m_bHitPlayer_8; }
	inline void set_m_bHitPlayer_8(bool value)
	{
		___m_bHitPlayer_8 = value;
	}

	inline static int32_t get_offset_of_bAttackFirst_9() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___bAttackFirst_9)); }
	inline bool get_bAttackFirst_9() const { return ___bAttackFirst_9; }
	inline bool* get_address_of_bAttackFirst_9() { return &___bAttackFirst_9; }
	inline void set_bAttackFirst_9(bool value)
	{
		___bAttackFirst_9 = value;
	}

	inline static int32_t get_offset_of_bSecondGnatAtacck_10() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___bSecondGnatAtacck_10)); }
	inline bool get_bSecondGnatAtacck_10() const { return ___bSecondGnatAtacck_10; }
	inline bool* get_address_of_bSecondGnatAtacck_10() { return &___bSecondGnatAtacck_10; }
	inline void set_bSecondGnatAtacck_10(bool value)
	{
		___bSecondGnatAtacck_10 = value;
	}

	inline static int32_t get_offset_of_bCombatEnded_11() { return static_cast<int32_t>(offsetof(GnatsCombat_t1950420656, ___bCombatEnded_11)); }
	inline bool get_bCombatEnded_11() const { return ___bCombatEnded_11; }
	inline bool* get_address_of_bCombatEnded_11() { return &___bCombatEnded_11; }
	inline void set_bCombatEnded_11(bool value)
	{
		___bCombatEnded_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSCOMBAT_T1950420656_H
#ifndef TRIGGERPARTICLES_T2790258363_H
#define TRIGGERPARTICLES_T2790258363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriggerParticles
struct  TriggerParticles_t2790258363  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.ParticleSystem TriggerParticles::rocks
	ParticleSystem_t2248283753 * ___rocks_2;

public:
	inline static int32_t get_offset_of_rocks_2() { return static_cast<int32_t>(offsetof(TriggerParticles_t2790258363, ___rocks_2)); }
	inline ParticleSystem_t2248283753 * get_rocks_2() const { return ___rocks_2; }
	inline ParticleSystem_t2248283753 ** get_address_of_rocks_2() { return &___rocks_2; }
	inline void set_rocks_2(ParticleSystem_t2248283753 * value)
	{
		___rocks_2 = value;
		Il2CppCodeGenWriteBarrier((&___rocks_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERPARTICLES_T2790258363_H
#ifndef SETANIMATIONSTARTFRAME_T1496270653_H
#define SETANIMATIONSTARTFRAME_T1496270653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimationStartFrame
struct  SetAnimationStartFrame_t1496270653  : public MonoBehaviour_t1618594486
{
public:
	// System.Single SetAnimationStartFrame::fTime
	float ___fTime_2;

public:
	inline static int32_t get_offset_of_fTime_2() { return static_cast<int32_t>(offsetof(SetAnimationStartFrame_t1496270653, ___fTime_2)); }
	inline float get_fTime_2() const { return ___fTime_2; }
	inline float* get_address_of_fTime_2() { return &___fTime_2; }
	inline void set_fTime_2(float value)
	{
		___fTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATIONSTARTFRAME_T1496270653_H
#ifndef QUICKSANDEFFECT_T4016288157_H
#define QUICKSANDEFFECT_T4016288157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuickSandEffect
struct  QuickSandEffect_t4016288157  : public MonoBehaviour_t1618594486
{
public:
	// System.Single QuickSandEffect::AccumulatedTime
	float ___AccumulatedTime_2;
	// System.Single QuickSandEffect::Speed
	float ___Speed_3;
	// System.Boolean QuickSandEffect::m_bInverseRotation
	bool ___m_bInverseRotation_4;
	// System.Single QuickSandEffect::fRotateDegrees
	float ___fRotateDegrees_5;

public:
	inline static int32_t get_offset_of_AccumulatedTime_2() { return static_cast<int32_t>(offsetof(QuickSandEffect_t4016288157, ___AccumulatedTime_2)); }
	inline float get_AccumulatedTime_2() const { return ___AccumulatedTime_2; }
	inline float* get_address_of_AccumulatedTime_2() { return &___AccumulatedTime_2; }
	inline void set_AccumulatedTime_2(float value)
	{
		___AccumulatedTime_2 = value;
	}

	inline static int32_t get_offset_of_Speed_3() { return static_cast<int32_t>(offsetof(QuickSandEffect_t4016288157, ___Speed_3)); }
	inline float get_Speed_3() const { return ___Speed_3; }
	inline float* get_address_of_Speed_3() { return &___Speed_3; }
	inline void set_Speed_3(float value)
	{
		___Speed_3 = value;
	}

	inline static int32_t get_offset_of_m_bInverseRotation_4() { return static_cast<int32_t>(offsetof(QuickSandEffect_t4016288157, ___m_bInverseRotation_4)); }
	inline bool get_m_bInverseRotation_4() const { return ___m_bInverseRotation_4; }
	inline bool* get_address_of_m_bInverseRotation_4() { return &___m_bInverseRotation_4; }
	inline void set_m_bInverseRotation_4(bool value)
	{
		___m_bInverseRotation_4 = value;
	}

	inline static int32_t get_offset_of_fRotateDegrees_5() { return static_cast<int32_t>(offsetof(QuickSandEffect_t4016288157, ___fRotateDegrees_5)); }
	inline float get_fRotateDegrees_5() const { return ___fRotateDegrees_5; }
	inline float* get_address_of_fRotateDegrees_5() { return &___fRotateDegrees_5; }
	inline void set_fRotateDegrees_5(float value)
	{
		___fRotateDegrees_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKSANDEFFECT_T4016288157_H
#ifndef LV7BRIDGEBREAK_T760961460_H
#define LV7BRIDGEBREAK_T760961460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7BridgeBreak
struct  Lv7BridgeBreak_t760961460  : public MonoBehaviour_t1618594486
{
public:
	// System.Single Lv7BridgeBreak::Distance
	float ___Distance_2;

public:
	inline static int32_t get_offset_of_Distance_2() { return static_cast<int32_t>(offsetof(Lv7BridgeBreak_t760961460, ___Distance_2)); }
	inline float get_Distance_2() const { return ___Distance_2; }
	inline float* get_address_of_Distance_2() { return &___Distance_2; }
	inline void set_Distance_2(float value)
	{
		___Distance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7BRIDGEBREAK_T760961460_H
#ifndef LIGHTFADE_T1716889777_H
#define LIGHTFADE_T1716889777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightFade
struct  LightFade_t1716889777  : public MonoBehaviour_t1618594486
{
public:
	// System.Single LightFade::m_fTime
	float ___m_fTime_2;

public:
	inline static int32_t get_offset_of_m_fTime_2() { return static_cast<int32_t>(offsetof(LightFade_t1716889777, ___m_fTime_2)); }
	inline float get_m_fTime_2() const { return ___m_fTime_2; }
	inline float* get_address_of_m_fTime_2() { return &___m_fTime_2; }
	inline void set_m_fTime_2(float value)
	{
		___m_fTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTFADE_T1716889777_H
#ifndef LEAFFLYING_T2321553312_H
#define LEAFFLYING_T2321553312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeafFlying
struct  LeafFlying_t2321553312  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 LeafFlying::MaximumLeafDisplay
	int32_t ___MaximumLeafDisplay_2;
	// System.Single LeafFlying::LeafDropRate
	float ___LeafDropRate_3;
	// System.Single LeafFlying::LeafDropElapsed
	float ___LeafDropElapsed_4;

public:
	inline static int32_t get_offset_of_MaximumLeafDisplay_2() { return static_cast<int32_t>(offsetof(LeafFlying_t2321553312, ___MaximumLeafDisplay_2)); }
	inline int32_t get_MaximumLeafDisplay_2() const { return ___MaximumLeafDisplay_2; }
	inline int32_t* get_address_of_MaximumLeafDisplay_2() { return &___MaximumLeafDisplay_2; }
	inline void set_MaximumLeafDisplay_2(int32_t value)
	{
		___MaximumLeafDisplay_2 = value;
	}

	inline static int32_t get_offset_of_LeafDropRate_3() { return static_cast<int32_t>(offsetof(LeafFlying_t2321553312, ___LeafDropRate_3)); }
	inline float get_LeafDropRate_3() const { return ___LeafDropRate_3; }
	inline float* get_address_of_LeafDropRate_3() { return &___LeafDropRate_3; }
	inline void set_LeafDropRate_3(float value)
	{
		___LeafDropRate_3 = value;
	}

	inline static int32_t get_offset_of_LeafDropElapsed_4() { return static_cast<int32_t>(offsetof(LeafFlying_t2321553312, ___LeafDropElapsed_4)); }
	inline float get_LeafDropElapsed_4() const { return ___LeafDropElapsed_4; }
	inline float* get_address_of_LeafDropElapsed_4() { return &___LeafDropElapsed_4; }
	inline void set_LeafDropElapsed_4(float value)
	{
		___LeafDropElapsed_4 = value;
	}
};

struct LeafFlying_t2321553312_StaticFields
{
public:
	// System.Int32 LeafFlying::NumOfLeafOnScreen
	int32_t ___NumOfLeafOnScreen_5;

public:
	inline static int32_t get_offset_of_NumOfLeafOnScreen_5() { return static_cast<int32_t>(offsetof(LeafFlying_t2321553312_StaticFields, ___NumOfLeafOnScreen_5)); }
	inline int32_t get_NumOfLeafOnScreen_5() const { return ___NumOfLeafOnScreen_5; }
	inline int32_t* get_address_of_NumOfLeafOnScreen_5() { return &___NumOfLeafOnScreen_5; }
	inline void set_NumOfLeafOnScreen_5(int32_t value)
	{
		___NumOfLeafOnScreen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFFLYING_T2321553312_H
#ifndef LEAFDROP_T1157882929_H
#define LEAFDROP_T1157882929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeafDrop
struct  LeafDrop_t1157882929  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LeafDrop::StartDroping
	bool ___StartDroping_2;
	// System.Single LeafDrop::RandomRotation
	float ___RandomRotation_3;
	// UnityEngine.Vector3 LeafDrop::Force
	Vector3_t1986933152  ___Force_4;

public:
	inline static int32_t get_offset_of_StartDroping_2() { return static_cast<int32_t>(offsetof(LeafDrop_t1157882929, ___StartDroping_2)); }
	inline bool get_StartDroping_2() const { return ___StartDroping_2; }
	inline bool* get_address_of_StartDroping_2() { return &___StartDroping_2; }
	inline void set_StartDroping_2(bool value)
	{
		___StartDroping_2 = value;
	}

	inline static int32_t get_offset_of_RandomRotation_3() { return static_cast<int32_t>(offsetof(LeafDrop_t1157882929, ___RandomRotation_3)); }
	inline float get_RandomRotation_3() const { return ___RandomRotation_3; }
	inline float* get_address_of_RandomRotation_3() { return &___RandomRotation_3; }
	inline void set_RandomRotation_3(float value)
	{
		___RandomRotation_3 = value;
	}

	inline static int32_t get_offset_of_Force_4() { return static_cast<int32_t>(offsetof(LeafDrop_t1157882929, ___Force_4)); }
	inline Vector3_t1986933152  get_Force_4() const { return ___Force_4; }
	inline Vector3_t1986933152 * get_address_of_Force_4() { return &___Force_4; }
	inline void set_Force_4(Vector3_t1986933152  value)
	{
		___Force_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFDROP_T1157882929_H
#ifndef ICEDROPLV2_T3491147653_H
#define ICEDROPLV2_T3491147653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceDropLv2
struct  IceDropLv2_t3491147653  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] IceDropLv2::IceParts
	GameObjectU5BU5D_t2988620542* ___IceParts_2;
	// System.Boolean IceDropLv2::m_bIsDropping
	bool ___m_bIsDropping_3;
	// System.Single IceDropLv2::FallSpeedMin
	float ___FallSpeedMin_4;
	// System.Single IceDropLv2::FallSpeedMax
	float ___FallSpeedMax_5;
	// System.Single[] IceDropLv2::FallSpeeds
	SingleU5BU5D_t2843050510* ___FallSpeeds_6;
	// System.Single IceDropLv2::IntervalMin
	float ___IntervalMin_7;
	// System.Single IceDropLv2::IntervalMax
	float ___IntervalMax_8;
	// UnityEngine.GameObject IceDropLv2::snowPuff
	GameObject_t2557347079 * ___snowPuff_9;
	// System.Single[] IceDropLv2::Intervals
	SingleU5BU5D_t2843050510* ___Intervals_10;
	// System.Boolean[] IceDropLv2::Dropping
	BooleanU5BU5D_t698278498* ___Dropping_11;
	// UnityEngine.Vector3[] IceDropLv2::OriginalPos
	Vector3U5BU5D_t32224225* ___OriginalPos_12;

public:
	inline static int32_t get_offset_of_IceParts_2() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___IceParts_2)); }
	inline GameObjectU5BU5D_t2988620542* get_IceParts_2() const { return ___IceParts_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_IceParts_2() { return &___IceParts_2; }
	inline void set_IceParts_2(GameObjectU5BU5D_t2988620542* value)
	{
		___IceParts_2 = value;
		Il2CppCodeGenWriteBarrier((&___IceParts_2), value);
	}

	inline static int32_t get_offset_of_m_bIsDropping_3() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___m_bIsDropping_3)); }
	inline bool get_m_bIsDropping_3() const { return ___m_bIsDropping_3; }
	inline bool* get_address_of_m_bIsDropping_3() { return &___m_bIsDropping_3; }
	inline void set_m_bIsDropping_3(bool value)
	{
		___m_bIsDropping_3 = value;
	}

	inline static int32_t get_offset_of_FallSpeedMin_4() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___FallSpeedMin_4)); }
	inline float get_FallSpeedMin_4() const { return ___FallSpeedMin_4; }
	inline float* get_address_of_FallSpeedMin_4() { return &___FallSpeedMin_4; }
	inline void set_FallSpeedMin_4(float value)
	{
		___FallSpeedMin_4 = value;
	}

	inline static int32_t get_offset_of_FallSpeedMax_5() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___FallSpeedMax_5)); }
	inline float get_FallSpeedMax_5() const { return ___FallSpeedMax_5; }
	inline float* get_address_of_FallSpeedMax_5() { return &___FallSpeedMax_5; }
	inline void set_FallSpeedMax_5(float value)
	{
		___FallSpeedMax_5 = value;
	}

	inline static int32_t get_offset_of_FallSpeeds_6() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___FallSpeeds_6)); }
	inline SingleU5BU5D_t2843050510* get_FallSpeeds_6() const { return ___FallSpeeds_6; }
	inline SingleU5BU5D_t2843050510** get_address_of_FallSpeeds_6() { return &___FallSpeeds_6; }
	inline void set_FallSpeeds_6(SingleU5BU5D_t2843050510* value)
	{
		___FallSpeeds_6 = value;
		Il2CppCodeGenWriteBarrier((&___FallSpeeds_6), value);
	}

	inline static int32_t get_offset_of_IntervalMin_7() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___IntervalMin_7)); }
	inline float get_IntervalMin_7() const { return ___IntervalMin_7; }
	inline float* get_address_of_IntervalMin_7() { return &___IntervalMin_7; }
	inline void set_IntervalMin_7(float value)
	{
		___IntervalMin_7 = value;
	}

	inline static int32_t get_offset_of_IntervalMax_8() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___IntervalMax_8)); }
	inline float get_IntervalMax_8() const { return ___IntervalMax_8; }
	inline float* get_address_of_IntervalMax_8() { return &___IntervalMax_8; }
	inline void set_IntervalMax_8(float value)
	{
		___IntervalMax_8 = value;
	}

	inline static int32_t get_offset_of_snowPuff_9() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___snowPuff_9)); }
	inline GameObject_t2557347079 * get_snowPuff_9() const { return ___snowPuff_9; }
	inline GameObject_t2557347079 ** get_address_of_snowPuff_9() { return &___snowPuff_9; }
	inline void set_snowPuff_9(GameObject_t2557347079 * value)
	{
		___snowPuff_9 = value;
		Il2CppCodeGenWriteBarrier((&___snowPuff_9), value);
	}

	inline static int32_t get_offset_of_Intervals_10() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___Intervals_10)); }
	inline SingleU5BU5D_t2843050510* get_Intervals_10() const { return ___Intervals_10; }
	inline SingleU5BU5D_t2843050510** get_address_of_Intervals_10() { return &___Intervals_10; }
	inline void set_Intervals_10(SingleU5BU5D_t2843050510* value)
	{
		___Intervals_10 = value;
		Il2CppCodeGenWriteBarrier((&___Intervals_10), value);
	}

	inline static int32_t get_offset_of_Dropping_11() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___Dropping_11)); }
	inline BooleanU5BU5D_t698278498* get_Dropping_11() const { return ___Dropping_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_Dropping_11() { return &___Dropping_11; }
	inline void set_Dropping_11(BooleanU5BU5D_t698278498* value)
	{
		___Dropping_11 = value;
		Il2CppCodeGenWriteBarrier((&___Dropping_11), value);
	}

	inline static int32_t get_offset_of_OriginalPos_12() { return static_cast<int32_t>(offsetof(IceDropLv2_t3491147653, ___OriginalPos_12)); }
	inline Vector3U5BU5D_t32224225* get_OriginalPos_12() const { return ___OriginalPos_12; }
	inline Vector3U5BU5D_t32224225** get_address_of_OriginalPos_12() { return &___OriginalPos_12; }
	inline void set_OriginalPos_12(Vector3U5BU5D_t32224225* value)
	{
		___OriginalPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPos_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICEDROPLV2_T3491147653_H
#ifndef CAMERASHAKE_T1998576306_H
#define CAMERASHAKE_T1998576306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShake
struct  CameraShake_t1998576306  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 CameraShake::originPosition
	Vector3_t1986933152  ___originPosition_2;
	// UnityEngine.Quaternion CameraShake::originRotation
	Quaternion_t704191599  ___originRotation_3;
	// System.Single CameraShake::shake_decay
	float ___shake_decay_4;
	// System.Single CameraShake::shake_intensity
	float ___shake_intensity_5;

public:
	inline static int32_t get_offset_of_originPosition_2() { return static_cast<int32_t>(offsetof(CameraShake_t1998576306, ___originPosition_2)); }
	inline Vector3_t1986933152  get_originPosition_2() const { return ___originPosition_2; }
	inline Vector3_t1986933152 * get_address_of_originPosition_2() { return &___originPosition_2; }
	inline void set_originPosition_2(Vector3_t1986933152  value)
	{
		___originPosition_2 = value;
	}

	inline static int32_t get_offset_of_originRotation_3() { return static_cast<int32_t>(offsetof(CameraShake_t1998576306, ___originRotation_3)); }
	inline Quaternion_t704191599  get_originRotation_3() const { return ___originRotation_3; }
	inline Quaternion_t704191599 * get_address_of_originRotation_3() { return &___originRotation_3; }
	inline void set_originRotation_3(Quaternion_t704191599  value)
	{
		___originRotation_3 = value;
	}

	inline static int32_t get_offset_of_shake_decay_4() { return static_cast<int32_t>(offsetof(CameraShake_t1998576306, ___shake_decay_4)); }
	inline float get_shake_decay_4() const { return ___shake_decay_4; }
	inline float* get_address_of_shake_decay_4() { return &___shake_decay_4; }
	inline void set_shake_decay_4(float value)
	{
		___shake_decay_4 = value;
	}

	inline static int32_t get_offset_of_shake_intensity_5() { return static_cast<int32_t>(offsetof(CameraShake_t1998576306, ___shake_intensity_5)); }
	inline float get_shake_intensity_5() const { return ___shake_intensity_5; }
	inline float* get_address_of_shake_intensity_5() { return &___shake_intensity_5; }
	inline void set_shake_intensity_5(float value)
	{
		___shake_intensity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKE_T1998576306_H
#ifndef SETANIMATIONSTARTFRAME1_T1668100355_H
#define SETANIMATIONSTARTFRAME1_T1668100355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimationStartFrame1
struct  SetAnimationStartFrame1_t1668100355  : public MonoBehaviour_t1618594486
{
public:
	// System.Single SetAnimationStartFrame1::fTime
	float ___fTime_2;

public:
	inline static int32_t get_offset_of_fTime_2() { return static_cast<int32_t>(offsetof(SetAnimationStartFrame1_t1668100355, ___fTime_2)); }
	inline float get_fTime_2() const { return ___fTime_2; }
	inline float* get_address_of_fTime_2() { return &___fTime_2; }
	inline void set_fTime_2(float value)
	{
		___fTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATIONSTARTFRAME1_T1668100355_H
#ifndef HELPSPARX_T654320582_H
#define HELPSPARX_T654320582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpSparx
struct  HelpSparx_t654320582  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin HelpSparx::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject HelpSparx::m_theGuardian
	GameObject_t2557347079 * ___m_theGuardian_3;
	// UnityEngine.GameObject HelpSparx::m_theMontor
	GameObject_t2557347079 * ___m_theMontor_4;
	// UnityEngine.Texture2D HelpSparx::m_mouseCorsorImage
	Texture2D_t3063074017 * ___m_mouseCorsorImage_5;
	// UnityEngine.Texture2D HelpSparx::m_closeImage
	Texture2D_t3063074017 * ___m_closeImage_6;
	// UnityEngine.Texture2D HelpSparx::m_backPackImage
	Texture2D_t3063074017 * ___m_backPackImage_7;
	// UnityEngine.Texture2D HelpSparx::m_sparxManulImage
	Texture2D_t3063074017 * ___m_sparxManulImage_8;
	// UnityEngine.Texture2D HelpSparx::loadingIcon
	Texture2D_t3063074017 * ___loadingIcon_9;
	// UnityEngine.Texture2D HelpSparx::m_Eagle
	Texture2D_t3063074017 * ___m_Eagle_10;
	// UnityEngine.Texture2D HelpSparx::m_Latern
	Texture2D_t3063074017 * ___m_Latern_11;
	// UnityEngine.Texture2D HelpSparx::m_Gnat
	Texture2D_t3063074017 * ___m_Gnat_12;
	// UnityEngine.Texture2D HelpSparx::m_Lader
	Texture2D_t3063074017 * ___m_Lader_13;
	// UnityEngine.Texture2D HelpSparx::m_Control
	Texture2D_t3063074017 * ___m_Control_14;
	// UnityEngine.Texture2D HelpSparx::m_Control2
	Texture2D_t3063074017 * ___m_Control2_15;
	// UnityEngine.GameObject HelpSparx::m_gardianClone
	GameObject_t2557347079 * ___m_gardianClone_16;
	// UnityEngine.GameObject HelpSparx::m_mentorClone
	GameObject_t2557347079 * ___m_mentorClone_17;
	// TalkScenes HelpSparx::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_18;
	// UnityEngine.GameObject HelpSparx::m_gGardianObject
	GameObject_t2557347079 * ___m_gGardianObject_19;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_theGuardian_3() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_theGuardian_3)); }
	inline GameObject_t2557347079 * get_m_theGuardian_3() const { return ___m_theGuardian_3; }
	inline GameObject_t2557347079 ** get_address_of_m_theGuardian_3() { return &___m_theGuardian_3; }
	inline void set_m_theGuardian_3(GameObject_t2557347079 * value)
	{
		___m_theGuardian_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_theGuardian_3), value);
	}

	inline static int32_t get_offset_of_m_theMontor_4() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_theMontor_4)); }
	inline GameObject_t2557347079 * get_m_theMontor_4() const { return ___m_theMontor_4; }
	inline GameObject_t2557347079 ** get_address_of_m_theMontor_4() { return &___m_theMontor_4; }
	inline void set_m_theMontor_4(GameObject_t2557347079 * value)
	{
		___m_theMontor_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_theMontor_4), value);
	}

	inline static int32_t get_offset_of_m_mouseCorsorImage_5() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_mouseCorsorImage_5)); }
	inline Texture2D_t3063074017 * get_m_mouseCorsorImage_5() const { return ___m_mouseCorsorImage_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_mouseCorsorImage_5() { return &___m_mouseCorsorImage_5; }
	inline void set_m_mouseCorsorImage_5(Texture2D_t3063074017 * value)
	{
		___m_mouseCorsorImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_mouseCorsorImage_5), value);
	}

	inline static int32_t get_offset_of_m_closeImage_6() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_closeImage_6)); }
	inline Texture2D_t3063074017 * get_m_closeImage_6() const { return ___m_closeImage_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_closeImage_6() { return &___m_closeImage_6; }
	inline void set_m_closeImage_6(Texture2D_t3063074017 * value)
	{
		___m_closeImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_closeImage_6), value);
	}

	inline static int32_t get_offset_of_m_backPackImage_7() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_backPackImage_7)); }
	inline Texture2D_t3063074017 * get_m_backPackImage_7() const { return ___m_backPackImage_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_backPackImage_7() { return &___m_backPackImage_7; }
	inline void set_m_backPackImage_7(Texture2D_t3063074017 * value)
	{
		___m_backPackImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_backPackImage_7), value);
	}

	inline static int32_t get_offset_of_m_sparxManulImage_8() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_sparxManulImage_8)); }
	inline Texture2D_t3063074017 * get_m_sparxManulImage_8() const { return ___m_sparxManulImage_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_sparxManulImage_8() { return &___m_sparxManulImage_8; }
	inline void set_m_sparxManulImage_8(Texture2D_t3063074017 * value)
	{
		___m_sparxManulImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_sparxManulImage_8), value);
	}

	inline static int32_t get_offset_of_loadingIcon_9() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___loadingIcon_9)); }
	inline Texture2D_t3063074017 * get_loadingIcon_9() const { return ___loadingIcon_9; }
	inline Texture2D_t3063074017 ** get_address_of_loadingIcon_9() { return &___loadingIcon_9; }
	inline void set_loadingIcon_9(Texture2D_t3063074017 * value)
	{
		___loadingIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&___loadingIcon_9), value);
	}

	inline static int32_t get_offset_of_m_Eagle_10() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Eagle_10)); }
	inline Texture2D_t3063074017 * get_m_Eagle_10() const { return ___m_Eagle_10; }
	inline Texture2D_t3063074017 ** get_address_of_m_Eagle_10() { return &___m_Eagle_10; }
	inline void set_m_Eagle_10(Texture2D_t3063074017 * value)
	{
		___m_Eagle_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Eagle_10), value);
	}

	inline static int32_t get_offset_of_m_Latern_11() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Latern_11)); }
	inline Texture2D_t3063074017 * get_m_Latern_11() const { return ___m_Latern_11; }
	inline Texture2D_t3063074017 ** get_address_of_m_Latern_11() { return &___m_Latern_11; }
	inline void set_m_Latern_11(Texture2D_t3063074017 * value)
	{
		___m_Latern_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Latern_11), value);
	}

	inline static int32_t get_offset_of_m_Gnat_12() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Gnat_12)); }
	inline Texture2D_t3063074017 * get_m_Gnat_12() const { return ___m_Gnat_12; }
	inline Texture2D_t3063074017 ** get_address_of_m_Gnat_12() { return &___m_Gnat_12; }
	inline void set_m_Gnat_12(Texture2D_t3063074017 * value)
	{
		___m_Gnat_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Gnat_12), value);
	}

	inline static int32_t get_offset_of_m_Lader_13() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Lader_13)); }
	inline Texture2D_t3063074017 * get_m_Lader_13() const { return ___m_Lader_13; }
	inline Texture2D_t3063074017 ** get_address_of_m_Lader_13() { return &___m_Lader_13; }
	inline void set_m_Lader_13(Texture2D_t3063074017 * value)
	{
		___m_Lader_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lader_13), value);
	}

	inline static int32_t get_offset_of_m_Control_14() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Control_14)); }
	inline Texture2D_t3063074017 * get_m_Control_14() const { return ___m_Control_14; }
	inline Texture2D_t3063074017 ** get_address_of_m_Control_14() { return &___m_Control_14; }
	inline void set_m_Control_14(Texture2D_t3063074017 * value)
	{
		___m_Control_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Control_14), value);
	}

	inline static int32_t get_offset_of_m_Control2_15() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_Control2_15)); }
	inline Texture2D_t3063074017 * get_m_Control2_15() const { return ___m_Control2_15; }
	inline Texture2D_t3063074017 ** get_address_of_m_Control2_15() { return &___m_Control2_15; }
	inline void set_m_Control2_15(Texture2D_t3063074017 * value)
	{
		___m_Control2_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Control2_15), value);
	}

	inline static int32_t get_offset_of_m_gardianClone_16() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_gardianClone_16)); }
	inline GameObject_t2557347079 * get_m_gardianClone_16() const { return ___m_gardianClone_16; }
	inline GameObject_t2557347079 ** get_address_of_m_gardianClone_16() { return &___m_gardianClone_16; }
	inline void set_m_gardianClone_16(GameObject_t2557347079 * value)
	{
		___m_gardianClone_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_gardianClone_16), value);
	}

	inline static int32_t get_offset_of_m_mentorClone_17() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_mentorClone_17)); }
	inline GameObject_t2557347079 * get_m_mentorClone_17() const { return ___m_mentorClone_17; }
	inline GameObject_t2557347079 ** get_address_of_m_mentorClone_17() { return &___m_mentorClone_17; }
	inline void set_m_mentorClone_17(GameObject_t2557347079 * value)
	{
		___m_mentorClone_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_mentorClone_17), value);
	}

	inline static int32_t get_offset_of_m_talkScene_18() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_talkScene_18)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_18() const { return ___m_talkScene_18; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_18() { return &___m_talkScene_18; }
	inline void set_m_talkScene_18(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_18), value);
	}

	inline static int32_t get_offset_of_m_gGardianObject_19() { return static_cast<int32_t>(offsetof(HelpSparx_t654320582, ___m_gGardianObject_19)); }
	inline GameObject_t2557347079 * get_m_gGardianObject_19() const { return ___m_gGardianObject_19; }
	inline GameObject_t2557347079 ** get_address_of_m_gGardianObject_19() { return &___m_gGardianObject_19; }
	inline void set_m_gGardianObject_19(GameObject_t2557347079 * value)
	{
		___m_gGardianObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_gGardianObject_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPSPARX_T654320582_H
#ifndef L7FINALCUTSCENE_T3460878615_H
#define L7FINALCUTSCENE_T3460878615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7FinalCutScene
struct  L7FinalCutScene_t3460878615  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L7FinalCutScene::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Single L7FinalCutScene::m_fFadeInAlpha
	float ___m_fFadeInAlpha_3;
	// System.Single L7FinalCutScene::m_fFadeOutAlpha
	float ___m_fFadeOutAlpha_4;
	// System.Single L7FinalCutScene::m_fFadeInStartTime
	float ___m_fFadeInStartTime_5;
	// System.Single L7FinalCutScene::m_fFadeOutStartTime
	float ___m_fFadeOutStartTime_6;
	// System.Boolean L7FinalCutScene::m_bCutsceneFadeInStart
	bool ___m_bCutsceneFadeInStart_7;
	// System.Boolean L7FinalCutScene::m_bCutsceneFadeOutStart
	bool ___m_bCutsceneFadeOutStart_8;
	// System.Boolean L7FinalCutScene::m_shieldStartMoving
	bool ___m_shieldStartMoving_9;
	// System.Boolean L7FinalCutScene::m_bShieldRaising
	bool ___m_bShieldRaising_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_fFadeInAlpha_3() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_fFadeInAlpha_3)); }
	inline float get_m_fFadeInAlpha_3() const { return ___m_fFadeInAlpha_3; }
	inline float* get_address_of_m_fFadeInAlpha_3() { return &___m_fFadeInAlpha_3; }
	inline void set_m_fFadeInAlpha_3(float value)
	{
		___m_fFadeInAlpha_3 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutAlpha_4() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_fFadeOutAlpha_4)); }
	inline float get_m_fFadeOutAlpha_4() const { return ___m_fFadeOutAlpha_4; }
	inline float* get_address_of_m_fFadeOutAlpha_4() { return &___m_fFadeOutAlpha_4; }
	inline void set_m_fFadeOutAlpha_4(float value)
	{
		___m_fFadeOutAlpha_4 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_5() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_fFadeInStartTime_5)); }
	inline float get_m_fFadeInStartTime_5() const { return ___m_fFadeInStartTime_5; }
	inline float* get_address_of_m_fFadeInStartTime_5() { return &___m_fFadeInStartTime_5; }
	inline void set_m_fFadeInStartTime_5(float value)
	{
		___m_fFadeInStartTime_5 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutStartTime_6() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_fFadeOutStartTime_6)); }
	inline float get_m_fFadeOutStartTime_6() const { return ___m_fFadeOutStartTime_6; }
	inline float* get_address_of_m_fFadeOutStartTime_6() { return &___m_fFadeOutStartTime_6; }
	inline void set_m_fFadeOutStartTime_6(float value)
	{
		___m_fFadeOutStartTime_6 = value;
	}

	inline static int32_t get_offset_of_m_bCutsceneFadeInStart_7() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_bCutsceneFadeInStart_7)); }
	inline bool get_m_bCutsceneFadeInStart_7() const { return ___m_bCutsceneFadeInStart_7; }
	inline bool* get_address_of_m_bCutsceneFadeInStart_7() { return &___m_bCutsceneFadeInStart_7; }
	inline void set_m_bCutsceneFadeInStart_7(bool value)
	{
		___m_bCutsceneFadeInStart_7 = value;
	}

	inline static int32_t get_offset_of_m_bCutsceneFadeOutStart_8() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_bCutsceneFadeOutStart_8)); }
	inline bool get_m_bCutsceneFadeOutStart_8() const { return ___m_bCutsceneFadeOutStart_8; }
	inline bool* get_address_of_m_bCutsceneFadeOutStart_8() { return &___m_bCutsceneFadeOutStart_8; }
	inline void set_m_bCutsceneFadeOutStart_8(bool value)
	{
		___m_bCutsceneFadeOutStart_8 = value;
	}

	inline static int32_t get_offset_of_m_shieldStartMoving_9() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_shieldStartMoving_9)); }
	inline bool get_m_shieldStartMoving_9() const { return ___m_shieldStartMoving_9; }
	inline bool* get_address_of_m_shieldStartMoving_9() { return &___m_shieldStartMoving_9; }
	inline void set_m_shieldStartMoving_9(bool value)
	{
		___m_shieldStartMoving_9 = value;
	}

	inline static int32_t get_offset_of_m_bShieldRaising_10() { return static_cast<int32_t>(offsetof(L7FinalCutScene_t3460878615, ___m_bShieldRaising_10)); }
	inline bool get_m_bShieldRaising_10() const { return ___m_bShieldRaising_10; }
	inline bool* get_address_of_m_bShieldRaising_10() { return &___m_bShieldRaising_10; }
	inline void set_m_bShieldRaising_10(bool value)
	{
		___m_bShieldRaising_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7FINALCUTSCENE_T3460878615_H
#ifndef GNATSINTERACTION_T1398631457_H
#define GNATSINTERACTION_T1398631457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsInteraction
struct  GnatsInteraction_t1398631457  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 GnatsInteraction::IndexOfAction
	int32_t ___IndexOfAction_2;
	// GnatsAction[] GnatsInteraction::Actions
	GnatsActionU5BU5D_t1693839301* ___Actions_3;
	// UnityEngine.Transform[] GnatsInteraction::TriggerPoints
	TransformU5BU5D_t2383644165* ___TriggerPoints_4;
	// System.String GnatsInteraction::CameraID
	String_t* ___CameraID_5;
	// UnityEngine.GUISkin GnatsInteraction::GUIskin
	GUISkin_t2122630221 * ___GUIskin_6;
	// System.Boolean GnatsInteraction::bActivated
	bool ___bActivated_7;
	// System.String GnatsInteraction::Text
	String_t* ___Text_8;
	// TalkScenes GnatsInteraction::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_9;
	// System.Single GnatsInteraction::distanceInteraction
	float ___distanceInteraction_10;

public:
	inline static int32_t get_offset_of_IndexOfAction_2() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___IndexOfAction_2)); }
	inline int32_t get_IndexOfAction_2() const { return ___IndexOfAction_2; }
	inline int32_t* get_address_of_IndexOfAction_2() { return &___IndexOfAction_2; }
	inline void set_IndexOfAction_2(int32_t value)
	{
		___IndexOfAction_2 = value;
	}

	inline static int32_t get_offset_of_Actions_3() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___Actions_3)); }
	inline GnatsActionU5BU5D_t1693839301* get_Actions_3() const { return ___Actions_3; }
	inline GnatsActionU5BU5D_t1693839301** get_address_of_Actions_3() { return &___Actions_3; }
	inline void set_Actions_3(GnatsActionU5BU5D_t1693839301* value)
	{
		___Actions_3 = value;
		Il2CppCodeGenWriteBarrier((&___Actions_3), value);
	}

	inline static int32_t get_offset_of_TriggerPoints_4() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___TriggerPoints_4)); }
	inline TransformU5BU5D_t2383644165* get_TriggerPoints_4() const { return ___TriggerPoints_4; }
	inline TransformU5BU5D_t2383644165** get_address_of_TriggerPoints_4() { return &___TriggerPoints_4; }
	inline void set_TriggerPoints_4(TransformU5BU5D_t2383644165* value)
	{
		___TriggerPoints_4 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerPoints_4), value);
	}

	inline static int32_t get_offset_of_CameraID_5() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___CameraID_5)); }
	inline String_t* get_CameraID_5() const { return ___CameraID_5; }
	inline String_t** get_address_of_CameraID_5() { return &___CameraID_5; }
	inline void set_CameraID_5(String_t* value)
	{
		___CameraID_5 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_5), value);
	}

	inline static int32_t get_offset_of_GUIskin_6() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___GUIskin_6)); }
	inline GUISkin_t2122630221 * get_GUIskin_6() const { return ___GUIskin_6; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_6() { return &___GUIskin_6; }
	inline void set_GUIskin_6(GUISkin_t2122630221 * value)
	{
		___GUIskin_6 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_6), value);
	}

	inline static int32_t get_offset_of_bActivated_7() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___bActivated_7)); }
	inline bool get_bActivated_7() const { return ___bActivated_7; }
	inline bool* get_address_of_bActivated_7() { return &___bActivated_7; }
	inline void set_bActivated_7(bool value)
	{
		___bActivated_7 = value;
	}

	inline static int32_t get_offset_of_Text_8() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___Text_8)); }
	inline String_t* get_Text_8() const { return ___Text_8; }
	inline String_t** get_address_of_Text_8() { return &___Text_8; }
	inline void set_Text_8(String_t* value)
	{
		___Text_8 = value;
		Il2CppCodeGenWriteBarrier((&___Text_8), value);
	}

	inline static int32_t get_offset_of_m_talkScene_9() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___m_talkScene_9)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_9() const { return ___m_talkScene_9; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_9() { return &___m_talkScene_9; }
	inline void set_m_talkScene_9(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_9), value);
	}

	inline static int32_t get_offset_of_distanceInteraction_10() { return static_cast<int32_t>(offsetof(GnatsInteraction_t1398631457, ___distanceInteraction_10)); }
	inline float get_distanceInteraction_10() const { return ___distanceInteraction_10; }
	inline float* get_address_of_distanceInteraction_10() { return &___distanceInteraction_10; }
	inline void set_distanceInteraction_10(float value)
	{
		___distanceInteraction_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSINTERACTION_T1398631457_H
#ifndef COLOREXTENSIONS_T1379459159_H
#define COLOREXTENSIONS_T1379459159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorExtensions
struct  ColorExtensions_t1379459159  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREXTENSIONS_T1379459159_H
#ifndef GOODBYE_T2957868680_H
#define GOODBYE_T2957868680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Goodbye
struct  Goodbye_t2957868680  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin Goodbye::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Boolean Goodbye::m_bPlayerIsWaving
	bool ___m_bPlayerIsWaving_3;
	// System.Boolean Goodbye::m_bShowGoodbyeScreen
	bool ___m_bShowGoodbyeScreen_4;
	// System.Boolean Goodbye::m_bSavePointRoutine
	bool ___m_bSavePointRoutine_5;
	// System.Boolean Goodbye::m_bGameWasQuit
	bool ___m_bGameWasQuit_6;
	// UnityEngine.GameObject Goodbye::menu
	GameObject_t2557347079 * ___menu_7;
	// CapturedDataInput Goodbye::instance
	CapturedDataInput_t2616152122 * ___instance_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_bPlayerIsWaving_3() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___m_bPlayerIsWaving_3)); }
	inline bool get_m_bPlayerIsWaving_3() const { return ___m_bPlayerIsWaving_3; }
	inline bool* get_address_of_m_bPlayerIsWaving_3() { return &___m_bPlayerIsWaving_3; }
	inline void set_m_bPlayerIsWaving_3(bool value)
	{
		___m_bPlayerIsWaving_3 = value;
	}

	inline static int32_t get_offset_of_m_bShowGoodbyeScreen_4() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___m_bShowGoodbyeScreen_4)); }
	inline bool get_m_bShowGoodbyeScreen_4() const { return ___m_bShowGoodbyeScreen_4; }
	inline bool* get_address_of_m_bShowGoodbyeScreen_4() { return &___m_bShowGoodbyeScreen_4; }
	inline void set_m_bShowGoodbyeScreen_4(bool value)
	{
		___m_bShowGoodbyeScreen_4 = value;
	}

	inline static int32_t get_offset_of_m_bSavePointRoutine_5() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___m_bSavePointRoutine_5)); }
	inline bool get_m_bSavePointRoutine_5() const { return ___m_bSavePointRoutine_5; }
	inline bool* get_address_of_m_bSavePointRoutine_5() { return &___m_bSavePointRoutine_5; }
	inline void set_m_bSavePointRoutine_5(bool value)
	{
		___m_bSavePointRoutine_5 = value;
	}

	inline static int32_t get_offset_of_m_bGameWasQuit_6() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___m_bGameWasQuit_6)); }
	inline bool get_m_bGameWasQuit_6() const { return ___m_bGameWasQuit_6; }
	inline bool* get_address_of_m_bGameWasQuit_6() { return &___m_bGameWasQuit_6; }
	inline void set_m_bGameWasQuit_6(bool value)
	{
		___m_bGameWasQuit_6 = value;
	}

	inline static int32_t get_offset_of_menu_7() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___menu_7)); }
	inline GameObject_t2557347079 * get_menu_7() const { return ___menu_7; }
	inline GameObject_t2557347079 ** get_address_of_menu_7() { return &___menu_7; }
	inline void set_menu_7(GameObject_t2557347079 * value)
	{
		___menu_7 = value;
		Il2CppCodeGenWriteBarrier((&___menu_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(Goodbye_t2957868680, ___instance_8)); }
	inline CapturedDataInput_t2616152122 * get_instance_8() const { return ___instance_8; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(CapturedDataInput_t2616152122 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOODBYE_T2957868680_H
#ifndef GL_T1956413763_H
#define GL_T1956413763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GL
struct  GL_t1956413763  : public MonoBehaviour_t1618594486
{
public:
	// System.String GL::CurrentInteractNPC
	String_t* ___CurrentInteractNPC_2;

public:
	inline static int32_t get_offset_of_CurrentInteractNPC_2() { return static_cast<int32_t>(offsetof(GL_t1956413763, ___CurrentInteractNPC_2)); }
	inline String_t* get_CurrentInteractNPC_2() const { return ___CurrentInteractNPC_2; }
	inline String_t** get_address_of_CurrentInteractNPC_2() { return &___CurrentInteractNPC_2; }
	inline void set_CurrentInteractNPC_2(String_t* value)
	{
		___CurrentInteractNPC_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentInteractNPC_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GL_T1956413763_H
#ifndef GETAROUNDSPARX_T2199752033_H
#define GETAROUNDSPARX_T2199752033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetAroundSparx
struct  GetAroundSparx_t2199752033  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin GetAroundSparx::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject GetAroundSparx::m_theGuardian
	GameObject_t2557347079 * ___m_theGuardian_3;
	// UnityEngine.GameObject GetAroundSparx::m_theMontor
	GameObject_t2557347079 * ___m_theMontor_4;
	// UnityEngine.Texture2D GetAroundSparx::m_mouseCorsorImage
	Texture2D_t3063074017 * ___m_mouseCorsorImage_5;
	// UnityEngine.Texture2D GetAroundSparx::m_closeImage
	Texture2D_t3063074017 * ___m_closeImage_6;
	// UnityEngine.Texture2D GetAroundSparx::m_backPackImage
	Texture2D_t3063074017 * ___m_backPackImage_7;
	// UnityEngine.Texture2D GetAroundSparx::m_sparxManulImage
	Texture2D_t3063074017 * ___m_sparxManulImage_8;
	// UnityEngine.Texture2D GetAroundSparx::loadingIcon
	Texture2D_t3063074017 * ___loadingIcon_9;
	// UnityEngine.Texture2D GetAroundSparx::m_Eagle
	Texture2D_t3063074017 * ___m_Eagle_10;
	// UnityEngine.Texture2D GetAroundSparx::m_Latern
	Texture2D_t3063074017 * ___m_Latern_11;
	// UnityEngine.Texture2D GetAroundSparx::m_Gnat
	Texture2D_t3063074017 * ___m_Gnat_12;
	// UnityEngine.Texture2D GetAroundSparx::m_Lader
	Texture2D_t3063074017 * ___m_Lader_13;
	// UnityEngine.Texture2D GetAroundSparx::m_Control
	Texture2D_t3063074017 * ___m_Control_14;
	// UnityEngine.Texture2D GetAroundSparx::m_Control2
	Texture2D_t3063074017 * ___m_Control2_15;
	// UnityEngine.GameObject GetAroundSparx::m_gardianClone
	GameObject_t2557347079 * ___m_gardianClone_16;
	// UnityEngine.GameObject GetAroundSparx::m_mentorClone
	GameObject_t2557347079 * ___m_mentorClone_17;
	// TalkScenes GetAroundSparx::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_18;
	// UnityEngine.GameObject GetAroundSparx::m_gGardianObject
	GameObject_t2557347079 * ___m_gGardianObject_19;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_theGuardian_3() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_theGuardian_3)); }
	inline GameObject_t2557347079 * get_m_theGuardian_3() const { return ___m_theGuardian_3; }
	inline GameObject_t2557347079 ** get_address_of_m_theGuardian_3() { return &___m_theGuardian_3; }
	inline void set_m_theGuardian_3(GameObject_t2557347079 * value)
	{
		___m_theGuardian_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_theGuardian_3), value);
	}

	inline static int32_t get_offset_of_m_theMontor_4() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_theMontor_4)); }
	inline GameObject_t2557347079 * get_m_theMontor_4() const { return ___m_theMontor_4; }
	inline GameObject_t2557347079 ** get_address_of_m_theMontor_4() { return &___m_theMontor_4; }
	inline void set_m_theMontor_4(GameObject_t2557347079 * value)
	{
		___m_theMontor_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_theMontor_4), value);
	}

	inline static int32_t get_offset_of_m_mouseCorsorImage_5() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_mouseCorsorImage_5)); }
	inline Texture2D_t3063074017 * get_m_mouseCorsorImage_5() const { return ___m_mouseCorsorImage_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_mouseCorsorImage_5() { return &___m_mouseCorsorImage_5; }
	inline void set_m_mouseCorsorImage_5(Texture2D_t3063074017 * value)
	{
		___m_mouseCorsorImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_mouseCorsorImage_5), value);
	}

	inline static int32_t get_offset_of_m_closeImage_6() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_closeImage_6)); }
	inline Texture2D_t3063074017 * get_m_closeImage_6() const { return ___m_closeImage_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_closeImage_6() { return &___m_closeImage_6; }
	inline void set_m_closeImage_6(Texture2D_t3063074017 * value)
	{
		___m_closeImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_closeImage_6), value);
	}

	inline static int32_t get_offset_of_m_backPackImage_7() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_backPackImage_7)); }
	inline Texture2D_t3063074017 * get_m_backPackImage_7() const { return ___m_backPackImage_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_backPackImage_7() { return &___m_backPackImage_7; }
	inline void set_m_backPackImage_7(Texture2D_t3063074017 * value)
	{
		___m_backPackImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_backPackImage_7), value);
	}

	inline static int32_t get_offset_of_m_sparxManulImage_8() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_sparxManulImage_8)); }
	inline Texture2D_t3063074017 * get_m_sparxManulImage_8() const { return ___m_sparxManulImage_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_sparxManulImage_8() { return &___m_sparxManulImage_8; }
	inline void set_m_sparxManulImage_8(Texture2D_t3063074017 * value)
	{
		___m_sparxManulImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_sparxManulImage_8), value);
	}

	inline static int32_t get_offset_of_loadingIcon_9() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___loadingIcon_9)); }
	inline Texture2D_t3063074017 * get_loadingIcon_9() const { return ___loadingIcon_9; }
	inline Texture2D_t3063074017 ** get_address_of_loadingIcon_9() { return &___loadingIcon_9; }
	inline void set_loadingIcon_9(Texture2D_t3063074017 * value)
	{
		___loadingIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&___loadingIcon_9), value);
	}

	inline static int32_t get_offset_of_m_Eagle_10() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Eagle_10)); }
	inline Texture2D_t3063074017 * get_m_Eagle_10() const { return ___m_Eagle_10; }
	inline Texture2D_t3063074017 ** get_address_of_m_Eagle_10() { return &___m_Eagle_10; }
	inline void set_m_Eagle_10(Texture2D_t3063074017 * value)
	{
		___m_Eagle_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Eagle_10), value);
	}

	inline static int32_t get_offset_of_m_Latern_11() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Latern_11)); }
	inline Texture2D_t3063074017 * get_m_Latern_11() const { return ___m_Latern_11; }
	inline Texture2D_t3063074017 ** get_address_of_m_Latern_11() { return &___m_Latern_11; }
	inline void set_m_Latern_11(Texture2D_t3063074017 * value)
	{
		___m_Latern_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Latern_11), value);
	}

	inline static int32_t get_offset_of_m_Gnat_12() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Gnat_12)); }
	inline Texture2D_t3063074017 * get_m_Gnat_12() const { return ___m_Gnat_12; }
	inline Texture2D_t3063074017 ** get_address_of_m_Gnat_12() { return &___m_Gnat_12; }
	inline void set_m_Gnat_12(Texture2D_t3063074017 * value)
	{
		___m_Gnat_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Gnat_12), value);
	}

	inline static int32_t get_offset_of_m_Lader_13() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Lader_13)); }
	inline Texture2D_t3063074017 * get_m_Lader_13() const { return ___m_Lader_13; }
	inline Texture2D_t3063074017 ** get_address_of_m_Lader_13() { return &___m_Lader_13; }
	inline void set_m_Lader_13(Texture2D_t3063074017 * value)
	{
		___m_Lader_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lader_13), value);
	}

	inline static int32_t get_offset_of_m_Control_14() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Control_14)); }
	inline Texture2D_t3063074017 * get_m_Control_14() const { return ___m_Control_14; }
	inline Texture2D_t3063074017 ** get_address_of_m_Control_14() { return &___m_Control_14; }
	inline void set_m_Control_14(Texture2D_t3063074017 * value)
	{
		___m_Control_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Control_14), value);
	}

	inline static int32_t get_offset_of_m_Control2_15() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_Control2_15)); }
	inline Texture2D_t3063074017 * get_m_Control2_15() const { return ___m_Control2_15; }
	inline Texture2D_t3063074017 ** get_address_of_m_Control2_15() { return &___m_Control2_15; }
	inline void set_m_Control2_15(Texture2D_t3063074017 * value)
	{
		___m_Control2_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Control2_15), value);
	}

	inline static int32_t get_offset_of_m_gardianClone_16() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_gardianClone_16)); }
	inline GameObject_t2557347079 * get_m_gardianClone_16() const { return ___m_gardianClone_16; }
	inline GameObject_t2557347079 ** get_address_of_m_gardianClone_16() { return &___m_gardianClone_16; }
	inline void set_m_gardianClone_16(GameObject_t2557347079 * value)
	{
		___m_gardianClone_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_gardianClone_16), value);
	}

	inline static int32_t get_offset_of_m_mentorClone_17() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_mentorClone_17)); }
	inline GameObject_t2557347079 * get_m_mentorClone_17() const { return ___m_mentorClone_17; }
	inline GameObject_t2557347079 ** get_address_of_m_mentorClone_17() { return &___m_mentorClone_17; }
	inline void set_m_mentorClone_17(GameObject_t2557347079 * value)
	{
		___m_mentorClone_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_mentorClone_17), value);
	}

	inline static int32_t get_offset_of_m_talkScene_18() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_talkScene_18)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_18() const { return ___m_talkScene_18; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_18() { return &___m_talkScene_18; }
	inline void set_m_talkScene_18(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_18), value);
	}

	inline static int32_t get_offset_of_m_gGardianObject_19() { return static_cast<int32_t>(offsetof(GetAroundSparx_t2199752033, ___m_gGardianObject_19)); }
	inline GameObject_t2557347079 * get_m_gGardianObject_19() const { return ___m_gGardianObject_19; }
	inline GameObject_t2557347079 ** get_address_of_m_gGardianObject_19() { return &___m_gGardianObject_19; }
	inline void set_m_gGardianObject_19(GameObject_t2557347079 * value)
	{
		___m_gGardianObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_gGardianObject_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAROUNDSPARX_T2199752033_H
#ifndef GEMHELPTRIGER_T1248575134_H
#define GEMHELPTRIGER_T1248575134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GemHelpTriger
struct  GemHelpTriger_t1248575134  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean GemHelpTriger::m_EagleTriggerEntered
	bool ___m_EagleTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_EagleTriggerEntered_2() { return static_cast<int32_t>(offsetof(GemHelpTriger_t1248575134, ___m_EagleTriggerEntered_2)); }
	inline bool get_m_EagleTriggerEntered_2() const { return ___m_EagleTriggerEntered_2; }
	inline bool* get_address_of_m_EagleTriggerEntered_2() { return &___m_EagleTriggerEntered_2; }
	inline void set_m_EagleTriggerEntered_2(bool value)
	{
		___m_EagleTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEMHELPTRIGER_T1248575134_H
#ifndef GEMHELP_T3073439280_H
#define GEMHELP_T3073439280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GemHelp
struct  GemHelp_t3073439280  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean GemHelp::bActivated
	bool ___bActivated_2;
	// UnityEngine.GUISkin GemHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_3;
	// System.String GemHelp::Text
	String_t* ___Text_4;

public:
	inline static int32_t get_offset_of_bActivated_2() { return static_cast<int32_t>(offsetof(GemHelp_t3073439280, ___bActivated_2)); }
	inline bool get_bActivated_2() const { return ___bActivated_2; }
	inline bool* get_address_of_bActivated_2() { return &___bActivated_2; }
	inline void set_bActivated_2(bool value)
	{
		___bActivated_2 = value;
	}

	inline static int32_t get_offset_of_GUIskin_3() { return static_cast<int32_t>(offsetof(GemHelp_t3073439280, ___GUIskin_3)); }
	inline GUISkin_t2122630221 * get_GUIskin_3() const { return ___GUIskin_3; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_3() { return &___GUIskin_3; }
	inline void set_GUIskin_3(GUISkin_t2122630221 * value)
	{
		___GUIskin_3 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(GemHelp_t3073439280, ___Text_4)); }
	inline String_t* get_Text_4() const { return ___Text_4; }
	inline String_t** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(String_t* value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEMHELP_T3073439280_H
#ifndef FEELINGDOWNSCREEN_T3177656924_H
#define FEELINGDOWNSCREEN_T3177656924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeelingDownScreen
struct  FeelingDownScreen_t3177656924  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin FeelingDownScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D FeelingDownScreen::m_tFeelingDownTexture
	Texture2D_t3063074017 * ___m_tFeelingDownTexture_3;
	// NextScreen FeelingDownScreen::screen
	NextScreen_t2084349028 * ___screen_4;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(FeelingDownScreen_t3177656924, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_tFeelingDownTexture_3() { return static_cast<int32_t>(offsetof(FeelingDownScreen_t3177656924, ___m_tFeelingDownTexture_3)); }
	inline Texture2D_t3063074017 * get_m_tFeelingDownTexture_3() const { return ___m_tFeelingDownTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFeelingDownTexture_3() { return &___m_tFeelingDownTexture_3; }
	inline void set_m_tFeelingDownTexture_3(Texture2D_t3063074017 * value)
	{
		___m_tFeelingDownTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFeelingDownTexture_3), value);
	}

	inline static int32_t get_offset_of_screen_4() { return static_cast<int32_t>(offsetof(FeelingDownScreen_t3177656924, ___screen_4)); }
	inline NextScreen_t2084349028 * get_screen_4() const { return ___screen_4; }
	inline NextScreen_t2084349028 ** get_address_of_screen_4() { return &___screen_4; }
	inline void set_screen_4(NextScreen_t2084349028 * value)
	{
		___screen_4 = value;
		Il2CppCodeGenWriteBarrier((&___screen_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEELINGDOWNSCREEN_T3177656924_H
#ifndef EAGLEHELPTRIGGER_T1763195580_H
#define EAGLEHELPTRIGGER_T1763195580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleHelpTrigger
struct  EagleHelpTrigger_t1763195580  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean EagleHelpTrigger::m_EagleTriggerEntered
	bool ___m_EagleTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_EagleTriggerEntered_2() { return static_cast<int32_t>(offsetof(EagleHelpTrigger_t1763195580, ___m_EagleTriggerEntered_2)); }
	inline bool get_m_EagleTriggerEntered_2() const { return ___m_EagleTriggerEntered_2; }
	inline bool* get_address_of_m_EagleTriggerEntered_2() { return &___m_EagleTriggerEntered_2; }
	inline void set_m_EagleTriggerEntered_2(bool value)
	{
		___m_EagleTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLEHELPTRIGGER_T1763195580_H
#ifndef CHARACTERCUSTOMISE_T2467338958_H
#define CHARACTERCUSTOMISE_T2467338958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterCustomise
struct  CharacterCustomise_t2467338958  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCUSTOMISE_T2467338958_H
#ifndef EAGLEHELP_T1347797201_H
#define EAGLEHELP_T1347797201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleHelp
struct  EagleHelp_t1347797201  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean EagleHelp::bActivated
	bool ___bActivated_2;
	// UnityEngine.GUISkin EagleHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_3;
	// System.String EagleHelp::Text
	String_t* ___Text_4;
	// OutlineObject EagleHelp::eagleOutline
	OutlineObject_t941793957 * ___eagleOutline_5;

public:
	inline static int32_t get_offset_of_bActivated_2() { return static_cast<int32_t>(offsetof(EagleHelp_t1347797201, ___bActivated_2)); }
	inline bool get_bActivated_2() const { return ___bActivated_2; }
	inline bool* get_address_of_bActivated_2() { return &___bActivated_2; }
	inline void set_bActivated_2(bool value)
	{
		___bActivated_2 = value;
	}

	inline static int32_t get_offset_of_GUIskin_3() { return static_cast<int32_t>(offsetof(EagleHelp_t1347797201, ___GUIskin_3)); }
	inline GUISkin_t2122630221 * get_GUIskin_3() const { return ___GUIskin_3; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_3() { return &___GUIskin_3; }
	inline void set_GUIskin_3(GUISkin_t2122630221 * value)
	{
		___GUIskin_3 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(EagleHelp_t1347797201, ___Text_4)); }
	inline String_t* get_Text_4() const { return ___Text_4; }
	inline String_t** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(String_t* value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of_eagleOutline_5() { return static_cast<int32_t>(offsetof(EagleHelp_t1347797201, ___eagleOutline_5)); }
	inline OutlineObject_t941793957 * get_eagleOutline_5() const { return ___eagleOutline_5; }
	inline OutlineObject_t941793957 ** get_address_of_eagleOutline_5() { return &___eagleOutline_5; }
	inline void set_eagleOutline_5(OutlineObject_t941793957 * value)
	{
		___eagleOutline_5 = value;
		Il2CppCodeGenWriteBarrier((&___eagleOutline_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLEHELP_T1347797201_H
#ifndef CHARACTERTALKTRIGGER_T3203602799_H
#define CHARACTERTALKTRIGGER_T3203602799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterTalkTrigger
struct  CharacterTalkTrigger_t3203602799  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERTALKTRIGGER_T3203602799_H
#ifndef DRAGANDDROPWORDS_T1762529990_H
#define DRAGANDDROPWORDS_T1762529990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragAndDropWords
struct  DragAndDropWords_t1762529990  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Rect DragAndDropWords::k_zeroRect
	Rect_t3039462994  ___k_zeroRect_2;
	// UnityEngine.GUISkin DragAndDropWords::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// System.String DragAndDropWords::m_conversationBoxText
	String_t* ___m_conversationBoxText_4;
	// UnityEngine.Vector2[] DragAndDropWords::m_KeywordDimensions
	Vector2U5BU5D_t1220531434* ___m_KeywordDimensions_7;
	// UnityEngine.Rect[] DragAndDropWords::m_myOriRect
	RectU5BU5D_t141872167* ___m_myOriRect_8;
	// System.String[] DragAndDropWords::m_keyWordText
	StringU5BU5D_t2511808107* ___m_keyWordText_9;
	// UnityEngine.Rect[] DragAndDropWords::m_destinationOriKeyWordRect
	RectU5BU5D_t141872167* ___m_destinationOriKeyWordRect_12;
	// UnityEngine.Rect[] DragAndDropWords::m_destinationRect
	RectU5BU5D_t141872167* ___m_destinationRect_14;
	// UnityEngine.Rect[] DragAndDropWords::m_movableStringRect
	RectU5BU5D_t141872167* ___m_movableStringRect_15;
	// UnityEngine.Rect DragAndDropWords::m_currentClickedStringRect
	Rect_t3039462994  ___m_currentClickedStringRect_16;
	// System.String DragAndDropWords::m_currentClickedString
	String_t* ___m_currentClickedString_17;
	// System.Int32 DragAndDropWords::m_currentIndex
	int32_t ___m_currentIndex_18;
	// System.Boolean DragAndDropWords::b_dragging
	bool ___b_dragging_19;
	// UnityEngine.Texture2D DragAndDropWords::m_tFillFieldBackground
	Texture2D_t3063074017 * ___m_tFillFieldBackground_20;
	// UnityEngine.Texture2D DragAndDropWords::m_tDestinationBackground
	Texture2D_t3063074017 * ___m_tDestinationBackground_21;
	// UnityEngine.Texture2D DragAndDropWords::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_22;
	// UnityEngine.GameObject DragAndDropWords::menu
	GameObject_t2557347079 * ___menu_23;

public:
	inline static int32_t get_offset_of_k_zeroRect_2() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___k_zeroRect_2)); }
	inline Rect_t3039462994  get_k_zeroRect_2() const { return ___k_zeroRect_2; }
	inline Rect_t3039462994 * get_address_of_k_zeroRect_2() { return &___k_zeroRect_2; }
	inline void set_k_zeroRect_2(Rect_t3039462994  value)
	{
		___k_zeroRect_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_4() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_conversationBoxText_4)); }
	inline String_t* get_m_conversationBoxText_4() const { return ___m_conversationBoxText_4; }
	inline String_t** get_address_of_m_conversationBoxText_4() { return &___m_conversationBoxText_4; }
	inline void set_m_conversationBoxText_4(String_t* value)
	{
		___m_conversationBoxText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_4), value);
	}

	inline static int32_t get_offset_of_m_KeywordDimensions_7() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_KeywordDimensions_7)); }
	inline Vector2U5BU5D_t1220531434* get_m_KeywordDimensions_7() const { return ___m_KeywordDimensions_7; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_KeywordDimensions_7() { return &___m_KeywordDimensions_7; }
	inline void set_m_KeywordDimensions_7(Vector2U5BU5D_t1220531434* value)
	{
		___m_KeywordDimensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeywordDimensions_7), value);
	}

	inline static int32_t get_offset_of_m_myOriRect_8() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_myOriRect_8)); }
	inline RectU5BU5D_t141872167* get_m_myOriRect_8() const { return ___m_myOriRect_8; }
	inline RectU5BU5D_t141872167** get_address_of_m_myOriRect_8() { return &___m_myOriRect_8; }
	inline void set_m_myOriRect_8(RectU5BU5D_t141872167* value)
	{
		___m_myOriRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_myOriRect_8), value);
	}

	inline static int32_t get_offset_of_m_keyWordText_9() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_keyWordText_9)); }
	inline StringU5BU5D_t2511808107* get_m_keyWordText_9() const { return ___m_keyWordText_9; }
	inline StringU5BU5D_t2511808107** get_address_of_m_keyWordText_9() { return &___m_keyWordText_9; }
	inline void set_m_keyWordText_9(StringU5BU5D_t2511808107* value)
	{
		___m_keyWordText_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyWordText_9), value);
	}

	inline static int32_t get_offset_of_m_destinationOriKeyWordRect_12() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_destinationOriKeyWordRect_12)); }
	inline RectU5BU5D_t141872167* get_m_destinationOriKeyWordRect_12() const { return ___m_destinationOriKeyWordRect_12; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationOriKeyWordRect_12() { return &___m_destinationOriKeyWordRect_12; }
	inline void set_m_destinationOriKeyWordRect_12(RectU5BU5D_t141872167* value)
	{
		___m_destinationOriKeyWordRect_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationOriKeyWordRect_12), value);
	}

	inline static int32_t get_offset_of_m_destinationRect_14() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_destinationRect_14)); }
	inline RectU5BU5D_t141872167* get_m_destinationRect_14() const { return ___m_destinationRect_14; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinationRect_14() { return &___m_destinationRect_14; }
	inline void set_m_destinationRect_14(RectU5BU5D_t141872167* value)
	{
		___m_destinationRect_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationRect_14), value);
	}

	inline static int32_t get_offset_of_m_movableStringRect_15() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_movableStringRect_15)); }
	inline RectU5BU5D_t141872167* get_m_movableStringRect_15() const { return ___m_movableStringRect_15; }
	inline RectU5BU5D_t141872167** get_address_of_m_movableStringRect_15() { return &___m_movableStringRect_15; }
	inline void set_m_movableStringRect_15(RectU5BU5D_t141872167* value)
	{
		___m_movableStringRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_movableStringRect_15), value);
	}

	inline static int32_t get_offset_of_m_currentClickedStringRect_16() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_currentClickedStringRect_16)); }
	inline Rect_t3039462994  get_m_currentClickedStringRect_16() const { return ___m_currentClickedStringRect_16; }
	inline Rect_t3039462994 * get_address_of_m_currentClickedStringRect_16() { return &___m_currentClickedStringRect_16; }
	inline void set_m_currentClickedStringRect_16(Rect_t3039462994  value)
	{
		___m_currentClickedStringRect_16 = value;
	}

	inline static int32_t get_offset_of_m_currentClickedString_17() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_currentClickedString_17)); }
	inline String_t* get_m_currentClickedString_17() const { return ___m_currentClickedString_17; }
	inline String_t** get_address_of_m_currentClickedString_17() { return &___m_currentClickedString_17; }
	inline void set_m_currentClickedString_17(String_t* value)
	{
		___m_currentClickedString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentClickedString_17), value);
	}

	inline static int32_t get_offset_of_m_currentIndex_18() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_currentIndex_18)); }
	inline int32_t get_m_currentIndex_18() const { return ___m_currentIndex_18; }
	inline int32_t* get_address_of_m_currentIndex_18() { return &___m_currentIndex_18; }
	inline void set_m_currentIndex_18(int32_t value)
	{
		___m_currentIndex_18 = value;
	}

	inline static int32_t get_offset_of_b_dragging_19() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___b_dragging_19)); }
	inline bool get_b_dragging_19() const { return ___b_dragging_19; }
	inline bool* get_address_of_b_dragging_19() { return &___b_dragging_19; }
	inline void set_b_dragging_19(bool value)
	{
		___b_dragging_19 = value;
	}

	inline static int32_t get_offset_of_m_tFillFieldBackground_20() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_tFillFieldBackground_20)); }
	inline Texture2D_t3063074017 * get_m_tFillFieldBackground_20() const { return ___m_tFillFieldBackground_20; }
	inline Texture2D_t3063074017 ** get_address_of_m_tFillFieldBackground_20() { return &___m_tFillFieldBackground_20; }
	inline void set_m_tFillFieldBackground_20(Texture2D_t3063074017 * value)
	{
		___m_tFillFieldBackground_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_tFillFieldBackground_20), value);
	}

	inline static int32_t get_offset_of_m_tDestinationBackground_21() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_tDestinationBackground_21)); }
	inline Texture2D_t3063074017 * get_m_tDestinationBackground_21() const { return ___m_tDestinationBackground_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_tDestinationBackground_21() { return &___m_tDestinationBackground_21; }
	inline void set_m_tDestinationBackground_21(Texture2D_t3063074017 * value)
	{
		___m_tDestinationBackground_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_tDestinationBackground_21), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_22() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___m_tConversationBackground_22)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_22() const { return ___m_tConversationBackground_22; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_22() { return &___m_tConversationBackground_22; }
	inline void set_m_tConversationBackground_22(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_22), value);
	}

	inline static int32_t get_offset_of_menu_23() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990, ___menu_23)); }
	inline GameObject_t2557347079 * get_menu_23() const { return ___menu_23; }
	inline GameObject_t2557347079 ** get_address_of_menu_23() { return &___menu_23; }
	inline void set_menu_23(GameObject_t2557347079 * value)
	{
		___menu_23 = value;
		Il2CppCodeGenWriteBarrier((&___menu_23), value);
	}
};

struct DragAndDropWords_t1762529990_StaticFields
{
public:
	// System.Single DragAndDropWords::m_keywordWidth
	float ___m_keywordWidth_5;
	// System.Single DragAndDropWords::m_keywordHeight
	float ___m_keywordHeight_6;
	// System.Single DragAndDropWords::m_dextinationRectWidth
	float ___m_dextinationRectWidth_10;
	// System.Single DragAndDropWords::m_dextinationRectHeight
	float ___m_dextinationRectHeight_11;
	// System.String[] DragAndDropWords::m_L1destinationText
	StringU5BU5D_t2511808107* ___m_L1destinationText_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DragAndDropWords::<>f__switch$map3
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map3_24;

public:
	inline static int32_t get_offset_of_m_keywordWidth_5() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___m_keywordWidth_5)); }
	inline float get_m_keywordWidth_5() const { return ___m_keywordWidth_5; }
	inline float* get_address_of_m_keywordWidth_5() { return &___m_keywordWidth_5; }
	inline void set_m_keywordWidth_5(float value)
	{
		___m_keywordWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_keywordHeight_6() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___m_keywordHeight_6)); }
	inline float get_m_keywordHeight_6() const { return ___m_keywordHeight_6; }
	inline float* get_address_of_m_keywordHeight_6() { return &___m_keywordHeight_6; }
	inline void set_m_keywordHeight_6(float value)
	{
		___m_keywordHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectWidth_10() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___m_dextinationRectWidth_10)); }
	inline float get_m_dextinationRectWidth_10() const { return ___m_dextinationRectWidth_10; }
	inline float* get_address_of_m_dextinationRectWidth_10() { return &___m_dextinationRectWidth_10; }
	inline void set_m_dextinationRectWidth_10(float value)
	{
		___m_dextinationRectWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_dextinationRectHeight_11() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___m_dextinationRectHeight_11)); }
	inline float get_m_dextinationRectHeight_11() const { return ___m_dextinationRectHeight_11; }
	inline float* get_address_of_m_dextinationRectHeight_11() { return &___m_dextinationRectHeight_11; }
	inline void set_m_dextinationRectHeight_11(float value)
	{
		___m_dextinationRectHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_L1destinationText_13() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___m_L1destinationText_13)); }
	inline StringU5BU5D_t2511808107* get_m_L1destinationText_13() const { return ___m_L1destinationText_13; }
	inline StringU5BU5D_t2511808107** get_address_of_m_L1destinationText_13() { return &___m_L1destinationText_13; }
	inline void set_m_L1destinationText_13(StringU5BU5D_t2511808107* value)
	{
		___m_L1destinationText_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_L1destinationText_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_24() { return static_cast<int32_t>(offsetof(DragAndDropWords_t1762529990_StaticFields, ___U3CU3Ef__switchU24map3_24)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map3_24() const { return ___U3CU3Ef__switchU24map3_24; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map3_24() { return &___U3CU3Ef__switchU24map3_24; }
	inline void set_U3CU3Ef__switchU24map3_24(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map3_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGANDDROPWORDS_T1762529990_H
#ifndef CUSTOMISATION_T1896805424_H
#define CUSTOMISATION_T1896805424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Customisation
struct  Customisation_t1896805424  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject Customisation::character
	GameObject_t2557347079 * ___character_2;
	// UnityEngine.Color Customisation::m_rCurrentHairColor
	Color_t2582018970  ___m_rCurrentHairColor_3;
	// UnityEngine.Color Customisation::m_rCurrentTrimColor
	Color_t2582018970  ___m_rCurrentTrimColor_4;
	// UnityEngine.Texture2D Customisation::m_tCurrentTrimTexture
	Texture2D_t3063074017 * ___m_tCurrentTrimTexture_5;
	// System.Int32 Customisation::m_iTotalHairColorNum
	int32_t ___m_iTotalHairColorNum_6;
	// System.Int32 Customisation::m_iCurrentHairColorNum
	int32_t ___m_iCurrentHairColorNum_7;
	// System.Int32 Customisation::m_iTotalEyeColorNum
	int32_t ___m_iTotalEyeColorNum_8;
	// System.Int32 Customisation::m_iCurrentEyeColorNum
	int32_t ___m_iCurrentEyeColorNum_9;
	// UnityEngine.Color Customisation::m_rCurrentEyeColor
	Color_t2582018970  ___m_rCurrentEyeColor_10;
	// System.Int32 Customisation::m_iTotalSkinColorNum
	int32_t ___m_iTotalSkinColorNum_11;
	// System.Int32 Customisation::m_iCurrentSkinColorNum
	int32_t ___m_iCurrentSkinColorNum_12;
	// System.Int32 Customisation::m_iCurrentHairStyleNum
	int32_t ___m_iCurrentHairStyleNum_13;
	// System.Int32 Customisation::m_iCurrentTrimNum
	int32_t ___m_iCurrentTrimNum_14;
	// System.Int32 Customisation::m_iCurrentBackpackNum
	int32_t ___m_iCurrentBackpackNum_15;
	// System.Int32 Customisation::m_iTotalTrimColorNum
	int32_t ___m_iTotalTrimColorNum_16;
	// System.Int32 Customisation::m_iCurrentTrimColorNum
	int32_t ___m_iCurrentTrimColorNum_17;
	// System.Int32 Customisation::m_iTotalClothColorNum
	int32_t ___m_iTotalClothColorNum_18;
	// System.Int32 Customisation::m_iCurrentClothColorNum
	int32_t ___m_iCurrentClothColorNum_19;
	// UnityEngine.Color Customisation::m_rCurrentColorColor
	Color_t2582018970  ___m_rCurrentColorColor_20;
	// System.Int32 Customisation::m_iTotalTrimNum
	int32_t ___m_iTotalTrimNum_21;
	// System.Int32 Customisation::m_iTotalBackpackNum
	int32_t ___m_iTotalBackpackNum_22;
	// System.Int32 Customisation::m_iBodyType
	int32_t ___m_iBodyType_23;
	// UnityEngine.Texture2D[] Customisation::m_tTrimTextures
	Texture2DU5BU5D_t3304433276* ___m_tTrimTextures_24;
	// System.Int32 Customisation::m_iTotalHairStyleNumber
	int32_t ___m_iTotalHairStyleNumber_25;
	// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer> Customisation::m_hairMeshes
	List_1_t1370252420 * ___m_hairMeshes_26;
	// CapturedDataInput Customisation::instance
	CapturedDataInput_t2616152122 * ___instance_27;
	// CharacterCreation Customisation::creationScreen
	CharacterCreation_t2214710272 * ___creationScreen_28;

public:
	inline static int32_t get_offset_of_character_2() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___character_2)); }
	inline GameObject_t2557347079 * get_character_2() const { return ___character_2; }
	inline GameObject_t2557347079 ** get_address_of_character_2() { return &___character_2; }
	inline void set_character_2(GameObject_t2557347079 * value)
	{
		___character_2 = value;
		Il2CppCodeGenWriteBarrier((&___character_2), value);
	}

	inline static int32_t get_offset_of_m_rCurrentHairColor_3() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentHairColor_3)); }
	inline Color_t2582018970  get_m_rCurrentHairColor_3() const { return ___m_rCurrentHairColor_3; }
	inline Color_t2582018970 * get_address_of_m_rCurrentHairColor_3() { return &___m_rCurrentHairColor_3; }
	inline void set_m_rCurrentHairColor_3(Color_t2582018970  value)
	{
		___m_rCurrentHairColor_3 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentTrimColor_4() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentTrimColor_4)); }
	inline Color_t2582018970  get_m_rCurrentTrimColor_4() const { return ___m_rCurrentTrimColor_4; }
	inline Color_t2582018970 * get_address_of_m_rCurrentTrimColor_4() { return &___m_rCurrentTrimColor_4; }
	inline void set_m_rCurrentTrimColor_4(Color_t2582018970  value)
	{
		___m_rCurrentTrimColor_4 = value;
	}

	inline static int32_t get_offset_of_m_tCurrentTrimTexture_5() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_tCurrentTrimTexture_5)); }
	inline Texture2D_t3063074017 * get_m_tCurrentTrimTexture_5() const { return ___m_tCurrentTrimTexture_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCurrentTrimTexture_5() { return &___m_tCurrentTrimTexture_5; }
	inline void set_m_tCurrentTrimTexture_5(Texture2D_t3063074017 * value)
	{
		___m_tCurrentTrimTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCurrentTrimTexture_5), value);
	}

	inline static int32_t get_offset_of_m_iTotalHairColorNum_6() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalHairColorNum_6)); }
	inline int32_t get_m_iTotalHairColorNum_6() const { return ___m_iTotalHairColorNum_6; }
	inline int32_t* get_address_of_m_iTotalHairColorNum_6() { return &___m_iTotalHairColorNum_6; }
	inline void set_m_iTotalHairColorNum_6(int32_t value)
	{
		___m_iTotalHairColorNum_6 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentHairColorNum_7() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentHairColorNum_7)); }
	inline int32_t get_m_iCurrentHairColorNum_7() const { return ___m_iCurrentHairColorNum_7; }
	inline int32_t* get_address_of_m_iCurrentHairColorNum_7() { return &___m_iCurrentHairColorNum_7; }
	inline void set_m_iCurrentHairColorNum_7(int32_t value)
	{
		___m_iCurrentHairColorNum_7 = value;
	}

	inline static int32_t get_offset_of_m_iTotalEyeColorNum_8() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalEyeColorNum_8)); }
	inline int32_t get_m_iTotalEyeColorNum_8() const { return ___m_iTotalEyeColorNum_8; }
	inline int32_t* get_address_of_m_iTotalEyeColorNum_8() { return &___m_iTotalEyeColorNum_8; }
	inline void set_m_iTotalEyeColorNum_8(int32_t value)
	{
		___m_iTotalEyeColorNum_8 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentEyeColorNum_9() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentEyeColorNum_9)); }
	inline int32_t get_m_iCurrentEyeColorNum_9() const { return ___m_iCurrentEyeColorNum_9; }
	inline int32_t* get_address_of_m_iCurrentEyeColorNum_9() { return &___m_iCurrentEyeColorNum_9; }
	inline void set_m_iCurrentEyeColorNum_9(int32_t value)
	{
		___m_iCurrentEyeColorNum_9 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentEyeColor_10() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentEyeColor_10)); }
	inline Color_t2582018970  get_m_rCurrentEyeColor_10() const { return ___m_rCurrentEyeColor_10; }
	inline Color_t2582018970 * get_address_of_m_rCurrentEyeColor_10() { return &___m_rCurrentEyeColor_10; }
	inline void set_m_rCurrentEyeColor_10(Color_t2582018970  value)
	{
		___m_rCurrentEyeColor_10 = value;
	}

	inline static int32_t get_offset_of_m_iTotalSkinColorNum_11() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalSkinColorNum_11)); }
	inline int32_t get_m_iTotalSkinColorNum_11() const { return ___m_iTotalSkinColorNum_11; }
	inline int32_t* get_address_of_m_iTotalSkinColorNum_11() { return &___m_iTotalSkinColorNum_11; }
	inline void set_m_iTotalSkinColorNum_11(int32_t value)
	{
		___m_iTotalSkinColorNum_11 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentSkinColorNum_12() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentSkinColorNum_12)); }
	inline int32_t get_m_iCurrentSkinColorNum_12() const { return ___m_iCurrentSkinColorNum_12; }
	inline int32_t* get_address_of_m_iCurrentSkinColorNum_12() { return &___m_iCurrentSkinColorNum_12; }
	inline void set_m_iCurrentSkinColorNum_12(int32_t value)
	{
		___m_iCurrentSkinColorNum_12 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentHairStyleNum_13() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentHairStyleNum_13)); }
	inline int32_t get_m_iCurrentHairStyleNum_13() const { return ___m_iCurrentHairStyleNum_13; }
	inline int32_t* get_address_of_m_iCurrentHairStyleNum_13() { return &___m_iCurrentHairStyleNum_13; }
	inline void set_m_iCurrentHairStyleNum_13(int32_t value)
	{
		___m_iCurrentHairStyleNum_13 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentTrimNum_14() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentTrimNum_14)); }
	inline int32_t get_m_iCurrentTrimNum_14() const { return ___m_iCurrentTrimNum_14; }
	inline int32_t* get_address_of_m_iCurrentTrimNum_14() { return &___m_iCurrentTrimNum_14; }
	inline void set_m_iCurrentTrimNum_14(int32_t value)
	{
		___m_iCurrentTrimNum_14 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentBackpackNum_15() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentBackpackNum_15)); }
	inline int32_t get_m_iCurrentBackpackNum_15() const { return ___m_iCurrentBackpackNum_15; }
	inline int32_t* get_address_of_m_iCurrentBackpackNum_15() { return &___m_iCurrentBackpackNum_15; }
	inline void set_m_iCurrentBackpackNum_15(int32_t value)
	{
		___m_iCurrentBackpackNum_15 = value;
	}

	inline static int32_t get_offset_of_m_iTotalTrimColorNum_16() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalTrimColorNum_16)); }
	inline int32_t get_m_iTotalTrimColorNum_16() const { return ___m_iTotalTrimColorNum_16; }
	inline int32_t* get_address_of_m_iTotalTrimColorNum_16() { return &___m_iTotalTrimColorNum_16; }
	inline void set_m_iTotalTrimColorNum_16(int32_t value)
	{
		___m_iTotalTrimColorNum_16 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentTrimColorNum_17() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentTrimColorNum_17)); }
	inline int32_t get_m_iCurrentTrimColorNum_17() const { return ___m_iCurrentTrimColorNum_17; }
	inline int32_t* get_address_of_m_iCurrentTrimColorNum_17() { return &___m_iCurrentTrimColorNum_17; }
	inline void set_m_iCurrentTrimColorNum_17(int32_t value)
	{
		___m_iCurrentTrimColorNum_17 = value;
	}

	inline static int32_t get_offset_of_m_iTotalClothColorNum_18() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalClothColorNum_18)); }
	inline int32_t get_m_iTotalClothColorNum_18() const { return ___m_iTotalClothColorNum_18; }
	inline int32_t* get_address_of_m_iTotalClothColorNum_18() { return &___m_iTotalClothColorNum_18; }
	inline void set_m_iTotalClothColorNum_18(int32_t value)
	{
		___m_iTotalClothColorNum_18 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentClothColorNum_19() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentClothColorNum_19)); }
	inline int32_t get_m_iCurrentClothColorNum_19() const { return ___m_iCurrentClothColorNum_19; }
	inline int32_t* get_address_of_m_iCurrentClothColorNum_19() { return &___m_iCurrentClothColorNum_19; }
	inline void set_m_iCurrentClothColorNum_19(int32_t value)
	{
		___m_iCurrentClothColorNum_19 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentColorColor_20() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentColorColor_20)); }
	inline Color_t2582018970  get_m_rCurrentColorColor_20() const { return ___m_rCurrentColorColor_20; }
	inline Color_t2582018970 * get_address_of_m_rCurrentColorColor_20() { return &___m_rCurrentColorColor_20; }
	inline void set_m_rCurrentColorColor_20(Color_t2582018970  value)
	{
		___m_rCurrentColorColor_20 = value;
	}

	inline static int32_t get_offset_of_m_iTotalTrimNum_21() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalTrimNum_21)); }
	inline int32_t get_m_iTotalTrimNum_21() const { return ___m_iTotalTrimNum_21; }
	inline int32_t* get_address_of_m_iTotalTrimNum_21() { return &___m_iTotalTrimNum_21; }
	inline void set_m_iTotalTrimNum_21(int32_t value)
	{
		___m_iTotalTrimNum_21 = value;
	}

	inline static int32_t get_offset_of_m_iTotalBackpackNum_22() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalBackpackNum_22)); }
	inline int32_t get_m_iTotalBackpackNum_22() const { return ___m_iTotalBackpackNum_22; }
	inline int32_t* get_address_of_m_iTotalBackpackNum_22() { return &___m_iTotalBackpackNum_22; }
	inline void set_m_iTotalBackpackNum_22(int32_t value)
	{
		___m_iTotalBackpackNum_22 = value;
	}

	inline static int32_t get_offset_of_m_iBodyType_23() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iBodyType_23)); }
	inline int32_t get_m_iBodyType_23() const { return ___m_iBodyType_23; }
	inline int32_t* get_address_of_m_iBodyType_23() { return &___m_iBodyType_23; }
	inline void set_m_iBodyType_23(int32_t value)
	{
		___m_iBodyType_23 = value;
	}

	inline static int32_t get_offset_of_m_tTrimTextures_24() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_tTrimTextures_24)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tTrimTextures_24() const { return ___m_tTrimTextures_24; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tTrimTextures_24() { return &___m_tTrimTextures_24; }
	inline void set_m_tTrimTextures_24(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tTrimTextures_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_tTrimTextures_24), value);
	}

	inline static int32_t get_offset_of_m_iTotalHairStyleNumber_25() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalHairStyleNumber_25)); }
	inline int32_t get_m_iTotalHairStyleNumber_25() const { return ___m_iTotalHairStyleNumber_25; }
	inline int32_t* get_address_of_m_iTotalHairStyleNumber_25() { return &___m_iTotalHairStyleNumber_25; }
	inline void set_m_iTotalHairStyleNumber_25(int32_t value)
	{
		___m_iTotalHairStyleNumber_25 = value;
	}

	inline static int32_t get_offset_of_m_hairMeshes_26() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_hairMeshes_26)); }
	inline List_1_t1370252420 * get_m_hairMeshes_26() const { return ___m_hairMeshes_26; }
	inline List_1_t1370252420 ** get_address_of_m_hairMeshes_26() { return &___m_hairMeshes_26; }
	inline void set_m_hairMeshes_26(List_1_t1370252420 * value)
	{
		___m_hairMeshes_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_hairMeshes_26), value);
	}

	inline static int32_t get_offset_of_instance_27() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___instance_27)); }
	inline CapturedDataInput_t2616152122 * get_instance_27() const { return ___instance_27; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_27() { return &___instance_27; }
	inline void set_instance_27(CapturedDataInput_t2616152122 * value)
	{
		___instance_27 = value;
		Il2CppCodeGenWriteBarrier((&___instance_27), value);
	}

	inline static int32_t get_offset_of_creationScreen_28() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___creationScreen_28)); }
	inline CharacterCreation_t2214710272 * get_creationScreen_28() const { return ___creationScreen_28; }
	inline CharacterCreation_t2214710272 ** get_address_of_creationScreen_28() { return &___creationScreen_28; }
	inline void set_creationScreen_28(CharacterCreation_t2214710272 * value)
	{
		___creationScreen_28 = value;
		Il2CppCodeGenWriteBarrier((&___creationScreen_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMISATION_T1896805424_H
#ifndef DOORHELP_T3298842472_H
#define DOORHELP_T3298842472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoorHelp
struct  DoorHelp_t3298842472  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean DoorHelp::bActivated
	bool ___bActivated_2;
	// UnityEngine.GUISkin DoorHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_3;
	// System.String DoorHelp::Text
	String_t* ___Text_4;
	// UnityEngine.GameObject DoorHelp::oDoor
	GameObject_t2557347079 * ___oDoor_5;
	// ObjectMovement DoorHelp::scriptObjectMovement
	ObjectMovement_t1497131487 * ___scriptObjectMovement_6;

public:
	inline static int32_t get_offset_of_bActivated_2() { return static_cast<int32_t>(offsetof(DoorHelp_t3298842472, ___bActivated_2)); }
	inline bool get_bActivated_2() const { return ___bActivated_2; }
	inline bool* get_address_of_bActivated_2() { return &___bActivated_2; }
	inline void set_bActivated_2(bool value)
	{
		___bActivated_2 = value;
	}

	inline static int32_t get_offset_of_GUIskin_3() { return static_cast<int32_t>(offsetof(DoorHelp_t3298842472, ___GUIskin_3)); }
	inline GUISkin_t2122630221 * get_GUIskin_3() const { return ___GUIskin_3; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_3() { return &___GUIskin_3; }
	inline void set_GUIskin_3(GUISkin_t2122630221 * value)
	{
		___GUIskin_3 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(DoorHelp_t3298842472, ___Text_4)); }
	inline String_t* get_Text_4() const { return ___Text_4; }
	inline String_t** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(String_t* value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of_oDoor_5() { return static_cast<int32_t>(offsetof(DoorHelp_t3298842472, ___oDoor_5)); }
	inline GameObject_t2557347079 * get_oDoor_5() const { return ___oDoor_5; }
	inline GameObject_t2557347079 ** get_address_of_oDoor_5() { return &___oDoor_5; }
	inline void set_oDoor_5(GameObject_t2557347079 * value)
	{
		___oDoor_5 = value;
		Il2CppCodeGenWriteBarrier((&___oDoor_5), value);
	}

	inline static int32_t get_offset_of_scriptObjectMovement_6() { return static_cast<int32_t>(offsetof(DoorHelp_t3298842472, ___scriptObjectMovement_6)); }
	inline ObjectMovement_t1497131487 * get_scriptObjectMovement_6() const { return ___scriptObjectMovement_6; }
	inline ObjectMovement_t1497131487 ** get_address_of_scriptObjectMovement_6() { return &___scriptObjectMovement_6; }
	inline void set_scriptObjectMovement_6(ObjectMovement_t1497131487 * value)
	{
		___scriptObjectMovement_6 = value;
		Il2CppCodeGenWriteBarrier((&___scriptObjectMovement_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOORHELP_T3298842472_H
#ifndef CUTSCENESTART_T3787429961_H
#define CUTSCENESTART_T3787429961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CutSceneStart
struct  CutSceneStart_t3787429961  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin CutSceneStart::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Int32 CutSceneStart::m_currentSceneNum
	int32_t ___m_currentSceneNum_3;
	// TalkScenes CutSceneStart::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_4;
	// UnityEngine.Texture2D CutSceneStart::m_tCutSceneStartTexture1
	Texture2D_t3063074017 * ___m_tCutSceneStartTexture1_5;
	// UnityEngine.Texture2D CutSceneStart::m_tCutSceneStartTexture2
	Texture2D_t3063074017 * ___m_tCutSceneStartTexture2_6;
	// UnityEngine.Texture2D CutSceneStart::m_tCutSceneStartTexture3
	Texture2D_t3063074017 * ___m_tCutSceneStartTexture3_7;
	// UnityEngine.Texture2D CutSceneStart::m_tCutSceneStartTexture4
	Texture2D_t3063074017 * ___m_tCutSceneStartTexture4_8;
	// UnityEngine.Texture2D CutSceneStart::m_displayedTexture
	Texture2D_t3063074017 * ___m_displayedTexture_9;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_currentSceneNum_3() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_currentSceneNum_3)); }
	inline int32_t get_m_currentSceneNum_3() const { return ___m_currentSceneNum_3; }
	inline int32_t* get_address_of_m_currentSceneNum_3() { return &___m_currentSceneNum_3; }
	inline void set_m_currentSceneNum_3(int32_t value)
	{
		___m_currentSceneNum_3 = value;
	}

	inline static int32_t get_offset_of_m_talkScene_4() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_talkScene_4)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_4() const { return ___m_talkScene_4; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_4() { return &___m_talkScene_4; }
	inline void set_m_talkScene_4(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_4), value);
	}

	inline static int32_t get_offset_of_m_tCutSceneStartTexture1_5() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_tCutSceneStartTexture1_5)); }
	inline Texture2D_t3063074017 * get_m_tCutSceneStartTexture1_5() const { return ___m_tCutSceneStartTexture1_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCutSceneStartTexture1_5() { return &___m_tCutSceneStartTexture1_5; }
	inline void set_m_tCutSceneStartTexture1_5(Texture2D_t3063074017 * value)
	{
		___m_tCutSceneStartTexture1_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCutSceneStartTexture1_5), value);
	}

	inline static int32_t get_offset_of_m_tCutSceneStartTexture2_6() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_tCutSceneStartTexture2_6)); }
	inline Texture2D_t3063074017 * get_m_tCutSceneStartTexture2_6() const { return ___m_tCutSceneStartTexture2_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCutSceneStartTexture2_6() { return &___m_tCutSceneStartTexture2_6; }
	inline void set_m_tCutSceneStartTexture2_6(Texture2D_t3063074017 * value)
	{
		___m_tCutSceneStartTexture2_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCutSceneStartTexture2_6), value);
	}

	inline static int32_t get_offset_of_m_tCutSceneStartTexture3_7() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_tCutSceneStartTexture3_7)); }
	inline Texture2D_t3063074017 * get_m_tCutSceneStartTexture3_7() const { return ___m_tCutSceneStartTexture3_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCutSceneStartTexture3_7() { return &___m_tCutSceneStartTexture3_7; }
	inline void set_m_tCutSceneStartTexture3_7(Texture2D_t3063074017 * value)
	{
		___m_tCutSceneStartTexture3_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCutSceneStartTexture3_7), value);
	}

	inline static int32_t get_offset_of_m_tCutSceneStartTexture4_8() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_tCutSceneStartTexture4_8)); }
	inline Texture2D_t3063074017 * get_m_tCutSceneStartTexture4_8() const { return ___m_tCutSceneStartTexture4_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCutSceneStartTexture4_8() { return &___m_tCutSceneStartTexture4_8; }
	inline void set_m_tCutSceneStartTexture4_8(Texture2D_t3063074017 * value)
	{
		___m_tCutSceneStartTexture4_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCutSceneStartTexture4_8), value);
	}

	inline static int32_t get_offset_of_m_displayedTexture_9() { return static_cast<int32_t>(offsetof(CutSceneStart_t3787429961, ___m_displayedTexture_9)); }
	inline Texture2D_t3063074017 * get_m_displayedTexture_9() const { return ___m_displayedTexture_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_displayedTexture_9() { return &___m_displayedTexture_9; }
	inline void set_m_displayedTexture_9(Texture2D_t3063074017 * value)
	{
		___m_displayedTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_displayedTexture_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUTSCENESTART_T3787429961_H
#ifndef CHARACTERSELECTION_T3241235818_H
#define CHARACTERSELECTION_T3241235818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterSelection
struct  CharacterSelection_t3241235818  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin CharacterSelection::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject CharacterSelection::m_boyObjectPrf
	GameObject_t2557347079 * ___m_boyObjectPrf_3;
	// UnityEngine.GameObject CharacterSelection::m_girlObjectPrf
	GameObject_t2557347079 * ___m_girlObjectPrf_4;
	// UnityEngine.GameObject CharacterSelection::characterCreation
	GameObject_t2557347079 * ___characterCreation_5;
	// UnityEngine.GameObject CharacterSelection::m_boyObjectClone
	GameObject_t2557347079 * ___m_boyObjectClone_6;
	// UnityEngine.GameObject CharacterSelection::m_girlObjectClone
	GameObject_t2557347079 * ___m_girlObjectClone_7;
	// UnityEngine.Texture2D CharacterSelection::m_backgroundImage
	Texture2D_t3063074017 * ___m_backgroundImage_8;
	// UnityEngine.GameObject CharacterSelection::m_characterCustomiseBG
	GameObject_t2557347079 * ___m_characterCustomiseBG_9;
	// UnityEngine.Vector3 CharacterSelection::m_vBoysPosition
	Vector3_t1986933152  ___m_vBoysPosition_10;
	// UnityEngine.Vector3 CharacterSelection::m_vGirlsPosition
	Vector3_t1986933152  ___m_vGirlsPosition_11;
	// UnityEngine.Rect CharacterSelection::m_rect
	Rect_t3039462994  ___m_rect_12;
	// UnityEngine.Transform CharacterSelection::theBoyTransform
	Transform_t362059596 * ___theBoyTransform_13;
	// UnityEngine.Vector3 CharacterSelection::m_v3WorldSpace
	Vector3_t1986933152  ___m_v3WorldSpace_14;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_boyObjectPrf_3() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_boyObjectPrf_3)); }
	inline GameObject_t2557347079 * get_m_boyObjectPrf_3() const { return ___m_boyObjectPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_boyObjectPrf_3() { return &___m_boyObjectPrf_3; }
	inline void set_m_boyObjectPrf_3(GameObject_t2557347079 * value)
	{
		___m_boyObjectPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_boyObjectPrf_3), value);
	}

	inline static int32_t get_offset_of_m_girlObjectPrf_4() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_girlObjectPrf_4)); }
	inline GameObject_t2557347079 * get_m_girlObjectPrf_4() const { return ___m_girlObjectPrf_4; }
	inline GameObject_t2557347079 ** get_address_of_m_girlObjectPrf_4() { return &___m_girlObjectPrf_4; }
	inline void set_m_girlObjectPrf_4(GameObject_t2557347079 * value)
	{
		___m_girlObjectPrf_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_girlObjectPrf_4), value);
	}

	inline static int32_t get_offset_of_characterCreation_5() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___characterCreation_5)); }
	inline GameObject_t2557347079 * get_characterCreation_5() const { return ___characterCreation_5; }
	inline GameObject_t2557347079 ** get_address_of_characterCreation_5() { return &___characterCreation_5; }
	inline void set_characterCreation_5(GameObject_t2557347079 * value)
	{
		___characterCreation_5 = value;
		Il2CppCodeGenWriteBarrier((&___characterCreation_5), value);
	}

	inline static int32_t get_offset_of_m_boyObjectClone_6() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_boyObjectClone_6)); }
	inline GameObject_t2557347079 * get_m_boyObjectClone_6() const { return ___m_boyObjectClone_6; }
	inline GameObject_t2557347079 ** get_address_of_m_boyObjectClone_6() { return &___m_boyObjectClone_6; }
	inline void set_m_boyObjectClone_6(GameObject_t2557347079 * value)
	{
		___m_boyObjectClone_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_boyObjectClone_6), value);
	}

	inline static int32_t get_offset_of_m_girlObjectClone_7() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_girlObjectClone_7)); }
	inline GameObject_t2557347079 * get_m_girlObjectClone_7() const { return ___m_girlObjectClone_7; }
	inline GameObject_t2557347079 ** get_address_of_m_girlObjectClone_7() { return &___m_girlObjectClone_7; }
	inline void set_m_girlObjectClone_7(GameObject_t2557347079 * value)
	{
		___m_girlObjectClone_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_girlObjectClone_7), value);
	}

	inline static int32_t get_offset_of_m_backgroundImage_8() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_backgroundImage_8)); }
	inline Texture2D_t3063074017 * get_m_backgroundImage_8() const { return ___m_backgroundImage_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_backgroundImage_8() { return &___m_backgroundImage_8; }
	inline void set_m_backgroundImage_8(Texture2D_t3063074017 * value)
	{
		___m_backgroundImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundImage_8), value);
	}

	inline static int32_t get_offset_of_m_characterCustomiseBG_9() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_characterCustomiseBG_9)); }
	inline GameObject_t2557347079 * get_m_characterCustomiseBG_9() const { return ___m_characterCustomiseBG_9; }
	inline GameObject_t2557347079 ** get_address_of_m_characterCustomiseBG_9() { return &___m_characterCustomiseBG_9; }
	inline void set_m_characterCustomiseBG_9(GameObject_t2557347079 * value)
	{
		___m_characterCustomiseBG_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterCustomiseBG_9), value);
	}

	inline static int32_t get_offset_of_m_vBoysPosition_10() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_vBoysPosition_10)); }
	inline Vector3_t1986933152  get_m_vBoysPosition_10() const { return ___m_vBoysPosition_10; }
	inline Vector3_t1986933152 * get_address_of_m_vBoysPosition_10() { return &___m_vBoysPosition_10; }
	inline void set_m_vBoysPosition_10(Vector3_t1986933152  value)
	{
		___m_vBoysPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_vGirlsPosition_11() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_vGirlsPosition_11)); }
	inline Vector3_t1986933152  get_m_vGirlsPosition_11() const { return ___m_vGirlsPosition_11; }
	inline Vector3_t1986933152 * get_address_of_m_vGirlsPosition_11() { return &___m_vGirlsPosition_11; }
	inline void set_m_vGirlsPosition_11(Vector3_t1986933152  value)
	{
		___m_vGirlsPosition_11 = value;
	}

	inline static int32_t get_offset_of_m_rect_12() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_rect_12)); }
	inline Rect_t3039462994  get_m_rect_12() const { return ___m_rect_12; }
	inline Rect_t3039462994 * get_address_of_m_rect_12() { return &___m_rect_12; }
	inline void set_m_rect_12(Rect_t3039462994  value)
	{
		___m_rect_12 = value;
	}

	inline static int32_t get_offset_of_theBoyTransform_13() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___theBoyTransform_13)); }
	inline Transform_t362059596 * get_theBoyTransform_13() const { return ___theBoyTransform_13; }
	inline Transform_t362059596 ** get_address_of_theBoyTransform_13() { return &___theBoyTransform_13; }
	inline void set_theBoyTransform_13(Transform_t362059596 * value)
	{
		___theBoyTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___theBoyTransform_13), value);
	}

	inline static int32_t get_offset_of_m_v3WorldSpace_14() { return static_cast<int32_t>(offsetof(CharacterSelection_t3241235818, ___m_v3WorldSpace_14)); }
	inline Vector3_t1986933152  get_m_v3WorldSpace_14() const { return ___m_v3WorldSpace_14; }
	inline Vector3_t1986933152 * get_address_of_m_v3WorldSpace_14() { return &___m_v3WorldSpace_14; }
	inline void set_m_v3WorldSpace_14(Vector3_t1986933152  value)
	{
		___m_v3WorldSpace_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTION_T3241235818_H
#ifndef BAG_T1045005451_H
#define BAG_T1045005451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bag
struct  Bag_t1045005451  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Bag::m_bShow
	bool ___m_bShow_2;
	// UnityEngine.GUISkin Bag::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.Texture2D Bag::m_tBag
	Texture2D_t3063074017 * ___m_tBag_4;
	// UnityEngine.Texture2D[] Bag::m_tItems
	Texture2DU5BU5D_t3304433276* ___m_tItems_5;
	// System.Single Bag::m_fMoveSpeed
	float ___m_fMoveSpeed_6;
	// System.Single Bag::m_fTimer
	float ___m_fTimer_9;
	// UnityEngine.Rect Bag::m_rBag
	Rect_t3039462994  ___m_rBag_10;
	// UnityEngine.Rect[] Bag::m_rItemRects
	RectU5BU5D_t141872167* ___m_rItemRects_11;
	// UnityEngine.Vector2[] Bag::m_vFinalPos
	Vector2U5BU5D_t1220531434* ___m_vFinalPos_12;
	// System.Boolean Bag::m_bToggleShowItems
	bool ___m_bToggleShowItems_13;
	// System.Boolean Bag::m_bMove
	bool ___m_bMove_14;
	// System.Boolean Bag::m_bNotebookUp
	bool ___m_bNotebookUp_15;

public:
	inline static int32_t get_offset_of_m_bShow_2() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_bShow_2)); }
	inline bool get_m_bShow_2() const { return ___m_bShow_2; }
	inline bool* get_address_of_m_bShow_2() { return &___m_bShow_2; }
	inline void set_m_bShow_2(bool value)
	{
		___m_bShow_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_tBag_4() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_tBag_4)); }
	inline Texture2D_t3063074017 * get_m_tBag_4() const { return ___m_tBag_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBag_4() { return &___m_tBag_4; }
	inline void set_m_tBag_4(Texture2D_t3063074017 * value)
	{
		___m_tBag_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBag_4), value);
	}

	inline static int32_t get_offset_of_m_tItems_5() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_tItems_5)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tItems_5() const { return ___m_tItems_5; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tItems_5() { return &___m_tItems_5; }
	inline void set_m_tItems_5(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tItems_5), value);
	}

	inline static int32_t get_offset_of_m_fMoveSpeed_6() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_fMoveSpeed_6)); }
	inline float get_m_fMoveSpeed_6() const { return ___m_fMoveSpeed_6; }
	inline float* get_address_of_m_fMoveSpeed_6() { return &___m_fMoveSpeed_6; }
	inline void set_m_fMoveSpeed_6(float value)
	{
		___m_fMoveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_fTimer_9() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_fTimer_9)); }
	inline float get_m_fTimer_9() const { return ___m_fTimer_9; }
	inline float* get_address_of_m_fTimer_9() { return &___m_fTimer_9; }
	inline void set_m_fTimer_9(float value)
	{
		___m_fTimer_9 = value;
	}

	inline static int32_t get_offset_of_m_rBag_10() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_rBag_10)); }
	inline Rect_t3039462994  get_m_rBag_10() const { return ___m_rBag_10; }
	inline Rect_t3039462994 * get_address_of_m_rBag_10() { return &___m_rBag_10; }
	inline void set_m_rBag_10(Rect_t3039462994  value)
	{
		___m_rBag_10 = value;
	}

	inline static int32_t get_offset_of_m_rItemRects_11() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_rItemRects_11)); }
	inline RectU5BU5D_t141872167* get_m_rItemRects_11() const { return ___m_rItemRects_11; }
	inline RectU5BU5D_t141872167** get_address_of_m_rItemRects_11() { return &___m_rItemRects_11; }
	inline void set_m_rItemRects_11(RectU5BU5D_t141872167* value)
	{
		___m_rItemRects_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_rItemRects_11), value);
	}

	inline static int32_t get_offset_of_m_vFinalPos_12() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_vFinalPos_12)); }
	inline Vector2U5BU5D_t1220531434* get_m_vFinalPos_12() const { return ___m_vFinalPos_12; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_vFinalPos_12() { return &___m_vFinalPos_12; }
	inline void set_m_vFinalPos_12(Vector2U5BU5D_t1220531434* value)
	{
		___m_vFinalPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_vFinalPos_12), value);
	}

	inline static int32_t get_offset_of_m_bToggleShowItems_13() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_bToggleShowItems_13)); }
	inline bool get_m_bToggleShowItems_13() const { return ___m_bToggleShowItems_13; }
	inline bool* get_address_of_m_bToggleShowItems_13() { return &___m_bToggleShowItems_13; }
	inline void set_m_bToggleShowItems_13(bool value)
	{
		___m_bToggleShowItems_13 = value;
	}

	inline static int32_t get_offset_of_m_bMove_14() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_bMove_14)); }
	inline bool get_m_bMove_14() const { return ___m_bMove_14; }
	inline bool* get_address_of_m_bMove_14() { return &___m_bMove_14; }
	inline void set_m_bMove_14(bool value)
	{
		___m_bMove_14 = value;
	}

	inline static int32_t get_offset_of_m_bNotebookUp_15() { return static_cast<int32_t>(offsetof(Bag_t1045005451, ___m_bNotebookUp_15)); }
	inline bool get_m_bNotebookUp_15() const { return ___m_bNotebookUp_15; }
	inline bool* get_address_of_m_bNotebookUp_15() { return &___m_bNotebookUp_15; }
	inline void set_m_bNotebookUp_15(bool value)
	{
		___m_bNotebookUp_15 = value;
	}
};

struct Bag_t1045005451_StaticFields
{
public:
	// System.Single Bag::m_fWidth
	float ___m_fWidth_7;
	// System.Single Bag::m_fHeight
	float ___m_fHeight_8;

public:
	inline static int32_t get_offset_of_m_fWidth_7() { return static_cast<int32_t>(offsetof(Bag_t1045005451_StaticFields, ___m_fWidth_7)); }
	inline float get_m_fWidth_7() const { return ___m_fWidth_7; }
	inline float* get_address_of_m_fWidth_7() { return &___m_fWidth_7; }
	inline void set_m_fWidth_7(float value)
	{
		___m_fWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_fHeight_8() { return static_cast<int32_t>(offsetof(Bag_t1045005451_StaticFields, ___m_fHeight_8)); }
	inline float get_m_fHeight_8() const { return ___m_fHeight_8; }
	inline float* get_address_of_m_fHeight_8() { return &___m_fHeight_8; }
	inline void set_m_fHeight_8(float value)
	{
		___m_fHeight_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BAG_T1045005451_H
#ifndef GNATSMOVEMENT_T1997391497_H
#define GNATSMOVEMENT_T1997391497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsMovement
struct  GnatsMovement_t1997391497  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean GnatsMovement::m_bIsTriggered
	bool ___m_bIsTriggered_2;
	// System.Boolean GnatsMovement::m_bCircling
	bool ___m_bCircling_3;
	// UnityEngine.Transform[] GnatsMovement::MovementTargets
	TransformU5BU5D_t2383644165* ___MovementTargets_4;
	// UnityEngine.Transform[] GnatsMovement::BattleFormation
	TransformU5BU5D_t2383644165* ___BattleFormation_5;
	// System.Int32[] GnatsMovement::CurrentTarget
	Int32U5BU5D_t1965588061* ___CurrentTarget_6;
	// UnityEngine.GameObject[] GnatsMovement::Gnats
	GameObjectU5BU5D_t2988620542* ___Gnats_7;

public:
	inline static int32_t get_offset_of_m_bIsTriggered_2() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___m_bIsTriggered_2)); }
	inline bool get_m_bIsTriggered_2() const { return ___m_bIsTriggered_2; }
	inline bool* get_address_of_m_bIsTriggered_2() { return &___m_bIsTriggered_2; }
	inline void set_m_bIsTriggered_2(bool value)
	{
		___m_bIsTriggered_2 = value;
	}

	inline static int32_t get_offset_of_m_bCircling_3() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___m_bCircling_3)); }
	inline bool get_m_bCircling_3() const { return ___m_bCircling_3; }
	inline bool* get_address_of_m_bCircling_3() { return &___m_bCircling_3; }
	inline void set_m_bCircling_3(bool value)
	{
		___m_bCircling_3 = value;
	}

	inline static int32_t get_offset_of_MovementTargets_4() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___MovementTargets_4)); }
	inline TransformU5BU5D_t2383644165* get_MovementTargets_4() const { return ___MovementTargets_4; }
	inline TransformU5BU5D_t2383644165** get_address_of_MovementTargets_4() { return &___MovementTargets_4; }
	inline void set_MovementTargets_4(TransformU5BU5D_t2383644165* value)
	{
		___MovementTargets_4 = value;
		Il2CppCodeGenWriteBarrier((&___MovementTargets_4), value);
	}

	inline static int32_t get_offset_of_BattleFormation_5() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___BattleFormation_5)); }
	inline TransformU5BU5D_t2383644165* get_BattleFormation_5() const { return ___BattleFormation_5; }
	inline TransformU5BU5D_t2383644165** get_address_of_BattleFormation_5() { return &___BattleFormation_5; }
	inline void set_BattleFormation_5(TransformU5BU5D_t2383644165* value)
	{
		___BattleFormation_5 = value;
		Il2CppCodeGenWriteBarrier((&___BattleFormation_5), value);
	}

	inline static int32_t get_offset_of_CurrentTarget_6() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___CurrentTarget_6)); }
	inline Int32U5BU5D_t1965588061* get_CurrentTarget_6() const { return ___CurrentTarget_6; }
	inline Int32U5BU5D_t1965588061** get_address_of_CurrentTarget_6() { return &___CurrentTarget_6; }
	inline void set_CurrentTarget_6(Int32U5BU5D_t1965588061* value)
	{
		___CurrentTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentTarget_6), value);
	}

	inline static int32_t get_offset_of_Gnats_7() { return static_cast<int32_t>(offsetof(GnatsMovement_t1997391497, ___Gnats_7)); }
	inline GameObjectU5BU5D_t2988620542* get_Gnats_7() const { return ___Gnats_7; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Gnats_7() { return &___Gnats_7; }
	inline void set_Gnats_7(GameObjectU5BU5D_t2988620542* value)
	{
		___Gnats_7 = value;
		Il2CppCodeGenWriteBarrier((&___Gnats_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSMOVEMENT_T1997391497_H
#ifndef DRAGDROPSHIELD_T3053351631_H
#define DRAGDROPSHIELD_T3053351631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragDropShield
struct  DragDropShield_t3053351631  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin DragDropShield::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject DragDropShield::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 DragDropShield::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject DragDropShield::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String DragDropShield::m_conversationBoxText
	String_t* ___m_conversationBoxText_6;
	// System.String[] DragDropShield::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_9;
	// System.String[] DragDropShield::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_10;
	// UnityEngine.Rect[] DragDropShield::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_11;
	// System.Boolean[] DragDropShield::displayName
	BooleanU5BU5D_t698278498* ___displayName_12;
	// UnityEngine.Color[] DragDropShield::displayColor
	ColorU5BU5D_t247935167* ___displayColor_13;
	// DraggableObject[] DragDropShield::m_wordsRects
	DraggableObjectU5BU5D_t2511386890* ___m_wordsRects_17;
	// UnityEngine.Rect[] DragDropShield::m_destinations
	RectU5BU5D_t141872167* ___m_destinations_19;
	// System.Int32[] DragDropShield::m_correctIndex
	Int32U5BU5D_t1965588061* ___m_correctIndex_20;
	// System.String DragDropShield::m_currentDragText
	String_t* ___m_currentDragText_21;
	// System.Int32 DragDropShield::m_currentDragIndex
	int32_t ___m_currentDragIndex_22;
	// System.Boolean DragDropShield::m_dragging
	bool ___m_dragging_23;
	// UnityEngine.Texture DragDropShield::colourTexture
	Texture_t2119925672 * ___colourTexture_24;
	// UnityEngine.Texture DragDropShield::colourFrameTexture
	Texture_t2119925672 * ___colourFrameTexture_25;
	// UnityEngine.Texture DragDropShield::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_26;
	// UnityEngine.Texture2D DragDropShield::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_27;
	// ShieldMenu DragDropShield::menu
	ShieldMenu_t3436809847 * ___menu_28;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_6() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_conversationBoxText_6)); }
	inline String_t* get_m_conversationBoxText_6() const { return ___m_conversationBoxText_6; }
	inline String_t** get_address_of_m_conversationBoxText_6() { return &___m_conversationBoxText_6; }
	inline void set_m_conversationBoxText_6(String_t* value)
	{
		___m_conversationBoxText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_6), value);
	}

	inline static int32_t get_offset_of_childObjectNames_9() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___childObjectNames_9)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_9() const { return ___childObjectNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_9() { return &___childObjectNames_9; }
	inline void set_childObjectNames_9(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNames_10() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___shieldNames_10)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_10() const { return ___shieldNames_10; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_10() { return &___shieldNames_10; }
	inline void set_shieldNames_10(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_10), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_11() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___shieldNameRects_11)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_11() const { return ___shieldNameRects_11; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_11() { return &___shieldNameRects_11; }
	inline void set_shieldNameRects_11(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_11 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_11), value);
	}

	inline static int32_t get_offset_of_displayName_12() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___displayName_12)); }
	inline BooleanU5BU5D_t698278498* get_displayName_12() const { return ___displayName_12; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_12() { return &___displayName_12; }
	inline void set_displayName_12(BooleanU5BU5D_t698278498* value)
	{
		___displayName_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_12), value);
	}

	inline static int32_t get_offset_of_displayColor_13() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___displayColor_13)); }
	inline ColorU5BU5D_t247935167* get_displayColor_13() const { return ___displayColor_13; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_13() { return &___displayColor_13; }
	inline void set_displayColor_13(ColorU5BU5D_t247935167* value)
	{
		___displayColor_13 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_13), value);
	}

	inline static int32_t get_offset_of_m_wordsRects_17() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_wordsRects_17)); }
	inline DraggableObjectU5BU5D_t2511386890* get_m_wordsRects_17() const { return ___m_wordsRects_17; }
	inline DraggableObjectU5BU5D_t2511386890** get_address_of_m_wordsRects_17() { return &___m_wordsRects_17; }
	inline void set_m_wordsRects_17(DraggableObjectU5BU5D_t2511386890* value)
	{
		___m_wordsRects_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_wordsRects_17), value);
	}

	inline static int32_t get_offset_of_m_destinations_19() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_destinations_19)); }
	inline RectU5BU5D_t141872167* get_m_destinations_19() const { return ___m_destinations_19; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinations_19() { return &___m_destinations_19; }
	inline void set_m_destinations_19(RectU5BU5D_t141872167* value)
	{
		___m_destinations_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinations_19), value);
	}

	inline static int32_t get_offset_of_m_correctIndex_20() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_correctIndex_20)); }
	inline Int32U5BU5D_t1965588061* get_m_correctIndex_20() const { return ___m_correctIndex_20; }
	inline Int32U5BU5D_t1965588061** get_address_of_m_correctIndex_20() { return &___m_correctIndex_20; }
	inline void set_m_correctIndex_20(Int32U5BU5D_t1965588061* value)
	{
		___m_correctIndex_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_correctIndex_20), value);
	}

	inline static int32_t get_offset_of_m_currentDragText_21() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_currentDragText_21)); }
	inline String_t* get_m_currentDragText_21() const { return ___m_currentDragText_21; }
	inline String_t** get_address_of_m_currentDragText_21() { return &___m_currentDragText_21; }
	inline void set_m_currentDragText_21(String_t* value)
	{
		___m_currentDragText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentDragText_21), value);
	}

	inline static int32_t get_offset_of_m_currentDragIndex_22() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_currentDragIndex_22)); }
	inline int32_t get_m_currentDragIndex_22() const { return ___m_currentDragIndex_22; }
	inline int32_t* get_address_of_m_currentDragIndex_22() { return &___m_currentDragIndex_22; }
	inline void set_m_currentDragIndex_22(int32_t value)
	{
		___m_currentDragIndex_22 = value;
	}

	inline static int32_t get_offset_of_m_dragging_23() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_dragging_23)); }
	inline bool get_m_dragging_23() const { return ___m_dragging_23; }
	inline bool* get_address_of_m_dragging_23() { return &___m_dragging_23; }
	inline void set_m_dragging_23(bool value)
	{
		___m_dragging_23 = value;
	}

	inline static int32_t get_offset_of_colourTexture_24() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___colourTexture_24)); }
	inline Texture_t2119925672 * get_colourTexture_24() const { return ___colourTexture_24; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_24() { return &___colourTexture_24; }
	inline void set_colourTexture_24(Texture_t2119925672 * value)
	{
		___colourTexture_24 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_24), value);
	}

	inline static int32_t get_offset_of_colourFrameTexture_25() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___colourFrameTexture_25)); }
	inline Texture_t2119925672 * get_colourFrameTexture_25() const { return ___colourFrameTexture_25; }
	inline Texture_t2119925672 ** get_address_of_colourFrameTexture_25() { return &___colourFrameTexture_25; }
	inline void set_colourFrameTexture_25(Texture_t2119925672 * value)
	{
		___colourFrameTexture_25 = value;
		Il2CppCodeGenWriteBarrier((&___colourFrameTexture_25), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_26() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___greyScaleTexture_26)); }
	inline Texture_t2119925672 * get_greyScaleTexture_26() const { return ___greyScaleTexture_26; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_26() { return &___greyScaleTexture_26; }
	inline void set_greyScaleTexture_26(Texture_t2119925672 * value)
	{
		___greyScaleTexture_26 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_26), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_27() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___m_tConversationBackground_27)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_27() const { return ___m_tConversationBackground_27; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_27() { return &___m_tConversationBackground_27; }
	inline void set_m_tConversationBackground_27(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_27), value);
	}

	inline static int32_t get_offset_of_menu_28() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631, ___menu_28)); }
	inline ShieldMenu_t3436809847 * get_menu_28() const { return ___menu_28; }
	inline ShieldMenu_t3436809847 ** get_address_of_menu_28() { return &___menu_28; }
	inline void set_menu_28(ShieldMenu_t3436809847 * value)
	{
		___menu_28 = value;
		Il2CppCodeGenWriteBarrier((&___menu_28), value);
	}
};

struct DragDropShield_t3053351631_StaticFields
{
public:
	// System.Single DragDropShield::TextWidth
	float ___TextWidth_7;
	// System.Single DragDropShield::TextHeight
	float ___TextHeight_8;
	// System.Single DragDropShield::m_collisiondimension
	float ___m_collisiondimension_14;
	// System.Single DragDropShield::m_movingTextwidth
	float ___m_movingTextwidth_15;
	// System.Single DragDropShield::m_movingTextHeight
	float ___m_movingTextHeight_16;
	// System.Single DragDropShield::m_targetDim
	float ___m_targetDim_18;

public:
	inline static int32_t get_offset_of_TextWidth_7() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___TextWidth_7)); }
	inline float get_TextWidth_7() const { return ___TextWidth_7; }
	inline float* get_address_of_TextWidth_7() { return &___TextWidth_7; }
	inline void set_TextWidth_7(float value)
	{
		___TextWidth_7 = value;
	}

	inline static int32_t get_offset_of_TextHeight_8() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___TextHeight_8)); }
	inline float get_TextHeight_8() const { return ___TextHeight_8; }
	inline float* get_address_of_TextHeight_8() { return &___TextHeight_8; }
	inline void set_TextHeight_8(float value)
	{
		___TextHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_collisiondimension_14() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___m_collisiondimension_14)); }
	inline float get_m_collisiondimension_14() const { return ___m_collisiondimension_14; }
	inline float* get_address_of_m_collisiondimension_14() { return &___m_collisiondimension_14; }
	inline void set_m_collisiondimension_14(float value)
	{
		___m_collisiondimension_14 = value;
	}

	inline static int32_t get_offset_of_m_movingTextwidth_15() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___m_movingTextwidth_15)); }
	inline float get_m_movingTextwidth_15() const { return ___m_movingTextwidth_15; }
	inline float* get_address_of_m_movingTextwidth_15() { return &___m_movingTextwidth_15; }
	inline void set_m_movingTextwidth_15(float value)
	{
		___m_movingTextwidth_15 = value;
	}

	inline static int32_t get_offset_of_m_movingTextHeight_16() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___m_movingTextHeight_16)); }
	inline float get_m_movingTextHeight_16() const { return ___m_movingTextHeight_16; }
	inline float* get_address_of_m_movingTextHeight_16() { return &___m_movingTextHeight_16; }
	inline void set_m_movingTextHeight_16(float value)
	{
		___m_movingTextHeight_16 = value;
	}

	inline static int32_t get_offset_of_m_targetDim_18() { return static_cast<int32_t>(offsetof(DragDropShield_t3053351631_StaticFields, ___m_targetDim_18)); }
	inline float get_m_targetDim_18() const { return ___m_targetDim_18; }
	inline float* get_address_of_m_targetDim_18() { return &___m_targetDim_18; }
	inline void set_m_targetDim_18(float value)
	{
		___m_targetDim_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGDROPSHIELD_T3053351631_H
#ifndef DOORHELPTRIGER_T3891523883_H
#define DOORHELPTRIGER_T3891523883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoorHelpTriger
struct  DoorHelpTriger_t3891523883  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean DoorHelpTriger::m_EagleTriggerEntered
	bool ___m_EagleTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_EagleTriggerEntered_2() { return static_cast<int32_t>(offsetof(DoorHelpTriger_t3891523883, ___m_EagleTriggerEntered_2)); }
	inline bool get_m_EagleTriggerEntered_2() const { return ___m_EagleTriggerEntered_2; }
	inline bool* get_address_of_m_EagleTriggerEntered_2() { return &___m_EagleTriggerEntered_2; }
	inline void set_m_EagleTriggerEntered_2(bool value)
	{
		___m_EagleTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOORHELPTRIGER_T3891523883_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (PostProcessingModel_t519727923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[1] = 
{
	PostProcessingModel_t519727923::get_offset_of_m_Enabled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (PostProcessingProfile_t1367512122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[15] = 
{
	PostProcessingProfile_t1367512122::get_offset_of_debugViews_2(),
	PostProcessingProfile_t1367512122::get_offset_of_fog_3(),
	PostProcessingProfile_t1367512122::get_offset_of_antialiasing_4(),
	PostProcessingProfile_t1367512122::get_offset_of_ambientOcclusion_5(),
	PostProcessingProfile_t1367512122::get_offset_of_screenSpaceReflection_6(),
	PostProcessingProfile_t1367512122::get_offset_of_depthOfField_7(),
	PostProcessingProfile_t1367512122::get_offset_of_motionBlur_8(),
	PostProcessingProfile_t1367512122::get_offset_of_eyeAdaptation_9(),
	PostProcessingProfile_t1367512122::get_offset_of_bloom_10(),
	PostProcessingProfile_t1367512122::get_offset_of_colorGrading_11(),
	PostProcessingProfile_t1367512122::get_offset_of_userLut_12(),
	PostProcessingProfile_t1367512122::get_offset_of_chromaticAberration_13(),
	PostProcessingProfile_t1367512122::get_offset_of_grain_14(),
	PostProcessingProfile_t1367512122::get_offset_of_vignette_15(),
	PostProcessingProfile_t1367512122::get_offset_of_dithering_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (ColorGradingCurve_t4179314942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	ColorGradingCurve_t4179314942::get_offset_of_curve_0(),
	ColorGradingCurve_t4179314942::get_offset_of_m_Loop_1(),
	ColorGradingCurve_t4179314942::get_offset_of_m_ZeroValue_2(),
	ColorGradingCurve_t4179314942::get_offset_of_m_Range_3(),
	ColorGradingCurve_t4179314942::get_offset_of_m_InternalLoopingCurve_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (GraphicsUtils_t2847288477), -1, sizeof(GraphicsUtils_t2847288477_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2803[2] = 
{
	GraphicsUtils_t2847288477_StaticFields::get_offset_of_s_WhiteTexture_0(),
	GraphicsUtils_t2847288477_StaticFields::get_offset_of_s_Quad_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (MaterialFactory_t1986441003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[1] = 
{
	MaterialFactory_t1986441003::get_offset_of_m_Materials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (RenderTextureFactory_t1364694010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[1] = 
{
	RenderTextureFactory_t1364694010::get_offset_of_m_TemporaryRTs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (ExampleWheelController_t1631372755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[3] = 
{
	ExampleWheelController_t1631372755::get_offset_of_acceleration_2(),
	ExampleWheelController_t1631372755::get_offset_of_motionVectorRenderer_3(),
	ExampleWheelController_t1631372755::get_offset_of_m_Rigidbody_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (Uniforms_t4027551681), -1, sizeof(Uniforms_t4027551681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[1] = 
{
	Uniforms_t4027551681_StaticFields::get_offset_of__MotionAmount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (ProvinceScript_t1528091046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[9] = 
{
	ProvinceScript_t1528091046::get_offset_of_pinPrefab_2(),
	ProvinceScript_t1528091046::get_offset_of_pinOffset_3(),
	ProvinceScript_t1528091046::get_offset_of_level_4(),
	ProvinceScript_t1528091046::get_offset_of_canTravelHere_5(),
	ProvinceScript_t1528091046::get_offset_of_levelID_6(),
	ProvinceScript_t1528091046::get_offset_of_pin_7(),
	ProvinceScript_t1528091046::get_offset_of_pinStartPos_8(),
	ProvinceScript_t1528091046::get_offset_of_pinClicked_9(),
	ProvinceScript_t1528091046::get_offset_of_dataInput_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (RelaxMuscles_t2970223765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[10] = 
{
	RelaxMuscles_t2970223765::get_offset_of_images_2(),
	RelaxMuscles_t2970223765::get_offset_of_text_3(),
	RelaxMuscles_t2970223765::get_offset_of_fadeRate_4(),
	RelaxMuscles_t2970223765::get_offset_of_nextButton_5(),
	RelaxMuscles_t2970223765::get_offset_of_textObject_6(),
	RelaxMuscles_t2970223765::get_offset_of_waitTime_7(),
	RelaxMuscles_t2970223765::get_offset_of_currentWaitTime_8(),
	RelaxMuscles_t2970223765::get_offset_of_currentImage_9(),
	RelaxMuscles_t2970223765::get_offset_of_currentAlpha_10(),
	RelaxMuscles_t2970223765::get_offset_of_currentState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (ERelaxState_t75127890)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2810[5] = 
{
	ERelaxState_t75127890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (BoatCutSceneManager_t78220184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[4] = 
{
	BoatCutSceneManager_t78220184::get_offset_of_boatsPlaceHolder_2(),
	BoatCutSceneManager_t78220184::get_offset_of_boatTimeline_3(),
	BoatCutSceneManager_t78220184::get_offset_of_fireball_4(),
	BoatCutSceneManager_t78220184::get_offset_of_cinematicTrigger_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (boatPlayerSpawner_t2586798380)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[3] = 
{
	boatPlayerSpawner_t2586798380::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (boatPlaceholder_t3308939318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2813[3] = 
{
	boatPlaceholder_t3308939318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (BoatTrigger_t357608549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[4] = 
{
	BoatTrigger_t357608549::get_offset_of__boatCutSceneManagerRef_2(),
	BoatTrigger_t357608549::get_offset_of_CutSceneInitialTime_3(),
	BoatTrigger_t357608549::get_offset_of_CutSceneStopTime_4(),
	BoatTrigger_t357608549::get_offset_of_teleportPos_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (U3CtimeToDelayU3Ec__Iterator0_t3570146540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[5] = 
{
	U3CtimeToDelayU3Ec__Iterator0_t3570146540::get_offset_of_teleportPos_0(),
	U3CtimeToDelayU3Ec__Iterator0_t3570146540::get_offset_of_U24this_1(),
	U3CtimeToDelayU3Ec__Iterator0_t3570146540::get_offset_of_U24current_2(),
	U3CtimeToDelayU3Ec__Iterator0_t3570146540::get_offset_of_U24disposing_3(),
	U3CtimeToDelayU3Ec__Iterator0_t3570146540::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (ButtonCallback_t1280411946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[2] = 
{
	ButtonCallback_t1280411946::get_offset_of_callback_2(),
	ButtonCallback_t1280411946::get_offset_of_param_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (ButtonCallbackDelegate_t333906069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (CMEagleDeactivateCam_t2782597658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[1] = 
{
	CMEagleDeactivateCam_t2782597658::get_offset_of_timelineGO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (CMFollowGemPlacement_t2444057678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[4] = 
{
	CMFollowGemPlacement_t2444057678::get_offset_of_timeline_2(),
	CMFollowGemPlacement_t2444057678::get_offset_of_camera2_3(),
	CMFollowGemPlacement_t2444057678::get_offset_of__pMovemenrRef_4(),
	CMFollowGemPlacement_t2444057678::get_offset_of_bMovementSet_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (L7CameraRotation_t3332317093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[6] = 
{
	L7CameraRotation_t3332317093::get_offset_of_fAngleRotated_2(),
	L7CameraRotation_t3332317093::get_offset_of_m_bCameraIsRotating_3(),
	L7CameraRotation_t3332317093::get_offset_of_m_bCameraCanRotate_4(),
	L7CameraRotation_t3332317093::get_offset_of_m_bMovingToPoint_5(),
	L7CameraRotation_t3332317093::get_offset_of_m_vStartVector_6(),
	L7CameraRotation_t3332317093::get_offset_of_m_vCurrentVector_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (MouseOrbitImproved_t2709099835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[10] = 
{
	MouseOrbitImproved_t2709099835::get_offset_of_target_2(),
	MouseOrbitImproved_t2709099835::get_offset_of_distance_3(),
	MouseOrbitImproved_t2709099835::get_offset_of_xSpeed_4(),
	MouseOrbitImproved_t2709099835::get_offset_of_ySpeed_5(),
	MouseOrbitImproved_t2709099835::get_offset_of_yMinLimit_6(),
	MouseOrbitImproved_t2709099835::get_offset_of_yMaxLimit_7(),
	MouseOrbitImproved_t2709099835::get_offset_of_distanceMin_8(),
	MouseOrbitImproved_t2709099835::get_offset_of_distanceMax_9(),
	MouseOrbitImproved_t2709099835::get_offset_of_x_10(),
	MouseOrbitImproved_t2709099835::get_offset_of_y_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (ColorExtensions_t1379459159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (CharacterTalkTrigger_t3203602799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (Customisation_t1896805424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[27] = 
{
	Customisation_t1896805424::get_offset_of_character_2(),
	Customisation_t1896805424::get_offset_of_m_rCurrentHairColor_3(),
	Customisation_t1896805424::get_offset_of_m_rCurrentTrimColor_4(),
	Customisation_t1896805424::get_offset_of_m_tCurrentTrimTexture_5(),
	Customisation_t1896805424::get_offset_of_m_iTotalHairColorNum_6(),
	Customisation_t1896805424::get_offset_of_m_iCurrentHairColorNum_7(),
	Customisation_t1896805424::get_offset_of_m_iTotalEyeColorNum_8(),
	Customisation_t1896805424::get_offset_of_m_iCurrentEyeColorNum_9(),
	Customisation_t1896805424::get_offset_of_m_rCurrentEyeColor_10(),
	Customisation_t1896805424::get_offset_of_m_iTotalSkinColorNum_11(),
	Customisation_t1896805424::get_offset_of_m_iCurrentSkinColorNum_12(),
	Customisation_t1896805424::get_offset_of_m_iCurrentHairStyleNum_13(),
	Customisation_t1896805424::get_offset_of_m_iCurrentTrimNum_14(),
	Customisation_t1896805424::get_offset_of_m_iCurrentBackpackNum_15(),
	Customisation_t1896805424::get_offset_of_m_iTotalTrimColorNum_16(),
	Customisation_t1896805424::get_offset_of_m_iCurrentTrimColorNum_17(),
	Customisation_t1896805424::get_offset_of_m_iTotalClothColorNum_18(),
	Customisation_t1896805424::get_offset_of_m_iCurrentClothColorNum_19(),
	Customisation_t1896805424::get_offset_of_m_rCurrentColorColor_20(),
	Customisation_t1896805424::get_offset_of_m_iTotalTrimNum_21(),
	Customisation_t1896805424::get_offset_of_m_iTotalBackpackNum_22(),
	Customisation_t1896805424::get_offset_of_m_iBodyType_23(),
	Customisation_t1896805424::get_offset_of_m_tTrimTextures_24(),
	Customisation_t1896805424::get_offset_of_m_iTotalHairStyleNumber_25(),
	Customisation_t1896805424::get_offset_of_m_hairMeshes_26(),
	Customisation_t1896805424::get_offset_of_instance_27(),
	Customisation_t1896805424::get_offset_of_creationScreen_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (CharacterCustomise_t2467338958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (L7FinalCutScene_t3460878615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[9] = 
{
	L7FinalCutScene_t3460878615::get_offset_of_m_skin_2(),
	L7FinalCutScene_t3460878615::get_offset_of_m_fFadeInAlpha_3(),
	L7FinalCutScene_t3460878615::get_offset_of_m_fFadeOutAlpha_4(),
	L7FinalCutScene_t3460878615::get_offset_of_m_fFadeInStartTime_5(),
	L7FinalCutScene_t3460878615::get_offset_of_m_fFadeOutStartTime_6(),
	L7FinalCutScene_t3460878615::get_offset_of_m_bCutsceneFadeInStart_7(),
	L7FinalCutScene_t3460878615::get_offset_of_m_bCutsceneFadeOutStart_8(),
	L7FinalCutScene_t3460878615::get_offset_of_m_shieldStartMoving_9(),
	L7FinalCutScene_t3460878615::get_offset_of_m_bShieldRaising_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (FindTriangles_t78342577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[1] = 
{
	FindTriangles_t78342577::get_offset_of_glist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (DragObject_t2975917658), -1, sizeof(DragObject_t2975917658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2828[5] = 
{
	DragObject_t2975917658::get_offset_of_callback_2(),
	DragObject_t2975917658::get_offset_of_dragging_3(),
	DragObject_t2975917658::get_offset_of_dragable_4(),
	DragObject_t2975917658::get_offset_of_snapped_5(),
	DragObject_t2975917658_StaticFields::get_offset_of_dragOne_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (DragCallback_t2341893212), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (CameraShake_t1998576306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[4] = 
{
	CameraShake_t1998576306::get_offset_of_originPosition_2(),
	CameraShake_t1998576306::get_offset_of_originRotation_3(),
	CameraShake_t1998576306::get_offset_of_shake_decay_4(),
	CameraShake_t1998576306::get_offset_of_shake_intensity_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (IceDropLv2_t3491147653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[11] = 
{
	IceDropLv2_t3491147653::get_offset_of_IceParts_2(),
	IceDropLv2_t3491147653::get_offset_of_m_bIsDropping_3(),
	IceDropLv2_t3491147653::get_offset_of_FallSpeedMin_4(),
	IceDropLv2_t3491147653::get_offset_of_FallSpeedMax_5(),
	IceDropLv2_t3491147653::get_offset_of_FallSpeeds_6(),
	IceDropLv2_t3491147653::get_offset_of_IntervalMin_7(),
	IceDropLv2_t3491147653::get_offset_of_IntervalMax_8(),
	IceDropLv2_t3491147653::get_offset_of_snowPuff_9(),
	IceDropLv2_t3491147653::get_offset_of_Intervals_10(),
	IceDropLv2_t3491147653::get_offset_of_Dropping_11(),
	IceDropLv2_t3491147653::get_offset_of_OriginalPos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (LeafDrop_t1157882929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[3] = 
{
	LeafDrop_t1157882929::get_offset_of_StartDroping_2(),
	LeafDrop_t1157882929::get_offset_of_RandomRotation_3(),
	LeafDrop_t1157882929::get_offset_of_Force_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (LeafFlying_t2321553312), -1, sizeof(LeafFlying_t2321553312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[4] = 
{
	LeafFlying_t2321553312::get_offset_of_MaximumLeafDisplay_2(),
	LeafFlying_t2321553312::get_offset_of_LeafDropRate_3(),
	LeafFlying_t2321553312::get_offset_of_LeafDropElapsed_4(),
	LeafFlying_t2321553312_StaticFields::get_offset_of_NumOfLeafOnScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (LightFade_t1716889777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[1] = 
{
	LightFade_t1716889777::get_offset_of_m_fTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (Lv7BridgeBreak_t760961460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[1] = 
{
	Lv7BridgeBreak_t760961460::get_offset_of_Distance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (QuickSandEffect_t4016288157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[4] = 
{
	QuickSandEffect_t4016288157::get_offset_of_AccumulatedTime_2(),
	QuickSandEffect_t4016288157::get_offset_of_Speed_3(),
	QuickSandEffect_t4016288157::get_offset_of_m_bInverseRotation_4(),
	QuickSandEffect_t4016288157::get_offset_of_fRotateDegrees_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (SetAnimationStartFrame_t1496270653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[1] = 
{
	SetAnimationStartFrame_t1496270653::get_offset_of_fTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (SetAnimationStartFrame1_t1668100355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[1] = 
{
	SetAnimationStartFrame1_t1668100355::get_offset_of_fTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (TriggerParticles_t2790258363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[1] = 
{
	TriggerParticles_t2790258363::get_offset_of_rocks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (WaterEffect_t779416252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[4] = 
{
	WaterEffect_t779416252::get_offset_of_Offset_2(),
	WaterEffect_t779416252::get_offset_of_tiling_3(),
	WaterEffect_t779416252::get_offset_of_AccumulatedTime_4(),
	WaterEffect_t779416252::get_offset_of_Speed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (FireSpritFlyAway_t1850743346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[4] = 
{
	FireSpritFlyAway_t1850743346::get_offset_of_m_vTargetPosition_2(),
	FireSpritFlyAway_t1850743346::get_offset_of_m_bMovingToTarget_3(),
	FireSpritFlyAway_t1850743346::get_offset_of_m_start_4(),
	FireSpritFlyAway_t1850743346::get_offset_of_m_fTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (GameTimer_t736613873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (GetGem_t2101980823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[3] = 
{
	GetGem_t2101980823::get_offset_of_m_bPlayerStartTakingGem_2(),
	GetGem_t2101980823::get_offset_of_MouseOverTexture_3(),
	GetGem_t2101980823::get_offset_of_MouseOriTexture_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (ReleaseGem_t3020869431), -1, sizeof(ReleaseGem_t3020869431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2844[12] = 
{
	ReleaseGem_t3020869431::get_offset_of_m_bReleaseGemRadius_2(),
	ReleaseGem_t3020869431::get_offset_of_m_bReadyToPutGem_3(),
	ReleaseGem_t3020869431::get_offset_of_m_bGemCanRelease_4(),
	ReleaseGem_t3020869431::get_offset_of_m_bGemStartsRaising_5(),
	ReleaseGem_t3020869431_StaticFields::get_offset_of_bagOff_6(),
	ReleaseGem_t3020869431::get_offset_of_m_bGemRaiseCompleted_7(),
	ReleaseGem_t3020869431::get_offset_of_outlineTrigger_8(),
	ReleaseGem_t3020869431::get_offset_of_releaseGemEffect_9(),
	ReleaseGem_t3020869431::get_offset_of_shader1_10(),
	ReleaseGem_t3020869431::get_offset_of_shader2_11(),
	ReleaseGem_t3020869431::get_offset_of_rend_12(),
	ReleaseGem_t3020869431::get_offset_of_waitOver_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (ShowGem_t26615769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[2] = 
{
	ShowGem_t26615769::get_offset_of_gem_2(),
	ShowGem_t26615769::get_offset_of_fTimer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (Global_t3489172580), -1, sizeof(Global_t3489172580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2846[84] = 
{
	Global_t3489172580::get_offset_of_m_skin_2(),
	Global_t3489172580_StaticFields::get_offset_of_instanceRef_3(),
	Global_t3489172580_StaticFields::get_offset_of_privateInteractNPC_4(),
	Global_t3489172580_StaticFields::get_offset_of_m_sCurrentFakeNPC_5(),
	Global_t3489172580_StaticFields::get_offset_of_CurrentPlayerName_6(),
	Global_t3489172580_StaticFields::get_offset_of_GemObtainedForTheLevel_7(),
	Global_t3489172580_StaticFields::get_offset_of_m_L6CurrentDoorNum_8(),
	Global_t3489172580_StaticFields::get_offset_of_TempTalkCharacter_9(),
	Global_t3489172580_StaticFields::get_offset_of_m_bGotCurrentLevelGem_10(),
	Global_t3489172580_StaticFields::get_offset_of_LevelSVisited_11(),
	Global_t3489172580_StaticFields::get_offset_of_currentLevelNumber_12(),
	Global_t3489172580_StaticFields::get_offset_of_PreviousSceneName_13(),
	Global_t3489172580_StaticFields::get_offset_of_m_bGemStartsRaising_14(),
	Global_t3489172580_StaticFields::get_offset_of_m_bGemIsFullyReleased_15(),
	Global_t3489172580_StaticFields::get_offset_of_GUIObjects_16(),
	Global_t3489172580_StaticFields::get_offset_of_userCurrentLevelNum_17(),
	Global_t3489172580_StaticFields::get_offset_of_userFeedBackScore_18(),
	Global_t3489172580_StaticFields::get_offset_of_userHairStyleNum_19(),
	Global_t3489172580_StaticFields::get_offset_of_userHairColor_20(),
	Global_t3489172580_StaticFields::get_offset_of_userSkinColor_21(),
	Global_t3489172580_StaticFields::get_offset_of_userEyeColor_22(),
	Global_t3489172580_StaticFields::get_offset_of_userClothColor_23(),
	Global_t3489172580_StaticFields::get_offset_of_userTrimTextureNum_24(),
	Global_t3489172580_StaticFields::get_offset_of_userTrimColor_25(),
	Global_t3489172580_StaticFields::get_offset_of_userBackPackNum_26(),
	Global_t3489172580_StaticFields::get_offset_of_userBodyType_27(),
	Global_t3489172580_StaticFields::get_offset_of_userL1NoteBookDataBlocks_28(),
	Global_t3489172580_StaticFields::get_offset_of_userL1NoteBookManualBlocks_29(),
	Global_t3489172580_StaticFields::get_offset_of_userL2NoteBookDataBlocks_30(),
	Global_t3489172580_StaticFields::get_offset_of_userL2NoteBookManualBlocks_31(),
	Global_t3489172580_StaticFields::get_offset_of_userL3NoteBookDataBlockOne_32(),
	Global_t3489172580_StaticFields::get_offset_of_userL3NoteBookDataBlockTwo_33(),
	Global_t3489172580_StaticFields::get_offset_of_userL3NoteBookDataBlockThree_34(),
	Global_t3489172580_StaticFields::get_offset_of_userL4NoteBookDataBlockOne_35(),
	Global_t3489172580_StaticFields::get_offset_of_userL4NoteBookDataBlockTwo_36(),
	Global_t3489172580_StaticFields::get_offset_of_userL4NoteBookStepsBlocks_37(),
	Global_t3489172580_StaticFields::get_offset_of_userL5NoteBookDataBlocks_38(),
	Global_t3489172580_StaticFields::get_offset_of_userL5NoteBookManualBlocks_39(),
	Global_t3489172580_StaticFields::get_offset_of_userL6NoteBookDataBlocks_40(),
	Global_t3489172580_StaticFields::get_offset_of_userL6NoteBookManualBlocks_41(),
	Global_t3489172580_StaticFields::get_offset_of_userL6NoteBookRapaBlocks_42(),
	Global_t3489172580_StaticFields::get_offset_of_userL7NoteBookDataBlocks_43(),
	Global_t3489172580_StaticFields::get_offset_of_lastLevelNoteBookData_44(),
	Global_t3489172580_StaticFields::get_offset_of_m_giveAwayNPCName_45(),
	Global_t3489172580_StaticFields::get_offset_of_m_giveAwayAniName_46(),
	Global_t3489172580_StaticFields::get_offset_of_m_giveAwayObjName_47(),
	Global_t3489172580_StaticFields::get_offset_of_m_giveAwayObjAppearPos_48(),
	Global_t3489172580_StaticFields::get_offset_of_m_gObjectStartGivingAway_49(),
	Global_t3489172580_StaticFields::get_offset_of_m_pickNPCName_50(),
	Global_t3489172580_StaticFields::get_offset_of_m_PickAniName_51(),
	Global_t3489172580_StaticFields::get_offset_of_m_gObjectTakingStart_52(),
	Global_t3489172580_StaticFields::get_offset_of_m_ObjectTaken_53(),
	Global_t3489172580_StaticFields::get_offset_of_multiChoice_54(),
	Global_t3489172580_StaticFields::get_offset_of_arrowKey_55(),
	Global_t3489172580_StaticFields::get_offset_of_playerReadyCheck_56(),
	Global_t3489172580_StaticFields::get_offset_of_m_sPrivateSavePoint_57(),
	Global_t3489172580_StaticFields::get_offset_of_tuiScene_58(),
	Global_t3489172580_StaticFields::get_offset_of_m_bTalkedWithMentor_59(),
	Global_t3489172580_StaticFields::get_offset_of_toggleGUI_60(),
	Global_t3489172580_StaticFields::get_offset_of_levelComplete_61(),
	Global_t3489172580_StaticFields::get_offset_of_savePointLoadLevelNumber_62(),
	Global_t3489172580_StaticFields::get_offset_of_loadedSavePoint_63(),
	Global_t3489172580_StaticFields::get_offset_of_levelStarted_64(),
	Global_t3489172580_StaticFields::get_offset_of_loadPos_65(),
	Global_t3489172580_StaticFields::get_offset_of_bsavePointToServerSuccess_66(),
	Global_t3489172580_StaticFields::get_offset_of_sSavePointToServerURL_67(),
	Global_t3489172580_StaticFields::get_offset_of_m_bPlayerAcceptHelp_68(),
	Global_t3489172580_StaticFields::get_offset_of_m_bPlayerAfterFire_69(),
	Global_t3489172580_StaticFields::get_offset_of_TestingOnline_70(),
	Global_t3489172580_StaticFields::get_offset_of_TestProgress_71(),
	Global_t3489172580_StaticFields::get_offset_of_TestSavePoint_72(),
	Global_t3489172580_StaticFields::get_offset_of_versionNumber_73(),
	Global_t3489172580_StaticFields::get_offset_of_Global_bshowVersionOnScreen_74(),
	Global_t3489172580_StaticFields::get_offset_of_TestingRetinaRes_75(),
	Global_t3489172580_StaticFields::get_offset_of_questionName_76(),
	Global_t3489172580_StaticFields::get_offset_of_LastResponseFromServer_77(),
	Global_t3489172580_StaticFields::get_offset_of_ScreenHeight_Factor_78(),
	Global_t3489172580_StaticFields::get_offset_of_ScreenWidth_Factor_79(),
	Global_t3489172580_StaticFields::get_offset_of_DPI_Factor_80(),
	Global_t3489172580_StaticFields::get_offset_of_Level2FireguyInteraction_81(),
	Global_t3489172580_StaticFields::get_offset_of_debugControls_82(),
	Global_t3489172580_StaticFields::get_offset_of_LogID_83(),
	Global_t3489172580_StaticFields::get_offset_of_LastEntry_84(),
	Global_t3489172580_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (GnatStatus_t3561057505)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2847[6] = 
{
	GnatStatus_t3561057505::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (GnatsCombat_t1950420656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[10] = 
{
	GnatsCombat_t1950420656::get_offset_of_m_bIsTriggered_2(),
	GnatsCombat_t1950420656::get_offset_of_Gnats_3(),
	GnatsCombat_t1950420656::get_offset_of_CombatTargets_4(),
	GnatsCombat_t1950420656::get_offset_of_iCurrentAttackingUnit_5(),
	GnatsCombat_t1950420656::get_offset_of_Defeated_6(),
	GnatsCombat_t1950420656::get_offset_of_DefeatPos_7(),
	GnatsCombat_t1950420656::get_offset_of_m_bHitPlayer_8(),
	GnatsCombat_t1950420656::get_offset_of_bAttackFirst_9(),
	GnatsCombat_t1950420656::get_offset_of_bSecondGnatAtacck_10(),
	GnatsCombat_t1950420656::get_offset_of_bCombatEnded_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[4] = 
{
	U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074::get_offset_of_U24this_0(),
	U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074::get_offset_of_U24current_1(),
	U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074::get_offset_of_U24disposing_2(),
	U3CWaitToRemoveHelpBannerU3Ec__Iterator0_t3780298074::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (GnatsAction_t2534711436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2850[4] = 
{
	GnatsAction_t2534711436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (GnatsInteraction_t1398631457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[9] = 
{
	GnatsInteraction_t1398631457::get_offset_of_IndexOfAction_2(),
	GnatsInteraction_t1398631457::get_offset_of_Actions_3(),
	GnatsInteraction_t1398631457::get_offset_of_TriggerPoints_4(),
	GnatsInteraction_t1398631457::get_offset_of_CameraID_5(),
	GnatsInteraction_t1398631457::get_offset_of_GUIskin_6(),
	GnatsInteraction_t1398631457::get_offset_of_bActivated_7(),
	GnatsInteraction_t1398631457::get_offset_of_Text_8(),
	GnatsInteraction_t1398631457::get_offset_of_m_talkScene_9(),
	GnatsInteraction_t1398631457::get_offset_of_distanceInteraction_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (GnatsMovement_t1997391497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[6] = 
{
	GnatsMovement_t1997391497::get_offset_of_m_bIsTriggered_2(),
	GnatsMovement_t1997391497::get_offset_of_m_bCircling_3(),
	GnatsMovement_t1997391497::get_offset_of_MovementTargets_4(),
	GnatsMovement_t1997391497::get_offset_of_BattleFormation_5(),
	GnatsMovement_t1997391497::get_offset_of_CurrentTarget_6(),
	GnatsMovement_t1997391497::get_offset_of_Gnats_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (Bag_t1045005451), -1, sizeof(Bag_t1045005451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2853[14] = 
{
	Bag_t1045005451::get_offset_of_m_bShow_2(),
	Bag_t1045005451::get_offset_of_m_skin_3(),
	Bag_t1045005451::get_offset_of_m_tBag_4(),
	Bag_t1045005451::get_offset_of_m_tItems_5(),
	Bag_t1045005451::get_offset_of_m_fMoveSpeed_6(),
	Bag_t1045005451_StaticFields::get_offset_of_m_fWidth_7(),
	Bag_t1045005451_StaticFields::get_offset_of_m_fHeight_8(),
	Bag_t1045005451::get_offset_of_m_fTimer_9(),
	Bag_t1045005451::get_offset_of_m_rBag_10(),
	Bag_t1045005451::get_offset_of_m_rItemRects_11(),
	Bag_t1045005451::get_offset_of_m_vFinalPos_12(),
	Bag_t1045005451::get_offset_of_m_bToggleShowItems_13(),
	Bag_t1045005451::get_offset_of_m_bMove_14(),
	Bag_t1045005451::get_offset_of_m_bNotebookUp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (CharacterSelection_t3241235818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[13] = 
{
	CharacterSelection_t3241235818::get_offset_of_m_skin_2(),
	CharacterSelection_t3241235818::get_offset_of_m_boyObjectPrf_3(),
	CharacterSelection_t3241235818::get_offset_of_m_girlObjectPrf_4(),
	CharacterSelection_t3241235818::get_offset_of_characterCreation_5(),
	CharacterSelection_t3241235818::get_offset_of_m_boyObjectClone_6(),
	CharacterSelection_t3241235818::get_offset_of_m_girlObjectClone_7(),
	CharacterSelection_t3241235818::get_offset_of_m_backgroundImage_8(),
	CharacterSelection_t3241235818::get_offset_of_m_characterCustomiseBG_9(),
	CharacterSelection_t3241235818::get_offset_of_m_vBoysPosition_10(),
	CharacterSelection_t3241235818::get_offset_of_m_vGirlsPosition_11(),
	CharacterSelection_t3241235818::get_offset_of_m_rect_12(),
	CharacterSelection_t3241235818::get_offset_of_theBoyTransform_13(),
	CharacterSelection_t3241235818::get_offset_of_m_v3WorldSpace_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (CutSceneStart_t3787429961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[8] = 
{
	CutSceneStart_t3787429961::get_offset_of_m_skin_2(),
	CutSceneStart_t3787429961::get_offset_of_m_currentSceneNum_3(),
	CutSceneStart_t3787429961::get_offset_of_m_talkScene_4(),
	CutSceneStart_t3787429961::get_offset_of_m_tCutSceneStartTexture1_5(),
	CutSceneStart_t3787429961::get_offset_of_m_tCutSceneStartTexture2_6(),
	CutSceneStart_t3787429961::get_offset_of_m_tCutSceneStartTexture3_7(),
	CutSceneStart_t3787429961::get_offset_of_m_tCutSceneStartTexture4_8(),
	CutSceneStart_t3787429961::get_offset_of_m_displayedTexture_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (DoorHelp_t3298842472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[5] = 
{
	DoorHelp_t3298842472::get_offset_of_bActivated_2(),
	DoorHelp_t3298842472::get_offset_of_GUIskin_3(),
	DoorHelp_t3298842472::get_offset_of_Text_4(),
	DoorHelp_t3298842472::get_offset_of_oDoor_5(),
	DoorHelp_t3298842472::get_offset_of_scriptObjectMovement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (DoorHelpTriger_t3891523883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	DoorHelpTriger_t3891523883::get_offset_of_m_EagleTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (DragAndDropWords_t1762529990), -1, sizeof(DragAndDropWords_t1762529990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[23] = 
{
	DragAndDropWords_t1762529990::get_offset_of_k_zeroRect_2(),
	DragAndDropWords_t1762529990::get_offset_of_m_skin_3(),
	DragAndDropWords_t1762529990::get_offset_of_m_conversationBoxText_4(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_m_keywordWidth_5(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_m_keywordHeight_6(),
	DragAndDropWords_t1762529990::get_offset_of_m_KeywordDimensions_7(),
	DragAndDropWords_t1762529990::get_offset_of_m_myOriRect_8(),
	DragAndDropWords_t1762529990::get_offset_of_m_keyWordText_9(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_m_dextinationRectWidth_10(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_m_dextinationRectHeight_11(),
	DragAndDropWords_t1762529990::get_offset_of_m_destinationOriKeyWordRect_12(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_m_L1destinationText_13(),
	DragAndDropWords_t1762529990::get_offset_of_m_destinationRect_14(),
	DragAndDropWords_t1762529990::get_offset_of_m_movableStringRect_15(),
	DragAndDropWords_t1762529990::get_offset_of_m_currentClickedStringRect_16(),
	DragAndDropWords_t1762529990::get_offset_of_m_currentClickedString_17(),
	DragAndDropWords_t1762529990::get_offset_of_m_currentIndex_18(),
	DragAndDropWords_t1762529990::get_offset_of_b_dragging_19(),
	DragAndDropWords_t1762529990::get_offset_of_m_tFillFieldBackground_20(),
	DragAndDropWords_t1762529990::get_offset_of_m_tDestinationBackground_21(),
	DragAndDropWords_t1762529990::get_offset_of_m_tConversationBackground_22(),
	DragAndDropWords_t1762529990::get_offset_of_menu_23(),
	DragAndDropWords_t1762529990_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (DraggableObject_t1812188907)+ sizeof (RuntimeObject), sizeof(DraggableObject_t1812188907_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[3] = 
{
	DraggableObject_t1812188907::get_offset_of_original_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DraggableObject_t1812188907::get_offset_of_movable_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DraggableObject_t1812188907::get_offset_of_words_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (DragDropShield_t3053351631), -1, sizeof(DragDropShield_t3053351631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2860[27] = 
{
	DragDropShield_t3053351631::get_offset_of_m_skin_2(),
	DragDropShield_t3053351631::get_offset_of_m_shieldPrf_3(),
	DragDropShield_t3053351631::get_offset_of_m_vShieldPosition_4(),
	DragDropShield_t3053351631::get_offset_of_m_shieldObjectClone_5(),
	DragDropShield_t3053351631::get_offset_of_m_conversationBoxText_6(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_TextWidth_7(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_TextHeight_8(),
	DragDropShield_t3053351631::get_offset_of_childObjectNames_9(),
	DragDropShield_t3053351631::get_offset_of_shieldNames_10(),
	DragDropShield_t3053351631::get_offset_of_shieldNameRects_11(),
	DragDropShield_t3053351631::get_offset_of_displayName_12(),
	DragDropShield_t3053351631::get_offset_of_displayColor_13(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_m_collisiondimension_14(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_m_movingTextwidth_15(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_m_movingTextHeight_16(),
	DragDropShield_t3053351631::get_offset_of_m_wordsRects_17(),
	DragDropShield_t3053351631_StaticFields::get_offset_of_m_targetDim_18(),
	DragDropShield_t3053351631::get_offset_of_m_destinations_19(),
	DragDropShield_t3053351631::get_offset_of_m_correctIndex_20(),
	DragDropShield_t3053351631::get_offset_of_m_currentDragText_21(),
	DragDropShield_t3053351631::get_offset_of_m_currentDragIndex_22(),
	DragDropShield_t3053351631::get_offset_of_m_dragging_23(),
	DragDropShield_t3053351631::get_offset_of_colourTexture_24(),
	DragDropShield_t3053351631::get_offset_of_colourFrameTexture_25(),
	DragDropShield_t3053351631::get_offset_of_greyScaleTexture_26(),
	DragDropShield_t3053351631::get_offset_of_m_tConversationBackground_27(),
	DragDropShield_t3053351631::get_offset_of_menu_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (EagleHelp_t1347797201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[4] = 
{
	EagleHelp_t1347797201::get_offset_of_bActivated_2(),
	EagleHelp_t1347797201::get_offset_of_GUIskin_3(),
	EagleHelp_t1347797201::get_offset_of_Text_4(),
	EagleHelp_t1347797201::get_offset_of_eagleOutline_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (EagleHelpTrigger_t1763195580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[1] = 
{
	EagleHelpTrigger_t1763195580::get_offset_of_m_EagleTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (FeelingDownScreen_t3177656924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[3] = 
{
	FeelingDownScreen_t3177656924::get_offset_of_m_skin_2(),
	FeelingDownScreen_t3177656924::get_offset_of_m_tFeelingDownTexture_3(),
	FeelingDownScreen_t3177656924::get_offset_of_screen_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (GemHelp_t3073439280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[3] = 
{
	GemHelp_t3073439280::get_offset_of_bActivated_2(),
	GemHelp_t3073439280::get_offset_of_GUIskin_3(),
	GemHelp_t3073439280::get_offset_of_Text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (GemHelpTriger_t1248575134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[1] = 
{
	GemHelpTriger_t1248575134::get_offset_of_m_EagleTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (GetAroundSparx_t2199752033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[18] = 
{
	GetAroundSparx_t2199752033::get_offset_of_m_skin_2(),
	GetAroundSparx_t2199752033::get_offset_of_m_theGuardian_3(),
	GetAroundSparx_t2199752033::get_offset_of_m_theMontor_4(),
	GetAroundSparx_t2199752033::get_offset_of_m_mouseCorsorImage_5(),
	GetAroundSparx_t2199752033::get_offset_of_m_closeImage_6(),
	GetAroundSparx_t2199752033::get_offset_of_m_backPackImage_7(),
	GetAroundSparx_t2199752033::get_offset_of_m_sparxManulImage_8(),
	GetAroundSparx_t2199752033::get_offset_of_loadingIcon_9(),
	GetAroundSparx_t2199752033::get_offset_of_m_Eagle_10(),
	GetAroundSparx_t2199752033::get_offset_of_m_Latern_11(),
	GetAroundSparx_t2199752033::get_offset_of_m_Gnat_12(),
	GetAroundSparx_t2199752033::get_offset_of_m_Lader_13(),
	GetAroundSparx_t2199752033::get_offset_of_m_Control_14(),
	GetAroundSparx_t2199752033::get_offset_of_m_Control2_15(),
	GetAroundSparx_t2199752033::get_offset_of_m_gardianClone_16(),
	GetAroundSparx_t2199752033::get_offset_of_m_mentorClone_17(),
	GetAroundSparx_t2199752033::get_offset_of_m_talkScene_18(),
	GetAroundSparx_t2199752033::get_offset_of_m_gGardianObject_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (GL_t1956413763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	GL_t1956413763::get_offset_of_CurrentInteractNPC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (Goodbye_t2957868680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[7] = 
{
	Goodbye_t2957868680::get_offset_of_m_skin_2(),
	Goodbye_t2957868680::get_offset_of_m_bPlayerIsWaving_3(),
	Goodbye_t2957868680::get_offset_of_m_bShowGoodbyeScreen_4(),
	Goodbye_t2957868680::get_offset_of_m_bSavePointRoutine_5(),
	Goodbye_t2957868680::get_offset_of_m_bGameWasQuit_6(),
	Goodbye_t2957868680::get_offset_of_menu_7(),
	Goodbye_t2957868680::get_offset_of_instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (U3CrestartApplicationU3Ec__Iterator0_t4161125173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[5] = 
{
	U3CrestartApplicationU3Ec__Iterator0_t4161125173::get_offset_of_U3CrunningU3E__0_0(),
	U3CrestartApplicationU3Ec__Iterator0_t4161125173::get_offset_of_U24this_1(),
	U3CrestartApplicationU3Ec__Iterator0_t4161125173::get_offset_of_U24current_2(),
	U3CrestartApplicationU3Ec__Iterator0_t4161125173::get_offset_of_U24disposing_3(),
	U3CrestartApplicationU3Ec__Iterator0_t4161125173::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (HelpLine_t2975842282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[1] = 
{
	HelpLine_t2975842282::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (HelpSparx_t654320582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[18] = 
{
	HelpSparx_t654320582::get_offset_of_m_skin_2(),
	HelpSparx_t654320582::get_offset_of_m_theGuardian_3(),
	HelpSparx_t654320582::get_offset_of_m_theMontor_4(),
	HelpSparx_t654320582::get_offset_of_m_mouseCorsorImage_5(),
	HelpSparx_t654320582::get_offset_of_m_closeImage_6(),
	HelpSparx_t654320582::get_offset_of_m_backPackImage_7(),
	HelpSparx_t654320582::get_offset_of_m_sparxManulImage_8(),
	HelpSparx_t654320582::get_offset_of_loadingIcon_9(),
	HelpSparx_t654320582::get_offset_of_m_Eagle_10(),
	HelpSparx_t654320582::get_offset_of_m_Latern_11(),
	HelpSparx_t654320582::get_offset_of_m_Gnat_12(),
	HelpSparx_t654320582::get_offset_of_m_Lader_13(),
	HelpSparx_t654320582::get_offset_of_m_Control_14(),
	HelpSparx_t654320582::get_offset_of_m_Control2_15(),
	HelpSparx_t654320582::get_offset_of_m_gardianClone_16(),
	HelpSparx_t654320582::get_offset_of_m_mentorClone_17(),
	HelpSparx_t654320582::get_offset_of_m_talkScene_18(),
	HelpSparx_t654320582::get_offset_of_m_gGardianObject_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (KeyButtonScriptRAPA_t1321666730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[6] = 
{
	KeyButtonScriptRAPA_t1321666730::get_offset_of_m_skin_2(),
	KeyButtonScriptRAPA_t1321666730::get_offset_of_m_blincRects_3(),
	KeyButtonScriptRAPA_t1321666730::get_offset_of_menu_4(),
	KeyButtonScriptRAPA_t1321666730::get_offset_of_m_blinc_5(),
	KeyButtonScriptRAPA_t1321666730::get_offset_of_m_conversationBoxText_6(),
	KeyButtonScriptRAPA_t1321666730::get_offset_of_m_tKey_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (L2DragAndDropWords_t1602406363), -1, sizeof(L2DragAndDropWords_t1602406363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2873[23] = 
{
	L2DragAndDropWords_t1602406363::get_offset_of_k_zeroRect_2(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_skin_3(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_conversationBoxText_4(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_m_keywordWidth_5(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_m_keywordHeight_6(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_KeywordDimensions_7(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_myOriRect_8(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_keyWordText_9(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_m_dextinationRectWidth_10(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_m_dextinationRectHeight_11(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_destinationOriKeyWordRect_12(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_m_L2destinationText_13(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_destinationRect_14(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_movableStringRect_15(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_currentClickedStringRect_16(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_currentClickedString_17(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_currentIndex_18(),
	L2DragAndDropWords_t1602406363::get_offset_of_b_dragging_19(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_tFillFieldBackground_20(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_tDestinationBackground_21(),
	L2DragAndDropWords_t1602406363::get_offset_of_m_tConversationBackground_22(),
	L2DragAndDropWords_t1602406363::get_offset_of_menu_23(),
	L2DragAndDropWords_t1602406363_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (L2RelaxYourMuscles_t1973662748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[6] = 
{
	L2RelaxYourMuscles_t1973662748::get_offset_of_m_skin_2(),
	L2RelaxYourMuscles_t1973662748::get_offset_of_m_tMuscles_3(),
	L2RelaxYourMuscles_t1973662748::get_offset_of_m_fTimer_4(),
	L2RelaxYourMuscles_t1973662748::get_offset_of_m_strNames_5(),
	L2RelaxYourMuscles_t1973662748::get_offset_of_m_iCurrentImage_6(),
	L2RelaxYourMuscles_t1973662748::get_offset_of_menu_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (L2RelexYourMusel_t1858011544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[9] = 
{
	L2RelexYourMusel_t1858011544::get_offset_of_m_skin_2(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_tBodyTexture0_3(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_tArmTexture1_4(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_tLegsTexture2_5(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_tChestTexture3_6(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_talkScene_7(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_displayedTexture_8(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_fStartTime_9(),
	L2RelexYourMusel_t1858011544::get_offset_of_m_sTitleName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (L2ShieldAgaDepression_t2978038194), -1, sizeof(L2ShieldAgaDepression_t2978038194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2876[12] = 
{
	L2ShieldAgaDepression_t2978038194::get_offset_of_m_skin_2(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_m_shieldPrf_3(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_m_vShieldPosition_4(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_m_shieldObjectClone_5(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_menuCanvasObject_6(),
	L2ShieldAgaDepression_t2978038194_StaticFields::get_offset_of_TextWidth_7(),
	L2ShieldAgaDepression_t2978038194_StaticFields::get_offset_of_TextHeight_8(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_childObjectNames_9(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_displayName_10(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_displayColor_11(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_colourTexture_12(),
	L2ShieldAgaDepression_t2978038194::get_offset_of_greyScaleTexture_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (L3DragAndDropBlinc_t4110504062), -1, sizeof(L3DragAndDropBlinc_t4110504062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2877[18] = 
{
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_skin_2(),
	L3DragAndDropBlinc_t4110504062_StaticFields::get_offset_of_ScreenHeight_Factor_3(),
	L3DragAndDropBlinc_t4110504062_StaticFields::get_offset_of_ScreenWidth_Factor_4(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_conversationBoxText_5(),
	L3DragAndDropBlinc_t4110504062_StaticFields::get_offset_of_m_collisiondimension_6(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterRects_7(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_placed_8(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_destinations_9(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_finalLetterPos_10(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_currentDragIndex_11(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_dragging_12(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterB_13(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterL_14(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterI_15(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterN_16(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_letterC_17(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_wordBLINC_18(),
	L3DragAndDropBlinc_t4110504062::get_offset_of_m_tConversationBackground_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (L3DragAndDropBlinc_Canvas_t4294777750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (L3DragAndDropBlinc_DragHandler_t2105775540), -1, sizeof(L3DragAndDropBlinc_DragHandler_t2105775540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2879[4] = 
{
	L3DragAndDropBlinc_DragHandler_t2105775540_StaticFields::get_offset_of_itemBeingDregged_2(),
	L3DragAndDropBlinc_DragHandler_t2105775540::get_offset_of_startPosittion_3(),
	L3DragAndDropBlinc_DragHandler_t2105775540::get_offset_of_oText_4(),
	L3DragAndDropBlinc_DragHandler_t2105775540::get_offset_of_startParent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (L3DragAndDropBlinc_Inventory_t2077180760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[2] = 
{
	L3DragAndDropBlinc_Inventory_t2077180760::get_offset_of_oNextButton_2(),
	L3DragAndDropBlinc_Inventory_t2077180760::get_offset_of_nItems_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (L3DragAndDropBlinc_Item_t2347719852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	L3DragAndDropBlinc_Item_t2347719852::get_offset_of_oMinigameManager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (L3DragDropWords_t3281039464), -1, sizeof(L3DragDropWords_t3281039464_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2882[23] = 
{
	L3DragDropWords_t3281039464::get_offset_of_k_zeroRect_2(),
	L3DragDropWords_t3281039464::get_offset_of_m_skin_3(),
	L3DragDropWords_t3281039464::get_offset_of_menu_4(),
	L3DragDropWords_t3281039464::get_offset_of_m_conversationBoxText_5(),
	L3DragDropWords_t3281039464_StaticFields::get_offset_of_m_keywordWidth_6(),
	L3DragDropWords_t3281039464_StaticFields::get_offset_of_m_keywordHeight_7(),
	L3DragDropWords_t3281039464::get_offset_of_m_KeywordDimensions_8(),
	L3DragDropWords_t3281039464::get_offset_of_m_myOriRect_9(),
	L3DragDropWords_t3281039464::get_offset_of_m_keyWordText_10(),
	L3DragDropWords_t3281039464_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragDropWords_t3281039464_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragDropWords_t3281039464::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragDropWords_t3281039464::get_offset_of_m_destinationText_14(),
	L3DragDropWords_t3281039464::get_offset_of_m_destinationRect_15(),
	L3DragDropWords_t3281039464::get_offset_of_m_movableStringRect_16(),
	L3DragDropWords_t3281039464::get_offset_of_m_currentClickedStringRect_17(),
	L3DragDropWords_t3281039464::get_offset_of_m_currentClickedString_18(),
	L3DragDropWords_t3281039464::get_offset_of_m_currentIndex_19(),
	L3DragDropWords_t3281039464::get_offset_of_b_dragging_20(),
	L3DragDropWords_t3281039464::get_offset_of_m_tFillFieldBackground_21(),
	L3DragDropWords_t3281039464::get_offset_of_m_tDestinationBackground_22(),
	L3DragDropWords_t3281039464::get_offset_of_m_tConversationBackground_23(),
	L3DragDropWords_t3281039464_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (L3DragDropWords2_t3194046937), -1, sizeof(L3DragDropWords2_t3194046937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2883[23] = 
{
	L3DragDropWords2_t3194046937::get_offset_of_k_zeroRect_2(),
	L3DragDropWords2_t3194046937::get_offset_of_m_skin_3(),
	L3DragDropWords2_t3194046937::get_offset_of_menu_4(),
	L3DragDropWords2_t3194046937::get_offset_of_m_conversationBoxText_5(),
	L3DragDropWords2_t3194046937_StaticFields::get_offset_of_m_keywordWidth_6(),
	L3DragDropWords2_t3194046937_StaticFields::get_offset_of_m_keywordHeight_7(),
	L3DragDropWords2_t3194046937::get_offset_of_m_KeywordDimensions_8(),
	L3DragDropWords2_t3194046937::get_offset_of_m_myOriRect_9(),
	L3DragDropWords2_t3194046937::get_offset_of_m_keyWordText_10(),
	L3DragDropWords2_t3194046937_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragDropWords2_t3194046937_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragDropWords2_t3194046937::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragDropWords2_t3194046937::get_offset_of_m_destinationText_14(),
	L3DragDropWords2_t3194046937::get_offset_of_m_destinationRect_15(),
	L3DragDropWords2_t3194046937::get_offset_of_m_movableStringRect_16(),
	L3DragDropWords2_t3194046937::get_offset_of_m_currentClickedStringRect_17(),
	L3DragDropWords2_t3194046937::get_offset_of_m_currentClickedString_18(),
	L3DragDropWords2_t3194046937::get_offset_of_m_currentIndex_19(),
	L3DragDropWords2_t3194046937::get_offset_of_b_dragging_20(),
	L3DragDropWords2_t3194046937::get_offset_of_m_tFillFieldBackground_21(),
	L3DragDropWords2_t3194046937::get_offset_of_m_tDestinationBackground_22(),
	L3DragDropWords2_t3194046937::get_offset_of_m_tConversationBackground_23(),
	L3DragDropWords2_t3194046937_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (L3DragDropWords3_t375893153), -1, sizeof(L3DragDropWords3_t375893153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2884[23] = 
{
	L3DragDropWords3_t375893153::get_offset_of_k_zeroRect_2(),
	L3DragDropWords3_t375893153::get_offset_of_m_skin_3(),
	L3DragDropWords3_t375893153::get_offset_of_menu_4(),
	L3DragDropWords3_t375893153::get_offset_of_m_conversationBoxText_5(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_m_keywordWidth_6(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_m_keywordHeight_7(),
	L3DragDropWords3_t375893153::get_offset_of_m_KeywordDimensions_8(),
	L3DragDropWords3_t375893153::get_offset_of_m_myOriRect_9(),
	L3DragDropWords3_t375893153::get_offset_of_m_keyWordText_10(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragDropWords3_t375893153::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_m_L3destinationText_14(),
	L3DragDropWords3_t375893153::get_offset_of_m_destinationRect_15(),
	L3DragDropWords3_t375893153::get_offset_of_m_movableStringRect_16(),
	L3DragDropWords3_t375893153::get_offset_of_m_currentClickedStringRect_17(),
	L3DragDropWords3_t375893153::get_offset_of_m_currentClickedString_18(),
	L3DragDropWords3_t375893153::get_offset_of_m_currentIndex_19(),
	L3DragDropWords3_t375893153::get_offset_of_b_dragging_20(),
	L3DragDropWords3_t375893153::get_offset_of_m_tFillFieldBackground_21(),
	L3DragDropWords3_t375893153::get_offset_of_m_tDestinationBackground_22(),
	L3DragDropWords3_t375893153::get_offset_of_m_tConversationBackground_23(),
	L3DragDropWords3_t375893153_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (L3DragDropWords4_t2882406281), -1, sizeof(L3DragDropWords4_t2882406281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2885[23] = 
{
	L3DragDropWords4_t2882406281::get_offset_of_k_zeroRect_2(),
	L3DragDropWords4_t2882406281::get_offset_of_m_skin_3(),
	L3DragDropWords4_t2882406281::get_offset_of_menu_4(),
	L3DragDropWords4_t2882406281::get_offset_of_m_conversationBoxText_5(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_m_keywordWidth_6(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_m_keywordHeight_7(),
	L3DragDropWords4_t2882406281::get_offset_of_m_KeywordDimensions_8(),
	L3DragDropWords4_t2882406281::get_offset_of_m_myOriRect_9(),
	L3DragDropWords4_t2882406281::get_offset_of_m_keyWordText_10(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragDropWords4_t2882406281::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_m_L3destinationText_14(),
	L3DragDropWords4_t2882406281::get_offset_of_m_destinationRect_15(),
	L3DragDropWords4_t2882406281::get_offset_of_m_movableStringRect_16(),
	L3DragDropWords4_t2882406281::get_offset_of_m_currentClickedStringRect_17(),
	L3DragDropWords4_t2882406281::get_offset_of_m_currentClickedString_18(),
	L3DragDropWords4_t2882406281::get_offset_of_m_currentIndex_19(),
	L3DragDropWords4_t2882406281::get_offset_of_b_dragging_20(),
	L3DragDropWords4_t2882406281::get_offset_of_m_tFillFieldBackground_21(),
	L3DragDropWords4_t2882406281::get_offset_of_m_tDestinationBackground_22(),
	L3DragDropWords4_t2882406281::get_offset_of_m_tConversationBackground_23(),
	L3DragDropWords4_t2882406281_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (L3DragDropWords5_t114919130), -1, sizeof(L3DragDropWords5_t114919130_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2886[22] = 
{
	L3DragDropWords5_t114919130::get_offset_of_k_zeroRect_2(),
	L3DragDropWords5_t114919130::get_offset_of_m_skin_3(),
	L3DragDropWords5_t114919130::get_offset_of_menu_4(),
	L3DragDropWords5_t114919130::get_offset_of_m_conversationBoxText_5(),
	L3DragDropWords5_t114919130_StaticFields::get_offset_of_m_keywordWidth_6(),
	L3DragDropWords5_t114919130_StaticFields::get_offset_of_m_keywordHeight_7(),
	L3DragDropWords5_t114919130::get_offset_of_m_KeywordDimensions_8(),
	L3DragDropWords5_t114919130::get_offset_of_m_myOriRect_9(),
	L3DragDropWords5_t114919130::get_offset_of_m_keyWordText_10(),
	L3DragDropWords5_t114919130_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragDropWords5_t114919130_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragDropWords5_t114919130::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragDropWords5_t114919130_StaticFields::get_offset_of_m_L3destinationText_14(),
	L3DragDropWords5_t114919130::get_offset_of_m_destinationRect_15(),
	L3DragDropWords5_t114919130::get_offset_of_m_movableStringRect_16(),
	L3DragDropWords5_t114919130::get_offset_of_m_currentClickedStringRect_17(),
	L3DragDropWords5_t114919130::get_offset_of_m_currentClickedString_18(),
	L3DragDropWords5_t114919130::get_offset_of_m_currentIndex_19(),
	L3DragDropWords5_t114919130::get_offset_of_b_dragging_20(),
	L3DragDropWords5_t114919130::get_offset_of_m_tFillFieldBackground_21(),
	L3DragDropWords5_t114919130::get_offset_of_m_tDestinationBackground_22(),
	L3DragDropWords5_t114919130::get_offset_of_m_tConversationBackground_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (L3DragWordsHowIReact_t1761835085), -1, sizeof(L3DragWordsHowIReact_t1761835085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2887[24] = 
{
	L3DragWordsHowIReact_t1761835085::get_offset_of_k_zeroRect_2(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_skin_3(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_conversationBoxText_4(),
	L3DragWordsHowIReact_t1761835085_StaticFields::get_offset_of_m_keywordWidth_5(),
	L3DragWordsHowIReact_t1761835085_StaticFields::get_offset_of_m_keywordHeight_6(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_KeywordDimensions_7(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_myOriRect_8(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_keyWordText_9(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_descriptions_10(),
	L3DragWordsHowIReact_t1761835085_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L3DragWordsHowIReact_t1761835085_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_destinationOriKeyWordRect_13(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_destinationText_14(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_destinationRect_15(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_movableStringRect_16(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_currentClickedStringRect_17(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_currentClickedString_18(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_currentIndex_19(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_b_dragging_20(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_tFillFieldBackground_21(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_tDestinationBackground_22(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_m_tConversationBackground_23(),
	L3DragWordsHowIReact_t1761835085::get_offset_of_listScreen_24(),
	L3DragWordsHowIReact_t1761835085_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (L3HowIFeelMiniGame_t490880838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[1] = 
{
	L3HowIFeelMiniGame_t490880838::get_offset_of_widhif_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (L3ShieldAgaDepression_t1653586630), -1, sizeof(L3ShieldAgaDepression_t1653586630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2889[13] = 
{
	L3ShieldAgaDepression_t1653586630::get_offset_of_m_skin_2(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_m_shieldPrf_3(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_m_vShieldPosition_4(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_m_shieldObjectClone_5(),
	L3ShieldAgaDepression_t1653586630_StaticFields::get_offset_of_TextWidth_6(),
	L3ShieldAgaDepression_t1653586630_StaticFields::get_offset_of_TextHeight_7(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_childObjectNames_8(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_shieldNames_9(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_displayName_10(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_displayColor_11(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_colourTexture_12(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_greyScaleTexture_13(),
	L3ShieldAgaDepression_t1653586630::get_offset_of_shieldScreen_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (L3StopIt_t150832697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[1] = 
{
	L3StopIt_t150832697::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (L3StopItWithText_t4263992057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[1] = 
{
	L3StopItWithText_t4263992057::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (L3TrashIt_t838776596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[1] = 
{
	L3TrashIt_t838776596::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (L3TurnItDown_t2719411953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[1] = 
{
	L3TurnItDown_t2719411953::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (L4DragDropWords_t3565732689), -1, sizeof(L4DragDropWords_t3565732689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2894[23] = 
{
	L4DragDropWords_t3565732689::get_offset_of_k_zeroRect_2(),
	L4DragDropWords_t3565732689::get_offset_of_m_skin_3(),
	L4DragDropWords_t3565732689::get_offset_of_menu_4(),
	L4DragDropWords_t3565732689::get_offset_of_m_conversationBoxText_5(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_m_keywordWidth_6(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_m_keywordHeight_7(),
	L4DragDropWords_t3565732689::get_offset_of_m_KeywordDimensions_8(),
	L4DragDropWords_t3565732689::get_offset_of_m_myOriRect_9(),
	L4DragDropWords_t3565732689::get_offset_of_m_keyWordText_10(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_m_dextinationRectWidth_11(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_m_dextinationRectHeight_12(),
	L4DragDropWords_t3565732689::get_offset_of_m_destinationOriKeyWordRect_13(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_m_L4destinationText_14(),
	L4DragDropWords_t3565732689::get_offset_of_m_destinationRect_15(),
	L4DragDropWords_t3565732689::get_offset_of_m_movableStringRect_16(),
	L4DragDropWords_t3565732689::get_offset_of_m_currentClickedStringRect_17(),
	L4DragDropWords_t3565732689::get_offset_of_m_currentClickedString_18(),
	L4DragDropWords_t3565732689::get_offset_of_m_currentIndex_19(),
	L4DragDropWords_t3565732689::get_offset_of_b_dragging_20(),
	L4DragDropWords_t3565732689::get_offset_of_m_tFillFieldBackground_21(),
	L4DragDropWords_t3565732689::get_offset_of_m_tDestinationBackground_22(),
	L4DragDropWords_t3565732689::get_offset_of_m_tConversationBackground_23(),
	L4DragDropWords_t3565732689_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (L4DragDropWords2_t4012073771), -1, sizeof(L4DragDropWords2_t4012073771_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2895[23] = 
{
	L4DragDropWords2_t4012073771::get_offset_of_k_zeroRect_2(),
	L4DragDropWords2_t4012073771::get_offset_of_m_skin_3(),
	L4DragDropWords2_t4012073771::get_offset_of_m_conversationBoxText_4(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_m_keywordWidth_5(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_m_keywordHeight_6(),
	L4DragDropWords2_t4012073771::get_offset_of_m_KeywordDimensions_7(),
	L4DragDropWords2_t4012073771::get_offset_of_m_myOriRect_8(),
	L4DragDropWords2_t4012073771::get_offset_of_m_keyWordText_9(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_m_dextinationRectWidth_10(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_m_dextinationRectHeight_11(),
	L4DragDropWords2_t4012073771::get_offset_of_m_destinationOriKeyWordRect_12(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_m_L4destinationText_13(),
	L4DragDropWords2_t4012073771::get_offset_of_m_destinationRect_14(),
	L4DragDropWords2_t4012073771::get_offset_of_m_movableStringRect_15(),
	L4DragDropWords2_t4012073771::get_offset_of_m_currentClickedStringRect_16(),
	L4DragDropWords2_t4012073771::get_offset_of_m_currentClickedString_17(),
	L4DragDropWords2_t4012073771::get_offset_of_m_currentIndex_18(),
	L4DragDropWords2_t4012073771::get_offset_of_b_dragging_19(),
	L4DragDropWords2_t4012073771::get_offset_of_m_tFillFieldBackground_20(),
	L4DragDropWords2_t4012073771::get_offset_of_m_tDestinationBackground_21(),
	L4DragDropWords2_t4012073771::get_offset_of_m_tConversationBackground_22(),
	L4DragDropWords2_t4012073771::get_offset_of_menu_23(),
	L4DragDropWords2_t4012073771_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (L4EnterTexts_t4102979614), -1, sizeof(L4EnterTexts_t4102979614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2896[3] = 
{
	L4EnterTexts_t4102979614_StaticFields::get_offset_of_m_bTriggered_2(),
	L4EnterTexts_t4102979614_StaticFields::get_offset_of_m_inputTexts_3(),
	L4EnterTexts_t4102979614::get_offset_of_menu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (L4ShieldAgaDepression_t944406110), -1, sizeof(L4ShieldAgaDepression_t944406110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2897[14] = 
{
	L4ShieldAgaDepression_t944406110::get_offset_of_m_skin_2(),
	L4ShieldAgaDepression_t944406110::get_offset_of_m_shieldPrf_3(),
	L4ShieldAgaDepression_t944406110::get_offset_of_m_vShieldPosition_4(),
	L4ShieldAgaDepression_t944406110::get_offset_of_m_shieldObjectClone_5(),
	L4ShieldAgaDepression_t944406110_StaticFields::get_offset_of_TextWidth_6(),
	L4ShieldAgaDepression_t944406110_StaticFields::get_offset_of_TextHeight_7(),
	L4ShieldAgaDepression_t944406110::get_offset_of_childObjectNames_8(),
	L4ShieldAgaDepression_t944406110::get_offset_of_shieldNames_9(),
	L4ShieldAgaDepression_t944406110::get_offset_of_shieldNameRects_10(),
	L4ShieldAgaDepression_t944406110::get_offset_of_displayName_11(),
	L4ShieldAgaDepression_t944406110::get_offset_of_displayColor_12(),
	L4ShieldAgaDepression_t944406110::get_offset_of_colourTexture_13(),
	L4ShieldAgaDepression_t944406110::get_offset_of_greyScaleTexture_14(),
	L4ShieldAgaDepression_t944406110::get_offset_of_menu_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (L5DragDropShield_t3805322197), -1, sizeof(L5DragDropShield_t3805322197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2898[27] = 
{
	L5DragDropShield_t3805322197::get_offset_of_m_skin_2(),
	L5DragDropShield_t3805322197::get_offset_of_m_shieldPrf_3(),
	L5DragDropShield_t3805322197::get_offset_of_m_vShieldPosition_4(),
	L5DragDropShield_t3805322197::get_offset_of_m_shieldObjectClone_5(),
	L5DragDropShield_t3805322197::get_offset_of_m_conversationBoxText_6(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_TextWidth_7(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_TextHeight_8(),
	L5DragDropShield_t3805322197::get_offset_of_childObjectNames_9(),
	L5DragDropShield_t3805322197::get_offset_of_shieldNames_10(),
	L5DragDropShield_t3805322197::get_offset_of_shieldNameRects_11(),
	L5DragDropShield_t3805322197::get_offset_of_displayName_12(),
	L5DragDropShield_t3805322197::get_offset_of_displayColor_13(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_m_collisiondimension_14(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_m_movingTextwidth_15(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_m_movingTextHeight_16(),
	L5DragDropShield_t3805322197::get_offset_of_menu_17(),
	L5DragDropShield_t3805322197::get_offset_of_m_wordsRects_18(),
	L5DragDropShield_t3805322197_StaticFields::get_offset_of_m_targetDim_19(),
	L5DragDropShield_t3805322197::get_offset_of_m_destinations_20(),
	L5DragDropShield_t3805322197::get_offset_of_m_correctIndex_21(),
	L5DragDropShield_t3805322197::get_offset_of_m_currentDragText_22(),
	L5DragDropShield_t3805322197::get_offset_of_m_currentDragIndex_23(),
	L5DragDropShield_t3805322197::get_offset_of_m_dragging_24(),
	L5DragDropShield_t3805322197::get_offset_of_colourTexture_25(),
	L5DragDropShield_t3805322197::get_offset_of_colourFrameTexture_26(),
	L5DragDropShield_t3805322197::get_offset_of_greyScaleTexture_27(),
	L5DragDropShield_t3805322197::get_offset_of_m_tConversationBackground_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (L5DragDropWords_t3511694991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[3] = 
{
	L5DragDropWords_t3511694991::get_offset_of_m_destinationText_2(),
	L5DragDropWords_t3511694991::get_offset_of_draggables_3(),
	L5DragDropWords_t3511694991::get_offset_of_menu_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
