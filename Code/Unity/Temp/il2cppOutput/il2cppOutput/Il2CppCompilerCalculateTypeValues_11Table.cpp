﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Net.CookieCollection
struct CookieCollection_t1776842238;
// System.UInt16[]
struct UInt16U5BU5D_t610847954;
// System.String
struct String_t;
// System.Uri
struct Uri_t3269222095;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t504895258;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.Exception
struct Exception_t4086964929;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2352629529;
// System.IO.Stream
struct Stream_t4138477179;
// System.Net.WebConnection
struct WebConnection_t3591103104;
// System.Net.HttpWebRequest
struct HttpWebRequest_t4046343009;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t592894254;
// System.Version
struct Version_t2250829542;
// System.Net.ServicePoint
struct ServicePoint_t3505702983;
// System.Collections.ArrayList
struct ArrayList_t4250946984;
// System.Random
struct Random_t562893486;
// System.Collections.Queue
struct Queue_t2654900338;
// System.Net.ICredentials
struct ICredentials_t3564800082;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t1895646286;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t1517694262;
// System.Collections.IComparer
struct IComparer_t2882245263;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t259346668;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1720530229;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t4123925056;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t4165028003;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t3005941167;
// System.Security.Cryptography.Oid
struct Oid_t2612574631;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t3687691741;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t3432709581;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t2543835050;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Net.FtpWebResponse
struct FtpWebResponse_t1950878246;
// System.Void
struct Void_t653366341;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct X509ExtensionCollection_t3160461637;
// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t3840515815;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct X500DistinguishedName_t572620573;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t3432709582;
// System.IO.MemoryStream
struct MemoryStream_t2933948287;
// System.Net.FtpWebRequest
struct FtpWebRequest_t4270856504;
// System.Net.IPAddress
struct IPAddress_t86924491;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.IO.FileStream
struct FileStream_t3854196930;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4030136713;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1549710636;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t439385300;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t2157293745;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2807660831;
// System.Net.CookieContainer
struct CookieContainer_t390599126;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t454981218;
// Mono.Security.X509.X509Store
struct X509Store_t3451023606;
// System.Net.Sockets.Socket
struct Socket_t4168736505;
// System.Threading.WaitCallback
struct WaitCallback_t4234066651;
// System.EventHandler
struct EventHandler_t369332490;
// System.Net.WebConnection/AbortHelper
struct AbortHelper_t2979836766;
// System.Net.WebConnectionData
struct WebConnectionData_t1471135888;
// System.Net.ChunkStream
struct ChunkStream_t262715251;
// System.Net.NetworkCredential
struct NetworkCredential_t1328334253;
// System.Type
struct Type_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Net.WebResponse
struct WebResponse_t3616229315;
// System.Net.IWebProxy
struct IWebProxy_t2987120949;
// System.Security.Cryptography.OidCollection
struct OidCollection_t678319162;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t1403970469;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t4062409115;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct X509ChainElementCollection_t2987578632;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct X509ChainPolicy_t45074389;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_t1254084101;
// System.Security.Cryptography.X509Certificates.X509Store
struct X509Store_t2057865090;
// System.Net.FileWebResponse
struct FileWebResponse_t982058165;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1210004121;
// System.Net.FileWebRequest
struct FileWebRequest_t3154753421;
// System.IO.StreamReader
struct StreamReader_t1796800705;
// System.Net.IPHostEntry
struct IPHostEntry_t4214309905;
// System.Net.IPEndPoint
struct IPEndPoint_t1226219872;
// System.Net.FtpAsyncResult
struct FtpAsyncResult_t3500703300;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t2642281915;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t1147245406;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4215975066;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1109801809;
// System.Net.DigestHeaderParser
struct DigestHeaderParser_t477534254;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t728319640;
// System.Net.HttpContinueDelegate
struct HttpContinueDelegate_t510056555;
// System.Net.WebConnectionStream
struct WebConnectionStream_t3750989120;
// System.Net.WebAsyncResult
struct WebAsyncResult_t1279310773;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COOKIECONTAINER_T390599126_H
#define COOKIECONTAINER_T390599126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieContainer
struct  CookieContainer_t390599126  : public RuntimeObject
{
public:
	// System.Int32 System.Net.CookieContainer::capacity
	int32_t ___capacity_0;
	// System.Int32 System.Net.CookieContainer::perDomainCapacity
	int32_t ___perDomainCapacity_1;
	// System.Int32 System.Net.CookieContainer::maxCookieSize
	int32_t ___maxCookieSize_2;
	// System.Net.CookieCollection System.Net.CookieContainer::cookies
	CookieCollection_t1776842238 * ___cookies_3;

public:
	inline static int32_t get_offset_of_capacity_0() { return static_cast<int32_t>(offsetof(CookieContainer_t390599126, ___capacity_0)); }
	inline int32_t get_capacity_0() const { return ___capacity_0; }
	inline int32_t* get_address_of_capacity_0() { return &___capacity_0; }
	inline void set_capacity_0(int32_t value)
	{
		___capacity_0 = value;
	}

	inline static int32_t get_offset_of_perDomainCapacity_1() { return static_cast<int32_t>(offsetof(CookieContainer_t390599126, ___perDomainCapacity_1)); }
	inline int32_t get_perDomainCapacity_1() const { return ___perDomainCapacity_1; }
	inline int32_t* get_address_of_perDomainCapacity_1() { return &___perDomainCapacity_1; }
	inline void set_perDomainCapacity_1(int32_t value)
	{
		___perDomainCapacity_1 = value;
	}

	inline static int32_t get_offset_of_maxCookieSize_2() { return static_cast<int32_t>(offsetof(CookieContainer_t390599126, ___maxCookieSize_2)); }
	inline int32_t get_maxCookieSize_2() const { return ___maxCookieSize_2; }
	inline int32_t* get_address_of_maxCookieSize_2() { return &___maxCookieSize_2; }
	inline void set_maxCookieSize_2(int32_t value)
	{
		___maxCookieSize_2 = value;
	}

	inline static int32_t get_offset_of_cookies_3() { return static_cast<int32_t>(offsetof(CookieContainer_t390599126, ___cookies_3)); }
	inline CookieCollection_t1776842238 * get_cookies_3() const { return ___cookies_3; }
	inline CookieCollection_t1776842238 ** get_address_of_cookies_3() { return &___cookies_3; }
	inline void set_cookies_3(CookieCollection_t1776842238 * value)
	{
		___cookies_3 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECONTAINER_T390599126_H
#ifndef IPV6ADDRESS_T496378312_H
#define IPV6ADDRESS_T496378312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6Address
struct  IPv6Address_t496378312  : public RuntimeObject
{
public:
	// System.UInt16[] System.Net.IPv6Address::address
	UInt16U5BU5D_t610847954* ___address_0;
	// System.Int32 System.Net.IPv6Address::prefixLength
	int32_t ___prefixLength_1;
	// System.Int64 System.Net.IPv6Address::scopeId
	int64_t ___scopeId_2;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6Address_t496378312, ___address_0)); }
	inline UInt16U5BU5D_t610847954* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t610847954** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t610847954* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_prefixLength_1() { return static_cast<int32_t>(offsetof(IPv6Address_t496378312, ___prefixLength_1)); }
	inline int32_t get_prefixLength_1() const { return ___prefixLength_1; }
	inline int32_t* get_address_of_prefixLength_1() { return &___prefixLength_1; }
	inline void set_prefixLength_1(int32_t value)
	{
		___prefixLength_1 = value;
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(IPv6Address_t496378312, ___scopeId_2)); }
	inline int64_t get_scopeId_2() const { return ___scopeId_2; }
	inline int64_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int64_t value)
	{
		___scopeId_2 = value;
	}
};

struct IPv6Address_t496378312_StaticFields
{
public:
	// System.Net.IPv6Address System.Net.IPv6Address::Loopback
	IPv6Address_t496378312 * ___Loopback_3;
	// System.Net.IPv6Address System.Net.IPv6Address::Unspecified
	IPv6Address_t496378312 * ___Unspecified_4;

public:
	inline static int32_t get_offset_of_Loopback_3() { return static_cast<int32_t>(offsetof(IPv6Address_t496378312_StaticFields, ___Loopback_3)); }
	inline IPv6Address_t496378312 * get_Loopback_3() const { return ___Loopback_3; }
	inline IPv6Address_t496378312 ** get_address_of_Loopback_3() { return &___Loopback_3; }
	inline void set_Loopback_3(IPv6Address_t496378312 * value)
	{
		___Loopback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_3), value);
	}

	inline static int32_t get_offset_of_Unspecified_4() { return static_cast<int32_t>(offsetof(IPv6Address_t496378312_StaticFields, ___Unspecified_4)); }
	inline IPv6Address_t496378312 * get_Unspecified_4() const { return ___Unspecified_4; }
	inline IPv6Address_t496378312 ** get_address_of_Unspecified_4() { return &___Unspecified_4; }
	inline void set_Unspecified_4(IPv6Address_t496378312 * value)
	{
		___Unspecified_4 = value;
		Il2CppCodeGenWriteBarrier((&___Unspecified_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESS_T496378312_H
#ifndef NETWORKCREDENTIAL_T1328334253_H
#define NETWORKCREDENTIAL_T1328334253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkCredential
struct  NetworkCredential_t1328334253  : public RuntimeObject
{
public:
	// System.String System.Net.NetworkCredential::userName
	String_t* ___userName_0;
	// System.String System.Net.NetworkCredential::password
	String_t* ___password_1;
	// System.String System.Net.NetworkCredential::domain
	String_t* ___domain_2;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t1328334253, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t1328334253, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_domain_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t1328334253, ___domain_2)); }
	inline String_t* get_domain_2() const { return ___domain_2; }
	inline String_t** get_address_of_domain_2() { return &___domain_2; }
	inline void set_domain_2(String_t* value)
	{
		___domain_2 = value;
		Il2CppCodeGenWriteBarrier((&___domain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T1328334253_H
#ifndef SPKEY_T742829915_H
#define SPKEY_T742829915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/SPKey
struct  SPKey_t742829915  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePointManager/SPKey::uri
	Uri_t3269222095 * ___uri_0;
	// System.Boolean System.Net.ServicePointManager/SPKey::use_connect
	bool ___use_connect_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(SPKey_t742829915, ___uri_0)); }
	inline Uri_t3269222095 * get_uri_0() const { return ___uri_0; }
	inline Uri_t3269222095 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t3269222095 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_use_connect_1() { return static_cast<int32_t>(offsetof(SPKey_t742829915, ___use_connect_1)); }
	inline bool get_use_connect_1() const { return ___use_connect_1; }
	inline bool* get_address_of_use_connect_1() { return &___use_connect_1; }
	inline void set_use_connect_1(bool value)
	{
		___use_connect_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPKEY_T742829915_H
#ifndef SOCKETADDRESS_T149599595_H
#define SOCKETADDRESS_T149599595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_t149599595  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.SocketAddress::data
	ByteU5BU5D_t434619169* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(SocketAddress_t149599595, ___data_0)); }
	inline ByteU5BU5D_t434619169* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t434619169** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t434619169* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_T149599595_H
#ifndef WEBASYNCRESULT_T1279310773_H
#define WEBASYNCRESULT_T1279310773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebAsyncResult
struct  WebAsyncResult_t1279310773  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.WebAsyncResult::handle
	ManualResetEvent_t504895258 * ___handle_0;
	// System.Boolean System.Net.WebAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.WebAsyncResult::isCompleted
	bool ___isCompleted_2;
	// System.AsyncCallback System.Net.WebAsyncResult::cb
	AsyncCallback_t3561663063 * ___cb_3;
	// System.Object System.Net.WebAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Int32 System.Net.WebAsyncResult::nbytes
	int32_t ___nbytes_5;
	// System.IAsyncResult System.Net.WebAsyncResult::innerAsyncResult
	RuntimeObject* ___innerAsyncResult_6;
	// System.Boolean System.Net.WebAsyncResult::callbackDone
	bool ___callbackDone_7;
	// System.Exception System.Net.WebAsyncResult::exc
	Exception_t4086964929 * ___exc_8;
	// System.Net.HttpWebResponse System.Net.WebAsyncResult::response
	HttpWebResponse_t2352629529 * ___response_9;
	// System.IO.Stream System.Net.WebAsyncResult::writeStream
	Stream_t4138477179 * ___writeStream_10;
	// System.Byte[] System.Net.WebAsyncResult::buffer
	ByteU5BU5D_t434619169* ___buffer_11;
	// System.Int32 System.Net.WebAsyncResult::offset
	int32_t ___offset_12;
	// System.Int32 System.Net.WebAsyncResult::size
	int32_t ___size_13;
	// System.Object System.Net.WebAsyncResult::locker
	RuntimeObject * ___locker_14;
	// System.Boolean System.Net.WebAsyncResult::EndCalled
	bool ___EndCalled_15;
	// System.Boolean System.Net.WebAsyncResult::AsyncWriteAll
	bool ___AsyncWriteAll_16;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___handle_0)); }
	inline ManualResetEvent_t504895258 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_t504895258 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_t504895258 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___cb_3)); }
	inline AsyncCallback_t3561663063 * get_cb_3() const { return ___cb_3; }
	inline AsyncCallback_t3561663063 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(AsyncCallback_t3561663063 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_nbytes_5() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___nbytes_5)); }
	inline int32_t get_nbytes_5() const { return ___nbytes_5; }
	inline int32_t* get_address_of_nbytes_5() { return &___nbytes_5; }
	inline void set_nbytes_5(int32_t value)
	{
		___nbytes_5 = value;
	}

	inline static int32_t get_offset_of_innerAsyncResult_6() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___innerAsyncResult_6)); }
	inline RuntimeObject* get_innerAsyncResult_6() const { return ___innerAsyncResult_6; }
	inline RuntimeObject** get_address_of_innerAsyncResult_6() { return &___innerAsyncResult_6; }
	inline void set_innerAsyncResult_6(RuntimeObject* value)
	{
		___innerAsyncResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___innerAsyncResult_6), value);
	}

	inline static int32_t get_offset_of_callbackDone_7() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___callbackDone_7)); }
	inline bool get_callbackDone_7() const { return ___callbackDone_7; }
	inline bool* get_address_of_callbackDone_7() { return &___callbackDone_7; }
	inline void set_callbackDone_7(bool value)
	{
		___callbackDone_7 = value;
	}

	inline static int32_t get_offset_of_exc_8() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___exc_8)); }
	inline Exception_t4086964929 * get_exc_8() const { return ___exc_8; }
	inline Exception_t4086964929 ** get_address_of_exc_8() { return &___exc_8; }
	inline void set_exc_8(Exception_t4086964929 * value)
	{
		___exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___exc_8), value);
	}

	inline static int32_t get_offset_of_response_9() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___response_9)); }
	inline HttpWebResponse_t2352629529 * get_response_9() const { return ___response_9; }
	inline HttpWebResponse_t2352629529 ** get_address_of_response_9() { return &___response_9; }
	inline void set_response_9(HttpWebResponse_t2352629529 * value)
	{
		___response_9 = value;
		Il2CppCodeGenWriteBarrier((&___response_9), value);
	}

	inline static int32_t get_offset_of_writeStream_10() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___writeStream_10)); }
	inline Stream_t4138477179 * get_writeStream_10() const { return ___writeStream_10; }
	inline Stream_t4138477179 ** get_address_of_writeStream_10() { return &___writeStream_10; }
	inline void set_writeStream_10(Stream_t4138477179 * value)
	{
		___writeStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_10), value);
	}

	inline static int32_t get_offset_of_buffer_11() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___buffer_11)); }
	inline ByteU5BU5D_t434619169* get_buffer_11() const { return ___buffer_11; }
	inline ByteU5BU5D_t434619169** get_address_of_buffer_11() { return &___buffer_11; }
	inline void set_buffer_11(ByteU5BU5D_t434619169* value)
	{
		___buffer_11 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_11), value);
	}

	inline static int32_t get_offset_of_offset_12() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___offset_12)); }
	inline int32_t get_offset_12() const { return ___offset_12; }
	inline int32_t* get_address_of_offset_12() { return &___offset_12; }
	inline void set_offset_12(int32_t value)
	{
		___offset_12 = value;
	}

	inline static int32_t get_offset_of_size_13() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___size_13)); }
	inline int32_t get_size_13() const { return ___size_13; }
	inline int32_t* get_address_of_size_13() { return &___size_13; }
	inline void set_size_13(int32_t value)
	{
		___size_13 = value;
	}

	inline static int32_t get_offset_of_locker_14() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___locker_14)); }
	inline RuntimeObject * get_locker_14() const { return ___locker_14; }
	inline RuntimeObject ** get_address_of_locker_14() { return &___locker_14; }
	inline void set_locker_14(RuntimeObject * value)
	{
		___locker_14 = value;
		Il2CppCodeGenWriteBarrier((&___locker_14), value);
	}

	inline static int32_t get_offset_of_EndCalled_15() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___EndCalled_15)); }
	inline bool get_EndCalled_15() const { return ___EndCalled_15; }
	inline bool* get_address_of_EndCalled_15() { return &___EndCalled_15; }
	inline void set_EndCalled_15(bool value)
	{
		___EndCalled_15 = value;
	}

	inline static int32_t get_offset_of_AsyncWriteAll_16() { return static_cast<int32_t>(offsetof(WebAsyncResult_t1279310773, ___AsyncWriteAll_16)); }
	inline bool get_AsyncWriteAll_16() const { return ___AsyncWriteAll_16; }
	inline bool* get_address_of_AsyncWriteAll_16() { return &___AsyncWriteAll_16; }
	inline void set_AsyncWriteAll_16(bool value)
	{
		___AsyncWriteAll_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBASYNCRESULT_T1279310773_H
#ifndef ABORTHELPER_T2979836766_H
#define ABORTHELPER_T2979836766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection/AbortHelper
struct  AbortHelper_t2979836766  : public RuntimeObject
{
public:
	// System.Net.WebConnection System.Net.WebConnection/AbortHelper::Connection
	WebConnection_t3591103104 * ___Connection_0;

public:
	inline static int32_t get_offset_of_Connection_0() { return static_cast<int32_t>(offsetof(AbortHelper_t2979836766, ___Connection_0)); }
	inline WebConnection_t3591103104 * get_Connection_0() const { return ___Connection_0; }
	inline WebConnection_t3591103104 ** get_address_of_Connection_0() { return &___Connection_0; }
	inline void set_Connection_0(WebConnection_t3591103104 * value)
	{
		___Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABORTHELPER_T2979836766_H
#ifndef WEBCONNECTIONDATA_T1471135888_H
#define WEBCONNECTIONDATA_T1471135888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionData
struct  WebConnectionData_t1471135888  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest System.Net.WebConnectionData::request
	HttpWebRequest_t4046343009 * ___request_0;
	// System.Int32 System.Net.WebConnectionData::StatusCode
	int32_t ___StatusCode_1;
	// System.String System.Net.WebConnectionData::StatusDescription
	String_t* ___StatusDescription_2;
	// System.Net.WebHeaderCollection System.Net.WebConnectionData::Headers
	WebHeaderCollection_t592894254 * ___Headers_3;
	// System.Version System.Net.WebConnectionData::Version
	Version_t2250829542 * ___Version_4;
	// System.IO.Stream System.Net.WebConnectionData::stream
	Stream_t4138477179 * ___stream_5;
	// System.String System.Net.WebConnectionData::Challenge
	String_t* ___Challenge_6;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___request_0)); }
	inline HttpWebRequest_t4046343009 * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t4046343009 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t4046343009 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_StatusCode_1() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___StatusCode_1)); }
	inline int32_t get_StatusCode_1() const { return ___StatusCode_1; }
	inline int32_t* get_address_of_StatusCode_1() { return &___StatusCode_1; }
	inline void set_StatusCode_1(int32_t value)
	{
		___StatusCode_1 = value;
	}

	inline static int32_t get_offset_of_StatusDescription_2() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___StatusDescription_2)); }
	inline String_t* get_StatusDescription_2() const { return ___StatusDescription_2; }
	inline String_t** get_address_of_StatusDescription_2() { return &___StatusDescription_2; }
	inline void set_StatusDescription_2(String_t* value)
	{
		___StatusDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___StatusDescription_2), value);
	}

	inline static int32_t get_offset_of_Headers_3() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___Headers_3)); }
	inline WebHeaderCollection_t592894254 * get_Headers_3() const { return ___Headers_3; }
	inline WebHeaderCollection_t592894254 ** get_address_of_Headers_3() { return &___Headers_3; }
	inline void set_Headers_3(WebHeaderCollection_t592894254 * value)
	{
		___Headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Headers_3), value);
	}

	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___Version_4)); }
	inline Version_t2250829542 * get_Version_4() const { return ___Version_4; }
	inline Version_t2250829542 ** get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(Version_t2250829542 * value)
	{
		___Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___Version_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___stream_5)); }
	inline Stream_t4138477179 * get_stream_5() const { return ___stream_5; }
	inline Stream_t4138477179 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t4138477179 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_Challenge_6() { return static_cast<int32_t>(offsetof(WebConnectionData_t1471135888, ___Challenge_6)); }
	inline String_t* get_Challenge_6() const { return ___Challenge_6; }
	inline String_t** get_address_of_Challenge_6() { return &___Challenge_6; }
	inline void set_Challenge_6(String_t* value)
	{
		___Challenge_6 = value;
		Il2CppCodeGenWriteBarrier((&___Challenge_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONDATA_T1471135888_H
#ifndef WEBCONNECTIONGROUP_T1241471072_H
#define WEBCONNECTIONGROUP_T1241471072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup
struct  WebConnectionGroup_t1241471072  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnectionGroup::sPoint
	ServicePoint_t3505702983 * ___sPoint_0;
	// System.String System.Net.WebConnectionGroup::name
	String_t* ___name_1;
	// System.Collections.ArrayList System.Net.WebConnectionGroup::connections
	ArrayList_t4250946984 * ___connections_2;
	// System.Random System.Net.WebConnectionGroup::rnd
	Random_t562893486 * ___rnd_3;
	// System.Collections.Queue System.Net.WebConnectionGroup::queue
	Queue_t2654900338 * ___queue_4;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1241471072, ___sPoint_0)); }
	inline ServicePoint_t3505702983 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t3505702983 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t3505702983 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1241471072, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1241471072, ___connections_2)); }
	inline ArrayList_t4250946984 * get_connections_2() const { return ___connections_2; }
	inline ArrayList_t4250946984 ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(ArrayList_t4250946984 * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier((&___connections_2), value);
	}

	inline static int32_t get_offset_of_rnd_3() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1241471072, ___rnd_3)); }
	inline Random_t562893486 * get_rnd_3() const { return ___rnd_3; }
	inline Random_t562893486 ** get_address_of_rnd_3() { return &___rnd_3; }
	inline void set_rnd_3(Random_t562893486 * value)
	{
		___rnd_3 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_3), value);
	}

	inline static int32_t get_offset_of_queue_4() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1241471072, ___queue_4)); }
	inline Queue_t2654900338 * get_queue_4() const { return ___queue_4; }
	inline Queue_t2654900338 ** get_address_of_queue_4() { return &___queue_4; }
	inline void set_queue_4(Queue_t2654900338 * value)
	{
		___queue_4 = value;
		Il2CppCodeGenWriteBarrier((&___queue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONGROUP_T1241471072_H
#ifndef WEBPROXY_T1068617986_H
#define WEBPROXY_T1068617986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t1068617986  : public RuntimeObject
{
public:
	// System.Uri System.Net.WebProxy::address
	Uri_t3269222095 * ___address_0;
	// System.Boolean System.Net.WebProxy::bypassOnLocal
	bool ___bypassOnLocal_1;
	// System.Collections.ArrayList System.Net.WebProxy::bypassList
	ArrayList_t4250946984 * ___bypassList_2;
	// System.Net.ICredentials System.Net.WebProxy::credentials
	RuntimeObject* ___credentials_3;
	// System.Boolean System.Net.WebProxy::useDefaultCredentials
	bool ___useDefaultCredentials_4;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(WebProxy_t1068617986, ___address_0)); }
	inline Uri_t3269222095 * get_address_0() const { return ___address_0; }
	inline Uri_t3269222095 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(Uri_t3269222095 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_bypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t1068617986, ___bypassOnLocal_1)); }
	inline bool get_bypassOnLocal_1() const { return ___bypassOnLocal_1; }
	inline bool* get_address_of_bypassOnLocal_1() { return &___bypassOnLocal_1; }
	inline void set_bypassOnLocal_1(bool value)
	{
		___bypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_bypassList_2() { return static_cast<int32_t>(offsetof(WebProxy_t1068617986, ___bypassList_2)); }
	inline ArrayList_t4250946984 * get_bypassList_2() const { return ___bypassList_2; }
	inline ArrayList_t4250946984 ** get_address_of_bypassList_2() { return &___bypassList_2; }
	inline void set_bypassList_2(ArrayList_t4250946984 * value)
	{
		___bypassList_2 = value;
		Il2CppCodeGenWriteBarrier((&___bypassList_2), value);
	}

	inline static int32_t get_offset_of_credentials_3() { return static_cast<int32_t>(offsetof(WebProxy_t1068617986, ___credentials_3)); }
	inline RuntimeObject* get_credentials_3() const { return ___credentials_3; }
	inline RuntimeObject** get_address_of_credentials_3() { return &___credentials_3; }
	inline void set_credentials_3(RuntimeObject* value)
	{
		___credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_3), value);
	}

	inline static int32_t get_offset_of_useDefaultCredentials_4() { return static_cast<int32_t>(offsetof(WebProxy_t1068617986, ___useDefaultCredentials_4)); }
	inline bool get_useDefaultCredentials_4() const { return ___useDefaultCredentials_4; }
	inline bool* get_address_of_useDefaultCredentials_4() { return &___useDefaultCredentials_4; }
	inline void set_useDefaultCredentials_4(bool value)
	{
		___useDefaultCredentials_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXY_T1068617986_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2211885611_H
#define NAMEOBJECTCOLLECTIONBASE_T2211885611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2211885611  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t2354558714 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t1895646286 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t4250946984 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t259346668 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t1720530229 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_ItemsContainer_0)); }
	inline Hashtable_t2354558714 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t2354558714 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t2354558714 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_NullKeyItem_1)); }
	inline _Item_t1895646286 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t1895646286 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t1895646286 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_ItemsArray_2)); }
	inline ArrayList_t4250946984 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t4250946984 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t4250946984 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___infoCopy_7)); }
	inline SerializationInfo_t259346668 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t259346668 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t259346668 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___keyscoll_8)); }
	inline KeysCollection_t1720530229 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t1720530229 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t1720530229 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2211885611, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2211885611_H
#ifndef PUBLICKEY_T3840515815_H
#define PUBLICKEY_T3840515815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.PublicKey
struct  PublicKey_t3840515815  : public RuntimeObject
{
public:
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::_key
	AsymmetricAlgorithm_t4165028003 * ____key_0;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_keyValue
	AsnEncodedData_t3005941167 * ____keyValue_1;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_params
	AsnEncodedData_t3005941167 * ____params_2;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::_oid
	Oid_t2612574631 * ____oid_3;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(PublicKey_t3840515815, ____key_0)); }
	inline AsymmetricAlgorithm_t4165028003 * get__key_0() const { return ____key_0; }
	inline AsymmetricAlgorithm_t4165028003 ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(AsymmetricAlgorithm_t4165028003 * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__keyValue_1() { return static_cast<int32_t>(offsetof(PublicKey_t3840515815, ____keyValue_1)); }
	inline AsnEncodedData_t3005941167 * get__keyValue_1() const { return ____keyValue_1; }
	inline AsnEncodedData_t3005941167 ** get_address_of__keyValue_1() { return &____keyValue_1; }
	inline void set__keyValue_1(AsnEncodedData_t3005941167 * value)
	{
		____keyValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyValue_1), value);
	}

	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(PublicKey_t3840515815, ____params_2)); }
	inline AsnEncodedData_t3005941167 * get__params_2() const { return ____params_2; }
	inline AsnEncodedData_t3005941167 ** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(AsnEncodedData_t3005941167 * value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}

	inline static int32_t get_offset_of__oid_3() { return static_cast<int32_t>(offsetof(PublicKey_t3840515815, ____oid_3)); }
	inline Oid_t2612574631 * get__oid_3() const { return ____oid_3; }
	inline Oid_t2612574631 ** get_address_of__oid_3() { return &____oid_3; }
	inline void set__oid_3(Oid_t2612574631 * value)
	{
		____oid_3 = value;
		Il2CppCodeGenWriteBarrier((&____oid_3), value);
	}
};

struct PublicKey_t3840515815_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.PublicKey::<>f__switch$map9
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map9_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_4() { return static_cast<int32_t>(offsetof(PublicKey_t3840515815_StaticFields, ___U3CU3Ef__switchU24map9_4)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map9_4() const { return ___U3CU3Ef__switchU24map9_4; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map9_4() { return &___U3CU3Ef__switchU24map9_4; }
	inline void set_U3CU3Ef__switchU24map9_4(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map9_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEY_T3840515815_H
#ifndef X509CERTIFICATE2ENUMERATOR_T4028473944_H
#define X509CERTIFICATE2ENUMERATOR_T4028473944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct  X509Certificate2Enumerator_t4028473944  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509Certificate2Enumerator_t4028473944, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2ENUMERATOR_T4028473944_H
#ifndef X509CERTIFICATEENUMERATOR_T3394667710_H
#define X509CERTIFICATEENUMERATOR_T3394667710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_t3394667710  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_t3394667710, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_T3394667710_H
#ifndef X509CHAINELEMENTCOLLECTION_T2987578632_H
#define X509CHAINELEMENTCOLLECTION_T2987578632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct  X509ChainElementCollection_t2987578632  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ChainElementCollection::_list
	ArrayList_t4250946984 * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ChainElementCollection_t2987578632, ____list_0)); }
	inline ArrayList_t4250946984 * get__list_0() const { return ____list_0; }
	inline ArrayList_t4250946984 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t4250946984 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTCOLLECTION_T2987578632_H
#ifndef X509CHAINELEMENTENUMERATOR_T1429529268_H
#define X509CHAINELEMENTENUMERATOR_T1429529268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct  X509ChainElementEnumerator_t1429529268  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ChainElementEnumerator_t1429529268, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTENUMERATOR_T1429529268_H
#ifndef X509EXTENSIONCOLLECTION_T3160461637_H
#define X509EXTENSIONCOLLECTION_T3160461637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_t3160461637  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t4250946984 * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t3160461637, ____list_0)); }
	inline ArrayList_t4250946984 * get__list_0() const { return ____list_0; }
	inline ArrayList_t4250946984 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t4250946984 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T3160461637_H
#ifndef X509EXTENSIONENUMERATOR_T2350753659_H
#define X509EXTENSIONENUMERATOR_T2350753659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
struct  X509ExtensionEnumerator_t2350753659  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ExtensionEnumerator_t2350753659, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONENUMERATOR_T2350753659_H
#ifndef ASNENCODEDDATA_T3005941167_H
#define ASNENCODEDDATA_T3005941167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t3005941167  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t2612574631 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t434619169* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t3005941167, ____oid_0)); }
	inline Oid_t2612574631 * get__oid_0() const { return ____oid_0; }
	inline Oid_t2612574631 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_t2612574631 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t3005941167, ____raw_1)); }
	inline ByteU5BU5D_t434619169* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_t434619169** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_t434619169* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

struct AsnEncodedData_t3005941167_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapA_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_2() { return static_cast<int32_t>(offsetof(AsnEncodedData_t3005941167_StaticFields, ___U3CU3Ef__switchU24mapA_2)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapA_2() const { return ___U3CU3Ef__switchU24mapA_2; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapA_2() { return &___U3CU3Ef__switchU24mapA_2; }
	inline void set_U3CU3Ef__switchU24mapA_2(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapA_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T3005941167_H
#ifndef STREAM_T4138477179_H
#define STREAM_T4138477179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t4138477179  : public RuntimeObject
{
public:

public:
};

struct Stream_t4138477179_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t4138477179 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t4138477179_StaticFields, ___Null_0)); }
	inline Stream_t4138477179 * get_Null_0() const { return ___Null_0; }
	inline Stream_t4138477179 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t4138477179 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T4138477179_H
#ifndef MARSHALBYREFOBJECT_T1482034164_H
#define MARSHALBYREFOBJECT_T1482034164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1482034164  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t3687691741 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1482034164, ____identity_0)); }
	inline ServerIdentity_t3687691741 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t3687691741 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t3687691741 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T1482034164_H
#ifndef X509CERTIFICATE_T2642281915_H
#define X509CERTIFICATE_T2642281915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_t2642281915  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::x509
	X509Certificate_t3432709581 * ___x509_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::cachedCertificateHash
	ByteU5BU5D_t434619169* ___cachedCertificateHash_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_3;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_4;

public:
	inline static int32_t get_offset_of_x509_0() { return static_cast<int32_t>(offsetof(X509Certificate_t2642281915, ___x509_0)); }
	inline X509Certificate_t3432709581 * get_x509_0() const { return ___x509_0; }
	inline X509Certificate_t3432709581 ** get_address_of_x509_0() { return &___x509_0; }
	inline void set_x509_0(X509Certificate_t3432709581 * value)
	{
		___x509_0 = value;
		Il2CppCodeGenWriteBarrier((&___x509_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_t2642281915, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_cachedCertificateHash_2() { return static_cast<int32_t>(offsetof(X509Certificate_t2642281915, ___cachedCertificateHash_2)); }
	inline ByteU5BU5D_t434619169* get_cachedCertificateHash_2() const { return ___cachedCertificateHash_2; }
	inline ByteU5BU5D_t434619169** get_address_of_cachedCertificateHash_2() { return &___cachedCertificateHash_2; }
	inline void set_cachedCertificateHash_2(ByteU5BU5D_t434619169* value)
	{
		___cachedCertificateHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCertificateHash_2), value);
	}

	inline static int32_t get_offset_of_issuer_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_t2642281915, ___issuer_name_3)); }
	inline String_t* get_issuer_name_3() const { return ___issuer_name_3; }
	inline String_t** get_address_of_issuer_name_3() { return &___issuer_name_3; }
	inline void set_issuer_name_3(String_t* value)
	{
		___issuer_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_3), value);
	}

	inline static int32_t get_offset_of_subject_name_4() { return static_cast<int32_t>(offsetof(X509Certificate_t2642281915, ___subject_name_4)); }
	inline String_t* get_subject_name_4() const { return ___subject_name_4; }
	inline String_t** get_address_of_subject_name_4() { return &___subject_name_4; }
	inline void set_subject_name_4(String_t* value)
	{
		___subject_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T2642281915_H
#ifndef COLLECTIONBASE_T1611667396_H
#define COLLECTIONBASE_T1611667396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t1611667396  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4250946984 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t1611667396, ___list_0)); }
	inline ArrayList_t4250946984 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4250946984 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4250946984 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T1611667396_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef IPHOSTENTRY_T4214309905_H
#define IPHOSTENTRY_T4214309905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPHostEntry
struct  IPHostEntry_t4214309905  : public RuntimeObject
{
public:
	// System.Net.IPAddress[] System.Net.IPHostEntry::addressList
	IPAddressU5BU5D_t2543835050* ___addressList_0;
	// System.String[] System.Net.IPHostEntry::aliases
	StringU5BU5D_t2511808107* ___aliases_1;
	// System.String System.Net.IPHostEntry::hostName
	String_t* ___hostName_2;

public:
	inline static int32_t get_offset_of_addressList_0() { return static_cast<int32_t>(offsetof(IPHostEntry_t4214309905, ___addressList_0)); }
	inline IPAddressU5BU5D_t2543835050* get_addressList_0() const { return ___addressList_0; }
	inline IPAddressU5BU5D_t2543835050** get_address_of_addressList_0() { return &___addressList_0; }
	inline void set_addressList_0(IPAddressU5BU5D_t2543835050* value)
	{
		___addressList_0 = value;
		Il2CppCodeGenWriteBarrier((&___addressList_0), value);
	}

	inline static int32_t get_offset_of_aliases_1() { return static_cast<int32_t>(offsetof(IPHostEntry_t4214309905, ___aliases_1)); }
	inline StringU5BU5D_t2511808107* get_aliases_1() const { return ___aliases_1; }
	inline StringU5BU5D_t2511808107** get_address_of_aliases_1() { return &___aliases_1; }
	inline void set_aliases_1(StringU5BU5D_t2511808107* value)
	{
		___aliases_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliases_1), value);
	}

	inline static int32_t get_offset_of_hostName_2() { return static_cast<int32_t>(offsetof(IPHostEntry_t4214309905, ___hostName_2)); }
	inline String_t* get_hostName_2() const { return ___hostName_2; }
	inline String_t** get_address_of_hostName_2() { return &___hostName_2; }
	inline void set_hostName_2(String_t* value)
	{
		___hostName_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPHOSTENTRY_T4214309905_H
#ifndef COOKIEPARSER_T3149482976_H
#define COOKIEPARSER_T3149482976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieParser
struct  CookieParser_t3149482976  : public RuntimeObject
{
public:
	// System.String System.Net.CookieParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.CookieParser::pos
	int32_t ___pos_1;
	// System.Int32 System.Net.CookieParser::length
	int32_t ___length_2;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(CookieParser_t3149482976, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(CookieParser_t3149482976, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(CookieParser_t3149482976, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEPARSER_T3149482976_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef DIGESTHEADERPARSER_T477534254_H
#define DIGESTHEADERPARSER_T477534254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestHeaderParser
struct  DigestHeaderParser_t477534254  : public RuntimeObject
{
public:
	// System.String System.Net.DigestHeaderParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.DigestHeaderParser::length
	int32_t ___length_1;
	// System.Int32 System.Net.DigestHeaderParser::pos
	int32_t ___pos_2;
	// System.String[] System.Net.DigestHeaderParser::values
	StringU5BU5D_t2511808107* ___values_4;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t477534254, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t477534254, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t477534254, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t477534254, ___values_4)); }
	inline StringU5BU5D_t2511808107* get_values_4() const { return ___values_4; }
	inline StringU5BU5D_t2511808107** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StringU5BU5D_t2511808107* value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier((&___values_4), value);
	}
};

struct DigestHeaderParser_t477534254_StaticFields
{
public:
	// System.String[] System.Net.DigestHeaderParser::keywords
	StringU5BU5D_t2511808107* ___keywords_3;

public:
	inline static int32_t get_offset_of_keywords_3() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t477534254_StaticFields, ___keywords_3)); }
	inline StringU5BU5D_t2511808107* get_keywords_3() const { return ___keywords_3; }
	inline StringU5BU5D_t2511808107** get_address_of_keywords_3() { return &___keywords_3; }
	inline void set_keywords_3(StringU5BU5D_t2511808107* value)
	{
		___keywords_3 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTHEADERPARSER_T477534254_H
#ifndef ENDPOINT_T2958140078_H
#define ENDPOINT_T2958140078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t2958140078  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T2958140078_H
#ifndef DNS_T633402713_H
#define DNS_T633402713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns
struct  Dns_t633402713  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNS_T633402713_H
#ifndef DIGESTCLIENT_T4096343030_H
#define DIGESTCLIENT_T4096343030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestClient
struct  DigestClient_t4096343030  : public RuntimeObject
{
public:

public:
};

struct DigestClient_t4096343030_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.DigestClient::cache
	Hashtable_t2354558714 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(DigestClient_t4096343030_StaticFields, ___cache_0)); }
	inline Hashtable_t2354558714 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t2354558714 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t2354558714 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTCLIENT_T4096343030_H
#ifndef FTPASYNCRESULT_T3500703300_H
#define FTPASYNCRESULT_T3500703300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpAsyncResult
struct  FtpAsyncResult_t3500703300  : public RuntimeObject
{
public:
	// System.Net.FtpWebResponse System.Net.FtpAsyncResult::response
	FtpWebResponse_t1950878246 * ___response_0;
	// System.Threading.ManualResetEvent System.Net.FtpAsyncResult::waitHandle
	ManualResetEvent_t504895258 * ___waitHandle_1;
	// System.Exception System.Net.FtpAsyncResult::exception
	Exception_t4086964929 * ___exception_2;
	// System.AsyncCallback System.Net.FtpAsyncResult::callback
	AsyncCallback_t3561663063 * ___callback_3;
	// System.IO.Stream System.Net.FtpAsyncResult::stream
	Stream_t4138477179 * ___stream_4;
	// System.Object System.Net.FtpAsyncResult::state
	RuntimeObject * ___state_5;
	// System.Boolean System.Net.FtpAsyncResult::completed
	bool ___completed_6;
	// System.Boolean System.Net.FtpAsyncResult::synch
	bool ___synch_7;
	// System.Object System.Net.FtpAsyncResult::locker
	RuntimeObject * ___locker_8;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___response_0)); }
	inline FtpWebResponse_t1950878246 * get_response_0() const { return ___response_0; }
	inline FtpWebResponse_t1950878246 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(FtpWebResponse_t1950878246 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_waitHandle_1() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___waitHandle_1)); }
	inline ManualResetEvent_t504895258 * get_waitHandle_1() const { return ___waitHandle_1; }
	inline ManualResetEvent_t504895258 ** get_address_of_waitHandle_1() { return &___waitHandle_1; }
	inline void set_waitHandle_1(ManualResetEvent_t504895258 * value)
	{
		___waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&___waitHandle_1), value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___exception_2)); }
	inline Exception_t4086964929 * get_exception_2() const { return ___exception_2; }
	inline Exception_t4086964929 ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t4086964929 * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___exception_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___callback_3)); }
	inline AsyncCallback_t3561663063 * get_callback_3() const { return ___callback_3; }
	inline AsyncCallback_t3561663063 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(AsyncCallback_t3561663063 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___stream_4)); }
	inline Stream_t4138477179 * get_stream_4() const { return ___stream_4; }
	inline Stream_t4138477179 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_t4138477179 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___state_5)); }
	inline RuntimeObject * get_state_5() const { return ___state_5; }
	inline RuntimeObject ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(RuntimeObject * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}

	inline static int32_t get_offset_of_synch_7() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___synch_7)); }
	inline bool get_synch_7() const { return ___synch_7; }
	inline bool* get_address_of_synch_7() { return &___synch_7; }
	inline void set_synch_7(bool value)
	{
		___synch_7 = value;
	}

	inline static int32_t get_offset_of_locker_8() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3500703300, ___locker_8)); }
	inline RuntimeObject * get_locker_8() const { return ___locker_8; }
	inline RuntimeObject ** get_address_of_locker_8() { return &___locker_8; }
	inline void set_locker_8(RuntimeObject * value)
	{
		___locker_8 = value;
		Il2CppCodeGenWriteBarrier((&___locker_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPASYNCRESULT_T3500703300_H
#ifndef DEFAULTCERTIFICATEPOLICY_T1704739774_H
#define DEFAULTCERTIFICATEPOLICY_T1704739774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DefaultCertificatePolicy
struct  DefaultCertificatePolicy_t1704739774  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCERTIFICATEPOLICY_T1704739774_H
#ifndef FTPREQUESTCREATOR_T508984423_H
#define FTPREQUESTCREATOR_T508984423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpRequestCreator
struct  FtpRequestCreator_t508984423  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPREQUESTCREATOR_T508984423_H
#ifndef HTTPREQUESTCREATOR_T2802833056_H
#define HTTPREQUESTCREATOR_T2802833056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpRequestCreator
struct  HttpRequestCreator_t2802833056  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTCREATOR_T2802833056_H
#ifndef HTTPVERSION_T2198520130_H
#define HTTPVERSION_T2198520130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpVersion
struct  HttpVersion_t2198520130  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t2198520130_StaticFields
{
public:
	// System.Version System.Net.HttpVersion::Version10
	Version_t2250829542 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t2250829542 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t2198520130_StaticFields, ___Version10_0)); }
	inline Version_t2250829542 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t2250829542 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t2250829542 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t2198520130_StaticFields, ___Version11_1)); }
	inline Version_t2250829542 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t2250829542 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t2250829542 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T2198520130_H
#ifndef FILEWEBREQUESTCREATOR_T2285499290_H
#define FILEWEBREQUESTCREATOR_T2285499290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequestCreator
struct  FileWebRequestCreator_t2285499290  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUESTCREATOR_T2285499290_H
#ifndef GLOBALPROXYSELECTION_T3081702395_H
#define GLOBALPROXYSELECTION_T3081702395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalProxySelection
struct  GlobalProxySelection_t3081702395  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALPROXYSELECTION_T3081702395_H
#ifndef X500DISTINGUISHEDNAME_T572620573_H
#define X500DISTINGUISHEDNAME_T572620573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct  X500DistinguishedName_t572620573  : public AsnEncodedData_t3005941167
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t572620573, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAME_T572620573_H
#ifndef NAMEVALUECOLLECTION_T1739534792_H
#define NAMEVALUECOLLECTION_T1739534792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t1739534792  : public NameObjectCollectionBase_t2211885611
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t2511808107* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t2511808107* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t1739534792, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t2511808107* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t2511808107** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t2511808107* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t1739534792, ___cachedAll_11)); }
	inline StringU5BU5D_t2511808107* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t2511808107** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t2511808107* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T1739534792_H
#ifndef X509EXTENSION_T1338557205_H
#define X509EXTENSION_T1338557205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Extension
struct  X509Extension_t1338557205  : public AsnEncodedData_t3005941167
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::_critical
	bool ____critical_3;

public:
	inline static int32_t get_offset_of__critical_3() { return static_cast<int32_t>(offsetof(X509Extension_t1338557205, ____critical_3)); }
	inline bool get__critical_3() const { return ____critical_3; }
	inline bool* get_address_of__critical_3() { return &____critical_3; }
	inline void set__critical_3(bool value)
	{
		____critical_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T1338557205_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef WEBRESPONSE_T3616229315_H
#define WEBRESPONSE_T3616229315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t3616229315  : public MarshalByRefObject_t1482034164
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T3616229315_H
#ifndef X509CERTIFICATE2_T1403970469_H
#define X509CERTIFICATE2_T1403970469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct  X509Certificate2_t1403970469  : public X509Certificate_t2642281915
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2::_archived
	bool ____archived_5;
	// System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2::_extensions
	X509ExtensionCollection_t3160461637 * ____extensions_6;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_name
	String_t* ____name_7;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_serial
	String_t* ____serial_8;
	// System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2::_publicKey
	PublicKey_t3840515815 * ____publicKey_9;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::issuer_name
	X500DistinguishedName_t572620573 * ___issuer_name_10;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::subject_name
	X500DistinguishedName_t572620573 * ___subject_name_11;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2::signature_algorithm
	Oid_t2612574631 * ___signature_algorithm_12;
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2::_cert
	X509Certificate_t3432709582 * ____cert_13;

public:
	inline static int32_t get_offset_of__archived_5() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____archived_5)); }
	inline bool get__archived_5() const { return ____archived_5; }
	inline bool* get_address_of__archived_5() { return &____archived_5; }
	inline void set__archived_5(bool value)
	{
		____archived_5 = value;
	}

	inline static int32_t get_offset_of__extensions_6() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____extensions_6)); }
	inline X509ExtensionCollection_t3160461637 * get__extensions_6() const { return ____extensions_6; }
	inline X509ExtensionCollection_t3160461637 ** get_address_of__extensions_6() { return &____extensions_6; }
	inline void set__extensions_6(X509ExtensionCollection_t3160461637 * value)
	{
		____extensions_6 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_6), value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__serial_8() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____serial_8)); }
	inline String_t* get__serial_8() const { return ____serial_8; }
	inline String_t** get_address_of__serial_8() { return &____serial_8; }
	inline void set__serial_8(String_t* value)
	{
		____serial_8 = value;
		Il2CppCodeGenWriteBarrier((&____serial_8), value);
	}

	inline static int32_t get_offset_of__publicKey_9() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____publicKey_9)); }
	inline PublicKey_t3840515815 * get__publicKey_9() const { return ____publicKey_9; }
	inline PublicKey_t3840515815 ** get_address_of__publicKey_9() { return &____publicKey_9; }
	inline void set__publicKey_9(PublicKey_t3840515815 * value)
	{
		____publicKey_9 = value;
		Il2CppCodeGenWriteBarrier((&____publicKey_9), value);
	}

	inline static int32_t get_offset_of_issuer_name_10() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ___issuer_name_10)); }
	inline X500DistinguishedName_t572620573 * get_issuer_name_10() const { return ___issuer_name_10; }
	inline X500DistinguishedName_t572620573 ** get_address_of_issuer_name_10() { return &___issuer_name_10; }
	inline void set_issuer_name_10(X500DistinguishedName_t572620573 * value)
	{
		___issuer_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_10), value);
	}

	inline static int32_t get_offset_of_subject_name_11() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ___subject_name_11)); }
	inline X500DistinguishedName_t572620573 * get_subject_name_11() const { return ___subject_name_11; }
	inline X500DistinguishedName_t572620573 ** get_address_of_subject_name_11() { return &___subject_name_11; }
	inline void set_subject_name_11(X500DistinguishedName_t572620573 * value)
	{
		___subject_name_11 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_11), value);
	}

	inline static int32_t get_offset_of_signature_algorithm_12() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ___signature_algorithm_12)); }
	inline Oid_t2612574631 * get_signature_algorithm_12() const { return ___signature_algorithm_12; }
	inline Oid_t2612574631 ** get_address_of_signature_algorithm_12() { return &___signature_algorithm_12; }
	inline void set_signature_algorithm_12(Oid_t2612574631 * value)
	{
		___signature_algorithm_12 = value;
		Il2CppCodeGenWriteBarrier((&___signature_algorithm_12), value);
	}

	inline static int32_t get_offset_of__cert_13() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469, ____cert_13)); }
	inline X509Certificate_t3432709582 * get__cert_13() const { return ____cert_13; }
	inline X509Certificate_t3432709582 ** get_address_of__cert_13() { return &____cert_13; }
	inline void set__cert_13(X509Certificate_t3432709582 * value)
	{
		____cert_13 = value;
		Il2CppCodeGenWriteBarrier((&____cert_13), value);
	}
};

struct X509Certificate2_t1403970469_StaticFields
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::empty_error
	String_t* ___empty_error_14;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::commonName
	ByteU5BU5D_t434619169* ___commonName_15;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::email
	ByteU5BU5D_t434619169* ___email_16;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::signedData
	ByteU5BU5D_t434619169* ___signedData_17;

public:
	inline static int32_t get_offset_of_empty_error_14() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469_StaticFields, ___empty_error_14)); }
	inline String_t* get_empty_error_14() const { return ___empty_error_14; }
	inline String_t** get_address_of_empty_error_14() { return &___empty_error_14; }
	inline void set_empty_error_14(String_t* value)
	{
		___empty_error_14 = value;
		Il2CppCodeGenWriteBarrier((&___empty_error_14), value);
	}

	inline static int32_t get_offset_of_commonName_15() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469_StaticFields, ___commonName_15)); }
	inline ByteU5BU5D_t434619169* get_commonName_15() const { return ___commonName_15; }
	inline ByteU5BU5D_t434619169** get_address_of_commonName_15() { return &___commonName_15; }
	inline void set_commonName_15(ByteU5BU5D_t434619169* value)
	{
		___commonName_15 = value;
		Il2CppCodeGenWriteBarrier((&___commonName_15), value);
	}

	inline static int32_t get_offset_of_email_16() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469_StaticFields, ___email_16)); }
	inline ByteU5BU5D_t434619169* get_email_16() const { return ___email_16; }
	inline ByteU5BU5D_t434619169** get_address_of_email_16() { return &___email_16; }
	inline void set_email_16(ByteU5BU5D_t434619169* value)
	{
		___email_16 = value;
		Il2CppCodeGenWriteBarrier((&___email_16), value);
	}

	inline static int32_t get_offset_of_signedData_17() { return static_cast<int32_t>(offsetof(X509Certificate2_t1403970469_StaticFields, ___signedData_17)); }
	inline ByteU5BU5D_t434619169* get_signedData_17() const { return ___signedData_17; }
	inline ByteU5BU5D_t434619169** get_address_of_signedData_17() { return &___signedData_17; }
	inline void set_signedData_17(ByteU5BU5D_t434619169* value)
	{
		___signedData_17 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2_T1403970469_H
#ifndef TIMESPAN_T2821448670_H
#define TIMESPAN_T2821448670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2821448670 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2821448670_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2821448670  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2821448670  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2821448670  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2821448670  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2821448670 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2821448670  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2821448670  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2821448670 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2821448670  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2821448670_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2821448670  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2821448670 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2821448670  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2821448670_H
#ifndef WEBCONNECTIONSTREAM_T3750989120_H
#define WEBCONNECTIONSTREAM_T3750989120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream
struct  WebConnectionStream_t3750989120  : public Stream_t4138477179
{
public:
	// System.Boolean System.Net.WebConnectionStream::isRead
	bool ___isRead_2;
	// System.Net.WebConnection System.Net.WebConnectionStream::cnc
	WebConnection_t3591103104 * ___cnc_3;
	// System.Net.HttpWebRequest System.Net.WebConnectionStream::request
	HttpWebRequest_t4046343009 * ___request_4;
	// System.Byte[] System.Net.WebConnectionStream::readBuffer
	ByteU5BU5D_t434619169* ___readBuffer_5;
	// System.Int32 System.Net.WebConnectionStream::readBufferOffset
	int32_t ___readBufferOffset_6;
	// System.Int32 System.Net.WebConnectionStream::readBufferSize
	int32_t ___readBufferSize_7;
	// System.Int32 System.Net.WebConnectionStream::contentLength
	int32_t ___contentLength_8;
	// System.Int32 System.Net.WebConnectionStream::totalRead
	int32_t ___totalRead_9;
	// System.Int64 System.Net.WebConnectionStream::totalWritten
	int64_t ___totalWritten_10;
	// System.Boolean System.Net.WebConnectionStream::nextReadCalled
	bool ___nextReadCalled_11;
	// System.Int32 System.Net.WebConnectionStream::pendingReads
	int32_t ___pendingReads_12;
	// System.Int32 System.Net.WebConnectionStream::pendingWrites
	int32_t ___pendingWrites_13;
	// System.Threading.ManualResetEvent System.Net.WebConnectionStream::pending
	ManualResetEvent_t504895258 * ___pending_14;
	// System.Boolean System.Net.WebConnectionStream::allowBuffering
	bool ___allowBuffering_15;
	// System.Boolean System.Net.WebConnectionStream::sendChunked
	bool ___sendChunked_16;
	// System.IO.MemoryStream System.Net.WebConnectionStream::writeBuffer
	MemoryStream_t2933948287 * ___writeBuffer_17;
	// System.Boolean System.Net.WebConnectionStream::requestWritten
	bool ___requestWritten_18;
	// System.Byte[] System.Net.WebConnectionStream::headers
	ByteU5BU5D_t434619169* ___headers_19;
	// System.Boolean System.Net.WebConnectionStream::disposed
	bool ___disposed_20;
	// System.Boolean System.Net.WebConnectionStream::headersSent
	bool ___headersSent_21;
	// System.Object System.Net.WebConnectionStream::locker
	RuntimeObject * ___locker_22;
	// System.Boolean System.Net.WebConnectionStream::initRead
	bool ___initRead_23;
	// System.Boolean System.Net.WebConnectionStream::read_eof
	bool ___read_eof_24;
	// System.Boolean System.Net.WebConnectionStream::complete_request_written
	bool ___complete_request_written_25;
	// System.Int32 System.Net.WebConnectionStream::read_timeout
	int32_t ___read_timeout_26;
	// System.Int32 System.Net.WebConnectionStream::write_timeout
	int32_t ___write_timeout_27;

public:
	inline static int32_t get_offset_of_isRead_2() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___isRead_2)); }
	inline bool get_isRead_2() const { return ___isRead_2; }
	inline bool* get_address_of_isRead_2() { return &___isRead_2; }
	inline void set_isRead_2(bool value)
	{
		___isRead_2 = value;
	}

	inline static int32_t get_offset_of_cnc_3() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___cnc_3)); }
	inline WebConnection_t3591103104 * get_cnc_3() const { return ___cnc_3; }
	inline WebConnection_t3591103104 ** get_address_of_cnc_3() { return &___cnc_3; }
	inline void set_cnc_3(WebConnection_t3591103104 * value)
	{
		___cnc_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_3), value);
	}

	inline static int32_t get_offset_of_request_4() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___request_4)); }
	inline HttpWebRequest_t4046343009 * get_request_4() const { return ___request_4; }
	inline HttpWebRequest_t4046343009 ** get_address_of_request_4() { return &___request_4; }
	inline void set_request_4(HttpWebRequest_t4046343009 * value)
	{
		___request_4 = value;
		Il2CppCodeGenWriteBarrier((&___request_4), value);
	}

	inline static int32_t get_offset_of_readBuffer_5() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___readBuffer_5)); }
	inline ByteU5BU5D_t434619169* get_readBuffer_5() const { return ___readBuffer_5; }
	inline ByteU5BU5D_t434619169** get_address_of_readBuffer_5() { return &___readBuffer_5; }
	inline void set_readBuffer_5(ByteU5BU5D_t434619169* value)
	{
		___readBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_5), value);
	}

	inline static int32_t get_offset_of_readBufferOffset_6() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___readBufferOffset_6)); }
	inline int32_t get_readBufferOffset_6() const { return ___readBufferOffset_6; }
	inline int32_t* get_address_of_readBufferOffset_6() { return &___readBufferOffset_6; }
	inline void set_readBufferOffset_6(int32_t value)
	{
		___readBufferOffset_6 = value;
	}

	inline static int32_t get_offset_of_readBufferSize_7() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___readBufferSize_7)); }
	inline int32_t get_readBufferSize_7() const { return ___readBufferSize_7; }
	inline int32_t* get_address_of_readBufferSize_7() { return &___readBufferSize_7; }
	inline void set_readBufferSize_7(int32_t value)
	{
		___readBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_contentLength_8() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___contentLength_8)); }
	inline int32_t get_contentLength_8() const { return ___contentLength_8; }
	inline int32_t* get_address_of_contentLength_8() { return &___contentLength_8; }
	inline void set_contentLength_8(int32_t value)
	{
		___contentLength_8 = value;
	}

	inline static int32_t get_offset_of_totalRead_9() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___totalRead_9)); }
	inline int32_t get_totalRead_9() const { return ___totalRead_9; }
	inline int32_t* get_address_of_totalRead_9() { return &___totalRead_9; }
	inline void set_totalRead_9(int32_t value)
	{
		___totalRead_9 = value;
	}

	inline static int32_t get_offset_of_totalWritten_10() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___totalWritten_10)); }
	inline int64_t get_totalWritten_10() const { return ___totalWritten_10; }
	inline int64_t* get_address_of_totalWritten_10() { return &___totalWritten_10; }
	inline void set_totalWritten_10(int64_t value)
	{
		___totalWritten_10 = value;
	}

	inline static int32_t get_offset_of_nextReadCalled_11() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___nextReadCalled_11)); }
	inline bool get_nextReadCalled_11() const { return ___nextReadCalled_11; }
	inline bool* get_address_of_nextReadCalled_11() { return &___nextReadCalled_11; }
	inline void set_nextReadCalled_11(bool value)
	{
		___nextReadCalled_11 = value;
	}

	inline static int32_t get_offset_of_pendingReads_12() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___pendingReads_12)); }
	inline int32_t get_pendingReads_12() const { return ___pendingReads_12; }
	inline int32_t* get_address_of_pendingReads_12() { return &___pendingReads_12; }
	inline void set_pendingReads_12(int32_t value)
	{
		___pendingReads_12 = value;
	}

	inline static int32_t get_offset_of_pendingWrites_13() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___pendingWrites_13)); }
	inline int32_t get_pendingWrites_13() const { return ___pendingWrites_13; }
	inline int32_t* get_address_of_pendingWrites_13() { return &___pendingWrites_13; }
	inline void set_pendingWrites_13(int32_t value)
	{
		___pendingWrites_13 = value;
	}

	inline static int32_t get_offset_of_pending_14() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___pending_14)); }
	inline ManualResetEvent_t504895258 * get_pending_14() const { return ___pending_14; }
	inline ManualResetEvent_t504895258 ** get_address_of_pending_14() { return &___pending_14; }
	inline void set_pending_14(ManualResetEvent_t504895258 * value)
	{
		___pending_14 = value;
		Il2CppCodeGenWriteBarrier((&___pending_14), value);
	}

	inline static int32_t get_offset_of_allowBuffering_15() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___allowBuffering_15)); }
	inline bool get_allowBuffering_15() const { return ___allowBuffering_15; }
	inline bool* get_address_of_allowBuffering_15() { return &___allowBuffering_15; }
	inline void set_allowBuffering_15(bool value)
	{
		___allowBuffering_15 = value;
	}

	inline static int32_t get_offset_of_sendChunked_16() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___sendChunked_16)); }
	inline bool get_sendChunked_16() const { return ___sendChunked_16; }
	inline bool* get_address_of_sendChunked_16() { return &___sendChunked_16; }
	inline void set_sendChunked_16(bool value)
	{
		___sendChunked_16 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_17() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___writeBuffer_17)); }
	inline MemoryStream_t2933948287 * get_writeBuffer_17() const { return ___writeBuffer_17; }
	inline MemoryStream_t2933948287 ** get_address_of_writeBuffer_17() { return &___writeBuffer_17; }
	inline void set_writeBuffer_17(MemoryStream_t2933948287 * value)
	{
		___writeBuffer_17 = value;
		Il2CppCodeGenWriteBarrier((&___writeBuffer_17), value);
	}

	inline static int32_t get_offset_of_requestWritten_18() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___requestWritten_18)); }
	inline bool get_requestWritten_18() const { return ___requestWritten_18; }
	inline bool* get_address_of_requestWritten_18() { return &___requestWritten_18; }
	inline void set_requestWritten_18(bool value)
	{
		___requestWritten_18 = value;
	}

	inline static int32_t get_offset_of_headers_19() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___headers_19)); }
	inline ByteU5BU5D_t434619169* get_headers_19() const { return ___headers_19; }
	inline ByteU5BU5D_t434619169** get_address_of_headers_19() { return &___headers_19; }
	inline void set_headers_19(ByteU5BU5D_t434619169* value)
	{
		___headers_19 = value;
		Il2CppCodeGenWriteBarrier((&___headers_19), value);
	}

	inline static int32_t get_offset_of_disposed_20() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___disposed_20)); }
	inline bool get_disposed_20() const { return ___disposed_20; }
	inline bool* get_address_of_disposed_20() { return &___disposed_20; }
	inline void set_disposed_20(bool value)
	{
		___disposed_20 = value;
	}

	inline static int32_t get_offset_of_headersSent_21() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___headersSent_21)); }
	inline bool get_headersSent_21() const { return ___headersSent_21; }
	inline bool* get_address_of_headersSent_21() { return &___headersSent_21; }
	inline void set_headersSent_21(bool value)
	{
		___headersSent_21 = value;
	}

	inline static int32_t get_offset_of_locker_22() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___locker_22)); }
	inline RuntimeObject * get_locker_22() const { return ___locker_22; }
	inline RuntimeObject ** get_address_of_locker_22() { return &___locker_22; }
	inline void set_locker_22(RuntimeObject * value)
	{
		___locker_22 = value;
		Il2CppCodeGenWriteBarrier((&___locker_22), value);
	}

	inline static int32_t get_offset_of_initRead_23() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___initRead_23)); }
	inline bool get_initRead_23() const { return ___initRead_23; }
	inline bool* get_address_of_initRead_23() { return &___initRead_23; }
	inline void set_initRead_23(bool value)
	{
		___initRead_23 = value;
	}

	inline static int32_t get_offset_of_read_eof_24() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___read_eof_24)); }
	inline bool get_read_eof_24() const { return ___read_eof_24; }
	inline bool* get_address_of_read_eof_24() { return &___read_eof_24; }
	inline void set_read_eof_24(bool value)
	{
		___read_eof_24 = value;
	}

	inline static int32_t get_offset_of_complete_request_written_25() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___complete_request_written_25)); }
	inline bool get_complete_request_written_25() const { return ___complete_request_written_25; }
	inline bool* get_address_of_complete_request_written_25() { return &___complete_request_written_25; }
	inline void set_complete_request_written_25(bool value)
	{
		___complete_request_written_25 = value;
	}

	inline static int32_t get_offset_of_read_timeout_26() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___read_timeout_26)); }
	inline int32_t get_read_timeout_26() const { return ___read_timeout_26; }
	inline int32_t* get_address_of_read_timeout_26() { return &___read_timeout_26; }
	inline void set_read_timeout_26(int32_t value)
	{
		___read_timeout_26 = value;
	}

	inline static int32_t get_offset_of_write_timeout_27() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120, ___write_timeout_27)); }
	inline int32_t get_write_timeout_27() const { return ___write_timeout_27; }
	inline int32_t* get_address_of_write_timeout_27() { return &___write_timeout_27; }
	inline void set_write_timeout_27(int32_t value)
	{
		___write_timeout_27 = value;
	}
};

struct WebConnectionStream_t3750989120_StaticFields
{
public:
	// System.Byte[] System.Net.WebConnectionStream::crlf
	ByteU5BU5D_t434619169* ___crlf_1;

public:
	inline static int32_t get_offset_of_crlf_1() { return static_cast<int32_t>(offsetof(WebConnectionStream_t3750989120_StaticFields, ___crlf_1)); }
	inline ByteU5BU5D_t434619169* get_crlf_1() const { return ___crlf_1; }
	inline ByteU5BU5D_t434619169** get_address_of_crlf_1() { return &___crlf_1; }
	inline void set_crlf_1(ByteU5BU5D_t434619169* value)
	{
		___crlf_1 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONSTREAM_T3750989120_H
#ifndef FTPDATASTREAM_T3803374522_H
#define FTPDATASTREAM_T3803374522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream
struct  FtpDataStream_t3803374522  : public Stream_t4138477179
{
public:
	// System.Net.FtpWebRequest System.Net.FtpDataStream::request
	FtpWebRequest_t4270856504 * ___request_1;
	// System.IO.Stream System.Net.FtpDataStream::networkStream
	Stream_t4138477179 * ___networkStream_2;
	// System.Boolean System.Net.FtpDataStream::disposed
	bool ___disposed_3;
	// System.Boolean System.Net.FtpDataStream::isRead
	bool ___isRead_4;
	// System.Int32 System.Net.FtpDataStream::totalRead
	int32_t ___totalRead_5;

public:
	inline static int32_t get_offset_of_request_1() { return static_cast<int32_t>(offsetof(FtpDataStream_t3803374522, ___request_1)); }
	inline FtpWebRequest_t4270856504 * get_request_1() const { return ___request_1; }
	inline FtpWebRequest_t4270856504 ** get_address_of_request_1() { return &___request_1; }
	inline void set_request_1(FtpWebRequest_t4270856504 * value)
	{
		___request_1 = value;
		Il2CppCodeGenWriteBarrier((&___request_1), value);
	}

	inline static int32_t get_offset_of_networkStream_2() { return static_cast<int32_t>(offsetof(FtpDataStream_t3803374522, ___networkStream_2)); }
	inline Stream_t4138477179 * get_networkStream_2() const { return ___networkStream_2; }
	inline Stream_t4138477179 ** get_address_of_networkStream_2() { return &___networkStream_2; }
	inline void set_networkStream_2(Stream_t4138477179 * value)
	{
		___networkStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_2), value);
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(FtpDataStream_t3803374522, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_isRead_4() { return static_cast<int32_t>(offsetof(FtpDataStream_t3803374522, ___isRead_4)); }
	inline bool get_isRead_4() const { return ___isRead_4; }
	inline bool* get_address_of_isRead_4() { return &___isRead_4; }
	inline void set_isRead_4(bool value)
	{
		___isRead_4 = value;
	}

	inline static int32_t get_offset_of_totalRead_5() { return static_cast<int32_t>(offsetof(FtpDataStream_t3803374522, ___totalRead_5)); }
	inline int32_t get_totalRead_5() const { return ___totalRead_5; }
	inline int32_t* get_address_of_totalRead_5() { return &___totalRead_5; }
	inline void set_totalRead_5(int32_t value)
	{
		___totalRead_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPDATASTREAM_T3803374522_H
#ifndef X509CERTIFICATECOLLECTION_T728319640_H
#define X509CERTIFICATECOLLECTION_T728319640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct  X509CertificateCollection_t728319640  : public CollectionBase_t1611667396
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T728319640_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef IPENDPOINT_T1226219872_H
#define IPENDPOINT_T1226219872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t1226219872  : public EndPoint_t2958140078
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::address
	IPAddress_t86924491 * ___address_0;
	// System.Int32 System.Net.IPEndPoint::port
	int32_t ___port_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPEndPoint_t1226219872, ___address_0)); }
	inline IPAddress_t86924491 * get_address_0() const { return ___address_0; }
	inline IPAddress_t86924491 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(IPAddress_t86924491 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(IPEndPoint_t1226219872, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_T1226219872_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T2306134369_H
#define X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T2306134369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
struct  X509SubjectKeyIdentifierHashAlgorithm_t2306134369 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierHashAlgorithm_t2306134369, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T2306134369_H
#ifndef INVALIDOPERATIONEXCEPTION_T3358289486_H
#define INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t3358289486  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T3358289486_H
#ifndef ADDRESSFAMILY_T688560430_H
#define ADDRESSFAMILY_T688560430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t688560430 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t688560430, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T688560430_H
#ifndef DECOMPRESSIONMETHODS_T639014177_H
#define DECOMPRESSIONMETHODS_T639014177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t639014177 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DecompressionMethods_t639014177, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T639014177_H
#ifndef AUTHENTICATIONLEVEL_T4067115101_H
#define AUTHENTICATIONLEVEL_T4067115101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_t4067115101 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationLevel_t4067115101, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_T4067115101_H
#ifndef X509CHAINSTATUSFLAGS_T849332985_H
#define X509CHAINSTATUSFLAGS_T849332985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t849332985 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t849332985, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T849332985_H
#ifndef X509REVOCATIONMODE_T2152948854_H
#define X509REVOCATIONMODE_T2152948854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2152948854 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2152948854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2152948854_H
#ifndef FORMATEXCEPTION_T501182138_H
#define FORMATEXCEPTION_T501182138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t501182138  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T501182138_H
#ifndef ASNDECODESTATUS_T1018007474_H
#define ASNDECODESTATUS_T1018007474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t1018007474 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t1018007474, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T1018007474_H
#ifndef X509VERIFICATIONFLAGS_T2292168690_H
#define X509VERIFICATIONFLAGS_T2292168690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t2292168690 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t2292168690, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T2292168690_H
#ifndef X509FINDTYPE_T2680667608_H
#define X509FINDTYPE_T2680667608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509FindType
struct  X509FindType_t2680667608 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509FindType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509FindType_t2680667608, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509FINDTYPE_T2680667608_H
#ifndef X509KEYUSAGEFLAGS_T2736574660_H
#define X509KEYUSAGEFLAGS_T2736574660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t2736574660 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t2736574660, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T2736574660_H
#ifndef X509NAMETYPE_T3605404887_H
#define X509NAMETYPE_T3605404887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509NameType
struct  X509NameType_t3605404887 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509NameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509NameType_t3605404887, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETYPE_T3605404887_H
#ifndef FILEACCESS_T3105075288_H
#define FILEACCESS_T3105075288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t3105075288 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAccess_t3105075288, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T3105075288_H
#ifndef X509REVOCATIONFLAG_T1554360456_H
#define X509REVOCATIONFLAG_T1554360456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_t1554360456 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationFlag_t1554360456, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_T1554360456_H
#ifndef X509CERTIFICATE2COLLECTION_T454981218_H
#define X509CERTIFICATE2COLLECTION_T454981218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct  X509Certificate2Collection_t454981218  : public X509CertificateCollection_t728319640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2COLLECTION_T454981218_H
#ifndef OPENFLAGS_T3015610782_H
#define OPENFLAGS_T3015610782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_t3015610782 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpenFlags_t3015610782, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_T3015610782_H
#ifndef READSTATE_T3061371541_H
#define READSTATE_T3061371541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ReadState
struct  ReadState_t3061371541 
{
public:
	// System.Int32 System.Net.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t3061371541, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T3061371541_H
#ifndef DATETIMEKIND_T1636762993_H
#define DATETIMEKIND_T1636762993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t1636762993 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t1636762993, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T1636762993_H
#ifndef FILEWEBRESPONSE_T982058165_H
#define FILEWEBRESPONSE_T982058165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebResponse
struct  FileWebResponse_t982058165  : public WebResponse_t3616229315
{
public:
	// System.Uri System.Net.FileWebResponse::responseUri
	Uri_t3269222095 * ___responseUri_1;
	// System.IO.FileStream System.Net.FileWebResponse::fileStream
	FileStream_t3854196930 * ___fileStream_2;
	// System.Int64 System.Net.FileWebResponse::contentLength
	int64_t ___contentLength_3;
	// System.Net.WebHeaderCollection System.Net.FileWebResponse::webHeaders
	WebHeaderCollection_t592894254 * ___webHeaders_4;
	// System.Boolean System.Net.FileWebResponse::disposed
	bool ___disposed_5;

public:
	inline static int32_t get_offset_of_responseUri_1() { return static_cast<int32_t>(offsetof(FileWebResponse_t982058165, ___responseUri_1)); }
	inline Uri_t3269222095 * get_responseUri_1() const { return ___responseUri_1; }
	inline Uri_t3269222095 ** get_address_of_responseUri_1() { return &___responseUri_1; }
	inline void set_responseUri_1(Uri_t3269222095 * value)
	{
		___responseUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseUri_1), value);
	}

	inline static int32_t get_offset_of_fileStream_2() { return static_cast<int32_t>(offsetof(FileWebResponse_t982058165, ___fileStream_2)); }
	inline FileStream_t3854196930 * get_fileStream_2() const { return ___fileStream_2; }
	inline FileStream_t3854196930 ** get_address_of_fileStream_2() { return &___fileStream_2; }
	inline void set_fileStream_2(FileStream_t3854196930 * value)
	{
		___fileStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileStream_2), value);
	}

	inline static int32_t get_offset_of_contentLength_3() { return static_cast<int32_t>(offsetof(FileWebResponse_t982058165, ___contentLength_3)); }
	inline int64_t get_contentLength_3() const { return ___contentLength_3; }
	inline int64_t* get_address_of_contentLength_3() { return &___contentLength_3; }
	inline void set_contentLength_3(int64_t value)
	{
		___contentLength_3 = value;
	}

	inline static int32_t get_offset_of_webHeaders_4() { return static_cast<int32_t>(offsetof(FileWebResponse_t982058165, ___webHeaders_4)); }
	inline WebHeaderCollection_t592894254 * get_webHeaders_4() const { return ___webHeaders_4; }
	inline WebHeaderCollection_t592894254 ** get_address_of_webHeaders_4() { return &___webHeaders_4; }
	inline void set_webHeaders_4(WebHeaderCollection_t592894254 * value)
	{
		___webHeaders_4 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_4), value);
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(FileWebResponse_t982058165, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBRESPONSE_T982058165_H
#ifndef SECURITYPROTOCOLTYPE_T1627302747_H
#define SECURITYPROTOCOLTYPE_T1627302747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t1627302747 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t1627302747, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T1627302747_H
#ifndef WEBEXCEPTIONSTATUS_T844493678_H
#define WEBEXCEPTIONSTATUS_T844493678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t844493678 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t844493678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T844493678_H
#ifndef WEBHEADERCOLLECTION_T592894254_H
#define WEBHEADERCOLLECTION_T592894254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_t592894254  : public NameValueCollection_t1739534792
{
public:
	// System.Boolean System.Net.WebHeaderCollection::internallyCreated
	bool ___internallyCreated_15;

public:
	inline static int32_t get_offset_of_internallyCreated_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t592894254, ___internallyCreated_15)); }
	inline bool get_internallyCreated_15() const { return ___internallyCreated_15; }
	inline bool* get_address_of_internallyCreated_15() { return &___internallyCreated_15; }
	inline void set_internallyCreated_15(bool value)
	{
		___internallyCreated_15 = value;
	}
};

struct WebHeaderCollection_t592894254_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.WebHeaderCollection::restricted
	Hashtable_t2354558714 * ___restricted_12;
	// System.Collections.Hashtable System.Net.WebHeaderCollection::multiValue
	Hashtable_t2354558714 * ___multiValue_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Net.WebHeaderCollection::restricted_response
	Dictionary_2_t4030136713 * ___restricted_response_14;
	// System.Boolean[] System.Net.WebHeaderCollection::allowed_chars
	BooleanU5BU5D_t698278498* ___allowed_chars_16;

public:
	inline static int32_t get_offset_of_restricted_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t592894254_StaticFields, ___restricted_12)); }
	inline Hashtable_t2354558714 * get_restricted_12() const { return ___restricted_12; }
	inline Hashtable_t2354558714 ** get_address_of_restricted_12() { return &___restricted_12; }
	inline void set_restricted_12(Hashtable_t2354558714 * value)
	{
		___restricted_12 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_12), value);
	}

	inline static int32_t get_offset_of_multiValue_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t592894254_StaticFields, ___multiValue_13)); }
	inline Hashtable_t2354558714 * get_multiValue_13() const { return ___multiValue_13; }
	inline Hashtable_t2354558714 ** get_address_of_multiValue_13() { return &___multiValue_13; }
	inline void set_multiValue_13(Hashtable_t2354558714 * value)
	{
		___multiValue_13 = value;
		Il2CppCodeGenWriteBarrier((&___multiValue_13), value);
	}

	inline static int32_t get_offset_of_restricted_response_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t592894254_StaticFields, ___restricted_response_14)); }
	inline Dictionary_2_t4030136713 * get_restricted_response_14() const { return ___restricted_response_14; }
	inline Dictionary_2_t4030136713 ** get_address_of_restricted_response_14() { return &___restricted_response_14; }
	inline void set_restricted_response_14(Dictionary_2_t4030136713 * value)
	{
		___restricted_response_14 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_response_14), value);
	}

	inline static int32_t get_offset_of_allowed_chars_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t592894254_StaticFields, ___allowed_chars_16)); }
	inline BooleanU5BU5D_t698278498* get_allowed_chars_16() const { return ___allowed_chars_16; }
	inline BooleanU5BU5D_t698278498** get_address_of_allowed_chars_16() { return &___allowed_chars_16; }
	inline void set_allowed_chars_16(BooleanU5BU5D_t698278498* value)
	{
		___allowed_chars_16 = value;
		Il2CppCodeGenWriteBarrier((&___allowed_chars_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T592894254_H
#ifndef FTPSTATUSCODE_T226517375_H
#define FTPSTATUSCODE_T226517375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatusCode
struct  FtpStatusCode_t226517375 
{
public:
	// System.Int32 System.Net.FtpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FtpStatusCode_t226517375, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUSCODE_T226517375_H
#ifndef SSLPROTOCOLS_T3922469944_H
#define SSLPROTOCOLS_T3922469944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t3922469944 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslProtocols_t3922469944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T3922469944_H
#ifndef REQUESTSTATE_T1112878644_H
#define REQUESTSTATE_T1112878644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest/RequestState
struct  RequestState_t1112878644 
{
public:
	// System.Int32 System.Net.FtpWebRequest/RequestState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequestState_t1112878644, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTATE_T1112878644_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef SECTRUSTRESULT_T1623568823_H
#define SECTRUSTRESULT_T1623568823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.OSX509Certificates/SecTrustResult
struct  SecTrustResult_t1623568823 
{
public:
	// System.Int32 Mono.Security.X509.OSX509Certificates/SecTrustResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecTrustResult_t1623568823, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTRUSTRESULT_T1623568823_H
#ifndef STORELOCATION_T3191330486_H
#define STORELOCATION_T3191330486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreLocation
struct  StoreLocation_t3191330486 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreLocation_t3191330486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELOCATION_T3191330486_H
#ifndef STORENAME_T3887743455_H
#define STORENAME_T3887743455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreName
struct  StoreName_t3887743455 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreName_t3887743455, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORENAME_T3887743455_H
#ifndef X500DISTINGUISHEDNAMEFLAGS_T1994920494_H
#define X500DISTINGUISHEDNAMEFLAGS_T1994920494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
struct  X500DistinguishedNameFlags_t1994920494 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X500DistinguishedNameFlags_t1994920494, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAMEFLAGS_T1994920494_H
#ifndef HTTPSTATUSCODE_T1537078283_H
#define HTTPSTATUSCODE_T1537078283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1537078283 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1537078283, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1537078283_H
#ifndef OSX509CERTIFICATES_T2020383372_H
#define OSX509CERTIFICATES_T2020383372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.OSX509Certificates
struct  OSX509Certificates_t2020383372  : public RuntimeObject
{
public:

public:
};

struct OSX509Certificates_t2020383372_StaticFields
{
public:
	// System.IntPtr Mono.Security.X509.OSX509Certificates::sslsecpolicy
	intptr_t ___sslsecpolicy_0;

public:
	inline static int32_t get_offset_of_sslsecpolicy_0() { return static_cast<int32_t>(offsetof(OSX509Certificates_t2020383372_StaticFields, ___sslsecpolicy_0)); }
	inline intptr_t get_sslsecpolicy_0() const { return ___sslsecpolicy_0; }
	inline intptr_t* get_address_of_sslsecpolicy_0() { return &___sslsecpolicy_0; }
	inline void set_sslsecpolicy_0(intptr_t value)
	{
		___sslsecpolicy_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSX509CERTIFICATES_T2020383372_H
#ifndef IPADDRESS_T86924491_H
#define IPADDRESS_T86924491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t86924491  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t610847954* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t86924491, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t86924491, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t86924491, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t610847954* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t610847954** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t610847954* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t86924491, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}
};

struct IPAddress_t86924491_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t86924491 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t86924491 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t86924491 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t86924491 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t86924491 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t86924491 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t86924491 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___Any_4)); }
	inline IPAddress_t86924491 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t86924491 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t86924491 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t86924491 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t86924491 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t86924491 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___Loopback_6)); }
	inline IPAddress_t86924491 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t86924491 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t86924491 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___None_7)); }
	inline IPAddress_t86924491 * get_None_7() const { return ___None_7; }
	inline IPAddress_t86924491 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t86924491 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t86924491 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t86924491 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t86924491 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t86924491 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t86924491 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t86924491 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t86924491_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t86924491 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t86924491 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t86924491 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T86924491_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef FILESTREAM_T3854196930_H
#define FILESTREAM_T3854196930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t3854196930  : public Stream_t4138477179
{
public:
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_1;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_2;
	// System.Boolean System.IO.FileStream::async
	bool ___async_3;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_4;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_5;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_6;
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t434619169* ___buf_7;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_8;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_9;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_10;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_11;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_12;
	// System.String System.IO.FileStream::name
	String_t* ___name_13;
	// System.IntPtr System.IO.FileStream::handle
	intptr_t ___handle_14;

public:
	inline static int32_t get_offset_of_access_1() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___access_1)); }
	inline int32_t get_access_1() const { return ___access_1; }
	inline int32_t* get_address_of_access_1() { return &___access_1; }
	inline void set_access_1(int32_t value)
	{
		___access_1 = value;
	}

	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___owner_2)); }
	inline bool get_owner_2() const { return ___owner_2; }
	inline bool* get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(bool value)
	{
		___owner_2 = value;
	}

	inline static int32_t get_offset_of_async_3() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___async_3)); }
	inline bool get_async_3() const { return ___async_3; }
	inline bool* get_address_of_async_3() { return &___async_3; }
	inline void set_async_3(bool value)
	{
		___async_3 = value;
	}

	inline static int32_t get_offset_of_canseek_4() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___canseek_4)); }
	inline bool get_canseek_4() const { return ___canseek_4; }
	inline bool* get_address_of_canseek_4() { return &___canseek_4; }
	inline void set_canseek_4(bool value)
	{
		___canseek_4 = value;
	}

	inline static int32_t get_offset_of_append_startpos_5() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___append_startpos_5)); }
	inline int64_t get_append_startpos_5() const { return ___append_startpos_5; }
	inline int64_t* get_address_of_append_startpos_5() { return &___append_startpos_5; }
	inline void set_append_startpos_5(int64_t value)
	{
		___append_startpos_5 = value;
	}

	inline static int32_t get_offset_of_anonymous_6() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___anonymous_6)); }
	inline bool get_anonymous_6() const { return ___anonymous_6; }
	inline bool* get_address_of_anonymous_6() { return &___anonymous_6; }
	inline void set_anonymous_6(bool value)
	{
		___anonymous_6 = value;
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_7)); }
	inline ByteU5BU5D_t434619169* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_t434619169** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_t434619169* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_buf_size_8() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_size_8)); }
	inline int32_t get_buf_size_8() const { return ___buf_size_8; }
	inline int32_t* get_address_of_buf_size_8() { return &___buf_size_8; }
	inline void set_buf_size_8(int32_t value)
	{
		___buf_size_8 = value;
	}

	inline static int32_t get_offset_of_buf_length_9() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_length_9)); }
	inline int32_t get_buf_length_9() const { return ___buf_length_9; }
	inline int32_t* get_address_of_buf_length_9() { return &___buf_length_9; }
	inline void set_buf_length_9(int32_t value)
	{
		___buf_length_9 = value;
	}

	inline static int32_t get_offset_of_buf_offset_10() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_offset_10)); }
	inline int32_t get_buf_offset_10() const { return ___buf_offset_10; }
	inline int32_t* get_address_of_buf_offset_10() { return &___buf_offset_10; }
	inline void set_buf_offset_10(int32_t value)
	{
		___buf_offset_10 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_11() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_dirty_11)); }
	inline bool get_buf_dirty_11() const { return ___buf_dirty_11; }
	inline bool* get_address_of_buf_dirty_11() { return &___buf_dirty_11; }
	inline void set_buf_dirty_11(bool value)
	{
		___buf_dirty_11 = value;
	}

	inline static int32_t get_offset_of_buf_start_12() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___buf_start_12)); }
	inline int64_t get_buf_start_12() const { return ___buf_start_12; }
	inline int64_t* get_address_of_buf_start_12() { return &___buf_start_12; }
	inline void set_buf_start_12(int64_t value)
	{
		___buf_start_12 = value;
	}

	inline static int32_t get_offset_of_name_13() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___name_13)); }
	inline String_t* get_name_13() const { return ___name_13; }
	inline String_t** get_address_of_name_13() { return &___name_13; }
	inline void set_name_13(String_t* value)
	{
		___name_13 = value;
		Il2CppCodeGenWriteBarrier((&___name_13), value);
	}

	inline static int32_t get_offset_of_handle_14() { return static_cast<int32_t>(offsetof(FileStream_t3854196930, ___handle_14)); }
	inline intptr_t get_handle_14() const { return ___handle_14; }
	inline intptr_t* get_address_of_handle_14() { return &___handle_14; }
	inline void set_handle_14(intptr_t value)
	{
		___handle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_T3854196930_H
#ifndef DATETIME_T359870098_H
#define DATETIME_T359870098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t359870098 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2821448670  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___ticks_0)); }
	inline TimeSpan_t2821448670  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2821448670 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2821448670  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t359870098, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t359870098_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t359870098  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t359870098  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t2511808107* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t2511808107* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t2511808107* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t2511808107* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t2511808107* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t2511808107* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t2511808107* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t1965588061* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t1965588061* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MaxValue_2)); }
	inline DateTime_t359870098  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t359870098 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t359870098  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MinValue_3)); }
	inline DateTime_t359870098  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t359870098 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t359870098  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t2511808107* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t2511808107* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t2511808107* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t2511808107* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t2511808107* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t2511808107* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t2511808107* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t2511808107* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t2511808107* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t2511808107** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t2511808107* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t2511808107* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t2511808107** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t2511808107* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t2511808107* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t2511808107** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t2511808107* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t1965588061* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t1965588061* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t1965588061* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t1965588061** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t1965588061* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t359870098_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T359870098_H
#ifndef SERVICEPOINTMANAGER_T3086999306_H
#define SERVICEPOINTMANAGER_T3086999306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_t3086999306  : public RuntimeObject
{
public:

public:
};

struct ServicePointManager_t3086999306_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t439385300 * ___servicePoints_0;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	RuntimeObject* ___policy_1;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_2;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_3;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_4;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_5;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_6;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_7;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_8;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::server_cert_cb
	RemoteCertificateValidationCallback_t2807660831 * ___server_cert_cb_9;

public:
	inline static int32_t get_offset_of_servicePoints_0() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___servicePoints_0)); }
	inline HybridDictionary_t439385300 * get_servicePoints_0() const { return ___servicePoints_0; }
	inline HybridDictionary_t439385300 ** get_address_of_servicePoints_0() { return &___servicePoints_0; }
	inline void set_servicePoints_0(HybridDictionary_t439385300 * value)
	{
		___servicePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoints_0), value);
	}

	inline static int32_t get_offset_of_policy_1() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___policy_1)); }
	inline RuntimeObject* get_policy_1() const { return ___policy_1; }
	inline RuntimeObject** get_address_of_policy_1() { return &___policy_1; }
	inline void set_policy_1(RuntimeObject* value)
	{
		___policy_1 = value;
		Il2CppCodeGenWriteBarrier((&___policy_1), value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_2() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___defaultConnectionLimit_2)); }
	inline int32_t get_defaultConnectionLimit_2() const { return ___defaultConnectionLimit_2; }
	inline int32_t* get_address_of_defaultConnectionLimit_2() { return &___defaultConnectionLimit_2; }
	inline void set_defaultConnectionLimit_2(int32_t value)
	{
		___defaultConnectionLimit_2 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_3() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___maxServicePointIdleTime_3)); }
	inline int32_t get_maxServicePointIdleTime_3() const { return ___maxServicePointIdleTime_3; }
	inline int32_t* get_address_of_maxServicePointIdleTime_3() { return &___maxServicePointIdleTime_3; }
	inline void set_maxServicePointIdleTime_3(int32_t value)
	{
		___maxServicePointIdleTime_3 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_4() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___maxServicePoints_4)); }
	inline int32_t get_maxServicePoints_4() const { return ___maxServicePoints_4; }
	inline int32_t* get_address_of_maxServicePoints_4() { return &___maxServicePoints_4; }
	inline void set_maxServicePoints_4(int32_t value)
	{
		___maxServicePoints_4 = value;
	}

	inline static int32_t get_offset_of__checkCRL_5() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ____checkCRL_5)); }
	inline bool get__checkCRL_5() const { return ____checkCRL_5; }
	inline bool* get_address_of__checkCRL_5() { return &____checkCRL_5; }
	inline void set__checkCRL_5(bool value)
	{
		____checkCRL_5 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_6() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ____securityProtocol_6)); }
	inline int32_t get__securityProtocol_6() const { return ____securityProtocol_6; }
	inline int32_t* get_address_of__securityProtocol_6() { return &____securityProtocol_6; }
	inline void set__securityProtocol_6(int32_t value)
	{
		____securityProtocol_6 = value;
	}

	inline static int32_t get_offset_of_expectContinue_7() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___expectContinue_7)); }
	inline bool get_expectContinue_7() const { return ___expectContinue_7; }
	inline bool* get_address_of_expectContinue_7() { return &___expectContinue_7; }
	inline void set_expectContinue_7(bool value)
	{
		___expectContinue_7 = value;
	}

	inline static int32_t get_offset_of_useNagle_8() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___useNagle_8)); }
	inline bool get_useNagle_8() const { return ___useNagle_8; }
	inline bool* get_address_of_useNagle_8() { return &___useNagle_8; }
	inline void set_useNagle_8(bool value)
	{
		___useNagle_8 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_9() { return static_cast<int32_t>(offsetof(ServicePointManager_t3086999306_StaticFields, ___server_cert_cb_9)); }
	inline RemoteCertificateValidationCallback_t2807660831 * get_server_cert_cb_9() const { return ___server_cert_cb_9; }
	inline RemoteCertificateValidationCallback_t2807660831 ** get_address_of_server_cert_cb_9() { return &___server_cert_cb_9; }
	inline void set_server_cert_cb_9(RemoteCertificateValidationCallback_t2807660831 * value)
	{
		___server_cert_cb_9 = value;
		Il2CppCodeGenWriteBarrier((&___server_cert_cb_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGER_T3086999306_H
#ifndef HTTPWEBRESPONSE_T2352629529_H
#define HTTPWEBRESPONSE_T2352629529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebResponse
struct  HttpWebResponse_t2352629529  : public WebResponse_t3616229315
{
public:
	// System.Uri System.Net.HttpWebResponse::uri
	Uri_t3269222095 * ___uri_1;
	// System.Net.WebHeaderCollection System.Net.HttpWebResponse::webHeaders
	WebHeaderCollection_t592894254 * ___webHeaders_2;
	// System.Net.CookieCollection System.Net.HttpWebResponse::cookieCollection
	CookieCollection_t1776842238 * ___cookieCollection_3;
	// System.String System.Net.HttpWebResponse::method
	String_t* ___method_4;
	// System.Version System.Net.HttpWebResponse::version
	Version_t2250829542 * ___version_5;
	// System.Net.HttpStatusCode System.Net.HttpWebResponse::statusCode
	int32_t ___statusCode_6;
	// System.String System.Net.HttpWebResponse::statusDescription
	String_t* ___statusDescription_7;
	// System.Int64 System.Net.HttpWebResponse::contentLength
	int64_t ___contentLength_8;
	// System.String System.Net.HttpWebResponse::contentType
	String_t* ___contentType_9;
	// System.Net.CookieContainer System.Net.HttpWebResponse::cookie_container
	CookieContainer_t390599126 * ___cookie_container_10;
	// System.Boolean System.Net.HttpWebResponse::disposed
	bool ___disposed_11;
	// System.IO.Stream System.Net.HttpWebResponse::stream
	Stream_t4138477179 * ___stream_12;
	// System.String[] System.Net.HttpWebResponse::cookieExpiresFormats
	StringU5BU5D_t2511808107* ___cookieExpiresFormats_13;

public:
	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___uri_1)); }
	inline Uri_t3269222095 * get_uri_1() const { return ___uri_1; }
	inline Uri_t3269222095 ** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(Uri_t3269222095 * value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_webHeaders_2() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___webHeaders_2)); }
	inline WebHeaderCollection_t592894254 * get_webHeaders_2() const { return ___webHeaders_2; }
	inline WebHeaderCollection_t592894254 ** get_address_of_webHeaders_2() { return &___webHeaders_2; }
	inline void set_webHeaders_2(WebHeaderCollection_t592894254 * value)
	{
		___webHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_2), value);
	}

	inline static int32_t get_offset_of_cookieCollection_3() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___cookieCollection_3)); }
	inline CookieCollection_t1776842238 * get_cookieCollection_3() const { return ___cookieCollection_3; }
	inline CookieCollection_t1776842238 ** get_address_of_cookieCollection_3() { return &___cookieCollection_3; }
	inline void set_cookieCollection_3(CookieCollection_t1776842238 * value)
	{
		___cookieCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___cookieCollection_3), value);
	}

	inline static int32_t get_offset_of_method_4() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___method_4)); }
	inline String_t* get_method_4() const { return ___method_4; }
	inline String_t** get_address_of_method_4() { return &___method_4; }
	inline void set_method_4(String_t* value)
	{
		___method_4 = value;
		Il2CppCodeGenWriteBarrier((&___method_4), value);
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___version_5)); }
	inline Version_t2250829542 * get_version_5() const { return ___version_5; }
	inline Version_t2250829542 ** get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(Version_t2250829542 * value)
	{
		___version_5 = value;
		Il2CppCodeGenWriteBarrier((&___version_5), value);
	}

	inline static int32_t get_offset_of_statusCode_6() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___statusCode_6)); }
	inline int32_t get_statusCode_6() const { return ___statusCode_6; }
	inline int32_t* get_address_of_statusCode_6() { return &___statusCode_6; }
	inline void set_statusCode_6(int32_t value)
	{
		___statusCode_6 = value;
	}

	inline static int32_t get_offset_of_statusDescription_7() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___statusDescription_7)); }
	inline String_t* get_statusDescription_7() const { return ___statusDescription_7; }
	inline String_t** get_address_of_statusDescription_7() { return &___statusDescription_7; }
	inline void set_statusDescription_7(String_t* value)
	{
		___statusDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_7), value);
	}

	inline static int32_t get_offset_of_contentLength_8() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___contentLength_8)); }
	inline int64_t get_contentLength_8() const { return ___contentLength_8; }
	inline int64_t* get_address_of_contentLength_8() { return &___contentLength_8; }
	inline void set_contentLength_8(int64_t value)
	{
		___contentLength_8 = value;
	}

	inline static int32_t get_offset_of_contentType_9() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___contentType_9)); }
	inline String_t* get_contentType_9() const { return ___contentType_9; }
	inline String_t** get_address_of_contentType_9() { return &___contentType_9; }
	inline void set_contentType_9(String_t* value)
	{
		___contentType_9 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_9), value);
	}

	inline static int32_t get_offset_of_cookie_container_10() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___cookie_container_10)); }
	inline CookieContainer_t390599126 * get_cookie_container_10() const { return ___cookie_container_10; }
	inline CookieContainer_t390599126 ** get_address_of_cookie_container_10() { return &___cookie_container_10; }
	inline void set_cookie_container_10(CookieContainer_t390599126 * value)
	{
		___cookie_container_10 = value;
		Il2CppCodeGenWriteBarrier((&___cookie_container_10), value);
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_stream_12() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___stream_12)); }
	inline Stream_t4138477179 * get_stream_12() const { return ___stream_12; }
	inline Stream_t4138477179 ** get_address_of_stream_12() { return &___stream_12; }
	inline void set_stream_12(Stream_t4138477179 * value)
	{
		___stream_12 = value;
		Il2CppCodeGenWriteBarrier((&___stream_12), value);
	}

	inline static int32_t get_offset_of_cookieExpiresFormats_13() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529, ___cookieExpiresFormats_13)); }
	inline StringU5BU5D_t2511808107* get_cookieExpiresFormats_13() const { return ___cookieExpiresFormats_13; }
	inline StringU5BU5D_t2511808107** get_address_of_cookieExpiresFormats_13() { return &___cookieExpiresFormats_13; }
	inline void set_cookieExpiresFormats_13(StringU5BU5D_t2511808107* value)
	{
		___cookieExpiresFormats_13 = value;
		Il2CppCodeGenWriteBarrier((&___cookieExpiresFormats_13), value);
	}
};

struct HttpWebResponse_t2352629529_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.HttpWebResponse::<>f__switch$map8
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map8_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_14() { return static_cast<int32_t>(offsetof(HttpWebResponse_t2352629529_StaticFields, ___U3CU3Ef__switchU24map8_14)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map8_14() const { return ___U3CU3Ef__switchU24map8_14; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map8_14() { return &___U3CU3Ef__switchU24map8_14; }
	inline void set_U3CU3Ef__switchU24map8_14(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map8_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBRESPONSE_T2352629529_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_T4070611298_H
#define PROTOCOLVIOLATIONEXCEPTION_T4070611298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t4070611298  : public InvalidOperationException_t3358289486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_T4070611298_H
#ifndef X509STORE_T2057865090_H
#define X509STORE_T2057865090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Store
struct  X509Store_t2057865090  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Store::_name
	String_t* ____name_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Store::_location
	int32_t ____location_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::list
	X509Certificate2Collection_t454981218 * ___list_2;
	// System.Security.Cryptography.X509Certificates.OpenFlags System.Security.Cryptography.X509Certificates.X509Store::_flags
	int32_t ____flags_3;
	// Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::store
	X509Store_t3451023606 * ___store_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(X509Store_t2057865090, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__location_1() { return static_cast<int32_t>(offsetof(X509Store_t2057865090, ____location_1)); }
	inline int32_t get__location_1() const { return ____location_1; }
	inline int32_t* get_address_of__location_1() { return &____location_1; }
	inline void set__location_1(int32_t value)
	{
		____location_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(X509Store_t2057865090, ___list_2)); }
	inline X509Certificate2Collection_t454981218 * get_list_2() const { return ___list_2; }
	inline X509Certificate2Collection_t454981218 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(X509Certificate2Collection_t454981218 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(X509Store_t2057865090, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(X509Store_t2057865090, ___store_4)); }
	inline X509Store_t3451023606 * get_store_4() const { return ___store_4; }
	inline X509Store_t3451023606 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(X509Store_t3451023606 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}
};

struct X509Store_t2057865090_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Store::<>f__switch$mapF
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapF_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_5() { return static_cast<int32_t>(offsetof(X509Store_t2057865090_StaticFields, ___U3CU3Ef__switchU24mapF_5)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapF_5() const { return ___U3CU3Ef__switchU24mapF_5; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapF_5() { return &___U3CU3Ef__switchU24mapF_5; }
	inline void set_U3CU3Ef__switchU24mapF_5(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapF_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T2057865090_H
#ifndef WEBCONNECTION_T3591103104_H
#define WEBCONNECTION_T3591103104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection
struct  WebConnection_t3591103104  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnection::sPoint
	ServicePoint_t3505702983 * ___sPoint_0;
	// System.IO.Stream System.Net.WebConnection::nstream
	Stream_t4138477179 * ___nstream_1;
	// System.Net.Sockets.Socket System.Net.WebConnection::socket
	Socket_t4168736505 * ___socket_2;
	// System.Object System.Net.WebConnection::socketLock
	RuntimeObject * ___socketLock_3;
	// System.Net.WebExceptionStatus System.Net.WebConnection::status
	int32_t ___status_4;
	// System.Threading.WaitCallback System.Net.WebConnection::initConn
	WaitCallback_t4234066651 * ___initConn_5;
	// System.Boolean System.Net.WebConnection::keepAlive
	bool ___keepAlive_6;
	// System.Byte[] System.Net.WebConnection::buffer
	ByteU5BU5D_t434619169* ___buffer_7;
	// System.EventHandler System.Net.WebConnection::abortHandler
	EventHandler_t369332490 * ___abortHandler_9;
	// System.Net.WebConnection/AbortHelper System.Net.WebConnection::abortHelper
	AbortHelper_t2979836766 * ___abortHelper_10;
	// System.Net.ReadState System.Net.WebConnection::readState
	int32_t ___readState_11;
	// System.Net.WebConnectionData System.Net.WebConnection::Data
	WebConnectionData_t1471135888 * ___Data_12;
	// System.Boolean System.Net.WebConnection::chunkedRead
	bool ___chunkedRead_13;
	// System.Net.ChunkStream System.Net.WebConnection::chunkStream
	ChunkStream_t262715251 * ___chunkStream_14;
	// System.Collections.Queue System.Net.WebConnection::queue
	Queue_t2654900338 * ___queue_15;
	// System.Boolean System.Net.WebConnection::reused
	bool ___reused_16;
	// System.Int32 System.Net.WebConnection::position
	int32_t ___position_17;
	// System.Boolean System.Net.WebConnection::busy
	bool ___busy_18;
	// System.Net.HttpWebRequest System.Net.WebConnection::priority_request
	HttpWebRequest_t4046343009 * ___priority_request_19;
	// System.Net.NetworkCredential System.Net.WebConnection::ntlm_credentials
	NetworkCredential_t1328334253 * ___ntlm_credentials_20;
	// System.Boolean System.Net.WebConnection::ntlm_authenticated
	bool ___ntlm_authenticated_21;
	// System.Boolean System.Net.WebConnection::unsafe_sharing
	bool ___unsafe_sharing_22;
	// System.Boolean System.Net.WebConnection::ssl
	bool ___ssl_23;
	// System.Boolean System.Net.WebConnection::certsAvailable
	bool ___certsAvailable_24;
	// System.Exception System.Net.WebConnection::connect_exception
	Exception_t4086964929 * ___connect_exception_25;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___sPoint_0)); }
	inline ServicePoint_t3505702983 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t3505702983 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t3505702983 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_nstream_1() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___nstream_1)); }
	inline Stream_t4138477179 * get_nstream_1() const { return ___nstream_1; }
	inline Stream_t4138477179 ** get_address_of_nstream_1() { return &___nstream_1; }
	inline void set_nstream_1(Stream_t4138477179 * value)
	{
		___nstream_1 = value;
		Il2CppCodeGenWriteBarrier((&___nstream_1), value);
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___socket_2)); }
	inline Socket_t4168736505 * get_socket_2() const { return ___socket_2; }
	inline Socket_t4168736505 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t4168736505 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_socketLock_3() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___socketLock_3)); }
	inline RuntimeObject * get_socketLock_3() const { return ___socketLock_3; }
	inline RuntimeObject ** get_address_of_socketLock_3() { return &___socketLock_3; }
	inline void set_socketLock_3(RuntimeObject * value)
	{
		___socketLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___socketLock_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_initConn_5() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___initConn_5)); }
	inline WaitCallback_t4234066651 * get_initConn_5() const { return ___initConn_5; }
	inline WaitCallback_t4234066651 ** get_address_of_initConn_5() { return &___initConn_5; }
	inline void set_initConn_5(WaitCallback_t4234066651 * value)
	{
		___initConn_5 = value;
		Il2CppCodeGenWriteBarrier((&___initConn_5), value);
	}

	inline static int32_t get_offset_of_keepAlive_6() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___keepAlive_6)); }
	inline bool get_keepAlive_6() const { return ___keepAlive_6; }
	inline bool* get_address_of_keepAlive_6() { return &___keepAlive_6; }
	inline void set_keepAlive_6(bool value)
	{
		___keepAlive_6 = value;
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___buffer_7)); }
	inline ByteU5BU5D_t434619169* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_t434619169** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_t434619169* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_abortHandler_9() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___abortHandler_9)); }
	inline EventHandler_t369332490 * get_abortHandler_9() const { return ___abortHandler_9; }
	inline EventHandler_t369332490 ** get_address_of_abortHandler_9() { return &___abortHandler_9; }
	inline void set_abortHandler_9(EventHandler_t369332490 * value)
	{
		___abortHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_9), value);
	}

	inline static int32_t get_offset_of_abortHelper_10() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___abortHelper_10)); }
	inline AbortHelper_t2979836766 * get_abortHelper_10() const { return ___abortHelper_10; }
	inline AbortHelper_t2979836766 ** get_address_of_abortHelper_10() { return &___abortHelper_10; }
	inline void set_abortHelper_10(AbortHelper_t2979836766 * value)
	{
		___abortHelper_10 = value;
		Il2CppCodeGenWriteBarrier((&___abortHelper_10), value);
	}

	inline static int32_t get_offset_of_readState_11() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___readState_11)); }
	inline int32_t get_readState_11() const { return ___readState_11; }
	inline int32_t* get_address_of_readState_11() { return &___readState_11; }
	inline void set_readState_11(int32_t value)
	{
		___readState_11 = value;
	}

	inline static int32_t get_offset_of_Data_12() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___Data_12)); }
	inline WebConnectionData_t1471135888 * get_Data_12() const { return ___Data_12; }
	inline WebConnectionData_t1471135888 ** get_address_of_Data_12() { return &___Data_12; }
	inline void set_Data_12(WebConnectionData_t1471135888 * value)
	{
		___Data_12 = value;
		Il2CppCodeGenWriteBarrier((&___Data_12), value);
	}

	inline static int32_t get_offset_of_chunkedRead_13() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___chunkedRead_13)); }
	inline bool get_chunkedRead_13() const { return ___chunkedRead_13; }
	inline bool* get_address_of_chunkedRead_13() { return &___chunkedRead_13; }
	inline void set_chunkedRead_13(bool value)
	{
		___chunkedRead_13 = value;
	}

	inline static int32_t get_offset_of_chunkStream_14() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___chunkStream_14)); }
	inline ChunkStream_t262715251 * get_chunkStream_14() const { return ___chunkStream_14; }
	inline ChunkStream_t262715251 ** get_address_of_chunkStream_14() { return &___chunkStream_14; }
	inline void set_chunkStream_14(ChunkStream_t262715251 * value)
	{
		___chunkStream_14 = value;
		Il2CppCodeGenWriteBarrier((&___chunkStream_14), value);
	}

	inline static int32_t get_offset_of_queue_15() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___queue_15)); }
	inline Queue_t2654900338 * get_queue_15() const { return ___queue_15; }
	inline Queue_t2654900338 ** get_address_of_queue_15() { return &___queue_15; }
	inline void set_queue_15(Queue_t2654900338 * value)
	{
		___queue_15 = value;
		Il2CppCodeGenWriteBarrier((&___queue_15), value);
	}

	inline static int32_t get_offset_of_reused_16() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___reused_16)); }
	inline bool get_reused_16() const { return ___reused_16; }
	inline bool* get_address_of_reused_16() { return &___reused_16; }
	inline void set_reused_16(bool value)
	{
		___reused_16 = value;
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___position_17)); }
	inline int32_t get_position_17() const { return ___position_17; }
	inline int32_t* get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(int32_t value)
	{
		___position_17 = value;
	}

	inline static int32_t get_offset_of_busy_18() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___busy_18)); }
	inline bool get_busy_18() const { return ___busy_18; }
	inline bool* get_address_of_busy_18() { return &___busy_18; }
	inline void set_busy_18(bool value)
	{
		___busy_18 = value;
	}

	inline static int32_t get_offset_of_priority_request_19() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___priority_request_19)); }
	inline HttpWebRequest_t4046343009 * get_priority_request_19() const { return ___priority_request_19; }
	inline HttpWebRequest_t4046343009 ** get_address_of_priority_request_19() { return &___priority_request_19; }
	inline void set_priority_request_19(HttpWebRequest_t4046343009 * value)
	{
		___priority_request_19 = value;
		Il2CppCodeGenWriteBarrier((&___priority_request_19), value);
	}

	inline static int32_t get_offset_of_ntlm_credentials_20() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___ntlm_credentials_20)); }
	inline NetworkCredential_t1328334253 * get_ntlm_credentials_20() const { return ___ntlm_credentials_20; }
	inline NetworkCredential_t1328334253 ** get_address_of_ntlm_credentials_20() { return &___ntlm_credentials_20; }
	inline void set_ntlm_credentials_20(NetworkCredential_t1328334253 * value)
	{
		___ntlm_credentials_20 = value;
		Il2CppCodeGenWriteBarrier((&___ntlm_credentials_20), value);
	}

	inline static int32_t get_offset_of_ntlm_authenticated_21() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___ntlm_authenticated_21)); }
	inline bool get_ntlm_authenticated_21() const { return ___ntlm_authenticated_21; }
	inline bool* get_address_of_ntlm_authenticated_21() { return &___ntlm_authenticated_21; }
	inline void set_ntlm_authenticated_21(bool value)
	{
		___ntlm_authenticated_21 = value;
	}

	inline static int32_t get_offset_of_unsafe_sharing_22() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___unsafe_sharing_22)); }
	inline bool get_unsafe_sharing_22() const { return ___unsafe_sharing_22; }
	inline bool* get_address_of_unsafe_sharing_22() { return &___unsafe_sharing_22; }
	inline void set_unsafe_sharing_22(bool value)
	{
		___unsafe_sharing_22 = value;
	}

	inline static int32_t get_offset_of_ssl_23() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___ssl_23)); }
	inline bool get_ssl_23() const { return ___ssl_23; }
	inline bool* get_address_of_ssl_23() { return &___ssl_23; }
	inline void set_ssl_23(bool value)
	{
		___ssl_23 = value;
	}

	inline static int32_t get_offset_of_certsAvailable_24() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___certsAvailable_24)); }
	inline bool get_certsAvailable_24() const { return ___certsAvailable_24; }
	inline bool* get_address_of_certsAvailable_24() { return &___certsAvailable_24; }
	inline void set_certsAvailable_24(bool value)
	{
		___certsAvailable_24 = value;
	}

	inline static int32_t get_offset_of_connect_exception_25() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104, ___connect_exception_25)); }
	inline Exception_t4086964929 * get_connect_exception_25() const { return ___connect_exception_25; }
	inline Exception_t4086964929 ** get_address_of_connect_exception_25() { return &___connect_exception_25; }
	inline void set_connect_exception_25(Exception_t4086964929 * value)
	{
		___connect_exception_25 = value;
		Il2CppCodeGenWriteBarrier((&___connect_exception_25), value);
	}
};

struct WebConnection_t3591103104_StaticFields
{
public:
	// System.AsyncCallback System.Net.WebConnection::readDoneDelegate
	AsyncCallback_t3561663063 * ___readDoneDelegate_8;
	// System.Object System.Net.WebConnection::classLock
	RuntimeObject * ___classLock_26;
	// System.Type System.Net.WebConnection::sslStream
	Type_t * ___sslStream_27;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piClient
	PropertyInfo_t * ___piClient_28;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piServer
	PropertyInfo_t * ___piServer_29;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piTrustFailure
	PropertyInfo_t * ___piTrustFailure_30;
	// System.Reflection.MethodInfo System.Net.WebConnection::method_GetSecurityPolicyFromNonMainThread
	MethodInfo_t * ___method_GetSecurityPolicyFromNonMainThread_31;

public:
	inline static int32_t get_offset_of_readDoneDelegate_8() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___readDoneDelegate_8)); }
	inline AsyncCallback_t3561663063 * get_readDoneDelegate_8() const { return ___readDoneDelegate_8; }
	inline AsyncCallback_t3561663063 ** get_address_of_readDoneDelegate_8() { return &___readDoneDelegate_8; }
	inline void set_readDoneDelegate_8(AsyncCallback_t3561663063 * value)
	{
		___readDoneDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___readDoneDelegate_8), value);
	}

	inline static int32_t get_offset_of_classLock_26() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___classLock_26)); }
	inline RuntimeObject * get_classLock_26() const { return ___classLock_26; }
	inline RuntimeObject ** get_address_of_classLock_26() { return &___classLock_26; }
	inline void set_classLock_26(RuntimeObject * value)
	{
		___classLock_26 = value;
		Il2CppCodeGenWriteBarrier((&___classLock_26), value);
	}

	inline static int32_t get_offset_of_sslStream_27() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___sslStream_27)); }
	inline Type_t * get_sslStream_27() const { return ___sslStream_27; }
	inline Type_t ** get_address_of_sslStream_27() { return &___sslStream_27; }
	inline void set_sslStream_27(Type_t * value)
	{
		___sslStream_27 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_27), value);
	}

	inline static int32_t get_offset_of_piClient_28() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___piClient_28)); }
	inline PropertyInfo_t * get_piClient_28() const { return ___piClient_28; }
	inline PropertyInfo_t ** get_address_of_piClient_28() { return &___piClient_28; }
	inline void set_piClient_28(PropertyInfo_t * value)
	{
		___piClient_28 = value;
		Il2CppCodeGenWriteBarrier((&___piClient_28), value);
	}

	inline static int32_t get_offset_of_piServer_29() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___piServer_29)); }
	inline PropertyInfo_t * get_piServer_29() const { return ___piServer_29; }
	inline PropertyInfo_t ** get_address_of_piServer_29() { return &___piServer_29; }
	inline void set_piServer_29(PropertyInfo_t * value)
	{
		___piServer_29 = value;
		Il2CppCodeGenWriteBarrier((&___piServer_29), value);
	}

	inline static int32_t get_offset_of_piTrustFailure_30() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___piTrustFailure_30)); }
	inline PropertyInfo_t * get_piTrustFailure_30() const { return ___piTrustFailure_30; }
	inline PropertyInfo_t ** get_address_of_piTrustFailure_30() { return &___piTrustFailure_30; }
	inline void set_piTrustFailure_30(PropertyInfo_t * value)
	{
		___piTrustFailure_30 = value;
		Il2CppCodeGenWriteBarrier((&___piTrustFailure_30), value);
	}

	inline static int32_t get_offset_of_method_GetSecurityPolicyFromNonMainThread_31() { return static_cast<int32_t>(offsetof(WebConnection_t3591103104_StaticFields, ___method_GetSecurityPolicyFromNonMainThread_31)); }
	inline MethodInfo_t * get_method_GetSecurityPolicyFromNonMainThread_31() const { return ___method_GetSecurityPolicyFromNonMainThread_31; }
	inline MethodInfo_t ** get_address_of_method_GetSecurityPolicyFromNonMainThread_31() { return &___method_GetSecurityPolicyFromNonMainThread_31; }
	inline void set_method_GetSecurityPolicyFromNonMainThread_31(MethodInfo_t * value)
	{
		___method_GetSecurityPolicyFromNonMainThread_31 = value;
		Il2CppCodeGenWriteBarrier((&___method_GetSecurityPolicyFromNonMainThread_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTION_T3591103104_H
#ifndef X509SUBJECTKEYIDENTIFIEREXTENSION_T2725568395_H
#define X509SUBJECTKEYIDENTIFIEREXTENSION_T2725568395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
struct  X509SubjectKeyIdentifierExtension_t2725568395  : public X509Extension_t1338557205
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_subjectKeyIdentifier
	ByteU5BU5D_t434619169* ____subjectKeyIdentifier_6;
	// System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_ski
	String_t* ____ski_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__subjectKeyIdentifier_6() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2725568395, ____subjectKeyIdentifier_6)); }
	inline ByteU5BU5D_t434619169* get__subjectKeyIdentifier_6() const { return ____subjectKeyIdentifier_6; }
	inline ByteU5BU5D_t434619169** get_address_of__subjectKeyIdentifier_6() { return &____subjectKeyIdentifier_6; }
	inline void set__subjectKeyIdentifier_6(ByteU5BU5D_t434619169* value)
	{
		____subjectKeyIdentifier_6 = value;
		Il2CppCodeGenWriteBarrier((&____subjectKeyIdentifier_6), value);
	}

	inline static int32_t get_offset_of__ski_7() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2725568395, ____ski_7)); }
	inline String_t* get__ski_7() const { return ____ski_7; }
	inline String_t** get_address_of__ski_7() { return &____ski_7; }
	inline void set__ski_7(String_t* value)
	{
		____ski_7 = value;
		Il2CppCodeGenWriteBarrier((&____ski_7), value);
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2725568395, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIEREXTENSION_T2725568395_H
#ifndef FTPSTATUS_T4241934835_H
#define FTPSTATUS_T4241934835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatus
struct  FtpStatus_t4241934835  : public RuntimeObject
{
public:
	// System.Net.FtpStatusCode System.Net.FtpStatus::statusCode
	int32_t ___statusCode_0;
	// System.String System.Net.FtpStatus::statusDescription
	String_t* ___statusDescription_1;

public:
	inline static int32_t get_offset_of_statusCode_0() { return static_cast<int32_t>(offsetof(FtpStatus_t4241934835, ___statusCode_0)); }
	inline int32_t get_statusCode_0() const { return ___statusCode_0; }
	inline int32_t* get_address_of_statusCode_0() { return &___statusCode_0; }
	inline void set_statusCode_0(int32_t value)
	{
		___statusCode_0 = value;
	}

	inline static int32_t get_offset_of_statusDescription_1() { return static_cast<int32_t>(offsetof(FtpStatus_t4241934835, ___statusDescription_1)); }
	inline String_t* get_statusDescription_1() const { return ___statusDescription_1; }
	inline String_t** get_address_of_statusDescription_1() { return &___statusDescription_1; }
	inline void set_statusDescription_1(String_t* value)
	{
		___statusDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUS_T4241934835_H
#ifndef COOKIEEXCEPTION_T2233408878_H
#define COOKIEEXCEPTION_T2233408878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieException
struct  CookieException_t2233408878  : public FormatException_t501182138
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T2233408878_H
#ifndef WEBEXCEPTION_T321944699_H
#define WEBEXCEPTION_T321944699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_t321944699  : public InvalidOperationException_t3358289486
{
public:
	// System.Net.WebResponse System.Net.WebException::response
	WebResponse_t3616229315 * ___response_12;
	// System.Net.WebExceptionStatus System.Net.WebException::status
	int32_t ___status_13;

public:
	inline static int32_t get_offset_of_response_12() { return static_cast<int32_t>(offsetof(WebException_t321944699, ___response_12)); }
	inline WebResponse_t3616229315 * get_response_12() const { return ___response_12; }
	inline WebResponse_t3616229315 ** get_address_of_response_12() { return &___response_12; }
	inline void set_response_12(WebResponse_t3616229315 * value)
	{
		___response_12 = value;
		Il2CppCodeGenWriteBarrier((&___response_12), value);
	}

	inline static int32_t get_offset_of_status_13() { return static_cast<int32_t>(offsetof(WebException_t321944699, ___status_13)); }
	inline int32_t get_status_13() const { return ___status_13; }
	inline int32_t* get_address_of_status_13() { return &___status_13; }
	inline void set_status_13(int32_t value)
	{
		___status_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_T321944699_H
#ifndef X509KEYUSAGEEXTENSION_T2457957465_H
#define X509KEYUSAGEEXTENSION_T2457957465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
struct  X509KeyUsageExtension_t2457957465  : public X509Extension_t1338557205
{
public:
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_keyUsages
	int32_t ____keyUsages_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__keyUsages_7() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t2457957465, ____keyUsages_7)); }
	inline int32_t get__keyUsages_7() const { return ____keyUsages_7; }
	inline int32_t* get_address_of__keyUsages_7() { return &____keyUsages_7; }
	inline void set__keyUsages_7(int32_t value)
	{
		____keyUsages_7 = value;
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t2457957465, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEEXTENSION_T2457957465_H
#ifndef WEBREQUEST_T1788933422_H
#define WEBREQUEST_T1788933422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t1788933422  : public MarshalByRefObject_t1482034164
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::authentication_level
	int32_t ___authentication_level_4;

public:
	inline static int32_t get_offset_of_authentication_level_4() { return static_cast<int32_t>(offsetof(WebRequest_t1788933422, ___authentication_level_4)); }
	inline int32_t get_authentication_level_4() const { return ___authentication_level_4; }
	inline int32_t* get_address_of_authentication_level_4() { return &___authentication_level_4; }
	inline void set_authentication_level_4(int32_t value)
	{
		___authentication_level_4 = value;
	}
};

struct WebRequest_t1788933422_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.WebRequest::prefixes
	HybridDictionary_t439385300 * ___prefixes_1;
	// System.Boolean System.Net.WebRequest::isDefaultWebProxySet
	bool ___isDefaultWebProxySet_2;
	// System.Net.IWebProxy System.Net.WebRequest::defaultWebProxy
	RuntimeObject* ___defaultWebProxy_3;
	// System.Object System.Net.WebRequest::lockobj
	RuntimeObject * ___lockobj_5;

public:
	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(WebRequest_t1788933422_StaticFields, ___prefixes_1)); }
	inline HybridDictionary_t439385300 * get_prefixes_1() const { return ___prefixes_1; }
	inline HybridDictionary_t439385300 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(HybridDictionary_t439385300 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_isDefaultWebProxySet_2() { return static_cast<int32_t>(offsetof(WebRequest_t1788933422_StaticFields, ___isDefaultWebProxySet_2)); }
	inline bool get_isDefaultWebProxySet_2() const { return ___isDefaultWebProxySet_2; }
	inline bool* get_address_of_isDefaultWebProxySet_2() { return &___isDefaultWebProxySet_2; }
	inline void set_isDefaultWebProxySet_2(bool value)
	{
		___isDefaultWebProxySet_2 = value;
	}

	inline static int32_t get_offset_of_defaultWebProxy_3() { return static_cast<int32_t>(offsetof(WebRequest_t1788933422_StaticFields, ___defaultWebProxy_3)); }
	inline RuntimeObject* get_defaultWebProxy_3() const { return ___defaultWebProxy_3; }
	inline RuntimeObject** get_address_of_defaultWebProxy_3() { return &___defaultWebProxy_3; }
	inline void set_defaultWebProxy_3(RuntimeObject* value)
	{
		___defaultWebProxy_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultWebProxy_3), value);
	}

	inline static int32_t get_offset_of_lockobj_5() { return static_cast<int32_t>(offsetof(WebRequest_t1788933422_StaticFields, ___lockobj_5)); }
	inline RuntimeObject * get_lockobj_5() const { return ___lockobj_5; }
	inline RuntimeObject ** get_address_of_lockobj_5() { return &___lockobj_5; }
	inline void set_lockobj_5(RuntimeObject * value)
	{
		___lockobj_5 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T1788933422_H
#ifndef X509ENHANCEDKEYUSAGEEXTENSION_T706278074_H
#define X509ENHANCEDKEYUSAGEEXTENSION_T706278074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
struct  X509EnhancedKeyUsageExtension_t706278074  : public X509Extension_t1338557205
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_enhKeyUsage
	OidCollection_t678319162 * ____enhKeyUsage_4;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_status
	int32_t ____status_5;

public:
	inline static int32_t get_offset_of__enhKeyUsage_4() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t706278074, ____enhKeyUsage_4)); }
	inline OidCollection_t678319162 * get__enhKeyUsage_4() const { return ____enhKeyUsage_4; }
	inline OidCollection_t678319162 ** get_address_of__enhKeyUsage_4() { return &____enhKeyUsage_4; }
	inline void set__enhKeyUsage_4(OidCollection_t678319162 * value)
	{
		____enhKeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&____enhKeyUsage_4), value);
	}

	inline static int32_t get_offset_of__status_5() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t706278074, ____status_5)); }
	inline int32_t get__status_5() const { return ____status_5; }
	inline int32_t* get_address_of__status_5() { return &____status_5; }
	inline void set__status_5(int32_t value)
	{
		____status_5 = value;
	}
};

struct X509EnhancedKeyUsageExtension_t706278074_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::<>f__switch$mapE
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapE_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_6() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t706278074_StaticFields, ___U3CU3Ef__switchU24mapE_6)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapE_6() const { return ___U3CU3Ef__switchU24mapE_6; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapE_6() { return &___U3CU3Ef__switchU24mapE_6; }
	inline void set_U3CU3Ef__switchU24mapE_6(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapE_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapE_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ENHANCEDKEYUSAGEEXTENSION_T706278074_H
#ifndef X509CHAINSTATUS_T3602726638_H
#define X509CHAINSTATUS_T3602726638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t3602726638 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t3602726638, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t3602726638, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t3602726638_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t3602726638_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T3602726638_H
#ifndef X509CHAINELEMENT_T1254084101_H
#define X509CHAINELEMENT_T1254084101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElement
struct  X509ChainElement_t1254084101  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainElement::certificate
	X509Certificate2_t1403970469 * ___certificate_0;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainElement::status
	X509ChainStatusU5BU5D_t4062409115* ___status_1;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainElement::info
	String_t* ___info_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainElement::compressed_status_flags
	int32_t ___compressed_status_flags_3;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(X509ChainElement_t1254084101, ___certificate_0)); }
	inline X509Certificate2_t1403970469 * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate2_t1403970469 ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate2_t1403970469 * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(X509ChainElement_t1254084101, ___status_1)); }
	inline X509ChainStatusU5BU5D_t4062409115* get_status_1() const { return ___status_1; }
	inline X509ChainStatusU5BU5D_t4062409115** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(X509ChainStatusU5BU5D_t4062409115* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(X509ChainElement_t1254084101, ___info_2)); }
	inline String_t* get_info_2() const { return ___info_2; }
	inline String_t** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(String_t* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_compressed_status_flags_3() { return static_cast<int32_t>(offsetof(X509ChainElement_t1254084101, ___compressed_status_flags_3)); }
	inline int32_t get_compressed_status_flags_3() const { return ___compressed_status_flags_3; }
	inline int32_t* get_address_of_compressed_status_flags_3() { return &___compressed_status_flags_3; }
	inline void set_compressed_status_flags_3(int32_t value)
	{
		___compressed_status_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENT_T1254084101_H
#ifndef X509CHAIN_T388833386_H
#define X509CHAIN_T388833386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t388833386  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Chain::location
	int32_t ___location_0;
	// System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509Chain::elements
	X509ChainElementCollection_t2987578632 * ___elements_1;
	// System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509Chain::policy
	X509ChainPolicy_t45074389 * ___policy_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::status
	X509ChainStatusU5BU5D_t4062409115* ___status_3;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509Chain::max_path_length
	int32_t ___max_path_length_5;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Chain::working_issuer_name
	X500DistinguishedName_t572620573 * ___working_issuer_name_6;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Chain::working_public_key
	AsymmetricAlgorithm_t4165028003 * ___working_public_key_7;
	// System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509Chain::bce_restriction
	X509ChainElement_t1254084101 * ___bce_restriction_8;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::roots
	X509Store_t2057865090 * ___roots_9;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::cas
	X509Store_t2057865090 * ___cas_10;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Chain::collection
	X509Certificate2Collection_t454981218 * ___collection_11;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___location_0)); }
	inline int32_t get_location_0() const { return ___location_0; }
	inline int32_t* get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(int32_t value)
	{
		___location_0 = value;
	}

	inline static int32_t get_offset_of_elements_1() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___elements_1)); }
	inline X509ChainElementCollection_t2987578632 * get_elements_1() const { return ___elements_1; }
	inline X509ChainElementCollection_t2987578632 ** get_address_of_elements_1() { return &___elements_1; }
	inline void set_elements_1(X509ChainElementCollection_t2987578632 * value)
	{
		___elements_1 = value;
		Il2CppCodeGenWriteBarrier((&___elements_1), value);
	}

	inline static int32_t get_offset_of_policy_2() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___policy_2)); }
	inline X509ChainPolicy_t45074389 * get_policy_2() const { return ___policy_2; }
	inline X509ChainPolicy_t45074389 ** get_address_of_policy_2() { return &___policy_2; }
	inline void set_policy_2(X509ChainPolicy_t45074389 * value)
	{
		___policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___policy_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___status_3)); }
	inline X509ChainStatusU5BU5D_t4062409115* get_status_3() const { return ___status_3; }
	inline X509ChainStatusU5BU5D_t4062409115** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(X509ChainStatusU5BU5D_t4062409115* value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_max_path_length_5() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___max_path_length_5)); }
	inline int32_t get_max_path_length_5() const { return ___max_path_length_5; }
	inline int32_t* get_address_of_max_path_length_5() { return &___max_path_length_5; }
	inline void set_max_path_length_5(int32_t value)
	{
		___max_path_length_5 = value;
	}

	inline static int32_t get_offset_of_working_issuer_name_6() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___working_issuer_name_6)); }
	inline X500DistinguishedName_t572620573 * get_working_issuer_name_6() const { return ___working_issuer_name_6; }
	inline X500DistinguishedName_t572620573 ** get_address_of_working_issuer_name_6() { return &___working_issuer_name_6; }
	inline void set_working_issuer_name_6(X500DistinguishedName_t572620573 * value)
	{
		___working_issuer_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___working_issuer_name_6), value);
	}

	inline static int32_t get_offset_of_working_public_key_7() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___working_public_key_7)); }
	inline AsymmetricAlgorithm_t4165028003 * get_working_public_key_7() const { return ___working_public_key_7; }
	inline AsymmetricAlgorithm_t4165028003 ** get_address_of_working_public_key_7() { return &___working_public_key_7; }
	inline void set_working_public_key_7(AsymmetricAlgorithm_t4165028003 * value)
	{
		___working_public_key_7 = value;
		Il2CppCodeGenWriteBarrier((&___working_public_key_7), value);
	}

	inline static int32_t get_offset_of_bce_restriction_8() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___bce_restriction_8)); }
	inline X509ChainElement_t1254084101 * get_bce_restriction_8() const { return ___bce_restriction_8; }
	inline X509ChainElement_t1254084101 ** get_address_of_bce_restriction_8() { return &___bce_restriction_8; }
	inline void set_bce_restriction_8(X509ChainElement_t1254084101 * value)
	{
		___bce_restriction_8 = value;
		Il2CppCodeGenWriteBarrier((&___bce_restriction_8), value);
	}

	inline static int32_t get_offset_of_roots_9() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___roots_9)); }
	inline X509Store_t2057865090 * get_roots_9() const { return ___roots_9; }
	inline X509Store_t2057865090 ** get_address_of_roots_9() { return &___roots_9; }
	inline void set_roots_9(X509Store_t2057865090 * value)
	{
		___roots_9 = value;
		Il2CppCodeGenWriteBarrier((&___roots_9), value);
	}

	inline static int32_t get_offset_of_cas_10() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___cas_10)); }
	inline X509Store_t2057865090 * get_cas_10() const { return ___cas_10; }
	inline X509Store_t2057865090 ** get_address_of_cas_10() { return &___cas_10; }
	inline void set_cas_10(X509Store_t2057865090 * value)
	{
		___cas_10 = value;
		Il2CppCodeGenWriteBarrier((&___cas_10), value);
	}

	inline static int32_t get_offset_of_collection_11() { return static_cast<int32_t>(offsetof(X509Chain_t388833386, ___collection_11)); }
	inline X509Certificate2Collection_t454981218 * get_collection_11() const { return ___collection_11; }
	inline X509Certificate2Collection_t454981218 ** get_address_of_collection_11() { return &___collection_11; }
	inline void set_collection_11(X509Certificate2Collection_t454981218 * value)
	{
		___collection_11 = value;
		Il2CppCodeGenWriteBarrier((&___collection_11), value);
	}
};

struct X509Chain_t388833386_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::Empty
	X509ChainStatusU5BU5D_t4062409115* ___Empty_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapB
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapB_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapC
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapC_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapD
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapD_14;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(X509Chain_t388833386_StaticFields, ___Empty_4)); }
	inline X509ChainStatusU5BU5D_t4062409115* get_Empty_4() const { return ___Empty_4; }
	inline X509ChainStatusU5BU5D_t4062409115** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(X509ChainStatusU5BU5D_t4062409115* value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_12() { return static_cast<int32_t>(offsetof(X509Chain_t388833386_StaticFields, ___U3CU3Ef__switchU24mapB_12)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapB_12() const { return ___U3CU3Ef__switchU24mapB_12; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapB_12() { return &___U3CU3Ef__switchU24mapB_12; }
	inline void set_U3CU3Ef__switchU24mapB_12(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapB_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_13() { return static_cast<int32_t>(offsetof(X509Chain_t388833386_StaticFields, ___U3CU3Ef__switchU24mapC_13)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapC_13() const { return ___U3CU3Ef__switchU24mapC_13; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapC_13() { return &___U3CU3Ef__switchU24mapC_13; }
	inline void set_U3CU3Ef__switchU24mapC_13(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapC_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_14() { return static_cast<int32_t>(offsetof(X509Chain_t388833386_StaticFields, ___U3CU3Ef__switchU24mapD_14)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapD_14() const { return ___U3CU3Ef__switchU24mapD_14; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapD_14() { return &___U3CU3Ef__switchU24mapD_14; }
	inline void set_U3CU3Ef__switchU24mapD_14(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapD_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapD_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T388833386_H
#ifndef CHAINVALIDATIONHELPER_T1757992727_H
#define CHAINVALIDATIONHELPER_T1757992727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/ChainValidationHelper
struct  ChainValidationHelper_t1757992727  : public RuntimeObject
{
public:
	// System.Object System.Net.ServicePointManager/ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// System.String System.Net.ServicePointManager/ChainValidationHelper::host
	String_t* ___host_1;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t1757992727, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t1757992727, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}
};

struct ChainValidationHelper_t1757992727_StaticFields
{
public:
	// System.Boolean System.Net.ServicePointManager/ChainValidationHelper::is_macosx
	bool ___is_macosx_2;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Net.ServicePointManager/ChainValidationHelper::s_flags
	int32_t ___s_flags_3;

public:
	inline static int32_t get_offset_of_is_macosx_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t1757992727_StaticFields, ___is_macosx_2)); }
	inline bool get_is_macosx_2() const { return ___is_macosx_2; }
	inline bool* get_address_of_is_macosx_2() { return &___is_macosx_2; }
	inline void set_is_macosx_2(bool value)
	{
		___is_macosx_2 = value;
	}

	inline static int32_t get_offset_of_s_flags_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t1757992727_StaticFields, ___s_flags_3)); }
	inline int32_t get_s_flags_3() const { return ___s_flags_3; }
	inline int32_t* get_address_of_s_flags_3() { return &___s_flags_3; }
	inline void set_s_flags_3(int32_t value)
	{
		___s_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T1757992727_H
#ifndef X509BASICCONSTRAINTSEXTENSION_T1243739534_H
#define X509BASICCONSTRAINTSEXTENSION_T1243739534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
struct  X509BasicConstraintsExtension_t1243739534  : public X509Extension_t1338557205
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_certificateAuthority
	bool ____certificateAuthority_6;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_hasPathLengthConstraint
	bool ____hasPathLengthConstraint_7;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_pathLengthConstraint
	int32_t ____pathLengthConstraint_8;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_status
	int32_t ____status_9;

public:
	inline static int32_t get_offset_of__certificateAuthority_6() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1243739534, ____certificateAuthority_6)); }
	inline bool get__certificateAuthority_6() const { return ____certificateAuthority_6; }
	inline bool* get_address_of__certificateAuthority_6() { return &____certificateAuthority_6; }
	inline void set__certificateAuthority_6(bool value)
	{
		____certificateAuthority_6 = value;
	}

	inline static int32_t get_offset_of__hasPathLengthConstraint_7() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1243739534, ____hasPathLengthConstraint_7)); }
	inline bool get__hasPathLengthConstraint_7() const { return ____hasPathLengthConstraint_7; }
	inline bool* get_address_of__hasPathLengthConstraint_7() { return &____hasPathLengthConstraint_7; }
	inline void set__hasPathLengthConstraint_7(bool value)
	{
		____hasPathLengthConstraint_7 = value;
	}

	inline static int32_t get_offset_of__pathLengthConstraint_8() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1243739534, ____pathLengthConstraint_8)); }
	inline int32_t get__pathLengthConstraint_8() const { return ____pathLengthConstraint_8; }
	inline int32_t* get_address_of__pathLengthConstraint_8() { return &____pathLengthConstraint_8; }
	inline void set__pathLengthConstraint_8(int32_t value)
	{
		____pathLengthConstraint_8 = value;
	}

	inline static int32_t get_offset_of__status_9() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1243739534, ____status_9)); }
	inline int32_t get__status_9() const { return ____status_9; }
	inline int32_t* get_address_of__status_9() { return &____status_9; }
	inline void set__status_9(int32_t value)
	{
		____status_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509BASICCONSTRAINTSEXTENSION_T1243739534_H
#ifndef GETRESPONSECALLBACK_T1254250368_H
#define GETRESPONSECALLBACK_T1254250368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest/GetResponseCallback
struct  GetResponseCallback_t1254250368  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRESPONSECALLBACK_T1254250368_H
#ifndef FILEWEBREQUEST_T3154753421_H
#define FILEWEBREQUEST_T3154753421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest
struct  FileWebRequest_t3154753421  : public WebRequest_t1788933422
{
public:
	// System.Uri System.Net.FileWebRequest::uri
	Uri_t3269222095 * ___uri_6;
	// System.Net.WebHeaderCollection System.Net.FileWebRequest::webHeaders
	WebHeaderCollection_t592894254 * ___webHeaders_7;
	// System.Net.ICredentials System.Net.FileWebRequest::credentials
	RuntimeObject* ___credentials_8;
	// System.String System.Net.FileWebRequest::connectionGroup
	String_t* ___connectionGroup_9;
	// System.Int64 System.Net.FileWebRequest::contentLength
	int64_t ___contentLength_10;
	// System.IO.FileAccess System.Net.FileWebRequest::fileAccess
	int32_t ___fileAccess_11;
	// System.String System.Net.FileWebRequest::method
	String_t* ___method_12;
	// System.Net.IWebProxy System.Net.FileWebRequest::proxy
	RuntimeObject* ___proxy_13;
	// System.Boolean System.Net.FileWebRequest::preAuthenticate
	bool ___preAuthenticate_14;
	// System.Int32 System.Net.FileWebRequest::timeout
	int32_t ___timeout_15;
	// System.Net.FileWebResponse System.Net.FileWebRequest::webResponse
	FileWebResponse_t982058165 * ___webResponse_16;
	// System.Threading.AutoResetEvent System.Net.FileWebRequest::requestEndEvent
	AutoResetEvent_t1210004121 * ___requestEndEvent_17;
	// System.Boolean System.Net.FileWebRequest::requesting
	bool ___requesting_18;
	// System.Boolean System.Net.FileWebRequest::asyncResponding
	bool ___asyncResponding_19;

public:
	inline static int32_t get_offset_of_uri_6() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___uri_6)); }
	inline Uri_t3269222095 * get_uri_6() const { return ___uri_6; }
	inline Uri_t3269222095 ** get_address_of_uri_6() { return &___uri_6; }
	inline void set_uri_6(Uri_t3269222095 * value)
	{
		___uri_6 = value;
		Il2CppCodeGenWriteBarrier((&___uri_6), value);
	}

	inline static int32_t get_offset_of_webHeaders_7() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___webHeaders_7)); }
	inline WebHeaderCollection_t592894254 * get_webHeaders_7() const { return ___webHeaders_7; }
	inline WebHeaderCollection_t592894254 ** get_address_of_webHeaders_7() { return &___webHeaders_7; }
	inline void set_webHeaders_7(WebHeaderCollection_t592894254 * value)
	{
		___webHeaders_7 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_7), value);
	}

	inline static int32_t get_offset_of_credentials_8() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___credentials_8)); }
	inline RuntimeObject* get_credentials_8() const { return ___credentials_8; }
	inline RuntimeObject** get_address_of_credentials_8() { return &___credentials_8; }
	inline void set_credentials_8(RuntimeObject* value)
	{
		___credentials_8 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_8), value);
	}

	inline static int32_t get_offset_of_connectionGroup_9() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___connectionGroup_9)); }
	inline String_t* get_connectionGroup_9() const { return ___connectionGroup_9; }
	inline String_t** get_address_of_connectionGroup_9() { return &___connectionGroup_9; }
	inline void set_connectionGroup_9(String_t* value)
	{
		___connectionGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_9), value);
	}

	inline static int32_t get_offset_of_contentLength_10() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___contentLength_10)); }
	inline int64_t get_contentLength_10() const { return ___contentLength_10; }
	inline int64_t* get_address_of_contentLength_10() { return &___contentLength_10; }
	inline void set_contentLength_10(int64_t value)
	{
		___contentLength_10 = value;
	}

	inline static int32_t get_offset_of_fileAccess_11() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___fileAccess_11)); }
	inline int32_t get_fileAccess_11() const { return ___fileAccess_11; }
	inline int32_t* get_address_of_fileAccess_11() { return &___fileAccess_11; }
	inline void set_fileAccess_11(int32_t value)
	{
		___fileAccess_11 = value;
	}

	inline static int32_t get_offset_of_method_12() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___method_12)); }
	inline String_t* get_method_12() const { return ___method_12; }
	inline String_t** get_address_of_method_12() { return &___method_12; }
	inline void set_method_12(String_t* value)
	{
		___method_12 = value;
		Il2CppCodeGenWriteBarrier((&___method_12), value);
	}

	inline static int32_t get_offset_of_proxy_13() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___proxy_13)); }
	inline RuntimeObject* get_proxy_13() const { return ___proxy_13; }
	inline RuntimeObject** get_address_of_proxy_13() { return &___proxy_13; }
	inline void set_proxy_13(RuntimeObject* value)
	{
		___proxy_13 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_13), value);
	}

	inline static int32_t get_offset_of_preAuthenticate_14() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___preAuthenticate_14)); }
	inline bool get_preAuthenticate_14() const { return ___preAuthenticate_14; }
	inline bool* get_address_of_preAuthenticate_14() { return &___preAuthenticate_14; }
	inline void set_preAuthenticate_14(bool value)
	{
		___preAuthenticate_14 = value;
	}

	inline static int32_t get_offset_of_timeout_15() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___timeout_15)); }
	inline int32_t get_timeout_15() const { return ___timeout_15; }
	inline int32_t* get_address_of_timeout_15() { return &___timeout_15; }
	inline void set_timeout_15(int32_t value)
	{
		___timeout_15 = value;
	}

	inline static int32_t get_offset_of_webResponse_16() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___webResponse_16)); }
	inline FileWebResponse_t982058165 * get_webResponse_16() const { return ___webResponse_16; }
	inline FileWebResponse_t982058165 ** get_address_of_webResponse_16() { return &___webResponse_16; }
	inline void set_webResponse_16(FileWebResponse_t982058165 * value)
	{
		___webResponse_16 = value;
		Il2CppCodeGenWriteBarrier((&___webResponse_16), value);
	}

	inline static int32_t get_offset_of_requestEndEvent_17() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___requestEndEvent_17)); }
	inline AutoResetEvent_t1210004121 * get_requestEndEvent_17() const { return ___requestEndEvent_17; }
	inline AutoResetEvent_t1210004121 ** get_address_of_requestEndEvent_17() { return &___requestEndEvent_17; }
	inline void set_requestEndEvent_17(AutoResetEvent_t1210004121 * value)
	{
		___requestEndEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestEndEvent_17), value);
	}

	inline static int32_t get_offset_of_requesting_18() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___requesting_18)); }
	inline bool get_requesting_18() const { return ___requesting_18; }
	inline bool* get_address_of_requesting_18() { return &___requesting_18; }
	inline void set_requesting_18(bool value)
	{
		___requesting_18 = value;
	}

	inline static int32_t get_offset_of_asyncResponding_19() { return static_cast<int32_t>(offsetof(FileWebRequest_t3154753421, ___asyncResponding_19)); }
	inline bool get_asyncResponding_19() const { return ___asyncResponding_19; }
	inline bool* get_address_of_asyncResponding_19() { return &___asyncResponding_19; }
	inline void set_asyncResponding_19(bool value)
	{
		___asyncResponding_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUEST_T3154753421_H
#ifndef X509CHAINPOLICY_T45074389_H
#define X509CHAINPOLICY_T45074389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct  X509ChainPolicy_t45074389  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::apps
	OidCollection_t678319162 * ___apps_0;
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::cert
	OidCollection_t678319162 * ___cert_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainPolicy::store
	X509Certificate2Collection_t454981218 * ___store_2;
	// System.Security.Cryptography.X509Certificates.X509RevocationFlag System.Security.Cryptography.X509Certificates.X509ChainPolicy::rflag
	int32_t ___rflag_3;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.Security.Cryptography.X509Certificates.X509ChainPolicy::mode
	int32_t ___mode_4;
	// System.TimeSpan System.Security.Cryptography.X509Certificates.X509ChainPolicy::timeout
	TimeSpan_t2821448670  ___timeout_5;
	// System.Security.Cryptography.X509Certificates.X509VerificationFlags System.Security.Cryptography.X509Certificates.X509ChainPolicy::vflags
	int32_t ___vflags_6;
	// System.DateTime System.Security.Cryptography.X509Certificates.X509ChainPolicy::vtime
	DateTime_t359870098  ___vtime_7;

public:
	inline static int32_t get_offset_of_apps_0() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___apps_0)); }
	inline OidCollection_t678319162 * get_apps_0() const { return ___apps_0; }
	inline OidCollection_t678319162 ** get_address_of_apps_0() { return &___apps_0; }
	inline void set_apps_0(OidCollection_t678319162 * value)
	{
		___apps_0 = value;
		Il2CppCodeGenWriteBarrier((&___apps_0), value);
	}

	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___cert_1)); }
	inline OidCollection_t678319162 * get_cert_1() const { return ___cert_1; }
	inline OidCollection_t678319162 ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(OidCollection_t678319162 * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier((&___cert_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___store_2)); }
	inline X509Certificate2Collection_t454981218 * get_store_2() const { return ___store_2; }
	inline X509Certificate2Collection_t454981218 ** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(X509Certificate2Collection_t454981218 * value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_rflag_3() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___rflag_3)); }
	inline int32_t get_rflag_3() const { return ___rflag_3; }
	inline int32_t* get_address_of_rflag_3() { return &___rflag_3; }
	inline void set_rflag_3(int32_t value)
	{
		___rflag_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_timeout_5() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___timeout_5)); }
	inline TimeSpan_t2821448670  get_timeout_5() const { return ___timeout_5; }
	inline TimeSpan_t2821448670 * get_address_of_timeout_5() { return &___timeout_5; }
	inline void set_timeout_5(TimeSpan_t2821448670  value)
	{
		___timeout_5 = value;
	}

	inline static int32_t get_offset_of_vflags_6() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___vflags_6)); }
	inline int32_t get_vflags_6() const { return ___vflags_6; }
	inline int32_t* get_address_of_vflags_6() { return &___vflags_6; }
	inline void set_vflags_6(int32_t value)
	{
		___vflags_6 = value;
	}

	inline static int32_t get_offset_of_vtime_7() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t45074389, ___vtime_7)); }
	inline DateTime_t359870098  get_vtime_7() const { return ___vtime_7; }
	inline DateTime_t359870098 * get_address_of_vtime_7() { return &___vtime_7; }
	inline void set_vtime_7(DateTime_t359870098  value)
	{
		___vtime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINPOLICY_T45074389_H
#ifndef FILEWEBSTREAM_T1782281022_H
#define FILEWEBSTREAM_T1782281022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest/FileWebStream
struct  FileWebStream_t1782281022  : public FileStream_t3854196930
{
public:
	// System.Net.FileWebRequest System.Net.FileWebRequest/FileWebStream::webRequest
	FileWebRequest_t3154753421 * ___webRequest_15;

public:
	inline static int32_t get_offset_of_webRequest_15() { return static_cast<int32_t>(offsetof(FileWebStream_t1782281022, ___webRequest_15)); }
	inline FileWebRequest_t3154753421 * get_webRequest_15() const { return ___webRequest_15; }
	inline FileWebRequest_t3154753421 ** get_address_of_webRequest_15() { return &___webRequest_15; }
	inline void set_webRequest_15(FileWebRequest_t3154753421 * value)
	{
		___webRequest_15 = value;
		Il2CppCodeGenWriteBarrier((&___webRequest_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBSTREAM_T1782281022_H
#ifndef WRITEDELEGATE_T949668826_H
#define WRITEDELEGATE_T949668826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/WriteDelegate
struct  WriteDelegate_t949668826  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_T949668826_H
#ifndef READDELEGATE_T87010224_H
#define READDELEGATE_T87010224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/ReadDelegate
struct  ReadDelegate_t87010224  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_T87010224_H
#ifndef FTPWEBREQUEST_T4270856504_H
#define FTPWEBREQUEST_T4270856504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t4270856504  : public WebRequest_t1788933422
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t3269222095 * ___requestUri_6;
	// System.String System.Net.FtpWebRequest::file_name
	String_t* ___file_name_7;
	// System.Net.ServicePoint System.Net.FtpWebRequest::servicePoint
	ServicePoint_t3505702983 * ___servicePoint_8;
	// System.IO.Stream System.Net.FtpWebRequest::origDataStream
	Stream_t4138477179 * ___origDataStream_9;
	// System.IO.Stream System.Net.FtpWebRequest::dataStream
	Stream_t4138477179 * ___dataStream_10;
	// System.IO.Stream System.Net.FtpWebRequest::controlStream
	Stream_t4138477179 * ___controlStream_11;
	// System.IO.StreamReader System.Net.FtpWebRequest::controlReader
	StreamReader_t1796800705 * ___controlReader_12;
	// System.Net.NetworkCredential System.Net.FtpWebRequest::credentials
	NetworkCredential_t1328334253 * ___credentials_13;
	// System.Net.IPHostEntry System.Net.FtpWebRequest::hostEntry
	IPHostEntry_t4214309905 * ___hostEntry_14;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::localEndPoint
	IPEndPoint_t1226219872 * ___localEndPoint_15;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	RuntimeObject* ___proxy_16;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_17;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_18;
	// System.Int64 System.Net.FtpWebRequest::offset
	int64_t ___offset_19;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_20;
	// System.Boolean System.Net.FtpWebRequest::enableSsl
	bool ___enableSsl_21;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_22;
	// System.Boolean System.Net.FtpWebRequest::keepAlive
	bool ___keepAlive_23;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_24;
	// System.String System.Net.FtpWebRequest::renameTo
	String_t* ___renameTo_25;
	// System.Object System.Net.FtpWebRequest::locker
	RuntimeObject * ___locker_26;
	// System.Net.FtpWebRequest/RequestState System.Net.FtpWebRequest::requestState
	int32_t ___requestState_27;
	// System.Net.FtpAsyncResult System.Net.FtpWebRequest::asyncResult
	FtpAsyncResult_t3500703300 * ___asyncResult_28;
	// System.Net.FtpWebResponse System.Net.FtpWebRequest::ftpResponse
	FtpWebResponse_t1950878246 * ___ftpResponse_29;
	// System.IO.Stream System.Net.FtpWebRequest::requestStream
	Stream_t4138477179 * ___requestStream_30;
	// System.String System.Net.FtpWebRequest::initial_path
	String_t* ___initial_path_31;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::callback
	RemoteCertificateValidationCallback_t2807660831 * ___callback_33;

public:
	inline static int32_t get_offset_of_requestUri_6() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___requestUri_6)); }
	inline Uri_t3269222095 * get_requestUri_6() const { return ___requestUri_6; }
	inline Uri_t3269222095 ** get_address_of_requestUri_6() { return &___requestUri_6; }
	inline void set_requestUri_6(Uri_t3269222095 * value)
	{
		___requestUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_6), value);
	}

	inline static int32_t get_offset_of_file_name_7() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___file_name_7)); }
	inline String_t* get_file_name_7() const { return ___file_name_7; }
	inline String_t** get_address_of_file_name_7() { return &___file_name_7; }
	inline void set_file_name_7(String_t* value)
	{
		___file_name_7 = value;
		Il2CppCodeGenWriteBarrier((&___file_name_7), value);
	}

	inline static int32_t get_offset_of_servicePoint_8() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___servicePoint_8)); }
	inline ServicePoint_t3505702983 * get_servicePoint_8() const { return ___servicePoint_8; }
	inline ServicePoint_t3505702983 ** get_address_of_servicePoint_8() { return &___servicePoint_8; }
	inline void set_servicePoint_8(ServicePoint_t3505702983 * value)
	{
		___servicePoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_8), value);
	}

	inline static int32_t get_offset_of_origDataStream_9() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___origDataStream_9)); }
	inline Stream_t4138477179 * get_origDataStream_9() const { return ___origDataStream_9; }
	inline Stream_t4138477179 ** get_address_of_origDataStream_9() { return &___origDataStream_9; }
	inline void set_origDataStream_9(Stream_t4138477179 * value)
	{
		___origDataStream_9 = value;
		Il2CppCodeGenWriteBarrier((&___origDataStream_9), value);
	}

	inline static int32_t get_offset_of_dataStream_10() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___dataStream_10)); }
	inline Stream_t4138477179 * get_dataStream_10() const { return ___dataStream_10; }
	inline Stream_t4138477179 ** get_address_of_dataStream_10() { return &___dataStream_10; }
	inline void set_dataStream_10(Stream_t4138477179 * value)
	{
		___dataStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataStream_10), value);
	}

	inline static int32_t get_offset_of_controlStream_11() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___controlStream_11)); }
	inline Stream_t4138477179 * get_controlStream_11() const { return ___controlStream_11; }
	inline Stream_t4138477179 ** get_address_of_controlStream_11() { return &___controlStream_11; }
	inline void set_controlStream_11(Stream_t4138477179 * value)
	{
		___controlStream_11 = value;
		Il2CppCodeGenWriteBarrier((&___controlStream_11), value);
	}

	inline static int32_t get_offset_of_controlReader_12() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___controlReader_12)); }
	inline StreamReader_t1796800705 * get_controlReader_12() const { return ___controlReader_12; }
	inline StreamReader_t1796800705 ** get_address_of_controlReader_12() { return &___controlReader_12; }
	inline void set_controlReader_12(StreamReader_t1796800705 * value)
	{
		___controlReader_12 = value;
		Il2CppCodeGenWriteBarrier((&___controlReader_12), value);
	}

	inline static int32_t get_offset_of_credentials_13() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___credentials_13)); }
	inline NetworkCredential_t1328334253 * get_credentials_13() const { return ___credentials_13; }
	inline NetworkCredential_t1328334253 ** get_address_of_credentials_13() { return &___credentials_13; }
	inline void set_credentials_13(NetworkCredential_t1328334253 * value)
	{
		___credentials_13 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_13), value);
	}

	inline static int32_t get_offset_of_hostEntry_14() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___hostEntry_14)); }
	inline IPHostEntry_t4214309905 * get_hostEntry_14() const { return ___hostEntry_14; }
	inline IPHostEntry_t4214309905 ** get_address_of_hostEntry_14() { return &___hostEntry_14; }
	inline void set_hostEntry_14(IPHostEntry_t4214309905 * value)
	{
		___hostEntry_14 = value;
		Il2CppCodeGenWriteBarrier((&___hostEntry_14), value);
	}

	inline static int32_t get_offset_of_localEndPoint_15() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___localEndPoint_15)); }
	inline IPEndPoint_t1226219872 * get_localEndPoint_15() const { return ___localEndPoint_15; }
	inline IPEndPoint_t1226219872 ** get_address_of_localEndPoint_15() { return &___localEndPoint_15; }
	inline void set_localEndPoint_15(IPEndPoint_t1226219872 * value)
	{
		___localEndPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___localEndPoint_15), value);
	}

	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___proxy_16)); }
	inline RuntimeObject* get_proxy_16() const { return ___proxy_16; }
	inline RuntimeObject** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(RuntimeObject* value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_16), value);
	}

	inline static int32_t get_offset_of_timeout_17() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___timeout_17)); }
	inline int32_t get_timeout_17() const { return ___timeout_17; }
	inline int32_t* get_address_of_timeout_17() { return &___timeout_17; }
	inline void set_timeout_17(int32_t value)
	{
		___timeout_17 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_18() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___rwTimeout_18)); }
	inline int32_t get_rwTimeout_18() const { return ___rwTimeout_18; }
	inline int32_t* get_address_of_rwTimeout_18() { return &___rwTimeout_18; }
	inline void set_rwTimeout_18(int32_t value)
	{
		___rwTimeout_18 = value;
	}

	inline static int32_t get_offset_of_offset_19() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___offset_19)); }
	inline int64_t get_offset_19() const { return ___offset_19; }
	inline int64_t* get_address_of_offset_19() { return &___offset_19; }
	inline void set_offset_19(int64_t value)
	{
		___offset_19 = value;
	}

	inline static int32_t get_offset_of_binary_20() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___binary_20)); }
	inline bool get_binary_20() const { return ___binary_20; }
	inline bool* get_address_of_binary_20() { return &___binary_20; }
	inline void set_binary_20(bool value)
	{
		___binary_20 = value;
	}

	inline static int32_t get_offset_of_enableSsl_21() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___enableSsl_21)); }
	inline bool get_enableSsl_21() const { return ___enableSsl_21; }
	inline bool* get_address_of_enableSsl_21() { return &___enableSsl_21; }
	inline void set_enableSsl_21(bool value)
	{
		___enableSsl_21 = value;
	}

	inline static int32_t get_offset_of_usePassive_22() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___usePassive_22)); }
	inline bool get_usePassive_22() const { return ___usePassive_22; }
	inline bool* get_address_of_usePassive_22() { return &___usePassive_22; }
	inline void set_usePassive_22(bool value)
	{
		___usePassive_22 = value;
	}

	inline static int32_t get_offset_of_keepAlive_23() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___keepAlive_23)); }
	inline bool get_keepAlive_23() const { return ___keepAlive_23; }
	inline bool* get_address_of_keepAlive_23() { return &___keepAlive_23; }
	inline void set_keepAlive_23(bool value)
	{
		___keepAlive_23 = value;
	}

	inline static int32_t get_offset_of_method_24() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___method_24)); }
	inline String_t* get_method_24() const { return ___method_24; }
	inline String_t** get_address_of_method_24() { return &___method_24; }
	inline void set_method_24(String_t* value)
	{
		___method_24 = value;
		Il2CppCodeGenWriteBarrier((&___method_24), value);
	}

	inline static int32_t get_offset_of_renameTo_25() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___renameTo_25)); }
	inline String_t* get_renameTo_25() const { return ___renameTo_25; }
	inline String_t** get_address_of_renameTo_25() { return &___renameTo_25; }
	inline void set_renameTo_25(String_t* value)
	{
		___renameTo_25 = value;
		Il2CppCodeGenWriteBarrier((&___renameTo_25), value);
	}

	inline static int32_t get_offset_of_locker_26() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___locker_26)); }
	inline RuntimeObject * get_locker_26() const { return ___locker_26; }
	inline RuntimeObject ** get_address_of_locker_26() { return &___locker_26; }
	inline void set_locker_26(RuntimeObject * value)
	{
		___locker_26 = value;
		Il2CppCodeGenWriteBarrier((&___locker_26), value);
	}

	inline static int32_t get_offset_of_requestState_27() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___requestState_27)); }
	inline int32_t get_requestState_27() const { return ___requestState_27; }
	inline int32_t* get_address_of_requestState_27() { return &___requestState_27; }
	inline void set_requestState_27(int32_t value)
	{
		___requestState_27 = value;
	}

	inline static int32_t get_offset_of_asyncResult_28() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___asyncResult_28)); }
	inline FtpAsyncResult_t3500703300 * get_asyncResult_28() const { return ___asyncResult_28; }
	inline FtpAsyncResult_t3500703300 ** get_address_of_asyncResult_28() { return &___asyncResult_28; }
	inline void set_asyncResult_28(FtpAsyncResult_t3500703300 * value)
	{
		___asyncResult_28 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_28), value);
	}

	inline static int32_t get_offset_of_ftpResponse_29() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___ftpResponse_29)); }
	inline FtpWebResponse_t1950878246 * get_ftpResponse_29() const { return ___ftpResponse_29; }
	inline FtpWebResponse_t1950878246 ** get_address_of_ftpResponse_29() { return &___ftpResponse_29; }
	inline void set_ftpResponse_29(FtpWebResponse_t1950878246 * value)
	{
		___ftpResponse_29 = value;
		Il2CppCodeGenWriteBarrier((&___ftpResponse_29), value);
	}

	inline static int32_t get_offset_of_requestStream_30() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___requestStream_30)); }
	inline Stream_t4138477179 * get_requestStream_30() const { return ___requestStream_30; }
	inline Stream_t4138477179 ** get_address_of_requestStream_30() { return &___requestStream_30; }
	inline void set_requestStream_30(Stream_t4138477179 * value)
	{
		___requestStream_30 = value;
		Il2CppCodeGenWriteBarrier((&___requestStream_30), value);
	}

	inline static int32_t get_offset_of_initial_path_31() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___initial_path_31)); }
	inline String_t* get_initial_path_31() const { return ___initial_path_31; }
	inline String_t** get_address_of_initial_path_31() { return &___initial_path_31; }
	inline void set_initial_path_31(String_t* value)
	{
		___initial_path_31 = value;
		Il2CppCodeGenWriteBarrier((&___initial_path_31), value);
	}

	inline static int32_t get_offset_of_callback_33() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504, ___callback_33)); }
	inline RemoteCertificateValidationCallback_t2807660831 * get_callback_33() const { return ___callback_33; }
	inline RemoteCertificateValidationCallback_t2807660831 ** get_address_of_callback_33() { return &___callback_33; }
	inline void set_callback_33(RemoteCertificateValidationCallback_t2807660831 * value)
	{
		___callback_33 = value;
		Il2CppCodeGenWriteBarrier((&___callback_33), value);
	}
};

struct FtpWebRequest_t4270856504_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t2511808107* ___supportedCommands_32;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::<>f__am$cache1C
	RemoteCertificateValidationCallback_t2807660831 * ___U3CU3Ef__amU24cache1C_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.FtpWebRequest::<>f__switch$map5
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map5_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.FtpWebRequest::<>f__switch$map6
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map6_36;

public:
	inline static int32_t get_offset_of_supportedCommands_32() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504_StaticFields, ___supportedCommands_32)); }
	inline StringU5BU5D_t2511808107* get_supportedCommands_32() const { return ___supportedCommands_32; }
	inline StringU5BU5D_t2511808107** get_address_of_supportedCommands_32() { return &___supportedCommands_32; }
	inline void set_supportedCommands_32(StringU5BU5D_t2511808107* value)
	{
		___supportedCommands_32 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCommands_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_34() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504_StaticFields, ___U3CU3Ef__amU24cache1C_34)); }
	inline RemoteCertificateValidationCallback_t2807660831 * get_U3CU3Ef__amU24cache1C_34() const { return ___U3CU3Ef__amU24cache1C_34; }
	inline RemoteCertificateValidationCallback_t2807660831 ** get_address_of_U3CU3Ef__amU24cache1C_34() { return &___U3CU3Ef__amU24cache1C_34; }
	inline void set_U3CU3Ef__amU24cache1C_34(RemoteCertificateValidationCallback_t2807660831 * value)
	{
		___U3CU3Ef__amU24cache1C_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1C_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_35() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504_StaticFields, ___U3CU3Ef__switchU24map5_35)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map5_35() const { return ___U3CU3Ef__switchU24map5_35; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map5_35() { return &___U3CU3Ef__switchU24map5_35; }
	inline void set_U3CU3Ef__switchU24map5_35(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map5_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_36() { return static_cast<int32_t>(offsetof(FtpWebRequest_t4270856504_StaticFields, ___U3CU3Ef__switchU24map6_36)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map6_36() const { return ___U3CU3Ef__switchU24map6_36; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map6_36() { return &___U3CU3Ef__switchU24map6_36; }
	inline void set_U3CU3Ef__switchU24map6_36(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map6_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBREQUEST_T4270856504_H
#ifndef SERVICEPOINT_T3505702983_H
#define SERVICEPOINT_T3505702983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_t3505702983  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_t3269222095 * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t359870098  ___idleSince_4;
	// System.Version System.Net.ServicePoint::protocolVersion
	Version_t2250829542 * ___protocolVersion_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::certificate
	X509Certificate_t2642281915 * ___certificate_6;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::clientCertificate
	X509Certificate_t2642281915 * ___clientCertificate_7;
	// System.Net.IPHostEntry System.Net.ServicePoint::host
	IPHostEntry_t4214309905 * ___host_8;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_9;
	// System.Collections.Hashtable System.Net.ServicePoint::groups
	Hashtable_t2354558714 * ___groups_10;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_11;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_12;
	// System.Object System.Net.ServicePoint::locker
	RuntimeObject * ___locker_13;
	// System.Object System.Net.ServicePoint::hostE
	RuntimeObject * ___hostE_14;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_15;
	// System.Net.BindIPEndPoint System.Net.ServicePoint::endPointCallback
	BindIPEndPoint_t1147245406 * ___endPointCallback_16;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___uri_0)); }
	inline Uri_t3269222095 * get_uri_0() const { return ___uri_0; }
	inline Uri_t3269222095 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t3269222095 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___idleSince_4)); }
	inline DateTime_t359870098  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t359870098 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t359870098  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_protocolVersion_5() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___protocolVersion_5)); }
	inline Version_t2250829542 * get_protocolVersion_5() const { return ___protocolVersion_5; }
	inline Version_t2250829542 ** get_address_of_protocolVersion_5() { return &___protocolVersion_5; }
	inline void set_protocolVersion_5(Version_t2250829542 * value)
	{
		___protocolVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___protocolVersion_5), value);
	}

	inline static int32_t get_offset_of_certificate_6() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___certificate_6)); }
	inline X509Certificate_t2642281915 * get_certificate_6() const { return ___certificate_6; }
	inline X509Certificate_t2642281915 ** get_address_of_certificate_6() { return &___certificate_6; }
	inline void set_certificate_6(X509Certificate_t2642281915 * value)
	{
		___certificate_6 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_6), value);
	}

	inline static int32_t get_offset_of_clientCertificate_7() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___clientCertificate_7)); }
	inline X509Certificate_t2642281915 * get_clientCertificate_7() const { return ___clientCertificate_7; }
	inline X509Certificate_t2642281915 ** get_address_of_clientCertificate_7() { return &___clientCertificate_7; }
	inline void set_clientCertificate_7(X509Certificate_t2642281915 * value)
	{
		___clientCertificate_7 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_7), value);
	}

	inline static int32_t get_offset_of_host_8() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___host_8)); }
	inline IPHostEntry_t4214309905 * get_host_8() const { return ___host_8; }
	inline IPHostEntry_t4214309905 ** get_address_of_host_8() { return &___host_8; }
	inline void set_host_8(IPHostEntry_t4214309905 * value)
	{
		___host_8 = value;
		Il2CppCodeGenWriteBarrier((&___host_8), value);
	}

	inline static int32_t get_offset_of_usesProxy_9() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___usesProxy_9)); }
	inline bool get_usesProxy_9() const { return ___usesProxy_9; }
	inline bool* get_address_of_usesProxy_9() { return &___usesProxy_9; }
	inline void set_usesProxy_9(bool value)
	{
		___usesProxy_9 = value;
	}

	inline static int32_t get_offset_of_groups_10() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___groups_10)); }
	inline Hashtable_t2354558714 * get_groups_10() const { return ___groups_10; }
	inline Hashtable_t2354558714 ** get_address_of_groups_10() { return &___groups_10; }
	inline void set_groups_10(Hashtable_t2354558714 * value)
	{
		___groups_10 = value;
		Il2CppCodeGenWriteBarrier((&___groups_10), value);
	}

	inline static int32_t get_offset_of_sendContinue_11() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___sendContinue_11)); }
	inline bool get_sendContinue_11() const { return ___sendContinue_11; }
	inline bool* get_address_of_sendContinue_11() { return &___sendContinue_11; }
	inline void set_sendContinue_11(bool value)
	{
		___sendContinue_11 = value;
	}

	inline static int32_t get_offset_of_useConnect_12() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___useConnect_12)); }
	inline bool get_useConnect_12() const { return ___useConnect_12; }
	inline bool* get_address_of_useConnect_12() { return &___useConnect_12; }
	inline void set_useConnect_12(bool value)
	{
		___useConnect_12 = value;
	}

	inline static int32_t get_offset_of_locker_13() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___locker_13)); }
	inline RuntimeObject * get_locker_13() const { return ___locker_13; }
	inline RuntimeObject ** get_address_of_locker_13() { return &___locker_13; }
	inline void set_locker_13(RuntimeObject * value)
	{
		___locker_13 = value;
		Il2CppCodeGenWriteBarrier((&___locker_13), value);
	}

	inline static int32_t get_offset_of_hostE_14() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___hostE_14)); }
	inline RuntimeObject * get_hostE_14() const { return ___hostE_14; }
	inline RuntimeObject ** get_address_of_hostE_14() { return &___hostE_14; }
	inline void set_hostE_14(RuntimeObject * value)
	{
		___hostE_14 = value;
		Il2CppCodeGenWriteBarrier((&___hostE_14), value);
	}

	inline static int32_t get_offset_of_useNagle_15() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___useNagle_15)); }
	inline bool get_useNagle_15() const { return ___useNagle_15; }
	inline bool* get_address_of_useNagle_15() { return &___useNagle_15; }
	inline void set_useNagle_15(bool value)
	{
		___useNagle_15 = value;
	}

	inline static int32_t get_offset_of_endPointCallback_16() { return static_cast<int32_t>(offsetof(ServicePoint_t3505702983, ___endPointCallback_16)); }
	inline BindIPEndPoint_t1147245406 * get_endPointCallback_16() const { return ___endPointCallback_16; }
	inline BindIPEndPoint_t1147245406 ** get_address_of_endPointCallback_16() { return &___endPointCallback_16; }
	inline void set_endPointCallback_16(BindIPEndPoint_t1147245406 * value)
	{
		___endPointCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___endPointCallback_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINT_T3505702983_H
#ifndef FTPWEBRESPONSE_T1950878246_H
#define FTPWEBRESPONSE_T1950878246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebResponse
struct  FtpWebResponse_t1950878246  : public WebResponse_t3616229315
{
public:
	// System.IO.Stream System.Net.FtpWebResponse::stream
	Stream_t4138477179 * ___stream_1;
	// System.Uri System.Net.FtpWebResponse::uri
	Uri_t3269222095 * ___uri_2;
	// System.Net.FtpStatusCode System.Net.FtpWebResponse::statusCode
	int32_t ___statusCode_3;
	// System.DateTime System.Net.FtpWebResponse::lastModified
	DateTime_t359870098  ___lastModified_4;
	// System.String System.Net.FtpWebResponse::bannerMessage
	String_t* ___bannerMessage_5;
	// System.String System.Net.FtpWebResponse::welcomeMessage
	String_t* ___welcomeMessage_6;
	// System.String System.Net.FtpWebResponse::exitMessage
	String_t* ___exitMessage_7;
	// System.String System.Net.FtpWebResponse::statusDescription
	String_t* ___statusDescription_8;
	// System.String System.Net.FtpWebResponse::method
	String_t* ___method_9;
	// System.Boolean System.Net.FtpWebResponse::disposed
	bool ___disposed_10;
	// System.Net.FtpWebRequest System.Net.FtpWebResponse::request
	FtpWebRequest_t4270856504 * ___request_11;
	// System.Int64 System.Net.FtpWebResponse::contentLength
	int64_t ___contentLength_12;

public:
	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___stream_1)); }
	inline Stream_t4138477179 * get_stream_1() const { return ___stream_1; }
	inline Stream_t4138477179 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t4138477179 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___uri_2)); }
	inline Uri_t3269222095 * get_uri_2() const { return ___uri_2; }
	inline Uri_t3269222095 ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t3269222095 * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_statusCode_3() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___statusCode_3)); }
	inline int32_t get_statusCode_3() const { return ___statusCode_3; }
	inline int32_t* get_address_of_statusCode_3() { return &___statusCode_3; }
	inline void set_statusCode_3(int32_t value)
	{
		___statusCode_3 = value;
	}

	inline static int32_t get_offset_of_lastModified_4() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___lastModified_4)); }
	inline DateTime_t359870098  get_lastModified_4() const { return ___lastModified_4; }
	inline DateTime_t359870098 * get_address_of_lastModified_4() { return &___lastModified_4; }
	inline void set_lastModified_4(DateTime_t359870098  value)
	{
		___lastModified_4 = value;
	}

	inline static int32_t get_offset_of_bannerMessage_5() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___bannerMessage_5)); }
	inline String_t* get_bannerMessage_5() const { return ___bannerMessage_5; }
	inline String_t** get_address_of_bannerMessage_5() { return &___bannerMessage_5; }
	inline void set_bannerMessage_5(String_t* value)
	{
		___bannerMessage_5 = value;
		Il2CppCodeGenWriteBarrier((&___bannerMessage_5), value);
	}

	inline static int32_t get_offset_of_welcomeMessage_6() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___welcomeMessage_6)); }
	inline String_t* get_welcomeMessage_6() const { return ___welcomeMessage_6; }
	inline String_t** get_address_of_welcomeMessage_6() { return &___welcomeMessage_6; }
	inline void set_welcomeMessage_6(String_t* value)
	{
		___welcomeMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___welcomeMessage_6), value);
	}

	inline static int32_t get_offset_of_exitMessage_7() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___exitMessage_7)); }
	inline String_t* get_exitMessage_7() const { return ___exitMessage_7; }
	inline String_t** get_address_of_exitMessage_7() { return &___exitMessage_7; }
	inline void set_exitMessage_7(String_t* value)
	{
		___exitMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___exitMessage_7), value);
	}

	inline static int32_t get_offset_of_statusDescription_8() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___statusDescription_8)); }
	inline String_t* get_statusDescription_8() const { return ___statusDescription_8; }
	inline String_t** get_address_of_statusDescription_8() { return &___statusDescription_8; }
	inline void set_statusDescription_8(String_t* value)
	{
		___statusDescription_8 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_8), value);
	}

	inline static int32_t get_offset_of_method_9() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___method_9)); }
	inline String_t* get_method_9() const { return ___method_9; }
	inline String_t** get_address_of_method_9() { return &___method_9; }
	inline void set_method_9(String_t* value)
	{
		___method_9 = value;
		Il2CppCodeGenWriteBarrier((&___method_9), value);
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_request_11() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___request_11)); }
	inline FtpWebRequest_t4270856504 * get_request_11() const { return ___request_11; }
	inline FtpWebRequest_t4270856504 ** get_address_of_request_11() { return &___request_11; }
	inline void set_request_11(FtpWebRequest_t4270856504 * value)
	{
		___request_11 = value;
		Il2CppCodeGenWriteBarrier((&___request_11), value);
	}

	inline static int32_t get_offset_of_contentLength_12() { return static_cast<int32_t>(offsetof(FtpWebResponse_t1950878246, ___contentLength_12)); }
	inline int64_t get_contentLength_12() const { return ___contentLength_12; }
	inline int64_t* get_address_of_contentLength_12() { return &___contentLength_12; }
	inline void set_contentLength_12(int64_t value)
	{
		___contentLength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBRESPONSE_T1950878246_H
#ifndef DIGESTSESSION_T3366850630_H
#define DIGESTSESSION_T3366850630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestSession
struct  DigestSession_t3366850630  : public RuntimeObject
{
public:
	// System.DateTime System.Net.DigestSession::lastUse
	DateTime_t359870098  ___lastUse_1;
	// System.Int32 System.Net.DigestSession::_nc
	int32_t ____nc_2;
	// System.Security.Cryptography.HashAlgorithm System.Net.DigestSession::hash
	HashAlgorithm_t1109801809 * ___hash_3;
	// System.Net.DigestHeaderParser System.Net.DigestSession::parser
	DigestHeaderParser_t477534254 * ___parser_4;
	// System.String System.Net.DigestSession::_cnonce
	String_t* ____cnonce_5;

public:
	inline static int32_t get_offset_of_lastUse_1() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630, ___lastUse_1)); }
	inline DateTime_t359870098  get_lastUse_1() const { return ___lastUse_1; }
	inline DateTime_t359870098 * get_address_of_lastUse_1() { return &___lastUse_1; }
	inline void set_lastUse_1(DateTime_t359870098  value)
	{
		___lastUse_1 = value;
	}

	inline static int32_t get_offset_of__nc_2() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630, ____nc_2)); }
	inline int32_t get__nc_2() const { return ____nc_2; }
	inline int32_t* get_address_of__nc_2() { return &____nc_2; }
	inline void set__nc_2(int32_t value)
	{
		____nc_2 = value;
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630, ___hash_3)); }
	inline HashAlgorithm_t1109801809 * get_hash_3() const { return ___hash_3; }
	inline HashAlgorithm_t1109801809 ** get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(HashAlgorithm_t1109801809 * value)
	{
		___hash_3 = value;
		Il2CppCodeGenWriteBarrier((&___hash_3), value);
	}

	inline static int32_t get_offset_of_parser_4() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630, ___parser_4)); }
	inline DigestHeaderParser_t477534254 * get_parser_4() const { return ___parser_4; }
	inline DigestHeaderParser_t477534254 ** get_address_of_parser_4() { return &___parser_4; }
	inline void set_parser_4(DigestHeaderParser_t477534254 * value)
	{
		___parser_4 = value;
		Il2CppCodeGenWriteBarrier((&___parser_4), value);
	}

	inline static int32_t get_offset_of__cnonce_5() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630, ____cnonce_5)); }
	inline String_t* get__cnonce_5() const { return ____cnonce_5; }
	inline String_t** get_address_of__cnonce_5() { return &____cnonce_5; }
	inline void set__cnonce_5(String_t* value)
	{
		____cnonce_5 = value;
		Il2CppCodeGenWriteBarrier((&____cnonce_5), value);
	}
};

struct DigestSession_t3366850630_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator System.Net.DigestSession::rng
	RandomNumberGenerator_t4215975066 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(DigestSession_t3366850630_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t4215975066 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t4215975066 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t4215975066 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSESSION_T3366850630_H
#ifndef HTTPWEBREQUEST_T4046343009_H
#define HTTPWEBREQUEST_T4046343009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest
struct  HttpWebRequest_t4046343009  : public WebRequest_t1788933422
{
public:
	// System.Uri System.Net.HttpWebRequest::requestUri
	Uri_t3269222095 * ___requestUri_6;
	// System.Uri System.Net.HttpWebRequest::actualUri
	Uri_t3269222095 * ___actualUri_7;
	// System.Boolean System.Net.HttpWebRequest::hostChanged
	bool ___hostChanged_8;
	// System.Boolean System.Net.HttpWebRequest::allowAutoRedirect
	bool ___allowAutoRedirect_9;
	// System.Boolean System.Net.HttpWebRequest::allowBuffering
	bool ___allowBuffering_10;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.HttpWebRequest::certificates
	X509CertificateCollection_t728319640 * ___certificates_11;
	// System.String System.Net.HttpWebRequest::connectionGroup
	String_t* ___connectionGroup_12;
	// System.Int64 System.Net.HttpWebRequest::contentLength
	int64_t ___contentLength_13;
	// System.Net.HttpContinueDelegate System.Net.HttpWebRequest::continueDelegate
	HttpContinueDelegate_t510056555 * ___continueDelegate_14;
	// System.Net.CookieContainer System.Net.HttpWebRequest::cookieContainer
	CookieContainer_t390599126 * ___cookieContainer_15;
	// System.Net.ICredentials System.Net.HttpWebRequest::credentials
	RuntimeObject* ___credentials_16;
	// System.Boolean System.Net.HttpWebRequest::haveResponse
	bool ___haveResponse_17;
	// System.Boolean System.Net.HttpWebRequest::haveRequest
	bool ___haveRequest_18;
	// System.Boolean System.Net.HttpWebRequest::requestSent
	bool ___requestSent_19;
	// System.Net.WebHeaderCollection System.Net.HttpWebRequest::webHeaders
	WebHeaderCollection_t592894254 * ___webHeaders_20;
	// System.Boolean System.Net.HttpWebRequest::keepAlive
	bool ___keepAlive_21;
	// System.Int32 System.Net.HttpWebRequest::maxAutoRedirect
	int32_t ___maxAutoRedirect_22;
	// System.String System.Net.HttpWebRequest::mediaType
	String_t* ___mediaType_23;
	// System.String System.Net.HttpWebRequest::method
	String_t* ___method_24;
	// System.String System.Net.HttpWebRequest::initialMethod
	String_t* ___initialMethod_25;
	// System.Boolean System.Net.HttpWebRequest::pipelined
	bool ___pipelined_26;
	// System.Boolean System.Net.HttpWebRequest::preAuthenticate
	bool ___preAuthenticate_27;
	// System.Boolean System.Net.HttpWebRequest::usedPreAuth
	bool ___usedPreAuth_28;
	// System.Version System.Net.HttpWebRequest::version
	Version_t2250829542 * ___version_29;
	// System.Version System.Net.HttpWebRequest::actualVersion
	Version_t2250829542 * ___actualVersion_30;
	// System.Net.IWebProxy System.Net.HttpWebRequest::proxy
	RuntimeObject* ___proxy_31;
	// System.Boolean System.Net.HttpWebRequest::sendChunked
	bool ___sendChunked_32;
	// System.Net.ServicePoint System.Net.HttpWebRequest::servicePoint
	ServicePoint_t3505702983 * ___servicePoint_33;
	// System.Int32 System.Net.HttpWebRequest::timeout
	int32_t ___timeout_34;
	// System.Net.WebConnectionStream System.Net.HttpWebRequest::writeStream
	WebConnectionStream_t3750989120 * ___writeStream_35;
	// System.Net.HttpWebResponse System.Net.HttpWebRequest::webResponse
	HttpWebResponse_t2352629529 * ___webResponse_36;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncWrite
	WebAsyncResult_t1279310773 * ___asyncWrite_37;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncRead
	WebAsyncResult_t1279310773 * ___asyncRead_38;
	// System.EventHandler System.Net.HttpWebRequest::abortHandler
	EventHandler_t369332490 * ___abortHandler_39;
	// System.Int32 System.Net.HttpWebRequest::aborted
	int32_t ___aborted_40;
	// System.Int32 System.Net.HttpWebRequest::redirects
	int32_t ___redirects_41;
	// System.Boolean System.Net.HttpWebRequest::expectContinue
	bool ___expectContinue_42;
	// System.Boolean System.Net.HttpWebRequest::authCompleted
	bool ___authCompleted_43;
	// System.Byte[] System.Net.HttpWebRequest::bodyBuffer
	ByteU5BU5D_t434619169* ___bodyBuffer_44;
	// System.Int32 System.Net.HttpWebRequest::bodyBufferLength
	int32_t ___bodyBufferLength_45;
	// System.Boolean System.Net.HttpWebRequest::getResponseCalled
	bool ___getResponseCalled_46;
	// System.Exception System.Net.HttpWebRequest::saved_exc
	Exception_t4086964929 * ___saved_exc_47;
	// System.Object System.Net.HttpWebRequest::locker
	RuntimeObject * ___locker_48;
	// System.Boolean System.Net.HttpWebRequest::is_ntlm_auth
	bool ___is_ntlm_auth_49;
	// System.Boolean System.Net.HttpWebRequest::finished_reading
	bool ___finished_reading_50;
	// System.Net.WebConnection System.Net.HttpWebRequest::WebConnection
	WebConnection_t3591103104 * ___WebConnection_51;
	// System.Net.DecompressionMethods System.Net.HttpWebRequest::auto_decomp
	int32_t ___auto_decomp_52;
	// System.Int32 System.Net.HttpWebRequest::readWriteTimeout
	int32_t ___readWriteTimeout_54;
	// System.Boolean System.Net.HttpWebRequest::unsafe_auth_blah
	bool ___unsafe_auth_blah_55;

public:
	inline static int32_t get_offset_of_requestUri_6() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___requestUri_6)); }
	inline Uri_t3269222095 * get_requestUri_6() const { return ___requestUri_6; }
	inline Uri_t3269222095 ** get_address_of_requestUri_6() { return &___requestUri_6; }
	inline void set_requestUri_6(Uri_t3269222095 * value)
	{
		___requestUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_6), value);
	}

	inline static int32_t get_offset_of_actualUri_7() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___actualUri_7)); }
	inline Uri_t3269222095 * get_actualUri_7() const { return ___actualUri_7; }
	inline Uri_t3269222095 ** get_address_of_actualUri_7() { return &___actualUri_7; }
	inline void set_actualUri_7(Uri_t3269222095 * value)
	{
		___actualUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___actualUri_7), value);
	}

	inline static int32_t get_offset_of_hostChanged_8() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___hostChanged_8)); }
	inline bool get_hostChanged_8() const { return ___hostChanged_8; }
	inline bool* get_address_of_hostChanged_8() { return &___hostChanged_8; }
	inline void set_hostChanged_8(bool value)
	{
		___hostChanged_8 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_9() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___allowAutoRedirect_9)); }
	inline bool get_allowAutoRedirect_9() const { return ___allowAutoRedirect_9; }
	inline bool* get_address_of_allowAutoRedirect_9() { return &___allowAutoRedirect_9; }
	inline void set_allowAutoRedirect_9(bool value)
	{
		___allowAutoRedirect_9 = value;
	}

	inline static int32_t get_offset_of_allowBuffering_10() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___allowBuffering_10)); }
	inline bool get_allowBuffering_10() const { return ___allowBuffering_10; }
	inline bool* get_address_of_allowBuffering_10() { return &___allowBuffering_10; }
	inline void set_allowBuffering_10(bool value)
	{
		___allowBuffering_10 = value;
	}

	inline static int32_t get_offset_of_certificates_11() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___certificates_11)); }
	inline X509CertificateCollection_t728319640 * get_certificates_11() const { return ___certificates_11; }
	inline X509CertificateCollection_t728319640 ** get_address_of_certificates_11() { return &___certificates_11; }
	inline void set_certificates_11(X509CertificateCollection_t728319640 * value)
	{
		___certificates_11 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_11), value);
	}

	inline static int32_t get_offset_of_connectionGroup_12() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___connectionGroup_12)); }
	inline String_t* get_connectionGroup_12() const { return ___connectionGroup_12; }
	inline String_t** get_address_of_connectionGroup_12() { return &___connectionGroup_12; }
	inline void set_connectionGroup_12(String_t* value)
	{
		___connectionGroup_12 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_12), value);
	}

	inline static int32_t get_offset_of_contentLength_13() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___contentLength_13)); }
	inline int64_t get_contentLength_13() const { return ___contentLength_13; }
	inline int64_t* get_address_of_contentLength_13() { return &___contentLength_13; }
	inline void set_contentLength_13(int64_t value)
	{
		___contentLength_13 = value;
	}

	inline static int32_t get_offset_of_continueDelegate_14() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___continueDelegate_14)); }
	inline HttpContinueDelegate_t510056555 * get_continueDelegate_14() const { return ___continueDelegate_14; }
	inline HttpContinueDelegate_t510056555 ** get_address_of_continueDelegate_14() { return &___continueDelegate_14; }
	inline void set_continueDelegate_14(HttpContinueDelegate_t510056555 * value)
	{
		___continueDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((&___continueDelegate_14), value);
	}

	inline static int32_t get_offset_of_cookieContainer_15() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___cookieContainer_15)); }
	inline CookieContainer_t390599126 * get_cookieContainer_15() const { return ___cookieContainer_15; }
	inline CookieContainer_t390599126 ** get_address_of_cookieContainer_15() { return &___cookieContainer_15; }
	inline void set_cookieContainer_15(CookieContainer_t390599126 * value)
	{
		___cookieContainer_15 = value;
		Il2CppCodeGenWriteBarrier((&___cookieContainer_15), value);
	}

	inline static int32_t get_offset_of_credentials_16() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___credentials_16)); }
	inline RuntimeObject* get_credentials_16() const { return ___credentials_16; }
	inline RuntimeObject** get_address_of_credentials_16() { return &___credentials_16; }
	inline void set_credentials_16(RuntimeObject* value)
	{
		___credentials_16 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_16), value);
	}

	inline static int32_t get_offset_of_haveResponse_17() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___haveResponse_17)); }
	inline bool get_haveResponse_17() const { return ___haveResponse_17; }
	inline bool* get_address_of_haveResponse_17() { return &___haveResponse_17; }
	inline void set_haveResponse_17(bool value)
	{
		___haveResponse_17 = value;
	}

	inline static int32_t get_offset_of_haveRequest_18() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___haveRequest_18)); }
	inline bool get_haveRequest_18() const { return ___haveRequest_18; }
	inline bool* get_address_of_haveRequest_18() { return &___haveRequest_18; }
	inline void set_haveRequest_18(bool value)
	{
		___haveRequest_18 = value;
	}

	inline static int32_t get_offset_of_requestSent_19() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___requestSent_19)); }
	inline bool get_requestSent_19() const { return ___requestSent_19; }
	inline bool* get_address_of_requestSent_19() { return &___requestSent_19; }
	inline void set_requestSent_19(bool value)
	{
		___requestSent_19 = value;
	}

	inline static int32_t get_offset_of_webHeaders_20() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___webHeaders_20)); }
	inline WebHeaderCollection_t592894254 * get_webHeaders_20() const { return ___webHeaders_20; }
	inline WebHeaderCollection_t592894254 ** get_address_of_webHeaders_20() { return &___webHeaders_20; }
	inline void set_webHeaders_20(WebHeaderCollection_t592894254 * value)
	{
		___webHeaders_20 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_20), value);
	}

	inline static int32_t get_offset_of_keepAlive_21() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___keepAlive_21)); }
	inline bool get_keepAlive_21() const { return ___keepAlive_21; }
	inline bool* get_address_of_keepAlive_21() { return &___keepAlive_21; }
	inline void set_keepAlive_21(bool value)
	{
		___keepAlive_21 = value;
	}

	inline static int32_t get_offset_of_maxAutoRedirect_22() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___maxAutoRedirect_22)); }
	inline int32_t get_maxAutoRedirect_22() const { return ___maxAutoRedirect_22; }
	inline int32_t* get_address_of_maxAutoRedirect_22() { return &___maxAutoRedirect_22; }
	inline void set_maxAutoRedirect_22(int32_t value)
	{
		___maxAutoRedirect_22 = value;
	}

	inline static int32_t get_offset_of_mediaType_23() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___mediaType_23)); }
	inline String_t* get_mediaType_23() const { return ___mediaType_23; }
	inline String_t** get_address_of_mediaType_23() { return &___mediaType_23; }
	inline void set_mediaType_23(String_t* value)
	{
		___mediaType_23 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_23), value);
	}

	inline static int32_t get_offset_of_method_24() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___method_24)); }
	inline String_t* get_method_24() const { return ___method_24; }
	inline String_t** get_address_of_method_24() { return &___method_24; }
	inline void set_method_24(String_t* value)
	{
		___method_24 = value;
		Il2CppCodeGenWriteBarrier((&___method_24), value);
	}

	inline static int32_t get_offset_of_initialMethod_25() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___initialMethod_25)); }
	inline String_t* get_initialMethod_25() const { return ___initialMethod_25; }
	inline String_t** get_address_of_initialMethod_25() { return &___initialMethod_25; }
	inline void set_initialMethod_25(String_t* value)
	{
		___initialMethod_25 = value;
		Il2CppCodeGenWriteBarrier((&___initialMethod_25), value);
	}

	inline static int32_t get_offset_of_pipelined_26() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___pipelined_26)); }
	inline bool get_pipelined_26() const { return ___pipelined_26; }
	inline bool* get_address_of_pipelined_26() { return &___pipelined_26; }
	inline void set_pipelined_26(bool value)
	{
		___pipelined_26 = value;
	}

	inline static int32_t get_offset_of_preAuthenticate_27() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___preAuthenticate_27)); }
	inline bool get_preAuthenticate_27() const { return ___preAuthenticate_27; }
	inline bool* get_address_of_preAuthenticate_27() { return &___preAuthenticate_27; }
	inline void set_preAuthenticate_27(bool value)
	{
		___preAuthenticate_27 = value;
	}

	inline static int32_t get_offset_of_usedPreAuth_28() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___usedPreAuth_28)); }
	inline bool get_usedPreAuth_28() const { return ___usedPreAuth_28; }
	inline bool* get_address_of_usedPreAuth_28() { return &___usedPreAuth_28; }
	inline void set_usedPreAuth_28(bool value)
	{
		___usedPreAuth_28 = value;
	}

	inline static int32_t get_offset_of_version_29() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___version_29)); }
	inline Version_t2250829542 * get_version_29() const { return ___version_29; }
	inline Version_t2250829542 ** get_address_of_version_29() { return &___version_29; }
	inline void set_version_29(Version_t2250829542 * value)
	{
		___version_29 = value;
		Il2CppCodeGenWriteBarrier((&___version_29), value);
	}

	inline static int32_t get_offset_of_actualVersion_30() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___actualVersion_30)); }
	inline Version_t2250829542 * get_actualVersion_30() const { return ___actualVersion_30; }
	inline Version_t2250829542 ** get_address_of_actualVersion_30() { return &___actualVersion_30; }
	inline void set_actualVersion_30(Version_t2250829542 * value)
	{
		___actualVersion_30 = value;
		Il2CppCodeGenWriteBarrier((&___actualVersion_30), value);
	}

	inline static int32_t get_offset_of_proxy_31() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___proxy_31)); }
	inline RuntimeObject* get_proxy_31() const { return ___proxy_31; }
	inline RuntimeObject** get_address_of_proxy_31() { return &___proxy_31; }
	inline void set_proxy_31(RuntimeObject* value)
	{
		___proxy_31 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_31), value);
	}

	inline static int32_t get_offset_of_sendChunked_32() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___sendChunked_32)); }
	inline bool get_sendChunked_32() const { return ___sendChunked_32; }
	inline bool* get_address_of_sendChunked_32() { return &___sendChunked_32; }
	inline void set_sendChunked_32(bool value)
	{
		___sendChunked_32 = value;
	}

	inline static int32_t get_offset_of_servicePoint_33() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___servicePoint_33)); }
	inline ServicePoint_t3505702983 * get_servicePoint_33() const { return ___servicePoint_33; }
	inline ServicePoint_t3505702983 ** get_address_of_servicePoint_33() { return &___servicePoint_33; }
	inline void set_servicePoint_33(ServicePoint_t3505702983 * value)
	{
		___servicePoint_33 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_33), value);
	}

	inline static int32_t get_offset_of_timeout_34() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___timeout_34)); }
	inline int32_t get_timeout_34() const { return ___timeout_34; }
	inline int32_t* get_address_of_timeout_34() { return &___timeout_34; }
	inline void set_timeout_34(int32_t value)
	{
		___timeout_34 = value;
	}

	inline static int32_t get_offset_of_writeStream_35() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___writeStream_35)); }
	inline WebConnectionStream_t3750989120 * get_writeStream_35() const { return ___writeStream_35; }
	inline WebConnectionStream_t3750989120 ** get_address_of_writeStream_35() { return &___writeStream_35; }
	inline void set_writeStream_35(WebConnectionStream_t3750989120 * value)
	{
		___writeStream_35 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_35), value);
	}

	inline static int32_t get_offset_of_webResponse_36() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___webResponse_36)); }
	inline HttpWebResponse_t2352629529 * get_webResponse_36() const { return ___webResponse_36; }
	inline HttpWebResponse_t2352629529 ** get_address_of_webResponse_36() { return &___webResponse_36; }
	inline void set_webResponse_36(HttpWebResponse_t2352629529 * value)
	{
		___webResponse_36 = value;
		Il2CppCodeGenWriteBarrier((&___webResponse_36), value);
	}

	inline static int32_t get_offset_of_asyncWrite_37() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___asyncWrite_37)); }
	inline WebAsyncResult_t1279310773 * get_asyncWrite_37() const { return ___asyncWrite_37; }
	inline WebAsyncResult_t1279310773 ** get_address_of_asyncWrite_37() { return &___asyncWrite_37; }
	inline void set_asyncWrite_37(WebAsyncResult_t1279310773 * value)
	{
		___asyncWrite_37 = value;
		Il2CppCodeGenWriteBarrier((&___asyncWrite_37), value);
	}

	inline static int32_t get_offset_of_asyncRead_38() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___asyncRead_38)); }
	inline WebAsyncResult_t1279310773 * get_asyncRead_38() const { return ___asyncRead_38; }
	inline WebAsyncResult_t1279310773 ** get_address_of_asyncRead_38() { return &___asyncRead_38; }
	inline void set_asyncRead_38(WebAsyncResult_t1279310773 * value)
	{
		___asyncRead_38 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRead_38), value);
	}

	inline static int32_t get_offset_of_abortHandler_39() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___abortHandler_39)); }
	inline EventHandler_t369332490 * get_abortHandler_39() const { return ___abortHandler_39; }
	inline EventHandler_t369332490 ** get_address_of_abortHandler_39() { return &___abortHandler_39; }
	inline void set_abortHandler_39(EventHandler_t369332490 * value)
	{
		___abortHandler_39 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_39), value);
	}

	inline static int32_t get_offset_of_aborted_40() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___aborted_40)); }
	inline int32_t get_aborted_40() const { return ___aborted_40; }
	inline int32_t* get_address_of_aborted_40() { return &___aborted_40; }
	inline void set_aborted_40(int32_t value)
	{
		___aborted_40 = value;
	}

	inline static int32_t get_offset_of_redirects_41() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___redirects_41)); }
	inline int32_t get_redirects_41() const { return ___redirects_41; }
	inline int32_t* get_address_of_redirects_41() { return &___redirects_41; }
	inline void set_redirects_41(int32_t value)
	{
		___redirects_41 = value;
	}

	inline static int32_t get_offset_of_expectContinue_42() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___expectContinue_42)); }
	inline bool get_expectContinue_42() const { return ___expectContinue_42; }
	inline bool* get_address_of_expectContinue_42() { return &___expectContinue_42; }
	inline void set_expectContinue_42(bool value)
	{
		___expectContinue_42 = value;
	}

	inline static int32_t get_offset_of_authCompleted_43() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___authCompleted_43)); }
	inline bool get_authCompleted_43() const { return ___authCompleted_43; }
	inline bool* get_address_of_authCompleted_43() { return &___authCompleted_43; }
	inline void set_authCompleted_43(bool value)
	{
		___authCompleted_43 = value;
	}

	inline static int32_t get_offset_of_bodyBuffer_44() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___bodyBuffer_44)); }
	inline ByteU5BU5D_t434619169* get_bodyBuffer_44() const { return ___bodyBuffer_44; }
	inline ByteU5BU5D_t434619169** get_address_of_bodyBuffer_44() { return &___bodyBuffer_44; }
	inline void set_bodyBuffer_44(ByteU5BU5D_t434619169* value)
	{
		___bodyBuffer_44 = value;
		Il2CppCodeGenWriteBarrier((&___bodyBuffer_44), value);
	}

	inline static int32_t get_offset_of_bodyBufferLength_45() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___bodyBufferLength_45)); }
	inline int32_t get_bodyBufferLength_45() const { return ___bodyBufferLength_45; }
	inline int32_t* get_address_of_bodyBufferLength_45() { return &___bodyBufferLength_45; }
	inline void set_bodyBufferLength_45(int32_t value)
	{
		___bodyBufferLength_45 = value;
	}

	inline static int32_t get_offset_of_getResponseCalled_46() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___getResponseCalled_46)); }
	inline bool get_getResponseCalled_46() const { return ___getResponseCalled_46; }
	inline bool* get_address_of_getResponseCalled_46() { return &___getResponseCalled_46; }
	inline void set_getResponseCalled_46(bool value)
	{
		___getResponseCalled_46 = value;
	}

	inline static int32_t get_offset_of_saved_exc_47() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___saved_exc_47)); }
	inline Exception_t4086964929 * get_saved_exc_47() const { return ___saved_exc_47; }
	inline Exception_t4086964929 ** get_address_of_saved_exc_47() { return &___saved_exc_47; }
	inline void set_saved_exc_47(Exception_t4086964929 * value)
	{
		___saved_exc_47 = value;
		Il2CppCodeGenWriteBarrier((&___saved_exc_47), value);
	}

	inline static int32_t get_offset_of_locker_48() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___locker_48)); }
	inline RuntimeObject * get_locker_48() const { return ___locker_48; }
	inline RuntimeObject ** get_address_of_locker_48() { return &___locker_48; }
	inline void set_locker_48(RuntimeObject * value)
	{
		___locker_48 = value;
		Il2CppCodeGenWriteBarrier((&___locker_48), value);
	}

	inline static int32_t get_offset_of_is_ntlm_auth_49() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___is_ntlm_auth_49)); }
	inline bool get_is_ntlm_auth_49() const { return ___is_ntlm_auth_49; }
	inline bool* get_address_of_is_ntlm_auth_49() { return &___is_ntlm_auth_49; }
	inline void set_is_ntlm_auth_49(bool value)
	{
		___is_ntlm_auth_49 = value;
	}

	inline static int32_t get_offset_of_finished_reading_50() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___finished_reading_50)); }
	inline bool get_finished_reading_50() const { return ___finished_reading_50; }
	inline bool* get_address_of_finished_reading_50() { return &___finished_reading_50; }
	inline void set_finished_reading_50(bool value)
	{
		___finished_reading_50 = value;
	}

	inline static int32_t get_offset_of_WebConnection_51() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___WebConnection_51)); }
	inline WebConnection_t3591103104 * get_WebConnection_51() const { return ___WebConnection_51; }
	inline WebConnection_t3591103104 ** get_address_of_WebConnection_51() { return &___WebConnection_51; }
	inline void set_WebConnection_51(WebConnection_t3591103104 * value)
	{
		___WebConnection_51 = value;
		Il2CppCodeGenWriteBarrier((&___WebConnection_51), value);
	}

	inline static int32_t get_offset_of_auto_decomp_52() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___auto_decomp_52)); }
	inline int32_t get_auto_decomp_52() const { return ___auto_decomp_52; }
	inline int32_t* get_address_of_auto_decomp_52() { return &___auto_decomp_52; }
	inline void set_auto_decomp_52(int32_t value)
	{
		___auto_decomp_52 = value;
	}

	inline static int32_t get_offset_of_readWriteTimeout_54() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___readWriteTimeout_54)); }
	inline int32_t get_readWriteTimeout_54() const { return ___readWriteTimeout_54; }
	inline int32_t* get_address_of_readWriteTimeout_54() { return &___readWriteTimeout_54; }
	inline void set_readWriteTimeout_54(int32_t value)
	{
		___readWriteTimeout_54 = value;
	}

	inline static int32_t get_offset_of_unsafe_auth_blah_55() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009, ___unsafe_auth_blah_55)); }
	inline bool get_unsafe_auth_blah_55() const { return ___unsafe_auth_blah_55; }
	inline bool* get_address_of_unsafe_auth_blah_55() { return &___unsafe_auth_blah_55; }
	inline void set_unsafe_auth_blah_55(bool value)
	{
		___unsafe_auth_blah_55 = value;
	}
};

struct HttpWebRequest_t4046343009_StaticFields
{
public:
	// System.Int32 System.Net.HttpWebRequest::defaultMaxResponseHeadersLength
	int32_t ___defaultMaxResponseHeadersLength_53;

public:
	inline static int32_t get_offset_of_defaultMaxResponseHeadersLength_53() { return static_cast<int32_t>(offsetof(HttpWebRequest_t4046343009_StaticFields, ___defaultMaxResponseHeadersLength_53)); }
	inline int32_t get_defaultMaxResponseHeadersLength_53() const { return ___defaultMaxResponseHeadersLength_53; }
	inline int32_t* get_address_of_defaultMaxResponseHeadersLength_53() { return &___defaultMaxResponseHeadersLength_53; }
	inline void set_defaultMaxResponseHeadersLength_53(int32_t value)
	{
		___defaultMaxResponseHeadersLength_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUEST_T4046343009_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (CookieContainer_t390599126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1100[4] = 
{
	CookieContainer_t390599126::get_offset_of_capacity_0(),
	CookieContainer_t390599126::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t390599126::get_offset_of_maxCookieSize_2(),
	CookieContainer_t390599126::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (CookieException_t2233408878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (DecompressionMethods_t639014177)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1102[4] = 
{
	DecompressionMethods_t639014177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (DefaultCertificatePolicy_t1704739774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (DigestHeaderParser_t477534254), -1, sizeof(DigestHeaderParser_t477534254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1104[5] = 
{
	DigestHeaderParser_t477534254::get_offset_of_header_0(),
	DigestHeaderParser_t477534254::get_offset_of_length_1(),
	DigestHeaderParser_t477534254::get_offset_of_pos_2(),
	DigestHeaderParser_t477534254_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t477534254::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (DigestSession_t3366850630), -1, sizeof(DigestSession_t3366850630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1105[6] = 
{
	DigestSession_t3366850630_StaticFields::get_offset_of_rng_0(),
	DigestSession_t3366850630::get_offset_of_lastUse_1(),
	DigestSession_t3366850630::get_offset_of__nc_2(),
	DigestSession_t3366850630::get_offset_of_hash_3(),
	DigestSession_t3366850630::get_offset_of_parser_4(),
	DigestSession_t3366850630::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (DigestClient_t4096343030), -1, sizeof(DigestClient_t4096343030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1106[1] = 
{
	DigestClient_t4096343030_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (Dns_t633402713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (EndPoint_t2958140078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (FileWebRequest_t3154753421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1109[14] = 
{
	FileWebRequest_t3154753421::get_offset_of_uri_6(),
	FileWebRequest_t3154753421::get_offset_of_webHeaders_7(),
	FileWebRequest_t3154753421::get_offset_of_credentials_8(),
	FileWebRequest_t3154753421::get_offset_of_connectionGroup_9(),
	FileWebRequest_t3154753421::get_offset_of_contentLength_10(),
	FileWebRequest_t3154753421::get_offset_of_fileAccess_11(),
	FileWebRequest_t3154753421::get_offset_of_method_12(),
	FileWebRequest_t3154753421::get_offset_of_proxy_13(),
	FileWebRequest_t3154753421::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t3154753421::get_offset_of_timeout_15(),
	FileWebRequest_t3154753421::get_offset_of_webResponse_16(),
	FileWebRequest_t3154753421::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t3154753421::get_offset_of_requesting_18(),
	FileWebRequest_t3154753421::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (FileWebStream_t1782281022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1110[1] = 
{
	FileWebStream_t1782281022::get_offset_of_webRequest_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (GetResponseCallback_t1254250368), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (FileWebRequestCreator_t2285499290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (FileWebResponse_t982058165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1113[5] = 
{
	FileWebResponse_t982058165::get_offset_of_responseUri_1(),
	FileWebResponse_t982058165::get_offset_of_fileStream_2(),
	FileWebResponse_t982058165::get_offset_of_contentLength_3(),
	FileWebResponse_t982058165::get_offset_of_webHeaders_4(),
	FileWebResponse_t982058165::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (FtpAsyncResult_t3500703300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1114[9] = 
{
	FtpAsyncResult_t3500703300::get_offset_of_response_0(),
	FtpAsyncResult_t3500703300::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t3500703300::get_offset_of_exception_2(),
	FtpAsyncResult_t3500703300::get_offset_of_callback_3(),
	FtpAsyncResult_t3500703300::get_offset_of_stream_4(),
	FtpAsyncResult_t3500703300::get_offset_of_state_5(),
	FtpAsyncResult_t3500703300::get_offset_of_completed_6(),
	FtpAsyncResult_t3500703300::get_offset_of_synch_7(),
	FtpAsyncResult_t3500703300::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (FtpDataStream_t3803374522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1115[5] = 
{
	FtpDataStream_t3803374522::get_offset_of_request_1(),
	FtpDataStream_t3803374522::get_offset_of_networkStream_2(),
	FtpDataStream_t3803374522::get_offset_of_disposed_3(),
	FtpDataStream_t3803374522::get_offset_of_isRead_4(),
	FtpDataStream_t3803374522::get_offset_of_totalRead_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (WriteDelegate_t949668826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (ReadDelegate_t87010224), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (FtpRequestCreator_t508984423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (FtpStatus_t4241934835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[2] = 
{
	FtpStatus_t4241934835::get_offset_of_statusCode_0(),
	FtpStatus_t4241934835::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (FtpStatusCode_t226517375)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1120[38] = 
{
	FtpStatusCode_t226517375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (FtpWebRequest_t4270856504), -1, sizeof(FtpWebRequest_t4270856504_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1121[31] = 
{
	FtpWebRequest_t4270856504::get_offset_of_requestUri_6(),
	FtpWebRequest_t4270856504::get_offset_of_file_name_7(),
	FtpWebRequest_t4270856504::get_offset_of_servicePoint_8(),
	FtpWebRequest_t4270856504::get_offset_of_origDataStream_9(),
	FtpWebRequest_t4270856504::get_offset_of_dataStream_10(),
	FtpWebRequest_t4270856504::get_offset_of_controlStream_11(),
	FtpWebRequest_t4270856504::get_offset_of_controlReader_12(),
	FtpWebRequest_t4270856504::get_offset_of_credentials_13(),
	FtpWebRequest_t4270856504::get_offset_of_hostEntry_14(),
	FtpWebRequest_t4270856504::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t4270856504::get_offset_of_proxy_16(),
	FtpWebRequest_t4270856504::get_offset_of_timeout_17(),
	FtpWebRequest_t4270856504::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t4270856504::get_offset_of_offset_19(),
	FtpWebRequest_t4270856504::get_offset_of_binary_20(),
	FtpWebRequest_t4270856504::get_offset_of_enableSsl_21(),
	FtpWebRequest_t4270856504::get_offset_of_usePassive_22(),
	FtpWebRequest_t4270856504::get_offset_of_keepAlive_23(),
	FtpWebRequest_t4270856504::get_offset_of_method_24(),
	FtpWebRequest_t4270856504::get_offset_of_renameTo_25(),
	FtpWebRequest_t4270856504::get_offset_of_locker_26(),
	FtpWebRequest_t4270856504::get_offset_of_requestState_27(),
	FtpWebRequest_t4270856504::get_offset_of_asyncResult_28(),
	FtpWebRequest_t4270856504::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t4270856504::get_offset_of_requestStream_30(),
	FtpWebRequest_t4270856504::get_offset_of_initial_path_31(),
	FtpWebRequest_t4270856504_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t4270856504::get_offset_of_callback_33(),
	FtpWebRequest_t4270856504_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t4270856504_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_35(),
	FtpWebRequest_t4270856504_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (RequestState_t1112878644)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1122[10] = 
{
	RequestState_t1112878644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (FtpWebResponse_t1950878246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[12] = 
{
	FtpWebResponse_t1950878246::get_offset_of_stream_1(),
	FtpWebResponse_t1950878246::get_offset_of_uri_2(),
	FtpWebResponse_t1950878246::get_offset_of_statusCode_3(),
	FtpWebResponse_t1950878246::get_offset_of_lastModified_4(),
	FtpWebResponse_t1950878246::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t1950878246::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t1950878246::get_offset_of_exitMessage_7(),
	FtpWebResponse_t1950878246::get_offset_of_statusDescription_8(),
	FtpWebResponse_t1950878246::get_offset_of_method_9(),
	FtpWebResponse_t1950878246::get_offset_of_disposed_10(),
	FtpWebResponse_t1950878246::get_offset_of_request_11(),
	FtpWebResponse_t1950878246::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (GlobalProxySelection_t3081702395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (HttpRequestCreator_t2802833056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (HttpStatusCode_t1537078283)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1126[47] = 
{
	HttpStatusCode_t1537078283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (HttpVersion_t2198520130), -1, sizeof(HttpVersion_t2198520130_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1127[2] = 
{
	HttpVersion_t2198520130_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t2198520130_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (HttpWebRequest_t4046343009), -1, sizeof(HttpWebRequest_t4046343009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1128[50] = 
{
	HttpWebRequest_t4046343009::get_offset_of_requestUri_6(),
	HttpWebRequest_t4046343009::get_offset_of_actualUri_7(),
	HttpWebRequest_t4046343009::get_offset_of_hostChanged_8(),
	HttpWebRequest_t4046343009::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t4046343009::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t4046343009::get_offset_of_certificates_11(),
	HttpWebRequest_t4046343009::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t4046343009::get_offset_of_contentLength_13(),
	HttpWebRequest_t4046343009::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t4046343009::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t4046343009::get_offset_of_credentials_16(),
	HttpWebRequest_t4046343009::get_offset_of_haveResponse_17(),
	HttpWebRequest_t4046343009::get_offset_of_haveRequest_18(),
	HttpWebRequest_t4046343009::get_offset_of_requestSent_19(),
	HttpWebRequest_t4046343009::get_offset_of_webHeaders_20(),
	HttpWebRequest_t4046343009::get_offset_of_keepAlive_21(),
	HttpWebRequest_t4046343009::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t4046343009::get_offset_of_mediaType_23(),
	HttpWebRequest_t4046343009::get_offset_of_method_24(),
	HttpWebRequest_t4046343009::get_offset_of_initialMethod_25(),
	HttpWebRequest_t4046343009::get_offset_of_pipelined_26(),
	HttpWebRequest_t4046343009::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t4046343009::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t4046343009::get_offset_of_version_29(),
	HttpWebRequest_t4046343009::get_offset_of_actualVersion_30(),
	HttpWebRequest_t4046343009::get_offset_of_proxy_31(),
	HttpWebRequest_t4046343009::get_offset_of_sendChunked_32(),
	HttpWebRequest_t4046343009::get_offset_of_servicePoint_33(),
	HttpWebRequest_t4046343009::get_offset_of_timeout_34(),
	HttpWebRequest_t4046343009::get_offset_of_writeStream_35(),
	HttpWebRequest_t4046343009::get_offset_of_webResponse_36(),
	HttpWebRequest_t4046343009::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t4046343009::get_offset_of_asyncRead_38(),
	HttpWebRequest_t4046343009::get_offset_of_abortHandler_39(),
	HttpWebRequest_t4046343009::get_offset_of_aborted_40(),
	HttpWebRequest_t4046343009::get_offset_of_redirects_41(),
	HttpWebRequest_t4046343009::get_offset_of_expectContinue_42(),
	HttpWebRequest_t4046343009::get_offset_of_authCompleted_43(),
	HttpWebRequest_t4046343009::get_offset_of_bodyBuffer_44(),
	HttpWebRequest_t4046343009::get_offset_of_bodyBufferLength_45(),
	HttpWebRequest_t4046343009::get_offset_of_getResponseCalled_46(),
	HttpWebRequest_t4046343009::get_offset_of_saved_exc_47(),
	HttpWebRequest_t4046343009::get_offset_of_locker_48(),
	HttpWebRequest_t4046343009::get_offset_of_is_ntlm_auth_49(),
	HttpWebRequest_t4046343009::get_offset_of_finished_reading_50(),
	HttpWebRequest_t4046343009::get_offset_of_WebConnection_51(),
	HttpWebRequest_t4046343009::get_offset_of_auto_decomp_52(),
	HttpWebRequest_t4046343009_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_53(),
	HttpWebRequest_t4046343009::get_offset_of_readWriteTimeout_54(),
	HttpWebRequest_t4046343009::get_offset_of_unsafe_auth_blah_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (HttpWebResponse_t2352629529), -1, sizeof(HttpWebResponse_t2352629529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1129[14] = 
{
	HttpWebResponse_t2352629529::get_offset_of_uri_1(),
	HttpWebResponse_t2352629529::get_offset_of_webHeaders_2(),
	HttpWebResponse_t2352629529::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t2352629529::get_offset_of_method_4(),
	HttpWebResponse_t2352629529::get_offset_of_version_5(),
	HttpWebResponse_t2352629529::get_offset_of_statusCode_6(),
	HttpWebResponse_t2352629529::get_offset_of_statusDescription_7(),
	HttpWebResponse_t2352629529::get_offset_of_contentLength_8(),
	HttpWebResponse_t2352629529::get_offset_of_contentType_9(),
	HttpWebResponse_t2352629529::get_offset_of_cookie_container_10(),
	HttpWebResponse_t2352629529::get_offset_of_disposed_11(),
	HttpWebResponse_t2352629529::get_offset_of_stream_12(),
	HttpWebResponse_t2352629529::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t2352629529_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (CookieParser_t3149482976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1130[3] = 
{
	CookieParser_t3149482976::get_offset_of_header_0(),
	CookieParser_t3149482976::get_offset_of_pos_1(),
	CookieParser_t3149482976::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (IPAddress_t86924491), -1, sizeof(IPAddress_t86924491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1135[11] = 
{
	IPAddress_t86924491::get_offset_of_m_Address_0(),
	IPAddress_t86924491::get_offset_of_m_Family_1(),
	IPAddress_t86924491::get_offset_of_m_Numbers_2(),
	IPAddress_t86924491::get_offset_of_m_ScopeId_3(),
	IPAddress_t86924491_StaticFields::get_offset_of_Any_4(),
	IPAddress_t86924491_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t86924491_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t86924491_StaticFields::get_offset_of_None_7(),
	IPAddress_t86924491_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t86924491_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t86924491_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (IPEndPoint_t1226219872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1136[2] = 
{
	IPEndPoint_t1226219872::get_offset_of_address_0(),
	IPEndPoint_t1226219872::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (IPHostEntry_t4214309905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1137[3] = 
{
	IPHostEntry_t4214309905::get_offset_of_addressList_0(),
	IPHostEntry_t4214309905::get_offset_of_aliases_1(),
	IPHostEntry_t4214309905::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (IPv6Address_t496378312), -1, sizeof(IPv6Address_t496378312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1138[5] = 
{
	IPv6Address_t496378312::get_offset_of_address_0(),
	IPv6Address_t496378312::get_offset_of_prefixLength_1(),
	IPv6Address_t496378312::get_offset_of_scopeId_2(),
	IPv6Address_t496378312_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t496378312_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (NetworkCredential_t1328334253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1141[3] = 
{
	NetworkCredential_t1328334253::get_offset_of_userName_0(),
	NetworkCredential_t1328334253::get_offset_of_password_1(),
	NetworkCredential_t1328334253::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (ProtocolViolationException_t4070611298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (SecurityProtocolType_t1627302747)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1143[3] = 
{
	SecurityProtocolType_t1627302747::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (ServicePoint_t3505702983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[17] = 
{
	ServicePoint_t3505702983::get_offset_of_uri_0(),
	ServicePoint_t3505702983::get_offset_of_connectionLimit_1(),
	ServicePoint_t3505702983::get_offset_of_maxIdleTime_2(),
	ServicePoint_t3505702983::get_offset_of_currentConnections_3(),
	ServicePoint_t3505702983::get_offset_of_idleSince_4(),
	ServicePoint_t3505702983::get_offset_of_protocolVersion_5(),
	ServicePoint_t3505702983::get_offset_of_certificate_6(),
	ServicePoint_t3505702983::get_offset_of_clientCertificate_7(),
	ServicePoint_t3505702983::get_offset_of_host_8(),
	ServicePoint_t3505702983::get_offset_of_usesProxy_9(),
	ServicePoint_t3505702983::get_offset_of_groups_10(),
	ServicePoint_t3505702983::get_offset_of_sendContinue_11(),
	ServicePoint_t3505702983::get_offset_of_useConnect_12(),
	ServicePoint_t3505702983::get_offset_of_locker_13(),
	ServicePoint_t3505702983::get_offset_of_hostE_14(),
	ServicePoint_t3505702983::get_offset_of_useNagle_15(),
	ServicePoint_t3505702983::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (ServicePointManager_t3086999306), -1, sizeof(ServicePointManager_t3086999306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1145[10] = 
{
	ServicePointManager_t3086999306_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t3086999306_StaticFields::get_offset_of_server_cert_cb_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (SPKey_t742829915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1146[2] = 
{
	SPKey_t742829915::get_offset_of_uri_0(),
	SPKey_t742829915::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (ChainValidationHelper_t1757992727), -1, sizeof(ChainValidationHelper_t1757992727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1147[4] = 
{
	ChainValidationHelper_t1757992727::get_offset_of_sender_0(),
	ChainValidationHelper_t1757992727::get_offset_of_host_1(),
	ChainValidationHelper_t1757992727_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t1757992727_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (SocketAddress_t149599595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1148[1] = 
{
	SocketAddress_t149599595::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (WebAsyncResult_t1279310773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1149[17] = 
{
	WebAsyncResult_t1279310773::get_offset_of_handle_0(),
	WebAsyncResult_t1279310773::get_offset_of_synch_1(),
	WebAsyncResult_t1279310773::get_offset_of_isCompleted_2(),
	WebAsyncResult_t1279310773::get_offset_of_cb_3(),
	WebAsyncResult_t1279310773::get_offset_of_state_4(),
	WebAsyncResult_t1279310773::get_offset_of_nbytes_5(),
	WebAsyncResult_t1279310773::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t1279310773::get_offset_of_callbackDone_7(),
	WebAsyncResult_t1279310773::get_offset_of_exc_8(),
	WebAsyncResult_t1279310773::get_offset_of_response_9(),
	WebAsyncResult_t1279310773::get_offset_of_writeStream_10(),
	WebAsyncResult_t1279310773::get_offset_of_buffer_11(),
	WebAsyncResult_t1279310773::get_offset_of_offset_12(),
	WebAsyncResult_t1279310773::get_offset_of_size_13(),
	WebAsyncResult_t1279310773::get_offset_of_locker_14(),
	WebAsyncResult_t1279310773::get_offset_of_EndCalled_15(),
	WebAsyncResult_t1279310773::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (ReadState_t3061371541)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1150[5] = 
{
	ReadState_t3061371541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (WebConnection_t3591103104), -1, sizeof(WebConnection_t3591103104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1151[32] = 
{
	WebConnection_t3591103104::get_offset_of_sPoint_0(),
	WebConnection_t3591103104::get_offset_of_nstream_1(),
	WebConnection_t3591103104::get_offset_of_socket_2(),
	WebConnection_t3591103104::get_offset_of_socketLock_3(),
	WebConnection_t3591103104::get_offset_of_status_4(),
	WebConnection_t3591103104::get_offset_of_initConn_5(),
	WebConnection_t3591103104::get_offset_of_keepAlive_6(),
	WebConnection_t3591103104::get_offset_of_buffer_7(),
	WebConnection_t3591103104_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t3591103104::get_offset_of_abortHandler_9(),
	WebConnection_t3591103104::get_offset_of_abortHelper_10(),
	WebConnection_t3591103104::get_offset_of_readState_11(),
	WebConnection_t3591103104::get_offset_of_Data_12(),
	WebConnection_t3591103104::get_offset_of_chunkedRead_13(),
	WebConnection_t3591103104::get_offset_of_chunkStream_14(),
	WebConnection_t3591103104::get_offset_of_queue_15(),
	WebConnection_t3591103104::get_offset_of_reused_16(),
	WebConnection_t3591103104::get_offset_of_position_17(),
	WebConnection_t3591103104::get_offset_of_busy_18(),
	WebConnection_t3591103104::get_offset_of_priority_request_19(),
	WebConnection_t3591103104::get_offset_of_ntlm_credentials_20(),
	WebConnection_t3591103104::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t3591103104::get_offset_of_unsafe_sharing_22(),
	WebConnection_t3591103104::get_offset_of_ssl_23(),
	WebConnection_t3591103104::get_offset_of_certsAvailable_24(),
	WebConnection_t3591103104::get_offset_of_connect_exception_25(),
	WebConnection_t3591103104_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t3591103104_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t3591103104_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t3591103104_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t3591103104_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t3591103104_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (AbortHelper_t2979836766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1152[1] = 
{
	AbortHelper_t2979836766::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (WebConnectionData_t1471135888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1153[7] = 
{
	WebConnectionData_t1471135888::get_offset_of_request_0(),
	WebConnectionData_t1471135888::get_offset_of_StatusCode_1(),
	WebConnectionData_t1471135888::get_offset_of_StatusDescription_2(),
	WebConnectionData_t1471135888::get_offset_of_Headers_3(),
	WebConnectionData_t1471135888::get_offset_of_Version_4(),
	WebConnectionData_t1471135888::get_offset_of_stream_5(),
	WebConnectionData_t1471135888::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (WebConnectionGroup_t1241471072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1154[5] = 
{
	WebConnectionGroup_t1241471072::get_offset_of_sPoint_0(),
	WebConnectionGroup_t1241471072::get_offset_of_name_1(),
	WebConnectionGroup_t1241471072::get_offset_of_connections_2(),
	WebConnectionGroup_t1241471072::get_offset_of_rnd_3(),
	WebConnectionGroup_t1241471072::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (WebConnectionStream_t3750989120), -1, sizeof(WebConnectionStream_t3750989120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1155[27] = 
{
	WebConnectionStream_t3750989120_StaticFields::get_offset_of_crlf_1(),
	WebConnectionStream_t3750989120::get_offset_of_isRead_2(),
	WebConnectionStream_t3750989120::get_offset_of_cnc_3(),
	WebConnectionStream_t3750989120::get_offset_of_request_4(),
	WebConnectionStream_t3750989120::get_offset_of_readBuffer_5(),
	WebConnectionStream_t3750989120::get_offset_of_readBufferOffset_6(),
	WebConnectionStream_t3750989120::get_offset_of_readBufferSize_7(),
	WebConnectionStream_t3750989120::get_offset_of_contentLength_8(),
	WebConnectionStream_t3750989120::get_offset_of_totalRead_9(),
	WebConnectionStream_t3750989120::get_offset_of_totalWritten_10(),
	WebConnectionStream_t3750989120::get_offset_of_nextReadCalled_11(),
	WebConnectionStream_t3750989120::get_offset_of_pendingReads_12(),
	WebConnectionStream_t3750989120::get_offset_of_pendingWrites_13(),
	WebConnectionStream_t3750989120::get_offset_of_pending_14(),
	WebConnectionStream_t3750989120::get_offset_of_allowBuffering_15(),
	WebConnectionStream_t3750989120::get_offset_of_sendChunked_16(),
	WebConnectionStream_t3750989120::get_offset_of_writeBuffer_17(),
	WebConnectionStream_t3750989120::get_offset_of_requestWritten_18(),
	WebConnectionStream_t3750989120::get_offset_of_headers_19(),
	WebConnectionStream_t3750989120::get_offset_of_disposed_20(),
	WebConnectionStream_t3750989120::get_offset_of_headersSent_21(),
	WebConnectionStream_t3750989120::get_offset_of_locker_22(),
	WebConnectionStream_t3750989120::get_offset_of_initRead_23(),
	WebConnectionStream_t3750989120::get_offset_of_read_eof_24(),
	WebConnectionStream_t3750989120::get_offset_of_complete_request_written_25(),
	WebConnectionStream_t3750989120::get_offset_of_read_timeout_26(),
	WebConnectionStream_t3750989120::get_offset_of_write_timeout_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { sizeof (WebException_t321944699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1156[2] = 
{
	WebException_t321944699::get_offset_of_response_12(),
	WebException_t321944699::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (WebExceptionStatus_t844493678)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1157[22] = 
{
	WebExceptionStatus_t844493678::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (WebHeaderCollection_t592894254), -1, sizeof(WebHeaderCollection_t592894254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1158[5] = 
{
	WebHeaderCollection_t592894254_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t592894254_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t592894254_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t592894254::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t592894254_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (WebProxy_t1068617986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1159[5] = 
{
	WebProxy_t1068617986::get_offset_of_address_0(),
	WebProxy_t1068617986::get_offset_of_bypassOnLocal_1(),
	WebProxy_t1068617986::get_offset_of_bypassList_2(),
	WebProxy_t1068617986::get_offset_of_credentials_3(),
	WebProxy_t1068617986::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (WebRequest_t1788933422), -1, sizeof(WebRequest_t1788933422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1160[5] = 
{
	WebRequest_t1788933422_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t1788933422_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t1788933422_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t1788933422::get_offset_of_authentication_level_4(),
	WebRequest_t1788933422_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (WebResponse_t3616229315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (SslProtocols_t3922469944)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1162[6] = 
{
	SslProtocols_t3922469944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (OSX509Certificates_t2020383372), -1, sizeof(OSX509Certificates_t2020383372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1163[1] = 
{
	OSX509Certificates_t2020383372_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (SecTrustResult_t1623568823)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1164[9] = 
{
	SecTrustResult_t1623568823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (OpenFlags_t3015610782)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1165[6] = 
{
	OpenFlags_t3015610782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (PublicKey_t3840515815), -1, sizeof(PublicKey_t3840515815_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1166[5] = 
{
	PublicKey_t3840515815::get_offset_of__key_0(),
	PublicKey_t3840515815::get_offset_of__keyValue_1(),
	PublicKey_t3840515815::get_offset_of__params_2(),
	PublicKey_t3840515815::get_offset_of__oid_3(),
	PublicKey_t3840515815_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (StoreLocation_t3191330486)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1167[3] = 
{
	StoreLocation_t3191330486::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (StoreName_t3887743455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1168[9] = 
{
	StoreName_t3887743455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (X500DistinguishedName_t572620573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1169[1] = 
{
	X500DistinguishedName_t572620573::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (X500DistinguishedNameFlags_t1994920494)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1170[11] = 
{
	X500DistinguishedNameFlags_t1994920494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (X509BasicConstraintsExtension_t1243739534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1243739534::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t1243739534::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1243739534::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t1243739534::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (X509Certificate2_t1403970469), -1, sizeof(X509Certificate2_t1403970469_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1172[13] = 
{
	X509Certificate2_t1403970469::get_offset_of__archived_5(),
	X509Certificate2_t1403970469::get_offset_of__extensions_6(),
	X509Certificate2_t1403970469::get_offset_of__name_7(),
	X509Certificate2_t1403970469::get_offset_of__serial_8(),
	X509Certificate2_t1403970469::get_offset_of__publicKey_9(),
	X509Certificate2_t1403970469::get_offset_of_issuer_name_10(),
	X509Certificate2_t1403970469::get_offset_of_subject_name_11(),
	X509Certificate2_t1403970469::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t1403970469::get_offset_of__cert_13(),
	X509Certificate2_t1403970469_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t1403970469_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t1403970469_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t1403970469_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (X509Certificate2Collection_t454981218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (X509Certificate2Enumerator_t4028473944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1174[1] = 
{
	X509Certificate2Enumerator_t4028473944::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (X509CertificateCollection_t728319640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (X509CertificateEnumerator_t3394667710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1176[1] = 
{
	X509CertificateEnumerator_t3394667710::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (X509Chain_t388833386), -1, sizeof(X509Chain_t388833386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1177[15] = 
{
	X509Chain_t388833386::get_offset_of_location_0(),
	X509Chain_t388833386::get_offset_of_elements_1(),
	X509Chain_t388833386::get_offset_of_policy_2(),
	X509Chain_t388833386::get_offset_of_status_3(),
	X509Chain_t388833386_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t388833386::get_offset_of_max_path_length_5(),
	X509Chain_t388833386::get_offset_of_working_issuer_name_6(),
	X509Chain_t388833386::get_offset_of_working_public_key_7(),
	X509Chain_t388833386::get_offset_of_bce_restriction_8(),
	X509Chain_t388833386::get_offset_of_roots_9(),
	X509Chain_t388833386::get_offset_of_cas_10(),
	X509Chain_t388833386::get_offset_of_collection_11(),
	X509Chain_t388833386_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_12(),
	X509Chain_t388833386_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_13(),
	X509Chain_t388833386_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (X509ChainElement_t1254084101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1178[4] = 
{
	X509ChainElement_t1254084101::get_offset_of_certificate_0(),
	X509ChainElement_t1254084101::get_offset_of_status_1(),
	X509ChainElement_t1254084101::get_offset_of_info_2(),
	X509ChainElement_t1254084101::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (X509ChainElementCollection_t2987578632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1179[1] = 
{
	X509ChainElementCollection_t2987578632::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (X509ChainElementEnumerator_t1429529268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1180[1] = 
{
	X509ChainElementEnumerator_t1429529268::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (X509ChainPolicy_t45074389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1181[8] = 
{
	X509ChainPolicy_t45074389::get_offset_of_apps_0(),
	X509ChainPolicy_t45074389::get_offset_of_cert_1(),
	X509ChainPolicy_t45074389::get_offset_of_store_2(),
	X509ChainPolicy_t45074389::get_offset_of_rflag_3(),
	X509ChainPolicy_t45074389::get_offset_of_mode_4(),
	X509ChainPolicy_t45074389::get_offset_of_timeout_5(),
	X509ChainPolicy_t45074389::get_offset_of_vflags_6(),
	X509ChainPolicy_t45074389::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (X509ChainStatus_t3602726638)+ sizeof (RuntimeObject), sizeof(X509ChainStatus_t3602726638_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1182[2] = 
{
	X509ChainStatus_t3602726638::get_offset_of_status_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	X509ChainStatus_t3602726638::get_offset_of_info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (X509ChainStatusFlags_t849332985)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1183[24] = 
{
	X509ChainStatusFlags_t849332985::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (X509EnhancedKeyUsageExtension_t706278074), -1, sizeof(X509EnhancedKeyUsageExtension_t706278074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1184[3] = 
{
	X509EnhancedKeyUsageExtension_t706278074::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t706278074::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t706278074_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (X509Extension_t1338557205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1185[1] = 
{
	X509Extension_t1338557205::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (X509ExtensionCollection_t3160461637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1186[1] = 
{
	X509ExtensionCollection_t3160461637::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (X509ExtensionEnumerator_t2350753659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1187[1] = 
{
	X509ExtensionEnumerator_t2350753659::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (X509FindType_t2680667608)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1188[16] = 
{
	X509FindType_t2680667608::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (X509KeyUsageExtension_t2457957465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t2457957465::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t2457957465::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (X509KeyUsageFlags_t2736574660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1190[11] = 
{
	X509KeyUsageFlags_t2736574660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (X509NameType_t3605404887)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1191[7] = 
{
	X509NameType_t3605404887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (X509RevocationFlag_t1554360456)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1192[4] = 
{
	X509RevocationFlag_t1554360456::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (X509RevocationMode_t2152948854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1193[4] = 
{
	X509RevocationMode_t2152948854::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (X509Store_t2057865090), -1, sizeof(X509Store_t2057865090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1194[6] = 
{
	X509Store_t2057865090::get_offset_of__name_0(),
	X509Store_t2057865090::get_offset_of__location_1(),
	X509Store_t2057865090::get_offset_of_list_2(),
	X509Store_t2057865090::get_offset_of__flags_3(),
	X509Store_t2057865090::get_offset_of_store_4(),
	X509Store_t2057865090_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (X509SubjectKeyIdentifierExtension_t2725568395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1195[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2725568395::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t2725568395::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t2725568395::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t2306134369)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1196[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t2306134369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (X509VerificationFlags_t2292168690)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1197[15] = 
{
	X509VerificationFlags_t2292168690::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (AsnDecodeStatus_t1018007474)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1198[7] = 
{
	AsnDecodeStatus_t1018007474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (AsnEncodedData_t3005941167), -1, sizeof(AsnEncodedData_t3005941167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1199[3] = 
{
	AsnEncodedData_t3005941167::get_offset_of__oid_0(),
	AsnEncodedData_t3005941167::get_offset_of__raw_1(),
	AsnEncodedData_t3005941167_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
