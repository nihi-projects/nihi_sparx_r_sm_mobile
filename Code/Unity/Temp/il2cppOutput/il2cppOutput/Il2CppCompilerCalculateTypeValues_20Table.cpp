﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.XmlParserInput
struct XmlParserInput_t2512531096;
// System.Collections.Stack
struct Stack_t535311253;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Text.StringBuilder
struct StringBuilder_t622404039;
// System.String
struct String_t;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t3150902482;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// System.UInt32[]
struct UInt32U5BU5D_t1653015247;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t4266884469;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t3634927218;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t748347165;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t1551549928;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t2395926230;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t2979384550;
// System.Collections.ArrayList
struct ArrayList_t4250946984;
// System.Xml.XmlResolver
struct XmlResolver_t161349657;
// System.Xml.XmlNameTable
struct XmlNameTable_t347431366;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t864114708;
// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// System.Xml.XmlNode
struct XmlNode_t3729859639;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t596510042;
// System.Xml.XmlDocument
struct XmlDocument_t2823332853;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t4201287521;
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t1877457368;
// System.Type
struct Type_t;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t1707096551;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t4041842875;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t2841412075;
// System.Uri
struct Uri_t3269222095;
// Mono.Xml.DTDNode
struct DTDNode_t3993235537;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4215975066;
// System.Xml.XmlElement
struct XmlElement_t687149941;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t1006717423;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1947670907;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t784538573;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct List_1_t1462362804;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t1130471374;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t2934028315;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t1708784850;
// Mono.Xml.Schema.XsdString
struct XsdString_t874272089;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t4261832402;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t1018821129;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t255382392;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t2642531948;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t2756618685;
// Mono.Xml.Schema.XsdName
struct XsdName_t3987376123;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t1572205312;
// Mono.Xml.Schema.XsdID
struct XsdID_t4001206665;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t1646020451;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t2404058149;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t915858520;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t63829613;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t1221254960;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t950276334;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t3891685094;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t1196606063;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t2543007977;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t1409564899;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t1345023881;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t1188126083;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t3166100678;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t3741596672;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t3230006419;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t2292208357;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t3062058043;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t206124302;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t535826610;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t1847923358;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t1419197403;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t1240435854;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t1271651578;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t232015842;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t3025817362;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t2592875340;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t2932582003;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t3734795353;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t1905470603;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t461718195;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t240306832;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t960257253;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t4242852660;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t2084261129;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t1441225556;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t411669023;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t3890991598;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t2375956300;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t1060067746;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t133429609;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t4072949440;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3397264039;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t803373732;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2744860620;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t2926683629;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t740477960;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t1082177942;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DTDREADER_T1016753964_H
#define DTDREADER_T1016753964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DTDReader
struct  DTDReader_t1016753964  : public RuntimeObject
{
public:
	// System.Xml.XmlParserInput System.Xml.DTDReader::currentInput
	XmlParserInput_t2512531096 * ___currentInput_0;
	// System.Collections.Stack System.Xml.DTDReader::parserInputStack
	Stack_t535311253 * ___parserInputStack_1;
	// System.Char[] System.Xml.DTDReader::nameBuffer
	CharU5BU5D_t3419619864* ___nameBuffer_2;
	// System.Int32 System.Xml.DTDReader::nameLength
	int32_t ___nameLength_3;
	// System.Int32 System.Xml.DTDReader::nameCapacity
	int32_t ___nameCapacity_4;
	// System.Text.StringBuilder System.Xml.DTDReader::valueBuffer
	StringBuilder_t622404039 * ___valueBuffer_5;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_6;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_7;
	// System.Int32 System.Xml.DTDReader::dtdIncludeSect
	int32_t ___dtdIncludeSect_8;
	// System.Boolean System.Xml.DTDReader::normalization
	bool ___normalization_9;
	// System.Boolean System.Xml.DTDReader::processingInternalSubset
	bool ___processingInternalSubset_10;
	// System.String System.Xml.DTDReader::cachedPublicId
	String_t* ___cachedPublicId_11;
	// System.String System.Xml.DTDReader::cachedSystemId
	String_t* ___cachedSystemId_12;
	// Mono.Xml.DTDObjectModel System.Xml.DTDReader::DTD
	DTDObjectModel_t3150902482 * ___DTD_13;

public:
	inline static int32_t get_offset_of_currentInput_0() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___currentInput_0)); }
	inline XmlParserInput_t2512531096 * get_currentInput_0() const { return ___currentInput_0; }
	inline XmlParserInput_t2512531096 ** get_address_of_currentInput_0() { return &___currentInput_0; }
	inline void set_currentInput_0(XmlParserInput_t2512531096 * value)
	{
		___currentInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentInput_0), value);
	}

	inline static int32_t get_offset_of_parserInputStack_1() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___parserInputStack_1)); }
	inline Stack_t535311253 * get_parserInputStack_1() const { return ___parserInputStack_1; }
	inline Stack_t535311253 ** get_address_of_parserInputStack_1() { return &___parserInputStack_1; }
	inline void set_parserInputStack_1(Stack_t535311253 * value)
	{
		___parserInputStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___parserInputStack_1), value);
	}

	inline static int32_t get_offset_of_nameBuffer_2() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___nameBuffer_2)); }
	inline CharU5BU5D_t3419619864* get_nameBuffer_2() const { return ___nameBuffer_2; }
	inline CharU5BU5D_t3419619864** get_address_of_nameBuffer_2() { return &___nameBuffer_2; }
	inline void set_nameBuffer_2(CharU5BU5D_t3419619864* value)
	{
		___nameBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameBuffer_2), value);
	}

	inline static int32_t get_offset_of_nameLength_3() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___nameLength_3)); }
	inline int32_t get_nameLength_3() const { return ___nameLength_3; }
	inline int32_t* get_address_of_nameLength_3() { return &___nameLength_3; }
	inline void set_nameLength_3(int32_t value)
	{
		___nameLength_3 = value;
	}

	inline static int32_t get_offset_of_nameCapacity_4() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___nameCapacity_4)); }
	inline int32_t get_nameCapacity_4() const { return ___nameCapacity_4; }
	inline int32_t* get_address_of_nameCapacity_4() { return &___nameCapacity_4; }
	inline void set_nameCapacity_4(int32_t value)
	{
		___nameCapacity_4 = value;
	}

	inline static int32_t get_offset_of_valueBuffer_5() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___valueBuffer_5)); }
	inline StringBuilder_t622404039 * get_valueBuffer_5() const { return ___valueBuffer_5; }
	inline StringBuilder_t622404039 ** get_address_of_valueBuffer_5() { return &___valueBuffer_5; }
	inline void set_valueBuffer_5(StringBuilder_t622404039 * value)
	{
		___valueBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_5), value);
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_6() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___currentLinkedNodeLineNumber_6)); }
	inline int32_t get_currentLinkedNodeLineNumber_6() const { return ___currentLinkedNodeLineNumber_6; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_6() { return &___currentLinkedNodeLineNumber_6; }
	inline void set_currentLinkedNodeLineNumber_6(int32_t value)
	{
		___currentLinkedNodeLineNumber_6 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_7() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___currentLinkedNodeLinePosition_7)); }
	inline int32_t get_currentLinkedNodeLinePosition_7() const { return ___currentLinkedNodeLinePosition_7; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_7() { return &___currentLinkedNodeLinePosition_7; }
	inline void set_currentLinkedNodeLinePosition_7(int32_t value)
	{
		___currentLinkedNodeLinePosition_7 = value;
	}

	inline static int32_t get_offset_of_dtdIncludeSect_8() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___dtdIncludeSect_8)); }
	inline int32_t get_dtdIncludeSect_8() const { return ___dtdIncludeSect_8; }
	inline int32_t* get_address_of_dtdIncludeSect_8() { return &___dtdIncludeSect_8; }
	inline void set_dtdIncludeSect_8(int32_t value)
	{
		___dtdIncludeSect_8 = value;
	}

	inline static int32_t get_offset_of_normalization_9() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___normalization_9)); }
	inline bool get_normalization_9() const { return ___normalization_9; }
	inline bool* get_address_of_normalization_9() { return &___normalization_9; }
	inline void set_normalization_9(bool value)
	{
		___normalization_9 = value;
	}

	inline static int32_t get_offset_of_processingInternalSubset_10() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___processingInternalSubset_10)); }
	inline bool get_processingInternalSubset_10() const { return ___processingInternalSubset_10; }
	inline bool* get_address_of_processingInternalSubset_10() { return &___processingInternalSubset_10; }
	inline void set_processingInternalSubset_10(bool value)
	{
		___processingInternalSubset_10 = value;
	}

	inline static int32_t get_offset_of_cachedPublicId_11() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___cachedPublicId_11)); }
	inline String_t* get_cachedPublicId_11() const { return ___cachedPublicId_11; }
	inline String_t** get_address_of_cachedPublicId_11() { return &___cachedPublicId_11; }
	inline void set_cachedPublicId_11(String_t* value)
	{
		___cachedPublicId_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPublicId_11), value);
	}

	inline static int32_t get_offset_of_cachedSystemId_12() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___cachedSystemId_12)); }
	inline String_t* get_cachedSystemId_12() const { return ___cachedSystemId_12; }
	inline String_t** get_address_of_cachedSystemId_12() { return &___cachedSystemId_12; }
	inline void set_cachedSystemId_12(String_t* value)
	{
		___cachedSystemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedSystemId_12), value);
	}

	inline static int32_t get_offset_of_DTD_13() { return static_cast<int32_t>(offsetof(DTDReader_t1016753964, ___DTD_13)); }
	inline DTDObjectModel_t3150902482 * get_DTD_13() const { return ___DTD_13; }
	inline DTDObjectModel_t3150902482 ** get_address_of_DTD_13() { return &___DTD_13; }
	inline void set_DTD_13(DTDObjectModel_t3150902482 * value)
	{
		___DTD_13 = value;
		Il2CppCodeGenWriteBarrier((&___DTD_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDREADER_T1016753964_H
#ifndef XMLSCHEMACOMPILATIONSETTINGS_T1130471374_H
#define XMLSCHEMACOMPILATIONSETTINGS_T1130471374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCompilationSettings
struct  XmlSchemaCompilationSettings_t1130471374  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaCompilationSettings::enable_upa_check
	bool ___enable_upa_check_0;

public:
	inline static int32_t get_offset_of_enable_upa_check_0() { return static_cast<int32_t>(offsetof(XmlSchemaCompilationSettings_t1130471374, ___enable_upa_check_0)); }
	inline bool get_enable_upa_check_0() const { return ___enable_upa_check_0; }
	inline bool* get_address_of_enable_upa_check_0() { return &___enable_upa_check_0; }
	inline void set_enable_upa_check_0(bool value)
	{
		___enable_upa_check_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPILATIONSETTINGS_T1130471374_H
#ifndef DTDPARAMETERENTITYDECLARATIONCOLLECTION_T1551549928_H
#define DTDPARAMETERENTITYDECLARATIONCOLLECTION_T1551549928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct  DTDParameterEntityDeclarationCollection_t1551549928  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.DTDParameterEntityDeclarationCollection::peDecls
	Hashtable_t2354558714 * ___peDecls_0;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDParameterEntityDeclarationCollection::root
	DTDObjectModel_t3150902482 * ___root_1;

public:
	inline static int32_t get_offset_of_peDecls_0() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t1551549928, ___peDecls_0)); }
	inline Hashtable_t2354558714 * get_peDecls_0() const { return ___peDecls_0; }
	inline Hashtable_t2354558714 ** get_address_of_peDecls_0() { return &___peDecls_0; }
	inline void set_peDecls_0(Hashtable_t2354558714 * value)
	{
		___peDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t1551549928, ___root_1)); }
	inline DTDObjectModel_t3150902482 * get_root_1() const { return ___root_1; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(DTDObjectModel_t3150902482 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATIONCOLLECTION_T1551549928_H
#ifndef XMLCHAR_T2364868096_H
#define XMLCHAR_T2364868096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t2364868096  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t2364868096_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t3419619864* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t434619169* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t434619169* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_t1653015247* ___nameBitmap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlChar::<>f__switch$map47
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map47_4;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t2364868096_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t3419619864* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t3419619864** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t3419619864* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t2364868096_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t434619169* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t434619169** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t434619169* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t2364868096_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t434619169* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t434619169** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t434619169* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t2364868096_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_t1653015247* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_t1653015247** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_t1653015247* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map47_4() { return static_cast<int32_t>(offsetof(XmlChar_t2364868096_StaticFields, ___U3CU3Ef__switchU24map47_4)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map47_4() const { return ___U3CU3Ef__switchU24map47_4; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map47_4() { return &___U3CU3Ef__switchU24map47_4; }
	inline void set_U3CU3Ef__switchU24map47_4(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map47_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map47_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T2364868096_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef DTDAUTOMATAFACTORY_T4266884469_H
#define DTDAUTOMATAFACTORY_T4266884469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomataFactory
struct  DTDAutomataFactory_t4266884469  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomataFactory::root
	DTDObjectModel_t3150902482 * ___root_0;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::choiceTable
	Hashtable_t2354558714 * ___choiceTable_1;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::sequenceTable
	Hashtable_t2354558714 * ___sequenceTable_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t4266884469, ___root_0)); }
	inline DTDObjectModel_t3150902482 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t3150902482 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_choiceTable_1() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t4266884469, ___choiceTable_1)); }
	inline Hashtable_t2354558714 * get_choiceTable_1() const { return ___choiceTable_1; }
	inline Hashtable_t2354558714 ** get_address_of_choiceTable_1() { return &___choiceTable_1; }
	inline void set_choiceTable_1(Hashtable_t2354558714 * value)
	{
		___choiceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceTable_1), value);
	}

	inline static int32_t get_offset_of_sequenceTable_2() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t4266884469, ___sequenceTable_2)); }
	inline Hashtable_t2354558714 * get_sequenceTable_2() const { return ___sequenceTable_2; }
	inline Hashtable_t2354558714 ** get_address_of_sequenceTable_2() { return &___sequenceTable_2; }
	inline void set_sequenceTable_2(Hashtable_t2354558714 * value)
	{
		___sequenceTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATAFACTORY_T4266884469_H
#ifndef DTDOBJECTMODEL_T3150902482_H
#define DTDOBJECTMODEL_T3150902482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDObjectModel
struct  DTDObjectModel_t3150902482  : public RuntimeObject
{
public:
	// Mono.Xml.DTDAutomataFactory Mono.Xml.DTDObjectModel::factory
	DTDAutomataFactory_t4266884469 * ___factory_0;
	// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::elementDecls
	DTDElementDeclarationCollection_t3634927218 * ___elementDecls_1;
	// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::attListDecls
	DTDAttListDeclarationCollection_t748347165 * ___attListDecls_2;
	// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::peDecls
	DTDParameterEntityDeclarationCollection_t1551549928 * ___peDecls_3;
	// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::entityDecls
	DTDEntityDeclarationCollection_t2395926230 * ___entityDecls_4;
	// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::notationDecls
	DTDNotationDeclarationCollection_t2979384550 * ___notationDecls_5;
	// System.Collections.ArrayList Mono.Xml.DTDObjectModel::validationErrors
	ArrayList_t4250946984 * ___validationErrors_6;
	// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::resolver
	XmlResolver_t161349657 * ___resolver_7;
	// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::nameTable
	XmlNameTable_t347431366 * ___nameTable_8;
	// System.Collections.Hashtable Mono.Xml.DTDObjectModel::externalResources
	Hashtable_t2354558714 * ___externalResources_9;
	// System.String Mono.Xml.DTDObjectModel::baseURI
	String_t* ___baseURI_10;
	// System.String Mono.Xml.DTDObjectModel::name
	String_t* ___name_11;
	// System.String Mono.Xml.DTDObjectModel::publicId
	String_t* ___publicId_12;
	// System.String Mono.Xml.DTDObjectModel::systemId
	String_t* ___systemId_13;
	// System.String Mono.Xml.DTDObjectModel::intSubset
	String_t* ___intSubset_14;
	// System.Boolean Mono.Xml.DTDObjectModel::intSubsetHasPERef
	bool ___intSubsetHasPERef_15;
	// System.Boolean Mono.Xml.DTDObjectModel::isStandalone
	bool ___isStandalone_16;
	// System.Int32 Mono.Xml.DTDObjectModel::lineNumber
	int32_t ___lineNumber_17;
	// System.Int32 Mono.Xml.DTDObjectModel::linePosition
	int32_t ___linePosition_18;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___factory_0)); }
	inline DTDAutomataFactory_t4266884469 * get_factory_0() const { return ___factory_0; }
	inline DTDAutomataFactory_t4266884469 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(DTDAutomataFactory_t4266884469 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___factory_0), value);
	}

	inline static int32_t get_offset_of_elementDecls_1() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___elementDecls_1)); }
	inline DTDElementDeclarationCollection_t3634927218 * get_elementDecls_1() const { return ___elementDecls_1; }
	inline DTDElementDeclarationCollection_t3634927218 ** get_address_of_elementDecls_1() { return &___elementDecls_1; }
	inline void set_elementDecls_1(DTDElementDeclarationCollection_t3634927218 * value)
	{
		___elementDecls_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_1), value);
	}

	inline static int32_t get_offset_of_attListDecls_2() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___attListDecls_2)); }
	inline DTDAttListDeclarationCollection_t748347165 * get_attListDecls_2() const { return ___attListDecls_2; }
	inline DTDAttListDeclarationCollection_t748347165 ** get_address_of_attListDecls_2() { return &___attListDecls_2; }
	inline void set_attListDecls_2(DTDAttListDeclarationCollection_t748347165 * value)
	{
		___attListDecls_2 = value;
		Il2CppCodeGenWriteBarrier((&___attListDecls_2), value);
	}

	inline static int32_t get_offset_of_peDecls_3() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___peDecls_3)); }
	inline DTDParameterEntityDeclarationCollection_t1551549928 * get_peDecls_3() const { return ___peDecls_3; }
	inline DTDParameterEntityDeclarationCollection_t1551549928 ** get_address_of_peDecls_3() { return &___peDecls_3; }
	inline void set_peDecls_3(DTDParameterEntityDeclarationCollection_t1551549928 * value)
	{
		___peDecls_3 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_3), value);
	}

	inline static int32_t get_offset_of_entityDecls_4() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___entityDecls_4)); }
	inline DTDEntityDeclarationCollection_t2395926230 * get_entityDecls_4() const { return ___entityDecls_4; }
	inline DTDEntityDeclarationCollection_t2395926230 ** get_address_of_entityDecls_4() { return &___entityDecls_4; }
	inline void set_entityDecls_4(DTDEntityDeclarationCollection_t2395926230 * value)
	{
		___entityDecls_4 = value;
		Il2CppCodeGenWriteBarrier((&___entityDecls_4), value);
	}

	inline static int32_t get_offset_of_notationDecls_5() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___notationDecls_5)); }
	inline DTDNotationDeclarationCollection_t2979384550 * get_notationDecls_5() const { return ___notationDecls_5; }
	inline DTDNotationDeclarationCollection_t2979384550 ** get_address_of_notationDecls_5() { return &___notationDecls_5; }
	inline void set_notationDecls_5(DTDNotationDeclarationCollection_t2979384550 * value)
	{
		___notationDecls_5 = value;
		Il2CppCodeGenWriteBarrier((&___notationDecls_5), value);
	}

	inline static int32_t get_offset_of_validationErrors_6() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___validationErrors_6)); }
	inline ArrayList_t4250946984 * get_validationErrors_6() const { return ___validationErrors_6; }
	inline ArrayList_t4250946984 ** get_address_of_validationErrors_6() { return &___validationErrors_6; }
	inline void set_validationErrors_6(ArrayList_t4250946984 * value)
	{
		___validationErrors_6 = value;
		Il2CppCodeGenWriteBarrier((&___validationErrors_6), value);
	}

	inline static int32_t get_offset_of_resolver_7() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___resolver_7)); }
	inline XmlResolver_t161349657 * get_resolver_7() const { return ___resolver_7; }
	inline XmlResolver_t161349657 ** get_address_of_resolver_7() { return &___resolver_7; }
	inline void set_resolver_7(XmlResolver_t161349657 * value)
	{
		___resolver_7 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_7), value);
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___nameTable_8)); }
	inline XmlNameTable_t347431366 * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_t347431366 * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_externalResources_9() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___externalResources_9)); }
	inline Hashtable_t2354558714 * get_externalResources_9() const { return ___externalResources_9; }
	inline Hashtable_t2354558714 ** get_address_of_externalResources_9() { return &___externalResources_9; }
	inline void set_externalResources_9(Hashtable_t2354558714 * value)
	{
		___externalResources_9 = value;
		Il2CppCodeGenWriteBarrier((&___externalResources_9), value);
	}

	inline static int32_t get_offset_of_baseURI_10() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___baseURI_10)); }
	inline String_t* get_baseURI_10() const { return ___baseURI_10; }
	inline String_t** get_address_of_baseURI_10() { return &___baseURI_10; }
	inline void set_baseURI_10(String_t* value)
	{
		___baseURI_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_10), value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier((&___name_11), value);
	}

	inline static int32_t get_offset_of_publicId_12() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___publicId_12)); }
	inline String_t* get_publicId_12() const { return ___publicId_12; }
	inline String_t** get_address_of_publicId_12() { return &___publicId_12; }
	inline void set_publicId_12(String_t* value)
	{
		___publicId_12 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_12), value);
	}

	inline static int32_t get_offset_of_systemId_13() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___systemId_13)); }
	inline String_t* get_systemId_13() const { return ___systemId_13; }
	inline String_t** get_address_of_systemId_13() { return &___systemId_13; }
	inline void set_systemId_13(String_t* value)
	{
		___systemId_13 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_13), value);
	}

	inline static int32_t get_offset_of_intSubset_14() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___intSubset_14)); }
	inline String_t* get_intSubset_14() const { return ___intSubset_14; }
	inline String_t** get_address_of_intSubset_14() { return &___intSubset_14; }
	inline void set_intSubset_14(String_t* value)
	{
		___intSubset_14 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_14), value);
	}

	inline static int32_t get_offset_of_intSubsetHasPERef_15() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___intSubsetHasPERef_15)); }
	inline bool get_intSubsetHasPERef_15() const { return ___intSubsetHasPERef_15; }
	inline bool* get_address_of_intSubsetHasPERef_15() { return &___intSubsetHasPERef_15; }
	inline void set_intSubsetHasPERef_15(bool value)
	{
		___intSubsetHasPERef_15 = value;
	}

	inline static int32_t get_offset_of_isStandalone_16() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___isStandalone_16)); }
	inline bool get_isStandalone_16() const { return ___isStandalone_16; }
	inline bool* get_address_of_isStandalone_16() { return &___isStandalone_16; }
	inline void set_isStandalone_16(bool value)
	{
		___isStandalone_16 = value;
	}

	inline static int32_t get_offset_of_lineNumber_17() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___lineNumber_17)); }
	inline int32_t get_lineNumber_17() const { return ___lineNumber_17; }
	inline int32_t* get_address_of_lineNumber_17() { return &___lineNumber_17; }
	inline void set_lineNumber_17(int32_t value)
	{
		___lineNumber_17 = value;
	}

	inline static int32_t get_offset_of_linePosition_18() { return static_cast<int32_t>(offsetof(DTDObjectModel_t3150902482, ___linePosition_18)); }
	inline int32_t get_linePosition_18() const { return ___linePosition_18; }
	inline int32_t* get_address_of_linePosition_18() { return &___linePosition_18; }
	inline void set_linePosition_18(int32_t value)
	{
		___linePosition_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOBJECTMODEL_T3150902482_H
#ifndef DTDCONTENTMODELCOLLECTION_T133429609_H
#define DTDCONTENTMODELCOLLECTION_T133429609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t133429609  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t4250946984 * ___contentModel_0;

public:
	inline static int32_t get_offset_of_contentModel_0() { return static_cast<int32_t>(offsetof(DTDContentModelCollection_t133429609, ___contentModel_0)); }
	inline ArrayList_t4250946984 * get_contentModel_0() const { return ___contentModel_0; }
	inline ArrayList_t4250946984 ** get_address_of_contentModel_0() { return &___contentModel_0; }
	inline void set_contentModel_0(ArrayList_t4250946984 * value)
	{
		___contentModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODELCOLLECTION_T133429609_H
#ifndef XMLSERIALIZERNAMESPACES_T2934028315_H
#define XMLSERIALIZERNAMESPACES_T2934028315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t2934028315  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_t864114708 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t2934028315, ___namespaces_0)); }
	inline ListDictionary_t864114708 * get_namespaces_0() const { return ___namespaces_0; }
	inline ListDictionary_t864114708 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(ListDictionary_t864114708 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T2934028315_H
#ifndef XMLNAMETABLE_T347431366_H
#define XMLNAMETABLE_T347431366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t347431366  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T347431366_H
#ifndef XMLNAMEDNODEMAP_T4038918308_H
#define XMLNAMEDNODEMAP_T4038918308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t4038918308  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t3729859639 * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t4250946984 * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___parent_1)); }
	inline XmlNode_t3729859639 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t3729859639 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t3729859639 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___nodeList_2)); }
	inline ArrayList_t4250946984 * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t4250946984 ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t4250946984 * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_t4038918308_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t4038918308_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T4038918308_H
#ifndef XMLNODE_T3729859639_H
#define XMLNODE_T3729859639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3729859639  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t2823332853 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3729859639 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t4201287521 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___ownerDocument_1)); }
	inline XmlDocument_t2823332853 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t2823332853 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t2823332853 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___parentNode_2)); }
	inline XmlNode_t3729859639 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t3729859639 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t3729859639 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639, ___childNodes_3)); }
	inline XmlNodeListChildren_t4201287521 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t4201287521 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t4201287521 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t3729859639_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t596510042 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map44_4;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t596510042 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t596510042 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t596510042 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t3729859639_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3729859639_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef ENTRY_T2245712786_H
#define ENTRY_T2245712786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_t2245712786  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hash
	int32_t ___hash_1;
	// System.Int32 System.Xml.NameTable/Entry::len
	int32_t ___len_2;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_t2245712786 * ___next_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_t2245712786, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(Entry_t2245712786, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Entry_t2245712786, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(Entry_t2245712786, ___next_3)); }
	inline Entry_t2245712786 * get_next_3() const { return ___next_3; }
	inline Entry_t2245712786 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(Entry_t2245712786 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2245712786_H
#ifndef DTDNODE_T3993235537_H
#define DTDNODE_T3993235537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNode
struct  DTDNode_t3993235537  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::root
	DTDObjectModel_t3150902482 * ___root_0;
	// System.Boolean Mono.Xml.DTDNode::isInternalSubset
	bool ___isInternalSubset_1;
	// System.String Mono.Xml.DTDNode::baseURI
	String_t* ___baseURI_2;
	// System.Int32 Mono.Xml.DTDNode::lineNumber
	int32_t ___lineNumber_3;
	// System.Int32 Mono.Xml.DTDNode::linePosition
	int32_t ___linePosition_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDNode_t3993235537, ___root_0)); }
	inline DTDObjectModel_t3150902482 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t3150902482 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_isInternalSubset_1() { return static_cast<int32_t>(offsetof(DTDNode_t3993235537, ___isInternalSubset_1)); }
	inline bool get_isInternalSubset_1() const { return ___isInternalSubset_1; }
	inline bool* get_address_of_isInternalSubset_1() { return &___isInternalSubset_1; }
	inline void set_isInternalSubset_1(bool value)
	{
		___isInternalSubset_1 = value;
	}

	inline static int32_t get_offset_of_baseURI_2() { return static_cast<int32_t>(offsetof(DTDNode_t3993235537, ___baseURI_2)); }
	inline String_t* get_baseURI_2() const { return ___baseURI_2; }
	inline String_t** get_address_of_baseURI_2() { return &___baseURI_2; }
	inline void set_baseURI_2(String_t* value)
	{
		___baseURI_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_2), value);
	}

	inline static int32_t get_offset_of_lineNumber_3() { return static_cast<int32_t>(offsetof(DTDNode_t3993235537, ___lineNumber_3)); }
	inline int32_t get_lineNumber_3() const { return ___lineNumber_3; }
	inline int32_t* get_address_of_lineNumber_3() { return &___lineNumber_3; }
	inline void set_lineNumber_3(int32_t value)
	{
		___lineNumber_3 = value;
	}

	inline static int32_t get_offset_of_linePosition_4() { return static_cast<int32_t>(offsetof(DTDNode_t3993235537, ___linePosition_4)); }
	inline int32_t get_linePosition_4() const { return ___linePosition_4; }
	inline int32_t* get_address_of_linePosition_4() { return &___linePosition_4; }
	inline void set_linePosition_4(int32_t value)
	{
		___linePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNODE_T3993235537_H
#ifndef LIST_1_T1462362804_H
#define LIST_1_T1462362804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  List_1_t1462362804  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t1877457368* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1462362804, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t1877457368* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t1877457368** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t1877457368* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1462362804, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1462362804, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1462362804_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t1877457368* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1462362804_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t1877457368* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t1877457368** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t1877457368* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1462362804_H
#ifndef XMLIGNOREATTRIBUTE_T1537762200_H
#define XMLIGNOREATTRIBUTE_T1537762200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t1537762200  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T1537762200_H
#ifndef XMLENUMATTRIBUTE_T1636689972_H
#define XMLENUMATTRIBUTE_T1636689972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t1636689972  : public Attribute_t1924466020
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t1636689972, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T1636689972_H
#ifndef XMLELEMENTATTRIBUTE_T1186985268_H
#define XMLELEMENTATTRIBUTE_T1186985268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t1186985268  : public Attribute_t1924466020
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1186985268, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1186985268, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1186985268, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T1186985268_H
#ifndef NAMETABLE_T3507653384_H
#define NAMETABLE_T3507653384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t3507653384  : public XmlNameTable_t347431366
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t1707096551* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t3507653384, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t3507653384, ___buckets_1)); }
	inline EntryU5BU5D_t1707096551* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t1707096551** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t1707096551* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t3507653384, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T3507653384_H
#ifndef XMLATTRIBUTEATTRIBUTE_T3964240084_H
#define XMLATTRIBUTEATTRIBUTE_T3964240084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_t3964240084  : public Attribute_t1924466020
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t3964240084, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_T3964240084_H
#ifndef DTDNOTATIONDECLARATION_T1773003655_H
#define DTDNOTATIONDECLARATION_T1773003655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t1773003655  : public DTDNode_t3993235537
{
public:
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1773003655, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1773003655, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1773003655, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1773003655, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1773003655, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATION_T1773003655_H
#ifndef DTDATTLISTDECLARATION_T2301331120_H
#define DTDATTLISTDECLARATION_T2301331120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t2301331120  : public DTDNode_t3993235537
{
public:
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_t2354558714 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t4250946984 * ___attributes_7;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2301331120, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_attributeOrders_6() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2301331120, ___attributeOrders_6)); }
	inline Hashtable_t2354558714 * get_attributeOrders_6() const { return ___attributeOrders_6; }
	inline Hashtable_t2354558714 ** get_address_of_attributeOrders_6() { return &___attributeOrders_6; }
	inline void set_attributeOrders_6(Hashtable_t2354558714 * value)
	{
		___attributeOrders_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOrders_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2301331120, ___attributes_7)); }
	inline ArrayList_t4250946984 * get_attributes_7() const { return ___attributes_7; }
	inline ArrayList_t4250946984 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(ArrayList_t4250946984 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATION_T2301331120_H
#ifndef DTDATTRIBUTEDEFINITION_T1581906730_H
#define DTDATTRIBUTEDEFINITION_T1581906730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeDefinition
struct  DTDAttributeDefinition_t1581906730  : public DTDNode_t3993235537
{
public:
	// System.String Mono.Xml.DTDAttributeDefinition::name
	String_t* ___name_5;
	// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::datatype
	XmlSchemaDatatype_t4041842875 * ___datatype_6;
	// System.String Mono.Xml.DTDAttributeDefinition::unresolvedDefault
	String_t* ___unresolvedDefault_7;
	// System.String Mono.Xml.DTDAttributeDefinition::resolvedDefaultValue
	String_t* ___resolvedDefaultValue_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t1581906730, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_datatype_6() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t1581906730, ___datatype_6)); }
	inline XmlSchemaDatatype_t4041842875 * get_datatype_6() const { return ___datatype_6; }
	inline XmlSchemaDatatype_t4041842875 ** get_address_of_datatype_6() { return &___datatype_6; }
	inline void set_datatype_6(XmlSchemaDatatype_t4041842875 * value)
	{
		___datatype_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_6), value);
	}

	inline static int32_t get_offset_of_unresolvedDefault_7() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t1581906730, ___unresolvedDefault_7)); }
	inline String_t* get_unresolvedDefault_7() const { return ___unresolvedDefault_7; }
	inline String_t** get_address_of_unresolvedDefault_7() { return &___unresolvedDefault_7; }
	inline void set_unresolvedDefault_7(String_t* value)
	{
		___unresolvedDefault_7 = value;
		Il2CppCodeGenWriteBarrier((&___unresolvedDefault_7), value);
	}

	inline static int32_t get_offset_of_resolvedDefaultValue_8() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t1581906730, ___resolvedDefaultValue_8)); }
	inline String_t* get_resolvedDefaultValue_8() const { return ___resolvedDefaultValue_8; }
	inline String_t** get_address_of_resolvedDefaultValue_8() { return &___resolvedDefaultValue_8; }
	inline void set_resolvedDefaultValue_8(String_t* value)
	{
		___resolvedDefaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___resolvedDefaultValue_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEDEFINITION_T1581906730_H
#ifndef DTDELEMENTDECLARATION_T3064560604_H
#define DTDELEMENTDECLARATION_T3064560604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclaration
struct  DTDElementDeclaration_t3064560604  : public DTDNode_t3993235537
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDElementDeclaration::root
	DTDObjectModel_t3150902482 * ___root_5;
	// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::contentModel
	DTDContentModel_t2841412075 * ___contentModel_6;
	// System.String Mono.Xml.DTDElementDeclaration::name
	String_t* ___name_7;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isEmpty
	bool ___isEmpty_8;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isAny
	bool ___isAny_9;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isMixedContent
	bool ___isMixedContent_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___root_5)); }
	inline DTDObjectModel_t3150902482 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t3150902482 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_contentModel_6() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___contentModel_6)); }
	inline DTDContentModel_t2841412075 * get_contentModel_6() const { return ___contentModel_6; }
	inline DTDContentModel_t2841412075 ** get_address_of_contentModel_6() { return &___contentModel_6; }
	inline void set_contentModel_6(DTDContentModel_t2841412075 * value)
	{
		___contentModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isEmpty_8() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___isEmpty_8)); }
	inline bool get_isEmpty_8() const { return ___isEmpty_8; }
	inline bool* get_address_of_isEmpty_8() { return &___isEmpty_8; }
	inline void set_isEmpty_8(bool value)
	{
		___isEmpty_8 = value;
	}

	inline static int32_t get_offset_of_isAny_9() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___isAny_9)); }
	inline bool get_isAny_9() const { return ___isAny_9; }
	inline bool* get_address_of_isAny_9() { return &___isAny_9; }
	inline void set_isAny_9(bool value)
	{
		___isAny_9 = value;
	}

	inline static int32_t get_offset_of_isMixedContent_10() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t3064560604, ___isMixedContent_10)); }
	inline bool get_isMixedContent_10() const { return ___isMixedContent_10; }
	inline bool* get_address_of_isMixedContent_10() { return &___isMixedContent_10; }
	inline void set_isMixedContent_10(bool value)
	{
		___isMixedContent_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATION_T3064560604_H
#ifndef DICTIONARYBASE_T4072949440_H
#define DICTIONARYBASE_T4072949440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase
struct  DictionaryBase_t4072949440  : public List_1_t1462362804
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBASE_T4072949440_H
#ifndef DTDENTITYBASE_T3196470699_H
#define DTDENTITYBASE_T3196470699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityBase
struct  DTDEntityBase_t3196470699  : public DTDNode_t3993235537
{
public:
	// System.String Mono.Xml.DTDEntityBase::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDEntityBase::publicId
	String_t* ___publicId_6;
	// System.String Mono.Xml.DTDEntityBase::systemId
	String_t* ___systemId_7;
	// System.String Mono.Xml.DTDEntityBase::literalValue
	String_t* ___literalValue_8;
	// System.String Mono.Xml.DTDEntityBase::replacementText
	String_t* ___replacementText_9;
	// System.String Mono.Xml.DTDEntityBase::uriString
	String_t* ___uriString_10;
	// System.Uri Mono.Xml.DTDEntityBase::absUri
	Uri_t3269222095 * ___absUri_11;
	// System.Boolean Mono.Xml.DTDEntityBase::isInvalid
	bool ___isInvalid_12;
	// System.Boolean Mono.Xml.DTDEntityBase::loadFailed
	bool ___loadFailed_13;
	// System.Xml.XmlResolver Mono.Xml.DTDEntityBase::resolver
	XmlResolver_t161349657 * ___resolver_14;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_literalValue_8() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___literalValue_8)); }
	inline String_t* get_literalValue_8() const { return ___literalValue_8; }
	inline String_t** get_address_of_literalValue_8() { return &___literalValue_8; }
	inline void set_literalValue_8(String_t* value)
	{
		___literalValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___literalValue_8), value);
	}

	inline static int32_t get_offset_of_replacementText_9() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___replacementText_9)); }
	inline String_t* get_replacementText_9() const { return ___replacementText_9; }
	inline String_t** get_address_of_replacementText_9() { return &___replacementText_9; }
	inline void set_replacementText_9(String_t* value)
	{
		___replacementText_9 = value;
		Il2CppCodeGenWriteBarrier((&___replacementText_9), value);
	}

	inline static int32_t get_offset_of_uriString_10() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___uriString_10)); }
	inline String_t* get_uriString_10() const { return ___uriString_10; }
	inline String_t** get_address_of_uriString_10() { return &___uriString_10; }
	inline void set_uriString_10(String_t* value)
	{
		___uriString_10 = value;
		Il2CppCodeGenWriteBarrier((&___uriString_10), value);
	}

	inline static int32_t get_offset_of_absUri_11() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___absUri_11)); }
	inline Uri_t3269222095 * get_absUri_11() const { return ___absUri_11; }
	inline Uri_t3269222095 ** get_address_of_absUri_11() { return &___absUri_11; }
	inline void set_absUri_11(Uri_t3269222095 * value)
	{
		___absUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___absUri_11), value);
	}

	inline static int32_t get_offset_of_isInvalid_12() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___isInvalid_12)); }
	inline bool get_isInvalid_12() const { return ___isInvalid_12; }
	inline bool* get_address_of_isInvalid_12() { return &___isInvalid_12; }
	inline void set_isInvalid_12(bool value)
	{
		___isInvalid_12 = value;
	}

	inline static int32_t get_offset_of_loadFailed_13() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___loadFailed_13)); }
	inline bool get_loadFailed_13() const { return ___loadFailed_13; }
	inline bool* get_address_of_loadFailed_13() { return &___loadFailed_13; }
	inline void set_loadFailed_13(bool value)
	{
		___loadFailed_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(DTDEntityBase_t3196470699, ___resolver_14)); }
	inline XmlResolver_t161349657 * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_t161349657 ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_t161349657 * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYBASE_T3196470699_H
#ifndef XMLLINKEDNODE_T1947670907_H
#define XMLLINKEDNODE_T1947670907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1947670907  : public XmlNode_t3729859639
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1947670907 * ___nextSibling_5;

public:
	inline static int32_t get_offset_of_nextSibling_5() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1947670907, ___nextSibling_5)); }
	inline XmlLinkedNode_t1947670907 * get_nextSibling_5() const { return ___nextSibling_5; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_nextSibling_5() { return &___nextSibling_5; }
	inline void set_nextSibling_5(XmlLinkedNode_t1947670907 * value)
	{
		___nextSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1947670907_H
#ifndef KEYVALUEPAIR_2_T2342735797_H
#define KEYVALUEPAIR_2_T2342735797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t2342735797 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t3993235537 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2342735797, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2342735797, ___value_1)); }
	inline DTDNode_t3993235537 * get_value_1() const { return ___value_1; }
	inline DTDNode_t3993235537 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(DTDNode_t3993235537 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2342735797_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t4215975066 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t4215975066 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t4215975066 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t4215975066 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef XMLATTRIBUTECOLLECTION_T3528594990_H
#define XMLATTRIBUTECOLLECTION_T3528594990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t3528594990  : public XmlNamedNodeMap_t4038918308
{
public:
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t687149941 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t2823332853 * ___ownerDocument_5;

public:
	inline static int32_t get_offset_of_ownerElement_4() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t3528594990, ___ownerElement_4)); }
	inline XmlElement_t687149941 * get_ownerElement_4() const { return ___ownerElement_4; }
	inline XmlElement_t687149941 ** get_address_of_ownerElement_4() { return &___ownerElement_4; }
	inline void set_ownerElement_4(XmlElement_t687149941 * value)
	{
		___ownerElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElement_4), value);
	}

	inline static int32_t get_offset_of_ownerDocument_5() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t3528594990, ___ownerDocument_5)); }
	inline XmlDocument_t2823332853 * get_ownerDocument_5() const { return ___ownerDocument_5; }
	inline XmlDocument_t2823332853 ** get_address_of_ownerDocument_5() { return &___ownerDocument_5; }
	inline void set_ownerDocument_5(XmlDocument_t2823332853 * value)
	{
		___ownerDocument_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T3528594990_H
#ifndef XMLATTRIBUTE_T3667600658_H
#define XMLATTRIBUTE_T3667600658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t3667600658  : public XmlNode_t3729859639
{
public:
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t1006717423 * ___name_5;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t1947670907 * ___lastLinkedChild_7;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	RuntimeObject* ___schemaInfo_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlAttribute_t3667600658, ___name_5)); }
	inline XmlNameEntry_t1006717423 * get_name_5() const { return ___name_5; }
	inline XmlNameEntry_t1006717423 ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XmlNameEntry_t1006717423 * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_isDefault_6() { return static_cast<int32_t>(offsetof(XmlAttribute_t3667600658, ___isDefault_6)); }
	inline bool get_isDefault_6() const { return ___isDefault_6; }
	inline bool* get_address_of_isDefault_6() { return &___isDefault_6; }
	inline void set_isDefault_6(bool value)
	{
		___isDefault_6 = value;
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlAttribute_t3667600658, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1947670907 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1947670907 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1947670907 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}

	inline static int32_t get_offset_of_schemaInfo_8() { return static_cast<int32_t>(offsetof(XmlAttribute_t3667600658, ___schemaInfo_8)); }
	inline RuntimeObject* get_schemaInfo_8() const { return ___schemaInfo_8; }
	inline RuntimeObject** get_address_of_schemaInfo_8() { return &___schemaInfo_8; }
	inline void set_schemaInfo_8(RuntimeObject* value)
	{
		___schemaInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T3667600658_H
#ifndef NEWLINEHANDLING_T3095395276_H
#define NEWLINEHANDLING_T3095395276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t3095395276 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t3095395276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T3095395276_H
#ifndef ENUMERATOR_T1238231328_H
#define ENUMERATOR_T1238231328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  Enumerator_t1238231328 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1462362804 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t2342735797  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1238231328, ___l_0)); }
	inline List_1_t1462362804 * get_l_0() const { return ___l_0; }
	inline List_1_t1462362804 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1462362804 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1238231328, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1238231328, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1238231328, ___current_3)); }
	inline KeyValuePair_2_t2342735797  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2342735797 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2342735797  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1238231328_H
#ifndef NUMBERSTYLES_T2594088858_H
#define NUMBERSTYLES_T2594088858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t2594088858 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NumberStyles_t2594088858, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T2594088858_H
#ifndef DTDCOLLECTIONBASE_T403511423_H
#define DTDCOLLECTIONBASE_T403511423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_t403511423  : public DictionaryBase_t4072949440
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t3150902482 * ___root_5;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDCollectionBase_t403511423, ___root_5)); }
	inline DTDObjectModel_t3150902482 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t3150902482 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCOLLECTIONBASE_T403511423_H
#ifndef XSDWHITESPACEFACET_T1825470350_H
#define XSDWHITESPACEFACET_T1825470350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_t1825470350 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_t1825470350, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_T1825470350_H
#ifndef CONFORMANCELEVEL_T2038759208_H
#define CONFORMANCELEVEL_T2038759208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t2038759208 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t2038759208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T2038759208_H
#ifndef XMLCHARACTERDATA_T4217070933_H
#define XMLCHARACTERDATA_T4217070933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t4217070933  : public XmlLinkedNode_t1947670907
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(XmlCharacterData_t4217070933, ___data_6)); }
	inline String_t* get_data_6() const { return ___data_6; }
	inline String_t** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(String_t* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T4217070933_H
#ifndef XMLSCHEMAVALIDITY_T2542592596_H
#define XMLSCHEMAVALIDITY_T2542592596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_t2542592596 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_t2542592596, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_T2542592596_H
#ifndef NAMESPACEHANDLING_T2739939700_H
#define NAMESPACEHANDLING_T2739939700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t2739939700 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t2739939700, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T2739939700_H
#ifndef DTDENTITYDECLARATION_T2202458961_H
#define DTDENTITYDECLARATION_T2202458961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t2202458961  : public DTDEntityBase_t3196470699
{
public:
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t4250946984 * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;

public:
	inline static int32_t get_offset_of_entityValue_15() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___entityValue_15)); }
	inline String_t* get_entityValue_15() const { return ___entityValue_15; }
	inline String_t** get_address_of_entityValue_15() { return &___entityValue_15; }
	inline void set_entityValue_15(String_t* value)
	{
		___entityValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___entityValue_15), value);
	}

	inline static int32_t get_offset_of_notationName_16() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___notationName_16)); }
	inline String_t* get_notationName_16() const { return ___notationName_16; }
	inline String_t** get_address_of_notationName_16() { return &___notationName_16; }
	inline void set_notationName_16(String_t* value)
	{
		___notationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_16), value);
	}

	inline static int32_t get_offset_of_ReferencingEntities_17() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___ReferencingEntities_17)); }
	inline ArrayList_t4250946984 * get_ReferencingEntities_17() const { return ___ReferencingEntities_17; }
	inline ArrayList_t4250946984 ** get_address_of_ReferencingEntities_17() { return &___ReferencingEntities_17; }
	inline void set_ReferencingEntities_17(ArrayList_t4250946984 * value)
	{
		___ReferencingEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencingEntities_17), value);
	}

	inline static int32_t get_offset_of_scanned_18() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___scanned_18)); }
	inline bool get_scanned_18() const { return ___scanned_18; }
	inline bool* get_address_of_scanned_18() { return &___scanned_18; }
	inline void set_scanned_18(bool value)
	{
		___scanned_18 = value;
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_hasExternalReference_20() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t2202458961, ___hasExternalReference_20)); }
	inline bool get_hasExternalReference_20() const { return ___hasExternalReference_20; }
	inline bool* get_address_of_hasExternalReference_20() { return &___hasExternalReference_20; }
	inline void set_hasExternalReference_20(bool value)
	{
		___hasExternalReference_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATION_T2202458961_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T1876323326_H
#define XMLSCHEMADERIVATIONMETHOD_T1876323326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t1876323326 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t1876323326, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T1876323326_H
#ifndef XMLSCHEMASET_T93272820_H
#define XMLSCHEMASET_T93272820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSet
struct  XmlSchemaSet_t93272820  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaSet::nameTable
	XmlNameTable_t347431366 * ___nameTable_0;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaSet::xmlResolver
	XmlResolver_t161349657 * ___xmlResolver_1;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaSet::schemas
	ArrayList_t4250946984 * ___schemas_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.XmlSchemaSet::settings
	XmlSchemaCompilationSettings_t1130471374 * ___settings_3;
	// System.Guid System.Xml.Schema.XmlSchemaSet::CompilationId
	Guid_t  ___CompilationId_4;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t93272820, ___nameTable_0)); }
	inline XmlNameTable_t347431366 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t347431366 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t347431366 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_xmlResolver_1() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t93272820, ___xmlResolver_1)); }
	inline XmlResolver_t161349657 * get_xmlResolver_1() const { return ___xmlResolver_1; }
	inline XmlResolver_t161349657 ** get_address_of_xmlResolver_1() { return &___xmlResolver_1; }
	inline void set_xmlResolver_1(XmlResolver_t161349657 * value)
	{
		___xmlResolver_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_1), value);
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t93272820, ___schemas_2)); }
	inline ArrayList_t4250946984 * get_schemas_2() const { return ___schemas_2; }
	inline ArrayList_t4250946984 ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(ArrayList_t4250946984 * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t93272820, ___settings_3)); }
	inline XmlSchemaCompilationSettings_t1130471374 * get_settings_3() const { return ___settings_3; }
	inline XmlSchemaCompilationSettings_t1130471374 ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(XmlSchemaCompilationSettings_t1130471374 * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___settings_3), value);
	}

	inline static int32_t get_offset_of_CompilationId_4() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t93272820, ___CompilationId_4)); }
	inline Guid_t  get_CompilationId_4() const { return ___CompilationId_4; }
	inline Guid_t * get_address_of_CompilationId_4() { return &___CompilationId_4; }
	inline void set_CompilationId_4(Guid_t  value)
	{
		___CompilationId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASET_T93272820_H
#ifndef ENTITYHANDLING_T3541905618_H
#define ENTITYHANDLING_T3541905618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t3541905618 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t3541905618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T3541905618_H
#ifndef XMLSCHEMAOBJECT_T4233170947_H
#define XMLSCHEMAOBJECT_T4233170947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_t4233170947  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t2934028315 * ___namespaces_0;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t4250946984 * ___unhandledAttributeList_1;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t  ___CompilationId_2;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t4233170947, ___namespaces_0)); }
	inline XmlSerializerNamespaces_t2934028315 * get_namespaces_0() const { return ___namespaces_0; }
	inline XmlSerializerNamespaces_t2934028315 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(XmlSerializerNamespaces_t2934028315 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}

	inline static int32_t get_offset_of_unhandledAttributeList_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t4233170947, ___unhandledAttributeList_1)); }
	inline ArrayList_t4250946984 * get_unhandledAttributeList_1() const { return ___unhandledAttributeList_1; }
	inline ArrayList_t4250946984 ** get_address_of_unhandledAttributeList_1() { return &___unhandledAttributeList_1; }
	inline void set_unhandledAttributeList_1(ArrayList_t4250946984 * value)
	{
		___unhandledAttributeList_1 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributeList_1), value);
	}

	inline static int32_t get_offset_of_CompilationId_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t4233170947, ___CompilationId_2)); }
	inline Guid_t  get_CompilationId_2() const { return ___CompilationId_2; }
	inline Guid_t * get_address_of_CompilationId_2() { return &___CompilationId_2; }
	inline void set_CompilationId_2(Guid_t  value)
	{
		___CompilationId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_T4233170947_H
#ifndef READSTATE_T3065260582_H
#define READSTATE_T3065260582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t3065260582 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t3065260582, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T3065260582_H
#ifndef DTDOCCURENCE_T3579469009_H
#define DTDOCCURENCE_T3579469009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOccurence
struct  DTDOccurence_t3579469009 
{
public:
	// System.Int32 Mono.Xml.DTDOccurence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDOccurence_t3579469009, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOCCURENCE_T3579469009_H
#ifndef FACET_T4059729603_H
#define FACET_T4059729603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_t4059729603 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_t4059729603, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_T4059729603_H
#ifndef DTDPARAMETERENTITYDECLARATION_T3047415913_H
#define DTDPARAMETERENTITYDECLARATION_T3047415913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_t3047415913  : public DTDEntityBase_t3196470699
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATION_T3047415913_H
#ifndef WHITESPACEHANDLING_T2116368771_H
#define WHITESPACEHANDLING_T2116368771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t2116368771 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t2116368771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T2116368771_H
#ifndef WRITESTATE_T899266867_H
#define WRITESTATE_T899266867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t899266867 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t899266867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T899266867_H
#ifndef DTDCONTENTORDERTYPE_T464626144_H
#define DTDCONTENTORDERTYPE_T464626144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentOrderType
struct  DTDContentOrderType_t464626144 
{
public:
	// System.Int32 Mono.Xml.DTDContentOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDContentOrderType_t464626144, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTORDERTYPE_T464626144_H
#ifndef XMLSCHEMADATATYPE_T4041842875_H
#define XMLSCHEMADATATYPE_T4041842875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t4041842875  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t622404039 * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875, ___sb_2)); }
	inline StringBuilder_t622404039 * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t622404039 ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t622404039 * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t4041842875_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t3419619864* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t1708784850 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t874272089 * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_t4261832402 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t1018821129 * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t255382392 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_t2642531948 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_t2756618685 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t3987376123 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t1572205312 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_t4001206665 * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_t1646020451 * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t2404058149 * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t915858520 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_t63829613 * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t1221254960 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t950276334 * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_t3891685094 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t1196606063 * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t2543007977 * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_t1409564899 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t1345023881 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t1188126083 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t3166100678 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t3741596672 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t3230006419 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_t2292208357 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_t3062058043 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t206124302 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t535826610 * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t1847923358 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_t1419197403 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t1240435854 * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_t1271651578 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t232015842 * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_t3025817362 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_t2592875340 * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t2932582003 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_t3734795353 * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t1905470603 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t461718195 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t240306832 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t960257253 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t4242852660 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t2084261129 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_t1441225556 * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t411669023 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t3890991598 * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t2375956300 * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t1060067746 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2C_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t3419619864* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t3419619864** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t3419619864* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t1708784850 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t1708784850 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t1708784850 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeString_4)); }
	inline XsdString_t874272089 * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t874272089 ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t874272089 * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_t4261832402 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_t4261832402 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_t4261832402 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t1018821129 * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t1018821129 ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t1018821129 * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t255382392 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t255382392 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t255382392 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_t2642531948 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_t2642531948 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_t2642531948 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_t2756618685 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_t2756618685 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_t2756618685 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeName_10)); }
	inline XsdName_t3987376123 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t3987376123 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t3987376123 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t1572205312 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t1572205312 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t1572205312 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeID_12)); }
	inline XsdID_t4001206665 * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_t4001206665 ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_t4001206665 * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_t1646020451 * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_t1646020451 ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_t1646020451 * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t2404058149 * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t2404058149 ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t2404058149 * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t915858520 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t915858520 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t915858520 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_t63829613 * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_t63829613 ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_t63829613 * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t1221254960 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t1221254960 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t1221254960 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t950276334 * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t950276334 ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t950276334 * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_t3891685094 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_t3891685094 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_t3891685094 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t1196606063 * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t1196606063 ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t1196606063 * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t2543007977 * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t2543007977 ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t2543007977 * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_t1409564899 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_t1409564899 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_t1409564899 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t1345023881 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t1345023881 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t1345023881 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t1188126083 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t1188126083 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t1188126083 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t3166100678 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t3166100678 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t3166100678 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t3741596672 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t3741596672 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t3741596672 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t3230006419 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t3230006419 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t3230006419 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_t2292208357 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_t2292208357 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_t2292208357 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_t3062058043 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_t3062058043 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_t3062058043 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t206124302 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t206124302 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t206124302 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t535826610 * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t535826610 ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t535826610 * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t1847923358 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t1847923358 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t1847923358 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_t1419197403 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_t1419197403 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_t1419197403 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t1240435854 * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t1240435854 ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t1240435854 * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_t1271651578 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_t1271651578 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_t1271651578 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t232015842 * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t232015842 ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t232015842 * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_t3025817362 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_t3025817362 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_t3025817362 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_t2592875340 * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_t2592875340 ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_t2592875340 * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t2932582003 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t2932582003 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t2932582003 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_t3734795353 * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_t3734795353 ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_t3734795353 * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t1905470603 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t1905470603 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t1905470603 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t461718195 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t461718195 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t461718195 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t240306832 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t240306832 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t240306832 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t960257253 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t960257253 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t960257253 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t4242852660 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t4242852660 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t4242852660 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t2084261129 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t2084261129 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t2084261129 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_t1441225556 * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_t1441225556 ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_t1441225556 * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t411669023 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t411669023 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t411669023 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t3890991598 * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t3890991598 ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t3890991598 * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t2375956300 * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t2375956300 ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t2375956300 * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t1060067746 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t1060067746 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t1060067746 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___U3CU3Ef__switchU24map2A_52)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2A_52() const { return ___U3CU3Ef__switchU24map2A_52; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2A_52() { return &___U3CU3Ef__switchU24map2A_52; }
	inline void set_U3CU3Ef__switchU24map2A_52(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2A_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___U3CU3Ef__switchU24map2B_53)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2B_53() const { return ___U3CU3Ef__switchU24map2B_53; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2B_53() { return &___U3CU3Ef__switchU24map2B_53; }
	inline void set_U3CU3Ef__switchU24map2B_53(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2B_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t4041842875_StaticFields, ___U3CU3Ef__switchU24map2C_54)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2C_54() const { return ___U3CU3Ef__switchU24map2C_54; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2C_54() { return &___U3CU3Ef__switchU24map2C_54; }
	inline void set_U3CU3Ef__switchU24map2C_54(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2C_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2C_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T4041842875_H
#ifndef DTDCONTENTMODEL_T2841412075_H
#define DTDCONTENTMODEL_T2841412075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModel
struct  DTDContentModel_t2841412075  : public DTDNode_t3993235537
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDContentModel::root
	DTDObjectModel_t3150902482 * ___root_5;
	// System.String Mono.Xml.DTDContentModel::ownerElementName
	String_t* ___ownerElementName_6;
	// System.String Mono.Xml.DTDContentModel::elementName
	String_t* ___elementName_7;
	// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::orderType
	int32_t ___orderType_8;
	// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::childModels
	DTDContentModelCollection_t133429609 * ___childModels_9;
	// Mono.Xml.DTDOccurence Mono.Xml.DTDContentModel::occurence
	int32_t ___occurence_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___root_5)); }
	inline DTDObjectModel_t3150902482 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t3150902482 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t3150902482 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_ownerElementName_6() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___ownerElementName_6)); }
	inline String_t* get_ownerElementName_6() const { return ___ownerElementName_6; }
	inline String_t** get_address_of_ownerElementName_6() { return &___ownerElementName_6; }
	inline void set_ownerElementName_6(String_t* value)
	{
		___ownerElementName_6 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElementName_6), value);
	}

	inline static int32_t get_offset_of_elementName_7() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___elementName_7)); }
	inline String_t* get_elementName_7() const { return ___elementName_7; }
	inline String_t** get_address_of_elementName_7() { return &___elementName_7; }
	inline void set_elementName_7(String_t* value)
	{
		___elementName_7 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_7), value);
	}

	inline static int32_t get_offset_of_orderType_8() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___orderType_8)); }
	inline int32_t get_orderType_8() const { return ___orderType_8; }
	inline int32_t* get_address_of_orderType_8() { return &___orderType_8; }
	inline void set_orderType_8(int32_t value)
	{
		___orderType_8 = value;
	}

	inline static int32_t get_offset_of_childModels_9() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___childModels_9)); }
	inline DTDContentModelCollection_t133429609 * get_childModels_9() const { return ___childModels_9; }
	inline DTDContentModelCollection_t133429609 ** get_address_of_childModels_9() { return &___childModels_9; }
	inline void set_childModels_9(DTDContentModelCollection_t133429609 * value)
	{
		___childModels_9 = value;
		Il2CppCodeGenWriteBarrier((&___childModels_9), value);
	}

	inline static int32_t get_offset_of_occurence_10() { return static_cast<int32_t>(offsetof(DTDContentModel_t2841412075, ___occurence_10)); }
	inline int32_t get_occurence_10() const { return ___occurence_10; }
	inline int32_t* get_address_of_occurence_10() { return &___occurence_10; }
	inline void set_occurence_10(int32_t value)
	{
		___occurence_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODEL_T2841412075_H
#ifndef DTDENTITYDECLARATIONCOLLECTION_T2395926230_H
#define DTDENTITYDECLARATIONCOLLECTION_T2395926230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t2395926230  : public DTDCollectionBase_t403511423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATIONCOLLECTION_T2395926230_H
#ifndef DTDNOTATIONDECLARATIONCOLLECTION_T2979384550_H
#define DTDNOTATIONDECLARATIONCOLLECTION_T2979384550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_t2979384550  : public DTDCollectionBase_t403511423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATIONCOLLECTION_T2979384550_H
#ifndef XMLSCHEMAANNOTATED_T2235692007_H
#define XMLSCHEMAANNOTATED_T2235692007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t2235692007  : public XmlSchemaObject_t4233170947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T2235692007_H
#ifndef DTDATTLISTDECLARATIONCOLLECTION_T748347165_H
#define DTDATTLISTDECLARATIONCOLLECTION_T748347165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclarationCollection
struct  DTDAttListDeclarationCollection_t748347165  : public DTDCollectionBase_t403511423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATIONCOLLECTION_T748347165_H
#ifndef DTDELEMENTDECLARATIONCOLLECTION_T3634927218_H
#define DTDELEMENTDECLARATIONCOLLECTION_T3634927218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclarationCollection
struct  DTDElementDeclarationCollection_t3634927218  : public DTDCollectionBase_t403511423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATIONCOLLECTION_T3634927218_H
#ifndef XMLSCHEMAUTIL_T4137508536_H
#define XMLSCHEMAUTIL_T4137508536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUtil
struct  XmlSchemaUtil_t4137508536  : public RuntimeObject
{
public:

public:
};

struct XmlSchemaUtil_t4137508536_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::FinalAllowed
	int32_t ___FinalAllowed_0;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ElementBlockAllowed
	int32_t ___ElementBlockAllowed_1;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ComplexTypeBlockAllowed
	int32_t ___ComplexTypeBlockAllowed_2;
	// System.Boolean System.Xml.Schema.XmlSchemaUtil::StrictMsCompliant
	bool ___StrictMsCompliant_3;

public:
	inline static int32_t get_offset_of_FinalAllowed_0() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t4137508536_StaticFields, ___FinalAllowed_0)); }
	inline int32_t get_FinalAllowed_0() const { return ___FinalAllowed_0; }
	inline int32_t* get_address_of_FinalAllowed_0() { return &___FinalAllowed_0; }
	inline void set_FinalAllowed_0(int32_t value)
	{
		___FinalAllowed_0 = value;
	}

	inline static int32_t get_offset_of_ElementBlockAllowed_1() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t4137508536_StaticFields, ___ElementBlockAllowed_1)); }
	inline int32_t get_ElementBlockAllowed_1() const { return ___ElementBlockAllowed_1; }
	inline int32_t* get_address_of_ElementBlockAllowed_1() { return &___ElementBlockAllowed_1; }
	inline void set_ElementBlockAllowed_1(int32_t value)
	{
		___ElementBlockAllowed_1 = value;
	}

	inline static int32_t get_offset_of_ComplexTypeBlockAllowed_2() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t4137508536_StaticFields, ___ComplexTypeBlockAllowed_2)); }
	inline int32_t get_ComplexTypeBlockAllowed_2() const { return ___ComplexTypeBlockAllowed_2; }
	inline int32_t* get_address_of_ComplexTypeBlockAllowed_2() { return &___ComplexTypeBlockAllowed_2; }
	inline void set_ComplexTypeBlockAllowed_2(int32_t value)
	{
		___ComplexTypeBlockAllowed_2 = value;
	}

	inline static int32_t get_offset_of_StrictMsCompliant_3() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t4137508536_StaticFields, ___StrictMsCompliant_3)); }
	inline bool get_StrictMsCompliant_3() const { return ___StrictMsCompliant_3; }
	inline bool* get_address_of_StrictMsCompliant_3() { return &___StrictMsCompliant_3; }
	inline void set_StrictMsCompliant_3(bool value)
	{
		___StrictMsCompliant_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUTIL_T4137508536_H
#ifndef U3CU3EC__ITERATOR3_T1821397857_H
#define U3CU3EC__ITERATOR3_T1821397857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct  U3CU3Ec__Iterator3_t1821397857  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase/<>c__Iterator3::<$s_431>__0
	Enumerator_t1238231328  ___U3CU24s_431U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::<p>__1
	KeyValuePair_2_t2342735797  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase/<>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::$current
	DTDNode_t3993235537 * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase/<>c__Iterator3::<>f__this
	DictionaryBase_t4072949440 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_431U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t1821397857, ___U3CU24s_431U3E__0_0)); }
	inline Enumerator_t1238231328  get_U3CU24s_431U3E__0_0() const { return ___U3CU24s_431U3E__0_0; }
	inline Enumerator_t1238231328 * get_address_of_U3CU24s_431U3E__0_0() { return &___U3CU24s_431U3E__0_0; }
	inline void set_U3CU24s_431U3E__0_0(Enumerator_t1238231328  value)
	{
		___U3CU24s_431U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t1821397857, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t2342735797  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t2342735797 * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t2342735797  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t1821397857, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t1821397857, ___U24current_3)); }
	inline DTDNode_t3993235537 * get_U24current_3() const { return ___U24current_3; }
	inline DTDNode_t3993235537 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(DTDNode_t3993235537 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t1821397857, ___U3CU3Ef__this_4)); }
	inline DictionaryBase_t4072949440 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline DictionaryBase_t4072949440 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(DictionaryBase_t4072949440 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR3_T1821397857_H
#ifndef XMLCOMMENT_T434142218_H
#define XMLCOMMENT_T434142218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t434142218  : public XmlCharacterData_t4217070933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T434142218_H
#ifndef XMLCDATASECTION_T3479907102_H
#define XMLCDATASECTION_T3479907102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t3479907102  : public XmlCharacterData_t4217070933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T3479907102_H
#ifndef XMLSCHEMAINFO_T933726380_H
#define XMLSCHEMAINFO_T933726380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInfo
struct  XmlSchemaInfo_t933726380  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isDefault
	bool ___isDefault_0;
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isNil
	bool ___isNil_1;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::memberType
	XmlSchemaSimpleType_t3397264039 * ___memberType_2;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::attr
	XmlSchemaAttribute_t803373732 * ___attr_3;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::elem
	XmlSchemaElement_t2744860620 * ___elem_4;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::type
	XmlSchemaType_t2926683629 * ___type_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::validity
	int32_t ___validity_6;

public:
	inline static int32_t get_offset_of_isDefault_0() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___isDefault_0)); }
	inline bool get_isDefault_0() const { return ___isDefault_0; }
	inline bool* get_address_of_isDefault_0() { return &___isDefault_0; }
	inline void set_isDefault_0(bool value)
	{
		___isDefault_0 = value;
	}

	inline static int32_t get_offset_of_isNil_1() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___isNil_1)); }
	inline bool get_isNil_1() const { return ___isNil_1; }
	inline bool* get_address_of_isNil_1() { return &___isNil_1; }
	inline void set_isNil_1(bool value)
	{
		___isNil_1 = value;
	}

	inline static int32_t get_offset_of_memberType_2() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___memberType_2)); }
	inline XmlSchemaSimpleType_t3397264039 * get_memberType_2() const { return ___memberType_2; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_memberType_2() { return &___memberType_2; }
	inline void set_memberType_2(XmlSchemaSimpleType_t3397264039 * value)
	{
		___memberType_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_2), value);
	}

	inline static int32_t get_offset_of_attr_3() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___attr_3)); }
	inline XmlSchemaAttribute_t803373732 * get_attr_3() const { return ___attr_3; }
	inline XmlSchemaAttribute_t803373732 ** get_address_of_attr_3() { return &___attr_3; }
	inline void set_attr_3(XmlSchemaAttribute_t803373732 * value)
	{
		___attr_3 = value;
		Il2CppCodeGenWriteBarrier((&___attr_3), value);
	}

	inline static int32_t get_offset_of_elem_4() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___elem_4)); }
	inline XmlSchemaElement_t2744860620 * get_elem_4() const { return ___elem_4; }
	inline XmlSchemaElement_t2744860620 ** get_address_of_elem_4() { return &___elem_4; }
	inline void set_elem_4(XmlSchemaElement_t2744860620 * value)
	{
		___elem_4 = value;
		Il2CppCodeGenWriteBarrier((&___elem_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___type_5)); }
	inline XmlSchemaType_t2926683629 * get_type_5() const { return ___type_5; }
	inline XmlSchemaType_t2926683629 ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(XmlSchemaType_t2926683629 * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_validity_6() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t933726380, ___validity_6)); }
	inline int32_t get_validity_6() const { return ___validity_6; }
	inline int32_t* get_address_of_validity_6() { return &___validity_6; }
	inline void set_validity_6(int32_t value)
	{
		___validity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFO_T933726380_H
#ifndef XSDANYSIMPLETYPE_T1708784850_H
#define XSDANYSIMPLETYPE_T1708784850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t1708784850  : public XmlSchemaDatatype_t4041842875
{
public:

public:
};

struct XsdAnySimpleType_t1708784850_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t1708784850 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t3419619864* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t1708784850 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t1708784850 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t1708784850 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t3419619864* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t3419619864** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t3419619864* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1708784850_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T1708784850_H
#ifndef XMLSCHEMAFACET_T2775705363_H
#define XMLSCHEMAFACET_T2775705363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t2775705363  : public XmlSchemaAnnotated_t2235692007
{
public:

public:
};

struct XmlSchemaFacet_t2775705363_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_3;

public:
	inline static int32_t get_offset_of_AllFacets_3() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t2775705363_StaticFields, ___AllFacets_3)); }
	inline int32_t get_AllFacets_3() const { return ___AllFacets_3; }
	inline int32_t* get_address_of_AllFacets_3() { return &___AllFacets_3; }
	inline void set_AllFacets_3(int32_t value)
	{
		___AllFacets_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_T2775705363_H
#ifndef XMLSCHEMATYPE_T2926683629_H
#define XMLSCHEMATYPE_T2926683629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_t2926683629  : public XmlSchemaAnnotated_t2235692007
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::BaseXmlSchemaTypeInternal
	XmlSchemaType_t2926683629 * ___BaseXmlSchemaTypeInternal_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::DatatypeInternal
	XmlSchemaDatatype_t4041842875 * ___DatatypeInternal_5;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::QNameInternal
	XmlQualifiedName_t740477960 * ___QNameInternal_6;

public:
	inline static int32_t get_offset_of_final_3() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629, ___final_3)); }
	inline int32_t get_final_3() const { return ___final_3; }
	inline int32_t* get_address_of_final_3() { return &___final_3; }
	inline void set_final_3(int32_t value)
	{
		___final_3 = value;
	}

	inline static int32_t get_offset_of_BaseXmlSchemaTypeInternal_4() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629, ___BaseXmlSchemaTypeInternal_4)); }
	inline XmlSchemaType_t2926683629 * get_BaseXmlSchemaTypeInternal_4() const { return ___BaseXmlSchemaTypeInternal_4; }
	inline XmlSchemaType_t2926683629 ** get_address_of_BaseXmlSchemaTypeInternal_4() { return &___BaseXmlSchemaTypeInternal_4; }
	inline void set_BaseXmlSchemaTypeInternal_4(XmlSchemaType_t2926683629 * value)
	{
		___BaseXmlSchemaTypeInternal_4 = value;
		Il2CppCodeGenWriteBarrier((&___BaseXmlSchemaTypeInternal_4), value);
	}

	inline static int32_t get_offset_of_DatatypeInternal_5() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629, ___DatatypeInternal_5)); }
	inline XmlSchemaDatatype_t4041842875 * get_DatatypeInternal_5() const { return ___DatatypeInternal_5; }
	inline XmlSchemaDatatype_t4041842875 ** get_address_of_DatatypeInternal_5() { return &___DatatypeInternal_5; }
	inline void set_DatatypeInternal_5(XmlSchemaDatatype_t4041842875 * value)
	{
		___DatatypeInternal_5 = value;
		Il2CppCodeGenWriteBarrier((&___DatatypeInternal_5), value);
	}

	inline static int32_t get_offset_of_QNameInternal_6() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629, ___QNameInternal_6)); }
	inline XmlQualifiedName_t740477960 * get_QNameInternal_6() const { return ___QNameInternal_6; }
	inline XmlQualifiedName_t740477960 ** get_address_of_QNameInternal_6() { return &___QNameInternal_6; }
	inline void set_QNameInternal_6(XmlQualifiedName_t740477960 * value)
	{
		___QNameInternal_6 = value;
		Il2CppCodeGenWriteBarrier((&___QNameInternal_6), value);
	}
};

struct XmlSchemaType_t2926683629_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2E
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2E_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2F
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map2F_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2E_7() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629_StaticFields, ___U3CU3Ef__switchU24map2E_7)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2E_7() const { return ___U3CU3Ef__switchU24map2E_7; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2E_7() { return &___U3CU3Ef__switchU24map2E_7; }
	inline void set_U3CU3Ef__switchU24map2E_7(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2E_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2E_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2F_8() { return static_cast<int32_t>(offsetof(XmlSchemaType_t2926683629_StaticFields, ___U3CU3Ef__switchU24map2F_8)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map2F_8() const { return ___U3CU3Ef__switchU24map2F_8; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map2F_8() { return &___U3CU3Ef__switchU24map2F_8; }
	inline void set_U3CU3Ef__switchU24map2F_8(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map2F_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2F_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATYPE_T2926683629_H
#ifndef XMLSCHEMAPARTICLE_T110409631_H
#define XMLSCHEMAPARTICLE_T110409631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_t110409631  : public XmlSchemaAnnotated_t2235692007
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPARTICLE_T110409631_H
#ifndef XMLSCHEMAATTRIBUTE_T803373732_H
#define XMLSCHEMAATTRIBUTE_T803373732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_t803373732  : public XmlSchemaAnnotated_t2235692007
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTE_T803373732_H
#ifndef XMLSCHEMASIMPLETYPECONTENT_T1082177942_H
#define XMLSCHEMASIMPLETYPECONTENT_T1082177942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct  XmlSchemaSimpleTypeContent_t1082177942  : public XmlSchemaAnnotated_t2235692007
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPECONTENT_T1082177942_H
#ifndef XSDSTRING_T874272089_H
#define XSDSTRING_T874272089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t874272089  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T874272089_H
#ifndef XSDDECIMAL_T950276334_H
#define XSDDECIMAL_T950276334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDecimal
struct  XsdDecimal_t950276334  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDECIMAL_T950276334_H
#ifndef XMLSCHEMASIMPLETYPERESTRICTION_T1836394642_H
#define XMLSCHEMASIMPLETYPERESTRICTION_T1836394642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct  XmlSchemaSimpleTypeRestriction_t1836394642  : public XmlSchemaSimpleTypeContent_t1082177942
{
public:

public:
};

struct XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields
{
public:
	// System.Globalization.NumberStyles System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthStyle
	int32_t ___lengthStyle_3;
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::listFacets
	int32_t ___listFacets_4;

public:
	inline static int32_t get_offset_of_lengthStyle_3() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields, ___lengthStyle_3)); }
	inline int32_t get_lengthStyle_3() const { return ___lengthStyle_3; }
	inline int32_t* get_address_of_lengthStyle_3() { return &___lengthStyle_3; }
	inline void set_lengthStyle_3(int32_t value)
	{
		___lengthStyle_3 = value;
	}

	inline static int32_t get_offset_of_listFacets_4() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields, ___listFacets_4)); }
	inline int32_t get_listFacets_4() const { return ___listFacets_4; }
	inline int32_t* get_address_of_listFacets_4() { return &___listFacets_4; }
	inline void set_listFacets_4(int32_t value)
	{
		___listFacets_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPERESTRICTION_T1836394642_H
#ifndef XSDBOOLEAN_T1271651578_H
#define XSDBOOLEAN_T1271651578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBoolean
struct  XsdBoolean_t1271651578  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBOOLEAN_T1271651578_H
#ifndef XSDDOUBLE_T1419197403_H
#define XSDDOUBLE_T1419197403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDouble
struct  XsdDouble_t1419197403  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDOUBLE_T1419197403_H
#ifndef XSDDURATION_T3025817362_H
#define XSDDURATION_T3025817362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDuration
struct  XsdDuration_t3025817362  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T3025817362_H
#ifndef XSDFLOAT_T1847923358_H
#define XSDFLOAT_T1847923358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdFloat
struct  XsdFloat_t1847923358  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDFLOAT_T1847923358_H
#ifndef XSDDATETIME_T2592875340_H
#define XSDDATETIME_T2592875340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDateTime
struct  XsdDateTime_t2592875340  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIME_T2592875340_H
#ifndef XSDDATE_T2932582003_H
#define XSDDATE_T2932582003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t2932582003  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATE_T2932582003_H
#ifndef XMLSCHEMASIMPLETYPEUNION_T4070439509_H
#define XMLSCHEMASIMPLETYPEUNION_T4070439509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct  XmlSchemaSimpleTypeUnion_t4070439509  : public XmlSchemaSimpleTypeContent_t1082177942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPEUNION_T4070439509_H
#ifndef XSDTIME_T3734795353_H
#define XSDTIME_T3734795353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdTime
struct  XsdTime_t3734795353  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

struct XsdTime_t3734795353_StaticFields
{
public:
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t2511808107* ___timeFormats_61;

public:
	inline static int32_t get_offset_of_timeFormats_61() { return static_cast<int32_t>(offsetof(XsdTime_t3734795353_StaticFields, ___timeFormats_61)); }
	inline StringU5BU5D_t2511808107* get_timeFormats_61() const { return ___timeFormats_61; }
	inline StringU5BU5D_t2511808107** get_address_of_timeFormats_61() { return &___timeFormats_61; }
	inline void set_timeFormats_61(StringU5BU5D_t2511808107* value)
	{
		___timeFormats_61 = value;
		Il2CppCodeGenWriteBarrier((&___timeFormats_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTIME_T3734795353_H
#ifndef XSDGMONTHDAY_T960257253_H
#define XSDGMONTHDAY_T960257253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonthDay
struct  XsdGMonthDay_t960257253  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTHDAY_T960257253_H
#ifndef XSDGYEAR_T4242852660_H
#define XSDGYEAR_T4242852660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYear
struct  XsdGYear_t4242852660  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEAR_T4242852660_H
#ifndef XSDGMONTH_T2084261129_H
#define XSDGMONTH_T2084261129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonth
struct  XsdGMonth_t2084261129  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTH_T2084261129_H
#ifndef XSDGDAY_T1441225556_H
#define XSDGDAY_T1441225556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGDay
struct  XsdGDay_t1441225556  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGDAY_T1441225556_H
#ifndef XMLSCHEMAELEMENT_T2744860620_H
#define XMLSCHEMAELEMENT_T2744860620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_t2744860620  : public XmlSchemaParticle_t110409631
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAELEMENT_T2744860620_H
#ifndef XMLSCHEMASIMPLETYPE_T3397264039_H
#define XMLSCHEMASIMPLETYPE_T3397264039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t3397264039  : public XmlSchemaType_t2926683629
{
public:
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t1082177942 * ___content_10;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::islocal
	bool ___islocal_11;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaSimpleType::variety
	int32_t ___variety_12;

public:
	inline static int32_t get_offset_of_content_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039, ___content_10)); }
	inline XmlSchemaSimpleTypeContent_t1082177942 * get_content_10() const { return ___content_10; }
	inline XmlSchemaSimpleTypeContent_t1082177942 ** get_address_of_content_10() { return &___content_10; }
	inline void set_content_10(XmlSchemaSimpleTypeContent_t1082177942 * value)
	{
		___content_10 = value;
		Il2CppCodeGenWriteBarrier((&___content_10), value);
	}

	inline static int32_t get_offset_of_islocal_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039, ___islocal_11)); }
	inline bool get_islocal_11() const { return ___islocal_11; }
	inline bool* get_address_of_islocal_11() { return &___islocal_11; }
	inline void set_islocal_11(bool value)
	{
		___islocal_11 = value;
	}

	inline static int32_t get_offset_of_variety_12() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039, ___variety_12)); }
	inline int32_t get_variety_12() const { return ___variety_12; }
	inline int32_t* get_address_of_variety_12() { return &___variety_12; }
	inline void set_variety_12(int32_t value)
	{
		___variety_12 = value;
	}
};

struct XmlSchemaSimpleType_t3397264039_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::schemaLocationType
	XmlSchemaSimpleType_t3397264039 * ___schemaLocationType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnySimpleType
	XmlSchemaSimpleType_t3397264039 * ___XsAnySimpleType_13;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsString
	XmlSchemaSimpleType_t3397264039 * ___XsString_14;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBoolean
	XmlSchemaSimpleType_t3397264039 * ___XsBoolean_15;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDecimal
	XmlSchemaSimpleType_t3397264039 * ___XsDecimal_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsFloat
	XmlSchemaSimpleType_t3397264039 * ___XsFloat_17;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDouble
	XmlSchemaSimpleType_t3397264039 * ___XsDouble_18;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDuration
	XmlSchemaSimpleType_t3397264039 * ___XsDuration_19;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDateTime
	XmlSchemaSimpleType_t3397264039 * ___XsDateTime_20;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsTime
	XmlSchemaSimpleType_t3397264039 * ___XsTime_21;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDate
	XmlSchemaSimpleType_t3397264039 * ___XsDate_22;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYearMonth
	XmlSchemaSimpleType_t3397264039 * ___XsGYearMonth_23;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYear
	XmlSchemaSimpleType_t3397264039 * ___XsGYear_24;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonthDay
	XmlSchemaSimpleType_t3397264039 * ___XsGMonthDay_25;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGDay
	XmlSchemaSimpleType_t3397264039 * ___XsGDay_26;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonth
	XmlSchemaSimpleType_t3397264039 * ___XsGMonth_27;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsHexBinary
	XmlSchemaSimpleType_t3397264039 * ___XsHexBinary_28;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBase64Binary
	XmlSchemaSimpleType_t3397264039 * ___XsBase64Binary_29;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnyUri
	XmlSchemaSimpleType_t3397264039 * ___XsAnyUri_30;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsQName
	XmlSchemaSimpleType_t3397264039 * ___XsQName_31;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNotation
	XmlSchemaSimpleType_t3397264039 * ___XsNotation_32;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNormalizedString
	XmlSchemaSimpleType_t3397264039 * ___XsNormalizedString_33;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsToken
	XmlSchemaSimpleType_t3397264039 * ___XsToken_34;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLanguage
	XmlSchemaSimpleType_t3397264039 * ___XsLanguage_35;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMToken
	XmlSchemaSimpleType_t3397264039 * ___XsNMToken_36;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMTokens
	XmlSchemaSimpleType_t3397264039 * ___XsNMTokens_37;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsName
	XmlSchemaSimpleType_t3397264039 * ___XsName_38;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNCName
	XmlSchemaSimpleType_t3397264039 * ___XsNCName_39;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsID
	XmlSchemaSimpleType_t3397264039 * ___XsID_40;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRef
	XmlSchemaSimpleType_t3397264039 * ___XsIDRef_41;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRefs
	XmlSchemaSimpleType_t3397264039 * ___XsIDRefs_42;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntity
	XmlSchemaSimpleType_t3397264039 * ___XsEntity_43;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntities
	XmlSchemaSimpleType_t3397264039 * ___XsEntities_44;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInteger
	XmlSchemaSimpleType_t3397264039 * ___XsInteger_45;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonPositiveInteger
	XmlSchemaSimpleType_t3397264039 * ___XsNonPositiveInteger_46;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNegativeInteger
	XmlSchemaSimpleType_t3397264039 * ___XsNegativeInteger_47;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLong
	XmlSchemaSimpleType_t3397264039 * ___XsLong_48;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInt
	XmlSchemaSimpleType_t3397264039 * ___XsInt_49;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsShort
	XmlSchemaSimpleType_t3397264039 * ___XsShort_50;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsByte
	XmlSchemaSimpleType_t3397264039 * ___XsByte_51;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonNegativeInteger
	XmlSchemaSimpleType_t3397264039 * ___XsNonNegativeInteger_52;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedLong
	XmlSchemaSimpleType_t3397264039 * ___XsUnsignedLong_53;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedInt
	XmlSchemaSimpleType_t3397264039 * ___XsUnsignedInt_54;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedShort
	XmlSchemaSimpleType_t3397264039 * ___XsUnsignedShort_55;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedByte
	XmlSchemaSimpleType_t3397264039 * ___XsUnsignedByte_56;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsPositiveInteger
	XmlSchemaSimpleType_t3397264039 * ___XsPositiveInteger_57;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtUntypedAtomic
	XmlSchemaSimpleType_t3397264039 * ___XdtUntypedAtomic_58;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtAnyAtomicType
	XmlSchemaSimpleType_t3397264039 * ___XdtAnyAtomicType_59;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtYearMonthDuration
	XmlSchemaSimpleType_t3397264039 * ___XdtYearMonthDuration_60;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtDayTimeDuration
	XmlSchemaSimpleType_t3397264039 * ___XdtDayTimeDuration_61;

public:
	inline static int32_t get_offset_of_schemaLocationType_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___schemaLocationType_9)); }
	inline XmlSchemaSimpleType_t3397264039 * get_schemaLocationType_9() const { return ___schemaLocationType_9; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_schemaLocationType_9() { return &___schemaLocationType_9; }
	inline void set_schemaLocationType_9(XmlSchemaSimpleType_t3397264039 * value)
	{
		___schemaLocationType_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocationType_9), value);
	}

	inline static int32_t get_offset_of_XsAnySimpleType_13() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsAnySimpleType_13)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsAnySimpleType_13() const { return ___XsAnySimpleType_13; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsAnySimpleType_13() { return &___XsAnySimpleType_13; }
	inline void set_XsAnySimpleType_13(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsAnySimpleType_13 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnySimpleType_13), value);
	}

	inline static int32_t get_offset_of_XsString_14() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsString_14)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsString_14() const { return ___XsString_14; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsString_14() { return &___XsString_14; }
	inline void set_XsString_14(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsString_14 = value;
		Il2CppCodeGenWriteBarrier((&___XsString_14), value);
	}

	inline static int32_t get_offset_of_XsBoolean_15() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsBoolean_15)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsBoolean_15() const { return ___XsBoolean_15; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsBoolean_15() { return &___XsBoolean_15; }
	inline void set_XsBoolean_15(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsBoolean_15 = value;
		Il2CppCodeGenWriteBarrier((&___XsBoolean_15), value);
	}

	inline static int32_t get_offset_of_XsDecimal_16() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsDecimal_16)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsDecimal_16() const { return ___XsDecimal_16; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsDecimal_16() { return &___XsDecimal_16; }
	inline void set_XsDecimal_16(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsDecimal_16 = value;
		Il2CppCodeGenWriteBarrier((&___XsDecimal_16), value);
	}

	inline static int32_t get_offset_of_XsFloat_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsFloat_17)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsFloat_17() const { return ___XsFloat_17; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsFloat_17() { return &___XsFloat_17; }
	inline void set_XsFloat_17(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsFloat_17 = value;
		Il2CppCodeGenWriteBarrier((&___XsFloat_17), value);
	}

	inline static int32_t get_offset_of_XsDouble_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsDouble_18)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsDouble_18() const { return ___XsDouble_18; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsDouble_18() { return &___XsDouble_18; }
	inline void set_XsDouble_18(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsDouble_18 = value;
		Il2CppCodeGenWriteBarrier((&___XsDouble_18), value);
	}

	inline static int32_t get_offset_of_XsDuration_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsDuration_19)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsDuration_19() const { return ___XsDuration_19; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsDuration_19() { return &___XsDuration_19; }
	inline void set_XsDuration_19(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsDuration_19 = value;
		Il2CppCodeGenWriteBarrier((&___XsDuration_19), value);
	}

	inline static int32_t get_offset_of_XsDateTime_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsDateTime_20)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsDateTime_20() const { return ___XsDateTime_20; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsDateTime_20() { return &___XsDateTime_20; }
	inline void set_XsDateTime_20(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsDateTime_20 = value;
		Il2CppCodeGenWriteBarrier((&___XsDateTime_20), value);
	}

	inline static int32_t get_offset_of_XsTime_21() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsTime_21)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsTime_21() const { return ___XsTime_21; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsTime_21() { return &___XsTime_21; }
	inline void set_XsTime_21(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsTime_21 = value;
		Il2CppCodeGenWriteBarrier((&___XsTime_21), value);
	}

	inline static int32_t get_offset_of_XsDate_22() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsDate_22)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsDate_22() const { return ___XsDate_22; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsDate_22() { return &___XsDate_22; }
	inline void set_XsDate_22(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsDate_22 = value;
		Il2CppCodeGenWriteBarrier((&___XsDate_22), value);
	}

	inline static int32_t get_offset_of_XsGYearMonth_23() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsGYearMonth_23)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsGYearMonth_23() const { return ___XsGYearMonth_23; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsGYearMonth_23() { return &___XsGYearMonth_23; }
	inline void set_XsGYearMonth_23(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsGYearMonth_23 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYearMonth_23), value);
	}

	inline static int32_t get_offset_of_XsGYear_24() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsGYear_24)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsGYear_24() const { return ___XsGYear_24; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsGYear_24() { return &___XsGYear_24; }
	inline void set_XsGYear_24(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsGYear_24 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYear_24), value);
	}

	inline static int32_t get_offset_of_XsGMonthDay_25() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsGMonthDay_25)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsGMonthDay_25() const { return ___XsGMonthDay_25; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsGMonthDay_25() { return &___XsGMonthDay_25; }
	inline void set_XsGMonthDay_25(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsGMonthDay_25 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonthDay_25), value);
	}

	inline static int32_t get_offset_of_XsGDay_26() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsGDay_26)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsGDay_26() const { return ___XsGDay_26; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsGDay_26() { return &___XsGDay_26; }
	inline void set_XsGDay_26(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsGDay_26 = value;
		Il2CppCodeGenWriteBarrier((&___XsGDay_26), value);
	}

	inline static int32_t get_offset_of_XsGMonth_27() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsGMonth_27)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsGMonth_27() const { return ___XsGMonth_27; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsGMonth_27() { return &___XsGMonth_27; }
	inline void set_XsGMonth_27(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsGMonth_27 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonth_27), value);
	}

	inline static int32_t get_offset_of_XsHexBinary_28() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsHexBinary_28)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsHexBinary_28() const { return ___XsHexBinary_28; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsHexBinary_28() { return &___XsHexBinary_28; }
	inline void set_XsHexBinary_28(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsHexBinary_28 = value;
		Il2CppCodeGenWriteBarrier((&___XsHexBinary_28), value);
	}

	inline static int32_t get_offset_of_XsBase64Binary_29() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsBase64Binary_29)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsBase64Binary_29() const { return ___XsBase64Binary_29; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsBase64Binary_29() { return &___XsBase64Binary_29; }
	inline void set_XsBase64Binary_29(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsBase64Binary_29 = value;
		Il2CppCodeGenWriteBarrier((&___XsBase64Binary_29), value);
	}

	inline static int32_t get_offset_of_XsAnyUri_30() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsAnyUri_30)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsAnyUri_30() const { return ___XsAnyUri_30; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsAnyUri_30() { return &___XsAnyUri_30; }
	inline void set_XsAnyUri_30(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsAnyUri_30 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnyUri_30), value);
	}

	inline static int32_t get_offset_of_XsQName_31() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsQName_31)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsQName_31() const { return ___XsQName_31; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsQName_31() { return &___XsQName_31; }
	inline void set_XsQName_31(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsQName_31 = value;
		Il2CppCodeGenWriteBarrier((&___XsQName_31), value);
	}

	inline static int32_t get_offset_of_XsNotation_32() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNotation_32)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNotation_32() const { return ___XsNotation_32; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNotation_32() { return &___XsNotation_32; }
	inline void set_XsNotation_32(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNotation_32 = value;
		Il2CppCodeGenWriteBarrier((&___XsNotation_32), value);
	}

	inline static int32_t get_offset_of_XsNormalizedString_33() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNormalizedString_33)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNormalizedString_33() const { return ___XsNormalizedString_33; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNormalizedString_33() { return &___XsNormalizedString_33; }
	inline void set_XsNormalizedString_33(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNormalizedString_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsNormalizedString_33), value);
	}

	inline static int32_t get_offset_of_XsToken_34() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsToken_34)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsToken_34() const { return ___XsToken_34; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsToken_34() { return &___XsToken_34; }
	inline void set_XsToken_34(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsToken_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsToken_34), value);
	}

	inline static int32_t get_offset_of_XsLanguage_35() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsLanguage_35)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsLanguage_35() const { return ___XsLanguage_35; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsLanguage_35() { return &___XsLanguage_35; }
	inline void set_XsLanguage_35(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsLanguage_35 = value;
		Il2CppCodeGenWriteBarrier((&___XsLanguage_35), value);
	}

	inline static int32_t get_offset_of_XsNMToken_36() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNMToken_36)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNMToken_36() const { return ___XsNMToken_36; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNMToken_36() { return &___XsNMToken_36; }
	inline void set_XsNMToken_36(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNMToken_36 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMToken_36), value);
	}

	inline static int32_t get_offset_of_XsNMTokens_37() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNMTokens_37)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNMTokens_37() const { return ___XsNMTokens_37; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNMTokens_37() { return &___XsNMTokens_37; }
	inline void set_XsNMTokens_37(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNMTokens_37 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMTokens_37), value);
	}

	inline static int32_t get_offset_of_XsName_38() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsName_38)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsName_38() const { return ___XsName_38; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsName_38() { return &___XsName_38; }
	inline void set_XsName_38(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsName_38 = value;
		Il2CppCodeGenWriteBarrier((&___XsName_38), value);
	}

	inline static int32_t get_offset_of_XsNCName_39() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNCName_39)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNCName_39() const { return ___XsNCName_39; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNCName_39() { return &___XsNCName_39; }
	inline void set_XsNCName_39(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNCName_39 = value;
		Il2CppCodeGenWriteBarrier((&___XsNCName_39), value);
	}

	inline static int32_t get_offset_of_XsID_40() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsID_40)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsID_40() const { return ___XsID_40; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsID_40() { return &___XsID_40; }
	inline void set_XsID_40(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsID_40 = value;
		Il2CppCodeGenWriteBarrier((&___XsID_40), value);
	}

	inline static int32_t get_offset_of_XsIDRef_41() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsIDRef_41)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsIDRef_41() const { return ___XsIDRef_41; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsIDRef_41() { return &___XsIDRef_41; }
	inline void set_XsIDRef_41(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsIDRef_41 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRef_41), value);
	}

	inline static int32_t get_offset_of_XsIDRefs_42() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsIDRefs_42)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsIDRefs_42() const { return ___XsIDRefs_42; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsIDRefs_42() { return &___XsIDRefs_42; }
	inline void set_XsIDRefs_42(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsIDRefs_42 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRefs_42), value);
	}

	inline static int32_t get_offset_of_XsEntity_43() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsEntity_43)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsEntity_43() const { return ___XsEntity_43; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsEntity_43() { return &___XsEntity_43; }
	inline void set_XsEntity_43(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsEntity_43 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntity_43), value);
	}

	inline static int32_t get_offset_of_XsEntities_44() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsEntities_44)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsEntities_44() const { return ___XsEntities_44; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsEntities_44() { return &___XsEntities_44; }
	inline void set_XsEntities_44(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsEntities_44 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntities_44), value);
	}

	inline static int32_t get_offset_of_XsInteger_45() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsInteger_45)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsInteger_45() const { return ___XsInteger_45; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsInteger_45() { return &___XsInteger_45; }
	inline void set_XsInteger_45(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsInteger_45 = value;
		Il2CppCodeGenWriteBarrier((&___XsInteger_45), value);
	}

	inline static int32_t get_offset_of_XsNonPositiveInteger_46() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNonPositiveInteger_46)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNonPositiveInteger_46() const { return ___XsNonPositiveInteger_46; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNonPositiveInteger_46() { return &___XsNonPositiveInteger_46; }
	inline void set_XsNonPositiveInteger_46(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNonPositiveInteger_46 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonPositiveInteger_46), value);
	}

	inline static int32_t get_offset_of_XsNegativeInteger_47() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNegativeInteger_47)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNegativeInteger_47() const { return ___XsNegativeInteger_47; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNegativeInteger_47() { return &___XsNegativeInteger_47; }
	inline void set_XsNegativeInteger_47(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNegativeInteger_47 = value;
		Il2CppCodeGenWriteBarrier((&___XsNegativeInteger_47), value);
	}

	inline static int32_t get_offset_of_XsLong_48() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsLong_48)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsLong_48() const { return ___XsLong_48; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsLong_48() { return &___XsLong_48; }
	inline void set_XsLong_48(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsLong_48 = value;
		Il2CppCodeGenWriteBarrier((&___XsLong_48), value);
	}

	inline static int32_t get_offset_of_XsInt_49() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsInt_49)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsInt_49() const { return ___XsInt_49; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsInt_49() { return &___XsInt_49; }
	inline void set_XsInt_49(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsInt_49 = value;
		Il2CppCodeGenWriteBarrier((&___XsInt_49), value);
	}

	inline static int32_t get_offset_of_XsShort_50() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsShort_50)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsShort_50() const { return ___XsShort_50; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsShort_50() { return &___XsShort_50; }
	inline void set_XsShort_50(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsShort_50 = value;
		Il2CppCodeGenWriteBarrier((&___XsShort_50), value);
	}

	inline static int32_t get_offset_of_XsByte_51() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsByte_51)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsByte_51() const { return ___XsByte_51; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsByte_51() { return &___XsByte_51; }
	inline void set_XsByte_51(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsByte_51 = value;
		Il2CppCodeGenWriteBarrier((&___XsByte_51), value);
	}

	inline static int32_t get_offset_of_XsNonNegativeInteger_52() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsNonNegativeInteger_52)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsNonNegativeInteger_52() const { return ___XsNonNegativeInteger_52; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsNonNegativeInteger_52() { return &___XsNonNegativeInteger_52; }
	inline void set_XsNonNegativeInteger_52(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsNonNegativeInteger_52 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonNegativeInteger_52), value);
	}

	inline static int32_t get_offset_of_XsUnsignedLong_53() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsUnsignedLong_53)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsUnsignedLong_53() const { return ___XsUnsignedLong_53; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsUnsignedLong_53() { return &___XsUnsignedLong_53; }
	inline void set_XsUnsignedLong_53(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsUnsignedLong_53 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedLong_53), value);
	}

	inline static int32_t get_offset_of_XsUnsignedInt_54() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsUnsignedInt_54)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsUnsignedInt_54() const { return ___XsUnsignedInt_54; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsUnsignedInt_54() { return &___XsUnsignedInt_54; }
	inline void set_XsUnsignedInt_54(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsUnsignedInt_54 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedInt_54), value);
	}

	inline static int32_t get_offset_of_XsUnsignedShort_55() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsUnsignedShort_55)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsUnsignedShort_55() const { return ___XsUnsignedShort_55; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsUnsignedShort_55() { return &___XsUnsignedShort_55; }
	inline void set_XsUnsignedShort_55(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsUnsignedShort_55 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedShort_55), value);
	}

	inline static int32_t get_offset_of_XsUnsignedByte_56() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsUnsignedByte_56)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsUnsignedByte_56() const { return ___XsUnsignedByte_56; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsUnsignedByte_56() { return &___XsUnsignedByte_56; }
	inline void set_XsUnsignedByte_56(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsUnsignedByte_56 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedByte_56), value);
	}

	inline static int32_t get_offset_of_XsPositiveInteger_57() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XsPositiveInteger_57)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XsPositiveInteger_57() const { return ___XsPositiveInteger_57; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XsPositiveInteger_57() { return &___XsPositiveInteger_57; }
	inline void set_XsPositiveInteger_57(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XsPositiveInteger_57 = value;
		Il2CppCodeGenWriteBarrier((&___XsPositiveInteger_57), value);
	}

	inline static int32_t get_offset_of_XdtUntypedAtomic_58() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XdtUntypedAtomic_58)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XdtUntypedAtomic_58() const { return ___XdtUntypedAtomic_58; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XdtUntypedAtomic_58() { return &___XdtUntypedAtomic_58; }
	inline void set_XdtUntypedAtomic_58(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XdtUntypedAtomic_58 = value;
		Il2CppCodeGenWriteBarrier((&___XdtUntypedAtomic_58), value);
	}

	inline static int32_t get_offset_of_XdtAnyAtomicType_59() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XdtAnyAtomicType_59)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XdtAnyAtomicType_59() const { return ___XdtAnyAtomicType_59; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XdtAnyAtomicType_59() { return &___XdtAnyAtomicType_59; }
	inline void set_XdtAnyAtomicType_59(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XdtAnyAtomicType_59 = value;
		Il2CppCodeGenWriteBarrier((&___XdtAnyAtomicType_59), value);
	}

	inline static int32_t get_offset_of_XdtYearMonthDuration_60() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XdtYearMonthDuration_60)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XdtYearMonthDuration_60() const { return ___XdtYearMonthDuration_60; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XdtYearMonthDuration_60() { return &___XdtYearMonthDuration_60; }
	inline void set_XdtYearMonthDuration_60(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XdtYearMonthDuration_60 = value;
		Il2CppCodeGenWriteBarrier((&___XdtYearMonthDuration_60), value);
	}

	inline static int32_t get_offset_of_XdtDayTimeDuration_61() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3397264039_StaticFields, ___XdtDayTimeDuration_61)); }
	inline XmlSchemaSimpleType_t3397264039 * get_XdtDayTimeDuration_61() const { return ___XdtDayTimeDuration_61; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_XdtDayTimeDuration_61() { return &___XdtDayTimeDuration_61; }
	inline void set_XdtDayTimeDuration_61(XmlSchemaSimpleType_t3397264039 * value)
	{
		___XdtDayTimeDuration_61 = value;
		Il2CppCodeGenWriteBarrier((&___XdtDayTimeDuration_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPE_T3397264039_H
#ifndef XMLSCHEMASIMPLETYPELIST_T3718422826_H
#define XMLSCHEMASIMPLETYPELIST_T3718422826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct  XmlSchemaSimpleTypeList_t3718422826  : public XmlSchemaSimpleTypeContent_t1082177942
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::itemType
	XmlSchemaSimpleType_t3397264039 * ___itemType_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeList::itemTypeName
	XmlQualifiedName_t740477960 * ___itemTypeName_4;

public:
	inline static int32_t get_offset_of_itemType_3() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t3718422826, ___itemType_3)); }
	inline XmlSchemaSimpleType_t3397264039 * get_itemType_3() const { return ___itemType_3; }
	inline XmlSchemaSimpleType_t3397264039 ** get_address_of_itemType_3() { return &___itemType_3; }
	inline void set_itemType_3(XmlSchemaSimpleType_t3397264039 * value)
	{
		___itemType_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_3), value);
	}

	inline static int32_t get_offset_of_itemTypeName_4() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t3718422826, ___itemTypeName_4)); }
	inline XmlQualifiedName_t740477960 * get_itemTypeName_4() const { return ___itemTypeName_4; }
	inline XmlQualifiedName_t740477960 ** get_address_of_itemTypeName_4() { return &___itemTypeName_4; }
	inline void set_itemTypeName_4(XmlQualifiedName_t740477960 * value)
	{
		___itemTypeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemTypeName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPELIST_T3718422826_H
#ifndef XSDGYEARMONTH_T240306832_H
#define XSDGYEARMONTH_T240306832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYearMonth
struct  XsdGYearMonth_t240306832  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEARMONTH_T240306832_H
#ifndef XSDHEXBINARY_T1905470603_H
#define XSDHEXBINARY_T1905470603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdHexBinary
struct  XsdHexBinary_t1905470603  : public XsdAnySimpleType_t1708784850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDHEXBINARY_T1905470603_H
#ifndef XSDINTEGER_T3891685094_H
#define XSDINTEGER_T3891685094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_t3891685094  : public XsdDecimal_t950276334
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINTEGER_T3891685094_H
#ifndef XSDBASE64BINARY_T1240435854_H
#define XSDBASE64BINARY_T1240435854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t1240435854  : public XsdString_t874272089
{
public:

public:
};

struct XsdBase64Binary_t1240435854_StaticFields
{
public:
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t434619169* ___decodeTable_62;

public:
	inline static int32_t get_offset_of_ALPHABET_61() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t1240435854_StaticFields, ___ALPHABET_61)); }
	inline String_t* get_ALPHABET_61() const { return ___ALPHABET_61; }
	inline String_t** get_address_of_ALPHABET_61() { return &___ALPHABET_61; }
	inline void set_ALPHABET_61(String_t* value)
	{
		___ALPHABET_61 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_61), value);
	}

	inline static int32_t get_offset_of_decodeTable_62() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t1240435854_StaticFields, ___decodeTable_62)); }
	inline ByteU5BU5D_t434619169* get_decodeTable_62() const { return ___decodeTable_62; }
	inline ByteU5BU5D_t434619169** get_address_of_decodeTable_62() { return &___decodeTable_62; }
	inline void set_decodeTable_62(ByteU5BU5D_t434619169* value)
	{
		___decodeTable_62 = value;
		Il2CppCodeGenWriteBarrier((&___decodeTable_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBASE64BINARY_T1240435854_H
#ifndef XSDANYURI_T232015842_H
#define XSDANYURI_T232015842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyURI
struct  XsdAnyURI_t232015842  : public XsdString_t874272089
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYURI_T232015842_H
#ifndef XDTYEARMONTHDURATION_T1060067746_H
#define XDTYEARMONTHDURATION_T1060067746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtYearMonthDuration
struct  XdtYearMonthDuration_t1060067746  : public XsdDuration_t3025817362
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTYEARMONTHDURATION_T1060067746_H
#ifndef XSDNORMALIZEDSTRING_T4261832402_H
#define XSDNORMALIZEDSTRING_T4261832402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_t4261832402  : public XsdString_t874272089
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_T4261832402_H
#ifndef XDTDAYTIMEDURATION_T2375956300_H
#define XDTDAYTIMEDURATION_T2375956300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtDayTimeDuration
struct  XdtDayTimeDuration_t2375956300  : public XsdDuration_t3025817362
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTDAYTIMEDURATION_T2375956300_H
#ifndef XSDNONPOSITIVEINTEGER_T206124302_H
#define XSDNONPOSITIVEINTEGER_T206124302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonPositiveInteger
struct  XsdNonPositiveInteger_t206124302  : public XsdInteger_t3891685094
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONPOSITIVEINTEGER_T206124302_H
#ifndef XSDNONNEGATIVEINTEGER_T1188126083_H
#define XSDNONNEGATIVEINTEGER_T1188126083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonNegativeInteger
struct  XsdNonNegativeInteger_t1188126083  : public XsdInteger_t3891685094
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONNEGATIVEINTEGER_T1188126083_H
#ifndef XSDLONG_T1196606063_H
#define XSDLONG_T1196606063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t1196606063  : public XsdInteger_t3891685094
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLONG_T1196606063_H
#ifndef XSDTOKEN_T1018821129_H
#define XSDTOKEN_T1018821129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t1018821129  : public XsdNormalizedString_t4261832402
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T1018821129_H
#ifndef XSDNEGATIVEINTEGER_T535826610_H
#define XSDNEGATIVEINTEGER_T535826610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t535826610  : public XsdNonPositiveInteger_t206124302
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNEGATIVEINTEGER_T535826610_H
#ifndef XSDPOSITIVEINTEGER_T3166100678_H
#define XSDPOSITIVEINTEGER_T3166100678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdPositiveInteger
struct  XsdPositiveInteger_t3166100678  : public XsdNonNegativeInteger_t1188126083
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPOSITIVEINTEGER_T3166100678_H
#ifndef XSDINT_T2543007977_H
#define XSDINT_T2543007977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInt
struct  XsdInt_t2543007977  : public XsdLong_t1196606063
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINT_T2543007977_H
#ifndef XSDNAME_T3987376123_H
#define XSDNAME_T3987376123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t3987376123  : public XsdToken_t1018821129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T3987376123_H
#ifndef XSDUNSIGNEDLONG_T3741596672_H
#define XSDUNSIGNEDLONG_T3741596672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t3741596672  : public XsdNonNegativeInteger_t1188126083
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDLONG_T3741596672_H
#ifndef XSDUNSIGNEDINT_T3230006419_H
#define XSDUNSIGNEDINT_T3230006419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t3230006419  : public XsdUnsignedLong_t3741596672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDINT_T3230006419_H
#ifndef XSDQNAME_T461718195_H
#define XSDQNAME_T461718195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t461718195  : public XsdName_t3987376123
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDQNAME_T461718195_H
#ifndef XSDSHORT_T1409564899_H
#define XSDSHORT_T1409564899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdShort
struct  XsdShort_t1409564899  : public XsdInt_t2543007977
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSHORT_T1409564899_H
#ifndef XSDUNSIGNEDSHORT_T2292208357_H
#define XSDUNSIGNEDSHORT_T2292208357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_t2292208357  : public XsdUnsignedInt_t3230006419
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDSHORT_T2292208357_H
#ifndef XSDBYTE_T1345023881_H
#define XSDBYTE_T1345023881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdByte
struct  XsdByte_t1345023881  : public XsdShort_t1409564899
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBYTE_T1345023881_H
#ifndef XSDUNSIGNEDBYTE_T3062058043_H
#define XSDUNSIGNEDBYTE_T3062058043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_t3062058043  : public XsdUnsignedShort_t2292208357
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDBYTE_T3062058043_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (XsdDecimal_t950276334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (XsdInteger_t3891685094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (XsdLong_t1196606063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (XsdInt_t2543007977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (XsdShort_t1409564899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (XsdByte_t1345023881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (XsdNonNegativeInteger_t1188126083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (XsdUnsignedLong_t3741596672), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (XsdUnsignedInt_t3230006419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (XsdUnsignedShort_t2292208357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (XsdUnsignedByte_t3062058043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (XsdPositiveInteger_t3166100678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (XsdNonPositiveInteger_t206124302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (XsdNegativeInteger_t535826610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (XsdFloat_t1847923358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (XsdDouble_t1419197403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (XsdBase64Binary_t1240435854), -1, sizeof(XsdBase64Binary_t1240435854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	XsdBase64Binary_t1240435854_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1240435854_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (XsdHexBinary_t1905470603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (XsdQName_t461718195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (XsdBoolean_t1271651578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (XsdAnyURI_t232015842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (XsdDuration_t3025817362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (XdtDayTimeDuration_t2375956300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (XdtYearMonthDuration_t1060067746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (XsdDateTime_t2592875340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (XsdDate_t2932582003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (XsdTime_t3734795353), -1, sizeof(XsdTime_t3734795353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	XsdTime_t3734795353_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (XsdGYearMonth_t240306832), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (XsdGMonthDay_t960257253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (XsdGYear_t4242852660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (XsdGMonth_t2084261129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (XsdGDay_t1441225556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (XmlSchemaAnnotated_t2235692007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (XmlSchemaAttribute_t803373732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (XmlSchemaCompilationSettings_t1130471374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	XmlSchemaCompilationSettings_t1130471374::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (XmlSchemaDatatype_t4041842875), -1, sizeof(XmlSchemaDatatype_t4041842875_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[55] = 
{
	XmlSchemaDatatype_t4041842875::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t4041842875::get_offset_of_sb_2(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t4041842875_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (XmlSchemaDerivationMethod_t1876323326)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2037[9] = 
{
	XmlSchemaDerivationMethod_t1876323326::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (XmlSchemaElement_t2744860620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (XmlSchemaFacet_t2775705363), -1, sizeof(XmlSchemaFacet_t2775705363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	XmlSchemaFacet_t2775705363_StaticFields::get_offset_of_AllFacets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Facet_t4059729603)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[14] = 
{
	Facet_t4059729603::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (XmlSchemaInfo_t933726380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[7] = 
{
	XmlSchemaInfo_t933726380::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t933726380::get_offset_of_isNil_1(),
	XmlSchemaInfo_t933726380::get_offset_of_memberType_2(),
	XmlSchemaInfo_t933726380::get_offset_of_attr_3(),
	XmlSchemaInfo_t933726380::get_offset_of_elem_4(),
	XmlSchemaInfo_t933726380::get_offset_of_type_5(),
	XmlSchemaInfo_t933726380::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (XmlSchemaObject_t4233170947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[3] = 
{
	XmlSchemaObject_t4233170947::get_offset_of_namespaces_0(),
	XmlSchemaObject_t4233170947::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t4233170947::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (XmlSchemaParticle_t110409631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (XmlSchemaSet_t93272820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[5] = 
{
	XmlSchemaSet_t93272820::get_offset_of_nameTable_0(),
	XmlSchemaSet_t93272820::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t93272820::get_offset_of_schemas_2(),
	XmlSchemaSet_t93272820::get_offset_of_settings_3(),
	XmlSchemaSet_t93272820::get_offset_of_CompilationId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (XmlSchemaSimpleType_t3397264039), -1, sizeof(XmlSchemaSimpleType_t3397264039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[53] = 
{
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t3397264039::get_offset_of_content_10(),
	XmlSchemaSimpleType_t3397264039::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t3397264039::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t3397264039_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (XmlSchemaSimpleTypeContent_t1082177942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (XmlSchemaSimpleTypeList_t3718422826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[2] = 
{
	XmlSchemaSimpleTypeList_t3718422826::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t3718422826::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (XmlSchemaSimpleTypeRestriction_t1836394642), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2048[2] = 
{
	XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t1836394642_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (XmlSchemaSimpleTypeUnion_t4070439509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (XmlSchemaType_t2926683629), -1, sizeof(XmlSchemaType_t2926683629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2050[6] = 
{
	XmlSchemaType_t2926683629::get_offset_of_final_3(),
	XmlSchemaType_t2926683629::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t2926683629::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t2926683629::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t2926683629_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t2926683629_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (XmlSchemaUtil_t4137508536), -1, sizeof(XmlSchemaUtil_t4137508536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[4] = 
{
	XmlSchemaUtil_t4137508536_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t4137508536_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t4137508536_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t4137508536_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (XmlSchemaValidity_t2542592596)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2052[4] = 
{
	XmlSchemaValidity_t2542592596::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (XmlAttributeAttribute_t3964240084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[1] = 
{
	XmlAttributeAttribute_t3964240084::get_offset_of_attributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (XmlElementAttribute_t1186985268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[3] = 
{
	XmlElementAttribute_t1186985268::get_offset_of_elementName_0(),
	XmlElementAttribute_t1186985268::get_offset_of_type_1(),
	XmlElementAttribute_t1186985268::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (XmlEnumAttribute_t1636689972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[1] = 
{
	XmlEnumAttribute_t1636689972::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (XmlIgnoreAttribute_t1537762200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (XmlSerializerNamespaces_t2934028315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	XmlSerializerNamespaces_t2934028315::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ConformanceLevel_t2038759208)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2059[4] = 
{
	ConformanceLevel_t2038759208::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (DTDAutomataFactory_t4266884469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[3] = 
{
	DTDAutomataFactory_t4266884469::get_offset_of_root_0(),
	DTDAutomataFactory_t4266884469::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t4266884469::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (DTDObjectModel_t3150902482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[19] = 
{
	DTDObjectModel_t3150902482::get_offset_of_factory_0(),
	DTDObjectModel_t3150902482::get_offset_of_elementDecls_1(),
	DTDObjectModel_t3150902482::get_offset_of_attListDecls_2(),
	DTDObjectModel_t3150902482::get_offset_of_peDecls_3(),
	DTDObjectModel_t3150902482::get_offset_of_entityDecls_4(),
	DTDObjectModel_t3150902482::get_offset_of_notationDecls_5(),
	DTDObjectModel_t3150902482::get_offset_of_validationErrors_6(),
	DTDObjectModel_t3150902482::get_offset_of_resolver_7(),
	DTDObjectModel_t3150902482::get_offset_of_nameTable_8(),
	DTDObjectModel_t3150902482::get_offset_of_externalResources_9(),
	DTDObjectModel_t3150902482::get_offset_of_baseURI_10(),
	DTDObjectModel_t3150902482::get_offset_of_name_11(),
	DTDObjectModel_t3150902482::get_offset_of_publicId_12(),
	DTDObjectModel_t3150902482::get_offset_of_systemId_13(),
	DTDObjectModel_t3150902482::get_offset_of_intSubset_14(),
	DTDObjectModel_t3150902482::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t3150902482::get_offset_of_isStandalone_16(),
	DTDObjectModel_t3150902482::get_offset_of_lineNumber_17(),
	DTDObjectModel_t3150902482::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (DictionaryBase_t4072949440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (U3CU3Ec__Iterator3_t1821397857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[5] = 
{
	U3CU3Ec__Iterator3_t1821397857::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t1821397857::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t1821397857::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t1821397857::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t1821397857::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (DTDCollectionBase_t403511423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[1] = 
{
	DTDCollectionBase_t403511423::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (DTDElementDeclarationCollection_t3634927218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (DTDAttListDeclarationCollection_t748347165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (DTDEntityDeclarationCollection_t2395926230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (DTDNotationDeclarationCollection_t2979384550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (DTDContentModel_t2841412075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[6] = 
{
	DTDContentModel_t2841412075::get_offset_of_root_5(),
	DTDContentModel_t2841412075::get_offset_of_ownerElementName_6(),
	DTDContentModel_t2841412075::get_offset_of_elementName_7(),
	DTDContentModel_t2841412075::get_offset_of_orderType_8(),
	DTDContentModel_t2841412075::get_offset_of_childModels_9(),
	DTDContentModel_t2841412075::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (DTDContentModelCollection_t133429609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[1] = 
{
	DTDContentModelCollection_t133429609::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (DTDNode_t3993235537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[5] = 
{
	DTDNode_t3993235537::get_offset_of_root_0(),
	DTDNode_t3993235537::get_offset_of_isInternalSubset_1(),
	DTDNode_t3993235537::get_offset_of_baseURI_2(),
	DTDNode_t3993235537::get_offset_of_lineNumber_3(),
	DTDNode_t3993235537::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (DTDElementDeclaration_t3064560604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[6] = 
{
	DTDElementDeclaration_t3064560604::get_offset_of_root_5(),
	DTDElementDeclaration_t3064560604::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t3064560604::get_offset_of_name_7(),
	DTDElementDeclaration_t3064560604::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t3064560604::get_offset_of_isAny_9(),
	DTDElementDeclaration_t3064560604::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (DTDAttributeDefinition_t1581906730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[4] = 
{
	DTDAttributeDefinition_t1581906730::get_offset_of_name_5(),
	DTDAttributeDefinition_t1581906730::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t1581906730::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t1581906730::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (DTDAttListDeclaration_t2301331120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[3] = 
{
	DTDAttListDeclaration_t2301331120::get_offset_of_name_5(),
	DTDAttListDeclaration_t2301331120::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2301331120::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (DTDEntityBase_t3196470699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[10] = 
{
	DTDEntityBase_t3196470699::get_offset_of_name_5(),
	DTDEntityBase_t3196470699::get_offset_of_publicId_6(),
	DTDEntityBase_t3196470699::get_offset_of_systemId_7(),
	DTDEntityBase_t3196470699::get_offset_of_literalValue_8(),
	DTDEntityBase_t3196470699::get_offset_of_replacementText_9(),
	DTDEntityBase_t3196470699::get_offset_of_uriString_10(),
	DTDEntityBase_t3196470699::get_offset_of_absUri_11(),
	DTDEntityBase_t3196470699::get_offset_of_isInvalid_12(),
	DTDEntityBase_t3196470699::get_offset_of_loadFailed_13(),
	DTDEntityBase_t3196470699::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (DTDEntityDeclaration_t2202458961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[6] = 
{
	DTDEntityDeclaration_t2202458961::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t2202458961::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t2202458961::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t2202458961::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t2202458961::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t2202458961::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (DTDNotationDeclaration_t1773003655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	DTDNotationDeclaration_t1773003655::get_offset_of_name_5(),
	DTDNotationDeclaration_t1773003655::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1773003655::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1773003655::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1773003655::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (DTDParameterEntityDeclarationCollection_t1551549928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[2] = 
{
	DTDParameterEntityDeclarationCollection_t1551549928::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t1551549928::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (DTDParameterEntityDeclaration_t3047415913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (DTDContentOrderType_t464626144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	DTDContentOrderType_t464626144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (DTDOccurence_t3579469009)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2081[5] = 
{
	DTDOccurence_t3579469009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (DTDReader_t1016753964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[14] = 
{
	DTDReader_t1016753964::get_offset_of_currentInput_0(),
	DTDReader_t1016753964::get_offset_of_parserInputStack_1(),
	DTDReader_t1016753964::get_offset_of_nameBuffer_2(),
	DTDReader_t1016753964::get_offset_of_nameLength_3(),
	DTDReader_t1016753964::get_offset_of_nameCapacity_4(),
	DTDReader_t1016753964::get_offset_of_valueBuffer_5(),
	DTDReader_t1016753964::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t1016753964::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t1016753964::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t1016753964::get_offset_of_normalization_9(),
	DTDReader_t1016753964::get_offset_of_processingInternalSubset_10(),
	DTDReader_t1016753964::get_offset_of_cachedPublicId_11(),
	DTDReader_t1016753964::get_offset_of_cachedSystemId_12(),
	DTDReader_t1016753964::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (EntityHandling_t3541905618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[3] = 
{
	EntityHandling_t3541905618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (NameTable_t3507653384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[3] = 
{
	NameTable_t3507653384::get_offset_of_count_0(),
	NameTable_t3507653384::get_offset_of_buckets_1(),
	NameTable_t3507653384::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (Entry_t2245712786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[4] = 
{
	Entry_t2245712786::get_offset_of_str_0(),
	Entry_t2245712786::get_offset_of_hash_1(),
	Entry_t2245712786::get_offset_of_len_2(),
	Entry_t2245712786::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (NamespaceHandling_t2739939700)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2089[3] = 
{
	NamespaceHandling_t2739939700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (NewLineHandling_t3095395276)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2090[4] = 
{
	NewLineHandling_t3095395276::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (ReadState_t3065260582)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2091[6] = 
{
	ReadState_t3065260582::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (WhitespaceHandling_t2116368771)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[4] = 
{
	WhitespaceHandling_t2116368771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (WriteState_t899266867)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[8] = 
{
	WriteState_t899266867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (XmlAttribute_t3667600658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[4] = 
{
	XmlAttribute_t3667600658::get_offset_of_name_5(),
	XmlAttribute_t3667600658::get_offset_of_isDefault_6(),
	XmlAttribute_t3667600658::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t3667600658::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (XmlAttributeCollection_t3528594990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[2] = 
{
	XmlAttributeCollection_t3528594990::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3528594990::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (XmlCDataSection_t3479907102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (XmlChar_t2364868096), -1, sizeof(XmlChar_t2364868096_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	XmlChar_t2364868096_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t2364868096_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t2364868096_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t2364868096_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t2364868096_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (XmlCharacterData_t4217070933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	XmlCharacterData_t4217070933::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (XmlComment_t434142218), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
