﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Camera
struct Camera_t2839736942;
// UnityEngine.Component
struct Component_t1632713610;
// UnityEngine.GUILayer
struct GUILayer_t478658086;
// UnityEngine.Object
struct Object_t692178351;
// UnityEngine.GUIElement
struct GUIElement_t2946029536;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// UnityEngine.Camera[]
struct CameraU5BU5D_t4045078555;
// UnityEngine.RenderTexture
struct RenderTexture_t971269558;
// System.String
struct String_t;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t333733317;
// System.Attribute
struct Attribute_t1924466020;
// UnityEngine.SerializeField
struct SerializeField_t3205734915;
// System.Collections.IEnumerator
struct IEnumerator_t3472601659;
// System.ArgumentException
struct ArgumentException_t1812645948;
// System.Type
struct Type_t;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t3838534887;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t381499487;
// UnityEngine.Sprite
struct Sprite_t2544745708;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// System.Diagnostics.StackTrace
struct StackTrace_t4180877726;
// System.Text.StringBuilder
struct StringBuilder_t622404039;
// System.Exception
struct Exception_t4086964929;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3943516840;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t909267116;
// UnityEngine.TextAsset
struct TextAsset_t2052027493;
// System.Byte[]
struct ByteU5BU5D_t434619169;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.Color[]
struct ColorU5BU5D_t247935167;
// UnityEngine.Texture3D
struct Texture3D_t2628067514;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t878041766;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t2025437004;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t1357543767;
// UnityEngine.TrackedReference
struct TrackedReference_t2624196464;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityEngine.Transform/Enumerator
struct Enumerator_t261617688;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t3400447372;
// System.Action`1<System.Object>
struct Action_1_t4119957313;
// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct RequestAtlasCallback_t1427750696;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t1129391324;
// System.IAsyncResult
struct IAsyncResult_t454385990;
// System.AsyncCallback
struct AsyncCallback_t3561663063;
// System.AppDomain
struct AppDomain_t3842411093;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t3749840881;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t1939597804;
// UnityEngine.UnityException
struct UnityException_t1933799526;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t259346668;
// UnityEngine.UnityLogWriter
struct UnityLogWriter_t1450454900;
// System.IO.TextWriter
struct TextWriter_t997444721;
// System.Text.Encoding
struct Encoding_t4004889433;
// System.Object[]
struct ObjectU5BU5D_t2737604620;
// UnityEngine.UnitySynchronizationContext
struct UnitySynchronizationContext_t485758571;
// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct Queue_1_t1480213181;
// System.Threading.Thread
struct Thread_t322231692;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t2274409467;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t2349539412;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t891147241;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t2909334802;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1056898812;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t4012476583;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t4272504447;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t3353236027;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t2273300952;
// UnityEngine.WritableAttribute
struct WritableAttribute_t482114951;
// UnityEngineInternal.GenericStack
struct GenericStack_t2697154139;
// System.Collections.Stack
struct Stack_t535311253;
// System.Delegate
struct Delegate_t3882343965;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t2902006326;
// System.Globalization.CultureInfo
struct CultureInfo_t2625678720;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t674921444;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2586599646;
// System.Globalization.TextInfo
struct TextInfo_t441345186;
// System.Globalization.CompareInfo
struct CompareInfo_t2505958045;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1804543921;
// System.Globalization.Calendar
struct Calendar_t1225199120;
// System.Collections.Hashtable
struct Hashtable_t2354558714;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// System.Int32
struct Int32_t972567508;
// System.Void
struct Void_t653366341;
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t3571349128;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Reflection.MethodBase
struct MethodBase_t2927642150;
// UnityEngine.UnitySynchronizationContext/WorkRequest[]
struct WorkRequestU5BU5D_t2656204507;
// System.Text.DecoderFallback
struct DecoderFallback_t1140074740;
// System.Text.EncoderFallback
struct EncoderFallback_t1057161456;
// System.Reflection.Assembly
struct Assembly_t3018166672;
// System.IFormatProvider
struct IFormatProvider_t1109252446;
// System.Collections.ArrayList
struct ArrayList_t4250946984;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t4119529693;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t3687691741;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t2568675203;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t504895258;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// System.Byte
struct Byte_t131097440;
// System.Double
struct Double_t3752657471;
// System.UInt16
struct UInt16_t14172355;
// System.DelegateData
struct DelegateData_t1549710636;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t1831156579;
// System.Type[]
struct TypeU5BU5D_t1985992169;
// System.Reflection.MemberFilter
struct MemberFilter_t2357198184;
// System.Security.Policy.Evidence
struct Evidence_t1158797702;
// System.Security.PermissionSet
struct PermissionSet_t4252355126;
// System.Security.Principal.IPrincipal
struct IPrincipal_t354029265;
// System.AppDomainManager
struct AppDomainManager_t3873064184;
// System.ActivationContext
struct ActivationContext_t1493131857;
// System.ApplicationIdentity
struct ApplicationIdentity_t1276138697;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t3835039474;
// System.ResolveEventHandler
struct ResolveEventHandler_t108319945;
// System.EventHandler
struct EventHandler_t369332490;
// System.Threading.ExecutionContext
struct ExecutionContext_t935670543;
// System.MulticastDelegate
struct MulticastDelegate_t1652681189;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t2394797874;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t657915608;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2209604263;

extern RuntimeClass* SendMouseEvents_t2768135016_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m2678903946_MetadataUsageId;
extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisGUILayer_t478658086_m1987472637_RuntimeMethod_var;
extern const uint32_t SendMouseEvents_HitTestLegacyGUI_m267751607_MetadataUsageId;
extern RuntimeClass* Input_t1735245659_il2cpp_TypeInfo_var;
extern RuntimeClass* CameraU5BU5D_t4045078555_il2cpp_TypeInfo_var;
extern RuntimeClass* HitInfo_t1469516070_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t2228218122_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m2533818309_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral170126824;
extern Il2CppCodeGenString* _stringLiteral299533630;
extern Il2CppCodeGenString* _stringLiteral1510456461;
extern Il2CppCodeGenString* _stringLiteral4276975187;
extern Il2CppCodeGenString* _stringLiteral1846547262;
extern Il2CppCodeGenString* _stringLiteral2958471445;
extern Il2CppCodeGenString* _stringLiteral1804740865;
extern const uint32_t SendMouseEvents_SendEvents_m814760037_MetadataUsageId;
extern RuntimeClass* HitInfoU5BU5D_t2568675203_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m4006956103_MetadataUsageId;
extern const uint32_t HitInfo_op_Implicit_m3774618170_MetadataUsageId;
extern const uint32_t HitInfo_Compare_m3678827560_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t1812645948_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t3472601659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3761283446;
extern Il2CppCodeGenString* _stringLiteral1363212644;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m2917246636_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m2001778250_MetadataUsageId;
extern const uint32_t Shader__ctor_m2013031543_MetadataUsageId;
extern const uint32_t Sprite__ctor_m78442145_MetadataUsageId;
extern RuntimeClass* Vector4_t2436299922_il2cpp_TypeInfo_var;
extern const uint32_t Sprite_Create_m1384549182_MetadataUsageId;
extern RuntimeClass* StackTraceUtility_t931691098_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2574419025;
extern Il2CppCodeGenString* _stringLiteral2278905470;
extern const uint32_t StackTraceUtility_SetProjectFolder_m3440359700_MetadataUsageId;
extern RuntimeClass* StackTrace_t4180877726_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m3667880611_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2414275388;
extern Il2CppCodeGenString* _stringLiteral1319789206;
extern Il2CppCodeGenString* _stringLiteral2850377577;
extern Il2CppCodeGenString* _stringLiteral2822882624;
extern Il2CppCodeGenString* _stringLiteral1204840630;
extern Il2CppCodeGenString* _stringLiteral195602857;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m1261986193_MetadataUsageId;
extern RuntimeClass* Exception_t4086964929_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t622404039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3407105065;
extern Il2CppCodeGenString* _stringLiteral2418974296;
extern Il2CppCodeGenString* _stringLiteral4185710657;
extern Il2CppCodeGenString* _stringLiteral415700502;
extern Il2CppCodeGenString* _stringLiteral159996285;
extern Il2CppCodeGenString* _stringLiteral1169143675;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m1202155996_MetadataUsageId;
extern RuntimeClass* CharU5BU5D_t3419619864_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4242007556;
extern Il2CppCodeGenString* _stringLiteral3302944042;
extern Il2CppCodeGenString* _stringLiteral2874061849;
extern Il2CppCodeGenString* _stringLiteral4102960008;
extern Il2CppCodeGenString* _stringLiteral3019173685;
extern Il2CppCodeGenString* _stringLiteral3205397905;
extern Il2CppCodeGenString* _stringLiteral2582683053;
extern Il2CppCodeGenString* _stringLiteral890430475;
extern Il2CppCodeGenString* _stringLiteral2906454100;
extern Il2CppCodeGenString* _stringLiteral2931583284;
extern Il2CppCodeGenString* _stringLiteral294405094;
extern Il2CppCodeGenString* _stringLiteral4110169122;
extern Il2CppCodeGenString* _stringLiteral1050932311;
extern Il2CppCodeGenString* _stringLiteral2908764213;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m1447856313_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2957364442;
extern Il2CppCodeGenString* _stringLiteral2284701596;
extern Il2CppCodeGenString* _stringLiteral4284158284;
extern Il2CppCodeGenString* _stringLiteral591250;
extern Il2CppCodeGenString* _stringLiteral3992975604;
extern Il2CppCodeGenString* _stringLiteral351502530;
extern Il2CppCodeGenString* _stringLiteral2206330720;
extern Il2CppCodeGenString* _stringLiteral432646105;
extern Il2CppCodeGenString* _stringLiteral3991562125;
extern Il2CppCodeGenString* _stringLiteral1060462586;
extern Il2CppCodeGenString* _stringLiteral972445331;
extern Il2CppCodeGenString* _stringLiteral1760345480;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m4260390550_MetadataUsageId;
extern const uint32_t StackTraceUtility__cctor_m58538887_MetadataUsageId;
extern const uint32_t Texture__ctor_m373917183_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3391388676;
extern const uint32_t Texture_set_width_m867714560_MetadataUsageId;
extern const uint32_t Texture_set_height_m475457339_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m1290530517_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m2529229425_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m1639172559_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchScreenKeyboardType_t1971784194_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2280471388_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m3418144028_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m2953139382_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m1445827200_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m1243226021_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_t1357543767_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m1341367847_MetadataUsageId;
extern const uint32_t TrackedReference_op_Equality_m2981311363_MetadataUsageId;
extern RuntimeClass* TrackedReference_t2624196464_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m3282386582_MetadataUsageId;
extern RuntimeClass* Quaternion_t704191599_il2cpp_TypeInfo_var;
extern const uint32_t Transform_set_eulerAngles_m266161425_MetadataUsageId;
extern const uint32_t Transform_set_localEulerAngles_m272559017_MetadataUsageId;
extern RuntimeClass* Vector3_t1986933152_il2cpp_TypeInfo_var;
extern const uint32_t Transform_get_right_m4293898467_MetadataUsageId;
extern const uint32_t Transform_set_right_m2128067954_MetadataUsageId;
extern const uint32_t Transform_get_up_m3741264314_MetadataUsageId;
extern const uint32_t Transform_get_forward_m3174539474_MetadataUsageId;
extern RuntimeClass* RectTransform_t859616204_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3419099923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3127190363;
extern const uint32_t Transform_set_parent_m3838886759_MetadataUsageId;
extern const uint32_t Transform_Translate_m219233999_MetadataUsageId;
extern const uint32_t Transform_Rotate_m4266981785_MetadataUsageId;
extern const uint32_t Transform_RotateAround_m3597956458_MetadataUsageId;
extern const uint32_t Transform_LookAt_m1576954949_MetadataUsageId;
extern const uint32_t Transform_LookAt_m169160536_MetadataUsageId;
extern const uint32_t Transform_LookAt_m483134985_MetadataUsageId;
extern RuntimeClass* Enumerator_t261617688_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m3479571372_MetadataUsageId;
extern RuntimeClass* SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t3400447372_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SpriteAtlasManager_Register_m1909933228_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m2452183723_RuntimeMethod_var;
extern const uint32_t SpriteAtlasManager_RequestAtlas_m3877826629_MetadataUsageId;
extern const uint32_t SpriteAtlasManager__cctor_m2900083981_MetadataUsageId;
extern RuntimeClass* UnhandledExceptionHandler_t4175730415_il2cpp_TypeInfo_var;
extern RuntimeClass* UnhandledExceptionEventHandler_t3749840881_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnhandledExceptionHandler_HandleUnhandledException_m3220153098_RuntimeMethod_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m3071828837_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2212345298;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m3220153098_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1675908104;
extern const uint32_t UnhandledExceptionHandler_PrintException_m72583591_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2610399101;
extern const uint32_t UnityException__ctor_m3060899425_MetadataUsageId;
extern RuntimeClass* TextWriter_t997444721_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter__ctor_m3298642088_MetadataUsageId;
extern RuntimeClass* UnityLogWriter_t1450454900_il2cpp_TypeInfo_var;
extern RuntimeClass* Console_t910768770_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter_Init_m532212392_MetadataUsageId;
extern RuntimeClass* Encoding_t4004889433_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter_get_Encoding_m3793726011_MetadataUsageId;
extern const uint32_t UnityString_Format_m3950186024_MetadataUsageId;
extern RuntimeClass* Queue_1_t1480213181_il2cpp_TypeInfo_var;
extern RuntimeClass* Thread_t322231692_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Queue_1__ctor_m528467501_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext__ctor_m3918494648_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Dequeue_m1469166360_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_m1367379469_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext_Exec_m2533712166_MetadataUsageId;
extern RuntimeClass* UnitySynchronizationContext_t485758571_il2cpp_TypeInfo_var;
extern const uint32_t UnitySynchronizationContext_InitializeSynchronizationContext_m911367017_MetadataUsageId;
extern const uint32_t UnitySynchronizationContext_ExecuteTasks_m1569520204_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral442860306;
extern const uint32_t Vector2_get_Item_m1634634565_MetadataUsageId;
extern const uint32_t Vector2_set_Item_m2125498180_MetadataUsageId;
extern const uint32_t Vector2_Lerp_m2779999928_MetadataUsageId;
extern RuntimeClass* Vector2_t328513675_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Normalize_m2506338390_MetadataUsageId;
extern RuntimeClass* Single_t485236535_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral492600876;
extern const uint32_t Vector2_ToString_m493646592_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1564422252;
extern const uint32_t Vector2_ToString_m1956738200_MetadataUsageId;
extern const uint32_t Vector2_Equals_m3452260837_MetadataUsageId;
extern const uint32_t Vector2_get_magnitude_m3070475644_MetadataUsageId;
extern const uint32_t Vector2_Distance_m1293157661_MetadataUsageId;
extern const uint32_t Vector2_op_Equality_m3259744715_MetadataUsageId;
extern const uint32_t Vector2_op_Inequality_m663134504_MetadataUsageId;
extern const uint32_t Vector2_get_zero_m187309959_MetadataUsageId;
extern const uint32_t Vector2_get_one_m1113418147_MetadataUsageId;
extern const uint32_t Vector2_get_up_m1378828326_MetadataUsageId;
extern const uint32_t Vector2_get_right_m1410176964_MetadataUsageId;
extern const uint32_t Vector2__cctor_m1820088036_MetadataUsageId;
extern const uint32_t Vector3_Slerp_m1979061459_MetadataUsageId;
extern const uint32_t Vector3_Lerp_m3944708698_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3205361995;
extern const uint32_t Vector3_get_Item_m3834540881_MetadataUsageId;
extern const uint32_t Vector3_set_Item_m4263389426_MetadataUsageId;
extern const uint32_t Vector3_Equals_m3593875679_MetadataUsageId;
extern const uint32_t Vector3_Normalize_m2417917258_MetadataUsageId;
extern const uint32_t Vector3_Normalize_m3915644803_MetadataUsageId;
extern const uint32_t Vector3_get_normalized_m1493202271_MetadataUsageId;
extern const uint32_t Vector3_Angle_m1273527711_MetadataUsageId;
extern const uint32_t Vector3_Distance_m659926138_MetadataUsageId;
extern const uint32_t Vector3_ClampMagnitude_m42878694_MetadataUsageId;
extern const uint32_t Vector3_Magnitude_m1454878665_MetadataUsageId;
extern const uint32_t Vector3_get_magnitude_m1161157060_MetadataUsageId;
extern const uint32_t Vector3_Min_m3122780938_MetadataUsageId;
extern const uint32_t Vector3_Max_m664546017_MetadataUsageId;
extern const uint32_t Vector3_get_zero_m2755426107_MetadataUsageId;
extern const uint32_t Vector3_get_one_m3852861474_MetadataUsageId;
extern const uint32_t Vector3_get_forward_m953890947_MetadataUsageId;
extern const uint32_t Vector3_get_back_m4261998315_MetadataUsageId;
extern const uint32_t Vector3_get_up_m467978780_MetadataUsageId;
extern const uint32_t Vector3_get_down_m1884173742_MetadataUsageId;
extern const uint32_t Vector3_get_left_m3516331364_MetadataUsageId;
extern const uint32_t Vector3_get_right_m878387312_MetadataUsageId;
extern const uint32_t Vector3_op_Equality_m511918437_MetadataUsageId;
extern const uint32_t Vector3_op_Inequality_m2317070390_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1436783535;
extern const uint32_t Vector3_ToString_m3238424746_MetadataUsageId;
extern const uint32_t Vector3__cctor_m3955612181_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral237839674;
extern const uint32_t Vector4_get_Item_m3649974304_MetadataUsageId;
extern const uint32_t Vector4_set_Item_m3281778497_MetadataUsageId;
extern const uint32_t Vector4_Equals_m754502683_MetadataUsageId;
extern const uint32_t Vector4_Normalize_m4277572146_MetadataUsageId;
extern const uint32_t Vector4_get_normalized_m1792997498_MetadataUsageId;
extern const uint32_t Vector4_Magnitude_m3047559685_MetadataUsageId;
extern const uint32_t Vector4_get_sqrMagnitude_m2365501330_MetadataUsageId;
extern const uint32_t Vector4_get_zero_m1931160304_MetadataUsageId;
extern const uint32_t Vector4_get_one_m336856355_MetadataUsageId;
extern const uint32_t Vector4_op_Equality_m1687602531_MetadataUsageId;
extern const uint32_t Vector4_op_Inequality_m594636851_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral197058514;
extern const uint32_t Vector4_ToString_m2547591814_MetadataUsageId;
extern const uint32_t Vector4_SqrMagnitude_m4263254105_MetadataUsageId;
extern const uint32_t Vector4__cctor_m3704097946_MetadataUsageId;
extern RuntimeClass* MathfInternal_t3317739077_il2cpp_TypeInfo_var;
extern const uint32_t MathfInternal__cctor_m515788773_MetadataUsageId;
extern RuntimeClass* TypeInferenceRules_t1200137651_il2cpp_TypeInfo_var;
extern const uint32_t TypeInferenceRuleAttribute__ctor_m2054129398_MetadataUsageId;

struct CameraU5BU5D_t4045078555;
struct HitInfoU5BU5D_t2568675203;
struct ObjectU5BU5D_t2737604620;
struct ParameterModifierU5BU5D_t2531273172;
struct StringU5BU5D_t2511808107;
struct CharU5BU5D_t3419619864;
struct ParameterInfoU5BU5D_t3316460985;
struct ByteU5BU5D_t434619169;
struct ColorU5BU5D_t247935167;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef CULTUREINFO_T2625678720_H
#define CULTUREINFO_T2625678720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t2625678720  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t674921444 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2586599646 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t441345186 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t2505958045 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t1804543921* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t2625678720 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1225199120 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t434619169* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___numInfo_14)); }
	inline NumberFormatInfo_t674921444 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t674921444 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t674921444 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t2586599646 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t2586599646 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t2586599646 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___textInfo_16)); }
	inline TextInfo_t441345186 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t441345186 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t441345186 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___compareInfo_26)); }
	inline CompareInfo_t2505958045 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t2505958045 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t2505958045 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t1804543921* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t1804543921** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t1804543921* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___parent_culture_30)); }
	inline CultureInfo_t2625678720 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t2625678720 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t2625678720 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___calendar_32)); }
	inline Calendar_t1225199120 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t1225199120 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t1225199120 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t434619169* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t434619169** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t434619169* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t2625678720_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t2625678720 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t2354558714 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t2354558714 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t2625678720 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t2625678720 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t2625678720 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t2354558714 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t2354558714 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t2354558714 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t2354558714 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t2354558714 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t2354558714 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t2625678720_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T2625678720_H
#ifndef SCRIPTINGUTILS_T3064593586_H
#define SCRIPTINGUTILS_T3064593586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.ScriptingUtils
struct  ScriptingUtils_t3064593586  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGUTILS_T3064593586_H
#ifndef NETFXCOREEXTENSIONS_T865589517_H
#define NETFXCOREEXTENSIONS_T865589517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.NetFxCoreExtensions
struct  NetFxCoreExtensions_t865589517  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETFXCOREEXTENSIONS_T865589517_H
#ifndef STACK_T535311253_H
#define STACK_T535311253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Stack
struct  Stack_t535311253  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.Stack::contents
	ObjectU5BU5D_t2737604620* ___contents_0;
	// System.Int32 System.Collections.Stack::current
	int32_t ___current_1;
	// System.Int32 System.Collections.Stack::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Stack::capacity
	int32_t ___capacity_3;
	// System.Int32 System.Collections.Stack::modCount
	int32_t ___modCount_4;

public:
	inline static int32_t get_offset_of_contents_0() { return static_cast<int32_t>(offsetof(Stack_t535311253, ___contents_0)); }
	inline ObjectU5BU5D_t2737604620* get_contents_0() const { return ___contents_0; }
	inline ObjectU5BU5D_t2737604620** get_address_of_contents_0() { return &___contents_0; }
	inline void set_contents_0(ObjectU5BU5D_t2737604620* value)
	{
		___contents_0 = value;
		Il2CppCodeGenWriteBarrier((&___contents_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Stack_t535311253, ___current_1)); }
	inline int32_t get_current_1() const { return ___current_1; }
	inline int32_t* get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(int32_t value)
	{
		___current_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Stack_t535311253, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_capacity_3() { return static_cast<int32_t>(offsetof(Stack_t535311253, ___capacity_3)); }
	inline int32_t get_capacity_3() const { return ___capacity_3; }
	inline int32_t* get_address_of_capacity_3() { return &___capacity_3; }
	inline void set_capacity_3(int32_t value)
	{
		___capacity_3 = value;
	}

	inline static int32_t get_offset_of_modCount_4() { return static_cast<int32_t>(offsetof(Stack_t535311253, ___modCount_4)); }
	inline int32_t get_modCount_4() const { return ___modCount_4; }
	inline int32_t* get_address_of_modCount_4() { return &___modCount_4; }
	inline void set_modCount_4(int32_t value)
	{
		___modCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_T535311253_H
#ifndef DATAUTILITY_T2829810044_H
#define DATAUTILITY_T2829810044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprites.DataUtility
struct  DataUtility_t2829810044  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAUTILITY_T2829810044_H
#ifndef STACKTRACEUTILITY_T931691098_H
#define STACKTRACEUTILITY_T931691098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StackTraceUtility
struct  StackTraceUtility_t931691098  : public RuntimeObject
{
public:

public:
};

struct StackTraceUtility_t931691098_StaticFields
{
public:
	// System.String UnityEngine.StackTraceUtility::projectFolder
	String_t* ___projectFolder_0;

public:
	inline static int32_t get_offset_of_projectFolder_0() { return static_cast<int32_t>(offsetof(StackTraceUtility_t931691098_StaticFields, ___projectFolder_0)); }
	inline String_t* get_projectFolder_0() const { return ___projectFolder_0; }
	inline String_t** get_address_of_projectFolder_0() { return &___projectFolder_0; }
	inline void set_projectFolder_0(String_t* value)
	{
		___projectFolder_0 = value;
		Il2CppCodeGenWriteBarrier((&___projectFolder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACEUTILITY_T931691098_H
#ifndef STACKTRACE_T4180877726_H
#define STACKTRACE_T4180877726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackTrace
struct  StackTrace_t4180877726  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::frames
	StackFrameU5BU5D_t3571349128* ___frames_1;
	// System.Boolean System.Diagnostics.StackTrace::debug_info
	bool ___debug_info_2;

public:
	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(StackTrace_t4180877726, ___frames_1)); }
	inline StackFrameU5BU5D_t3571349128* get_frames_1() const { return ___frames_1; }
	inline StackFrameU5BU5D_t3571349128** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(StackFrameU5BU5D_t3571349128* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_debug_info_2() { return static_cast<int32_t>(offsetof(StackTrace_t4180877726, ___debug_info_2)); }
	inline bool get_debug_info_2() const { return ___debug_info_2; }
	inline bool* get_address_of_debug_info_2() { return &___debug_info_2; }
	inline void set_debug_info_2(bool value)
	{
		___debug_info_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACE_T4180877726_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef STRINGBUILDER_T622404039_H
#define STRINGBUILDER_T622404039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t622404039  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t622404039, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T622404039_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef BINDER_T1695596754_H
#define BINDER_T1695596754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t1695596754  : public RuntimeObject
{
public:

public:
};

struct Binder_t1695596754_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t1695596754 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t1695596754_StaticFields, ___default_binder_0)); }
	inline Binder_t1695596754 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t1695596754 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t1695596754 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T1695596754_H
#ifndef STACKFRAME_T1778166981_H
#define STACKFRAME_T1778166981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackFrame
struct  StackFrame_t1778166981  : public RuntimeObject
{
public:
	// System.Int32 System.Diagnostics.StackFrame::ilOffset
	int32_t ___ilOffset_1;
	// System.Int32 System.Diagnostics.StackFrame::nativeOffset
	int32_t ___nativeOffset_2;
	// System.Reflection.MethodBase System.Diagnostics.StackFrame::methodBase
	MethodBase_t2927642150 * ___methodBase_3;
	// System.String System.Diagnostics.StackFrame::fileName
	String_t* ___fileName_4;
	// System.Int32 System.Diagnostics.StackFrame::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 System.Diagnostics.StackFrame::columnNumber
	int32_t ___columnNumber_6;
	// System.String System.Diagnostics.StackFrame::internalMethodName
	String_t* ___internalMethodName_7;

public:
	inline static int32_t get_offset_of_ilOffset_1() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___ilOffset_1)); }
	inline int32_t get_ilOffset_1() const { return ___ilOffset_1; }
	inline int32_t* get_address_of_ilOffset_1() { return &___ilOffset_1; }
	inline void set_ilOffset_1(int32_t value)
	{
		___ilOffset_1 = value;
	}

	inline static int32_t get_offset_of_nativeOffset_2() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___nativeOffset_2)); }
	inline int32_t get_nativeOffset_2() const { return ___nativeOffset_2; }
	inline int32_t* get_address_of_nativeOffset_2() { return &___nativeOffset_2; }
	inline void set_nativeOffset_2(int32_t value)
	{
		___nativeOffset_2 = value;
	}

	inline static int32_t get_offset_of_methodBase_3() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___methodBase_3)); }
	inline MethodBase_t2927642150 * get_methodBase_3() const { return ___methodBase_3; }
	inline MethodBase_t2927642150 ** get_address_of_methodBase_3() { return &___methodBase_3; }
	inline void set_methodBase_3(MethodBase_t2927642150 * value)
	{
		___methodBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___methodBase_3), value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_4), value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_columnNumber_6() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___columnNumber_6)); }
	inline int32_t get_columnNumber_6() const { return ___columnNumber_6; }
	inline int32_t* get_address_of_columnNumber_6() { return &___columnNumber_6; }
	inline void set_columnNumber_6(int32_t value)
	{
		___columnNumber_6 = value;
	}

	inline static int32_t get_offset_of_internalMethodName_7() { return static_cast<int32_t>(offsetof(StackFrame_t1778166981, ___internalMethodName_7)); }
	inline String_t* get_internalMethodName_7() const { return ___internalMethodName_7; }
	inline String_t** get_address_of_internalMethodName_7() { return &___internalMethodName_7; }
	inline void set_internalMethodName_7(String_t* value)
	{
		___internalMethodName_7 = value;
		Il2CppCodeGenWriteBarrier((&___internalMethodName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKFRAME_T1778166981_H
#ifndef SYSTEMINFO_T2969836265_H
#define SYSTEMINFO_T2969836265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemInfo
struct  SystemInfo_t2969836265  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMINFO_T2969836265_H
#ifndef YIELDINSTRUCTION_T3270995273_H
#define YIELDINSTRUCTION_T3270995273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3270995273  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3270995273_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3270995273_H
#ifndef SYNCHRONIZATIONCONTEXT_T2274409467_H
#define SYNCHRONIZATIONCONTEXT_T2274409467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t2274409467  : public RuntimeObject
{
public:

public:
};

struct SynchronizationContext_t2274409467_ThreadStaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t2274409467 * ___currentContext_0;

public:
	inline static int32_t get_offset_of_currentContext_0() { return static_cast<int32_t>(offsetof(SynchronizationContext_t2274409467_ThreadStaticFields, ___currentContext_0)); }
	inline SynchronizationContext_t2274409467 * get_currentContext_0() const { return ___currentContext_0; }
	inline SynchronizationContext_t2274409467 ** get_address_of_currentContext_0() { return &___currentContext_0; }
	inline void set_currentContext_0(SynchronizationContext_t2274409467 * value)
	{
		___currentContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZATIONCONTEXT_T2274409467_H
#ifndef QUEUE_1_T1480213181_H
#define QUEUE_1_T1480213181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Queue_1_t1480213181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	WorkRequestU5BU5D_t2656204507* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t1480213181, ____array_0)); }
	inline WorkRequestU5BU5D_t2656204507* get__array_0() const { return ____array_0; }
	inline WorkRequestU5BU5D_t2656204507** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(WorkRequestU5BU5D_t2656204507* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t1480213181, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Queue_1_t1480213181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Queue_1_t1480213181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T1480213181_H
#ifndef TIME_T3452838990_H
#define TIME_T3452838990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Time
struct  Time_t3452838990  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T3452838990_H
#ifndef UNITYSTRING_T897915772_H
#define UNITYSTRING_T897915772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityString
struct  UnityString_t897915772  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSTRING_T897915772_H
#ifndef ENCODING_T4004889433_H
#define ENCODING_T4004889433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t4004889433  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t1140074740 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1057161456 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___decoder_fallback_3)); }
	inline DecoderFallback_t1140074740 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t1140074740 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t1140074740 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___encoder_fallback_4)); }
	inline EncoderFallback_t1057161456 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1057161456 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1057161456 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t4004889433, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t4004889433_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t3018166672 * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t2737604620* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t4004889433 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t4004889433 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t4004889433 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t4004889433 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t4004889433 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t4004889433 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t4004889433 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t4004889433 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t4004889433 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t4004889433 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t4004889433 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t3018166672 * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t3018166672 ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t3018166672 * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t2737604620* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t2737604620** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t2737604620* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t4004889433 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t4004889433 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t4004889433 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t4004889433 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t4004889433 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t4004889433 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t4004889433 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t4004889433 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t4004889433 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t4004889433 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t4004889433 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t4004889433 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t4004889433 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t4004889433 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t4004889433 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t4004889433 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t4004889433 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t4004889433 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t4004889433 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t4004889433 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t4004889433 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t4004889433 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t4004889433 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t4004889433 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t4004889433 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t4004889433 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t4004889433 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t4004889433 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t4004889433 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t4004889433 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t4004889433 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t4004889433_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T4004889433_H
#ifndef TEXTWRITER_T997444721_H
#define TEXTWRITER_T997444721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t997444721  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t3419619864* ___CoreNewLine_0;
	// System.IFormatProvider System.IO.TextWriter::internalFormatProvider
	RuntimeObject* ___internalFormatProvider_1;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t997444721, ___CoreNewLine_0)); }
	inline CharU5BU5D_t3419619864* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t3419619864** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t3419619864* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}

	inline static int32_t get_offset_of_internalFormatProvider_1() { return static_cast<int32_t>(offsetof(TextWriter_t997444721, ___internalFormatProvider_1)); }
	inline RuntimeObject* get_internalFormatProvider_1() const { return ___internalFormatProvider_1; }
	inline RuntimeObject** get_address_of_internalFormatProvider_1() { return &___internalFormatProvider_1; }
	inline void set_internalFormatProvider_1(RuntimeObject* value)
	{
		___internalFormatProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalFormatProvider_1), value);
	}
};

struct TextWriter_t997444721_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t997444721 * ___Null_2;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(TextWriter_t997444721_StaticFields, ___Null_2)); }
	inline TextWriter_t997444721 * get_Null_2() const { return ___Null_2; }
	inline TextWriter_t997444721 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(TextWriter_t997444721 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T997444721_H
#ifndef SERIALIZATIONINFO_T259346668_H
#define SERIALIZATIONINFO_T259346668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t259346668  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t2354558714 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t4250946984 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___serialized_0)); }
	inline Hashtable_t2354558714 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t2354558714 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t2354558714 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___values_1)); }
	inline ArrayList_t4250946984 * get_values_1() const { return ___values_1; }
	inline ArrayList_t4250946984 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t4250946984 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t259346668, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T259346668_H
#ifndef UNHANDLEDEXCEPTIONHANDLER_T4175730415_H
#define UNHANDLEDEXCEPTIONHANDLER_T4175730415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnhandledExceptionHandler
struct  UnhandledExceptionHandler_t4175730415  : public RuntimeObject
{
public:

public:
};

struct UnhandledExceptionHandler_t4175730415_StaticFields
{
public:
	// System.UnhandledExceptionEventHandler UnityEngine.UnhandledExceptionHandler::<>f__mg$cache0
	UnhandledExceptionEventHandler_t3749840881 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(UnhandledExceptionHandler_t4175730415_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline UnhandledExceptionEventHandler_t3749840881 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline UnhandledExceptionEventHandler_t3749840881 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(UnhandledExceptionEventHandler_t3749840881 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONHANDLER_T4175730415_H
#ifndef SPRITEATLASMANAGER_T901682312_H
#define SPRITEATLASMANAGER_T901682312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager
struct  SpriteAtlasManager_t901682312  : public RuntimeObject
{
public:

public:
};

struct SpriteAtlasManager_t901682312_StaticFields
{
public:
	// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback UnityEngine.U2D.SpriteAtlasManager::atlasRequested
	RequestAtlasCallback_t1427750696 * ___atlasRequested_0;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.U2D.SpriteAtlasManager::<>f__mg$cache0
	Action_1_t3400447372 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_atlasRequested_0() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t901682312_StaticFields, ___atlasRequested_0)); }
	inline RequestAtlasCallback_t1427750696 * get_atlasRequested_0() const { return ___atlasRequested_0; }
	inline RequestAtlasCallback_t1427750696 ** get_address_of_atlasRequested_0() { return &___atlasRequested_0; }
	inline void set_atlasRequested_0(RequestAtlasCallback_t1427750696 * value)
	{
		___atlasRequested_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasRequested_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t901682312_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Action_1_t3400447372 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Action_1_t3400447372 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Action_1_t3400447372 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLASMANAGER_T901682312_H
#ifndef CUSTOMYIELDINSTRUCTION_T2273300952_H
#define CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t2273300952  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T2273300952_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef ENUMERATOR_T261617688_H
#define ENUMERATOR_T261617688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform/Enumerator
struct  Enumerator_t261617688  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityEngine.Transform/Enumerator::outer
	Transform_t362059596 * ___outer_0;
	// System.Int32 UnityEngine.Transform/Enumerator::currentIndex
	int32_t ___currentIndex_1;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t261617688, ___outer_0)); }
	inline Transform_t362059596 * get_outer_0() const { return ___outer_0; }
	inline Transform_t362059596 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Transform_t362059596 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t261617688, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T261617688_H
#ifndef MARSHALBYREFOBJECT_T1482034164_H
#define MARSHALBYREFOBJECT_T1482034164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1482034164  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t3687691741 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1482034164, ____identity_0)); }
	inline ServerIdentity_t3687691741 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t3687691741 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t3687691741 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T1482034164_H
#ifndef SENDMOUSEEVENTS_T2768135016_H
#define SENDMOUSEEVENTS_T2768135016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents
struct  SendMouseEvents_t2768135016  : public RuntimeObject
{
public:

public:
};

struct SendMouseEvents_t2768135016_StaticFields
{
public:
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_t2568675203* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_t2568675203* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_t2568675203* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t4045078555* ___m_Cameras_4;

public:
	inline static int32_t get_offset_of_s_MouseUsed_0() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2768135016_StaticFields, ___s_MouseUsed_0)); }
	inline bool get_s_MouseUsed_0() const { return ___s_MouseUsed_0; }
	inline bool* get_address_of_s_MouseUsed_0() { return &___s_MouseUsed_0; }
	inline void set_s_MouseUsed_0(bool value)
	{
		___s_MouseUsed_0 = value;
	}

	inline static int32_t get_offset_of_m_LastHit_1() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2768135016_StaticFields, ___m_LastHit_1)); }
	inline HitInfoU5BU5D_t2568675203* get_m_LastHit_1() const { return ___m_LastHit_1; }
	inline HitInfoU5BU5D_t2568675203** get_address_of_m_LastHit_1() { return &___m_LastHit_1; }
	inline void set_m_LastHit_1(HitInfoU5BU5D_t2568675203* value)
	{
		___m_LastHit_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastHit_1), value);
	}

	inline static int32_t get_offset_of_m_MouseDownHit_2() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2768135016_StaticFields, ___m_MouseDownHit_2)); }
	inline HitInfoU5BU5D_t2568675203* get_m_MouseDownHit_2() const { return ___m_MouseDownHit_2; }
	inline HitInfoU5BU5D_t2568675203** get_address_of_m_MouseDownHit_2() { return &___m_MouseDownHit_2; }
	inline void set_m_MouseDownHit_2(HitInfoU5BU5D_t2568675203* value)
	{
		___m_MouseDownHit_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseDownHit_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentHit_3() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2768135016_StaticFields, ___m_CurrentHit_3)); }
	inline HitInfoU5BU5D_t2568675203* get_m_CurrentHit_3() const { return ___m_CurrentHit_3; }
	inline HitInfoU5BU5D_t2568675203** get_address_of_m_CurrentHit_3() { return &___m_CurrentHit_3; }
	inline void set_m_CurrentHit_3(HitInfoU5BU5D_t2568675203* value)
	{
		___m_CurrentHit_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentHit_3), value);
	}

	inline static int32_t get_offset_of_m_Cameras_4() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2768135016_StaticFields, ___m_Cameras_4)); }
	inline CameraU5BU5D_t4045078555* get_m_Cameras_4() const { return ___m_Cameras_4; }
	inline CameraU5BU5D_t4045078555** get_address_of_m_Cameras_4() { return &___m_Cameras_4; }
	inline void set_m_Cameras_4(CameraU5BU5D_t4045078555* value)
	{
		___m_Cameras_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cameras_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMOUSEEVENTS_T2768135016_H
#ifndef SETUPCOROUTINE_T82219935_H
#define SETUPCOROUTINE_T82219935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SetupCoroutine
struct  SetupCoroutine_t82219935  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPCOROUTINE_T82219935_H
#ifndef EVENTARGS_T2659587769_H
#define EVENTARGS_T2659587769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t2659587769  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t2659587769_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t2659587769 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t2659587769_StaticFields, ___Empty_0)); }
	inline EventArgs_t2659587769 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t2659587769 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t2659587769 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T2659587769_H
#ifndef ATTRIBUTE_T1924466020_H
#define ATTRIBUTE_T1924466020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1924466020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1924466020_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef CRITICALFINALIZEROBJECT_T2012302976_H
#define CRITICALFINALIZEROBJECT_T2012302976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t2012302976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T2012302976_H
#ifndef WAITFORSECONDS_T4272504447_H
#define WAITFORSECONDS_T4272504447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t4272504447  : public YieldInstruction_t3270995273
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t4272504447, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t4272504447_marshaled_pinvoke : public YieldInstruction_t3270995273_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t4272504447_marshaled_com : public YieldInstruction_t3270995273_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T4272504447_H
#ifndef WAITFORFIXEDUPDATE_T4012476583_H
#define WAITFORFIXEDUPDATE_T4012476583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForFixedUpdate
struct  WaitForFixedUpdate_t4012476583  : public YieldInstruction_t3270995273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORFIXEDUPDATE_T4012476583_H
#ifndef WAITFORENDOFFRAME_T1056898812_H
#define WAITFORENDOFFRAME_T1056898812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t1056898812  : public YieldInstruction_t3270995273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORENDOFFRAME_T1056898812_H
#ifndef BYTE_T131097440_H
#define BYTE_T131097440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t131097440 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t131097440, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T131097440_H
#ifndef WORKREQUEST_T1716204206_H
#define WORKREQUEST_T1716204206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext/WorkRequest
struct  WorkRequest_t1716204206 
{
public:
	// System.Threading.SendOrPostCallback UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateCallback
	SendOrPostCallback_t2349539412 * ___m_DelagateCallback_0;
	// System.Object UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateState
	RuntimeObject * ___m_DelagateState_1;
	// System.Threading.ManualResetEvent UnityEngine.UnitySynchronizationContext/WorkRequest::m_WaitHandle
	ManualResetEvent_t504895258 * ___m_WaitHandle_2;

public:
	inline static int32_t get_offset_of_m_DelagateCallback_0() { return static_cast<int32_t>(offsetof(WorkRequest_t1716204206, ___m_DelagateCallback_0)); }
	inline SendOrPostCallback_t2349539412 * get_m_DelagateCallback_0() const { return ___m_DelagateCallback_0; }
	inline SendOrPostCallback_t2349539412 ** get_address_of_m_DelagateCallback_0() { return &___m_DelagateCallback_0; }
	inline void set_m_DelagateCallback_0(SendOrPostCallback_t2349539412 * value)
	{
		___m_DelagateCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateCallback_0), value);
	}

	inline static int32_t get_offset_of_m_DelagateState_1() { return static_cast<int32_t>(offsetof(WorkRequest_t1716204206, ___m_DelagateState_1)); }
	inline RuntimeObject * get_m_DelagateState_1() const { return ___m_DelagateState_1; }
	inline RuntimeObject ** get_address_of_m_DelagateState_1() { return &___m_DelagateState_1; }
	inline void set_m_DelagateState_1(RuntimeObject * value)
	{
		___m_DelagateState_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateState_1), value);
	}

	inline static int32_t get_offset_of_m_WaitHandle_2() { return static_cast<int32_t>(offsetof(WorkRequest_t1716204206, ___m_WaitHandle_2)); }
	inline ManualResetEvent_t504895258 * get_m_WaitHandle_2() const { return ___m_WaitHandle_2; }
	inline ManualResetEvent_t504895258 ** get_address_of_m_WaitHandle_2() { return &___m_WaitHandle_2; }
	inline void set_m_WaitHandle_2(ManualResetEvent_t504895258 * value)
	{
		___m_WaitHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t1716204206_marshaled_pinvoke
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t504895258 * ___m_WaitHandle_2;
};
// Native definition for COM marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t1716204206_marshaled_com
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t504895258 * ___m_WaitHandle_2;
};
#endif // WORKREQUEST_T1716204206_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef THREADANDSERIALIZATIONSAFEATTRIBUTE_T878041766_H
#define THREADANDSERIALIZATIONSAFEATTRIBUTE_T878041766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadAndSerializationSafeAttribute
struct  ThreadAndSerializationSafeAttribute_t878041766  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADANDSERIALIZATIONSAFEATTRIBUTE_T878041766_H
#ifndef UNITYSYNCHRONIZATIONCONTEXT_T485758571_H
#define UNITYSYNCHRONIZATIONCONTEXT_T485758571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext
struct  UnitySynchronizationContext_t485758571  : public SynchronizationContext_t2274409467
{
public:
	// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest> UnityEngine.UnitySynchronizationContext::m_AsyncWorkQueue
	Queue_1_t1480213181 * ___m_AsyncWorkQueue_1;
	// System.Int32 UnityEngine.UnitySynchronizationContext::m_MainThreadID
	int32_t ___m_MainThreadID_2;

public:
	inline static int32_t get_offset_of_m_AsyncWorkQueue_1() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t485758571, ___m_AsyncWorkQueue_1)); }
	inline Queue_1_t1480213181 * get_m_AsyncWorkQueue_1() const { return ___m_AsyncWorkQueue_1; }
	inline Queue_1_t1480213181 ** get_address_of_m_AsyncWorkQueue_1() { return &___m_AsyncWorkQueue_1; }
	inline void set_m_AsyncWorkQueue_1(Queue_1_t1480213181 * value)
	{
		___m_AsyncWorkQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncWorkQueue_1), value);
	}

	inline static int32_t get_offset_of_m_MainThreadID_2() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t485758571, ___m_MainThreadID_2)); }
	inline int32_t get_m_MainThreadID_2() const { return ___m_MainThreadID_2; }
	inline int32_t* get_address_of_m_MainThreadID_2() { return &___m_MainThreadID_2; }
	inline void set_m_MainThreadID_2(int32_t value)
	{
		___m_MainThreadID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSYNCHRONIZATIONCONTEXT_T485758571_H
#ifndef HITINFO_T1469516070_H
#define HITINFO_T1469516070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t1469516070 
{
public:
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t2557347079 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t2839736942 * ___camera_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(HitInfo_t1469516070, ___target_0)); }
	inline GameObject_t2557347079 * get_target_0() const { return ___target_0; }
	inline GameObject_t2557347079 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t2557347079 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(HitInfo_t1469516070, ___camera_1)); }
	inline Camera_t2839736942 * get_camera_1() const { return ___camera_1; }
	inline Camera_t2839736942 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t2839736942 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t1469516070_marshaled_pinvoke
{
	GameObject_t2557347079 * ___target_0;
	Camera_t2839736942 * ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t1469516070_marshaled_com
{
	GameObject_t2557347079 * ___target_0;
	Camera_t2839736942 * ___camera_1;
};
#endif // HITINFO_T1469516070_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef UNITYLOGWRITER_T1450454900_H
#define UNITYLOGWRITER_T1450454900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityLogWriter
struct  UnityLogWriter_t1450454900  : public TextWriter_t997444721
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYLOGWRITER_T1450454900_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1049358371_H
#define TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1049358371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
struct  TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371 
{
public:
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::keyboardType
	uint32_t ___keyboardType_0;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::autocorrection
	uint32_t ___autocorrection_1;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::multiline
	uint32_t ___multiline_2;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::secure
	uint32_t ___secure_3;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::alert
	uint32_t ___alert_4;

public:
	inline static int32_t get_offset_of_keyboardType_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371, ___keyboardType_0)); }
	inline uint32_t get_keyboardType_0() const { return ___keyboardType_0; }
	inline uint32_t* get_address_of_keyboardType_0() { return &___keyboardType_0; }
	inline void set_keyboardType_0(uint32_t value)
	{
		___keyboardType_0 = value;
	}

	inline static int32_t get_offset_of_autocorrection_1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371, ___autocorrection_1)); }
	inline uint32_t get_autocorrection_1() const { return ___autocorrection_1; }
	inline uint32_t* get_address_of_autocorrection_1() { return &___autocorrection_1; }
	inline void set_autocorrection_1(uint32_t value)
	{
		___autocorrection_1 = value;
	}

	inline static int32_t get_offset_of_multiline_2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371, ___multiline_2)); }
	inline uint32_t get_multiline_2() const { return ___multiline_2; }
	inline uint32_t* get_address_of_multiline_2() { return &___multiline_2; }
	inline void set_multiline_2(uint32_t value)
	{
		___multiline_2 = value;
	}

	inline static int32_t get_offset_of_secure_3() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371, ___secure_3)); }
	inline uint32_t get_secure_3() const { return ___secure_3; }
	inline uint32_t* get_address_of_secure_3() { return &___secure_3; }
	inline void set_secure_3(uint32_t value)
	{
		___secure_3 = value;
	}

	inline static int32_t get_offset_of_alert_4() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371, ___alert_4)); }
	inline uint32_t get_alert_4() const { return ___alert_4; }
	inline uint32_t* get_address_of_alert_4() { return &___alert_4; }
	inline void set_alert_4(uint32_t value)
	{
		___alert_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1049358371_H
#ifndef UNITYEXCEPTION_T1933799526_H
#define UNITYEXCEPTION_T1933799526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t1933799526  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXCEPTION_T1933799526_H
#ifndef RANGEINT_T66892349_H
#define RANGEINT_T66892349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeInt
struct  RangeInt_t66892349 
{
public:
	// System.Int32 UnityEngine.RangeInt::start
	int32_t ___start_0;
	// System.Int32 UnityEngine.RangeInt::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RangeInt_t66892349, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RangeInt_t66892349, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEINT_T66892349_H
#ifndef UNHANDLEDEXCEPTIONEVENTARGS_T1939597804_H
#define UNHANDLEDEXCEPTIONEVENTARGS_T1939597804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventArgs
struct  UnhandledExceptionEventArgs_t1939597804  : public EventArgs_t2659587769
{
public:
	// System.Object System.UnhandledExceptionEventArgs::exception
	RuntimeObject * ___exception_1;
	// System.Boolean System.UnhandledExceptionEventArgs::m_isTerminating
	bool ___m_isTerminating_2;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t1939597804, ___exception_1)); }
	inline RuntimeObject * get_exception_1() const { return ___exception_1; }
	inline RuntimeObject ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(RuntimeObject * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_m_isTerminating_2() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t1939597804, ___m_isTerminating_2)); }
	inline bool get_m_isTerminating_2() const { return ___m_isTerminating_2; }
	inline bool* get_address_of_m_isTerminating_2() { return &___m_isTerminating_2; }
	inline void set_m_isTerminating_2(bool value)
	{
		___m_isTerminating_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTARGS_T1939597804_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef QUATERNION_T704191599_H
#define QUATERNION_T704191599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t704191599 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t704191599, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t704191599_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t704191599  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t704191599_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t704191599  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t704191599 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t704191599  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T704191599_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline uintptr_t get_Zero_0() const { return ___Zero_0; }
	inline uintptr_t* get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(uintptr_t value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef WAITFORSECONDSREALTIME_T3353236027_H
#define WAITFORSECONDSREALTIME_T3353236027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSecondsRealtime
struct  WaitForSecondsRealtime_t3353236027  : public CustomYieldInstruction_t2273300952
{
public:
	// System.Single UnityEngine.WaitForSecondsRealtime::waitTime
	float ___waitTime_0;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t3353236027, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSECONDSREALTIME_T3353236027_H
#ifndef GENERICSTACK_T2697154139_H
#define GENERICSTACK_T2697154139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.GenericStack
struct  GenericStack_t2697154139  : public Stack_t535311253
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICSTACK_T2697154139_H
#ifndef METHODBASE_T2927642150_H
#define METHODBASE_T2927642150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t2927642150  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T2927642150_H
#ifndef TYPEINFERENCERULEATTRIBUTE_T2902006326_H
#define TYPEINFERENCERULEATTRIBUTE_T2902006326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRuleAttribute
struct  TypeInferenceRuleAttribute_t2902006326  : public Attribute_t1924466020
{
public:
	// System.String UnityEngineInternal.TypeInferenceRuleAttribute::_rule
	String_t* ____rule_0;

public:
	inline static int32_t get_offset_of__rule_0() { return static_cast<int32_t>(offsetof(TypeInferenceRuleAttribute_t2902006326, ____rule_0)); }
	inline String_t* get__rule_0() const { return ____rule_0; }
	inline String_t** get_address_of__rule_0() { return &____rule_0; }
	inline void set__rule_0(String_t* value)
	{
		____rule_0 = value;
		Il2CppCodeGenWriteBarrier((&____rule_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULEATTRIBUTE_T2902006326_H
#ifndef PARAMETERMODIFIER_T999651945_H
#define PARAMETERMODIFIER_T999651945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t999651945 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t698278498* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t999651945, ____byref_0)); }
	inline BooleanU5BU5D_t698278498* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t698278498** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t698278498* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t999651945_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t999651945_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T999651945_H
#ifndef SERIALIZEPRIVATEVARIABLES_T1297531198_H
#define SERIALIZEPRIVATEVARIABLES_T1297531198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializePrivateVariables
struct  SerializePrivateVariables_t1297531198  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEPRIVATEVARIABLES_T1297531198_H
#ifndef SERIALIZEFIELD_T3205734915_H
#define SERIALIZEFIELD_T3205734915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializeField
struct  SerializeField_t3205734915  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEFIELD_T3205734915_H
#ifndef SORTINGLAYER_T3679355086_H
#define SORTINGLAYER_T3679355086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SortingLayer
struct  SortingLayer_t3679355086 
{
public:
	// System.Int32 UnityEngine.SortingLayer::m_Id
	int32_t ___m_Id_0;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(SortingLayer_t3679355086, ___m_Id_0)); }
	inline int32_t get_m_Id_0() const { return ___m_Id_0; }
	inline int32_t* get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(int32_t value)
	{
		___m_Id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYER_T3679355086_H
#ifndef PROPERTYATTRIBUTE_T381499487_H
#define PROPERTYATTRIBUTE_T381499487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t381499487  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T381499487_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T333733317_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T333733317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t333733317  : public Attribute_t1924466020
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t333733317, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T333733317_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef VECTOR4_T2436299922_H
#define VECTOR4_T2436299922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2436299922 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2436299922, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2436299922_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2436299922  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2436299922  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2436299922  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2436299922  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2436299922  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2436299922 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2436299922  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___oneVector_6)); }
	inline Vector4_t2436299922  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2436299922 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2436299922  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2436299922  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2436299922 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2436299922  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2436299922_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2436299922  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2436299922 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2436299922  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2436299922_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef UINT32_T1721902794_H
#define UINT32_T1721902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1721902794 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1721902794, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1721902794_H
#ifndef MATRIX4X4_T1237934469_H
#define MATRIX4X4_T1237934469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1237934469 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1237934469_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1237934469  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1237934469  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1237934469  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1237934469 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1237934469  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1237934469_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1237934469  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1237934469 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1237934469  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1237934469_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef CHAR_T3714759797_H
#define CHAR_T3714759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3714759797 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3714759797, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3714759797_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3714759797_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3714759797_H
#ifndef WRITABLEATTRIBUTE_T482114951_H
#define WRITABLEATTRIBUTE_T482114951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WritableAttribute
struct  WritableAttribute_t482114951  : public Attribute_t1924466020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITABLEATTRIBUTE_T482114951_H
#ifndef STREAMINGCONTEXTSTATES_T968392603_H
#define STREAMINGCONTEXTSTATES_T968392603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t968392603 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t968392603, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T968392603_H
#ifndef PRINCIPALPOLICY_T3318939238_H
#define PRINCIPALPOLICY_T3318939238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.PrincipalPolicy
struct  PrincipalPolicy_t3318939238 
{
public:
	// System.Int32 System.Security.Principal.PrincipalPolicy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrincipalPolicy_t3318939238, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINCIPALPOLICY_T3318939238_H
#ifndef PARAMETERATTRIBUTES_T2006579846_H
#define PARAMETERATTRIBUTES_T2006579846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t2006579846 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t2006579846, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T2006579846_H
#ifndef DELEGATE_T3882343965_H
#define DELEGATE_T3882343965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3882343965  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1549710636 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3882343965, ___data_8)); }
	inline DelegateData_t1549710636 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1549710636 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1549710636 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3882343965_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t2927642150
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef VRTEXTUREUSAGE_T2459540541_H
#define VRTEXTUREUSAGE_T2459540541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t2459540541 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VRTextureUsage_t2459540541, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRTEXTUREUSAGE_T2459540541_H
#ifndef MATHFINTERNAL_T3317739077_H
#define MATHFINTERNAL_T3317739077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.MathfInternal
struct  MathfInternal_t3317739077 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MathfInternal_t3317739077__padding[1];
	};

public:
};

struct MathfInternal_t3317739077_StaticFields
{
public:
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;

public:
	inline static int32_t get_offset_of_FloatMinNormal_0() { return static_cast<int32_t>(offsetof(MathfInternal_t3317739077_StaticFields, ___FloatMinNormal_0)); }
	inline float get_FloatMinNormal_0() const { return ___FloatMinNormal_0; }
	inline float* get_address_of_FloatMinNormal_0() { return &___FloatMinNormal_0; }
	inline void set_FloatMinNormal_0(float value)
	{
		___FloatMinNormal_0 = value;
	}

	inline static int32_t get_offset_of_FloatMinDenormal_1() { return static_cast<int32_t>(offsetof(MathfInternal_t3317739077_StaticFields, ___FloatMinDenormal_1)); }
	inline float get_FloatMinDenormal_1() const { return ___FloatMinDenormal_1; }
	inline float* get_address_of_FloatMinDenormal_1() { return &___FloatMinDenormal_1; }
	inline void set_FloatMinDenormal_1(float value)
	{
		___FloatMinDenormal_1 = value;
	}

	inline static int32_t get_offset_of_IsFlushToZeroEnabled_2() { return static_cast<int32_t>(offsetof(MathfInternal_t3317739077_StaticFields, ___IsFlushToZeroEnabled_2)); }
	inline bool get_IsFlushToZeroEnabled_2() const { return ___IsFlushToZeroEnabled_2; }
	inline bool* get_address_of_IsFlushToZeroEnabled_2() { return &___IsFlushToZeroEnabled_2; }
	inline void set_IsFlushToZeroEnabled_2(bool value)
	{
		___IsFlushToZeroEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHFINTERNAL_T3317739077_H
#ifndef THREADSTATE_T3037581080_H
#define THREADSTATE_T3037581080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadState
struct  ThreadState_t3037581080 
{
public:
	// System.Int32 System.Threading.ThreadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadState_t3037581080, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTATE_T3037581080_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#define INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t2909334802  : public SystemException_t2062748594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T2909334802_H
#ifndef RUNTIMETYPEHANDLE_T3069131627_H
#define RUNTIMETYPEHANDLE_T3069131627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3069131627 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3069131627, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3069131627_H
#ifndef TYPEINFERENCERULES_T1200137651_H
#define TYPEINFERENCERULES_T1200137651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRules
struct  TypeInferenceRules_t1200137651 
{
public:
	// System.Int32 UnityEngineInternal.TypeInferenceRules::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeInferenceRules_t1200137651, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULES_T1200137651_H
#ifndef TOOLTIPATTRIBUTE_T2025437004_H
#define TOOLTIPATTRIBUTE_T2025437004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t2025437004  : public PropertyAttribute_t381499487
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t2025437004, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T2025437004_H
#ifndef GRAPHICSDEVICETYPE_T4248724190_H
#define GRAPHICSDEVICETYPE_T4248724190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.GraphicsDeviceType
struct  GraphicsDeviceType_t4248724190 
{
public:
	// System.Int32 UnityEngine.Rendering.GraphicsDeviceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GraphicsDeviceType_t4248724190, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICSDEVICETYPE_T4248724190_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef RAY_T249333749_H
#define RAY_T249333749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t249333749 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t1986933152  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t1986933152  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t249333749, ___m_Origin_0)); }
	inline Vector3_t1986933152  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t1986933152 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t1986933152  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t249333749, ___m_Direction_1)); }
	inline Vector3_t1986933152  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t1986933152 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t1986933152  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T249333749_H
#ifndef CAMERACLEARFLAGS_T3338048873_H
#define CAMERACLEARFLAGS_T3338048873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t3338048873 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t3338048873, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T3338048873_H
#ifndef SENDMESSAGEOPTIONS_T878493381_H
#define SENDMESSAGEOPTIONS_T878493381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t878493381 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t878493381, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T878493381_H
#ifndef ARGUMENTEXCEPTION_T1812645948_H
#define ARGUMENTEXCEPTION_T1812645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1812645948  : public SystemException_t2062748594
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1812645948, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1812645948_H
#ifndef BINDINGFLAGS_T1992594115_H
#define BINDINGFLAGS_T1992594115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1992594115 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1992594115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1992594115_H
#ifndef SPACE_T2800318751_H
#define SPACE_T2800318751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t2800318751 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t2800318751, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T2800318751_H
#ifndef SPACEATTRIBUTE_T3838534887_H
#define SPACEATTRIBUTE_T3838534887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t3838534887  : public PropertyAttribute_t381499487
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t3838534887, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEATTRIBUTE_T3838534887_H
#ifndef SPRITEMESHTYPE_T2991914614_H
#define SPRITEMESHTYPE_T2991914614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteMeshType
struct  SpriteMeshType_t2991914614 
{
public:
	// System.Int32 UnityEngine.SpriteMeshType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteMeshType_t2991914614, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEMESHTYPE_T2991914614_H
#ifndef OPERATINGSYSTEMFAMILY_T3334831006_H
#define OPERATINGSYSTEMFAMILY_T3334831006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_t3334831006 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_t3334831006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_T3334831006_H
#ifndef RENDERTEXTUREFORMAT_T2838254077_H
#define RENDERTEXTUREFORMAT_T2838254077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2838254077 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2838254077, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T2838254077_H
#ifndef TEXTAREAATTRIBUTE_T909267116_H
#define TEXTAREAATTRIBUTE_T909267116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAreaAttribute
struct  TextAreaAttribute_t909267116  : public PropertyAttribute_t381499487
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t909267116, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t909267116, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAATTRIBUTE_T909267116_H
#ifndef FILTERMODE_T2934613156_H
#define FILTERMODE_T2934613156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t2934613156 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t2934613156, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T2934613156_H
#ifndef TEXTUREWRAPMODE_T58247005_H
#define TEXTUREWRAPMODE_T58247005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t58247005 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t58247005, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T58247005_H
#ifndef TEXTUREFORMAT_T1579517457_H
#define TEXTUREFORMAT_T1579517457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t1579517457 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t1579517457, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T1579517457_H
#ifndef WAITHANDLE_T283688518_H
#define WAITHANDLE_T283688518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_t283688518  : public MarshalByRefObject_t1482034164
{
public:
	// Microsoft.Win32.SafeHandles.SafeWaitHandle System.Threading.WaitHandle::safe_wait_handle
	SafeWaitHandle_t1831156579 * ___safe_wait_handle_2;
	// System.Boolean System.Threading.WaitHandle::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_safe_wait_handle_2() { return static_cast<int32_t>(offsetof(WaitHandle_t283688518, ___safe_wait_handle_2)); }
	inline SafeWaitHandle_t1831156579 * get_safe_wait_handle_2() const { return ___safe_wait_handle_2; }
	inline SafeWaitHandle_t1831156579 ** get_address_of_safe_wait_handle_2() { return &___safe_wait_handle_2; }
	inline void set_safe_wait_handle_2(SafeWaitHandle_t1831156579 * value)
	{
		___safe_wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___safe_wait_handle_2), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(WaitHandle_t283688518, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};

struct WaitHandle_t283688518_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_3;

public:
	inline static int32_t get_offset_of_InvalidHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_t283688518_StaticFields, ___InvalidHandle_3)); }
	inline intptr_t get_InvalidHandle_3() const { return ___InvalidHandle_3; }
	inline intptr_t* get_address_of_InvalidHandle_3() { return &___InvalidHandle_3; }
	inline void set_InvalidHandle_3(intptr_t value)
	{
		___InvalidHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITHANDLE_T283688518_H
#ifndef TRACKEDREFERENCE_T2624196464_H
#define TRACKEDREFERENCE_T2624196464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t2624196464  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t2624196464, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2624196464_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2624196464_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T2624196464_H
#ifndef THREADPRIORITY_T3576530062_H
#define THREADPRIORITY_T3576530062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadPriority
struct  ThreadPriority_t3576530062 
{
public:
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadPriority_t3576530062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T3576530062_H
#ifndef TOUCHPHASE_T2806880854_H
#define TOUCHPHASE_T2806880854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2806880854 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2806880854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2806880854_H
#ifndef TOUCHTYPE_T1113074055_H
#define TOUCHTYPE_T1113074055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t1113074055 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t1113074055, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T1113074055_H
#ifndef TOUCHSCREENKEYBOARD_T1357543767_H
#define TOUCHSCREENKEYBOARD_T1357543767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard
struct  TouchScreenKeyboard_t1357543767  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TouchScreenKeyboard::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t1357543767, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_T1357543767_H
#ifndef RUNTIMEPLATFORM_T2843441307_H
#define RUNTIMEPLATFORM_T2843441307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t2843441307 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t2843441307, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T2843441307_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#define TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1971784194 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1971784194, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1971784194_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef GAMEOBJECT_T2557347079_H
#define GAMEOBJECT_T2557347079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2557347079  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2557347079_H
#ifndef MULTICASTDELEGATE_T1652681189_H
#define MULTICASTDELEGATE_T1652681189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1652681189  : public Delegate_t3882343965
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1652681189 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1652681189 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___prev_9)); }
	inline MulticastDelegate_t1652681189 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1652681189 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1652681189 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1652681189, ___kpm_next_10)); }
	inline MulticastDelegate_t1652681189 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1652681189 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1652681189 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1652681189_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3069131627  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3069131627  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3069131627 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3069131627  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1985992169* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2357198184 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2357198184 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2357198184 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1985992169* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1985992169** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1985992169* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2357198184 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2357198184 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2357198184 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2357198184 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2357198184 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2357198184 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2357198184 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef APPDOMAIN_T3842411093_H
#define APPDOMAIN_T3842411093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppDomain
struct  AppDomain_t3842411093  : public MarshalByRefObject_t1482034164
{
public:
	// System.IntPtr System.AppDomain::_mono_app_domain
	intptr_t ____mono_app_domain_1;
	// System.Security.Policy.Evidence System.AppDomain::_evidence
	Evidence_t1158797702 * ____evidence_6;
	// System.Security.PermissionSet System.AppDomain::_granted
	PermissionSet_t4252355126 * ____granted_7;
	// System.Security.Principal.PrincipalPolicy System.AppDomain::_principalPolicy
	int32_t ____principalPolicy_8;
	// System.AppDomainManager System.AppDomain::_domain_manager
	AppDomainManager_t3873064184 * ____domain_manager_11;
	// System.ActivationContext System.AppDomain::_activation
	ActivationContext_t1493131857 * ____activation_12;
	// System.ApplicationIdentity System.AppDomain::_applicationIdentity
	ApplicationIdentity_t1276138697 * ____applicationIdentity_13;
	// System.AssemblyLoadEventHandler System.AppDomain::AssemblyLoad
	AssemblyLoadEventHandler_t3835039474 * ___AssemblyLoad_14;
	// System.ResolveEventHandler System.AppDomain::AssemblyResolve
	ResolveEventHandler_t108319945 * ___AssemblyResolve_15;
	// System.EventHandler System.AppDomain::DomainUnload
	EventHandler_t369332490 * ___DomainUnload_16;
	// System.EventHandler System.AppDomain::ProcessExit
	EventHandler_t369332490 * ___ProcessExit_17;
	// System.ResolveEventHandler System.AppDomain::ResourceResolve
	ResolveEventHandler_t108319945 * ___ResourceResolve_18;
	// System.ResolveEventHandler System.AppDomain::TypeResolve
	ResolveEventHandler_t108319945 * ___TypeResolve_19;
	// System.UnhandledExceptionEventHandler System.AppDomain::UnhandledException
	UnhandledExceptionEventHandler_t3749840881 * ___UnhandledException_20;
	// System.ResolveEventHandler System.AppDomain::ReflectionOnlyAssemblyResolve
	ResolveEventHandler_t108319945 * ___ReflectionOnlyAssemblyResolve_21;

public:
	inline static int32_t get_offset_of__mono_app_domain_1() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____mono_app_domain_1)); }
	inline intptr_t get__mono_app_domain_1() const { return ____mono_app_domain_1; }
	inline intptr_t* get_address_of__mono_app_domain_1() { return &____mono_app_domain_1; }
	inline void set__mono_app_domain_1(intptr_t value)
	{
		____mono_app_domain_1 = value;
	}

	inline static int32_t get_offset_of__evidence_6() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____evidence_6)); }
	inline Evidence_t1158797702 * get__evidence_6() const { return ____evidence_6; }
	inline Evidence_t1158797702 ** get_address_of__evidence_6() { return &____evidence_6; }
	inline void set__evidence_6(Evidence_t1158797702 * value)
	{
		____evidence_6 = value;
		Il2CppCodeGenWriteBarrier((&____evidence_6), value);
	}

	inline static int32_t get_offset_of__granted_7() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____granted_7)); }
	inline PermissionSet_t4252355126 * get__granted_7() const { return ____granted_7; }
	inline PermissionSet_t4252355126 ** get_address_of__granted_7() { return &____granted_7; }
	inline void set__granted_7(PermissionSet_t4252355126 * value)
	{
		____granted_7 = value;
		Il2CppCodeGenWriteBarrier((&____granted_7), value);
	}

	inline static int32_t get_offset_of__principalPolicy_8() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____principalPolicy_8)); }
	inline int32_t get__principalPolicy_8() const { return ____principalPolicy_8; }
	inline int32_t* get_address_of__principalPolicy_8() { return &____principalPolicy_8; }
	inline void set__principalPolicy_8(int32_t value)
	{
		____principalPolicy_8 = value;
	}

	inline static int32_t get_offset_of__domain_manager_11() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____domain_manager_11)); }
	inline AppDomainManager_t3873064184 * get__domain_manager_11() const { return ____domain_manager_11; }
	inline AppDomainManager_t3873064184 ** get_address_of__domain_manager_11() { return &____domain_manager_11; }
	inline void set__domain_manager_11(AppDomainManager_t3873064184 * value)
	{
		____domain_manager_11 = value;
		Il2CppCodeGenWriteBarrier((&____domain_manager_11), value);
	}

	inline static int32_t get_offset_of__activation_12() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____activation_12)); }
	inline ActivationContext_t1493131857 * get__activation_12() const { return ____activation_12; }
	inline ActivationContext_t1493131857 ** get_address_of__activation_12() { return &____activation_12; }
	inline void set__activation_12(ActivationContext_t1493131857 * value)
	{
		____activation_12 = value;
		Il2CppCodeGenWriteBarrier((&____activation_12), value);
	}

	inline static int32_t get_offset_of__applicationIdentity_13() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ____applicationIdentity_13)); }
	inline ApplicationIdentity_t1276138697 * get__applicationIdentity_13() const { return ____applicationIdentity_13; }
	inline ApplicationIdentity_t1276138697 ** get_address_of__applicationIdentity_13() { return &____applicationIdentity_13; }
	inline void set__applicationIdentity_13(ApplicationIdentity_t1276138697 * value)
	{
		____applicationIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&____applicationIdentity_13), value);
	}

	inline static int32_t get_offset_of_AssemblyLoad_14() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___AssemblyLoad_14)); }
	inline AssemblyLoadEventHandler_t3835039474 * get_AssemblyLoad_14() const { return ___AssemblyLoad_14; }
	inline AssemblyLoadEventHandler_t3835039474 ** get_address_of_AssemblyLoad_14() { return &___AssemblyLoad_14; }
	inline void set_AssemblyLoad_14(AssemblyLoadEventHandler_t3835039474 * value)
	{
		___AssemblyLoad_14 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyLoad_14), value);
	}

	inline static int32_t get_offset_of_AssemblyResolve_15() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___AssemblyResolve_15)); }
	inline ResolveEventHandler_t108319945 * get_AssemblyResolve_15() const { return ___AssemblyResolve_15; }
	inline ResolveEventHandler_t108319945 ** get_address_of_AssemblyResolve_15() { return &___AssemblyResolve_15; }
	inline void set_AssemblyResolve_15(ResolveEventHandler_t108319945 * value)
	{
		___AssemblyResolve_15 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyResolve_15), value);
	}

	inline static int32_t get_offset_of_DomainUnload_16() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___DomainUnload_16)); }
	inline EventHandler_t369332490 * get_DomainUnload_16() const { return ___DomainUnload_16; }
	inline EventHandler_t369332490 ** get_address_of_DomainUnload_16() { return &___DomainUnload_16; }
	inline void set_DomainUnload_16(EventHandler_t369332490 * value)
	{
		___DomainUnload_16 = value;
		Il2CppCodeGenWriteBarrier((&___DomainUnload_16), value);
	}

	inline static int32_t get_offset_of_ProcessExit_17() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___ProcessExit_17)); }
	inline EventHandler_t369332490 * get_ProcessExit_17() const { return ___ProcessExit_17; }
	inline EventHandler_t369332490 ** get_address_of_ProcessExit_17() { return &___ProcessExit_17; }
	inline void set_ProcessExit_17(EventHandler_t369332490 * value)
	{
		___ProcessExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___ProcessExit_17), value);
	}

	inline static int32_t get_offset_of_ResourceResolve_18() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___ResourceResolve_18)); }
	inline ResolveEventHandler_t108319945 * get_ResourceResolve_18() const { return ___ResourceResolve_18; }
	inline ResolveEventHandler_t108319945 ** get_address_of_ResourceResolve_18() { return &___ResourceResolve_18; }
	inline void set_ResourceResolve_18(ResolveEventHandler_t108319945 * value)
	{
		___ResourceResolve_18 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceResolve_18), value);
	}

	inline static int32_t get_offset_of_TypeResolve_19() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___TypeResolve_19)); }
	inline ResolveEventHandler_t108319945 * get_TypeResolve_19() const { return ___TypeResolve_19; }
	inline ResolveEventHandler_t108319945 ** get_address_of_TypeResolve_19() { return &___TypeResolve_19; }
	inline void set_TypeResolve_19(ResolveEventHandler_t108319945 * value)
	{
		___TypeResolve_19 = value;
		Il2CppCodeGenWriteBarrier((&___TypeResolve_19), value);
	}

	inline static int32_t get_offset_of_UnhandledException_20() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___UnhandledException_20)); }
	inline UnhandledExceptionEventHandler_t3749840881 * get_UnhandledException_20() const { return ___UnhandledException_20; }
	inline UnhandledExceptionEventHandler_t3749840881 ** get_address_of_UnhandledException_20() { return &___UnhandledException_20; }
	inline void set_UnhandledException_20(UnhandledExceptionEventHandler_t3749840881 * value)
	{
		___UnhandledException_20 = value;
		Il2CppCodeGenWriteBarrier((&___UnhandledException_20), value);
	}

	inline static int32_t get_offset_of_ReflectionOnlyAssemblyResolve_21() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093, ___ReflectionOnlyAssemblyResolve_21)); }
	inline ResolveEventHandler_t108319945 * get_ReflectionOnlyAssemblyResolve_21() const { return ___ReflectionOnlyAssemblyResolve_21; }
	inline ResolveEventHandler_t108319945 ** get_address_of_ReflectionOnlyAssemblyResolve_21() { return &___ReflectionOnlyAssemblyResolve_21; }
	inline void set_ReflectionOnlyAssemblyResolve_21(ResolveEventHandler_t108319945 * value)
	{
		___ReflectionOnlyAssemblyResolve_21 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionOnlyAssemblyResolve_21), value);
	}
};

struct AppDomain_t3842411093_StaticFields
{
public:
	// System.String System.AppDomain::_process_guid
	String_t* ____process_guid_2;
	// System.AppDomain System.AppDomain::default_domain
	AppDomain_t3842411093 * ___default_domain_10;

public:
	inline static int32_t get_offset_of__process_guid_2() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_StaticFields, ____process_guid_2)); }
	inline String_t* get__process_guid_2() const { return ____process_guid_2; }
	inline String_t** get_address_of__process_guid_2() { return &____process_guid_2; }
	inline void set__process_guid_2(String_t* value)
	{
		____process_guid_2 = value;
		Il2CppCodeGenWriteBarrier((&____process_guid_2), value);
	}

	inline static int32_t get_offset_of_default_domain_10() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_StaticFields, ___default_domain_10)); }
	inline AppDomain_t3842411093 * get_default_domain_10() const { return ___default_domain_10; }
	inline AppDomain_t3842411093 ** get_address_of_default_domain_10() { return &___default_domain_10; }
	inline void set_default_domain_10(AppDomain_t3842411093 * value)
	{
		___default_domain_10 = value;
		Il2CppCodeGenWriteBarrier((&___default_domain_10), value);
	}
};

struct AppDomain_t3842411093_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.AppDomain::type_resolve_in_progress
	Hashtable_t2354558714 * ___type_resolve_in_progress_3;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress
	Hashtable_t2354558714 * ___assembly_resolve_in_progress_4;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress_refonly
	Hashtable_t2354558714 * ___assembly_resolve_in_progress_refonly_5;
	// System.Security.Principal.IPrincipal System.AppDomain::_principal
	RuntimeObject* ____principal_9;

public:
	inline static int32_t get_offset_of_type_resolve_in_progress_3() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_ThreadStaticFields, ___type_resolve_in_progress_3)); }
	inline Hashtable_t2354558714 * get_type_resolve_in_progress_3() const { return ___type_resolve_in_progress_3; }
	inline Hashtable_t2354558714 ** get_address_of_type_resolve_in_progress_3() { return &___type_resolve_in_progress_3; }
	inline void set_type_resolve_in_progress_3(Hashtable_t2354558714 * value)
	{
		___type_resolve_in_progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_resolve_in_progress_3), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_4() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_ThreadStaticFields, ___assembly_resolve_in_progress_4)); }
	inline Hashtable_t2354558714 * get_assembly_resolve_in_progress_4() const { return ___assembly_resolve_in_progress_4; }
	inline Hashtable_t2354558714 ** get_address_of_assembly_resolve_in_progress_4() { return &___assembly_resolve_in_progress_4; }
	inline void set_assembly_resolve_in_progress_4(Hashtable_t2354558714 * value)
	{
		___assembly_resolve_in_progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_4), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_refonly_5() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_ThreadStaticFields, ___assembly_resolve_in_progress_refonly_5)); }
	inline Hashtable_t2354558714 * get_assembly_resolve_in_progress_refonly_5() const { return ___assembly_resolve_in_progress_refonly_5; }
	inline Hashtable_t2354558714 ** get_address_of_assembly_resolve_in_progress_refonly_5() { return &___assembly_resolve_in_progress_refonly_5; }
	inline void set_assembly_resolve_in_progress_refonly_5(Hashtable_t2354558714 * value)
	{
		___assembly_resolve_in_progress_refonly_5 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_refonly_5), value);
	}

	inline static int32_t get_offset_of__principal_9() { return static_cast<int32_t>(offsetof(AppDomain_t3842411093_ThreadStaticFields, ____principal_9)); }
	inline RuntimeObject* get__principal_9() const { return ____principal_9; }
	inline RuntimeObject** get_address_of__principal_9() { return &____principal_9; }
	inline void set__principal_9(RuntimeObject* value)
	{
		____principal_9 = value;
		Il2CppCodeGenWriteBarrier((&____principal_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPDOMAIN_T3842411093_H
#ifndef SHADER_T1881769421_H
#define SHADER_T1881769421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t1881769421  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T1881769421_H
#ifndef SPRITE_T2544745708_H
#define SPRITE_T2544745708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t2544745708  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T2544745708_H
#ifndef THREAD_T322231692_H
#define THREAD_T322231692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t322231692  : public CriticalFinalizerObject_t2012302976
{
public:
	// System.Int32 System.Threading.Thread::lock_thread_id
	int32_t ___lock_thread_id_0;
	// System.IntPtr System.Threading.Thread::system_thread_handle
	intptr_t ___system_thread_handle_1;
	// System.Object System.Threading.Thread::cached_culture_info
	RuntimeObject * ___cached_culture_info_2;
	// System.IntPtr System.Threading.Thread::unused0
	intptr_t ___unused0_3;
	// System.Boolean System.Threading.Thread::threadpool_thread
	bool ___threadpool_thread_4;
	// System.IntPtr System.Threading.Thread::name
	intptr_t ___name_5;
	// System.Int32 System.Threading.Thread::name_len
	int32_t ___name_len_6;
	// System.Threading.ThreadState System.Threading.Thread::state
	int32_t ___state_7;
	// System.Object System.Threading.Thread::abort_exc
	RuntimeObject * ___abort_exc_8;
	// System.Int32 System.Threading.Thread::abort_state_handle
	int32_t ___abort_state_handle_9;
	// System.Int64 System.Threading.Thread::thread_id
	int64_t ___thread_id_10;
	// System.IntPtr System.Threading.Thread::start_notify
	intptr_t ___start_notify_11;
	// System.IntPtr System.Threading.Thread::stack_ptr
	intptr_t ___stack_ptr_12;
	// System.UIntPtr System.Threading.Thread::static_data
	uintptr_t ___static_data_13;
	// System.IntPtr System.Threading.Thread::jit_data
	intptr_t ___jit_data_14;
	// System.IntPtr System.Threading.Thread::lock_data
	intptr_t ___lock_data_15;
	// System.Object System.Threading.Thread::current_appcontext
	RuntimeObject * ___current_appcontext_16;
	// System.Int32 System.Threading.Thread::stack_size
	int32_t ___stack_size_17;
	// System.Object System.Threading.Thread::start_obj
	RuntimeObject * ___start_obj_18;
	// System.IntPtr System.Threading.Thread::appdomain_refs
	intptr_t ___appdomain_refs_19;
	// System.Int32 System.Threading.Thread::interruption_requested
	int32_t ___interruption_requested_20;
	// System.IntPtr System.Threading.Thread::suspend_event
	intptr_t ___suspend_event_21;
	// System.IntPtr System.Threading.Thread::suspended_event
	intptr_t ___suspended_event_22;
	// System.IntPtr System.Threading.Thread::resume_event
	intptr_t ___resume_event_23;
	// System.IntPtr System.Threading.Thread::synch_cs
	intptr_t ___synch_cs_24;
	// System.IntPtr System.Threading.Thread::serialized_culture_info
	intptr_t ___serialized_culture_info_25;
	// System.Int32 System.Threading.Thread::serialized_culture_info_len
	int32_t ___serialized_culture_info_len_26;
	// System.IntPtr System.Threading.Thread::serialized_ui_culture_info
	intptr_t ___serialized_ui_culture_info_27;
	// System.Int32 System.Threading.Thread::serialized_ui_culture_info_len
	int32_t ___serialized_ui_culture_info_len_28;
	// System.Boolean System.Threading.Thread::thread_dump_requested
	bool ___thread_dump_requested_29;
	// System.IntPtr System.Threading.Thread::end_stack
	intptr_t ___end_stack_30;
	// System.Boolean System.Threading.Thread::thread_interrupt_requested
	bool ___thread_interrupt_requested_31;
	// System.Byte System.Threading.Thread::apartment_state
	uint8_t ___apartment_state_32;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Thread::critical_region_level
	int32_t ___critical_region_level_33;
	// System.Int32 System.Threading.Thread::small_id
	int32_t ___small_id_34;
	// System.IntPtr System.Threading.Thread::manage_callback
	intptr_t ___manage_callback_35;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_36;
	// System.Threading.ExecutionContext System.Threading.Thread::ec_to_set
	ExecutionContext_t935670543 * ___ec_to_set_37;
	// System.IntPtr System.Threading.Thread::interrupt_on_stop
	intptr_t ___interrupt_on_stop_38;
	// System.IntPtr System.Threading.Thread::unused3
	intptr_t ___unused3_39;
	// System.IntPtr System.Threading.Thread::unused4
	intptr_t ___unused4_40;
	// System.IntPtr System.Threading.Thread::unused5
	intptr_t ___unused5_41;
	// System.IntPtr System.Threading.Thread::unused6
	intptr_t ___unused6_42;
	// System.MulticastDelegate System.Threading.Thread::threadstart
	MulticastDelegate_t1652681189 * ___threadstart_45;
	// System.Int32 System.Threading.Thread::managed_id
	int32_t ___managed_id_46;
	// System.Security.Principal.IPrincipal System.Threading.Thread::_principal
	RuntimeObject* ____principal_47;
	// System.Boolean System.Threading.Thread::in_currentculture
	bool ___in_currentculture_50;

public:
	inline static int32_t get_offset_of_lock_thread_id_0() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___lock_thread_id_0)); }
	inline int32_t get_lock_thread_id_0() const { return ___lock_thread_id_0; }
	inline int32_t* get_address_of_lock_thread_id_0() { return &___lock_thread_id_0; }
	inline void set_lock_thread_id_0(int32_t value)
	{
		___lock_thread_id_0 = value;
	}

	inline static int32_t get_offset_of_system_thread_handle_1() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___system_thread_handle_1)); }
	inline intptr_t get_system_thread_handle_1() const { return ___system_thread_handle_1; }
	inline intptr_t* get_address_of_system_thread_handle_1() { return &___system_thread_handle_1; }
	inline void set_system_thread_handle_1(intptr_t value)
	{
		___system_thread_handle_1 = value;
	}

	inline static int32_t get_offset_of_cached_culture_info_2() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___cached_culture_info_2)); }
	inline RuntimeObject * get_cached_culture_info_2() const { return ___cached_culture_info_2; }
	inline RuntimeObject ** get_address_of_cached_culture_info_2() { return &___cached_culture_info_2; }
	inline void set_cached_culture_info_2(RuntimeObject * value)
	{
		___cached_culture_info_2 = value;
		Il2CppCodeGenWriteBarrier((&___cached_culture_info_2), value);
	}

	inline static int32_t get_offset_of_unused0_3() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___unused0_3)); }
	inline intptr_t get_unused0_3() const { return ___unused0_3; }
	inline intptr_t* get_address_of_unused0_3() { return &___unused0_3; }
	inline void set_unused0_3(intptr_t value)
	{
		___unused0_3 = value;
	}

	inline static int32_t get_offset_of_threadpool_thread_4() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___threadpool_thread_4)); }
	inline bool get_threadpool_thread_4() const { return ___threadpool_thread_4; }
	inline bool* get_address_of_threadpool_thread_4() { return &___threadpool_thread_4; }
	inline void set_threadpool_thread_4(bool value)
	{
		___threadpool_thread_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___name_5)); }
	inline intptr_t get_name_5() const { return ___name_5; }
	inline intptr_t* get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(intptr_t value)
	{
		___name_5 = value;
	}

	inline static int32_t get_offset_of_name_len_6() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___name_len_6)); }
	inline int32_t get_name_len_6() const { return ___name_len_6; }
	inline int32_t* get_address_of_name_len_6() { return &___name_len_6; }
	inline void set_name_len_6(int32_t value)
	{
		___name_len_6 = value;
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_abort_exc_8() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___abort_exc_8)); }
	inline RuntimeObject * get_abort_exc_8() const { return ___abort_exc_8; }
	inline RuntimeObject ** get_address_of_abort_exc_8() { return &___abort_exc_8; }
	inline void set_abort_exc_8(RuntimeObject * value)
	{
		___abort_exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___abort_exc_8), value);
	}

	inline static int32_t get_offset_of_abort_state_handle_9() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___abort_state_handle_9)); }
	inline int32_t get_abort_state_handle_9() const { return ___abort_state_handle_9; }
	inline int32_t* get_address_of_abort_state_handle_9() { return &___abort_state_handle_9; }
	inline void set_abort_state_handle_9(int32_t value)
	{
		___abort_state_handle_9 = value;
	}

	inline static int32_t get_offset_of_thread_id_10() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___thread_id_10)); }
	inline int64_t get_thread_id_10() const { return ___thread_id_10; }
	inline int64_t* get_address_of_thread_id_10() { return &___thread_id_10; }
	inline void set_thread_id_10(int64_t value)
	{
		___thread_id_10 = value;
	}

	inline static int32_t get_offset_of_start_notify_11() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___start_notify_11)); }
	inline intptr_t get_start_notify_11() const { return ___start_notify_11; }
	inline intptr_t* get_address_of_start_notify_11() { return &___start_notify_11; }
	inline void set_start_notify_11(intptr_t value)
	{
		___start_notify_11 = value;
	}

	inline static int32_t get_offset_of_stack_ptr_12() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___stack_ptr_12)); }
	inline intptr_t get_stack_ptr_12() const { return ___stack_ptr_12; }
	inline intptr_t* get_address_of_stack_ptr_12() { return &___stack_ptr_12; }
	inline void set_stack_ptr_12(intptr_t value)
	{
		___stack_ptr_12 = value;
	}

	inline static int32_t get_offset_of_static_data_13() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___static_data_13)); }
	inline uintptr_t get_static_data_13() const { return ___static_data_13; }
	inline uintptr_t* get_address_of_static_data_13() { return &___static_data_13; }
	inline void set_static_data_13(uintptr_t value)
	{
		___static_data_13 = value;
	}

	inline static int32_t get_offset_of_jit_data_14() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___jit_data_14)); }
	inline intptr_t get_jit_data_14() const { return ___jit_data_14; }
	inline intptr_t* get_address_of_jit_data_14() { return &___jit_data_14; }
	inline void set_jit_data_14(intptr_t value)
	{
		___jit_data_14 = value;
	}

	inline static int32_t get_offset_of_lock_data_15() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___lock_data_15)); }
	inline intptr_t get_lock_data_15() const { return ___lock_data_15; }
	inline intptr_t* get_address_of_lock_data_15() { return &___lock_data_15; }
	inline void set_lock_data_15(intptr_t value)
	{
		___lock_data_15 = value;
	}

	inline static int32_t get_offset_of_current_appcontext_16() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___current_appcontext_16)); }
	inline RuntimeObject * get_current_appcontext_16() const { return ___current_appcontext_16; }
	inline RuntimeObject ** get_address_of_current_appcontext_16() { return &___current_appcontext_16; }
	inline void set_current_appcontext_16(RuntimeObject * value)
	{
		___current_appcontext_16 = value;
		Il2CppCodeGenWriteBarrier((&___current_appcontext_16), value);
	}

	inline static int32_t get_offset_of_stack_size_17() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___stack_size_17)); }
	inline int32_t get_stack_size_17() const { return ___stack_size_17; }
	inline int32_t* get_address_of_stack_size_17() { return &___stack_size_17; }
	inline void set_stack_size_17(int32_t value)
	{
		___stack_size_17 = value;
	}

	inline static int32_t get_offset_of_start_obj_18() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___start_obj_18)); }
	inline RuntimeObject * get_start_obj_18() const { return ___start_obj_18; }
	inline RuntimeObject ** get_address_of_start_obj_18() { return &___start_obj_18; }
	inline void set_start_obj_18(RuntimeObject * value)
	{
		___start_obj_18 = value;
		Il2CppCodeGenWriteBarrier((&___start_obj_18), value);
	}

	inline static int32_t get_offset_of_appdomain_refs_19() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___appdomain_refs_19)); }
	inline intptr_t get_appdomain_refs_19() const { return ___appdomain_refs_19; }
	inline intptr_t* get_address_of_appdomain_refs_19() { return &___appdomain_refs_19; }
	inline void set_appdomain_refs_19(intptr_t value)
	{
		___appdomain_refs_19 = value;
	}

	inline static int32_t get_offset_of_interruption_requested_20() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___interruption_requested_20)); }
	inline int32_t get_interruption_requested_20() const { return ___interruption_requested_20; }
	inline int32_t* get_address_of_interruption_requested_20() { return &___interruption_requested_20; }
	inline void set_interruption_requested_20(int32_t value)
	{
		___interruption_requested_20 = value;
	}

	inline static int32_t get_offset_of_suspend_event_21() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___suspend_event_21)); }
	inline intptr_t get_suspend_event_21() const { return ___suspend_event_21; }
	inline intptr_t* get_address_of_suspend_event_21() { return &___suspend_event_21; }
	inline void set_suspend_event_21(intptr_t value)
	{
		___suspend_event_21 = value;
	}

	inline static int32_t get_offset_of_suspended_event_22() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___suspended_event_22)); }
	inline intptr_t get_suspended_event_22() const { return ___suspended_event_22; }
	inline intptr_t* get_address_of_suspended_event_22() { return &___suspended_event_22; }
	inline void set_suspended_event_22(intptr_t value)
	{
		___suspended_event_22 = value;
	}

	inline static int32_t get_offset_of_resume_event_23() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___resume_event_23)); }
	inline intptr_t get_resume_event_23() const { return ___resume_event_23; }
	inline intptr_t* get_address_of_resume_event_23() { return &___resume_event_23; }
	inline void set_resume_event_23(intptr_t value)
	{
		___resume_event_23 = value;
	}

	inline static int32_t get_offset_of_synch_cs_24() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___synch_cs_24)); }
	inline intptr_t get_synch_cs_24() const { return ___synch_cs_24; }
	inline intptr_t* get_address_of_synch_cs_24() { return &___synch_cs_24; }
	inline void set_synch_cs_24(intptr_t value)
	{
		___synch_cs_24 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_25() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___serialized_culture_info_25)); }
	inline intptr_t get_serialized_culture_info_25() const { return ___serialized_culture_info_25; }
	inline intptr_t* get_address_of_serialized_culture_info_25() { return &___serialized_culture_info_25; }
	inline void set_serialized_culture_info_25(intptr_t value)
	{
		___serialized_culture_info_25 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_len_26() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___serialized_culture_info_len_26)); }
	inline int32_t get_serialized_culture_info_len_26() const { return ___serialized_culture_info_len_26; }
	inline int32_t* get_address_of_serialized_culture_info_len_26() { return &___serialized_culture_info_len_26; }
	inline void set_serialized_culture_info_len_26(int32_t value)
	{
		___serialized_culture_info_len_26 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_27() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___serialized_ui_culture_info_27)); }
	inline intptr_t get_serialized_ui_culture_info_27() const { return ___serialized_ui_culture_info_27; }
	inline intptr_t* get_address_of_serialized_ui_culture_info_27() { return &___serialized_ui_culture_info_27; }
	inline void set_serialized_ui_culture_info_27(intptr_t value)
	{
		___serialized_ui_culture_info_27 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_len_28() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___serialized_ui_culture_info_len_28)); }
	inline int32_t get_serialized_ui_culture_info_len_28() const { return ___serialized_ui_culture_info_len_28; }
	inline int32_t* get_address_of_serialized_ui_culture_info_len_28() { return &___serialized_ui_culture_info_len_28; }
	inline void set_serialized_ui_culture_info_len_28(int32_t value)
	{
		___serialized_ui_culture_info_len_28 = value;
	}

	inline static int32_t get_offset_of_thread_dump_requested_29() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___thread_dump_requested_29)); }
	inline bool get_thread_dump_requested_29() const { return ___thread_dump_requested_29; }
	inline bool* get_address_of_thread_dump_requested_29() { return &___thread_dump_requested_29; }
	inline void set_thread_dump_requested_29(bool value)
	{
		___thread_dump_requested_29 = value;
	}

	inline static int32_t get_offset_of_end_stack_30() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___end_stack_30)); }
	inline intptr_t get_end_stack_30() const { return ___end_stack_30; }
	inline intptr_t* get_address_of_end_stack_30() { return &___end_stack_30; }
	inline void set_end_stack_30(intptr_t value)
	{
		___end_stack_30 = value;
	}

	inline static int32_t get_offset_of_thread_interrupt_requested_31() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___thread_interrupt_requested_31)); }
	inline bool get_thread_interrupt_requested_31() const { return ___thread_interrupt_requested_31; }
	inline bool* get_address_of_thread_interrupt_requested_31() { return &___thread_interrupt_requested_31; }
	inline void set_thread_interrupt_requested_31(bool value)
	{
		___thread_interrupt_requested_31 = value;
	}

	inline static int32_t get_offset_of_apartment_state_32() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___apartment_state_32)); }
	inline uint8_t get_apartment_state_32() const { return ___apartment_state_32; }
	inline uint8_t* get_address_of_apartment_state_32() { return &___apartment_state_32; }
	inline void set_apartment_state_32(uint8_t value)
	{
		___apartment_state_32 = value;
	}

	inline static int32_t get_offset_of_critical_region_level_33() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___critical_region_level_33)); }
	inline int32_t get_critical_region_level_33() const { return ___critical_region_level_33; }
	inline int32_t* get_address_of_critical_region_level_33() { return &___critical_region_level_33; }
	inline void set_critical_region_level_33(int32_t value)
	{
		___critical_region_level_33 = value;
	}

	inline static int32_t get_offset_of_small_id_34() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___small_id_34)); }
	inline int32_t get_small_id_34() const { return ___small_id_34; }
	inline int32_t* get_address_of_small_id_34() { return &___small_id_34; }
	inline void set_small_id_34(int32_t value)
	{
		___small_id_34 = value;
	}

	inline static int32_t get_offset_of_manage_callback_35() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___manage_callback_35)); }
	inline intptr_t get_manage_callback_35() const { return ___manage_callback_35; }
	inline intptr_t* get_address_of_manage_callback_35() { return &___manage_callback_35; }
	inline void set_manage_callback_35(intptr_t value)
	{
		___manage_callback_35 = value;
	}

	inline static int32_t get_offset_of_pending_exception_36() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___pending_exception_36)); }
	inline RuntimeObject * get_pending_exception_36() const { return ___pending_exception_36; }
	inline RuntimeObject ** get_address_of_pending_exception_36() { return &___pending_exception_36; }
	inline void set_pending_exception_36(RuntimeObject * value)
	{
		___pending_exception_36 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_36), value);
	}

	inline static int32_t get_offset_of_ec_to_set_37() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___ec_to_set_37)); }
	inline ExecutionContext_t935670543 * get_ec_to_set_37() const { return ___ec_to_set_37; }
	inline ExecutionContext_t935670543 ** get_address_of_ec_to_set_37() { return &___ec_to_set_37; }
	inline void set_ec_to_set_37(ExecutionContext_t935670543 * value)
	{
		___ec_to_set_37 = value;
		Il2CppCodeGenWriteBarrier((&___ec_to_set_37), value);
	}

	inline static int32_t get_offset_of_interrupt_on_stop_38() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___interrupt_on_stop_38)); }
	inline intptr_t get_interrupt_on_stop_38() const { return ___interrupt_on_stop_38; }
	inline intptr_t* get_address_of_interrupt_on_stop_38() { return &___interrupt_on_stop_38; }
	inline void set_interrupt_on_stop_38(intptr_t value)
	{
		___interrupt_on_stop_38 = value;
	}

	inline static int32_t get_offset_of_unused3_39() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___unused3_39)); }
	inline intptr_t get_unused3_39() const { return ___unused3_39; }
	inline intptr_t* get_address_of_unused3_39() { return &___unused3_39; }
	inline void set_unused3_39(intptr_t value)
	{
		___unused3_39 = value;
	}

	inline static int32_t get_offset_of_unused4_40() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___unused4_40)); }
	inline intptr_t get_unused4_40() const { return ___unused4_40; }
	inline intptr_t* get_address_of_unused4_40() { return &___unused4_40; }
	inline void set_unused4_40(intptr_t value)
	{
		___unused4_40 = value;
	}

	inline static int32_t get_offset_of_unused5_41() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___unused5_41)); }
	inline intptr_t get_unused5_41() const { return ___unused5_41; }
	inline intptr_t* get_address_of_unused5_41() { return &___unused5_41; }
	inline void set_unused5_41(intptr_t value)
	{
		___unused5_41 = value;
	}

	inline static int32_t get_offset_of_unused6_42() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___unused6_42)); }
	inline intptr_t get_unused6_42() const { return ___unused6_42; }
	inline intptr_t* get_address_of_unused6_42() { return &___unused6_42; }
	inline void set_unused6_42(intptr_t value)
	{
		___unused6_42 = value;
	}

	inline static int32_t get_offset_of_threadstart_45() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___threadstart_45)); }
	inline MulticastDelegate_t1652681189 * get_threadstart_45() const { return ___threadstart_45; }
	inline MulticastDelegate_t1652681189 ** get_address_of_threadstart_45() { return &___threadstart_45; }
	inline void set_threadstart_45(MulticastDelegate_t1652681189 * value)
	{
		___threadstart_45 = value;
		Il2CppCodeGenWriteBarrier((&___threadstart_45), value);
	}

	inline static int32_t get_offset_of_managed_id_46() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___managed_id_46)); }
	inline int32_t get_managed_id_46() const { return ___managed_id_46; }
	inline int32_t* get_address_of_managed_id_46() { return &___managed_id_46; }
	inline void set_managed_id_46(int32_t value)
	{
		___managed_id_46 = value;
	}

	inline static int32_t get_offset_of__principal_47() { return static_cast<int32_t>(offsetof(Thread_t322231692, ____principal_47)); }
	inline RuntimeObject* get__principal_47() const { return ____principal_47; }
	inline RuntimeObject** get_address_of__principal_47() { return &____principal_47; }
	inline void set__principal_47(RuntimeObject* value)
	{
		____principal_47 = value;
		Il2CppCodeGenWriteBarrier((&____principal_47), value);
	}

	inline static int32_t get_offset_of_in_currentculture_50() { return static_cast<int32_t>(offsetof(Thread_t322231692, ___in_currentculture_50)); }
	inline bool get_in_currentculture_50() const { return ___in_currentculture_50; }
	inline bool* get_address_of_in_currentculture_50() { return &___in_currentculture_50; }
	inline void set_in_currentculture_50(bool value)
	{
		___in_currentculture_50 = value;
	}
};

struct Thread_t322231692_StaticFields
{
public:
	// System.Collections.Hashtable System.Threading.Thread::datastorehash
	Hashtable_t2354558714 * ___datastorehash_48;
	// System.Object System.Threading.Thread::datastore_lock
	RuntimeObject * ___datastore_lock_49;
	// System.Object System.Threading.Thread::culture_lock
	RuntimeObject * ___culture_lock_51;

public:
	inline static int32_t get_offset_of_datastorehash_48() { return static_cast<int32_t>(offsetof(Thread_t322231692_StaticFields, ___datastorehash_48)); }
	inline Hashtable_t2354558714 * get_datastorehash_48() const { return ___datastorehash_48; }
	inline Hashtable_t2354558714 ** get_address_of_datastorehash_48() { return &___datastorehash_48; }
	inline void set_datastorehash_48(Hashtable_t2354558714 * value)
	{
		___datastorehash_48 = value;
		Il2CppCodeGenWriteBarrier((&___datastorehash_48), value);
	}

	inline static int32_t get_offset_of_datastore_lock_49() { return static_cast<int32_t>(offsetof(Thread_t322231692_StaticFields, ___datastore_lock_49)); }
	inline RuntimeObject * get_datastore_lock_49() const { return ___datastore_lock_49; }
	inline RuntimeObject ** get_address_of_datastore_lock_49() { return &___datastore_lock_49; }
	inline void set_datastore_lock_49(RuntimeObject * value)
	{
		___datastore_lock_49 = value;
		Il2CppCodeGenWriteBarrier((&___datastore_lock_49), value);
	}

	inline static int32_t get_offset_of_culture_lock_51() { return static_cast<int32_t>(offsetof(Thread_t322231692_StaticFields, ___culture_lock_51)); }
	inline RuntimeObject * get_culture_lock_51() const { return ___culture_lock_51; }
	inline RuntimeObject ** get_address_of_culture_lock_51() { return &___culture_lock_51; }
	inline void set_culture_lock_51(RuntimeObject * value)
	{
		___culture_lock_51 = value;
		Il2CppCodeGenWriteBarrier((&___culture_lock_51), value);
	}
};

struct Thread_t322231692_ThreadStaticFields
{
public:
	// System.Object[] System.Threading.Thread::local_slots
	ObjectU5BU5D_t2737604620* ___local_slots_43;
	// System.Threading.ExecutionContext System.Threading.Thread::_ec
	ExecutionContext_t935670543 * ____ec_44;

public:
	inline static int32_t get_offset_of_local_slots_43() { return static_cast<int32_t>(offsetof(Thread_t322231692_ThreadStaticFields, ___local_slots_43)); }
	inline ObjectU5BU5D_t2737604620* get_local_slots_43() const { return ___local_slots_43; }
	inline ObjectU5BU5D_t2737604620** get_address_of_local_slots_43() { return &___local_slots_43; }
	inline void set_local_slots_43(ObjectU5BU5D_t2737604620* value)
	{
		___local_slots_43 = value;
		Il2CppCodeGenWriteBarrier((&___local_slots_43), value);
	}

	inline static int32_t get_offset_of__ec_44() { return static_cast<int32_t>(offsetof(Thread_t322231692_ThreadStaticFields, ____ec_44)); }
	inline ExecutionContext_t935670543 * get__ec_44() const { return ____ec_44; }
	inline ExecutionContext_t935670543 ** get_address_of__ec_44() { return &____ec_44; }
	inline void set__ec_44(ExecutionContext_t935670543 * value)
	{
		____ec_44 = value;
		Il2CppCodeGenWriteBarrier((&____ec_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_T322231692_H
#ifndef PARAMETERINFO_T3943516840_H
#define PARAMETERINFO_T3943516840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3943516840  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t2394797874 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3943516840, ___marshalAs_6)); }
	inline UnmanagedMarshal_t2394797874 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t2394797874 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t2394797874 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3943516840_H
#ifndef STREAMINGCONTEXT_T648298805_H
#define STREAMINGCONTEXT_T648298805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t648298805 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t648298805, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t648298805, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t648298805_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t648298805_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T648298805_H
#ifndef SPRITEATLAS_T1129391324_H
#define SPRITEATLAS_T1129391324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlas
struct  SpriteAtlas_t1129391324  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLAS_T1129391324_H
#ifndef TOUCH_T1048744301_H
#define TOUCH_T1048744301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1048744301 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t328513675  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t328513675  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t328513675  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Position_1)); }
	inline Vector2_t328513675  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t328513675 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t328513675  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_RawPosition_2)); }
	inline Vector2_t328513675  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t328513675 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t328513675  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_PositionDelta_3)); }
	inline Vector2_t328513675  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t328513675 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t328513675  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1048744301, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1048744301_H
#ifndef TEXTASSET_T2052027493_H
#define TEXTASSET_T2052027493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAsset
struct  TextAsset_t2052027493  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTASSET_T2052027493_H
#ifndef TEXTURE_T2119925672_H
#define TEXTURE_T2119925672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2119925672  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2119925672_H
#ifndef EVENTWAITHANDLE_T891147241_H
#define EVENTWAITHANDLE_T891147241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.EventWaitHandle
struct  EventWaitHandle_t891147241  : public WaitHandle_t283688518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTWAITHANDLE_T891147241_H
#ifndef REQUESTATLASCALLBACK_T1427750696_H
#define REQUESTATLASCALLBACK_T1427750696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct  RequestAtlasCallback_t1427750696  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTATLASCALLBACK_T1427750696_H
#ifndef SENDORPOSTCALLBACK_T2349539412_H
#define SENDORPOSTCALLBACK_T2349539412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SendOrPostCallback
struct  SendOrPostCallback_t2349539412  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDORPOSTCALLBACK_T2349539412_H
#ifndef MANUALRESETEVENT_T504895258_H
#define MANUALRESETEVENT_T504895258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ManualResetEvent
struct  ManualResetEvent_t504895258  : public EventWaitHandle_t891147241
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALRESETEVENT_T504895258_H
#ifndef ACTION_1_T3400447372_H
#define ACTION_1_T3400447372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct  Action_1_t3400447372  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3400447372_H
#ifndef RENDERER_T1418648713_H
#define RENDERER_T1418648713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t1418648713  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T1418648713_H
#ifndef ASYNCCALLBACK_T3561663063_H
#define ASYNCCALLBACK_T3561663063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3561663063  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3561663063_H
#ifndef RENDERTEXTURE_T971269558_H
#define RENDERTEXTURE_T971269558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t971269558  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T971269558_H
#ifndef TRANSFORM_T362059596_H
#define TRANSFORM_T362059596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t362059596  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T362059596_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef UNHANDLEDEXCEPTIONEVENTHANDLER_T3749840881_H
#define UNHANDLEDEXCEPTIONEVENTHANDLER_T3749840881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t3749840881  : public MulticastDelegate_t1652681189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTHANDLER_T3749840881_H
#ifndef TEXTURE3D_T2628067514_H
#define TEXTURE3D_T2628067514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture3D
struct  Texture3D_t2628067514  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE3D_T2628067514_H
#ifndef TEXTURE2D_T3063074017_H
#define TEXTURE2D_T3063074017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3063074017  : public Texture_t2119925672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3063074017_H
#ifndef SKINNEDMESHRENDERER_T2250625413_H
#define SKINNEDMESHRENDERER_T2250625413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SkinnedMeshRenderer
struct  SkinnedMeshRenderer_t2250625413  : public Renderer_t1418648713
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINNEDMESHRENDERER_T2250625413_H
#ifndef GUIELEMENT_T2946029536_H
#define GUIELEMENT_T2946029536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t2946029536  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T2946029536_H
#ifndef GUILAYER_T478658086_H
#define GUILAYER_T478658086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t478658086  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T478658086_H
#ifndef CAMERA_T2839736942_H
#define CAMERA_T2839736942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t2839736942  : public Behaviour_t2850977393
{
public:

public:
};

struct Camera_t2839736942_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t657915608 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t657915608 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t657915608 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t657915608 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t657915608 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t657915608 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t657915608 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t657915608 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t657915608 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t2839736942_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t657915608 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t657915608 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t657915608 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T2839736942_H
#ifndef SPRITERENDERER_T1432209864_H
#define SPRITERENDERER_T1432209864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t1432209864  : public Renderer_t1418648713
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T1432209864_H
#ifndef RECTTRANSFORM_T859616204_H
#define RECTTRANSFORM_T859616204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t859616204  : public Transform_t362059596
{
public:

public:
};

struct RectTransform_t859616204_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t2209604263 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t859616204_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t2209604263 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t2209604263 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t2209604263 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T859616204_H
// UnityEngine.Camera[]
struct CameraU5BU5D_t4045078555  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t2839736942 * m_Items[1];

public:
	inline Camera_t2839736942 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t2839736942 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t2839736942 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t2839736942 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t2839736942 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t2839736942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t2568675203  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HitInfo_t1469516070  m_Items[1];

public:
	inline HitInfo_t1469516070  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HitInfo_t1469516070 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HitInfo_t1469516070  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HitInfo_t1469516070  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HitInfo_t1469516070 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HitInfo_t1469516070  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2737604620  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t2531273172  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t999651945  m_Items[1];

public:
	inline ParameterModifier_t999651945  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t999651945 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t999651945  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t999651945  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t999651945 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t999651945  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t2511808107  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3419619864  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t3316460985  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3943516840 * m_Items[1];

public:
	inline ParameterInfo_t3943516840 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3943516840 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3943516840 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3943516840 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3943516840 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3943516840 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t434619169  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t247935167  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2582018970  m_Items[1];

public:
	inline Color_t2582018970  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2582018970 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2582018970  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2582018970  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2582018970 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2582018970  value)
	{
		m_Items[index] = value;
	}
};


// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2360518480_gshared (Component_t1632713610 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2674952980_gshared (Action_1_t4119957313 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m528467501_gshared (Queue_1_t1480213181 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
extern "C"  WorkRequest_t1716204206  Queue_1_Dequeue_m1469166360_gshared (Queue_1_t1480213181 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m1367379469_gshared (Queue_1_t1480213181 * __this, const RuntimeMethod* method);

// T UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t478658086_m1987472637(__this, method) ((  GUILayer_t478658086 * (*) (Component_t1632713610 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2360518480_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3123183460 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t2946029536 * GUILayer_HitTest_m2110564057 (GUILayer_t478658086 * __this, Vector3_t1986933152  ___screenPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2557347079 * Component_get_gameObject_m1032427207 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t1986933152  Input_get_mousePosition_m4034714020 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1216147617 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m3041928650 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t4045078555* ___cameras0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___x0, Object_t692178351 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t971269558 * Camera_get_targetTexture_m478696062 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1032523438 (RuntimeObject * __this /* static, unused */, Object_t692178351 * ___x0, Object_t692178351 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3039462994  Camera_get_pixelRect_m3077452777 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1261928192 (Rect_t3039462994 * __this, Vector3_t1986933152  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m267751607 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___camera0, Vector3_t1986933152  ___mousePosition1, HitInfo_t1469516070 * ___hitInfo2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3749265454 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t249333749  Camera_ScreenPointToRay_m1212959324 (Camera_t2839736942 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t1986933152  Ray_get_direction_m2429538176 (Ray_t249333749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m3559106540 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m713887456 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m249543630 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m3464126921 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_RaycastTry_m736311948 (Camera_t2839736942 * __this, Ray_t249333749  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m950950364 (Camera_t2839736942 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2557347079 * Camera_RaycastTry2D_m3310148661 (Camera_t2839736942 * __this, Ray_t249333749  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m814760037 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t1469516070  ___hit1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m1898615268 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m823933278 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m3774618170 (RuntimeObject * __this /* static, unused */, HitInfo_t1469516070  ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m364563472 (HitInfo_t1469516070 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m3678827560 (RuntimeObject * __this /* static, unused */, HitInfo_t1469516070  ___lhs0, HitInfo_t1469516070  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m814261268 (GameObject_t2557347079 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m2479467358 (Attribute_t1924466020 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1348886383 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m4009562449 (ArgumentException_t1812645948 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m803950198 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m2972540513 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m4147771927 (Object_t692178351 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1725140046 (PropertyAttribute_t381499487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t2436299922  Vector4_get_zero_m1931160304 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
extern "C"  Sprite_t2544745708 * Sprite_INTERNAL_CALL_Create_m140032498 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Rect_t3039462994 * ___rect1, Vector2_t328513675 * ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, Vector4_t2436299922 * ___border6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m2187623600 (Sprite_t2544745708 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m3988265876 (Sprite_t2544745708 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m1535245036 (Sprite_t2544745708 * __this, Vector4_t2436299922 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m826210465 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m1892881781 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m3550847450 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector2_t328513675 * ___output1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m2628855948 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Int32,System.Boolean)
extern "C"  void StackTrace__ctor_m2103493457 (StackTrace_t4180877726 * __this, int32_t p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m4260390550 (RuntimeObject * __this /* static, unused */, StackTrace_t4180877726 * ___stackTrace0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1698616109 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m560113655 (ArgumentException_t1812645948 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m315776189 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m1594975206 (StringBuilder_t622404039 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1542845994 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Exception::GetType()
extern "C"  Type_t * Exception_GetType_m2106978941 (Exception_t4086964929 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim()
extern "C"  String_t* String_Trim_m2686817291 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2395435812 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::get_InnerException()
extern "C"  Exception_t4086964929 * Exception_get_InnerException_m1086399415 (Exception_t4086964929 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1568077200 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t622404039 * StringBuilder_Append_m3980566461 (StringBuilder_t622404039 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t2511808107* String_Split_m1115057867 (String_t* __this, CharU5BU5D_t3419619864* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4268332576 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m1261986193 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m3326695972 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1881321258 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m3111453544 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Remove(System.Int32,System.Int32)
extern "C"  String_t* String_Remove_m4139867499 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String,System.Int32)
extern "C"  int32_t String_IndexOf_m873403889 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.Char,System.Char)
extern "C"  String_t* String_Replace_m3191968003 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.String)
extern "C"  int32_t String_LastIndexOf_m3162040027 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Insert(System.Int32,System.String)
extern "C"  String_t* String_Insert_m1500258161 (String_t* __this, int32_t p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1683246493 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2781354364 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m1789449816 (TextAsset_t2052027493 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m2443978672 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m3397592417 (Exception_t4086964929 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m2937360541 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m3241763420 (Texture_t2119925672 * __this, Vector2_t328513675 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m373917183 (Texture_t2119925672 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m1101533493 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_SetPixel_m1365054379 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___self0, int32_t ___x1, int32_t ___y2, Color_t2582018970 * ___color3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___self0, float ___u1, float ___v2, Color_t2582018970 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1098893380 (Texture2D_t3063074017 * __this, ColorU5BU5D_t247935167* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1623044052 (Texture2D_t3063074017 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t247935167* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t247935167* Texture2D_GetPixels_m4026066991 (Texture2D_t3063074017 * __this, int32_t ___miplevel0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t247935167* Texture2D_GetPixels_m1645989123 (Texture2D_t3063074017 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m1860200133 (Texture2D_t3063074017 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture3D_Internal_Create_m1869539287 (RuntimeObject * __this /* static, unused */, Texture3D_t2628067514 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___depth3, int32_t ___format4, bool ___mipmap5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture3D_SetPixels_m4146233771 (Texture3D_t2628067514 * __this, ColorU5BU5D_t247935167* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture3D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture3D_Apply_m2534196399 (Texture3D_t2628067514 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m2127391186 (Touch_t1048744301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t328513675  Touch_get_position_m736668558 (Touch_t1048744301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m475972970 (Touch_t1048744301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m637716675 (Touch_t1048744301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2968844594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Object)
extern "C"  uint32_t Convert_ToUInt32_m1514246592 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Boolean)
extern "C"  uint32_t Convert_ToUInt32_m4047979761 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743 (TouchScreenKeyboard_t1357543767 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m2451490838 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m350649604 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m4080113311 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1357543767 * TouchScreenKeyboard_Open_m1341367847 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3418144028 (TouchScreenKeyboard_t1357543767 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m820781723 (TouchScreenKeyboard_t1357543767 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m2981311363 (RuntimeObject * __this /* static, unused */, TrackedReference_t2624196464 * ___x0, TrackedReference_t2624196464 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  int32_t IntPtr_op_Explicit_m1509168872 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m1184908066 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m14402994 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m2987532667 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m4096849723 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m1215190010 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t704191599  Transform_get_rotation_m317637018 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t1986933152  Quaternion_get_eulerAngles_m2988902996 (Quaternion_t704191599 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t704191599  Quaternion_Euler_m3333075930 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___euler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m609809763 (Transform_t362059596 * __this, Quaternion_t704191599  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m2730169435 (Transform_t362059596 * __this, Quaternion_t704191599  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t1986933152  Vector3_get_right_m878387312 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Quaternion_op_Multiply_m2036929790 (RuntimeObject * __this /* static, unused */, Quaternion_t704191599  ___rotation0, Vector3_t1986933152  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t704191599  Quaternion_FromToRotation_m2140668740 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___fromDirection0, Vector3_t1986933152  ___toDirection1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t1986933152  Vector3_get_up_m467978780 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t1986933152  Vector3_get_forward_m953890947 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m3071042677 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2478131545 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m3917542876 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m3633882113 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m3696539245 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m1480886321 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t362059596 * Transform_get_parentInternal_m2338151590 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m1775422903 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t692178351 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m2226751317 (Transform_t362059596 * __this, Transform_t362059596 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m1496610538 (Transform_t362059596 * __this, Transform_t362059596 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m577067920 (Transform_t362059596 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m3833575919 (Transform_t362059596 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m219233999 (Transform_t362059596 * __this, Vector3_t1986933152  ___translation0, int32_t ___relativeTo1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t1986933152  Transform_get_position_m3101184820 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Addition_m1125374618 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2724629716 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_TransformDirection_m4117630985 (Transform_t362059596 * __this, Vector3_t1986933152  ___direction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m4266981785 (Transform_t362059596 * __this, Vector3_t1986933152  ___eulerAngles0, int32_t ___relativeTo1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t704191599  Quaternion_Euler_m1199696711 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t704191599  Transform_get_localRotation_m934836654 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t704191599  Quaternion_op_Multiply_m3500285847 (RuntimeObject * __this /* static, unused */, Quaternion_t704191599  ___lhs0, Quaternion_t704191599  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t704191599  Quaternion_Inverse_m2151856247 (RuntimeObject * __this /* static, unused */, Quaternion_t704191599  ___rotation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m3984935415 (Transform_t362059596 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, int32_t ___relativeTo3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3282197930 (Vector3_t1986933152 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
extern "C"  void Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___axis1, float ___angle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m2802219837 (Transform_t362059596 * __this, Vector3_t1986933152  ___axis0, float ___angle1, int32_t ___relativeTo2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t362059596 * Component_get_transform_m520192871 (Component_t1632713610 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAroundInternal_m3765567956 (Transform_t362059596 * __this, Vector3_t1986933152  ___axis0, float ___angle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t704191599  Quaternion_AngleAxis_m1713259603 (RuntimeObject * __this /* static, unused */, float ___angle0, Vector3_t1986933152  ___axis1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Subtraction_m1182848491 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m169160536 (Transform_t362059596 * __this, Transform_t362059596 * ___target0, Vector3_t1986933152  ___worldUp1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m4187688555 (Transform_t362059596 * __this, Vector3_t1986933152  ___worldPosition0, Vector3_t1986933152  ___worldUp1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_LookAt_m4110212569 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___worldPosition1, Vector3_t1986933152 * ___worldUp2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m3141765377 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___direction1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m925141460 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___direction1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformVector_m3369314559 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___vector1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m2509381006 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m324663584 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_lossyScale_m3090738516 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3133563284 (Enumerator_t261617688 * __this, Transform_t362059596 * ___outer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t362059596 * Transform_GetChild_m260943136 (Transform_t362059596 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m3678280669 (Transform_t362059596 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.U2D.SpriteAtlas>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2452183723(__this, p0, p1, method) ((  void (*) (Action_1_t3400447372 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m2674952980_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m3858662825 (RequestAtlasCallback_t1427750696 * __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C"  AppDomain_t3842411093 * AppDomain_get_CurrentDomain_m467664504 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UnhandledExceptionEventHandler__ctor_m3638669703 (UnhandledExceptionEventHandler_t3749840881 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C"  void AppDomain_add_UnhandledException_m308340575 (AppDomain_t3842411093 * __this, UnhandledExceptionEventHandler_t3749840881 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern "C"  RuntimeObject * UnhandledExceptionEventArgs_get_ExceptionObject_m2034580413 (UnhandledExceptionEventArgs_t1939597804 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m72583591 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t4086964929 * ___e1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1994504921 (RuntimeObject * __this /* static, unused */, Exception_t4086964929 * ___exception0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HResult(System.Int32)
extern "C"  void Exception_set_HResult_m2899388954 (Exception_t4086964929 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Exception__ctor_m2650806397 (Exception_t4086964929 * __this, SerializationInfo_t259346668 * p0, StreamingContext_t648298805  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::.ctor()
extern "C"  void TextWriter__ctor_m1383723067 (TextWriter_t997444721 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m3298642088 (UnityLogWriter_t1450454900 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetOut(System.IO.TextWriter)
extern "C"  void Console_SetOut_m4234273152 (RuntimeObject * __this /* static, unused */, TextWriter_t997444721 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C"  Encoding_t4004889433 * Encoding_get_UTF8_m3585116113 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m910852106 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m3425010958 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m29320051 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2737604620* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
#define Queue_1__ctor_m528467501(__this, p0, method) ((  void (*) (Queue_1_t1480213181 *, int32_t, const RuntimeMethod*))Queue_1__ctor_m528467501_gshared)(__this, p0, method)
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t322231692 * Thread_get_CurrentThread_m1475819766 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m2189280200 (Thread_t322231692 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::.ctor()
extern "C"  void SynchronizationContext__ctor_m3027455558 (SynchronizationContext_t2274409467 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m1950427727 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
#define Queue_1_Dequeue_m1469166360(__this, method) ((  WorkRequest_t1716204206  (*) (Queue_1_t1480213181 *, const RuntimeMethod*))Queue_1_Dequeue_m1469166360_gshared)(__this, method)
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m2597236312 (WorkRequest_t1716204206 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
#define Queue_1_get_Count_m1367379469(__this, method) ((  int32_t (*) (Queue_1_t1480213181 *, const RuntimeMethod*))Queue_1_get_Count_m1367379469_gshared)(__this, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m1791852365 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
extern "C"  SynchronizationContext_t2274409467 * SynchronizationContext_get_Current_m2659194758 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m3918494648 (UnitySynchronizationContext_t485758571 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetSynchronizationContext(System.Threading.SynchronizationContext)
extern "C"  void SynchronizationContext_SetSynchronizationContext_m190080245 (RuntimeObject * __this /* static, unused */, SynchronizationContext_t2274409467 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m2533712166 (UnitySynchronizationContext_t485758571 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SendOrPostCallback::Invoke(System.Object)
extern "C"  void SendOrPostCallback_Invoke_m1935197691 (SendOrPostCallback_t2349539412 * __this, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m724129446 (EventWaitHandle_t891147241 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m22874897 (Vector2_t328513675 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m1303321160 (IndexOutOfRangeException_t2909334802 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m1634634565 (Vector2_t328513675 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m2125498180 (Vector2_t328513675 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::Set(System.Single,System.Single)
extern "C"  void Vector2_Set_m1028988183 (Vector2_t328513675 * __this, float ___newX0, float ___newY1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2193645629 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::Scale(UnityEngine.Vector2)
extern "C"  void Vector2_Scale_m1155063387 (Vector2_t328513675 * __this, Vector2_t328513675  ___scale0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m3070475644 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t328513675  Vector2_op_Division_m4163545787 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t328513675  Vector2_get_zero_m187309959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::Normalize()
extern "C"  void Vector2_Normalize_m2506338390 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t328513675  Vector2_get_normalized_m2925455182 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3950186024 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m493646592 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m356387322 (float* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString(System.String)
extern "C"  String_t* Vector2_ToString_m1956738200 (Vector2_t328513675 * __this, String_t* ___format0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m2097607317 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m3492920259 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m2960811275 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m3452260837 (Vector2_t328513675 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m3613460260 (Vector2_t328513675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t328513675  Vector2_op_Subtraction_m3140329220 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m3259744715 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___lhs0, Vector2_t328513675  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m4040229299 (Vector3_t1986933152 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)
extern "C"  void Vector3_INTERNAL_CALL_Slerp_m4141590721 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___a0, Vector3_t1986933152 * ___b1, float ___t2, Vector3_t1986933152 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern "C"  float Vector3_get_Item_m3834540881 (Vector3_t1986933152 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern "C"  void Vector3_set_Item_m4263389426 (Vector3_t1986933152 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Set(System.Single,System.Single,System.Single)
extern "C"  void Vector3_Set_m1703619586 (Vector3_t1986933152 * __this, float ___newX0, float ___newY1, float ___newZ2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
extern "C"  void Vector3_Scale_m463428114 (Vector3_t1986933152 * __this, Vector3_t1986933152  ___scale0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m4139137898 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m3593875679 (Vector3_t1986933152 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C"  float Vector3_Magnitude_m1454878665 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___vector0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_op_Division_m2151251249 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t1986933152  Vector3_get_zero_m2755426107 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3915644803 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Normalize_m2417917258 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t1986933152  Vector3_get_normalized_m1493202271 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2502327341 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1298020578 (RuntimeObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m124294925 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_op_Multiply_m952283854 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m1161157060 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m2485682987 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m1363999114 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m1775679755 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___vector0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m511918437 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m3238424746 (Vector3_t1986933152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1296751250 (Vector4_t2436299922 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m3649974304 (Vector4_t2436299922 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern "C"  void Vector4_set_Item_m3281778497 (Vector4_t2436299922 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3459216764 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m754502683 (Vector4_t2436299922 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Magnitude(UnityEngine.Vector4)
extern "C"  float Vector4_Magnitude_m3047559685 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2436299922  Vector4_op_Division_m837022830 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Normalize(UnityEngine.Vector4)
extern "C"  Vector4_t2436299922  Vector4_Normalize_m4277572146 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_normalized()
extern "C"  Vector4_t2436299922  Vector4_get_normalized_m1792997498 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2342208158 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, Vector4_t2436299922  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m2365501330 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2436299922  Vector4_op_Subtraction_m941026247 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, Vector4_t2436299922  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m4263254105 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m1687602531 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___lhs0, Vector4_t2436299922  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2547591814 (Vector4_t2436299922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m874787853 (YieldInstruction_t3270995273 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m900305041 (CustomYieldInstruction_t2273300952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m101132769 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Stack::.ctor()
extern "C"  void Stack__ctor_m3569009555 (Stack_t535311253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Object,System.Reflection.MethodInfo)
extern "C"  Delegate_t3882343965 * Delegate_CreateDelegate_m3618370599 (RuntimeObject * __this /* static, unused */, Type_t * p0, RuntimeObject * p1, MethodInfo_t * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Delegate::get_Method()
extern "C"  MethodInfo_t * Delegate_get_Method_m158526026 (Delegate_t3882343965 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t3882343965 * Delegate_CreateDelegate_m2822449990 (RuntimeObject * __this /* static, unused */, Type_t * p0, MethodInfo_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m2160180413 (TypeInferenceRuleAttribute_t2902006326 * __this, String_t* ___rule0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern "C"  void SendMouseEvents_SetMouseMoved_m2678903946 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m2678903946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m267751607 (RuntimeObject * __this /* static, unused */, Camera_t2839736942 * ___camera0, Vector3_t1986933152  ___mousePosition1, HitInfo_t1469516070 * ___hitInfo2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_HitTestLegacyGUI_m267751607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUILayer_t478658086 * V_0 = NULL;
	GUIElement_t2946029536 * V_1 = NULL;
	{
		Camera_t2839736942 * L_0 = ___camera0;
		NullCheck(L_0);
		GUILayer_t478658086 * L_1 = Component_GetComponent_TisGUILayer_t478658086_m1987472637(L_0, /*hidden argument*/Component_GetComponent_TisGUILayer_t478658086_m1987472637_RuntimeMethod_var);
		V_0 = L_1;
		GUILayer_t478658086 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m3123183460(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		GUILayer_t478658086 * L_4 = V_0;
		Vector3_t1986933152  L_5 = ___mousePosition1;
		NullCheck(L_4);
		GUIElement_t2946029536 * L_6 = GUILayer_HitTest_m2110564057(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GUIElement_t2946029536 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3123183460(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		HitInfo_t1469516070 * L_9 = ___hitInfo2;
		GUIElement_t2946029536 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t2557347079 * L_11 = Component_get_gameObject_m1032427207(L_10, /*hidden argument*/NULL);
		L_9->set_target_0(L_11);
		HitInfo_t1469516070 * L_12 = ___hitInfo2;
		Camera_t2839736942 * L_13 = ___camera0;
		L_12->set_camera_1(L_13);
		goto IL_0051;
	}

IL_0041:
	{
		HitInfo_t1469516070 * L_14 = ___hitInfo2;
		L_14->set_target_0((GameObject_t2557347079 *)NULL);
		HitInfo_t1469516070 * L_15 = ___hitInfo2;
		L_15->set_camera_1((Camera_t2839736942 *)NULL);
	}

IL_0051:
	{
	}

IL_0052:
	{
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern "C"  void SendMouseEvents_DoSendMouseEvents_m2533818309 (RuntimeObject * __this /* static, unused */, int32_t ___skipRTCameras0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m2533818309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	HitInfo_t1469516070  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Camera_t2839736942 * V_4 = NULL;
	CameraU5BU5D_t4045078555* V_5 = NULL;
	int32_t V_6 = 0;
	Rect_t3039462994  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Ray_t249333749  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector3_t1986933152  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	GameObject_t2557347079 * V_12 = NULL;
	GameObject_t2557347079 * V_13 = NULL;
	int32_t V_14 = 0;
	float G_B19_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = Input_get_mousePosition_m4034714020(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m1216147617(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		CameraU5BU5D_t4045078555* L_2 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		CameraU5BU5D_t4045078555* L_3 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002f;
		}
	}

IL_0024:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_m_Cameras_4(((CameraU5BU5D_t4045078555*)SZArrayNew(CameraU5BU5D_t4045078555_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		CameraU5BU5D_t4045078555* L_6 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		Camera_GetAllCameras_m3041928650(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_7 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1469516070  L_9 = V_3;
		*(HitInfo_t1469516070 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_12 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_027e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		CameraU5BU5D_t4045078555* L_14 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		V_5 = L_14;
		V_6 = 0;
		goto IL_0272;
	}

IL_0086:
	{
		CameraU5BU5D_t4045078555* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Camera_t2839736942 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		Camera_t2839736942 * L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_19, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b8;
		}
	}
	{
		Camera_t2839736942 * L_22 = V_4;
		NullCheck(L_22);
		RenderTexture_t971269558 * L_23 = Camera_get_targetTexture_m478696062(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_23, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b8;
		}
	}

IL_00b3:
	{
		goto IL_026c;
	}

IL_00b8:
	{
		Camera_t2839736942 * L_25 = V_4;
		NullCheck(L_25);
		Rect_t3039462994  L_26 = Camera_get_pixelRect_m3077452777(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		Vector3_t1986933152  L_27 = V_0;
		bool L_28 = Rect_Contains_m1261928192((&V_7), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_026c;
	}

IL_00d3:
	{
		Camera_t2839736942 * L_29 = V_4;
		Vector3_t1986933152  L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_31 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_31);
		SendMouseEvents_HitTestLegacyGUI_m267751607(NULL /*static, unused*/, L_29, L_30, ((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		Camera_t2839736942 * L_32 = V_4;
		NullCheck(L_32);
		int32_t L_33 = Camera_get_eventMask_m3749265454(L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_026c;
	}

IL_00f7:
	{
		Camera_t2839736942 * L_34 = V_4;
		Vector3_t1986933152  L_35 = V_0;
		NullCheck(L_34);
		Ray_t249333749  L_36 = Camera_ScreenPointToRay_m1212959324(L_34, L_35, /*hidden argument*/NULL);
		V_8 = L_36;
		Vector3_t1986933152  L_37 = Ray_get_direction_m2429538176((&V_8), /*hidden argument*/NULL);
		V_10 = L_37;
		float L_38 = (&V_10)->get_z_3();
		V_9 = L_38;
		float L_39 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		bool L_40 = Mathf_Approximately_m3559106540(NULL /*static, unused*/, (0.0f), L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_012e;
		}
	}
	{
		G_B19_0 = (std::numeric_limits<float>::infinity());
		goto IL_0145;
	}

IL_012e:
	{
		Camera_t2839736942 * L_41 = V_4;
		NullCheck(L_41);
		float L_42 = Camera_get_farClipPlane_m713887456(L_41, /*hidden argument*/NULL);
		Camera_t2839736942 * L_43 = V_4;
		NullCheck(L_43);
		float L_44 = Camera_get_nearClipPlane_m249543630(L_43, /*hidden argument*/NULL);
		float L_45 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_46 = fabsf(((float)((float)((float)((float)L_42-(float)L_44))/(float)L_45)));
		G_B19_0 = L_46;
	}

IL_0145:
	{
		V_11 = G_B19_0;
		Camera_t2839736942 * L_47 = V_4;
		Ray_t249333749  L_48 = V_8;
		float L_49 = V_11;
		Camera_t2839736942 * L_50 = V_4;
		NullCheck(L_50);
		int32_t L_51 = Camera_get_cullingMask_m3464126921(L_50, /*hidden argument*/NULL);
		Camera_t2839736942 * L_52 = V_4;
		NullCheck(L_52);
		int32_t L_53 = Camera_get_eventMask_m3749265454(L_52, /*hidden argument*/NULL);
		NullCheck(L_47);
		GameObject_t2557347079 * L_54 = Camera_RaycastTry_m736311948(L_47, L_48, L_49, ((int32_t)((int32_t)L_51&(int32_t)L_53)), /*hidden argument*/NULL);
		V_12 = L_54;
		GameObject_t2557347079 * L_55 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_55, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_019b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_57 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_57);
		GameObject_t2557347079 * L_58 = V_12;
		((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_58);
		HitInfoU5BU5D_t2568675203* L_59 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_59);
		Camera_t2839736942 * L_60 = V_4;
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_60);
		goto IL_01d9;
	}

IL_019b:
	{
		Camera_t2839736942 * L_61 = V_4;
		NullCheck(L_61);
		int32_t L_62 = Camera_get_clearFlags_m950950364(L_61, /*hidden argument*/NULL);
		if ((((int32_t)L_62) == ((int32_t)1)))
		{
			goto IL_01b5;
		}
	}
	{
		Camera_t2839736942 * L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_clearFlags_m950950364(L_63, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_64) == ((uint32_t)2))))
		{
			goto IL_01d9;
		}
	}

IL_01b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_65 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_65);
		((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t2557347079 *)NULL);
		HitInfoU5BU5D_t2568675203* L_66 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_66);
		((L_66)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t2839736942 *)NULL);
	}

IL_01d9:
	{
		Camera_t2839736942 * L_67 = V_4;
		Ray_t249333749  L_68 = V_8;
		float L_69 = V_11;
		Camera_t2839736942 * L_70 = V_4;
		NullCheck(L_70);
		int32_t L_71 = Camera_get_cullingMask_m3464126921(L_70, /*hidden argument*/NULL);
		Camera_t2839736942 * L_72 = V_4;
		NullCheck(L_72);
		int32_t L_73 = Camera_get_eventMask_m3749265454(L_72, /*hidden argument*/NULL);
		NullCheck(L_67);
		GameObject_t2557347079 * L_74 = Camera_RaycastTry2D_m3310148661(L_67, L_68, L_69, ((int32_t)((int32_t)L_71&(int32_t)L_73)), /*hidden argument*/NULL);
		V_13 = L_74;
		GameObject_t2557347079 * L_75 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_76 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_75, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_022d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_77 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_77);
		GameObject_t2557347079 * L_78 = V_13;
		((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_78);
		HitInfoU5BU5D_t2568675203* L_79 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_79);
		Camera_t2839736942 * L_80 = V_4;
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_80);
		goto IL_026b;
	}

IL_022d:
	{
		Camera_t2839736942 * L_81 = V_4;
		NullCheck(L_81);
		int32_t L_82 = Camera_get_clearFlags_m950950364(L_81, /*hidden argument*/NULL);
		if ((((int32_t)L_82) == ((int32_t)1)))
		{
			goto IL_0247;
		}
	}
	{
		Camera_t2839736942 * L_83 = V_4;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_clearFlags_m950950364(L_83, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_84) == ((uint32_t)2))))
		{
			goto IL_026b;
		}
	}

IL_0247:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_85 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_85);
		((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t2557347079 *)NULL);
		HitInfoU5BU5D_t2568675203* L_86 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_86);
		((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t2839736942 *)NULL);
	}

IL_026b:
	{
	}

IL_026c:
	{
		int32_t L_87 = V_6;
		V_6 = ((int32_t)((int32_t)L_87+(int32_t)1));
	}

IL_0272:
	{
		int32_t L_88 = V_6;
		CameraU5BU5D_t4045078555* L_89 = V_5;
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_89)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
	}

IL_027e:
	{
		V_14 = 0;
		goto IL_02a4;
	}

IL_0286:
	{
		int32_t L_90 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_91 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_92 = V_14;
		NullCheck(L_91);
		SendMouseEvents_SendEvents_m814760037(NULL /*static, unused*/, L_90, (*(HitInfo_t1469516070 *)((L_91)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_92)))), /*hidden argument*/NULL);
		int32_t L_93 = V_14;
		V_14 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_02a4:
	{
		int32_t L_94 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_95 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_95);
		if ((((int32_t)L_94) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_95)->max_length)))))))
		{
			goto IL_0286;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m814760037 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t1469516070  ___hit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m814760037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t1469516070  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1735245659_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m1898615268(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m823933278(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		HitInfo_t1469516070  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_5 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		HitInfo_t1469516070  L_7 = ___hit1;
		*(HitInfo_t1469516070 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))) = L_7;
		HitInfoU5BU5D_t2568675203* L_8 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		HitInfo_SendMessage_m364563472(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral170126824, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0107;
	}

IL_004f:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_11 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		bool L_13 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, (*(HitInfo_t1469516070 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00d0;
		}
	}
	{
		HitInfo_t1469516070  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_15 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		bool L_17 = HitInfo_Compare_m3678827560(NULL /*static, unused*/, L_14, (*(HitInfo_t1469516070 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_18 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		HitInfo_SendMessage_m364563472(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral299533630, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_20 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		HitInfo_SendMessage_m364563472(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral1510456461, /*hidden argument*/NULL);
		HitInfoU5BU5D_t2568675203* L_22 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1469516070  L_24 = V_2;
		*(HitInfo_t1469516070 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23))) = L_24;
	}

IL_00d0:
	{
		goto IL_0107;
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_25 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		bool L_27 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, (*(HitInfo_t1469516070 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_28 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		HitInfo_SendMessage_m364563472(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral4276975187, /*hidden argument*/NULL);
	}

IL_0107:
	{
		HitInfo_t1469516070  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_31 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		bool L_33 = HitInfo_Compare_m3678827560(NULL /*static, unused*/, L_30, (*(HitInfo_t1469516070 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0140;
		}
	}
	{
		HitInfo_t1469516070  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_013a;
		}
	}
	{
		HitInfo_SendMessage_m364563472((&___hit1), _stringLiteral1846547262, /*hidden argument*/NULL);
	}

IL_013a:
	{
		goto IL_0198;
	}

IL_0140:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_36 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		bool L_38 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, (*(HitInfo_t1469516070 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0172;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_39 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		HitInfo_SendMessage_m364563472(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2958471445, /*hidden argument*/NULL);
	}

IL_0172:
	{
		HitInfo_t1469516070  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m3774618170(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0197;
		}
	}
	{
		HitInfo_SendMessage_m364563472((&___hit1), _stringLiteral1804740865, /*hidden argument*/NULL);
		HitInfo_SendMessage_m364563472((&___hit1), _stringLiteral1846547262, /*hidden argument*/NULL);
	}

IL_0197:
	{
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t2568675203* L_43 = ((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		HitInfo_t1469516070  L_45 = ___hit1;
		*(HitInfo_t1469516070 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern "C"  void SendMouseEvents__cctor_m4006956103 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m4006956103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t1469516070  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t1469516070  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t1469516070  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t1469516070  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t1469516070  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t1469516070  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t1469516070  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t1469516070  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t1469516070  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t2568675203* L_0 = ((HitInfoU5BU5D_t2568675203*)SZArrayNew(HitInfoU5BU5D_t2568675203_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t1469516070  L_1 = V_0;
		*(HitInfo_t1469516070 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		HitInfoU5BU5D_t2568675203* L_2 = L_0;
		NullCheck(L_2);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t1469516070  L_3 = V_1;
		*(HitInfo_t1469516070 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		HitInfoU5BU5D_t2568675203* L_4 = L_2;
		NullCheck(L_4);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1469516070  L_5 = V_2;
		*(HitInfo_t1469516070 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_5;
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t2568675203* L_6 = ((HitInfoU5BU5D_t2568675203*)SZArrayNew(HitInfoU5BU5D_t2568675203_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1469516070  L_7 = V_3;
		*(HitInfo_t1469516070 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_7;
		HitInfoU5BU5D_t2568675203* L_8 = L_6;
		NullCheck(L_8);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t1469516070  L_9 = V_4;
		*(HitInfo_t1469516070 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_9;
		HitInfoU5BU5D_t2568675203* L_10 = L_8;
		NullCheck(L_10);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t1469516070  L_11 = V_5;
		*(HitInfo_t1469516070 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_11;
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t2568675203* L_12 = ((HitInfoU5BU5D_t2568675203*)SZArrayNew(HitInfoU5BU5D_t2568675203_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t1469516070  L_13 = V_6;
		*(HitInfo_t1469516070 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_13;
		HitInfoU5BU5D_t2568675203* L_14 = L_12;
		NullCheck(L_14);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t1469516070  L_15 = V_7;
		*(HitInfo_t1469516070 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_15;
		HitInfoU5BU5D_t2568675203* L_16 = L_14;
		NullCheck(L_16);
		Initobj (HitInfo_t1469516070_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t1469516070  L_17 = V_8;
		*(HitInfo_t1469516070 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_17;
		((SendMouseEvents_t2768135016_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2768135016_il2cpp_TypeInfo_var))->set_m_CurrentHit_3(L_16);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1469516070_marshal_pinvoke(const HitInfo_t1469516070& unmarshaled, HitInfo_t1469516070_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1469516070_marshal_pinvoke_back(const HitInfo_t1469516070_marshaled_pinvoke& marshaled, HitInfo_t1469516070& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1469516070_marshal_pinvoke_cleanup(HitInfo_t1469516070_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1469516070_marshal_com(const HitInfo_t1469516070& unmarshaled, HitInfo_t1469516070_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1469516070_marshal_com_back(const HitInfo_t1469516070_marshaled_com& marshaled, HitInfo_t1469516070& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1469516070_marshal_com_cleanup(HitInfo_t1469516070_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m364563472 (HitInfo_t1469516070 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		GameObject_t2557347079 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m814261268(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m364563472_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	HitInfo_t1469516070 * _thisAdjusted = reinterpret_cast<HitInfo_t1469516070 *>(__this + 1);
	HitInfo_SendMessage_m364563472(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m3774618170 (RuntimeObject * __this /* static, unused */, HitInfo_t1469516070  ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m3774618170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t2557347079 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Camera_t2839736942 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1032523438(NULL /*static, unused*/, L_2, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m3678827560 (RuntimeObject * __this /* static, unused */, HitInfo_t1469516070  ___lhs0, HitInfo_t1469516070  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m3678827560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t2557347079 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t2557347079 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Camera_t2839736942 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t2839736942 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m2073215811 (FormerlySerializedAsAttribute_t333733317 * __this, String_t* ___oldName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m300987958 (SerializeField_t3205734915 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern "C"  void SetupCoroutine_InvokeMoveNext_m2917246636 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___enumerator0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m2917246636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m1348886383(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t1812645948 * L_2 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m4009562449(L_2, _stringLiteral3761283446, _stringLiteral1363212644, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		intptr_t L_3 = ___returnValueAddress1;
		void* L_4 = IntPtr_op_Explicit_m803950198(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		RuntimeObject* L_5 = ___enumerator0;
		NullCheck(L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3472601659_il2cpp_TypeInfo_var, L_5);
		*((int8_t*)(L_4)) = (int8_t)L_6;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern "C"  RuntimeObject * SetupCoroutine_InvokeMember_m2001778250 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___behaviour0, String_t* ___name1, RuntimeObject * ___variable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m2001778250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2737604620* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		V_0 = (ObjectU5BU5D_t2737604620*)NULL;
		RuntimeObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t2737604620* L_1 = V_0;
		RuntimeObject * L_2 = ___variable2;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
	}

IL_0016:
	{
		RuntimeObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2972540513(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		RuntimeObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t2737604620* L_7 = V_0;
		NullCheck(L_4);
		RuntimeObject * L_8 = VirtFuncInvoker8< RuntimeObject *, String_t*, int32_t, Binder_t1695596754 *, RuntimeObject *, ObjectU5BU5D_t2737604620*, ParameterModifierU5BU5D_t2531273172*, CultureInfo_t2625678720 *, StringU5BU5D_t2511808107* >::Invoke(73 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1695596754 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t2531273172*)(ParameterModifierU5BU5D_t2531273172*)NULL, (CultureInfo_t2625678720 *)NULL, (StringU5BU5D_t2511808107*)(StringU5BU5D_t2511808107*)NULL);
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		RuntimeObject * L_9 = V_1;
		return L_9;
	}
}
// System.Void UnityEngine.Shader::.ctor()
extern "C"  void Shader__ctor_m2013031543 (Shader_t1881769421 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shader__ctor_m2013031543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t1881769421 * Shader_Find_m2823491362 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef Shader_t1881769421 * (*Shader_Find_m2823491362_ftn) (String_t*);
	static Shader_Find_m2823491362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m2823491362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	Shader_t1881769421 * retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C"  bool Shader_get_isSupported_m35518636 (Shader_t1881769421 * __this, const RuntimeMethod* method)
{
	typedef bool (*Shader_get_isSupported_m35518636_ftn) (Shader_t1881769421 *);
	static Shader_get_isSupported_m35518636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_isSupported_m35518636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_isSupported()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
extern "C"  void Shader_set_globalMaximumLOD_m3706264495 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Shader_set_globalMaximumLOD_m3706264495_ftn) (int32_t);
	static Shader_set_globalMaximumLOD_m3706264495_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_set_globalMaximumLOD_m3706264495_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::set_globalMaximumLOD(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m1579628175 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Shader_PropertyToID_m1579628175_ftn) (String_t*);
	static Shader_PropertyToID_m1579628175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m1579628175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C"  int32_t SortingLayer_GetLayerValueFromID_m543444545 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m543444545_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m543444545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m543444545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	int32_t retVal = _il2cpp_icall_func(___id0);
	return retVal;
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C"  void SpaceAttribute__ctor_m3811011082 (SpaceAttribute_t3838534887 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		__this->set_height_0((8.0f));
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C"  void SpaceAttribute__ctor_m1445520994 (SpaceAttribute_t3838534887 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_height_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Sprite::.ctor()
extern "C"  void Sprite__ctor_m78442145 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Sprite__ctor_m78442145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Sprite_t2544745708 * Sprite_Create_m1384549182 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Rect_t3039462994  ___rect1, Vector2_t328513675  ___pivot2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Sprite_Create_m1384549182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	float V_3 = 0.0f;
	Sprite_t2544745708 * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_0 = Vector4_get_zero_m1931160304(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 1;
		V_2 = 0;
		V_3 = (100.0f);
		Texture2D_t3063074017 * L_1 = ___texture0;
		float L_2 = V_3;
		uint32_t L_3 = V_2;
		int32_t L_4 = V_1;
		Sprite_t2544745708 * L_5 = Sprite_INTERNAL_CALL_Create_m140032498(NULL /*static, unused*/, L_1, (&___rect1), (&___pivot2), L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		V_4 = L_5;
		goto IL_0027;
	}

IL_0027:
	{
		Sprite_t2544745708 * L_6 = V_4;
		return L_6;
	}
}
// UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
extern "C"  Sprite_t2544745708 * Sprite_INTERNAL_CALL_Create_m140032498 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___texture0, Rect_t3039462994 * ___rect1, Vector2_t328513675 * ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, Vector4_t2436299922 * ___border6, const RuntimeMethod* method)
{
	typedef Sprite_t2544745708 * (*Sprite_INTERNAL_CALL_Create_m140032498_ftn) (Texture2D_t3063074017 *, Rect_t3039462994 *, Vector2_t328513675 *, float, uint32_t, int32_t, Vector4_t2436299922 *);
	static Sprite_INTERNAL_CALL_Create_m140032498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_CALL_Create_m140032498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)");
	Sprite_t2544745708 * retVal = _il2cpp_icall_func(___texture0, ___rect1, ___pivot2, ___pixelsPerUnit3, ___extrude4, ___meshType5, ___border6);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t3039462994  Sprite_get_rect_m129973010 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	Rect_t3039462994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3039462994  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_rect_m2187623600(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3039462994  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3039462994  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m2187623600 (Sprite_t2544745708 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m2187623600_ftn) (Sprite_t2544745708 *, Rect_t3039462994 *);
	static Sprite_INTERNAL_get_rect_m2187623600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m2187623600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m2300208359 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m2300208359_ftn) (Sprite_t2544745708 *);
	static Sprite_get_pixelsPerUnit_m2300208359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m2300208359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t3063074017 * Sprite_get_texture_m336868374 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t3063074017 * (*Sprite_get_texture_m336868374_ftn) (Sprite_t2544745708 *);
	static Sprite_get_texture_m336868374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m336868374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	Texture2D_t3063074017 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t3063074017 * Sprite_get_associatedAlphaSplitTexture_m1663347590 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t3063074017 * (*Sprite_get_associatedAlphaSplitTexture_m1663347590_ftn) (Sprite_t2544745708 *);
	static Sprite_get_associatedAlphaSplitTexture_m1663347590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_associatedAlphaSplitTexture_m1663347590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_associatedAlphaSplitTexture()");
	Texture2D_t3063074017 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t3039462994  Sprite_get_textureRect_m434122779 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	Rect_t3039462994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3039462994  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_textureRect_m3988265876(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3039462994  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3039462994  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m3988265876 (Sprite_t2544745708 * __this, Rect_t3039462994 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m3988265876_ftn) (Sprite_t2544745708 *, Rect_t3039462994 *);
	static Sprite_INTERNAL_get_textureRect_m3988265876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m3988265876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Sprite::get_packed()
extern "C"  bool Sprite_get_packed_m561520925 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	typedef bool (*Sprite_get_packed_m561520925_ftn) (Sprite_t2544745708 *);
	static Sprite_get_packed_m561520925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_packed_m561520925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_packed()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t2436299922  Sprite_get_border_m495357595 (Sprite_t2544745708 * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_border_m1535245036(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t2436299922  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t2436299922  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m1535245036 (Sprite_t2544745708 * __this, Vector4_t2436299922 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m1535245036_ftn) (Sprite_t2544745708 *, Vector4_t2436299922 *);
	static Sprite_INTERNAL_get_border_m1535245036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m1535245036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C"  Vector4_t2436299922  DataUtility_GetInnerUV_m598120119 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2544745708 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2436299922  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t2436299922  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236_ftn) (Sprite_t2544745708 *, Vector4_t2436299922 *);
	static DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetInnerUV_m1399485236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C"  Vector4_t2436299922  DataUtility_GetOuterUV_m1424393419 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2544745708 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetOuterUV_m826210465(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2436299922  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t2436299922  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m826210465 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetOuterUV_m826210465_ftn) (Sprite_t2544745708 *, Vector4_t2436299922 *);
	static DataUtility_INTERNAL_CALL_GetOuterUV_m826210465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetOuterUV_m826210465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C"  Vector4_t2436299922  DataUtility_GetPadding_m2914989533 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2544745708 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetPadding_m1892881781(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2436299922  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t2436299922  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m1892881781 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector4_t2436299922 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetPadding_m1892881781_ftn) (Sprite_t2544745708 *, Vector4_t2436299922 *);
	static DataUtility_INTERNAL_CALL_GetPadding_m1892881781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetPadding_m1892881781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C"  Vector2_t328513675  DataUtility_GetMinSize_m2151022889 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2544745708 * L_0 = ___sprite0;
		DataUtility_Internal_GetMinSize_m3550847450(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t328513675  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t328513675  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m3550847450 (RuntimeObject * __this /* static, unused */, Sprite_t2544745708 * ___sprite0, Vector2_t328513675 * ___output1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m3550847450_ftn) (Sprite_t2544745708 *, Vector2_t328513675 *);
	static DataUtility_Internal_GetMinSize_m3550847450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m3550847450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite0, ___output1);
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern "C"  void StackTraceUtility_SetProjectFolder_m3440359700 (RuntimeObject * __this /* static, unused */, String_t* ___folder0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m3440359700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m2628855948(L_0, _stringLiteral2574419025, _stringLiteral2278905470, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->set_projectFolder_0(L_1);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m3667880611 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m3667880611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StackTrace_t4180877726 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		StackTrace_t4180877726 * L_0 = (StackTrace_t4180877726 *)il2cpp_codegen_object_new(StackTrace_t4180877726_il2cpp_TypeInfo_var);
		StackTrace__ctor_m2103493457(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t4180877726 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m4260390550(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_1 = L_3;
		String_t* L_4 = V_1;
		V_2 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m1261986193 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m1261986193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1698616109(L_1, _stringLiteral2414275388, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1698616109(L_3, _stringLiteral1319789206, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1698616109(L_5, _stringLiteral2850377577, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1698616109(L_7, _stringLiteral2822882624, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1698616109(L_9, _stringLiteral1204840630, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1698616109(L_11, _stringLiteral195602857, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0066;
	}

IL_0065:
	{
		G_B7_0 = 1;
	}

IL_0066:
	{
		V_1 = (bool)G_B7_0;
		goto IL_006c;
	}

IL_006c:
	{
		bool L_13 = V_1;
		return L_13;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m1202155996 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m1202155996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t4086964929 * V_0 = NULL;
	StringBuilder_t622404039 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t4180877726 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentException_t1812645948 * L_1 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_1, _stringLiteral3407105065, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t4086964929 *)IsInstClass((RuntimeObject*)L_2, Exception_t4086964929_il2cpp_TypeInfo_var));
		Exception_t4086964929 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentException_t1812645948 * L_4 = (ArgumentException_t1812645948 *)il2cpp_codegen_object_new(ArgumentException_t1812645948_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m560113655(L_4, _stringLiteral2418974296, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002a:
	{
		Exception_t4086964929 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004c;
	}

IL_003f:
	{
		Exception_t4086964929 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m315776189(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004c:
	{
		StringBuilder_t622404039 * L_10 = (StringBuilder_t622404039 *)il2cpp_codegen_object_new(StringBuilder_t622404039_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1594975206(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		*((RuntimeObject **)(L_11)) = (RuntimeObject *)_stringLiteral4185710657;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_11), (RuntimeObject *)_stringLiteral4185710657);
		V_2 = _stringLiteral4185710657;
		goto IL_0106;
	}

IL_0064:
	{
		String_t* L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m315776189(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		Exception_t4086964929 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_StackTrace() */, L_14);
		V_2 = L_15;
		goto IL_008e;
	}

IL_007c:
	{
		Exception_t4086964929 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_StackTrace() */, L_16);
		String_t* L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1542845994(NULL /*static, unused*/, L_17, _stringLiteral415700502, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
	}

IL_008e:
	{
		Exception_t4086964929 * L_20 = V_0;
		NullCheck(L_20);
		Type_t * L_21 = Exception_GetType_m2106978941(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		V_3 = L_22;
		V_4 = _stringLiteral4185710657;
		Exception_t4086964929 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_23);
		if (!L_24)
		{
			goto IL_00b4;
		}
	}
	{
		Exception_t4086964929 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_25);
		V_4 = L_26;
	}

IL_00b4:
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		String_t* L_28 = String_Trim_m2686817291(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m315776189(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00dc;
		}
	}
	{
		String_t* L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m2395435812(NULL /*static, unused*/, L_30, _stringLiteral159996285, /*hidden argument*/NULL);
		V_3 = L_31;
		String_t* L_32 = V_3;
		String_t* L_33 = V_4;
		String_t* L_34 = String_Concat_m2395435812(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		V_3 = L_34;
	}

IL_00dc:
	{
		String_t** L_35 = ___message1;
		String_t* L_36 = V_3;
		*((RuntimeObject **)(L_35)) = (RuntimeObject *)L_36;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_35), (RuntimeObject *)L_36);
		Exception_t4086964929 * L_37 = V_0;
		NullCheck(L_37);
		Exception_t4086964929 * L_38 = Exception_get_InnerException_m1086399415(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00fe;
		}
	}
	{
		String_t* L_39 = V_3;
		String_t* L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1568077200(NULL /*static, unused*/, _stringLiteral1169143675, L_39, _stringLiteral415700502, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
	}

IL_00fe:
	{
		Exception_t4086964929 * L_42 = V_0;
		NullCheck(L_42);
		Exception_t4086964929 * L_43 = Exception_get_InnerException_m1086399415(L_42, /*hidden argument*/NULL);
		V_0 = L_43;
	}

IL_0106:
	{
		Exception_t4086964929 * L_44 = V_0;
		if (L_44)
		{
			goto IL_0064;
		}
	}
	{
		StringBuilder_t622404039 * L_45 = V_1;
		String_t* L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m2395435812(NULL /*static, unused*/, L_46, _stringLiteral415700502, /*hidden argument*/NULL);
		NullCheck(L_45);
		StringBuilder_Append_m3980566461(L_45, L_47, /*hidden argument*/NULL);
		StackTrace_t4180877726 * L_48 = (StackTrace_t4180877726 *)il2cpp_codegen_object_new(StackTrace_t4180877726_il2cpp_TypeInfo_var);
		StackTrace__ctor_m2103493457(L_48, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_48;
		StringBuilder_t622404039 * L_49 = V_1;
		StackTrace_t4180877726 * L_50 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		String_t* L_51 = StackTraceUtility_ExtractFormattedStackTrace_m4260390550(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		StringBuilder_Append_m3980566461(L_49, L_51, /*hidden argument*/NULL);
		String_t** L_52 = ___stackTrace2;
		StringBuilder_t622404039 * L_53 = V_1;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_53);
		*((RuntimeObject **)(L_52)) = (RuntimeObject *)L_54;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_52), (RuntimeObject *)L_54);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m1447856313 (RuntimeObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m1447856313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2511808107* V_1 = NULL;
	StringBuilder_t622404039 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_1;
		goto IL_02a4;
	}

IL_0012:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t3419619864* L_3 = ((CharU5BU5D_t3419619864*)SZArrayNew(CharU5BU5D_t3419619864_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t2511808107* L_4 = String_Split_m1115057867(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m315776189(L_5, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_7 = (StringBuilder_t622404039 *)il2cpp_codegen_object_new(StringBuilder_t622404039_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1594975206(L_7, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_0046;
	}

IL_0037:
	{
		StringU5BU5D_t2511808107* L_8 = V_1;
		int32_t L_9 = V_3;
		StringU5BU5D_t2511808107* L_10 = V_1;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m2686817291(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_16 = V_3;
		StringU5BU5D_t2511808107* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		V_4 = 0;
		goto IL_028e;
	}

IL_0057:
	{
		StringU5BU5D_t2511808107* L_18 = V_1;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_5 = L_21;
		String_t* L_22 = V_5;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m315776189(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_24 = V_5;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m4268332576(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_007e;
		}
	}

IL_0079:
	{
		goto IL_0288;
	}

IL_007e:
	{
		String_t* L_26 = V_5;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m1698616109(L_26, _stringLiteral4242007556, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_0288;
	}

IL_0094:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_29 = V_5;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m1698616109(L_29, _stringLiteral3302944042, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00b0;
		}
	}
	{
		goto IL_0298;
	}

IL_00b0:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_32 = V_4;
		StringU5BU5D_t2511808107* L_33 = V_1;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_0107;
		}
	}
	{
		String_t* L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m1261986193(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0107;
		}
	}
	{
		StringU5BU5D_t2511808107* L_36 = V_1;
		int32_t L_37 = V_4;
		NullCheck(L_36);
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m1261986193(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00e4;
		}
	}
	{
		goto IL_0288;
	}

IL_00e4:
	{
		String_t* L_41 = V_5;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m3326695972(L_41, _stringLiteral2874061849, /*hidden argument*/NULL);
		V_6 = L_42;
		int32_t L_43 = V_6;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_0106;
		}
	}
	{
		String_t* L_44 = V_5;
		int32_t L_45 = V_6;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m1881321258(L_44, 0, L_45, /*hidden argument*/NULL);
		V_5 = L_46;
	}

IL_0106:
	{
	}

IL_0107:
	{
		String_t* L_47 = V_5;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m3326695972(L_47, _stringLiteral4102960008, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_011e;
		}
	}
	{
		goto IL_0288;
	}

IL_011e:
	{
		String_t* L_49 = V_5;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m3326695972(L_49, _stringLiteral3019173685, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0288;
	}

IL_0135:
	{
		String_t* L_51 = V_5;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m3326695972(L_51, _stringLiteral3205397905, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_014c;
		}
	}
	{
		goto IL_0288;
	}

IL_014c:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_54 = V_5;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m1698616109(L_54, _stringLiteral2582683053, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_56 = V_5;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m3111453544(L_56, _stringLiteral890430475, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0179;
		}
	}
	{
		goto IL_0288;
	}

IL_0179:
	{
		String_t* L_58 = V_5;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m1698616109(L_58, _stringLiteral2906454100, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0197;
		}
	}
	{
		String_t* L_60 = V_5;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m4139867499(L_60, 0, 3, /*hidden argument*/NULL);
		V_5 = L_61;
	}

IL_0197:
	{
		String_t* L_62 = V_5;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m3326695972(L_62, _stringLiteral2931583284, /*hidden argument*/NULL);
		V_7 = L_63;
		V_8 = (-1);
		int32_t L_64 = V_7;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01c0;
		}
	}
	{
		String_t* L_65 = V_5;
		int32_t L_66 = V_7;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m873403889(L_65, _stringLiteral890430475, L_66, /*hidden argument*/NULL);
		V_8 = L_67;
	}

IL_01c0:
	{
		int32_t L_68 = V_7;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_69 = V_8;
		int32_t L_70 = V_7;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01e5;
		}
	}
	{
		String_t* L_71 = V_5;
		int32_t L_72 = V_7;
		int32_t L_73 = V_8;
		int32_t L_74 = V_7;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m4139867499(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_5 = L_75;
	}

IL_01e5:
	{
		String_t* L_76 = V_5;
		NullCheck(L_76);
		String_t* L_77 = String_Replace_m2628855948(L_76, _stringLiteral294405094, _stringLiteral4185710657, /*hidden argument*/NULL);
		V_5 = L_77;
		String_t* L_78 = V_5;
		NullCheck(L_78);
		String_t* L_79 = String_Replace_m2628855948(L_78, _stringLiteral2574419025, _stringLiteral2278905470, /*hidden argument*/NULL);
		V_5 = L_79;
		String_t* L_80 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		String_t* L_82 = String_Replace_m2628855948(L_80, L_81, _stringLiteral4185710657, /*hidden argument*/NULL);
		V_5 = L_82;
		String_t* L_83 = V_5;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m3191968003(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_5 = L_84;
		String_t* L_85 = V_5;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m3162040027(L_85, _stringLiteral4110169122, /*hidden argument*/NULL);
		V_9 = L_86;
		int32_t L_87 = V_9;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_0274;
		}
	}
	{
		String_t* L_88 = V_5;
		int32_t L_89 = V_9;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m4139867499(L_88, L_89, 5, /*hidden argument*/NULL);
		V_5 = L_90;
		String_t* L_91 = V_5;
		int32_t L_92 = V_9;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m1500258161(L_91, L_92, _stringLiteral1050932311, /*hidden argument*/NULL);
		V_5 = L_93;
		String_t* L_94 = V_5;
		String_t* L_95 = V_5;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m315776189(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m1500258161(L_94, L_96, _stringLiteral2908764213, /*hidden argument*/NULL);
		V_5 = L_97;
	}

IL_0274:
	{
		StringBuilder_t622404039 * L_98 = V_2;
		String_t* L_99 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m2395435812(NULL /*static, unused*/, L_99, _stringLiteral415700502, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m3980566461(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0288:
	{
		int32_t L_101 = V_4;
		V_4 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_028e:
	{
		int32_t L_102 = V_4;
		StringU5BU5D_t2511808107* L_103 = V_1;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_103)->max_length)))))))
		{
			goto IL_0057;
		}
	}

IL_0298:
	{
		StringBuilder_t622404039 * L_104 = V_2;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_0 = L_105;
		goto IL_02a4;
	}

IL_02a4:
	{
		String_t* L_106 = V_0;
		return L_106;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m4260390550 (RuntimeObject * __this /* static, unused */, StackTrace_t4180877726 * ___stackTrace0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m4260390550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t622404039 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t1778166981 * V_2 = NULL;
	MethodBase_t2927642150 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t3316460985* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	String_t* V_12 = NULL;
	int32_t G_B27_0 = 0;
	int32_t G_B29_0 = 0;
	{
		StringBuilder_t622404039 * L_0 = (StringBuilder_t622404039 *)il2cpp_codegen_object_new(StringBuilder_t622404039_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1594975206(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_02ba;
	}

IL_0013:
	{
		StackTrace_t4180877726 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1778166981 * L_3 = VirtFuncInvoker1< StackFrame_t1778166981 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1778166981 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t2927642150 * L_5 = VirtFuncInvoker0< MethodBase_t2927642150 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t2927642150 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_02b6;
	}

IL_002e:
	{
		MethodBase_t2927642150 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_02b6;
	}

IL_0042:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m315776189(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		StringBuilder_t622404039 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m3980566461(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3980566461(L_17, _stringLiteral2957364442, /*hidden argument*/NULL);
	}

IL_0075:
	{
		StringBuilder_t622404039 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m3980566461(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3980566461(L_21, _stringLiteral2284701596, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_22 = V_0;
		MethodBase_t2927642150 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m3980566461(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3980566461(L_25, _stringLiteral4284158284, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t2927642150 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t3316460985* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t3316460985* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00f4;
	}

IL_00bb:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00d4;
		}
	}
	{
		StringBuilder_t622404039 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3980566461(L_29, _stringLiteral591250, /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_00d4:
	{
		V_8 = (bool)0;
	}

IL_00d7:
	{
		StringBuilder_t622404039 * L_30 = V_0;
		ParameterInfoU5BU5D_t3316460985* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		ParameterInfo_t3943516840 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m3980566461(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t3316460985* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length)))))))
		{
			goto IL_00bb;
		}
	}
	{
		StringBuilder_t622404039 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m3980566461(L_40, _stringLiteral2908764213, /*hidden argument*/NULL);
		StackFrame_t1778166981 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_02a9;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_45, _stringLiteral3992975604, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0147;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_48, _stringLiteral351502530, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_020c;
		}
	}

IL_0147:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_51, _stringLiteral2206330720, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0173;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_54, _stringLiteral351502530, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_020c;
		}
	}

IL_0173:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_57, _stringLiteral432646105, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_019f;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_60, _stringLiteral351502530, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_020c;
		}
	}

IL_019f:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_63, _stringLiteral3991562125, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01cb;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_66, _stringLiteral1060462586, /*hidden argument*/NULL);
		if (L_67)
		{
			goto IL_020c;
		}
	}

IL_01cb:
	{
		MethodBase_t2927642150 * L_68 = V_3;
		NullCheck(L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_68);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_70 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_69, _stringLiteral972445331, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_71 = V_4;
		NullCheck(L_71);
		String_t* L_72 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_73 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_72, _stringLiteral1760345480, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_74 = V_4;
		NullCheck(L_74);
		String_t* L_75 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_74);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_76 = String_op_Equality_m1683246493(NULL /*static, unused*/, L_75, _stringLiteral351502530, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_76));
		goto IL_020a;
	}

IL_0209:
	{
		G_B27_0 = 0;
	}

IL_020a:
	{
		G_B29_0 = G_B27_0;
		goto IL_020d;
	}

IL_020c:
	{
		G_B29_0 = 1;
	}

IL_020d:
	{
		V_10 = (bool)G_B29_0;
		bool L_77 = V_10;
		if (L_77)
		{
			goto IL_02a8;
		}
	}
	{
		StringBuilder_t622404039 * L_78 = V_0;
		NullCheck(L_78);
		StringBuilder_Append_m3980566461(L_78, _stringLiteral1050932311, /*hidden argument*/NULL);
		String_t* L_79 = V_9;
		NullCheck(L_79);
		String_t* L_80 = String_Replace_m2628855948(L_79, _stringLiteral2574419025, _stringLiteral2278905470, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		bool L_82 = String_StartsWith_m1698616109(L_80, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_026a;
		}
	}
	{
		String_t* L_83 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t931691098_il2cpp_TypeInfo_var);
		String_t* L_84 = ((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_84);
		int32_t L_85 = String_get_Length_m315776189(L_84, /*hidden argument*/NULL);
		String_t* L_86 = V_9;
		NullCheck(L_86);
		int32_t L_87 = String_get_Length_m315776189(L_86, /*hidden argument*/NULL);
		String_t* L_88 = ((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_88);
		int32_t L_89 = String_get_Length_m315776189(L_88, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_90 = String_Substring_m1881321258(L_83, L_85, ((int32_t)((int32_t)L_87-(int32_t)L_89)), /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_026a:
	{
		StringBuilder_t622404039 * L_91 = V_0;
		String_t* L_92 = V_9;
		NullCheck(L_91);
		StringBuilder_Append_m3980566461(L_91, L_92, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_93 = V_0;
		NullCheck(L_93);
		StringBuilder_Append_m3980566461(L_93, _stringLiteral2284701596, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_94 = V_0;
		StackFrame_t1778166981 * L_95 = V_2;
		NullCheck(L_95);
		int32_t L_96 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_95);
		V_11 = L_96;
		String_t* L_97 = Int32_ToString_m2781354364((&V_11), /*hidden argument*/NULL);
		NullCheck(L_94);
		StringBuilder_Append_m3980566461(L_94, L_97, /*hidden argument*/NULL);
		StringBuilder_t622404039 * L_98 = V_0;
		NullCheck(L_98);
		StringBuilder_Append_m3980566461(L_98, _stringLiteral2908764213, /*hidden argument*/NULL);
	}

IL_02a8:
	{
	}

IL_02a9:
	{
		StringBuilder_t622404039 * L_99 = V_0;
		NullCheck(L_99);
		StringBuilder_Append_m3980566461(L_99, _stringLiteral415700502, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		int32_t L_100 = V_1;
		V_1 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02ba:
	{
		int32_t L_101 = V_1;
		StackTrace_t4180877726 * L_102 = ___stackTrace0;
		NullCheck(L_102);
		int32_t L_103 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_102);
		if ((((int32_t)L_101) < ((int32_t)L_103)))
		{
			goto IL_0013;
		}
	}
	{
		StringBuilder_t622404039 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_12 = L_105;
		goto IL_02d3;
	}

IL_02d3:
	{
		String_t* L_106 = V_12;
		return L_106;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern "C"  void StackTraceUtility__cctor_m58538887 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m58538887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((StackTraceUtility_t931691098_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t931691098_il2cpp_TypeInfo_var))->set_projectFolder_0(_stringLiteral4185710657);
		return;
	}
}
// UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern "C"  int32_t SystemInfo_get_operatingSystemFamily_m1283507281 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_operatingSystemFamily_m1283507281_ftn) ();
	static SystemInfo_get_operatingSystemFamily_m1283507281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystemFamily_m1283507281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystemFamily()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::get_graphicsDeviceType()
extern "C"  int32_t SystemInfo_get_graphicsDeviceType_m98152835 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_graphicsDeviceType_m98152835_ftn) ();
	static SystemInfo_get_graphicsDeviceType_m98152835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceType_m98152835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceType()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C"  int32_t SystemInfo_get_graphicsShaderLevel_m3909654614 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_graphicsShaderLevel_m3909654614_ftn) ();
	static SystemInfo_get_graphicsShaderLevel_m3909654614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsShaderLevel_m3909654614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsShaderLevel()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.SystemInfo::get_supportsMotionVectors()
extern "C"  bool SystemInfo_get_supportsMotionVectors_m2320415195 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*SystemInfo_get_supportsMotionVectors_m2320415195_ftn) ();
	static SystemInfo_get_supportsMotionVectors_m2320415195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsMotionVectors_m2320415195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsMotionVectors()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C"  bool SystemInfo_get_supportsImageEffects_m1208323176 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*SystemInfo_get_supportsImageEffects_m1208323176_ftn) ();
	static SystemInfo_get_supportsImageEffects_m1208323176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsImageEffects_m1208323176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsImageEffects()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern "C"  bool SystemInfo_get_supports3DTextures_m477829254 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*SystemInfo_get_supports3DTextures_m477829254_ftn) ();
	static SystemInfo_get_supports3DTextures_m477829254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supports3DTextures_m477829254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supports3DTextures()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C"  bool SystemInfo_get_supportsComputeShaders_m2967532227 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*SystemInfo_get_supportsComputeShaders_m2967532227_ftn) ();
	static SystemInfo_get_supportsComputeShaders_m2967532227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsComputeShaders_m2967532227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsComputeShaders()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
extern "C"  int32_t SystemInfo_get_supportedRenderTargetCount_m3985359166 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_supportedRenderTargetCount_m3985359166_ftn) ();
	static SystemInfo_get_supportedRenderTargetCount_m3985359166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportedRenderTargetCount_m3985359166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportedRenderTargetCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C"  bool SystemInfo_SupportsRenderTextureFormat_m3172496137 (RuntimeObject * __this /* static, unused */, int32_t ___format0, const RuntimeMethod* method)
{
	typedef bool (*SystemInfo_SupportsRenderTextureFormat_m3172496137_ftn) (int32_t);
	static SystemInfo_SupportsRenderTextureFormat_m3172496137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_SupportsRenderTextureFormat_m3172496137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)");
	bool retVal = _il2cpp_icall_func(___format0);
	return retVal;
}
// System.Void UnityEngine.TextAreaAttribute::.ctor()
extern "C"  void TextAreaAttribute__ctor_m81792599 (TextAreaAttribute_t909267116 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		__this->set_minLines_0(3);
		__this->set_maxLines_1(3);
		return;
	}
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m1611109693 (TextAreaAttribute_t909267116 * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m1789449816 (TextAsset_t2052027493 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TextAsset_get_text_m1789449816_ftn) (TextAsset_t2052027493 *);
	static TextAsset_get_text_m1789449816_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m1789449816_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Byte[] UnityEngine.TextAsset::get_bytes()
extern "C"  ByteU5BU5D_t434619169* TextAsset_get_bytes_m3550577764 (TextAsset_t2052027493 * __this, const RuntimeMethod* method)
{
	typedef ByteU5BU5D_t434619169* (*TextAsset_get_bytes_m3550577764_ftn) (TextAsset_t2052027493 *);
	static TextAsset_get_bytes_m3550577764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_bytes_m3550577764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_bytes()");
	ByteU5BU5D_t434619169* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.String UnityEngine.TextAsset::ToString()
extern "C"  String_t* TextAsset_ToString_m1221759584 (TextAsset_t2052027493 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = TextAsset_get_text_m1789449816(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m373917183 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture__ctor_m373917183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		Object__ctor_m4147771927(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m2443978672 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m2443978672_ftn) (Texture_t2119925672 *);
	static Texture_Internal_GetWidth_m2443978672_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m2443978672_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m2937360541 (RuntimeObject * __this /* static, unused */, Texture_t2119925672 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m2937360541_ftn) (Texture_t2119925672 *);
	static Texture_Internal_GetHeight_m2937360541_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m2937360541_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m3891116380 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetWidth_m2443978672(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern "C"  void Texture_set_width_m867714560 (Texture_t2119925672 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_width_m867714560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t4086964929 * L_0 = (Exception_t4086964929 *)il2cpp_codegen_object_new(Exception_t4086964929_il2cpp_TypeInfo_var);
		Exception__ctor_m3397592417(L_0, _stringLiteral3391388676, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m2862696561 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetHeight_m2937360541(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern "C"  void Texture_set_height_m475457339 (Texture_t2119925672 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_height_m475457339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t4086964929 * L_0 = (Exception_t4086964929 *)il2cpp_codegen_object_new(Exception_t4086964929_il2cpp_TypeInfo_var);
		Exception__ctor_m3397592417(L_0, _stringLiteral3391388676, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
extern "C"  int32_t Texture_get_filterMode_m825057287 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_filterMode_m825057287_ftn) (Texture_t2119925672 *);
	static Texture_get_filterMode_m825057287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_filterMode_m825057287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_filterMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m4014606988 (Texture_t2119925672 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_filterMode_m4014606988_ftn) (Texture_t2119925672 *, int32_t);
	static Texture_set_filterMode_m4014606988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m4014606988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
extern "C"  void Texture_set_anisoLevel_m2706613657 (Texture_t2119925672 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_anisoLevel_m2706613657_ftn) (Texture_t2119925672 *, int32_t);
	static Texture_set_anisoLevel_m2706613657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_anisoLevel_m2706613657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_anisoLevel(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C"  int32_t Texture_get_wrapMode_m3862337492 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_wrapMode_m3862337492_ftn) (Texture_t2119925672 *);
	static Texture_get_wrapMode_m3862337492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_wrapMode_m3862337492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_wrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m1613607856 (Texture_t2119925672 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_wrapMode_m1613607856_ftn) (Texture_t2119925672 *, int32_t);
	static Texture_set_wrapMode_m1613607856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m1613607856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern "C"  Vector2_t328513675  Texture_get_texelSize_m300126573 (Texture_t2119925672 * __this, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Texture_INTERNAL_get_texelSize_m3241763420(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t328513675  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t328513675  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m3241763420 (Texture_t2119925672 * __this, Vector2_t328513675 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_INTERNAL_get_texelSize_m3241763420_ftn) (Texture_t2119925672 *, Vector2_t328513675 *);
	static Texture_INTERNAL_get_texelSize_m3241763420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_INTERNAL_get_texelSize_m3241763420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m1290530517 (Texture2D_t3063074017 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m1290530517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m373917183(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		Texture2D_Internal_Create_m1101533493(NULL /*static, unused*/, __this, L_0, L_1, 4, (bool)1, (bool)0, (intptr_t)(0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture2D__ctor_m2529229425 (Texture2D_t3063074017 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m2529229425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m373917183(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		Texture2D_Internal_Create_m1101533493(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (bool)0, (intptr_t)(0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern "C"  void Texture2D__ctor_m1639172559 (Texture2D_t3063074017 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m1639172559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m373917183(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		bool L_4 = ___linear4;
		Texture2D_Internal_Create_m1101533493(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, (intptr_t)(0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m1101533493 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Internal_Create_m1101533493_ftn) (Texture2D_t3063074017 *, int32_t, int32_t, int32_t, bool, bool, intptr_t);
	static Texture2D_Internal_Create_m1101533493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m1101533493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t3063074017 * Texture2D_get_whiteTexture_m3893489429 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Texture2D_t3063074017 * (*Texture2D_get_whiteTexture_m3893489429_ftn) ();
	static Texture2D_get_whiteTexture_m3893489429_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m3893489429_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	Texture2D_t3063074017 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Texture2D_SetPixel_m2018927763 (Texture2D_t3063074017 * __this, int32_t ___x0, int32_t ___y1, Color_t2582018970  ___color2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		Texture2D_INTERNAL_CALL_SetPixel_m1365054379(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_SetPixel_m1365054379 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___self0, int32_t ___x1, int32_t ___y2, Color_t2582018970 * ___color3, const RuntimeMethod* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_SetPixel_m1365054379_ftn) (Texture2D_t3063074017 *, int32_t, int32_t, Color_t2582018970 *);
	static Texture2D_INTERNAL_CALL_SetPixel_m1365054379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_SetPixel_m1365054379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___x1, ___y2, ___color3);
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t2582018970  Texture2D_GetPixelBilinear_m4073010053 (Texture2D_t3063074017 * __this, float ___u0, float ___v1, const RuntimeMethod* method)
{
	Color_t2582018970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2582018970  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t2582018970  L_2 = V_0;
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Color_t2582018970  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349 (RuntimeObject * __this /* static, unused */, Texture2D_t3063074017 * ___self0, float ___u1, float ___v2, Color_t2582018970 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349_ftn) (Texture2D_t3063074017 *, float, float, Color_t2582018970 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m1934362349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m1815841140 (Texture2D_t3063074017 * __this, ColorU5BU5D_t247935167* ___colors0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t247935167* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m1098893380(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1098893380 (Texture2D_t3063074017 * __this, ColorU5BU5D_t247935167* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel1;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		V_0 = 1;
	}

IL_0016:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel1;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		V_1 = 1;
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t247935167* L_8 = ___colors0;
		int32_t L_9 = ___miplevel1;
		Texture2D_SetPixels_m1623044052(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1623044052 (Texture2D_t3063074017 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t247935167* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetPixels_m1623044052_ftn) (Texture2D_t3063074017 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t247935167*, int32_t);
	static Texture2D_SetPixels_m1623044052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m1623044052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C"  ColorU5BU5D_t247935167* Texture2D_GetPixels_m2185916174 (Texture2D_t3063074017 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ColorU5BU5D_t247935167* V_1 = NULL;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		ColorU5BU5D_t247935167* L_1 = Texture2D_GetPixels_m4026066991(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		ColorU5BU5D_t247935167* L_2 = V_1;
		return L_2;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t247935167* Texture2D_GetPixels_m4026066991 (Texture2D_t3063074017 * __this, int32_t ___miplevel0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ColorU5BU5D_t247935167* V_2 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		V_0 = 1;
	}

IL_0016:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel0;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		V_1 = 1;
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___miplevel0;
		ColorU5BU5D_t247935167* L_9 = Texture2D_GetPixels_m1645989123(__this, 0, 0, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_003c;
	}

IL_003c:
	{
		ColorU5BU5D_t247935167* L_10 = V_2;
		return L_10;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t247935167* Texture2D_GetPixels_m1645989123 (Texture2D_t3063074017 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const RuntimeMethod* method)
{
	typedef ColorU5BU5D_t247935167* (*Texture2D_GetPixels_m1645989123_ftn) (Texture2D_t3063074017 *, int32_t, int32_t, int32_t, int32_t, int32_t);
	static Texture2D_GetPixels_m1645989123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels_m1645989123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)");
	ColorU5BU5D_t247935167* retVal = _il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___miplevel4);
	return retVal;
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m1860200133 (Texture2D_t3063074017 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Apply_m1860200133_ftn) (Texture2D_t3063074017 *, bool, bool);
	static Texture2D_Apply_m1860200133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m1860200133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m3276494578 (Texture2D_t3063074017 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m1860200133(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture3D__ctor_m4087377844 (Texture3D_t2628067514 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, bool ___mipmap4, const RuntimeMethod* method)
{
	{
		Texture__ctor_m373917183(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depth2;
		int32_t L_3 = ___format3;
		bool L_4 = ___mipmap4;
		Texture3D_Internal_Create_m1869539287(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture3D_SetPixels_m4146233771 (Texture3D_t2628067514 * __this, ColorU5BU5D_t247935167* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	typedef void (*Texture3D_SetPixels_m4146233771_ftn) (Texture3D_t2628067514 *, ColorU5BU5D_t247935167*, int32_t);
	static Texture3D_SetPixels_m4146233771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture3D_SetPixels_m4146233771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture3D_SetPixels_m854642347 (Texture3D_t2628067514 * __this, ColorU5BU5D_t247935167* ___colors0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t247935167* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture3D_SetPixels_m4146233771(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture3D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture3D_Apply_m2534196399 (Texture3D_t2628067514 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method)
{
	typedef void (*Texture3D_Apply_m2534196399_ftn) (Texture3D_t2628067514 *, bool, bool);
	static Texture3D_Apply_m2534196399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture3D_Apply_m2534196399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture3D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture3D::Apply()
extern "C"  void Texture3D_Apply_m3472964774 (Texture3D_t2628067514 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture3D_Apply_m2534196399(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture3D_Internal_Create_m1869539287 (RuntimeObject * __this /* static, unused */, Texture3D_t2628067514 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___depth3, int32_t ___format4, bool ___mipmap5, const RuntimeMethod* method)
{
	typedef void (*Texture3D_Internal_Create_m1869539287_ftn) (Texture3D_t2628067514 *, int32_t, int32_t, int32_t, int32_t, bool);
	static Texture3D_Internal_Create_m1869539287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture3D_Internal_Create_m1869539287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___depth3, ___format4, ___mipmap5);
}
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m3627464270 (ThreadAndSerializationSafeAttribute_t878041766 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m3605797736 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_time_m3605797736_ftn) ();
	static Time_get_time_m3605797736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m3605797736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m1083949780 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_timeSinceLevelLoad_m1083949780_ftn) ();
	static Time_get_timeSinceLevelLoad_m1083949780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeSinceLevelLoad_m1083949780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeSinceLevelLoad()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m1955003375 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_deltaTime_m1955003375_ftn) ();
	static Time_get_deltaTime_m1955003375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m1955003375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m1543239379 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledTime_m1543239379_ftn) ();
	static Time_get_unscaledTime_m1543239379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m1543239379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m1853198255 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m1853198255_ftn) ();
	static Time_get_unscaledDeltaTime_m1853198255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m1853198255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_fixedDeltaTime()
extern "C"  float Time_get_fixedDeltaTime_m1176046005 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_fixedDeltaTime_m1176046005_ftn) ();
	static Time_get_fixedDeltaTime_m1176046005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_fixedDeltaTime_m1176046005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_fixedDeltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m1694241990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_timeScale_m1694241990_ftn) ();
	static Time_get_timeScale_m1694241990_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m1694241990_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m242319891 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Time_get_frameCount_m242319891_ftn) ();
	static Time_get_frameCount_m242319891_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m242319891_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m101132769 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m101132769_ftn) ();
	static Time_get_realtimeSinceStartup_m101132769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m101132769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m4010442091 (TooltipAttribute_t2025437004 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1725140046(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m2127391186 (Touch_t1048744301 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_fingerId_m2127391186_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1048744301 * _thisAdjusted = reinterpret_cast<Touch_t1048744301 *>(__this + 1);
	return Touch_get_fingerId_m2127391186(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t328513675  Touch_get_position_m736668558 (Touch_t1048744301 * __this, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t328513675  L_0 = __this->get_m_Position_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t328513675  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t328513675  Touch_get_position_m736668558_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1048744301 * _thisAdjusted = reinterpret_cast<Touch_t1048744301 *>(__this + 1);
	return Touch_get_position_m736668558(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m475972970 (Touch_t1048744301 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Phase_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_phase_m475972970_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1048744301 * _thisAdjusted = reinterpret_cast<Touch_t1048744301 *>(__this + 1);
	return Touch_get_phase_m475972970(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m637716675 (Touch_t1048744301 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_type_m637716675_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1048744301 * _thisAdjusted = reinterpret_cast<Touch_t1048744301 *>(__this + 1);
	return Touch_get_type_m637716675(_thisAdjusted, method);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3418144028 (TouchScreenKeyboard_t1357543767 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m3418144028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(TouchScreenKeyboardType_t1971784194_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2280471388_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m1514246592(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m4047979761(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m4047979761(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m4047979761(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m4047979761(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m2451490838 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m2451490838_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_Destroy_m2451490838_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m2451490838_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m1444211032 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m2451490838(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m350649604(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743 (TouchScreenKeyboard_t1357543767 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743_ftn) (TouchScreenKeyboard_t1357543767 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t1049358371 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2580586743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m739847322 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = Application_get_platform_m4080113311(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))))
		{
			case 0:
			{
				goto IL_006d;
			}
			case 1:
			{
				goto IL_006d;
			}
			case 2:
			{
				goto IL_006d;
			}
			case 3:
			{
				goto IL_0034;
			}
			case 4:
			{
				goto IL_0034;
			}
			case 5:
			{
				goto IL_0066;
			}
			case 6:
			{
				goto IL_0034;
			}
			case 7:
			{
				goto IL_0034;
			}
			case 8:
			{
				goto IL_0066;
			}
		}
	}

IL_0034:
	{
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)((int32_t)30))))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0066;
			}
		}
	}
	{
		int32_t L_3 = V_0;
		switch (((int32_t)((int32_t)L_3-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0074;
			}
			case 2:
			{
				goto IL_0074;
			}
			case 3:
			{
				goto IL_0066;
			}
		}
	}
	{
		goto IL_0074;
	}

IL_0066:
	{
		V_1 = (bool)1;
		goto IL_007b;
	}

IL_006d:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_0074:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_007b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1357543767 * TouchScreenKeyboard_Open_m2953139382 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2953139382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	TouchScreenKeyboard_t1357543767 * V_2 = NULL;
	{
		V_0 = _stringLiteral4185710657;
		V_1 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1357543767 * L_7 = TouchScreenKeyboard_Open_m1341367847(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_001c;
	}

IL_001c:
	{
		TouchScreenKeyboard_t1357543767 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1357543767 * TouchScreenKeyboard_Open_m1445827200 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m1445827200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	TouchScreenKeyboard_t1357543767 * V_3 = NULL;
	{
		V_0 = _stringLiteral4185710657;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = V_2;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1357543767 * L_7 = TouchScreenKeyboard_Open_m1341367847(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		goto IL_001d;
	}

IL_001d:
	{
		TouchScreenKeyboard_t1357543767 * L_8 = V_3;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType)
extern "C"  TouchScreenKeyboard_t1357543767 * TouchScreenKeyboard_Open_m1243226021 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m1243226021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	TouchScreenKeyboard_t1357543767 * V_5 = NULL;
	{
		V_0 = _stringLiteral4185710657;
		V_1 = (bool)0;
		V_2 = (bool)0;
		V_3 = (bool)0;
		V_4 = (bool)1;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = V_4;
		bool L_3 = V_3;
		bool L_4 = V_2;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1357543767 * L_7 = TouchScreenKeyboard_Open_m1341367847(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_5 = L_7;
		goto IL_0024;
	}

IL_0024:
	{
		TouchScreenKeyboard_t1357543767 * L_8 = V_5;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1357543767 * TouchScreenKeyboard_Open_m1341367847 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m1341367847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_t1357543767 * V_0 = NULL;
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t1357543767 * L_7 = (TouchScreenKeyboard_t1357543767 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t1357543767_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m3418144028(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0016;
	}

IL_0016:
	{
		TouchScreenKeyboard_t1357543767 * L_8 = V_0;
		return L_8;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m8652925 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m8652925_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_get_text_m8652925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m8652925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m1586696597 (TouchScreenKeyboard_t1357543767 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m1586696597_ftn) (TouchScreenKeyboard_t1357543767 *, String_t*);
	static TouchScreenKeyboard_set_text_m1586696597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m1586696597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m3365012065 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m3365012065_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m3365012065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m3365012065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m1229524999 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m1229524999_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_get_active_m1229524999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m1229524999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m76013268 (TouchScreenKeyboard_t1357543767 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m76013268_ftn) (TouchScreenKeyboard_t1357543767 *, bool);
	static TouchScreenKeyboard_set_active_m76013268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m76013268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m315855154 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m315855154_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_get_done_m315855154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m315855154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m2152783293 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m2152783293_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_get_wasCanceled_m2152783293_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m2152783293_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern "C"  bool TouchScreenKeyboard_get_canGetSelection_m239612360 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_canGetSelection_m239612360_ftn) (TouchScreenKeyboard_t1357543767 *);
	static TouchScreenKeyboard_get_canGetSelection_m239612360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_canGetSelection_m239612360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_canGetSelection()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern "C"  RangeInt_t66892349  TouchScreenKeyboard_get_selection_m1588574461 (TouchScreenKeyboard_t1357543767 * __this, const RuntimeMethod* method)
{
	RangeInt_t66892349  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RangeInt_t66892349  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t* L_0 = (&V_0)->get_address_of_start_0();
		int32_t* L_1 = (&V_0)->get_address_of_length_1();
		TouchScreenKeyboard_GetSelectionInternal_m820781723(__this, L_0, L_1, /*hidden argument*/NULL);
		RangeInt_t66892349  L_2 = V_0;
		V_1 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		RangeInt_t66892349  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m820781723 (TouchScreenKeyboard_t1357543767 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_GetSelectionInternal_m820781723_ftn) (TouchScreenKeyboard_t1357543767 *, int32_t*, int32_t*);
	static TouchScreenKeyboard_GetSelectionInternal_m820781723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_GetSelectionInternal_m820781723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)");
	_il2cpp_icall_func(__this, ___start0, ___length1);
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2624196464_marshal_pinvoke(const TrackedReference_t2624196464& unmarshaled, TrackedReference_t2624196464_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t2624196464_marshal_pinvoke_back(const TrackedReference_t2624196464_marshaled_pinvoke& marshaled, TrackedReference_t2624196464& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2624196464_marshal_pinvoke_cleanup(TrackedReference_t2624196464_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2624196464_marshal_com(const TrackedReference_t2624196464& unmarshaled, TrackedReference_t2624196464_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t2624196464_marshal_com_back(const TrackedReference_t2624196464_marshaled_com& marshaled, TrackedReference_t2624196464& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2624196464_marshal_com_cleanup(TrackedReference_t2624196464_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.TrackedReference::.ctor()
extern "C"  void TrackedReference__ctor_m1051293765 (TrackedReference_t2624196464 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m2981311363 (RuntimeObject * __this /* static, unused */, TrackedReference_t2624196464 * ___x0, TrackedReference_t2624196464 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m2981311363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	{
		TrackedReference_t2624196464 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t2624196464 * L_1 = ___y1;
		V_1 = L_1;
		RuntimeObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0067;
	}

IL_0018:
	{
		RuntimeObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		TrackedReference_t2624196464 * L_5 = ___x0;
		NullCheck(L_5);
		intptr_t L_6 = L_5->get_m_Ptr_0();
		bool L_7 = IntPtr_op_Equality_m1348886383(NULL /*static, unused*/, L_6, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0067;
	}

IL_0034:
	{
		RuntimeObject * L_8 = V_0;
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		TrackedReference_t2624196464 * L_9 = ___y1;
		NullCheck(L_9);
		intptr_t L_10 = L_9->get_m_Ptr_0();
		bool L_11 = IntPtr_op_Equality_m1348886383(NULL /*static, unused*/, L_10, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_11;
		goto IL_0067;
	}

IL_0050:
	{
		TrackedReference_t2624196464 * L_12 = ___x0;
		NullCheck(L_12);
		intptr_t L_13 = L_12->get_m_Ptr_0();
		TrackedReference_t2624196464 * L_14 = ___y1;
		NullCheck(L_14);
		intptr_t L_15 = L_14->get_m_Ptr_0();
		bool L_16 = IntPtr_op_Equality_m1348886383(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		goto IL_0067;
	}

IL_0067:
	{
		bool L_17 = V_2;
		return L_17;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern "C"  bool TrackedReference_Equals_m3282386582 (TrackedReference_t2624196464 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m3282386582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m2981311363(NULL /*static, unused*/, ((TrackedReference_t2624196464 *)IsInstClass((RuntimeObject*)L_0, TrackedReference_t2624196464_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m289370444 (TrackedReference_t2624196464 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m1509168872(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Transform::.ctor()
extern "C"  void Transform__ctor_m1946354846 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m1184908066(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t1986933152  Transform_get_position_m3101184820 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_position_m14402994(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2724629716 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_position_m2987532667(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m14402994 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_position_m14402994_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_get_position_m14402994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m14402994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m2987532667 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_position_m2987532667_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_set_position_m2987532667_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m2987532667_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t1986933152  Transform_get_localPosition_m3192377339 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localPosition_m4096849723(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1624079682 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localPosition_m1215190010(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m4096849723 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m4096849723_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_get_localPosition_m4096849723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m4096849723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m1215190010 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m1215190010_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_set_localPosition_m1215190010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m1215190010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t1986933152  Transform_get_eulerAngles_m2908492024 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_t704191599  L_0 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t1986933152  L_1 = Quaternion_get_eulerAngles_m2988902996((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0015;
	}

IL_0015:
	{
		Vector3_t1986933152  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m266161425 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_eulerAngles_m266161425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_1 = Quaternion_Euler_m3333075930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m609809763(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m272559017 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_localEulerAngles_m272559017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_1 = Quaternion_Euler_m3333075930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_localRotation_m2730169435(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t1986933152  Transform_get_right_m4293898467 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_right_m4293898467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t704191599  L_0 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = Vector3_get_right_m878387312(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Quaternion_op_Multiply_m2036929790(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Transform::set_right(UnityEngine.Vector3)
extern "C"  void Transform_set_right_m2128067954 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_right_m2128067954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = Vector3_get_right_m878387312(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_2 = Quaternion_FromToRotation_m2140668740(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Transform_set_rotation_m609809763(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t1986933152  Transform_get_up_m3741264314 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_up_m3741264314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t704191599  L_0 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = Vector3_get_up_m467978780(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Quaternion_op_Multiply_m2036929790(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t1986933152  Transform_get_forward_m3174539474 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_forward_m3174539474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t704191599  L_0 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_1 = Vector3_get_forward_m953890947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Quaternion_op_Multiply_m2036929790(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t704191599  Transform_get_rotation_m317637018 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t704191599  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_rotation_m3071042677(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t704191599  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t704191599  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m609809763 (Transform_t362059596 * __this, Quaternion_t704191599  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_rotation_m2478131545(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m3071042677 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m3071042677_ftn) (Transform_t362059596 *, Quaternion_t704191599 *);
	static Transform_INTERNAL_get_rotation_m3071042677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m3071042677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2478131545 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m2478131545_ftn) (Transform_t362059596 *, Quaternion_t704191599 *);
	static Transform_INTERNAL_set_rotation_m2478131545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m2478131545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t704191599  Transform_get_localRotation_m934836654 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t704191599  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localRotation_m3917542876(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t704191599  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t704191599  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m2730169435 (Transform_t362059596 * __this, Quaternion_t704191599  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localRotation_m3633882113(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m3917542876 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m3917542876_ftn) (Transform_t362059596 *, Quaternion_t704191599 *);
	static Transform_INTERNAL_get_localRotation_m3917542876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m3917542876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m3633882113 (Transform_t362059596 * __this, Quaternion_t704191599 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m3633882113_ftn) (Transform_t362059596 *, Quaternion_t704191599 *);
	static Transform_INTERNAL_set_localRotation_m3633882113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m3633882113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t1986933152  Transform_get_localScale_m3081928808 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localScale_m3696539245(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m1041063835 (Transform_t362059596 * __this, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localScale_m1480886321(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m3696539245 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m3696539245_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_get_localScale_m3696539245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m3696539245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m1480886321 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m1480886321_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_set_localScale_m1480886321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m1480886321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t362059596 * Transform_get_parent_m1661917747 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Transform_t362059596 * V_0 = NULL;
	{
		Transform_t362059596 * L_0 = Transform_get_parentInternal_m2338151590(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Transform_t362059596 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m3838886759 (Transform_t362059596 * __this, Transform_t362059596 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_parent_m3838886759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		if (!((RectTransform_t859616204 *)IsInstSealed((RuntimeObject*)__this, RectTransform_t859616204_il2cpp_TypeInfo_var)))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1775422903(NULL /*static, unused*/, _stringLiteral3127190363, __this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Transform_t362059596 * L_0 = ___value0;
		Transform_set_parentInternal_m2226751317(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t362059596 * Transform_get_parentInternal_m2338151590 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	typedef Transform_t362059596 * (*Transform_get_parentInternal_m2338151590_ftn) (Transform_t362059596 *);
	static Transform_get_parentInternal_m2338151590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m2338151590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	Transform_t362059596 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m2226751317 (Transform_t362059596 * __this, Transform_t362059596 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_set_parentInternal_m2226751317_ftn) (Transform_t362059596 *, Transform_t362059596 *);
	static Transform_set_parentInternal_m2226751317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m2226751317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m1065784181 (Transform_t362059596 * __this, Transform_t362059596 * ___parent0, const RuntimeMethod* method)
{
	{
		Transform_t362059596 * L_0 = ___parent0;
		Transform_SetParent_m1496610538(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m1496610538 (Transform_t362059596 * __this, Transform_t362059596 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method)
{
	typedef void (*Transform_SetParent_m1496610538_ftn) (Transform_t362059596 *, Transform_t362059596 *, bool);
	static Transform_SetParent_m1496610538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m1496610538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t1237934469  Transform_get_worldToLocalMatrix_m3199522029 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m577067920(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t1237934469  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m577067920 (Transform_t362059596 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m577067920_ftn) (Transform_t362059596 *, Matrix4x4_t1237934469 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m577067920_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m577067920_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t1237934469  Transform_get_localToWorldMatrix_m1130524736 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t1237934469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1237934469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localToWorldMatrix_m3833575919(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1237934469  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t1237934469  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m3833575919 (Transform_t362059596 * __this, Matrix4x4_t1237934469 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m3833575919_ftn) (Transform_t362059596 *, Matrix4x4_t1237934469 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m3833575919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m3833575919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m2970246227 (Transform_t362059596 * __this, Vector3_t1986933152  ___translation0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t1986933152  L_0 = ___translation0;
		int32_t L_1 = V_0;
		Transform_Translate_m219233999(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m219233999 (Transform_t362059596 * __this, Vector3_t1986933152  ___translation0, int32_t ___relativeTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_Translate_m219233999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___relativeTo1;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t1986933152  L_1 = Transform_get_position_m3101184820(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_2 = ___translation0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_3 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m2724629716(__this, L_3, /*hidden argument*/NULL);
		goto IL_0036;
	}

IL_001e:
	{
		Vector3_t1986933152  L_4 = Transform_get_position_m3101184820(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_5 = ___translation0;
		Vector3_t1986933152  L_6 = Transform_TransformDirection_m4117630985(__this, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_7 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m2724629716(__this, L_7, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m3191668733 (Transform_t362059596 * __this, Vector3_t1986933152  ___eulerAngles0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t1986933152  L_0 = ___eulerAngles0;
		int32_t L_1 = V_0;
		Transform_Rotate_m4266981785(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m4266981785 (Transform_t362059596 * __this, Vector3_t1986933152  ___eulerAngles0, int32_t ___relativeTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_Rotate_m4266981785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t704191599  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___eulerAngles0)->get_x_1();
		float L_1 = (&___eulerAngles0)->get_y_2();
		float L_2 = (&___eulerAngles0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_3 = Quaternion_Euler_m1199696711(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_003a;
		}
	}
	{
		Quaternion_t704191599  L_5 = Transform_get_localRotation_m934836654(__this, /*hidden argument*/NULL);
		Quaternion_t704191599  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_7 = Quaternion_op_Multiply_m3500285847(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m2730169435(__this, L_7, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_003a:
	{
		Quaternion_t704191599  L_8 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		Quaternion_t704191599  L_9 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_10 = Quaternion_Inverse_m2151856247(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t704191599  L_11 = V_0;
		Quaternion_t704191599  L_12 = Quaternion_op_Multiply_m3500285847(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t704191599  L_13 = Transform_get_rotation_m317637018(__this, /*hidden argument*/NULL);
		Quaternion_t704191599  L_14 = Quaternion_op_Multiply_m3500285847(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t704191599  L_15 = Quaternion_op_Multiply_m3500285847(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m609809763(__this, L_15, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Rotate_m2537472123 (Transform_t362059596 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		float L_0 = ___xAngle0;
		float L_1 = ___yAngle1;
		float L_2 = ___zAngle2;
		int32_t L_3 = V_0;
		Transform_Rotate_m3984935415(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m3984935415 (Transform_t362059596 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, int32_t ___relativeTo3, const RuntimeMethod* method)
{
	{
		float L_0 = ___xAngle0;
		float L_1 = ___yAngle1;
		float L_2 = ___zAngle2;
		Vector3_t1986933152  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3282197930((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo3;
		Transform_Rotate_m4266981785(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAroundInternal_m3765567956 (Transform_t362059596 * __this, Vector3_t1986933152  ___axis0, float ___angle1, const RuntimeMethod* method)
{
	{
		float L_0 = ___angle1;
		Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768(NULL /*static, unused*/, __this, (&___axis0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
extern "C"  void Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___axis1, float ___angle2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, float);
	static Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_RotateAroundInternal_m3687696768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self0, ___axis1, ___angle2);
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_Rotate_m3459573376 (Transform_t362059596 * __this, Vector3_t1986933152  ___axis0, float ___angle1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t1986933152  L_0 = ___axis0;
		float L_1 = ___angle1;
		int32_t L_2 = V_0;
		Transform_Rotate_m2802219837(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m2802219837 (Transform_t362059596 * __this, Vector3_t1986933152  ___axis0, float ___angle1, int32_t ___relativeTo2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___relativeTo2;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0026;
		}
	}
	{
		Transform_t362059596 * L_1 = Component_get_transform_m520192871(__this, /*hidden argument*/NULL);
		Vector3_t1986933152  L_2 = ___axis0;
		NullCheck(L_1);
		Vector3_t1986933152  L_3 = Transform_TransformDirection_m4117630985(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___angle1;
		Transform_RotateAroundInternal_m3765567956(__this, L_3, ((float)((float)L_4*(float)(0.0174532924f))), /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0026:
	{
		Vector3_t1986933152  L_5 = ___axis0;
		float L_6 = ___angle1;
		Transform_RotateAroundInternal_m3765567956(__this, L_5, ((float)((float)L_6*(float)(0.0174532924f))), /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAround_m3597956458 (Transform_t362059596 * __this, Vector3_t1986933152  ___point0, Vector3_t1986933152  ___axis1, float ___angle2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_RotateAround_m3597956458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t704191599  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t1986933152  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t1986933152  L_0 = Transform_get_position_m3101184820(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___angle2;
		Vector3_t1986933152  L_2 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t704191599_il2cpp_TypeInfo_var);
		Quaternion_t704191599  L_3 = Quaternion_AngleAxis_m1713259603(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t1986933152  L_4 = V_0;
		Vector3_t1986933152  L_5 = ___point0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_6 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Quaternion_t704191599  L_7 = V_1;
		Vector3_t1986933152  L_8 = V_2;
		Vector3_t1986933152  L_9 = Quaternion_op_Multiply_m2036929790(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t1986933152  L_10 = ___point0;
		Vector3_t1986933152  L_11 = V_2;
		Vector3_t1986933152  L_12 = Vector3_op_Addition_m1125374618(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t1986933152  L_13 = V_0;
		Transform_set_position_m2724629716(__this, L_13, /*hidden argument*/NULL);
		Vector3_t1986933152  L_14 = ___axis1;
		float L_15 = ___angle2;
		Transform_RotateAroundInternal_m3765567956(__this, L_14, ((float)((float)L_15*(float)(0.0174532924f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C"  void Transform_LookAt_m1576954949 (Transform_t362059596 * __this, Transform_t362059596 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_LookAt_m1576954949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = Vector3_get_up_m467978780(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t362059596 * L_1 = ___target0;
		Vector3_t1986933152  L_2 = V_0;
		Transform_LookAt_m169160536(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m169160536 (Transform_t362059596 * __this, Transform_t362059596 * ___target0, Vector3_t1986933152  ___worldUp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_LookAt_m169160536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t362059596 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3123183460(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Transform_t362059596 * L_2 = ___target0;
		NullCheck(L_2);
		Vector3_t1986933152  L_3 = Transform_get_position_m3101184820(L_2, /*hidden argument*/NULL);
		Vector3_t1986933152  L_4 = ___worldUp1;
		Transform_LookAt_m4187688555(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m4187688555 (Transform_t362059596 * __this, Vector3_t1986933152  ___worldPosition0, Vector3_t1986933152  ___worldUp1, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m4110212569(NULL /*static, unused*/, __this, (&___worldPosition0), (&___worldUp1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m483134985 (Transform_t362059596 * __this, Vector3_t1986933152  ___worldPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_LookAt_m483134985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = Vector3_get_up_m467978780(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_INTERNAL_CALL_LookAt_m4110212569(NULL /*static, unused*/, __this, (&___worldPosition0), (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_LookAt_m4110212569 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___worldPosition1, Vector3_t1986933152 * ___worldUp2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m4110212569_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_LookAt_m4110212569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m4110212569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPosition1, ___worldUp2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_TransformDirection_m4117630985 (Transform_t362059596 * __this, Vector3_t1986933152  ___direction0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformDirection_m3141765377(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m3141765377 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___direction1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformDirection_m3141765377_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_TransformDirection_m3141765377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m3141765377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_InverseTransformDirection_m1807423081 (Transform_t362059596 * __this, Vector3_t1986933152  ___direction0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformDirection_m925141460(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m925141460 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___direction1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformDirection_m925141460_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_InverseTransformDirection_m925141460_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformDirection_m925141460_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformVector(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_TransformVector_m2993650824 (Transform_t362059596 * __this, Vector3_t1986933152  ___vector0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformVector_m3369314559(NULL /*static, unused*/, __this, (&___vector0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformVector_m3369314559 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___vector1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformVector_m3369314559_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_TransformVector_m3369314559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformVector_m3369314559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___vector1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_TransformPoint_m2769075246 (Transform_t362059596 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformPoint_m2509381006(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m2509381006 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m2509381006_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_TransformPoint_m2509381006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m2509381006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Transform_InverseTransformPoint_m3240399982 (Transform_t362059596 * __this, Vector3_t1986933152  ___position0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m324663584(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m324663584 (RuntimeObject * __this /* static, unused */, Transform_t362059596 * ___self0, Vector3_t1986933152 * ___position1, Vector3_t1986933152 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m324663584_ftn) (Transform_t362059596 *, Vector3_t1986933152 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m324663584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m324663584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m3678280669 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Transform_get_childCount_m3678280669_ftn) (Transform_t362059596 *);
	static Transform_get_childCount_m3678280669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m3678280669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::DetachChildren()
extern "C"  void Transform_DetachChildren_m763200202 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	typedef void (*Transform_DetachChildren_m763200202_ftn) (Transform_t362059596 *);
	static Transform_DetachChildren_m763200202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_DetachChildren_m763200202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::DetachChildren()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m926641648 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	typedef void (*Transform_SetAsFirstSibling_m926641648_ftn) (Transform_t362059596 *);
	static Transform_SetAsFirstSibling_m926641648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m926641648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t362059596 * Transform_Find_m209989494 (Transform_t362059596 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef Transform_t362059596 * (*Transform_Find_m209989494_ftn) (Transform_t362059596 *, String_t*);
	static Transform_Find_m209989494_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m209989494_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	Transform_t362059596 * retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t1986933152  Transform_get_lossyScale_m199306114 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_lossyScale_m3090738516(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t1986933152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_lossyScale_m3090738516 (Transform_t362059596 * __this, Vector3_t1986933152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m3090738516_ftn) (Transform_t362059596 *, Vector3_t1986933152 *);
	static Transform_INTERNAL_get_lossyScale_m3090738516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m3090738516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m366908815 (Transform_t362059596 * __this, Transform_t362059596 * ___parent0, const RuntimeMethod* method)
{
	typedef bool (*Transform_IsChildOf_m366908815_ftn) (Transform_t362059596 *, Transform_t362059596 *);
	static Transform_IsChildOf_m366908815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m366908815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	bool retVal = _il2cpp_icall_func(__this, ___parent0);
	return retVal;
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  RuntimeObject* Transform_GetEnumerator_m3479571372 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m3479571372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t261617688 * L_0 = (Enumerator_t261617688 *)il2cpp_codegen_object_new(Enumerator_t261617688_il2cpp_TypeInfo_var);
		Enumerator__ctor_m3133563284(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t362059596 * Transform_GetChild_m260943136 (Transform_t362059596 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef Transform_t362059596 * (*Transform_GetChild_m260943136_ftn) (Transform_t362059596 *, int32_t);
	static Transform_GetChild_m260943136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m260943136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	Transform_t362059596 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Int32 UnityEngine.Transform::GetChildCount()
extern "C"  int32_t Transform_GetChildCount_m117194481 (Transform_t362059596 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Transform_GetChildCount_m117194481_ftn) (Transform_t362059596 *);
	static Transform_GetChildCount_m117194481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChildCount_m117194481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChildCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3133563284 (Enumerator_t261617688 * __this, Transform_t362059596 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		Transform_t362059596 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2952545756 (Enumerator_t261617688 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Transform_t362059596 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t362059596 * L_2 = Transform_GetChild_m260943136(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2769456620 (Enumerator_t261617688 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Transform_t362059596 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m3678280669(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		goto IL_0027;
	}

IL_0027:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m3611301832 (Enumerator_t261617688 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern "C"  bool SpriteAtlasManager_RequestAtlas_m3877826629 (RuntimeObject * __this /* static, unused */, String_t* ___tag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager_RequestAtlas_m3877826629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B3_0 = NULL;
	RequestAtlasCallback_t1427750696 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	RequestAtlasCallback_t1427750696 * G_B2_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t1427750696 * L_0 = ((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t1427750696 * L_1 = ((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		String_t* L_2 = ___tag0;
		Action_1_t3400447372 * L_3 = ((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_002a;
		}
	}
	{
		intptr_t L_4 = (intptr_t)SpriteAtlasManager_Register_m1909933228_RuntimeMethod_var;
		Action_1_t3400447372 * L_5 = (Action_1_t3400447372 *)il2cpp_codegen_object_new(Action_1_t3400447372_il2cpp_TypeInfo_var);
		Action_1__ctor_m2452183723(L_5, NULL, L_4, /*hidden argument*/Action_1__ctor_m2452183723_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var);
		((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_1(L_5);
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var);
		Action_1_t3400447372 * L_6 = ((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		NullCheck(G_B3_1);
		RequestAtlasCallback_Invoke_m3858662825(G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0042;
	}

IL_003b:
	{
		V_0 = (bool)0;
		goto IL_0042;
	}

IL_0042:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern "C"  void SpriteAtlasManager_Register_m1909933228 (RuntimeObject * __this /* static, unused */, SpriteAtlas_t1129391324 * ___spriteAtlas0, const RuntimeMethod* method)
{
	typedef void (*SpriteAtlasManager_Register_m1909933228_ftn) (SpriteAtlas_t1129391324 *);
	static SpriteAtlasManager_Register_m1909933228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteAtlasManager_Register_m1909933228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)");
	_il2cpp_icall_func(___spriteAtlas0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern "C"  void SpriteAtlasManager__cctor_m2900083981 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager__cctor_m2900083981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SpriteAtlasManager_t901682312_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t901682312_il2cpp_TypeInfo_var))->set_atlasRequested_0((RequestAtlasCallback_t1427750696 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestAtlasCallback_t1427750696 (RequestAtlasCallback_t1427750696 * __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, Il2CppMethodPointer);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___tag0' to native representation
	char* ____tag0_marshaled = NULL;
	____tag0_marshaled = il2cpp_codegen_marshal_string(___tag0);

	// Marshaling of parameter '___action1' to native representation
	Il2CppMethodPointer ____action1_marshaled = NULL;
	____action1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___action1));

	// Native function invocation
	il2cppPInvokeFunc(____tag0_marshaled, ____action1_marshaled);

	// Marshaling cleanup of parameter '___tag0' native representation
	il2cpp_codegen_marshal_free(____tag0_marshaled);
	____tag0_marshaled = NULL;

}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestAtlasCallback__ctor_m1475249938 (RequestAtlasCallback_t1427750696 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m3858662825 (RequestAtlasCallback_t1427750696 * __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestAtlasCallback_Invoke_m3858662825((RequestAtlasCallback_t1427750696 *)__this->get_prev_9(),___tag0, ___action1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Action_1_t3400447372 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::BeginInvoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* RequestAtlasCallback_BeginInvoke_m3931701011 (RequestAtlasCallback_t1427750696 * __this, String_t* ___tag0, Action_1_t3400447372 * ___action1, AsyncCallback_t3561663063 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___tag0;
	__d_args[1] = ___action1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RequestAtlasCallback_EndInvoke_m26955631 (RequestAtlasCallback_t1427750696 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m3071828837 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m3071828837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppDomain_t3842411093 * G_B2_0 = NULL;
	AppDomain_t3842411093 * G_B1_0 = NULL;
	{
		AppDomain_t3842411093 * L_0 = AppDomain_get_CurrentDomain_m467664504(NULL /*static, unused*/, /*hidden argument*/NULL);
		UnhandledExceptionEventHandler_t3749840881 * L_1 = ((UnhandledExceptionHandler_t4175730415_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4175730415_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		intptr_t L_2 = (intptr_t)UnhandledExceptionHandler_HandleUnhandledException_m3220153098_RuntimeMethod_var;
		UnhandledExceptionEventHandler_t3749840881 * L_3 = (UnhandledExceptionEventHandler_t3749840881 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t3749840881_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m3638669703(L_3, NULL, L_2, /*hidden argument*/NULL);
		((UnhandledExceptionHandler_t4175730415_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4175730415_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		UnhandledExceptionEventHandler_t3749840881 * L_4 = ((UnhandledExceptionHandler_t4175730415_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4175730415_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		NullCheck(G_B2_0);
		AppDomain_add_UnhandledException_m308340575(G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m3220153098 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___sender0, UnhandledExceptionEventArgs_t1939597804 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m3220153098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t4086964929 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t1939597804 * L_0 = ___args1;
		NullCheck(L_0);
		RuntimeObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m2034580413(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t4086964929 *)IsInstClass((RuntimeObject*)L_1, Exception_t4086964929_il2cpp_TypeInfo_var));
		Exception_t4086964929 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Exception_t4086964929 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m72583591(NULL /*static, unused*/, _stringLiteral2212345298, L_3, /*hidden argument*/NULL);
		Exception_t4086964929 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Exception_GetType_m2106978941(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		Exception_t4086964929 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_7);
		Exception_t4086964929 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_StackTrace() */, L_9);
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		goto IL_004b;
	}

IL_0041:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422(NULL /*static, unused*/, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m72583591 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t4086964929 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m72583591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t4086964929 * L_0 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3419099923_il2cpp_TypeInfo_var);
		Debug_LogException_m1994504921(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t4086964929 * L_1 = ___e1;
		NullCheck(L_1);
		Exception_t4086964929 * L_2 = Exception_get_InnerException_m1086399415(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Exception_t4086964929 * L_3 = ___e1;
		NullCheck(L_3);
		Exception_t4086964929 * L_4 = Exception_get_InnerException_m1086399415(L_3, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m72583591(NULL /*static, unused*/, _stringLiteral1675908104, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422_ftn) (String_t*, String_t*, String_t*);
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m63527422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)");
	_il2cpp_icall_func(___managedExceptionType0, ___managedExceptionMessage1, ___managedExceptionStack2);
}
// System.Void UnityEngine.UnityException::.ctor()
extern "C"  void UnityException__ctor_m3060899425 (UnityException_t1933799526 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m3060899425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception__ctor_m3397592417(__this, _stringLiteral2610399101, /*hidden argument*/NULL);
		Exception_set_HResult_m2899388954(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m327838295 (UnityException_t1933799526 * __this, SerializationInfo_t259346668 * ___info0, StreamingContext_t648298805  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t259346668 * L_0 = ___info0;
		StreamingContext_t648298805  L_1 = ___context1;
		Exception__ctor_m2650806397(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m3298642088 (UnityLogWriter_t1450454900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter__ctor_m3298642088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextWriter_t997444721_il2cpp_TypeInfo_var);
		TextWriter__ctor_m1383723067(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m3425010958 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	typedef void (*UnityLogWriter_WriteStringToUnityLog_m3425010958_ftn) (String_t*);
	static UnityLogWriter_WriteStringToUnityLog_m3425010958_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityLogWriter_WriteStringToUnityLog_m3425010958_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)");
	_il2cpp_icall_func(___s0);
}
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m532212392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter_Init_m532212392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityLogWriter_t1450454900 * L_0 = (UnityLogWriter_t1450454900 *)il2cpp_codegen_object_new(UnityLogWriter_t1450454900_il2cpp_TypeInfo_var);
		UnityLogWriter__ctor_m3298642088(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t910768770_il2cpp_TypeInfo_var);
		Console_SetOut_m4234273152(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Text.Encoding UnityEngine.UnityLogWriter::get_Encoding()
extern "C"  Encoding_t4004889433 * UnityLogWriter_get_Encoding_m3793726011 (UnityLogWriter_t1450454900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter_get_Encoding_m3793726011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Encoding_t4004889433 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t4004889433_il2cpp_TypeInfo_var);
		Encoding_t4004889433 * L_0 = Encoding_get_UTF8_m3585116113(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Encoding_t4004889433 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern "C"  void UnityLogWriter_Write_m2466655059 (UnityLogWriter_t1450454900 * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Char_ToString_m910852106((&___value0), /*hidden argument*/NULL);
		UnityLogWriter_WriteStringToUnityLog_m3425010958(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern "C"  void UnityLogWriter_Write_m3953790140 (UnityLogWriter_t1450454900 * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		UnityLogWriter_WriteStringToUnityLog_m3425010958(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3950186024 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2737604620* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m3950186024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t2737604620* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m29320051(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m3918494648 (UnitySynchronizationContext_t485758571 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__ctor_m3918494648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t1480213181 * L_0 = (Queue_1_t1480213181 *)il2cpp_codegen_object_new(Queue_1_t1480213181_il2cpp_TypeInfo_var);
		Queue_1__ctor_m528467501(L_0, ((int32_t)20), /*hidden argument*/Queue_1__ctor_m528467501_RuntimeMethod_var);
		__this->set_m_AsyncWorkQueue_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t322231692_il2cpp_TypeInfo_var);
		Thread_t322231692 * L_1 = Thread_get_CurrentThread_m1475819766(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m2189280200(L_1, /*hidden argument*/NULL);
		__this->set_m_MainThreadID_2(L_2);
		SynchronizationContext__ctor_m3027455558(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m2533712166 (UnitySynchronizationContext_t485758571 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Exec_m2533712166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	WorkRequest_t1716204206  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t4086964929 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t4086964929 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t1480213181 * L_0 = __this->get_m_AsyncWorkQueue_1();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m1950427727(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0014:
		{
			Queue_1_t1480213181 * L_2 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_2);
			WorkRequest_t1716204206  L_3 = Queue_1_Dequeue_m1469166360(L_2, /*hidden argument*/Queue_1_Dequeue_m1469166360_RuntimeMethod_var);
			V_1 = L_3;
			WorkRequest_Invoke_m2597236312((&V_1), /*hidden argument*/NULL);
		}

IL_0029:
		{
			Queue_1_t1480213181 * L_4 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_4);
			int32_t L_5 = Queue_1_get_Count_m1367379469(L_4, /*hidden argument*/Queue_1_get_Count_m1367379469_RuntimeMethod_var);
			if ((((int32_t)L_5) > ((int32_t)0)))
			{
				goto IL_0014;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t4086964929 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		RuntimeObject * L_6 = V_0;
		Monitor_Exit_m1791852365(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t4086964929 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern "C"  void UnitySynchronizationContext_InitializeSynchronizationContext_m911367017 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_InitializeSynchronizationContext_m911367017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SynchronizationContext_t2274409467 * L_0 = SynchronizationContext_get_Current_m2659194758(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		UnitySynchronizationContext_t485758571 * L_1 = (UnitySynchronizationContext_t485758571 *)il2cpp_codegen_object_new(UnitySynchronizationContext_t485758571_il2cpp_TypeInfo_var);
		UnitySynchronizationContext__ctor_m3918494648(L_1, /*hidden argument*/NULL);
		SynchronizationContext_SetSynchronizationContext_m190080245(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern "C"  void UnitySynchronizationContext_ExecuteTasks_m1569520204 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_ExecuteTasks_m1569520204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnitySynchronizationContext_t485758571 * V_0 = NULL;
	{
		SynchronizationContext_t2274409467 * L_0 = SynchronizationContext_get_Current_m2659194758(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((UnitySynchronizationContext_t485758571 *)IsInstSealed((RuntimeObject*)L_0, UnitySynchronizationContext_t485758571_il2cpp_TypeInfo_var));
		UnitySynchronizationContext_t485758571 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UnitySynchronizationContext_t485758571 * L_2 = V_0;
		NullCheck(L_2);
		UnitySynchronizationContext_Exec_m2533712166(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t1716204206_marshal_pinvoke(const WorkRequest_t1716204206& unmarshaled, WorkRequest_t1716204206_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t1716204206_marshal_pinvoke_back(const WorkRequest_t1716204206_marshaled_pinvoke& marshaled, WorkRequest_t1716204206& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t1716204206_marshal_pinvoke_cleanup(WorkRequest_t1716204206_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t1716204206_marshal_com(const WorkRequest_t1716204206& unmarshaled, WorkRequest_t1716204206_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t1716204206_marshal_com_back(const WorkRequest_t1716204206_marshaled_com& marshaled, WorkRequest_t1716204206& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t1716204206_marshal_com_cleanup(WorkRequest_t1716204206_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m2597236312 (WorkRequest_t1716204206 * __this, const RuntimeMethod* method)
{
	{
		SendOrPostCallback_t2349539412 * L_0 = __this->get_m_DelagateCallback_0();
		RuntimeObject * L_1 = __this->get_m_DelagateState_1();
		NullCheck(L_0);
		SendOrPostCallback_Invoke_m1935197691(L_0, L_1, /*hidden argument*/NULL);
		ManualResetEvent_t504895258 * L_2 = __this->get_m_WaitHandle_2();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		ManualResetEvent_t504895258 * L_3 = __this->get_m_WaitHandle_2();
		NullCheck(L_3);
		EventWaitHandle_Set_m724129446(L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void WorkRequest_Invoke_m2597236312_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	WorkRequest_t1716204206 * _thisAdjusted = reinterpret_cast<WorkRequest_t1716204206 *>(__this + 1);
	WorkRequest_Invoke_m2597236312(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m22874897 (Vector2_t328513675 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m22874897_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	Vector2__ctor_m22874897(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m1634634565 (Vector2_t328513675 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m1634634565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = __this->get_x_0();
		V_0 = L_2;
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = __this->get_y_1();
		V_0 = L_3;
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t2909334802 * L_4 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_4, _stringLiteral442860306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector2_get_Item_m1634634565_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_get_Item_m1634634565(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m2125498180 (Vector2_t328513675 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_set_Item_m2125498180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = ___value1;
		__this->set_x_0(L_2);
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = ___value1;
		__this->set_y_1(L_3);
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t2909334802 * L_4 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_4, _stringLiteral442860306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		return;
	}
}
extern "C"  void Vector2_set_Item_m2125498180_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	Vector2_set_Item_m2125498180(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector2::Set(System.Single,System.Single)
extern "C"  void Vector2_Set_m1028988183 (Vector2_t328513675 * __this, float ___newX0, float ___newY1, const RuntimeMethod* method)
{
	{
		float L_0 = ___newX0;
		__this->set_x_0(L_0);
		float L_1 = ___newY1;
		__this->set_y_1(L_1);
		return;
	}
}
extern "C"  void Vector2_Set_m1028988183_AdjustorThunk (RuntimeObject * __this, float ___newX0, float ___newY1, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	Vector2_Set_m1028988183(_thisAdjusted, ___newX0, ___newY1, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t328513675  Vector2_Lerp_m2779999928 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Lerp_m2779999928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_0();
		float L_3 = (&___b1)->get_x_0();
		float L_4 = (&___a0)->get_x_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_1();
		float L_7 = (&___b1)->get_y_1();
		float L_8 = (&___a0)->get_y_1();
		float L_9 = ___t2;
		Vector2_t328513675  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m22874897((&L_10), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_0046;
	}

IL_0046:
	{
		Vector2_t328513675  L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t328513675  Vector2_Scale_m624125632 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t328513675  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Vector2::Scale(UnityEngine.Vector2)
extern "C"  void Vector2_Scale_m1155063387 (Vector2_t328513675 * __this, Vector2_t328513675  ___scale0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_x_0();
		float L_1 = (&___scale0)->get_x_0();
		__this->set_x_0(((float)((float)L_0*(float)L_1)));
		float L_2 = __this->get_y_1();
		float L_3 = (&___scale0)->get_y_1();
		__this->set_y_1(((float)((float)L_2*(float)L_3)));
		return;
	}
}
extern "C"  void Vector2_Scale_m1155063387_AdjustorThunk (RuntimeObject * __this, Vector2_t328513675  ___scale0, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	Vector2_Scale_m1155063387(_thisAdjusted, ___scale0, method);
}
// System.Void UnityEngine.Vector2::Normalize()
extern "C"  void Vector2_Normalize_m2506338390 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Normalize_m2506338390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Vector2_get_magnitude_m3070475644(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002a;
		}
	}
	{
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_3 = Vector2_op_Division_m4163545787(NULL /*static, unused*/, (*(Vector2_t328513675 *)__this), L_2, /*hidden argument*/NULL);
		*(Vector2_t328513675 *)__this = L_3;
		goto IL_0035;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_4 = Vector2_get_zero_m187309959(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector2_t328513675 *)__this = L_4;
	}

IL_0035:
	{
		return;
	}
}
extern "C"  void Vector2_Normalize_m2506338390_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	Vector2_Normalize_m2506338390(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t328513675  Vector2_get_normalized_m2925455182 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		Vector2__ctor_m22874897((&V_0), L_0, L_1, /*hidden argument*/NULL);
		Vector2_Normalize_m2506338390((&V_0), /*hidden argument*/NULL);
		Vector2_t328513675  L_2 = V_0;
		V_1 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		Vector2_t328513675  L_3 = V_1;
		return L_3;
	}
}
extern "C"  Vector2_t328513675  Vector2_get_normalized_m2925455182_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_get_normalized_m2925455182(_thisAdjusted, method);
}
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m493646592 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m493646592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral492600876, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Vector2_ToString_m493646592_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_ToString_m493646592(_thisAdjusted, method);
}
// System.String UnityEngine.Vector2::ToString(System.String)
extern "C"  String_t* Vector2_ToString_m1956738200 (Vector2_t328513675 * __this, String_t* ___format0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m1956738200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)2));
		float* L_1 = __this->get_address_of_x_0();
		String_t* L_2 = ___format0;
		String_t* L_3 = Single_ToString_m356387322(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		float* L_5 = __this->get_address_of_y_1();
		String_t* L_6 = ___format0;
		String_t* L_7 = Single_ToString_m356387322(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral1564422252, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0035;
	}

IL_0035:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Vector2_ToString_m1956738200_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_ToString_m1956738200(_thisAdjusted, ___format0, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m3492920259 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m2097607317(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m2097607317(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_002c;
	}

IL_002c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t Vector2_GetHashCode_m3492920259_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_GetHashCode_m3492920259(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m3452260837 (Vector2_t328513675 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m3452260837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector2_t328513675  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector2_t328513675_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_004c;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector2_t328513675 *)((Vector2_t328513675 *)UnBox(L_1, Vector2_t328513675_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m2960811275(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m2960811275(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004c;
	}

IL_004c:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Vector2_Equals_m3452260837_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_Equals_m3452260837(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m3309505425 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___lhs0, Vector2_t328513675  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___lhs0)->get_x_0();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_y_1();
		float L_3 = (&___rhs1)->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0026;
	}

IL_0026:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m3070475644 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_magnitude_m3070475644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_4 = sqrtf(((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3)))));
		V_0 = L_4;
		goto IL_0027;
	}

IL_0027:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector2_get_magnitude_m3070475644_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_get_magnitude_m3070475644(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m3613460260 (Vector2_t328513675 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0022;
	}

IL_0022:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m3613460260_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t328513675 * _thisAdjusted = reinterpret_cast<Vector2_t328513675 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m3613460260(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Distance_m1293157661 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Distance_m1293157661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector2_t328513675  L_0 = ___a0;
		Vector2_t328513675  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_2 = Vector2_op_Subtraction_m3140329220(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_magnitude_m3070475644((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0016;
	}

IL_0016:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t328513675  Vector2_op_Addition_m2936674869 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t328513675  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t328513675  Vector2_op_Subtraction_m3140329220 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, Vector2_t328513675  ___b1, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t328513675  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C"  Vector2_t328513675  Vector2_op_UnaryNegation_m2124526043 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___a0)->get_y_1();
		Vector2_t328513675  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m22874897((&L_2), ((-L_0)), ((-L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		Vector2_t328513675  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t328513675  Vector2_op_Multiply_m3993698970 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t328513675  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t328513675  Vector2_op_Division_m4163545787 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t328513675  L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m3259744715 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___lhs0, Vector2_t328513675  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Equality_m3259744715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t328513675  L_0 = ___lhs0;
		Vector2_t328513675  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_2 = Vector2_op_Subtraction_m3140329220(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_sqrMagnitude_m3613460260((&V_0), /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m663134504 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___lhs0, Vector2_t328513675  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Inequality_m663134504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector2_t328513675  L_0 = ___lhs0;
		Vector2_t328513675  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		bool L_2 = Vector2_op_Equality_m3259744715(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t328513675  Vector2_op_Implicit_m2745927330 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___v0, const RuntimeMethod* method)
{
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t328513675  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m22874897((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t328513675  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t1986933152  Vector2_op_Implicit_m659271803 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___v0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_0();
		float L_1 = (&___v0)->get_y_1();
		Vector3_t1986933152  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3282197930((&L_2), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001f;
	}

IL_001f:
	{
		Vector3_t1986933152  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t328513675  Vector2_get_zero_m187309959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_zero_m187309959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_0 = ((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->get_zeroVector_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t328513675  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t328513675  Vector2_get_one_m1113418147 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_one_m1113418147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_0 = ((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->get_oneVector_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t328513675  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t328513675  Vector2_get_up_m1378828326 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_up_m1378828326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_0 = ((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->get_upVector_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t328513675  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t328513675  Vector2_get_right_m1410176964 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_right_m1410176964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t328513675  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t328513675_il2cpp_TypeInfo_var);
		Vector2_t328513675  L_0 = ((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->get_rightVector_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t328513675  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector2::.cctor()
extern "C"  void Vector2__cctor_m1820088036 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2__cctor_m1820088036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t328513675  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m22874897((&L_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_zeroVector_2(L_0);
		Vector2_t328513675  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m22874897((&L_1), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_oneVector_3(L_1);
		Vector2_t328513675  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m22874897((&L_2), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_upVector_4(L_2);
		Vector2_t328513675  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m22874897((&L_3), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_downVector_5(L_3);
		Vector2_t328513675  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m22874897((&L_4), (-1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_leftVector_6(L_4);
		Vector2_t328513675  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m22874897((&L_5), (1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_rightVector_7(L_5);
		Vector2_t328513675  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m22874897((&L_6), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_positiveInfinityVector_8(L_6);
		Vector2_t328513675  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m22874897((&L_7), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t328513675_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t328513675_il2cpp_TypeInfo_var))->set_negativeInfinityVector_9(L_7);
		return;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3282197930 (Vector3_t1986933152 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m3282197930_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3__ctor_m3282197930(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m4040229299 (Vector3_t1986933152 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		__this->set_z_3((0.0f));
		return;
	}
}
extern "C"  void Vector3__ctor_m4040229299_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3__ctor_m4040229299(_thisAdjusted, ___x0, ___y1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_Slerp_m1979061459 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Slerp_m1979061459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_INTERNAL_CALL_Slerp_m4141590721(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Vector3_t1986933152  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)
extern "C"  void Vector3_INTERNAL_CALL_Slerp_m4141590721 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152 * ___a0, Vector3_t1986933152 * ___b1, float ___t2, Vector3_t1986933152 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Vector3_INTERNAL_CALL_Slerp_m4141590721_ftn) (Vector3_t1986933152 *, Vector3_t1986933152 *, float, Vector3_t1986933152 *);
	static Vector3_INTERNAL_CALL_Slerp_m4141590721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Vector3_INTERNAL_CALL_Slerp_m4141590721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_Lerp_m3944708698 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Lerp_m3944708698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2193645629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_z_3();
		float L_11 = (&___b1)->get_z_3();
		float L_12 = (&___a0)->get_z_3();
		float L_13 = ___t2;
		Vector3_t1986933152  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3282197930((&L_14), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_005f;
	}

IL_005f:
	{
		Vector3_t1986933152  L_15 = V_0;
		return L_15;
	}
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern "C"  float Vector3_get_Item_m3834540881 (Vector3_t1986933152 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_Item_m3834540881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_0024;
			}
			case 2:
			{
				goto IL_0030;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_0018:
	{
		float L_1 = __this->get_x_1();
		V_0 = L_1;
		goto IL_0047;
	}

IL_0024:
	{
		float L_2 = __this->get_y_2();
		V_0 = L_2;
		goto IL_0047;
	}

IL_0030:
	{
		float L_3 = __this->get_z_3();
		V_0 = L_3;
		goto IL_0047;
	}

IL_003c:
	{
		IndexOutOfRangeException_t2909334802 * L_4 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_4, _stringLiteral3205361995, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0047:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector3_get_Item_m3834540881_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_get_Item_m3834540881(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern "C"  void Vector3_set_Item_m4263389426 (Vector3_t1986933152 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_set_Item_m4263389426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_0024;
			}
			case 2:
			{
				goto IL_0030;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_0018:
	{
		float L_1 = ___value1;
		__this->set_x_1(L_1);
		goto IL_0047;
	}

IL_0024:
	{
		float L_2 = ___value1;
		__this->set_y_2(L_2);
		goto IL_0047;
	}

IL_0030:
	{
		float L_3 = ___value1;
		__this->set_z_3(L_3);
		goto IL_0047;
	}

IL_003c:
	{
		IndexOutOfRangeException_t2909334802 * L_4 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_4, _stringLiteral3205361995, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Vector3_set_Item_m4263389426_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3_set_Item_m4263389426(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector3::Set(System.Single,System.Single,System.Single)
extern "C"  void Vector3_Set_m1703619586 (Vector3_t1986933152 * __this, float ___newX0, float ___newY1, float ___newZ2, const RuntimeMethod* method)
{
	{
		float L_0 = ___newX0;
		__this->set_x_1(L_0);
		float L_1 = ___newY1;
		__this->set_y_2(L_1);
		float L_2 = ___newZ2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3_Set_m1703619586_AdjustorThunk (RuntimeObject * __this, float ___newX0, float ___newY1, float ___newZ2, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3_Set_m1703619586(_thisAdjusted, ___newX0, ___newY1, ___newZ2, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Scale_m1154154076 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
extern "C"  void Vector3_Scale_m463428114 (Vector3_t1986933152 * __this, Vector3_t1986933152  ___scale0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = (&___scale0)->get_x_1();
		__this->set_x_1(((float)((float)L_0*(float)L_1)));
		float L_2 = __this->get_y_2();
		float L_3 = (&___scale0)->get_y_2();
		__this->set_y_2(((float)((float)L_2*(float)L_3)));
		float L_4 = __this->get_z_3();
		float L_5 = (&___scale0)->get_z_3();
		__this->set_z_3(((float)((float)L_4*(float)L_5)));
		return;
	}
}
extern "C"  void Vector3_Scale_m463428114_AdjustorThunk (RuntimeObject * __this, Vector3_t1986933152  ___scale0, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3_Scale_m463428114(_thisAdjusted, ___scale0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Cross_m2192187373 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_y_2();
		float L_1 = (&___rhs1)->get_z_3();
		float L_2 = (&___lhs0)->get_z_3();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_x_1();
		float L_6 = (&___lhs0)->get_x_1();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = (&___lhs0)->get_x_1();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_x_1();
		Vector3_t1986933152  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m3282197930((&L_12), ((float)((float)((float)((float)L_0*(float)L_1))-(float)((float)((float)L_2*(float)L_3)))), ((float)((float)((float)((float)L_4*(float)L_5))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)L_8*(float)L_9))-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0069;
	}

IL_0069:
	{
		Vector3_t1986933152  L_13 = V_0;
		return L_13;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m4139137898 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m2097607317(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m2097607317(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m2097607317(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
		goto IL_0040;
	}

IL_0040:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
extern "C"  int32_t Vector3_GetHashCode_m4139137898_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_GetHashCode_m4139137898(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m3593875679 (Vector3_t1986933152 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m3593875679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B6_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector3_t1986933152_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0063;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector3_t1986933152 *)((Vector3_t1986933152 *)UnBox(L_1, Vector3_t1986933152_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_1)->get_x_1();
		bool L_4 = Single_Equals_m2960811275(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_1)->get_y_2();
		bool L_7 = Single_Equals_m2960811275(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005c;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_1)->get_z_3();
		bool L_10 = Single_Equals_m2960811275(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_005d;
	}

IL_005c:
	{
		G_B6_0 = 0;
	}

IL_005d:
	{
		V_0 = (bool)G_B6_0;
		goto IL_0063;
	}

IL_0063:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
extern "C"  bool Vector3_Equals_m3593875679_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_Equals_m3593875679(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Normalize_m2417917258 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Normalize_m2417917258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t1986933152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t1986933152  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		float L_1 = Vector3_Magnitude_m1454878665(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_0020;
		}
	}
	{
		Vector3_t1986933152  L_3 = ___value0;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_5 = Vector3_op_Division_m2151251249(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002b;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_6 = Vector3_get_zero_m2755426107(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002b;
	}

IL_002b:
	{
		Vector3_t1986933152  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3915644803 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Normalize_m3915644803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		float L_0 = Vector3_Magnitude_m1454878665(NULL /*static, unused*/, (*(Vector3_t1986933152 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_3 = Vector3_op_Division_m2151251249(NULL /*static, unused*/, (*(Vector3_t1986933152 *)__this), L_2, /*hidden argument*/NULL);
		*(Vector3_t1986933152 *)__this = L_3;
		goto IL_003a;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_4 = Vector3_get_zero_m2755426107(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t1986933152 *)__this = L_4;
	}

IL_003a:
	{
		return;
	}
}
extern "C"  void Vector3_Normalize_m3915644803_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	Vector3_Normalize_m3915644803(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t1986933152  Vector3_get_normalized_m1493202271 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_normalized_m1493202271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = Vector3_Normalize_m2417917258(NULL /*static, unused*/, (*(Vector3_t1986933152 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t1986933152  Vector3_get_normalized_m1493202271_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_get_normalized_m1493202271(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2502327341 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0036;
	}

IL_0036:
	{
		float L_6 = V_0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Angle_m1273527711 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___from0, Vector3_t1986933152  ___to1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Angle_m1273527711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t1986933152  L_0 = Vector3_get_normalized_m1493202271((&___from0), /*hidden argument*/NULL);
		Vector3_t1986933152  L_1 = Vector3_get_normalized_m1493202271((&___to1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m2502327341(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m1298020578(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		V_0 = ((float)((float)L_4*(float)(57.29578f)));
		goto IL_0034;
	}

IL_0034:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m659926138 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Distance_m659926138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3__ctor_m3282197930((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_x_1();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = (&V_0)->get_y_2();
		float L_10 = (&V_0)->get_z_3();
		float L_11 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		V_1 = L_12;
		goto IL_006f;
	}

IL_006f:
	{
		float L_13 = V_1;
		return L_13;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::ClampMagnitude(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_ClampMagnitude_m42878694 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___vector0, float ___maxLength1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ClampMagnitude_m42878694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Vector3_get_sqrMagnitude_m124294925((&___vector0), /*hidden argument*/NULL);
		float L_1 = ___maxLength1;
		float L_2 = ___maxLength1;
		if ((!(((float)L_0) > ((float)((float)((float)L_1*(float)L_2))))))
		{
			goto IL_0023;
		}
	}
	{
		Vector3_t1986933152  L_3 = Vector3_get_normalized_m1493202271((&___vector0), /*hidden argument*/NULL);
		float L_4 = ___maxLength1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_5 = Vector3_op_Multiply_m952283854(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_002a;
	}

IL_0023:
	{
		Vector3_t1986933152  L_6 = ___vector0;
		V_0 = L_6;
		goto IL_002a;
	}

IL_002a:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C"  float Vector3_Magnitude_m1454878665 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___vector0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Magnitude_m1454878665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = (&___vector0)->get_x_1();
		float L_1 = (&___vector0)->get_x_1();
		float L_2 = (&___vector0)->get_y_2();
		float L_3 = (&___vector0)->get_y_2();
		float L_4 = (&___vector0)->get_z_3();
		float L_5 = (&___vector0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		V_0 = L_6;
		goto IL_003b;
	}

IL_003b:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m1161157060 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_magnitude_m1161157060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		V_0 = L_6;
		goto IL_0035;
	}

IL_0035:
	{
		float L_7 = V_0;
		return L_7;
	}
}
extern "C"  float Vector3_get_magnitude_m1161157060_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_get_magnitude_m1161157060(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m1775679755 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___vector0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___vector0)->get_x_1();
		float L_1 = (&___vector0)->get_x_1();
		float L_2 = (&___vector0)->get_y_2();
		float L_3 = (&___vector0)->get_y_2();
		float L_4 = (&___vector0)->get_z_3();
		float L_5 = (&___vector0)->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0036;
	}

IL_0036:
	{
		float L_6 = V_0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m124294925 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0030;
	}

IL_0030:
	{
		float L_6 = V_0;
		return L_6;
	}
}
extern "C"  float Vector3_get_sqrMagnitude_m124294925_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_get_sqrMagnitude_m124294925(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Min_m3122780938 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Min_m3122780938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m2485682987(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Min_m2485682987(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Min_m2485682987(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t1986933152  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3282197930((&L_9), L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0045;
	}

IL_0045:
	{
		Vector3_t1986933152  L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_Max_m664546017 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Max_m664546017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m1363999114(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Max_m1363999114(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Max_m1363999114(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t1986933152  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3282197930((&L_9), L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0045;
	}

IL_0045:
	{
		Vector3_t1986933152  L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t1986933152  Vector3_get_zero_m2755426107 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_zero_m2755426107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_zeroVector_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t1986933152  Vector3_get_one_m3852861474 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_one_m3852861474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_oneVector_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t1986933152  Vector3_get_forward_m953890947 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_forward_m953890947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_forwardVector_10();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t1986933152  Vector3_get_back_m4261998315 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_back_m4261998315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_backVector_11();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t1986933152  Vector3_get_up_m467978780 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_up_m467978780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_upVector_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t1986933152  Vector3_get_down_m1884173742 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_down_m1884173742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_downVector_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C"  Vector3_t1986933152  Vector3_get_left_m3516331364 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_left_m3516331364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_leftVector_8();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t1986933152  Vector3_get_right_m878387312 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_right_m878387312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_0 = ((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->get_rightVector_9();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t1986933152  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Addition_m1125374618 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Subtraction_m1182848491 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, Vector3_t1986933152  ___b1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_UnaryNegation_m1212356519 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		float L_2 = (&___a0)->get_z_3();
		Vector3_t1986933152  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3282197930((&L_3), ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0024;
	}

IL_0024:
	{
		Vector3_t1986933152  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_op_Multiply_m952283854 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t1986933152  Vector3_op_Multiply_m763753486 (RuntimeObject * __this /* static, unused */, float ___d0, Vector3_t1986933152  ___a1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		float L_4 = (&___a1)->get_z_3();
		float L_5 = ___d0;
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t1986933152  Vector3_op_Division_m2151251249 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t1986933152  L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m511918437 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_op_Equality_m511918437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector3_t1986933152  L_0 = ___lhs0;
		Vector3_t1986933152  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		Vector3_t1986933152  L_2 = Vector3_op_Subtraction_m1182848491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m1775679755(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m2317070390 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___lhs0, Vector3_t1986933152  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_op_Inequality_m2317070390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector3_t1986933152  L_0 = ___lhs0;
		Vector3_t1986933152  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t1986933152_il2cpp_TypeInfo_var);
		bool L_2 = Vector3_op_Equality_m511918437(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m3238424746 (Vector3_t1986933152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m3238424746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2737604620* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral1436783535, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Vector3_ToString_m3238424746_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t1986933152 * _thisAdjusted = reinterpret_cast<Vector3_t1986933152 *>(__this + 1);
	return Vector3_ToString_m3238424746(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector3::.cctor()
extern "C"  void Vector3__cctor_m3955612181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3__cctor_m3955612181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t1986933152  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3282197930((&L_0), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_zeroVector_4(L_0);
		Vector3_t1986933152  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3282197930((&L_1), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_oneVector_5(L_1);
		Vector3_t1986933152  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3282197930((&L_2), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_upVector_6(L_2);
		Vector3_t1986933152  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3282197930((&L_3), (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_downVector_7(L_3);
		Vector3_t1986933152  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3282197930((&L_4), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_leftVector_8(L_4);
		Vector3_t1986933152  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m3282197930((&L_5), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_rightVector_9(L_5);
		Vector3_t1986933152  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3282197930((&L_6), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_forwardVector_10(L_6);
		Vector3_t1986933152  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3282197930((&L_7), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_backVector_11(L_7);
		Vector3_t1986933152  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m3282197930((&L_8), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_positiveInfinityVector_12(L_8);
		Vector3_t1986933152  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3282197930((&L_9), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector3_t1986933152_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t1986933152_il2cpp_TypeInfo_var))->set_negativeInfinityVector_13(L_9);
		return;
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1296751250 (Vector4_t2436299922 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m1296751250_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	Vector4__ctor_m1296751250(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m3649974304 (Vector4_t2436299922 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_Item_m3649974304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0034;
			}
			case 3:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_001c:
	{
		float L_1 = __this->get_x_1();
		V_0 = L_1;
		goto IL_0057;
	}

IL_0028:
	{
		float L_2 = __this->get_y_2();
		V_0 = L_2;
		goto IL_0057;
	}

IL_0034:
	{
		float L_3 = __this->get_z_3();
		V_0 = L_3;
		goto IL_0057;
	}

IL_0040:
	{
		float L_4 = __this->get_w_4();
		V_0 = L_4;
		goto IL_0057;
	}

IL_004c:
	{
		IndexOutOfRangeException_t2909334802 * L_5 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_5, _stringLiteral237839674, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0057:
	{
		float L_6 = V_0;
		return L_6;
	}
}
extern "C"  float Vector4_get_Item_m3649974304_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_get_Item_m3649974304(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern "C"  void Vector4_set_Item_m3281778497 (Vector4_t2436299922 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_set_Item_m3281778497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0034;
			}
			case 3:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_001c:
	{
		float L_1 = ___value1;
		__this->set_x_1(L_1);
		goto IL_0057;
	}

IL_0028:
	{
		float L_2 = ___value1;
		__this->set_y_2(L_2);
		goto IL_0057;
	}

IL_0034:
	{
		float L_3 = ___value1;
		__this->set_z_3(L_3);
		goto IL_0057;
	}

IL_0040:
	{
		float L_4 = ___value1;
		__this->set_w_4(L_4);
		goto IL_0057;
	}

IL_004c:
	{
		IndexOutOfRangeException_t2909334802 * L_5 = (IndexOutOfRangeException_t2909334802 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t2909334802_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1303321160(L_5, _stringLiteral237839674, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0057:
	{
		return;
	}
}
extern "C"  void Vector4_set_Item_m3281778497_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	Vector4_set_Item_m3281778497(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3459216764 (Vector4_t2436299922 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m2097607317(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m2097607317(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m2097607317(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m2097607317(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Vector4_GetHashCode_m3459216764_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_GetHashCode_m3459216764(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m754502683 (Vector4_t2436299922 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m754502683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector4_t2436299922_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector4_t2436299922 *)((Vector4_t2436299922 *)UnBox(L_1, Vector4_t2436299922_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_1)->get_x_1();
		bool L_4 = Single_Equals_m2960811275(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_1)->get_y_2();
		bool L_7 = Single_Equals_m2960811275(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_1)->get_z_3();
		bool L_10 = Single_Equals_m2960811275(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_1)->get_w_4();
		bool L_13 = Single_Equals_m2960811275(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Vector4_Equals_m754502683_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_Equals_m754502683(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::Normalize(UnityEngine.Vector4)
extern "C"  Vector4_t2436299922  Vector4_Normalize_m4277572146 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Normalize_m4277572146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector4_t2436299922  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector4_t2436299922  L_0 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		float L_1 = Vector4_Magnitude_m3047559685(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_0020;
		}
	}
	{
		Vector4_t2436299922  L_3 = ___a0;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_5 = Vector4_op_Division_m837022830(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002b;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_6 = Vector4_get_zero_m1931160304(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002b;
	}

IL_002b:
	{
		Vector4_t2436299922  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_normalized()
extern "C"  Vector4_t2436299922  Vector4_get_normalized_m1792997498 (Vector4_t2436299922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_normalized_m1792997498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_0 = Vector4_Normalize_m4277572146(NULL /*static, unused*/, (*(Vector4_t2436299922 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector4_t2436299922  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector4_t2436299922  Vector4_get_normalized_m1792997498_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_get_normalized_m1792997498(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2342208158 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, Vector4_t2436299922  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.Vector4::Magnitude(UnityEngine.Vector4)
extern "C"  float Vector4_Magnitude_m3047559685 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Magnitude_m3047559685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector4_t2436299922  L_0 = ___a0;
		Vector4_t2436299922  L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		float L_2 = Vector4_Dot_m2342208158(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2228218122_il2cpp_TypeInfo_var);
		float L_3 = sqrtf(L_2);
		V_0 = L_3;
		goto IL_0013;
	}

IL_0013:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m2365501330 (Vector4_t2436299922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_sqrMagnitude_m2365501330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		float L_0 = Vector4_Dot_m2342208158(NULL /*static, unused*/, (*(Vector4_t2436299922 *)__this), (*(Vector4_t2436299922 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0018;
	}

IL_0018:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Vector4_get_sqrMagnitude_m2365501330_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_get_sqrMagnitude_m2365501330(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t2436299922  Vector4_get_zero_m1931160304 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_zero_m1931160304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_0 = ((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->get_zeroVector_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector4_t2436299922  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_one()
extern "C"  Vector4_t2436299922  Vector4_get_one_m336856355 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_one_m336856355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_0 = ((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->get_oneVector_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector4_t2436299922  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2436299922  Vector4_op_Subtraction_m941026247 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, Vector4_t2436299922  ___b1, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		Vector4_t2436299922  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1296751250((&L_8), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0048;
	}

IL_0048:
	{
		Vector4_t2436299922  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2436299922  Vector4_op_Multiply_m644815020 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t2436299922  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1296751250((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Vector4_t2436299922  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(System.Single,UnityEngine.Vector4)
extern "C"  Vector4_t2436299922  Vector4_op_Multiply_m1725896655 (RuntimeObject * __this /* static, unused */, float ___d0, Vector4_t2436299922  ___a1, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		float L_4 = (&___a1)->get_z_3();
		float L_5 = ___d0;
		float L_6 = (&___a1)->get_w_4();
		float L_7 = ___d0;
		Vector4_t2436299922  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1296751250((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Vector4_t2436299922  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2436299922  Vector4_op_Division_m837022830 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t2436299922  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1296751250((&L_8), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Vector4_t2436299922  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m1687602531 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___lhs0, Vector4_t2436299922  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_op_Equality_m1687602531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector4_t2436299922  L_0 = ___lhs0;
		Vector4_t2436299922  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		Vector4_t2436299922  L_2 = Vector4_op_Subtraction_m941026247(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m4263254105(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Inequality_m594636851 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___lhs0, Vector4_t2436299922  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_op_Inequality_m594636851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector4_t2436299922  L_0 = ___lhs0;
		Vector4_t2436299922  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		bool L_2 = Vector4_op_Equality_m1687602531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t2436299922  Vector4_op_Implicit_m4147546254 (RuntimeObject * __this /* static, unused */, Vector3_t1986933152  ___v0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector4_t2436299922  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m1296751250((&L_3), L_0, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0026;
	}

IL_0026:
	{
		Vector4_t2436299922  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t1986933152  Vector4_op_Implicit_m2024390721 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___v0, const RuntimeMethod* method)
{
	Vector3_t1986933152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector3_t1986933152  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3282197930((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t1986933152  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector4_t2436299922  Vector4_op_Implicit_m1813995694 (RuntimeObject * __this /* static, unused */, Vector2_t328513675  ___v0, const RuntimeMethod* method)
{
	Vector4_t2436299922  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_0();
		float L_1 = (&___v0)->get_y_1();
		Vector4_t2436299922  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m1296751250((&L_2), L_0, L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0024;
	}

IL_0024:
	{
		Vector4_t2436299922  L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2547591814 (Vector4_t2436299922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m2547591814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2737604620* L_0 = ((ObjectU5BU5D_t2737604620*)SZArrayNew(ObjectU5BU5D_t2737604620_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2737604620* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2737604620* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2737604620* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t485236535_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m3950186024(NULL /*static, unused*/, _stringLiteral197058514, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Vector4_ToString_m2547591814_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2436299922 * _thisAdjusted = reinterpret_cast<Vector4_t2436299922 *>(__this + 1);
	return Vector4_ToString_m2547591814(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m4263254105 (RuntimeObject * __this /* static, unused */, Vector4_t2436299922  ___a0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_SqrMagnitude_m4263254105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector4_t2436299922  L_0 = ___a0;
		Vector4_t2436299922  L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2436299922_il2cpp_TypeInfo_var);
		float L_2 = Vector4_Dot_m2342208158(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Vector4::.cctor()
extern "C"  void Vector4__cctor_m3704097946 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4__cctor_m3704097946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t2436299922  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m1296751250((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->set_zeroVector_5(L_0);
		Vector4_t2436299922  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m1296751250((&L_1), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->set_oneVector_6(L_1);
		Vector4_t2436299922  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m1296751250((&L_2), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->set_positiveInfinityVector_7(L_2);
		Vector4_t2436299922  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m1296751250((&L_3), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector4_t2436299922_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2436299922_il2cpp_TypeInfo_var))->set_negativeInfinityVector_8(L_3);
		return;
	}
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m162600592 (WaitForEndOfFrame_t1056898812 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m874787853(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m2075195580 (WaitForFixedUpdate_t4012476583 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m874787853(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke(const WaitForSeconds_t4272504447& unmarshaled, WaitForSeconds_t4272504447_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke_back(const WaitForSeconds_t4272504447_marshaled_pinvoke& marshaled, WaitForSeconds_t4272504447& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4272504447_marshal_pinvoke_cleanup(WaitForSeconds_t4272504447_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4272504447_marshal_com(const WaitForSeconds_t4272504447& unmarshaled, WaitForSeconds_t4272504447_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t4272504447_marshal_com_back(const WaitForSeconds_t4272504447_marshaled_com& marshaled, WaitForSeconds_t4272504447& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4272504447_marshal_com_cleanup(WaitForSeconds_t4272504447_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m997861548 (WaitForSeconds_t4272504447 * __this, float ___seconds0, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m874787853(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
extern "C"  void WaitForSecondsRealtime__ctor_m2843391783 (WaitForSecondsRealtime_t3353236027 * __this, float ___time0, const RuntimeMethod* method)
{
	{
		CustomYieldInstruction__ctor_m900305041(__this, /*hidden argument*/NULL);
		float L_0 = Time_get_realtimeSinceStartup_m101132769(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = ___time0;
		__this->set_waitTime_0(((float)((float)L_0+(float)L_1)));
		return;
	}
}
// System.Boolean UnityEngine.WaitForSecondsRealtime::get_keepWaiting()
extern "C"  bool WaitForSecondsRealtime_get_keepWaiting_m2372019659 (WaitForSecondsRealtime_t3353236027 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		float L_0 = Time_get_realtimeSinceStartup_m101132769(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_waitTime_0();
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		goto IL_0014;
	}

IL_0014:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m747179028 (WritableAttribute_t482114951 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke(const YieldInstruction_t3270995273& unmarshaled, YieldInstruction_t3270995273_marshaled_pinvoke& marshaled)
{
}
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke_back(const YieldInstruction_t3270995273_marshaled_pinvoke& marshaled, YieldInstruction_t3270995273& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3270995273_marshal_pinvoke_cleanup(YieldInstruction_t3270995273_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3270995273_marshal_com(const YieldInstruction_t3270995273& unmarshaled, YieldInstruction_t3270995273_marshaled_com& marshaled)
{
}
extern "C" void YieldInstruction_t3270995273_marshal_com_back(const YieldInstruction_t3270995273_marshaled_com& marshaled, YieldInstruction_t3270995273& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3270995273_marshal_com_cleanup(YieldInstruction_t3270995273_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m874787853 (YieldInstruction_t3270995273 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2968844594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C"  void GenericStack__ctor_m3196150353 (GenericStack_t2697154139 * __this, const RuntimeMethod* method)
{
	{
		Stack__ctor_m3569009555(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern "C"  void MathfInternal__cctor_m515788773 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MathfInternal__cctor_m515788773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t3317739077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t3317739077_il2cpp_TypeInfo_var))->set_FloatMinNormal_0((1.17549435E-38f));
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t3317739077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t3317739077_il2cpp_TypeInfo_var))->set_FloatMinDenormal_1((1.401298E-45f));
		((MathfInternal_t3317739077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t3317739077_il2cpp_TypeInfo_var))->set_IsFlushToZeroEnabled_2((bool)1);
		return;
	}
}
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t3882343965 * NetFxCoreExtensions_CreateDelegate_m1322929972 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method)
{
	Delegate_t3882343965 * V_0 = NULL;
	{
		Type_t * L_0 = ___delegateType1;
		RuntimeObject * L_1 = ___target2;
		MethodInfo_t * L_2 = ___self0;
		Delegate_t3882343965 * L_3 = Delegate_CreateDelegate_m3618370599(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		Delegate_t3882343965 * L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m3023103954 (RuntimeObject * __this /* static, unused */, Delegate_t3882343965 * ___self0, const RuntimeMethod* method)
{
	MethodInfo_t * V_0 = NULL;
	{
		Delegate_t3882343965 * L_0 = ___self0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m158526026(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MethodInfo_t * L_2 = V_0;
		return L_2;
	}
}
// System.Delegate UnityEngineInternal.ScriptingUtils::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t3882343965 * ScriptingUtils_CreateDelegate_m3980956832 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, MethodInfo_t * ___methodInfo1, const RuntimeMethod* method)
{
	Delegate_t3882343965 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		MethodInfo_t * L_1 = ___methodInfo1;
		Delegate_t3882343965 * L_2 = Delegate_CreateDelegate_m2822449990(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		Delegate_t3882343965 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern "C"  void TypeInferenceRuleAttribute__ctor_m2054129398 (TypeInferenceRuleAttribute_t2902006326 * __this, int32_t ___rule0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeInferenceRuleAttribute__ctor_m2054129398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = Box(TypeInferenceRules_t1200137651_il2cpp_TypeInfo_var, (&___rule0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		*(&___rule0) = *(int32_t*)UnBox(L_0);
		TypeInferenceRuleAttribute__ctor_m2160180413(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m2160180413 (TypeInferenceRuleAttribute_t2902006326 * __this, String_t* ___rule0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2479467358(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule0;
		__this->set__rule_0(L_0);
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C"  String_t* TypeInferenceRuleAttribute_ToString_m991356425 (TypeInferenceRuleAttribute_t2902006326 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get__rule_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
