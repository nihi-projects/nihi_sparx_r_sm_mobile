﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Component
struct Component_t1632713610;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2248283753;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1867887001;
// UnityEngine.Object
struct Object_t692178351;
// System.ArgumentNullException
struct ArgumentNullException_t3857807348;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t3991970239;
// System.IntPtr[]
struct IntPtrU5BU5D_t1552124580;
// System.Collections.IDictionary
struct IDictionary_t896034555;
// System.Void
struct Void_t653366341;

extern RuntimeClass* Object_t692178351_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t3857807348_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral754755796;
extern Il2CppCodeGenString* _stringLiteral49693148;
extern const uint32_t ParticlePhysicsExtensions_GetCollisionEvents_m832864803_MetadataUsageId;



#ifndef U3CMODULEU3E_T1429447276_H
#define U3CMODULEU3E_T1429447276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447276 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447276_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef PARTICLESYSTEMEXTENSIONSIMPL_T3637200445_H
#define PARTICLESYSTEMEXTENSIONSIMPL_T3637200445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemExtensionsImpl
struct  ParticleSystemExtensionsImpl_t3637200445  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMEXTENSIONSIMPL_T3637200445_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3419619864* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3419619864* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3419619864** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3419619864* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T1867887001_H
#define LIST_1_T1867887001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct  List_1_t1867887001  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleCollisionEventU5BU5D_t3991970239* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1867887001, ____items_1)); }
	inline ParticleCollisionEventU5BU5D_t3991970239* get__items_1() const { return ____items_1; }
	inline ParticleCollisionEventU5BU5D_t3991970239** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleCollisionEventU5BU5D_t3991970239* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1867887001, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1867887001, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1867887001_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ParticleCollisionEventU5BU5D_t3991970239* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1867887001_StaticFields, ___EmptyArray_4)); }
	inline ParticleCollisionEventU5BU5D_t3991970239* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ParticleCollisionEventU5BU5D_t3991970239** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ParticleCollisionEventU5BU5D_t3991970239* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1867887001_H
#ifndef EXCEPTION_T4086964929_H
#define EXCEPTION_T4086964929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t4086964929  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1552124580* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t4086964929 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1552124580* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1552124580** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1552124580* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___inner_exception_1)); }
	inline Exception_t4086964929 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t4086964929 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t4086964929 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t4086964929, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T4086964929_H
#ifndef PARTICLEPHYSICSEXTENSIONS_T117113889_H
#define PARTICLEPHYSICSEXTENSIONS_T117113889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticlePhysicsExtensions
struct  ParticlePhysicsExtensions_t117113889  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPHYSICSEXTENSIONS_T117113889_H
#ifndef UINT32_T1721902794_H
#define UINT32_T1721902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1721902794 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1721902794, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1721902794_H
#ifndef INT32_T972567508_H
#define INT32_T972567508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t972567508 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t972567508, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T972567508_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef SYSTEMEXCEPTION_T2062748594_H
#define SYSTEMEXCEPTION_T2062748594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2062748594  : public Exception_t4086964929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2062748594_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef SINGLE_T485236535_H
#define SINGLE_T485236535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t485236535 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t485236535, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T485236535_H
#ifndef EMISSIONMODULE_T3558523942_H
#define EMISSIONMODULE_T3558523942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmissionModule
struct  EmissionModule_t3558523942 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t3558523942, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2248283753 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2248283753 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2248283753 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t3558523942_marshaled_pinvoke
{
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t3558523942_marshaled_com
{
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;
};
#endif // EMISSIONMODULE_T3558523942_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef MAINMODULE_T418589237_H
#define MAINMODULE_T418589237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t418589237 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t418589237, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2248283753 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2248283753 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2248283753 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t418589237_marshaled_pinvoke
{
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t418589237_marshaled_com
{
	ParticleSystem_t2248283753 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T418589237_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T1599299226_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T1599299226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t1599299226 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t1599299226, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T1599299226_H
#ifndef ARGUMENTEXCEPTION_T1812645948_H
#define ARGUMENTEXCEPTION_T1812645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1812645948  : public SystemException_t2062748594
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1812645948, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1812645948_H
#ifndef PARTICLECOLLISIONEVENT_T2748259994_H
#define PARTICLECOLLISIONEVENT_T2748259994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleCollisionEvent
struct  ParticleCollisionEvent_t2748259994 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Intersection
	Vector3_t1986933152  ___m_Intersection_0;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Normal
	Vector3_t1986933152  ___m_Normal_1;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Velocity
	Vector3_t1986933152  ___m_Velocity_2;
	// System.Int32 UnityEngine.ParticleCollisionEvent::m_ColliderInstanceID
	int32_t ___m_ColliderInstanceID_3;

public:
	inline static int32_t get_offset_of_m_Intersection_0() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Intersection_0)); }
	inline Vector3_t1986933152  get_m_Intersection_0() const { return ___m_Intersection_0; }
	inline Vector3_t1986933152 * get_address_of_m_Intersection_0() { return &___m_Intersection_0; }
	inline void set_m_Intersection_0(Vector3_t1986933152  value)
	{
		___m_Intersection_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Normal_1)); }
	inline Vector3_t1986933152  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t1986933152 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t1986933152  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_2() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_Velocity_2)); }
	inline Vector3_t1986933152  get_m_Velocity_2() const { return ___m_Velocity_2; }
	inline Vector3_t1986933152 * get_address_of_m_Velocity_2() { return &___m_Velocity_2; }
	inline void set_m_Velocity_2(Vector3_t1986933152  value)
	{
		___m_Velocity_2 = value;
	}

	inline static int32_t get_offset_of_m_ColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t2748259994, ___m_ColliderInstanceID_3)); }
	inline int32_t get_m_ColliderInstanceID_3() const { return ___m_ColliderInstanceID_3; }
	inline int32_t* get_address_of_m_ColliderInstanceID_3() { return &___m_ColliderInstanceID_3; }
	inline void set_m_ColliderInstanceID_3(int32_t value)
	{
		___m_ColliderInstanceID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISIONEVENT_T2748259994_H
#ifndef ARGUMENTNULLEXCEPTION_T3857807348_H
#define ARGUMENTNULLEXCEPTION_T3857807348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t3857807348  : public ArgumentException_t1812645948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T3857807348_H
#ifndef GAMEOBJECT_T2557347079_H
#define GAMEOBJECT_T2557347079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2557347079  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2557347079_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef PARTICLESYSTEM_T2248283753_H
#define PARTICLESYSTEM_T2248283753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t2248283753  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T2248283753_H



// UnityEngine.Component UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)
extern "C"  Component_t1632713610 * ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::get_colliderComponent()
extern "C"  Component_t1632713610 * ParticleCollisionEvent_get_colliderComponent_m289036907 (ParticleCollisionEvent_t2748259994 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1171065100 (RuntimeObject * __this /* static, unused */, Object_t692178351 * p0, Object_t692178351 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1941537752 (ArgumentNullException_t3857807348 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)
extern "C"  int32_t ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___ps0, GameObject_t2557347079 * ___go1, RuntimeObject * ___collisionEvents2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t3558523942  ParticleSystem_get_emission_m3809979999 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m641055197 (EmissionModule_t3558523942 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m342169714 (MainModule_t418589237 * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m2438798785 (EmissionModule_t3558523942 * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m2640268034 (ParticleSystem_t2248283753 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2562351319 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m3058321125 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2611434960 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m1062650618 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m1547507491 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)
extern "C"  float MainModule_GetDuration_m100416448 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
extern "C"  float MainModule_get_duration_m3815434254 (MainModule_t418589237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)
extern "C"  bool MainModule_GetLoop_m3780956676 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern "C"  bool MainModule_get_loop_m2838401359 (MainModule_t418589237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::get_colliderComponent()
extern "C"  Component_t1632713610 * ParticleCollisionEvent_get_colliderComponent_m289036907 (ParticleCollisionEvent_t2748259994 * __this, const RuntimeMethod* method)
{
	Component_t1632713610 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_ColliderInstanceID_3();
		Component_t1632713610 * L_1 = ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Component_t1632713610 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Component_t1632713610 * ParticleCollisionEvent_get_colliderComponent_m289036907_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ParticleCollisionEvent_t2748259994 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t2748259994 *>(__this + 1);
	return ParticleCollisionEvent_get_colliderComponent_m289036907(_thisAdjusted, method);
}
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)
extern "C"  Component_t1632713610 * ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method)
{
	typedef Component_t1632713610 * (*ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916_ftn) (int32_t);
	static ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleCollisionEvent_InstanceIDToColliderComponent_m3466426916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)");
	Component_t1632713610 * retVal = _il2cpp_icall_func(___instanceID0);
	return retVal;
}
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>)
extern "C"  int32_t ParticlePhysicsExtensions_GetCollisionEvents_m832864803 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___ps0, GameObject_t2557347079 * ___go1, List_1_t1867887001 * ___collisionEvents2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePhysicsExtensions_GetCollisionEvents_m832864803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t2557347079 * L_0 = ___go1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t692178351_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1171065100(NULL /*static, unused*/, L_0, (Object_t692178351 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_2 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_2, _stringLiteral754755796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		List_1_t1867887001 * L_3 = ___collisionEvents2;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t3857807348 * L_4 = (ArgumentNullException_t3857807348 *)il2cpp_codegen_object_new(ArgumentNullException_t3857807348_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1941537752(L_4, _stringLiteral49693148, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		ParticleSystem_t2248283753 * L_5 = ___ps0;
		GameObject_t2557347079 * L_6 = ___go1;
		List_1_t1867887001 * L_7 = ___collisionEvents2;
		int32_t L_8 = ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0037;
	}

IL_0037:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m530497922 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m530497922_ftn) (ParticleSystem_t2248283753 *);
	static ParticleSystem_get_isPlaying_m530497922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m530497922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.ParticleSystem::get_time()
extern "C"  float ParticleSystem_get_time_m1758408458 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystem_get_time_m1758408458_ftn) (ParticleSystem_t2248283753 *);
	static ParticleSystem_get_time_m1758408458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_time_m1758408458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_time()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::set_time(System.Single)
extern "C"  void ParticleSystem_set_time_m1846331373 (ParticleSystem_t2248283753 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_time_m1846331373_ftn) (ParticleSystem_t2248283753 *, float);
	static ParticleSystem_set_time_m1846331373_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_time_m1846331373_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.ParticleSystem::get_particleCount()
extern "C"  int32_t ParticleSystem_get_particleCount_m3406361851 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_get_particleCount_m3406361851_ftn) (ParticleSystem_t2248283753 *);
	static ParticleSystem_get_particleCount_m3406361851_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_particleCount_m3406361851_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_particleCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern "C"  void ParticleSystem_set_enableEmission_m4169105811 (ParticleSystem_t2248283753 * __this, bool ___value0, const RuntimeMethod* method)
{
	EmissionModule_t3558523942  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t3558523942  L_0 = ParticleSystem_get_emission_m3809979999(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___value0;
		EmissionModule_set_enabled_m641055197((&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
extern "C"  uint32_t ParticleSystem_get_randomSeed_m3681207322 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	typedef uint32_t (*ParticleSystem_get_randomSeed_m3681207322_ftn) (ParticleSystem_t2248283753 *);
	static ParticleSystem_get_randomSeed_m3681207322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_randomSeed_m3681207322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_randomSeed()");
	uint32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
extern "C"  void ParticleSystem_set_randomSeed_m551587840 (ParticleSystem_t2248283753 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_randomSeed_m551587840_ftn) (ParticleSystem_t2248283753 *, uint32_t);
	static ParticleSystem_set_randomSeed_m551587840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_randomSeed_m551587840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
extern "C"  void ParticleSystem_set_useAutoRandomSeed_m398395975 (ParticleSystem_t2248283753 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_useAutoRandomSeed_m398395975_ftn) (ParticleSystem_t2248283753 *, bool);
	static ParticleSystem_set_useAutoRandomSeed_m398395975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_useAutoRandomSeed_m398395975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t418589237  ParticleSystem_get_main_m3375455161 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	MainModule_t418589237  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t418589237  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m342169714((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t418589237  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t3558523942  ParticleSystem_get_emission_m3809979999 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	EmissionModule_t3558523942  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t3558523942  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m2438798785((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t3558523942  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m2640268034 (ParticleSystem_t2248283753 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Simulate_m2640268034_ftn) (ParticleSystem_t2248283753 *, float, bool, bool, bool);
	static ParticleSystem_Simulate_m2640268034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Simulate_m2640268034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___t0, ___withChildren1, ___restart2, ___fixedTimeStep3);
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m1055586212 (ParticleSystem_t2248283753 * __this, float ___t0, bool ___withChildren1, bool ___restart2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = ___restart2;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m2640268034(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2562351319 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Play_m2562351319_ftn) (ParticleSystem_t2248283753 *, bool);
	static ParticleSystem_Play_m2562351319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Play_m2562351319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Play(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m3924652879 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m2562351319(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m3058321125 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Pause_m3058321125_ftn) (ParticleSystem_t2248283753 *, bool);
	static ParticleSystem_Pause_m3058321125_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Pause_m3058321125_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Pause(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Pause()
extern "C"  void ParticleSystem_Pause_m1689856452 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Pause_m3058321125(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2611434960 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m2611434960_ftn) (ParticleSystem_t2248283753 *, bool, int32_t);
	static ParticleSystem_Stop_m2611434960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m2611434960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m1132427580 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2611434960(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m1062650618 (ParticleSystem_t2248283753 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_IsAlive_m1062650618_ftn) (ParticleSystem_t2248283753 *, bool);
	static ParticleSystem_IsAlive_m1062650618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_IsAlive_m1062650618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::IsAlive(System.Boolean)");
	bool retVal = _il2cpp_icall_func(__this, ___withChildren0);
	return retVal;
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern "C"  bool ParticleSystem_IsAlive_m3891886471 (ParticleSystem_t2248283753 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		bool L_1 = ParticleSystem_IsAlive_m1062650618(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t3558523942_marshal_pinvoke(const EmissionModule_t3558523942& unmarshaled, EmissionModule_t3558523942_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t3558523942_marshal_pinvoke_back(const EmissionModule_t3558523942_marshaled_pinvoke& marshaled, EmissionModule_t3558523942& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t3558523942_marshal_pinvoke_cleanup(EmissionModule_t3558523942_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t3558523942_marshal_com(const EmissionModule_t3558523942& unmarshaled, EmissionModule_t3558523942_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t3558523942_marshal_com_back(const EmissionModule_t3558523942_marshaled_com& marshaled, EmissionModule_t3558523942& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t3558523942_marshal_com_cleanup(EmissionModule_t3558523942_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m2438798785 (EmissionModule_t3558523942 * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2248283753 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m2438798785_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method)
{
	EmissionModule_t3558523942 * _thisAdjusted = reinterpret_cast<EmissionModule_t3558523942 *>(__this + 1);
	EmissionModule__ctor_m2438798785(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m641055197 (EmissionModule_t3558523942 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2248283753 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ___value0;
		EmissionModule_SetEnabled_m1547507491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_enabled_m641055197_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	EmissionModule_t3558523942 * _thisAdjusted = reinterpret_cast<EmissionModule_t3558523942 *>(__this + 1);
	EmissionModule_set_enabled_m641055197(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m1547507491 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_SetEnabled_m1547507491_ftn) (ParticleSystem_t2248283753 *, bool);
	static EmissionModule_SetEnabled_m1547507491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetEnabled_m1547507491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t418589237_marshal_pinvoke(const MainModule_t418589237& unmarshaled, MainModule_t418589237_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t418589237_marshal_pinvoke_back(const MainModule_t418589237_marshaled_pinvoke& marshaled, MainModule_t418589237& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t418589237_marshal_pinvoke_cleanup(MainModule_t418589237_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t418589237_marshal_com(const MainModule_t418589237& unmarshaled, MainModule_t418589237_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t418589237_marshal_com_back(const MainModule_t418589237_marshaled_com& marshaled, MainModule_t418589237& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t418589237_marshal_com_cleanup(MainModule_t418589237_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m342169714 (MainModule_t418589237 * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2248283753 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m342169714_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2248283753 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t418589237 * _thisAdjusted = reinterpret_cast<MainModule_t418589237 *>(__this + 1);
	MainModule__ctor_m342169714(_thisAdjusted, ___particleSystem0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
extern "C"  float MainModule_get_duration_m3815434254 (MainModule_t418589237 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t2248283753 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = MainModule_GetDuration_m100416448(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MainModule_get_duration_m3815434254_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t418589237 * _thisAdjusted = reinterpret_cast<MainModule_t418589237 *>(__this + 1);
	return MainModule_get_duration_m3815434254(_thisAdjusted, method);
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern "C"  bool MainModule_get_loop_m2838401359 (MainModule_t418589237 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t2248283753 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = MainModule_GetLoop_m3780956676(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool MainModule_get_loop_m2838401359_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t418589237 * _thisAdjusted = reinterpret_cast<MainModule_t418589237 *>(__this + 1);
	return MainModule_get_loop_m2838401359(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)
extern "C"  float MainModule_GetDuration_m100416448 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, const RuntimeMethod* method)
{
	typedef float (*MainModule_GetDuration_m100416448_ftn) (ParticleSystem_t2248283753 *);
	static MainModule_GetDuration_m100416448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetDuration_m100416448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)
extern "C"  bool MainModule_GetLoop_m3780956676 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___system0, const RuntimeMethod* method)
{
	typedef bool (*MainModule_GetLoop_m3780956676_ftn) (ParticleSystem_t2248283753 *);
	static MainModule_GetLoop_m3780956676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetLoop_m3780956676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)");
	bool retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)
extern "C"  int32_t ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873 (RuntimeObject * __this /* static, unused */, ParticleSystem_t2248283753 * ___ps0, GameObject_t2557347079 * ___go1, RuntimeObject * ___collisionEvents2, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873_ftn) (ParticleSystem_t2248283753 *, GameObject_t2557347079 *, RuntimeObject *);
	static ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemExtensionsImpl_GetCollisionEvents_m1330515873_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)");
	int32_t retVal = _il2cpp_icall_func(___ps0, ___go1, ___collisionEvents2);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
