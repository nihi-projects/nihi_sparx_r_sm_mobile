﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Text.UTF8Encoding
struct UTF8Encoding_t347516576;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1060102516;
// CapturedDataInput
struct CapturedDataInput_t2616152122;
// UnityEngine.WWWForm
struct WWWForm_t815382151;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t2742677291;
// Boomlagoon.JSON.JSONValue
struct JSONValue_t3864304964;
// Registration
struct Registration_t2162293902;
// System.IO.StreamWriter
struct StreamWriter_t2875046975;
// System.IO.StreamReader
struct StreamReader_t1796800705;
// UnityEngine.WWW
struct WWW_t3599262362;
// PlayerFreeze
struct PlayerFreeze_t446343823;
// DropRock
struct DropRock_t875601513;
// PushRockMiniGame
struct PushRockMiniGame_t1414473092;
// System.String[]
struct StringU5BU5D_t2511808107;
// System.Collections.Generic.List`1<System.String>
struct List_1_t4069179741;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t539441572;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// System.Void
struct Void_t653366341;
// System.Char[]
struct CharU5BU5D_t3419619864;
// Notifications
struct Notifications_t688528964;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// TokenHolderLegacy
struct TokenHolderLegacy_t1380075429;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t3304433276;
// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>
struct List_1_t1370252420;
// CharacterCreation
struct CharacterCreation_t2214710272;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// CharacterSelection
struct CharacterSelection_t3241235818;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2146020731;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434;
// UnityEngine.AudioSource
struct AudioSource_t4025721661;
// OutlineObject[]
struct OutlineObjectU5BU5D_t3517943080;
// UnityEngine.UI.Dropdown
struct Dropdown_t3945702362;
// UnityEngine.UI.InputField
struct InputField_t3123460221;
// UnityEngine.UI.Text
struct Text_t1790657652;
// System.IO.TextReader
struct TextReader_t218744201;
// System.Xml.XmlNodeList
struct XmlNodeList_t3849781475;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct List_1_t179729523;
// Goodbye
struct Goodbye_t2957868680;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// UnityEngine.GUISkin
struct GUISkin_t2122630221;
// UnityEngine.AudioClip
struct AudioClip_t1419814565;
// UnityEngine.Transform[]
struct TransformU5BU5D_t2383644165;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2248283753;
// UnityEngine.Texture
struct Texture_t2119925672;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3840885519;
// UnityEngine.Rect[]
struct RectU5BU5D_t141872167;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t1489827645;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t32224225;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSUBCONNECT3U3EC__ITERATORD_T1010320540_H
#define U3CSUBCONNECT3U3EC__ITERATORD_T1010320540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<subConnect3>c__IteratorD
struct  U3CsubConnect3U3Ec__IteratorD_t1010320540  : public RuntimeObject
{
public:
	// System.Int32 CapturedDataInput/<subConnect3>c__IteratorD::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.String CapturedDataInput/<subConnect3>c__IteratorD::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_1;
	// System.String CapturedDataInput/<subConnect3>c__IteratorD::sJsonString
	String_t* ___sJsonString_2;
	// System.Text.UTF8Encoding CapturedDataInput/<subConnect3>c__IteratorD::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<subConnect3>c__IteratorD::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_4;
	// CapturedDataInput CapturedDataInput/<subConnect3>c__IteratorD::$this
	CapturedDataInput_t2616152122 * ___U24this_5;
	// System.Object CapturedDataInput/<subConnect3>c__IteratorD::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean CapturedDataInput/<subConnect3>c__IteratorD::$disposing
	bool ___U24disposing_7;
	// System.Int32 CapturedDataInput/<subConnect3>c__IteratorD::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U3CpostScoreURLU3E__0_1)); }
	inline String_t* get_U3CpostScoreURLU3E__0_1() const { return ___U3CpostScoreURLU3E__0_1; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_1() { return &___U3CpostScoreURLU3E__0_1; }
	inline void set_U3CpostScoreURLU3E__0_1(String_t* value)
	{
		___U3CpostScoreURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of_sJsonString_2() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___sJsonString_2)); }
	inline String_t* get_sJsonString_2() const { return ___sJsonString_2; }
	inline String_t** get_address_of_sJsonString_2() { return &___sJsonString_2; }
	inline void set_sJsonString_2(String_t* value)
	{
		___sJsonString_2 = value;
		Il2CppCodeGenWriteBarrier((&___sJsonString_2), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U3CencodingU3E__0_3)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_3() const { return ___U3CencodingU3E__0_3; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_3() { return &___U3CencodingU3E__0_3; }
	inline void set_U3CencodingU3E__0_3(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U3CpostHeaderU3E__0_4)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_4() const { return ___U3CpostHeaderU3E__0_4; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_4() { return &___U3CpostHeaderU3E__0_4; }
	inline void set_U3CpostHeaderU3E__0_4(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U24this_5)); }
	inline CapturedDataInput_t2616152122 * get_U24this_5() const { return ___U24this_5; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CapturedDataInput_t2616152122 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsubConnect3U3Ec__IteratorD_t1010320540, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBCONNECT3U3EC__ITERATORD_T1010320540_H
#ifndef U3CLOADSAVEPOINTFROMSERVER_NEWJSONSCHEMAU3EC__ITERATORC_T648798626_H
#define U3CLOADSAVEPOINTFROMSERVER_NEWJSONSCHEMAU3EC__ITERATORC_T648798626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC
struct  U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<test>__0
	String_t* ___U3CtestU3E__0_0;
	// System.Boolean CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::bOffline
	bool ___bOffline_1;
	// UnityEngine.WWWForm CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<loadForm>__1
	WWWForm_t815382151 * ___U3CloadFormU3E__1_2;
	// System.String CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<m_sLoadURL>__1
	String_t* ___U3Cm_sLoadURLU3E__1_3;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<json>__0
	JSONObject_t2742677291 * ___U3CjsonU3E__0_4;
	// Boomlagoon.JSON.JSONValue CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<tempVal>__2
	JSONValue_t3864304964 * ___U3CtempValU3E__2_5;
	// System.String CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<tempString>__2
	String_t* ___U3CtempStringU3E__2_6;
	// System.Boolean CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<tempBool>__3
	bool ___U3CtempBoolU3E__3_7;
	// System.String CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::<tempNum>__4
	String_t* ___U3CtempNumU3E__4_8;
	// CapturedDataInput CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::$this
	CapturedDataInput_t2616152122 * ___U24this_9;
	// System.Object CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::$disposing
	bool ___U24disposing_11;
	// System.Int32 CapturedDataInput/<loadSavePointFromServer_NewJsonSchema>c__IteratorC::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CtestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CtestU3E__0_0)); }
	inline String_t* get_U3CtestU3E__0_0() const { return ___U3CtestU3E__0_0; }
	inline String_t** get_address_of_U3CtestU3E__0_0() { return &___U3CtestU3E__0_0; }
	inline void set_U3CtestU3E__0_0(String_t* value)
	{
		___U3CtestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_bOffline_1() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___bOffline_1)); }
	inline bool get_bOffline_1() const { return ___bOffline_1; }
	inline bool* get_address_of_bOffline_1() { return &___bOffline_1; }
	inline void set_bOffline_1(bool value)
	{
		___bOffline_1 = value;
	}

	inline static int32_t get_offset_of_U3CloadFormU3E__1_2() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CloadFormU3E__1_2)); }
	inline WWWForm_t815382151 * get_U3CloadFormU3E__1_2() const { return ___U3CloadFormU3E__1_2; }
	inline WWWForm_t815382151 ** get_address_of_U3CloadFormU3E__1_2() { return &___U3CloadFormU3E__1_2; }
	inline void set_U3CloadFormU3E__1_2(WWWForm_t815382151 * value)
	{
		___U3CloadFormU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadFormU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3Cm_sLoadURLU3E__1_3() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3Cm_sLoadURLU3E__1_3)); }
	inline String_t* get_U3Cm_sLoadURLU3E__1_3() const { return ___U3Cm_sLoadURLU3E__1_3; }
	inline String_t** get_address_of_U3Cm_sLoadURLU3E__1_3() { return &___U3Cm_sLoadURLU3E__1_3; }
	inline void set_U3Cm_sLoadURLU3E__1_3(String_t* value)
	{
		___U3Cm_sLoadURLU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_sLoadURLU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_4() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CjsonU3E__0_4)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__0_4() const { return ___U3CjsonU3E__0_4; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__0_4() { return &___U3CjsonU3E__0_4; }
	inline void set_U3CjsonU3E__0_4(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CtempValU3E__2_5() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CtempValU3E__2_5)); }
	inline JSONValue_t3864304964 * get_U3CtempValU3E__2_5() const { return ___U3CtempValU3E__2_5; }
	inline JSONValue_t3864304964 ** get_address_of_U3CtempValU3E__2_5() { return &___U3CtempValU3E__2_5; }
	inline void set_U3CtempValU3E__2_5(JSONValue_t3864304964 * value)
	{
		___U3CtempValU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempValU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CtempStringU3E__2_6() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CtempStringU3E__2_6)); }
	inline String_t* get_U3CtempStringU3E__2_6() const { return ___U3CtempStringU3E__2_6; }
	inline String_t** get_address_of_U3CtempStringU3E__2_6() { return &___U3CtempStringU3E__2_6; }
	inline void set_U3CtempStringU3E__2_6(String_t* value)
	{
		___U3CtempStringU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempStringU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CtempBoolU3E__3_7() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CtempBoolU3E__3_7)); }
	inline bool get_U3CtempBoolU3E__3_7() const { return ___U3CtempBoolU3E__3_7; }
	inline bool* get_address_of_U3CtempBoolU3E__3_7() { return &___U3CtempBoolU3E__3_7; }
	inline void set_U3CtempBoolU3E__3_7(bool value)
	{
		___U3CtempBoolU3E__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CtempNumU3E__4_8() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U3CtempNumU3E__4_8)); }
	inline String_t* get_U3CtempNumU3E__4_8() const { return ___U3CtempNumU3E__4_8; }
	inline String_t** get_address_of_U3CtempNumU3E__4_8() { return &___U3CtempNumU3E__4_8; }
	inline void set_U3CtempNumU3E__4_8(String_t* value)
	{
		___U3CtempNumU3E__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempNumU3E__4_8), value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U24this_9)); }
	inline CapturedDataInput_t2616152122 * get_U24this_9() const { return ___U24this_9; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(CapturedDataInput_t2616152122 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSAVEPOINTFROMSERVER_NEWJSONSCHEMAU3EC__ITERATORC_T648798626_H
#ifndef U3CVALIDATEANDSENDU3EC__ITERATOR0_T3923129369_H
#define U3CVALIDATEANDSENDU3EC__ITERATOR0_T3923129369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Registration/<ValidateandSend>c__Iterator0
struct  U3CValidateandSendU3Ec__Iterator0_t3923129369  : public RuntimeObject
{
public:
	// Registration Registration/<ValidateandSend>c__Iterator0::$this
	Registration_t2162293902 * ___U24this_0;
	// System.Object Registration/<ValidateandSend>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Registration/<ValidateandSend>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Registration/<ValidateandSend>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CValidateandSendU3Ec__Iterator0_t3923129369, ___U24this_0)); }
	inline Registration_t2162293902 * get_U24this_0() const { return ___U24this_0; }
	inline Registration_t2162293902 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Registration_t2162293902 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CValidateandSendU3Ec__Iterator0_t3923129369, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CValidateandSendU3Ec__Iterator0_t3923129369, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CValidateandSendU3Ec__Iterator0_t3923129369, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATEANDSENDU3EC__ITERATOR0_T3923129369_H
#ifndef U3CSAVEPOINTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORB_T2461306033_H
#define U3CSAVEPOINTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORB_T2461306033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB
struct  U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_0;
	// System.Int32 CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::_levelNum
	int32_t ____levelNum_1;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::_feedback
	String_t* ____feedback_2;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::_savePoint
	String_t* ____savePoint_3;
	// System.Boolean CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::bSaveCurrenLevelNotebook
	bool ___bSaveCurrenLevelNotebook_4;
	// System.Boolean CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::_bSendNotebookdata
	bool ____bSendNotebookdata_5;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_6;
	// System.Text.UTF8Encoding CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_8;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<test>__0
	String_t* ___U3CtestU3E__0_9;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<json>__0
	JSONObject_t2742677291 * ___U3CjsonU3E__0_10;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<_SaveLevel>__0
	String_t* ___U3C_SaveLevelU3E__0_11;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<SavePoint>__0
	String_t* ___U3CSavePointU3E__0_12;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<SaveLevel>__0
	String_t* ___U3CSaveLevelU3E__0_13;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<_savePointString>__0
	String_t* ___U3C_savePointStringU3E__0_14;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<CurrentUserResponse>__0
	String_t* ___U3CCurrentUserResponseU3E__0_15;
	// System.IO.StreamWriter CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<writer>__1
	StreamWriter_t2875046975 * ___U3CwriterU3E__1_16;
	// System.IO.StreamReader CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<reader>__1
	StreamReader_t1796800705 * ___U3CreaderU3E__1_17;
	// System.String CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::<content>__1
	String_t* ___U3CcontentU3E__1_18;
	// CapturedDataInput CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::$this
	CapturedDataInput_t2616152122 * ___U24this_19;
	// System.Object CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::$current
	RuntimeObject * ___U24current_20;
	// System.Boolean CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::$disposing
	bool ___U24disposing_21;
	// System.Int32 CapturedDataInput/<savePointToServer_NewJsonSchema>c__IteratorB::$PC
	int32_t ___U24PC_22;

public:
	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CpostScoreURLU3E__0_0)); }
	inline String_t* get_U3CpostScoreURLU3E__0_0() const { return ___U3CpostScoreURLU3E__0_0; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_0() { return &___U3CpostScoreURLU3E__0_0; }
	inline void set_U3CpostScoreURLU3E__0_0(String_t* value)
	{
		___U3CpostScoreURLU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_0), value);
	}

	inline static int32_t get_offset_of__levelNum_1() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ____levelNum_1)); }
	inline int32_t get__levelNum_1() const { return ____levelNum_1; }
	inline int32_t* get_address_of__levelNum_1() { return &____levelNum_1; }
	inline void set__levelNum_1(int32_t value)
	{
		____levelNum_1 = value;
	}

	inline static int32_t get_offset_of__feedback_2() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ____feedback_2)); }
	inline String_t* get__feedback_2() const { return ____feedback_2; }
	inline String_t** get_address_of__feedback_2() { return &____feedback_2; }
	inline void set__feedback_2(String_t* value)
	{
		____feedback_2 = value;
		Il2CppCodeGenWriteBarrier((&____feedback_2), value);
	}

	inline static int32_t get_offset_of__savePoint_3() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ____savePoint_3)); }
	inline String_t* get__savePoint_3() const { return ____savePoint_3; }
	inline String_t** get_address_of__savePoint_3() { return &____savePoint_3; }
	inline void set__savePoint_3(String_t* value)
	{
		____savePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&____savePoint_3), value);
	}

	inline static int32_t get_offset_of_bSaveCurrenLevelNotebook_4() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___bSaveCurrenLevelNotebook_4)); }
	inline bool get_bSaveCurrenLevelNotebook_4() const { return ___bSaveCurrenLevelNotebook_4; }
	inline bool* get_address_of_bSaveCurrenLevelNotebook_4() { return &___bSaveCurrenLevelNotebook_4; }
	inline void set_bSaveCurrenLevelNotebook_4(bool value)
	{
		___bSaveCurrenLevelNotebook_4 = value;
	}

	inline static int32_t get_offset_of__bSendNotebookdata_5() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ____bSendNotebookdata_5)); }
	inline bool get__bSendNotebookdata_5() const { return ____bSendNotebookdata_5; }
	inline bool* get_address_of__bSendNotebookdata_5() { return &____bSendNotebookdata_5; }
	inline void set__bSendNotebookdata_5(bool value)
	{
		____bSendNotebookdata_5 = value;
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_6() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CjsonStringU3E__0_6)); }
	inline String_t* get_U3CjsonStringU3E__0_6() const { return ___U3CjsonStringU3E__0_6; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_6() { return &___U3CjsonStringU3E__0_6; }
	inline void set_U3CjsonStringU3E__0_6(String_t* value)
	{
		___U3CjsonStringU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_7() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CencodingU3E__0_7)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_7() const { return ___U3CencodingU3E__0_7; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_7() { return &___U3CencodingU3E__0_7; }
	inline void set_U3CencodingU3E__0_7(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_8() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CpostHeaderU3E__0_8)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_8() const { return ___U3CpostHeaderU3E__0_8; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_8() { return &___U3CpostHeaderU3E__0_8; }
	inline void set_U3CpostHeaderU3E__0_8(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U3CtestU3E__0_9() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CtestU3E__0_9)); }
	inline String_t* get_U3CtestU3E__0_9() const { return ___U3CtestU3E__0_9; }
	inline String_t** get_address_of_U3CtestU3E__0_9() { return &___U3CtestU3E__0_9; }
	inline void set_U3CtestU3E__0_9(String_t* value)
	{
		___U3CtestU3E__0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__0_9), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_10() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CjsonU3E__0_10)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__0_10() const { return ___U3CjsonU3E__0_10; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__0_10() { return &___U3CjsonU3E__0_10; }
	inline void set_U3CjsonU3E__0_10(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_10), value);
	}

	inline static int32_t get_offset_of_U3C_SaveLevelU3E__0_11() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3C_SaveLevelU3E__0_11)); }
	inline String_t* get_U3C_SaveLevelU3E__0_11() const { return ___U3C_SaveLevelU3E__0_11; }
	inline String_t** get_address_of_U3C_SaveLevelU3E__0_11() { return &___U3C_SaveLevelU3E__0_11; }
	inline void set_U3C_SaveLevelU3E__0_11(String_t* value)
	{
		___U3C_SaveLevelU3E__0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_SaveLevelU3E__0_11), value);
	}

	inline static int32_t get_offset_of_U3CSavePointU3E__0_12() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CSavePointU3E__0_12)); }
	inline String_t* get_U3CSavePointU3E__0_12() const { return ___U3CSavePointU3E__0_12; }
	inline String_t** get_address_of_U3CSavePointU3E__0_12() { return &___U3CSavePointU3E__0_12; }
	inline void set_U3CSavePointU3E__0_12(String_t* value)
	{
		___U3CSavePointU3E__0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSavePointU3E__0_12), value);
	}

	inline static int32_t get_offset_of_U3CSaveLevelU3E__0_13() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CSaveLevelU3E__0_13)); }
	inline String_t* get_U3CSaveLevelU3E__0_13() const { return ___U3CSaveLevelU3E__0_13; }
	inline String_t** get_address_of_U3CSaveLevelU3E__0_13() { return &___U3CSaveLevelU3E__0_13; }
	inline void set_U3CSaveLevelU3E__0_13(String_t* value)
	{
		___U3CSaveLevelU3E__0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLevelU3E__0_13), value);
	}

	inline static int32_t get_offset_of_U3C_savePointStringU3E__0_14() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3C_savePointStringU3E__0_14)); }
	inline String_t* get_U3C_savePointStringU3E__0_14() const { return ___U3C_savePointStringU3E__0_14; }
	inline String_t** get_address_of_U3C_savePointStringU3E__0_14() { return &___U3C_savePointStringU3E__0_14; }
	inline void set_U3C_savePointStringU3E__0_14(String_t* value)
	{
		___U3C_savePointStringU3E__0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_savePointStringU3E__0_14), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUserResponseU3E__0_15() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CCurrentUserResponseU3E__0_15)); }
	inline String_t* get_U3CCurrentUserResponseU3E__0_15() const { return ___U3CCurrentUserResponseU3E__0_15; }
	inline String_t** get_address_of_U3CCurrentUserResponseU3E__0_15() { return &___U3CCurrentUserResponseU3E__0_15; }
	inline void set_U3CCurrentUserResponseU3E__0_15(String_t* value)
	{
		___U3CCurrentUserResponseU3E__0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentUserResponseU3E__0_15), value);
	}

	inline static int32_t get_offset_of_U3CwriterU3E__1_16() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CwriterU3E__1_16)); }
	inline StreamWriter_t2875046975 * get_U3CwriterU3E__1_16() const { return ___U3CwriterU3E__1_16; }
	inline StreamWriter_t2875046975 ** get_address_of_U3CwriterU3E__1_16() { return &___U3CwriterU3E__1_16; }
	inline void set_U3CwriterU3E__1_16(StreamWriter_t2875046975 * value)
	{
		___U3CwriterU3E__1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwriterU3E__1_16), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E__1_17() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CreaderU3E__1_17)); }
	inline StreamReader_t1796800705 * get_U3CreaderU3E__1_17() const { return ___U3CreaderU3E__1_17; }
	inline StreamReader_t1796800705 ** get_address_of_U3CreaderU3E__1_17() { return &___U3CreaderU3E__1_17; }
	inline void set_U3CreaderU3E__1_17(StreamReader_t1796800705 * value)
	{
		___U3CreaderU3E__1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E__1_17), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3E__1_18() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U3CcontentU3E__1_18)); }
	inline String_t* get_U3CcontentU3E__1_18() const { return ___U3CcontentU3E__1_18; }
	inline String_t** get_address_of_U3CcontentU3E__1_18() { return &___U3CcontentU3E__1_18; }
	inline void set_U3CcontentU3E__1_18(String_t* value)
	{
		___U3CcontentU3E__1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3E__1_18), value);
	}

	inline static int32_t get_offset_of_U24this_19() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U24this_19)); }
	inline CapturedDataInput_t2616152122 * get_U24this_19() const { return ___U24this_19; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_19() { return &___U24this_19; }
	inline void set_U24this_19(CapturedDataInput_t2616152122 * value)
	{
		___U24this_19 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_19), value);
	}

	inline static int32_t get_offset_of_U24current_20() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U24current_20)); }
	inline RuntimeObject * get_U24current_20() const { return ___U24current_20; }
	inline RuntimeObject ** get_address_of_U24current_20() { return &___U24current_20; }
	inline void set_U24current_20(RuntimeObject * value)
	{
		___U24current_20 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_20), value);
	}

	inline static int32_t get_offset_of_U24disposing_21() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U24disposing_21)); }
	inline bool get_U24disposing_21() const { return ___U24disposing_21; }
	inline bool* get_address_of_U24disposing_21() { return &___U24disposing_21; }
	inline void set_U24disposing_21(bool value)
	{
		___U24disposing_21 = value;
	}

	inline static int32_t get_offset_of_U24PC_22() { return static_cast<int32_t>(offsetof(U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033, ___U24PC_22)); }
	inline int32_t get_U24PC_22() const { return ___U24PC_22; }
	inline int32_t* get_address_of_U24PC_22() { return &___U24PC_22; }
	inline void set_U24PC_22(int32_t value)
	{
		___U24PC_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEPOINTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORB_T2461306033_H
#ifndef U3CSAVEPOINTEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR9_T737388940_H
#define U3CSAVEPOINTEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR9_T737388940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9
struct  U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940  : public RuntimeObject
{
public:
	// System.Int32 CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::_eventID
	int32_t ____eventID_0;
	// System.String CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_1;
	// System.Int32 CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::_levelNum
	int32_t ____levelNum_2;
	// System.String CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::savePoint
	String_t* ___savePoint_3;
	// System.String CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_4;
	// System.Text.UTF8Encoding CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_6;
	// CapturedDataInput CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::$this
	CapturedDataInput_t2616152122 * ___U24this_7;
	// System.Object CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::$disposing
	bool ___U24disposing_9;
	// System.Int32 CapturedDataInput/<SavePointEventToServer_NewJsonSchema>c__Iterator9::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of__eventID_0() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ____eventID_0)); }
	inline int32_t get__eventID_0() const { return ____eventID_0; }
	inline int32_t* get_address_of__eventID_0() { return &____eventID_0; }
	inline void set__eventID_0(int32_t value)
	{
		____eventID_0 = value;
	}

	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U3CpostScoreURLU3E__0_1)); }
	inline String_t* get_U3CpostScoreURLU3E__0_1() const { return ___U3CpostScoreURLU3E__0_1; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_1() { return &___U3CpostScoreURLU3E__0_1; }
	inline void set_U3CpostScoreURLU3E__0_1(String_t* value)
	{
		___U3CpostScoreURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of__levelNum_2() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ____levelNum_2)); }
	inline int32_t get__levelNum_2() const { return ____levelNum_2; }
	inline int32_t* get_address_of__levelNum_2() { return &____levelNum_2; }
	inline void set__levelNum_2(int32_t value)
	{
		____levelNum_2 = value;
	}

	inline static int32_t get_offset_of_savePoint_3() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___savePoint_3)); }
	inline String_t* get_savePoint_3() const { return ___savePoint_3; }
	inline String_t** get_address_of_savePoint_3() { return &___savePoint_3; }
	inline void set_savePoint_3(String_t* value)
	{
		___savePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___savePoint_3), value);
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U3CjsonStringU3E__0_4)); }
	inline String_t* get_U3CjsonStringU3E__0_4() const { return ___U3CjsonStringU3E__0_4; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_4() { return &___U3CjsonStringU3E__0_4; }
	inline void set_U3CjsonStringU3E__0_4(String_t* value)
	{
		___U3CjsonStringU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_5() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U3CencodingU3E__0_5)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_5() const { return ___U3CencodingU3E__0_5; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_5() { return &___U3CencodingU3E__0_5; }
	inline void set_U3CencodingU3E__0_5(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_6() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U3CpostHeaderU3E__0_6)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_6() const { return ___U3CpostHeaderU3E__0_6; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_6() { return &___U3CpostHeaderU3E__0_6; }
	inline void set_U3CpostHeaderU3E__0_6(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U24this_7)); }
	inline CapturedDataInput_t2616152122 * get_U24this_7() const { return ___U24this_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(CapturedDataInput_t2616152122 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEPOINTEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR9_T737388940_H
#ifndef U3CSAVELEVELSTARTENDEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR8_T2983818842_H
#define U3CSAVELEVELSTARTENDEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR8_T2983818842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8
struct  U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842  : public RuntimeObject
{
public:
	// System.Int32 CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::_eventID
	int32_t ____eventID_0;
	// System.String CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_1;
	// System.Int32 CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::_levelNum
	int32_t ____levelNum_2;
	// System.String CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_3;
	// System.Text.UTF8Encoding CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_5;
	// CapturedDataInput CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::$this
	CapturedDataInput_t2616152122 * ___U24this_6;
	// System.Object CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::$disposing
	bool ___U24disposing_8;
	// System.Int32 CapturedDataInput/<SaveLevelStartEndEventToServer_NewJsonSchema>c__Iterator8::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of__eventID_0() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ____eventID_0)); }
	inline int32_t get__eventID_0() const { return ____eventID_0; }
	inline int32_t* get_address_of__eventID_0() { return &____eventID_0; }
	inline void set__eventID_0(int32_t value)
	{
		____eventID_0 = value;
	}

	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U3CpostScoreURLU3E__0_1)); }
	inline String_t* get_U3CpostScoreURLU3E__0_1() const { return ___U3CpostScoreURLU3E__0_1; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_1() { return &___U3CpostScoreURLU3E__0_1; }
	inline void set_U3CpostScoreURLU3E__0_1(String_t* value)
	{
		___U3CpostScoreURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of__levelNum_2() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ____levelNum_2)); }
	inline int32_t get__levelNum_2() const { return ____levelNum_2; }
	inline int32_t* get_address_of__levelNum_2() { return &____levelNum_2; }
	inline void set__levelNum_2(int32_t value)
	{
		____levelNum_2 = value;
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U3CjsonStringU3E__0_3)); }
	inline String_t* get_U3CjsonStringU3E__0_3() const { return ___U3CjsonStringU3E__0_3; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_3() { return &___U3CjsonStringU3E__0_3; }
	inline void set_U3CjsonStringU3E__0_3(String_t* value)
	{
		___U3CjsonStringU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U3CencodingU3E__0_4)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_4() const { return ___U3CencodingU3E__0_4; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_4() { return &___U3CencodingU3E__0_4; }
	inline void set_U3CencodingU3E__0_4(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_5() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U3CpostHeaderU3E__0_5)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_5() const { return ___U3CpostHeaderU3E__0_5; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_5() { return &___U3CpostHeaderU3E__0_5; }
	inline void set_U3CpostHeaderU3E__0_5(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U24this_6)); }
	inline CapturedDataInput_t2616152122 * get_U24this_6() const { return ___U24this_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(CapturedDataInput_t2616152122 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVELEVELSTARTENDEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR8_T2983818842_H
#ifndef U3CREADUSERLEVELDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR7_T731772144_H
#define U3CREADUSERLEVELDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR7_T731772144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7
struct  U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::<test>__0
	String_t* ___U3CtestU3E__0_0;
	// System.Boolean CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::bOffline
	bool ___bOffline_1;
	// System.String CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::<characterURL>__1
	String_t* ___U3CcharacterURLU3E__1_2;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::<json>__0
	JSONObject_t2742677291 * ___U3CjsonU3E__0_3;
	// CapturedDataInput CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::$this
	CapturedDataInput_t2616152122 * ___U24this_4;
	// System.Object CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::$disposing
	bool ___U24disposing_6;
	// System.Int32 CapturedDataInput/<ReadUserLevelDataFromServer_NewJsonSchema>c__Iterator7::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CtestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U3CtestU3E__0_0)); }
	inline String_t* get_U3CtestU3E__0_0() const { return ___U3CtestU3E__0_0; }
	inline String_t** get_address_of_U3CtestU3E__0_0() { return &___U3CtestU3E__0_0; }
	inline void set_U3CtestU3E__0_0(String_t* value)
	{
		___U3CtestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_bOffline_1() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___bOffline_1)); }
	inline bool get_bOffline_1() const { return ___bOffline_1; }
	inline bool* get_address_of_bOffline_1() { return &___bOffline_1; }
	inline void set_bOffline_1(bool value)
	{
		___bOffline_1 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterURLU3E__1_2() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U3CcharacterURLU3E__1_2)); }
	inline String_t* get_U3CcharacterURLU3E__1_2() const { return ___U3CcharacterURLU3E__1_2; }
	inline String_t** get_address_of_U3CcharacterURLU3E__1_2() { return &___U3CcharacterURLU3E__1_2; }
	inline void set_U3CcharacterURLU3E__1_2(String_t* value)
	{
		___U3CcharacterURLU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterURLU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_3() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U3CjsonU3E__0_3)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__0_3() const { return ___U3CjsonU3E__0_3; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__0_3() { return &___U3CjsonU3E__0_3; }
	inline void set_U3CjsonU3E__0_3(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U24this_4)); }
	inline CapturedDataInput_t2616152122 * get_U24this_4() const { return ___U24this_4; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CapturedDataInput_t2616152122 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADUSERLEVELDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR7_T731772144_H
#ifndef U3CSAVEUSERLEVELTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR6_T2683436496_H
#define U3CSAVEUSERLEVELTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR6_T2683436496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6
struct  U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_0;
	// System.String CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_1;
	// System.Text.UTF8Encoding CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_3;
	// System.IO.StreamWriter CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<writer>__1
	StreamWriter_t2875046975 * ___U3CwriterU3E__1_4;
	// System.IO.StreamReader CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<reader>__1
	StreamReader_t1796800705 * ___U3CreaderU3E__1_5;
	// System.String CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::<content>__1
	String_t* ___U3CcontentU3E__1_6;
	// CapturedDataInput CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::$this
	CapturedDataInput_t2616152122 * ___U24this_7;
	// System.Object CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::$disposing
	bool ___U24disposing_9;
	// System.Int32 CapturedDataInput/<SaveUserLevelToServer_NewJsonSchema>c__Iterator6::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CpostScoreURLU3E__0_0)); }
	inline String_t* get_U3CpostScoreURLU3E__0_0() const { return ___U3CpostScoreURLU3E__0_0; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_0() { return &___U3CpostScoreURLU3E__0_0; }
	inline void set_U3CpostScoreURLU3E__0_0(String_t* value)
	{
		___U3CpostScoreURLU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CjsonStringU3E__0_1)); }
	inline String_t* get_U3CjsonStringU3E__0_1() const { return ___U3CjsonStringU3E__0_1; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_1() { return &___U3CjsonStringU3E__0_1; }
	inline void set_U3CjsonStringU3E__0_1(String_t* value)
	{
		___U3CjsonStringU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CencodingU3E__0_2)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_2() const { return ___U3CencodingU3E__0_2; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_2() { return &___U3CencodingU3E__0_2; }
	inline void set_U3CencodingU3E__0_2(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CpostHeaderU3E__0_3)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_3() const { return ___U3CpostHeaderU3E__0_3; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_3() { return &___U3CpostHeaderU3E__0_3; }
	inline void set_U3CpostHeaderU3E__0_3(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwriterU3E__1_4() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CwriterU3E__1_4)); }
	inline StreamWriter_t2875046975 * get_U3CwriterU3E__1_4() const { return ___U3CwriterU3E__1_4; }
	inline StreamWriter_t2875046975 ** get_address_of_U3CwriterU3E__1_4() { return &___U3CwriterU3E__1_4; }
	inline void set_U3CwriterU3E__1_4(StreamWriter_t2875046975 * value)
	{
		___U3CwriterU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwriterU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CreaderU3E__1_5)); }
	inline StreamReader_t1796800705 * get_U3CreaderU3E__1_5() const { return ___U3CreaderU3E__1_5; }
	inline StreamReader_t1796800705 ** get_address_of_U3CreaderU3E__1_5() { return &___U3CreaderU3E__1_5; }
	inline void set_U3CreaderU3E__1_5(StreamReader_t1796800705 * value)
	{
		___U3CreaderU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3E__1_6() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U3CcontentU3E__1_6)); }
	inline String_t* get_U3CcontentU3E__1_6() const { return ___U3CcontentU3E__1_6; }
	inline String_t** get_address_of_U3CcontentU3E__1_6() { return &___U3CcontentU3E__1_6; }
	inline void set_U3CcontentU3E__1_6(String_t* value)
	{
		___U3CcontentU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3E__1_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U24this_7)); }
	inline CapturedDataInput_t2616152122 * get_U24this_7() const { return ___U24this_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(CapturedDataInput_t2616152122 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEUSERLEVELTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR6_T2683436496_H
#ifndef U3CSAVEONSERVERORLOCALU3EC__ITERATORE_T2814865380_H
#define U3CSAVEONSERVERORLOCALU3EC__ITERATORE_T2814865380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE
struct  U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<saveForm>__0
	WWWForm_t815382151 * ___U3CsaveFormU3E__0_0;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_1;
	// System.Int32 CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::_levelNum
	int32_t ____levelNum_2;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::_feedback
	String_t* ____feedback_3;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::_savePoint
	String_t* ____savePoint_4;
	// System.Boolean CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::bNewPageNotebook
	bool ___bNewPageNotebook_5;
	// System.Text.UTF8Encoding CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_6;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<test>__1
	String_t* ___U3CtestU3E__1_7;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<json>__1
	JSONObject_t2742677291 * ___U3CjsonU3E__1_8;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<SavePoint>__1
	String_t* ___U3CSavePointU3E__1_9;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<SaveLevel>__1
	String_t* ___U3CSaveLevelU3E__1_10;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<_SaveLevel>__1
	String_t* ___U3C_SaveLevelU3E__1_11;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<_savePointString>__1
	String_t* ___U3C_savePointStringU3E__1_12;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<CurrentUserResponse>__1
	String_t* ___U3CCurrentUserResponseU3E__1_13;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<postScoreURL>__2
	String_t* ___U3CpostScoreURLU3E__2_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<postHeader>__2
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__2_15;
	// UnityEngine.WWW CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<www>__2
	WWW_t3599262362 * ___U3CwwwU3E__2_16;
	// System.IO.StreamWriter CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<writer>__3
	StreamWriter_t2875046975 * ___U3CwriterU3E__3_17;
	// System.IO.StreamReader CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<reader>__3
	StreamReader_t1796800705 * ___U3CreaderU3E__3_18;
	// System.String CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::<content>__3
	String_t* ___U3CcontentU3E__3_19;
	// CapturedDataInput CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::$this
	CapturedDataInput_t2616152122 * ___U24this_20;
	// System.Object CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::$current
	RuntimeObject * ___U24current_21;
	// System.Boolean CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::$disposing
	bool ___U24disposing_22;
	// System.Int32 CapturedDataInput/<SaveOnServerOrLocal>c__IteratorE::$PC
	int32_t ___U24PC_23;

public:
	inline static int32_t get_offset_of_U3CsaveFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CsaveFormU3E__0_0)); }
	inline WWWForm_t815382151 * get_U3CsaveFormU3E__0_0() const { return ___U3CsaveFormU3E__0_0; }
	inline WWWForm_t815382151 ** get_address_of_U3CsaveFormU3E__0_0() { return &___U3CsaveFormU3E__0_0; }
	inline void set_U3CsaveFormU3E__0_0(WWWForm_t815382151 * value)
	{
		___U3CsaveFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsaveFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CjsonStringU3E__0_1)); }
	inline String_t* get_U3CjsonStringU3E__0_1() const { return ___U3CjsonStringU3E__0_1; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_1() { return &___U3CjsonStringU3E__0_1; }
	inline void set_U3CjsonStringU3E__0_1(String_t* value)
	{
		___U3CjsonStringU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_1), value);
	}

	inline static int32_t get_offset_of__levelNum_2() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ____levelNum_2)); }
	inline int32_t get__levelNum_2() const { return ____levelNum_2; }
	inline int32_t* get_address_of__levelNum_2() { return &____levelNum_2; }
	inline void set__levelNum_2(int32_t value)
	{
		____levelNum_2 = value;
	}

	inline static int32_t get_offset_of__feedback_3() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ____feedback_3)); }
	inline String_t* get__feedback_3() const { return ____feedback_3; }
	inline String_t** get_address_of__feedback_3() { return &____feedback_3; }
	inline void set__feedback_3(String_t* value)
	{
		____feedback_3 = value;
		Il2CppCodeGenWriteBarrier((&____feedback_3), value);
	}

	inline static int32_t get_offset_of__savePoint_4() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ____savePoint_4)); }
	inline String_t* get__savePoint_4() const { return ____savePoint_4; }
	inline String_t** get_address_of__savePoint_4() { return &____savePoint_4; }
	inline void set__savePoint_4(String_t* value)
	{
		____savePoint_4 = value;
		Il2CppCodeGenWriteBarrier((&____savePoint_4), value);
	}

	inline static int32_t get_offset_of_bNewPageNotebook_5() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___bNewPageNotebook_5)); }
	inline bool get_bNewPageNotebook_5() const { return ___bNewPageNotebook_5; }
	inline bool* get_address_of_bNewPageNotebook_5() { return &___bNewPageNotebook_5; }
	inline void set_bNewPageNotebook_5(bool value)
	{
		___bNewPageNotebook_5 = value;
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_6() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CencodingU3E__0_6)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_6() const { return ___U3CencodingU3E__0_6; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_6() { return &___U3CencodingU3E__0_6; }
	inline void set_U3CencodingU3E__0_6(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CtestU3E__1_7() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CtestU3E__1_7)); }
	inline String_t* get_U3CtestU3E__1_7() const { return ___U3CtestU3E__1_7; }
	inline String_t** get_address_of_U3CtestU3E__1_7() { return &___U3CtestU3E__1_7; }
	inline void set_U3CtestU3E__1_7(String_t* value)
	{
		___U3CtestU3E__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__1_7), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_8() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CjsonU3E__1_8)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__1_8() const { return ___U3CjsonU3E__1_8; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__1_8() { return &___U3CjsonU3E__1_8; }
	inline void set_U3CjsonU3E__1_8(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__1_8), value);
	}

	inline static int32_t get_offset_of_U3CSavePointU3E__1_9() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CSavePointU3E__1_9)); }
	inline String_t* get_U3CSavePointU3E__1_9() const { return ___U3CSavePointU3E__1_9; }
	inline String_t** get_address_of_U3CSavePointU3E__1_9() { return &___U3CSavePointU3E__1_9; }
	inline void set_U3CSavePointU3E__1_9(String_t* value)
	{
		___U3CSavePointU3E__1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSavePointU3E__1_9), value);
	}

	inline static int32_t get_offset_of_U3CSaveLevelU3E__1_10() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CSaveLevelU3E__1_10)); }
	inline String_t* get_U3CSaveLevelU3E__1_10() const { return ___U3CSaveLevelU3E__1_10; }
	inline String_t** get_address_of_U3CSaveLevelU3E__1_10() { return &___U3CSaveLevelU3E__1_10; }
	inline void set_U3CSaveLevelU3E__1_10(String_t* value)
	{
		___U3CSaveLevelU3E__1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLevelU3E__1_10), value);
	}

	inline static int32_t get_offset_of_U3C_SaveLevelU3E__1_11() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3C_SaveLevelU3E__1_11)); }
	inline String_t* get_U3C_SaveLevelU3E__1_11() const { return ___U3C_SaveLevelU3E__1_11; }
	inline String_t** get_address_of_U3C_SaveLevelU3E__1_11() { return &___U3C_SaveLevelU3E__1_11; }
	inline void set_U3C_SaveLevelU3E__1_11(String_t* value)
	{
		___U3C_SaveLevelU3E__1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_SaveLevelU3E__1_11), value);
	}

	inline static int32_t get_offset_of_U3C_savePointStringU3E__1_12() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3C_savePointStringU3E__1_12)); }
	inline String_t* get_U3C_savePointStringU3E__1_12() const { return ___U3C_savePointStringU3E__1_12; }
	inline String_t** get_address_of_U3C_savePointStringU3E__1_12() { return &___U3C_savePointStringU3E__1_12; }
	inline void set_U3C_savePointStringU3E__1_12(String_t* value)
	{
		___U3C_savePointStringU3E__1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_savePointStringU3E__1_12), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUserResponseU3E__1_13() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CCurrentUserResponseU3E__1_13)); }
	inline String_t* get_U3CCurrentUserResponseU3E__1_13() const { return ___U3CCurrentUserResponseU3E__1_13; }
	inline String_t** get_address_of_U3CCurrentUserResponseU3E__1_13() { return &___U3CCurrentUserResponseU3E__1_13; }
	inline void set_U3CCurrentUserResponseU3E__1_13(String_t* value)
	{
		___U3CCurrentUserResponseU3E__1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentUserResponseU3E__1_13), value);
	}

	inline static int32_t get_offset_of_U3CpostScoreURLU3E__2_14() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CpostScoreURLU3E__2_14)); }
	inline String_t* get_U3CpostScoreURLU3E__2_14() const { return ___U3CpostScoreURLU3E__2_14; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__2_14() { return &___U3CpostScoreURLU3E__2_14; }
	inline void set_U3CpostScoreURLU3E__2_14(String_t* value)
	{
		___U3CpostScoreURLU3E__2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__2_14), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__2_15() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CpostHeaderU3E__2_15)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__2_15() const { return ___U3CpostHeaderU3E__2_15; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__2_15() { return &___U3CpostHeaderU3E__2_15; }
	inline void set_U3CpostHeaderU3E__2_15(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__2_15), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_16() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CwwwU3E__2_16)); }
	inline WWW_t3599262362 * get_U3CwwwU3E__2_16() const { return ___U3CwwwU3E__2_16; }
	inline WWW_t3599262362 ** get_address_of_U3CwwwU3E__2_16() { return &___U3CwwwU3E__2_16; }
	inline void set_U3CwwwU3E__2_16(WWW_t3599262362 * value)
	{
		___U3CwwwU3E__2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__2_16), value);
	}

	inline static int32_t get_offset_of_U3CwriterU3E__3_17() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CwriterU3E__3_17)); }
	inline StreamWriter_t2875046975 * get_U3CwriterU3E__3_17() const { return ___U3CwriterU3E__3_17; }
	inline StreamWriter_t2875046975 ** get_address_of_U3CwriterU3E__3_17() { return &___U3CwriterU3E__3_17; }
	inline void set_U3CwriterU3E__3_17(StreamWriter_t2875046975 * value)
	{
		___U3CwriterU3E__3_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwriterU3E__3_17), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E__3_18() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CreaderU3E__3_18)); }
	inline StreamReader_t1796800705 * get_U3CreaderU3E__3_18() const { return ___U3CreaderU3E__3_18; }
	inline StreamReader_t1796800705 ** get_address_of_U3CreaderU3E__3_18() { return &___U3CreaderU3E__3_18; }
	inline void set_U3CreaderU3E__3_18(StreamReader_t1796800705 * value)
	{
		___U3CreaderU3E__3_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E__3_18), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3E__3_19() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U3CcontentU3E__3_19)); }
	inline String_t* get_U3CcontentU3E__3_19() const { return ___U3CcontentU3E__3_19; }
	inline String_t** get_address_of_U3CcontentU3E__3_19() { return &___U3CcontentU3E__3_19; }
	inline void set_U3CcontentU3E__3_19(String_t* value)
	{
		___U3CcontentU3E__3_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3E__3_19), value);
	}

	inline static int32_t get_offset_of_U24this_20() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U24this_20)); }
	inline CapturedDataInput_t2616152122 * get_U24this_20() const { return ___U24this_20; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_20() { return &___U24this_20; }
	inline void set_U24this_20(CapturedDataInput_t2616152122 * value)
	{
		___U24this_20 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_20), value);
	}

	inline static int32_t get_offset_of_U24current_21() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U24current_21)); }
	inline RuntimeObject * get_U24current_21() const { return ___U24current_21; }
	inline RuntimeObject ** get_address_of_U24current_21() { return &___U24current_21; }
	inline void set_U24current_21(RuntimeObject * value)
	{
		___U24current_21 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_21), value);
	}

	inline static int32_t get_offset_of_U24disposing_22() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U24disposing_22)); }
	inline bool get_U24disposing_22() const { return ___U24disposing_22; }
	inline bool* get_address_of_U24disposing_22() { return &___U24disposing_22; }
	inline void set_U24disposing_22(bool value)
	{
		___U24disposing_22 = value;
	}

	inline static int32_t get_offset_of_U24PC_23() { return static_cast<int32_t>(offsetof(U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380, ___U24PC_23)); }
	inline int32_t get_U24PC_23() const { return ___U24PC_23; }
	inline int32_t* get_address_of_U24PC_23() { return &___U24PC_23; }
	inline void set_U24PC_23(int32_t value)
	{
		___U24PC_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEONSERVERORLOCALU3EC__ITERATORE_T2814865380_H
#ifndef U3CSAVEUSERPHQAEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORA_T1904098941_H
#define U3CSAVEUSERPHQAEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORA_T1904098941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA
struct  U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941  : public RuntimeObject
{
public:
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_eventID
	int32_t ____eventID_0;
	// System.String CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_1;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_levelNum
	int32_t ____levelNum_2;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q1A
	int32_t ____Q1A_3;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q2A
	int32_t ____Q2A_4;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q3A
	int32_t ____Q3A_5;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q4A
	int32_t ____Q4A_6;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q5A
	int32_t ____Q5A_7;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q6A
	int32_t ____Q6A_8;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q7A
	int32_t ____Q7A_9;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q8A
	int32_t ____Q8A_10;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::_Q9A
	int32_t ____Q9A_11;
	// System.String CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_12;
	// System.Text.UTF8Encoding CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_14;
	// CapturedDataInput CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::$this
	CapturedDataInput_t2616152122 * ___U24this_15;
	// System.Object CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::$current
	RuntimeObject * ___U24current_16;
	// System.Boolean CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::$disposing
	bool ___U24disposing_17;
	// System.Int32 CapturedDataInput/<SaveUserPHQAEventToServer_NewJsonSchema>c__IteratorA::$PC
	int32_t ___U24PC_18;

public:
	inline static int32_t get_offset_of__eventID_0() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____eventID_0)); }
	inline int32_t get__eventID_0() const { return ____eventID_0; }
	inline int32_t* get_address_of__eventID_0() { return &____eventID_0; }
	inline void set__eventID_0(int32_t value)
	{
		____eventID_0 = value;
	}

	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U3CpostScoreURLU3E__0_1)); }
	inline String_t* get_U3CpostScoreURLU3E__0_1() const { return ___U3CpostScoreURLU3E__0_1; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_1() { return &___U3CpostScoreURLU3E__0_1; }
	inline void set_U3CpostScoreURLU3E__0_1(String_t* value)
	{
		___U3CpostScoreURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of__levelNum_2() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____levelNum_2)); }
	inline int32_t get__levelNum_2() const { return ____levelNum_2; }
	inline int32_t* get_address_of__levelNum_2() { return &____levelNum_2; }
	inline void set__levelNum_2(int32_t value)
	{
		____levelNum_2 = value;
	}

	inline static int32_t get_offset_of__Q1A_3() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q1A_3)); }
	inline int32_t get__Q1A_3() const { return ____Q1A_3; }
	inline int32_t* get_address_of__Q1A_3() { return &____Q1A_3; }
	inline void set__Q1A_3(int32_t value)
	{
		____Q1A_3 = value;
	}

	inline static int32_t get_offset_of__Q2A_4() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q2A_4)); }
	inline int32_t get__Q2A_4() const { return ____Q2A_4; }
	inline int32_t* get_address_of__Q2A_4() { return &____Q2A_4; }
	inline void set__Q2A_4(int32_t value)
	{
		____Q2A_4 = value;
	}

	inline static int32_t get_offset_of__Q3A_5() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q3A_5)); }
	inline int32_t get__Q3A_5() const { return ____Q3A_5; }
	inline int32_t* get_address_of__Q3A_5() { return &____Q3A_5; }
	inline void set__Q3A_5(int32_t value)
	{
		____Q3A_5 = value;
	}

	inline static int32_t get_offset_of__Q4A_6() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q4A_6)); }
	inline int32_t get__Q4A_6() const { return ____Q4A_6; }
	inline int32_t* get_address_of__Q4A_6() { return &____Q4A_6; }
	inline void set__Q4A_6(int32_t value)
	{
		____Q4A_6 = value;
	}

	inline static int32_t get_offset_of__Q5A_7() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q5A_7)); }
	inline int32_t get__Q5A_7() const { return ____Q5A_7; }
	inline int32_t* get_address_of__Q5A_7() { return &____Q5A_7; }
	inline void set__Q5A_7(int32_t value)
	{
		____Q5A_7 = value;
	}

	inline static int32_t get_offset_of__Q6A_8() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q6A_8)); }
	inline int32_t get__Q6A_8() const { return ____Q6A_8; }
	inline int32_t* get_address_of__Q6A_8() { return &____Q6A_8; }
	inline void set__Q6A_8(int32_t value)
	{
		____Q6A_8 = value;
	}

	inline static int32_t get_offset_of__Q7A_9() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q7A_9)); }
	inline int32_t get__Q7A_9() const { return ____Q7A_9; }
	inline int32_t* get_address_of__Q7A_9() { return &____Q7A_9; }
	inline void set__Q7A_9(int32_t value)
	{
		____Q7A_9 = value;
	}

	inline static int32_t get_offset_of__Q8A_10() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q8A_10)); }
	inline int32_t get__Q8A_10() const { return ____Q8A_10; }
	inline int32_t* get_address_of__Q8A_10() { return &____Q8A_10; }
	inline void set__Q8A_10(int32_t value)
	{
		____Q8A_10 = value;
	}

	inline static int32_t get_offset_of__Q9A_11() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ____Q9A_11)); }
	inline int32_t get__Q9A_11() const { return ____Q9A_11; }
	inline int32_t* get_address_of__Q9A_11() { return &____Q9A_11; }
	inline void set__Q9A_11(int32_t value)
	{
		____Q9A_11 = value;
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_12() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U3CjsonStringU3E__0_12)); }
	inline String_t* get_U3CjsonStringU3E__0_12() const { return ___U3CjsonStringU3E__0_12; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_12() { return &___U3CjsonStringU3E__0_12; }
	inline void set_U3CjsonStringU3E__0_12(String_t* value)
	{
		___U3CjsonStringU3E__0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_12), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_13() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U3CencodingU3E__0_13)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_13() const { return ___U3CencodingU3E__0_13; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_13() { return &___U3CencodingU3E__0_13; }
	inline void set_U3CencodingU3E__0_13(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_13), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_14() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U3CpostHeaderU3E__0_14)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_14() const { return ___U3CpostHeaderU3E__0_14; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_14() { return &___U3CpostHeaderU3E__0_14; }
	inline void set_U3CpostHeaderU3E__0_14(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_14), value);
	}

	inline static int32_t get_offset_of_U24this_15() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U24this_15)); }
	inline CapturedDataInput_t2616152122 * get_U24this_15() const { return ___U24this_15; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_15() { return &___U24this_15; }
	inline void set_U24this_15(CapturedDataInput_t2616152122 * value)
	{
		___U24this_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_15), value);
	}

	inline static int32_t get_offset_of_U24current_16() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U24current_16)); }
	inline RuntimeObject * get_U24current_16() const { return ___U24current_16; }
	inline RuntimeObject ** get_address_of_U24current_16() { return &___U24current_16; }
	inline void set_U24current_16(RuntimeObject * value)
	{
		___U24current_16 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_16), value);
	}

	inline static int32_t get_offset_of_U24disposing_17() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U24disposing_17)); }
	inline bool get_U24disposing_17() const { return ___U24disposing_17; }
	inline bool* get_address_of_U24disposing_17() { return &___U24disposing_17; }
	inline void set_U24disposing_17(bool value)
	{
		___U24disposing_17 = value;
	}

	inline static int32_t get_offset_of_U24PC_18() { return static_cast<int32_t>(offsetof(U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941, ___U24PC_18)); }
	inline int32_t get_U24PC_18() const { return ___U24PC_18; }
	inline int32_t* get_address_of_U24PC_18() { return &___U24PC_18; }
	inline void set_U24PC_18(int32_t value)
	{
		___U24PC_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEUSERPHQAEVENTTOSERVER_NEWJSONSCHEMAU3EC__ITERATORA_T1904098941_H
#ifndef U3CREADUSERPROGRESSDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR4_T454470102_H
#define U3CREADUSERPROGRESSDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR4_T454470102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4
struct  U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::<test>__0
	String_t* ___U3CtestU3E__0_0;
	// System.Boolean CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::bOffline
	bool ___bOffline_1;
	// System.String CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::<userResponseURL>__1
	String_t* ___U3CuserResponseURLU3E__1_2;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::<json>__0
	JSONObject_t2742677291 * ___U3CjsonU3E__0_3;
	// System.String CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::<tempNum>__2
	String_t* ___U3CtempNumU3E__2_4;
	// System.String CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::<SavePoint>__3
	String_t* ___U3CSavePointU3E__3_5;
	// CapturedDataInput CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::$this
	CapturedDataInput_t2616152122 * ___U24this_6;
	// System.Object CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::$disposing
	bool ___U24disposing_8;
	// System.Int32 CapturedDataInput/<ReadUserProgressDataFromServer_NewJsonSchema>c__Iterator4::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U3CtestU3E__0_0)); }
	inline String_t* get_U3CtestU3E__0_0() const { return ___U3CtestU3E__0_0; }
	inline String_t** get_address_of_U3CtestU3E__0_0() { return &___U3CtestU3E__0_0; }
	inline void set_U3CtestU3E__0_0(String_t* value)
	{
		___U3CtestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_bOffline_1() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___bOffline_1)); }
	inline bool get_bOffline_1() const { return ___bOffline_1; }
	inline bool* get_address_of_bOffline_1() { return &___bOffline_1; }
	inline void set_bOffline_1(bool value)
	{
		___bOffline_1 = value;
	}

	inline static int32_t get_offset_of_U3CuserResponseURLU3E__1_2() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U3CuserResponseURLU3E__1_2)); }
	inline String_t* get_U3CuserResponseURLU3E__1_2() const { return ___U3CuserResponseURLU3E__1_2; }
	inline String_t** get_address_of_U3CuserResponseURLU3E__1_2() { return &___U3CuserResponseURLU3E__1_2; }
	inline void set_U3CuserResponseURLU3E__1_2(String_t* value)
	{
		___U3CuserResponseURLU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserResponseURLU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_3() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U3CjsonU3E__0_3)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__0_3() const { return ___U3CjsonU3E__0_3; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__0_3() { return &___U3CjsonU3E__0_3; }
	inline void set_U3CjsonU3E__0_3(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CtempNumU3E__2_4() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U3CtempNumU3E__2_4)); }
	inline String_t* get_U3CtempNumU3E__2_4() const { return ___U3CtempNumU3E__2_4; }
	inline String_t** get_address_of_U3CtempNumU3E__2_4() { return &___U3CtempNumU3E__2_4; }
	inline void set_U3CtempNumU3E__2_4(String_t* value)
	{
		___U3CtempNumU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempNumU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3CSavePointU3E__3_5() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U3CSavePointU3E__3_5)); }
	inline String_t* get_U3CSavePointU3E__3_5() const { return ___U3CSavePointU3E__3_5; }
	inline String_t** get_address_of_U3CSavePointU3E__3_5() { return &___U3CSavePointU3E__3_5; }
	inline void set_U3CSavePointU3E__3_5(String_t* value)
	{
		___U3CSavePointU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSavePointU3E__3_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U24this_6)); }
	inline CapturedDataInput_t2616152122 * get_U24this_6() const { return ___U24this_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(CapturedDataInput_t2616152122 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADUSERPROGRESSDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR4_T454470102_H
#ifndef U3CSAVEUSERPROCESSTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR3_T3799581858_H
#define U3CSAVEUSERPROCESSTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR3_T3799581858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3
struct  U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::<postScoreURL>__0
	String_t* ___U3CpostScoreURLU3E__0_0;
	// System.Int32 CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::_levelNum
	int32_t ____levelNum_1;
	// System.String CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_2;
	// System.Text.UTF8Encoding CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_4;
	// CapturedDataInput CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::$this
	CapturedDataInput_t2616152122 * ___U24this_5;
	// System.Object CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 CapturedDataInput/<SaveUserProcessToServer_NewJsonSchema>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CpostScoreURLU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U3CpostScoreURLU3E__0_0)); }
	inline String_t* get_U3CpostScoreURLU3E__0_0() const { return ___U3CpostScoreURLU3E__0_0; }
	inline String_t** get_address_of_U3CpostScoreURLU3E__0_0() { return &___U3CpostScoreURLU3E__0_0; }
	inline void set_U3CpostScoreURLU3E__0_0(String_t* value)
	{
		___U3CpostScoreURLU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostScoreURLU3E__0_0), value);
	}

	inline static int32_t get_offset_of__levelNum_1() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ____levelNum_1)); }
	inline int32_t get__levelNum_1() const { return ____levelNum_1; }
	inline int32_t* get_address_of__levelNum_1() { return &____levelNum_1; }
	inline void set__levelNum_1(int32_t value)
	{
		____levelNum_1 = value;
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U3CjsonStringU3E__0_2)); }
	inline String_t* get_U3CjsonStringU3E__0_2() const { return ___U3CjsonStringU3E__0_2; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_2() { return &___U3CjsonStringU3E__0_2; }
	inline void set_U3CjsonStringU3E__0_2(String_t* value)
	{
		___U3CjsonStringU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U3CencodingU3E__0_3)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_3() const { return ___U3CencodingU3E__0_3; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_3() { return &___U3CencodingU3E__0_3; }
	inline void set_U3CencodingU3E__0_3(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U3CpostHeaderU3E__0_4)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_4() const { return ___U3CpostHeaderU3E__0_4; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_4() { return &___U3CpostHeaderU3E__0_4; }
	inline void set_U3CpostHeaderU3E__0_4(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U24this_5)); }
	inline CapturedDataInput_t2616152122 * get_U24this_5() const { return ___U24this_5; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CapturedDataInput_t2616152122 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEUSERPROCESSTOSERVER_NEWJSONSCHEMAU3EC__ITERATOR3_T3799581858_H
#ifndef U3CSENDOFFGAMECONFIGURL_NEWJSONSCHEMAU3EC__ITERATOR2_T3358985275_H
#define U3CSENDOFFGAMECONFIGURL_NEWJSONSCHEMAU3EC__ITERATOR2_T3358985275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2
struct  U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2::_url
	String_t* ____url_0;
	// CapturedDataInput CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2::$this
	CapturedDataInput_t2616152122 * ___U24this_1;
	// System.Object CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 CapturedDataInput/<SendOffGameConfigURL_NewJsonSchema>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of__url_0() { return static_cast<int32_t>(offsetof(U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275, ____url_0)); }
	inline String_t* get__url_0() const { return ____url_0; }
	inline String_t** get_address_of__url_0() { return &____url_0; }
	inline void set__url_0(String_t* value)
	{
		____url_0 = value;
		Il2CppCodeGenWriteBarrier((&____url_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275, ___U24this_1)); }
	inline CapturedDataInput_t2616152122 * get_U24this_1() const { return ___U24this_1; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CapturedDataInput_t2616152122 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDOFFGAMECONFIGURL_NEWJSONSCHEMAU3EC__ITERATOR2_T3358985275_H
#ifndef U3CREGISTERPROCESS_NEWJSONSCHEMAU3EC__ITERATOR1_T1441432181_H
#define U3CREGISTERPROCESS_NEWJSONSCHEMAU3EC__ITERATOR1_T1441432181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1
struct  U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::<regURL>__0
	String_t* ___U3CregURLU3E__0_0;
	// System.Text.UTF8Encoding CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_2;
	// System.String CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::jsonString
	String_t* ___jsonString_3;
	// CapturedDataInput CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::$this
	CapturedDataInput_t2616152122 * ___U24this_4;
	// System.Object CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 CapturedDataInput/<RegisterProcess_NewJsonSchema>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CregURLU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U3CregURLU3E__0_0)); }
	inline String_t* get_U3CregURLU3E__0_0() const { return ___U3CregURLU3E__0_0; }
	inline String_t** get_address_of_U3CregURLU3E__0_0() { return &___U3CregURLU3E__0_0; }
	inline void set_U3CregURLU3E__0_0(String_t* value)
	{
		___U3CregURLU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CregURLU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U3CencodingU3E__0_1)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_1() const { return ___U3CencodingU3E__0_1; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_1() { return &___U3CencodingU3E__0_1; }
	inline void set_U3CencodingU3E__0_1(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U3CpostHeaderU3E__0_2)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_2() const { return ___U3CpostHeaderU3E__0_2; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_2() { return &___U3CpostHeaderU3E__0_2; }
	inline void set_U3CpostHeaderU3E__0_2(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_2), value);
	}

	inline static int32_t get_offset_of_jsonString_3() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___jsonString_3)); }
	inline String_t* get_jsonString_3() const { return ___jsonString_3; }
	inline String_t** get_address_of_jsonString_3() { return &___jsonString_3; }
	inline void set_jsonString_3(String_t* value)
	{
		___jsonString_3 = value;
		Il2CppCodeGenWriteBarrier((&___jsonString_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U24this_4)); }
	inline CapturedDataInput_t2616152122 * get_U24this_4() const { return ___U24this_4; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CapturedDataInput_t2616152122 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREGISTERPROCESS_NEWJSONSCHEMAU3EC__ITERATOR1_T1441432181_H
#ifndef U3CLOGINPROCESS_NEWJSONSCHEMAU3EC__ITERATOR0_T2529769024_H
#define U3CLOGINPROCESS_NEWJSONSCHEMAU3EC__ITERATOR0_T2529769024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0
struct  U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::<loginURL>__0
	String_t* ___U3CloginURLU3E__0_0;
	// System.String CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::Username
	String_t* ___Username_1;
	// System.String CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::Password
	String_t* ___Password_2;
	// System.String CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::<jsonString>__0
	String_t* ___U3CjsonStringU3E__0_3;
	// System.Text.UTF8Encoding CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::<encoding>__0
	UTF8Encoding_t347516576 * ___U3CencodingU3E__0_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::<postHeader>__0
	Dictionary_2_t1060102516 * ___U3CpostHeaderU3E__0_5;
	// CapturedDataInput CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::$this
	CapturedDataInput_t2616152122 * ___U24this_6;
	// System.Object CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 CapturedDataInput/<LoginProcess_NewJsonSchema>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CloginURLU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U3CloginURLU3E__0_0)); }
	inline String_t* get_U3CloginURLU3E__0_0() const { return ___U3CloginURLU3E__0_0; }
	inline String_t** get_address_of_U3CloginURLU3E__0_0() { return &___U3CloginURLU3E__0_0; }
	inline void set_U3CloginURLU3E__0_0(String_t* value)
	{
		___U3CloginURLU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloginURLU3E__0_0), value);
	}

	inline static int32_t get_offset_of_Username_1() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___Username_1)); }
	inline String_t* get_Username_1() const { return ___Username_1; }
	inline String_t** get_address_of_Username_1() { return &___Username_1; }
	inline void set_Username_1(String_t* value)
	{
		___Username_1 = value;
		Il2CppCodeGenWriteBarrier((&___Username_1), value);
	}

	inline static int32_t get_offset_of_Password_2() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___Password_2)); }
	inline String_t* get_Password_2() const { return ___Password_2; }
	inline String_t** get_address_of_Password_2() { return &___Password_2; }
	inline void set_Password_2(String_t* value)
	{
		___Password_2 = value;
		Il2CppCodeGenWriteBarrier((&___Password_2), value);
	}

	inline static int32_t get_offset_of_U3CjsonStringU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U3CjsonStringU3E__0_3)); }
	inline String_t* get_U3CjsonStringU3E__0_3() const { return ___U3CjsonStringU3E__0_3; }
	inline String_t** get_address_of_U3CjsonStringU3E__0_3() { return &___U3CjsonStringU3E__0_3; }
	inline void set_U3CjsonStringU3E__0_3(String_t* value)
	{
		___U3CjsonStringU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStringU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__0_4() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U3CencodingU3E__0_4)); }
	inline UTF8Encoding_t347516576 * get_U3CencodingU3E__0_4() const { return ___U3CencodingU3E__0_4; }
	inline UTF8Encoding_t347516576 ** get_address_of_U3CencodingU3E__0_4() { return &___U3CencodingU3E__0_4; }
	inline void set_U3CencodingU3E__0_4(UTF8Encoding_t347516576 * value)
	{
		___U3CencodingU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CencodingU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__0_5() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U3CpostHeaderU3E__0_5)); }
	inline Dictionary_2_t1060102516 * get_U3CpostHeaderU3E__0_5() const { return ___U3CpostHeaderU3E__0_5; }
	inline Dictionary_2_t1060102516 ** get_address_of_U3CpostHeaderU3E__0_5() { return &___U3CpostHeaderU3E__0_5; }
	inline void set_U3CpostHeaderU3E__0_5(Dictionary_2_t1060102516 * value)
	{
		___U3CpostHeaderU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostHeaderU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U24this_6)); }
	inline CapturedDataInput_t2616152122 * get_U24this_6() const { return ___U24this_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(CapturedDataInput_t2616152122 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGINPROCESS_NEWJSONSCHEMAU3EC__ITERATOR0_T2529769024_H
#ifndef U3CWAITFORHITU3EC__ITERATOR0_T582703684_H
#define U3CWAITFORHITU3EC__ITERATOR0_T582703684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerFreeze/<WaitForHit>c__Iterator0
struct  U3CWaitForHitU3Ec__Iterator0_t582703684  : public RuntimeObject
{
public:
	// PlayerFreeze PlayerFreeze/<WaitForHit>c__Iterator0::$this
	PlayerFreeze_t446343823 * ___U24this_0;
	// System.Object PlayerFreeze/<WaitForHit>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PlayerFreeze/<WaitForHit>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PlayerFreeze/<WaitForHit>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitForHitU3Ec__Iterator0_t582703684, ___U24this_0)); }
	inline PlayerFreeze_t446343823 * get_U24this_0() const { return ___U24this_0; }
	inline PlayerFreeze_t446343823 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlayerFreeze_t446343823 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitForHitU3Ec__Iterator0_t582703684, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitForHitU3Ec__Iterator0_t582703684, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitForHitU3Ec__Iterator0_t582703684, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORHITU3EC__ITERATOR0_T582703684_H
#ifndef U3CWAITFORANIMTOFINISHU3EC__ITERATOR0_T3925549808_H
#define U3CWAITFORANIMTOFINISHU3EC__ITERATOR0_T3925549808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropRock/<WaitForAnimToFinish>c__Iterator0
struct  U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808  : public RuntimeObject
{
public:
	// DropRock DropRock/<WaitForAnimToFinish>c__Iterator0::$this
	DropRock_t875601513 * ___U24this_0;
	// System.Object DropRock/<WaitForAnimToFinish>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DropRock/<WaitForAnimToFinish>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DropRock/<WaitForAnimToFinish>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808, ___U24this_0)); }
	inline DropRock_t875601513 * get_U24this_0() const { return ___U24this_0; }
	inline DropRock_t875601513 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(DropRock_t875601513 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORANIMTOFINISHU3EC__ITERATOR0_T3925549808_H
#ifndef U3CWAITTOENDU3EC__ITERATOR0_T1749295493_H
#define U3CWAITTOENDU3EC__ITERATOR0_T1749295493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PushRockMiniGame/<WaitToEnd>c__Iterator0
struct  U3CWaitToEndU3Ec__Iterator0_t1749295493  : public RuntimeObject
{
public:
	// PushRockMiniGame PushRockMiniGame/<WaitToEnd>c__Iterator0::$this
	PushRockMiniGame_t1414473092 * ___U24this_0;
	// System.Object PushRockMiniGame/<WaitToEnd>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PushRockMiniGame/<WaitToEnd>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PushRockMiniGame/<WaitToEnd>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitToEndU3Ec__Iterator0_t1749295493, ___U24this_0)); }
	inline PushRockMiniGame_t1414473092 * get_U24this_0() const { return ___U24this_0; }
	inline PushRockMiniGame_t1414473092 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PushRockMiniGame_t1414473092 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitToEndU3Ec__Iterator0_t1749295493, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitToEndU3Ec__Iterator0_t1749295493, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitToEndU3Ec__Iterator0_t1749295493, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTOENDU3EC__ITERATOR0_T1749295493_H
#ifndef U3CREADUSERPROGRESSNOTEBOOKDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR5_T2419969789_H
#define U3CREADUSERPROGRESSNOTEBOOKDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR5_T2419969789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5
struct  U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789  : public RuntimeObject
{
public:
	// System.String CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::<test>__0
	String_t* ___U3CtestU3E__0_0;
	// System.Boolean CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::bOffline
	bool ___bOffline_1;
	// UnityEngine.WWWForm CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::<form>__1
	WWWForm_t815382151 * ___U3CformU3E__1_2;
	// System.Int32 CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::_levelNum
	int32_t ____levelNum_3;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::<json>__0
	JSONObject_t2742677291 * ___U3CjsonU3E__0_4;
	// Boomlagoon.JSON.JSONObject CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::<jsonNotebookItem>__0
	JSONObject_t2742677291 * ___U3CjsonNotebookItemU3E__0_5;
	// CapturedDataInput CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::$this
	CapturedDataInput_t2616152122 * ___U24this_6;
	// System.Object CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::$disposing
	bool ___U24disposing_8;
	// System.Int32 CapturedDataInput/<ReadUserProgressNoteBookDataFromServer_NewJsonSchema>c__Iterator5::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U3CtestU3E__0_0)); }
	inline String_t* get_U3CtestU3E__0_0() const { return ___U3CtestU3E__0_0; }
	inline String_t** get_address_of_U3CtestU3E__0_0() { return &___U3CtestU3E__0_0; }
	inline void set_U3CtestU3E__0_0(String_t* value)
	{
		___U3CtestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_bOffline_1() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___bOffline_1)); }
	inline bool get_bOffline_1() const { return ___bOffline_1; }
	inline bool* get_address_of_bOffline_1() { return &___bOffline_1; }
	inline void set_bOffline_1(bool value)
	{
		___bOffline_1 = value;
	}

	inline static int32_t get_offset_of_U3CformU3E__1_2() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U3CformU3E__1_2)); }
	inline WWWForm_t815382151 * get_U3CformU3E__1_2() const { return ___U3CformU3E__1_2; }
	inline WWWForm_t815382151 ** get_address_of_U3CformU3E__1_2() { return &___U3CformU3E__1_2; }
	inline void set_U3CformU3E__1_2(WWWForm_t815382151 * value)
	{
		___U3CformU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__1_2), value);
	}

	inline static int32_t get_offset_of__levelNum_3() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ____levelNum_3)); }
	inline int32_t get__levelNum_3() const { return ____levelNum_3; }
	inline int32_t* get_address_of__levelNum_3() { return &____levelNum_3; }
	inline void set__levelNum_3(int32_t value)
	{
		____levelNum_3 = value;
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_4() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U3CjsonU3E__0_4)); }
	inline JSONObject_t2742677291 * get_U3CjsonU3E__0_4() const { return ___U3CjsonU3E__0_4; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonU3E__0_4() { return &___U3CjsonU3E__0_4; }
	inline void set_U3CjsonU3E__0_4(JSONObject_t2742677291 * value)
	{
		___U3CjsonU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CjsonNotebookItemU3E__0_5() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U3CjsonNotebookItemU3E__0_5)); }
	inline JSONObject_t2742677291 * get_U3CjsonNotebookItemU3E__0_5() const { return ___U3CjsonNotebookItemU3E__0_5; }
	inline JSONObject_t2742677291 ** get_address_of_U3CjsonNotebookItemU3E__0_5() { return &___U3CjsonNotebookItemU3E__0_5; }
	inline void set_U3CjsonNotebookItemU3E__0_5(JSONObject_t2742677291 * value)
	{
		___U3CjsonNotebookItemU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonNotebookItemU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U24this_6)); }
	inline CapturedDataInput_t2616152122 * get_U24this_6() const { return ___U24this_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(CapturedDataInput_t2616152122 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADUSERPROGRESSNOTEBOOKDATAFROMSERVER_NEWJSONSCHEMAU3EC__ITERATOR5_T2419969789_H
#ifndef U3CSTARTFILEUPLOADU3EC__ITERATORF_T3320435443_H
#define U3CSTARTFILEUPLOADU3EC__ITERATORF_T3320435443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<startFileUpload>c__IteratorF
struct  U3CstartFileUploadU3Ec__IteratorF_t3320435443  : public RuntimeObject
{
public:
	// System.String[] CapturedDataInput/<startFileUpload>c__IteratorF::<allLines>__0
	StringU5BU5D_t2511808107* ___U3CallLinesU3E__0_0;
	// System.IO.StreamReader CapturedDataInput/<startFileUpload>c__IteratorF::<reader>__0
	StreamReader_t1796800705 * ___U3CreaderU3E__0_1;
	// System.Collections.Generic.List`1<System.String> CapturedDataInput/<startFileUpload>c__IteratorF::<allContent>__0
	List_1_t4069179741 * ___U3CallContentU3E__0_2;
	// System.String CapturedDataInput/<startFileUpload>c__IteratorF::<content>__0
	String_t* ___U3CcontentU3E__0_3;
	// System.String[] CapturedDataInput/<startFileUpload>c__IteratorF::$locvar0
	StringU5BU5D_t2511808107* ___U24locvar0_4;
	// System.Int32 CapturedDataInput/<startFileUpload>c__IteratorF::$locvar1
	int32_t ___U24locvar1_5;
	// System.String CapturedDataInput/<startFileUpload>c__IteratorF::<jsonStr>__1
	String_t* ___U3CjsonStrU3E__1_6;
	// CapturedDataInput CapturedDataInput/<startFileUpload>c__IteratorF::$this
	CapturedDataInput_t2616152122 * ___U24this_7;
	// System.Object CapturedDataInput/<startFileUpload>c__IteratorF::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean CapturedDataInput/<startFileUpload>c__IteratorF::$disposing
	bool ___U24disposing_9;
	// System.Int32 CapturedDataInput/<startFileUpload>c__IteratorF::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CallLinesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U3CallLinesU3E__0_0)); }
	inline StringU5BU5D_t2511808107* get_U3CallLinesU3E__0_0() const { return ___U3CallLinesU3E__0_0; }
	inline StringU5BU5D_t2511808107** get_address_of_U3CallLinesU3E__0_0() { return &___U3CallLinesU3E__0_0; }
	inline void set_U3CallLinesU3E__0_0(StringU5BU5D_t2511808107* value)
	{
		___U3CallLinesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallLinesU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E__0_1() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U3CreaderU3E__0_1)); }
	inline StreamReader_t1796800705 * get_U3CreaderU3E__0_1() const { return ___U3CreaderU3E__0_1; }
	inline StreamReader_t1796800705 ** get_address_of_U3CreaderU3E__0_1() { return &___U3CreaderU3E__0_1; }
	inline void set_U3CreaderU3E__0_1(StreamReader_t1796800705 * value)
	{
		___U3CreaderU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CallContentU3E__0_2() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U3CallContentU3E__0_2)); }
	inline List_1_t4069179741 * get_U3CallContentU3E__0_2() const { return ___U3CallContentU3E__0_2; }
	inline List_1_t4069179741 ** get_address_of_U3CallContentU3E__0_2() { return &___U3CallContentU3E__0_2; }
	inline void set_U3CallContentU3E__0_2(List_1_t4069179741 * value)
	{
		___U3CallContentU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallContentU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3E__0_3() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U3CcontentU3E__0_3)); }
	inline String_t* get_U3CcontentU3E__0_3() const { return ___U3CcontentU3E__0_3; }
	inline String_t** get_address_of_U3CcontentU3E__0_3() { return &___U3CcontentU3E__0_3; }
	inline void set_U3CcontentU3E__0_3(String_t* value)
	{
		___U3CcontentU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24locvar0_4)); }
	inline StringU5BU5D_t2511808107* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline StringU5BU5D_t2511808107** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(StringU5BU5D_t2511808107* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_4), value);
	}

	inline static int32_t get_offset_of_U24locvar1_5() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24locvar1_5)); }
	inline int32_t get_U24locvar1_5() const { return ___U24locvar1_5; }
	inline int32_t* get_address_of_U24locvar1_5() { return &___U24locvar1_5; }
	inline void set_U24locvar1_5(int32_t value)
	{
		___U24locvar1_5 = value;
	}

	inline static int32_t get_offset_of_U3CjsonStrU3E__1_6() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U3CjsonStrU3E__1_6)); }
	inline String_t* get_U3CjsonStrU3E__1_6() const { return ___U3CjsonStrU3E__1_6; }
	inline String_t** get_address_of_U3CjsonStrU3E__1_6() { return &___U3CjsonStrU3E__1_6; }
	inline void set_U3CjsonStrU3E__1_6(String_t* value)
	{
		___U3CjsonStrU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonStrU3E__1_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24this_7)); }
	inline CapturedDataInput_t2616152122 * get_U24this_7() const { return ___U24this_7; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(CapturedDataInput_t2616152122 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CstartFileUploadU3Ec__IteratorF_t3320435443, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFILEUPLOADU3EC__ITERATORF_T3320435443_H
#ifndef SOUNDPLAYER_T4159161212_H
#define SOUNDPLAYER_T4159161212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundPlayer
struct  SoundPlayer_t4159161212  : public RuntimeObject
{
public:

public:
};

struct SoundPlayer_t4159161212_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundPlayer::m_acSoundClips
	List_1_t539441572 * ___m_acSoundClips_0;
	// UnityEngine.GameObject SoundPlayer::instance
	GameObject_t2557347079 * ___instance_1;
	// UnityEngine.GameObject SoundPlayer::loopPlayer
	GameObject_t2557347079 * ___loopPlayer_2;

public:
	inline static int32_t get_offset_of_m_acSoundClips_0() { return static_cast<int32_t>(offsetof(SoundPlayer_t4159161212_StaticFields, ___m_acSoundClips_0)); }
	inline List_1_t539441572 * get_m_acSoundClips_0() const { return ___m_acSoundClips_0; }
	inline List_1_t539441572 ** get_address_of_m_acSoundClips_0() { return &___m_acSoundClips_0; }
	inline void set_m_acSoundClips_0(List_1_t539441572 * value)
	{
		___m_acSoundClips_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_acSoundClips_0), value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(SoundPlayer_t4159161212_StaticFields, ___instance_1)); }
	inline GameObject_t2557347079 * get_instance_1() const { return ___instance_1; }
	inline GameObject_t2557347079 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(GameObject_t2557347079 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_loopPlayer_2() { return static_cast<int32_t>(offsetof(SoundPlayer_t4159161212_StaticFields, ___loopPlayer_2)); }
	inline GameObject_t2557347079 * get_loopPlayer_2() const { return ___loopPlayer_2; }
	inline GameObject_t2557347079 ** get_address_of_loopPlayer_2() { return &___loopPlayer_2; }
	inline void set_loopPlayer_2(GameObject_t2557347079 * value)
	{
		___loopPlayer_2 = value;
		Il2CppCodeGenWriteBarrier((&___loopPlayer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDPLAYER_T4159161212_H
#ifndef U3CWAITU3EC__ITERATOR10_T3869735969_H
#define U3CWAITU3EC__ITERATOR10_T3869735969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput/<Wait>c__Iterator10
struct  U3CWaitU3Ec__Iterator10_t3869735969  : public RuntimeObject
{
public:
	// System.Single CapturedDataInput/<Wait>c__Iterator10::seconds
	float ___seconds_0;
	// CapturedDataInput CapturedDataInput/<Wait>c__Iterator10::$this
	CapturedDataInput_t2616152122 * ___U24this_1;
	// System.Object CapturedDataInput/<Wait>c__Iterator10::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CapturedDataInput/<Wait>c__Iterator10::$disposing
	bool ___U24disposing_3;
	// System.Int32 CapturedDataInput/<Wait>c__Iterator10::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_seconds_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator10_t3869735969, ___seconds_0)); }
	inline float get_seconds_0() const { return ___seconds_0; }
	inline float* get_address_of_seconds_0() { return &___seconds_0; }
	inline void set_seconds_0(float value)
	{
		___seconds_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator10_t3869735969, ___U24this_1)); }
	inline CapturedDataInput_t2616152122 * get_U24this_1() const { return ___U24this_1; }
	inline CapturedDataInput_t2616152122 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CapturedDataInput_t2616152122 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator10_t3869735969, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator10_t3869735969, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator10_t3869735969, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3EC__ITERATOR10_T3869735969_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T328513675_H
#define VECTOR2_T328513675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t328513675 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t328513675, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t328513675_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t328513675  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t328513675  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t328513675  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t328513675  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t328513675  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t328513675  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t328513675  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t328513675  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___zeroVector_2)); }
	inline Vector2_t328513675  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t328513675 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t328513675  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___oneVector_3)); }
	inline Vector2_t328513675  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t328513675 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t328513675  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___upVector_4)); }
	inline Vector2_t328513675  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t328513675 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t328513675  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___downVector_5)); }
	inline Vector2_t328513675  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t328513675 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t328513675  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___leftVector_6)); }
	inline Vector2_t328513675  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t328513675 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t328513675  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___rightVector_7)); }
	inline Vector2_t328513675  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t328513675 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t328513675  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t328513675  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t328513675 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t328513675  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t328513675_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t328513675  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t328513675 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t328513675  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T328513675_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef KOGSTATUS_T3201523329_H
#define KOGSTATUS_T3201523329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KOGStatus
struct  KOGStatus_t3201523329 
{
public:
	// System.Int32 KOGStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KOGStatus_t3201523329, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KOGSTATUS_T3201523329_H
#ifndef LV7BRIDGEGNATSTATUS_T3321750872_H
#define LV7BRIDGEGNATSTATUS_T3321750872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7BridgeGnatStatus
struct  Lv7BridgeGnatStatus_t3321750872 
{
public:
	// System.Int32 Lv7BridgeGnatStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Lv7BridgeGnatStatus_t3321750872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7BRIDGEGNATSTATUS_T3321750872_H
#ifndef ENDPARTSTATUS_T1026950437_H
#define ENDPARTSTATUS_T1026950437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EndPartStatus
struct  EndPartStatus_t1026950437 
{
public:
	// System.Int32 EndPartStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EndPartStatus_t1026950437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPARTSTATUS_T1026950437_H
#ifndef LEVELONESTATUS_T1390847730_H
#define LEVELONESTATUS_T1390847730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelOneStatus
struct  LevelOneStatus_t1390847730 
{
public:
	// System.Int32 LevelOneStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LevelOneStatus_t1390847730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELONESTATUS_T1390847730_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef LV6OPENCHESTCUTSCENE_T1631773017_H
#define LV6OPENCHESTCUTSCENE_T1631773017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv6OpenChestCutScene
struct  Lv6OpenChestCutScene_t1631773017 
{
public:
	// System.Int32 Lv6OpenChestCutScene::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Lv6OpenChestCutScene_t1631773017, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV6OPENCHESTCUTSCENE_T1631773017_H
#ifndef U3CSHOWTEXTU3EC__ITERATOR0_T3562466708_H
#define U3CSHOWTEXTU3EC__ITERATOR0_T3562466708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Notifications/<ShowText>c__Iterator0
struct  U3CShowTextU3Ec__Iterator0_t3562466708  : public RuntimeObject
{
public:
	// UnityEngine.Color Notifications/<ShowText>c__Iterator0::<oColor>__0
	Color_t2582018970  ___U3CoColorU3E__0_0;
	// Notifications Notifications/<ShowText>c__Iterator0::$this
	Notifications_t688528964 * ___U24this_1;
	// System.Object Notifications/<ShowText>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Notifications/<ShowText>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Notifications/<ShowText>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoColorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowTextU3Ec__Iterator0_t3562466708, ___U3CoColorU3E__0_0)); }
	inline Color_t2582018970  get_U3CoColorU3E__0_0() const { return ___U3CoColorU3E__0_0; }
	inline Color_t2582018970 * get_address_of_U3CoColorU3E__0_0() { return &___U3CoColorU3E__0_0; }
	inline void set_U3CoColorU3E__0_0(Color_t2582018970  value)
	{
		___U3CoColorU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowTextU3Ec__Iterator0_t3562466708, ___U24this_1)); }
	inline Notifications_t688528964 * get_U24this_1() const { return ___U24this_1; }
	inline Notifications_t688528964 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Notifications_t688528964 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowTextU3Ec__Iterator0_t3562466708, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowTextU3Ec__Iterator0_t3562466708, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowTextU3Ec__Iterator0_t3562466708, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWTEXTU3EC__ITERATOR0_T3562466708_H
#ifndef BARRELSTATUS_T3369822665_H
#define BARRELSTATUS_T3369822665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelStatus
struct  BarrelStatus_t3369822665 
{
public:
	// System.Int32 BarrelStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BarrelStatus_t3369822665, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARRELSTATUS_T3369822665_H
#ifndef GNATSTALKSTATUS_T808147551_H
#define GNATSTALKSTATUS_T808147551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsTalkStatus
struct  GnatsTalkStatus_t808147551 
{
public:
	// System.Int32 GnatsTalkStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GnatsTalkStatus_t808147551, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSTALKSTATUS_T808147551_H
#ifndef GNATSTATUS_T3561057505_H
#define GNATSTATUS_T3561057505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatStatus
struct  GnatStatus_t3561057505 
{
public:
	// System.Int32 GnatStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GnatStatus_t3561057505, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSTATUS_T3561057505_H
#ifndef U3CHIDETEXTU3EC__ITERATOR1_T2182125989_H
#define U3CHIDETEXTU3EC__ITERATOR1_T2182125989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Notifications/<HideText>c__Iterator1
struct  U3CHideTextU3Ec__Iterator1_t2182125989  : public RuntimeObject
{
public:
	// UnityEngine.Color Notifications/<HideText>c__Iterator1::<oColor>__0
	Color_t2582018970  ___U3CoColorU3E__0_0;
	// Notifications Notifications/<HideText>c__Iterator1::$this
	Notifications_t688528964 * ___U24this_1;
	// System.Object Notifications/<HideText>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Notifications/<HideText>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Notifications/<HideText>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoColorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CHideTextU3Ec__Iterator1_t2182125989, ___U3CoColorU3E__0_0)); }
	inline Color_t2582018970  get_U3CoColorU3E__0_0() const { return ___U3CoColorU3E__0_0; }
	inline Color_t2582018970 * get_address_of_U3CoColorU3E__0_0() { return &___U3CoColorU3E__0_0; }
	inline void set_U3CoColorU3E__0_0(Color_t2582018970  value)
	{
		___U3CoColorU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CHideTextU3Ec__Iterator1_t2182125989, ___U24this_1)); }
	inline Notifications_t688528964 * get_U24this_1() const { return ___U24this_1; }
	inline Notifications_t688528964 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Notifications_t688528964 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CHideTextU3Ec__Iterator1_t2182125989, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CHideTextU3Ec__Iterator1_t2182125989, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CHideTextU3Ec__Iterator1_t2182125989, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDETEXTU3EC__ITERATOR1_T2182125989_H
#ifndef EMOVEMENT_T4140963026_H
#define EMOVEMENT_T4140963026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EMovement
struct  EMovement_t4140963026 
{
public:
	// System.Int32 EMovement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EMovement_t4140963026, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMOVEMENT_T4140963026_H
#ifndef LV7CLIFFSCENE_T1442947585_H
#define LV7CLIFFSCENE_T1442947585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7CliffScene
struct  Lv7CliffScene_t1442947585 
{
public:
	// System.Int32 Lv7CliffScene::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Lv7CliffScene_t1442947585, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7CLIFFSCENE_T1442947585_H
#ifndef KOGATTACK_T789146765_H
#define KOGATTACK_T789146765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KOGAttack
struct  KOGAttack_t789146765 
{
public:
	// System.Int32 KOGAttack::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KOGAttack_t789146765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KOGATTACK_T789146765_H
#ifndef ATTACKMETHOD_T1775622954_H
#define ATTACKMETHOD_T1775622954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackMethod
struct  AttackMethod_t1775622954 
{
public:
	// System.Int32 AttackMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttackMethod_t1775622954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKMETHOD_T1775622954_H
#ifndef BARRELGNATSTATUS_T1584189780_H
#define BARRELGNATSTATUS_T1584189780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelGnatStatus
struct  BarrelGnatStatus_t1584189780 
{
public:
	// System.Int32 BarrelGnatStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BarrelGnatStatus_t1584189780, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARRELGNATSTATUS_T1584189780_H
#ifndef EAGLERETURNTOLVS_T188099863_H
#define EAGLERETURNTOLVS_T188099863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleReturnToLvS
struct  EagleReturnToLvS_t188099863 
{
public:
	// System.Int32 EagleReturnToLvS::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EagleReturnToLvS_t188099863, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLERETURNTOLVS_T188099863_H
#ifndef LV5MINIGAME2STATUS_T3456318513_H
#define LV5MINIGAME2STATUS_T3456318513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv5MiniGame2Status
struct  Lv5MiniGame2Status_t3456318513 
{
public:
	// System.Int32 Lv5MiniGame2Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Lv5MiniGame2Status_t3456318513, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV5MINIGAME2STATUS_T3456318513_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef CAMERASHAKES_T3649564702_H
#define CAMERASHAKES_T3649564702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShakes
struct  CameraShakes_t3649564702  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform CameraShakes::camTransform
	Transform_t362059596 * ___camTransform_2;
	// System.Single CameraShakes::shakeAmount
	float ___shakeAmount_3;
	// UnityEngine.Vector3 CameraShakes::originalPos
	Vector3_t1986933152  ___originalPos_4;

public:
	inline static int32_t get_offset_of_camTransform_2() { return static_cast<int32_t>(offsetof(CameraShakes_t3649564702, ___camTransform_2)); }
	inline Transform_t362059596 * get_camTransform_2() const { return ___camTransform_2; }
	inline Transform_t362059596 ** get_address_of_camTransform_2() { return &___camTransform_2; }
	inline void set_camTransform_2(Transform_t362059596 * value)
	{
		___camTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_2), value);
	}

	inline static int32_t get_offset_of_shakeAmount_3() { return static_cast<int32_t>(offsetof(CameraShakes_t3649564702, ___shakeAmount_3)); }
	inline float get_shakeAmount_3() const { return ___shakeAmount_3; }
	inline float* get_address_of_shakeAmount_3() { return &___shakeAmount_3; }
	inline void set_shakeAmount_3(float value)
	{
		___shakeAmount_3 = value;
	}

	inline static int32_t get_offset_of_originalPos_4() { return static_cast<int32_t>(offsetof(CameraShakes_t3649564702, ___originalPos_4)); }
	inline Vector3_t1986933152  get_originalPos_4() const { return ___originalPos_4; }
	inline Vector3_t1986933152 * get_address_of_originalPos_4() { return &___originalPos_4; }
	inline void set_originalPos_4(Vector3_t1986933152  value)
	{
		___originalPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKES_T3649564702_H
#ifndef REMOVEDUPLICATEOBJECTS_T2188025678_H
#define REMOVEDUPLICATEOBJECTS_T2188025678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoveDuplicateObjects
struct  RemoveDuplicateObjects_t2188025678  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 RemoveDuplicateObjects::frameCount
	int32_t ___frameCount_2;

public:
	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(RemoveDuplicateObjects_t2188025678, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEDUPLICATEOBJECTS_T2188025678_H
#ifndef PLAYERINFOLOADONLEVELLOAD_T3228362066_H
#define PLAYERINFOLOADONLEVELLOAD_T3228362066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerInfoLoadOnLevelLoad
struct  PlayerInfoLoadOnLevelLoad_t3228362066  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean PlayerInfoLoadOnLevelLoad::m_bLevelDataIsLoaded
	bool ___m_bLevelDataIsLoaded_3;
	// System.Int32 PlayerInfoLoadOnLevelLoad::m_iTotalHairStyleNumber
	int32_t ___m_iTotalHairStyleNumber_4;
	// System.Boolean PlayerInfoLoadOnLevelLoad::m_bRetrievingLevelData
	bool ___m_bRetrievingLevelData_5;
	// System.Boolean PlayerInfoLoadOnLevelLoad::m_bLoadDataOnce
	bool ___m_bLoadDataOnce_6;
	// UnityEngine.Texture2D PlayerInfoLoadOnLevelLoad::m_tTrimTexture
	Texture2D_t3063074017 * ___m_tTrimTexture_7;
	// CapturedDataInput PlayerInfoLoadOnLevelLoad::instance
	CapturedDataInput_t2616152122 * ___instance_8;

public:
	inline static int32_t get_offset_of_m_bLevelDataIsLoaded_3() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___m_bLevelDataIsLoaded_3)); }
	inline bool get_m_bLevelDataIsLoaded_3() const { return ___m_bLevelDataIsLoaded_3; }
	inline bool* get_address_of_m_bLevelDataIsLoaded_3() { return &___m_bLevelDataIsLoaded_3; }
	inline void set_m_bLevelDataIsLoaded_3(bool value)
	{
		___m_bLevelDataIsLoaded_3 = value;
	}

	inline static int32_t get_offset_of_m_iTotalHairStyleNumber_4() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___m_iTotalHairStyleNumber_4)); }
	inline int32_t get_m_iTotalHairStyleNumber_4() const { return ___m_iTotalHairStyleNumber_4; }
	inline int32_t* get_address_of_m_iTotalHairStyleNumber_4() { return &___m_iTotalHairStyleNumber_4; }
	inline void set_m_iTotalHairStyleNumber_4(int32_t value)
	{
		___m_iTotalHairStyleNumber_4 = value;
	}

	inline static int32_t get_offset_of_m_bRetrievingLevelData_5() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___m_bRetrievingLevelData_5)); }
	inline bool get_m_bRetrievingLevelData_5() const { return ___m_bRetrievingLevelData_5; }
	inline bool* get_address_of_m_bRetrievingLevelData_5() { return &___m_bRetrievingLevelData_5; }
	inline void set_m_bRetrievingLevelData_5(bool value)
	{
		___m_bRetrievingLevelData_5 = value;
	}

	inline static int32_t get_offset_of_m_bLoadDataOnce_6() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___m_bLoadDataOnce_6)); }
	inline bool get_m_bLoadDataOnce_6() const { return ___m_bLoadDataOnce_6; }
	inline bool* get_address_of_m_bLoadDataOnce_6() { return &___m_bLoadDataOnce_6; }
	inline void set_m_bLoadDataOnce_6(bool value)
	{
		___m_bLoadDataOnce_6 = value;
	}

	inline static int32_t get_offset_of_m_tTrimTexture_7() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___m_tTrimTexture_7)); }
	inline Texture2D_t3063074017 * get_m_tTrimTexture_7() const { return ___m_tTrimTexture_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tTrimTexture_7() { return &___m_tTrimTexture_7; }
	inline void set_m_tTrimTexture_7(Texture2D_t3063074017 * value)
	{
		___m_tTrimTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tTrimTexture_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066, ___instance_8)); }
	inline CapturedDataInput_t2616152122 * get_instance_8() const { return ___instance_8; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(CapturedDataInput_t2616152122 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

struct PlayerInfoLoadOnLevelLoad_t3228362066_StaticFields
{
public:
	// PlayerInfoLoadOnLevelLoad PlayerInfoLoadOnLevelLoad::instanceRef
	PlayerInfoLoadOnLevelLoad_t3228362066 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(PlayerInfoLoadOnLevelLoad_t3228362066_StaticFields, ___instanceRef_2)); }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline PlayerInfoLoadOnLevelLoad_t3228362066 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(PlayerInfoLoadOnLevelLoad_t3228362066 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINFOLOADONLEVELLOAD_T3228362066_H
#ifndef CAPTUREDDATAINPUT_T2616152122_H
#define CAPTUREDDATAINPUT_T2616152122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CapturedDataInput
struct  CapturedDataInput_t2616152122  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean CapturedDataInput::bGotDynamicURL_RESPONSE_NULL
	bool ___bGotDynamicURL_RESPONSE_NULL_3;
	// System.Boolean CapturedDataInput::bDynamicURL_ATTEMPTING
	bool ___bDynamicURL_ATTEMPTING_4;
	// System.Boolean CapturedDataInput::bNoDynamicURL_AFTERATTEMPTS
	bool ___bNoDynamicURL_AFTERATTEMPTS_5;
	// System.Int32 CapturedDataInput::attemps
	int32_t ___attemps_6;
	// System.String CapturedDataInput::m_sGameLoginToken
	String_t* ___m_sGameLoginToken_7;
	// System.String CapturedDataInput::m_sGameDynamicToken
	String_t* ___m_sGameDynamicToken_8;
	// System.String CapturedDataInput::m_sGameConfigServerURL
	String_t* ___m_sGameConfigServerURL_9;
	// System.String CapturedDataInput::m_sDynamicURL
	String_t* ___m_sDynamicURL_10;
	// System.Int32 CapturedDataInput::m_iServerLevelNum
	int32_t ___m_iServerLevelNum_11;
	// System.String CapturedDataInput::m_sSaveProgressURL
	String_t* ___m_sSaveProgressURL_12;
	// System.String CapturedDataInput::m_sReadProgressURL
	String_t* ___m_sReadProgressURL_13;
	// UnityEngine.WWW CapturedDataInput::m_dynamiURLRetriever
	WWW_t3599262362 * ___m_dynamiURLRetriever_14;
	// UnityEngine.WWW CapturedDataInput::m_ProcessDataPostRetriever
	WWW_t3599262362 * ___m_ProcessDataPostRetriever_15;
	// UnityEngine.WWW CapturedDataInput::m_ProcessDataGetRetriever
	WWW_t3599262362 * ___m_ProcessDataGetRetriever_16;
	// UnityEngine.WWW CapturedDataInput::m_ProcessNotebookDataGetRetriever
	WWW_t3599262362 * ___m_ProcessNotebookDataGetRetriever_17;
	// UnityEngine.WWW CapturedDataInput::m_LevelDataPostRetriever
	WWW_t3599262362 * ___m_LevelDataPostRetriever_18;
	// UnityEngine.WWW CapturedDataInput::m_LevelDataGetRetriever
	WWW_t3599262362 * ___m_LevelDataGetRetriever_19;
	// UnityEngine.WWW CapturedDataInput::m_EventLevelStartEndDataPostRetriever
	WWW_t3599262362 * ___m_EventLevelStartEndDataPostRetriever_20;
	// UnityEngine.WWW CapturedDataInput::m_EventPHQADataPostRetriever
	WWW_t3599262362 * ___m_EventPHQADataPostRetriever_21;
	// UnityEngine.WWW CapturedDataInput::m_sLoadURLReturn
	WWW_t3599262362 * ___m_sLoadURLReturn_22;
	// UnityEngine.WWW CapturedDataInput::m_sSaveURLReturn
	WWW_t3599262362 * ___m_sSaveURLReturn_23;
	// UnityEngine.WWW CapturedDataInput::m_txtDataSender
	WWW_t3599262362 * ___m_txtDataSender_24;
	// UnityEngine.WWW CapturedDataInput::m_ProcessLoginAPIDrupal
	WWW_t3599262362 * ___m_ProcessLoginAPIDrupal_25;
	// UnityEngine.WWW CapturedDataInput::m_ProcessRegistrationAPIDrupal
	WWW_t3599262362 * ___m_ProcessRegistrationAPIDrupal_26;
	// System.String CapturedDataInput::failedFileName
	String_t* ___failedFileName_27;
	// System.Boolean CapturedDataInput::m_bFoundLevelNumber
	bool ___m_bFoundLevelNumber_28;
	// System.Boolean CapturedDataInput::m_bLoggedInConfirmed
	bool ___m_bLoggedInConfirmed_29;
	// System.Boolean CapturedDataInput::m_dynamicLoginSuccessful
	bool ___m_dynamicLoginSuccessful_30;
	// System.Boolean CapturedDataInput::m_loginSuccessful
	bool ___m_loginSuccessful_31;
	// System.String CapturedDataInput::m_sNotebookHeader
	String_t* ___m_sNotebookHeader_32;
	// System.String CapturedDataInput::platform_type
	String_t* ___platform_type_33;
	// UnityEngine.GameObject CapturedDataInput::LoginPanel
	GameObject_t2557347079 * ___LoginPanel_34;
	// UnityEngine.GameObject CapturedDataInput::SignUpPanel
	GameObject_t2557347079 * ___SignUpPanel_35;
	// TokenHolderLegacy CapturedDataInput::instanceTHL
	TokenHolderLegacy_t1380075429 * ___instanceTHL_37;
	// System.Boolean CapturedDataInput::bUploadFileComplete
	bool ___bUploadFileComplete_38;
	// System.Boolean CapturedDataInput::bWaitingForSeconds
	bool ___bWaitingForSeconds_39;
	// System.Boolean CapturedDataInput::bOfflineFinishLevelSave
	bool ___bOfflineFinishLevelSave_40;
	// System.Boolean CapturedDataInput::bLevelNumberLoaded
	bool ___bLevelNumberLoaded_41;

public:
	inline static int32_t get_offset_of_bGotDynamicURL_RESPONSE_NULL_3() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bGotDynamicURL_RESPONSE_NULL_3)); }
	inline bool get_bGotDynamicURL_RESPONSE_NULL_3() const { return ___bGotDynamicURL_RESPONSE_NULL_3; }
	inline bool* get_address_of_bGotDynamicURL_RESPONSE_NULL_3() { return &___bGotDynamicURL_RESPONSE_NULL_3; }
	inline void set_bGotDynamicURL_RESPONSE_NULL_3(bool value)
	{
		___bGotDynamicURL_RESPONSE_NULL_3 = value;
	}

	inline static int32_t get_offset_of_bDynamicURL_ATTEMPTING_4() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bDynamicURL_ATTEMPTING_4)); }
	inline bool get_bDynamicURL_ATTEMPTING_4() const { return ___bDynamicURL_ATTEMPTING_4; }
	inline bool* get_address_of_bDynamicURL_ATTEMPTING_4() { return &___bDynamicURL_ATTEMPTING_4; }
	inline void set_bDynamicURL_ATTEMPTING_4(bool value)
	{
		___bDynamicURL_ATTEMPTING_4 = value;
	}

	inline static int32_t get_offset_of_bNoDynamicURL_AFTERATTEMPTS_5() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bNoDynamicURL_AFTERATTEMPTS_5)); }
	inline bool get_bNoDynamicURL_AFTERATTEMPTS_5() const { return ___bNoDynamicURL_AFTERATTEMPTS_5; }
	inline bool* get_address_of_bNoDynamicURL_AFTERATTEMPTS_5() { return &___bNoDynamicURL_AFTERATTEMPTS_5; }
	inline void set_bNoDynamicURL_AFTERATTEMPTS_5(bool value)
	{
		___bNoDynamicURL_AFTERATTEMPTS_5 = value;
	}

	inline static int32_t get_offset_of_attemps_6() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___attemps_6)); }
	inline int32_t get_attemps_6() const { return ___attemps_6; }
	inline int32_t* get_address_of_attemps_6() { return &___attemps_6; }
	inline void set_attemps_6(int32_t value)
	{
		___attemps_6 = value;
	}

	inline static int32_t get_offset_of_m_sGameLoginToken_7() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sGameLoginToken_7)); }
	inline String_t* get_m_sGameLoginToken_7() const { return ___m_sGameLoginToken_7; }
	inline String_t** get_address_of_m_sGameLoginToken_7() { return &___m_sGameLoginToken_7; }
	inline void set_m_sGameLoginToken_7(String_t* value)
	{
		___m_sGameLoginToken_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_sGameLoginToken_7), value);
	}

	inline static int32_t get_offset_of_m_sGameDynamicToken_8() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sGameDynamicToken_8)); }
	inline String_t* get_m_sGameDynamicToken_8() const { return ___m_sGameDynamicToken_8; }
	inline String_t** get_address_of_m_sGameDynamicToken_8() { return &___m_sGameDynamicToken_8; }
	inline void set_m_sGameDynamicToken_8(String_t* value)
	{
		___m_sGameDynamicToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_sGameDynamicToken_8), value);
	}

	inline static int32_t get_offset_of_m_sGameConfigServerURL_9() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sGameConfigServerURL_9)); }
	inline String_t* get_m_sGameConfigServerURL_9() const { return ___m_sGameConfigServerURL_9; }
	inline String_t** get_address_of_m_sGameConfigServerURL_9() { return &___m_sGameConfigServerURL_9; }
	inline void set_m_sGameConfigServerURL_9(String_t* value)
	{
		___m_sGameConfigServerURL_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_sGameConfigServerURL_9), value);
	}

	inline static int32_t get_offset_of_m_sDynamicURL_10() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sDynamicURL_10)); }
	inline String_t* get_m_sDynamicURL_10() const { return ___m_sDynamicURL_10; }
	inline String_t** get_address_of_m_sDynamicURL_10() { return &___m_sDynamicURL_10; }
	inline void set_m_sDynamicURL_10(String_t* value)
	{
		___m_sDynamicURL_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sDynamicURL_10), value);
	}

	inline static int32_t get_offset_of_m_iServerLevelNum_11() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_iServerLevelNum_11)); }
	inline int32_t get_m_iServerLevelNum_11() const { return ___m_iServerLevelNum_11; }
	inline int32_t* get_address_of_m_iServerLevelNum_11() { return &___m_iServerLevelNum_11; }
	inline void set_m_iServerLevelNum_11(int32_t value)
	{
		___m_iServerLevelNum_11 = value;
	}

	inline static int32_t get_offset_of_m_sSaveProgressURL_12() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sSaveProgressURL_12)); }
	inline String_t* get_m_sSaveProgressURL_12() const { return ___m_sSaveProgressURL_12; }
	inline String_t** get_address_of_m_sSaveProgressURL_12() { return &___m_sSaveProgressURL_12; }
	inline void set_m_sSaveProgressURL_12(String_t* value)
	{
		___m_sSaveProgressURL_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_sSaveProgressURL_12), value);
	}

	inline static int32_t get_offset_of_m_sReadProgressURL_13() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sReadProgressURL_13)); }
	inline String_t* get_m_sReadProgressURL_13() const { return ___m_sReadProgressURL_13; }
	inline String_t** get_address_of_m_sReadProgressURL_13() { return &___m_sReadProgressURL_13; }
	inline void set_m_sReadProgressURL_13(String_t* value)
	{
		___m_sReadProgressURL_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sReadProgressURL_13), value);
	}

	inline static int32_t get_offset_of_m_dynamiURLRetriever_14() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_dynamiURLRetriever_14)); }
	inline WWW_t3599262362 * get_m_dynamiURLRetriever_14() const { return ___m_dynamiURLRetriever_14; }
	inline WWW_t3599262362 ** get_address_of_m_dynamiURLRetriever_14() { return &___m_dynamiURLRetriever_14; }
	inline void set_m_dynamiURLRetriever_14(WWW_t3599262362 * value)
	{
		___m_dynamiURLRetriever_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_dynamiURLRetriever_14), value);
	}

	inline static int32_t get_offset_of_m_ProcessDataPostRetriever_15() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_ProcessDataPostRetriever_15)); }
	inline WWW_t3599262362 * get_m_ProcessDataPostRetriever_15() const { return ___m_ProcessDataPostRetriever_15; }
	inline WWW_t3599262362 ** get_address_of_m_ProcessDataPostRetriever_15() { return &___m_ProcessDataPostRetriever_15; }
	inline void set_m_ProcessDataPostRetriever_15(WWW_t3599262362 * value)
	{
		___m_ProcessDataPostRetriever_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessDataPostRetriever_15), value);
	}

	inline static int32_t get_offset_of_m_ProcessDataGetRetriever_16() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_ProcessDataGetRetriever_16)); }
	inline WWW_t3599262362 * get_m_ProcessDataGetRetriever_16() const { return ___m_ProcessDataGetRetriever_16; }
	inline WWW_t3599262362 ** get_address_of_m_ProcessDataGetRetriever_16() { return &___m_ProcessDataGetRetriever_16; }
	inline void set_m_ProcessDataGetRetriever_16(WWW_t3599262362 * value)
	{
		___m_ProcessDataGetRetriever_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessDataGetRetriever_16), value);
	}

	inline static int32_t get_offset_of_m_ProcessNotebookDataGetRetriever_17() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_ProcessNotebookDataGetRetriever_17)); }
	inline WWW_t3599262362 * get_m_ProcessNotebookDataGetRetriever_17() const { return ___m_ProcessNotebookDataGetRetriever_17; }
	inline WWW_t3599262362 ** get_address_of_m_ProcessNotebookDataGetRetriever_17() { return &___m_ProcessNotebookDataGetRetriever_17; }
	inline void set_m_ProcessNotebookDataGetRetriever_17(WWW_t3599262362 * value)
	{
		___m_ProcessNotebookDataGetRetriever_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessNotebookDataGetRetriever_17), value);
	}

	inline static int32_t get_offset_of_m_LevelDataPostRetriever_18() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_LevelDataPostRetriever_18)); }
	inline WWW_t3599262362 * get_m_LevelDataPostRetriever_18() const { return ___m_LevelDataPostRetriever_18; }
	inline WWW_t3599262362 ** get_address_of_m_LevelDataPostRetriever_18() { return &___m_LevelDataPostRetriever_18; }
	inline void set_m_LevelDataPostRetriever_18(WWW_t3599262362 * value)
	{
		___m_LevelDataPostRetriever_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelDataPostRetriever_18), value);
	}

	inline static int32_t get_offset_of_m_LevelDataGetRetriever_19() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_LevelDataGetRetriever_19)); }
	inline WWW_t3599262362 * get_m_LevelDataGetRetriever_19() const { return ___m_LevelDataGetRetriever_19; }
	inline WWW_t3599262362 ** get_address_of_m_LevelDataGetRetriever_19() { return &___m_LevelDataGetRetriever_19; }
	inline void set_m_LevelDataGetRetriever_19(WWW_t3599262362 * value)
	{
		___m_LevelDataGetRetriever_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelDataGetRetriever_19), value);
	}

	inline static int32_t get_offset_of_m_EventLevelStartEndDataPostRetriever_20() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_EventLevelStartEndDataPostRetriever_20)); }
	inline WWW_t3599262362 * get_m_EventLevelStartEndDataPostRetriever_20() const { return ___m_EventLevelStartEndDataPostRetriever_20; }
	inline WWW_t3599262362 ** get_address_of_m_EventLevelStartEndDataPostRetriever_20() { return &___m_EventLevelStartEndDataPostRetriever_20; }
	inline void set_m_EventLevelStartEndDataPostRetriever_20(WWW_t3599262362 * value)
	{
		___m_EventLevelStartEndDataPostRetriever_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventLevelStartEndDataPostRetriever_20), value);
	}

	inline static int32_t get_offset_of_m_EventPHQADataPostRetriever_21() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_EventPHQADataPostRetriever_21)); }
	inline WWW_t3599262362 * get_m_EventPHQADataPostRetriever_21() const { return ___m_EventPHQADataPostRetriever_21; }
	inline WWW_t3599262362 ** get_address_of_m_EventPHQADataPostRetriever_21() { return &___m_EventPHQADataPostRetriever_21; }
	inline void set_m_EventPHQADataPostRetriever_21(WWW_t3599262362 * value)
	{
		___m_EventPHQADataPostRetriever_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPHQADataPostRetriever_21), value);
	}

	inline static int32_t get_offset_of_m_sLoadURLReturn_22() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sLoadURLReturn_22)); }
	inline WWW_t3599262362 * get_m_sLoadURLReturn_22() const { return ___m_sLoadURLReturn_22; }
	inline WWW_t3599262362 ** get_address_of_m_sLoadURLReturn_22() { return &___m_sLoadURLReturn_22; }
	inline void set_m_sLoadURLReturn_22(WWW_t3599262362 * value)
	{
		___m_sLoadURLReturn_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_sLoadURLReturn_22), value);
	}

	inline static int32_t get_offset_of_m_sSaveURLReturn_23() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sSaveURLReturn_23)); }
	inline WWW_t3599262362 * get_m_sSaveURLReturn_23() const { return ___m_sSaveURLReturn_23; }
	inline WWW_t3599262362 ** get_address_of_m_sSaveURLReturn_23() { return &___m_sSaveURLReturn_23; }
	inline void set_m_sSaveURLReturn_23(WWW_t3599262362 * value)
	{
		___m_sSaveURLReturn_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_sSaveURLReturn_23), value);
	}

	inline static int32_t get_offset_of_m_txtDataSender_24() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_txtDataSender_24)); }
	inline WWW_t3599262362 * get_m_txtDataSender_24() const { return ___m_txtDataSender_24; }
	inline WWW_t3599262362 ** get_address_of_m_txtDataSender_24() { return &___m_txtDataSender_24; }
	inline void set_m_txtDataSender_24(WWW_t3599262362 * value)
	{
		___m_txtDataSender_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtDataSender_24), value);
	}

	inline static int32_t get_offset_of_m_ProcessLoginAPIDrupal_25() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_ProcessLoginAPIDrupal_25)); }
	inline WWW_t3599262362 * get_m_ProcessLoginAPIDrupal_25() const { return ___m_ProcessLoginAPIDrupal_25; }
	inline WWW_t3599262362 ** get_address_of_m_ProcessLoginAPIDrupal_25() { return &___m_ProcessLoginAPIDrupal_25; }
	inline void set_m_ProcessLoginAPIDrupal_25(WWW_t3599262362 * value)
	{
		___m_ProcessLoginAPIDrupal_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessLoginAPIDrupal_25), value);
	}

	inline static int32_t get_offset_of_m_ProcessRegistrationAPIDrupal_26() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_ProcessRegistrationAPIDrupal_26)); }
	inline WWW_t3599262362 * get_m_ProcessRegistrationAPIDrupal_26() const { return ___m_ProcessRegistrationAPIDrupal_26; }
	inline WWW_t3599262362 ** get_address_of_m_ProcessRegistrationAPIDrupal_26() { return &___m_ProcessRegistrationAPIDrupal_26; }
	inline void set_m_ProcessRegistrationAPIDrupal_26(WWW_t3599262362 * value)
	{
		___m_ProcessRegistrationAPIDrupal_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessRegistrationAPIDrupal_26), value);
	}

	inline static int32_t get_offset_of_failedFileName_27() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___failedFileName_27)); }
	inline String_t* get_failedFileName_27() const { return ___failedFileName_27; }
	inline String_t** get_address_of_failedFileName_27() { return &___failedFileName_27; }
	inline void set_failedFileName_27(String_t* value)
	{
		___failedFileName_27 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_27), value);
	}

	inline static int32_t get_offset_of_m_bFoundLevelNumber_28() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_bFoundLevelNumber_28)); }
	inline bool get_m_bFoundLevelNumber_28() const { return ___m_bFoundLevelNumber_28; }
	inline bool* get_address_of_m_bFoundLevelNumber_28() { return &___m_bFoundLevelNumber_28; }
	inline void set_m_bFoundLevelNumber_28(bool value)
	{
		___m_bFoundLevelNumber_28 = value;
	}

	inline static int32_t get_offset_of_m_bLoggedInConfirmed_29() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_bLoggedInConfirmed_29)); }
	inline bool get_m_bLoggedInConfirmed_29() const { return ___m_bLoggedInConfirmed_29; }
	inline bool* get_address_of_m_bLoggedInConfirmed_29() { return &___m_bLoggedInConfirmed_29; }
	inline void set_m_bLoggedInConfirmed_29(bool value)
	{
		___m_bLoggedInConfirmed_29 = value;
	}

	inline static int32_t get_offset_of_m_dynamicLoginSuccessful_30() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_dynamicLoginSuccessful_30)); }
	inline bool get_m_dynamicLoginSuccessful_30() const { return ___m_dynamicLoginSuccessful_30; }
	inline bool* get_address_of_m_dynamicLoginSuccessful_30() { return &___m_dynamicLoginSuccessful_30; }
	inline void set_m_dynamicLoginSuccessful_30(bool value)
	{
		___m_dynamicLoginSuccessful_30 = value;
	}

	inline static int32_t get_offset_of_m_loginSuccessful_31() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_loginSuccessful_31)); }
	inline bool get_m_loginSuccessful_31() const { return ___m_loginSuccessful_31; }
	inline bool* get_address_of_m_loginSuccessful_31() { return &___m_loginSuccessful_31; }
	inline void set_m_loginSuccessful_31(bool value)
	{
		___m_loginSuccessful_31 = value;
	}

	inline static int32_t get_offset_of_m_sNotebookHeader_32() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___m_sNotebookHeader_32)); }
	inline String_t* get_m_sNotebookHeader_32() const { return ___m_sNotebookHeader_32; }
	inline String_t** get_address_of_m_sNotebookHeader_32() { return &___m_sNotebookHeader_32; }
	inline void set_m_sNotebookHeader_32(String_t* value)
	{
		___m_sNotebookHeader_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_sNotebookHeader_32), value);
	}

	inline static int32_t get_offset_of_platform_type_33() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___platform_type_33)); }
	inline String_t* get_platform_type_33() const { return ___platform_type_33; }
	inline String_t** get_address_of_platform_type_33() { return &___platform_type_33; }
	inline void set_platform_type_33(String_t* value)
	{
		___platform_type_33 = value;
		Il2CppCodeGenWriteBarrier((&___platform_type_33), value);
	}

	inline static int32_t get_offset_of_LoginPanel_34() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___LoginPanel_34)); }
	inline GameObject_t2557347079 * get_LoginPanel_34() const { return ___LoginPanel_34; }
	inline GameObject_t2557347079 ** get_address_of_LoginPanel_34() { return &___LoginPanel_34; }
	inline void set_LoginPanel_34(GameObject_t2557347079 * value)
	{
		___LoginPanel_34 = value;
		Il2CppCodeGenWriteBarrier((&___LoginPanel_34), value);
	}

	inline static int32_t get_offset_of_SignUpPanel_35() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___SignUpPanel_35)); }
	inline GameObject_t2557347079 * get_SignUpPanel_35() const { return ___SignUpPanel_35; }
	inline GameObject_t2557347079 ** get_address_of_SignUpPanel_35() { return &___SignUpPanel_35; }
	inline void set_SignUpPanel_35(GameObject_t2557347079 * value)
	{
		___SignUpPanel_35 = value;
		Il2CppCodeGenWriteBarrier((&___SignUpPanel_35), value);
	}

	inline static int32_t get_offset_of_instanceTHL_37() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___instanceTHL_37)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceTHL_37() const { return ___instanceTHL_37; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceTHL_37() { return &___instanceTHL_37; }
	inline void set_instanceTHL_37(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceTHL_37 = value;
		Il2CppCodeGenWriteBarrier((&___instanceTHL_37), value);
	}

	inline static int32_t get_offset_of_bUploadFileComplete_38() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bUploadFileComplete_38)); }
	inline bool get_bUploadFileComplete_38() const { return ___bUploadFileComplete_38; }
	inline bool* get_address_of_bUploadFileComplete_38() { return &___bUploadFileComplete_38; }
	inline void set_bUploadFileComplete_38(bool value)
	{
		___bUploadFileComplete_38 = value;
	}

	inline static int32_t get_offset_of_bWaitingForSeconds_39() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bWaitingForSeconds_39)); }
	inline bool get_bWaitingForSeconds_39() const { return ___bWaitingForSeconds_39; }
	inline bool* get_address_of_bWaitingForSeconds_39() { return &___bWaitingForSeconds_39; }
	inline void set_bWaitingForSeconds_39(bool value)
	{
		___bWaitingForSeconds_39 = value;
	}

	inline static int32_t get_offset_of_bOfflineFinishLevelSave_40() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bOfflineFinishLevelSave_40)); }
	inline bool get_bOfflineFinishLevelSave_40() const { return ___bOfflineFinishLevelSave_40; }
	inline bool* get_address_of_bOfflineFinishLevelSave_40() { return &___bOfflineFinishLevelSave_40; }
	inline void set_bOfflineFinishLevelSave_40(bool value)
	{
		___bOfflineFinishLevelSave_40 = value;
	}

	inline static int32_t get_offset_of_bLevelNumberLoaded_41() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122, ___bLevelNumberLoaded_41)); }
	inline bool get_bLevelNumberLoaded_41() const { return ___bLevelNumberLoaded_41; }
	inline bool* get_address_of_bLevelNumberLoaded_41() { return &___bLevelNumberLoaded_41; }
	inline void set_bLevelNumberLoaded_41(bool value)
	{
		___bLevelNumberLoaded_41 = value;
	}
};

struct CapturedDataInput_t2616152122_StaticFields
{
public:
	// CapturedDataInput CapturedDataInput::instanceRef
	CapturedDataInput_t2616152122 * ___instanceRef_2;
	// CapturedDataInput CapturedDataInput::instance
	CapturedDataInput_t2616152122 * ___instance_36;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122_StaticFields, ___instanceRef_2)); }
	inline CapturedDataInput_t2616152122 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(CapturedDataInput_t2616152122 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}

	inline static int32_t get_offset_of_instance_36() { return static_cast<int32_t>(offsetof(CapturedDataInput_t2616152122_StaticFields, ___instance_36)); }
	inline CapturedDataInput_t2616152122 * get_instance_36() const { return ___instance_36; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_36() { return &___instance_36; }
	inline void set_instance_36(CapturedDataInput_t2616152122 * value)
	{
		___instance_36 = value;
		Il2CppCodeGenWriteBarrier((&___instance_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREDDATAINPUT_T2616152122_H
#ifndef PLAYERFREEZE_T446343823_H
#define PLAYERFREEZE_T446343823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerFreeze
struct  PlayerFreeze_t446343823  : public MonoBehaviour_t1618594486
{
public:
	// System.Single PlayerFreeze::fFreezeGauge
	float ___fFreezeGauge_2;
	// UnityEngine.GameObject PlayerFreeze::Overlay
	GameObject_t2557347079 * ___Overlay_3;
	// System.String PlayerFreeze::ResourceNameBase
	String_t* ___ResourceNameBase_4;
	// System.String PlayerFreeze::CollideObjectName
	String_t* ___CollideObjectName_5;
	// UnityEngine.Transform PlayerFreeze::ResetPos
	Transform_t362059596 * ___ResetPos_6;
	// UnityEngine.GameObject PlayerFreeze::FinishPos
	GameObject_t2557347079 * ___FinishPos_7;
	// System.Single PlayerFreeze::numberIncrease
	float ___numberIncrease_8;
	// UnityEngine.Texture2D PlayerFreeze::m_resourceNameBase4Texture
	Texture2D_t3063074017 * ___m_resourceNameBase4Texture_9;
	// UnityEngine.Texture2D PlayerFreeze::m_resourceNameBase3Texture
	Texture2D_t3063074017 * ___m_resourceNameBase3Texture_10;
	// UnityEngine.Texture2D PlayerFreeze::m_resourceNameBase2Texture
	Texture2D_t3063074017 * ___m_resourceNameBase2Texture_11;
	// UnityEngine.Texture2D PlayerFreeze::m_resourceNameBase1Texture
	Texture2D_t3063074017 * ___m_resourceNameBase1Texture_12;
	// System.Single PlayerFreeze::time
	float ___time_13;

public:
	inline static int32_t get_offset_of_fFreezeGauge_2() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___fFreezeGauge_2)); }
	inline float get_fFreezeGauge_2() const { return ___fFreezeGauge_2; }
	inline float* get_address_of_fFreezeGauge_2() { return &___fFreezeGauge_2; }
	inline void set_fFreezeGauge_2(float value)
	{
		___fFreezeGauge_2 = value;
	}

	inline static int32_t get_offset_of_Overlay_3() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___Overlay_3)); }
	inline GameObject_t2557347079 * get_Overlay_3() const { return ___Overlay_3; }
	inline GameObject_t2557347079 ** get_address_of_Overlay_3() { return &___Overlay_3; }
	inline void set_Overlay_3(GameObject_t2557347079 * value)
	{
		___Overlay_3 = value;
		Il2CppCodeGenWriteBarrier((&___Overlay_3), value);
	}

	inline static int32_t get_offset_of_ResourceNameBase_4() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___ResourceNameBase_4)); }
	inline String_t* get_ResourceNameBase_4() const { return ___ResourceNameBase_4; }
	inline String_t** get_address_of_ResourceNameBase_4() { return &___ResourceNameBase_4; }
	inline void set_ResourceNameBase_4(String_t* value)
	{
		___ResourceNameBase_4 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceNameBase_4), value);
	}

	inline static int32_t get_offset_of_CollideObjectName_5() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___CollideObjectName_5)); }
	inline String_t* get_CollideObjectName_5() const { return ___CollideObjectName_5; }
	inline String_t** get_address_of_CollideObjectName_5() { return &___CollideObjectName_5; }
	inline void set_CollideObjectName_5(String_t* value)
	{
		___CollideObjectName_5 = value;
		Il2CppCodeGenWriteBarrier((&___CollideObjectName_5), value);
	}

	inline static int32_t get_offset_of_ResetPos_6() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___ResetPos_6)); }
	inline Transform_t362059596 * get_ResetPos_6() const { return ___ResetPos_6; }
	inline Transform_t362059596 ** get_address_of_ResetPos_6() { return &___ResetPos_6; }
	inline void set_ResetPos_6(Transform_t362059596 * value)
	{
		___ResetPos_6 = value;
		Il2CppCodeGenWriteBarrier((&___ResetPos_6), value);
	}

	inline static int32_t get_offset_of_FinishPos_7() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___FinishPos_7)); }
	inline GameObject_t2557347079 * get_FinishPos_7() const { return ___FinishPos_7; }
	inline GameObject_t2557347079 ** get_address_of_FinishPos_7() { return &___FinishPos_7; }
	inline void set_FinishPos_7(GameObject_t2557347079 * value)
	{
		___FinishPos_7 = value;
		Il2CppCodeGenWriteBarrier((&___FinishPos_7), value);
	}

	inline static int32_t get_offset_of_numberIncrease_8() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___numberIncrease_8)); }
	inline float get_numberIncrease_8() const { return ___numberIncrease_8; }
	inline float* get_address_of_numberIncrease_8() { return &___numberIncrease_8; }
	inline void set_numberIncrease_8(float value)
	{
		___numberIncrease_8 = value;
	}

	inline static int32_t get_offset_of_m_resourceNameBase4Texture_9() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___m_resourceNameBase4Texture_9)); }
	inline Texture2D_t3063074017 * get_m_resourceNameBase4Texture_9() const { return ___m_resourceNameBase4Texture_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_resourceNameBase4Texture_9() { return &___m_resourceNameBase4Texture_9; }
	inline void set_m_resourceNameBase4Texture_9(Texture2D_t3063074017 * value)
	{
		___m_resourceNameBase4Texture_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_resourceNameBase4Texture_9), value);
	}

	inline static int32_t get_offset_of_m_resourceNameBase3Texture_10() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___m_resourceNameBase3Texture_10)); }
	inline Texture2D_t3063074017 * get_m_resourceNameBase3Texture_10() const { return ___m_resourceNameBase3Texture_10; }
	inline Texture2D_t3063074017 ** get_address_of_m_resourceNameBase3Texture_10() { return &___m_resourceNameBase3Texture_10; }
	inline void set_m_resourceNameBase3Texture_10(Texture2D_t3063074017 * value)
	{
		___m_resourceNameBase3Texture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_resourceNameBase3Texture_10), value);
	}

	inline static int32_t get_offset_of_m_resourceNameBase2Texture_11() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___m_resourceNameBase2Texture_11)); }
	inline Texture2D_t3063074017 * get_m_resourceNameBase2Texture_11() const { return ___m_resourceNameBase2Texture_11; }
	inline Texture2D_t3063074017 ** get_address_of_m_resourceNameBase2Texture_11() { return &___m_resourceNameBase2Texture_11; }
	inline void set_m_resourceNameBase2Texture_11(Texture2D_t3063074017 * value)
	{
		___m_resourceNameBase2Texture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_resourceNameBase2Texture_11), value);
	}

	inline static int32_t get_offset_of_m_resourceNameBase1Texture_12() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___m_resourceNameBase1Texture_12)); }
	inline Texture2D_t3063074017 * get_m_resourceNameBase1Texture_12() const { return ___m_resourceNameBase1Texture_12; }
	inline Texture2D_t3063074017 ** get_address_of_m_resourceNameBase1Texture_12() { return &___m_resourceNameBase1Texture_12; }
	inline void set_m_resourceNameBase1Texture_12(Texture2D_t3063074017 * value)
	{
		___m_resourceNameBase1Texture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_resourceNameBase1Texture_12), value);
	}

	inline static int32_t get_offset_of_time_13() { return static_cast<int32_t>(offsetof(PlayerFreeze_t446343823, ___time_13)); }
	inline float get_time_13() const { return ___time_13; }
	inline float* get_address_of_time_13() { return &___time_13; }
	inline void set_time_13(float value)
	{
		___time_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERFREEZE_T446343823_H
#ifndef PLAYEREFFECT_T398009925_H
#define PLAYEREFFECT_T398009925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerEffect
struct  PlayerEffect_t398009925  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject PlayerEffect::FireBall
	GameObject_t2557347079 * ___FireBall_2;
	// UnityEngine.GameObject PlayerEffect::FireEffect
	GameObject_t2557347079 * ___FireEffect_3;
	// UnityEngine.GameObject PlayerEffect::AttackThisObject
	GameObject_t2557347079 * ___AttackThisObject_4;
	// UnityEngine.GameObject PlayerEffect::tempEffect
	GameObject_t2557347079 * ___tempEffect_5;
	// UnityEngine.GameObject PlayerEffect::ExplosionEffect
	GameObject_t2557347079 * ___ExplosionEffect_6;
	// System.Boolean PlayerEffect::m_bAttack
	bool ___m_bAttack_7;
	// System.Single PlayerEffect::fTime
	float ___fTime_8;

public:
	inline static int32_t get_offset_of_FireBall_2() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___FireBall_2)); }
	inline GameObject_t2557347079 * get_FireBall_2() const { return ___FireBall_2; }
	inline GameObject_t2557347079 ** get_address_of_FireBall_2() { return &___FireBall_2; }
	inline void set_FireBall_2(GameObject_t2557347079 * value)
	{
		___FireBall_2 = value;
		Il2CppCodeGenWriteBarrier((&___FireBall_2), value);
	}

	inline static int32_t get_offset_of_FireEffect_3() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___FireEffect_3)); }
	inline GameObject_t2557347079 * get_FireEffect_3() const { return ___FireEffect_3; }
	inline GameObject_t2557347079 ** get_address_of_FireEffect_3() { return &___FireEffect_3; }
	inline void set_FireEffect_3(GameObject_t2557347079 * value)
	{
		___FireEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___FireEffect_3), value);
	}

	inline static int32_t get_offset_of_AttackThisObject_4() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___AttackThisObject_4)); }
	inline GameObject_t2557347079 * get_AttackThisObject_4() const { return ___AttackThisObject_4; }
	inline GameObject_t2557347079 ** get_address_of_AttackThisObject_4() { return &___AttackThisObject_4; }
	inline void set_AttackThisObject_4(GameObject_t2557347079 * value)
	{
		___AttackThisObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___AttackThisObject_4), value);
	}

	inline static int32_t get_offset_of_tempEffect_5() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___tempEffect_5)); }
	inline GameObject_t2557347079 * get_tempEffect_5() const { return ___tempEffect_5; }
	inline GameObject_t2557347079 ** get_address_of_tempEffect_5() { return &___tempEffect_5; }
	inline void set_tempEffect_5(GameObject_t2557347079 * value)
	{
		___tempEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___tempEffect_5), value);
	}

	inline static int32_t get_offset_of_ExplosionEffect_6() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___ExplosionEffect_6)); }
	inline GameObject_t2557347079 * get_ExplosionEffect_6() const { return ___ExplosionEffect_6; }
	inline GameObject_t2557347079 ** get_address_of_ExplosionEffect_6() { return &___ExplosionEffect_6; }
	inline void set_ExplosionEffect_6(GameObject_t2557347079 * value)
	{
		___ExplosionEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___ExplosionEffect_6), value);
	}

	inline static int32_t get_offset_of_m_bAttack_7() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___m_bAttack_7)); }
	inline bool get_m_bAttack_7() const { return ___m_bAttack_7; }
	inline bool* get_address_of_m_bAttack_7() { return &___m_bAttack_7; }
	inline void set_m_bAttack_7(bool value)
	{
		___m_bAttack_7 = value;
	}

	inline static int32_t get_offset_of_fTime_8() { return static_cast<int32_t>(offsetof(PlayerEffect_t398009925, ___fTime_8)); }
	inline float get_fTime_8() const { return ___fTime_8; }
	inline float* get_address_of_fTime_8() { return &___fTime_8; }
	inline void set_fTime_8(float value)
	{
		___fTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREFFECT_T398009925_H
#ifndef PLAYERCOMBAT_T6700974_H
#define PLAYERCOMBAT_T6700974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerCombat
struct  PlayerCombat_t6700974  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCOMBAT_T6700974_H
#ifndef CUSTOMISATION_T1896805424_H
#define CUSTOMISATION_T1896805424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Customisation
struct  Customisation_t1896805424  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject Customisation::character
	GameObject_t2557347079 * ___character_2;
	// UnityEngine.Color Customisation::m_rCurrentHairColor
	Color_t2582018970  ___m_rCurrentHairColor_3;
	// UnityEngine.Color Customisation::m_rCurrentTrimColor
	Color_t2582018970  ___m_rCurrentTrimColor_4;
	// UnityEngine.Texture2D Customisation::m_tCurrentTrimTexture
	Texture2D_t3063074017 * ___m_tCurrentTrimTexture_5;
	// System.Int32 Customisation::m_iTotalHairColorNum
	int32_t ___m_iTotalHairColorNum_6;
	// System.Int32 Customisation::m_iCurrentHairColorNum
	int32_t ___m_iCurrentHairColorNum_7;
	// System.Int32 Customisation::m_iTotalEyeColorNum
	int32_t ___m_iTotalEyeColorNum_8;
	// System.Int32 Customisation::m_iCurrentEyeColorNum
	int32_t ___m_iCurrentEyeColorNum_9;
	// UnityEngine.Color Customisation::m_rCurrentEyeColor
	Color_t2582018970  ___m_rCurrentEyeColor_10;
	// System.Int32 Customisation::m_iTotalSkinColorNum
	int32_t ___m_iTotalSkinColorNum_11;
	// System.Int32 Customisation::m_iCurrentSkinColorNum
	int32_t ___m_iCurrentSkinColorNum_12;
	// System.Int32 Customisation::m_iCurrentHairStyleNum
	int32_t ___m_iCurrentHairStyleNum_13;
	// System.Int32 Customisation::m_iCurrentTrimNum
	int32_t ___m_iCurrentTrimNum_14;
	// System.Int32 Customisation::m_iCurrentBackpackNum
	int32_t ___m_iCurrentBackpackNum_15;
	// System.Int32 Customisation::m_iTotalTrimColorNum
	int32_t ___m_iTotalTrimColorNum_16;
	// System.Int32 Customisation::m_iCurrentTrimColorNum
	int32_t ___m_iCurrentTrimColorNum_17;
	// System.Int32 Customisation::m_iTotalClothColorNum
	int32_t ___m_iTotalClothColorNum_18;
	// System.Int32 Customisation::m_iCurrentClothColorNum
	int32_t ___m_iCurrentClothColorNum_19;
	// UnityEngine.Color Customisation::m_rCurrentColorColor
	Color_t2582018970  ___m_rCurrentColorColor_20;
	// System.Int32 Customisation::m_iTotalTrimNum
	int32_t ___m_iTotalTrimNum_21;
	// System.Int32 Customisation::m_iTotalBackpackNum
	int32_t ___m_iTotalBackpackNum_22;
	// System.Int32 Customisation::m_iBodyType
	int32_t ___m_iBodyType_23;
	// UnityEngine.Texture2D[] Customisation::m_tTrimTextures
	Texture2DU5BU5D_t3304433276* ___m_tTrimTextures_24;
	// System.Int32 Customisation::m_iTotalHairStyleNumber
	int32_t ___m_iTotalHairStyleNumber_25;
	// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer> Customisation::m_hairMeshes
	List_1_t1370252420 * ___m_hairMeshes_26;
	// CapturedDataInput Customisation::instance
	CapturedDataInput_t2616152122 * ___instance_27;
	// CharacterCreation Customisation::creationScreen
	CharacterCreation_t2214710272 * ___creationScreen_28;

public:
	inline static int32_t get_offset_of_character_2() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___character_2)); }
	inline GameObject_t2557347079 * get_character_2() const { return ___character_2; }
	inline GameObject_t2557347079 ** get_address_of_character_2() { return &___character_2; }
	inline void set_character_2(GameObject_t2557347079 * value)
	{
		___character_2 = value;
		Il2CppCodeGenWriteBarrier((&___character_2), value);
	}

	inline static int32_t get_offset_of_m_rCurrentHairColor_3() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentHairColor_3)); }
	inline Color_t2582018970  get_m_rCurrentHairColor_3() const { return ___m_rCurrentHairColor_3; }
	inline Color_t2582018970 * get_address_of_m_rCurrentHairColor_3() { return &___m_rCurrentHairColor_3; }
	inline void set_m_rCurrentHairColor_3(Color_t2582018970  value)
	{
		___m_rCurrentHairColor_3 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentTrimColor_4() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentTrimColor_4)); }
	inline Color_t2582018970  get_m_rCurrentTrimColor_4() const { return ___m_rCurrentTrimColor_4; }
	inline Color_t2582018970 * get_address_of_m_rCurrentTrimColor_4() { return &___m_rCurrentTrimColor_4; }
	inline void set_m_rCurrentTrimColor_4(Color_t2582018970  value)
	{
		___m_rCurrentTrimColor_4 = value;
	}

	inline static int32_t get_offset_of_m_tCurrentTrimTexture_5() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_tCurrentTrimTexture_5)); }
	inline Texture2D_t3063074017 * get_m_tCurrentTrimTexture_5() const { return ___m_tCurrentTrimTexture_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCurrentTrimTexture_5() { return &___m_tCurrentTrimTexture_5; }
	inline void set_m_tCurrentTrimTexture_5(Texture2D_t3063074017 * value)
	{
		___m_tCurrentTrimTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCurrentTrimTexture_5), value);
	}

	inline static int32_t get_offset_of_m_iTotalHairColorNum_6() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalHairColorNum_6)); }
	inline int32_t get_m_iTotalHairColorNum_6() const { return ___m_iTotalHairColorNum_6; }
	inline int32_t* get_address_of_m_iTotalHairColorNum_6() { return &___m_iTotalHairColorNum_6; }
	inline void set_m_iTotalHairColorNum_6(int32_t value)
	{
		___m_iTotalHairColorNum_6 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentHairColorNum_7() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentHairColorNum_7)); }
	inline int32_t get_m_iCurrentHairColorNum_7() const { return ___m_iCurrentHairColorNum_7; }
	inline int32_t* get_address_of_m_iCurrentHairColorNum_7() { return &___m_iCurrentHairColorNum_7; }
	inline void set_m_iCurrentHairColorNum_7(int32_t value)
	{
		___m_iCurrentHairColorNum_7 = value;
	}

	inline static int32_t get_offset_of_m_iTotalEyeColorNum_8() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalEyeColorNum_8)); }
	inline int32_t get_m_iTotalEyeColorNum_8() const { return ___m_iTotalEyeColorNum_8; }
	inline int32_t* get_address_of_m_iTotalEyeColorNum_8() { return &___m_iTotalEyeColorNum_8; }
	inline void set_m_iTotalEyeColorNum_8(int32_t value)
	{
		___m_iTotalEyeColorNum_8 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentEyeColorNum_9() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentEyeColorNum_9)); }
	inline int32_t get_m_iCurrentEyeColorNum_9() const { return ___m_iCurrentEyeColorNum_9; }
	inline int32_t* get_address_of_m_iCurrentEyeColorNum_9() { return &___m_iCurrentEyeColorNum_9; }
	inline void set_m_iCurrentEyeColorNum_9(int32_t value)
	{
		___m_iCurrentEyeColorNum_9 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentEyeColor_10() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentEyeColor_10)); }
	inline Color_t2582018970  get_m_rCurrentEyeColor_10() const { return ___m_rCurrentEyeColor_10; }
	inline Color_t2582018970 * get_address_of_m_rCurrentEyeColor_10() { return &___m_rCurrentEyeColor_10; }
	inline void set_m_rCurrentEyeColor_10(Color_t2582018970  value)
	{
		___m_rCurrentEyeColor_10 = value;
	}

	inline static int32_t get_offset_of_m_iTotalSkinColorNum_11() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalSkinColorNum_11)); }
	inline int32_t get_m_iTotalSkinColorNum_11() const { return ___m_iTotalSkinColorNum_11; }
	inline int32_t* get_address_of_m_iTotalSkinColorNum_11() { return &___m_iTotalSkinColorNum_11; }
	inline void set_m_iTotalSkinColorNum_11(int32_t value)
	{
		___m_iTotalSkinColorNum_11 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentSkinColorNum_12() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentSkinColorNum_12)); }
	inline int32_t get_m_iCurrentSkinColorNum_12() const { return ___m_iCurrentSkinColorNum_12; }
	inline int32_t* get_address_of_m_iCurrentSkinColorNum_12() { return &___m_iCurrentSkinColorNum_12; }
	inline void set_m_iCurrentSkinColorNum_12(int32_t value)
	{
		___m_iCurrentSkinColorNum_12 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentHairStyleNum_13() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentHairStyleNum_13)); }
	inline int32_t get_m_iCurrentHairStyleNum_13() const { return ___m_iCurrentHairStyleNum_13; }
	inline int32_t* get_address_of_m_iCurrentHairStyleNum_13() { return &___m_iCurrentHairStyleNum_13; }
	inline void set_m_iCurrentHairStyleNum_13(int32_t value)
	{
		___m_iCurrentHairStyleNum_13 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentTrimNum_14() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentTrimNum_14)); }
	inline int32_t get_m_iCurrentTrimNum_14() const { return ___m_iCurrentTrimNum_14; }
	inline int32_t* get_address_of_m_iCurrentTrimNum_14() { return &___m_iCurrentTrimNum_14; }
	inline void set_m_iCurrentTrimNum_14(int32_t value)
	{
		___m_iCurrentTrimNum_14 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentBackpackNum_15() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentBackpackNum_15)); }
	inline int32_t get_m_iCurrentBackpackNum_15() const { return ___m_iCurrentBackpackNum_15; }
	inline int32_t* get_address_of_m_iCurrentBackpackNum_15() { return &___m_iCurrentBackpackNum_15; }
	inline void set_m_iCurrentBackpackNum_15(int32_t value)
	{
		___m_iCurrentBackpackNum_15 = value;
	}

	inline static int32_t get_offset_of_m_iTotalTrimColorNum_16() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalTrimColorNum_16)); }
	inline int32_t get_m_iTotalTrimColorNum_16() const { return ___m_iTotalTrimColorNum_16; }
	inline int32_t* get_address_of_m_iTotalTrimColorNum_16() { return &___m_iTotalTrimColorNum_16; }
	inline void set_m_iTotalTrimColorNum_16(int32_t value)
	{
		___m_iTotalTrimColorNum_16 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentTrimColorNum_17() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentTrimColorNum_17)); }
	inline int32_t get_m_iCurrentTrimColorNum_17() const { return ___m_iCurrentTrimColorNum_17; }
	inline int32_t* get_address_of_m_iCurrentTrimColorNum_17() { return &___m_iCurrentTrimColorNum_17; }
	inline void set_m_iCurrentTrimColorNum_17(int32_t value)
	{
		___m_iCurrentTrimColorNum_17 = value;
	}

	inline static int32_t get_offset_of_m_iTotalClothColorNum_18() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalClothColorNum_18)); }
	inline int32_t get_m_iTotalClothColorNum_18() const { return ___m_iTotalClothColorNum_18; }
	inline int32_t* get_address_of_m_iTotalClothColorNum_18() { return &___m_iTotalClothColorNum_18; }
	inline void set_m_iTotalClothColorNum_18(int32_t value)
	{
		___m_iTotalClothColorNum_18 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentClothColorNum_19() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iCurrentClothColorNum_19)); }
	inline int32_t get_m_iCurrentClothColorNum_19() const { return ___m_iCurrentClothColorNum_19; }
	inline int32_t* get_address_of_m_iCurrentClothColorNum_19() { return &___m_iCurrentClothColorNum_19; }
	inline void set_m_iCurrentClothColorNum_19(int32_t value)
	{
		___m_iCurrentClothColorNum_19 = value;
	}

	inline static int32_t get_offset_of_m_rCurrentColorColor_20() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_rCurrentColorColor_20)); }
	inline Color_t2582018970  get_m_rCurrentColorColor_20() const { return ___m_rCurrentColorColor_20; }
	inline Color_t2582018970 * get_address_of_m_rCurrentColorColor_20() { return &___m_rCurrentColorColor_20; }
	inline void set_m_rCurrentColorColor_20(Color_t2582018970  value)
	{
		___m_rCurrentColorColor_20 = value;
	}

	inline static int32_t get_offset_of_m_iTotalTrimNum_21() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalTrimNum_21)); }
	inline int32_t get_m_iTotalTrimNum_21() const { return ___m_iTotalTrimNum_21; }
	inline int32_t* get_address_of_m_iTotalTrimNum_21() { return &___m_iTotalTrimNum_21; }
	inline void set_m_iTotalTrimNum_21(int32_t value)
	{
		___m_iTotalTrimNum_21 = value;
	}

	inline static int32_t get_offset_of_m_iTotalBackpackNum_22() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalBackpackNum_22)); }
	inline int32_t get_m_iTotalBackpackNum_22() const { return ___m_iTotalBackpackNum_22; }
	inline int32_t* get_address_of_m_iTotalBackpackNum_22() { return &___m_iTotalBackpackNum_22; }
	inline void set_m_iTotalBackpackNum_22(int32_t value)
	{
		___m_iTotalBackpackNum_22 = value;
	}

	inline static int32_t get_offset_of_m_iBodyType_23() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iBodyType_23)); }
	inline int32_t get_m_iBodyType_23() const { return ___m_iBodyType_23; }
	inline int32_t* get_address_of_m_iBodyType_23() { return &___m_iBodyType_23; }
	inline void set_m_iBodyType_23(int32_t value)
	{
		___m_iBodyType_23 = value;
	}

	inline static int32_t get_offset_of_m_tTrimTextures_24() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_tTrimTextures_24)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tTrimTextures_24() const { return ___m_tTrimTextures_24; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tTrimTextures_24() { return &___m_tTrimTextures_24; }
	inline void set_m_tTrimTextures_24(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tTrimTextures_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_tTrimTextures_24), value);
	}

	inline static int32_t get_offset_of_m_iTotalHairStyleNumber_25() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_iTotalHairStyleNumber_25)); }
	inline int32_t get_m_iTotalHairStyleNumber_25() const { return ___m_iTotalHairStyleNumber_25; }
	inline int32_t* get_address_of_m_iTotalHairStyleNumber_25() { return &___m_iTotalHairStyleNumber_25; }
	inline void set_m_iTotalHairStyleNumber_25(int32_t value)
	{
		___m_iTotalHairStyleNumber_25 = value;
	}

	inline static int32_t get_offset_of_m_hairMeshes_26() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___m_hairMeshes_26)); }
	inline List_1_t1370252420 * get_m_hairMeshes_26() const { return ___m_hairMeshes_26; }
	inline List_1_t1370252420 ** get_address_of_m_hairMeshes_26() { return &___m_hairMeshes_26; }
	inline void set_m_hairMeshes_26(List_1_t1370252420 * value)
	{
		___m_hairMeshes_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_hairMeshes_26), value);
	}

	inline static int32_t get_offset_of_instance_27() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___instance_27)); }
	inline CapturedDataInput_t2616152122 * get_instance_27() const { return ___instance_27; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_27() { return &___instance_27; }
	inline void set_instance_27(CapturedDataInput_t2616152122 * value)
	{
		___instance_27 = value;
		Il2CppCodeGenWriteBarrier((&___instance_27), value);
	}

	inline static int32_t get_offset_of_creationScreen_28() { return static_cast<int32_t>(offsetof(Customisation_t1896805424, ___creationScreen_28)); }
	inline CharacterCreation_t2214710272 * get_creationScreen_28() const { return ___creationScreen_28; }
	inline CharacterCreation_t2214710272 ** get_address_of_creationScreen_28() { return &___creationScreen_28; }
	inline void set_creationScreen_28(CharacterCreation_t2214710272 * value)
	{
		___creationScreen_28 = value;
		Il2CppCodeGenWriteBarrier((&___creationScreen_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMISATION_T1896805424_H
#ifndef INVENTORY_T2659890484_H
#define INVENTORY_T2659890484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory
struct  Inventory_t2659890484  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] Inventory::Items
	GameObjectU5BU5D_t2988620542* ___Items_2;
	// System.Int32 Inventory::m_icurrentPlayingMinigameIndex
	int32_t ___m_icurrentPlayingMinigameIndex_3;
	// System.String Inventory::m_sTempSceneName
	String_t* ___m_sTempSceneName_4;

public:
	inline static int32_t get_offset_of_Items_2() { return static_cast<int32_t>(offsetof(Inventory_t2659890484, ___Items_2)); }
	inline GameObjectU5BU5D_t2988620542* get_Items_2() const { return ___Items_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Items_2() { return &___Items_2; }
	inline void set_Items_2(GameObjectU5BU5D_t2988620542* value)
	{
		___Items_2 = value;
		Il2CppCodeGenWriteBarrier((&___Items_2), value);
	}

	inline static int32_t get_offset_of_m_icurrentPlayingMinigameIndex_3() { return static_cast<int32_t>(offsetof(Inventory_t2659890484, ___m_icurrentPlayingMinigameIndex_3)); }
	inline int32_t get_m_icurrentPlayingMinigameIndex_3() const { return ___m_icurrentPlayingMinigameIndex_3; }
	inline int32_t* get_address_of_m_icurrentPlayingMinigameIndex_3() { return &___m_icurrentPlayingMinigameIndex_3; }
	inline void set_m_icurrentPlayingMinigameIndex_3(int32_t value)
	{
		___m_icurrentPlayingMinigameIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_sTempSceneName_4() { return static_cast<int32_t>(offsetof(Inventory_t2659890484, ___m_sTempSceneName_4)); }
	inline String_t* get_m_sTempSceneName_4() const { return ___m_sTempSceneName_4; }
	inline String_t** get_address_of_m_sTempSceneName_4() { return &___m_sTempSceneName_4; }
	inline void set_m_sTempSceneName_4(String_t* value)
	{
		___m_sTempSceneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTempSceneName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T2659890484_H
#ifndef CAMERAMOVEMENT_T1630064389_H
#define CAMERAMOVEMENT_T1630064389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMovement
struct  CameraMovement_t1630064389  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform CameraMovement::PlayerTransform
	Transform_t362059596 * ___PlayerTransform_2;
	// System.Single CameraMovement::UpScaler
	float ___UpScaler_3;
	// System.Single CameraMovement::ForwardScaler
	float ___ForwardScaler_4;
	// System.Single CameraMovement::LookAtScaler
	float ___LookAtScaler_5;
	// System.Boolean CameraMovement::m_bFollow
	bool ___m_bFollow_6;
	// System.Boolean CameraMovement::m_bCameraLookAt
	bool ___m_bCameraLookAt_7;
	// System.Single CameraMovement::m_fDamping
	float ___m_fDamping_8;
	// UnityEngine.Vector3 CameraMovement::m_vLookAtTarget
	Vector3_t1986933152  ___m_vLookAtTarget_9;
	// UnityEngine.Vector3 CameraMovement::m_vTargetPosition
	Vector3_t1986933152  ___m_vTargetPosition_10;
	// System.Boolean CameraMovement::m_bArrivedAtTarget
	bool ___m_bArrivedAtTarget_11;

public:
	inline static int32_t get_offset_of_PlayerTransform_2() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___PlayerTransform_2)); }
	inline Transform_t362059596 * get_PlayerTransform_2() const { return ___PlayerTransform_2; }
	inline Transform_t362059596 ** get_address_of_PlayerTransform_2() { return &___PlayerTransform_2; }
	inline void set_PlayerTransform_2(Transform_t362059596 * value)
	{
		___PlayerTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerTransform_2), value);
	}

	inline static int32_t get_offset_of_UpScaler_3() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___UpScaler_3)); }
	inline float get_UpScaler_3() const { return ___UpScaler_3; }
	inline float* get_address_of_UpScaler_3() { return &___UpScaler_3; }
	inline void set_UpScaler_3(float value)
	{
		___UpScaler_3 = value;
	}

	inline static int32_t get_offset_of_ForwardScaler_4() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___ForwardScaler_4)); }
	inline float get_ForwardScaler_4() const { return ___ForwardScaler_4; }
	inline float* get_address_of_ForwardScaler_4() { return &___ForwardScaler_4; }
	inline void set_ForwardScaler_4(float value)
	{
		___ForwardScaler_4 = value;
	}

	inline static int32_t get_offset_of_LookAtScaler_5() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___LookAtScaler_5)); }
	inline float get_LookAtScaler_5() const { return ___LookAtScaler_5; }
	inline float* get_address_of_LookAtScaler_5() { return &___LookAtScaler_5; }
	inline void set_LookAtScaler_5(float value)
	{
		___LookAtScaler_5 = value;
	}

	inline static int32_t get_offset_of_m_bFollow_6() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_bFollow_6)); }
	inline bool get_m_bFollow_6() const { return ___m_bFollow_6; }
	inline bool* get_address_of_m_bFollow_6() { return &___m_bFollow_6; }
	inline void set_m_bFollow_6(bool value)
	{
		___m_bFollow_6 = value;
	}

	inline static int32_t get_offset_of_m_bCameraLookAt_7() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_bCameraLookAt_7)); }
	inline bool get_m_bCameraLookAt_7() const { return ___m_bCameraLookAt_7; }
	inline bool* get_address_of_m_bCameraLookAt_7() { return &___m_bCameraLookAt_7; }
	inline void set_m_bCameraLookAt_7(bool value)
	{
		___m_bCameraLookAt_7 = value;
	}

	inline static int32_t get_offset_of_m_fDamping_8() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_fDamping_8)); }
	inline float get_m_fDamping_8() const { return ___m_fDamping_8; }
	inline float* get_address_of_m_fDamping_8() { return &___m_fDamping_8; }
	inline void set_m_fDamping_8(float value)
	{
		___m_fDamping_8 = value;
	}

	inline static int32_t get_offset_of_m_vLookAtTarget_9() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_vLookAtTarget_9)); }
	inline Vector3_t1986933152  get_m_vLookAtTarget_9() const { return ___m_vLookAtTarget_9; }
	inline Vector3_t1986933152 * get_address_of_m_vLookAtTarget_9() { return &___m_vLookAtTarget_9; }
	inline void set_m_vLookAtTarget_9(Vector3_t1986933152  value)
	{
		___m_vLookAtTarget_9 = value;
	}

	inline static int32_t get_offset_of_m_vTargetPosition_10() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_vTargetPosition_10)); }
	inline Vector3_t1986933152  get_m_vTargetPosition_10() const { return ___m_vTargetPosition_10; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetPosition_10() { return &___m_vTargetPosition_10; }
	inline void set_m_vTargetPosition_10(Vector3_t1986933152  value)
	{
		___m_vTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_bArrivedAtTarget_11() { return static_cast<int32_t>(offsetof(CameraMovement_t1630064389, ___m_bArrivedAtTarget_11)); }
	inline bool get_m_bArrivedAtTarget_11() const { return ___m_bArrivedAtTarget_11; }
	inline bool* get_address_of_m_bArrivedAtTarget_11() { return &___m_bArrivedAtTarget_11; }
	inline void set_m_bArrivedAtTarget_11(bool value)
	{
		___m_bArrivedAtTarget_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMOVEMENT_T1630064389_H
#ifndef DROPROCK_T875601513_H
#define DROPROCK_T875601513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropRock
struct  DropRock_t875601513  : public MonoBehaviour_t1618594486
{
public:
	// System.Single DropRock::m_fDropRate
	float ___m_fDropRate_2;
	// System.Single DropRock::m_fSpeed
	float ___m_fSpeed_3;
	// UnityEngine.Texture2D DropRock::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_4;
	// UnityEngine.Texture2D DropRock::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_5;
	// System.Boolean DropRock::m_bIsDropping
	bool ___m_bIsDropping_6;
	// System.Boolean DropRock::m_bPushOff
	bool ___m_bPushOff_7;
	// System.Boolean DropRock::m_bFinished
	bool ___m_bFinished_8;
	// UnityEngine.Vector3 DropRock::position
	Vector3_t1986933152  ___position_9;

public:
	inline static int32_t get_offset_of_m_fDropRate_2() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___m_fDropRate_2)); }
	inline float get_m_fDropRate_2() const { return ___m_fDropRate_2; }
	inline float* get_address_of_m_fDropRate_2() { return &___m_fDropRate_2; }
	inline void set_m_fDropRate_2(float value)
	{
		___m_fDropRate_2 = value;
	}

	inline static int32_t get_offset_of_m_fSpeed_3() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___m_fSpeed_3)); }
	inline float get_m_fSpeed_3() const { return ___m_fSpeed_3; }
	inline float* get_address_of_m_fSpeed_3() { return &___m_fSpeed_3; }
	inline void set_m_fSpeed_3(float value)
	{
		___m_fSpeed_3 = value;
	}

	inline static int32_t get_offset_of_MouseOverTexture_4() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___MouseOverTexture_4)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_4() const { return ___MouseOverTexture_4; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_4() { return &___MouseOverTexture_4; }
	inline void set_MouseOverTexture_4(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_4), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_5() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___MouseOriTexture_5)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_5() const { return ___MouseOriTexture_5; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_5() { return &___MouseOriTexture_5; }
	inline void set_MouseOriTexture_5(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_5), value);
	}

	inline static int32_t get_offset_of_m_bIsDropping_6() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___m_bIsDropping_6)); }
	inline bool get_m_bIsDropping_6() const { return ___m_bIsDropping_6; }
	inline bool* get_address_of_m_bIsDropping_6() { return &___m_bIsDropping_6; }
	inline void set_m_bIsDropping_6(bool value)
	{
		___m_bIsDropping_6 = value;
	}

	inline static int32_t get_offset_of_m_bPushOff_7() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___m_bPushOff_7)); }
	inline bool get_m_bPushOff_7() const { return ___m_bPushOff_7; }
	inline bool* get_address_of_m_bPushOff_7() { return &___m_bPushOff_7; }
	inline void set_m_bPushOff_7(bool value)
	{
		___m_bPushOff_7 = value;
	}

	inline static int32_t get_offset_of_m_bFinished_8() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___m_bFinished_8)); }
	inline bool get_m_bFinished_8() const { return ___m_bFinished_8; }
	inline bool* get_address_of_m_bFinished_8() { return &___m_bFinished_8; }
	inline void set_m_bFinished_8(bool value)
	{
		___m_bFinished_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(DropRock_t875601513, ___position_9)); }
	inline Vector3_t1986933152  get_position_9() const { return ___position_9; }
	inline Vector3_t1986933152 * get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(Vector3_t1986933152  value)
	{
		___position_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPROCK_T875601513_H
#ifndef CLICKCHARACTER_T1040688154_H
#define CLICKCHARACTER_T1040688154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickCharacter
struct  ClickCharacter_t1040688154  : public MonoBehaviour_t1618594486
{
public:
	// CharacterSelection ClickCharacter::m_characterSelection
	CharacterSelection_t3241235818 * ___m_characterSelection_2;

public:
	inline static int32_t get_offset_of_m_characterSelection_2() { return static_cast<int32_t>(offsetof(ClickCharacter_t1040688154, ___m_characterSelection_2)); }
	inline CharacterSelection_t3241235818 * get_m_characterSelection_2() const { return ___m_characterSelection_2; }
	inline CharacterSelection_t3241235818 ** get_address_of_m_characterSelection_2() { return &___m_characterSelection_2; }
	inline void set_m_characterSelection_2(CharacterSelection_t3241235818 * value)
	{
		___m_characterSelection_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterSelection_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKCHARACTER_T1040688154_H
#ifndef PLAYERMOVEMENT_T3557127520_H
#define PLAYERMOVEMENT_T3557127520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovement
struct  PlayerMovement_t3557127520  : public MonoBehaviour_t1618594486
{
public:
	// System.Single PlayerMovement::m_fSpeed
	float ___m_fSpeed_2;
	// System.Single PlayerMovement::m_fRunningSpeed
	float ___m_fRunningSpeed_3;
	// System.Single PlayerMovement::m_fWalkingSpeed
	float ___m_fWalkingSpeed_4;
	// System.Single PlayerMovement::m_fRotationSpeed
	float ___m_fRotationSpeed_5;
	// System.Boolean PlayerMovement::m_bWalkOnly
	bool ___m_bWalkOnly_6;
	// System.Boolean PlayerMovement::m_bMovement
	bool ___m_bMovement_7;
	// System.Boolean PlayerMovement::m_movingPrivate
	bool ___m_movingPrivate_8;
	// System.Boolean PlayerMovement::m_bTuiMovementAttach
	bool ___m_bTuiMovementAttach_9;
	// UnityEngine.Vector3 PlayerMovement::m_vTargetPosition
	Vector3_t1986933152  ___m_vTargetPosition_10;
	// UnityEngine.Vector3 PlayerMovement::m_vMoveFromPosition
	Vector3_t1986933152  ___m_vMoveFromPosition_11;
	// UnityEngine.Vector3 PlayerMovement::m_vTargetLookAt
	Vector3_t1986933152  ___m_vTargetLookAt_12;
	// System.String PlayerMovement::m_strInteractionName
	String_t* ___m_strInteractionName_13;
	// System.Single PlayerMovement::m_fTime
	float ___m_fTime_14;
	// System.Single PlayerMovement::m_fNormalisedSpeed
	float ___m_fNormalisedSpeed_15;
	// System.Boolean PlayerMovement::m_bMoveTowardsNPC
	bool ___m_bMoveTowardsNPC_16;
	// System.Boolean PlayerMovement::m_bRotateTowardsTarget
	bool ___m_bRotateTowardsTarget_17;
	// System.Boolean PlayerMovement::isJumping
	bool ___isJumping_18;
	// UnityEngine.Material[] PlayerMovement::mats
	MaterialU5BU5D_t2146020731* ___mats_20;
	// System.Single PlayerMovement::jumpHeight
	float ___jumpHeight_22;
	// UnityEngine.Vector3 PlayerMovement::jumpForce
	Vector3_t1986933152  ___jumpForce_23;
	// System.Single PlayerMovement::gravity
	float ___gravity_24;
	// System.Double PlayerMovement::moveCap
	double ___moveCap_25;
	// System.Double PlayerMovement::runCap
	double ___runCap_26;
	// UnityEngine.GameObject PlayerMovement::mobileControl
	GameObject_t2557347079 * ___mobileControl_27;
	// UnityEngine.GameObject PlayerMovement::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_28;
	// UnityEngine.GameObject PlayerMovement::m_ObjectMobileControllerMove
	GameObject_t2557347079 * ___m_ObjectMobileControllerMove_29;
	// UnityEngine.GameObject PlayerMovement::m_ObjectMobileControllerSprint
	GameObject_t2557347079 * ___m_ObjectMobileControllerSprint_30;
	// UnityEngine.Vector3 PlayerMovement::m_ObjectMobileController_LeftPosition
	Vector3_t1986933152  ___m_ObjectMobileController_LeftPosition_31;
	// UnityEngine.Vector3 PlayerMovement::m_ObjectMobileController_RightPosition
	Vector3_t1986933152  ___m_ObjectMobileController_RightPosition_32;
	// UnityEngine.Vector3 PlayerMovement::slideMove
	Vector3_t1986933152  ___slideMove_33;
	// System.Boolean PlayerMovement::scriptsCanStopMovement
	bool ___scriptsCanStopMovement_34;
	// UnityEngine.Vector3 PlayerMovement::moveVector
	Vector3_t1986933152  ___moveVector_35;

public:
	inline static int32_t get_offset_of_m_fSpeed_2() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fSpeed_2)); }
	inline float get_m_fSpeed_2() const { return ___m_fSpeed_2; }
	inline float* get_address_of_m_fSpeed_2() { return &___m_fSpeed_2; }
	inline void set_m_fSpeed_2(float value)
	{
		___m_fSpeed_2 = value;
	}

	inline static int32_t get_offset_of_m_fRunningSpeed_3() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fRunningSpeed_3)); }
	inline float get_m_fRunningSpeed_3() const { return ___m_fRunningSpeed_3; }
	inline float* get_address_of_m_fRunningSpeed_3() { return &___m_fRunningSpeed_3; }
	inline void set_m_fRunningSpeed_3(float value)
	{
		___m_fRunningSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_fWalkingSpeed_4() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fWalkingSpeed_4)); }
	inline float get_m_fWalkingSpeed_4() const { return ___m_fWalkingSpeed_4; }
	inline float* get_address_of_m_fWalkingSpeed_4() { return &___m_fWalkingSpeed_4; }
	inline void set_m_fWalkingSpeed_4(float value)
	{
		___m_fWalkingSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_fRotationSpeed_5() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fRotationSpeed_5)); }
	inline float get_m_fRotationSpeed_5() const { return ___m_fRotationSpeed_5; }
	inline float* get_address_of_m_fRotationSpeed_5() { return &___m_fRotationSpeed_5; }
	inline void set_m_fRotationSpeed_5(float value)
	{
		___m_fRotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_bWalkOnly_6() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_bWalkOnly_6)); }
	inline bool get_m_bWalkOnly_6() const { return ___m_bWalkOnly_6; }
	inline bool* get_address_of_m_bWalkOnly_6() { return &___m_bWalkOnly_6; }
	inline void set_m_bWalkOnly_6(bool value)
	{
		___m_bWalkOnly_6 = value;
	}

	inline static int32_t get_offset_of_m_bMovement_7() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_bMovement_7)); }
	inline bool get_m_bMovement_7() const { return ___m_bMovement_7; }
	inline bool* get_address_of_m_bMovement_7() { return &___m_bMovement_7; }
	inline void set_m_bMovement_7(bool value)
	{
		___m_bMovement_7 = value;
	}

	inline static int32_t get_offset_of_m_movingPrivate_8() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_movingPrivate_8)); }
	inline bool get_m_movingPrivate_8() const { return ___m_movingPrivate_8; }
	inline bool* get_address_of_m_movingPrivate_8() { return &___m_movingPrivate_8; }
	inline void set_m_movingPrivate_8(bool value)
	{
		___m_movingPrivate_8 = value;
	}

	inline static int32_t get_offset_of_m_bTuiMovementAttach_9() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_bTuiMovementAttach_9)); }
	inline bool get_m_bTuiMovementAttach_9() const { return ___m_bTuiMovementAttach_9; }
	inline bool* get_address_of_m_bTuiMovementAttach_9() { return &___m_bTuiMovementAttach_9; }
	inline void set_m_bTuiMovementAttach_9(bool value)
	{
		___m_bTuiMovementAttach_9 = value;
	}

	inline static int32_t get_offset_of_m_vTargetPosition_10() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_vTargetPosition_10)); }
	inline Vector3_t1986933152  get_m_vTargetPosition_10() const { return ___m_vTargetPosition_10; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetPosition_10() { return &___m_vTargetPosition_10; }
	inline void set_m_vTargetPosition_10(Vector3_t1986933152  value)
	{
		___m_vTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_vMoveFromPosition_11() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_vMoveFromPosition_11)); }
	inline Vector3_t1986933152  get_m_vMoveFromPosition_11() const { return ___m_vMoveFromPosition_11; }
	inline Vector3_t1986933152 * get_address_of_m_vMoveFromPosition_11() { return &___m_vMoveFromPosition_11; }
	inline void set_m_vMoveFromPosition_11(Vector3_t1986933152  value)
	{
		___m_vMoveFromPosition_11 = value;
	}

	inline static int32_t get_offset_of_m_vTargetLookAt_12() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_vTargetLookAt_12)); }
	inline Vector3_t1986933152  get_m_vTargetLookAt_12() const { return ___m_vTargetLookAt_12; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetLookAt_12() { return &___m_vTargetLookAt_12; }
	inline void set_m_vTargetLookAt_12(Vector3_t1986933152  value)
	{
		___m_vTargetLookAt_12 = value;
	}

	inline static int32_t get_offset_of_m_strInteractionName_13() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_strInteractionName_13)); }
	inline String_t* get_m_strInteractionName_13() const { return ___m_strInteractionName_13; }
	inline String_t** get_address_of_m_strInteractionName_13() { return &___m_strInteractionName_13; }
	inline void set_m_strInteractionName_13(String_t* value)
	{
		___m_strInteractionName_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_strInteractionName_13), value);
	}

	inline static int32_t get_offset_of_m_fTime_14() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fTime_14)); }
	inline float get_m_fTime_14() const { return ___m_fTime_14; }
	inline float* get_address_of_m_fTime_14() { return &___m_fTime_14; }
	inline void set_m_fTime_14(float value)
	{
		___m_fTime_14 = value;
	}

	inline static int32_t get_offset_of_m_fNormalisedSpeed_15() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_fNormalisedSpeed_15)); }
	inline float get_m_fNormalisedSpeed_15() const { return ___m_fNormalisedSpeed_15; }
	inline float* get_address_of_m_fNormalisedSpeed_15() { return &___m_fNormalisedSpeed_15; }
	inline void set_m_fNormalisedSpeed_15(float value)
	{
		___m_fNormalisedSpeed_15 = value;
	}

	inline static int32_t get_offset_of_m_bMoveTowardsNPC_16() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_bMoveTowardsNPC_16)); }
	inline bool get_m_bMoveTowardsNPC_16() const { return ___m_bMoveTowardsNPC_16; }
	inline bool* get_address_of_m_bMoveTowardsNPC_16() { return &___m_bMoveTowardsNPC_16; }
	inline void set_m_bMoveTowardsNPC_16(bool value)
	{
		___m_bMoveTowardsNPC_16 = value;
	}

	inline static int32_t get_offset_of_m_bRotateTowardsTarget_17() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_bRotateTowardsTarget_17)); }
	inline bool get_m_bRotateTowardsTarget_17() const { return ___m_bRotateTowardsTarget_17; }
	inline bool* get_address_of_m_bRotateTowardsTarget_17() { return &___m_bRotateTowardsTarget_17; }
	inline void set_m_bRotateTowardsTarget_17(bool value)
	{
		___m_bRotateTowardsTarget_17 = value;
	}

	inline static int32_t get_offset_of_isJumping_18() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___isJumping_18)); }
	inline bool get_isJumping_18() const { return ___isJumping_18; }
	inline bool* get_address_of_isJumping_18() { return &___isJumping_18; }
	inline void set_isJumping_18(bool value)
	{
		___isJumping_18 = value;
	}

	inline static int32_t get_offset_of_mats_20() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___mats_20)); }
	inline MaterialU5BU5D_t2146020731* get_mats_20() const { return ___mats_20; }
	inline MaterialU5BU5D_t2146020731** get_address_of_mats_20() { return &___mats_20; }
	inline void set_mats_20(MaterialU5BU5D_t2146020731* value)
	{
		___mats_20 = value;
		Il2CppCodeGenWriteBarrier((&___mats_20), value);
	}

	inline static int32_t get_offset_of_jumpHeight_22() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___jumpHeight_22)); }
	inline float get_jumpHeight_22() const { return ___jumpHeight_22; }
	inline float* get_address_of_jumpHeight_22() { return &___jumpHeight_22; }
	inline void set_jumpHeight_22(float value)
	{
		___jumpHeight_22 = value;
	}

	inline static int32_t get_offset_of_jumpForce_23() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___jumpForce_23)); }
	inline Vector3_t1986933152  get_jumpForce_23() const { return ___jumpForce_23; }
	inline Vector3_t1986933152 * get_address_of_jumpForce_23() { return &___jumpForce_23; }
	inline void set_jumpForce_23(Vector3_t1986933152  value)
	{
		___jumpForce_23 = value;
	}

	inline static int32_t get_offset_of_gravity_24() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___gravity_24)); }
	inline float get_gravity_24() const { return ___gravity_24; }
	inline float* get_address_of_gravity_24() { return &___gravity_24; }
	inline void set_gravity_24(float value)
	{
		___gravity_24 = value;
	}

	inline static int32_t get_offset_of_moveCap_25() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___moveCap_25)); }
	inline double get_moveCap_25() const { return ___moveCap_25; }
	inline double* get_address_of_moveCap_25() { return &___moveCap_25; }
	inline void set_moveCap_25(double value)
	{
		___moveCap_25 = value;
	}

	inline static int32_t get_offset_of_runCap_26() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___runCap_26)); }
	inline double get_runCap_26() const { return ___runCap_26; }
	inline double* get_address_of_runCap_26() { return &___runCap_26; }
	inline void set_runCap_26(double value)
	{
		___runCap_26 = value;
	}

	inline static int32_t get_offset_of_mobileControl_27() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___mobileControl_27)); }
	inline GameObject_t2557347079 * get_mobileControl_27() const { return ___mobileControl_27; }
	inline GameObject_t2557347079 ** get_address_of_mobileControl_27() { return &___mobileControl_27; }
	inline void set_mobileControl_27(GameObject_t2557347079 * value)
	{
		___mobileControl_27 = value;
		Il2CppCodeGenWriteBarrier((&___mobileControl_27), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_28() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_ObjectMobileController_28)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_28() const { return ___m_ObjectMobileController_28; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_28() { return &___m_ObjectMobileController_28; }
	inline void set_m_ObjectMobileController_28(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_28), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerMove_29() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_ObjectMobileControllerMove_29)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerMove_29() const { return ___m_ObjectMobileControllerMove_29; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerMove_29() { return &___m_ObjectMobileControllerMove_29; }
	inline void set_m_ObjectMobileControllerMove_29(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerMove_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerMove_29), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerSprint_30() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_ObjectMobileControllerSprint_30)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerSprint_30() const { return ___m_ObjectMobileControllerSprint_30; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerSprint_30() { return &___m_ObjectMobileControllerSprint_30; }
	inline void set_m_ObjectMobileControllerSprint_30(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerSprint_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerSprint_30), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_LeftPosition_31() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_ObjectMobileController_LeftPosition_31)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_LeftPosition_31() const { return ___m_ObjectMobileController_LeftPosition_31; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_LeftPosition_31() { return &___m_ObjectMobileController_LeftPosition_31; }
	inline void set_m_ObjectMobileController_LeftPosition_31(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_LeftPosition_31 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_RightPosition_32() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___m_ObjectMobileController_RightPosition_32)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_RightPosition_32() const { return ___m_ObjectMobileController_RightPosition_32; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_RightPosition_32() { return &___m_ObjectMobileController_RightPosition_32; }
	inline void set_m_ObjectMobileController_RightPosition_32(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_RightPosition_32 = value;
	}

	inline static int32_t get_offset_of_slideMove_33() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___slideMove_33)); }
	inline Vector3_t1986933152  get_slideMove_33() const { return ___slideMove_33; }
	inline Vector3_t1986933152 * get_address_of_slideMove_33() { return &___slideMove_33; }
	inline void set_slideMove_33(Vector3_t1986933152  value)
	{
		___slideMove_33 = value;
	}

	inline static int32_t get_offset_of_scriptsCanStopMovement_34() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___scriptsCanStopMovement_34)); }
	inline bool get_scriptsCanStopMovement_34() const { return ___scriptsCanStopMovement_34; }
	inline bool* get_address_of_scriptsCanStopMovement_34() { return &___scriptsCanStopMovement_34; }
	inline void set_scriptsCanStopMovement_34(bool value)
	{
		___scriptsCanStopMovement_34 = value;
	}

	inline static int32_t get_offset_of_moveVector_35() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520, ___moveVector_35)); }
	inline Vector3_t1986933152  get_moveVector_35() const { return ___moveVector_35; }
	inline Vector3_t1986933152 * get_address_of_moveVector_35() { return &___moveVector_35; }
	inline void set_moveVector_35(Vector3_t1986933152  value)
	{
		___moveVector_35 = value;
	}
};

struct PlayerMovement_t3557127520_StaticFields
{
public:
	// UnityEngine.GameObject PlayerMovement::bagGlowMat
	GameObject_t2557347079 * ___bagGlowMat_19;
	// UnityEngine.Material[] PlayerMovement::allMats
	MaterialU5BU5D_t2146020731* ___allMats_21;

public:
	inline static int32_t get_offset_of_bagGlowMat_19() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520_StaticFields, ___bagGlowMat_19)); }
	inline GameObject_t2557347079 * get_bagGlowMat_19() const { return ___bagGlowMat_19; }
	inline GameObject_t2557347079 ** get_address_of_bagGlowMat_19() { return &___bagGlowMat_19; }
	inline void set_bagGlowMat_19(GameObject_t2557347079 * value)
	{
		___bagGlowMat_19 = value;
		Il2CppCodeGenWriteBarrier((&___bagGlowMat_19), value);
	}

	inline static int32_t get_offset_of_allMats_21() { return static_cast<int32_t>(offsetof(PlayerMovement_t3557127520_StaticFields, ___allMats_21)); }
	inline MaterialU5BU5D_t2146020731* get_allMats_21() const { return ___allMats_21; }
	inline MaterialU5BU5D_t2146020731** get_address_of_allMats_21() { return &___allMats_21; }
	inline void set_allMats_21(MaterialU5BU5D_t2146020731* value)
	{
		___allMats_21 = value;
		Il2CppCodeGenWriteBarrier((&___allMats_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEMENT_T3557127520_H
#ifndef FONTSIZEMANAGER_T3303031200_H
#define FONTSIZEMANAGER_T3303031200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FontSizeManager
struct  FontSizeManager_t3303031200  : public MonoBehaviour_t1618594486
{
public:

public:
};

struct FontSizeManager_t3303031200_StaticFields
{
public:
	// System.Boolean FontSizeManager::done
	bool ___done_2;

public:
	inline static int32_t get_offset_of_done_2() { return static_cast<int32_t>(offsetof(FontSizeManager_t3303031200_StaticFields, ___done_2)); }
	inline bool get_done_2() const { return ___done_2; }
	inline bool* get_address_of_done_2() { return &___done_2; }
	inline void set_done_2(bool value)
	{
		___done_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSIZEMANAGER_T3303031200_H
#ifndef PHRASESCRIPT_T112947168_H
#define PHRASESCRIPT_T112947168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhraseScript
struct  PhraseScript_t112947168  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32[] PhraseScript::CorrectLetterIndex
	Int32U5BU5D_t1965588061* ___CorrectLetterIndex_2;
	// System.Boolean[] PhraseScript::LettersCovered
	BooleanU5BU5D_t698278498* ___LettersCovered_3;
	// System.Boolean PhraseScript::m_bClear
	bool ___m_bClear_4;

public:
	inline static int32_t get_offset_of_CorrectLetterIndex_2() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___CorrectLetterIndex_2)); }
	inline Int32U5BU5D_t1965588061* get_CorrectLetterIndex_2() const { return ___CorrectLetterIndex_2; }
	inline Int32U5BU5D_t1965588061** get_address_of_CorrectLetterIndex_2() { return &___CorrectLetterIndex_2; }
	inline void set_CorrectLetterIndex_2(Int32U5BU5D_t1965588061* value)
	{
		___CorrectLetterIndex_2 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectLetterIndex_2), value);
	}

	inline static int32_t get_offset_of_LettersCovered_3() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___LettersCovered_3)); }
	inline BooleanU5BU5D_t698278498* get_LettersCovered_3() const { return ___LettersCovered_3; }
	inline BooleanU5BU5D_t698278498** get_address_of_LettersCovered_3() { return &___LettersCovered_3; }
	inline void set_LettersCovered_3(BooleanU5BU5D_t698278498* value)
	{
		___LettersCovered_3 = value;
		Il2CppCodeGenWriteBarrier((&___LettersCovered_3), value);
	}

	inline static int32_t get_offset_of_m_bClear_4() { return static_cast<int32_t>(offsetof(PhraseScript_t112947168, ___m_bClear_4)); }
	inline bool get_m_bClear_4() const { return ___m_bClear_4; }
	inline bool* get_address_of_m_bClear_4() { return &___m_bClear_4; }
	inline void set_m_bClear_4(bool value)
	{
		___m_bClear_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHRASESCRIPT_T112947168_H
#ifndef JIGSAWPUZZLE_T1096861692_H
#define JIGSAWPUZZLE_T1096861692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JigSawPuzzle
struct  JigSawPuzzle_t1096861692  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector2[] JigSawPuzzle::StartingPositions
	Vector2U5BU5D_t1220531434* ___StartingPositions_2;
	// UnityEngine.Vector2[] JigSawPuzzle::CorrectPosition
	Vector2U5BU5D_t1220531434* ___CorrectPosition_3;
	// System.Boolean JigSawPuzzle::m_bAnimatePuzzleClear
	bool ___m_bAnimatePuzzleClear_4;
	// System.Boolean JigSawPuzzle::m_bCheckPuzzle
	bool ___m_bCheckPuzzle_5;
	// System.Boolean JigSawPuzzle::bSolved
	bool ___bSolved_6;
	// System.Int32 JigSawPuzzle::NumOfJigSawPuzzle
	int32_t ___NumOfJigSawPuzzle_7;
	// UnityEngine.Texture2D JigSawPuzzle::m_5jigsawbg2
	Texture2D_t3063074017 * ___m_5jigsawbg2_8;
	// UnityEngine.Texture2D JigSawPuzzle::m_5jigsawbg
	Texture2D_t3063074017 * ___m_5jigsawbg_9;
	// System.Boolean JigSawPuzzle::m_bAnimateRed
	bool ___m_bAnimateRed_10;
	// System.Single JigSawPuzzle::m_fDelays
	float ___m_fDelays_11;
	// System.Single JigSawPuzzle::m_fDelayElapsed
	float ___m_fDelayElapsed_12;
	// System.Int32 JigSawPuzzle::NumOfChanges
	int32_t ___NumOfChanges_13;

public:
	inline static int32_t get_offset_of_StartingPositions_2() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___StartingPositions_2)); }
	inline Vector2U5BU5D_t1220531434* get_StartingPositions_2() const { return ___StartingPositions_2; }
	inline Vector2U5BU5D_t1220531434** get_address_of_StartingPositions_2() { return &___StartingPositions_2; }
	inline void set_StartingPositions_2(Vector2U5BU5D_t1220531434* value)
	{
		___StartingPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartingPositions_2), value);
	}

	inline static int32_t get_offset_of_CorrectPosition_3() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___CorrectPosition_3)); }
	inline Vector2U5BU5D_t1220531434* get_CorrectPosition_3() const { return ___CorrectPosition_3; }
	inline Vector2U5BU5D_t1220531434** get_address_of_CorrectPosition_3() { return &___CorrectPosition_3; }
	inline void set_CorrectPosition_3(Vector2U5BU5D_t1220531434* value)
	{
		___CorrectPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectPosition_3), value);
	}

	inline static int32_t get_offset_of_m_bAnimatePuzzleClear_4() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_bAnimatePuzzleClear_4)); }
	inline bool get_m_bAnimatePuzzleClear_4() const { return ___m_bAnimatePuzzleClear_4; }
	inline bool* get_address_of_m_bAnimatePuzzleClear_4() { return &___m_bAnimatePuzzleClear_4; }
	inline void set_m_bAnimatePuzzleClear_4(bool value)
	{
		___m_bAnimatePuzzleClear_4 = value;
	}

	inline static int32_t get_offset_of_m_bCheckPuzzle_5() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_bCheckPuzzle_5)); }
	inline bool get_m_bCheckPuzzle_5() const { return ___m_bCheckPuzzle_5; }
	inline bool* get_address_of_m_bCheckPuzzle_5() { return &___m_bCheckPuzzle_5; }
	inline void set_m_bCheckPuzzle_5(bool value)
	{
		___m_bCheckPuzzle_5 = value;
	}

	inline static int32_t get_offset_of_bSolved_6() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___bSolved_6)); }
	inline bool get_bSolved_6() const { return ___bSolved_6; }
	inline bool* get_address_of_bSolved_6() { return &___bSolved_6; }
	inline void set_bSolved_6(bool value)
	{
		___bSolved_6 = value;
	}

	inline static int32_t get_offset_of_NumOfJigSawPuzzle_7() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___NumOfJigSawPuzzle_7)); }
	inline int32_t get_NumOfJigSawPuzzle_7() const { return ___NumOfJigSawPuzzle_7; }
	inline int32_t* get_address_of_NumOfJigSawPuzzle_7() { return &___NumOfJigSawPuzzle_7; }
	inline void set_NumOfJigSawPuzzle_7(int32_t value)
	{
		___NumOfJigSawPuzzle_7 = value;
	}

	inline static int32_t get_offset_of_m_5jigsawbg2_8() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_5jigsawbg2_8)); }
	inline Texture2D_t3063074017 * get_m_5jigsawbg2_8() const { return ___m_5jigsawbg2_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_5jigsawbg2_8() { return &___m_5jigsawbg2_8; }
	inline void set_m_5jigsawbg2_8(Texture2D_t3063074017 * value)
	{
		___m_5jigsawbg2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_5jigsawbg2_8), value);
	}

	inline static int32_t get_offset_of_m_5jigsawbg_9() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_5jigsawbg_9)); }
	inline Texture2D_t3063074017 * get_m_5jigsawbg_9() const { return ___m_5jigsawbg_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_5jigsawbg_9() { return &___m_5jigsawbg_9; }
	inline void set_m_5jigsawbg_9(Texture2D_t3063074017 * value)
	{
		___m_5jigsawbg_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_5jigsawbg_9), value);
	}

	inline static int32_t get_offset_of_m_bAnimateRed_10() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_bAnimateRed_10)); }
	inline bool get_m_bAnimateRed_10() const { return ___m_bAnimateRed_10; }
	inline bool* get_address_of_m_bAnimateRed_10() { return &___m_bAnimateRed_10; }
	inline void set_m_bAnimateRed_10(bool value)
	{
		___m_bAnimateRed_10 = value;
	}

	inline static int32_t get_offset_of_m_fDelays_11() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_fDelays_11)); }
	inline float get_m_fDelays_11() const { return ___m_fDelays_11; }
	inline float* get_address_of_m_fDelays_11() { return &___m_fDelays_11; }
	inline void set_m_fDelays_11(float value)
	{
		___m_fDelays_11 = value;
	}

	inline static int32_t get_offset_of_m_fDelayElapsed_12() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___m_fDelayElapsed_12)); }
	inline float get_m_fDelayElapsed_12() const { return ___m_fDelayElapsed_12; }
	inline float* get_address_of_m_fDelayElapsed_12() { return &___m_fDelayElapsed_12; }
	inline void set_m_fDelayElapsed_12(float value)
	{
		___m_fDelayElapsed_12 = value;
	}

	inline static int32_t get_offset_of_NumOfChanges_13() { return static_cast<int32_t>(offsetof(JigSawPuzzle_t1096861692, ___NumOfChanges_13)); }
	inline int32_t get_NumOfChanges_13() const { return ___NumOfChanges_13; }
	inline int32_t* get_address_of_NumOfChanges_13() { return &___NumOfChanges_13; }
	inline void set_NumOfChanges_13(int32_t value)
	{
		___NumOfChanges_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JIGSAWPUZZLE_T1096861692_H
#ifndef BARRELSPARX_T1842418686_H
#define BARRELSPARX_T1842418686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelSparx
struct  BarrelSparx_t1842418686  : public MonoBehaviour_t1618594486
{
public:
	// System.String BarrelSparx::Line
	String_t* ___Line_2;
	// System.Single BarrelSparx::StartTime
	float ___StartTime_3;
	// System.Single BarrelSparx::EndTime
	float ___EndTime_4;

public:
	inline static int32_t get_offset_of_Line_2() { return static_cast<int32_t>(offsetof(BarrelSparx_t1842418686, ___Line_2)); }
	inline String_t* get_Line_2() const { return ___Line_2; }
	inline String_t** get_address_of_Line_2() { return &___Line_2; }
	inline void set_Line_2(String_t* value)
	{
		___Line_2 = value;
		Il2CppCodeGenWriteBarrier((&___Line_2), value);
	}

	inline static int32_t get_offset_of_StartTime_3() { return static_cast<int32_t>(offsetof(BarrelSparx_t1842418686, ___StartTime_3)); }
	inline float get_StartTime_3() const { return ___StartTime_3; }
	inline float* get_address_of_StartTime_3() { return &___StartTime_3; }
	inline void set_StartTime_3(float value)
	{
		___StartTime_3 = value;
	}

	inline static int32_t get_offset_of_EndTime_4() { return static_cast<int32_t>(offsetof(BarrelSparx_t1842418686, ___EndTime_4)); }
	inline float get_EndTime_4() const { return ___EndTime_4; }
	inline float* get_address_of_EndTime_4() { return &___EndTime_4; }
	inline void set_EndTime_4(float value)
	{
		___EndTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARRELSPARX_T1842418686_H
#ifndef BARRELGNAT_T3244074211_H
#define BARRELGNAT_T3244074211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelGnat
struct  BarrelGnat_t3244074211  : public MonoBehaviour_t1618594486
{
public:
	// System.String BarrelGnat::GnatWords
	String_t* ___GnatWords_2;
	// System.Single BarrelGnat::StartTime
	float ___StartTime_3;
	// System.Single BarrelGnat::EndTime
	float ___EndTime_4;
	// UnityEngine.Transform BarrelGnat::CorrectBarrel
	Transform_t362059596 * ___CorrectBarrel_5;
	// UnityEngine.Vector3 BarrelGnat::CenterPos
	Vector3_t1986933152  ___CenterPos_6;
	// UnityEngine.Vector3 BarrelGnat::FlyAwayPos
	Vector3_t1986933152  ___FlyAwayPos_7;
	// BarrelGnatStatus BarrelGnat::Status
	int32_t ___Status_8;
	// UnityEngine.AudioSource BarrelGnat::m_VoiceAS
	AudioSource_t4025721661 * ___m_VoiceAS_9;

public:
	inline static int32_t get_offset_of_GnatWords_2() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___GnatWords_2)); }
	inline String_t* get_GnatWords_2() const { return ___GnatWords_2; }
	inline String_t** get_address_of_GnatWords_2() { return &___GnatWords_2; }
	inline void set_GnatWords_2(String_t* value)
	{
		___GnatWords_2 = value;
		Il2CppCodeGenWriteBarrier((&___GnatWords_2), value);
	}

	inline static int32_t get_offset_of_StartTime_3() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___StartTime_3)); }
	inline float get_StartTime_3() const { return ___StartTime_3; }
	inline float* get_address_of_StartTime_3() { return &___StartTime_3; }
	inline void set_StartTime_3(float value)
	{
		___StartTime_3 = value;
	}

	inline static int32_t get_offset_of_EndTime_4() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___EndTime_4)); }
	inline float get_EndTime_4() const { return ___EndTime_4; }
	inline float* get_address_of_EndTime_4() { return &___EndTime_4; }
	inline void set_EndTime_4(float value)
	{
		___EndTime_4 = value;
	}

	inline static int32_t get_offset_of_CorrectBarrel_5() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___CorrectBarrel_5)); }
	inline Transform_t362059596 * get_CorrectBarrel_5() const { return ___CorrectBarrel_5; }
	inline Transform_t362059596 ** get_address_of_CorrectBarrel_5() { return &___CorrectBarrel_5; }
	inline void set_CorrectBarrel_5(Transform_t362059596 * value)
	{
		___CorrectBarrel_5 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectBarrel_5), value);
	}

	inline static int32_t get_offset_of_CenterPos_6() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___CenterPos_6)); }
	inline Vector3_t1986933152  get_CenterPos_6() const { return ___CenterPos_6; }
	inline Vector3_t1986933152 * get_address_of_CenterPos_6() { return &___CenterPos_6; }
	inline void set_CenterPos_6(Vector3_t1986933152  value)
	{
		___CenterPos_6 = value;
	}

	inline static int32_t get_offset_of_FlyAwayPos_7() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___FlyAwayPos_7)); }
	inline Vector3_t1986933152  get_FlyAwayPos_7() const { return ___FlyAwayPos_7; }
	inline Vector3_t1986933152 * get_address_of_FlyAwayPos_7() { return &___FlyAwayPos_7; }
	inline void set_FlyAwayPos_7(Vector3_t1986933152  value)
	{
		___FlyAwayPos_7 = value;
	}

	inline static int32_t get_offset_of_Status_8() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___Status_8)); }
	inline int32_t get_Status_8() const { return ___Status_8; }
	inline int32_t* get_address_of_Status_8() { return &___Status_8; }
	inline void set_Status_8(int32_t value)
	{
		___Status_8 = value;
	}

	inline static int32_t get_offset_of_m_VoiceAS_9() { return static_cast<int32_t>(offsetof(BarrelGnat_t3244074211, ___m_VoiceAS_9)); }
	inline AudioSource_t4025721661 * get_m_VoiceAS_9() const { return ___m_VoiceAS_9; }
	inline AudioSource_t4025721661 ** get_address_of_m_VoiceAS_9() { return &___m_VoiceAS_9; }
	inline void set_m_VoiceAS_9(AudioSource_t4025721661 * value)
	{
		___m_VoiceAS_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_VoiceAS_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARRELGNAT_T3244074211_H
#ifndef BARRELGAME_T565740629_H
#define BARRELGAME_T565740629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelGame
struct  BarrelGame_t565740629  : public MonoBehaviour_t1618594486
{
public:
	// OutlineObject[] BarrelGame::outlines
	OutlineObjectU5BU5D_t3517943080* ___outlines_2;
	// UnityEngine.GameObject[] BarrelGame::Gnats
	GameObjectU5BU5D_t2988620542* ___Gnats_3;
	// UnityEngine.GameObject[] BarrelGame::Sparx
	GameObjectU5BU5D_t2988620542* ___Sparx_4;
	// BarrelStatus BarrelGame::Status
	int32_t ___Status_5;
	// System.Int32 BarrelGame::iCurrentPair
	int32_t ___iCurrentPair_6;
	// System.String BarrelGame::BarrelName
	String_t* ___BarrelName_7;
	// System.Boolean BarrelGame::m_bEnd
	bool ___m_bEnd_8;
	// System.Boolean BarrelGame::m_bCanPlay
	bool ___m_bCanPlay_9;
	// System.Boolean BarrelGame::CorrectBarrel
	bool ___CorrectBarrel_10;
	// UnityEngine.Transform BarrelGame::LeftPos
	Transform_t362059596 * ___LeftPos_11;
	// UnityEngine.Transform BarrelGame::RightPos
	Transform_t362059596 * ___RightPos_12;
	// UnityEngine.Transform BarrelGame::CenterPos
	Transform_t362059596 * ___CenterPos_13;
	// UnityEngine.Transform BarrelGame::FlyFrom
	Transform_t362059596 * ___FlyFrom_14;
	// System.Boolean BarrelGame::m_bGameStart
	bool ___m_bGameStart_15;
	// UnityEngine.GameObject[] BarrelGame::Platforms
	GameObjectU5BU5D_t2988620542* ___Platforms_16;
	// UnityEngine.GameObject BarrelGame::CurrentObject
	GameObject_t2557347079 * ___CurrentObject_17;
	// UnityEngine.GameObject BarrelGame::explosionEffect
	GameObject_t2557347079 * ___explosionEffect_18;
	// System.Boolean BarrelGame::m_bTuiConversationStarts
	bool ___m_bTuiConversationStarts_19;
	// System.String BarrelGame::PlayerLookat
	String_t* ___PlayerLookat_20;
	// UnityEngine.Vector3 BarrelGame::start
	Vector3_t1986933152  ___start_21;
	// System.Single BarrelGame::m_fTime
	float ___m_fTime_22;

public:
	inline static int32_t get_offset_of_outlines_2() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___outlines_2)); }
	inline OutlineObjectU5BU5D_t3517943080* get_outlines_2() const { return ___outlines_2; }
	inline OutlineObjectU5BU5D_t3517943080** get_address_of_outlines_2() { return &___outlines_2; }
	inline void set_outlines_2(OutlineObjectU5BU5D_t3517943080* value)
	{
		___outlines_2 = value;
		Il2CppCodeGenWriteBarrier((&___outlines_2), value);
	}

	inline static int32_t get_offset_of_Gnats_3() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___Gnats_3)); }
	inline GameObjectU5BU5D_t2988620542* get_Gnats_3() const { return ___Gnats_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Gnats_3() { return &___Gnats_3; }
	inline void set_Gnats_3(GameObjectU5BU5D_t2988620542* value)
	{
		___Gnats_3 = value;
		Il2CppCodeGenWriteBarrier((&___Gnats_3), value);
	}

	inline static int32_t get_offset_of_Sparx_4() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___Sparx_4)); }
	inline GameObjectU5BU5D_t2988620542* get_Sparx_4() const { return ___Sparx_4; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Sparx_4() { return &___Sparx_4; }
	inline void set_Sparx_4(GameObjectU5BU5D_t2988620542* value)
	{
		___Sparx_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sparx_4), value);
	}

	inline static int32_t get_offset_of_Status_5() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___Status_5)); }
	inline int32_t get_Status_5() const { return ___Status_5; }
	inline int32_t* get_address_of_Status_5() { return &___Status_5; }
	inline void set_Status_5(int32_t value)
	{
		___Status_5 = value;
	}

	inline static int32_t get_offset_of_iCurrentPair_6() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___iCurrentPair_6)); }
	inline int32_t get_iCurrentPair_6() const { return ___iCurrentPair_6; }
	inline int32_t* get_address_of_iCurrentPair_6() { return &___iCurrentPair_6; }
	inline void set_iCurrentPair_6(int32_t value)
	{
		___iCurrentPair_6 = value;
	}

	inline static int32_t get_offset_of_BarrelName_7() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___BarrelName_7)); }
	inline String_t* get_BarrelName_7() const { return ___BarrelName_7; }
	inline String_t** get_address_of_BarrelName_7() { return &___BarrelName_7; }
	inline void set_BarrelName_7(String_t* value)
	{
		___BarrelName_7 = value;
		Il2CppCodeGenWriteBarrier((&___BarrelName_7), value);
	}

	inline static int32_t get_offset_of_m_bEnd_8() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___m_bEnd_8)); }
	inline bool get_m_bEnd_8() const { return ___m_bEnd_8; }
	inline bool* get_address_of_m_bEnd_8() { return &___m_bEnd_8; }
	inline void set_m_bEnd_8(bool value)
	{
		___m_bEnd_8 = value;
	}

	inline static int32_t get_offset_of_m_bCanPlay_9() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___m_bCanPlay_9)); }
	inline bool get_m_bCanPlay_9() const { return ___m_bCanPlay_9; }
	inline bool* get_address_of_m_bCanPlay_9() { return &___m_bCanPlay_9; }
	inline void set_m_bCanPlay_9(bool value)
	{
		___m_bCanPlay_9 = value;
	}

	inline static int32_t get_offset_of_CorrectBarrel_10() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___CorrectBarrel_10)); }
	inline bool get_CorrectBarrel_10() const { return ___CorrectBarrel_10; }
	inline bool* get_address_of_CorrectBarrel_10() { return &___CorrectBarrel_10; }
	inline void set_CorrectBarrel_10(bool value)
	{
		___CorrectBarrel_10 = value;
	}

	inline static int32_t get_offset_of_LeftPos_11() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___LeftPos_11)); }
	inline Transform_t362059596 * get_LeftPos_11() const { return ___LeftPos_11; }
	inline Transform_t362059596 ** get_address_of_LeftPos_11() { return &___LeftPos_11; }
	inline void set_LeftPos_11(Transform_t362059596 * value)
	{
		___LeftPos_11 = value;
		Il2CppCodeGenWriteBarrier((&___LeftPos_11), value);
	}

	inline static int32_t get_offset_of_RightPos_12() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___RightPos_12)); }
	inline Transform_t362059596 * get_RightPos_12() const { return ___RightPos_12; }
	inline Transform_t362059596 ** get_address_of_RightPos_12() { return &___RightPos_12; }
	inline void set_RightPos_12(Transform_t362059596 * value)
	{
		___RightPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___RightPos_12), value);
	}

	inline static int32_t get_offset_of_CenterPos_13() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___CenterPos_13)); }
	inline Transform_t362059596 * get_CenterPos_13() const { return ___CenterPos_13; }
	inline Transform_t362059596 ** get_address_of_CenterPos_13() { return &___CenterPos_13; }
	inline void set_CenterPos_13(Transform_t362059596 * value)
	{
		___CenterPos_13 = value;
		Il2CppCodeGenWriteBarrier((&___CenterPos_13), value);
	}

	inline static int32_t get_offset_of_FlyFrom_14() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___FlyFrom_14)); }
	inline Transform_t362059596 * get_FlyFrom_14() const { return ___FlyFrom_14; }
	inline Transform_t362059596 ** get_address_of_FlyFrom_14() { return &___FlyFrom_14; }
	inline void set_FlyFrom_14(Transform_t362059596 * value)
	{
		___FlyFrom_14 = value;
		Il2CppCodeGenWriteBarrier((&___FlyFrom_14), value);
	}

	inline static int32_t get_offset_of_m_bGameStart_15() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___m_bGameStart_15)); }
	inline bool get_m_bGameStart_15() const { return ___m_bGameStart_15; }
	inline bool* get_address_of_m_bGameStart_15() { return &___m_bGameStart_15; }
	inline void set_m_bGameStart_15(bool value)
	{
		___m_bGameStart_15 = value;
	}

	inline static int32_t get_offset_of_Platforms_16() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___Platforms_16)); }
	inline GameObjectU5BU5D_t2988620542* get_Platforms_16() const { return ___Platforms_16; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Platforms_16() { return &___Platforms_16; }
	inline void set_Platforms_16(GameObjectU5BU5D_t2988620542* value)
	{
		___Platforms_16 = value;
		Il2CppCodeGenWriteBarrier((&___Platforms_16), value);
	}

	inline static int32_t get_offset_of_CurrentObject_17() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___CurrentObject_17)); }
	inline GameObject_t2557347079 * get_CurrentObject_17() const { return ___CurrentObject_17; }
	inline GameObject_t2557347079 ** get_address_of_CurrentObject_17() { return &___CurrentObject_17; }
	inline void set_CurrentObject_17(GameObject_t2557347079 * value)
	{
		___CurrentObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentObject_17), value);
	}

	inline static int32_t get_offset_of_explosionEffect_18() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___explosionEffect_18)); }
	inline GameObject_t2557347079 * get_explosionEffect_18() const { return ___explosionEffect_18; }
	inline GameObject_t2557347079 ** get_address_of_explosionEffect_18() { return &___explosionEffect_18; }
	inline void set_explosionEffect_18(GameObject_t2557347079 * value)
	{
		___explosionEffect_18 = value;
		Il2CppCodeGenWriteBarrier((&___explosionEffect_18), value);
	}

	inline static int32_t get_offset_of_m_bTuiConversationStarts_19() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___m_bTuiConversationStarts_19)); }
	inline bool get_m_bTuiConversationStarts_19() const { return ___m_bTuiConversationStarts_19; }
	inline bool* get_address_of_m_bTuiConversationStarts_19() { return &___m_bTuiConversationStarts_19; }
	inline void set_m_bTuiConversationStarts_19(bool value)
	{
		___m_bTuiConversationStarts_19 = value;
	}

	inline static int32_t get_offset_of_PlayerLookat_20() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___PlayerLookat_20)); }
	inline String_t* get_PlayerLookat_20() const { return ___PlayerLookat_20; }
	inline String_t** get_address_of_PlayerLookat_20() { return &___PlayerLookat_20; }
	inline void set_PlayerLookat_20(String_t* value)
	{
		___PlayerLookat_20 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerLookat_20), value);
	}

	inline static int32_t get_offset_of_start_21() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___start_21)); }
	inline Vector3_t1986933152  get_start_21() const { return ___start_21; }
	inline Vector3_t1986933152 * get_address_of_start_21() { return &___start_21; }
	inline void set_start_21(Vector3_t1986933152  value)
	{
		___start_21 = value;
	}

	inline static int32_t get_offset_of_m_fTime_22() { return static_cast<int32_t>(offsetof(BarrelGame_t565740629, ___m_fTime_22)); }
	inline float get_m_fTime_22() const { return ___m_fTime_22; }
	inline float* get_address_of_m_fTime_22() { return &___m_fTime_22; }
	inline void set_m_fTime_22(float value)
	{
		___m_fTime_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARRELGAME_T565740629_H
#ifndef REGISTRATION_T2162293902_H
#define REGISTRATION_T2162293902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Registration
struct  Registration_t2162293902  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.UI.Dropdown Registration::DropWho
	Dropdown_t3945702362 * ___DropWho_2;
	// UnityEngine.GameObject Registration::UserPanel
	GameObject_t2557347079 * ___UserPanel_3;
	// UnityEngine.GameObject Registration::YoungPersonPanel
	GameObject_t2557347079 * ___YoungPersonPanel_4;
	// UnityEngine.GameObject Registration::GenderPanel
	GameObject_t2557347079 * ___GenderPanel_5;
	// UnityEngine.GameObject Registration::EmailPanel
	GameObject_t2557347079 * ___EmailPanel_6;
	// UnityEngine.GameObject Registration::LivePanel
	GameObject_t2557347079 * ___LivePanel_7;
	// UnityEngine.GameObject Registration::ContinueButton
	GameObject_t2557347079 * ___ContinueButton_8;
	// UnityEngine.GameObject Registration::BackButton
	GameObject_t2557347079 * ___BackButton_9;
	// UnityEngine.GameObject Registration::OtherEthnicToggle
	GameObject_t2557347079 * ___OtherEthnicToggle_10;
	// UnityEngine.UI.Dropdown Registration::FindDropdown
	Dropdown_t3945702362 * ___FindDropdown_11;
	// UnityEngine.GameObject Registration::OtherWho
	GameObject_t2557347079 * ___OtherWho_12;
	// UnityEngine.GameObject Registration::OtherEthnic
	GameObject_t2557347079 * ___OtherEthnic_13;
	// UnityEngine.GameObject Registration::OtherFind
	GameObject_t2557347079 * ___OtherFind_14;
	// UnityEngine.GameObject Registration::OtherFindLabel
	GameObject_t2557347079 * ___OtherFindLabel_15;
	// UnityEngine.UI.InputField Registration::Field_UserUserName
	InputField_t3123460221 * ___Field_UserUserName_16;
	// UnityEngine.UI.Dropdown Registration::Field_Who
	Dropdown_t3945702362 * ___Field_Who_17;
	// UnityEngine.UI.InputField Registration::Field_WhoOtro
	InputField_t3123460221 * ___Field_WhoOtro_18;
	// UnityEngine.UI.InputField Registration::Field_Old
	InputField_t3123460221 * ___Field_Old_19;
	// UnityEngine.GameObject Registration::Field_NZ
	GameObject_t2557347079 * ___Field_NZ_20;
	// UnityEngine.GameObject Registration::Field_Maori
	GameObject_t2557347079 * ___Field_Maori_21;
	// UnityEngine.GameObject Registration::Field_Samoan
	GameObject_t2557347079 * ___Field_Samoan_22;
	// UnityEngine.GameObject Registration::Field_Cook
	GameObject_t2557347079 * ___Field_Cook_23;
	// UnityEngine.GameObject Registration::Field_Tongan
	GameObject_t2557347079 * ___Field_Tongan_24;
	// UnityEngine.GameObject Registration::Field_Niuean
	GameObject_t2557347079 * ___Field_Niuean_25;
	// UnityEngine.GameObject Registration::Field_Chinesse
	GameObject_t2557347079 * ___Field_Chinesse_26;
	// UnityEngine.GameObject Registration::Field_Indian
	GameObject_t2557347079 * ___Field_Indian_27;
	// UnityEngine.GameObject Registration::Field_Other
	GameObject_t2557347079 * ___Field_Other_28;
	// UnityEngine.UI.InputField Registration::Field_OtherTEXT
	InputField_t3123460221 * ___Field_OtherTEXT_29;
	// UnityEngine.UI.Dropdown Registration::Field_Gender
	Dropdown_t3945702362 * ___Field_Gender_30;
	// UnityEngine.UI.InputField Registration::Field_eMail
	InputField_t3123460221 * ___Field_eMail_31;
	// UnityEngine.UI.InputField Registration::Field_ReeMail
	InputField_t3123460221 * ___Field_ReeMail_32;
	// UnityEngine.GameObject Registration::Field_Information
	GameObject_t2557347079 * ___Field_Information_33;
	// UnityEngine.UI.InputField Registration::Field_Password
	InputField_t3123460221 * ___Field_Password_34;
	// UnityEngine.UI.InputField Registration::Field_RePassword
	InputField_t3123460221 * ___Field_RePassword_35;
	// UnityEngine.UI.Dropdown Registration::Field_Code
	Dropdown_t3945702362 * ___Field_Code_36;
	// UnityEngine.UI.InputField Registration::Field_Mobile
	InputField_t3123460221 * ___Field_Mobile_37;
	// UnityEngine.UI.Dropdown Registration::Field_Live
	Dropdown_t3945702362 * ___Field_Live_38;
	// UnityEngine.UI.Dropdown Registration::Field_Find
	Dropdown_t3945702362 * ___Field_Find_39;
	// UnityEngine.UI.InputField Registration::Field_SpecifyFind
	InputField_t3123460221 * ___Field_SpecifyFind_40;
	// UnityEngine.GameObject Registration::Field_Conact
	GameObject_t2557347079 * ___Field_Conact_41;
	// UnityEngine.GameObject Registration::Field_ExtraHelp
	GameObject_t2557347079 * ___Field_ExtraHelp_42;
	// UnityEngine.GameObject Registration::Field_Terms
	GameObject_t2557347079 * ___Field_Terms_43;
	// CapturedDataInput Registration::instance
	CapturedDataInput_t2616152122 * ___instance_44;
	// System.Boolean Registration::m_bGotRegistrationTokenDone
	bool ___m_bGotRegistrationTokenDone_45;
	// UnityEngine.GameObject Registration::LoginPanel
	GameObject_t2557347079 * ___LoginPanel_46;
	// UnityEngine.GameObject Registration::SignUpPanel
	GameObject_t2557347079 * ___SignUpPanel_47;
	// UnityEngine.UI.Text Registration::oTextRegisterNotificationMessage
	Text_t1790657652 * ___oTextRegisterNotificationMessage_48;
	// System.Boolean Registration::bSubmit
	bool ___bSubmit_49;
	// UnityEngine.Vector3 Registration::EmailPanelRectTransform
	Vector3_t1986933152  ___EmailPanelRectTransform_50;
	// UnityEngine.GameObject Registration::oNotifications
	GameObject_t2557347079 * ___oNotifications_51;

public:
	inline static int32_t get_offset_of_DropWho_2() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___DropWho_2)); }
	inline Dropdown_t3945702362 * get_DropWho_2() const { return ___DropWho_2; }
	inline Dropdown_t3945702362 ** get_address_of_DropWho_2() { return &___DropWho_2; }
	inline void set_DropWho_2(Dropdown_t3945702362 * value)
	{
		___DropWho_2 = value;
		Il2CppCodeGenWriteBarrier((&___DropWho_2), value);
	}

	inline static int32_t get_offset_of_UserPanel_3() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___UserPanel_3)); }
	inline GameObject_t2557347079 * get_UserPanel_3() const { return ___UserPanel_3; }
	inline GameObject_t2557347079 ** get_address_of_UserPanel_3() { return &___UserPanel_3; }
	inline void set_UserPanel_3(GameObject_t2557347079 * value)
	{
		___UserPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___UserPanel_3), value);
	}

	inline static int32_t get_offset_of_YoungPersonPanel_4() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___YoungPersonPanel_4)); }
	inline GameObject_t2557347079 * get_YoungPersonPanel_4() const { return ___YoungPersonPanel_4; }
	inline GameObject_t2557347079 ** get_address_of_YoungPersonPanel_4() { return &___YoungPersonPanel_4; }
	inline void set_YoungPersonPanel_4(GameObject_t2557347079 * value)
	{
		___YoungPersonPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___YoungPersonPanel_4), value);
	}

	inline static int32_t get_offset_of_GenderPanel_5() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___GenderPanel_5)); }
	inline GameObject_t2557347079 * get_GenderPanel_5() const { return ___GenderPanel_5; }
	inline GameObject_t2557347079 ** get_address_of_GenderPanel_5() { return &___GenderPanel_5; }
	inline void set_GenderPanel_5(GameObject_t2557347079 * value)
	{
		___GenderPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___GenderPanel_5), value);
	}

	inline static int32_t get_offset_of_EmailPanel_6() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___EmailPanel_6)); }
	inline GameObject_t2557347079 * get_EmailPanel_6() const { return ___EmailPanel_6; }
	inline GameObject_t2557347079 ** get_address_of_EmailPanel_6() { return &___EmailPanel_6; }
	inline void set_EmailPanel_6(GameObject_t2557347079 * value)
	{
		___EmailPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmailPanel_6), value);
	}

	inline static int32_t get_offset_of_LivePanel_7() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___LivePanel_7)); }
	inline GameObject_t2557347079 * get_LivePanel_7() const { return ___LivePanel_7; }
	inline GameObject_t2557347079 ** get_address_of_LivePanel_7() { return &___LivePanel_7; }
	inline void set_LivePanel_7(GameObject_t2557347079 * value)
	{
		___LivePanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___LivePanel_7), value);
	}

	inline static int32_t get_offset_of_ContinueButton_8() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___ContinueButton_8)); }
	inline GameObject_t2557347079 * get_ContinueButton_8() const { return ___ContinueButton_8; }
	inline GameObject_t2557347079 ** get_address_of_ContinueButton_8() { return &___ContinueButton_8; }
	inline void set_ContinueButton_8(GameObject_t2557347079 * value)
	{
		___ContinueButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___ContinueButton_8), value);
	}

	inline static int32_t get_offset_of_BackButton_9() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___BackButton_9)); }
	inline GameObject_t2557347079 * get_BackButton_9() const { return ___BackButton_9; }
	inline GameObject_t2557347079 ** get_address_of_BackButton_9() { return &___BackButton_9; }
	inline void set_BackButton_9(GameObject_t2557347079 * value)
	{
		___BackButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___BackButton_9), value);
	}

	inline static int32_t get_offset_of_OtherEthnicToggle_10() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___OtherEthnicToggle_10)); }
	inline GameObject_t2557347079 * get_OtherEthnicToggle_10() const { return ___OtherEthnicToggle_10; }
	inline GameObject_t2557347079 ** get_address_of_OtherEthnicToggle_10() { return &___OtherEthnicToggle_10; }
	inline void set_OtherEthnicToggle_10(GameObject_t2557347079 * value)
	{
		___OtherEthnicToggle_10 = value;
		Il2CppCodeGenWriteBarrier((&___OtherEthnicToggle_10), value);
	}

	inline static int32_t get_offset_of_FindDropdown_11() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___FindDropdown_11)); }
	inline Dropdown_t3945702362 * get_FindDropdown_11() const { return ___FindDropdown_11; }
	inline Dropdown_t3945702362 ** get_address_of_FindDropdown_11() { return &___FindDropdown_11; }
	inline void set_FindDropdown_11(Dropdown_t3945702362 * value)
	{
		___FindDropdown_11 = value;
		Il2CppCodeGenWriteBarrier((&___FindDropdown_11), value);
	}

	inline static int32_t get_offset_of_OtherWho_12() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___OtherWho_12)); }
	inline GameObject_t2557347079 * get_OtherWho_12() const { return ___OtherWho_12; }
	inline GameObject_t2557347079 ** get_address_of_OtherWho_12() { return &___OtherWho_12; }
	inline void set_OtherWho_12(GameObject_t2557347079 * value)
	{
		___OtherWho_12 = value;
		Il2CppCodeGenWriteBarrier((&___OtherWho_12), value);
	}

	inline static int32_t get_offset_of_OtherEthnic_13() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___OtherEthnic_13)); }
	inline GameObject_t2557347079 * get_OtherEthnic_13() const { return ___OtherEthnic_13; }
	inline GameObject_t2557347079 ** get_address_of_OtherEthnic_13() { return &___OtherEthnic_13; }
	inline void set_OtherEthnic_13(GameObject_t2557347079 * value)
	{
		___OtherEthnic_13 = value;
		Il2CppCodeGenWriteBarrier((&___OtherEthnic_13), value);
	}

	inline static int32_t get_offset_of_OtherFind_14() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___OtherFind_14)); }
	inline GameObject_t2557347079 * get_OtherFind_14() const { return ___OtherFind_14; }
	inline GameObject_t2557347079 ** get_address_of_OtherFind_14() { return &___OtherFind_14; }
	inline void set_OtherFind_14(GameObject_t2557347079 * value)
	{
		___OtherFind_14 = value;
		Il2CppCodeGenWriteBarrier((&___OtherFind_14), value);
	}

	inline static int32_t get_offset_of_OtherFindLabel_15() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___OtherFindLabel_15)); }
	inline GameObject_t2557347079 * get_OtherFindLabel_15() const { return ___OtherFindLabel_15; }
	inline GameObject_t2557347079 ** get_address_of_OtherFindLabel_15() { return &___OtherFindLabel_15; }
	inline void set_OtherFindLabel_15(GameObject_t2557347079 * value)
	{
		___OtherFindLabel_15 = value;
		Il2CppCodeGenWriteBarrier((&___OtherFindLabel_15), value);
	}

	inline static int32_t get_offset_of_Field_UserUserName_16() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_UserUserName_16)); }
	inline InputField_t3123460221 * get_Field_UserUserName_16() const { return ___Field_UserUserName_16; }
	inline InputField_t3123460221 ** get_address_of_Field_UserUserName_16() { return &___Field_UserUserName_16; }
	inline void set_Field_UserUserName_16(InputField_t3123460221 * value)
	{
		___Field_UserUserName_16 = value;
		Il2CppCodeGenWriteBarrier((&___Field_UserUserName_16), value);
	}

	inline static int32_t get_offset_of_Field_Who_17() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Who_17)); }
	inline Dropdown_t3945702362 * get_Field_Who_17() const { return ___Field_Who_17; }
	inline Dropdown_t3945702362 ** get_address_of_Field_Who_17() { return &___Field_Who_17; }
	inline void set_Field_Who_17(Dropdown_t3945702362 * value)
	{
		___Field_Who_17 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Who_17), value);
	}

	inline static int32_t get_offset_of_Field_WhoOtro_18() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_WhoOtro_18)); }
	inline InputField_t3123460221 * get_Field_WhoOtro_18() const { return ___Field_WhoOtro_18; }
	inline InputField_t3123460221 ** get_address_of_Field_WhoOtro_18() { return &___Field_WhoOtro_18; }
	inline void set_Field_WhoOtro_18(InputField_t3123460221 * value)
	{
		___Field_WhoOtro_18 = value;
		Il2CppCodeGenWriteBarrier((&___Field_WhoOtro_18), value);
	}

	inline static int32_t get_offset_of_Field_Old_19() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Old_19)); }
	inline InputField_t3123460221 * get_Field_Old_19() const { return ___Field_Old_19; }
	inline InputField_t3123460221 ** get_address_of_Field_Old_19() { return &___Field_Old_19; }
	inline void set_Field_Old_19(InputField_t3123460221 * value)
	{
		___Field_Old_19 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Old_19), value);
	}

	inline static int32_t get_offset_of_Field_NZ_20() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_NZ_20)); }
	inline GameObject_t2557347079 * get_Field_NZ_20() const { return ___Field_NZ_20; }
	inline GameObject_t2557347079 ** get_address_of_Field_NZ_20() { return &___Field_NZ_20; }
	inline void set_Field_NZ_20(GameObject_t2557347079 * value)
	{
		___Field_NZ_20 = value;
		Il2CppCodeGenWriteBarrier((&___Field_NZ_20), value);
	}

	inline static int32_t get_offset_of_Field_Maori_21() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Maori_21)); }
	inline GameObject_t2557347079 * get_Field_Maori_21() const { return ___Field_Maori_21; }
	inline GameObject_t2557347079 ** get_address_of_Field_Maori_21() { return &___Field_Maori_21; }
	inline void set_Field_Maori_21(GameObject_t2557347079 * value)
	{
		___Field_Maori_21 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Maori_21), value);
	}

	inline static int32_t get_offset_of_Field_Samoan_22() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Samoan_22)); }
	inline GameObject_t2557347079 * get_Field_Samoan_22() const { return ___Field_Samoan_22; }
	inline GameObject_t2557347079 ** get_address_of_Field_Samoan_22() { return &___Field_Samoan_22; }
	inline void set_Field_Samoan_22(GameObject_t2557347079 * value)
	{
		___Field_Samoan_22 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Samoan_22), value);
	}

	inline static int32_t get_offset_of_Field_Cook_23() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Cook_23)); }
	inline GameObject_t2557347079 * get_Field_Cook_23() const { return ___Field_Cook_23; }
	inline GameObject_t2557347079 ** get_address_of_Field_Cook_23() { return &___Field_Cook_23; }
	inline void set_Field_Cook_23(GameObject_t2557347079 * value)
	{
		___Field_Cook_23 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Cook_23), value);
	}

	inline static int32_t get_offset_of_Field_Tongan_24() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Tongan_24)); }
	inline GameObject_t2557347079 * get_Field_Tongan_24() const { return ___Field_Tongan_24; }
	inline GameObject_t2557347079 ** get_address_of_Field_Tongan_24() { return &___Field_Tongan_24; }
	inline void set_Field_Tongan_24(GameObject_t2557347079 * value)
	{
		___Field_Tongan_24 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Tongan_24), value);
	}

	inline static int32_t get_offset_of_Field_Niuean_25() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Niuean_25)); }
	inline GameObject_t2557347079 * get_Field_Niuean_25() const { return ___Field_Niuean_25; }
	inline GameObject_t2557347079 ** get_address_of_Field_Niuean_25() { return &___Field_Niuean_25; }
	inline void set_Field_Niuean_25(GameObject_t2557347079 * value)
	{
		___Field_Niuean_25 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Niuean_25), value);
	}

	inline static int32_t get_offset_of_Field_Chinesse_26() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Chinesse_26)); }
	inline GameObject_t2557347079 * get_Field_Chinesse_26() const { return ___Field_Chinesse_26; }
	inline GameObject_t2557347079 ** get_address_of_Field_Chinesse_26() { return &___Field_Chinesse_26; }
	inline void set_Field_Chinesse_26(GameObject_t2557347079 * value)
	{
		___Field_Chinesse_26 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Chinesse_26), value);
	}

	inline static int32_t get_offset_of_Field_Indian_27() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Indian_27)); }
	inline GameObject_t2557347079 * get_Field_Indian_27() const { return ___Field_Indian_27; }
	inline GameObject_t2557347079 ** get_address_of_Field_Indian_27() { return &___Field_Indian_27; }
	inline void set_Field_Indian_27(GameObject_t2557347079 * value)
	{
		___Field_Indian_27 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Indian_27), value);
	}

	inline static int32_t get_offset_of_Field_Other_28() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Other_28)); }
	inline GameObject_t2557347079 * get_Field_Other_28() const { return ___Field_Other_28; }
	inline GameObject_t2557347079 ** get_address_of_Field_Other_28() { return &___Field_Other_28; }
	inline void set_Field_Other_28(GameObject_t2557347079 * value)
	{
		___Field_Other_28 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Other_28), value);
	}

	inline static int32_t get_offset_of_Field_OtherTEXT_29() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_OtherTEXT_29)); }
	inline InputField_t3123460221 * get_Field_OtherTEXT_29() const { return ___Field_OtherTEXT_29; }
	inline InputField_t3123460221 ** get_address_of_Field_OtherTEXT_29() { return &___Field_OtherTEXT_29; }
	inline void set_Field_OtherTEXT_29(InputField_t3123460221 * value)
	{
		___Field_OtherTEXT_29 = value;
		Il2CppCodeGenWriteBarrier((&___Field_OtherTEXT_29), value);
	}

	inline static int32_t get_offset_of_Field_Gender_30() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Gender_30)); }
	inline Dropdown_t3945702362 * get_Field_Gender_30() const { return ___Field_Gender_30; }
	inline Dropdown_t3945702362 ** get_address_of_Field_Gender_30() { return &___Field_Gender_30; }
	inline void set_Field_Gender_30(Dropdown_t3945702362 * value)
	{
		___Field_Gender_30 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Gender_30), value);
	}

	inline static int32_t get_offset_of_Field_eMail_31() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_eMail_31)); }
	inline InputField_t3123460221 * get_Field_eMail_31() const { return ___Field_eMail_31; }
	inline InputField_t3123460221 ** get_address_of_Field_eMail_31() { return &___Field_eMail_31; }
	inline void set_Field_eMail_31(InputField_t3123460221 * value)
	{
		___Field_eMail_31 = value;
		Il2CppCodeGenWriteBarrier((&___Field_eMail_31), value);
	}

	inline static int32_t get_offset_of_Field_ReeMail_32() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_ReeMail_32)); }
	inline InputField_t3123460221 * get_Field_ReeMail_32() const { return ___Field_ReeMail_32; }
	inline InputField_t3123460221 ** get_address_of_Field_ReeMail_32() { return &___Field_ReeMail_32; }
	inline void set_Field_ReeMail_32(InputField_t3123460221 * value)
	{
		___Field_ReeMail_32 = value;
		Il2CppCodeGenWriteBarrier((&___Field_ReeMail_32), value);
	}

	inline static int32_t get_offset_of_Field_Information_33() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Information_33)); }
	inline GameObject_t2557347079 * get_Field_Information_33() const { return ___Field_Information_33; }
	inline GameObject_t2557347079 ** get_address_of_Field_Information_33() { return &___Field_Information_33; }
	inline void set_Field_Information_33(GameObject_t2557347079 * value)
	{
		___Field_Information_33 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Information_33), value);
	}

	inline static int32_t get_offset_of_Field_Password_34() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Password_34)); }
	inline InputField_t3123460221 * get_Field_Password_34() const { return ___Field_Password_34; }
	inline InputField_t3123460221 ** get_address_of_Field_Password_34() { return &___Field_Password_34; }
	inline void set_Field_Password_34(InputField_t3123460221 * value)
	{
		___Field_Password_34 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Password_34), value);
	}

	inline static int32_t get_offset_of_Field_RePassword_35() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_RePassword_35)); }
	inline InputField_t3123460221 * get_Field_RePassword_35() const { return ___Field_RePassword_35; }
	inline InputField_t3123460221 ** get_address_of_Field_RePassword_35() { return &___Field_RePassword_35; }
	inline void set_Field_RePassword_35(InputField_t3123460221 * value)
	{
		___Field_RePassword_35 = value;
		Il2CppCodeGenWriteBarrier((&___Field_RePassword_35), value);
	}

	inline static int32_t get_offset_of_Field_Code_36() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Code_36)); }
	inline Dropdown_t3945702362 * get_Field_Code_36() const { return ___Field_Code_36; }
	inline Dropdown_t3945702362 ** get_address_of_Field_Code_36() { return &___Field_Code_36; }
	inline void set_Field_Code_36(Dropdown_t3945702362 * value)
	{
		___Field_Code_36 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Code_36), value);
	}

	inline static int32_t get_offset_of_Field_Mobile_37() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Mobile_37)); }
	inline InputField_t3123460221 * get_Field_Mobile_37() const { return ___Field_Mobile_37; }
	inline InputField_t3123460221 ** get_address_of_Field_Mobile_37() { return &___Field_Mobile_37; }
	inline void set_Field_Mobile_37(InputField_t3123460221 * value)
	{
		___Field_Mobile_37 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Mobile_37), value);
	}

	inline static int32_t get_offset_of_Field_Live_38() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Live_38)); }
	inline Dropdown_t3945702362 * get_Field_Live_38() const { return ___Field_Live_38; }
	inline Dropdown_t3945702362 ** get_address_of_Field_Live_38() { return &___Field_Live_38; }
	inline void set_Field_Live_38(Dropdown_t3945702362 * value)
	{
		___Field_Live_38 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Live_38), value);
	}

	inline static int32_t get_offset_of_Field_Find_39() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Find_39)); }
	inline Dropdown_t3945702362 * get_Field_Find_39() const { return ___Field_Find_39; }
	inline Dropdown_t3945702362 ** get_address_of_Field_Find_39() { return &___Field_Find_39; }
	inline void set_Field_Find_39(Dropdown_t3945702362 * value)
	{
		___Field_Find_39 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Find_39), value);
	}

	inline static int32_t get_offset_of_Field_SpecifyFind_40() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_SpecifyFind_40)); }
	inline InputField_t3123460221 * get_Field_SpecifyFind_40() const { return ___Field_SpecifyFind_40; }
	inline InputField_t3123460221 ** get_address_of_Field_SpecifyFind_40() { return &___Field_SpecifyFind_40; }
	inline void set_Field_SpecifyFind_40(InputField_t3123460221 * value)
	{
		___Field_SpecifyFind_40 = value;
		Il2CppCodeGenWriteBarrier((&___Field_SpecifyFind_40), value);
	}

	inline static int32_t get_offset_of_Field_Conact_41() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Conact_41)); }
	inline GameObject_t2557347079 * get_Field_Conact_41() const { return ___Field_Conact_41; }
	inline GameObject_t2557347079 ** get_address_of_Field_Conact_41() { return &___Field_Conact_41; }
	inline void set_Field_Conact_41(GameObject_t2557347079 * value)
	{
		___Field_Conact_41 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Conact_41), value);
	}

	inline static int32_t get_offset_of_Field_ExtraHelp_42() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_ExtraHelp_42)); }
	inline GameObject_t2557347079 * get_Field_ExtraHelp_42() const { return ___Field_ExtraHelp_42; }
	inline GameObject_t2557347079 ** get_address_of_Field_ExtraHelp_42() { return &___Field_ExtraHelp_42; }
	inline void set_Field_ExtraHelp_42(GameObject_t2557347079 * value)
	{
		___Field_ExtraHelp_42 = value;
		Il2CppCodeGenWriteBarrier((&___Field_ExtraHelp_42), value);
	}

	inline static int32_t get_offset_of_Field_Terms_43() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___Field_Terms_43)); }
	inline GameObject_t2557347079 * get_Field_Terms_43() const { return ___Field_Terms_43; }
	inline GameObject_t2557347079 ** get_address_of_Field_Terms_43() { return &___Field_Terms_43; }
	inline void set_Field_Terms_43(GameObject_t2557347079 * value)
	{
		___Field_Terms_43 = value;
		Il2CppCodeGenWriteBarrier((&___Field_Terms_43), value);
	}

	inline static int32_t get_offset_of_instance_44() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___instance_44)); }
	inline CapturedDataInput_t2616152122 * get_instance_44() const { return ___instance_44; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_44() { return &___instance_44; }
	inline void set_instance_44(CapturedDataInput_t2616152122 * value)
	{
		___instance_44 = value;
		Il2CppCodeGenWriteBarrier((&___instance_44), value);
	}

	inline static int32_t get_offset_of_m_bGotRegistrationTokenDone_45() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___m_bGotRegistrationTokenDone_45)); }
	inline bool get_m_bGotRegistrationTokenDone_45() const { return ___m_bGotRegistrationTokenDone_45; }
	inline bool* get_address_of_m_bGotRegistrationTokenDone_45() { return &___m_bGotRegistrationTokenDone_45; }
	inline void set_m_bGotRegistrationTokenDone_45(bool value)
	{
		___m_bGotRegistrationTokenDone_45 = value;
	}

	inline static int32_t get_offset_of_LoginPanel_46() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___LoginPanel_46)); }
	inline GameObject_t2557347079 * get_LoginPanel_46() const { return ___LoginPanel_46; }
	inline GameObject_t2557347079 ** get_address_of_LoginPanel_46() { return &___LoginPanel_46; }
	inline void set_LoginPanel_46(GameObject_t2557347079 * value)
	{
		___LoginPanel_46 = value;
		Il2CppCodeGenWriteBarrier((&___LoginPanel_46), value);
	}

	inline static int32_t get_offset_of_SignUpPanel_47() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___SignUpPanel_47)); }
	inline GameObject_t2557347079 * get_SignUpPanel_47() const { return ___SignUpPanel_47; }
	inline GameObject_t2557347079 ** get_address_of_SignUpPanel_47() { return &___SignUpPanel_47; }
	inline void set_SignUpPanel_47(GameObject_t2557347079 * value)
	{
		___SignUpPanel_47 = value;
		Il2CppCodeGenWriteBarrier((&___SignUpPanel_47), value);
	}

	inline static int32_t get_offset_of_oTextRegisterNotificationMessage_48() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___oTextRegisterNotificationMessage_48)); }
	inline Text_t1790657652 * get_oTextRegisterNotificationMessage_48() const { return ___oTextRegisterNotificationMessage_48; }
	inline Text_t1790657652 ** get_address_of_oTextRegisterNotificationMessage_48() { return &___oTextRegisterNotificationMessage_48; }
	inline void set_oTextRegisterNotificationMessage_48(Text_t1790657652 * value)
	{
		___oTextRegisterNotificationMessage_48 = value;
		Il2CppCodeGenWriteBarrier((&___oTextRegisterNotificationMessage_48), value);
	}

	inline static int32_t get_offset_of_bSubmit_49() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___bSubmit_49)); }
	inline bool get_bSubmit_49() const { return ___bSubmit_49; }
	inline bool* get_address_of_bSubmit_49() { return &___bSubmit_49; }
	inline void set_bSubmit_49(bool value)
	{
		___bSubmit_49 = value;
	}

	inline static int32_t get_offset_of_EmailPanelRectTransform_50() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___EmailPanelRectTransform_50)); }
	inline Vector3_t1986933152  get_EmailPanelRectTransform_50() const { return ___EmailPanelRectTransform_50; }
	inline Vector3_t1986933152 * get_address_of_EmailPanelRectTransform_50() { return &___EmailPanelRectTransform_50; }
	inline void set_EmailPanelRectTransform_50(Vector3_t1986933152  value)
	{
		___EmailPanelRectTransform_50 = value;
	}

	inline static int32_t get_offset_of_oNotifications_51() { return static_cast<int32_t>(offsetof(Registration_t2162293902, ___oNotifications_51)); }
	inline GameObject_t2557347079 * get_oNotifications_51() const { return ___oNotifications_51; }
	inline GameObject_t2557347079 ** get_address_of_oNotifications_51() { return &___oNotifications_51; }
	inline void set_oNotifications_51(GameObject_t2557347079 * value)
	{
		___oNotifications_51 = value;
		Il2CppCodeGenWriteBarrier((&___oNotifications_51), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTRATION_T2162293902_H
#ifndef NOTIFICATIONS_T688528964_H
#define NOTIFICATIONS_T688528964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Notifications
struct  Notifications_t688528964  : public MonoBehaviour_t1618594486
{
public:
	// System.String Notifications::stext
	String_t* ___stext_2;
	// UnityEngine.UI.Text Notifications::oText
	Text_t1790657652 * ___oText_3;
	// System.Boolean Notifications::bshowNotification
	bool ___bshowNotification_4;
	// System.Boolean Notifications::bhideNotification
	bool ___bhideNotification_5;
	// System.Single Notifications::fFade
	float ___fFade_6;

public:
	inline static int32_t get_offset_of_stext_2() { return static_cast<int32_t>(offsetof(Notifications_t688528964, ___stext_2)); }
	inline String_t* get_stext_2() const { return ___stext_2; }
	inline String_t** get_address_of_stext_2() { return &___stext_2; }
	inline void set_stext_2(String_t* value)
	{
		___stext_2 = value;
		Il2CppCodeGenWriteBarrier((&___stext_2), value);
	}

	inline static int32_t get_offset_of_oText_3() { return static_cast<int32_t>(offsetof(Notifications_t688528964, ___oText_3)); }
	inline Text_t1790657652 * get_oText_3() const { return ___oText_3; }
	inline Text_t1790657652 ** get_address_of_oText_3() { return &___oText_3; }
	inline void set_oText_3(Text_t1790657652 * value)
	{
		___oText_3 = value;
		Il2CppCodeGenWriteBarrier((&___oText_3), value);
	}

	inline static int32_t get_offset_of_bshowNotification_4() { return static_cast<int32_t>(offsetof(Notifications_t688528964, ___bshowNotification_4)); }
	inline bool get_bshowNotification_4() const { return ___bshowNotification_4; }
	inline bool* get_address_of_bshowNotification_4() { return &___bshowNotification_4; }
	inline void set_bshowNotification_4(bool value)
	{
		___bshowNotification_4 = value;
	}

	inline static int32_t get_offset_of_bhideNotification_5() { return static_cast<int32_t>(offsetof(Notifications_t688528964, ___bhideNotification_5)); }
	inline bool get_bhideNotification_5() const { return ___bhideNotification_5; }
	inline bool* get_address_of_bhideNotification_5() { return &___bhideNotification_5; }
	inline void set_bhideNotification_5(bool value)
	{
		___bhideNotification_5 = value;
	}

	inline static int32_t get_offset_of_fFade_6() { return static_cast<int32_t>(offsetof(Notifications_t688528964, ___fFade_6)); }
	inline float get_fFade_6() const { return ___fFade_6; }
	inline float* get_address_of_fFade_6() { return &___fFade_6; }
	inline void set_fFade_6(float value)
	{
		___fFade_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONS_T688528964_H
#ifndef LOGIN_T3582553427_H
#define LOGIN_T3582553427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Login
struct  Login_t3582553427  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject Login::RegistrationForm
	GameObject_t2557347079 * ___RegistrationForm_2;
	// UnityEngine.Transform Login::ParentRegistrationForm
	Transform_t362059596 * ___ParentRegistrationForm_3;
	// CapturedDataInput Login::instance
	CapturedDataInput_t2616152122 * ___instance_4;
	// TokenHolderLegacy Login::instanceTHL
	TokenHolderLegacy_t1380075429 * ___instanceTHL_5;
	// System.Boolean Login::m_bGotLoginTokenDone
	bool ___m_bGotLoginTokenDone_6;
	// UnityEngine.UI.InputField Login::UserName
	InputField_t3123460221 * ___UserName_7;
	// UnityEngine.UI.InputField Login::Password
	InputField_t3123460221 * ___Password_8;
	// UnityEngine.GameObject Login::LoginPanel
	GameObject_t2557347079 * ___LoginPanel_9;
	// UnityEngine.GameObject Login::SignUpPanel
	GameObject_t2557347079 * ___SignUpPanel_10;
	// UnityEngine.GameObject Login::oNotifications
	GameObject_t2557347079 * ___oNotifications_11;
	// System.String Login::sNotificationError
	String_t* ___sNotificationError_12;

public:
	inline static int32_t get_offset_of_RegistrationForm_2() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___RegistrationForm_2)); }
	inline GameObject_t2557347079 * get_RegistrationForm_2() const { return ___RegistrationForm_2; }
	inline GameObject_t2557347079 ** get_address_of_RegistrationForm_2() { return &___RegistrationForm_2; }
	inline void set_RegistrationForm_2(GameObject_t2557347079 * value)
	{
		___RegistrationForm_2 = value;
		Il2CppCodeGenWriteBarrier((&___RegistrationForm_2), value);
	}

	inline static int32_t get_offset_of_ParentRegistrationForm_3() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___ParentRegistrationForm_3)); }
	inline Transform_t362059596 * get_ParentRegistrationForm_3() const { return ___ParentRegistrationForm_3; }
	inline Transform_t362059596 ** get_address_of_ParentRegistrationForm_3() { return &___ParentRegistrationForm_3; }
	inline void set_ParentRegistrationForm_3(Transform_t362059596 * value)
	{
		___ParentRegistrationForm_3 = value;
		Il2CppCodeGenWriteBarrier((&___ParentRegistrationForm_3), value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___instance_4)); }
	inline CapturedDataInput_t2616152122 * get_instance_4() const { return ___instance_4; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CapturedDataInput_t2616152122 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_instanceTHL_5() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___instanceTHL_5)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceTHL_5() const { return ___instanceTHL_5; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceTHL_5() { return &___instanceTHL_5; }
	inline void set_instanceTHL_5(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceTHL_5 = value;
		Il2CppCodeGenWriteBarrier((&___instanceTHL_5), value);
	}

	inline static int32_t get_offset_of_m_bGotLoginTokenDone_6() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___m_bGotLoginTokenDone_6)); }
	inline bool get_m_bGotLoginTokenDone_6() const { return ___m_bGotLoginTokenDone_6; }
	inline bool* get_address_of_m_bGotLoginTokenDone_6() { return &___m_bGotLoginTokenDone_6; }
	inline void set_m_bGotLoginTokenDone_6(bool value)
	{
		___m_bGotLoginTokenDone_6 = value;
	}

	inline static int32_t get_offset_of_UserName_7() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___UserName_7)); }
	inline InputField_t3123460221 * get_UserName_7() const { return ___UserName_7; }
	inline InputField_t3123460221 ** get_address_of_UserName_7() { return &___UserName_7; }
	inline void set_UserName_7(InputField_t3123460221 * value)
	{
		___UserName_7 = value;
		Il2CppCodeGenWriteBarrier((&___UserName_7), value);
	}

	inline static int32_t get_offset_of_Password_8() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___Password_8)); }
	inline InputField_t3123460221 * get_Password_8() const { return ___Password_8; }
	inline InputField_t3123460221 ** get_address_of_Password_8() { return &___Password_8; }
	inline void set_Password_8(InputField_t3123460221 * value)
	{
		___Password_8 = value;
		Il2CppCodeGenWriteBarrier((&___Password_8), value);
	}

	inline static int32_t get_offset_of_LoginPanel_9() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___LoginPanel_9)); }
	inline GameObject_t2557347079 * get_LoginPanel_9() const { return ___LoginPanel_9; }
	inline GameObject_t2557347079 ** get_address_of_LoginPanel_9() { return &___LoginPanel_9; }
	inline void set_LoginPanel_9(GameObject_t2557347079 * value)
	{
		___LoginPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___LoginPanel_9), value);
	}

	inline static int32_t get_offset_of_SignUpPanel_10() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___SignUpPanel_10)); }
	inline GameObject_t2557347079 * get_SignUpPanel_10() const { return ___SignUpPanel_10; }
	inline GameObject_t2557347079 ** get_address_of_SignUpPanel_10() { return &___SignUpPanel_10; }
	inline void set_SignUpPanel_10(GameObject_t2557347079 * value)
	{
		___SignUpPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___SignUpPanel_10), value);
	}

	inline static int32_t get_offset_of_oNotifications_11() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___oNotifications_11)); }
	inline GameObject_t2557347079 * get_oNotifications_11() const { return ___oNotifications_11; }
	inline GameObject_t2557347079 ** get_address_of_oNotifications_11() { return &___oNotifications_11; }
	inline void set_oNotifications_11(GameObject_t2557347079 * value)
	{
		___oNotifications_11 = value;
		Il2CppCodeGenWriteBarrier((&___oNotifications_11), value);
	}

	inline static int32_t get_offset_of_sNotificationError_12() { return static_cast<int32_t>(offsetof(Login_t3582553427, ___sNotificationError_12)); }
	inline String_t* get_sNotificationError_12() const { return ___sNotificationError_12; }
	inline String_t** get_address_of_sNotificationError_12() { return &___sNotificationError_12; }
	inline void set_sNotificationError_12(String_t* value)
	{
		___sNotificationError_12 = value;
		Il2CppCodeGenWriteBarrier((&___sNotificationError_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGIN_T3582553427_H
#ifndef LOADTALKSCENEXMLDATA_T2381108950_H
#define LOADTALKSCENEXMLDATA_T2381108950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTalkSceneXMLData
struct  LoadTalkSceneXMLData_t2381108950  : public MonoBehaviour_t1618594486
{
public:
	// System.String LoadTalkSceneXMLData::sm_strFilename
	String_t* ___sm_strFilename_2;
	// System.IO.TextReader LoadTalkSceneXMLData::sm_Read
	TextReader_t218744201 * ___sm_Read_3;
	// System.Int32 LoadTalkSceneXMLData::MulitiQuestionNum
	int32_t ___MulitiQuestionNum_4;
	// System.Xml.XmlNodeList LoadTalkSceneXMLData::scenesList
	XmlNodeList_t3849781475 * ___scenesList_5;
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.String>> LoadTalkSceneXMLData::m_xmlScenes
	List_1_t179729523 * ___m_xmlScenes_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> LoadTalkSceneXMLData::m_xmlElement
	Dictionary_2_t1060102516 * ___m_xmlElement_7;
	// Goodbye LoadTalkSceneXMLData::GoodByeScript
	Goodbye_t2957868680 * ___GoodByeScript_8;
	// UnityEngine.Transform LoadTalkSceneXMLData::MiniGame_L3DragAndDropBlinc
	Transform_t362059596 * ___MiniGame_L3DragAndDropBlinc_9;

public:
	inline static int32_t get_offset_of_sm_strFilename_2() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___sm_strFilename_2)); }
	inline String_t* get_sm_strFilename_2() const { return ___sm_strFilename_2; }
	inline String_t** get_address_of_sm_strFilename_2() { return &___sm_strFilename_2; }
	inline void set_sm_strFilename_2(String_t* value)
	{
		___sm_strFilename_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm_strFilename_2), value);
	}

	inline static int32_t get_offset_of_sm_Read_3() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___sm_Read_3)); }
	inline TextReader_t218744201 * get_sm_Read_3() const { return ___sm_Read_3; }
	inline TextReader_t218744201 ** get_address_of_sm_Read_3() { return &___sm_Read_3; }
	inline void set_sm_Read_3(TextReader_t218744201 * value)
	{
		___sm_Read_3 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Read_3), value);
	}

	inline static int32_t get_offset_of_MulitiQuestionNum_4() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___MulitiQuestionNum_4)); }
	inline int32_t get_MulitiQuestionNum_4() const { return ___MulitiQuestionNum_4; }
	inline int32_t* get_address_of_MulitiQuestionNum_4() { return &___MulitiQuestionNum_4; }
	inline void set_MulitiQuestionNum_4(int32_t value)
	{
		___MulitiQuestionNum_4 = value;
	}

	inline static int32_t get_offset_of_scenesList_5() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___scenesList_5)); }
	inline XmlNodeList_t3849781475 * get_scenesList_5() const { return ___scenesList_5; }
	inline XmlNodeList_t3849781475 ** get_address_of_scenesList_5() { return &___scenesList_5; }
	inline void set_scenesList_5(XmlNodeList_t3849781475 * value)
	{
		___scenesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___scenesList_5), value);
	}

	inline static int32_t get_offset_of_m_xmlScenes_6() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___m_xmlScenes_6)); }
	inline List_1_t179729523 * get_m_xmlScenes_6() const { return ___m_xmlScenes_6; }
	inline List_1_t179729523 ** get_address_of_m_xmlScenes_6() { return &___m_xmlScenes_6; }
	inline void set_m_xmlScenes_6(List_1_t179729523 * value)
	{
		___m_xmlScenes_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlScenes_6), value);
	}

	inline static int32_t get_offset_of_m_xmlElement_7() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___m_xmlElement_7)); }
	inline Dictionary_2_t1060102516 * get_m_xmlElement_7() const { return ___m_xmlElement_7; }
	inline Dictionary_2_t1060102516 ** get_address_of_m_xmlElement_7() { return &___m_xmlElement_7; }
	inline void set_m_xmlElement_7(Dictionary_2_t1060102516 * value)
	{
		___m_xmlElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlElement_7), value);
	}

	inline static int32_t get_offset_of_GoodByeScript_8() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___GoodByeScript_8)); }
	inline Goodbye_t2957868680 * get_GoodByeScript_8() const { return ___GoodByeScript_8; }
	inline Goodbye_t2957868680 ** get_address_of_GoodByeScript_8() { return &___GoodByeScript_8; }
	inline void set_GoodByeScript_8(Goodbye_t2957868680 * value)
	{
		___GoodByeScript_8 = value;
		Il2CppCodeGenWriteBarrier((&___GoodByeScript_8), value);
	}

	inline static int32_t get_offset_of_MiniGame_L3DragAndDropBlinc_9() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950, ___MiniGame_L3DragAndDropBlinc_9)); }
	inline Transform_t362059596 * get_MiniGame_L3DragAndDropBlinc_9() const { return ___MiniGame_L3DragAndDropBlinc_9; }
	inline Transform_t362059596 ** get_address_of_MiniGame_L3DragAndDropBlinc_9() { return &___MiniGame_L3DragAndDropBlinc_9; }
	inline void set_MiniGame_L3DragAndDropBlinc_9(Transform_t362059596 * value)
	{
		___MiniGame_L3DragAndDropBlinc_9 = value;
		Il2CppCodeGenWriteBarrier((&___MiniGame_L3DragAndDropBlinc_9), value);
	}
};

struct LoadTalkSceneXMLData_t2381108950_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LoadTalkSceneXMLData::<>f__switch$mapF
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapF_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LoadTalkSceneXMLData::<>f__switch$map10
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map10_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LoadTalkSceneXMLData::<>f__switch$map11
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map11_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LoadTalkSceneXMLData::<>f__switch$map12
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map12_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LoadTalkSceneXMLData::<>f__switch$map13
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24map13_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_10() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950_StaticFields, ___U3CU3Ef__switchU24mapF_10)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapF_10() const { return ___U3CU3Ef__switchU24mapF_10; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapF_10() { return &___U3CU3Ef__switchU24mapF_10; }
	inline void set_U3CU3Ef__switchU24mapF_10(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapF_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_11() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950_StaticFields, ___U3CU3Ef__switchU24map10_11)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map10_11() const { return ___U3CU3Ef__switchU24map10_11; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map10_11() { return &___U3CU3Ef__switchU24map10_11; }
	inline void set_U3CU3Ef__switchU24map10_11(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map10_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map10_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_12() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950_StaticFields, ___U3CU3Ef__switchU24map11_12)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map11_12() const { return ___U3CU3Ef__switchU24map11_12; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map11_12() { return &___U3CU3Ef__switchU24map11_12; }
	inline void set_U3CU3Ef__switchU24map11_12(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map11_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map11_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_13() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950_StaticFields, ___U3CU3Ef__switchU24map12_13)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map12_13() const { return ___U3CU3Ef__switchU24map12_13; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map12_13() { return &___U3CU3Ef__switchU24map12_13; }
	inline void set_U3CU3Ef__switchU24map12_13(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map12_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_14() { return static_cast<int32_t>(offsetof(LoadTalkSceneXMLData_t2381108950_StaticFields, ___U3CU3Ef__switchU24map13_14)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24map13_14() const { return ___U3CU3Ef__switchU24map13_14; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24map13_14() { return &___U3CU3Ef__switchU24map13_14; }
	inline void set_U3CU3Ef__switchU24map13_14(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24map13_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTALKSCENEXMLDATA_T2381108950_H
#ifndef SHIELDACTION_T3393513384_H
#define SHIELDACTION_T3393513384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldAction
struct  ShieldAction_t3393513384  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean ShieldAction::shieldIsOn
	bool ___shieldIsOn_2;
	// System.Boolean ShieldAction::m_bShieldFadeInFinished
	bool ___m_bShieldFadeInFinished_3;
	// System.Boolean ShieldAction::m_bShieldFadeOutFinished
	bool ___m_bShieldFadeOutFinished_4;
	// UnityEngine.Color ShieldAction::m_cShieldColor
	Color_t2582018970  ___m_cShieldColor_5;
	// System.Single ShieldAction::m_fShieldFadeAlfa
	float ___m_fShieldFadeAlfa_6;
	// System.Boolean ShieldAction::m_bFadeInStart
	bool ___m_bFadeInStart_7;
	// System.Boolean ShieldAction::m_bFadeOutStart
	bool ___m_bFadeOutStart_8;
	// System.Single ShieldAction::m_fShieldAppearTime
	float ___m_fShieldAppearTime_9;

public:
	inline static int32_t get_offset_of_shieldIsOn_2() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___shieldIsOn_2)); }
	inline bool get_shieldIsOn_2() const { return ___shieldIsOn_2; }
	inline bool* get_address_of_shieldIsOn_2() { return &___shieldIsOn_2; }
	inline void set_shieldIsOn_2(bool value)
	{
		___shieldIsOn_2 = value;
	}

	inline static int32_t get_offset_of_m_bShieldFadeInFinished_3() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_bShieldFadeInFinished_3)); }
	inline bool get_m_bShieldFadeInFinished_3() const { return ___m_bShieldFadeInFinished_3; }
	inline bool* get_address_of_m_bShieldFadeInFinished_3() { return &___m_bShieldFadeInFinished_3; }
	inline void set_m_bShieldFadeInFinished_3(bool value)
	{
		___m_bShieldFadeInFinished_3 = value;
	}

	inline static int32_t get_offset_of_m_bShieldFadeOutFinished_4() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_bShieldFadeOutFinished_4)); }
	inline bool get_m_bShieldFadeOutFinished_4() const { return ___m_bShieldFadeOutFinished_4; }
	inline bool* get_address_of_m_bShieldFadeOutFinished_4() { return &___m_bShieldFadeOutFinished_4; }
	inline void set_m_bShieldFadeOutFinished_4(bool value)
	{
		___m_bShieldFadeOutFinished_4 = value;
	}

	inline static int32_t get_offset_of_m_cShieldColor_5() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_cShieldColor_5)); }
	inline Color_t2582018970  get_m_cShieldColor_5() const { return ___m_cShieldColor_5; }
	inline Color_t2582018970 * get_address_of_m_cShieldColor_5() { return &___m_cShieldColor_5; }
	inline void set_m_cShieldColor_5(Color_t2582018970  value)
	{
		___m_cShieldColor_5 = value;
	}

	inline static int32_t get_offset_of_m_fShieldFadeAlfa_6() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_fShieldFadeAlfa_6)); }
	inline float get_m_fShieldFadeAlfa_6() const { return ___m_fShieldFadeAlfa_6; }
	inline float* get_address_of_m_fShieldFadeAlfa_6() { return &___m_fShieldFadeAlfa_6; }
	inline void set_m_fShieldFadeAlfa_6(float value)
	{
		___m_fShieldFadeAlfa_6 = value;
	}

	inline static int32_t get_offset_of_m_bFadeInStart_7() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_bFadeInStart_7)); }
	inline bool get_m_bFadeInStart_7() const { return ___m_bFadeInStart_7; }
	inline bool* get_address_of_m_bFadeInStart_7() { return &___m_bFadeInStart_7; }
	inline void set_m_bFadeInStart_7(bool value)
	{
		___m_bFadeInStart_7 = value;
	}

	inline static int32_t get_offset_of_m_bFadeOutStart_8() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_bFadeOutStart_8)); }
	inline bool get_m_bFadeOutStart_8() const { return ___m_bFadeOutStart_8; }
	inline bool* get_address_of_m_bFadeOutStart_8() { return &___m_bFadeOutStart_8; }
	inline void set_m_bFadeOutStart_8(bool value)
	{
		___m_bFadeOutStart_8 = value;
	}

	inline static int32_t get_offset_of_m_fShieldAppearTime_9() { return static_cast<int32_t>(offsetof(ShieldAction_t3393513384, ___m_fShieldAppearTime_9)); }
	inline float get_m_fShieldAppearTime_9() const { return ___m_fShieldAppearTime_9; }
	inline float* get_address_of_m_fShieldAppearTime_9() { return &___m_fShieldAppearTime_9; }
	inline void set_m_fShieldAppearTime_9(float value)
	{
		___m_fShieldAppearTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELDACTION_T3393513384_H
#ifndef LV7KINGOFGNATSFADEIN_T1641895077_H
#define LV7KINGOFGNATSFADEIN_T1641895077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7KingOfGnatsFadeIn
struct  Lv7KingOfGnatsFadeIn_t1641895077  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Lv7KingOfGnatsFadeIn::KOGFadeInFinished
	bool ___KOGFadeInFinished_2;
	// System.Single Lv7KingOfGnatsFadeIn::KOGAppearTime
	float ___KOGAppearTime_3;
	// System.Single Lv7KingOfGnatsFadeIn::m_fMonsterFadeInAlpha
	float ___m_fMonsterFadeInAlpha_4;
	// System.Single Lv7KingOfGnatsFadeIn::fTime
	float ___fTime_5;
	// UnityEngine.GameObject Lv7KingOfGnatsFadeIn::KOG
	GameObject_t2557347079 * ___KOG_6;
	// UnityEngine.Color Lv7KingOfGnatsFadeIn::m_cMonsterColor
	Color_t2582018970  ___m_cMonsterColor_7;
	// UnityEngine.Color Lv7KingOfGnatsFadeIn::m_cMonsterEyeColor
	Color_t2582018970  ___m_cMonsterEyeColor_8;
	// UnityEngine.Color Lv7KingOfGnatsFadeIn::m_cMonsterFurColor
	Color_t2582018970  ___m_cMonsterFurColor_9;

public:
	inline static int32_t get_offset_of_KOGFadeInFinished_2() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___KOGFadeInFinished_2)); }
	inline bool get_KOGFadeInFinished_2() const { return ___KOGFadeInFinished_2; }
	inline bool* get_address_of_KOGFadeInFinished_2() { return &___KOGFadeInFinished_2; }
	inline void set_KOGFadeInFinished_2(bool value)
	{
		___KOGFadeInFinished_2 = value;
	}

	inline static int32_t get_offset_of_KOGAppearTime_3() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___KOGAppearTime_3)); }
	inline float get_KOGAppearTime_3() const { return ___KOGAppearTime_3; }
	inline float* get_address_of_KOGAppearTime_3() { return &___KOGAppearTime_3; }
	inline void set_KOGAppearTime_3(float value)
	{
		___KOGAppearTime_3 = value;
	}

	inline static int32_t get_offset_of_m_fMonsterFadeInAlpha_4() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___m_fMonsterFadeInAlpha_4)); }
	inline float get_m_fMonsterFadeInAlpha_4() const { return ___m_fMonsterFadeInAlpha_4; }
	inline float* get_address_of_m_fMonsterFadeInAlpha_4() { return &___m_fMonsterFadeInAlpha_4; }
	inline void set_m_fMonsterFadeInAlpha_4(float value)
	{
		___m_fMonsterFadeInAlpha_4 = value;
	}

	inline static int32_t get_offset_of_fTime_5() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___fTime_5)); }
	inline float get_fTime_5() const { return ___fTime_5; }
	inline float* get_address_of_fTime_5() { return &___fTime_5; }
	inline void set_fTime_5(float value)
	{
		___fTime_5 = value;
	}

	inline static int32_t get_offset_of_KOG_6() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___KOG_6)); }
	inline GameObject_t2557347079 * get_KOG_6() const { return ___KOG_6; }
	inline GameObject_t2557347079 ** get_address_of_KOG_6() { return &___KOG_6; }
	inline void set_KOG_6(GameObject_t2557347079 * value)
	{
		___KOG_6 = value;
		Il2CppCodeGenWriteBarrier((&___KOG_6), value);
	}

	inline static int32_t get_offset_of_m_cMonsterColor_7() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___m_cMonsterColor_7)); }
	inline Color_t2582018970  get_m_cMonsterColor_7() const { return ___m_cMonsterColor_7; }
	inline Color_t2582018970 * get_address_of_m_cMonsterColor_7() { return &___m_cMonsterColor_7; }
	inline void set_m_cMonsterColor_7(Color_t2582018970  value)
	{
		___m_cMonsterColor_7 = value;
	}

	inline static int32_t get_offset_of_m_cMonsterEyeColor_8() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___m_cMonsterEyeColor_8)); }
	inline Color_t2582018970  get_m_cMonsterEyeColor_8() const { return ___m_cMonsterEyeColor_8; }
	inline Color_t2582018970 * get_address_of_m_cMonsterEyeColor_8() { return &___m_cMonsterEyeColor_8; }
	inline void set_m_cMonsterEyeColor_8(Color_t2582018970  value)
	{
		___m_cMonsterEyeColor_8 = value;
	}

	inline static int32_t get_offset_of_m_cMonsterFurColor_9() { return static_cast<int32_t>(offsetof(Lv7KingOfGnatsFadeIn_t1641895077, ___m_cMonsterFurColor_9)); }
	inline Color_t2582018970  get_m_cMonsterFurColor_9() const { return ___m_cMonsterFurColor_9; }
	inline Color_t2582018970 * get_address_of_m_cMonsterFurColor_9() { return &___m_cMonsterFurColor_9; }
	inline void set_m_cMonsterFurColor_9(Color_t2582018970  value)
	{
		___m_cMonsterFurColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7KINGOFGNATSFADEIN_T1641895077_H
#ifndef DISPLAYQUESTION_T773709411_H
#define DISPLAYQUESTION_T773709411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayQuestion
struct  DisplayQuestion_t773709411  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean DisplayQuestion::bCorrectAnswer
	bool ___bCorrectAnswer_2;
	// System.Boolean DisplayQuestion::bFinishedAnswering
	bool ___bFinishedAnswering_3;
	// UnityEngine.GUISkin DisplayQuestion::GUIskin
	GUISkin_t2122630221 * ___GUIskin_4;
	// System.Boolean DisplayQuestion::bActivated
	bool ___bActivated_5;
	// System.String[] DisplayQuestion::Options
	StringU5BU5D_t2511808107* ___Options_6;
	// System.String DisplayQuestion::CorrectOption
	String_t* ___CorrectOption_7;
	// System.String DisplayQuestion::Question
	String_t* ___Question_8;
	// UnityEngine.GameObject DisplayQuestion::CorrespondingSparx
	GameObject_t2557347079 * ___CorrespondingSparx_9;
	// System.Single DisplayQuestion::fStartVoice
	float ___fStartVoice_10;
	// System.Single DisplayQuestion::fEndVoice
	float ___fEndVoice_11;
	// System.String DisplayQuestion::VoiceFile
	String_t* ___VoiceFile_12;
	// UnityEngine.AudioClip DisplayQuestion::L4GnatsAudioClip
	AudioClip_t1419814565 * ___L4GnatsAudioClip_13;
	// UnityEngine.AudioSource DisplayQuestion::m_Voice
	AudioSource_t4025721661 * ___m_Voice_14;
	// System.Single DisplayQuestion::fHeight
	float ___fHeight_15;
	// System.String DisplayQuestion::Name
	String_t* ___Name_16;
	// System.Single DisplayQuestion::countdown
	float ___countdown_17;

public:
	inline static int32_t get_offset_of_bCorrectAnswer_2() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___bCorrectAnswer_2)); }
	inline bool get_bCorrectAnswer_2() const { return ___bCorrectAnswer_2; }
	inline bool* get_address_of_bCorrectAnswer_2() { return &___bCorrectAnswer_2; }
	inline void set_bCorrectAnswer_2(bool value)
	{
		___bCorrectAnswer_2 = value;
	}

	inline static int32_t get_offset_of_bFinishedAnswering_3() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___bFinishedAnswering_3)); }
	inline bool get_bFinishedAnswering_3() const { return ___bFinishedAnswering_3; }
	inline bool* get_address_of_bFinishedAnswering_3() { return &___bFinishedAnswering_3; }
	inline void set_bFinishedAnswering_3(bool value)
	{
		___bFinishedAnswering_3 = value;
	}

	inline static int32_t get_offset_of_GUIskin_4() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___GUIskin_4)); }
	inline GUISkin_t2122630221 * get_GUIskin_4() const { return ___GUIskin_4; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_4() { return &___GUIskin_4; }
	inline void set_GUIskin_4(GUISkin_t2122630221 * value)
	{
		___GUIskin_4 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_4), value);
	}

	inline static int32_t get_offset_of_bActivated_5() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___bActivated_5)); }
	inline bool get_bActivated_5() const { return ___bActivated_5; }
	inline bool* get_address_of_bActivated_5() { return &___bActivated_5; }
	inline void set_bActivated_5(bool value)
	{
		___bActivated_5 = value;
	}

	inline static int32_t get_offset_of_Options_6() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___Options_6)); }
	inline StringU5BU5D_t2511808107* get_Options_6() const { return ___Options_6; }
	inline StringU5BU5D_t2511808107** get_address_of_Options_6() { return &___Options_6; }
	inline void set_Options_6(StringU5BU5D_t2511808107* value)
	{
		___Options_6 = value;
		Il2CppCodeGenWriteBarrier((&___Options_6), value);
	}

	inline static int32_t get_offset_of_CorrectOption_7() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___CorrectOption_7)); }
	inline String_t* get_CorrectOption_7() const { return ___CorrectOption_7; }
	inline String_t** get_address_of_CorrectOption_7() { return &___CorrectOption_7; }
	inline void set_CorrectOption_7(String_t* value)
	{
		___CorrectOption_7 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectOption_7), value);
	}

	inline static int32_t get_offset_of_Question_8() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___Question_8)); }
	inline String_t* get_Question_8() const { return ___Question_8; }
	inline String_t** get_address_of_Question_8() { return &___Question_8; }
	inline void set_Question_8(String_t* value)
	{
		___Question_8 = value;
		Il2CppCodeGenWriteBarrier((&___Question_8), value);
	}

	inline static int32_t get_offset_of_CorrespondingSparx_9() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___CorrespondingSparx_9)); }
	inline GameObject_t2557347079 * get_CorrespondingSparx_9() const { return ___CorrespondingSparx_9; }
	inline GameObject_t2557347079 ** get_address_of_CorrespondingSparx_9() { return &___CorrespondingSparx_9; }
	inline void set_CorrespondingSparx_9(GameObject_t2557347079 * value)
	{
		___CorrespondingSparx_9 = value;
		Il2CppCodeGenWriteBarrier((&___CorrespondingSparx_9), value);
	}

	inline static int32_t get_offset_of_fStartVoice_10() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___fStartVoice_10)); }
	inline float get_fStartVoice_10() const { return ___fStartVoice_10; }
	inline float* get_address_of_fStartVoice_10() { return &___fStartVoice_10; }
	inline void set_fStartVoice_10(float value)
	{
		___fStartVoice_10 = value;
	}

	inline static int32_t get_offset_of_fEndVoice_11() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___fEndVoice_11)); }
	inline float get_fEndVoice_11() const { return ___fEndVoice_11; }
	inline float* get_address_of_fEndVoice_11() { return &___fEndVoice_11; }
	inline void set_fEndVoice_11(float value)
	{
		___fEndVoice_11 = value;
	}

	inline static int32_t get_offset_of_VoiceFile_12() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___VoiceFile_12)); }
	inline String_t* get_VoiceFile_12() const { return ___VoiceFile_12; }
	inline String_t** get_address_of_VoiceFile_12() { return &___VoiceFile_12; }
	inline void set_VoiceFile_12(String_t* value)
	{
		___VoiceFile_12 = value;
		Il2CppCodeGenWriteBarrier((&___VoiceFile_12), value);
	}

	inline static int32_t get_offset_of_L4GnatsAudioClip_13() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___L4GnatsAudioClip_13)); }
	inline AudioClip_t1419814565 * get_L4GnatsAudioClip_13() const { return ___L4GnatsAudioClip_13; }
	inline AudioClip_t1419814565 ** get_address_of_L4GnatsAudioClip_13() { return &___L4GnatsAudioClip_13; }
	inline void set_L4GnatsAudioClip_13(AudioClip_t1419814565 * value)
	{
		___L4GnatsAudioClip_13 = value;
		Il2CppCodeGenWriteBarrier((&___L4GnatsAudioClip_13), value);
	}

	inline static int32_t get_offset_of_m_Voice_14() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___m_Voice_14)); }
	inline AudioSource_t4025721661 * get_m_Voice_14() const { return ___m_Voice_14; }
	inline AudioSource_t4025721661 ** get_address_of_m_Voice_14() { return &___m_Voice_14; }
	inline void set_m_Voice_14(AudioSource_t4025721661 * value)
	{
		___m_Voice_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Voice_14), value);
	}

	inline static int32_t get_offset_of_fHeight_15() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___fHeight_15)); }
	inline float get_fHeight_15() const { return ___fHeight_15; }
	inline float* get_address_of_fHeight_15() { return &___fHeight_15; }
	inline void set_fHeight_15(float value)
	{
		___fHeight_15 = value;
	}

	inline static int32_t get_offset_of_Name_16() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___Name_16)); }
	inline String_t* get_Name_16() const { return ___Name_16; }
	inline String_t** get_address_of_Name_16() { return &___Name_16; }
	inline void set_Name_16(String_t* value)
	{
		___Name_16 = value;
		Il2CppCodeGenWriteBarrier((&___Name_16), value);
	}

	inline static int32_t get_offset_of_countdown_17() { return static_cast<int32_t>(offsetof(DisplayQuestion_t773709411, ___countdown_17)); }
	inline float get_countdown_17() const { return ___countdown_17; }
	inline float* get_address_of_countdown_17() { return &___countdown_17; }
	inline void set_countdown_17(float value)
	{
		___countdown_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYQUESTION_T773709411_H
#ifndef LV7KINGOFGNATS_T3383848971_H
#define LV7KINGOFGNATS_T3383848971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7KingOfGnats
struct  Lv7KingOfGnats_t3383848971  : public MonoBehaviour_t1618594486
{
public:
	// KOGStatus Lv7KingOfGnats::status
	int32_t ___status_2;
	// UnityEngine.GameObject Lv7KingOfGnats::KoG
	GameObject_t2557347079 * ___KoG_3;
	// UnityEngine.GameObject Lv7KingOfGnats::goGnat
	GameObject_t2557347079 * ___goGnat_4;
	// UnityEngine.GameObject Lv7KingOfGnats::goSpark
	GameObject_t2557347079 * ___goSpark_5;
	// UnityEngine.GameObject Lv7KingOfGnats::shield
	GameObject_t2557347079 * ___shield_6;
	// UnityEngine.GameObject Lv7KingOfGnats::gem
	GameObject_t2557347079 * ___gem_7;
	// UnityEngine.Transform[] Lv7KingOfGnats::GnatsStayPosition
	TransformU5BU5D_t2383644165* ___GnatsStayPosition_8;
	// UnityEngine.Transform Lv7KingOfGnats::gnatKilledPosition
	Transform_t362059596 * ___gnatKilledPosition_9;
	// UnityEngine.GameObject[] Lv7KingOfGnats::AttackingGnats
	GameObjectU5BU5D_t2988620542* ___AttackingGnats_10;
	// UnityEngine.GameObject[] Lv7KingOfGnats::Sparks
	GameObjectU5BU5D_t2988620542* ___Sparks_11;
	// UnityEngine.GameObject Lv7KingOfGnats::CurrentGnat
	GameObject_t2557347079 * ___CurrentGnat_12;
	// System.String Lv7KingOfGnats::CameraID
	String_t* ___CameraID_13;
	// AttackMethod Lv7KingOfGnats::AttackStrategy
	int32_t ___AttackStrategy_14;
	// KOGAttack Lv7KingOfGnats::KingAttack
	int32_t ___KingAttack_15;
	// UnityEngine.Vector3 Lv7KingOfGnats::hitPosition
	Vector3_t1986933152  ___hitPosition_16;
	// System.Int32 Lv7KingOfGnats::m_iNumOfGnatsFiredOut
	int32_t ___m_iNumOfGnatsFiredOut_17;
	// System.Int32 Lv7KingOfGnats::m_iNumOfGnatsKilled
	int32_t ___m_iNumOfGnatsKilled_18;
	// System.Int32 Lv7KingOfGnats::m_iNumToFire
	int32_t ___m_iNumToFire_19;
	// System.Int32 Lv7KingOfGnats::CurrentEmptySpace
	int32_t ___CurrentEmptySpace_20;
	// System.Single Lv7KingOfGnats::fUpCounter
	float ___fUpCounter_21;
	// System.Single Lv7KingOfGnats::fTime
	float ___fTime_22;
	// System.Single Lv7KingOfGnats::fStatusTimer
	float ___fStatusTimer_23;
	// System.Boolean Lv7KingOfGnats::fireNext
	bool ___fireNext_24;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_KoG_3() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___KoG_3)); }
	inline GameObject_t2557347079 * get_KoG_3() const { return ___KoG_3; }
	inline GameObject_t2557347079 ** get_address_of_KoG_3() { return &___KoG_3; }
	inline void set_KoG_3(GameObject_t2557347079 * value)
	{
		___KoG_3 = value;
		Il2CppCodeGenWriteBarrier((&___KoG_3), value);
	}

	inline static int32_t get_offset_of_goGnat_4() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___goGnat_4)); }
	inline GameObject_t2557347079 * get_goGnat_4() const { return ___goGnat_4; }
	inline GameObject_t2557347079 ** get_address_of_goGnat_4() { return &___goGnat_4; }
	inline void set_goGnat_4(GameObject_t2557347079 * value)
	{
		___goGnat_4 = value;
		Il2CppCodeGenWriteBarrier((&___goGnat_4), value);
	}

	inline static int32_t get_offset_of_goSpark_5() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___goSpark_5)); }
	inline GameObject_t2557347079 * get_goSpark_5() const { return ___goSpark_5; }
	inline GameObject_t2557347079 ** get_address_of_goSpark_5() { return &___goSpark_5; }
	inline void set_goSpark_5(GameObject_t2557347079 * value)
	{
		___goSpark_5 = value;
		Il2CppCodeGenWriteBarrier((&___goSpark_5), value);
	}

	inline static int32_t get_offset_of_shield_6() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___shield_6)); }
	inline GameObject_t2557347079 * get_shield_6() const { return ___shield_6; }
	inline GameObject_t2557347079 ** get_address_of_shield_6() { return &___shield_6; }
	inline void set_shield_6(GameObject_t2557347079 * value)
	{
		___shield_6 = value;
		Il2CppCodeGenWriteBarrier((&___shield_6), value);
	}

	inline static int32_t get_offset_of_gem_7() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___gem_7)); }
	inline GameObject_t2557347079 * get_gem_7() const { return ___gem_7; }
	inline GameObject_t2557347079 ** get_address_of_gem_7() { return &___gem_7; }
	inline void set_gem_7(GameObject_t2557347079 * value)
	{
		___gem_7 = value;
		Il2CppCodeGenWriteBarrier((&___gem_7), value);
	}

	inline static int32_t get_offset_of_GnatsStayPosition_8() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___GnatsStayPosition_8)); }
	inline TransformU5BU5D_t2383644165* get_GnatsStayPosition_8() const { return ___GnatsStayPosition_8; }
	inline TransformU5BU5D_t2383644165** get_address_of_GnatsStayPosition_8() { return &___GnatsStayPosition_8; }
	inline void set_GnatsStayPosition_8(TransformU5BU5D_t2383644165* value)
	{
		___GnatsStayPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___GnatsStayPosition_8), value);
	}

	inline static int32_t get_offset_of_gnatKilledPosition_9() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___gnatKilledPosition_9)); }
	inline Transform_t362059596 * get_gnatKilledPosition_9() const { return ___gnatKilledPosition_9; }
	inline Transform_t362059596 ** get_address_of_gnatKilledPosition_9() { return &___gnatKilledPosition_9; }
	inline void set_gnatKilledPosition_9(Transform_t362059596 * value)
	{
		___gnatKilledPosition_9 = value;
		Il2CppCodeGenWriteBarrier((&___gnatKilledPosition_9), value);
	}

	inline static int32_t get_offset_of_AttackingGnats_10() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___AttackingGnats_10)); }
	inline GameObjectU5BU5D_t2988620542* get_AttackingGnats_10() const { return ___AttackingGnats_10; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_AttackingGnats_10() { return &___AttackingGnats_10; }
	inline void set_AttackingGnats_10(GameObjectU5BU5D_t2988620542* value)
	{
		___AttackingGnats_10 = value;
		Il2CppCodeGenWriteBarrier((&___AttackingGnats_10), value);
	}

	inline static int32_t get_offset_of_Sparks_11() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___Sparks_11)); }
	inline GameObjectU5BU5D_t2988620542* get_Sparks_11() const { return ___Sparks_11; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Sparks_11() { return &___Sparks_11; }
	inline void set_Sparks_11(GameObjectU5BU5D_t2988620542* value)
	{
		___Sparks_11 = value;
		Il2CppCodeGenWriteBarrier((&___Sparks_11), value);
	}

	inline static int32_t get_offset_of_CurrentGnat_12() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___CurrentGnat_12)); }
	inline GameObject_t2557347079 * get_CurrentGnat_12() const { return ___CurrentGnat_12; }
	inline GameObject_t2557347079 ** get_address_of_CurrentGnat_12() { return &___CurrentGnat_12; }
	inline void set_CurrentGnat_12(GameObject_t2557347079 * value)
	{
		___CurrentGnat_12 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentGnat_12), value);
	}

	inline static int32_t get_offset_of_CameraID_13() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___CameraID_13)); }
	inline String_t* get_CameraID_13() const { return ___CameraID_13; }
	inline String_t** get_address_of_CameraID_13() { return &___CameraID_13; }
	inline void set_CameraID_13(String_t* value)
	{
		___CameraID_13 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_13), value);
	}

	inline static int32_t get_offset_of_AttackStrategy_14() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___AttackStrategy_14)); }
	inline int32_t get_AttackStrategy_14() const { return ___AttackStrategy_14; }
	inline int32_t* get_address_of_AttackStrategy_14() { return &___AttackStrategy_14; }
	inline void set_AttackStrategy_14(int32_t value)
	{
		___AttackStrategy_14 = value;
	}

	inline static int32_t get_offset_of_KingAttack_15() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___KingAttack_15)); }
	inline int32_t get_KingAttack_15() const { return ___KingAttack_15; }
	inline int32_t* get_address_of_KingAttack_15() { return &___KingAttack_15; }
	inline void set_KingAttack_15(int32_t value)
	{
		___KingAttack_15 = value;
	}

	inline static int32_t get_offset_of_hitPosition_16() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___hitPosition_16)); }
	inline Vector3_t1986933152  get_hitPosition_16() const { return ___hitPosition_16; }
	inline Vector3_t1986933152 * get_address_of_hitPosition_16() { return &___hitPosition_16; }
	inline void set_hitPosition_16(Vector3_t1986933152  value)
	{
		___hitPosition_16 = value;
	}

	inline static int32_t get_offset_of_m_iNumOfGnatsFiredOut_17() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___m_iNumOfGnatsFiredOut_17)); }
	inline int32_t get_m_iNumOfGnatsFiredOut_17() const { return ___m_iNumOfGnatsFiredOut_17; }
	inline int32_t* get_address_of_m_iNumOfGnatsFiredOut_17() { return &___m_iNumOfGnatsFiredOut_17; }
	inline void set_m_iNumOfGnatsFiredOut_17(int32_t value)
	{
		___m_iNumOfGnatsFiredOut_17 = value;
	}

	inline static int32_t get_offset_of_m_iNumOfGnatsKilled_18() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___m_iNumOfGnatsKilled_18)); }
	inline int32_t get_m_iNumOfGnatsKilled_18() const { return ___m_iNumOfGnatsKilled_18; }
	inline int32_t* get_address_of_m_iNumOfGnatsKilled_18() { return &___m_iNumOfGnatsKilled_18; }
	inline void set_m_iNumOfGnatsKilled_18(int32_t value)
	{
		___m_iNumOfGnatsKilled_18 = value;
	}

	inline static int32_t get_offset_of_m_iNumToFire_19() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___m_iNumToFire_19)); }
	inline int32_t get_m_iNumToFire_19() const { return ___m_iNumToFire_19; }
	inline int32_t* get_address_of_m_iNumToFire_19() { return &___m_iNumToFire_19; }
	inline void set_m_iNumToFire_19(int32_t value)
	{
		___m_iNumToFire_19 = value;
	}

	inline static int32_t get_offset_of_CurrentEmptySpace_20() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___CurrentEmptySpace_20)); }
	inline int32_t get_CurrentEmptySpace_20() const { return ___CurrentEmptySpace_20; }
	inline int32_t* get_address_of_CurrentEmptySpace_20() { return &___CurrentEmptySpace_20; }
	inline void set_CurrentEmptySpace_20(int32_t value)
	{
		___CurrentEmptySpace_20 = value;
	}

	inline static int32_t get_offset_of_fUpCounter_21() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___fUpCounter_21)); }
	inline float get_fUpCounter_21() const { return ___fUpCounter_21; }
	inline float* get_address_of_fUpCounter_21() { return &___fUpCounter_21; }
	inline void set_fUpCounter_21(float value)
	{
		___fUpCounter_21 = value;
	}

	inline static int32_t get_offset_of_fTime_22() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___fTime_22)); }
	inline float get_fTime_22() const { return ___fTime_22; }
	inline float* get_address_of_fTime_22() { return &___fTime_22; }
	inline void set_fTime_22(float value)
	{
		___fTime_22 = value;
	}

	inline static int32_t get_offset_of_fStatusTimer_23() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___fStatusTimer_23)); }
	inline float get_fStatusTimer_23() const { return ___fStatusTimer_23; }
	inline float* get_address_of_fStatusTimer_23() { return &___fStatusTimer_23; }
	inline void set_fStatusTimer_23(float value)
	{
		___fStatusTimer_23 = value;
	}

	inline static int32_t get_offset_of_fireNext_24() { return static_cast<int32_t>(offsetof(Lv7KingOfGnats_t3383848971, ___fireNext_24)); }
	inline bool get_fireNext_24() const { return ___fireNext_24; }
	inline bool* get_address_of_fireNext_24() { return &___fireNext_24; }
	inline void set_fireNext_24(bool value)
	{
		___fireNext_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7KINGOFGNATS_T3383848971_H
#ifndef LV6GNATSCONV_T2053771584_H
#define LV6GNATSCONV_T2053771584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv6GnatsConv
struct  Lv6GnatsConv_t2053771584  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Lv6GnatsConv::bActive
	bool ___bActive_2;
	// System.Boolean Lv6GnatsConv::bAnsweredCorrectly
	bool ___bAnsweredCorrectly_3;
	// UnityEngine.Vector3 Lv6GnatsConv::FlyAwayPos
	Vector3_t1986933152  ___FlyAwayPos_4;

public:
	inline static int32_t get_offset_of_bActive_2() { return static_cast<int32_t>(offsetof(Lv6GnatsConv_t2053771584, ___bActive_2)); }
	inline bool get_bActive_2() const { return ___bActive_2; }
	inline bool* get_address_of_bActive_2() { return &___bActive_2; }
	inline void set_bActive_2(bool value)
	{
		___bActive_2 = value;
	}

	inline static int32_t get_offset_of_bAnsweredCorrectly_3() { return static_cast<int32_t>(offsetof(Lv6GnatsConv_t2053771584, ___bAnsweredCorrectly_3)); }
	inline bool get_bAnsweredCorrectly_3() const { return ___bAnsweredCorrectly_3; }
	inline bool* get_address_of_bAnsweredCorrectly_3() { return &___bAnsweredCorrectly_3; }
	inline void set_bAnsweredCorrectly_3(bool value)
	{
		___bAnsweredCorrectly_3 = value;
	}

	inline static int32_t get_offset_of_FlyAwayPos_4() { return static_cast<int32_t>(offsetof(Lv6GnatsConv_t2053771584, ___FlyAwayPos_4)); }
	inline Vector3_t1986933152  get_FlyAwayPos_4() const { return ___FlyAwayPos_4; }
	inline Vector3_t1986933152 * get_address_of_FlyAwayPos_4() { return &___FlyAwayPos_4; }
	inline void set_FlyAwayPos_4(Vector3_t1986933152  value)
	{
		___FlyAwayPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV6GNATSCONV_T2053771584_H
#ifndef LEVELTWOSCRIPT_T1243006306_H
#define LEVELTWOSCRIPT_T1243006306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelTwoScript
struct  LevelTwoScript_t1243006306  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32[] LevelTwoScript::NumOfLightsMeltIce
	Int32U5BU5D_t1965588061* ___NumOfLightsMeltIce_2;
	// System.Int32 LevelTwoScript::CurrentNumMeltedIce
	int32_t ___CurrentNumMeltedIce_3;
	// System.String LevelTwoScript::IceMeltCutSceneCamID
	String_t* ___IceMeltCutSceneCamID_4;
	// System.Boolean LevelTwoScript::m_bPlayCutScene
	bool ___m_bPlayCutScene_5;
	// System.Int32 LevelTwoScript::CurrentIceMeltIndex
	int32_t ___CurrentIceMeltIndex_6;
	// System.Single[] LevelTwoScript::IceBelowMeltPosY
	SingleU5BU5D_t2843050510* ___IceBelowMeltPosY_7;
	// System.Single[] LevelTwoScript::IceAboveMeltPosY
	SingleU5BU5D_t2843050510* ___IceAboveMeltPosY_8;
	// UnityEngine.GameObject LevelTwoScript::TopIce
	GameObject_t2557347079 * ___TopIce_9;
	// UnityEngine.GameObject LevelTwoScript::BottomIce
	GameObject_t2557347079 * ___BottomIce_10;
	// System.Boolean LevelTwoScript::m_hopeTalkFinished
	bool ___m_hopeTalkFinished_11;
	// UnityEngine.GameObject LevelTwoScript::Tui
	GameObject_t2557347079 * ___Tui_12;
	// System.Boolean LevelTwoScript::m_bTriggeredIceFall
	bool ___m_bTriggeredIceFall_13;
	// System.Int32 LevelTwoScript::m_iTriggeredCaveTui
	int32_t ___m_iTriggeredCaveTui_14;
	// System.Int32 LevelTwoScript::m_iTriggeredDoorTui
	int32_t ___m_iTriggeredDoorTui_15;

public:
	inline static int32_t get_offset_of_NumOfLightsMeltIce_2() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___NumOfLightsMeltIce_2)); }
	inline Int32U5BU5D_t1965588061* get_NumOfLightsMeltIce_2() const { return ___NumOfLightsMeltIce_2; }
	inline Int32U5BU5D_t1965588061** get_address_of_NumOfLightsMeltIce_2() { return &___NumOfLightsMeltIce_2; }
	inline void set_NumOfLightsMeltIce_2(Int32U5BU5D_t1965588061* value)
	{
		___NumOfLightsMeltIce_2 = value;
		Il2CppCodeGenWriteBarrier((&___NumOfLightsMeltIce_2), value);
	}

	inline static int32_t get_offset_of_CurrentNumMeltedIce_3() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___CurrentNumMeltedIce_3)); }
	inline int32_t get_CurrentNumMeltedIce_3() const { return ___CurrentNumMeltedIce_3; }
	inline int32_t* get_address_of_CurrentNumMeltedIce_3() { return &___CurrentNumMeltedIce_3; }
	inline void set_CurrentNumMeltedIce_3(int32_t value)
	{
		___CurrentNumMeltedIce_3 = value;
	}

	inline static int32_t get_offset_of_IceMeltCutSceneCamID_4() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___IceMeltCutSceneCamID_4)); }
	inline String_t* get_IceMeltCutSceneCamID_4() const { return ___IceMeltCutSceneCamID_4; }
	inline String_t** get_address_of_IceMeltCutSceneCamID_4() { return &___IceMeltCutSceneCamID_4; }
	inline void set_IceMeltCutSceneCamID_4(String_t* value)
	{
		___IceMeltCutSceneCamID_4 = value;
		Il2CppCodeGenWriteBarrier((&___IceMeltCutSceneCamID_4), value);
	}

	inline static int32_t get_offset_of_m_bPlayCutScene_5() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___m_bPlayCutScene_5)); }
	inline bool get_m_bPlayCutScene_5() const { return ___m_bPlayCutScene_5; }
	inline bool* get_address_of_m_bPlayCutScene_5() { return &___m_bPlayCutScene_5; }
	inline void set_m_bPlayCutScene_5(bool value)
	{
		___m_bPlayCutScene_5 = value;
	}

	inline static int32_t get_offset_of_CurrentIceMeltIndex_6() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___CurrentIceMeltIndex_6)); }
	inline int32_t get_CurrentIceMeltIndex_6() const { return ___CurrentIceMeltIndex_6; }
	inline int32_t* get_address_of_CurrentIceMeltIndex_6() { return &___CurrentIceMeltIndex_6; }
	inline void set_CurrentIceMeltIndex_6(int32_t value)
	{
		___CurrentIceMeltIndex_6 = value;
	}

	inline static int32_t get_offset_of_IceBelowMeltPosY_7() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___IceBelowMeltPosY_7)); }
	inline SingleU5BU5D_t2843050510* get_IceBelowMeltPosY_7() const { return ___IceBelowMeltPosY_7; }
	inline SingleU5BU5D_t2843050510** get_address_of_IceBelowMeltPosY_7() { return &___IceBelowMeltPosY_7; }
	inline void set_IceBelowMeltPosY_7(SingleU5BU5D_t2843050510* value)
	{
		___IceBelowMeltPosY_7 = value;
		Il2CppCodeGenWriteBarrier((&___IceBelowMeltPosY_7), value);
	}

	inline static int32_t get_offset_of_IceAboveMeltPosY_8() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___IceAboveMeltPosY_8)); }
	inline SingleU5BU5D_t2843050510* get_IceAboveMeltPosY_8() const { return ___IceAboveMeltPosY_8; }
	inline SingleU5BU5D_t2843050510** get_address_of_IceAboveMeltPosY_8() { return &___IceAboveMeltPosY_8; }
	inline void set_IceAboveMeltPosY_8(SingleU5BU5D_t2843050510* value)
	{
		___IceAboveMeltPosY_8 = value;
		Il2CppCodeGenWriteBarrier((&___IceAboveMeltPosY_8), value);
	}

	inline static int32_t get_offset_of_TopIce_9() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___TopIce_9)); }
	inline GameObject_t2557347079 * get_TopIce_9() const { return ___TopIce_9; }
	inline GameObject_t2557347079 ** get_address_of_TopIce_9() { return &___TopIce_9; }
	inline void set_TopIce_9(GameObject_t2557347079 * value)
	{
		___TopIce_9 = value;
		Il2CppCodeGenWriteBarrier((&___TopIce_9), value);
	}

	inline static int32_t get_offset_of_BottomIce_10() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___BottomIce_10)); }
	inline GameObject_t2557347079 * get_BottomIce_10() const { return ___BottomIce_10; }
	inline GameObject_t2557347079 ** get_address_of_BottomIce_10() { return &___BottomIce_10; }
	inline void set_BottomIce_10(GameObject_t2557347079 * value)
	{
		___BottomIce_10 = value;
		Il2CppCodeGenWriteBarrier((&___BottomIce_10), value);
	}

	inline static int32_t get_offset_of_m_hopeTalkFinished_11() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___m_hopeTalkFinished_11)); }
	inline bool get_m_hopeTalkFinished_11() const { return ___m_hopeTalkFinished_11; }
	inline bool* get_address_of_m_hopeTalkFinished_11() { return &___m_hopeTalkFinished_11; }
	inline void set_m_hopeTalkFinished_11(bool value)
	{
		___m_hopeTalkFinished_11 = value;
	}

	inline static int32_t get_offset_of_Tui_12() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___Tui_12)); }
	inline GameObject_t2557347079 * get_Tui_12() const { return ___Tui_12; }
	inline GameObject_t2557347079 ** get_address_of_Tui_12() { return &___Tui_12; }
	inline void set_Tui_12(GameObject_t2557347079 * value)
	{
		___Tui_12 = value;
		Il2CppCodeGenWriteBarrier((&___Tui_12), value);
	}

	inline static int32_t get_offset_of_m_bTriggeredIceFall_13() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___m_bTriggeredIceFall_13)); }
	inline bool get_m_bTriggeredIceFall_13() const { return ___m_bTriggeredIceFall_13; }
	inline bool* get_address_of_m_bTriggeredIceFall_13() { return &___m_bTriggeredIceFall_13; }
	inline void set_m_bTriggeredIceFall_13(bool value)
	{
		___m_bTriggeredIceFall_13 = value;
	}

	inline static int32_t get_offset_of_m_iTriggeredCaveTui_14() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___m_iTriggeredCaveTui_14)); }
	inline int32_t get_m_iTriggeredCaveTui_14() const { return ___m_iTriggeredCaveTui_14; }
	inline int32_t* get_address_of_m_iTriggeredCaveTui_14() { return &___m_iTriggeredCaveTui_14; }
	inline void set_m_iTriggeredCaveTui_14(int32_t value)
	{
		___m_iTriggeredCaveTui_14 = value;
	}

	inline static int32_t get_offset_of_m_iTriggeredDoorTui_15() { return static_cast<int32_t>(offsetof(LevelTwoScript_t1243006306, ___m_iTriggeredDoorTui_15)); }
	inline int32_t get_m_iTriggeredDoorTui_15() const { return ___m_iTriggeredDoorTui_15; }
	inline int32_t* get_address_of_m_iTriggeredDoorTui_15() { return &___m_iTriggeredDoorTui_15; }
	inline void set_m_iTriggeredDoorTui_15(int32_t value)
	{
		___m_iTriggeredDoorTui_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELTWOSCRIPT_T1243006306_H
#ifndef LEVELTHREESCRIPT_T992746369_H
#define LEVELTHREESCRIPT_T992746369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelThreeScript
struct  LevelThreeScript_t992746369  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform[] LevelThreeScript::Triggers
	TransformU5BU5D_t2383644165* ___Triggers_2;
	// UnityEngine.GameObject[] LevelThreeScript::MiniGames
	GameObjectU5BU5D_t2988620542* ___MiniGames_3;

public:
	inline static int32_t get_offset_of_Triggers_2() { return static_cast<int32_t>(offsetof(LevelThreeScript_t992746369, ___Triggers_2)); }
	inline TransformU5BU5D_t2383644165* get_Triggers_2() const { return ___Triggers_2; }
	inline TransformU5BU5D_t2383644165** get_address_of_Triggers_2() { return &___Triggers_2; }
	inline void set_Triggers_2(TransformU5BU5D_t2383644165* value)
	{
		___Triggers_2 = value;
		Il2CppCodeGenWriteBarrier((&___Triggers_2), value);
	}

	inline static int32_t get_offset_of_MiniGames_3() { return static_cast<int32_t>(offsetof(LevelThreeScript_t992746369, ___MiniGames_3)); }
	inline GameObjectU5BU5D_t2988620542* get_MiniGames_3() const { return ___MiniGames_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_MiniGames_3() { return &___MiniGames_3; }
	inline void set_MiniGames_3(GameObjectU5BU5D_t2988620542* value)
	{
		___MiniGames_3 = value;
		Il2CppCodeGenWriteBarrier((&___MiniGames_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELTHREESCRIPT_T992746369_H
#ifndef LEVELSSCRIPT_T2908075387_H
#define LEVELSSCRIPT_T2908075387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSScript
struct  LevelSScript_t2908075387  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LevelSScript::m_bStartSetupComplete
	bool ___m_bStartSetupComplete_2;
	// EagleReturnToLvS LevelSScript::status
	int32_t ___status_3;
	// UnityEngine.Transform LevelSScript::SeatPosition
	Transform_t362059596 * ___SeatPosition_4;
	// UnityEngine.Transform LevelSScript::StationaryPosition
	Transform_t362059596 * ___StationaryPosition_5;
	// UnityEngine.Transform LevelSScript::FlyFrom
	Transform_t362059596 * ___FlyFrom_6;
	// UnityEngine.Transform LevelSScript::PlayerPos
	Transform_t362059596 * ___PlayerPos_7;
	// UnityEngine.GameObject LevelSScript::Eagle
	GameObject_t2557347079 * ___Eagle_8;
	// UnityEngine.Vector3 LevelSScript::start
	Vector3_t1986933152  ___start_9;
	// System.Single LevelSScript::PauseTimer
	float ___PauseTimer_10;
	// System.Single LevelSScript::m_fTime
	float ___m_fTime_11;
	// UnityEngine.GameObject LevelSScript::m_gNPCsOnLevels
	GameObject_t2557347079 * ___m_gNPCsOnLevels_12;
	// UnityEngine.Texture2D LevelSScript::m_tTrimTexture
	Texture2D_t3063074017 * ___m_tTrimTexture_13;

public:
	inline static int32_t get_offset_of_m_bStartSetupComplete_2() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___m_bStartSetupComplete_2)); }
	inline bool get_m_bStartSetupComplete_2() const { return ___m_bStartSetupComplete_2; }
	inline bool* get_address_of_m_bStartSetupComplete_2() { return &___m_bStartSetupComplete_2; }
	inline void set_m_bStartSetupComplete_2(bool value)
	{
		___m_bStartSetupComplete_2 = value;
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___status_3)); }
	inline int32_t get_status_3() const { return ___status_3; }
	inline int32_t* get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(int32_t value)
	{
		___status_3 = value;
	}

	inline static int32_t get_offset_of_SeatPosition_4() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___SeatPosition_4)); }
	inline Transform_t362059596 * get_SeatPosition_4() const { return ___SeatPosition_4; }
	inline Transform_t362059596 ** get_address_of_SeatPosition_4() { return &___SeatPosition_4; }
	inline void set_SeatPosition_4(Transform_t362059596 * value)
	{
		___SeatPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___SeatPosition_4), value);
	}

	inline static int32_t get_offset_of_StationaryPosition_5() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___StationaryPosition_5)); }
	inline Transform_t362059596 * get_StationaryPosition_5() const { return ___StationaryPosition_5; }
	inline Transform_t362059596 ** get_address_of_StationaryPosition_5() { return &___StationaryPosition_5; }
	inline void set_StationaryPosition_5(Transform_t362059596 * value)
	{
		___StationaryPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&___StationaryPosition_5), value);
	}

	inline static int32_t get_offset_of_FlyFrom_6() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___FlyFrom_6)); }
	inline Transform_t362059596 * get_FlyFrom_6() const { return ___FlyFrom_6; }
	inline Transform_t362059596 ** get_address_of_FlyFrom_6() { return &___FlyFrom_6; }
	inline void set_FlyFrom_6(Transform_t362059596 * value)
	{
		___FlyFrom_6 = value;
		Il2CppCodeGenWriteBarrier((&___FlyFrom_6), value);
	}

	inline static int32_t get_offset_of_PlayerPos_7() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___PlayerPos_7)); }
	inline Transform_t362059596 * get_PlayerPos_7() const { return ___PlayerPos_7; }
	inline Transform_t362059596 ** get_address_of_PlayerPos_7() { return &___PlayerPos_7; }
	inline void set_PlayerPos_7(Transform_t362059596 * value)
	{
		___PlayerPos_7 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerPos_7), value);
	}

	inline static int32_t get_offset_of_Eagle_8() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___Eagle_8)); }
	inline GameObject_t2557347079 * get_Eagle_8() const { return ___Eagle_8; }
	inline GameObject_t2557347079 ** get_address_of_Eagle_8() { return &___Eagle_8; }
	inline void set_Eagle_8(GameObject_t2557347079 * value)
	{
		___Eagle_8 = value;
		Il2CppCodeGenWriteBarrier((&___Eagle_8), value);
	}

	inline static int32_t get_offset_of_start_9() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___start_9)); }
	inline Vector3_t1986933152  get_start_9() const { return ___start_9; }
	inline Vector3_t1986933152 * get_address_of_start_9() { return &___start_9; }
	inline void set_start_9(Vector3_t1986933152  value)
	{
		___start_9 = value;
	}

	inline static int32_t get_offset_of_PauseTimer_10() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___PauseTimer_10)); }
	inline float get_PauseTimer_10() const { return ___PauseTimer_10; }
	inline float* get_address_of_PauseTimer_10() { return &___PauseTimer_10; }
	inline void set_PauseTimer_10(float value)
	{
		___PauseTimer_10 = value;
	}

	inline static int32_t get_offset_of_m_fTime_11() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___m_fTime_11)); }
	inline float get_m_fTime_11() const { return ___m_fTime_11; }
	inline float* get_address_of_m_fTime_11() { return &___m_fTime_11; }
	inline void set_m_fTime_11(float value)
	{
		___m_fTime_11 = value;
	}

	inline static int32_t get_offset_of_m_gNPCsOnLevels_12() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___m_gNPCsOnLevels_12)); }
	inline GameObject_t2557347079 * get_m_gNPCsOnLevels_12() const { return ___m_gNPCsOnLevels_12; }
	inline GameObject_t2557347079 ** get_address_of_m_gNPCsOnLevels_12() { return &___m_gNPCsOnLevels_12; }
	inline void set_m_gNPCsOnLevels_12(GameObject_t2557347079 * value)
	{
		___m_gNPCsOnLevels_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_gNPCsOnLevels_12), value);
	}

	inline static int32_t get_offset_of_m_tTrimTexture_13() { return static_cast<int32_t>(offsetof(LevelSScript_t2908075387, ___m_tTrimTexture_13)); }
	inline Texture2D_t3063074017 * get_m_tTrimTexture_13() const { return ___m_tTrimTexture_13; }
	inline Texture2D_t3063074017 ** get_address_of_m_tTrimTexture_13() { return &___m_tTrimTexture_13; }
	inline void set_m_tTrimTexture_13(Texture2D_t3063074017 * value)
	{
		___m_tTrimTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_tTrimTexture_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSSCRIPT_T2908075387_H
#ifndef LEVELSIXSCRIPT_T1926494436_H
#define LEVELSIXSCRIPT_T1926494436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSixScript
struct  LevelSixScript_t1926494436  : public MonoBehaviour_t1618594486
{
public:
	// Lv6OpenChestCutScene LevelSixScript::cutscene
	int32_t ___cutscene_2;
	// UnityEngine.GameObject LevelSixScript::Trigger
	GameObject_t2557347079 * ___Trigger_3;
	// System.Boolean LevelSixScript::DoneWithGuard
	bool ___DoneWithGuard_4;
	// System.Boolean LevelSixScript::m_bTriggerFirstDialog
	bool ___m_bTriggerFirstDialog_5;
	// UnityEngine.GameObject LevelSixScript::NPC1
	GameObject_t2557347079 * ___NPC1_6;
	// UnityEngine.GameObject[] LevelSixScript::MiniGames
	GameObjectU5BU5D_t2988620542* ___MiniGames_7;
	// UnityEngine.GameObject[] LevelSixScript::Doors
	GameObjectU5BU5D_t2988620542* ___Doors_8;
	// System.Int32 LevelSixScript::iCurrentMiniGameIndex
	int32_t ___iCurrentMiniGameIndex_9;
	// UnityEngine.GameObject[] LevelSixScript::MiniGameTriggers
	GameObjectU5BU5D_t2988620542* ___MiniGameTriggers_10;
	// System.Boolean LevelSixScript::bCheckForPlayingGame
	bool ___bCheckForPlayingGame_11;
	// UnityEngine.GameObject LevelSixScript::MiniGame
	GameObject_t2557347079 * ___MiniGame_12;
	// UnityEngine.GameObject LevelSixScript::Eagle
	GameObject_t2557347079 * ___Eagle_13;
	// UnityEngine.GameObject LevelSixScript::Gem
	GameObject_t2557347079 * ___Gem_14;
	// System.Boolean LevelSixScript::m_bPlayerInfrontOfChest
	bool ___m_bPlayerInfrontOfChest_15;

public:
	inline static int32_t get_offset_of_cutscene_2() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___cutscene_2)); }
	inline int32_t get_cutscene_2() const { return ___cutscene_2; }
	inline int32_t* get_address_of_cutscene_2() { return &___cutscene_2; }
	inline void set_cutscene_2(int32_t value)
	{
		___cutscene_2 = value;
	}

	inline static int32_t get_offset_of_Trigger_3() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___Trigger_3)); }
	inline GameObject_t2557347079 * get_Trigger_3() const { return ___Trigger_3; }
	inline GameObject_t2557347079 ** get_address_of_Trigger_3() { return &___Trigger_3; }
	inline void set_Trigger_3(GameObject_t2557347079 * value)
	{
		___Trigger_3 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_3), value);
	}

	inline static int32_t get_offset_of_DoneWithGuard_4() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___DoneWithGuard_4)); }
	inline bool get_DoneWithGuard_4() const { return ___DoneWithGuard_4; }
	inline bool* get_address_of_DoneWithGuard_4() { return &___DoneWithGuard_4; }
	inline void set_DoneWithGuard_4(bool value)
	{
		___DoneWithGuard_4 = value;
	}

	inline static int32_t get_offset_of_m_bTriggerFirstDialog_5() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___m_bTriggerFirstDialog_5)); }
	inline bool get_m_bTriggerFirstDialog_5() const { return ___m_bTriggerFirstDialog_5; }
	inline bool* get_address_of_m_bTriggerFirstDialog_5() { return &___m_bTriggerFirstDialog_5; }
	inline void set_m_bTriggerFirstDialog_5(bool value)
	{
		___m_bTriggerFirstDialog_5 = value;
	}

	inline static int32_t get_offset_of_NPC1_6() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___NPC1_6)); }
	inline GameObject_t2557347079 * get_NPC1_6() const { return ___NPC1_6; }
	inline GameObject_t2557347079 ** get_address_of_NPC1_6() { return &___NPC1_6; }
	inline void set_NPC1_6(GameObject_t2557347079 * value)
	{
		___NPC1_6 = value;
		Il2CppCodeGenWriteBarrier((&___NPC1_6), value);
	}

	inline static int32_t get_offset_of_MiniGames_7() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___MiniGames_7)); }
	inline GameObjectU5BU5D_t2988620542* get_MiniGames_7() const { return ___MiniGames_7; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_MiniGames_7() { return &___MiniGames_7; }
	inline void set_MiniGames_7(GameObjectU5BU5D_t2988620542* value)
	{
		___MiniGames_7 = value;
		Il2CppCodeGenWriteBarrier((&___MiniGames_7), value);
	}

	inline static int32_t get_offset_of_Doors_8() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___Doors_8)); }
	inline GameObjectU5BU5D_t2988620542* get_Doors_8() const { return ___Doors_8; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Doors_8() { return &___Doors_8; }
	inline void set_Doors_8(GameObjectU5BU5D_t2988620542* value)
	{
		___Doors_8 = value;
		Il2CppCodeGenWriteBarrier((&___Doors_8), value);
	}

	inline static int32_t get_offset_of_iCurrentMiniGameIndex_9() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___iCurrentMiniGameIndex_9)); }
	inline int32_t get_iCurrentMiniGameIndex_9() const { return ___iCurrentMiniGameIndex_9; }
	inline int32_t* get_address_of_iCurrentMiniGameIndex_9() { return &___iCurrentMiniGameIndex_9; }
	inline void set_iCurrentMiniGameIndex_9(int32_t value)
	{
		___iCurrentMiniGameIndex_9 = value;
	}

	inline static int32_t get_offset_of_MiniGameTriggers_10() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___MiniGameTriggers_10)); }
	inline GameObjectU5BU5D_t2988620542* get_MiniGameTriggers_10() const { return ___MiniGameTriggers_10; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_MiniGameTriggers_10() { return &___MiniGameTriggers_10; }
	inline void set_MiniGameTriggers_10(GameObjectU5BU5D_t2988620542* value)
	{
		___MiniGameTriggers_10 = value;
		Il2CppCodeGenWriteBarrier((&___MiniGameTriggers_10), value);
	}

	inline static int32_t get_offset_of_bCheckForPlayingGame_11() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___bCheckForPlayingGame_11)); }
	inline bool get_bCheckForPlayingGame_11() const { return ___bCheckForPlayingGame_11; }
	inline bool* get_address_of_bCheckForPlayingGame_11() { return &___bCheckForPlayingGame_11; }
	inline void set_bCheckForPlayingGame_11(bool value)
	{
		___bCheckForPlayingGame_11 = value;
	}

	inline static int32_t get_offset_of_MiniGame_12() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___MiniGame_12)); }
	inline GameObject_t2557347079 * get_MiniGame_12() const { return ___MiniGame_12; }
	inline GameObject_t2557347079 ** get_address_of_MiniGame_12() { return &___MiniGame_12; }
	inline void set_MiniGame_12(GameObject_t2557347079 * value)
	{
		___MiniGame_12 = value;
		Il2CppCodeGenWriteBarrier((&___MiniGame_12), value);
	}

	inline static int32_t get_offset_of_Eagle_13() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___Eagle_13)); }
	inline GameObject_t2557347079 * get_Eagle_13() const { return ___Eagle_13; }
	inline GameObject_t2557347079 ** get_address_of_Eagle_13() { return &___Eagle_13; }
	inline void set_Eagle_13(GameObject_t2557347079 * value)
	{
		___Eagle_13 = value;
		Il2CppCodeGenWriteBarrier((&___Eagle_13), value);
	}

	inline static int32_t get_offset_of_Gem_14() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___Gem_14)); }
	inline GameObject_t2557347079 * get_Gem_14() const { return ___Gem_14; }
	inline GameObject_t2557347079 ** get_address_of_Gem_14() { return &___Gem_14; }
	inline void set_Gem_14(GameObject_t2557347079 * value)
	{
		___Gem_14 = value;
		Il2CppCodeGenWriteBarrier((&___Gem_14), value);
	}

	inline static int32_t get_offset_of_m_bPlayerInfrontOfChest_15() { return static_cast<int32_t>(offsetof(LevelSixScript_t1926494436, ___m_bPlayerInfrontOfChest_15)); }
	inline bool get_m_bPlayerInfrontOfChest_15() const { return ___m_bPlayerInfrontOfChest_15; }
	inline bool* get_address_of_m_bPlayerInfrontOfChest_15() { return &___m_bPlayerInfrontOfChest_15; }
	inline void set_m_bPlayerInfrontOfChest_15(bool value)
	{
		___m_bPlayerInfrontOfChest_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSIXSCRIPT_T1926494436_H
#ifndef LEVELSEVENSCRIPT_T1159386256_H
#define LEVELSEVENSCRIPT_T1159386256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSevenScript
struct  LevelSevenScript_t1159386256  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] LevelSevenScript::DoorOne
	GameObjectU5BU5D_t2988620542* ___DoorOne_2;
	// UnityEngine.GameObject[] LevelSevenScript::DoorTwo
	GameObjectU5BU5D_t2988620542* ___DoorTwo_3;
	// UnityEngine.GameObject[] LevelSevenScript::NPCs
	GameObjectU5BU5D_t2988620542* ___NPCs_4;
	// System.Boolean LevelSevenScript::m_bCassFirstTalkFinished
	bool ___m_bCassFirstTalkFinished_5;
	// System.Boolean LevelSevenScript::m_bPickSparkStarted
	bool ___m_bPickSparkStarted_6;
	// System.Boolean LevelSevenScript::StartGetHelpTimer
	bool ___StartGetHelpTimer_8;
	// System.Single LevelSevenScript::fTimeForTen
	float ___fTimeForTen_9;
	// System.Boolean LevelSevenScript::bPlayedBreath
	bool ___bPlayedBreath_10;
	// System.Boolean LevelSevenScript::m_bOpenFirstDoor
	bool ___m_bOpenFirstDoor_11;
	// System.Boolean LevelSevenScript::FinishedOpenningDoorOne
	bool ___FinishedOpenningDoorOne_12;
	// System.Boolean LevelSevenScript::StartMiniGame
	bool ___StartMiniGame_13;
	// System.Boolean LevelSevenScript::StartHelpConversation
	bool ___StartHelpConversation_14;
	// UnityEngine.GameObject LevelSevenScript::Barrel
	GameObject_t2557347079 * ___Barrel_15;
	// System.Boolean LevelSevenScript::m_bRotatePlayerOnce
	bool ___m_bRotatePlayerOnce_16;
	// System.Boolean LevelSevenScript::m_bPlayerAcceptHelp
	bool ___m_bPlayerAcceptHelp_17;
	// System.Boolean LevelSevenScript::m_bPlayerWayToMiniGame
	bool ___m_bPlayerWayToMiniGame_18;
	// System.Boolean LevelSevenScript::m_bMoveTotargetCallOnce
	bool ___m_bMoveTotargetCallOnce_19;
	// System.Boolean LevelSevenScript::bForceMovementAfterHelpGame
	bool ___bForceMovementAfterHelpGame_20;

public:
	inline static int32_t get_offset_of_DoorOne_2() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___DoorOne_2)); }
	inline GameObjectU5BU5D_t2988620542* get_DoorOne_2() const { return ___DoorOne_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_DoorOne_2() { return &___DoorOne_2; }
	inline void set_DoorOne_2(GameObjectU5BU5D_t2988620542* value)
	{
		___DoorOne_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoorOne_2), value);
	}

	inline static int32_t get_offset_of_DoorTwo_3() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___DoorTwo_3)); }
	inline GameObjectU5BU5D_t2988620542* get_DoorTwo_3() const { return ___DoorTwo_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_DoorTwo_3() { return &___DoorTwo_3; }
	inline void set_DoorTwo_3(GameObjectU5BU5D_t2988620542* value)
	{
		___DoorTwo_3 = value;
		Il2CppCodeGenWriteBarrier((&___DoorTwo_3), value);
	}

	inline static int32_t get_offset_of_NPCs_4() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___NPCs_4)); }
	inline GameObjectU5BU5D_t2988620542* get_NPCs_4() const { return ___NPCs_4; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_NPCs_4() { return &___NPCs_4; }
	inline void set_NPCs_4(GameObjectU5BU5D_t2988620542* value)
	{
		___NPCs_4 = value;
		Il2CppCodeGenWriteBarrier((&___NPCs_4), value);
	}

	inline static int32_t get_offset_of_m_bCassFirstTalkFinished_5() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bCassFirstTalkFinished_5)); }
	inline bool get_m_bCassFirstTalkFinished_5() const { return ___m_bCassFirstTalkFinished_5; }
	inline bool* get_address_of_m_bCassFirstTalkFinished_5() { return &___m_bCassFirstTalkFinished_5; }
	inline void set_m_bCassFirstTalkFinished_5(bool value)
	{
		___m_bCassFirstTalkFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_bPickSparkStarted_6() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bPickSparkStarted_6)); }
	inline bool get_m_bPickSparkStarted_6() const { return ___m_bPickSparkStarted_6; }
	inline bool* get_address_of_m_bPickSparkStarted_6() { return &___m_bPickSparkStarted_6; }
	inline void set_m_bPickSparkStarted_6(bool value)
	{
		___m_bPickSparkStarted_6 = value;
	}

	inline static int32_t get_offset_of_StartGetHelpTimer_8() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___StartGetHelpTimer_8)); }
	inline bool get_StartGetHelpTimer_8() const { return ___StartGetHelpTimer_8; }
	inline bool* get_address_of_StartGetHelpTimer_8() { return &___StartGetHelpTimer_8; }
	inline void set_StartGetHelpTimer_8(bool value)
	{
		___StartGetHelpTimer_8 = value;
	}

	inline static int32_t get_offset_of_fTimeForTen_9() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___fTimeForTen_9)); }
	inline float get_fTimeForTen_9() const { return ___fTimeForTen_9; }
	inline float* get_address_of_fTimeForTen_9() { return &___fTimeForTen_9; }
	inline void set_fTimeForTen_9(float value)
	{
		___fTimeForTen_9 = value;
	}

	inline static int32_t get_offset_of_bPlayedBreath_10() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___bPlayedBreath_10)); }
	inline bool get_bPlayedBreath_10() const { return ___bPlayedBreath_10; }
	inline bool* get_address_of_bPlayedBreath_10() { return &___bPlayedBreath_10; }
	inline void set_bPlayedBreath_10(bool value)
	{
		___bPlayedBreath_10 = value;
	}

	inline static int32_t get_offset_of_m_bOpenFirstDoor_11() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bOpenFirstDoor_11)); }
	inline bool get_m_bOpenFirstDoor_11() const { return ___m_bOpenFirstDoor_11; }
	inline bool* get_address_of_m_bOpenFirstDoor_11() { return &___m_bOpenFirstDoor_11; }
	inline void set_m_bOpenFirstDoor_11(bool value)
	{
		___m_bOpenFirstDoor_11 = value;
	}

	inline static int32_t get_offset_of_FinishedOpenningDoorOne_12() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___FinishedOpenningDoorOne_12)); }
	inline bool get_FinishedOpenningDoorOne_12() const { return ___FinishedOpenningDoorOne_12; }
	inline bool* get_address_of_FinishedOpenningDoorOne_12() { return &___FinishedOpenningDoorOne_12; }
	inline void set_FinishedOpenningDoorOne_12(bool value)
	{
		___FinishedOpenningDoorOne_12 = value;
	}

	inline static int32_t get_offset_of_StartMiniGame_13() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___StartMiniGame_13)); }
	inline bool get_StartMiniGame_13() const { return ___StartMiniGame_13; }
	inline bool* get_address_of_StartMiniGame_13() { return &___StartMiniGame_13; }
	inline void set_StartMiniGame_13(bool value)
	{
		___StartMiniGame_13 = value;
	}

	inline static int32_t get_offset_of_StartHelpConversation_14() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___StartHelpConversation_14)); }
	inline bool get_StartHelpConversation_14() const { return ___StartHelpConversation_14; }
	inline bool* get_address_of_StartHelpConversation_14() { return &___StartHelpConversation_14; }
	inline void set_StartHelpConversation_14(bool value)
	{
		___StartHelpConversation_14 = value;
	}

	inline static int32_t get_offset_of_Barrel_15() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___Barrel_15)); }
	inline GameObject_t2557347079 * get_Barrel_15() const { return ___Barrel_15; }
	inline GameObject_t2557347079 ** get_address_of_Barrel_15() { return &___Barrel_15; }
	inline void set_Barrel_15(GameObject_t2557347079 * value)
	{
		___Barrel_15 = value;
		Il2CppCodeGenWriteBarrier((&___Barrel_15), value);
	}

	inline static int32_t get_offset_of_m_bRotatePlayerOnce_16() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bRotatePlayerOnce_16)); }
	inline bool get_m_bRotatePlayerOnce_16() const { return ___m_bRotatePlayerOnce_16; }
	inline bool* get_address_of_m_bRotatePlayerOnce_16() { return &___m_bRotatePlayerOnce_16; }
	inline void set_m_bRotatePlayerOnce_16(bool value)
	{
		___m_bRotatePlayerOnce_16 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerAcceptHelp_17() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bPlayerAcceptHelp_17)); }
	inline bool get_m_bPlayerAcceptHelp_17() const { return ___m_bPlayerAcceptHelp_17; }
	inline bool* get_address_of_m_bPlayerAcceptHelp_17() { return &___m_bPlayerAcceptHelp_17; }
	inline void set_m_bPlayerAcceptHelp_17(bool value)
	{
		___m_bPlayerAcceptHelp_17 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerWayToMiniGame_18() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bPlayerWayToMiniGame_18)); }
	inline bool get_m_bPlayerWayToMiniGame_18() const { return ___m_bPlayerWayToMiniGame_18; }
	inline bool* get_address_of_m_bPlayerWayToMiniGame_18() { return &___m_bPlayerWayToMiniGame_18; }
	inline void set_m_bPlayerWayToMiniGame_18(bool value)
	{
		___m_bPlayerWayToMiniGame_18 = value;
	}

	inline static int32_t get_offset_of_m_bMoveTotargetCallOnce_19() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___m_bMoveTotargetCallOnce_19)); }
	inline bool get_m_bMoveTotargetCallOnce_19() const { return ___m_bMoveTotargetCallOnce_19; }
	inline bool* get_address_of_m_bMoveTotargetCallOnce_19() { return &___m_bMoveTotargetCallOnce_19; }
	inline void set_m_bMoveTotargetCallOnce_19(bool value)
	{
		___m_bMoveTotargetCallOnce_19 = value;
	}

	inline static int32_t get_offset_of_bForceMovementAfterHelpGame_20() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256, ___bForceMovementAfterHelpGame_20)); }
	inline bool get_bForceMovementAfterHelpGame_20() const { return ___bForceMovementAfterHelpGame_20; }
	inline bool* get_address_of_bForceMovementAfterHelpGame_20() { return &___bForceMovementAfterHelpGame_20; }
	inline void set_bForceMovementAfterHelpGame_20(bool value)
	{
		___bForceMovementAfterHelpGame_20 = value;
	}
};

struct LevelSevenScript_t1159386256_StaticFields
{
public:
	// System.Int32 LevelSevenScript::iSparxCaught
	int32_t ___iSparxCaught_7;

public:
	inline static int32_t get_offset_of_iSparxCaught_7() { return static_cast<int32_t>(offsetof(LevelSevenScript_t1159386256_StaticFields, ___iSparxCaught_7)); }
	inline int32_t get_iSparxCaught_7() const { return ___iSparxCaught_7; }
	inline int32_t* get_address_of_iSparxCaught_7() { return &___iSparxCaught_7; }
	inline void set_iSparxCaught_7(int32_t value)
	{
		___iSparxCaught_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSEVENSCRIPT_T1159386256_H
#ifndef LEVELONESCRIPT_T2347138676_H
#define LEVELONESCRIPT_T2347138676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelOneScript
struct  LevelOneScript_t2347138676  : public MonoBehaviour_t1618594486
{
public:

public:
};

struct LevelOneScript_t2347138676_StaticFields
{
public:
	// LevelOneScript LevelOneScript::instanceRef
	LevelOneScript_t2347138676 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(LevelOneScript_t2347138676_StaticFields, ___instanceRef_2)); }
	inline LevelOneScript_t2347138676 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline LevelOneScript_t2347138676 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(LevelOneScript_t2347138676 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELONESCRIPT_T2347138676_H
#ifndef LEVELFOURSCRIPT_T3944070347_H
#define LEVELFOURSCRIPT_T3944070347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelFourScript
struct  LevelFourScript_t3944070347  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LevelFourScript::m_bGnatsDefeated
	bool ___m_bGnatsDefeated_2;

public:
	inline static int32_t get_offset_of_m_bGnatsDefeated_2() { return static_cast<int32_t>(offsetof(LevelFourScript_t3944070347, ___m_bGnatsDefeated_2)); }
	inline bool get_m_bGnatsDefeated_2() const { return ___m_bGnatsDefeated_2; }
	inline bool* get_address_of_m_bGnatsDefeated_2() { return &___m_bGnatsDefeated_2; }
	inline void set_m_bGnatsDefeated_2(bool value)
	{
		___m_bGnatsDefeated_2 = value;
	}
};

struct LevelFourScript_t3944070347_StaticFields
{
public:
	// System.Single LevelFourScript::PlayerHeight
	float ___PlayerHeight_3;
	// System.Single LevelFourScript::m_fClimbSpeed
	float ___m_fClimbSpeed_4;

public:
	inline static int32_t get_offset_of_PlayerHeight_3() { return static_cast<int32_t>(offsetof(LevelFourScript_t3944070347_StaticFields, ___PlayerHeight_3)); }
	inline float get_PlayerHeight_3() const { return ___PlayerHeight_3; }
	inline float* get_address_of_PlayerHeight_3() { return &___PlayerHeight_3; }
	inline void set_PlayerHeight_3(float value)
	{
		___PlayerHeight_3 = value;
	}

	inline static int32_t get_offset_of_m_fClimbSpeed_4() { return static_cast<int32_t>(offsetof(LevelFourScript_t3944070347_StaticFields, ___m_fClimbSpeed_4)); }
	inline float get_m_fClimbSpeed_4() const { return ___m_fClimbSpeed_4; }
	inline float* get_address_of_m_fClimbSpeed_4() { return &___m_fClimbSpeed_4; }
	inline void set_m_fClimbSpeed_4(float value)
	{
		___m_fClimbSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELFOURSCRIPT_T3944070347_H
#ifndef LEVELFIVESCRIPT_T2930300399_H
#define LEVELFIVESCRIPT_T2930300399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelFiveScript
struct  LevelFiveScript_t2930300399  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LevelFiveScript::bStartDoorOpen
	bool ___bStartDoorOpen_2;

public:
	inline static int32_t get_offset_of_bStartDoorOpen_2() { return static_cast<int32_t>(offsetof(LevelFiveScript_t2930300399, ___bStartDoorOpen_2)); }
	inline bool get_bStartDoorOpen_2() const { return ___bStartDoorOpen_2; }
	inline bool* get_address_of_bStartDoorOpen_2() { return &___bStartDoorOpen_2; }
	inline void set_bStartDoorOpen_2(bool value)
	{
		___bStartDoorOpen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELFIVESCRIPT_T2930300399_H
#ifndef LV7CLIFFCUTSCENE_T2618734545_H
#define LV7CLIFFCUTSCENE_T2618734545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7CliffCutScene
struct  Lv7CliffCutScene_t2618734545  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] Lv7CliffCutScene::Sparks
	GameObjectU5BU5D_t2988620542* ___Sparks_2;
	// UnityEngine.Transform[] Lv7CliffCutScene::SparksPosition
	TransformU5BU5D_t2383644165* ___SparksPosition_3;
	// Lv7CliffScene Lv7CliffCutScene::SceneStatus
	int32_t ___SceneStatus_4;
	// UnityEngine.Transform[] Lv7CliffCutScene::TopLadderPos
	TransformU5BU5D_t2383644165* ___TopLadderPos_5;
	// UnityEngine.Transform[] Lv7CliffCutScene::BottomLadderPos
	TransformU5BU5D_t2383644165* ___BottomLadderPos_6;
	// System.Boolean Lv7CliffCutScene::m_hopeTalkFinished
	bool ___m_hopeTalkFinished_7;
	// System.Single Lv7CliffCutScene::m_fTime
	float ___m_fTime_8;
	// UnityEngine.Vector3 Lv7CliffCutScene::start
	Vector3_t1986933152  ___start_9;

public:
	inline static int32_t get_offset_of_Sparks_2() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___Sparks_2)); }
	inline GameObjectU5BU5D_t2988620542* get_Sparks_2() const { return ___Sparks_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Sparks_2() { return &___Sparks_2; }
	inline void set_Sparks_2(GameObjectU5BU5D_t2988620542* value)
	{
		___Sparks_2 = value;
		Il2CppCodeGenWriteBarrier((&___Sparks_2), value);
	}

	inline static int32_t get_offset_of_SparksPosition_3() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___SparksPosition_3)); }
	inline TransformU5BU5D_t2383644165* get_SparksPosition_3() const { return ___SparksPosition_3; }
	inline TransformU5BU5D_t2383644165** get_address_of_SparksPosition_3() { return &___SparksPosition_3; }
	inline void set_SparksPosition_3(TransformU5BU5D_t2383644165* value)
	{
		___SparksPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___SparksPosition_3), value);
	}

	inline static int32_t get_offset_of_SceneStatus_4() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___SceneStatus_4)); }
	inline int32_t get_SceneStatus_4() const { return ___SceneStatus_4; }
	inline int32_t* get_address_of_SceneStatus_4() { return &___SceneStatus_4; }
	inline void set_SceneStatus_4(int32_t value)
	{
		___SceneStatus_4 = value;
	}

	inline static int32_t get_offset_of_TopLadderPos_5() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___TopLadderPos_5)); }
	inline TransformU5BU5D_t2383644165* get_TopLadderPos_5() const { return ___TopLadderPos_5; }
	inline TransformU5BU5D_t2383644165** get_address_of_TopLadderPos_5() { return &___TopLadderPos_5; }
	inline void set_TopLadderPos_5(TransformU5BU5D_t2383644165* value)
	{
		___TopLadderPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___TopLadderPos_5), value);
	}

	inline static int32_t get_offset_of_BottomLadderPos_6() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___BottomLadderPos_6)); }
	inline TransformU5BU5D_t2383644165* get_BottomLadderPos_6() const { return ___BottomLadderPos_6; }
	inline TransformU5BU5D_t2383644165** get_address_of_BottomLadderPos_6() { return &___BottomLadderPos_6; }
	inline void set_BottomLadderPos_6(TransformU5BU5D_t2383644165* value)
	{
		___BottomLadderPos_6 = value;
		Il2CppCodeGenWriteBarrier((&___BottomLadderPos_6), value);
	}

	inline static int32_t get_offset_of_m_hopeTalkFinished_7() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___m_hopeTalkFinished_7)); }
	inline bool get_m_hopeTalkFinished_7() const { return ___m_hopeTalkFinished_7; }
	inline bool* get_address_of_m_hopeTalkFinished_7() { return &___m_hopeTalkFinished_7; }
	inline void set_m_hopeTalkFinished_7(bool value)
	{
		___m_hopeTalkFinished_7 = value;
	}

	inline static int32_t get_offset_of_m_fTime_8() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___m_fTime_8)); }
	inline float get_m_fTime_8() const { return ___m_fTime_8; }
	inline float* get_address_of_m_fTime_8() { return &___m_fTime_8; }
	inline void set_m_fTime_8(float value)
	{
		___m_fTime_8 = value;
	}

	inline static int32_t get_offset_of_start_9() { return static_cast<int32_t>(offsetof(Lv7CliffCutScene_t2618734545, ___start_9)); }
	inline Vector3_t1986933152  get_start_9() const { return ___start_9; }
	inline Vector3_t1986933152 * get_address_of_start_9() { return &___start_9; }
	inline void set_start_9(Vector3_t1986933152  value)
	{
		___start_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7CLIFFCUTSCENE_T2618734545_H
#ifndef FLYINGSPARX_T1256167344_H
#define FLYINGSPARX_T1256167344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlyingSparx
struct  FlyingSparx_t1256167344  : public MonoBehaviour_t1618594486
{
public:
	// System.Single FlyingSparx::fSpawnDelay
	float ___fSpawnDelay_2;
	// UnityEngine.Vector3 FlyingSparx::Target
	Vector3_t1986933152  ___Target_3;
	// System.Boolean FlyingSparx::bSpawn
	bool ___bSpawn_4;
	// System.Boolean FlyingSparx::bFlying
	bool ___bFlying_5;

public:
	inline static int32_t get_offset_of_fSpawnDelay_2() { return static_cast<int32_t>(offsetof(FlyingSparx_t1256167344, ___fSpawnDelay_2)); }
	inline float get_fSpawnDelay_2() const { return ___fSpawnDelay_2; }
	inline float* get_address_of_fSpawnDelay_2() { return &___fSpawnDelay_2; }
	inline void set_fSpawnDelay_2(float value)
	{
		___fSpawnDelay_2 = value;
	}

	inline static int32_t get_offset_of_Target_3() { return static_cast<int32_t>(offsetof(FlyingSparx_t1256167344, ___Target_3)); }
	inline Vector3_t1986933152  get_Target_3() const { return ___Target_3; }
	inline Vector3_t1986933152 * get_address_of_Target_3() { return &___Target_3; }
	inline void set_Target_3(Vector3_t1986933152  value)
	{
		___Target_3 = value;
	}

	inline static int32_t get_offset_of_bSpawn_4() { return static_cast<int32_t>(offsetof(FlyingSparx_t1256167344, ___bSpawn_4)); }
	inline bool get_bSpawn_4() const { return ___bSpawn_4; }
	inline bool* get_address_of_bSpawn_4() { return &___bSpawn_4; }
	inline void set_bSpawn_4(bool value)
	{
		___bSpawn_4 = value;
	}

	inline static int32_t get_offset_of_bFlying_5() { return static_cast<int32_t>(offsetof(FlyingSparx_t1256167344, ___bFlying_5)); }
	inline bool get_bFlying_5() const { return ___bFlying_5; }
	inline bool* get_address_of_bFlying_5() { return &___bFlying_5; }
	inline void set_bFlying_5(bool value)
	{
		___bFlying_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLYINGSPARX_T1256167344_H
#ifndef FIREFOUNTAINROUTINE_T2875428386_H
#define FIREFOUNTAINROUTINE_T2875428386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireFountainRoutine
struct  FireFountainRoutine_t2875428386  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] FireFountainRoutine::Fountains
	GameObjectU5BU5D_t2988620542* ___Fountains_2;
	// System.Single[] FireFountainRoutine::OnTime
	SingleU5BU5D_t2843050510* ___OnTime_3;
	// System.Single[] FireFountainRoutine::OffTime
	SingleU5BU5D_t2843050510* ___OffTime_4;
	// System.Single FireFountainRoutine::fMin
	float ___fMin_5;
	// System.Single FireFountainRoutine::fMax
	float ___fMax_6;

public:
	inline static int32_t get_offset_of_Fountains_2() { return static_cast<int32_t>(offsetof(FireFountainRoutine_t2875428386, ___Fountains_2)); }
	inline GameObjectU5BU5D_t2988620542* get_Fountains_2() const { return ___Fountains_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Fountains_2() { return &___Fountains_2; }
	inline void set_Fountains_2(GameObjectU5BU5D_t2988620542* value)
	{
		___Fountains_2 = value;
		Il2CppCodeGenWriteBarrier((&___Fountains_2), value);
	}

	inline static int32_t get_offset_of_OnTime_3() { return static_cast<int32_t>(offsetof(FireFountainRoutine_t2875428386, ___OnTime_3)); }
	inline SingleU5BU5D_t2843050510* get_OnTime_3() const { return ___OnTime_3; }
	inline SingleU5BU5D_t2843050510** get_address_of_OnTime_3() { return &___OnTime_3; }
	inline void set_OnTime_3(SingleU5BU5D_t2843050510* value)
	{
		___OnTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTime_3), value);
	}

	inline static int32_t get_offset_of_OffTime_4() { return static_cast<int32_t>(offsetof(FireFountainRoutine_t2875428386, ___OffTime_4)); }
	inline SingleU5BU5D_t2843050510* get_OffTime_4() const { return ___OffTime_4; }
	inline SingleU5BU5D_t2843050510** get_address_of_OffTime_4() { return &___OffTime_4; }
	inline void set_OffTime_4(SingleU5BU5D_t2843050510* value)
	{
		___OffTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___OffTime_4), value);
	}

	inline static int32_t get_offset_of_fMin_5() { return static_cast<int32_t>(offsetof(FireFountainRoutine_t2875428386, ___fMin_5)); }
	inline float get_fMin_5() const { return ___fMin_5; }
	inline float* get_address_of_fMin_5() { return &___fMin_5; }
	inline void set_fMin_5(float value)
	{
		___fMin_5 = value;
	}

	inline static int32_t get_offset_of_fMax_6() { return static_cast<int32_t>(offsetof(FireFountainRoutine_t2875428386, ___fMax_6)); }
	inline float get_fMax_6() const { return ___fMax_6; }
	inline float* get_address_of_fMax_6() { return &___fMax_6; }
	inline void set_fMax_6(float value)
	{
		___fMax_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREFOUNTAINROUTINE_T2875428386_H
#ifndef JIGSAWPUZZLEWORD_T52544638_H
#define JIGSAWPUZZLEWORD_T52544638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JigSawPuzzleWord
struct  JigSawPuzzleWord_t52544638  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector2[] JigSawPuzzleWord::StartingPositions
	Vector2U5BU5D_t1220531434* ___StartingPositions_2;
	// UnityEngine.Vector2[] JigSawPuzzleWord::CorrectPosition
	Vector2U5BU5D_t1220531434* ___CorrectPosition_3;
	// System.String JigSawPuzzleWord::CorrectWord
	String_t* ___CorrectWord_4;
	// System.Boolean JigSawPuzzleWord::m_bSolved
	bool ___m_bSolved_5;
	// System.Boolean JigSawPuzzleWord::m_bFinished
	bool ___m_bFinished_6;
	// System.Single JigSawPuzzleWord::m_fTimer
	float ___m_fTimer_7;
	// UnityEngine.Texture2D JigSawPuzzleWord::m_6perpsectivebg2
	Texture2D_t3063074017 * ___m_6perpsectivebg2_8;

public:
	inline static int32_t get_offset_of_StartingPositions_2() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___StartingPositions_2)); }
	inline Vector2U5BU5D_t1220531434* get_StartingPositions_2() const { return ___StartingPositions_2; }
	inline Vector2U5BU5D_t1220531434** get_address_of_StartingPositions_2() { return &___StartingPositions_2; }
	inline void set_StartingPositions_2(Vector2U5BU5D_t1220531434* value)
	{
		___StartingPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartingPositions_2), value);
	}

	inline static int32_t get_offset_of_CorrectPosition_3() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___CorrectPosition_3)); }
	inline Vector2U5BU5D_t1220531434* get_CorrectPosition_3() const { return ___CorrectPosition_3; }
	inline Vector2U5BU5D_t1220531434** get_address_of_CorrectPosition_3() { return &___CorrectPosition_3; }
	inline void set_CorrectPosition_3(Vector2U5BU5D_t1220531434* value)
	{
		___CorrectPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectPosition_3), value);
	}

	inline static int32_t get_offset_of_CorrectWord_4() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___CorrectWord_4)); }
	inline String_t* get_CorrectWord_4() const { return ___CorrectWord_4; }
	inline String_t** get_address_of_CorrectWord_4() { return &___CorrectWord_4; }
	inline void set_CorrectWord_4(String_t* value)
	{
		___CorrectWord_4 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectWord_4), value);
	}

	inline static int32_t get_offset_of_m_bSolved_5() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___m_bSolved_5)); }
	inline bool get_m_bSolved_5() const { return ___m_bSolved_5; }
	inline bool* get_address_of_m_bSolved_5() { return &___m_bSolved_5; }
	inline void set_m_bSolved_5(bool value)
	{
		___m_bSolved_5 = value;
	}

	inline static int32_t get_offset_of_m_bFinished_6() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___m_bFinished_6)); }
	inline bool get_m_bFinished_6() const { return ___m_bFinished_6; }
	inline bool* get_address_of_m_bFinished_6() { return &___m_bFinished_6; }
	inline void set_m_bFinished_6(bool value)
	{
		___m_bFinished_6 = value;
	}

	inline static int32_t get_offset_of_m_fTimer_7() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___m_fTimer_7)); }
	inline float get_m_fTimer_7() const { return ___m_fTimer_7; }
	inline float* get_address_of_m_fTimer_7() { return &___m_fTimer_7; }
	inline void set_m_fTimer_7(float value)
	{
		___m_fTimer_7 = value;
	}

	inline static int32_t get_offset_of_m_6perpsectivebg2_8() { return static_cast<int32_t>(offsetof(JigSawPuzzleWord_t52544638, ___m_6perpsectivebg2_8)); }
	inline Texture2D_t3063074017 * get_m_6perpsectivebg2_8() const { return ___m_6perpsectivebg2_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_6perpsectivebg2_8() { return &___m_6perpsectivebg2_8; }
	inline void set_m_6perpsectivebg2_8(Texture2D_t3063074017 * value)
	{
		___m_6perpsectivebg2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_6perpsectivebg2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JIGSAWPUZZLEWORD_T52544638_H
#ifndef WORDSEARCHPUZZLE_T817895283_H
#define WORDSEARCHPUZZLE_T817895283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WordSearchPuzzle
struct  WordSearchPuzzle_t817895283  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject WordSearchPuzzle::Particles
	GameObject_t2557347079 * ___Particles_2;
	// System.String WordSearchPuzzle::Grid
	String_t* ___Grid_3;
	// System.Int32 WordSearchPuzzle::GridWidth
	int32_t ___GridWidth_4;
	// System.Int32 WordSearchPuzzle::GridHeight
	int32_t ___GridHeight_5;
	// System.String WordSearchPuzzle::GoalString
	String_t* ___GoalString_6;
	// System.Int32 WordSearchPuzzle::NumOfPhrase
	int32_t ___NumOfPhrase_7;
	// System.Single WordSearchPuzzle::BlockLeft
	float ___BlockLeft_8;
	// System.Single WordSearchPuzzle::BlockTop
	float ___BlockTop_9;
	// System.Single WordSearchPuzzle::BlockWidth
	float ___BlockWidth_10;
	// System.Single WordSearchPuzzle::BlockHeight
	float ___BlockHeight_11;
	// UnityEngine.GUISkin WordSearchPuzzle::Skin
	GUISkin_t2122630221 * ___Skin_12;
	// System.Single WordSearchPuzzle::m_fEndTimer
	float ___m_fEndTimer_13;
	// System.Boolean[] WordSearchPuzzle::ClearedLetters
	BooleanU5BU5D_t698278498* ___ClearedLetters_14;
	// System.Boolean WordSearchPuzzle::allclear
	bool ___allclear_15;
	// UnityEngine.Texture2D WordSearchPuzzle::m_5Action2Texture
	Texture2D_t3063074017 * ___m_5Action2Texture_16;

public:
	inline static int32_t get_offset_of_Particles_2() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Particles_2)); }
	inline GameObject_t2557347079 * get_Particles_2() const { return ___Particles_2; }
	inline GameObject_t2557347079 ** get_address_of_Particles_2() { return &___Particles_2; }
	inline void set_Particles_2(GameObject_t2557347079 * value)
	{
		___Particles_2 = value;
		Il2CppCodeGenWriteBarrier((&___Particles_2), value);
	}

	inline static int32_t get_offset_of_Grid_3() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Grid_3)); }
	inline String_t* get_Grid_3() const { return ___Grid_3; }
	inline String_t** get_address_of_Grid_3() { return &___Grid_3; }
	inline void set_Grid_3(String_t* value)
	{
		___Grid_3 = value;
		Il2CppCodeGenWriteBarrier((&___Grid_3), value);
	}

	inline static int32_t get_offset_of_GridWidth_4() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GridWidth_4)); }
	inline int32_t get_GridWidth_4() const { return ___GridWidth_4; }
	inline int32_t* get_address_of_GridWidth_4() { return &___GridWidth_4; }
	inline void set_GridWidth_4(int32_t value)
	{
		___GridWidth_4 = value;
	}

	inline static int32_t get_offset_of_GridHeight_5() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GridHeight_5)); }
	inline int32_t get_GridHeight_5() const { return ___GridHeight_5; }
	inline int32_t* get_address_of_GridHeight_5() { return &___GridHeight_5; }
	inline void set_GridHeight_5(int32_t value)
	{
		___GridHeight_5 = value;
	}

	inline static int32_t get_offset_of_GoalString_6() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___GoalString_6)); }
	inline String_t* get_GoalString_6() const { return ___GoalString_6; }
	inline String_t** get_address_of_GoalString_6() { return &___GoalString_6; }
	inline void set_GoalString_6(String_t* value)
	{
		___GoalString_6 = value;
		Il2CppCodeGenWriteBarrier((&___GoalString_6), value);
	}

	inline static int32_t get_offset_of_NumOfPhrase_7() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___NumOfPhrase_7)); }
	inline int32_t get_NumOfPhrase_7() const { return ___NumOfPhrase_7; }
	inline int32_t* get_address_of_NumOfPhrase_7() { return &___NumOfPhrase_7; }
	inline void set_NumOfPhrase_7(int32_t value)
	{
		___NumOfPhrase_7 = value;
	}

	inline static int32_t get_offset_of_BlockLeft_8() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockLeft_8)); }
	inline float get_BlockLeft_8() const { return ___BlockLeft_8; }
	inline float* get_address_of_BlockLeft_8() { return &___BlockLeft_8; }
	inline void set_BlockLeft_8(float value)
	{
		___BlockLeft_8 = value;
	}

	inline static int32_t get_offset_of_BlockTop_9() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockTop_9)); }
	inline float get_BlockTop_9() const { return ___BlockTop_9; }
	inline float* get_address_of_BlockTop_9() { return &___BlockTop_9; }
	inline void set_BlockTop_9(float value)
	{
		___BlockTop_9 = value;
	}

	inline static int32_t get_offset_of_BlockWidth_10() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockWidth_10)); }
	inline float get_BlockWidth_10() const { return ___BlockWidth_10; }
	inline float* get_address_of_BlockWidth_10() { return &___BlockWidth_10; }
	inline void set_BlockWidth_10(float value)
	{
		___BlockWidth_10 = value;
	}

	inline static int32_t get_offset_of_BlockHeight_11() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___BlockHeight_11)); }
	inline float get_BlockHeight_11() const { return ___BlockHeight_11; }
	inline float* get_address_of_BlockHeight_11() { return &___BlockHeight_11; }
	inline void set_BlockHeight_11(float value)
	{
		___BlockHeight_11 = value;
	}

	inline static int32_t get_offset_of_Skin_12() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___Skin_12)); }
	inline GUISkin_t2122630221 * get_Skin_12() const { return ___Skin_12; }
	inline GUISkin_t2122630221 ** get_address_of_Skin_12() { return &___Skin_12; }
	inline void set_Skin_12(GUISkin_t2122630221 * value)
	{
		___Skin_12 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_12), value);
	}

	inline static int32_t get_offset_of_m_fEndTimer_13() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___m_fEndTimer_13)); }
	inline float get_m_fEndTimer_13() const { return ___m_fEndTimer_13; }
	inline float* get_address_of_m_fEndTimer_13() { return &___m_fEndTimer_13; }
	inline void set_m_fEndTimer_13(float value)
	{
		___m_fEndTimer_13 = value;
	}

	inline static int32_t get_offset_of_ClearedLetters_14() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___ClearedLetters_14)); }
	inline BooleanU5BU5D_t698278498* get_ClearedLetters_14() const { return ___ClearedLetters_14; }
	inline BooleanU5BU5D_t698278498** get_address_of_ClearedLetters_14() { return &___ClearedLetters_14; }
	inline void set_ClearedLetters_14(BooleanU5BU5D_t698278498* value)
	{
		___ClearedLetters_14 = value;
		Il2CppCodeGenWriteBarrier((&___ClearedLetters_14), value);
	}

	inline static int32_t get_offset_of_allclear_15() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___allclear_15)); }
	inline bool get_allclear_15() const { return ___allclear_15; }
	inline bool* get_address_of_allclear_15() { return &___allclear_15; }
	inline void set_allclear_15(bool value)
	{
		___allclear_15 = value;
	}

	inline static int32_t get_offset_of_m_5Action2Texture_16() { return static_cast<int32_t>(offsetof(WordSearchPuzzle_t817895283, ___m_5Action2Texture_16)); }
	inline Texture2D_t3063074017 * get_m_5Action2Texture_16() const { return ___m_5Action2Texture_16; }
	inline Texture2D_t3063074017 ** get_address_of_m_5Action2Texture_16() { return &___m_5Action2Texture_16; }
	inline void set_m_5Action2Texture_16(Texture2D_t3063074017 * value)
	{
		___m_5Action2Texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_5Action2Texture_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSEARCHPUZZLE_T817895283_H
#ifndef PUZZLEPIECEWORD_T3115385946_H
#define PUZZLEPIECEWORD_T3115385946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzlePieceWord
struct  PuzzlePieceWord_t3115385946  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector2 PuzzlePieceWord::m_vCorrectPos
	Vector2_t328513675  ___m_vCorrectPos_2;
	// UnityEngine.Vector2 PuzzlePieceWord::m_vStartPos
	Vector2_t328513675  ___m_vStartPos_3;
	// System.Boolean PuzzlePieceWord::m_bOnFocus
	bool ___m_bOnFocus_4;
	// System.Boolean PuzzlePieceWord::m_bCanClick
	bool ___m_bCanClick_5;
	// System.Boolean PuzzlePieceWord::m_bCorrectPosition
	bool ___m_bCorrectPosition_6;
	// System.Boolean PuzzlePieceWord::m_bMovement
	bool ___m_bMovement_7;

public:
	inline static int32_t get_offset_of_m_vCorrectPos_2() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_vCorrectPos_2)); }
	inline Vector2_t328513675  get_m_vCorrectPos_2() const { return ___m_vCorrectPos_2; }
	inline Vector2_t328513675 * get_address_of_m_vCorrectPos_2() { return &___m_vCorrectPos_2; }
	inline void set_m_vCorrectPos_2(Vector2_t328513675  value)
	{
		___m_vCorrectPos_2 = value;
	}

	inline static int32_t get_offset_of_m_vStartPos_3() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_vStartPos_3)); }
	inline Vector2_t328513675  get_m_vStartPos_3() const { return ___m_vStartPos_3; }
	inline Vector2_t328513675 * get_address_of_m_vStartPos_3() { return &___m_vStartPos_3; }
	inline void set_m_vStartPos_3(Vector2_t328513675  value)
	{
		___m_vStartPos_3 = value;
	}

	inline static int32_t get_offset_of_m_bOnFocus_4() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_bOnFocus_4)); }
	inline bool get_m_bOnFocus_4() const { return ___m_bOnFocus_4; }
	inline bool* get_address_of_m_bOnFocus_4() { return &___m_bOnFocus_4; }
	inline void set_m_bOnFocus_4(bool value)
	{
		___m_bOnFocus_4 = value;
	}

	inline static int32_t get_offset_of_m_bCanClick_5() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_bCanClick_5)); }
	inline bool get_m_bCanClick_5() const { return ___m_bCanClick_5; }
	inline bool* get_address_of_m_bCanClick_5() { return &___m_bCanClick_5; }
	inline void set_m_bCanClick_5(bool value)
	{
		___m_bCanClick_5 = value;
	}

	inline static int32_t get_offset_of_m_bCorrectPosition_6() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_bCorrectPosition_6)); }
	inline bool get_m_bCorrectPosition_6() const { return ___m_bCorrectPosition_6; }
	inline bool* get_address_of_m_bCorrectPosition_6() { return &___m_bCorrectPosition_6; }
	inline void set_m_bCorrectPosition_6(bool value)
	{
		___m_bCorrectPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_bMovement_7() { return static_cast<int32_t>(offsetof(PuzzlePieceWord_t3115385946, ___m_bMovement_7)); }
	inline bool get_m_bMovement_7() const { return ___m_bMovement_7; }
	inline bool* get_address_of_m_bMovement_7() { return &___m_bMovement_7; }
	inline void set_m_bMovement_7(bool value)
	{
		___m_bMovement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEPIECEWORD_T3115385946_H
#ifndef PUZZLEPIECE_T1346995122_H
#define PUZZLEPIECE_T1346995122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzlePiece
struct  PuzzlePiece_t1346995122  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector2 PuzzlePiece::m_vCorrectPos
	Vector2_t328513675  ___m_vCorrectPos_2;
	// System.Boolean PuzzlePiece::m_bOnFocus
	bool ___m_bOnFocus_3;
	// System.Boolean PuzzlePiece::m_bCanClick
	bool ___m_bCanClick_4;
	// System.Boolean PuzzlePiece::m_bCorrectPosition
	bool ___m_bCorrectPosition_5;
	// UnityEngine.ParticleSystem PuzzlePiece::Particles
	ParticleSystem_t2248283753 * ___Particles_6;

public:
	inline static int32_t get_offset_of_m_vCorrectPos_2() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1346995122, ___m_vCorrectPos_2)); }
	inline Vector2_t328513675  get_m_vCorrectPos_2() const { return ___m_vCorrectPos_2; }
	inline Vector2_t328513675 * get_address_of_m_vCorrectPos_2() { return &___m_vCorrectPos_2; }
	inline void set_m_vCorrectPos_2(Vector2_t328513675  value)
	{
		___m_vCorrectPos_2 = value;
	}

	inline static int32_t get_offset_of_m_bOnFocus_3() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1346995122, ___m_bOnFocus_3)); }
	inline bool get_m_bOnFocus_3() const { return ___m_bOnFocus_3; }
	inline bool* get_address_of_m_bOnFocus_3() { return &___m_bOnFocus_3; }
	inline void set_m_bOnFocus_3(bool value)
	{
		___m_bOnFocus_3 = value;
	}

	inline static int32_t get_offset_of_m_bCanClick_4() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1346995122, ___m_bCanClick_4)); }
	inline bool get_m_bCanClick_4() const { return ___m_bCanClick_4; }
	inline bool* get_address_of_m_bCanClick_4() { return &___m_bCanClick_4; }
	inline void set_m_bCanClick_4(bool value)
	{
		___m_bCanClick_4 = value;
	}

	inline static int32_t get_offset_of_m_bCorrectPosition_5() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1346995122, ___m_bCorrectPosition_5)); }
	inline bool get_m_bCorrectPosition_5() const { return ___m_bCorrectPosition_5; }
	inline bool* get_address_of_m_bCorrectPosition_5() { return &___m_bCorrectPosition_5; }
	inline void set_m_bCorrectPosition_5(bool value)
	{
		___m_bCorrectPosition_5 = value;
	}

	inline static int32_t get_offset_of_Particles_6() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1346995122, ___Particles_6)); }
	inline ParticleSystem_t2248283753 * get_Particles_6() const { return ___Particles_6; }
	inline ParticleSystem_t2248283753 ** get_address_of_Particles_6() { return &___Particles_6; }
	inline void set_Particles_6(ParticleSystem_t2248283753 * value)
	{
		___Particles_6 = value;
		Il2CppCodeGenWriteBarrier((&___Particles_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEPIECE_T1346995122_H
#ifndef PUSHROCKMINIGAME_T1414473092_H
#define PUSHROCKMINIGAME_T1414473092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PushRockMiniGame
struct  PushRockMiniGame_t1414473092  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] PushRockMiniGame::Rocks
	GameObjectU5BU5D_t2988620542* ___Rocks_2;
	// System.Int32[] PushRockMiniGame::RockFallToPos
	Int32U5BU5D_t1965588061* ___RockFallToPos_3;
	// System.Boolean[] PushRockMiniGame::EmptySpace
	BooleanU5BU5D_t698278498* ___EmptySpace_4;
	// System.String PushRockMiniGame::CameraID
	String_t* ___CameraID_5;
	// UnityEngine.Transform PushRockMiniGame::TriggerPos
	Transform_t362059596 * ___TriggerPos_6;
	// UnityEngine.Transform[] PushRockMiniGame::Barriers
	TransformU5BU5D_t2383644165* ___Barriers_7;
	// UnityEngine.Transform[] PushRockMiniGame::MoveToPos
	TransformU5BU5D_t2383644165* ___MoveToPos_8;
	// System.Boolean PushRockMiniGame::Triggered
	bool ___Triggered_9;
	// System.Single PushRockMiniGame::FallInterval
	float ___FallInterval_10;
	// System.Single PushRockMiniGame::fMin
	float ___fMin_11;
	// System.Single PushRockMiniGame::fMax
	float ___fMax_12;
	// System.Int32 PushRockMiniGame::m_iCurrentRock
	int32_t ___m_iCurrentRock_13;
	// System.Int32 PushRockMiniGame::m_iNumOfRockPushed
	int32_t ___m_iNumOfRockPushed_14;
	// System.Single PushRockMiniGame::m_fExplodeTimer
	float ___m_fExplodeTimer_15;
	// System.Single PushRockMiniGame::endCutsceneLength
	float ___endCutsceneLength_16;
	// System.Single PushRockMiniGame::m_fEndTimer
	float ___m_fEndTimer_17;
	// UnityEngine.Texture2D PushRockMiniGame::m_stream4Texture
	Texture2D_t3063074017 * ___m_stream4Texture_18;
	// UnityEngine.Texture2D PushRockMiniGame::m_stream3Texture
	Texture2D_t3063074017 * ___m_stream3Texture_19;
	// UnityEngine.Texture2D PushRockMiniGame::m_stream2Texture
	Texture2D_t3063074017 * ___m_stream2Texture_20;
	// UnityEngine.Texture2D PushRockMiniGame::m_stream1Texture
	Texture2D_t3063074017 * ___m_stream1Texture_21;
	// System.Boolean PushRockMiniGame::m_bStartMovebackToLava2
	bool ___m_bStartMovebackToLava2_22;
	// UnityEngine.Vector3 PushRockMiniGame::CurrentPos
	Vector3_t1986933152  ___CurrentPos_23;
	// UnityEngine.Vector3 PushRockMiniGame::TargetPos
	Vector3_t1986933152  ___TargetPos_24;
	// System.Int32 PushRockMiniGame::RockPushed
	int32_t ___RockPushed_25;
	// System.Boolean PushRockMiniGame::PlayGame
	bool ___PlayGame_26;
	// System.Boolean PushRockMiniGame::Moving
	bool ___Moving_27;
	// UnityEngine.GameObject PushRockMiniGame::explosionCutscene
	GameObject_t2557347079 * ___explosionCutscene_28;

public:
	inline static int32_t get_offset_of_Rocks_2() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___Rocks_2)); }
	inline GameObjectU5BU5D_t2988620542* get_Rocks_2() const { return ___Rocks_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Rocks_2() { return &___Rocks_2; }
	inline void set_Rocks_2(GameObjectU5BU5D_t2988620542* value)
	{
		___Rocks_2 = value;
		Il2CppCodeGenWriteBarrier((&___Rocks_2), value);
	}

	inline static int32_t get_offset_of_RockFallToPos_3() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___RockFallToPos_3)); }
	inline Int32U5BU5D_t1965588061* get_RockFallToPos_3() const { return ___RockFallToPos_3; }
	inline Int32U5BU5D_t1965588061** get_address_of_RockFallToPos_3() { return &___RockFallToPos_3; }
	inline void set_RockFallToPos_3(Int32U5BU5D_t1965588061* value)
	{
		___RockFallToPos_3 = value;
		Il2CppCodeGenWriteBarrier((&___RockFallToPos_3), value);
	}

	inline static int32_t get_offset_of_EmptySpace_4() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___EmptySpace_4)); }
	inline BooleanU5BU5D_t698278498* get_EmptySpace_4() const { return ___EmptySpace_4; }
	inline BooleanU5BU5D_t698278498** get_address_of_EmptySpace_4() { return &___EmptySpace_4; }
	inline void set_EmptySpace_4(BooleanU5BU5D_t698278498* value)
	{
		___EmptySpace_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptySpace_4), value);
	}

	inline static int32_t get_offset_of_CameraID_5() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___CameraID_5)); }
	inline String_t* get_CameraID_5() const { return ___CameraID_5; }
	inline String_t** get_address_of_CameraID_5() { return &___CameraID_5; }
	inline void set_CameraID_5(String_t* value)
	{
		___CameraID_5 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_5), value);
	}

	inline static int32_t get_offset_of_TriggerPos_6() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___TriggerPos_6)); }
	inline Transform_t362059596 * get_TriggerPos_6() const { return ___TriggerPos_6; }
	inline Transform_t362059596 ** get_address_of_TriggerPos_6() { return &___TriggerPos_6; }
	inline void set_TriggerPos_6(Transform_t362059596 * value)
	{
		___TriggerPos_6 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerPos_6), value);
	}

	inline static int32_t get_offset_of_Barriers_7() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___Barriers_7)); }
	inline TransformU5BU5D_t2383644165* get_Barriers_7() const { return ___Barriers_7; }
	inline TransformU5BU5D_t2383644165** get_address_of_Barriers_7() { return &___Barriers_7; }
	inline void set_Barriers_7(TransformU5BU5D_t2383644165* value)
	{
		___Barriers_7 = value;
		Il2CppCodeGenWriteBarrier((&___Barriers_7), value);
	}

	inline static int32_t get_offset_of_MoveToPos_8() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___MoveToPos_8)); }
	inline TransformU5BU5D_t2383644165* get_MoveToPos_8() const { return ___MoveToPos_8; }
	inline TransformU5BU5D_t2383644165** get_address_of_MoveToPos_8() { return &___MoveToPos_8; }
	inline void set_MoveToPos_8(TransformU5BU5D_t2383644165* value)
	{
		___MoveToPos_8 = value;
		Il2CppCodeGenWriteBarrier((&___MoveToPos_8), value);
	}

	inline static int32_t get_offset_of_Triggered_9() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___Triggered_9)); }
	inline bool get_Triggered_9() const { return ___Triggered_9; }
	inline bool* get_address_of_Triggered_9() { return &___Triggered_9; }
	inline void set_Triggered_9(bool value)
	{
		___Triggered_9 = value;
	}

	inline static int32_t get_offset_of_FallInterval_10() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___FallInterval_10)); }
	inline float get_FallInterval_10() const { return ___FallInterval_10; }
	inline float* get_address_of_FallInterval_10() { return &___FallInterval_10; }
	inline void set_FallInterval_10(float value)
	{
		___FallInterval_10 = value;
	}

	inline static int32_t get_offset_of_fMin_11() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___fMin_11)); }
	inline float get_fMin_11() const { return ___fMin_11; }
	inline float* get_address_of_fMin_11() { return &___fMin_11; }
	inline void set_fMin_11(float value)
	{
		___fMin_11 = value;
	}

	inline static int32_t get_offset_of_fMax_12() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___fMax_12)); }
	inline float get_fMax_12() const { return ___fMax_12; }
	inline float* get_address_of_fMax_12() { return &___fMax_12; }
	inline void set_fMax_12(float value)
	{
		___fMax_12 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentRock_13() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_iCurrentRock_13)); }
	inline int32_t get_m_iCurrentRock_13() const { return ___m_iCurrentRock_13; }
	inline int32_t* get_address_of_m_iCurrentRock_13() { return &___m_iCurrentRock_13; }
	inline void set_m_iCurrentRock_13(int32_t value)
	{
		___m_iCurrentRock_13 = value;
	}

	inline static int32_t get_offset_of_m_iNumOfRockPushed_14() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_iNumOfRockPushed_14)); }
	inline int32_t get_m_iNumOfRockPushed_14() const { return ___m_iNumOfRockPushed_14; }
	inline int32_t* get_address_of_m_iNumOfRockPushed_14() { return &___m_iNumOfRockPushed_14; }
	inline void set_m_iNumOfRockPushed_14(int32_t value)
	{
		___m_iNumOfRockPushed_14 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeTimer_15() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_fExplodeTimer_15)); }
	inline float get_m_fExplodeTimer_15() const { return ___m_fExplodeTimer_15; }
	inline float* get_address_of_m_fExplodeTimer_15() { return &___m_fExplodeTimer_15; }
	inline void set_m_fExplodeTimer_15(float value)
	{
		___m_fExplodeTimer_15 = value;
	}

	inline static int32_t get_offset_of_endCutsceneLength_16() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___endCutsceneLength_16)); }
	inline float get_endCutsceneLength_16() const { return ___endCutsceneLength_16; }
	inline float* get_address_of_endCutsceneLength_16() { return &___endCutsceneLength_16; }
	inline void set_endCutsceneLength_16(float value)
	{
		___endCutsceneLength_16 = value;
	}

	inline static int32_t get_offset_of_m_fEndTimer_17() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_fEndTimer_17)); }
	inline float get_m_fEndTimer_17() const { return ___m_fEndTimer_17; }
	inline float* get_address_of_m_fEndTimer_17() { return &___m_fEndTimer_17; }
	inline void set_m_fEndTimer_17(float value)
	{
		___m_fEndTimer_17 = value;
	}

	inline static int32_t get_offset_of_m_stream4Texture_18() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_stream4Texture_18)); }
	inline Texture2D_t3063074017 * get_m_stream4Texture_18() const { return ___m_stream4Texture_18; }
	inline Texture2D_t3063074017 ** get_address_of_m_stream4Texture_18() { return &___m_stream4Texture_18; }
	inline void set_m_stream4Texture_18(Texture2D_t3063074017 * value)
	{
		___m_stream4Texture_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream4Texture_18), value);
	}

	inline static int32_t get_offset_of_m_stream3Texture_19() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_stream3Texture_19)); }
	inline Texture2D_t3063074017 * get_m_stream3Texture_19() const { return ___m_stream3Texture_19; }
	inline Texture2D_t3063074017 ** get_address_of_m_stream3Texture_19() { return &___m_stream3Texture_19; }
	inline void set_m_stream3Texture_19(Texture2D_t3063074017 * value)
	{
		___m_stream3Texture_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream3Texture_19), value);
	}

	inline static int32_t get_offset_of_m_stream2Texture_20() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_stream2Texture_20)); }
	inline Texture2D_t3063074017 * get_m_stream2Texture_20() const { return ___m_stream2Texture_20; }
	inline Texture2D_t3063074017 ** get_address_of_m_stream2Texture_20() { return &___m_stream2Texture_20; }
	inline void set_m_stream2Texture_20(Texture2D_t3063074017 * value)
	{
		___m_stream2Texture_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream2Texture_20), value);
	}

	inline static int32_t get_offset_of_m_stream1Texture_21() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_stream1Texture_21)); }
	inline Texture2D_t3063074017 * get_m_stream1Texture_21() const { return ___m_stream1Texture_21; }
	inline Texture2D_t3063074017 ** get_address_of_m_stream1Texture_21() { return &___m_stream1Texture_21; }
	inline void set_m_stream1Texture_21(Texture2D_t3063074017 * value)
	{
		___m_stream1Texture_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream1Texture_21), value);
	}

	inline static int32_t get_offset_of_m_bStartMovebackToLava2_22() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___m_bStartMovebackToLava2_22)); }
	inline bool get_m_bStartMovebackToLava2_22() const { return ___m_bStartMovebackToLava2_22; }
	inline bool* get_address_of_m_bStartMovebackToLava2_22() { return &___m_bStartMovebackToLava2_22; }
	inline void set_m_bStartMovebackToLava2_22(bool value)
	{
		___m_bStartMovebackToLava2_22 = value;
	}

	inline static int32_t get_offset_of_CurrentPos_23() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___CurrentPos_23)); }
	inline Vector3_t1986933152  get_CurrentPos_23() const { return ___CurrentPos_23; }
	inline Vector3_t1986933152 * get_address_of_CurrentPos_23() { return &___CurrentPos_23; }
	inline void set_CurrentPos_23(Vector3_t1986933152  value)
	{
		___CurrentPos_23 = value;
	}

	inline static int32_t get_offset_of_TargetPos_24() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___TargetPos_24)); }
	inline Vector3_t1986933152  get_TargetPos_24() const { return ___TargetPos_24; }
	inline Vector3_t1986933152 * get_address_of_TargetPos_24() { return &___TargetPos_24; }
	inline void set_TargetPos_24(Vector3_t1986933152  value)
	{
		___TargetPos_24 = value;
	}

	inline static int32_t get_offset_of_RockPushed_25() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___RockPushed_25)); }
	inline int32_t get_RockPushed_25() const { return ___RockPushed_25; }
	inline int32_t* get_address_of_RockPushed_25() { return &___RockPushed_25; }
	inline void set_RockPushed_25(int32_t value)
	{
		___RockPushed_25 = value;
	}

	inline static int32_t get_offset_of_PlayGame_26() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___PlayGame_26)); }
	inline bool get_PlayGame_26() const { return ___PlayGame_26; }
	inline bool* get_address_of_PlayGame_26() { return &___PlayGame_26; }
	inline void set_PlayGame_26(bool value)
	{
		___PlayGame_26 = value;
	}

	inline static int32_t get_offset_of_Moving_27() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___Moving_27)); }
	inline bool get_Moving_27() const { return ___Moving_27; }
	inline bool* get_address_of_Moving_27() { return &___Moving_27; }
	inline void set_Moving_27(bool value)
	{
		___Moving_27 = value;
	}

	inline static int32_t get_offset_of_explosionCutscene_28() { return static_cast<int32_t>(offsetof(PushRockMiniGame_t1414473092, ___explosionCutscene_28)); }
	inline GameObject_t2557347079 * get_explosionCutscene_28() const { return ___explosionCutscene_28; }
	inline GameObject_t2557347079 ** get_address_of_explosionCutscene_28() { return &___explosionCutscene_28; }
	inline void set_explosionCutscene_28(GameObject_t2557347079 * value)
	{
		___explosionCutscene_28 = value;
		Il2CppCodeGenWriteBarrier((&___explosionCutscene_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSHROCKMINIGAME_T1414473092_H
#ifndef PLAYDIALOG_T1022950615_H
#define PLAYDIALOG_T1022950615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayDialog
struct  PlayDialog_t1022950615  : public MonoBehaviour_t1618594486
{
public:
	// System.String PlayDialog::Dialog
	String_t* ___Dialog_2;
	// System.String PlayDialog::Name
	String_t* ___Name_3;
	// System.Single PlayDialog::fStartingAnchor
	float ___fStartingAnchor_4;
	// System.Single PlayDialog::fFinishingAnchor
	float ___fFinishingAnchor_5;
	// System.Boolean PlayDialog::StartPlaying
	bool ___StartPlaying_6;
	// System.Boolean PlayDialog::IsPlaying
	bool ___IsPlaying_7;
	// System.String PlayDialog::VoiceFile
	String_t* ___VoiceFile_8;
	// UnityEngine.AudioClip PlayDialog::VoiceAudilClip
	AudioClip_t1419814565 * ___VoiceAudilClip_9;
	// UnityEngine.AudioSource PlayDialog::m_Voice
	AudioSource_t4025721661 * ___m_Voice_10;
	// UnityEngine.GameObject PlayDialog::CorrectBarrel
	GameObject_t2557347079 * ___CorrectBarrel_11;
	// System.String PlayDialog::GameID
	String_t* ___GameID_12;
	// UnityEngine.Texture2D PlayDialog::buttonTexture
	Texture2D_t3063074017 * ___buttonTexture_14;
	// UnityEngine.GUISkin PlayDialog::skin
	GUISkin_t2122630221 * ___skin_15;

public:
	inline static int32_t get_offset_of_Dialog_2() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___Dialog_2)); }
	inline String_t* get_Dialog_2() const { return ___Dialog_2; }
	inline String_t** get_address_of_Dialog_2() { return &___Dialog_2; }
	inline void set_Dialog_2(String_t* value)
	{
		___Dialog_2 = value;
		Il2CppCodeGenWriteBarrier((&___Dialog_2), value);
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}

	inline static int32_t get_offset_of_fStartingAnchor_4() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___fStartingAnchor_4)); }
	inline float get_fStartingAnchor_4() const { return ___fStartingAnchor_4; }
	inline float* get_address_of_fStartingAnchor_4() { return &___fStartingAnchor_4; }
	inline void set_fStartingAnchor_4(float value)
	{
		___fStartingAnchor_4 = value;
	}

	inline static int32_t get_offset_of_fFinishingAnchor_5() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___fFinishingAnchor_5)); }
	inline float get_fFinishingAnchor_5() const { return ___fFinishingAnchor_5; }
	inline float* get_address_of_fFinishingAnchor_5() { return &___fFinishingAnchor_5; }
	inline void set_fFinishingAnchor_5(float value)
	{
		___fFinishingAnchor_5 = value;
	}

	inline static int32_t get_offset_of_StartPlaying_6() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___StartPlaying_6)); }
	inline bool get_StartPlaying_6() const { return ___StartPlaying_6; }
	inline bool* get_address_of_StartPlaying_6() { return &___StartPlaying_6; }
	inline void set_StartPlaying_6(bool value)
	{
		___StartPlaying_6 = value;
	}

	inline static int32_t get_offset_of_IsPlaying_7() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___IsPlaying_7)); }
	inline bool get_IsPlaying_7() const { return ___IsPlaying_7; }
	inline bool* get_address_of_IsPlaying_7() { return &___IsPlaying_7; }
	inline void set_IsPlaying_7(bool value)
	{
		___IsPlaying_7 = value;
	}

	inline static int32_t get_offset_of_VoiceFile_8() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___VoiceFile_8)); }
	inline String_t* get_VoiceFile_8() const { return ___VoiceFile_8; }
	inline String_t** get_address_of_VoiceFile_8() { return &___VoiceFile_8; }
	inline void set_VoiceFile_8(String_t* value)
	{
		___VoiceFile_8 = value;
		Il2CppCodeGenWriteBarrier((&___VoiceFile_8), value);
	}

	inline static int32_t get_offset_of_VoiceAudilClip_9() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___VoiceAudilClip_9)); }
	inline AudioClip_t1419814565 * get_VoiceAudilClip_9() const { return ___VoiceAudilClip_9; }
	inline AudioClip_t1419814565 ** get_address_of_VoiceAudilClip_9() { return &___VoiceAudilClip_9; }
	inline void set_VoiceAudilClip_9(AudioClip_t1419814565 * value)
	{
		___VoiceAudilClip_9 = value;
		Il2CppCodeGenWriteBarrier((&___VoiceAudilClip_9), value);
	}

	inline static int32_t get_offset_of_m_Voice_10() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___m_Voice_10)); }
	inline AudioSource_t4025721661 * get_m_Voice_10() const { return ___m_Voice_10; }
	inline AudioSource_t4025721661 ** get_address_of_m_Voice_10() { return &___m_Voice_10; }
	inline void set_m_Voice_10(AudioSource_t4025721661 * value)
	{
		___m_Voice_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Voice_10), value);
	}

	inline static int32_t get_offset_of_CorrectBarrel_11() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___CorrectBarrel_11)); }
	inline GameObject_t2557347079 * get_CorrectBarrel_11() const { return ___CorrectBarrel_11; }
	inline GameObject_t2557347079 ** get_address_of_CorrectBarrel_11() { return &___CorrectBarrel_11; }
	inline void set_CorrectBarrel_11(GameObject_t2557347079 * value)
	{
		___CorrectBarrel_11 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectBarrel_11), value);
	}

	inline static int32_t get_offset_of_GameID_12() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___GameID_12)); }
	inline String_t* get_GameID_12() const { return ___GameID_12; }
	inline String_t** get_address_of_GameID_12() { return &___GameID_12; }
	inline void set_GameID_12(String_t* value)
	{
		___GameID_12 = value;
		Il2CppCodeGenWriteBarrier((&___GameID_12), value);
	}

	inline static int32_t get_offset_of_buttonTexture_14() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___buttonTexture_14)); }
	inline Texture2D_t3063074017 * get_buttonTexture_14() const { return ___buttonTexture_14; }
	inline Texture2D_t3063074017 ** get_address_of_buttonTexture_14() { return &___buttonTexture_14; }
	inline void set_buttonTexture_14(Texture2D_t3063074017 * value)
	{
		___buttonTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTexture_14), value);
	}

	inline static int32_t get_offset_of_skin_15() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615, ___skin_15)); }
	inline GUISkin_t2122630221 * get_skin_15() const { return ___skin_15; }
	inline GUISkin_t2122630221 ** get_address_of_skin_15() { return &___skin_15; }
	inline void set_skin_15(GUISkin_t2122630221 * value)
	{
		___skin_15 = value;
		Il2CppCodeGenWriteBarrier((&___skin_15), value);
	}
};

struct PlayDialog_t1022950615_StaticFields
{
public:
	// UnityEngine.Texture PlayDialog::button
	Texture_t2119925672 * ___button_13;

public:
	inline static int32_t get_offset_of_button_13() { return static_cast<int32_t>(offsetof(PlayDialog_t1022950615_StaticFields, ___button_13)); }
	inline Texture_t2119925672 * get_button_13() const { return ___button_13; }
	inline Texture_t2119925672 ** get_address_of_button_13() { return &___button_13; }
	inline void set_button_13(Texture_t2119925672 * value)
	{
		___button_13 = value;
		Il2CppCodeGenWriteBarrier((&___button_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYDIALOG_T1022950615_H
#ifndef OPENCHEST_T1184101868_H
#define OPENCHEST_T1184101868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenChest
struct  OpenChest_t1184101868  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] OpenChest::Buttons
	GameObjectU5BU5D_t2988620542* ___Buttons_2;
	// System.String[] OpenChest::ListOne
	StringU5BU5D_t2511808107* ___ListOne_3;
	// System.String[] OpenChest::ListTwo
	StringU5BU5D_t2511808107* ___ListTwo_4;
	// System.String[] OpenChest::ListThree
	StringU5BU5D_t2511808107* ___ListThree_5;
	// System.Int32[] OpenChest::Index
	Int32U5BU5D_t1965588061* ___Index_6;
	// System.Int32[] OpenChest::CorrectAnswer
	Int32U5BU5D_t1965588061* ___CorrectAnswer_7;
	// System.String OpenChest::strCameraID
	String_t* ___strCameraID_8;
	// System.Boolean OpenChest::m_bTextEnabled
	bool ___m_bTextEnabled_9;
	// System.Boolean OpenChest::m_bActive
	bool ___m_bActive_10;
	// UnityEngine.Component[] OpenChest::Texts
	ComponentU5BU5D_t3840885519* ___Texts_11;
	// UnityEngine.Texture2D OpenChest::Background
	Texture2D_t3063074017 * ___Background_12;
	// UnityEngine.Texture2D OpenChest::Background_Unlocked
	Texture2D_t3063074017 * ___Background_Unlocked_13;
	// UnityEngine.Texture2D OpenChest::m_chestLight3y
	Texture2D_t3063074017 * ___m_chestLight3y_14;
	// UnityEngine.Texture2D OpenChest::m_chestLight3
	Texture2D_t3063074017 * ___m_chestLight3_15;
	// UnityEngine.Texture2D OpenChest::m_chestLight2y
	Texture2D_t3063074017 * ___m_chestLight2y_16;
	// UnityEngine.Texture2D OpenChest::m_chestLight2
	Texture2D_t3063074017 * ___m_chestLight2_17;
	// UnityEngine.Texture2D OpenChest::m_chestLight1y
	Texture2D_t3063074017 * ___m_chestLight1y_18;
	// UnityEngine.Texture2D OpenChest::m_chestLight1
	Texture2D_t3063074017 * ___m_chestLight1_19;
	// UnityEngine.Rect[] OpenChest::m_Rect
	RectU5BU5D_t141872167* ___m_Rect_20;
	// UnityEngine.Transform OpenChest::ChestLid
	Transform_t362059596 * ___ChestLid_21;
	// UnityEngine.Transform OpenChest::CaveDoor
	Transform_t362059596 * ___CaveDoor_22;
	// System.Boolean OpenChest::m_bEndTimer
	bool ___m_bEndTimer_23;
	// System.Single OpenChest::m_fEndTimer
	float ___m_fEndTimer_24;
	// System.Boolean OpenChest::m_bIs2D
	bool ___m_bIs2D_25;
	// System.Boolean OpenChest::m_bTuiStartFlying
	bool ___m_bTuiStartFlying_26;
	// System.Boolean OpenChest::m_bEffectsStarted
	bool ___m_bEffectsStarted_27;
	// System.Boolean OpenChest::m_bIsOpened
	bool ___m_bIsOpened_28;
	// UnityEngine.GameObject OpenChest::m_gTuiFlyoutParticleClone
	GameObject_t2557347079 * ___m_gTuiFlyoutParticleClone_29;
	// UnityEngine.GameObject OpenChest::m_gTuiFlyoutparticle
	GameObject_t2557347079 * ___m_gTuiFlyoutparticle_30;
	// UnityEngine.UI.Text[] OpenChest::m_threeWords
	TextU5BU5D_t1489827645* ___m_threeWords_31;

public:
	inline static int32_t get_offset_of_Buttons_2() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___Buttons_2)); }
	inline GameObjectU5BU5D_t2988620542* get_Buttons_2() const { return ___Buttons_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Buttons_2() { return &___Buttons_2; }
	inline void set_Buttons_2(GameObjectU5BU5D_t2988620542* value)
	{
		___Buttons_2 = value;
		Il2CppCodeGenWriteBarrier((&___Buttons_2), value);
	}

	inline static int32_t get_offset_of_ListOne_3() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___ListOne_3)); }
	inline StringU5BU5D_t2511808107* get_ListOne_3() const { return ___ListOne_3; }
	inline StringU5BU5D_t2511808107** get_address_of_ListOne_3() { return &___ListOne_3; }
	inline void set_ListOne_3(StringU5BU5D_t2511808107* value)
	{
		___ListOne_3 = value;
		Il2CppCodeGenWriteBarrier((&___ListOne_3), value);
	}

	inline static int32_t get_offset_of_ListTwo_4() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___ListTwo_4)); }
	inline StringU5BU5D_t2511808107* get_ListTwo_4() const { return ___ListTwo_4; }
	inline StringU5BU5D_t2511808107** get_address_of_ListTwo_4() { return &___ListTwo_4; }
	inline void set_ListTwo_4(StringU5BU5D_t2511808107* value)
	{
		___ListTwo_4 = value;
		Il2CppCodeGenWriteBarrier((&___ListTwo_4), value);
	}

	inline static int32_t get_offset_of_ListThree_5() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___ListThree_5)); }
	inline StringU5BU5D_t2511808107* get_ListThree_5() const { return ___ListThree_5; }
	inline StringU5BU5D_t2511808107** get_address_of_ListThree_5() { return &___ListThree_5; }
	inline void set_ListThree_5(StringU5BU5D_t2511808107* value)
	{
		___ListThree_5 = value;
		Il2CppCodeGenWriteBarrier((&___ListThree_5), value);
	}

	inline static int32_t get_offset_of_Index_6() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___Index_6)); }
	inline Int32U5BU5D_t1965588061* get_Index_6() const { return ___Index_6; }
	inline Int32U5BU5D_t1965588061** get_address_of_Index_6() { return &___Index_6; }
	inline void set_Index_6(Int32U5BU5D_t1965588061* value)
	{
		___Index_6 = value;
		Il2CppCodeGenWriteBarrier((&___Index_6), value);
	}

	inline static int32_t get_offset_of_CorrectAnswer_7() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___CorrectAnswer_7)); }
	inline Int32U5BU5D_t1965588061* get_CorrectAnswer_7() const { return ___CorrectAnswer_7; }
	inline Int32U5BU5D_t1965588061** get_address_of_CorrectAnswer_7() { return &___CorrectAnswer_7; }
	inline void set_CorrectAnswer_7(Int32U5BU5D_t1965588061* value)
	{
		___CorrectAnswer_7 = value;
		Il2CppCodeGenWriteBarrier((&___CorrectAnswer_7), value);
	}

	inline static int32_t get_offset_of_strCameraID_8() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___strCameraID_8)); }
	inline String_t* get_strCameraID_8() const { return ___strCameraID_8; }
	inline String_t** get_address_of_strCameraID_8() { return &___strCameraID_8; }
	inline void set_strCameraID_8(String_t* value)
	{
		___strCameraID_8 = value;
		Il2CppCodeGenWriteBarrier((&___strCameraID_8), value);
	}

	inline static int32_t get_offset_of_m_bTextEnabled_9() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bTextEnabled_9)); }
	inline bool get_m_bTextEnabled_9() const { return ___m_bTextEnabled_9; }
	inline bool* get_address_of_m_bTextEnabled_9() { return &___m_bTextEnabled_9; }
	inline void set_m_bTextEnabled_9(bool value)
	{
		___m_bTextEnabled_9 = value;
	}

	inline static int32_t get_offset_of_m_bActive_10() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bActive_10)); }
	inline bool get_m_bActive_10() const { return ___m_bActive_10; }
	inline bool* get_address_of_m_bActive_10() { return &___m_bActive_10; }
	inline void set_m_bActive_10(bool value)
	{
		___m_bActive_10 = value;
	}

	inline static int32_t get_offset_of_Texts_11() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___Texts_11)); }
	inline ComponentU5BU5D_t3840885519* get_Texts_11() const { return ___Texts_11; }
	inline ComponentU5BU5D_t3840885519** get_address_of_Texts_11() { return &___Texts_11; }
	inline void set_Texts_11(ComponentU5BU5D_t3840885519* value)
	{
		___Texts_11 = value;
		Il2CppCodeGenWriteBarrier((&___Texts_11), value);
	}

	inline static int32_t get_offset_of_Background_12() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___Background_12)); }
	inline Texture2D_t3063074017 * get_Background_12() const { return ___Background_12; }
	inline Texture2D_t3063074017 ** get_address_of_Background_12() { return &___Background_12; }
	inline void set_Background_12(Texture2D_t3063074017 * value)
	{
		___Background_12 = value;
		Il2CppCodeGenWriteBarrier((&___Background_12), value);
	}

	inline static int32_t get_offset_of_Background_Unlocked_13() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___Background_Unlocked_13)); }
	inline Texture2D_t3063074017 * get_Background_Unlocked_13() const { return ___Background_Unlocked_13; }
	inline Texture2D_t3063074017 ** get_address_of_Background_Unlocked_13() { return &___Background_Unlocked_13; }
	inline void set_Background_Unlocked_13(Texture2D_t3063074017 * value)
	{
		___Background_Unlocked_13 = value;
		Il2CppCodeGenWriteBarrier((&___Background_Unlocked_13), value);
	}

	inline static int32_t get_offset_of_m_chestLight3y_14() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight3y_14)); }
	inline Texture2D_t3063074017 * get_m_chestLight3y_14() const { return ___m_chestLight3y_14; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight3y_14() { return &___m_chestLight3y_14; }
	inline void set_m_chestLight3y_14(Texture2D_t3063074017 * value)
	{
		___m_chestLight3y_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight3y_14), value);
	}

	inline static int32_t get_offset_of_m_chestLight3_15() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight3_15)); }
	inline Texture2D_t3063074017 * get_m_chestLight3_15() const { return ___m_chestLight3_15; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight3_15() { return &___m_chestLight3_15; }
	inline void set_m_chestLight3_15(Texture2D_t3063074017 * value)
	{
		___m_chestLight3_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight3_15), value);
	}

	inline static int32_t get_offset_of_m_chestLight2y_16() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight2y_16)); }
	inline Texture2D_t3063074017 * get_m_chestLight2y_16() const { return ___m_chestLight2y_16; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight2y_16() { return &___m_chestLight2y_16; }
	inline void set_m_chestLight2y_16(Texture2D_t3063074017 * value)
	{
		___m_chestLight2y_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight2y_16), value);
	}

	inline static int32_t get_offset_of_m_chestLight2_17() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight2_17)); }
	inline Texture2D_t3063074017 * get_m_chestLight2_17() const { return ___m_chestLight2_17; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight2_17() { return &___m_chestLight2_17; }
	inline void set_m_chestLight2_17(Texture2D_t3063074017 * value)
	{
		___m_chestLight2_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight2_17), value);
	}

	inline static int32_t get_offset_of_m_chestLight1y_18() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight1y_18)); }
	inline Texture2D_t3063074017 * get_m_chestLight1y_18() const { return ___m_chestLight1y_18; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight1y_18() { return &___m_chestLight1y_18; }
	inline void set_m_chestLight1y_18(Texture2D_t3063074017 * value)
	{
		___m_chestLight1y_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight1y_18), value);
	}

	inline static int32_t get_offset_of_m_chestLight1_19() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_chestLight1_19)); }
	inline Texture2D_t3063074017 * get_m_chestLight1_19() const { return ___m_chestLight1_19; }
	inline Texture2D_t3063074017 ** get_address_of_m_chestLight1_19() { return &___m_chestLight1_19; }
	inline void set_m_chestLight1_19(Texture2D_t3063074017 * value)
	{
		___m_chestLight1_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_chestLight1_19), value);
	}

	inline static int32_t get_offset_of_m_Rect_20() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_Rect_20)); }
	inline RectU5BU5D_t141872167* get_m_Rect_20() const { return ___m_Rect_20; }
	inline RectU5BU5D_t141872167** get_address_of_m_Rect_20() { return &___m_Rect_20; }
	inline void set_m_Rect_20(RectU5BU5D_t141872167* value)
	{
		___m_Rect_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_20), value);
	}

	inline static int32_t get_offset_of_ChestLid_21() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___ChestLid_21)); }
	inline Transform_t362059596 * get_ChestLid_21() const { return ___ChestLid_21; }
	inline Transform_t362059596 ** get_address_of_ChestLid_21() { return &___ChestLid_21; }
	inline void set_ChestLid_21(Transform_t362059596 * value)
	{
		___ChestLid_21 = value;
		Il2CppCodeGenWriteBarrier((&___ChestLid_21), value);
	}

	inline static int32_t get_offset_of_CaveDoor_22() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___CaveDoor_22)); }
	inline Transform_t362059596 * get_CaveDoor_22() const { return ___CaveDoor_22; }
	inline Transform_t362059596 ** get_address_of_CaveDoor_22() { return &___CaveDoor_22; }
	inline void set_CaveDoor_22(Transform_t362059596 * value)
	{
		___CaveDoor_22 = value;
		Il2CppCodeGenWriteBarrier((&___CaveDoor_22), value);
	}

	inline static int32_t get_offset_of_m_bEndTimer_23() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bEndTimer_23)); }
	inline bool get_m_bEndTimer_23() const { return ___m_bEndTimer_23; }
	inline bool* get_address_of_m_bEndTimer_23() { return &___m_bEndTimer_23; }
	inline void set_m_bEndTimer_23(bool value)
	{
		___m_bEndTimer_23 = value;
	}

	inline static int32_t get_offset_of_m_fEndTimer_24() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_fEndTimer_24)); }
	inline float get_m_fEndTimer_24() const { return ___m_fEndTimer_24; }
	inline float* get_address_of_m_fEndTimer_24() { return &___m_fEndTimer_24; }
	inline void set_m_fEndTimer_24(float value)
	{
		___m_fEndTimer_24 = value;
	}

	inline static int32_t get_offset_of_m_bIs2D_25() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bIs2D_25)); }
	inline bool get_m_bIs2D_25() const { return ___m_bIs2D_25; }
	inline bool* get_address_of_m_bIs2D_25() { return &___m_bIs2D_25; }
	inline void set_m_bIs2D_25(bool value)
	{
		___m_bIs2D_25 = value;
	}

	inline static int32_t get_offset_of_m_bTuiStartFlying_26() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bTuiStartFlying_26)); }
	inline bool get_m_bTuiStartFlying_26() const { return ___m_bTuiStartFlying_26; }
	inline bool* get_address_of_m_bTuiStartFlying_26() { return &___m_bTuiStartFlying_26; }
	inline void set_m_bTuiStartFlying_26(bool value)
	{
		___m_bTuiStartFlying_26 = value;
	}

	inline static int32_t get_offset_of_m_bEffectsStarted_27() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bEffectsStarted_27)); }
	inline bool get_m_bEffectsStarted_27() const { return ___m_bEffectsStarted_27; }
	inline bool* get_address_of_m_bEffectsStarted_27() { return &___m_bEffectsStarted_27; }
	inline void set_m_bEffectsStarted_27(bool value)
	{
		___m_bEffectsStarted_27 = value;
	}

	inline static int32_t get_offset_of_m_bIsOpened_28() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_bIsOpened_28)); }
	inline bool get_m_bIsOpened_28() const { return ___m_bIsOpened_28; }
	inline bool* get_address_of_m_bIsOpened_28() { return &___m_bIsOpened_28; }
	inline void set_m_bIsOpened_28(bool value)
	{
		___m_bIsOpened_28 = value;
	}

	inline static int32_t get_offset_of_m_gTuiFlyoutParticleClone_29() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_gTuiFlyoutParticleClone_29)); }
	inline GameObject_t2557347079 * get_m_gTuiFlyoutParticleClone_29() const { return ___m_gTuiFlyoutParticleClone_29; }
	inline GameObject_t2557347079 ** get_address_of_m_gTuiFlyoutParticleClone_29() { return &___m_gTuiFlyoutParticleClone_29; }
	inline void set_m_gTuiFlyoutParticleClone_29(GameObject_t2557347079 * value)
	{
		___m_gTuiFlyoutParticleClone_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_gTuiFlyoutParticleClone_29), value);
	}

	inline static int32_t get_offset_of_m_gTuiFlyoutparticle_30() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_gTuiFlyoutparticle_30)); }
	inline GameObject_t2557347079 * get_m_gTuiFlyoutparticle_30() const { return ___m_gTuiFlyoutparticle_30; }
	inline GameObject_t2557347079 ** get_address_of_m_gTuiFlyoutparticle_30() { return &___m_gTuiFlyoutparticle_30; }
	inline void set_m_gTuiFlyoutparticle_30(GameObject_t2557347079 * value)
	{
		___m_gTuiFlyoutparticle_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_gTuiFlyoutparticle_30), value);
	}

	inline static int32_t get_offset_of_m_threeWords_31() { return static_cast<int32_t>(offsetof(OpenChest_t1184101868, ___m_threeWords_31)); }
	inline TextU5BU5D_t1489827645* get_m_threeWords_31() const { return ___m_threeWords_31; }
	inline TextU5BU5D_t1489827645** get_address_of_m_threeWords_31() { return &___m_threeWords_31; }
	inline void set_m_threeWords_31(TextU5BU5D_t1489827645* value)
	{
		___m_threeWords_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_threeWords_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENCHEST_T1184101868_H
#ifndef MINIGAMECOLLIDER_T469925002_H
#define MINIGAMECOLLIDER_T469925002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameCollider
struct  MiniGameCollider_t469925002  : public MonoBehaviour_t1618594486
{
public:
	// System.Int32 MiniGameCollider::index
	int32_t ___index_2;
	// System.Boolean MiniGameCollider::bTriggered
	bool ___bTriggered_3;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(MiniGameCollider_t469925002, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_bTriggered_3() { return static_cast<int32_t>(offsetof(MiniGameCollider_t469925002, ___bTriggered_3)); }
	inline bool get_bTriggered_3() const { return ___bTriggered_3; }
	inline bool* get_address_of_bTriggered_3() { return &___bTriggered_3; }
	inline void set_bTriggered_3(bool value)
	{
		___bTriggered_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIGAMECOLLIDER_T469925002_H
#ifndef LV7CATCHSPARX_T385659907_H
#define LV7CATCHSPARX_T385659907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7CatchSparx
struct  Lv7CatchSparx_t385659907  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject Lv7CatchSparx::move
	GameObject_t2557347079 * ___move_7;
	// UnityEngine.GameObject Lv7CatchSparx::sprint
	GameObject_t2557347079 * ___sprint_8;
	// UnityEngine.GameObject Lv7CatchSparx::bag
	GameObject_t2557347079 * ___bag_9;

public:
	inline static int32_t get_offset_of_move_7() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907, ___move_7)); }
	inline GameObject_t2557347079 * get_move_7() const { return ___move_7; }
	inline GameObject_t2557347079 ** get_address_of_move_7() { return &___move_7; }
	inline void set_move_7(GameObject_t2557347079 * value)
	{
		___move_7 = value;
		Il2CppCodeGenWriteBarrier((&___move_7), value);
	}

	inline static int32_t get_offset_of_sprint_8() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907, ___sprint_8)); }
	inline GameObject_t2557347079 * get_sprint_8() const { return ___sprint_8; }
	inline GameObject_t2557347079 ** get_address_of_sprint_8() { return &___sprint_8; }
	inline void set_sprint_8(GameObject_t2557347079 * value)
	{
		___sprint_8 = value;
		Il2CppCodeGenWriteBarrier((&___sprint_8), value);
	}

	inline static int32_t get_offset_of_bag_9() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907, ___bag_9)); }
	inline GameObject_t2557347079 * get_bag_9() const { return ___bag_9; }
	inline GameObject_t2557347079 ** get_address_of_bag_9() { return &___bag_9; }
	inline void set_bag_9(GameObject_t2557347079 * value)
	{
		___bag_9 = value;
		Il2CppCodeGenWriteBarrier((&___bag_9), value);
	}
};

struct Lv7CatchSparx_t385659907_StaticFields
{
public:
	// System.Boolean Lv7CatchSparx::PrepareStart
	bool ___PrepareStart_2;
	// System.Boolean Lv7CatchSparx::GameIsStarted
	bool ___GameIsStarted_3;
	// System.Int32 Lv7CatchSparx::NumOfSparksCaught
	int32_t ___NumOfSparksCaught_4;
	// System.Single Lv7CatchSparx::fTimer
	float ___fTimer_5;
	// System.Boolean Lv7CatchSparx::m_bGameIsEnded
	bool ___m_bGameIsEnded_6;

public:
	inline static int32_t get_offset_of_PrepareStart_2() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907_StaticFields, ___PrepareStart_2)); }
	inline bool get_PrepareStart_2() const { return ___PrepareStart_2; }
	inline bool* get_address_of_PrepareStart_2() { return &___PrepareStart_2; }
	inline void set_PrepareStart_2(bool value)
	{
		___PrepareStart_2 = value;
	}

	inline static int32_t get_offset_of_GameIsStarted_3() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907_StaticFields, ___GameIsStarted_3)); }
	inline bool get_GameIsStarted_3() const { return ___GameIsStarted_3; }
	inline bool* get_address_of_GameIsStarted_3() { return &___GameIsStarted_3; }
	inline void set_GameIsStarted_3(bool value)
	{
		___GameIsStarted_3 = value;
	}

	inline static int32_t get_offset_of_NumOfSparksCaught_4() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907_StaticFields, ___NumOfSparksCaught_4)); }
	inline int32_t get_NumOfSparksCaught_4() const { return ___NumOfSparksCaught_4; }
	inline int32_t* get_address_of_NumOfSparksCaught_4() { return &___NumOfSparksCaught_4; }
	inline void set_NumOfSparksCaught_4(int32_t value)
	{
		___NumOfSparksCaught_4 = value;
	}

	inline static int32_t get_offset_of_fTimer_5() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907_StaticFields, ___fTimer_5)); }
	inline float get_fTimer_5() const { return ___fTimer_5; }
	inline float* get_address_of_fTimer_5() { return &___fTimer_5; }
	inline void set_fTimer_5(float value)
	{
		___fTimer_5 = value;
	}

	inline static int32_t get_offset_of_m_bGameIsEnded_6() { return static_cast<int32_t>(offsetof(Lv7CatchSparx_t385659907_StaticFields, ___m_bGameIsEnded_6)); }
	inline bool get_m_bGameIsEnded_6() const { return ___m_bGameIsEnded_6; }
	inline bool* get_address_of_m_bGameIsEnded_6() { return &___m_bGameIsEnded_6; }
	inline void set_m_bGameIsEnded_6(bool value)
	{
		___m_bGameIsEnded_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7CATCHSPARX_T385659907_H
#ifndef LV7BRIDGEGNATS_T35555675_H
#define LV7BRIDGEGNATS_T35555675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7BridgeGnats
struct  Lv7BridgeGnats_t35555675  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Lv7BridgeGnats::StartGame
	bool ___StartGame_2;
	// System.Single Lv7BridgeGnats::Timer
	float ___Timer_3;
	// System.Boolean Lv7BridgeGnats::StartAttack
	bool ___StartAttack_4;

public:
	inline static int32_t get_offset_of_StartGame_2() { return static_cast<int32_t>(offsetof(Lv7BridgeGnats_t35555675, ___StartGame_2)); }
	inline bool get_StartGame_2() const { return ___StartGame_2; }
	inline bool* get_address_of_StartGame_2() { return &___StartGame_2; }
	inline void set_StartGame_2(bool value)
	{
		___StartGame_2 = value;
	}

	inline static int32_t get_offset_of_Timer_3() { return static_cast<int32_t>(offsetof(Lv7BridgeGnats_t35555675, ___Timer_3)); }
	inline float get_Timer_3() const { return ___Timer_3; }
	inline float* get_address_of_Timer_3() { return &___Timer_3; }
	inline void set_Timer_3(float value)
	{
		___Timer_3 = value;
	}

	inline static int32_t get_offset_of_StartAttack_4() { return static_cast<int32_t>(offsetof(Lv7BridgeGnats_t35555675, ___StartAttack_4)); }
	inline bool get_StartAttack_4() const { return ___StartAttack_4; }
	inline bool* get_address_of_StartAttack_4() { return &___StartAttack_4; }
	inline void set_StartAttack_4(bool value)
	{
		___StartAttack_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7BRIDGEGNATS_T35555675_H
#ifndef INGAMEMUSICSWITCH_T653995320_H
#define INGAMEMUSICSWITCH_T653995320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameMusicSwitch
struct  IngameMusicSwitch_t653995320  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.AudioSource IngameMusicSwitch::m_gameMusicAs
	AudioSource_t4025721661 * ___m_gameMusicAs_3;
	// System.String IngameMusicSwitch::currentSceneName
	String_t* ___currentSceneName_4;
	// UnityEngine.AudioClip IngameMusicSwitch::m_gameStartFadeMusic
	AudioClip_t1419814565 * ___m_gameStartFadeMusic_5;
	// UnityEngine.AudioClip IngameMusicSwitch::m_guideFadeMusic
	AudioClip_t1419814565 * ___m_guideFadeMusic_6;
	// UnityEngine.AudioClip IngameMusicSwitch::m_mentorMusic
	AudioClip_t1419814565 * ___m_mentorMusic_7;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l1Music
	AudioClip_t1419814565 * ___m_l1Music_8;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l2Music
	AudioClip_t1419814565 * ___m_l2Music_9;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l3Music
	AudioClip_t1419814565 * ___m_l3Music_10;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l4Music
	AudioClip_t1419814565 * ___m_l4Music_11;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l5Music
	AudioClip_t1419814565 * ___m_l5Music_12;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l6Music
	AudioClip_t1419814565 * ___m_l6Music_13;
	// UnityEngine.AudioClip IngameMusicSwitch::m_l7Music
	AudioClip_t1419814565 * ___m_l7Music_14;
	// System.Boolean IngameMusicSwitch::m_playStartFadeMisic
	bool ___m_playStartFadeMisic_15;
	// System.Boolean IngameMusicSwitch::m_playFadeMisic
	bool ___m_playFadeMisic_16;

public:
	inline static int32_t get_offset_of_m_gameMusicAs_3() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_gameMusicAs_3)); }
	inline AudioSource_t4025721661 * get_m_gameMusicAs_3() const { return ___m_gameMusicAs_3; }
	inline AudioSource_t4025721661 ** get_address_of_m_gameMusicAs_3() { return &___m_gameMusicAs_3; }
	inline void set_m_gameMusicAs_3(AudioSource_t4025721661 * value)
	{
		___m_gameMusicAs_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameMusicAs_3), value);
	}

	inline static int32_t get_offset_of_currentSceneName_4() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___currentSceneName_4)); }
	inline String_t* get_currentSceneName_4() const { return ___currentSceneName_4; }
	inline String_t** get_address_of_currentSceneName_4() { return &___currentSceneName_4; }
	inline void set_currentSceneName_4(String_t* value)
	{
		___currentSceneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentSceneName_4), value);
	}

	inline static int32_t get_offset_of_m_gameStartFadeMusic_5() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_gameStartFadeMusic_5)); }
	inline AudioClip_t1419814565 * get_m_gameStartFadeMusic_5() const { return ___m_gameStartFadeMusic_5; }
	inline AudioClip_t1419814565 ** get_address_of_m_gameStartFadeMusic_5() { return &___m_gameStartFadeMusic_5; }
	inline void set_m_gameStartFadeMusic_5(AudioClip_t1419814565 * value)
	{
		___m_gameStartFadeMusic_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameStartFadeMusic_5), value);
	}

	inline static int32_t get_offset_of_m_guideFadeMusic_6() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_guideFadeMusic_6)); }
	inline AudioClip_t1419814565 * get_m_guideFadeMusic_6() const { return ___m_guideFadeMusic_6; }
	inline AudioClip_t1419814565 ** get_address_of_m_guideFadeMusic_6() { return &___m_guideFadeMusic_6; }
	inline void set_m_guideFadeMusic_6(AudioClip_t1419814565 * value)
	{
		___m_guideFadeMusic_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_guideFadeMusic_6), value);
	}

	inline static int32_t get_offset_of_m_mentorMusic_7() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_mentorMusic_7)); }
	inline AudioClip_t1419814565 * get_m_mentorMusic_7() const { return ___m_mentorMusic_7; }
	inline AudioClip_t1419814565 ** get_address_of_m_mentorMusic_7() { return &___m_mentorMusic_7; }
	inline void set_m_mentorMusic_7(AudioClip_t1419814565 * value)
	{
		___m_mentorMusic_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_mentorMusic_7), value);
	}

	inline static int32_t get_offset_of_m_l1Music_8() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l1Music_8)); }
	inline AudioClip_t1419814565 * get_m_l1Music_8() const { return ___m_l1Music_8; }
	inline AudioClip_t1419814565 ** get_address_of_m_l1Music_8() { return &___m_l1Music_8; }
	inline void set_m_l1Music_8(AudioClip_t1419814565 * value)
	{
		___m_l1Music_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_l1Music_8), value);
	}

	inline static int32_t get_offset_of_m_l2Music_9() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l2Music_9)); }
	inline AudioClip_t1419814565 * get_m_l2Music_9() const { return ___m_l2Music_9; }
	inline AudioClip_t1419814565 ** get_address_of_m_l2Music_9() { return &___m_l2Music_9; }
	inline void set_m_l2Music_9(AudioClip_t1419814565 * value)
	{
		___m_l2Music_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_l2Music_9), value);
	}

	inline static int32_t get_offset_of_m_l3Music_10() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l3Music_10)); }
	inline AudioClip_t1419814565 * get_m_l3Music_10() const { return ___m_l3Music_10; }
	inline AudioClip_t1419814565 ** get_address_of_m_l3Music_10() { return &___m_l3Music_10; }
	inline void set_m_l3Music_10(AudioClip_t1419814565 * value)
	{
		___m_l3Music_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_l3Music_10), value);
	}

	inline static int32_t get_offset_of_m_l4Music_11() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l4Music_11)); }
	inline AudioClip_t1419814565 * get_m_l4Music_11() const { return ___m_l4Music_11; }
	inline AudioClip_t1419814565 ** get_address_of_m_l4Music_11() { return &___m_l4Music_11; }
	inline void set_m_l4Music_11(AudioClip_t1419814565 * value)
	{
		___m_l4Music_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_l4Music_11), value);
	}

	inline static int32_t get_offset_of_m_l5Music_12() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l5Music_12)); }
	inline AudioClip_t1419814565 * get_m_l5Music_12() const { return ___m_l5Music_12; }
	inline AudioClip_t1419814565 ** get_address_of_m_l5Music_12() { return &___m_l5Music_12; }
	inline void set_m_l5Music_12(AudioClip_t1419814565 * value)
	{
		___m_l5Music_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_l5Music_12), value);
	}

	inline static int32_t get_offset_of_m_l6Music_13() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l6Music_13)); }
	inline AudioClip_t1419814565 * get_m_l6Music_13() const { return ___m_l6Music_13; }
	inline AudioClip_t1419814565 ** get_address_of_m_l6Music_13() { return &___m_l6Music_13; }
	inline void set_m_l6Music_13(AudioClip_t1419814565 * value)
	{
		___m_l6Music_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_l6Music_13), value);
	}

	inline static int32_t get_offset_of_m_l7Music_14() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_l7Music_14)); }
	inline AudioClip_t1419814565 * get_m_l7Music_14() const { return ___m_l7Music_14; }
	inline AudioClip_t1419814565 ** get_address_of_m_l7Music_14() { return &___m_l7Music_14; }
	inline void set_m_l7Music_14(AudioClip_t1419814565 * value)
	{
		___m_l7Music_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_l7Music_14), value);
	}

	inline static int32_t get_offset_of_m_playStartFadeMisic_15() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_playStartFadeMisic_15)); }
	inline bool get_m_playStartFadeMisic_15() const { return ___m_playStartFadeMisic_15; }
	inline bool* get_address_of_m_playStartFadeMisic_15() { return &___m_playStartFadeMisic_15; }
	inline void set_m_playStartFadeMisic_15(bool value)
	{
		___m_playStartFadeMisic_15 = value;
	}

	inline static int32_t get_offset_of_m_playFadeMisic_16() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320, ___m_playFadeMisic_16)); }
	inline bool get_m_playFadeMisic_16() const { return ___m_playFadeMisic_16; }
	inline bool* get_address_of_m_playFadeMisic_16() { return &___m_playFadeMisic_16; }
	inline void set_m_playFadeMisic_16(bool value)
	{
		___m_playFadeMisic_16 = value;
	}
};

struct IngameMusicSwitch_t653995320_StaticFields
{
public:
	// IngameMusicSwitch IngameMusicSwitch::instanceRef
	IngameMusicSwitch_t653995320 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(IngameMusicSwitch_t653995320_StaticFields, ___instanceRef_2)); }
	inline IngameMusicSwitch_t653995320 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline IngameMusicSwitch_t653995320 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(IngameMusicSwitch_t653995320 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMEMUSICSWITCH_T653995320_H
#ifndef LV7BRIDGEGNATACTION_T427326750_H
#define LV7BRIDGEGNATACTION_T427326750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv7BridgeGnatAction
struct  Lv7BridgeGnatAction_t427326750  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 Lv7BridgeGnatAction::OriginalPos
	Vector3_t1986933152  ___OriginalPos_2;
	// Lv7BridgeGnatStatus Lv7BridgeGnatAction::action
	int32_t ___action_3;
	// System.Single Lv7BridgeGnatAction::fStandbyTime
	float ___fStandbyTime_4;
	// System.Single Lv7BridgeGnatAction::fRespawnTime
	float ___fRespawnTime_5;

public:
	inline static int32_t get_offset_of_OriginalPos_2() { return static_cast<int32_t>(offsetof(Lv7BridgeGnatAction_t427326750, ___OriginalPos_2)); }
	inline Vector3_t1986933152  get_OriginalPos_2() const { return ___OriginalPos_2; }
	inline Vector3_t1986933152 * get_address_of_OriginalPos_2() { return &___OriginalPos_2; }
	inline void set_OriginalPos_2(Vector3_t1986933152  value)
	{
		___OriginalPos_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(Lv7BridgeGnatAction_t427326750, ___action_3)); }
	inline int32_t get_action_3() const { return ___action_3; }
	inline int32_t* get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(int32_t value)
	{
		___action_3 = value;
	}

	inline static int32_t get_offset_of_fStandbyTime_4() { return static_cast<int32_t>(offsetof(Lv7BridgeGnatAction_t427326750, ___fStandbyTime_4)); }
	inline float get_fStandbyTime_4() const { return ___fStandbyTime_4; }
	inline float* get_address_of_fStandbyTime_4() { return &___fStandbyTime_4; }
	inline void set_fStandbyTime_4(float value)
	{
		___fStandbyTime_4 = value;
	}

	inline static int32_t get_offset_of_fRespawnTime_5() { return static_cast<int32_t>(offsetof(Lv7BridgeGnatAction_t427326750, ___fRespawnTime_5)); }
	inline float get_fRespawnTime_5() const { return ___fRespawnTime_5; }
	inline float* get_address_of_fRespawnTime_5() { return &___fRespawnTime_5; }
	inline void set_fRespawnTime_5(float value)
	{
		___fRespawnTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV7BRIDGEGNATACTION_T427326750_H
#ifndef LEVEL5MINIGAME2_T1561600819_H
#define LEVEL5MINIGAME2_T1561600819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level5MiniGame2
struct  Level5MiniGame2_t1561600819  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform Level5MiniGame2::CenterPos
	Transform_t362059596 * ___CenterPos_2;
	// UnityEngine.Vector3 Level5MiniGame2::GnatOriginalPos
	Vector3_t1986933152  ___GnatOriginalPos_3;
	// UnityEngine.GameObject Level5MiniGame2::CurrentObject
	GameObject_t2557347079 * ___CurrentObject_4;
	// System.Boolean Level5MiniGame2::GameStart
	bool ___GameStart_5;
	// System.String Level5MiniGame2::CameraID
	String_t* ___CameraID_6;
	// Lv5MiniGame2Status Level5MiniGame2::Status
	int32_t ___Status_7;
	// UnityEngine.Vector3 Level5MiniGame2::SparkFlyToPos
	Vector3_t1986933152  ___SparkFlyToPos_8;
	// UnityEngine.GameObject Level5MiniGame2::explosionEffect
	GameObject_t2557347079 * ___explosionEffect_9;
	// UnityEngine.Vector3 Level5MiniGame2::StartPos
	Vector3_t1986933152  ___StartPos_10;
	// System.Single Level5MiniGame2::m_fTime
	float ___m_fTime_11;
	// System.Boolean Level5MiniGame2::m_bTuiConversationStarts
	bool ___m_bTuiConversationStarts_12;
	// System.Boolean Level5MiniGame2::m_bPlayerMovingToPoint
	bool ___m_bPlayerMovingToPoint_13;

public:
	inline static int32_t get_offset_of_CenterPos_2() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___CenterPos_2)); }
	inline Transform_t362059596 * get_CenterPos_2() const { return ___CenterPos_2; }
	inline Transform_t362059596 ** get_address_of_CenterPos_2() { return &___CenterPos_2; }
	inline void set_CenterPos_2(Transform_t362059596 * value)
	{
		___CenterPos_2 = value;
		Il2CppCodeGenWriteBarrier((&___CenterPos_2), value);
	}

	inline static int32_t get_offset_of_GnatOriginalPos_3() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___GnatOriginalPos_3)); }
	inline Vector3_t1986933152  get_GnatOriginalPos_3() const { return ___GnatOriginalPos_3; }
	inline Vector3_t1986933152 * get_address_of_GnatOriginalPos_3() { return &___GnatOriginalPos_3; }
	inline void set_GnatOriginalPos_3(Vector3_t1986933152  value)
	{
		___GnatOriginalPos_3 = value;
	}

	inline static int32_t get_offset_of_CurrentObject_4() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___CurrentObject_4)); }
	inline GameObject_t2557347079 * get_CurrentObject_4() const { return ___CurrentObject_4; }
	inline GameObject_t2557347079 ** get_address_of_CurrentObject_4() { return &___CurrentObject_4; }
	inline void set_CurrentObject_4(GameObject_t2557347079 * value)
	{
		___CurrentObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentObject_4), value);
	}

	inline static int32_t get_offset_of_GameStart_5() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___GameStart_5)); }
	inline bool get_GameStart_5() const { return ___GameStart_5; }
	inline bool* get_address_of_GameStart_5() { return &___GameStart_5; }
	inline void set_GameStart_5(bool value)
	{
		___GameStart_5 = value;
	}

	inline static int32_t get_offset_of_CameraID_6() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___CameraID_6)); }
	inline String_t* get_CameraID_6() const { return ___CameraID_6; }
	inline String_t** get_address_of_CameraID_6() { return &___CameraID_6; }
	inline void set_CameraID_6(String_t* value)
	{
		___CameraID_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_6), value);
	}

	inline static int32_t get_offset_of_Status_7() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___Status_7)); }
	inline int32_t get_Status_7() const { return ___Status_7; }
	inline int32_t* get_address_of_Status_7() { return &___Status_7; }
	inline void set_Status_7(int32_t value)
	{
		___Status_7 = value;
	}

	inline static int32_t get_offset_of_SparkFlyToPos_8() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___SparkFlyToPos_8)); }
	inline Vector3_t1986933152  get_SparkFlyToPos_8() const { return ___SparkFlyToPos_8; }
	inline Vector3_t1986933152 * get_address_of_SparkFlyToPos_8() { return &___SparkFlyToPos_8; }
	inline void set_SparkFlyToPos_8(Vector3_t1986933152  value)
	{
		___SparkFlyToPos_8 = value;
	}

	inline static int32_t get_offset_of_explosionEffect_9() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___explosionEffect_9)); }
	inline GameObject_t2557347079 * get_explosionEffect_9() const { return ___explosionEffect_9; }
	inline GameObject_t2557347079 ** get_address_of_explosionEffect_9() { return &___explosionEffect_9; }
	inline void set_explosionEffect_9(GameObject_t2557347079 * value)
	{
		___explosionEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___explosionEffect_9), value);
	}

	inline static int32_t get_offset_of_StartPos_10() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___StartPos_10)); }
	inline Vector3_t1986933152  get_StartPos_10() const { return ___StartPos_10; }
	inline Vector3_t1986933152 * get_address_of_StartPos_10() { return &___StartPos_10; }
	inline void set_StartPos_10(Vector3_t1986933152  value)
	{
		___StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_fTime_11() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___m_fTime_11)); }
	inline float get_m_fTime_11() const { return ___m_fTime_11; }
	inline float* get_address_of_m_fTime_11() { return &___m_fTime_11; }
	inline void set_m_fTime_11(float value)
	{
		___m_fTime_11 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationStarts_12() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___m_bTuiConversationStarts_12)); }
	inline bool get_m_bTuiConversationStarts_12() const { return ___m_bTuiConversationStarts_12; }
	inline bool* get_address_of_m_bTuiConversationStarts_12() { return &___m_bTuiConversationStarts_12; }
	inline void set_m_bTuiConversationStarts_12(bool value)
	{
		___m_bTuiConversationStarts_12 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerMovingToPoint_13() { return static_cast<int32_t>(offsetof(Level5MiniGame2_t1561600819, ___m_bPlayerMovingToPoint_13)); }
	inline bool get_m_bPlayerMovingToPoint_13() const { return ___m_bPlayerMovingToPoint_13; }
	inline bool* get_address_of_m_bPlayerMovingToPoint_13() { return &___m_bPlayerMovingToPoint_13; }
	inline void set_m_bPlayerMovingToPoint_13(bool value)
	{
		___m_bPlayerMovingToPoint_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL5MINIGAME2_T1561600819_H
#ifndef L5GNATCLICK_T676204875_H
#define L5GNATCLICK_T676204875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5GnatClick
struct  L5GnatClick_t676204875  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Texture2D L5GnatClick::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_2;
	// UnityEngine.Texture2D L5GnatClick::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_3;
	// System.Boolean L5GnatClick::ClickAble
	bool ___ClickAble_4;

public:
	inline static int32_t get_offset_of_MouseOverTexture_2() { return static_cast<int32_t>(offsetof(L5GnatClick_t676204875, ___MouseOverTexture_2)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_2() const { return ___MouseOverTexture_2; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_2() { return &___MouseOverTexture_2; }
	inline void set_MouseOverTexture_2(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_2), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_3() { return static_cast<int32_t>(offsetof(L5GnatClick_t676204875, ___MouseOriTexture_3)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_3() const { return ___MouseOriTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_3() { return &___MouseOriTexture_3; }
	inline void set_MouseOriTexture_3(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_3), value);
	}

	inline static int32_t get_offset_of_ClickAble_4() { return static_cast<int32_t>(offsetof(L5GnatClick_t676204875, ___ClickAble_4)); }
	inline bool get_ClickAble_4() const { return ___ClickAble_4; }
	inline bool* get_address_of_ClickAble_4() { return &___ClickAble_4; }
	inline void set_ClickAble_4(bool value)
	{
		___ClickAble_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5GNATCLICK_T676204875_H
#ifndef L4STEPSMINIGAME_T1956426605_H
#define L4STEPSMINIGAME_T1956426605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4StepsMiniGame
struct  L4StepsMiniGame_t1956426605  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L4StepsMiniGame::m_Gem
	GameObject_t2557347079 * ___m_Gem_2;
	// UnityEngine.GameObject[] L4StepsMiniGame::m_Sparks
	GameObjectU5BU5D_t2988620542* ___m_Sparks_3;
	// UnityEngine.Transform[] L4StepsMiniGame::m_SparksWaitPos
	TransformU5BU5D_t2383644165* ___m_SparksWaitPos_4;
	// UnityEngine.Transform[] L4StepsMiniGame::m_HoverPos
	TransformU5BU5D_t2383644165* ___m_HoverPos_5;
	// UnityEngine.Transform L4StepsMiniGame::m_PlayerMoveTo
	Transform_t362059596 * ___m_PlayerMoveTo_6;
	// UnityEngine.Transform L4StepsMiniGame::m_SparkMoveTo
	Transform_t362059596 * ___m_SparkMoveTo_7;
	// UnityEngine.GameObject L4StepsMiniGame::player
	GameObject_t2557347079 * ___player_8;
	// UnityEngine.Vector3 L4StepsMiniGame::m_vSparksMoveFrom
	Vector3_t1986933152  ___m_vSparksMoveFrom_9;
	// EndPartStatus L4StepsMiniGame::m_EndStatus
	int32_t ___m_EndStatus_10;
	// System.Single L4StepsMiniGame::m_fTime
	float ___m_fTime_11;
	// System.Int32 L4StepsMiniGame::m_iCurrentQuestion
	int32_t ___m_iCurrentQuestion_12;
	// System.Int32 L4StepsMiniGame::m_iDisplayedQuestion
	int32_t ___m_iDisplayedQuestion_13;
	// System.Boolean L4StepsMiniGame::m_bInteracted
	bool ___m_bInteracted_14;
	// System.Boolean L4StepsMiniGame::m_bMovingPlayer
	bool ___m_bMovingPlayer_15;
	// System.Boolean L4StepsMiniGame::m_bTalkingToSpark
	bool ___m_bTalkingToSpark_16;
	// System.Boolean L4StepsMiniGame::m_bGameActive
	bool ___m_bGameActive_17;
	// System.Boolean L4StepsMiniGame::m_bDoneTalking
	bool ___m_bDoneTalking_18;
	// System.Boolean L4StepsMiniGame::m_bEndScene
	bool ___m_bEndScene_19;
	// System.Boolean L4StepsMiniGame::m_bSparkMovingToGem
	bool ___m_bSparkMovingToGem_20;
	// System.Boolean L4StepsMiniGame::m_bTuiFlyingDown
	bool ___m_bTuiFlyingDown_21;

public:
	inline static int32_t get_offset_of_m_Gem_2() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_Gem_2)); }
	inline GameObject_t2557347079 * get_m_Gem_2() const { return ___m_Gem_2; }
	inline GameObject_t2557347079 ** get_address_of_m_Gem_2() { return &___m_Gem_2; }
	inline void set_m_Gem_2(GameObject_t2557347079 * value)
	{
		___m_Gem_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Gem_2), value);
	}

	inline static int32_t get_offset_of_m_Sparks_3() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_Sparks_3)); }
	inline GameObjectU5BU5D_t2988620542* get_m_Sparks_3() const { return ___m_Sparks_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_m_Sparks_3() { return &___m_Sparks_3; }
	inline void set_m_Sparks_3(GameObjectU5BU5D_t2988620542* value)
	{
		___m_Sparks_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sparks_3), value);
	}

	inline static int32_t get_offset_of_m_SparksWaitPos_4() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_SparksWaitPos_4)); }
	inline TransformU5BU5D_t2383644165* get_m_SparksWaitPos_4() const { return ___m_SparksWaitPos_4; }
	inline TransformU5BU5D_t2383644165** get_address_of_m_SparksWaitPos_4() { return &___m_SparksWaitPos_4; }
	inline void set_m_SparksWaitPos_4(TransformU5BU5D_t2383644165* value)
	{
		___m_SparksWaitPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SparksWaitPos_4), value);
	}

	inline static int32_t get_offset_of_m_HoverPos_5() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_HoverPos_5)); }
	inline TransformU5BU5D_t2383644165* get_m_HoverPos_5() const { return ___m_HoverPos_5; }
	inline TransformU5BU5D_t2383644165** get_address_of_m_HoverPos_5() { return &___m_HoverPos_5; }
	inline void set_m_HoverPos_5(TransformU5BU5D_t2383644165* value)
	{
		___m_HoverPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_HoverPos_5), value);
	}

	inline static int32_t get_offset_of_m_PlayerMoveTo_6() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_PlayerMoveTo_6)); }
	inline Transform_t362059596 * get_m_PlayerMoveTo_6() const { return ___m_PlayerMoveTo_6; }
	inline Transform_t362059596 ** get_address_of_m_PlayerMoveTo_6() { return &___m_PlayerMoveTo_6; }
	inline void set_m_PlayerMoveTo_6(Transform_t362059596 * value)
	{
		___m_PlayerMoveTo_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerMoveTo_6), value);
	}

	inline static int32_t get_offset_of_m_SparkMoveTo_7() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_SparkMoveTo_7)); }
	inline Transform_t362059596 * get_m_SparkMoveTo_7() const { return ___m_SparkMoveTo_7; }
	inline Transform_t362059596 ** get_address_of_m_SparkMoveTo_7() { return &___m_SparkMoveTo_7; }
	inline void set_m_SparkMoveTo_7(Transform_t362059596 * value)
	{
		___m_SparkMoveTo_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SparkMoveTo_7), value);
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___player_8)); }
	inline GameObject_t2557347079 * get_player_8() const { return ___player_8; }
	inline GameObject_t2557347079 ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(GameObject_t2557347079 * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier((&___player_8), value);
	}

	inline static int32_t get_offset_of_m_vSparksMoveFrom_9() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_vSparksMoveFrom_9)); }
	inline Vector3_t1986933152  get_m_vSparksMoveFrom_9() const { return ___m_vSparksMoveFrom_9; }
	inline Vector3_t1986933152 * get_address_of_m_vSparksMoveFrom_9() { return &___m_vSparksMoveFrom_9; }
	inline void set_m_vSparksMoveFrom_9(Vector3_t1986933152  value)
	{
		___m_vSparksMoveFrom_9 = value;
	}

	inline static int32_t get_offset_of_m_EndStatus_10() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_EndStatus_10)); }
	inline int32_t get_m_EndStatus_10() const { return ___m_EndStatus_10; }
	inline int32_t* get_address_of_m_EndStatus_10() { return &___m_EndStatus_10; }
	inline void set_m_EndStatus_10(int32_t value)
	{
		___m_EndStatus_10 = value;
	}

	inline static int32_t get_offset_of_m_fTime_11() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_fTime_11)); }
	inline float get_m_fTime_11() const { return ___m_fTime_11; }
	inline float* get_address_of_m_fTime_11() { return &___m_fTime_11; }
	inline void set_m_fTime_11(float value)
	{
		___m_fTime_11 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentQuestion_12() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_iCurrentQuestion_12)); }
	inline int32_t get_m_iCurrentQuestion_12() const { return ___m_iCurrentQuestion_12; }
	inline int32_t* get_address_of_m_iCurrentQuestion_12() { return &___m_iCurrentQuestion_12; }
	inline void set_m_iCurrentQuestion_12(int32_t value)
	{
		___m_iCurrentQuestion_12 = value;
	}

	inline static int32_t get_offset_of_m_iDisplayedQuestion_13() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_iDisplayedQuestion_13)); }
	inline int32_t get_m_iDisplayedQuestion_13() const { return ___m_iDisplayedQuestion_13; }
	inline int32_t* get_address_of_m_iDisplayedQuestion_13() { return &___m_iDisplayedQuestion_13; }
	inline void set_m_iDisplayedQuestion_13(int32_t value)
	{
		___m_iDisplayedQuestion_13 = value;
	}

	inline static int32_t get_offset_of_m_bInteracted_14() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bInteracted_14)); }
	inline bool get_m_bInteracted_14() const { return ___m_bInteracted_14; }
	inline bool* get_address_of_m_bInteracted_14() { return &___m_bInteracted_14; }
	inline void set_m_bInteracted_14(bool value)
	{
		___m_bInteracted_14 = value;
	}

	inline static int32_t get_offset_of_m_bMovingPlayer_15() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bMovingPlayer_15)); }
	inline bool get_m_bMovingPlayer_15() const { return ___m_bMovingPlayer_15; }
	inline bool* get_address_of_m_bMovingPlayer_15() { return &___m_bMovingPlayer_15; }
	inline void set_m_bMovingPlayer_15(bool value)
	{
		___m_bMovingPlayer_15 = value;
	}

	inline static int32_t get_offset_of_m_bTalkingToSpark_16() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bTalkingToSpark_16)); }
	inline bool get_m_bTalkingToSpark_16() const { return ___m_bTalkingToSpark_16; }
	inline bool* get_address_of_m_bTalkingToSpark_16() { return &___m_bTalkingToSpark_16; }
	inline void set_m_bTalkingToSpark_16(bool value)
	{
		___m_bTalkingToSpark_16 = value;
	}

	inline static int32_t get_offset_of_m_bGameActive_17() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bGameActive_17)); }
	inline bool get_m_bGameActive_17() const { return ___m_bGameActive_17; }
	inline bool* get_address_of_m_bGameActive_17() { return &___m_bGameActive_17; }
	inline void set_m_bGameActive_17(bool value)
	{
		___m_bGameActive_17 = value;
	}

	inline static int32_t get_offset_of_m_bDoneTalking_18() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bDoneTalking_18)); }
	inline bool get_m_bDoneTalking_18() const { return ___m_bDoneTalking_18; }
	inline bool* get_address_of_m_bDoneTalking_18() { return &___m_bDoneTalking_18; }
	inline void set_m_bDoneTalking_18(bool value)
	{
		___m_bDoneTalking_18 = value;
	}

	inline static int32_t get_offset_of_m_bEndScene_19() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bEndScene_19)); }
	inline bool get_m_bEndScene_19() const { return ___m_bEndScene_19; }
	inline bool* get_address_of_m_bEndScene_19() { return &___m_bEndScene_19; }
	inline void set_m_bEndScene_19(bool value)
	{
		___m_bEndScene_19 = value;
	}

	inline static int32_t get_offset_of_m_bSparkMovingToGem_20() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bSparkMovingToGem_20)); }
	inline bool get_m_bSparkMovingToGem_20() const { return ___m_bSparkMovingToGem_20; }
	inline bool* get_address_of_m_bSparkMovingToGem_20() { return &___m_bSparkMovingToGem_20; }
	inline void set_m_bSparkMovingToGem_20(bool value)
	{
		___m_bSparkMovingToGem_20 = value;
	}

	inline static int32_t get_offset_of_m_bTuiFlyingDown_21() { return static_cast<int32_t>(offsetof(L4StepsMiniGame_t1956426605, ___m_bTuiFlyingDown_21)); }
	inline bool get_m_bTuiFlyingDown_21() const { return ___m_bTuiFlyingDown_21; }
	inline bool* get_address_of_m_bTuiFlyingDown_21() { return &___m_bTuiFlyingDown_21; }
	inline void set_m_bTuiFlyingDown_21(bool value)
	{
		___m_bTuiFlyingDown_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4STEPSMINIGAME_T1956426605_H
#ifndef L4SPARK_T2135258226_H
#define L4SPARK_T2135258226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4Spark
struct  L4Spark_t2135258226  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L4Spark::m_bSparkInteracted
	bool ___m_bSparkInteracted_2;
	// System.Single L4Spark::m_bSparkInteractionRadius
	float ___m_bSparkInteractionRadius_3;
	// System.Boolean L4Spark::m_bSparkIsInteracting
	bool ___m_bSparkIsInteracting_4;
	// System.Boolean L4Spark::m_bGrabbed
	bool ___m_bGrabbed_5;
	// System.Boolean L4Spark::m_bCollected
	bool ___m_bCollected_6;
	// UnityEngine.Texture2D L4Spark::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_7;
	// UnityEngine.Texture2D L4Spark::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_8;
	// System.Single L4Spark::m_fTime
	float ___m_fTime_9;

public:
	inline static int32_t get_offset_of_m_bSparkInteracted_2() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_bSparkInteracted_2)); }
	inline bool get_m_bSparkInteracted_2() const { return ___m_bSparkInteracted_2; }
	inline bool* get_address_of_m_bSparkInteracted_2() { return &___m_bSparkInteracted_2; }
	inline void set_m_bSparkInteracted_2(bool value)
	{
		___m_bSparkInteracted_2 = value;
	}

	inline static int32_t get_offset_of_m_bSparkInteractionRadius_3() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_bSparkInteractionRadius_3)); }
	inline float get_m_bSparkInteractionRadius_3() const { return ___m_bSparkInteractionRadius_3; }
	inline float* get_address_of_m_bSparkInteractionRadius_3() { return &___m_bSparkInteractionRadius_3; }
	inline void set_m_bSparkInteractionRadius_3(float value)
	{
		___m_bSparkInteractionRadius_3 = value;
	}

	inline static int32_t get_offset_of_m_bSparkIsInteracting_4() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_bSparkIsInteracting_4)); }
	inline bool get_m_bSparkIsInteracting_4() const { return ___m_bSparkIsInteracting_4; }
	inline bool* get_address_of_m_bSparkIsInteracting_4() { return &___m_bSparkIsInteracting_4; }
	inline void set_m_bSparkIsInteracting_4(bool value)
	{
		___m_bSparkIsInteracting_4 = value;
	}

	inline static int32_t get_offset_of_m_bGrabbed_5() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_bGrabbed_5)); }
	inline bool get_m_bGrabbed_5() const { return ___m_bGrabbed_5; }
	inline bool* get_address_of_m_bGrabbed_5() { return &___m_bGrabbed_5; }
	inline void set_m_bGrabbed_5(bool value)
	{
		___m_bGrabbed_5 = value;
	}

	inline static int32_t get_offset_of_m_bCollected_6() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_bCollected_6)); }
	inline bool get_m_bCollected_6() const { return ___m_bCollected_6; }
	inline bool* get_address_of_m_bCollected_6() { return &___m_bCollected_6; }
	inline void set_m_bCollected_6(bool value)
	{
		___m_bCollected_6 = value;
	}

	inline static int32_t get_offset_of_MouseOverTexture_7() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___MouseOverTexture_7)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_7() const { return ___MouseOverTexture_7; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_7() { return &___MouseOverTexture_7; }
	inline void set_MouseOverTexture_7(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_7), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_8() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___MouseOriTexture_8)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_8() const { return ___MouseOriTexture_8; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_8() { return &___MouseOriTexture_8; }
	inline void set_MouseOriTexture_8(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_8), value);
	}

	inline static int32_t get_offset_of_m_fTime_9() { return static_cast<int32_t>(offsetof(L4Spark_t2135258226, ___m_fTime_9)); }
	inline float get_m_fTime_9() const { return ___m_fTime_9; }
	inline float* get_address_of_m_fTime_9() { return &___m_fTime_9; }
	inline void set_m_fTime_9(float value)
	{
		___m_fTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4SPARK_T2135258226_H
#ifndef L4LADDERHELPTRIGERRIGHT_T4238512495_H
#define L4LADDERHELPTRIGERRIGHT_T4238512495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4LadderHelpTrigerRight
struct  L4LadderHelpTrigerRight_t4238512495  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L4LadderHelpTrigerRight::m_rightLadderTriggerEntered
	bool ___m_rightLadderTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_rightLadderTriggerEntered_2() { return static_cast<int32_t>(offsetof(L4LadderHelpTrigerRight_t4238512495, ___m_rightLadderTriggerEntered_2)); }
	inline bool get_m_rightLadderTriggerEntered_2() const { return ___m_rightLadderTriggerEntered_2; }
	inline bool* get_address_of_m_rightLadderTriggerEntered_2() { return &___m_rightLadderTriggerEntered_2; }
	inline void set_m_rightLadderTriggerEntered_2(bool value)
	{
		___m_rightLadderTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4LADDERHELPTRIGERRIGHT_T4238512495_H
#ifndef L4LADDERHELPTRIGERLEFT_T104804450_H
#define L4LADDERHELPTRIGERLEFT_T104804450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4LadderHelpTrigerLeft
struct  L4LadderHelpTrigerLeft_t104804450  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L4LadderHelpTrigerLeft::m_leftLadderTriggerEntered
	bool ___m_leftLadderTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_leftLadderTriggerEntered_2() { return static_cast<int32_t>(offsetof(L4LadderHelpTrigerLeft_t104804450, ___m_leftLadderTriggerEntered_2)); }
	inline bool get_m_leftLadderTriggerEntered_2() const { return ___m_leftLadderTriggerEntered_2; }
	inline bool* get_address_of_m_leftLadderTriggerEntered_2() { return &___m_leftLadderTriggerEntered_2; }
	inline void set_m_leftLadderTriggerEntered_2(bool value)
	{
		___m_leftLadderTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4LADDERHELPTRIGERLEFT_T104804450_H
#ifndef L4LADDERHELP_T112018707_H
#define L4LADDERHELP_T112018707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4LadderHelp
struct  L4LadderHelp_t112018707  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L4LadderHelp::oLadderLeft
	GameObject_t2557347079 * ___oLadderLeft_2;
	// UnityEngine.GameObject L4LadderHelp::oLadderRight
	GameObject_t2557347079 * ___oLadderRight_3;
	// System.Boolean L4LadderHelp::bActivated
	bool ___bActivated_4;
	// UnityEngine.GUISkin L4LadderHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_5;
	// System.String L4LadderHelp::Text
	String_t* ___Text_6;

public:
	inline static int32_t get_offset_of_oLadderLeft_2() { return static_cast<int32_t>(offsetof(L4LadderHelp_t112018707, ___oLadderLeft_2)); }
	inline GameObject_t2557347079 * get_oLadderLeft_2() const { return ___oLadderLeft_2; }
	inline GameObject_t2557347079 ** get_address_of_oLadderLeft_2() { return &___oLadderLeft_2; }
	inline void set_oLadderLeft_2(GameObject_t2557347079 * value)
	{
		___oLadderLeft_2 = value;
		Il2CppCodeGenWriteBarrier((&___oLadderLeft_2), value);
	}

	inline static int32_t get_offset_of_oLadderRight_3() { return static_cast<int32_t>(offsetof(L4LadderHelp_t112018707, ___oLadderRight_3)); }
	inline GameObject_t2557347079 * get_oLadderRight_3() const { return ___oLadderRight_3; }
	inline GameObject_t2557347079 ** get_address_of_oLadderRight_3() { return &___oLadderRight_3; }
	inline void set_oLadderRight_3(GameObject_t2557347079 * value)
	{
		___oLadderRight_3 = value;
		Il2CppCodeGenWriteBarrier((&___oLadderRight_3), value);
	}

	inline static int32_t get_offset_of_bActivated_4() { return static_cast<int32_t>(offsetof(L4LadderHelp_t112018707, ___bActivated_4)); }
	inline bool get_bActivated_4() const { return ___bActivated_4; }
	inline bool* get_address_of_bActivated_4() { return &___bActivated_4; }
	inline void set_bActivated_4(bool value)
	{
		___bActivated_4 = value;
	}

	inline static int32_t get_offset_of_GUIskin_5() { return static_cast<int32_t>(offsetof(L4LadderHelp_t112018707, ___GUIskin_5)); }
	inline GUISkin_t2122630221 * get_GUIskin_5() const { return ___GUIskin_5; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_5() { return &___GUIskin_5; }
	inline void set_GUIskin_5(GUISkin_t2122630221 * value)
	{
		___GUIskin_5 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_5), value);
	}

	inline static int32_t get_offset_of_Text_6() { return static_cast<int32_t>(offsetof(L4LadderHelp_t112018707, ___Text_6)); }
	inline String_t* get_Text_6() const { return ___Text_6; }
	inline String_t** get_address_of_Text_6() { return &___Text_6; }
	inline void set_Text_6(String_t* value)
	{
		___Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___Text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4LADDERHELP_T112018707_H
#ifndef L4GNATSTALK_T2079079531_H
#define L4GNATSTALK_T2079079531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4GnatsTalk
struct  L4GnatsTalk_t2079079531  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] L4GnatsTalk::m_Gnats
	GameObjectU5BU5D_t2988620542* ___m_Gnats_2;
	// UnityEngine.GameObject L4GnatsTalk::m_Spark
	GameObject_t2557347079 * ___m_Spark_3;
	// UnityEngine.GameObject L4GnatsTalk::m_Effect
	GameObject_t2557347079 * ___m_Effect_4;
	// UnityEngine.Transform L4GnatsTalk::m_FlyAwayPos
	Transform_t362059596 * ___m_FlyAwayPos_5;
	// UnityEngine.Transform L4GnatsTalk::m_QuestionPos
	Transform_t362059596 * ___m_QuestionPos_6;
	// System.String L4GnatsTalk::CameraID
	String_t* ___CameraID_7;
	// UnityEngine.GameObject L4GnatsTalk::m_CurMover
	GameObject_t2557347079 * ___m_CurMover_8;
	// UnityEngine.GameObject L4GnatsTalk::m_TempSpark
	GameObject_t2557347079 * ___m_TempSpark_9;
	// GnatsTalkStatus L4GnatsTalk::m_status
	int32_t ___m_status_10;
	// UnityEngine.Vector3 L4GnatsTalk::m_vStartPos
	Vector3_t1986933152  ___m_vStartPos_11;
	// System.Single L4GnatsTalk::m_fMoveTime
	float ___m_fMoveTime_12;
	// System.Single L4GnatsTalk::m_fStatusTime
	float ___m_fStatusTime_13;
	// System.Single L4GnatsTalk::m_fTalkEnd
	float ___m_fTalkEnd_14;
	// System.Int32 L4GnatsTalk::m_iCurrentGnat
	int32_t ___m_iCurrentGnat_15;
	// System.Boolean L4GnatsTalk::m_bTriggered
	bool ___m_bTriggered_16;

public:
	inline static int32_t get_offset_of_m_Gnats_2() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_Gnats_2)); }
	inline GameObjectU5BU5D_t2988620542* get_m_Gnats_2() const { return ___m_Gnats_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_m_Gnats_2() { return &___m_Gnats_2; }
	inline void set_m_Gnats_2(GameObjectU5BU5D_t2988620542* value)
	{
		___m_Gnats_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Gnats_2), value);
	}

	inline static int32_t get_offset_of_m_Spark_3() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_Spark_3)); }
	inline GameObject_t2557347079 * get_m_Spark_3() const { return ___m_Spark_3; }
	inline GameObject_t2557347079 ** get_address_of_m_Spark_3() { return &___m_Spark_3; }
	inline void set_m_Spark_3(GameObject_t2557347079 * value)
	{
		___m_Spark_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Spark_3), value);
	}

	inline static int32_t get_offset_of_m_Effect_4() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_Effect_4)); }
	inline GameObject_t2557347079 * get_m_Effect_4() const { return ___m_Effect_4; }
	inline GameObject_t2557347079 ** get_address_of_m_Effect_4() { return &___m_Effect_4; }
	inline void set_m_Effect_4(GameObject_t2557347079 * value)
	{
		___m_Effect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Effect_4), value);
	}

	inline static int32_t get_offset_of_m_FlyAwayPos_5() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_FlyAwayPos_5)); }
	inline Transform_t362059596 * get_m_FlyAwayPos_5() const { return ___m_FlyAwayPos_5; }
	inline Transform_t362059596 ** get_address_of_m_FlyAwayPos_5() { return &___m_FlyAwayPos_5; }
	inline void set_m_FlyAwayPos_5(Transform_t362059596 * value)
	{
		___m_FlyAwayPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FlyAwayPos_5), value);
	}

	inline static int32_t get_offset_of_m_QuestionPos_6() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_QuestionPos_6)); }
	inline Transform_t362059596 * get_m_QuestionPos_6() const { return ___m_QuestionPos_6; }
	inline Transform_t362059596 ** get_address_of_m_QuestionPos_6() { return &___m_QuestionPos_6; }
	inline void set_m_QuestionPos_6(Transform_t362059596 * value)
	{
		___m_QuestionPos_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_QuestionPos_6), value);
	}

	inline static int32_t get_offset_of_CameraID_7() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___CameraID_7)); }
	inline String_t* get_CameraID_7() const { return ___CameraID_7; }
	inline String_t** get_address_of_CameraID_7() { return &___CameraID_7; }
	inline void set_CameraID_7(String_t* value)
	{
		___CameraID_7 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_7), value);
	}

	inline static int32_t get_offset_of_m_CurMover_8() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_CurMover_8)); }
	inline GameObject_t2557347079 * get_m_CurMover_8() const { return ___m_CurMover_8; }
	inline GameObject_t2557347079 ** get_address_of_m_CurMover_8() { return &___m_CurMover_8; }
	inline void set_m_CurMover_8(GameObject_t2557347079 * value)
	{
		___m_CurMover_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurMover_8), value);
	}

	inline static int32_t get_offset_of_m_TempSpark_9() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_TempSpark_9)); }
	inline GameObject_t2557347079 * get_m_TempSpark_9() const { return ___m_TempSpark_9; }
	inline GameObject_t2557347079 ** get_address_of_m_TempSpark_9() { return &___m_TempSpark_9; }
	inline void set_m_TempSpark_9(GameObject_t2557347079 * value)
	{
		___m_TempSpark_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempSpark_9), value);
	}

	inline static int32_t get_offset_of_m_status_10() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_status_10)); }
	inline int32_t get_m_status_10() const { return ___m_status_10; }
	inline int32_t* get_address_of_m_status_10() { return &___m_status_10; }
	inline void set_m_status_10(int32_t value)
	{
		___m_status_10 = value;
	}

	inline static int32_t get_offset_of_m_vStartPos_11() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_vStartPos_11)); }
	inline Vector3_t1986933152  get_m_vStartPos_11() const { return ___m_vStartPos_11; }
	inline Vector3_t1986933152 * get_address_of_m_vStartPos_11() { return &___m_vStartPos_11; }
	inline void set_m_vStartPos_11(Vector3_t1986933152  value)
	{
		___m_vStartPos_11 = value;
	}

	inline static int32_t get_offset_of_m_fMoveTime_12() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_fMoveTime_12)); }
	inline float get_m_fMoveTime_12() const { return ___m_fMoveTime_12; }
	inline float* get_address_of_m_fMoveTime_12() { return &___m_fMoveTime_12; }
	inline void set_m_fMoveTime_12(float value)
	{
		___m_fMoveTime_12 = value;
	}

	inline static int32_t get_offset_of_m_fStatusTime_13() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_fStatusTime_13)); }
	inline float get_m_fStatusTime_13() const { return ___m_fStatusTime_13; }
	inline float* get_address_of_m_fStatusTime_13() { return &___m_fStatusTime_13; }
	inline void set_m_fStatusTime_13(float value)
	{
		___m_fStatusTime_13 = value;
	}

	inline static int32_t get_offset_of_m_fTalkEnd_14() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_fTalkEnd_14)); }
	inline float get_m_fTalkEnd_14() const { return ___m_fTalkEnd_14; }
	inline float* get_address_of_m_fTalkEnd_14() { return &___m_fTalkEnd_14; }
	inline void set_m_fTalkEnd_14(float value)
	{
		___m_fTalkEnd_14 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentGnat_15() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_iCurrentGnat_15)); }
	inline int32_t get_m_iCurrentGnat_15() const { return ___m_iCurrentGnat_15; }
	inline int32_t* get_address_of_m_iCurrentGnat_15() { return &___m_iCurrentGnat_15; }
	inline void set_m_iCurrentGnat_15(int32_t value)
	{
		___m_iCurrentGnat_15 = value;
	}

	inline static int32_t get_offset_of_m_bTriggered_16() { return static_cast<int32_t>(offsetof(L4GnatsTalk_t2079079531, ___m_bTriggered_16)); }
	inline bool get_m_bTriggered_16() const { return ___m_bTriggered_16; }
	inline bool* get_address_of_m_bTriggered_16() { return &___m_bTriggered_16; }
	inline void set_m_bTriggered_16(bool value)
	{
		___m_bTriggered_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4GNATSTALK_T2079079531_H
#ifndef L3TRIGAMEHANDLER_T1673593620_H
#define L3TRIGAMEHANDLER_T1673593620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3TriGameHandler
struct  L3TriGameHandler_t1673593620  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject[] L3TriGameHandler::m_Games
	GameObjectU5BU5D_t2988620542* ___m_Games_2;
	// UnityEngine.GameObject[] L3TriGameHandler::m_Birds
	GameObjectU5BU5D_t2988620542* ___m_Birds_3;
	// UnityEngine.Transform[] L3TriGameHandler::m_TriggerPos
	TransformU5BU5D_t2383644165* ___m_TriggerPos_4;
	// UnityEngine.Transform[] L3TriGameHandler::m_MoveToPos
	TransformU5BU5D_t2383644165* ___m_MoveToPos_5;
	// UnityEngine.Transform[] L3TriGameHandler::m_LookAts
	TransformU5BU5D_t2383644165* ___m_LookAts_6;
	// System.Single L3TriGameHandler::m_fGameStartDistance
	float ___m_fGameStartDistance_7;
	// UnityEngine.GameObject L3TriGameHandler::m_player
	GameObject_t2557347079 * ___m_player_8;
	// UnityEngine.Vector3 L3TriGameHandler::startPos
	Vector3_t1986933152  ___startPos_9;
	// System.Int32 L3TriGameHandler::m_iCurrentGame
	int32_t ___m_iCurrentGame_10;
	// System.Boolean[] L3TriGameHandler::m_bIsGameDone
	BooleanU5BU5D_t698278498* ___m_bIsGameDone_11;
	// System.Boolean L3TriGameHandler::m_bMovingTo
	bool ___m_bMovingTo_12;
	// System.Boolean L3TriGameHandler::m_bGameDone
	bool ___m_bGameDone_13;
	// System.Boolean L3TriGameHandler::m_bWaiting
	bool ___m_bWaiting_14;

public:
	inline static int32_t get_offset_of_m_Games_2() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_Games_2)); }
	inline GameObjectU5BU5D_t2988620542* get_m_Games_2() const { return ___m_Games_2; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_m_Games_2() { return &___m_Games_2; }
	inline void set_m_Games_2(GameObjectU5BU5D_t2988620542* value)
	{
		___m_Games_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Games_2), value);
	}

	inline static int32_t get_offset_of_m_Birds_3() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_Birds_3)); }
	inline GameObjectU5BU5D_t2988620542* get_m_Birds_3() const { return ___m_Birds_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_m_Birds_3() { return &___m_Birds_3; }
	inline void set_m_Birds_3(GameObjectU5BU5D_t2988620542* value)
	{
		___m_Birds_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Birds_3), value);
	}

	inline static int32_t get_offset_of_m_TriggerPos_4() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_TriggerPos_4)); }
	inline TransformU5BU5D_t2383644165* get_m_TriggerPos_4() const { return ___m_TriggerPos_4; }
	inline TransformU5BU5D_t2383644165** get_address_of_m_TriggerPos_4() { return &___m_TriggerPos_4; }
	inline void set_m_TriggerPos_4(TransformU5BU5D_t2383644165* value)
	{
		___m_TriggerPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerPos_4), value);
	}

	inline static int32_t get_offset_of_m_MoveToPos_5() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_MoveToPos_5)); }
	inline TransformU5BU5D_t2383644165* get_m_MoveToPos_5() const { return ___m_MoveToPos_5; }
	inline TransformU5BU5D_t2383644165** get_address_of_m_MoveToPos_5() { return &___m_MoveToPos_5; }
	inline void set_m_MoveToPos_5(TransformU5BU5D_t2383644165* value)
	{
		___m_MoveToPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoveToPos_5), value);
	}

	inline static int32_t get_offset_of_m_LookAts_6() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_LookAts_6)); }
	inline TransformU5BU5D_t2383644165* get_m_LookAts_6() const { return ___m_LookAts_6; }
	inline TransformU5BU5D_t2383644165** get_address_of_m_LookAts_6() { return &___m_LookAts_6; }
	inline void set_m_LookAts_6(TransformU5BU5D_t2383644165* value)
	{
		___m_LookAts_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAts_6), value);
	}

	inline static int32_t get_offset_of_m_fGameStartDistance_7() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_fGameStartDistance_7)); }
	inline float get_m_fGameStartDistance_7() const { return ___m_fGameStartDistance_7; }
	inline float* get_address_of_m_fGameStartDistance_7() { return &___m_fGameStartDistance_7; }
	inline void set_m_fGameStartDistance_7(float value)
	{
		___m_fGameStartDistance_7 = value;
	}

	inline static int32_t get_offset_of_m_player_8() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_player_8)); }
	inline GameObject_t2557347079 * get_m_player_8() const { return ___m_player_8; }
	inline GameObject_t2557347079 ** get_address_of_m_player_8() { return &___m_player_8; }
	inline void set_m_player_8(GameObject_t2557347079 * value)
	{
		___m_player_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_player_8), value);
	}

	inline static int32_t get_offset_of_startPos_9() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___startPos_9)); }
	inline Vector3_t1986933152  get_startPos_9() const { return ___startPos_9; }
	inline Vector3_t1986933152 * get_address_of_startPos_9() { return &___startPos_9; }
	inline void set_startPos_9(Vector3_t1986933152  value)
	{
		___startPos_9 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentGame_10() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_iCurrentGame_10)); }
	inline int32_t get_m_iCurrentGame_10() const { return ___m_iCurrentGame_10; }
	inline int32_t* get_address_of_m_iCurrentGame_10() { return &___m_iCurrentGame_10; }
	inline void set_m_iCurrentGame_10(int32_t value)
	{
		___m_iCurrentGame_10 = value;
	}

	inline static int32_t get_offset_of_m_bIsGameDone_11() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_bIsGameDone_11)); }
	inline BooleanU5BU5D_t698278498* get_m_bIsGameDone_11() const { return ___m_bIsGameDone_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_m_bIsGameDone_11() { return &___m_bIsGameDone_11; }
	inline void set_m_bIsGameDone_11(BooleanU5BU5D_t698278498* value)
	{
		___m_bIsGameDone_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_bIsGameDone_11), value);
	}

	inline static int32_t get_offset_of_m_bMovingTo_12() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_bMovingTo_12)); }
	inline bool get_m_bMovingTo_12() const { return ___m_bMovingTo_12; }
	inline bool* get_address_of_m_bMovingTo_12() { return &___m_bMovingTo_12; }
	inline void set_m_bMovingTo_12(bool value)
	{
		___m_bMovingTo_12 = value;
	}

	inline static int32_t get_offset_of_m_bGameDone_13() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_bGameDone_13)); }
	inline bool get_m_bGameDone_13() const { return ___m_bGameDone_13; }
	inline bool* get_address_of_m_bGameDone_13() { return &___m_bGameDone_13; }
	inline void set_m_bGameDone_13(bool value)
	{
		___m_bGameDone_13 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting_14() { return static_cast<int32_t>(offsetof(L3TriGameHandler_t1673593620, ___m_bWaiting_14)); }
	inline bool get_m_bWaiting_14() const { return ___m_bWaiting_14; }
	inline bool* get_address_of_m_bWaiting_14() { return &___m_bWaiting_14; }
	inline void set_m_bWaiting_14(bool value)
	{
		___m_bWaiting_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3TRIGAMEHANDLER_T1673593620_H
#ifndef LEVELSIXINBUILDINGMINIGAME_T3344869765_H
#define LEVELSIXINBUILDINGMINIGAME_T3344869765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSixInBuildingMiniGame
struct  LevelSixInBuildingMiniGame_t3344869765  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LevelSixInBuildingMiniGame::m_bMoveToPos
	bool ___m_bMoveToPos_2;
	// System.Boolean LevelSixInBuildingMiniGame::m_bStart
	bool ___m_bStart_3;
	// GnatStatus LevelSixInBuildingMiniGame::CurrentStatus
	int32_t ___CurrentStatus_4;
	// UnityEngine.GameObject[] LevelSixInBuildingMiniGame::Gnats
	GameObjectU5BU5D_t2988620542* ___Gnats_5;
	// UnityEngine.Vector3[] LevelSixInBuildingMiniGame::BasePos
	Vector3U5BU5D_t32224225* ___BasePos_6;
	// System.Single LevelSixInBuildingMiniGame::DefeatedFlyUp
	float ___DefeatedFlyUp_7;
	// System.Int32 LevelSixInBuildingMiniGame::iCurrentGnat
	int32_t ___iCurrentGnat_8;

public:
	inline static int32_t get_offset_of_m_bMoveToPos_2() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___m_bMoveToPos_2)); }
	inline bool get_m_bMoveToPos_2() const { return ___m_bMoveToPos_2; }
	inline bool* get_address_of_m_bMoveToPos_2() { return &___m_bMoveToPos_2; }
	inline void set_m_bMoveToPos_2(bool value)
	{
		___m_bMoveToPos_2 = value;
	}

	inline static int32_t get_offset_of_m_bStart_3() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___m_bStart_3)); }
	inline bool get_m_bStart_3() const { return ___m_bStart_3; }
	inline bool* get_address_of_m_bStart_3() { return &___m_bStart_3; }
	inline void set_m_bStart_3(bool value)
	{
		___m_bStart_3 = value;
	}

	inline static int32_t get_offset_of_CurrentStatus_4() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___CurrentStatus_4)); }
	inline int32_t get_CurrentStatus_4() const { return ___CurrentStatus_4; }
	inline int32_t* get_address_of_CurrentStatus_4() { return &___CurrentStatus_4; }
	inline void set_CurrentStatus_4(int32_t value)
	{
		___CurrentStatus_4 = value;
	}

	inline static int32_t get_offset_of_Gnats_5() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___Gnats_5)); }
	inline GameObjectU5BU5D_t2988620542* get_Gnats_5() const { return ___Gnats_5; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_Gnats_5() { return &___Gnats_5; }
	inline void set_Gnats_5(GameObjectU5BU5D_t2988620542* value)
	{
		___Gnats_5 = value;
		Il2CppCodeGenWriteBarrier((&___Gnats_5), value);
	}

	inline static int32_t get_offset_of_BasePos_6() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___BasePos_6)); }
	inline Vector3U5BU5D_t32224225* get_BasePos_6() const { return ___BasePos_6; }
	inline Vector3U5BU5D_t32224225** get_address_of_BasePos_6() { return &___BasePos_6; }
	inline void set_BasePos_6(Vector3U5BU5D_t32224225* value)
	{
		___BasePos_6 = value;
		Il2CppCodeGenWriteBarrier((&___BasePos_6), value);
	}

	inline static int32_t get_offset_of_DefeatedFlyUp_7() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___DefeatedFlyUp_7)); }
	inline float get_DefeatedFlyUp_7() const { return ___DefeatedFlyUp_7; }
	inline float* get_address_of_DefeatedFlyUp_7() { return &___DefeatedFlyUp_7; }
	inline void set_DefeatedFlyUp_7(float value)
	{
		___DefeatedFlyUp_7 = value;
	}

	inline static int32_t get_offset_of_iCurrentGnat_8() { return static_cast<int32_t>(offsetof(LevelSixInBuildingMiniGame_t3344869765, ___iCurrentGnat_8)); }
	inline int32_t get_iCurrentGnat_8() const { return ___iCurrentGnat_8; }
	inline int32_t* get_address_of_iCurrentGnat_8() { return &___iCurrentGnat_8; }
	inline void set_iCurrentGnat_8(int32_t value)
	{
		___iCurrentGnat_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSIXINBUILDINGMINIGAME_T3344869765_H
#ifndef GIRLCUSTOMISATION_T851347520_H
#define GIRLCUSTOMISATION_T851347520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GirlCustomisation
struct  GirlCustomisation_t851347520  : public Customisation_t1896805424
{
public:
	// UnityEngine.GUISkin GirlCustomisation::m_skin
	GUISkin_t2122630221 * ___m_skin_29;
	// UnityEngine.GameObject GirlCustomisation::m_girlCharacterPrf
	GameObject_t2557347079 * ___m_girlCharacterPrf_30;
	// UnityEngine.Vector3 GirlCustomisation::m_vGirlsCustomisePosition
	Vector3_t1986933152  ___m_vGirlsCustomisePosition_31;
	// System.Single GirlCustomisation::m_rotationAngle
	float ___m_rotationAngle_32;

public:
	inline static int32_t get_offset_of_m_skin_29() { return static_cast<int32_t>(offsetof(GirlCustomisation_t851347520, ___m_skin_29)); }
	inline GUISkin_t2122630221 * get_m_skin_29() const { return ___m_skin_29; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_29() { return &___m_skin_29; }
	inline void set_m_skin_29(GUISkin_t2122630221 * value)
	{
		___m_skin_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_29), value);
	}

	inline static int32_t get_offset_of_m_girlCharacterPrf_30() { return static_cast<int32_t>(offsetof(GirlCustomisation_t851347520, ___m_girlCharacterPrf_30)); }
	inline GameObject_t2557347079 * get_m_girlCharacterPrf_30() const { return ___m_girlCharacterPrf_30; }
	inline GameObject_t2557347079 ** get_address_of_m_girlCharacterPrf_30() { return &___m_girlCharacterPrf_30; }
	inline void set_m_girlCharacterPrf_30(GameObject_t2557347079 * value)
	{
		___m_girlCharacterPrf_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_girlCharacterPrf_30), value);
	}

	inline static int32_t get_offset_of_m_vGirlsCustomisePosition_31() { return static_cast<int32_t>(offsetof(GirlCustomisation_t851347520, ___m_vGirlsCustomisePosition_31)); }
	inline Vector3_t1986933152  get_m_vGirlsCustomisePosition_31() const { return ___m_vGirlsCustomisePosition_31; }
	inline Vector3_t1986933152 * get_address_of_m_vGirlsCustomisePosition_31() { return &___m_vGirlsCustomisePosition_31; }
	inline void set_m_vGirlsCustomisePosition_31(Vector3_t1986933152  value)
	{
		___m_vGirlsCustomisePosition_31 = value;
	}

	inline static int32_t get_offset_of_m_rotationAngle_32() { return static_cast<int32_t>(offsetof(GirlCustomisation_t851347520, ___m_rotationAngle_32)); }
	inline float get_m_rotationAngle_32() const { return ___m_rotationAngle_32; }
	inline float* get_address_of_m_rotationAngle_32() { return &___m_rotationAngle_32; }
	inline void set_m_rotationAngle_32(float value)
	{
		___m_rotationAngle_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIRLCUSTOMISATION_T851347520_H
#ifndef BOYCUSTOMISATION_T2828205644_H
#define BOYCUSTOMISATION_T2828205644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoyCustomisation
struct  BoyCustomisation_t2828205644  : public Customisation_t1896805424
{
public:
	// UnityEngine.GUISkin BoyCustomisation::m_skin
	GUISkin_t2122630221 * ___m_skin_29;
	// UnityEngine.GameObject BoyCustomisation::m_boyCharacterPrf
	GameObject_t2557347079 * ___m_boyCharacterPrf_30;
	// UnityEngine.Vector3 BoyCustomisation::m_vBoysCustomisePosition
	Vector3_t1986933152  ___m_vBoysCustomisePosition_31;
	// System.Single BoyCustomisation::m_rotationAngle
	float ___m_rotationAngle_32;

public:
	inline static int32_t get_offset_of_m_skin_29() { return static_cast<int32_t>(offsetof(BoyCustomisation_t2828205644, ___m_skin_29)); }
	inline GUISkin_t2122630221 * get_m_skin_29() const { return ___m_skin_29; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_29() { return &___m_skin_29; }
	inline void set_m_skin_29(GUISkin_t2122630221 * value)
	{
		___m_skin_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_29), value);
	}

	inline static int32_t get_offset_of_m_boyCharacterPrf_30() { return static_cast<int32_t>(offsetof(BoyCustomisation_t2828205644, ___m_boyCharacterPrf_30)); }
	inline GameObject_t2557347079 * get_m_boyCharacterPrf_30() const { return ___m_boyCharacterPrf_30; }
	inline GameObject_t2557347079 ** get_address_of_m_boyCharacterPrf_30() { return &___m_boyCharacterPrf_30; }
	inline void set_m_boyCharacterPrf_30(GameObject_t2557347079 * value)
	{
		___m_boyCharacterPrf_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_boyCharacterPrf_30), value);
	}

	inline static int32_t get_offset_of_m_vBoysCustomisePosition_31() { return static_cast<int32_t>(offsetof(BoyCustomisation_t2828205644, ___m_vBoysCustomisePosition_31)); }
	inline Vector3_t1986933152  get_m_vBoysCustomisePosition_31() const { return ___m_vBoysCustomisePosition_31; }
	inline Vector3_t1986933152 * get_address_of_m_vBoysCustomisePosition_31() { return &___m_vBoysCustomisePosition_31; }
	inline void set_m_vBoysCustomisePosition_31(Vector3_t1986933152  value)
	{
		___m_vBoysCustomisePosition_31 = value;
	}

	inline static int32_t get_offset_of_m_rotationAngle_32() { return static_cast<int32_t>(offsetof(BoyCustomisation_t2828205644, ___m_rotationAngle_32)); }
	inline float get_m_rotationAngle_32() const { return ___m_rotationAngle_32; }
	inline float* get_address_of_m_rotationAngle_32() { return &___m_rotationAngle_32; }
	inline void set_m_rotationAngle_32(float value)
	{
		___m_rotationAngle_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOYCUSTOMISATION_T2828205644_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (FireFountainRoutine_t2875428386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[5] = 
{
	FireFountainRoutine_t2875428386::get_offset_of_Fountains_2(),
	FireFountainRoutine_t2875428386::get_offset_of_OnTime_3(),
	FireFountainRoutine_t2875428386::get_offset_of_OffTime_4(),
	FireFountainRoutine_t2875428386::get_offset_of_fMin_5(),
	FireFountainRoutine_t2875428386::get_offset_of_fMax_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (LevelFiveScript_t2930300399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[1] = 
{
	LevelFiveScript_t2930300399::get_offset_of_bStartDoorOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (LevelFourScript_t3944070347), -1, sizeof(LevelFourScript_t3944070347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3002[3] = 
{
	LevelFourScript_t3944070347::get_offset_of_m_bGnatsDefeated_2(),
	LevelFourScript_t3944070347_StaticFields::get_offset_of_PlayerHeight_3(),
	LevelFourScript_t3944070347_StaticFields::get_offset_of_m_fClimbSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (LevelOneStatus_t1390847730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3003[6] = 
{
	LevelOneStatus_t1390847730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (LevelOneScript_t2347138676), -1, sizeof(LevelOneScript_t2347138676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	LevelOneScript_t2347138676_StaticFields::get_offset_of_instanceRef_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (LevelSevenScript_t1159386256), -1, sizeof(LevelSevenScript_t1159386256_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3005[19] = 
{
	LevelSevenScript_t1159386256::get_offset_of_DoorOne_2(),
	LevelSevenScript_t1159386256::get_offset_of_DoorTwo_3(),
	LevelSevenScript_t1159386256::get_offset_of_NPCs_4(),
	LevelSevenScript_t1159386256::get_offset_of_m_bCassFirstTalkFinished_5(),
	LevelSevenScript_t1159386256::get_offset_of_m_bPickSparkStarted_6(),
	LevelSevenScript_t1159386256_StaticFields::get_offset_of_iSparxCaught_7(),
	LevelSevenScript_t1159386256::get_offset_of_StartGetHelpTimer_8(),
	LevelSevenScript_t1159386256::get_offset_of_fTimeForTen_9(),
	LevelSevenScript_t1159386256::get_offset_of_bPlayedBreath_10(),
	LevelSevenScript_t1159386256::get_offset_of_m_bOpenFirstDoor_11(),
	LevelSevenScript_t1159386256::get_offset_of_FinishedOpenningDoorOne_12(),
	LevelSevenScript_t1159386256::get_offset_of_StartMiniGame_13(),
	LevelSevenScript_t1159386256::get_offset_of_StartHelpConversation_14(),
	LevelSevenScript_t1159386256::get_offset_of_Barrel_15(),
	LevelSevenScript_t1159386256::get_offset_of_m_bRotatePlayerOnce_16(),
	LevelSevenScript_t1159386256::get_offset_of_m_bPlayerAcceptHelp_17(),
	LevelSevenScript_t1159386256::get_offset_of_m_bPlayerWayToMiniGame_18(),
	LevelSevenScript_t1159386256::get_offset_of_m_bMoveTotargetCallOnce_19(),
	LevelSevenScript_t1159386256::get_offset_of_bForceMovementAfterHelpGame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (Lv6OpenChestCutScene_t1631773017)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3006[8] = 
{
	Lv6OpenChestCutScene_t1631773017::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (LevelSixScript_t1926494436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[14] = 
{
	LevelSixScript_t1926494436::get_offset_of_cutscene_2(),
	LevelSixScript_t1926494436::get_offset_of_Trigger_3(),
	LevelSixScript_t1926494436::get_offset_of_DoneWithGuard_4(),
	LevelSixScript_t1926494436::get_offset_of_m_bTriggerFirstDialog_5(),
	LevelSixScript_t1926494436::get_offset_of_NPC1_6(),
	LevelSixScript_t1926494436::get_offset_of_MiniGames_7(),
	LevelSixScript_t1926494436::get_offset_of_Doors_8(),
	LevelSixScript_t1926494436::get_offset_of_iCurrentMiniGameIndex_9(),
	LevelSixScript_t1926494436::get_offset_of_MiniGameTriggers_10(),
	LevelSixScript_t1926494436::get_offset_of_bCheckForPlayingGame_11(),
	LevelSixScript_t1926494436::get_offset_of_MiniGame_12(),
	LevelSixScript_t1926494436::get_offset_of_Eagle_13(),
	LevelSixScript_t1926494436::get_offset_of_Gem_14(),
	LevelSixScript_t1926494436::get_offset_of_m_bPlayerInfrontOfChest_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (EagleReturnToLvS_t188099863)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3008[4] = 
{
	EagleReturnToLvS_t188099863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (LevelSScript_t2908075387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[12] = 
{
	LevelSScript_t2908075387::get_offset_of_m_bStartSetupComplete_2(),
	LevelSScript_t2908075387::get_offset_of_status_3(),
	LevelSScript_t2908075387::get_offset_of_SeatPosition_4(),
	LevelSScript_t2908075387::get_offset_of_StationaryPosition_5(),
	LevelSScript_t2908075387::get_offset_of_FlyFrom_6(),
	LevelSScript_t2908075387::get_offset_of_PlayerPos_7(),
	LevelSScript_t2908075387::get_offset_of_Eagle_8(),
	LevelSScript_t2908075387::get_offset_of_start_9(),
	LevelSScript_t2908075387::get_offset_of_PauseTimer_10(),
	LevelSScript_t2908075387::get_offset_of_m_fTime_11(),
	LevelSScript_t2908075387::get_offset_of_m_gNPCsOnLevels_12(),
	LevelSScript_t2908075387::get_offset_of_m_tTrimTexture_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (LevelThreeScript_t992746369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[2] = 
{
	LevelThreeScript_t992746369::get_offset_of_Triggers_2(),
	LevelThreeScript_t992746369::get_offset_of_MiniGames_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (LevelTwoScript_t1243006306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[14] = 
{
	LevelTwoScript_t1243006306::get_offset_of_NumOfLightsMeltIce_2(),
	LevelTwoScript_t1243006306::get_offset_of_CurrentNumMeltedIce_3(),
	LevelTwoScript_t1243006306::get_offset_of_IceMeltCutSceneCamID_4(),
	LevelTwoScript_t1243006306::get_offset_of_m_bPlayCutScene_5(),
	LevelTwoScript_t1243006306::get_offset_of_CurrentIceMeltIndex_6(),
	LevelTwoScript_t1243006306::get_offset_of_IceBelowMeltPosY_7(),
	LevelTwoScript_t1243006306::get_offset_of_IceAboveMeltPosY_8(),
	LevelTwoScript_t1243006306::get_offset_of_TopIce_9(),
	LevelTwoScript_t1243006306::get_offset_of_BottomIce_10(),
	LevelTwoScript_t1243006306::get_offset_of_m_hopeTalkFinished_11(),
	LevelTwoScript_t1243006306::get_offset_of_Tui_12(),
	LevelTwoScript_t1243006306::get_offset_of_m_bTriggeredIceFall_13(),
	LevelTwoScript_t1243006306::get_offset_of_m_iTriggeredCaveTui_14(),
	LevelTwoScript_t1243006306::get_offset_of_m_iTriggeredDoorTui_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (Lv6GnatsConv_t2053771584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[3] = 
{
	Lv6GnatsConv_t2053771584::get_offset_of_bActive_2(),
	Lv6GnatsConv_t2053771584::get_offset_of_bAnsweredCorrectly_3(),
	Lv6GnatsConv_t2053771584::get_offset_of_FlyAwayPos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (Lv7CliffScene_t1442947585)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3013[10] = 
{
	Lv7CliffScene_t1442947585::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (Lv7CliffCutScene_t2618734545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[8] = 
{
	Lv7CliffCutScene_t2618734545::get_offset_of_Sparks_2(),
	Lv7CliffCutScene_t2618734545::get_offset_of_SparksPosition_3(),
	Lv7CliffCutScene_t2618734545::get_offset_of_SceneStatus_4(),
	Lv7CliffCutScene_t2618734545::get_offset_of_TopLadderPos_5(),
	Lv7CliffCutScene_t2618734545::get_offset_of_BottomLadderPos_6(),
	Lv7CliffCutScene_t2618734545::get_offset_of_m_hopeTalkFinished_7(),
	Lv7CliffCutScene_t2618734545::get_offset_of_m_fTime_8(),
	Lv7CliffCutScene_t2618734545::get_offset_of_start_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (KOGStatus_t3201523329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[14] = 
{
	KOGStatus_t3201523329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (AttackMethod_t1775622954)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3016[3] = 
{
	AttackMethod_t1775622954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (KOGAttack_t789146765)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3017[5] = 
{
	KOGAttack_t789146765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (Lv7KingOfGnats_t3383848971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[23] = 
{
	Lv7KingOfGnats_t3383848971::get_offset_of_status_2(),
	Lv7KingOfGnats_t3383848971::get_offset_of_KoG_3(),
	Lv7KingOfGnats_t3383848971::get_offset_of_goGnat_4(),
	Lv7KingOfGnats_t3383848971::get_offset_of_goSpark_5(),
	Lv7KingOfGnats_t3383848971::get_offset_of_shield_6(),
	Lv7KingOfGnats_t3383848971::get_offset_of_gem_7(),
	Lv7KingOfGnats_t3383848971::get_offset_of_GnatsStayPosition_8(),
	Lv7KingOfGnats_t3383848971::get_offset_of_gnatKilledPosition_9(),
	Lv7KingOfGnats_t3383848971::get_offset_of_AttackingGnats_10(),
	Lv7KingOfGnats_t3383848971::get_offset_of_Sparks_11(),
	Lv7KingOfGnats_t3383848971::get_offset_of_CurrentGnat_12(),
	Lv7KingOfGnats_t3383848971::get_offset_of_CameraID_13(),
	Lv7KingOfGnats_t3383848971::get_offset_of_AttackStrategy_14(),
	Lv7KingOfGnats_t3383848971::get_offset_of_KingAttack_15(),
	Lv7KingOfGnats_t3383848971::get_offset_of_hitPosition_16(),
	Lv7KingOfGnats_t3383848971::get_offset_of_m_iNumOfGnatsFiredOut_17(),
	Lv7KingOfGnats_t3383848971::get_offset_of_m_iNumOfGnatsKilled_18(),
	Lv7KingOfGnats_t3383848971::get_offset_of_m_iNumToFire_19(),
	Lv7KingOfGnats_t3383848971::get_offset_of_CurrentEmptySpace_20(),
	Lv7KingOfGnats_t3383848971::get_offset_of_fUpCounter_21(),
	Lv7KingOfGnats_t3383848971::get_offset_of_fTime_22(),
	Lv7KingOfGnats_t3383848971::get_offset_of_fStatusTimer_23(),
	Lv7KingOfGnats_t3383848971::get_offset_of_fireNext_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (Lv7KingOfGnatsFadeIn_t1641895077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[8] = 
{
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_KOGFadeInFinished_2(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_KOGAppearTime_3(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_m_fMonsterFadeInAlpha_4(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_fTime_5(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_KOG_6(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_m_cMonsterColor_7(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_m_cMonsterEyeColor_8(),
	Lv7KingOfGnatsFadeIn_t1641895077::get_offset_of_m_cMonsterFurColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (ShieldAction_t3393513384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[8] = 
{
	ShieldAction_t3393513384::get_offset_of_shieldIsOn_2(),
	ShieldAction_t3393513384::get_offset_of_m_bShieldFadeInFinished_3(),
	ShieldAction_t3393513384::get_offset_of_m_bShieldFadeOutFinished_4(),
	ShieldAction_t3393513384::get_offset_of_m_cShieldColor_5(),
	ShieldAction_t3393513384::get_offset_of_m_fShieldFadeAlfa_6(),
	ShieldAction_t3393513384::get_offset_of_m_bFadeInStart_7(),
	ShieldAction_t3393513384::get_offset_of_m_bFadeOutStart_8(),
	ShieldAction_t3393513384::get_offset_of_m_fShieldAppearTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (LoadTalkSceneXMLData_t2381108950), -1, sizeof(LoadTalkSceneXMLData_t2381108950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3021[13] = 
{
	LoadTalkSceneXMLData_t2381108950::get_offset_of_sm_strFilename_2(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_sm_Read_3(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_MulitiQuestionNum_4(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_scenesList_5(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_m_xmlScenes_6(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_m_xmlElement_7(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_GoodByeScript_8(),
	LoadTalkSceneXMLData_t2381108950::get_offset_of_MiniGame_L3DragAndDropBlinc_9(),
	LoadTalkSceneXMLData_t2381108950_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_10(),
	LoadTalkSceneXMLData_t2381108950_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_11(),
	LoadTalkSceneXMLData_t2381108950_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_12(),
	LoadTalkSceneXMLData_t2381108950_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_13(),
	LoadTalkSceneXMLData_t2381108950_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (Login_t3582553427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[11] = 
{
	Login_t3582553427::get_offset_of_RegistrationForm_2(),
	Login_t3582553427::get_offset_of_ParentRegistrationForm_3(),
	Login_t3582553427::get_offset_of_instance_4(),
	Login_t3582553427::get_offset_of_instanceTHL_5(),
	Login_t3582553427::get_offset_of_m_bGotLoginTokenDone_6(),
	Login_t3582553427::get_offset_of_UserName_7(),
	Login_t3582553427::get_offset_of_Password_8(),
	Login_t3582553427::get_offset_of_LoginPanel_9(),
	Login_t3582553427::get_offset_of_SignUpPanel_10(),
	Login_t3582553427::get_offset_of_oNotifications_11(),
	Login_t3582553427::get_offset_of_sNotificationError_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (Notifications_t688528964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[5] = 
{
	Notifications_t688528964::get_offset_of_stext_2(),
	Notifications_t688528964::get_offset_of_oText_3(),
	Notifications_t688528964::get_offset_of_bshowNotification_4(),
	Notifications_t688528964::get_offset_of_bhideNotification_5(),
	Notifications_t688528964::get_offset_of_fFade_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (U3CShowTextU3Ec__Iterator0_t3562466708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[5] = 
{
	U3CShowTextU3Ec__Iterator0_t3562466708::get_offset_of_U3CoColorU3E__0_0(),
	U3CShowTextU3Ec__Iterator0_t3562466708::get_offset_of_U24this_1(),
	U3CShowTextU3Ec__Iterator0_t3562466708::get_offset_of_U24current_2(),
	U3CShowTextU3Ec__Iterator0_t3562466708::get_offset_of_U24disposing_3(),
	U3CShowTextU3Ec__Iterator0_t3562466708::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (U3CHideTextU3Ec__Iterator1_t2182125989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[5] = 
{
	U3CHideTextU3Ec__Iterator1_t2182125989::get_offset_of_U3CoColorU3E__0_0(),
	U3CHideTextU3Ec__Iterator1_t2182125989::get_offset_of_U24this_1(),
	U3CHideTextU3Ec__Iterator1_t2182125989::get_offset_of_U24current_2(),
	U3CHideTextU3Ec__Iterator1_t2182125989::get_offset_of_U24disposing_3(),
	U3CHideTextU3Ec__Iterator1_t2182125989::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (Registration_t2162293902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[50] = 
{
	Registration_t2162293902::get_offset_of_DropWho_2(),
	Registration_t2162293902::get_offset_of_UserPanel_3(),
	Registration_t2162293902::get_offset_of_YoungPersonPanel_4(),
	Registration_t2162293902::get_offset_of_GenderPanel_5(),
	Registration_t2162293902::get_offset_of_EmailPanel_6(),
	Registration_t2162293902::get_offset_of_LivePanel_7(),
	Registration_t2162293902::get_offset_of_ContinueButton_8(),
	Registration_t2162293902::get_offset_of_BackButton_9(),
	Registration_t2162293902::get_offset_of_OtherEthnicToggle_10(),
	Registration_t2162293902::get_offset_of_FindDropdown_11(),
	Registration_t2162293902::get_offset_of_OtherWho_12(),
	Registration_t2162293902::get_offset_of_OtherEthnic_13(),
	Registration_t2162293902::get_offset_of_OtherFind_14(),
	Registration_t2162293902::get_offset_of_OtherFindLabel_15(),
	Registration_t2162293902::get_offset_of_Field_UserUserName_16(),
	Registration_t2162293902::get_offset_of_Field_Who_17(),
	Registration_t2162293902::get_offset_of_Field_WhoOtro_18(),
	Registration_t2162293902::get_offset_of_Field_Old_19(),
	Registration_t2162293902::get_offset_of_Field_NZ_20(),
	Registration_t2162293902::get_offset_of_Field_Maori_21(),
	Registration_t2162293902::get_offset_of_Field_Samoan_22(),
	Registration_t2162293902::get_offset_of_Field_Cook_23(),
	Registration_t2162293902::get_offset_of_Field_Tongan_24(),
	Registration_t2162293902::get_offset_of_Field_Niuean_25(),
	Registration_t2162293902::get_offset_of_Field_Chinesse_26(),
	Registration_t2162293902::get_offset_of_Field_Indian_27(),
	Registration_t2162293902::get_offset_of_Field_Other_28(),
	Registration_t2162293902::get_offset_of_Field_OtherTEXT_29(),
	Registration_t2162293902::get_offset_of_Field_Gender_30(),
	Registration_t2162293902::get_offset_of_Field_eMail_31(),
	Registration_t2162293902::get_offset_of_Field_ReeMail_32(),
	Registration_t2162293902::get_offset_of_Field_Information_33(),
	Registration_t2162293902::get_offset_of_Field_Password_34(),
	Registration_t2162293902::get_offset_of_Field_RePassword_35(),
	Registration_t2162293902::get_offset_of_Field_Code_36(),
	Registration_t2162293902::get_offset_of_Field_Mobile_37(),
	Registration_t2162293902::get_offset_of_Field_Live_38(),
	Registration_t2162293902::get_offset_of_Field_Find_39(),
	Registration_t2162293902::get_offset_of_Field_SpecifyFind_40(),
	Registration_t2162293902::get_offset_of_Field_Conact_41(),
	Registration_t2162293902::get_offset_of_Field_ExtraHelp_42(),
	Registration_t2162293902::get_offset_of_Field_Terms_43(),
	Registration_t2162293902::get_offset_of_instance_44(),
	Registration_t2162293902::get_offset_of_m_bGotRegistrationTokenDone_45(),
	Registration_t2162293902::get_offset_of_LoginPanel_46(),
	Registration_t2162293902::get_offset_of_SignUpPanel_47(),
	Registration_t2162293902::get_offset_of_oTextRegisterNotificationMessage_48(),
	Registration_t2162293902::get_offset_of_bSubmit_49(),
	Registration_t2162293902::get_offset_of_EmailPanelRectTransform_50(),
	Registration_t2162293902::get_offset_of_oNotifications_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (U3CValidateandSendU3Ec__Iterator0_t3923129369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[4] = 
{
	U3CValidateandSendU3Ec__Iterator0_t3923129369::get_offset_of_U24this_0(),
	U3CValidateandSendU3Ec__Iterator0_t3923129369::get_offset_of_U24current_1(),
	U3CValidateandSendU3Ec__Iterator0_t3923129369::get_offset_of_U24disposing_2(),
	U3CValidateandSendU3Ec__Iterator0_t3923129369::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (BarrelStatus_t3369822665)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[8] = 
{
	BarrelStatus_t3369822665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (BarrelGame_t565740629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[21] = 
{
	BarrelGame_t565740629::get_offset_of_outlines_2(),
	BarrelGame_t565740629::get_offset_of_Gnats_3(),
	BarrelGame_t565740629::get_offset_of_Sparx_4(),
	BarrelGame_t565740629::get_offset_of_Status_5(),
	BarrelGame_t565740629::get_offset_of_iCurrentPair_6(),
	BarrelGame_t565740629::get_offset_of_BarrelName_7(),
	BarrelGame_t565740629::get_offset_of_m_bEnd_8(),
	BarrelGame_t565740629::get_offset_of_m_bCanPlay_9(),
	BarrelGame_t565740629::get_offset_of_CorrectBarrel_10(),
	BarrelGame_t565740629::get_offset_of_LeftPos_11(),
	BarrelGame_t565740629::get_offset_of_RightPos_12(),
	BarrelGame_t565740629::get_offset_of_CenterPos_13(),
	BarrelGame_t565740629::get_offset_of_FlyFrom_14(),
	BarrelGame_t565740629::get_offset_of_m_bGameStart_15(),
	BarrelGame_t565740629::get_offset_of_Platforms_16(),
	BarrelGame_t565740629::get_offset_of_CurrentObject_17(),
	BarrelGame_t565740629::get_offset_of_explosionEffect_18(),
	BarrelGame_t565740629::get_offset_of_m_bTuiConversationStarts_19(),
	BarrelGame_t565740629::get_offset_of_PlayerLookat_20(),
	BarrelGame_t565740629::get_offset_of_start_21(),
	BarrelGame_t565740629::get_offset_of_m_fTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (BarrelGnatStatus_t1584189780)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3030[5] = 
{
	BarrelGnatStatus_t1584189780::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (BarrelGnat_t3244074211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[8] = 
{
	BarrelGnat_t3244074211::get_offset_of_GnatWords_2(),
	BarrelGnat_t3244074211::get_offset_of_StartTime_3(),
	BarrelGnat_t3244074211::get_offset_of_EndTime_4(),
	BarrelGnat_t3244074211::get_offset_of_CorrectBarrel_5(),
	BarrelGnat_t3244074211::get_offset_of_CenterPos_6(),
	BarrelGnat_t3244074211::get_offset_of_FlyAwayPos_7(),
	BarrelGnat_t3244074211::get_offset_of_Status_8(),
	BarrelGnat_t3244074211::get_offset_of_m_VoiceAS_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (BarrelSparx_t1842418686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[3] = 
{
	BarrelSparx_t1842418686::get_offset_of_Line_2(),
	BarrelSparx_t1842418686::get_offset_of_StartTime_3(),
	BarrelSparx_t1842418686::get_offset_of_EndTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (DisplayQuestion_t773709411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[16] = 
{
	DisplayQuestion_t773709411::get_offset_of_bCorrectAnswer_2(),
	DisplayQuestion_t773709411::get_offset_of_bFinishedAnswering_3(),
	DisplayQuestion_t773709411::get_offset_of_GUIskin_4(),
	DisplayQuestion_t773709411::get_offset_of_bActivated_5(),
	DisplayQuestion_t773709411::get_offset_of_Options_6(),
	DisplayQuestion_t773709411::get_offset_of_CorrectOption_7(),
	DisplayQuestion_t773709411::get_offset_of_Question_8(),
	DisplayQuestion_t773709411::get_offset_of_CorrespondingSparx_9(),
	DisplayQuestion_t773709411::get_offset_of_fStartVoice_10(),
	DisplayQuestion_t773709411::get_offset_of_fEndVoice_11(),
	DisplayQuestion_t773709411::get_offset_of_VoiceFile_12(),
	DisplayQuestion_t773709411::get_offset_of_L4GnatsAudioClip_13(),
	DisplayQuestion_t773709411::get_offset_of_m_Voice_14(),
	DisplayQuestion_t773709411::get_offset_of_fHeight_15(),
	DisplayQuestion_t773709411::get_offset_of_Name_16(),
	DisplayQuestion_t773709411::get_offset_of_countdown_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (FlyingSparx_t1256167344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[4] = 
{
	FlyingSparx_t1256167344::get_offset_of_fSpawnDelay_2(),
	FlyingSparx_t1256167344::get_offset_of_Target_3(),
	FlyingSparx_t1256167344::get_offset_of_bSpawn_4(),
	FlyingSparx_t1256167344::get_offset_of_bFlying_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (JigSawPuzzle_t1096861692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[12] = 
{
	JigSawPuzzle_t1096861692::get_offset_of_StartingPositions_2(),
	JigSawPuzzle_t1096861692::get_offset_of_CorrectPosition_3(),
	JigSawPuzzle_t1096861692::get_offset_of_m_bAnimatePuzzleClear_4(),
	JigSawPuzzle_t1096861692::get_offset_of_m_bCheckPuzzle_5(),
	JigSawPuzzle_t1096861692::get_offset_of_bSolved_6(),
	JigSawPuzzle_t1096861692::get_offset_of_NumOfJigSawPuzzle_7(),
	JigSawPuzzle_t1096861692::get_offset_of_m_5jigsawbg2_8(),
	JigSawPuzzle_t1096861692::get_offset_of_m_5jigsawbg_9(),
	JigSawPuzzle_t1096861692::get_offset_of_m_bAnimateRed_10(),
	JigSawPuzzle_t1096861692::get_offset_of_m_fDelays_11(),
	JigSawPuzzle_t1096861692::get_offset_of_m_fDelayElapsed_12(),
	JigSawPuzzle_t1096861692::get_offset_of_NumOfChanges_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (JigSawPuzzleWord_t52544638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[7] = 
{
	JigSawPuzzleWord_t52544638::get_offset_of_StartingPositions_2(),
	JigSawPuzzleWord_t52544638::get_offset_of_CorrectPosition_3(),
	JigSawPuzzleWord_t52544638::get_offset_of_CorrectWord_4(),
	JigSawPuzzleWord_t52544638::get_offset_of_m_bSolved_5(),
	JigSawPuzzleWord_t52544638::get_offset_of_m_bFinished_6(),
	JigSawPuzzleWord_t52544638::get_offset_of_m_fTimer_7(),
	JigSawPuzzleWord_t52544638::get_offset_of_m_6perpsectivebg2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (L3TriGameHandler_t1673593620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[13] = 
{
	L3TriGameHandler_t1673593620::get_offset_of_m_Games_2(),
	L3TriGameHandler_t1673593620::get_offset_of_m_Birds_3(),
	L3TriGameHandler_t1673593620::get_offset_of_m_TriggerPos_4(),
	L3TriGameHandler_t1673593620::get_offset_of_m_MoveToPos_5(),
	L3TriGameHandler_t1673593620::get_offset_of_m_LookAts_6(),
	L3TriGameHandler_t1673593620::get_offset_of_m_fGameStartDistance_7(),
	L3TriGameHandler_t1673593620::get_offset_of_m_player_8(),
	L3TriGameHandler_t1673593620::get_offset_of_startPos_9(),
	L3TriGameHandler_t1673593620::get_offset_of_m_iCurrentGame_10(),
	L3TriGameHandler_t1673593620::get_offset_of_m_bIsGameDone_11(),
	L3TriGameHandler_t1673593620::get_offset_of_m_bMovingTo_12(),
	L3TriGameHandler_t1673593620::get_offset_of_m_bGameDone_13(),
	L3TriGameHandler_t1673593620::get_offset_of_m_bWaiting_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (GnatsTalkStatus_t808147551)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3038[9] = 
{
	GnatsTalkStatus_t808147551::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (L4GnatsTalk_t2079079531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[15] = 
{
	L4GnatsTalk_t2079079531::get_offset_of_m_Gnats_2(),
	L4GnatsTalk_t2079079531::get_offset_of_m_Spark_3(),
	L4GnatsTalk_t2079079531::get_offset_of_m_Effect_4(),
	L4GnatsTalk_t2079079531::get_offset_of_m_FlyAwayPos_5(),
	L4GnatsTalk_t2079079531::get_offset_of_m_QuestionPos_6(),
	L4GnatsTalk_t2079079531::get_offset_of_CameraID_7(),
	L4GnatsTalk_t2079079531::get_offset_of_m_CurMover_8(),
	L4GnatsTalk_t2079079531::get_offset_of_m_TempSpark_9(),
	L4GnatsTalk_t2079079531::get_offset_of_m_status_10(),
	L4GnatsTalk_t2079079531::get_offset_of_m_vStartPos_11(),
	L4GnatsTalk_t2079079531::get_offset_of_m_fMoveTime_12(),
	L4GnatsTalk_t2079079531::get_offset_of_m_fStatusTime_13(),
	L4GnatsTalk_t2079079531::get_offset_of_m_fTalkEnd_14(),
	L4GnatsTalk_t2079079531::get_offset_of_m_iCurrentGnat_15(),
	L4GnatsTalk_t2079079531::get_offset_of_m_bTriggered_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (L4LadderHelp_t112018707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[5] = 
{
	L4LadderHelp_t112018707::get_offset_of_oLadderLeft_2(),
	L4LadderHelp_t112018707::get_offset_of_oLadderRight_3(),
	L4LadderHelp_t112018707::get_offset_of_bActivated_4(),
	L4LadderHelp_t112018707::get_offset_of_GUIskin_5(),
	L4LadderHelp_t112018707::get_offset_of_Text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (L4LadderHelpTrigerLeft_t104804450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[1] = 
{
	L4LadderHelpTrigerLeft_t104804450::get_offset_of_m_leftLadderTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (L4LadderHelpTrigerRight_t4238512495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[1] = 
{
	L4LadderHelpTrigerRight_t4238512495::get_offset_of_m_rightLadderTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (L4Spark_t2135258226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[8] = 
{
	L4Spark_t2135258226::get_offset_of_m_bSparkInteracted_2(),
	L4Spark_t2135258226::get_offset_of_m_bSparkInteractionRadius_3(),
	L4Spark_t2135258226::get_offset_of_m_bSparkIsInteracting_4(),
	L4Spark_t2135258226::get_offset_of_m_bGrabbed_5(),
	L4Spark_t2135258226::get_offset_of_m_bCollected_6(),
	L4Spark_t2135258226::get_offset_of_MouseOverTexture_7(),
	L4Spark_t2135258226::get_offset_of_MouseOriTexture_8(),
	L4Spark_t2135258226::get_offset_of_m_fTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (EndPartStatus_t1026950437)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3044[5] = 
{
	EndPartStatus_t1026950437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (L4StepsMiniGame_t1956426605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[20] = 
{
	L4StepsMiniGame_t1956426605::get_offset_of_m_Gem_2(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_Sparks_3(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_SparksWaitPos_4(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_HoverPos_5(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_PlayerMoveTo_6(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_SparkMoveTo_7(),
	L4StepsMiniGame_t1956426605::get_offset_of_player_8(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_vSparksMoveFrom_9(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_EndStatus_10(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_fTime_11(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_iCurrentQuestion_12(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_iDisplayedQuestion_13(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bInteracted_14(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bMovingPlayer_15(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bTalkingToSpark_16(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bGameActive_17(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bDoneTalking_18(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bEndScene_19(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bSparkMovingToGem_20(),
	L4StepsMiniGame_t1956426605::get_offset_of_m_bTuiFlyingDown_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (L5GnatClick_t676204875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[3] = 
{
	L5GnatClick_t676204875::get_offset_of_MouseOverTexture_2(),
	L5GnatClick_t676204875::get_offset_of_MouseOriTexture_3(),
	L5GnatClick_t676204875::get_offset_of_ClickAble_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (Lv5MiniGame2Status_t3456318513)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3047[10] = 
{
	Lv5MiniGame2Status_t3456318513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (Level5MiniGame2_t1561600819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[12] = 
{
	Level5MiniGame2_t1561600819::get_offset_of_CenterPos_2(),
	Level5MiniGame2_t1561600819::get_offset_of_GnatOriginalPos_3(),
	Level5MiniGame2_t1561600819::get_offset_of_CurrentObject_4(),
	Level5MiniGame2_t1561600819::get_offset_of_GameStart_5(),
	Level5MiniGame2_t1561600819::get_offset_of_CameraID_6(),
	Level5MiniGame2_t1561600819::get_offset_of_Status_7(),
	Level5MiniGame2_t1561600819::get_offset_of_SparkFlyToPos_8(),
	Level5MiniGame2_t1561600819::get_offset_of_explosionEffect_9(),
	Level5MiniGame2_t1561600819::get_offset_of_StartPos_10(),
	Level5MiniGame2_t1561600819::get_offset_of_m_fTime_11(),
	Level5MiniGame2_t1561600819::get_offset_of_m_bTuiConversationStarts_12(),
	Level5MiniGame2_t1561600819::get_offset_of_m_bPlayerMovingToPoint_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (LevelSixInBuildingMiniGame_t3344869765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[7] = 
{
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_m_bMoveToPos_2(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_m_bStart_3(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_CurrentStatus_4(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_Gnats_5(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_BasePos_6(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_DefeatedFlyUp_7(),
	LevelSixInBuildingMiniGame_t3344869765::get_offset_of_iCurrentGnat_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (Lv7BridgeGnatStatus_t3321750872)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3050[9] = 
{
	Lv7BridgeGnatStatus_t3321750872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (Lv7BridgeGnatAction_t427326750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[4] = 
{
	Lv7BridgeGnatAction_t427326750::get_offset_of_OriginalPos_2(),
	Lv7BridgeGnatAction_t427326750::get_offset_of_action_3(),
	Lv7BridgeGnatAction_t427326750::get_offset_of_fStandbyTime_4(),
	Lv7BridgeGnatAction_t427326750::get_offset_of_fRespawnTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (Lv7BridgeGnats_t35555675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[3] = 
{
	Lv7BridgeGnats_t35555675::get_offset_of_StartGame_2(),
	Lv7BridgeGnats_t35555675::get_offset_of_Timer_3(),
	Lv7BridgeGnats_t35555675::get_offset_of_StartAttack_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (Lv7CatchSparx_t385659907), -1, sizeof(Lv7CatchSparx_t385659907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3053[8] = 
{
	Lv7CatchSparx_t385659907_StaticFields::get_offset_of_PrepareStart_2(),
	Lv7CatchSparx_t385659907_StaticFields::get_offset_of_GameIsStarted_3(),
	Lv7CatchSparx_t385659907_StaticFields::get_offset_of_NumOfSparksCaught_4(),
	Lv7CatchSparx_t385659907_StaticFields::get_offset_of_fTimer_5(),
	Lv7CatchSparx_t385659907_StaticFields::get_offset_of_m_bGameIsEnded_6(),
	Lv7CatchSparx_t385659907::get_offset_of_move_7(),
	Lv7CatchSparx_t385659907::get_offset_of_sprint_8(),
	Lv7CatchSparx_t385659907::get_offset_of_bag_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (MiniGameCollider_t469925002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[2] = 
{
	MiniGameCollider_t469925002::get_offset_of_index_2(),
	MiniGameCollider_t469925002::get_offset_of_bTriggered_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (OpenChest_t1184101868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[30] = 
{
	OpenChest_t1184101868::get_offset_of_Buttons_2(),
	OpenChest_t1184101868::get_offset_of_ListOne_3(),
	OpenChest_t1184101868::get_offset_of_ListTwo_4(),
	OpenChest_t1184101868::get_offset_of_ListThree_5(),
	OpenChest_t1184101868::get_offset_of_Index_6(),
	OpenChest_t1184101868::get_offset_of_CorrectAnswer_7(),
	OpenChest_t1184101868::get_offset_of_strCameraID_8(),
	OpenChest_t1184101868::get_offset_of_m_bTextEnabled_9(),
	OpenChest_t1184101868::get_offset_of_m_bActive_10(),
	OpenChest_t1184101868::get_offset_of_Texts_11(),
	OpenChest_t1184101868::get_offset_of_Background_12(),
	OpenChest_t1184101868::get_offset_of_Background_Unlocked_13(),
	OpenChest_t1184101868::get_offset_of_m_chestLight3y_14(),
	OpenChest_t1184101868::get_offset_of_m_chestLight3_15(),
	OpenChest_t1184101868::get_offset_of_m_chestLight2y_16(),
	OpenChest_t1184101868::get_offset_of_m_chestLight2_17(),
	OpenChest_t1184101868::get_offset_of_m_chestLight1y_18(),
	OpenChest_t1184101868::get_offset_of_m_chestLight1_19(),
	OpenChest_t1184101868::get_offset_of_m_Rect_20(),
	OpenChest_t1184101868::get_offset_of_ChestLid_21(),
	OpenChest_t1184101868::get_offset_of_CaveDoor_22(),
	OpenChest_t1184101868::get_offset_of_m_bEndTimer_23(),
	OpenChest_t1184101868::get_offset_of_m_fEndTimer_24(),
	OpenChest_t1184101868::get_offset_of_m_bIs2D_25(),
	OpenChest_t1184101868::get_offset_of_m_bTuiStartFlying_26(),
	OpenChest_t1184101868::get_offset_of_m_bEffectsStarted_27(),
	OpenChest_t1184101868::get_offset_of_m_bIsOpened_28(),
	OpenChest_t1184101868::get_offset_of_m_gTuiFlyoutParticleClone_29(),
	OpenChest_t1184101868::get_offset_of_m_gTuiFlyoutparticle_30(),
	OpenChest_t1184101868::get_offset_of_m_threeWords_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (PhraseScript_t112947168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	PhraseScript_t112947168::get_offset_of_CorrectLetterIndex_2(),
	PhraseScript_t112947168::get_offset_of_LettersCovered_3(),
	PhraseScript_t112947168::get_offset_of_m_bClear_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (PlayDialog_t1022950615), -1, sizeof(PlayDialog_t1022950615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3057[14] = 
{
	PlayDialog_t1022950615::get_offset_of_Dialog_2(),
	PlayDialog_t1022950615::get_offset_of_Name_3(),
	PlayDialog_t1022950615::get_offset_of_fStartingAnchor_4(),
	PlayDialog_t1022950615::get_offset_of_fFinishingAnchor_5(),
	PlayDialog_t1022950615::get_offset_of_StartPlaying_6(),
	PlayDialog_t1022950615::get_offset_of_IsPlaying_7(),
	PlayDialog_t1022950615::get_offset_of_VoiceFile_8(),
	PlayDialog_t1022950615::get_offset_of_VoiceAudilClip_9(),
	PlayDialog_t1022950615::get_offset_of_m_Voice_10(),
	PlayDialog_t1022950615::get_offset_of_CorrectBarrel_11(),
	PlayDialog_t1022950615::get_offset_of_GameID_12(),
	PlayDialog_t1022950615_StaticFields::get_offset_of_button_13(),
	PlayDialog_t1022950615::get_offset_of_buttonTexture_14(),
	PlayDialog_t1022950615::get_offset_of_skin_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (PushRockMiniGame_t1414473092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[27] = 
{
	PushRockMiniGame_t1414473092::get_offset_of_Rocks_2(),
	PushRockMiniGame_t1414473092::get_offset_of_RockFallToPos_3(),
	PushRockMiniGame_t1414473092::get_offset_of_EmptySpace_4(),
	PushRockMiniGame_t1414473092::get_offset_of_CameraID_5(),
	PushRockMiniGame_t1414473092::get_offset_of_TriggerPos_6(),
	PushRockMiniGame_t1414473092::get_offset_of_Barriers_7(),
	PushRockMiniGame_t1414473092::get_offset_of_MoveToPos_8(),
	PushRockMiniGame_t1414473092::get_offset_of_Triggered_9(),
	PushRockMiniGame_t1414473092::get_offset_of_FallInterval_10(),
	PushRockMiniGame_t1414473092::get_offset_of_fMin_11(),
	PushRockMiniGame_t1414473092::get_offset_of_fMax_12(),
	PushRockMiniGame_t1414473092::get_offset_of_m_iCurrentRock_13(),
	PushRockMiniGame_t1414473092::get_offset_of_m_iNumOfRockPushed_14(),
	PushRockMiniGame_t1414473092::get_offset_of_m_fExplodeTimer_15(),
	PushRockMiniGame_t1414473092::get_offset_of_endCutsceneLength_16(),
	PushRockMiniGame_t1414473092::get_offset_of_m_fEndTimer_17(),
	PushRockMiniGame_t1414473092::get_offset_of_m_stream4Texture_18(),
	PushRockMiniGame_t1414473092::get_offset_of_m_stream3Texture_19(),
	PushRockMiniGame_t1414473092::get_offset_of_m_stream2Texture_20(),
	PushRockMiniGame_t1414473092::get_offset_of_m_stream1Texture_21(),
	PushRockMiniGame_t1414473092::get_offset_of_m_bStartMovebackToLava2_22(),
	PushRockMiniGame_t1414473092::get_offset_of_CurrentPos_23(),
	PushRockMiniGame_t1414473092::get_offset_of_TargetPos_24(),
	PushRockMiniGame_t1414473092::get_offset_of_RockPushed_25(),
	PushRockMiniGame_t1414473092::get_offset_of_PlayGame_26(),
	PushRockMiniGame_t1414473092::get_offset_of_Moving_27(),
	PushRockMiniGame_t1414473092::get_offset_of_explosionCutscene_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (U3CWaitToEndU3Ec__Iterator0_t1749295493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[4] = 
{
	U3CWaitToEndU3Ec__Iterator0_t1749295493::get_offset_of_U24this_0(),
	U3CWaitToEndU3Ec__Iterator0_t1749295493::get_offset_of_U24current_1(),
	U3CWaitToEndU3Ec__Iterator0_t1749295493::get_offset_of_U24disposing_2(),
	U3CWaitToEndU3Ec__Iterator0_t1749295493::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (PuzzlePiece_t1346995122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[5] = 
{
	PuzzlePiece_t1346995122::get_offset_of_m_vCorrectPos_2(),
	PuzzlePiece_t1346995122::get_offset_of_m_bOnFocus_3(),
	PuzzlePiece_t1346995122::get_offset_of_m_bCanClick_4(),
	PuzzlePiece_t1346995122::get_offset_of_m_bCorrectPosition_5(),
	PuzzlePiece_t1346995122::get_offset_of_Particles_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (PuzzlePieceWord_t3115385946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[6] = 
{
	PuzzlePieceWord_t3115385946::get_offset_of_m_vCorrectPos_2(),
	PuzzlePieceWord_t3115385946::get_offset_of_m_vStartPos_3(),
	PuzzlePieceWord_t3115385946::get_offset_of_m_bOnFocus_4(),
	PuzzlePieceWord_t3115385946::get_offset_of_m_bCanClick_5(),
	PuzzlePieceWord_t3115385946::get_offset_of_m_bCorrectPosition_6(),
	PuzzlePieceWord_t3115385946::get_offset_of_m_bMovement_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (WordSearchPuzzle_t817895283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[15] = 
{
	WordSearchPuzzle_t817895283::get_offset_of_Particles_2(),
	WordSearchPuzzle_t817895283::get_offset_of_Grid_3(),
	WordSearchPuzzle_t817895283::get_offset_of_GridWidth_4(),
	WordSearchPuzzle_t817895283::get_offset_of_GridHeight_5(),
	WordSearchPuzzle_t817895283::get_offset_of_GoalString_6(),
	WordSearchPuzzle_t817895283::get_offset_of_NumOfPhrase_7(),
	WordSearchPuzzle_t817895283::get_offset_of_BlockLeft_8(),
	WordSearchPuzzle_t817895283::get_offset_of_BlockTop_9(),
	WordSearchPuzzle_t817895283::get_offset_of_BlockWidth_10(),
	WordSearchPuzzle_t817895283::get_offset_of_BlockHeight_11(),
	WordSearchPuzzle_t817895283::get_offset_of_Skin_12(),
	WordSearchPuzzle_t817895283::get_offset_of_m_fEndTimer_13(),
	WordSearchPuzzle_t817895283::get_offset_of_ClearedLetters_14(),
	WordSearchPuzzle_t817895283::get_offset_of_allclear_15(),
	WordSearchPuzzle_t817895283::get_offset_of_m_5Action2Texture_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (IngameMusicSwitch_t653995320), -1, sizeof(IngameMusicSwitch_t653995320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3063[15] = 
{
	IngameMusicSwitch_t653995320_StaticFields::get_offset_of_instanceRef_2(),
	IngameMusicSwitch_t653995320::get_offset_of_m_gameMusicAs_3(),
	IngameMusicSwitch_t653995320::get_offset_of_currentSceneName_4(),
	IngameMusicSwitch_t653995320::get_offset_of_m_gameStartFadeMusic_5(),
	IngameMusicSwitch_t653995320::get_offset_of_m_guideFadeMusic_6(),
	IngameMusicSwitch_t653995320::get_offset_of_m_mentorMusic_7(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l1Music_8(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l2Music_9(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l3Music_10(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l4Music_11(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l5Music_12(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l6Music_13(),
	IngameMusicSwitch_t653995320::get_offset_of_m_l7Music_14(),
	IngameMusicSwitch_t653995320::get_offset_of_m_playStartFadeMisic_15(),
	IngameMusicSwitch_t653995320::get_offset_of_m_playFadeMisic_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (SoundPlayer_t4159161212), -1, sizeof(SoundPlayer_t4159161212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3064[3] = 
{
	SoundPlayer_t4159161212_StaticFields::get_offset_of_m_acSoundClips_0(),
	SoundPlayer_t4159161212_StaticFields::get_offset_of_instance_1(),
	SoundPlayer_t4159161212_StaticFields::get_offset_of_loopPlayer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (BoyCustomisation_t2828205644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[4] = 
{
	BoyCustomisation_t2828205644::get_offset_of_m_skin_29(),
	BoyCustomisation_t2828205644::get_offset_of_m_boyCharacterPrf_30(),
	BoyCustomisation_t2828205644::get_offset_of_m_vBoysCustomisePosition_31(),
	BoyCustomisation_t2828205644::get_offset_of_m_rotationAngle_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (CameraMovement_t1630064389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[10] = 
{
	CameraMovement_t1630064389::get_offset_of_PlayerTransform_2(),
	CameraMovement_t1630064389::get_offset_of_UpScaler_3(),
	CameraMovement_t1630064389::get_offset_of_ForwardScaler_4(),
	CameraMovement_t1630064389::get_offset_of_LookAtScaler_5(),
	CameraMovement_t1630064389::get_offset_of_m_bFollow_6(),
	CameraMovement_t1630064389::get_offset_of_m_bCameraLookAt_7(),
	CameraMovement_t1630064389::get_offset_of_m_fDamping_8(),
	CameraMovement_t1630064389::get_offset_of_m_vLookAtTarget_9(),
	CameraMovement_t1630064389::get_offset_of_m_vTargetPosition_10(),
	CameraMovement_t1630064389::get_offset_of_m_bArrivedAtTarget_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (ClickCharacter_t1040688154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[1] = 
{
	ClickCharacter_t1040688154::get_offset_of_m_characterSelection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (DropRock_t875601513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3068[8] = 
{
	DropRock_t875601513::get_offset_of_m_fDropRate_2(),
	DropRock_t875601513::get_offset_of_m_fSpeed_3(),
	DropRock_t875601513::get_offset_of_MouseOverTexture_4(),
	DropRock_t875601513::get_offset_of_MouseOriTexture_5(),
	DropRock_t875601513::get_offset_of_m_bIsDropping_6(),
	DropRock_t875601513::get_offset_of_m_bPushOff_7(),
	DropRock_t875601513::get_offset_of_m_bFinished_8(),
	DropRock_t875601513::get_offset_of_position_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[4] = 
{
	U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808::get_offset_of_U24this_0(),
	U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808::get_offset_of_U24current_1(),
	U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808::get_offset_of_U24disposing_2(),
	U3CWaitForAnimToFinishU3Ec__Iterator0_t3925549808::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (GirlCustomisation_t851347520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[4] = 
{
	GirlCustomisation_t851347520::get_offset_of_m_skin_29(),
	GirlCustomisation_t851347520::get_offset_of_m_girlCharacterPrf_30(),
	GirlCustomisation_t851347520::get_offset_of_m_vGirlsCustomisePosition_31(),
	GirlCustomisation_t851347520::get_offset_of_m_rotationAngle_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (Inventory_t2659890484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[3] = 
{
	Inventory_t2659890484::get_offset_of_Items_2(),
	Inventory_t2659890484::get_offset_of_m_icurrentPlayingMinigameIndex_3(),
	Inventory_t2659890484::get_offset_of_m_sTempSceneName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (PlayerCombat_t6700974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (PlayerEffect_t398009925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[7] = 
{
	PlayerEffect_t398009925::get_offset_of_FireBall_2(),
	PlayerEffect_t398009925::get_offset_of_FireEffect_3(),
	PlayerEffect_t398009925::get_offset_of_AttackThisObject_4(),
	PlayerEffect_t398009925::get_offset_of_tempEffect_5(),
	PlayerEffect_t398009925::get_offset_of_ExplosionEffect_6(),
	PlayerEffect_t398009925::get_offset_of_m_bAttack_7(),
	PlayerEffect_t398009925::get_offset_of_fTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (PlayerFreeze_t446343823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[12] = 
{
	PlayerFreeze_t446343823::get_offset_of_fFreezeGauge_2(),
	PlayerFreeze_t446343823::get_offset_of_Overlay_3(),
	PlayerFreeze_t446343823::get_offset_of_ResourceNameBase_4(),
	PlayerFreeze_t446343823::get_offset_of_CollideObjectName_5(),
	PlayerFreeze_t446343823::get_offset_of_ResetPos_6(),
	PlayerFreeze_t446343823::get_offset_of_FinishPos_7(),
	PlayerFreeze_t446343823::get_offset_of_numberIncrease_8(),
	PlayerFreeze_t446343823::get_offset_of_m_resourceNameBase4Texture_9(),
	PlayerFreeze_t446343823::get_offset_of_m_resourceNameBase3Texture_10(),
	PlayerFreeze_t446343823::get_offset_of_m_resourceNameBase2Texture_11(),
	PlayerFreeze_t446343823::get_offset_of_m_resourceNameBase1Texture_12(),
	PlayerFreeze_t446343823::get_offset_of_time_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (U3CWaitForHitU3Ec__Iterator0_t582703684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[4] = 
{
	U3CWaitForHitU3Ec__Iterator0_t582703684::get_offset_of_U24this_0(),
	U3CWaitForHitU3Ec__Iterator0_t582703684::get_offset_of_U24current_1(),
	U3CWaitForHitU3Ec__Iterator0_t582703684::get_offset_of_U24disposing_2(),
	U3CWaitForHitU3Ec__Iterator0_t582703684::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (PlayerInfoLoadOnLevelLoad_t3228362066), -1, sizeof(PlayerInfoLoadOnLevelLoad_t3228362066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3076[7] = 
{
	PlayerInfoLoadOnLevelLoad_t3228362066_StaticFields::get_offset_of_instanceRef_2(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_m_bLevelDataIsLoaded_3(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_m_iTotalHairStyleNumber_4(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_m_bRetrievingLevelData_5(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_m_bLoadDataOnce_6(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_m_tTrimTexture_7(),
	PlayerInfoLoadOnLevelLoad_t3228362066::get_offset_of_instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (EMovement_t4140963026)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3077[6] = 
{
	EMovement_t4140963026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (PlayerMovement_t3557127520), -1, sizeof(PlayerMovement_t3557127520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3078[34] = 
{
	PlayerMovement_t3557127520::get_offset_of_m_fSpeed_2(),
	PlayerMovement_t3557127520::get_offset_of_m_fRunningSpeed_3(),
	PlayerMovement_t3557127520::get_offset_of_m_fWalkingSpeed_4(),
	PlayerMovement_t3557127520::get_offset_of_m_fRotationSpeed_5(),
	PlayerMovement_t3557127520::get_offset_of_m_bWalkOnly_6(),
	PlayerMovement_t3557127520::get_offset_of_m_bMovement_7(),
	PlayerMovement_t3557127520::get_offset_of_m_movingPrivate_8(),
	PlayerMovement_t3557127520::get_offset_of_m_bTuiMovementAttach_9(),
	PlayerMovement_t3557127520::get_offset_of_m_vTargetPosition_10(),
	PlayerMovement_t3557127520::get_offset_of_m_vMoveFromPosition_11(),
	PlayerMovement_t3557127520::get_offset_of_m_vTargetLookAt_12(),
	PlayerMovement_t3557127520::get_offset_of_m_strInteractionName_13(),
	PlayerMovement_t3557127520::get_offset_of_m_fTime_14(),
	PlayerMovement_t3557127520::get_offset_of_m_fNormalisedSpeed_15(),
	PlayerMovement_t3557127520::get_offset_of_m_bMoveTowardsNPC_16(),
	PlayerMovement_t3557127520::get_offset_of_m_bRotateTowardsTarget_17(),
	PlayerMovement_t3557127520::get_offset_of_isJumping_18(),
	PlayerMovement_t3557127520_StaticFields::get_offset_of_bagGlowMat_19(),
	PlayerMovement_t3557127520::get_offset_of_mats_20(),
	PlayerMovement_t3557127520_StaticFields::get_offset_of_allMats_21(),
	PlayerMovement_t3557127520::get_offset_of_jumpHeight_22(),
	PlayerMovement_t3557127520::get_offset_of_jumpForce_23(),
	PlayerMovement_t3557127520::get_offset_of_gravity_24(),
	PlayerMovement_t3557127520::get_offset_of_moveCap_25(),
	PlayerMovement_t3557127520::get_offset_of_runCap_26(),
	PlayerMovement_t3557127520::get_offset_of_mobileControl_27(),
	PlayerMovement_t3557127520::get_offset_of_m_ObjectMobileController_28(),
	PlayerMovement_t3557127520::get_offset_of_m_ObjectMobileControllerMove_29(),
	PlayerMovement_t3557127520::get_offset_of_m_ObjectMobileControllerSprint_30(),
	PlayerMovement_t3557127520::get_offset_of_m_ObjectMobileController_LeftPosition_31(),
	PlayerMovement_t3557127520::get_offset_of_m_ObjectMobileController_RightPosition_32(),
	PlayerMovement_t3557127520::get_offset_of_slideMove_33(),
	PlayerMovement_t3557127520::get_offset_of_scriptsCanStopMovement_34(),
	PlayerMovement_t3557127520::get_offset_of_moveVector_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (CameraShakes_t3649564702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[3] = 
{
	CameraShakes_t3649564702::get_offset_of_camTransform_2(),
	CameraShakes_t3649564702::get_offset_of_shakeAmount_3(),
	CameraShakes_t3649564702::get_offset_of_originalPos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (RemoveDuplicateObjects_t2188025678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[2] = 
{
	RemoveDuplicateObjects_t2188025678::get_offset_of_frameCount_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (CapturedDataInput_t2616152122), -1, sizeof(CapturedDataInput_t2616152122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3081[40] = 
{
	CapturedDataInput_t2616152122_StaticFields::get_offset_of_instanceRef_2(),
	CapturedDataInput_t2616152122::get_offset_of_bGotDynamicURL_RESPONSE_NULL_3(),
	CapturedDataInput_t2616152122::get_offset_of_bDynamicURL_ATTEMPTING_4(),
	CapturedDataInput_t2616152122::get_offset_of_bNoDynamicURL_AFTERATTEMPTS_5(),
	CapturedDataInput_t2616152122::get_offset_of_attemps_6(),
	CapturedDataInput_t2616152122::get_offset_of_m_sGameLoginToken_7(),
	CapturedDataInput_t2616152122::get_offset_of_m_sGameDynamicToken_8(),
	CapturedDataInput_t2616152122::get_offset_of_m_sGameConfigServerURL_9(),
	CapturedDataInput_t2616152122::get_offset_of_m_sDynamicURL_10(),
	CapturedDataInput_t2616152122::get_offset_of_m_iServerLevelNum_11(),
	CapturedDataInput_t2616152122::get_offset_of_m_sSaveProgressURL_12(),
	CapturedDataInput_t2616152122::get_offset_of_m_sReadProgressURL_13(),
	CapturedDataInput_t2616152122::get_offset_of_m_dynamiURLRetriever_14(),
	CapturedDataInput_t2616152122::get_offset_of_m_ProcessDataPostRetriever_15(),
	CapturedDataInput_t2616152122::get_offset_of_m_ProcessDataGetRetriever_16(),
	CapturedDataInput_t2616152122::get_offset_of_m_ProcessNotebookDataGetRetriever_17(),
	CapturedDataInput_t2616152122::get_offset_of_m_LevelDataPostRetriever_18(),
	CapturedDataInput_t2616152122::get_offset_of_m_LevelDataGetRetriever_19(),
	CapturedDataInput_t2616152122::get_offset_of_m_EventLevelStartEndDataPostRetriever_20(),
	CapturedDataInput_t2616152122::get_offset_of_m_EventPHQADataPostRetriever_21(),
	CapturedDataInput_t2616152122::get_offset_of_m_sLoadURLReturn_22(),
	CapturedDataInput_t2616152122::get_offset_of_m_sSaveURLReturn_23(),
	CapturedDataInput_t2616152122::get_offset_of_m_txtDataSender_24(),
	CapturedDataInput_t2616152122::get_offset_of_m_ProcessLoginAPIDrupal_25(),
	CapturedDataInput_t2616152122::get_offset_of_m_ProcessRegistrationAPIDrupal_26(),
	CapturedDataInput_t2616152122::get_offset_of_failedFileName_27(),
	CapturedDataInput_t2616152122::get_offset_of_m_bFoundLevelNumber_28(),
	CapturedDataInput_t2616152122::get_offset_of_m_bLoggedInConfirmed_29(),
	CapturedDataInput_t2616152122::get_offset_of_m_dynamicLoginSuccessful_30(),
	CapturedDataInput_t2616152122::get_offset_of_m_loginSuccessful_31(),
	CapturedDataInput_t2616152122::get_offset_of_m_sNotebookHeader_32(),
	CapturedDataInput_t2616152122::get_offset_of_platform_type_33(),
	CapturedDataInput_t2616152122::get_offset_of_LoginPanel_34(),
	CapturedDataInput_t2616152122::get_offset_of_SignUpPanel_35(),
	CapturedDataInput_t2616152122_StaticFields::get_offset_of_instance_36(),
	CapturedDataInput_t2616152122::get_offset_of_instanceTHL_37(),
	CapturedDataInput_t2616152122::get_offset_of_bUploadFileComplete_38(),
	CapturedDataInput_t2616152122::get_offset_of_bWaitingForSeconds_39(),
	CapturedDataInput_t2616152122::get_offset_of_bOfflineFinishLevelSave_40(),
	CapturedDataInput_t2616152122::get_offset_of_bLevelNumberLoaded_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[10] = 
{
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U3CloginURLU3E__0_0(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_Username_1(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_Password_2(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U3CjsonStringU3E__0_3(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U3CencodingU3E__0_4(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U3CpostHeaderU3E__0_5(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U24this_6(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U24current_7(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U24disposing_8(),
	U3CLoginProcess_NewJsonSchemaU3Ec__Iterator0_t2529769024::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[8] = 
{
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U3CregURLU3E__0_0(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U3CencodingU3E__0_1(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U3CpostHeaderU3E__0_2(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_jsonString_3(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U24this_4(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U24current_5(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U24disposing_6(),
	U3CRegisterProcess_NewJsonSchemaU3Ec__Iterator1_t1441432181::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[5] = 
{
	U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275::get_offset_of__url_0(),
	U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275::get_offset_of_U24this_1(),
	U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275::get_offset_of_U24current_2(),
	U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275::get_offset_of_U24disposing_3(),
	U3CSendOffGameConfigURL_NewJsonSchemaU3Ec__Iterator2_t3358985275::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[9] = 
{
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U3CpostScoreURLU3E__0_0(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of__levelNum_1(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U3CjsonStringU3E__0_2(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U3CencodingU3E__0_3(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U3CpostHeaderU3E__0_4(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U24this_5(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U24current_6(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U24disposing_7(),
	U3CSaveUserProcessToServer_NewJsonSchemaU3Ec__Iterator3_t3799581858::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[10] = 
{
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U3CtestU3E__0_0(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_bOffline_1(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U3CuserResponseURLU3E__1_2(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U3CjsonU3E__0_3(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U3CtempNumU3E__2_4(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U3CSavePointU3E__3_5(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U24this_6(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U24current_7(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U24disposing_8(),
	U3CReadUserProgressDataFromServer_NewJsonSchemaU3Ec__Iterator4_t454470102::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[10] = 
{
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U3CtestU3E__0_0(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_bOffline_1(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U3CformU3E__1_2(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of__levelNum_3(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U3CjsonU3E__0_4(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U3CjsonNotebookItemU3E__0_5(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U24this_6(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U24current_7(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U24disposing_8(),
	U3CReadUserProgressNoteBookDataFromServer_NewJsonSchemaU3Ec__Iterator5_t2419969789::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[11] = 
{
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CpostScoreURLU3E__0_0(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CjsonStringU3E__0_1(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CencodingU3E__0_2(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CpostHeaderU3E__0_3(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CwriterU3E__1_4(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CreaderU3E__1_5(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U3CcontentU3E__1_6(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U24this_7(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U24current_8(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U24disposing_9(),
	U3CSaveUserLevelToServer_NewJsonSchemaU3Ec__Iterator6_t2683436496::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[8] = 
{
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U3CtestU3E__0_0(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_bOffline_1(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U3CcharacterURLU3E__1_2(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U3CjsonU3E__0_3(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U24this_4(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U24current_5(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U24disposing_6(),
	U3CReadUserLevelDataFromServer_NewJsonSchemaU3Ec__Iterator7_t731772144::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[10] = 
{
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of__eventID_0(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U3CpostScoreURLU3E__0_1(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of__levelNum_2(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U3CjsonStringU3E__0_3(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U3CencodingU3E__0_4(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U3CpostHeaderU3E__0_5(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U24this_6(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U24current_7(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U24disposing_8(),
	U3CSaveLevelStartEndEventToServer_NewJsonSchemaU3Ec__Iterator8_t2983818842::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[11] = 
{
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of__eventID_0(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U3CpostScoreURLU3E__0_1(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of__levelNum_2(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_savePoint_3(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U3CjsonStringU3E__0_4(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U3CencodingU3E__0_5(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U3CpostHeaderU3E__0_6(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U24this_7(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U24current_8(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U24disposing_9(),
	U3CSavePointEventToServer_NewJsonSchemaU3Ec__Iterator9_t737388940::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[19] = 
{
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__eventID_0(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U3CpostScoreURLU3E__0_1(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__levelNum_2(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q1A_3(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q2A_4(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q3A_5(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q4A_6(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q5A_7(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q6A_8(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q7A_9(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q8A_10(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of__Q9A_11(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U3CjsonStringU3E__0_12(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U3CencodingU3E__0_13(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U3CpostHeaderU3E__0_14(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U24this_15(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U24current_16(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U24disposing_17(),
	U3CSaveUserPHQAEventToServer_NewJsonSchemaU3Ec__IteratorA_t1904098941::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[23] = 
{
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CpostScoreURLU3E__0_0(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of__levelNum_1(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of__feedback_2(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of__savePoint_3(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_bSaveCurrenLevelNotebook_4(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of__bSendNotebookdata_5(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CjsonStringU3E__0_6(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CencodingU3E__0_7(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CpostHeaderU3E__0_8(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CtestU3E__0_9(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CjsonU3E__0_10(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3C_SaveLevelU3E__0_11(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CSavePointU3E__0_12(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CSaveLevelU3E__0_13(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3C_savePointStringU3E__0_14(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CCurrentUserResponseU3E__0_15(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CwriterU3E__1_16(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CreaderU3E__1_17(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U3CcontentU3E__1_18(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U24this_19(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U24current_20(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U24disposing_21(),
	U3CsavePointToServer_NewJsonSchemaU3Ec__IteratorB_t2461306033::get_offset_of_U24PC_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[13] = 
{
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CtestU3E__0_0(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_bOffline_1(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CloadFormU3E__1_2(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3Cm_sLoadURLU3E__1_3(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CjsonU3E__0_4(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CtempValU3E__2_5(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CtempStringU3E__2_6(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CtempBoolU3E__3_7(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U3CtempNumU3E__4_8(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U24this_9(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U24current_10(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U24disposing_11(),
	U3CloadSavePointFromServer_NewJsonSchemaU3Ec__IteratorC_t648798626::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (U3CsubConnect3U3Ec__IteratorD_t1010320540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[9] = 
{
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U3CiU3E__0_0(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U3CpostScoreURLU3E__0_1(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_sJsonString_2(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U3CencodingU3E__0_3(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U3CpostHeaderU3E__0_4(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U24this_5(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U24current_6(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U24disposing_7(),
	U3CsubConnect3U3Ec__IteratorD_t1010320540::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[24] = 
{
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CsaveFormU3E__0_0(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CjsonStringU3E__0_1(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of__levelNum_2(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of__feedback_3(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of__savePoint_4(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_bNewPageNotebook_5(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CencodingU3E__0_6(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CtestU3E__1_7(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CjsonU3E__1_8(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CSavePointU3E__1_9(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CSaveLevelU3E__1_10(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3C_SaveLevelU3E__1_11(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3C_savePointStringU3E__1_12(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CCurrentUserResponseU3E__1_13(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CpostScoreURLU3E__2_14(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CpostHeaderU3E__2_15(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CwwwU3E__2_16(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CwriterU3E__3_17(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CreaderU3E__3_18(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U3CcontentU3E__3_19(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U24this_20(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U24current_21(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U24disposing_22(),
	U3CSaveOnServerOrLocalU3Ec__IteratorE_t2814865380::get_offset_of_U24PC_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (U3CstartFileUploadU3Ec__IteratorF_t3320435443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[11] = 
{
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U3CallLinesU3E__0_0(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U3CreaderU3E__0_1(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U3CallContentU3E__0_2(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U3CcontentU3E__0_3(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24locvar0_4(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24locvar1_5(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U3CjsonStrU3E__1_6(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24this_7(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24current_8(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24disposing_9(),
	U3CstartFileUploadU3Ec__IteratorF_t3320435443::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (U3CWaitU3Ec__Iterator10_t3869735969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[5] = 
{
	U3CWaitU3Ec__Iterator10_t3869735969::get_offset_of_seconds_0(),
	U3CWaitU3Ec__Iterator10_t3869735969::get_offset_of_U24this_1(),
	U3CWaitU3Ec__Iterator10_t3869735969::get_offset_of_U24current_2(),
	U3CWaitU3Ec__Iterator10_t3869735969::get_offset_of_U24disposing_3(),
	U3CWaitU3Ec__Iterator10_t3869735969::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (FontSizeManager_t3303031200), -1, sizeof(FontSizeManager_t3303031200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3099[1] = 
{
	FontSizeManager_t3303031200_StaticFields::get_offset_of_done_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
