﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// LogoBackgroundScreen
struct LogoBackgroundScreen_t3640353420;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1468153686;
// PortalInteraction
struct PortalInteraction_t2393667535;
// System.Void
struct Void_t653366341;
// System.Char[]
struct CharU5BU5D_t3419619864;
// UnityEngine.GameObject
struct GameObject_t2557347079;
// ObjectMovement
struct ObjectMovement_t1497131487;
// UnityEngine.Texture2D
struct Texture2D_t3063074017;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1378084586;
// UnityEngine.Transform
struct Transform_t362059596;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2988620542;
// System.Int32[]
struct Int32U5BU5D_t1965588061;
// UnityEngine.Transform[]
struct TransformU5BU5D_t2383644165;
// UnityEngine.Shader
struct Shader_t1881769421;
// UnityEngine.Renderer
struct Renderer_t1418648713;
// UnityEngine.GUISkin
struct GUISkin_t2122630221;
// CapturedDataInput
struct CapturedDataInput_t2616152122;
// UnityEngine.Texture[]
struct TextureU5BU5D_t152995001;
// UnityEngine.Texture
struct Texture_t2119925672;
// System.String[]
struct StringU5BU5D_t2511808107;
// UnityEngine.Rect[]
struct RectU5BU5D_t141872167;
// System.Boolean[]
struct BooleanU5BU5D_t698278498;
// UnityEngine.Color[]
struct ColorU5BU5D_t247935167;
// TokenHolderLegacy
struct TokenHolderLegacy_t1380075429;
// UnityEngine.AudioClip
struct AudioClip_t1419814565;
// LoadTalkSceneXMLData
struct LoadTalkSceneXMLData_t2381108950;
// UnityEngine.TextAsset
struct TextAsset_t2052027493;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t3624204584;
// UnityEngine.AudioSource
struct AudioSource_t4025721661;
// ShieldMenu
struct ShieldMenu_t3436809847;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t3304433276;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1220531434;
// PushRockMiniGame
struct PushRockMiniGame_t1414473092;
// TalkScenes
struct TalkScenes_t1215051795;
// System.Single[]
struct SingleU5BU5D_t2843050510;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t27653605;
// MoodGraph
struct MoodGraph_t2484840849;
// UnityEngine.Camera
struct Camera_t2839736942;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4049948907_H
#define U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4049948907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogoBackgroundScreen/<restartApplication>c__Iterator0
struct  U3CrestartApplicationU3Ec__Iterator0_t4049948907  : public RuntimeObject
{
public:
	// System.Boolean LogoBackgroundScreen/<restartApplication>c__Iterator0::<running>__0
	bool ___U3CrunningU3E__0_0;
	// System.Boolean LogoBackgroundScreen/<restartApplication>c__Iterator0::bDynamicUrlISNULL
	bool ___bDynamicUrlISNULL_1;
	// LogoBackgroundScreen LogoBackgroundScreen/<restartApplication>c__Iterator0::$this
	LogoBackgroundScreen_t3640353420 * ___U24this_2;
	// System.Object LogoBackgroundScreen/<restartApplication>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LogoBackgroundScreen/<restartApplication>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 LogoBackgroundScreen/<restartApplication>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrunningU3E__0_0() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___U3CrunningU3E__0_0)); }
	inline bool get_U3CrunningU3E__0_0() const { return ___U3CrunningU3E__0_0; }
	inline bool* get_address_of_U3CrunningU3E__0_0() { return &___U3CrunningU3E__0_0; }
	inline void set_U3CrunningU3E__0_0(bool value)
	{
		___U3CrunningU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_bDynamicUrlISNULL_1() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___bDynamicUrlISNULL_1)); }
	inline bool get_bDynamicUrlISNULL_1() const { return ___bDynamicUrlISNULL_1; }
	inline bool* get_address_of_bDynamicUrlISNULL_1() { return &___bDynamicUrlISNULL_1; }
	inline void set_bDynamicUrlISNULL_1(bool value)
	{
		___bDynamicUrlISNULL_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___U24this_2)); }
	inline LogoBackgroundScreen_t3640353420 * get_U24this_2() const { return ___U24this_2; }
	inline LogoBackgroundScreen_t3640353420 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LogoBackgroundScreen_t3640353420 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CrestartApplicationU3Ec__Iterator0_t4049948907, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTARTAPPLICATIONU3EC__ITERATOR0_T4049948907_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef U3CLOADGUIDESCENEU3EC__ITERATOR0_T4090987736_H
#define U3CLOADGUIDESCENEU3EC__ITERATOR0_T4090987736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PortalInteraction/<LoadGuideScene>c__Iterator0
struct  U3CLoadGuideSceneU3Ec__Iterator0_t4090987736  : public RuntimeObject
{
public:
	// System.Single PortalInteraction/<LoadGuideScene>c__Iterator0::<fTimeUntilLoad>__0
	float ___U3CfTimeUntilLoadU3E__0_0;
	// UnityEngine.AsyncOperation PortalInteraction/<LoadGuideScene>c__Iterator0::<op>__0
	AsyncOperation_t1468153686 * ___U3CopU3E__0_1;
	// PortalInteraction PortalInteraction/<LoadGuideScene>c__Iterator0::$this
	PortalInteraction_t2393667535 * ___U24this_2;
	// System.Object PortalInteraction/<LoadGuideScene>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PortalInteraction/<LoadGuideScene>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PortalInteraction/<LoadGuideScene>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CfTimeUntilLoadU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U3CfTimeUntilLoadU3E__0_0)); }
	inline float get_U3CfTimeUntilLoadU3E__0_0() const { return ___U3CfTimeUntilLoadU3E__0_0; }
	inline float* get_address_of_U3CfTimeUntilLoadU3E__0_0() { return &___U3CfTimeUntilLoadU3E__0_0; }
	inline void set_U3CfTimeUntilLoadU3E__0_0(float value)
	{
		___U3CfTimeUntilLoadU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CopU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U3CopU3E__0_1)); }
	inline AsyncOperation_t1468153686 * get_U3CopU3E__0_1() const { return ___U3CopU3E__0_1; }
	inline AsyncOperation_t1468153686 ** get_address_of_U3CopU3E__0_1() { return &___U3CopU3E__0_1; }
	inline void set_U3CopU3E__0_1(AsyncOperation_t1468153686 * value)
	{
		___U3CopU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U24this_2)); }
	inline PortalInteraction_t2393667535 * get_U24this_2() const { return ___U24this_2; }
	inline PortalInteraction_t2393667535 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PortalInteraction_t2393667535 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadGuideSceneU3Ec__Iterator0_t4090987736, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADGUIDESCENEU3EC__ITERATOR0_T4090987736_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef COLOR_T2582018970_H
#define COLOR_T2582018970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2582018970 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2582018970, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2582018970_H
#ifndef RECT_T3039462994_H
#define RECT_T3039462994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3039462994 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3039462994, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3039462994_H
#ifndef VECTOR3_T1986933152_H
#define VECTOR3_T1986933152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t1986933152 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t1986933152, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t1986933152_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t1986933152  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t1986933152  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t1986933152  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t1986933152  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t1986933152  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t1986933152  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t1986933152  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t1986933152  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t1986933152  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t1986933152  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___zeroVector_4)); }
	inline Vector3_t1986933152  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t1986933152 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t1986933152  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___oneVector_5)); }
	inline Vector3_t1986933152  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t1986933152 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t1986933152  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___upVector_6)); }
	inline Vector3_t1986933152  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t1986933152 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t1986933152  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___downVector_7)); }
	inline Vector3_t1986933152  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t1986933152 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t1986933152  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___leftVector_8)); }
	inline Vector3_t1986933152  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t1986933152 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t1986933152  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___rightVector_9)); }
	inline Vector3_t1986933152  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t1986933152 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t1986933152  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___forwardVector_10)); }
	inline Vector3_t1986933152  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t1986933152 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t1986933152  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___backVector_11)); }
	inline Vector3_t1986933152  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t1986933152 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t1986933152  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t1986933152  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t1986933152 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t1986933152  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t1986933152_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t1986933152  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t1986933152 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t1986933152  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T1986933152_H
#ifndef ICEMELTROUTINE_T2599549131_H
#define ICEMELTROUTINE_T2599549131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceMeltRoutine
struct  IceMeltRoutine_t2599549131 
{
public:
	// System.Int32 IceMeltRoutine::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IceMeltRoutine_t2599549131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICEMELTROUTINE_T2599549131_H
#ifndef U3CROTATEASCOROUTINEU3EC__ITERATOR0_T2189652608_H
#define U3CROTATEASCOROUTINEU3EC__ITERATOR0_T2189652608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectMovement/<rotateAsCoroutine>c__Iterator0
struct  U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608  : public RuntimeObject
{
public:
	// UnityEngine.GameObject ObjectMovement/<rotateAsCoroutine>c__Iterator0::go
	GameObject_t2557347079 * ___go_0;
	// UnityEngine.Vector3 ObjectMovement/<rotateAsCoroutine>c__Iterator0::finalRotation
	Vector3_t1986933152  ___finalRotation_1;
	// System.Single ObjectMovement/<rotateAsCoroutine>c__Iterator0::<Angle>__0
	float ___U3CAngleU3E__0_2;
	// ObjectMovement ObjectMovement/<rotateAsCoroutine>c__Iterator0::$this
	ObjectMovement_t1497131487 * ___U24this_3;
	// System.Object ObjectMovement/<rotateAsCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ObjectMovement/<rotateAsCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ObjectMovement/<rotateAsCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___go_0)); }
	inline GameObject_t2557347079 * get_go_0() const { return ___go_0; }
	inline GameObject_t2557347079 ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_t2557347079 * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_finalRotation_1() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___finalRotation_1)); }
	inline Vector3_t1986933152  get_finalRotation_1() const { return ___finalRotation_1; }
	inline Vector3_t1986933152 * get_address_of_finalRotation_1() { return &___finalRotation_1; }
	inline void set_finalRotation_1(Vector3_t1986933152  value)
	{
		___finalRotation_1 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3E__0_2() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___U3CAngleU3E__0_2)); }
	inline float get_U3CAngleU3E__0_2() const { return ___U3CAngleU3E__0_2; }
	inline float* get_address_of_U3CAngleU3E__0_2() { return &___U3CAngleU3E__0_2; }
	inline void set_U3CAngleU3E__0_2(float value)
	{
		___U3CAngleU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___U24this_3)); }
	inline ObjectMovement_t1497131487 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectMovement_t1497131487 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectMovement_t1497131487 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CROTATEASCOROUTINEU3EC__ITERATOR0_T2189652608_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef MONOBEHAVIOUR_T1618594486_H
#define MONOBEHAVIOUR_T1618594486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1618594486  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1618594486_H
#ifndef RIGHTDOORTRIGGERENTER_T3471759167_H
#define RIGHTDOORTRIGGERENTER_T3471759167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RightDoorTriggerEnter
struct  RightDoorTriggerEnter_t3471759167  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean RightDoorTriggerEnter::m_rightDoorTriggerEntered
	bool ___m_rightDoorTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_rightDoorTriggerEntered_2() { return static_cast<int32_t>(offsetof(RightDoorTriggerEnter_t3471759167, ___m_rightDoorTriggerEntered_2)); }
	inline bool get_m_rightDoorTriggerEntered_2() const { return ___m_rightDoorTriggerEntered_2; }
	inline bool* get_address_of_m_rightDoorTriggerEntered_2() { return &___m_rightDoorTriggerEntered_2; }
	inline void set_m_rightDoorTriggerEntered_2(bool value)
	{
		___m_rightDoorTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTDOORTRIGGERENTER_T3471759167_H
#ifndef CAVEFIREINTERACTION_T3365142469_H
#define CAVEFIREINTERACTION_T3365142469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaveFireInteraction
struct  CaveFireInteraction_t3365142469  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Texture2D CaveFireInteraction::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_2;
	// UnityEngine.Texture2D CaveFireInteraction::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_3;
	// System.Boolean CaveFireInteraction::StartToLight
	bool ___StartToLight_4;
	// System.Boolean CaveFireInteraction::Lighted
	bool ___Lighted_5;

public:
	inline static int32_t get_offset_of_MouseOverTexture_2() { return static_cast<int32_t>(offsetof(CaveFireInteraction_t3365142469, ___MouseOverTexture_2)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_2() const { return ___MouseOverTexture_2; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_2() { return &___MouseOverTexture_2; }
	inline void set_MouseOverTexture_2(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_2), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_3() { return static_cast<int32_t>(offsetof(CaveFireInteraction_t3365142469, ___MouseOriTexture_3)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_3() const { return ___MouseOriTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_3() { return &___MouseOriTexture_3; }
	inline void set_MouseOriTexture_3(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_3), value);
	}

	inline static int32_t get_offset_of_StartToLight_4() { return static_cast<int32_t>(offsetof(CaveFireInteraction_t3365142469, ___StartToLight_4)); }
	inline bool get_StartToLight_4() const { return ___StartToLight_4; }
	inline bool* get_address_of_StartToLight_4() { return &___StartToLight_4; }
	inline void set_StartToLight_4(bool value)
	{
		___StartToLight_4 = value;
	}

	inline static int32_t get_offset_of_Lighted_5() { return static_cast<int32_t>(offsetof(CaveFireInteraction_t3365142469, ___Lighted_5)); }
	inline bool get_Lighted_5() const { return ___Lighted_5; }
	inline bool* get_address_of_Lighted_5() { return &___Lighted_5; }
	inline void set_Lighted_5(bool value)
	{
		___Lighted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAVEFIREINTERACTION_T3365142469_H
#ifndef TUITERMINATESCENE_T962315422_H
#define TUITERMINATESCENE_T962315422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TuiTerminateScene
struct  TuiTerminateScene_t962315422  : public MonoBehaviour_t1618594486
{
public:
	// System.Single TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// System.Single TuiTerminateScene::tuiToPlayerDistance
	float ___tuiToPlayerDistance_3;
	// System.Boolean TuiTerminateScene::m_bTuiIsFlying
	bool ___m_bTuiIsFlying_4;
	// UnityEngine.GameObject TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_5;
	// System.String TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_6;
	// System.Boolean TuiTerminateScene::m_bTuiHasToWaitPointOne
	bool ___m_bTuiHasToWaitPointOne_7;
	// System.Boolean TuiTerminateScene::m_bTuiFirstStandPointArrived
	bool ___m_bTuiFirstStandPointArrived_8;
	// System.Boolean TuiTerminateScene::m_bTuiHasToWaitPointTwo
	bool ___m_bTuiHasToWaitPointTwo_9;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_tuiToPlayerDistance_3() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___tuiToPlayerDistance_3)); }
	inline float get_tuiToPlayerDistance_3() const { return ___tuiToPlayerDistance_3; }
	inline float* get_address_of_tuiToPlayerDistance_3() { return &___tuiToPlayerDistance_3; }
	inline void set_tuiToPlayerDistance_3(float value)
	{
		___tuiToPlayerDistance_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiIsFlying_4() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___m_bTuiIsFlying_4)); }
	inline bool get_m_bTuiIsFlying_4() const { return ___m_bTuiIsFlying_4; }
	inline bool* get_address_of_m_bTuiIsFlying_4() { return &___m_bTuiIsFlying_4; }
	inline void set_m_bTuiIsFlying_4(bool value)
	{
		___m_bTuiIsFlying_4 = value;
	}

	inline static int32_t get_offset_of_interactObject_5() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___interactObject_5)); }
	inline GameObject_t2557347079 * get_interactObject_5() const { return ___interactObject_5; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_5() { return &___interactObject_5; }
	inline void set_interactObject_5(GameObject_t2557347079 * value)
	{
		___interactObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_5), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_6() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___tuiFlyHeadToID_6)); }
	inline String_t* get_tuiFlyHeadToID_6() const { return ___tuiFlyHeadToID_6; }
	inline String_t** get_address_of_tuiFlyHeadToID_6() { return &___tuiFlyHeadToID_6; }
	inline void set_tuiFlyHeadToID_6(String_t* value)
	{
		___tuiFlyHeadToID_6 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_6), value);
	}

	inline static int32_t get_offset_of_m_bTuiHasToWaitPointOne_7() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___m_bTuiHasToWaitPointOne_7)); }
	inline bool get_m_bTuiHasToWaitPointOne_7() const { return ___m_bTuiHasToWaitPointOne_7; }
	inline bool* get_address_of_m_bTuiHasToWaitPointOne_7() { return &___m_bTuiHasToWaitPointOne_7; }
	inline void set_m_bTuiHasToWaitPointOne_7(bool value)
	{
		___m_bTuiHasToWaitPointOne_7 = value;
	}

	inline static int32_t get_offset_of_m_bTuiFirstStandPointArrived_8() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___m_bTuiFirstStandPointArrived_8)); }
	inline bool get_m_bTuiFirstStandPointArrived_8() const { return ___m_bTuiFirstStandPointArrived_8; }
	inline bool* get_address_of_m_bTuiFirstStandPointArrived_8() { return &___m_bTuiFirstStandPointArrived_8; }
	inline void set_m_bTuiFirstStandPointArrived_8(bool value)
	{
		___m_bTuiFirstStandPointArrived_8 = value;
	}

	inline static int32_t get_offset_of_m_bTuiHasToWaitPointTwo_9() { return static_cast<int32_t>(offsetof(TuiTerminateScene_t962315422, ___m_bTuiHasToWaitPointTwo_9)); }
	inline bool get_m_bTuiHasToWaitPointTwo_9() const { return ___m_bTuiHasToWaitPointTwo_9; }
	inline bool* get_address_of_m_bTuiHasToWaitPointTwo_9() { return &___m_bTuiHasToWaitPointTwo_9; }
	inline void set_m_bTuiHasToWaitPointTwo_9(bool value)
	{
		___m_bTuiHasToWaitPointTwo_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUITERMINATESCENE_T962315422_H
#ifndef L7TUITERMINATESCENE_T3027460065_H
#define L7TUITERMINATESCENE_T3027460065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7TuiTerminateScene
struct  L7TuiTerminateScene_t3027460065  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L7TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// UnityEngine.GameObject L7TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// System.String L7TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_4;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L7TuiTerminateScene_t3027460065, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(L7TuiTerminateScene_t3027460065, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_4() { return static_cast<int32_t>(offsetof(L7TuiTerminateScene_t3027460065, ___tuiFlyHeadToID_4)); }
	inline String_t* get_tuiFlyHeadToID_4() const { return ___tuiFlyHeadToID_4; }
	inline String_t** get_address_of_tuiFlyHeadToID_4() { return &___tuiFlyHeadToID_4; }
	inline void set_tuiFlyHeadToID_4(String_t* value)
	{
		___tuiFlyHeadToID_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7TUITERMINATESCENE_T3027460065_H
#ifndef L7DETECTTUIFLYDOWN_T382364299_H
#define L7DETECTTUIFLYDOWN_T382364299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7DetectTuiFlyDown
struct  L7DetectTuiFlyDown_t382364299  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L7DetectTuiFlyDown::m_fDoorPlayerDis
	float ___m_fDoorPlayerDis_2;
	// System.Single L7DetectTuiFlyDown::m_tuiNeedtoFlydownDis
	float ___m_tuiNeedtoFlydownDis_3;
	// System.Boolean L7DetectTuiFlyDown::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_4;
	// System.Boolean L7DetectTuiFlyDown::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_5;
	// System.String L7DetectTuiFlyDown::m_sTuiFlyToPoint
	String_t* ___m_sTuiFlyToPoint_6;

public:
	inline static int32_t get_offset_of_m_fDoorPlayerDis_2() { return static_cast<int32_t>(offsetof(L7DetectTuiFlyDown_t382364299, ___m_fDoorPlayerDis_2)); }
	inline float get_m_fDoorPlayerDis_2() const { return ___m_fDoorPlayerDis_2; }
	inline float* get_address_of_m_fDoorPlayerDis_2() { return &___m_fDoorPlayerDis_2; }
	inline void set_m_fDoorPlayerDis_2(float value)
	{
		___m_fDoorPlayerDis_2 = value;
	}

	inline static int32_t get_offset_of_m_tuiNeedtoFlydownDis_3() { return static_cast<int32_t>(offsetof(L7DetectTuiFlyDown_t382364299, ___m_tuiNeedtoFlydownDis_3)); }
	inline float get_m_tuiNeedtoFlydownDis_3() const { return ___m_tuiNeedtoFlydownDis_3; }
	inline float* get_address_of_m_tuiNeedtoFlydownDis_3() { return &___m_tuiNeedtoFlydownDis_3; }
	inline void set_m_tuiNeedtoFlydownDis_3(float value)
	{
		___m_tuiNeedtoFlydownDis_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationTriggered_4() { return static_cast<int32_t>(offsetof(L7DetectTuiFlyDown_t382364299, ___m_bTuiConversationTriggered_4)); }
	inline bool get_m_bTuiConversationTriggered_4() const { return ___m_bTuiConversationTriggered_4; }
	inline bool* get_address_of_m_bTuiConversationTriggered_4() { return &___m_bTuiConversationTriggered_4; }
	inline void set_m_bTuiConversationTriggered_4(bool value)
	{
		___m_bTuiConversationTriggered_4 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_5() { return static_cast<int32_t>(offsetof(L7DetectTuiFlyDown_t382364299, ___m_bFirstConversationFinished_5)); }
	inline bool get_m_bFirstConversationFinished_5() const { return ___m_bFirstConversationFinished_5; }
	inline bool* get_address_of_m_bFirstConversationFinished_5() { return &___m_bFirstConversationFinished_5; }
	inline void set_m_bFirstConversationFinished_5(bool value)
	{
		___m_bFirstConversationFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_sTuiFlyToPoint_6() { return static_cast<int32_t>(offsetof(L7DetectTuiFlyDown_t382364299, ___m_sTuiFlyToPoint_6)); }
	inline String_t* get_m_sTuiFlyToPoint_6() const { return ___m_sTuiFlyToPoint_6; }
	inline String_t** get_address_of_m_sTuiFlyToPoint_6() { return &___m_sTuiFlyToPoint_6; }
	inline void set_m_sTuiFlyToPoint_6(String_t* value)
	{
		___m_sTuiFlyToPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTuiFlyToPoint_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7DETECTTUIFLYDOWN_T382364299_H
#ifndef L6TUIWOMANBRIDGELANDSWITCH_T1546825341_H
#define L6TUIWOMANBRIDGELANDSWITCH_T1546825341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6TuiWomanBridgelandSwitch
struct  L6TuiWomanBridgelandSwitch_t1546825341  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L6TuiWomanBridgelandSwitch::m_bTuiTalkCanStart
	bool ___m_bTuiTalkCanStart_2;

public:
	inline static int32_t get_offset_of_m_bTuiTalkCanStart_2() { return static_cast<int32_t>(offsetof(L6TuiWomanBridgelandSwitch_t1546825341, ___m_bTuiTalkCanStart_2)); }
	inline bool get_m_bTuiTalkCanStart_2() const { return ___m_bTuiTalkCanStart_2; }
	inline bool* get_address_of_m_bTuiTalkCanStart_2() { return &___m_bTuiTalkCanStart_2; }
	inline void set_m_bTuiTalkCanStart_2(bool value)
	{
		___m_bTuiTalkCanStart_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6TUIWOMANBRIDGELANDSWITCH_T1546825341_H
#ifndef L6TUITERMINATESCENE_T4160061475_H
#define L6TUITERMINATESCENE_T4160061475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6TuiTerminateScene
struct  L6TuiTerminateScene_t4160061475  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L6TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// UnityEngine.GameObject L6TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// System.String L6TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_4;
	// System.Boolean L6TuiTerminateScene::m_bFirstPartWalkingFinished
	bool ___m_bFirstPartWalkingFinished_5;
	// System.Boolean L6TuiTerminateScene::m_bSecondpartWalkingFinished
	bool ___m_bSecondpartWalkingFinished_6;
	// System.Boolean L6TuiTerminateScene::m_bTuiFliedAway
	bool ___m_bTuiFliedAway_7;
	// System.Boolean L6TuiTerminateScene::m_bcallOnceTrigger
	bool ___m_bcallOnceTrigger_8;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_4() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___tuiFlyHeadToID_4)); }
	inline String_t* get_tuiFlyHeadToID_4() const { return ___tuiFlyHeadToID_4; }
	inline String_t** get_address_of_tuiFlyHeadToID_4() { return &___tuiFlyHeadToID_4; }
	inline void set_tuiFlyHeadToID_4(String_t* value)
	{
		___tuiFlyHeadToID_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_4), value);
	}

	inline static int32_t get_offset_of_m_bFirstPartWalkingFinished_5() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___m_bFirstPartWalkingFinished_5)); }
	inline bool get_m_bFirstPartWalkingFinished_5() const { return ___m_bFirstPartWalkingFinished_5; }
	inline bool* get_address_of_m_bFirstPartWalkingFinished_5() { return &___m_bFirstPartWalkingFinished_5; }
	inline void set_m_bFirstPartWalkingFinished_5(bool value)
	{
		___m_bFirstPartWalkingFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_bSecondpartWalkingFinished_6() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___m_bSecondpartWalkingFinished_6)); }
	inline bool get_m_bSecondpartWalkingFinished_6() const { return ___m_bSecondpartWalkingFinished_6; }
	inline bool* get_address_of_m_bSecondpartWalkingFinished_6() { return &___m_bSecondpartWalkingFinished_6; }
	inline void set_m_bSecondpartWalkingFinished_6(bool value)
	{
		___m_bSecondpartWalkingFinished_6 = value;
	}

	inline static int32_t get_offset_of_m_bTuiFliedAway_7() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___m_bTuiFliedAway_7)); }
	inline bool get_m_bTuiFliedAway_7() const { return ___m_bTuiFliedAway_7; }
	inline bool* get_address_of_m_bTuiFliedAway_7() { return &___m_bTuiFliedAway_7; }
	inline void set_m_bTuiFliedAway_7(bool value)
	{
		___m_bTuiFliedAway_7 = value;
	}

	inline static int32_t get_offset_of_m_bcallOnceTrigger_8() { return static_cast<int32_t>(offsetof(L6TuiTerminateScene_t4160061475, ___m_bcallOnceTrigger_8)); }
	inline bool get_m_bcallOnceTrigger_8() const { return ___m_bcallOnceTrigger_8; }
	inline bool* get_address_of_m_bcallOnceTrigger_8() { return &___m_bcallOnceTrigger_8; }
	inline void set_m_bcallOnceTrigger_8(bool value)
	{
		___m_bcallOnceTrigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6TUITERMINATESCENE_T4160061475_H
#ifndef L6TUITEMPLEGUARDIANSWITCH_T4249979332_H
#define L6TUITEMPLEGUARDIANSWITCH_T4249979332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6TuiTempleGuardianSwitch
struct  L6TuiTempleGuardianSwitch_t4249979332  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L6TuiTempleGuardianSwitch::m_bTuiTalkCanStart
	bool ___m_bTuiTalkCanStart_2;

public:
	inline static int32_t get_offset_of_m_bTuiTalkCanStart_2() { return static_cast<int32_t>(offsetof(L6TuiTempleGuardianSwitch_t4249979332, ___m_bTuiTalkCanStart_2)); }
	inline bool get_m_bTuiTalkCanStart_2() const { return ___m_bTuiTalkCanStart_2; }
	inline bool* get_address_of_m_bTuiTalkCanStart_2() { return &___m_bTuiTalkCanStart_2; }
	inline void set_m_bTuiTalkCanStart_2(bool value)
	{
		___m_bTuiTalkCanStart_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6TUITEMPLEGUARDIANSWITCH_T4249979332_H
#ifndef L6DETECTTUIFLYDOWN_T4267688540_H
#define L6DETECTTUIFLYDOWN_T4267688540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6DetectTuiFlyDown
struct  L6DetectTuiFlyDown_t4267688540  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L6DetectTuiFlyDown::m_fDoorPlayerDis
	float ___m_fDoorPlayerDis_2;
	// System.Single L6DetectTuiFlyDown::m_tuiNeedtoFlydownDis
	float ___m_tuiNeedtoFlydownDis_3;
	// System.Boolean L6DetectTuiFlyDown::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_4;
	// System.Boolean L6DetectTuiFlyDown::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_5;
	// System.String L6DetectTuiFlyDown::m_sTuiFlyToPoint
	String_t* ___m_sTuiFlyToPoint_6;

public:
	inline static int32_t get_offset_of_m_fDoorPlayerDis_2() { return static_cast<int32_t>(offsetof(L6DetectTuiFlyDown_t4267688540, ___m_fDoorPlayerDis_2)); }
	inline float get_m_fDoorPlayerDis_2() const { return ___m_fDoorPlayerDis_2; }
	inline float* get_address_of_m_fDoorPlayerDis_2() { return &___m_fDoorPlayerDis_2; }
	inline void set_m_fDoorPlayerDis_2(float value)
	{
		___m_fDoorPlayerDis_2 = value;
	}

	inline static int32_t get_offset_of_m_tuiNeedtoFlydownDis_3() { return static_cast<int32_t>(offsetof(L6DetectTuiFlyDown_t4267688540, ___m_tuiNeedtoFlydownDis_3)); }
	inline float get_m_tuiNeedtoFlydownDis_3() const { return ___m_tuiNeedtoFlydownDis_3; }
	inline float* get_address_of_m_tuiNeedtoFlydownDis_3() { return &___m_tuiNeedtoFlydownDis_3; }
	inline void set_m_tuiNeedtoFlydownDis_3(float value)
	{
		___m_tuiNeedtoFlydownDis_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationTriggered_4() { return static_cast<int32_t>(offsetof(L6DetectTuiFlyDown_t4267688540, ___m_bTuiConversationTriggered_4)); }
	inline bool get_m_bTuiConversationTriggered_4() const { return ___m_bTuiConversationTriggered_4; }
	inline bool* get_address_of_m_bTuiConversationTriggered_4() { return &___m_bTuiConversationTriggered_4; }
	inline void set_m_bTuiConversationTriggered_4(bool value)
	{
		___m_bTuiConversationTriggered_4 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_5() { return static_cast<int32_t>(offsetof(L6DetectTuiFlyDown_t4267688540, ___m_bFirstConversationFinished_5)); }
	inline bool get_m_bFirstConversationFinished_5() const { return ___m_bFirstConversationFinished_5; }
	inline bool* get_address_of_m_bFirstConversationFinished_5() { return &___m_bFirstConversationFinished_5; }
	inline void set_m_bFirstConversationFinished_5(bool value)
	{
		___m_bFirstConversationFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_sTuiFlyToPoint_6() { return static_cast<int32_t>(offsetof(L6DetectTuiFlyDown_t4267688540, ___m_sTuiFlyToPoint_6)); }
	inline String_t* get_m_sTuiFlyToPoint_6() const { return ___m_sTuiFlyToPoint_6; }
	inline String_t** get_address_of_m_sTuiFlyToPoint_6() { return &___m_sTuiFlyToPoint_6; }
	inline void set_m_sTuiFlyToPoint_6(String_t* value)
	{
		___m_sTuiFlyToPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTuiFlyToPoint_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6DETECTTUIFLYDOWN_T4267688540_H
#ifndef L4TUITERMINATESCENE_T788386278_H
#define L4TUITERMINATESCENE_T788386278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4TuiTerminateScene
struct  L4TuiTerminateScene_t788386278  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L4TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// UnityEngine.GameObject L4TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// System.String L4TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_4;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L4TuiTerminateScene_t788386278, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(L4TuiTerminateScene_t788386278, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_4() { return static_cast<int32_t>(offsetof(L4TuiTerminateScene_t788386278, ___tuiFlyHeadToID_4)); }
	inline String_t* get_tuiFlyHeadToID_4() const { return ___tuiFlyHeadToID_4; }
	inline String_t** get_address_of_tuiFlyHeadToID_4() { return &___tuiFlyHeadToID_4; }
	inline void set_tuiFlyHeadToID_4(String_t* value)
	{
		___tuiFlyHeadToID_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_4), value);
	}
};

struct L4TuiTerminateScene_t788386278_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L4TuiTerminateScene::<>f__switch$mapD
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapD_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_5() { return static_cast<int32_t>(offsetof(L4TuiTerminateScene_t788386278_StaticFields, ___U3CU3Ef__switchU24mapD_5)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapD_5() const { return ___U3CU3Ef__switchU24mapD_5; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapD_5() { return &___U3CU3Ef__switchU24mapD_5; }
	inline void set_U3CU3Ef__switchU24mapD_5(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapD_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapD_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4TUITERMINATESCENE_T788386278_H
#ifndef CHARACTERINTERACTION_T1587752913_H
#define CHARACTERINTERACTION_T1587752913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterInteraction
struct  CharacterInteraction_t1587752913  : public MonoBehaviour_t1618594486
{
public:
	// System.Single CharacterInteraction::InteractionRadius
	float ___InteractionRadius_2;
	// System.Single CharacterInteraction::m_fFaceToFaceTalkDistance
	float ___m_fFaceToFaceTalkDistance_3;
	// System.Single CharacterInteraction::m_fPlayerNPCDistance
	float ___m_fPlayerNPCDistance_4;
	// System.Boolean CharacterInteraction::IsAutomatic
	bool ___IsAutomatic_5;
	// System.Boolean CharacterInteraction::Interacting
	bool ___Interacting_6;
	// System.Boolean CharacterInteraction::privateInteracted
	bool ___privateInteracted_7;
	// System.Single CharacterInteraction::m_fCassDistance
	float ___m_fCassDistance_8;
	// System.Boolean CharacterInteraction::m_bHopeIsFlyingL2
	bool ___m_bHopeIsFlyingL2_9;
	// System.Boolean CharacterInteraction::wasInteracting
	bool ___wasInteracting_10;

public:
	inline static int32_t get_offset_of_InteractionRadius_2() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___InteractionRadius_2)); }
	inline float get_InteractionRadius_2() const { return ___InteractionRadius_2; }
	inline float* get_address_of_InteractionRadius_2() { return &___InteractionRadius_2; }
	inline void set_InteractionRadius_2(float value)
	{
		___InteractionRadius_2 = value;
	}

	inline static int32_t get_offset_of_m_fFaceToFaceTalkDistance_3() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___m_fFaceToFaceTalkDistance_3)); }
	inline float get_m_fFaceToFaceTalkDistance_3() const { return ___m_fFaceToFaceTalkDistance_3; }
	inline float* get_address_of_m_fFaceToFaceTalkDistance_3() { return &___m_fFaceToFaceTalkDistance_3; }
	inline void set_m_fFaceToFaceTalkDistance_3(float value)
	{
		___m_fFaceToFaceTalkDistance_3 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerNPCDistance_4() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___m_fPlayerNPCDistance_4)); }
	inline float get_m_fPlayerNPCDistance_4() const { return ___m_fPlayerNPCDistance_4; }
	inline float* get_address_of_m_fPlayerNPCDistance_4() { return &___m_fPlayerNPCDistance_4; }
	inline void set_m_fPlayerNPCDistance_4(float value)
	{
		___m_fPlayerNPCDistance_4 = value;
	}

	inline static int32_t get_offset_of_IsAutomatic_5() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___IsAutomatic_5)); }
	inline bool get_IsAutomatic_5() const { return ___IsAutomatic_5; }
	inline bool* get_address_of_IsAutomatic_5() { return &___IsAutomatic_5; }
	inline void set_IsAutomatic_5(bool value)
	{
		___IsAutomatic_5 = value;
	}

	inline static int32_t get_offset_of_Interacting_6() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___Interacting_6)); }
	inline bool get_Interacting_6() const { return ___Interacting_6; }
	inline bool* get_address_of_Interacting_6() { return &___Interacting_6; }
	inline void set_Interacting_6(bool value)
	{
		___Interacting_6 = value;
	}

	inline static int32_t get_offset_of_privateInteracted_7() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___privateInteracted_7)); }
	inline bool get_privateInteracted_7() const { return ___privateInteracted_7; }
	inline bool* get_address_of_privateInteracted_7() { return &___privateInteracted_7; }
	inline void set_privateInteracted_7(bool value)
	{
		___privateInteracted_7 = value;
	}

	inline static int32_t get_offset_of_m_fCassDistance_8() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___m_fCassDistance_8)); }
	inline float get_m_fCassDistance_8() const { return ___m_fCassDistance_8; }
	inline float* get_address_of_m_fCassDistance_8() { return &___m_fCassDistance_8; }
	inline void set_m_fCassDistance_8(float value)
	{
		___m_fCassDistance_8 = value;
	}

	inline static int32_t get_offset_of_m_bHopeIsFlyingL2_9() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___m_bHopeIsFlyingL2_9)); }
	inline bool get_m_bHopeIsFlyingL2_9() const { return ___m_bHopeIsFlyingL2_9; }
	inline bool* get_address_of_m_bHopeIsFlyingL2_9() { return &___m_bHopeIsFlyingL2_9; }
	inline void set_m_bHopeIsFlyingL2_9(bool value)
	{
		___m_bHopeIsFlyingL2_9 = value;
	}

	inline static int32_t get_offset_of_wasInteracting_10() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913, ___wasInteracting_10)); }
	inline bool get_wasInteracting_10() const { return ___wasInteracting_10; }
	inline bool* get_address_of_wasInteracting_10() { return &___wasInteracting_10; }
	inline void set_wasInteracting_10(bool value)
	{
		___wasInteracting_10 = value;
	}
};

struct CharacterInteraction_t1587752913_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CharacterInteraction::<>f__switch$mapE
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapE_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_11() { return static_cast<int32_t>(offsetof(CharacterInteraction_t1587752913_StaticFields, ___U3CU3Ef__switchU24mapE_11)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapE_11() const { return ___U3CU3Ef__switchU24mapE_11; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapE_11() { return &___U3CU3Ef__switchU24mapE_11; }
	inline void set_U3CU3Ef__switchU24mapE_11(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapE_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapE_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERINTERACTION_T1587752913_H
#ifndef L4DETECTTUIFLYDOWN_T822593849_H
#define L4DETECTTUIFLYDOWN_T822593849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4DetectTuiFlyDown
struct  L4DetectTuiFlyDown_t822593849  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L4DetectTuiFlyDown::m_bFisConversationTriged
	bool ___m_bFisConversationTriged_2;
	// System.Boolean L4DetectTuiFlyDown::m_bFisConversationFinished
	bool ___m_bFisConversationFinished_3;
	// System.Boolean L4DetectTuiFlyDown::m_bOnCliffConversationTriged
	bool ___m_bOnCliffConversationTriged_4;
	// System.Single L4DetectTuiFlyDown::m_fCorrectTriggerPlayerDis
	float ___m_fCorrectTriggerPlayerDis_5;
	// System.Single L4DetectTuiFlyDown::m_fInCorrectTriggerPlayerDis
	float ___m_fInCorrectTriggerPlayerDis_6;
	// System.Single L4DetectTuiFlyDown::m_fAllowTrigerDis
	float ___m_fAllowTrigerDis_7;
	// System.String L4DetectTuiFlyDown::m_sTuiOnCliffFlyToPoint
	String_t* ___m_sTuiOnCliffFlyToPoint_8;
	// System.Boolean L4DetectTuiFlyDown::m_bTuiSratFlying
	bool ___m_bTuiSratFlying_9;
	// System.String L4DetectTuiFlyDown::m_sTuiSceneName
	String_t* ___m_sTuiSceneName_10;

public:
	inline static int32_t get_offset_of_m_bFisConversationTriged_2() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_bFisConversationTriged_2)); }
	inline bool get_m_bFisConversationTriged_2() const { return ___m_bFisConversationTriged_2; }
	inline bool* get_address_of_m_bFisConversationTriged_2() { return &___m_bFisConversationTriged_2; }
	inline void set_m_bFisConversationTriged_2(bool value)
	{
		___m_bFisConversationTriged_2 = value;
	}

	inline static int32_t get_offset_of_m_bFisConversationFinished_3() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_bFisConversationFinished_3)); }
	inline bool get_m_bFisConversationFinished_3() const { return ___m_bFisConversationFinished_3; }
	inline bool* get_address_of_m_bFisConversationFinished_3() { return &___m_bFisConversationFinished_3; }
	inline void set_m_bFisConversationFinished_3(bool value)
	{
		___m_bFisConversationFinished_3 = value;
	}

	inline static int32_t get_offset_of_m_bOnCliffConversationTriged_4() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_bOnCliffConversationTriged_4)); }
	inline bool get_m_bOnCliffConversationTriged_4() const { return ___m_bOnCliffConversationTriged_4; }
	inline bool* get_address_of_m_bOnCliffConversationTriged_4() { return &___m_bOnCliffConversationTriged_4; }
	inline void set_m_bOnCliffConversationTriged_4(bool value)
	{
		___m_bOnCliffConversationTriged_4 = value;
	}

	inline static int32_t get_offset_of_m_fCorrectTriggerPlayerDis_5() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_fCorrectTriggerPlayerDis_5)); }
	inline float get_m_fCorrectTriggerPlayerDis_5() const { return ___m_fCorrectTriggerPlayerDis_5; }
	inline float* get_address_of_m_fCorrectTriggerPlayerDis_5() { return &___m_fCorrectTriggerPlayerDis_5; }
	inline void set_m_fCorrectTriggerPlayerDis_5(float value)
	{
		___m_fCorrectTriggerPlayerDis_5 = value;
	}

	inline static int32_t get_offset_of_m_fInCorrectTriggerPlayerDis_6() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_fInCorrectTriggerPlayerDis_6)); }
	inline float get_m_fInCorrectTriggerPlayerDis_6() const { return ___m_fInCorrectTriggerPlayerDis_6; }
	inline float* get_address_of_m_fInCorrectTriggerPlayerDis_6() { return &___m_fInCorrectTriggerPlayerDis_6; }
	inline void set_m_fInCorrectTriggerPlayerDis_6(float value)
	{
		___m_fInCorrectTriggerPlayerDis_6 = value;
	}

	inline static int32_t get_offset_of_m_fAllowTrigerDis_7() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_fAllowTrigerDis_7)); }
	inline float get_m_fAllowTrigerDis_7() const { return ___m_fAllowTrigerDis_7; }
	inline float* get_address_of_m_fAllowTrigerDis_7() { return &___m_fAllowTrigerDis_7; }
	inline void set_m_fAllowTrigerDis_7(float value)
	{
		___m_fAllowTrigerDis_7 = value;
	}

	inline static int32_t get_offset_of_m_sTuiOnCliffFlyToPoint_8() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_sTuiOnCliffFlyToPoint_8)); }
	inline String_t* get_m_sTuiOnCliffFlyToPoint_8() const { return ___m_sTuiOnCliffFlyToPoint_8; }
	inline String_t** get_address_of_m_sTuiOnCliffFlyToPoint_8() { return &___m_sTuiOnCliffFlyToPoint_8; }
	inline void set_m_sTuiOnCliffFlyToPoint_8(String_t* value)
	{
		___m_sTuiOnCliffFlyToPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTuiOnCliffFlyToPoint_8), value);
	}

	inline static int32_t get_offset_of_m_bTuiSratFlying_9() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_bTuiSratFlying_9)); }
	inline bool get_m_bTuiSratFlying_9() const { return ___m_bTuiSratFlying_9; }
	inline bool* get_address_of_m_bTuiSratFlying_9() { return &___m_bTuiSratFlying_9; }
	inline void set_m_bTuiSratFlying_9(bool value)
	{
		___m_bTuiSratFlying_9 = value;
	}

	inline static int32_t get_offset_of_m_sTuiSceneName_10() { return static_cast<int32_t>(offsetof(L4DetectTuiFlyDown_t822593849, ___m_sTuiSceneName_10)); }
	inline String_t* get_m_sTuiSceneName_10() const { return ___m_sTuiSceneName_10; }
	inline String_t** get_address_of_m_sTuiSceneName_10() { return &___m_sTuiSceneName_10; }
	inline void set_m_sTuiSceneName_10(String_t* value)
	{
		___m_sTuiSceneName_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTuiSceneName_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4DETECTTUIFLYDOWN_T822593849_H
#ifndef L3TUITERMINATESCENE_T3365052491_H
#define L3TUITERMINATESCENE_T3365052491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3TuiTerminateScene
struct  L3TuiTerminateScene_t3365052491  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L3TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// UnityEngine.GameObject L3TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// System.String L3TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_4;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L3TuiTerminateScene_t3365052491, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(L3TuiTerminateScene_t3365052491, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_4() { return static_cast<int32_t>(offsetof(L3TuiTerminateScene_t3365052491, ___tuiFlyHeadToID_4)); }
	inline String_t* get_tuiFlyHeadToID_4() const { return ___tuiFlyHeadToID_4; }
	inline String_t** get_address_of_tuiFlyHeadToID_4() { return &___tuiFlyHeadToID_4; }
	inline void set_tuiFlyHeadToID_4(String_t* value)
	{
		___tuiFlyHeadToID_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_4), value);
	}
};

struct L3TuiTerminateScene_t3365052491_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> L3TuiTerminateScene::<>f__switch$mapC
	Dictionary_2_t1378084586 * ___U3CU3Ef__switchU24mapC_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_5() { return static_cast<int32_t>(offsetof(L3TuiTerminateScene_t3365052491_StaticFields, ___U3CU3Ef__switchU24mapC_5)); }
	inline Dictionary_2_t1378084586 * get_U3CU3Ef__switchU24mapC_5() const { return ___U3CU3Ef__switchU24mapC_5; }
	inline Dictionary_2_t1378084586 ** get_address_of_U3CU3Ef__switchU24mapC_5() { return &___U3CU3Ef__switchU24mapC_5; }
	inline void set_U3CU3Ef__switchU24mapC_5(Dictionary_2_t1378084586 * value)
	{
		___U3CU3Ef__switchU24mapC_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3TUITERMINATESCENE_T3365052491_H
#ifndef L3DETECTTUIFLYDOWNNPCLAVA2_T2399048424_H
#define L3DETECTTUIFLYDOWNNPCLAVA2_T2399048424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DetectTuiFlyDownnpclava2
struct  L3DetectTuiFlyDownnpclava2_t2399048424  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L3DetectTuiFlyDownnpclava2::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_2;
	// System.Boolean L3DetectTuiFlyDownnpclava2::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_3;

public:
	inline static int32_t get_offset_of_m_bTuiConversationTriggered_2() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownnpclava2_t2399048424, ___m_bTuiConversationTriggered_2)); }
	inline bool get_m_bTuiConversationTriggered_2() const { return ___m_bTuiConversationTriggered_2; }
	inline bool* get_address_of_m_bTuiConversationTriggered_2() { return &___m_bTuiConversationTriggered_2; }
	inline void set_m_bTuiConversationTriggered_2(bool value)
	{
		___m_bTuiConversationTriggered_2 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_3() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownnpclava2_t2399048424, ___m_bFirstConversationFinished_3)); }
	inline bool get_m_bFirstConversationFinished_3() const { return ___m_bFirstConversationFinished_3; }
	inline bool* get_address_of_m_bFirstConversationFinished_3() { return &___m_bFirstConversationFinished_3; }
	inline void set_m_bFirstConversationFinished_3(bool value)
	{
		___m_bFirstConversationFinished_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DETECTTUIFLYDOWNNPCLAVA2_T2399048424_H
#ifndef L3DETECTTUIFLYDOWNMINIGAMES_T4225375576_H
#define L3DETECTTUIFLYDOWNMINIGAMES_T4225375576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DetectTuiFlyDownMinigames
struct  L3DetectTuiFlyDownMinigames_t4225375576  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L3DetectTuiFlyDownMinigames::m_vercanosmallPlayerDis
	float ___m_vercanosmallPlayerDis_2;
	// System.Single L3DetectTuiFlyDownMinigames::m_tuiNeedtoFlydownDis
	float ___m_tuiNeedtoFlydownDis_3;
	// System.Boolean L3DetectTuiFlyDownMinigames::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_4;
	// System.Boolean L3DetectTuiFlyDownMinigames::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_5;

public:
	inline static int32_t get_offset_of_m_vercanosmallPlayerDis_2() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownMinigames_t4225375576, ___m_vercanosmallPlayerDis_2)); }
	inline float get_m_vercanosmallPlayerDis_2() const { return ___m_vercanosmallPlayerDis_2; }
	inline float* get_address_of_m_vercanosmallPlayerDis_2() { return &___m_vercanosmallPlayerDis_2; }
	inline void set_m_vercanosmallPlayerDis_2(float value)
	{
		___m_vercanosmallPlayerDis_2 = value;
	}

	inline static int32_t get_offset_of_m_tuiNeedtoFlydownDis_3() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownMinigames_t4225375576, ___m_tuiNeedtoFlydownDis_3)); }
	inline float get_m_tuiNeedtoFlydownDis_3() const { return ___m_tuiNeedtoFlydownDis_3; }
	inline float* get_address_of_m_tuiNeedtoFlydownDis_3() { return &___m_tuiNeedtoFlydownDis_3; }
	inline void set_m_tuiNeedtoFlydownDis_3(float value)
	{
		___m_tuiNeedtoFlydownDis_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationTriggered_4() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownMinigames_t4225375576, ___m_bTuiConversationTriggered_4)); }
	inline bool get_m_bTuiConversationTriggered_4() const { return ___m_bTuiConversationTriggered_4; }
	inline bool* get_address_of_m_bTuiConversationTriggered_4() { return &___m_bTuiConversationTriggered_4; }
	inline void set_m_bTuiConversationTriggered_4(bool value)
	{
		___m_bTuiConversationTriggered_4 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_5() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownMinigames_t4225375576, ___m_bFirstConversationFinished_5)); }
	inline bool get_m_bFirstConversationFinished_5() const { return ___m_bFirstConversationFinished_5; }
	inline bool* get_address_of_m_bFirstConversationFinished_5() { return &___m_bFirstConversationFinished_5; }
	inline void set_m_bFirstConversationFinished_5(bool value)
	{
		___m_bFirstConversationFinished_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DETECTTUIFLYDOWNMINIGAMES_T4225375576_H
#ifndef L3DETECTTUIFLYDOWNFS_T3516154103_H
#define L3DETECTTUIFLYDOWNFS_T3516154103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DetectTuiFlyDownFs
struct  L3DetectTuiFlyDownFs_t3516154103  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L3DetectTuiFlyDownFs::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_2;
	// System.Boolean L3DetectTuiFlyDownFs::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_3;

public:
	inline static int32_t get_offset_of_m_bTuiConversationTriggered_2() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownFs_t3516154103, ___m_bTuiConversationTriggered_2)); }
	inline bool get_m_bTuiConversationTriggered_2() const { return ___m_bTuiConversationTriggered_2; }
	inline bool* get_address_of_m_bTuiConversationTriggered_2() { return &___m_bTuiConversationTriggered_2; }
	inline void set_m_bTuiConversationTriggered_2(bool value)
	{
		___m_bTuiConversationTriggered_2 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_3() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDownFs_t3516154103, ___m_bFirstConversationFinished_3)); }
	inline bool get_m_bFirstConversationFinished_3() const { return ___m_bFirstConversationFinished_3; }
	inline bool* get_address_of_m_bFirstConversationFinished_3() { return &___m_bFirstConversationFinished_3; }
	inline void set_m_bFirstConversationFinished_3(bool value)
	{
		___m_bFirstConversationFinished_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DETECTTUIFLYDOWNFS_T3516154103_H
#ifndef L3DETECTTUIFLYDOWN_T1649945521_H
#define L3DETECTTUIFLYDOWN_T1649945521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3DetectTuiFlyDown
struct  L3DetectTuiFlyDown_t1649945521  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L3DetectTuiFlyDown::m_vercanosmallPlayerDis
	float ___m_vercanosmallPlayerDis_2;
	// System.Single L3DetectTuiFlyDown::m_tuiNeedtoFlydownDis
	float ___m_tuiNeedtoFlydownDis_3;
	// System.Boolean L3DetectTuiFlyDown::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_4;
	// System.Boolean L3DetectTuiFlyDown::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_5;

public:
	inline static int32_t get_offset_of_m_vercanosmallPlayerDis_2() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDown_t1649945521, ___m_vercanosmallPlayerDis_2)); }
	inline float get_m_vercanosmallPlayerDis_2() const { return ___m_vercanosmallPlayerDis_2; }
	inline float* get_address_of_m_vercanosmallPlayerDis_2() { return &___m_vercanosmallPlayerDis_2; }
	inline void set_m_vercanosmallPlayerDis_2(float value)
	{
		___m_vercanosmallPlayerDis_2 = value;
	}

	inline static int32_t get_offset_of_m_tuiNeedtoFlydownDis_3() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDown_t1649945521, ___m_tuiNeedtoFlydownDis_3)); }
	inline float get_m_tuiNeedtoFlydownDis_3() const { return ___m_tuiNeedtoFlydownDis_3; }
	inline float* get_address_of_m_tuiNeedtoFlydownDis_3() { return &___m_tuiNeedtoFlydownDis_3; }
	inline void set_m_tuiNeedtoFlydownDis_3(float value)
	{
		___m_tuiNeedtoFlydownDis_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationTriggered_4() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDown_t1649945521, ___m_bTuiConversationTriggered_4)); }
	inline bool get_m_bTuiConversationTriggered_4() const { return ___m_bTuiConversationTriggered_4; }
	inline bool* get_address_of_m_bTuiConversationTriggered_4() { return &___m_bTuiConversationTriggered_4; }
	inline void set_m_bTuiConversationTriggered_4(bool value)
	{
		___m_bTuiConversationTriggered_4 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_5() { return static_cast<int32_t>(offsetof(L3DetectTuiFlyDown_t1649945521, ___m_bFirstConversationFinished_5)); }
	inline bool get_m_bFirstConversationFinished_5() const { return ___m_bFirstConversationFinished_5; }
	inline bool* get_address_of_m_bFirstConversationFinished_5() { return &___m_bFirstConversationFinished_5; }
	inline void set_m_bFirstConversationFinished_5(bool value)
	{
		___m_bFirstConversationFinished_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3DETECTTUIFLYDOWN_T1649945521_H
#ifndef L5TUITERMINATESCENE_T1564783844_H
#define L5TUITERMINATESCENE_T1564783844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5TuiTerminateScene
struct  L5TuiTerminateScene_t1564783844  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L5TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// UnityEngine.GameObject L5TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// System.String L5TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_4;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L5TuiTerminateScene_t1564783844, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(L5TuiTerminateScene_t1564783844, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_4() { return static_cast<int32_t>(offsetof(L5TuiTerminateScene_t1564783844, ___tuiFlyHeadToID_4)); }
	inline String_t* get_tuiFlyHeadToID_4() const { return ___tuiFlyHeadToID_4; }
	inline String_t** get_address_of_tuiFlyHeadToID_4() { return &___tuiFlyHeadToID_4; }
	inline void set_tuiFlyHeadToID_4(String_t* value)
	{
		___tuiFlyHeadToID_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5TUITERMINATESCENE_T1564783844_H
#ifndef CHARACTERWALKAWAY_T961142816_H
#define CHARACTERWALKAWAY_T961142816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterWalkAway
struct  CharacterWalkAway_t961142816  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 CharacterWalkAway::m_vTargetPosition
	Vector3_t1986933152  ___m_vTargetPosition_2;
	// System.Boolean CharacterWalkAway::m_bMovingToTarget
	bool ___m_bMovingToTarget_3;
	// System.Single CharacterWalkAway::m_fWalkingSpeed
	float ___m_fWalkingSpeed_4;

public:
	inline static int32_t get_offset_of_m_vTargetPosition_2() { return static_cast<int32_t>(offsetof(CharacterWalkAway_t961142816, ___m_vTargetPosition_2)); }
	inline Vector3_t1986933152  get_m_vTargetPosition_2() const { return ___m_vTargetPosition_2; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetPosition_2() { return &___m_vTargetPosition_2; }
	inline void set_m_vTargetPosition_2(Vector3_t1986933152  value)
	{
		___m_vTargetPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToTarget_3() { return static_cast<int32_t>(offsetof(CharacterWalkAway_t961142816, ___m_bMovingToTarget_3)); }
	inline bool get_m_bMovingToTarget_3() const { return ___m_bMovingToTarget_3; }
	inline bool* get_address_of_m_bMovingToTarget_3() { return &___m_bMovingToTarget_3; }
	inline void set_m_bMovingToTarget_3(bool value)
	{
		___m_bMovingToTarget_3 = value;
	}

	inline static int32_t get_offset_of_m_fWalkingSpeed_4() { return static_cast<int32_t>(offsetof(CharacterWalkAway_t961142816, ___m_fWalkingSpeed_4)); }
	inline float get_m_fWalkingSpeed_4() const { return ___m_fWalkingSpeed_4; }
	inline float* get_address_of_m_fWalkingSpeed_4() { return &___m_fWalkingSpeed_4; }
	inline void set_m_fWalkingSpeed_4(float value)
	{
		___m_fWalkingSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERWALKAWAY_T961142816_H
#ifndef DOORTRIGGERENTER_T3147943931_H
#define DOORTRIGGERENTER_T3147943931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoorTriggerEnter
struct  DoorTriggerEnter_t3147943931  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean DoorTriggerEnter::m_DoorTriggerEntered
	bool ___m_DoorTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_DoorTriggerEntered_2() { return static_cast<int32_t>(offsetof(DoorTriggerEnter_t3147943931, ___m_DoorTriggerEntered_2)); }
	inline bool get_m_DoorTriggerEntered_2() const { return ___m_DoorTriggerEntered_2; }
	inline bool* get_address_of_m_DoorTriggerEntered_2() { return &___m_DoorTriggerEntered_2; }
	inline void set_m_DoorTriggerEntered_2(bool value)
	{
		___m_DoorTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOORTRIGGERENTER_T3147943931_H
#ifndef SIDEDOORTRIGGER_T550685523_H
#define SIDEDOORTRIGGER_T550685523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SideDoorTrigger
struct  SideDoorTrigger_t550685523  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIDEDOORTRIGGER_T550685523_H
#ifndef GNATSGOTOPOS_T2206653697_H
#define GNATSGOTOPOS_T2206653697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GnatsGoToPos
struct  GnatsGoToPos_t2206653697  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform GnatsGoToPos::TargetPos
	Transform_t362059596 * ___TargetPos_2;
	// System.Boolean GnatsGoToPos::m_bFinishedMoving
	bool ___m_bFinishedMoving_3;

public:
	inline static int32_t get_offset_of_TargetPos_2() { return static_cast<int32_t>(offsetof(GnatsGoToPos_t2206653697, ___TargetPos_2)); }
	inline Transform_t362059596 * get_TargetPos_2() const { return ___TargetPos_2; }
	inline Transform_t362059596 ** get_address_of_TargetPos_2() { return &___TargetPos_2; }
	inline void set_TargetPos_2(Transform_t362059596 * value)
	{
		___TargetPos_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetPos_2), value);
	}

	inline static int32_t get_offset_of_m_bFinishedMoving_3() { return static_cast<int32_t>(offsetof(GnatsGoToPos_t2206653697, ___m_bFinishedMoving_3)); }
	inline bool get_m_bFinishedMoving_3() const { return ___m_bFinishedMoving_3; }
	inline bool* get_address_of_m_bFinishedMoving_3() { return &___m_bFinishedMoving_3; }
	inline void set_m_bFinishedMoving_3(bool value)
	{
		___m_bFinishedMoving_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNATSGOTOPOS_T2206653697_H
#ifndef PORTALINTERACTION_T2393667535_H
#define PORTALINTERACTION_T2393667535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PortalInteraction
struct  PortalInteraction_t2393667535  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 PortalInteraction::CameraPosition
	Vector3_t1986933152  ___CameraPosition_2;
	// System.Single PortalInteraction::InteractionRadius
	float ___InteractionRadius_3;
	// System.Single PortalInteraction::m_fTransitionStartTime
	float ___m_fTransitionStartTime_4;
	// System.Single PortalInteraction::m_fTransitionTime
	float ___m_fTransitionTime_5;
	// System.Boolean PortalInteraction::IsAutomatic
	bool ___IsAutomatic_6;
	// System.Boolean PortalInteraction::Interacting
	bool ___Interacting_7;
	// System.Boolean PortalInteraction::m_bTransitionStarts
	bool ___m_bTransitionStarts_8;
	// System.Boolean PortalInteraction::m_playerStartMoving
	bool ___m_playerStartMoving_9;
	// System.Boolean PortalInteraction::whiteOutStart
	bool ___whiteOutStart_10;
	// UnityEngine.GameObject PortalInteraction::whiteout
	GameObject_t2557347079 * ___whiteout_11;
	// UnityEngine.GameObject PortalInteraction::newPortalEffect
	GameObject_t2557347079 * ___newPortalEffect_12;

public:
	inline static int32_t get_offset_of_CameraPosition_2() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___CameraPosition_2)); }
	inline Vector3_t1986933152  get_CameraPosition_2() const { return ___CameraPosition_2; }
	inline Vector3_t1986933152 * get_address_of_CameraPosition_2() { return &___CameraPosition_2; }
	inline void set_CameraPosition_2(Vector3_t1986933152  value)
	{
		___CameraPosition_2 = value;
	}

	inline static int32_t get_offset_of_InteractionRadius_3() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___InteractionRadius_3)); }
	inline float get_InteractionRadius_3() const { return ___InteractionRadius_3; }
	inline float* get_address_of_InteractionRadius_3() { return &___InteractionRadius_3; }
	inline void set_InteractionRadius_3(float value)
	{
		___InteractionRadius_3 = value;
	}

	inline static int32_t get_offset_of_m_fTransitionStartTime_4() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___m_fTransitionStartTime_4)); }
	inline float get_m_fTransitionStartTime_4() const { return ___m_fTransitionStartTime_4; }
	inline float* get_address_of_m_fTransitionStartTime_4() { return &___m_fTransitionStartTime_4; }
	inline void set_m_fTransitionStartTime_4(float value)
	{
		___m_fTransitionStartTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fTransitionTime_5() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___m_fTransitionTime_5)); }
	inline float get_m_fTransitionTime_5() const { return ___m_fTransitionTime_5; }
	inline float* get_address_of_m_fTransitionTime_5() { return &___m_fTransitionTime_5; }
	inline void set_m_fTransitionTime_5(float value)
	{
		___m_fTransitionTime_5 = value;
	}

	inline static int32_t get_offset_of_IsAutomatic_6() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___IsAutomatic_6)); }
	inline bool get_IsAutomatic_6() const { return ___IsAutomatic_6; }
	inline bool* get_address_of_IsAutomatic_6() { return &___IsAutomatic_6; }
	inline void set_IsAutomatic_6(bool value)
	{
		___IsAutomatic_6 = value;
	}

	inline static int32_t get_offset_of_Interacting_7() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___Interacting_7)); }
	inline bool get_Interacting_7() const { return ___Interacting_7; }
	inline bool* get_address_of_Interacting_7() { return &___Interacting_7; }
	inline void set_Interacting_7(bool value)
	{
		___Interacting_7 = value;
	}

	inline static int32_t get_offset_of_m_bTransitionStarts_8() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___m_bTransitionStarts_8)); }
	inline bool get_m_bTransitionStarts_8() const { return ___m_bTransitionStarts_8; }
	inline bool* get_address_of_m_bTransitionStarts_8() { return &___m_bTransitionStarts_8; }
	inline void set_m_bTransitionStarts_8(bool value)
	{
		___m_bTransitionStarts_8 = value;
	}

	inline static int32_t get_offset_of_m_playerStartMoving_9() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___m_playerStartMoving_9)); }
	inline bool get_m_playerStartMoving_9() const { return ___m_playerStartMoving_9; }
	inline bool* get_address_of_m_playerStartMoving_9() { return &___m_playerStartMoving_9; }
	inline void set_m_playerStartMoving_9(bool value)
	{
		___m_playerStartMoving_9 = value;
	}

	inline static int32_t get_offset_of_whiteOutStart_10() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___whiteOutStart_10)); }
	inline bool get_whiteOutStart_10() const { return ___whiteOutStart_10; }
	inline bool* get_address_of_whiteOutStart_10() { return &___whiteOutStart_10; }
	inline void set_whiteOutStart_10(bool value)
	{
		___whiteOutStart_10 = value;
	}

	inline static int32_t get_offset_of_whiteout_11() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___whiteout_11)); }
	inline GameObject_t2557347079 * get_whiteout_11() const { return ___whiteout_11; }
	inline GameObject_t2557347079 ** get_address_of_whiteout_11() { return &___whiteout_11; }
	inline void set_whiteout_11(GameObject_t2557347079 * value)
	{
		___whiteout_11 = value;
		Il2CppCodeGenWriteBarrier((&___whiteout_11), value);
	}

	inline static int32_t get_offset_of_newPortalEffect_12() { return static_cast<int32_t>(offsetof(PortalInteraction_t2393667535, ___newPortalEffect_12)); }
	inline GameObject_t2557347079 * get_newPortalEffect_12() const { return ___newPortalEffect_12; }
	inline GameObject_t2557347079 ** get_address_of_newPortalEffect_12() { return &___newPortalEffect_12; }
	inline void set_newPortalEffect_12(GameObject_t2557347079 * value)
	{
		___newPortalEffect_12 = value;
		Il2CppCodeGenWriteBarrier((&___newPortalEffect_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTALINTERACTION_T2393667535_H
#ifndef OBJECTMOVEMENT_T1497131487_H
#define OBJECTMOVEMENT_T1497131487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectMovement
struct  ObjectMovement_t1497131487  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean ObjectMovement::bPosition
	bool ___bPosition_2;
	// System.Boolean ObjectMovement::bRotation
	bool ___bRotation_3;
	// UnityEngine.Vector3 ObjectMovement::m_vTarget
	Vector3_t1986933152  ___m_vTarget_4;
	// UnityEngine.Vector3 ObjectMovement::m_vMovement
	Vector3_t1986933152  ___m_vMovement_5;
	// System.Boolean ObjectMovement::m_bStartMoving
	bool ___m_bStartMoving_6;
	// UnityEngine.Vector3 ObjectMovement::m_vRotation
	Vector3_t1986933152  ___m_vRotation_7;
	// UnityEngine.Vector3 ObjectMovement::m_vRotationBackwards
	Vector3_t1986933152  ___m_vRotationBackwards_8;
	// System.Boolean ObjectMovement::m_bMovementComplete
	bool ___m_bMovementComplete_9;
	// UnityEngine.Vector3 ObjectMovement::m_endRotation
	Vector3_t1986933152  ___m_endRotation_10;
	// UnityEngine.Vector3 ObjectMovement::m_vStartPosition
	Vector3_t1986933152  ___m_vStartPosition_11;
	// UnityEngine.Vector3 ObjectMovement::m_vOriginalPosition
	Vector3_t1986933152  ___m_vOriginalPosition_12;
	// System.Single ObjectMovement::m_fMovementProgress
	float ___m_fMovementProgress_13;
	// System.Single ObjectMovement::m_fbackWardMovementProgress
	float ___m_fbackWardMovementProgress_14;
	// UnityEngine.Vector3 ObjectMovement::m_vRotationElapsed
	Vector3_t1986933152  ___m_vRotationElapsed_15;
	// System.Boolean ObjectMovement::RotationDone
	bool ___RotationDone_16;

public:
	inline static int32_t get_offset_of_bPosition_2() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___bPosition_2)); }
	inline bool get_bPosition_2() const { return ___bPosition_2; }
	inline bool* get_address_of_bPosition_2() { return &___bPosition_2; }
	inline void set_bPosition_2(bool value)
	{
		___bPosition_2 = value;
	}

	inline static int32_t get_offset_of_bRotation_3() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___bRotation_3)); }
	inline bool get_bRotation_3() const { return ___bRotation_3; }
	inline bool* get_address_of_bRotation_3() { return &___bRotation_3; }
	inline void set_bRotation_3(bool value)
	{
		___bRotation_3 = value;
	}

	inline static int32_t get_offset_of_m_vTarget_4() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vTarget_4)); }
	inline Vector3_t1986933152  get_m_vTarget_4() const { return ___m_vTarget_4; }
	inline Vector3_t1986933152 * get_address_of_m_vTarget_4() { return &___m_vTarget_4; }
	inline void set_m_vTarget_4(Vector3_t1986933152  value)
	{
		___m_vTarget_4 = value;
	}

	inline static int32_t get_offset_of_m_vMovement_5() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vMovement_5)); }
	inline Vector3_t1986933152  get_m_vMovement_5() const { return ___m_vMovement_5; }
	inline Vector3_t1986933152 * get_address_of_m_vMovement_5() { return &___m_vMovement_5; }
	inline void set_m_vMovement_5(Vector3_t1986933152  value)
	{
		___m_vMovement_5 = value;
	}

	inline static int32_t get_offset_of_m_bStartMoving_6() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_bStartMoving_6)); }
	inline bool get_m_bStartMoving_6() const { return ___m_bStartMoving_6; }
	inline bool* get_address_of_m_bStartMoving_6() { return &___m_bStartMoving_6; }
	inline void set_m_bStartMoving_6(bool value)
	{
		___m_bStartMoving_6 = value;
	}

	inline static int32_t get_offset_of_m_vRotation_7() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vRotation_7)); }
	inline Vector3_t1986933152  get_m_vRotation_7() const { return ___m_vRotation_7; }
	inline Vector3_t1986933152 * get_address_of_m_vRotation_7() { return &___m_vRotation_7; }
	inline void set_m_vRotation_7(Vector3_t1986933152  value)
	{
		___m_vRotation_7 = value;
	}

	inline static int32_t get_offset_of_m_vRotationBackwards_8() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vRotationBackwards_8)); }
	inline Vector3_t1986933152  get_m_vRotationBackwards_8() const { return ___m_vRotationBackwards_8; }
	inline Vector3_t1986933152 * get_address_of_m_vRotationBackwards_8() { return &___m_vRotationBackwards_8; }
	inline void set_m_vRotationBackwards_8(Vector3_t1986933152  value)
	{
		___m_vRotationBackwards_8 = value;
	}

	inline static int32_t get_offset_of_m_bMovementComplete_9() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_bMovementComplete_9)); }
	inline bool get_m_bMovementComplete_9() const { return ___m_bMovementComplete_9; }
	inline bool* get_address_of_m_bMovementComplete_9() { return &___m_bMovementComplete_9; }
	inline void set_m_bMovementComplete_9(bool value)
	{
		___m_bMovementComplete_9 = value;
	}

	inline static int32_t get_offset_of_m_endRotation_10() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_endRotation_10)); }
	inline Vector3_t1986933152  get_m_endRotation_10() const { return ___m_endRotation_10; }
	inline Vector3_t1986933152 * get_address_of_m_endRotation_10() { return &___m_endRotation_10; }
	inline void set_m_endRotation_10(Vector3_t1986933152  value)
	{
		___m_endRotation_10 = value;
	}

	inline static int32_t get_offset_of_m_vStartPosition_11() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vStartPosition_11)); }
	inline Vector3_t1986933152  get_m_vStartPosition_11() const { return ___m_vStartPosition_11; }
	inline Vector3_t1986933152 * get_address_of_m_vStartPosition_11() { return &___m_vStartPosition_11; }
	inline void set_m_vStartPosition_11(Vector3_t1986933152  value)
	{
		___m_vStartPosition_11 = value;
	}

	inline static int32_t get_offset_of_m_vOriginalPosition_12() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vOriginalPosition_12)); }
	inline Vector3_t1986933152  get_m_vOriginalPosition_12() const { return ___m_vOriginalPosition_12; }
	inline Vector3_t1986933152 * get_address_of_m_vOriginalPosition_12() { return &___m_vOriginalPosition_12; }
	inline void set_m_vOriginalPosition_12(Vector3_t1986933152  value)
	{
		___m_vOriginalPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_fMovementProgress_13() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_fMovementProgress_13)); }
	inline float get_m_fMovementProgress_13() const { return ___m_fMovementProgress_13; }
	inline float* get_address_of_m_fMovementProgress_13() { return &___m_fMovementProgress_13; }
	inline void set_m_fMovementProgress_13(float value)
	{
		___m_fMovementProgress_13 = value;
	}

	inline static int32_t get_offset_of_m_fbackWardMovementProgress_14() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_fbackWardMovementProgress_14)); }
	inline float get_m_fbackWardMovementProgress_14() const { return ___m_fbackWardMovementProgress_14; }
	inline float* get_address_of_m_fbackWardMovementProgress_14() { return &___m_fbackWardMovementProgress_14; }
	inline void set_m_fbackWardMovementProgress_14(float value)
	{
		___m_fbackWardMovementProgress_14 = value;
	}

	inline static int32_t get_offset_of_m_vRotationElapsed_15() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___m_vRotationElapsed_15)); }
	inline Vector3_t1986933152  get_m_vRotationElapsed_15() const { return ___m_vRotationElapsed_15; }
	inline Vector3_t1986933152 * get_address_of_m_vRotationElapsed_15() { return &___m_vRotationElapsed_15; }
	inline void set_m_vRotationElapsed_15(Vector3_t1986933152  value)
	{
		___m_vRotationElapsed_15 = value;
	}

	inline static int32_t get_offset_of_RotationDone_16() { return static_cast<int32_t>(offsetof(ObjectMovement_t1497131487, ___RotationDone_16)); }
	inline bool get_RotationDone_16() const { return ___RotationDone_16; }
	inline bool* get_address_of_RotationDone_16() { return &___RotationDone_16; }
	inline void set_RotationDone_16(bool value)
	{
		___RotationDone_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTMOVEMENT_T1497131487_H
#ifndef NPCMOVETO_T1450212510_H
#define NPCMOVETO_T1450212510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NPCMoveTo
struct  NPCMoveTo_t1450212510  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform NPCMoveTo::TargetPosition
	Transform_t362059596 * ___TargetPosition_2;
	// System.Boolean NPCMoveTo::IsWalking
	bool ___IsWalking_3;
	// System.Boolean NPCMoveTo::ArrivedAtPosition
	bool ___ArrivedAtPosition_4;

public:
	inline static int32_t get_offset_of_TargetPosition_2() { return static_cast<int32_t>(offsetof(NPCMoveTo_t1450212510, ___TargetPosition_2)); }
	inline Transform_t362059596 * get_TargetPosition_2() const { return ___TargetPosition_2; }
	inline Transform_t362059596 ** get_address_of_TargetPosition_2() { return &___TargetPosition_2; }
	inline void set_TargetPosition_2(Transform_t362059596 * value)
	{
		___TargetPosition_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetPosition_2), value);
	}

	inline static int32_t get_offset_of_IsWalking_3() { return static_cast<int32_t>(offsetof(NPCMoveTo_t1450212510, ___IsWalking_3)); }
	inline bool get_IsWalking_3() const { return ___IsWalking_3; }
	inline bool* get_address_of_IsWalking_3() { return &___IsWalking_3; }
	inline void set_IsWalking_3(bool value)
	{
		___IsWalking_3 = value;
	}

	inline static int32_t get_offset_of_ArrivedAtPosition_4() { return static_cast<int32_t>(offsetof(NPCMoveTo_t1450212510, ___ArrivedAtPosition_4)); }
	inline bool get_ArrivedAtPosition_4() const { return ___ArrivedAtPosition_4; }
	inline bool* get_address_of_ArrivedAtPosition_4() { return &___ArrivedAtPosition_4; }
	inline void set_ArrivedAtPosition_4(bool value)
	{
		___ArrivedAtPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NPCMOVETO_T1450212510_H
#ifndef MELTICEYETI_T3902559005_H
#define MELTICEYETI_T3902559005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeltIceYeti
struct  MeltIceYeti_t3902559005  : public MonoBehaviour_t1618594486
{
public:
	// IceMeltRoutine MeltIceYeti::MeltStatus
	int32_t ___MeltStatus_2;
	// UnityEngine.GameObject[] MeltIceYeti::IceParts
	GameObjectU5BU5D_t2988620542* ___IceParts_3;
	// UnityEngine.GameObject MeltIceYeti::ZoneBox
	GameObject_t2557347079 * ___ZoneBox_4;
	// UnityEngine.Transform MeltIceYeti::WalkToPosition
	Transform_t362059596 * ___WalkToPosition_5;
	// System.Int32[] MeltIceYeti::FireCount
	Int32U5BU5D_t1965588061* ___FireCount_6;
	// System.String MeltIceYeti::YetiCameraID
	String_t* ___YetiCameraID_7;
	// System.Boolean MeltIceYeti::m_bAttackingIce
	bool ___m_bAttackingIce_8;
	// System.Int32 MeltIceYeti::CurrentAttackingPart
	int32_t ___CurrentAttackingPart_9;
	// System.Boolean MeltIceYeti::m_bTuiConversationFirst
	bool ___m_bTuiConversationFirst_10;

public:
	inline static int32_t get_offset_of_MeltStatus_2() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___MeltStatus_2)); }
	inline int32_t get_MeltStatus_2() const { return ___MeltStatus_2; }
	inline int32_t* get_address_of_MeltStatus_2() { return &___MeltStatus_2; }
	inline void set_MeltStatus_2(int32_t value)
	{
		___MeltStatus_2 = value;
	}

	inline static int32_t get_offset_of_IceParts_3() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___IceParts_3)); }
	inline GameObjectU5BU5D_t2988620542* get_IceParts_3() const { return ___IceParts_3; }
	inline GameObjectU5BU5D_t2988620542** get_address_of_IceParts_3() { return &___IceParts_3; }
	inline void set_IceParts_3(GameObjectU5BU5D_t2988620542* value)
	{
		___IceParts_3 = value;
		Il2CppCodeGenWriteBarrier((&___IceParts_3), value);
	}

	inline static int32_t get_offset_of_ZoneBox_4() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___ZoneBox_4)); }
	inline GameObject_t2557347079 * get_ZoneBox_4() const { return ___ZoneBox_4; }
	inline GameObject_t2557347079 ** get_address_of_ZoneBox_4() { return &___ZoneBox_4; }
	inline void set_ZoneBox_4(GameObject_t2557347079 * value)
	{
		___ZoneBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___ZoneBox_4), value);
	}

	inline static int32_t get_offset_of_WalkToPosition_5() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___WalkToPosition_5)); }
	inline Transform_t362059596 * get_WalkToPosition_5() const { return ___WalkToPosition_5; }
	inline Transform_t362059596 ** get_address_of_WalkToPosition_5() { return &___WalkToPosition_5; }
	inline void set_WalkToPosition_5(Transform_t362059596 * value)
	{
		___WalkToPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&___WalkToPosition_5), value);
	}

	inline static int32_t get_offset_of_FireCount_6() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___FireCount_6)); }
	inline Int32U5BU5D_t1965588061* get_FireCount_6() const { return ___FireCount_6; }
	inline Int32U5BU5D_t1965588061** get_address_of_FireCount_6() { return &___FireCount_6; }
	inline void set_FireCount_6(Int32U5BU5D_t1965588061* value)
	{
		___FireCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___FireCount_6), value);
	}

	inline static int32_t get_offset_of_YetiCameraID_7() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___YetiCameraID_7)); }
	inline String_t* get_YetiCameraID_7() const { return ___YetiCameraID_7; }
	inline String_t** get_address_of_YetiCameraID_7() { return &___YetiCameraID_7; }
	inline void set_YetiCameraID_7(String_t* value)
	{
		___YetiCameraID_7 = value;
		Il2CppCodeGenWriteBarrier((&___YetiCameraID_7), value);
	}

	inline static int32_t get_offset_of_m_bAttackingIce_8() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___m_bAttackingIce_8)); }
	inline bool get_m_bAttackingIce_8() const { return ___m_bAttackingIce_8; }
	inline bool* get_address_of_m_bAttackingIce_8() { return &___m_bAttackingIce_8; }
	inline void set_m_bAttackingIce_8(bool value)
	{
		___m_bAttackingIce_8 = value;
	}

	inline static int32_t get_offset_of_CurrentAttackingPart_9() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___CurrentAttackingPart_9)); }
	inline int32_t get_CurrentAttackingPart_9() const { return ___CurrentAttackingPart_9; }
	inline int32_t* get_address_of_CurrentAttackingPart_9() { return &___CurrentAttackingPart_9; }
	inline void set_CurrentAttackingPart_9(int32_t value)
	{
		___CurrentAttackingPart_9 = value;
	}

	inline static int32_t get_offset_of_m_bTuiConversationFirst_10() { return static_cast<int32_t>(offsetof(MeltIceYeti_t3902559005, ___m_bTuiConversationFirst_10)); }
	inline bool get_m_bTuiConversationFirst_10() const { return ___m_bTuiConversationFirst_10; }
	inline bool* get_address_of_m_bTuiConversationFirst_10() { return &___m_bTuiConversationFirst_10; }
	inline void set_m_bTuiConversationFirst_10(bool value)
	{
		___m_bTuiConversationFirst_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MELTICEYETI_T3902559005_H
#ifndef LVSIXDOOR_T3751206300_H
#define LVSIXDOOR_T3751206300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LvSixDoor
struct  LvSixDoor_t3751206300  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject LvSixDoor::LeftDoor
	GameObject_t2557347079 * ___LeftDoor_2;
	// UnityEngine.GameObject LvSixDoor::RightDoor
	GameObject_t2557347079 * ___RightDoor_3;
	// System.Boolean LvSixDoor::bOpen
	bool ___bOpen_4;

public:
	inline static int32_t get_offset_of_LeftDoor_2() { return static_cast<int32_t>(offsetof(LvSixDoor_t3751206300, ___LeftDoor_2)); }
	inline GameObject_t2557347079 * get_LeftDoor_2() const { return ___LeftDoor_2; }
	inline GameObject_t2557347079 ** get_address_of_LeftDoor_2() { return &___LeftDoor_2; }
	inline void set_LeftDoor_2(GameObject_t2557347079 * value)
	{
		___LeftDoor_2 = value;
		Il2CppCodeGenWriteBarrier((&___LeftDoor_2), value);
	}

	inline static int32_t get_offset_of_RightDoor_3() { return static_cast<int32_t>(offsetof(LvSixDoor_t3751206300, ___RightDoor_3)); }
	inline GameObject_t2557347079 * get_RightDoor_3() const { return ___RightDoor_3; }
	inline GameObject_t2557347079 ** get_address_of_RightDoor_3() { return &___RightDoor_3; }
	inline void set_RightDoor_3(GameObject_t2557347079 * value)
	{
		___RightDoor_3 = value;
		Il2CppCodeGenWriteBarrier((&___RightDoor_3), value);
	}

	inline static int32_t get_offset_of_bOpen_4() { return static_cast<int32_t>(offsetof(LvSixDoor_t3751206300, ___bOpen_4)); }
	inline bool get_bOpen_4() const { return ___bOpen_4; }
	inline bool* get_address_of_bOpen_4() { return &___bOpen_4; }
	inline void set_bOpen_4(bool value)
	{
		___bOpen_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LVSIXDOOR_T3751206300_H
#ifndef LV5GNATSPARXGAME_T1320105928_H
#define LV5GNATSPARXGAME_T1320105928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lv5GnatSparxGame
struct  Lv5GnatSparxGame_t1320105928  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Lv5GnatSparxGame::bTriggered
	bool ___bTriggered_2;
	// System.Single Lv5GnatSparxGame::fSpeed
	float ___fSpeed_3;
	// System.String Lv5GnatSparxGame::CameraID
	String_t* ___CameraID_4;
	// UnityEngine.GameObject Lv5GnatSparxGame::Trigger
	GameObject_t2557347079 * ___Trigger_5;
	// UnityEngine.GameObject Lv5GnatSparxGame::TargetObj
	GameObject_t2557347079 * ___TargetObj_6;
	// UnityEngine.GameObject Lv5GnatSparxGame::FireBall
	GameObject_t2557347079 * ___FireBall_7;
	// System.Boolean Lv5GnatSparxGame::Firing
	bool ___Firing_8;
	// System.Int32 Lv5GnatSparxGame::iCurrentNumOfGnatsShot
	int32_t ___iCurrentNumOfGnatsShot_9;
	// UnityEngine.Transform[] Lv5GnatSparxGame::GnatEndPosition
	TransformU5BU5D_t2383644165* ___GnatEndPosition_10;
	// UnityEngine.Vector3 Lv5GnatSparxGame::tempPos
	Vector3_t1986933152  ___tempPos_11;

public:
	inline static int32_t get_offset_of_bTriggered_2() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___bTriggered_2)); }
	inline bool get_bTriggered_2() const { return ___bTriggered_2; }
	inline bool* get_address_of_bTriggered_2() { return &___bTriggered_2; }
	inline void set_bTriggered_2(bool value)
	{
		___bTriggered_2 = value;
	}

	inline static int32_t get_offset_of_fSpeed_3() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___fSpeed_3)); }
	inline float get_fSpeed_3() const { return ___fSpeed_3; }
	inline float* get_address_of_fSpeed_3() { return &___fSpeed_3; }
	inline void set_fSpeed_3(float value)
	{
		___fSpeed_3 = value;
	}

	inline static int32_t get_offset_of_CameraID_4() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___CameraID_4)); }
	inline String_t* get_CameraID_4() const { return ___CameraID_4; }
	inline String_t** get_address_of_CameraID_4() { return &___CameraID_4; }
	inline void set_CameraID_4(String_t* value)
	{
		___CameraID_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_4), value);
	}

	inline static int32_t get_offset_of_Trigger_5() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___Trigger_5)); }
	inline GameObject_t2557347079 * get_Trigger_5() const { return ___Trigger_5; }
	inline GameObject_t2557347079 ** get_address_of_Trigger_5() { return &___Trigger_5; }
	inline void set_Trigger_5(GameObject_t2557347079 * value)
	{
		___Trigger_5 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_5), value);
	}

	inline static int32_t get_offset_of_TargetObj_6() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___TargetObj_6)); }
	inline GameObject_t2557347079 * get_TargetObj_6() const { return ___TargetObj_6; }
	inline GameObject_t2557347079 ** get_address_of_TargetObj_6() { return &___TargetObj_6; }
	inline void set_TargetObj_6(GameObject_t2557347079 * value)
	{
		___TargetObj_6 = value;
		Il2CppCodeGenWriteBarrier((&___TargetObj_6), value);
	}

	inline static int32_t get_offset_of_FireBall_7() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___FireBall_7)); }
	inline GameObject_t2557347079 * get_FireBall_7() const { return ___FireBall_7; }
	inline GameObject_t2557347079 ** get_address_of_FireBall_7() { return &___FireBall_7; }
	inline void set_FireBall_7(GameObject_t2557347079 * value)
	{
		___FireBall_7 = value;
		Il2CppCodeGenWriteBarrier((&___FireBall_7), value);
	}

	inline static int32_t get_offset_of_Firing_8() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___Firing_8)); }
	inline bool get_Firing_8() const { return ___Firing_8; }
	inline bool* get_address_of_Firing_8() { return &___Firing_8; }
	inline void set_Firing_8(bool value)
	{
		___Firing_8 = value;
	}

	inline static int32_t get_offset_of_iCurrentNumOfGnatsShot_9() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___iCurrentNumOfGnatsShot_9)); }
	inline int32_t get_iCurrentNumOfGnatsShot_9() const { return ___iCurrentNumOfGnatsShot_9; }
	inline int32_t* get_address_of_iCurrentNumOfGnatsShot_9() { return &___iCurrentNumOfGnatsShot_9; }
	inline void set_iCurrentNumOfGnatsShot_9(int32_t value)
	{
		___iCurrentNumOfGnatsShot_9 = value;
	}

	inline static int32_t get_offset_of_GnatEndPosition_10() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___GnatEndPosition_10)); }
	inline TransformU5BU5D_t2383644165* get_GnatEndPosition_10() const { return ___GnatEndPosition_10; }
	inline TransformU5BU5D_t2383644165** get_address_of_GnatEndPosition_10() { return &___GnatEndPosition_10; }
	inline void set_GnatEndPosition_10(TransformU5BU5D_t2383644165* value)
	{
		___GnatEndPosition_10 = value;
		Il2CppCodeGenWriteBarrier((&___GnatEndPosition_10), value);
	}

	inline static int32_t get_offset_of_tempPos_11() { return static_cast<int32_t>(offsetof(Lv5GnatSparxGame_t1320105928, ___tempPos_11)); }
	inline Vector3_t1986933152  get_tempPos_11() const { return ___tempPos_11; }
	inline Vector3_t1986933152 * get_address_of_tempPos_11() { return &___tempPos_11; }
	inline void set_tempPos_11(Vector3_t1986933152  value)
	{
		___tempPos_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LV5GNATSPARXGAME_T1320105928_H
#ifndef LEVEL5GNATSINTERACTION_T264916876_H
#define LEVEL5GNATSINTERACTION_T264916876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level5GnatsInteraction
struct  Level5GnatsInteraction_t264916876  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Level5GnatsInteraction::Active
	bool ___Active_2;
	// System.Boolean Level5GnatsInteraction::bPlayedParticle
	bool ___bPlayedParticle_3;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(Level5GnatsInteraction_t264916876, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}

	inline static int32_t get_offset_of_bPlayedParticle_3() { return static_cast<int32_t>(offsetof(Level5GnatsInteraction_t264916876, ___bPlayedParticle_3)); }
	inline bool get_bPlayedParticle_3() const { return ___bPlayedParticle_3; }
	inline bool* get_address_of_bPlayedParticle_3() { return &___bPlayedParticle_3; }
	inline void set_bPlayedParticle_3(bool value)
	{
		___bPlayedParticle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL5GNATSINTERACTION_T264916876_H
#ifndef CHESTINTERACTION_T173854283_H
#define CHESTINTERACTION_T173854283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestInteraction
struct  ChestInteraction_t173854283  : public MonoBehaviour_t1618594486
{
public:
	// System.Single ChestInteraction::radius
	float ___radius_2;
	// UnityEngine.GameObject ChestInteraction::FightGnats
	GameObject_t2557347079 * ___FightGnats_3;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(ChestInteraction_t173854283, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_FightGnats_3() { return static_cast<int32_t>(offsetof(ChestInteraction_t173854283, ___FightGnats_3)); }
	inline GameObject_t2557347079 * get_FightGnats_3() const { return ___FightGnats_3; }
	inline GameObject_t2557347079 ** get_address_of_FightGnats_3() { return &___FightGnats_3; }
	inline void set_FightGnats_3(GameObject_t2557347079 * value)
	{
		___FightGnats_3 = value;
		Il2CppCodeGenWriteBarrier((&___FightGnats_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTINTERACTION_T173854283_H
#ifndef LEFTDOORTRIGGERENTER_T863745348_H
#define LEFTDOORTRIGGERENTER_T863745348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftDoorTriggerEnter
struct  LeftDoorTriggerEnter_t863745348  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LeftDoorTriggerEnter::m_leftDoorTriggerEntered
	bool ___m_leftDoorTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_leftDoorTriggerEntered_2() { return static_cast<int32_t>(offsetof(LeftDoorTriggerEnter_t863745348, ___m_leftDoorTriggerEntered_2)); }
	inline bool get_m_leftDoorTriggerEntered_2() const { return ___m_leftDoorTriggerEntered_2; }
	inline bool* get_address_of_m_leftDoorTriggerEntered_2() { return &___m_leftDoorTriggerEntered_2; }
	inline void set_m_leftDoorTriggerEntered_2(bool value)
	{
		___m_leftDoorTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEFTDOORTRIGGERENTER_T863745348_H
#ifndef LADDERINTERACTION_T560741349_H
#define LADDERINTERACTION_T560741349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LadderInteraction
struct  LadderInteraction_t560741349  : public MonoBehaviour_t1618594486
{
public:
	// System.Single LadderInteraction::fLengthOfLadder
	float ___fLengthOfLadder_2;
	// System.Single LadderInteraction::fPlayerPositionOnLadder
	float ___fPlayerPositionOnLadder_3;
	// System.Boolean LadderInteraction::m_bCorrectLadder
	bool ___m_bCorrectLadder_4;
	// UnityEngine.Transform LadderInteraction::EndPos
	Transform_t362059596 * ___EndPos_5;
	// UnityEngine.GameObject LadderInteraction::Trigger
	GameObject_t2557347079 * ___Trigger_6;
	// System.Boolean LadderInteraction::m_bOnThisLadder
	bool ___m_bOnThisLadder_7;
	// System.Boolean LadderInteraction::m_bClimbing
	bool ___m_bClimbing_8;
	// System.Boolean LadderInteraction::m_bReachedTop
	bool ___m_bReachedTop_9;
	// System.Single LadderInteraction::moveCap
	float ___moveCap_10;

public:
	inline static int32_t get_offset_of_fLengthOfLadder_2() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___fLengthOfLadder_2)); }
	inline float get_fLengthOfLadder_2() const { return ___fLengthOfLadder_2; }
	inline float* get_address_of_fLengthOfLadder_2() { return &___fLengthOfLadder_2; }
	inline void set_fLengthOfLadder_2(float value)
	{
		___fLengthOfLadder_2 = value;
	}

	inline static int32_t get_offset_of_fPlayerPositionOnLadder_3() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___fPlayerPositionOnLadder_3)); }
	inline float get_fPlayerPositionOnLadder_3() const { return ___fPlayerPositionOnLadder_3; }
	inline float* get_address_of_fPlayerPositionOnLadder_3() { return &___fPlayerPositionOnLadder_3; }
	inline void set_fPlayerPositionOnLadder_3(float value)
	{
		___fPlayerPositionOnLadder_3 = value;
	}

	inline static int32_t get_offset_of_m_bCorrectLadder_4() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___m_bCorrectLadder_4)); }
	inline bool get_m_bCorrectLadder_4() const { return ___m_bCorrectLadder_4; }
	inline bool* get_address_of_m_bCorrectLadder_4() { return &___m_bCorrectLadder_4; }
	inline void set_m_bCorrectLadder_4(bool value)
	{
		___m_bCorrectLadder_4 = value;
	}

	inline static int32_t get_offset_of_EndPos_5() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___EndPos_5)); }
	inline Transform_t362059596 * get_EndPos_5() const { return ___EndPos_5; }
	inline Transform_t362059596 ** get_address_of_EndPos_5() { return &___EndPos_5; }
	inline void set_EndPos_5(Transform_t362059596 * value)
	{
		___EndPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___EndPos_5), value);
	}

	inline static int32_t get_offset_of_Trigger_6() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___Trigger_6)); }
	inline GameObject_t2557347079 * get_Trigger_6() const { return ___Trigger_6; }
	inline GameObject_t2557347079 ** get_address_of_Trigger_6() { return &___Trigger_6; }
	inline void set_Trigger_6(GameObject_t2557347079 * value)
	{
		___Trigger_6 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_6), value);
	}

	inline static int32_t get_offset_of_m_bOnThisLadder_7() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___m_bOnThisLadder_7)); }
	inline bool get_m_bOnThisLadder_7() const { return ___m_bOnThisLadder_7; }
	inline bool* get_address_of_m_bOnThisLadder_7() { return &___m_bOnThisLadder_7; }
	inline void set_m_bOnThisLadder_7(bool value)
	{
		___m_bOnThisLadder_7 = value;
	}

	inline static int32_t get_offset_of_m_bClimbing_8() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___m_bClimbing_8)); }
	inline bool get_m_bClimbing_8() const { return ___m_bClimbing_8; }
	inline bool* get_address_of_m_bClimbing_8() { return &___m_bClimbing_8; }
	inline void set_m_bClimbing_8(bool value)
	{
		___m_bClimbing_8 = value;
	}

	inline static int32_t get_offset_of_m_bReachedTop_9() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___m_bReachedTop_9)); }
	inline bool get_m_bReachedTop_9() const { return ___m_bReachedTop_9; }
	inline bool* get_address_of_m_bReachedTop_9() { return &___m_bReachedTop_9; }
	inline void set_m_bReachedTop_9(bool value)
	{
		___m_bReachedTop_9 = value;
	}

	inline static int32_t get_offset_of_moveCap_10() { return static_cast<int32_t>(offsetof(LadderInteraction_t560741349, ___moveCap_10)); }
	inline float get_moveCap_10() const { return ___moveCap_10; }
	inline float* get_address_of_moveCap_10() { return &___moveCap_10; }
	inline void set_moveCap_10(float value)
	{
		___moveCap_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LADDERINTERACTION_T560741349_H
#ifndef L7GATECLOSETRIGGER_T558060013_H
#define L7GATECLOSETRIGGER_T558060013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7GateCloseTrigger
struct  L7GateCloseTrigger_t558060013  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L7GateCloseTrigger::m_bGate1NeedsToClose
	bool ___m_bGate1NeedsToClose_2;
	// System.Boolean L7GateCloseTrigger::m_bGate2NeedsToClose
	bool ___m_bGate2NeedsToClose_3;

public:
	inline static int32_t get_offset_of_m_bGate1NeedsToClose_2() { return static_cast<int32_t>(offsetof(L7GateCloseTrigger_t558060013, ___m_bGate1NeedsToClose_2)); }
	inline bool get_m_bGate1NeedsToClose_2() const { return ___m_bGate1NeedsToClose_2; }
	inline bool* get_address_of_m_bGate1NeedsToClose_2() { return &___m_bGate1NeedsToClose_2; }
	inline void set_m_bGate1NeedsToClose_2(bool value)
	{
		___m_bGate1NeedsToClose_2 = value;
	}

	inline static int32_t get_offset_of_m_bGate2NeedsToClose_3() { return static_cast<int32_t>(offsetof(L7GateCloseTrigger_t558060013, ___m_bGate2NeedsToClose_3)); }
	inline bool get_m_bGate2NeedsToClose_3() const { return ___m_bGate2NeedsToClose_3; }
	inline bool* get_address_of_m_bGate2NeedsToClose_3() { return &___m_bGate2NeedsToClose_3; }
	inline void set_m_bGate2NeedsToClose_3(bool value)
	{
		___m_bGate2NeedsToClose_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7GATECLOSETRIGGER_T558060013_H
#ifndef L2TUITERMINATESCENE_T3978838945_H
#define L2TUITERMINATESCENE_T3978838945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2TuiTerminateScene
struct  L2TuiTerminateScene_t3978838945  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L2TuiTerminateScene::tuiAndPlayerDistance
	float ___tuiAndPlayerDistance_2;
	// System.Single L2TuiTerminateScene::tuiToPlayerDistance
	float ___tuiToPlayerDistance_3;
	// System.Boolean L2TuiTerminateScene::m_bTuiIsFlying
	bool ___m_bTuiIsFlying_4;
	// UnityEngine.GameObject L2TuiTerminateScene::interactObject
	GameObject_t2557347079 * ___interactObject_5;
	// System.String L2TuiTerminateScene::tuiFlyHeadToID
	String_t* ___tuiFlyHeadToID_6;
	// System.Boolean L2TuiTerminateScene::m_bTuiNeedTransmit
	bool ___m_bTuiNeedTransmit_7;

public:
	inline static int32_t get_offset_of_tuiAndPlayerDistance_2() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___tuiAndPlayerDistance_2)); }
	inline float get_tuiAndPlayerDistance_2() const { return ___tuiAndPlayerDistance_2; }
	inline float* get_address_of_tuiAndPlayerDistance_2() { return &___tuiAndPlayerDistance_2; }
	inline void set_tuiAndPlayerDistance_2(float value)
	{
		___tuiAndPlayerDistance_2 = value;
	}

	inline static int32_t get_offset_of_tuiToPlayerDistance_3() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___tuiToPlayerDistance_3)); }
	inline float get_tuiToPlayerDistance_3() const { return ___tuiToPlayerDistance_3; }
	inline float* get_address_of_tuiToPlayerDistance_3() { return &___tuiToPlayerDistance_3; }
	inline void set_tuiToPlayerDistance_3(float value)
	{
		___tuiToPlayerDistance_3 = value;
	}

	inline static int32_t get_offset_of_m_bTuiIsFlying_4() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___m_bTuiIsFlying_4)); }
	inline bool get_m_bTuiIsFlying_4() const { return ___m_bTuiIsFlying_4; }
	inline bool* get_address_of_m_bTuiIsFlying_4() { return &___m_bTuiIsFlying_4; }
	inline void set_m_bTuiIsFlying_4(bool value)
	{
		___m_bTuiIsFlying_4 = value;
	}

	inline static int32_t get_offset_of_interactObject_5() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___interactObject_5)); }
	inline GameObject_t2557347079 * get_interactObject_5() const { return ___interactObject_5; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_5() { return &___interactObject_5; }
	inline void set_interactObject_5(GameObject_t2557347079 * value)
	{
		___interactObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_5), value);
	}

	inline static int32_t get_offset_of_tuiFlyHeadToID_6() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___tuiFlyHeadToID_6)); }
	inline String_t* get_tuiFlyHeadToID_6() const { return ___tuiFlyHeadToID_6; }
	inline String_t** get_address_of_tuiFlyHeadToID_6() { return &___tuiFlyHeadToID_6; }
	inline void set_tuiFlyHeadToID_6(String_t* value)
	{
		___tuiFlyHeadToID_6 = value;
		Il2CppCodeGenWriteBarrier((&___tuiFlyHeadToID_6), value);
	}

	inline static int32_t get_offset_of_m_bTuiNeedTransmit_7() { return static_cast<int32_t>(offsetof(L2TuiTerminateScene_t3978838945, ___m_bTuiNeedTransmit_7)); }
	inline bool get_m_bTuiNeedTransmit_7() const { return ___m_bTuiNeedTransmit_7; }
	inline bool* get_address_of_m_bTuiNeedTransmit_7() { return &___m_bTuiNeedTransmit_7; }
	inline void set_m_bTuiNeedTransmit_7(bool value)
	{
		___m_bTuiNeedTransmit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2TUITERMINATESCENE_T3978838945_H
#ifndef ICECAVEDOOR_T4167718126_H
#define ICECAVEDOOR_T4167718126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceCaveDoor
struct  IceCaveDoor_t4167718126  : public MonoBehaviour_t1618594486
{
public:
	// System.String IceCaveDoor::CameraID
	String_t* ___CameraID_2;
	// System.String IceCaveDoor::BirdFlyToPos
	String_t* ___BirdFlyToPos_3;
	// System.Boolean IceCaveDoor::m_birdStartFlying
	bool ___m_birdStartFlying_4;
	// System.Boolean IceCaveDoor::m_hopeTalkFinished
	bool ___m_hopeTalkFinished_5;
	// System.Boolean IceCaveDoor::m_bGotL2OpenDoorFire
	bool ___m_bGotL2OpenDoorFire_6;
	// System.Boolean IceCaveDoor::m_bDoorOpenClicked
	bool ___m_bDoorOpenClicked_7;
	// System.Boolean IceCaveDoor::m_bDoorCanOpen
	bool ___m_bDoorCanOpen_8;
	// UnityEngine.Texture2D IceCaveDoor::IceDoorMouseOverTexture
	Texture2D_t3063074017 * ___IceDoorMouseOverTexture_9;
	// UnityEngine.Texture2D IceCaveDoor::IceDoorMouseOriTexture
	Texture2D_t3063074017 * ___IceDoorMouseOriTexture_10;

public:
	inline static int32_t get_offset_of_CameraID_2() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___CameraID_2)); }
	inline String_t* get_CameraID_2() const { return ___CameraID_2; }
	inline String_t** get_address_of_CameraID_2() { return &___CameraID_2; }
	inline void set_CameraID_2(String_t* value)
	{
		___CameraID_2 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_2), value);
	}

	inline static int32_t get_offset_of_BirdFlyToPos_3() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___BirdFlyToPos_3)); }
	inline String_t* get_BirdFlyToPos_3() const { return ___BirdFlyToPos_3; }
	inline String_t** get_address_of_BirdFlyToPos_3() { return &___BirdFlyToPos_3; }
	inline void set_BirdFlyToPos_3(String_t* value)
	{
		___BirdFlyToPos_3 = value;
		Il2CppCodeGenWriteBarrier((&___BirdFlyToPos_3), value);
	}

	inline static int32_t get_offset_of_m_birdStartFlying_4() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___m_birdStartFlying_4)); }
	inline bool get_m_birdStartFlying_4() const { return ___m_birdStartFlying_4; }
	inline bool* get_address_of_m_birdStartFlying_4() { return &___m_birdStartFlying_4; }
	inline void set_m_birdStartFlying_4(bool value)
	{
		___m_birdStartFlying_4 = value;
	}

	inline static int32_t get_offset_of_m_hopeTalkFinished_5() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___m_hopeTalkFinished_5)); }
	inline bool get_m_hopeTalkFinished_5() const { return ___m_hopeTalkFinished_5; }
	inline bool* get_address_of_m_hopeTalkFinished_5() { return &___m_hopeTalkFinished_5; }
	inline void set_m_hopeTalkFinished_5(bool value)
	{
		___m_hopeTalkFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_bGotL2OpenDoorFire_6() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___m_bGotL2OpenDoorFire_6)); }
	inline bool get_m_bGotL2OpenDoorFire_6() const { return ___m_bGotL2OpenDoorFire_6; }
	inline bool* get_address_of_m_bGotL2OpenDoorFire_6() { return &___m_bGotL2OpenDoorFire_6; }
	inline void set_m_bGotL2OpenDoorFire_6(bool value)
	{
		___m_bGotL2OpenDoorFire_6 = value;
	}

	inline static int32_t get_offset_of_m_bDoorOpenClicked_7() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___m_bDoorOpenClicked_7)); }
	inline bool get_m_bDoorOpenClicked_7() const { return ___m_bDoorOpenClicked_7; }
	inline bool* get_address_of_m_bDoorOpenClicked_7() { return &___m_bDoorOpenClicked_7; }
	inline void set_m_bDoorOpenClicked_7(bool value)
	{
		___m_bDoorOpenClicked_7 = value;
	}

	inline static int32_t get_offset_of_m_bDoorCanOpen_8() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___m_bDoorCanOpen_8)); }
	inline bool get_m_bDoorCanOpen_8() const { return ___m_bDoorCanOpen_8; }
	inline bool* get_address_of_m_bDoorCanOpen_8() { return &___m_bDoorCanOpen_8; }
	inline void set_m_bDoorCanOpen_8(bool value)
	{
		___m_bDoorCanOpen_8 = value;
	}

	inline static int32_t get_offset_of_IceDoorMouseOverTexture_9() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___IceDoorMouseOverTexture_9)); }
	inline Texture2D_t3063074017 * get_IceDoorMouseOverTexture_9() const { return ___IceDoorMouseOverTexture_9; }
	inline Texture2D_t3063074017 ** get_address_of_IceDoorMouseOverTexture_9() { return &___IceDoorMouseOverTexture_9; }
	inline void set_IceDoorMouseOverTexture_9(Texture2D_t3063074017 * value)
	{
		___IceDoorMouseOverTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___IceDoorMouseOverTexture_9), value);
	}

	inline static int32_t get_offset_of_IceDoorMouseOriTexture_10() { return static_cast<int32_t>(offsetof(IceCaveDoor_t4167718126, ___IceDoorMouseOriTexture_10)); }
	inline Texture2D_t3063074017 * get_IceDoorMouseOriTexture_10() const { return ___IceDoorMouseOriTexture_10; }
	inline Texture2D_t3063074017 ** get_address_of_IceDoorMouseOriTexture_10() { return &___IceDoorMouseOriTexture_10; }
	inline void set_IceDoorMouseOriTexture_10(Texture2D_t3063074017 * value)
	{
		___IceDoorMouseOriTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___IceDoorMouseOriTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICECAVEDOOR_T4167718126_H
#ifndef EAGLESHADER_T2557250954_H
#define EAGLESHADER_T2557250954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// eagleShader
struct  eagleShader_t2557250954  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Shader eagleShader::shader1
	Shader_t1881769421 * ___shader1_2;
	// UnityEngine.Shader eagleShader::shader2
	Shader_t1881769421 * ___shader2_3;
	// UnityEngine.Renderer eagleShader::rend
	Renderer_t1418648713 * ___rend_4;

public:
	inline static int32_t get_offset_of_shader1_2() { return static_cast<int32_t>(offsetof(eagleShader_t2557250954, ___shader1_2)); }
	inline Shader_t1881769421 * get_shader1_2() const { return ___shader1_2; }
	inline Shader_t1881769421 ** get_address_of_shader1_2() { return &___shader1_2; }
	inline void set_shader1_2(Shader_t1881769421 * value)
	{
		___shader1_2 = value;
		Il2CppCodeGenWriteBarrier((&___shader1_2), value);
	}

	inline static int32_t get_offset_of_shader2_3() { return static_cast<int32_t>(offsetof(eagleShader_t2557250954, ___shader2_3)); }
	inline Shader_t1881769421 * get_shader2_3() const { return ___shader2_3; }
	inline Shader_t1881769421 ** get_address_of_shader2_3() { return &___shader2_3; }
	inline void set_shader2_3(Shader_t1881769421 * value)
	{
		___shader2_3 = value;
		Il2CppCodeGenWriteBarrier((&___shader2_3), value);
	}

	inline static int32_t get_offset_of_rend_4() { return static_cast<int32_t>(offsetof(eagleShader_t2557250954, ___rend_4)); }
	inline Renderer_t1418648713 * get_rend_4() const { return ___rend_4; }
	inline Renderer_t1418648713 ** get_address_of_rend_4() { return &___rend_4; }
	inline void set_rend_4(Renderer_t1418648713 * value)
	{
		___rend_4 = value;
		Il2CppCodeGenWriteBarrier((&___rend_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLESHADER_T2557250954_H
#ifndef EAGLEINTERACTION_T711864857_H
#define EAGLEINTERACTION_T711864857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleInteraction
struct  EagleInteraction_t711864857  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform EagleInteraction::StartPosition
	Transform_t362059596 * ___StartPosition_2;
	// UnityEngine.Transform EagleInteraction::StationaryPosition
	Transform_t362059596 * ___StationaryPosition_3;
	// UnityEngine.Transform EagleInteraction::FlyTowards
	Transform_t362059596 * ___FlyTowards_4;
	// System.Boolean EagleInteraction::m_bInteracted
	bool ___m_bInteracted_5;
	// System.Boolean EagleInteraction::m_bPlayerGotOn
	bool ___m_bPlayerGotOn_6;
	// System.Boolean EagleInteraction::m_bPlayerMoving
	bool ___m_bPlayerMoving_7;
	// System.Single EagleInteraction::m_fLerpTime
	float ___m_fLerpTime_8;
	// System.String EagleInteraction::CameraID
	String_t* ___CameraID_9;
	// System.Single EagleInteraction::fadeOutTime
	float ___fadeOutTime_10;
	// UnityEngine.GameObject EagleInteraction::eagleCutscene
	GameObject_t2557347079 * ___eagleCutscene_11;
	// UnityEngine.Texture2D EagleInteraction::EagleMouseOverTexture
	Texture2D_t3063074017 * ___EagleMouseOverTexture_12;
	// UnityEngine.Texture2D EagleInteraction::EagleMouseOriTexture
	Texture2D_t3063074017 * ___EagleMouseOriTexture_13;

public:
	inline static int32_t get_offset_of_StartPosition_2() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___StartPosition_2)); }
	inline Transform_t362059596 * get_StartPosition_2() const { return ___StartPosition_2; }
	inline Transform_t362059596 ** get_address_of_StartPosition_2() { return &___StartPosition_2; }
	inline void set_StartPosition_2(Transform_t362059596 * value)
	{
		___StartPosition_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartPosition_2), value);
	}

	inline static int32_t get_offset_of_StationaryPosition_3() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___StationaryPosition_3)); }
	inline Transform_t362059596 * get_StationaryPosition_3() const { return ___StationaryPosition_3; }
	inline Transform_t362059596 ** get_address_of_StationaryPosition_3() { return &___StationaryPosition_3; }
	inline void set_StationaryPosition_3(Transform_t362059596 * value)
	{
		___StationaryPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___StationaryPosition_3), value);
	}

	inline static int32_t get_offset_of_FlyTowards_4() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___FlyTowards_4)); }
	inline Transform_t362059596 * get_FlyTowards_4() const { return ___FlyTowards_4; }
	inline Transform_t362059596 ** get_address_of_FlyTowards_4() { return &___FlyTowards_4; }
	inline void set_FlyTowards_4(Transform_t362059596 * value)
	{
		___FlyTowards_4 = value;
		Il2CppCodeGenWriteBarrier((&___FlyTowards_4), value);
	}

	inline static int32_t get_offset_of_m_bInteracted_5() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___m_bInteracted_5)); }
	inline bool get_m_bInteracted_5() const { return ___m_bInteracted_5; }
	inline bool* get_address_of_m_bInteracted_5() { return &___m_bInteracted_5; }
	inline void set_m_bInteracted_5(bool value)
	{
		___m_bInteracted_5 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerGotOn_6() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___m_bPlayerGotOn_6)); }
	inline bool get_m_bPlayerGotOn_6() const { return ___m_bPlayerGotOn_6; }
	inline bool* get_address_of_m_bPlayerGotOn_6() { return &___m_bPlayerGotOn_6; }
	inline void set_m_bPlayerGotOn_6(bool value)
	{
		___m_bPlayerGotOn_6 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerMoving_7() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___m_bPlayerMoving_7)); }
	inline bool get_m_bPlayerMoving_7() const { return ___m_bPlayerMoving_7; }
	inline bool* get_address_of_m_bPlayerMoving_7() { return &___m_bPlayerMoving_7; }
	inline void set_m_bPlayerMoving_7(bool value)
	{
		___m_bPlayerMoving_7 = value;
	}

	inline static int32_t get_offset_of_m_fLerpTime_8() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___m_fLerpTime_8)); }
	inline float get_m_fLerpTime_8() const { return ___m_fLerpTime_8; }
	inline float* get_address_of_m_fLerpTime_8() { return &___m_fLerpTime_8; }
	inline void set_m_fLerpTime_8(float value)
	{
		___m_fLerpTime_8 = value;
	}

	inline static int32_t get_offset_of_CameraID_9() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___CameraID_9)); }
	inline String_t* get_CameraID_9() const { return ___CameraID_9; }
	inline String_t** get_address_of_CameraID_9() { return &___CameraID_9; }
	inline void set_CameraID_9(String_t* value)
	{
		___CameraID_9 = value;
		Il2CppCodeGenWriteBarrier((&___CameraID_9), value);
	}

	inline static int32_t get_offset_of_fadeOutTime_10() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___fadeOutTime_10)); }
	inline float get_fadeOutTime_10() const { return ___fadeOutTime_10; }
	inline float* get_address_of_fadeOutTime_10() { return &___fadeOutTime_10; }
	inline void set_fadeOutTime_10(float value)
	{
		___fadeOutTime_10 = value;
	}

	inline static int32_t get_offset_of_eagleCutscene_11() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___eagleCutscene_11)); }
	inline GameObject_t2557347079 * get_eagleCutscene_11() const { return ___eagleCutscene_11; }
	inline GameObject_t2557347079 ** get_address_of_eagleCutscene_11() { return &___eagleCutscene_11; }
	inline void set_eagleCutscene_11(GameObject_t2557347079 * value)
	{
		___eagleCutscene_11 = value;
		Il2CppCodeGenWriteBarrier((&___eagleCutscene_11), value);
	}

	inline static int32_t get_offset_of_EagleMouseOverTexture_12() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___EagleMouseOverTexture_12)); }
	inline Texture2D_t3063074017 * get_EagleMouseOverTexture_12() const { return ___EagleMouseOverTexture_12; }
	inline Texture2D_t3063074017 ** get_address_of_EagleMouseOverTexture_12() { return &___EagleMouseOverTexture_12; }
	inline void set_EagleMouseOverTexture_12(Texture2D_t3063074017 * value)
	{
		___EagleMouseOverTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___EagleMouseOverTexture_12), value);
	}

	inline static int32_t get_offset_of_EagleMouseOriTexture_13() { return static_cast<int32_t>(offsetof(EagleInteraction_t711864857, ___EagleMouseOriTexture_13)); }
	inline Texture2D_t3063074017 * get_EagleMouseOriTexture_13() const { return ___EagleMouseOriTexture_13; }
	inline Texture2D_t3063074017 ** get_address_of_EagleMouseOriTexture_13() { return &___EagleMouseOriTexture_13; }
	inline void set_EagleMouseOriTexture_13(Texture2D_t3063074017 * value)
	{
		___EagleMouseOriTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___EagleMouseOriTexture_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLEINTERACTION_T711864857_H
#ifndef EAGLEFLYTOPOSITION_T189743191_H
#define EAGLEFLYTOPOSITION_T189743191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleFlyToPosition
struct  EagleFlyToPosition_t189743191  : public MonoBehaviour_t1618594486
{
public:
	// System.String EagleFlyToPosition::EagleFlyCamID
	String_t* ___EagleFlyCamID_2;
	// UnityEngine.Transform EagleFlyToPosition::DownPosition
	Transform_t362059596 * ___DownPosition_3;
	// UnityEngine.Transform EagleFlyToPosition::FlyAwayPosition
	Transform_t362059596 * ___FlyAwayPosition_4;
	// UnityEngine.Transform EagleFlyToPosition::PlayerPosition
	Transform_t362059596 * ___PlayerPosition_5;
	// System.Boolean EagleFlyToPosition::m_bIsFlyingDown
	bool ___m_bIsFlyingDown_6;
	// UnityEngine.Transform EagleFlyToPosition::EagleEndPos
	Transform_t362059596 * ___EagleEndPos_7;
	// UnityEngine.Transform EagleFlyToPosition::ridePos
	Transform_t362059596 * ___ridePos_8;
	// System.Boolean EagleFlyToPosition::hasBeenInitialised
	bool ___hasBeenInitialised_9;

public:
	inline static int32_t get_offset_of_EagleFlyCamID_2() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___EagleFlyCamID_2)); }
	inline String_t* get_EagleFlyCamID_2() const { return ___EagleFlyCamID_2; }
	inline String_t** get_address_of_EagleFlyCamID_2() { return &___EagleFlyCamID_2; }
	inline void set_EagleFlyCamID_2(String_t* value)
	{
		___EagleFlyCamID_2 = value;
		Il2CppCodeGenWriteBarrier((&___EagleFlyCamID_2), value);
	}

	inline static int32_t get_offset_of_DownPosition_3() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___DownPosition_3)); }
	inline Transform_t362059596 * get_DownPosition_3() const { return ___DownPosition_3; }
	inline Transform_t362059596 ** get_address_of_DownPosition_3() { return &___DownPosition_3; }
	inline void set_DownPosition_3(Transform_t362059596 * value)
	{
		___DownPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___DownPosition_3), value);
	}

	inline static int32_t get_offset_of_FlyAwayPosition_4() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___FlyAwayPosition_4)); }
	inline Transform_t362059596 * get_FlyAwayPosition_4() const { return ___FlyAwayPosition_4; }
	inline Transform_t362059596 ** get_address_of_FlyAwayPosition_4() { return &___FlyAwayPosition_4; }
	inline void set_FlyAwayPosition_4(Transform_t362059596 * value)
	{
		___FlyAwayPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___FlyAwayPosition_4), value);
	}

	inline static int32_t get_offset_of_PlayerPosition_5() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___PlayerPosition_5)); }
	inline Transform_t362059596 * get_PlayerPosition_5() const { return ___PlayerPosition_5; }
	inline Transform_t362059596 ** get_address_of_PlayerPosition_5() { return &___PlayerPosition_5; }
	inline void set_PlayerPosition_5(Transform_t362059596 * value)
	{
		___PlayerPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerPosition_5), value);
	}

	inline static int32_t get_offset_of_m_bIsFlyingDown_6() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___m_bIsFlyingDown_6)); }
	inline bool get_m_bIsFlyingDown_6() const { return ___m_bIsFlyingDown_6; }
	inline bool* get_address_of_m_bIsFlyingDown_6() { return &___m_bIsFlyingDown_6; }
	inline void set_m_bIsFlyingDown_6(bool value)
	{
		___m_bIsFlyingDown_6 = value;
	}

	inline static int32_t get_offset_of_EagleEndPos_7() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___EagleEndPos_7)); }
	inline Transform_t362059596 * get_EagleEndPos_7() const { return ___EagleEndPos_7; }
	inline Transform_t362059596 ** get_address_of_EagleEndPos_7() { return &___EagleEndPos_7; }
	inline void set_EagleEndPos_7(Transform_t362059596 * value)
	{
		___EagleEndPos_7 = value;
		Il2CppCodeGenWriteBarrier((&___EagleEndPos_7), value);
	}

	inline static int32_t get_offset_of_ridePos_8() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___ridePos_8)); }
	inline Transform_t362059596 * get_ridePos_8() const { return ___ridePos_8; }
	inline Transform_t362059596 ** get_address_of_ridePos_8() { return &___ridePos_8; }
	inline void set_ridePos_8(Transform_t362059596 * value)
	{
		___ridePos_8 = value;
		Il2CppCodeGenWriteBarrier((&___ridePos_8), value);
	}

	inline static int32_t get_offset_of_hasBeenInitialised_9() { return static_cast<int32_t>(offsetof(EagleFlyToPosition_t189743191, ___hasBeenInitialised_9)); }
	inline bool get_hasBeenInitialised_9() const { return ___hasBeenInitialised_9; }
	inline bool* get_address_of_hasBeenInitialised_9() { return &___hasBeenInitialised_9; }
	inline void set_hasBeenInitialised_9(bool value)
	{
		___hasBeenInitialised_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLEFLYTOPOSITION_T189743191_H
#ifndef EAGLEBACKTOLEVELS_T2712814472_H
#define EAGLEBACKTOLEVELS_T2712814472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EagleBackToLevelS
struct  EagleBackToLevelS_t2712814472  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform EagleBackToLevelS::FlyTo
	Transform_t362059596 * ___FlyTo_2;
	// UnityEngine.Transform EagleBackToLevelS::WaitPos
	Transform_t362059596 * ___WaitPos_3;
	// UnityEngine.Texture2D EagleBackToLevelS::EagleMouseOverTexture
	Texture2D_t3063074017 * ___EagleMouseOverTexture_4;
	// UnityEngine.Texture2D EagleBackToLevelS::EagleMouseOriTexture
	Texture2D_t3063074017 * ___EagleMouseOriTexture_5;
	// System.Single EagleBackToLevelS::fadeOutTime
	float ___fadeOutTime_6;
	// UnityEngine.GameObject EagleBackToLevelS::eagleCutscene
	GameObject_t2557347079 * ___eagleCutscene_7;
	// UnityEngine.Vector3 EagleBackToLevelS::start
	Vector3_t1986933152  ___start_8;
	// System.Single EagleBackToLevelS::m_fTime
	float ___m_fTime_9;
	// System.Boolean EagleBackToLevelS::ReadyToLeave
	bool ___ReadyToLeave_10;
	// System.Boolean EagleBackToLevelS::PlayerAtEagle
	bool ___PlayerAtEagle_11;

public:
	inline static int32_t get_offset_of_FlyTo_2() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___FlyTo_2)); }
	inline Transform_t362059596 * get_FlyTo_2() const { return ___FlyTo_2; }
	inline Transform_t362059596 ** get_address_of_FlyTo_2() { return &___FlyTo_2; }
	inline void set_FlyTo_2(Transform_t362059596 * value)
	{
		___FlyTo_2 = value;
		Il2CppCodeGenWriteBarrier((&___FlyTo_2), value);
	}

	inline static int32_t get_offset_of_WaitPos_3() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___WaitPos_3)); }
	inline Transform_t362059596 * get_WaitPos_3() const { return ___WaitPos_3; }
	inline Transform_t362059596 ** get_address_of_WaitPos_3() { return &___WaitPos_3; }
	inline void set_WaitPos_3(Transform_t362059596 * value)
	{
		___WaitPos_3 = value;
		Il2CppCodeGenWriteBarrier((&___WaitPos_3), value);
	}

	inline static int32_t get_offset_of_EagleMouseOverTexture_4() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___EagleMouseOverTexture_4)); }
	inline Texture2D_t3063074017 * get_EagleMouseOverTexture_4() const { return ___EagleMouseOverTexture_4; }
	inline Texture2D_t3063074017 ** get_address_of_EagleMouseOverTexture_4() { return &___EagleMouseOverTexture_4; }
	inline void set_EagleMouseOverTexture_4(Texture2D_t3063074017 * value)
	{
		___EagleMouseOverTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___EagleMouseOverTexture_4), value);
	}

	inline static int32_t get_offset_of_EagleMouseOriTexture_5() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___EagleMouseOriTexture_5)); }
	inline Texture2D_t3063074017 * get_EagleMouseOriTexture_5() const { return ___EagleMouseOriTexture_5; }
	inline Texture2D_t3063074017 ** get_address_of_EagleMouseOriTexture_5() { return &___EagleMouseOriTexture_5; }
	inline void set_EagleMouseOriTexture_5(Texture2D_t3063074017 * value)
	{
		___EagleMouseOriTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___EagleMouseOriTexture_5), value);
	}

	inline static int32_t get_offset_of_fadeOutTime_6() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___fadeOutTime_6)); }
	inline float get_fadeOutTime_6() const { return ___fadeOutTime_6; }
	inline float* get_address_of_fadeOutTime_6() { return &___fadeOutTime_6; }
	inline void set_fadeOutTime_6(float value)
	{
		___fadeOutTime_6 = value;
	}

	inline static int32_t get_offset_of_eagleCutscene_7() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___eagleCutscene_7)); }
	inline GameObject_t2557347079 * get_eagleCutscene_7() const { return ___eagleCutscene_7; }
	inline GameObject_t2557347079 ** get_address_of_eagleCutscene_7() { return &___eagleCutscene_7; }
	inline void set_eagleCutscene_7(GameObject_t2557347079 * value)
	{
		___eagleCutscene_7 = value;
		Il2CppCodeGenWriteBarrier((&___eagleCutscene_7), value);
	}

	inline static int32_t get_offset_of_start_8() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___start_8)); }
	inline Vector3_t1986933152  get_start_8() const { return ___start_8; }
	inline Vector3_t1986933152 * get_address_of_start_8() { return &___start_8; }
	inline void set_start_8(Vector3_t1986933152  value)
	{
		___start_8 = value;
	}

	inline static int32_t get_offset_of_m_fTime_9() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___m_fTime_9)); }
	inline float get_m_fTime_9() const { return ___m_fTime_9; }
	inline float* get_address_of_m_fTime_9() { return &___m_fTime_9; }
	inline void set_m_fTime_9(float value)
	{
		___m_fTime_9 = value;
	}

	inline static int32_t get_offset_of_ReadyToLeave_10() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___ReadyToLeave_10)); }
	inline bool get_ReadyToLeave_10() const { return ___ReadyToLeave_10; }
	inline bool* get_address_of_ReadyToLeave_10() { return &___ReadyToLeave_10; }
	inline void set_ReadyToLeave_10(bool value)
	{
		___ReadyToLeave_10 = value;
	}

	inline static int32_t get_offset_of_PlayerAtEagle_11() { return static_cast<int32_t>(offsetof(EagleBackToLevelS_t2712814472, ___PlayerAtEagle_11)); }
	inline bool get_PlayerAtEagle_11() const { return ___PlayerAtEagle_11; }
	inline bool* get_address_of_PlayerAtEagle_11() { return &___PlayerAtEagle_11; }
	inline void set_PlayerAtEagle_11(bool value)
	{
		___PlayerAtEagle_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAGLEBACKTOLEVELS_T2712814472_H
#ifndef LADDERTRIGGER_T2224323142_H
#define LADDERTRIGGER_T2224323142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LadderTrigger
struct  LadderTrigger_t2224323142  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Texture2D LadderTrigger::MouseOverTexture
	Texture2D_t3063074017 * ___MouseOverTexture_2;
	// UnityEngine.Texture2D LadderTrigger::MouseOriTexture
	Texture2D_t3063074017 * ___MouseOriTexture_3;
	// System.Boolean LadderTrigger::closeEnough
	bool ___closeEnough_4;

public:
	inline static int32_t get_offset_of_MouseOverTexture_2() { return static_cast<int32_t>(offsetof(LadderTrigger_t2224323142, ___MouseOverTexture_2)); }
	inline Texture2D_t3063074017 * get_MouseOverTexture_2() const { return ___MouseOverTexture_2; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOverTexture_2() { return &___MouseOverTexture_2; }
	inline void set_MouseOverTexture_2(Texture2D_t3063074017 * value)
	{
		___MouseOverTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOverTexture_2), value);
	}

	inline static int32_t get_offset_of_MouseOriTexture_3() { return static_cast<int32_t>(offsetof(LadderTrigger_t2224323142, ___MouseOriTexture_3)); }
	inline Texture2D_t3063074017 * get_MouseOriTexture_3() const { return ___MouseOriTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_MouseOriTexture_3() { return &___MouseOriTexture_3; }
	inline void set_MouseOriTexture_3(Texture2D_t3063074017 * value)
	{
		___MouseOriTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___MouseOriTexture_3), value);
	}

	inline static int32_t get_offset_of_closeEnough_4() { return static_cast<int32_t>(offsetof(LadderTrigger_t2224323142, ___closeEnough_4)); }
	inline bool get_closeEnough_4() const { return ___closeEnough_4; }
	inline bool* get_address_of_closeEnough_4() { return &___closeEnough_4; }
	inline void set_closeEnough_4(bool value)
	{
		___closeEnough_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LADDERTRIGGER_T2224323142_H
#ifndef ICEDOOROBJMOVEMENT_T1143558271_H
#define ICEDOOROBJMOVEMENT_T1143558271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceDoorObjMovement
struct  IceDoorObjMovement_t1143558271  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean IceDoorObjMovement::bRotation
	bool ___bRotation_2;
	// UnityEngine.Vector3 IceDoorObjMovement::m_vTarget
	Vector3_t1986933152  ___m_vTarget_3;
	// UnityEngine.Vector3 IceDoorObjMovement::m_vMovement
	Vector3_t1986933152  ___m_vMovement_4;
	// System.Boolean IceDoorObjMovement::m_bStartMoving
	bool ___m_bStartMoving_5;
	// UnityEngine.Vector3 IceDoorObjMovement::m_vRotation
	Vector3_t1986933152  ___m_vRotation_6;
	// System.Boolean IceDoorObjMovement::m_bMovementComplete
	bool ___m_bMovementComplete_7;
	// UnityEngine.Vector3 IceDoorObjMovement::m_vOriginalPosition
	Vector3_t1986933152  ___m_vOriginalPosition_8;
	// System.Single IceDoorObjMovement::m_fMovementProgress
	float ___m_fMovementProgress_9;
	// System.Single IceDoorObjMovement::m_fRotateYValue
	float ___m_fRotateYValue_10;
	// System.Single IceDoorObjMovement::m_fAngle
	float ___m_fAngle_11;

public:
	inline static int32_t get_offset_of_bRotation_2() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___bRotation_2)); }
	inline bool get_bRotation_2() const { return ___bRotation_2; }
	inline bool* get_address_of_bRotation_2() { return &___bRotation_2; }
	inline void set_bRotation_2(bool value)
	{
		___bRotation_2 = value;
	}

	inline static int32_t get_offset_of_m_vTarget_3() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_vTarget_3)); }
	inline Vector3_t1986933152  get_m_vTarget_3() const { return ___m_vTarget_3; }
	inline Vector3_t1986933152 * get_address_of_m_vTarget_3() { return &___m_vTarget_3; }
	inline void set_m_vTarget_3(Vector3_t1986933152  value)
	{
		___m_vTarget_3 = value;
	}

	inline static int32_t get_offset_of_m_vMovement_4() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_vMovement_4)); }
	inline Vector3_t1986933152  get_m_vMovement_4() const { return ___m_vMovement_4; }
	inline Vector3_t1986933152 * get_address_of_m_vMovement_4() { return &___m_vMovement_4; }
	inline void set_m_vMovement_4(Vector3_t1986933152  value)
	{
		___m_vMovement_4 = value;
	}

	inline static int32_t get_offset_of_m_bStartMoving_5() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_bStartMoving_5)); }
	inline bool get_m_bStartMoving_5() const { return ___m_bStartMoving_5; }
	inline bool* get_address_of_m_bStartMoving_5() { return &___m_bStartMoving_5; }
	inline void set_m_bStartMoving_5(bool value)
	{
		___m_bStartMoving_5 = value;
	}

	inline static int32_t get_offset_of_m_vRotation_6() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_vRotation_6)); }
	inline Vector3_t1986933152  get_m_vRotation_6() const { return ___m_vRotation_6; }
	inline Vector3_t1986933152 * get_address_of_m_vRotation_6() { return &___m_vRotation_6; }
	inline void set_m_vRotation_6(Vector3_t1986933152  value)
	{
		___m_vRotation_6 = value;
	}

	inline static int32_t get_offset_of_m_bMovementComplete_7() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_bMovementComplete_7)); }
	inline bool get_m_bMovementComplete_7() const { return ___m_bMovementComplete_7; }
	inline bool* get_address_of_m_bMovementComplete_7() { return &___m_bMovementComplete_7; }
	inline void set_m_bMovementComplete_7(bool value)
	{
		___m_bMovementComplete_7 = value;
	}

	inline static int32_t get_offset_of_m_vOriginalPosition_8() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_vOriginalPosition_8)); }
	inline Vector3_t1986933152  get_m_vOriginalPosition_8() const { return ___m_vOriginalPosition_8; }
	inline Vector3_t1986933152 * get_address_of_m_vOriginalPosition_8() { return &___m_vOriginalPosition_8; }
	inline void set_m_vOriginalPosition_8(Vector3_t1986933152  value)
	{
		___m_vOriginalPosition_8 = value;
	}

	inline static int32_t get_offset_of_m_fMovementProgress_9() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_fMovementProgress_9)); }
	inline float get_m_fMovementProgress_9() const { return ___m_fMovementProgress_9; }
	inline float* get_address_of_m_fMovementProgress_9() { return &___m_fMovementProgress_9; }
	inline void set_m_fMovementProgress_9(float value)
	{
		___m_fMovementProgress_9 = value;
	}

	inline static int32_t get_offset_of_m_fRotateYValue_10() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_fRotateYValue_10)); }
	inline float get_m_fRotateYValue_10() const { return ___m_fRotateYValue_10; }
	inline float* get_address_of_m_fRotateYValue_10() { return &___m_fRotateYValue_10; }
	inline void set_m_fRotateYValue_10(float value)
	{
		___m_fRotateYValue_10 = value;
	}

	inline static int32_t get_offset_of_m_fAngle_11() { return static_cast<int32_t>(offsetof(IceDoorObjMovement_t1143558271, ___m_fAngle_11)); }
	inline float get_m_fAngle_11() const { return ___m_fAngle_11; }
	inline float* get_address_of_m_fAngle_11() { return &___m_fAngle_11; }
	inline void set_m_fAngle_11(float value)
	{
		___m_fAngle_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICEDOOROBJMOVEMENT_T1143558271_H
#ifndef L1DETECTTUIFLYDOWN_T2612992016_H
#define L1DETECTTUIFLYDOWN_T2612992016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L1DetectTuiFlyDown
struct  L1DetectTuiFlyDown_t2612992016  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L1DetectTuiFlyDown::m_bTuiConversationTriggered
	bool ___m_bTuiConversationTriggered_2;
	// System.Boolean L1DetectTuiFlyDown::m_bFirstConversationFinished
	bool ___m_bFirstConversationFinished_3;
	// System.Boolean L1DetectTuiFlyDown::m_bSecondConversationFinished
	bool ___m_bSecondConversationFinished_4;
	// System.Boolean L1DetectTuiFlyDown::m_bPool3MiniGameFinished
	bool ___m_bPool3MiniGameFinished_5;
	// System.Boolean L1DetectTuiFlyDown::m_bMiniGameConversationtriggered
	bool ___m_bMiniGameConversationtriggered_6;

public:
	inline static int32_t get_offset_of_m_bTuiConversationTriggered_2() { return static_cast<int32_t>(offsetof(L1DetectTuiFlyDown_t2612992016, ___m_bTuiConversationTriggered_2)); }
	inline bool get_m_bTuiConversationTriggered_2() const { return ___m_bTuiConversationTriggered_2; }
	inline bool* get_address_of_m_bTuiConversationTriggered_2() { return &___m_bTuiConversationTriggered_2; }
	inline void set_m_bTuiConversationTriggered_2(bool value)
	{
		___m_bTuiConversationTriggered_2 = value;
	}

	inline static int32_t get_offset_of_m_bFirstConversationFinished_3() { return static_cast<int32_t>(offsetof(L1DetectTuiFlyDown_t2612992016, ___m_bFirstConversationFinished_3)); }
	inline bool get_m_bFirstConversationFinished_3() const { return ___m_bFirstConversationFinished_3; }
	inline bool* get_address_of_m_bFirstConversationFinished_3() { return &___m_bFirstConversationFinished_3; }
	inline void set_m_bFirstConversationFinished_3(bool value)
	{
		___m_bFirstConversationFinished_3 = value;
	}

	inline static int32_t get_offset_of_m_bSecondConversationFinished_4() { return static_cast<int32_t>(offsetof(L1DetectTuiFlyDown_t2612992016, ___m_bSecondConversationFinished_4)); }
	inline bool get_m_bSecondConversationFinished_4() const { return ___m_bSecondConversationFinished_4; }
	inline bool* get_address_of_m_bSecondConversationFinished_4() { return &___m_bSecondConversationFinished_4; }
	inline void set_m_bSecondConversationFinished_4(bool value)
	{
		___m_bSecondConversationFinished_4 = value;
	}

	inline static int32_t get_offset_of_m_bPool3MiniGameFinished_5() { return static_cast<int32_t>(offsetof(L1DetectTuiFlyDown_t2612992016, ___m_bPool3MiniGameFinished_5)); }
	inline bool get_m_bPool3MiniGameFinished_5() const { return ___m_bPool3MiniGameFinished_5; }
	inline bool* get_address_of_m_bPool3MiniGameFinished_5() { return &___m_bPool3MiniGameFinished_5; }
	inline void set_m_bPool3MiniGameFinished_5(bool value)
	{
		___m_bPool3MiniGameFinished_5 = value;
	}

	inline static int32_t get_offset_of_m_bMiniGameConversationtriggered_6() { return static_cast<int32_t>(offsetof(L1DetectTuiFlyDown_t2612992016, ___m_bMiniGameConversationtriggered_6)); }
	inline bool get_m_bMiniGameConversationtriggered_6() const { return ___m_bMiniGameConversationtriggered_6; }
	inline bool* get_address_of_m_bMiniGameConversationtriggered_6() { return &___m_bMiniGameConversationtriggered_6; }
	inline void set_m_bMiniGameConversationtriggered_6(bool value)
	{
		___m_bMiniGameConversationtriggered_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L1DETECTTUIFLYDOWN_T2612992016_H
#ifndef SPARXLOGOSCREEN_T2754749120_H
#define SPARXLOGOSCREEN_T2754749120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SparxLogoScreen
struct  SparxLogoScreen_t2754749120  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SparxLogoScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Boolean SparxLogoScreen::m_bLogoFadeInStart
	bool ___m_bLogoFadeInStart_3;
	// System.Single SparxLogoScreen::m_fFadeInStartTime
	float ___m_fFadeInStartTime_4;
	// System.Single SparxLogoScreen::m_fLogoStayTime
	float ___m_fLogoStayTime_5;
	// System.Boolean SparxLogoScreen::m_bLogoFadeOutStart
	bool ___m_bLogoFadeOutStart_6;
	// System.Single SparxLogoScreen::m_fFadeOutTime
	float ___m_fFadeOutTime_7;
	// System.Single SparxLogoScreen::m_fFadeInLogoAlpha
	float ___m_fFadeInLogoAlpha_8;
	// System.Single SparxLogoScreen::m_fFadeOutLogoAlpha
	float ___m_fFadeOutLogoAlpha_9;
	// System.Boolean SparxLogoScreen::m_bScriptIsKilled
	bool ___m_bScriptIsKilled_10;
	// System.Boolean SparxLogoScreen::m_bGotTokenDone
	bool ___m_bGotTokenDone_11;
	// CapturedDataInput SparxLogoScreen::instance
	CapturedDataInput_t2616152122 * ___instance_12;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_bLogoFadeInStart_3() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_bLogoFadeInStart_3)); }
	inline bool get_m_bLogoFadeInStart_3() const { return ___m_bLogoFadeInStart_3; }
	inline bool* get_address_of_m_bLogoFadeInStart_3() { return &___m_bLogoFadeInStart_3; }
	inline void set_m_bLogoFadeInStart_3(bool value)
	{
		___m_bLogoFadeInStart_3 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_4() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_fFadeInStartTime_4)); }
	inline float get_m_fFadeInStartTime_4() const { return ___m_fFadeInStartTime_4; }
	inline float* get_address_of_m_fFadeInStartTime_4() { return &___m_fFadeInStartTime_4; }
	inline void set_m_fFadeInStartTime_4(float value)
	{
		___m_fFadeInStartTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fLogoStayTime_5() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_fLogoStayTime_5)); }
	inline float get_m_fLogoStayTime_5() const { return ___m_fLogoStayTime_5; }
	inline float* get_address_of_m_fLogoStayTime_5() { return &___m_fLogoStayTime_5; }
	inline void set_m_fLogoStayTime_5(float value)
	{
		___m_fLogoStayTime_5 = value;
	}

	inline static int32_t get_offset_of_m_bLogoFadeOutStart_6() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_bLogoFadeOutStart_6)); }
	inline bool get_m_bLogoFadeOutStart_6() const { return ___m_bLogoFadeOutStart_6; }
	inline bool* get_address_of_m_bLogoFadeOutStart_6() { return &___m_bLogoFadeOutStart_6; }
	inline void set_m_bLogoFadeOutStart_6(bool value)
	{
		___m_bLogoFadeOutStart_6 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutTime_7() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_fFadeOutTime_7)); }
	inline float get_m_fFadeOutTime_7() const { return ___m_fFadeOutTime_7; }
	inline float* get_address_of_m_fFadeOutTime_7() { return &___m_fFadeOutTime_7; }
	inline void set_m_fFadeOutTime_7(float value)
	{
		___m_fFadeOutTime_7 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInLogoAlpha_8() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_fFadeInLogoAlpha_8)); }
	inline float get_m_fFadeInLogoAlpha_8() const { return ___m_fFadeInLogoAlpha_8; }
	inline float* get_address_of_m_fFadeInLogoAlpha_8() { return &___m_fFadeInLogoAlpha_8; }
	inline void set_m_fFadeInLogoAlpha_8(float value)
	{
		___m_fFadeInLogoAlpha_8 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutLogoAlpha_9() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_fFadeOutLogoAlpha_9)); }
	inline float get_m_fFadeOutLogoAlpha_9() const { return ___m_fFadeOutLogoAlpha_9; }
	inline float* get_address_of_m_fFadeOutLogoAlpha_9() { return &___m_fFadeOutLogoAlpha_9; }
	inline void set_m_fFadeOutLogoAlpha_9(float value)
	{
		___m_fFadeOutLogoAlpha_9 = value;
	}

	inline static int32_t get_offset_of_m_bScriptIsKilled_10() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_bScriptIsKilled_10)); }
	inline bool get_m_bScriptIsKilled_10() const { return ___m_bScriptIsKilled_10; }
	inline bool* get_address_of_m_bScriptIsKilled_10() { return &___m_bScriptIsKilled_10; }
	inline void set_m_bScriptIsKilled_10(bool value)
	{
		___m_bScriptIsKilled_10 = value;
	}

	inline static int32_t get_offset_of_m_bGotTokenDone_11() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___m_bGotTokenDone_11)); }
	inline bool get_m_bGotTokenDone_11() const { return ___m_bGotTokenDone_11; }
	inline bool* get_address_of_m_bGotTokenDone_11() { return &___m_bGotTokenDone_11; }
	inline void set_m_bGotTokenDone_11(bool value)
	{
		___m_bGotTokenDone_11 = value;
	}

	inline static int32_t get_offset_of_instance_12() { return static_cast<int32_t>(offsetof(SparxLogoScreen_t2754749120, ___instance_12)); }
	inline CapturedDataInput_t2616152122 * get_instance_12() const { return ___instance_12; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_12() { return &___instance_12; }
	inline void set_instance_12(CapturedDataInput_t2616152122 * value)
	{
		___instance_12 = value;
		Il2CppCodeGenWriteBarrier((&___instance_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARXLOGOSCREEN_T2754749120_H
#ifndef L5NOTEBOOK_T443533060_H
#define L5NOTEBOOK_T443533060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5NoteBook
struct  L5NoteBook_t443533060  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L5NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String L5NoteBook::text1
	String_t* ___text1_3;
	// System.String L5NoteBook::text2
	String_t* ___text2_4;
	// System.String L5NoteBook::text3
	String_t* ___text3_5;
	// CapturedDataInput L5NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_6;
	// UnityEngine.Texture2D L5NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_7;
	// System.Int32 L5NoteBook::m_iIndex
	int32_t ___m_iIndex_8;
	// System.String L5NoteBook::failedFileName
	String_t* ___failedFileName_9;
	// UnityEngine.GameObject L5NoteBook::menu
	GameObject_t2557347079 * ___menu_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___instance_6)); }
	inline CapturedDataInput_t2616152122 * get_instance_6() const { return ___instance_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(CapturedDataInput_t2616152122 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_7() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___m_tNoteBook_7)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_7() const { return ___m_tNoteBook_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_7() { return &___m_tNoteBook_7; }
	inline void set_m_tNoteBook_7(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_7), value);
	}

	inline static int32_t get_offset_of_m_iIndex_8() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___m_iIndex_8)); }
	inline int32_t get_m_iIndex_8() const { return ___m_iIndex_8; }
	inline int32_t* get_address_of_m_iIndex_8() { return &___m_iIndex_8; }
	inline void set_m_iIndex_8(int32_t value)
	{
		___m_iIndex_8 = value;
	}

	inline static int32_t get_offset_of_failedFileName_9() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___failedFileName_9)); }
	inline String_t* get_failedFileName_9() const { return ___failedFileName_9; }
	inline String_t** get_address_of_failedFileName_9() { return &___failedFileName_9; }
	inline void set_failedFileName_9(String_t* value)
	{
		___failedFileName_9 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_9), value);
	}

	inline static int32_t get_offset_of_menu_10() { return static_cast<int32_t>(offsetof(L5NoteBook_t443533060, ___menu_10)); }
	inline GameObject_t2557347079 * get_menu_10() const { return ___menu_10; }
	inline GameObject_t2557347079 ** get_address_of_menu_10() { return &___menu_10; }
	inline void set_menu_10(GameObject_t2557347079 * value)
	{
		___menu_10 = value;
		Il2CppCodeGenWriteBarrier((&___menu_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5NOTEBOOK_T443533060_H
#ifndef L4NOTEBOOK_T2932888333_H
#define L4NOTEBOOK_T2932888333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L4NoteBook
struct  L4NoteBook_t2932888333  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L4NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String L4NoteBook::text1
	String_t* ___text1_3;
	// System.String L4NoteBook::text2
	String_t* ___text2_4;
	// System.String L4NoteBook::text3
	String_t* ___text3_5;
	// System.String L4NoteBook::text4
	String_t* ___text4_6;
	// System.String L4NoteBook::text5
	String_t* ___text5_7;
	// CapturedDataInput L4NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_8;
	// UnityEngine.Texture2D L4NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_9;
	// System.Int32 L4NoteBook::m_iIndex
	int32_t ___m_iIndex_10;
	// System.String L4NoteBook::failedFileName
	String_t* ___failedFileName_11;
	// UnityEngine.GameObject L4NoteBook::menu
	GameObject_t2557347079 * ___menu_12;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_text4_6() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___text4_6)); }
	inline String_t* get_text4_6() const { return ___text4_6; }
	inline String_t** get_address_of_text4_6() { return &___text4_6; }
	inline void set_text4_6(String_t* value)
	{
		___text4_6 = value;
		Il2CppCodeGenWriteBarrier((&___text4_6), value);
	}

	inline static int32_t get_offset_of_text5_7() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___text5_7)); }
	inline String_t* get_text5_7() const { return ___text5_7; }
	inline String_t** get_address_of_text5_7() { return &___text5_7; }
	inline void set_text5_7(String_t* value)
	{
		___text5_7 = value;
		Il2CppCodeGenWriteBarrier((&___text5_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___instance_8)); }
	inline CapturedDataInput_t2616152122 * get_instance_8() const { return ___instance_8; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(CapturedDataInput_t2616152122 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_9() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___m_tNoteBook_9)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_9() const { return ___m_tNoteBook_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_9() { return &___m_tNoteBook_9; }
	inline void set_m_tNoteBook_9(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_9), value);
	}

	inline static int32_t get_offset_of_m_iIndex_10() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___m_iIndex_10)); }
	inline int32_t get_m_iIndex_10() const { return ___m_iIndex_10; }
	inline int32_t* get_address_of_m_iIndex_10() { return &___m_iIndex_10; }
	inline void set_m_iIndex_10(int32_t value)
	{
		___m_iIndex_10 = value;
	}

	inline static int32_t get_offset_of_failedFileName_11() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___failedFileName_11)); }
	inline String_t* get_failedFileName_11() const { return ___failedFileName_11; }
	inline String_t** get_address_of_failedFileName_11() { return &___failedFileName_11; }
	inline void set_failedFileName_11(String_t* value)
	{
		___failedFileName_11 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_11), value);
	}

	inline static int32_t get_offset_of_menu_12() { return static_cast<int32_t>(offsetof(L4NoteBook_t2932888333, ___menu_12)); }
	inline GameObject_t2557347079 * get_menu_12() const { return ___menu_12; }
	inline GameObject_t2557347079 ** get_address_of_menu_12() { return &___menu_12; }
	inline void set_menu_12(GameObject_t2557347079 * value)
	{
		___menu_12 = value;
		Il2CppCodeGenWriteBarrier((&___menu_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L4NOTEBOOK_T2932888333_H
#ifndef L3NOTEBOOK_T121990616_H
#define L3NOTEBOOK_T121990616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L3NoteBook
struct  L3NoteBook_t121990616  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L3NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// CapturedDataInput L3NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_3;
	// UnityEngine.Texture2D L3NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_4;
	// System.Int32 L3NoteBook::m_iIndex
	int32_t ___m_iIndex_5;
	// System.String L3NoteBook::failedFileName
	String_t* ___failedFileName_6;
	// UnityEngine.GameObject L3NoteBook::menu
	GameObject_t2557347079 * ___menu_7;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___instance_3)); }
	inline CapturedDataInput_t2616152122 * get_instance_3() const { return ___instance_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CapturedDataInput_t2616152122 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_4() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___m_tNoteBook_4)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_4() const { return ___m_tNoteBook_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_4() { return &___m_tNoteBook_4; }
	inline void set_m_tNoteBook_4(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_4), value);
	}

	inline static int32_t get_offset_of_m_iIndex_5() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___m_iIndex_5)); }
	inline int32_t get_m_iIndex_5() const { return ___m_iIndex_5; }
	inline int32_t* get_address_of_m_iIndex_5() { return &___m_iIndex_5; }
	inline void set_m_iIndex_5(int32_t value)
	{
		___m_iIndex_5 = value;
	}

	inline static int32_t get_offset_of_failedFileName_6() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___failedFileName_6)); }
	inline String_t* get_failedFileName_6() const { return ___failedFileName_6; }
	inline String_t** get_address_of_failedFileName_6() { return &___failedFileName_6; }
	inline void set_failedFileName_6(String_t* value)
	{
		___failedFileName_6 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_6), value);
	}

	inline static int32_t get_offset_of_menu_7() { return static_cast<int32_t>(offsetof(L3NoteBook_t121990616, ___menu_7)); }
	inline GameObject_t2557347079 * get_menu_7() const { return ___menu_7; }
	inline GameObject_t2557347079 ** get_address_of_menu_7() { return &___menu_7; }
	inline void set_menu_7(GameObject_t2557347079 * value)
	{
		___menu_7 = value;
		Il2CppCodeGenWriteBarrier((&___menu_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L3NOTEBOOK_T121990616_H
#ifndef L2NOTEBOOK_T3759025291_H
#define L2NOTEBOOK_T3759025291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L2NoteBook
struct  L2NoteBook_t3759025291  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L2NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String L2NoteBook::text1
	String_t* ___text1_3;
	// System.String L2NoteBook::text2
	String_t* ___text2_4;
	// System.String L2NoteBook::text3
	String_t* ___text3_5;
	// CapturedDataInput L2NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_6;
	// UnityEngine.Texture2D L2NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_7;
	// System.Int32 L2NoteBook::m_iIndex
	int32_t ___m_iIndex_8;
	// System.String L2NoteBook::failedFileName
	String_t* ___failedFileName_9;
	// UnityEngine.GameObject L2NoteBook::menu
	GameObject_t2557347079 * ___menu_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___instance_6)); }
	inline CapturedDataInput_t2616152122 * get_instance_6() const { return ___instance_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(CapturedDataInput_t2616152122 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_7() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___m_tNoteBook_7)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_7() const { return ___m_tNoteBook_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_7() { return &___m_tNoteBook_7; }
	inline void set_m_tNoteBook_7(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_7), value);
	}

	inline static int32_t get_offset_of_m_iIndex_8() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___m_iIndex_8)); }
	inline int32_t get_m_iIndex_8() const { return ___m_iIndex_8; }
	inline int32_t* get_address_of_m_iIndex_8() { return &___m_iIndex_8; }
	inline void set_m_iIndex_8(int32_t value)
	{
		___m_iIndex_8 = value;
	}

	inline static int32_t get_offset_of_failedFileName_9() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___failedFileName_9)); }
	inline String_t* get_failedFileName_9() const { return ___failedFileName_9; }
	inline String_t** get_address_of_failedFileName_9() { return &___failedFileName_9; }
	inline void set_failedFileName_9(String_t* value)
	{
		___failedFileName_9 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_9), value);
	}

	inline static int32_t get_offset_of_menu_10() { return static_cast<int32_t>(offsetof(L2NoteBook_t3759025291, ___menu_10)); }
	inline GameObject_t2557347079 * get_menu_10() const { return ___menu_10; }
	inline GameObject_t2557347079 ** get_address_of_menu_10() { return &___menu_10; }
	inline void set_menu_10(GameObject_t2557347079 * value)
	{
		___menu_10 = value;
		Il2CppCodeGenWriteBarrier((&___menu_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L2NOTEBOOK_T3759025291_H
#ifndef L1NOTEBOOK_T1106537385_H
#define L1NOTEBOOK_T1106537385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L1NoteBook
struct  L1NoteBook_t1106537385  : public MonoBehaviour_t1618594486
{
public:
	// CapturedDataInput L1NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_2;
	// UnityEngine.Texture2D L1NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_3;
	// System.Int32 L1NoteBook::m_iIndex
	int32_t ___m_iIndex_4;
	// System.String L1NoteBook::failedFileName
	String_t* ___failedFileName_5;
	// UnityEngine.GameObject L1NoteBook::menu
	GameObject_t2557347079 * ___menu_6;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(L1NoteBook_t1106537385, ___instance_2)); }
	inline CapturedDataInput_t2616152122 * get_instance_2() const { return ___instance_2; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CapturedDataInput_t2616152122 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_3() { return static_cast<int32_t>(offsetof(L1NoteBook_t1106537385, ___m_tNoteBook_3)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_3() const { return ___m_tNoteBook_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_3() { return &___m_tNoteBook_3; }
	inline void set_m_tNoteBook_3(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_3), value);
	}

	inline static int32_t get_offset_of_m_iIndex_4() { return static_cast<int32_t>(offsetof(L1NoteBook_t1106537385, ___m_iIndex_4)); }
	inline int32_t get_m_iIndex_4() const { return ___m_iIndex_4; }
	inline int32_t* get_address_of_m_iIndex_4() { return &___m_iIndex_4; }
	inline void set_m_iIndex_4(int32_t value)
	{
		___m_iIndex_4 = value;
	}

	inline static int32_t get_offset_of_failedFileName_5() { return static_cast<int32_t>(offsetof(L1NoteBook_t1106537385, ___failedFileName_5)); }
	inline String_t* get_failedFileName_5() const { return ___failedFileName_5; }
	inline String_t** get_address_of_failedFileName_5() { return &___failedFileName_5; }
	inline void set_failedFileName_5(String_t* value)
	{
		___failedFileName_5 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_5), value);
	}

	inline static int32_t get_offset_of_menu_6() { return static_cast<int32_t>(offsetof(L1NoteBook_t1106537385, ___menu_6)); }
	inline GameObject_t2557347079 * get_menu_6() const { return ___menu_6; }
	inline GameObject_t2557347079 ** get_address_of_menu_6() { return &___menu_6; }
	inline void set_menu_6(GameObject_t2557347079 * value)
	{
		___menu_6 = value;
		Il2CppCodeGenWriteBarrier((&___menu_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L1NOTEBOOK_T1106537385_H
#ifndef MOUSECURSORCHANGE_T4178418145_H
#define MOUSECURSORCHANGE_T4178418145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MouseCursorChange
struct  MouseCursorChange_t4178418145  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Texture2D MouseCursorChange::cursorTexture
	Texture2D_t3063074017 * ___cursorTexture_3;
	// System.Boolean MouseCursorChange::ccEnabled
	bool ___ccEnabled_4;

public:
	inline static int32_t get_offset_of_cursorTexture_3() { return static_cast<int32_t>(offsetof(MouseCursorChange_t4178418145, ___cursorTexture_3)); }
	inline Texture2D_t3063074017 * get_cursorTexture_3() const { return ___cursorTexture_3; }
	inline Texture2D_t3063074017 ** get_address_of_cursorTexture_3() { return &___cursorTexture_3; }
	inline void set_cursorTexture_3(Texture2D_t3063074017 * value)
	{
		___cursorTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___cursorTexture_3), value);
	}

	inline static int32_t get_offset_of_ccEnabled_4() { return static_cast<int32_t>(offsetof(MouseCursorChange_t4178418145, ___ccEnabled_4)); }
	inline bool get_ccEnabled_4() const { return ___ccEnabled_4; }
	inline bool* get_address_of_ccEnabled_4() { return &___ccEnabled_4; }
	inline void set_ccEnabled_4(bool value)
	{
		___ccEnabled_4 = value;
	}
};

struct MouseCursorChange_t4178418145_StaticFields
{
public:
	// MouseCursorChange MouseCursorChange::instanceRef
	MouseCursorChange_t4178418145 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(MouseCursorChange_t4178418145_StaticFields, ___instanceRef_2)); }
	inline MouseCursorChange_t4178418145 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline MouseCursorChange_t4178418145 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(MouseCursorChange_t4178418145 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSECURSORCHANGE_T4178418145_H
#ifndef LANTERNHELP_T4264799245_H
#define LANTERNHELP_T4264799245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanternHelp
struct  LanternHelp_t4264799245  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean LanternHelp::bActivated
	bool ___bActivated_2;
	// UnityEngine.GUISkin LanternHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_3;
	// System.String LanternHelp::Text
	String_t* ___Text_4;

public:
	inline static int32_t get_offset_of_bActivated_2() { return static_cast<int32_t>(offsetof(LanternHelp_t4264799245, ___bActivated_2)); }
	inline bool get_bActivated_2() const { return ___bActivated_2; }
	inline bool* get_address_of_bActivated_2() { return &___bActivated_2; }
	inline void set_bActivated_2(bool value)
	{
		___bActivated_2 = value;
	}

	inline static int32_t get_offset_of_GUIskin_3() { return static_cast<int32_t>(offsetof(LanternHelp_t4264799245, ___GUIskin_3)); }
	inline GUISkin_t2122630221 * get_GUIskin_3() const { return ___GUIskin_3; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_3() { return &___GUIskin_3; }
	inline void set_GUIskin_3(GUISkin_t2122630221 * value)
	{
		___GUIskin_3 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(LanternHelp_t4264799245, ___Text_4)); }
	inline String_t* get_Text_4() const { return ___Text_4; }
	inline String_t** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(String_t* value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANTERNHELP_T4264799245_H
#ifndef L7WEATHERCHANGE_T3480560370_H
#define L7WEATHERCHANGE_T3480560370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7WeatherChange
struct  L7WeatherChange_t3480560370  : public MonoBehaviour_t1618594486
{
public:
	// System.Single L7WeatherChange::m_fFadeInStartTime
	float ___m_fFadeInStartTime_2;
	// System.Single L7WeatherChange::m_fFadeOutStartTime
	float ___m_fFadeOutStartTime_3;
	// System.Single L7WeatherChange::m_fWaitTime
	float ___m_fWaitTime_4;
	// System.Single L7WeatherChange::m_fLightMainIntensity
	float ___m_fLightMainIntensity_5;
	// System.Single L7WeatherChange::m_fLightRimIntensity
	float ___m_fLightRimIntensity_6;
	// System.Single L7WeatherChange::m_fFogDensity
	float ___m_fFogDensity_7;
	// System.Single L7WeatherChange::m_fTime
	float ___m_fTime_8;
	// System.Boolean L7WeatherChange::m_bFadeInFinished
	bool ___m_bFadeInFinished_9;
	// System.Boolean L7WeatherChange::m_bPlayerStartsWait
	bool ___m_bPlayerStartsWait_10;
	// System.Boolean L7WeatherChange::m_bFadeOutStarted
	bool ___m_bFadeOutStarted_11;
	// System.Boolean L7WeatherChange::m_bTuiIsFlyingDown
	bool ___m_bTuiIsFlyingDown_12;
	// System.Boolean L7WeatherChange::paused
	bool ___paused_13;

public:
	inline static int32_t get_offset_of_m_fFadeInStartTime_2() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fFadeInStartTime_2)); }
	inline float get_m_fFadeInStartTime_2() const { return ___m_fFadeInStartTime_2; }
	inline float* get_address_of_m_fFadeInStartTime_2() { return &___m_fFadeInStartTime_2; }
	inline void set_m_fFadeInStartTime_2(float value)
	{
		___m_fFadeInStartTime_2 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutStartTime_3() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fFadeOutStartTime_3)); }
	inline float get_m_fFadeOutStartTime_3() const { return ___m_fFadeOutStartTime_3; }
	inline float* get_address_of_m_fFadeOutStartTime_3() { return &___m_fFadeOutStartTime_3; }
	inline void set_m_fFadeOutStartTime_3(float value)
	{
		___m_fFadeOutStartTime_3 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTime_4() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fWaitTime_4)); }
	inline float get_m_fWaitTime_4() const { return ___m_fWaitTime_4; }
	inline float* get_address_of_m_fWaitTime_4() { return &___m_fWaitTime_4; }
	inline void set_m_fWaitTime_4(float value)
	{
		___m_fWaitTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fLightMainIntensity_5() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fLightMainIntensity_5)); }
	inline float get_m_fLightMainIntensity_5() const { return ___m_fLightMainIntensity_5; }
	inline float* get_address_of_m_fLightMainIntensity_5() { return &___m_fLightMainIntensity_5; }
	inline void set_m_fLightMainIntensity_5(float value)
	{
		___m_fLightMainIntensity_5 = value;
	}

	inline static int32_t get_offset_of_m_fLightRimIntensity_6() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fLightRimIntensity_6)); }
	inline float get_m_fLightRimIntensity_6() const { return ___m_fLightRimIntensity_6; }
	inline float* get_address_of_m_fLightRimIntensity_6() { return &___m_fLightRimIntensity_6; }
	inline void set_m_fLightRimIntensity_6(float value)
	{
		___m_fLightRimIntensity_6 = value;
	}

	inline static int32_t get_offset_of_m_fFogDensity_7() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fFogDensity_7)); }
	inline float get_m_fFogDensity_7() const { return ___m_fFogDensity_7; }
	inline float* get_address_of_m_fFogDensity_7() { return &___m_fFogDensity_7; }
	inline void set_m_fFogDensity_7(float value)
	{
		___m_fFogDensity_7 = value;
	}

	inline static int32_t get_offset_of_m_fTime_8() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_fTime_8)); }
	inline float get_m_fTime_8() const { return ___m_fTime_8; }
	inline float* get_address_of_m_fTime_8() { return &___m_fTime_8; }
	inline void set_m_fTime_8(float value)
	{
		___m_fTime_8 = value;
	}

	inline static int32_t get_offset_of_m_bFadeInFinished_9() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_bFadeInFinished_9)); }
	inline bool get_m_bFadeInFinished_9() const { return ___m_bFadeInFinished_9; }
	inline bool* get_address_of_m_bFadeInFinished_9() { return &___m_bFadeInFinished_9; }
	inline void set_m_bFadeInFinished_9(bool value)
	{
		___m_bFadeInFinished_9 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerStartsWait_10() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_bPlayerStartsWait_10)); }
	inline bool get_m_bPlayerStartsWait_10() const { return ___m_bPlayerStartsWait_10; }
	inline bool* get_address_of_m_bPlayerStartsWait_10() { return &___m_bPlayerStartsWait_10; }
	inline void set_m_bPlayerStartsWait_10(bool value)
	{
		___m_bPlayerStartsWait_10 = value;
	}

	inline static int32_t get_offset_of_m_bFadeOutStarted_11() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_bFadeOutStarted_11)); }
	inline bool get_m_bFadeOutStarted_11() const { return ___m_bFadeOutStarted_11; }
	inline bool* get_address_of_m_bFadeOutStarted_11() { return &___m_bFadeOutStarted_11; }
	inline void set_m_bFadeOutStarted_11(bool value)
	{
		___m_bFadeOutStarted_11 = value;
	}

	inline static int32_t get_offset_of_m_bTuiIsFlyingDown_12() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___m_bTuiIsFlyingDown_12)); }
	inline bool get_m_bTuiIsFlyingDown_12() const { return ___m_bTuiIsFlyingDown_12; }
	inline bool* get_address_of_m_bTuiIsFlyingDown_12() { return &___m_bTuiIsFlyingDown_12; }
	inline void set_m_bTuiIsFlyingDown_12(bool value)
	{
		___m_bTuiIsFlyingDown_12 = value;
	}

	inline static int32_t get_offset_of_paused_13() { return static_cast<int32_t>(offsetof(L7WeatherChange_t3480560370, ___paused_13)); }
	inline bool get_paused_13() const { return ___paused_13; }
	inline bool* get_address_of_paused_13() { return &___paused_13; }
	inline void set_paused_13(bool value)
	{
		___paused_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7WEATHERCHANGE_T3480560370_H
#ifndef L7TAKINGHOME_T92063034_H
#define L7TAKINGHOME_T92063034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7TakingHome
struct  L7TakingHome_t92063034  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L7TakingHome::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture[] L7TakingHome::m_textures
	TextureU5BU5D_t152995001* ___m_textures_3;
	// System.String[] L7TakingHome::takeHomeArray
	StringU5BU5D_t2511808107* ___takeHomeArray_5;
	// System.String L7TakingHome::m_conversationBoxText
	String_t* ___m_conversationBoxText_6;
	// System.Int32 L7TakingHome::m_selectedIndex
	int32_t ___m_selectedIndex_7;
	// UnityEngine.Texture2D L7TakingHome::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_8;
	// UnityEngine.GameObject L7TakingHome::menu
	GameObject_t2557347079 * ___menu_9;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_textures_3() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___m_textures_3)); }
	inline TextureU5BU5D_t152995001* get_m_textures_3() const { return ___m_textures_3; }
	inline TextureU5BU5D_t152995001** get_address_of_m_textures_3() { return &___m_textures_3; }
	inline void set_m_textures_3(TextureU5BU5D_t152995001* value)
	{
		___m_textures_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textures_3), value);
	}

	inline static int32_t get_offset_of_takeHomeArray_5() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___takeHomeArray_5)); }
	inline StringU5BU5D_t2511808107* get_takeHomeArray_5() const { return ___takeHomeArray_5; }
	inline StringU5BU5D_t2511808107** get_address_of_takeHomeArray_5() { return &___takeHomeArray_5; }
	inline void set_takeHomeArray_5(StringU5BU5D_t2511808107* value)
	{
		___takeHomeArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___takeHomeArray_5), value);
	}

	inline static int32_t get_offset_of_m_conversationBoxText_6() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___m_conversationBoxText_6)); }
	inline String_t* get_m_conversationBoxText_6() const { return ___m_conversationBoxText_6; }
	inline String_t** get_address_of_m_conversationBoxText_6() { return &___m_conversationBoxText_6; }
	inline void set_m_conversationBoxText_6(String_t* value)
	{
		___m_conversationBoxText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_conversationBoxText_6), value);
	}

	inline static int32_t get_offset_of_m_selectedIndex_7() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___m_selectedIndex_7)); }
	inline int32_t get_m_selectedIndex_7() const { return ___m_selectedIndex_7; }
	inline int32_t* get_address_of_m_selectedIndex_7() { return &___m_selectedIndex_7; }
	inline void set_m_selectedIndex_7(int32_t value)
	{
		___m_selectedIndex_7 = value;
	}

	inline static int32_t get_offset_of_m_tConversationBackground_8() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___m_tConversationBackground_8)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_8() const { return ___m_tConversationBackground_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_8() { return &___m_tConversationBackground_8; }
	inline void set_m_tConversationBackground_8(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_8), value);
	}

	inline static int32_t get_offset_of_menu_9() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034, ___menu_9)); }
	inline GameObject_t2557347079 * get_menu_9() const { return ___menu_9; }
	inline GameObject_t2557347079 ** get_address_of_menu_9() { return &___menu_9; }
	inline void set_menu_9(GameObject_t2557347079 * value)
	{
		___menu_9 = value;
		Il2CppCodeGenWriteBarrier((&___menu_9), value);
	}
};

struct L7TakingHome_t92063034_StaticFields
{
public:
	// UnityEngine.Texture L7TakingHome::m_tChoice
	Texture_t2119925672 * ___m_tChoice_4;

public:
	inline static int32_t get_offset_of_m_tChoice_4() { return static_cast<int32_t>(offsetof(L7TakingHome_t92063034_StaticFields, ___m_tChoice_4)); }
	inline Texture_t2119925672 * get_m_tChoice_4() const { return ___m_tChoice_4; }
	inline Texture_t2119925672 ** get_address_of_m_tChoice_4() { return &___m_tChoice_4; }
	inline void set_m_tChoice_4(Texture_t2119925672 * value)
	{
		___m_tChoice_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tChoice_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7TAKINGHOME_T92063034_H
#ifndef L7STARTCATCHSPARKS_T2930775878_H
#define L7STARTCATCHSPARKS_T2930775878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7StartCatchSparks
struct  L7StartCatchSparks_t2930775878  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7STARTCATCHSPARKS_T2930775878_H
#ifndef L7SHIELDAGADEPRESSION2_T1497217947_H
#define L7SHIELDAGADEPRESSION2_T1497217947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7ShieldAgaDepression2
struct  L7ShieldAgaDepression2_t1497217947  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L7ShieldAgaDepression2::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L7ShieldAgaDepression2::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L7ShieldAgaDepression2::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L7ShieldAgaDepression2::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] L7ShieldAgaDepression2::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] L7ShieldAgaDepression2::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// UnityEngine.Rect[] L7ShieldAgaDepression2::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_10;
	// System.Boolean[] L7ShieldAgaDepression2::displayName
	BooleanU5BU5D_t698278498* ___displayName_11;
	// UnityEngine.Color[] L7ShieldAgaDepression2::displayColor
	ColorU5BU5D_t247935167* ___displayColor_12;
	// UnityEngine.Rect[] L7ShieldAgaDepression2::m_destinations
	RectU5BU5D_t141872167* ___m_destinations_14;
	// UnityEngine.Texture L7ShieldAgaDepression2::colourTexture
	Texture_t2119925672 * ___colourTexture_15;
	// UnityEngine.Texture L7ShieldAgaDepression2::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_16;
	// UnityEngine.GameObject L7ShieldAgaDepression2::menu
	GameObject_t2557347079 * ___menu_17;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_10() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___shieldNameRects_10)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_10() const { return ___shieldNameRects_10; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_10() { return &___shieldNameRects_10; }
	inline void set_shieldNameRects_10(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_10), value);
	}

	inline static int32_t get_offset_of_displayName_11() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___displayName_11)); }
	inline BooleanU5BU5D_t698278498* get_displayName_11() const { return ___displayName_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_11() { return &___displayName_11; }
	inline void set_displayName_11(BooleanU5BU5D_t698278498* value)
	{
		___displayName_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_11), value);
	}

	inline static int32_t get_offset_of_displayColor_12() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___displayColor_12)); }
	inline ColorU5BU5D_t247935167* get_displayColor_12() const { return ___displayColor_12; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_12() { return &___displayColor_12; }
	inline void set_displayColor_12(ColorU5BU5D_t247935167* value)
	{
		___displayColor_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_12), value);
	}

	inline static int32_t get_offset_of_m_destinations_14() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___m_destinations_14)); }
	inline RectU5BU5D_t141872167* get_m_destinations_14() const { return ___m_destinations_14; }
	inline RectU5BU5D_t141872167** get_address_of_m_destinations_14() { return &___m_destinations_14; }
	inline void set_m_destinations_14(RectU5BU5D_t141872167* value)
	{
		___m_destinations_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinations_14), value);
	}

	inline static int32_t get_offset_of_colourTexture_15() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___colourTexture_15)); }
	inline Texture_t2119925672 * get_colourTexture_15() const { return ___colourTexture_15; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_15() { return &___colourTexture_15; }
	inline void set_colourTexture_15(Texture_t2119925672 * value)
	{
		___colourTexture_15 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_15), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_16() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___greyScaleTexture_16)); }
	inline Texture_t2119925672 * get_greyScaleTexture_16() const { return ___greyScaleTexture_16; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_16() { return &___greyScaleTexture_16; }
	inline void set_greyScaleTexture_16(Texture_t2119925672 * value)
	{
		___greyScaleTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_16), value);
	}

	inline static int32_t get_offset_of_menu_17() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947, ___menu_17)); }
	inline GameObject_t2557347079 * get_menu_17() const { return ___menu_17; }
	inline GameObject_t2557347079 ** get_address_of_menu_17() { return &___menu_17; }
	inline void set_menu_17(GameObject_t2557347079 * value)
	{
		___menu_17 = value;
		Il2CppCodeGenWriteBarrier((&___menu_17), value);
	}
};

struct L7ShieldAgaDepression2_t1497217947_StaticFields
{
public:
	// System.Single L7ShieldAgaDepression2::TextWidth
	float ___TextWidth_6;
	// System.Single L7ShieldAgaDepression2::TextHeight
	float ___TextHeight_7;
	// System.Single L7ShieldAgaDepression2::m_targetDim
	float ___m_targetDim_13;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_targetDim_13() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression2_t1497217947_StaticFields, ___m_targetDim_13)); }
	inline float get_m_targetDim_13() const { return ___m_targetDim_13; }
	inline float* get_address_of_m_targetDim_13() { return &___m_targetDim_13; }
	inline void set_m_targetDim_13(float value)
	{
		___m_targetDim_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7SHIELDAGADEPRESSION2_T1497217947_H
#ifndef L7SHIELDAGADEPRESSION_T3190915008_H
#define L7SHIELDAGADEPRESSION_T3190915008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7ShieldAgaDepression
struct  L7ShieldAgaDepression_t3190915008  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L7ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_2;
	// UnityEngine.Vector3 L7ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_3;
	// UnityEngine.GameObject L7ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_4;
	// System.Boolean[] L7ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_5;
	// UnityEngine.GameObject L7ShieldAgaDepression::menu
	GameObject_t2557347079 * ___menu_6;

public:
	inline static int32_t get_offset_of_m_shieldPrf_2() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression_t3190915008, ___m_shieldPrf_2)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_2() const { return ___m_shieldPrf_2; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_2() { return &___m_shieldPrf_2; }
	inline void set_m_shieldPrf_2(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_2), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_3() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression_t3190915008, ___m_vShieldPosition_3)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_3() const { return ___m_vShieldPosition_3; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_3() { return &___m_vShieldPosition_3; }
	inline void set_m_vShieldPosition_3(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_3 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_4() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression_t3190915008, ___m_shieldObjectClone_4)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_4() const { return ___m_shieldObjectClone_4; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_4() { return &___m_shieldObjectClone_4; }
	inline void set_m_shieldObjectClone_4(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_4), value);
	}

	inline static int32_t get_offset_of_displayName_5() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression_t3190915008, ___displayName_5)); }
	inline BooleanU5BU5D_t698278498* get_displayName_5() const { return ___displayName_5; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_5() { return &___displayName_5; }
	inline void set_displayName_5(BooleanU5BU5D_t698278498* value)
	{
		___displayName_5 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_5), value);
	}

	inline static int32_t get_offset_of_menu_6() { return static_cast<int32_t>(offsetof(L7ShieldAgaDepression_t3190915008, ___menu_6)); }
	inline GameObject_t2557347079 * get_menu_6() const { return ___menu_6; }
	inline GameObject_t2557347079 ** get_address_of_menu_6() { return &___menu_6; }
	inline void set_menu_6(GameObject_t2557347079 * value)
	{
		___menu_6 = value;
		Il2CppCodeGenWriteBarrier((&___menu_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7SHIELDAGADEPRESSION_T3190915008_H
#ifndef L7PLAYERMOVEROCK_T3460284416_H
#define L7PLAYERMOVEROCK_T3460284416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7PlayerMoveRock
struct  L7PlayerMoveRock_t3460284416  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7PLAYERMOVEROCK_T3460284416_H
#ifndef L7LESSONSMATCH_T1153153057_H
#define L7LESSONSMATCH_T1153153057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7LessonsMatch
struct  L7LessonsMatch_t1153153057  : public MonoBehaviour_t1618594486
{
public:
	// System.String[] L7LessonsMatch::strHowDoesItRelate
	StringU5BU5D_t2511808107* ___strHowDoesItRelate_2;
	// System.String[] L7LessonsMatch::strWhatsTheMessage
	StringU5BU5D_t2511808107* ___strWhatsTheMessage_3;
	// System.String[] L7LessonsMatch::questions
	StringU5BU5D_t2511808107* ___questions_4;
	// UnityEngine.GameObject L7LessonsMatch::menu
	GameObject_t2557347079 * ___menu_5;

public:
	inline static int32_t get_offset_of_strHowDoesItRelate_2() { return static_cast<int32_t>(offsetof(L7LessonsMatch_t1153153057, ___strHowDoesItRelate_2)); }
	inline StringU5BU5D_t2511808107* get_strHowDoesItRelate_2() const { return ___strHowDoesItRelate_2; }
	inline StringU5BU5D_t2511808107** get_address_of_strHowDoesItRelate_2() { return &___strHowDoesItRelate_2; }
	inline void set_strHowDoesItRelate_2(StringU5BU5D_t2511808107* value)
	{
		___strHowDoesItRelate_2 = value;
		Il2CppCodeGenWriteBarrier((&___strHowDoesItRelate_2), value);
	}

	inline static int32_t get_offset_of_strWhatsTheMessage_3() { return static_cast<int32_t>(offsetof(L7LessonsMatch_t1153153057, ___strWhatsTheMessage_3)); }
	inline StringU5BU5D_t2511808107* get_strWhatsTheMessage_3() const { return ___strWhatsTheMessage_3; }
	inline StringU5BU5D_t2511808107** get_address_of_strWhatsTheMessage_3() { return &___strWhatsTheMessage_3; }
	inline void set_strWhatsTheMessage_3(StringU5BU5D_t2511808107* value)
	{
		___strWhatsTheMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___strWhatsTheMessage_3), value);
	}

	inline static int32_t get_offset_of_questions_4() { return static_cast<int32_t>(offsetof(L7LessonsMatch_t1153153057, ___questions_4)); }
	inline StringU5BU5D_t2511808107* get_questions_4() const { return ___questions_4; }
	inline StringU5BU5D_t2511808107** get_address_of_questions_4() { return &___questions_4; }
	inline void set_questions_4(StringU5BU5D_t2511808107* value)
	{
		___questions_4 = value;
		Il2CppCodeGenWriteBarrier((&___questions_4), value);
	}

	inline static int32_t get_offset_of_menu_5() { return static_cast<int32_t>(offsetof(L7LessonsMatch_t1153153057, ___menu_5)); }
	inline GameObject_t2557347079 * get_menu_5() const { return ___menu_5; }
	inline GameObject_t2557347079 ** get_address_of_menu_5() { return &___menu_5; }
	inline void set_menu_5(GameObject_t2557347079 * value)
	{
		___menu_5 = value;
		Il2CppCodeGenWriteBarrier((&___menu_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7LESSONSMATCH_T1153153057_H
#ifndef L6STOPTRASHTURNIT_T1936386210_H
#define L6STOPTRASHTURNIT_T1936386210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6StopTrashTurnIt
struct  L6StopTrashTurnIt_t1936386210  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L6StopTrashTurnIt::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L6StopTrashTurnIt_t1936386210, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6STOPTRASHTURNIT_T1936386210_H
#ifndef L6SORTITNEGOTIATE_T1352594894_H
#define L6SORTITNEGOTIATE_T1352594894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6SortItNegotiate
struct  L6SortItNegotiate_t1352594894  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L6SortItNegotiate::menu
	GameObject_t2557347079 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(L6SortItNegotiate_t1352594894, ___menu_2)); }
	inline GameObject_t2557347079 * get_menu_2() const { return ___menu_2; }
	inline GameObject_t2557347079 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(GameObject_t2557347079 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6SORTITNEGOTIATE_T1352594894_H
#ifndef L6SHIELDAGADEPRESSION_T544321687_H
#define L6SHIELDAGADEPRESSION_T544321687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6ShieldAgaDepression
struct  L6ShieldAgaDepression_t544321687  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L6ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L6ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L6ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L6ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] L6ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] L6ShieldAgaDepression::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// UnityEngine.Rect[] L6ShieldAgaDepression::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_10;
	// System.Boolean[] L6ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_11;
	// UnityEngine.Color[] L6ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_12;
	// UnityEngine.Texture L6ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_13;
	// UnityEngine.Texture L6ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_14;
	// UnityEngine.GameObject L6ShieldAgaDepression::menu
	GameObject_t2557347079 * ___menu_15;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_10() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___shieldNameRects_10)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_10() const { return ___shieldNameRects_10; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_10() { return &___shieldNameRects_10; }
	inline void set_shieldNameRects_10(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_10), value);
	}

	inline static int32_t get_offset_of_displayName_11() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___displayName_11)); }
	inline BooleanU5BU5D_t698278498* get_displayName_11() const { return ___displayName_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_11() { return &___displayName_11; }
	inline void set_displayName_11(BooleanU5BU5D_t698278498* value)
	{
		___displayName_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_11), value);
	}

	inline static int32_t get_offset_of_displayColor_12() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___displayColor_12)); }
	inline ColorU5BU5D_t247935167* get_displayColor_12() const { return ___displayColor_12; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_12() { return &___displayColor_12; }
	inline void set_displayColor_12(ColorU5BU5D_t247935167* value)
	{
		___displayColor_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_12), value);
	}

	inline static int32_t get_offset_of_colourTexture_13() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___colourTexture_13)); }
	inline Texture_t2119925672 * get_colourTexture_13() const { return ___colourTexture_13; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_13() { return &___colourTexture_13; }
	inline void set_colourTexture_13(Texture_t2119925672 * value)
	{
		___colourTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_13), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_14() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___greyScaleTexture_14)); }
	inline Texture_t2119925672 * get_greyScaleTexture_14() const { return ___greyScaleTexture_14; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_14() { return &___greyScaleTexture_14; }
	inline void set_greyScaleTexture_14(Texture_t2119925672 * value)
	{
		___greyScaleTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_14), value);
	}

	inline static int32_t get_offset_of_menu_15() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687, ___menu_15)); }
	inline GameObject_t2557347079 * get_menu_15() const { return ___menu_15; }
	inline GameObject_t2557347079 ** get_address_of_menu_15() { return &___menu_15; }
	inline void set_menu_15(GameObject_t2557347079 * value)
	{
		___menu_15 = value;
		Il2CppCodeGenWriteBarrier((&___menu_15), value);
	}
};

struct L6ShieldAgaDepression_t544321687_StaticFields
{
public:
	// System.Single L6ShieldAgaDepression::TextWidth
	float ___TextWidth_6;
	// System.Single L6ShieldAgaDepression::TextHeight
	float ___TextHeight_7;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(L6ShieldAgaDepression_t544321687_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6SHIELDAGADEPRESSION_T544321687_H
#ifndef L6NEGATIVESTATEMENT_T2202065469_H
#define L6NEGATIVESTATEMENT_T2202065469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6NegativeStatement
struct  L6NegativeStatement_t2202065469  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean[] L6NegativeStatement::m_bNegativeStatementSelectList
	BooleanU5BU5D_t698278498* ___m_bNegativeStatementSelectList_2;
	// UnityEngine.GameObject L6NegativeStatement::menu
	GameObject_t2557347079 * ___menu_4;
	// System.String[] L6NegativeStatement::choices
	StringU5BU5D_t2511808107* ___choices_5;

public:
	inline static int32_t get_offset_of_m_bNegativeStatementSelectList_2() { return static_cast<int32_t>(offsetof(L6NegativeStatement_t2202065469, ___m_bNegativeStatementSelectList_2)); }
	inline BooleanU5BU5D_t698278498* get_m_bNegativeStatementSelectList_2() const { return ___m_bNegativeStatementSelectList_2; }
	inline BooleanU5BU5D_t698278498** get_address_of_m_bNegativeStatementSelectList_2() { return &___m_bNegativeStatementSelectList_2; }
	inline void set_m_bNegativeStatementSelectList_2(BooleanU5BU5D_t698278498* value)
	{
		___m_bNegativeStatementSelectList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_bNegativeStatementSelectList_2), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L6NegativeStatement_t2202065469, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_choices_5() { return static_cast<int32_t>(offsetof(L6NegativeStatement_t2202065469, ___choices_5)); }
	inline StringU5BU5D_t2511808107* get_choices_5() const { return ___choices_5; }
	inline StringU5BU5D_t2511808107** get_address_of_choices_5() { return &___choices_5; }
	inline void set_choices_5(StringU5BU5D_t2511808107* value)
	{
		___choices_5 = value;
		Il2CppCodeGenWriteBarrier((&___choices_5), value);
	}
};

struct L6NegativeStatement_t2202065469_StaticFields
{
public:
	// System.String L6NegativeStatement::m_sChoice
	String_t* ___m_sChoice_3;

public:
	inline static int32_t get_offset_of_m_sChoice_3() { return static_cast<int32_t>(offsetof(L6NegativeStatement_t2202065469_StaticFields, ___m_sChoice_3)); }
	inline String_t* get_m_sChoice_3() const { return ___m_sChoice_3; }
	inline String_t** get_address_of_m_sChoice_3() { return &___m_sChoice_3; }
	inline void set_m_sChoice_3(String_t* value)
	{
		___m_sChoice_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_sChoice_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6NEGATIVESTATEMENT_T2202065469_H
#ifndef L6MATCHINGTEXTDRAGDROP_T1098812931_H
#define L6MATCHINGTEXTDRAGDROP_T1098812931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6MatchingTextDragDrop
struct  L6MatchingTextDragDrop_t1098812931  : public MonoBehaviour_t1618594486
{
public:
	// System.String[] L6MatchingTextDragDrop::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_2;
	// System.String[] L6MatchingTextDragDrop::draggables
	StringU5BU5D_t2511808107* ___draggables_3;
	// UnityEngine.GameObject L6MatchingTextDragDrop::menu
	GameObject_t2557347079 * ___menu_4;

public:
	inline static int32_t get_offset_of_m_destinationText_2() { return static_cast<int32_t>(offsetof(L6MatchingTextDragDrop_t1098812931, ___m_destinationText_2)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_2() const { return ___m_destinationText_2; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_2() { return &___m_destinationText_2; }
	inline void set_m_destinationText_2(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_2), value);
	}

	inline static int32_t get_offset_of_draggables_3() { return static_cast<int32_t>(offsetof(L6MatchingTextDragDrop_t1098812931, ___draggables_3)); }
	inline StringU5BU5D_t2511808107* get_draggables_3() const { return ___draggables_3; }
	inline StringU5BU5D_t2511808107** get_address_of_draggables_3() { return &___draggables_3; }
	inline void set_draggables_3(StringU5BU5D_t2511808107* value)
	{
		___draggables_3 = value;
		Il2CppCodeGenWriteBarrier((&___draggables_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L6MatchingTextDragDrop_t1098812931, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6MATCHINGTEXTDRAGDROP_T1098812931_H
#ifndef L6ANALYSENEGATIVESTATEMENT_T2433320051_H
#define L6ANALYSENEGATIVESTATEMENT_T2433320051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6AnalyseNegativeStatement
struct  L6AnalyseNegativeStatement_t2433320051  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L6AnalyseNegativeStatement::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String[] L6AnalyseNegativeStatement::headings
	StringU5BU5D_t2511808107* ___headings_4;
	// System.String[] L6AnalyseNegativeStatement::titles
	StringU5BU5D_t2511808107* ___titles_5;
	// UnityEngine.GameObject L6AnalyseNegativeStatement::menu
	GameObject_t2557347079 * ___menu_6;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L6AnalyseNegativeStatement_t2433320051, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_headings_4() { return static_cast<int32_t>(offsetof(L6AnalyseNegativeStatement_t2433320051, ___headings_4)); }
	inline StringU5BU5D_t2511808107* get_headings_4() const { return ___headings_4; }
	inline StringU5BU5D_t2511808107** get_address_of_headings_4() { return &___headings_4; }
	inline void set_headings_4(StringU5BU5D_t2511808107* value)
	{
		___headings_4 = value;
		Il2CppCodeGenWriteBarrier((&___headings_4), value);
	}

	inline static int32_t get_offset_of_titles_5() { return static_cast<int32_t>(offsetof(L6AnalyseNegativeStatement_t2433320051, ___titles_5)); }
	inline StringU5BU5D_t2511808107* get_titles_5() const { return ___titles_5; }
	inline StringU5BU5D_t2511808107** get_address_of_titles_5() { return &___titles_5; }
	inline void set_titles_5(StringU5BU5D_t2511808107* value)
	{
		___titles_5 = value;
		Il2CppCodeGenWriteBarrier((&___titles_5), value);
	}

	inline static int32_t get_offset_of_menu_6() { return static_cast<int32_t>(offsetof(L6AnalyseNegativeStatement_t2433320051, ___menu_6)); }
	inline GameObject_t2557347079 * get_menu_6() const { return ___menu_6; }
	inline GameObject_t2557347079 ** get_address_of_menu_6() { return &___menu_6; }
	inline void set_menu_6(GameObject_t2557347079 * value)
	{
		___menu_6 = value;
		Il2CppCodeGenWriteBarrier((&___menu_6), value);
	}
};

struct L6AnalyseNegativeStatement_t2433320051_StaticFields
{
public:
	// System.String[] L6AnalyseNegativeStatement::m_sChoices
	StringU5BU5D_t2511808107* ___m_sChoices_3;

public:
	inline static int32_t get_offset_of_m_sChoices_3() { return static_cast<int32_t>(offsetof(L6AnalyseNegativeStatement_t2433320051_StaticFields, ___m_sChoices_3)); }
	inline StringU5BU5D_t2511808107* get_m_sChoices_3() const { return ___m_sChoices_3; }
	inline StringU5BU5D_t2511808107** get_address_of_m_sChoices_3() { return &___m_sChoices_3; }
	inline void set_m_sChoices_3(StringU5BU5D_t2511808107* value)
	{
		___m_sChoices_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_sChoices_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6ANALYSENEGATIVESTATEMENT_T2433320051_H
#ifndef L5SHIELDAGADEPRESSION_T2788179544_H
#define L5SHIELDAGADEPRESSION_T2788179544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5ShieldAgaDepression
struct  L5ShieldAgaDepression_t2788179544  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L5ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject L5ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 L5ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject L5ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] L5ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] L5ShieldAgaDepression::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// UnityEngine.Rect[] L5ShieldAgaDepression::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_10;
	// System.Boolean[] L5ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_11;
	// UnityEngine.Color[] L5ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_12;
	// UnityEngine.Texture L5ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_13;
	// UnityEngine.Texture L5ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_14;
	// UnityEngine.GameObject L5ShieldAgaDepression::menu
	GameObject_t2557347079 * ___menu_15;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_10() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___shieldNameRects_10)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_10() const { return ___shieldNameRects_10; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_10() { return &___shieldNameRects_10; }
	inline void set_shieldNameRects_10(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_10), value);
	}

	inline static int32_t get_offset_of_displayName_11() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___displayName_11)); }
	inline BooleanU5BU5D_t698278498* get_displayName_11() const { return ___displayName_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_11() { return &___displayName_11; }
	inline void set_displayName_11(BooleanU5BU5D_t698278498* value)
	{
		___displayName_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_11), value);
	}

	inline static int32_t get_offset_of_displayColor_12() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___displayColor_12)); }
	inline ColorU5BU5D_t247935167* get_displayColor_12() const { return ___displayColor_12; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_12() { return &___displayColor_12; }
	inline void set_displayColor_12(ColorU5BU5D_t247935167* value)
	{
		___displayColor_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_12), value);
	}

	inline static int32_t get_offset_of_colourTexture_13() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___colourTexture_13)); }
	inline Texture_t2119925672 * get_colourTexture_13() const { return ___colourTexture_13; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_13() { return &___colourTexture_13; }
	inline void set_colourTexture_13(Texture_t2119925672 * value)
	{
		___colourTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_13), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_14() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___greyScaleTexture_14)); }
	inline Texture_t2119925672 * get_greyScaleTexture_14() const { return ___greyScaleTexture_14; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_14() { return &___greyScaleTexture_14; }
	inline void set_greyScaleTexture_14(Texture_t2119925672 * value)
	{
		___greyScaleTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_14), value);
	}

	inline static int32_t get_offset_of_menu_15() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544, ___menu_15)); }
	inline GameObject_t2557347079 * get_menu_15() const { return ___menu_15; }
	inline GameObject_t2557347079 ** get_address_of_menu_15() { return &___menu_15; }
	inline void set_menu_15(GameObject_t2557347079 * value)
	{
		___menu_15 = value;
		Il2CppCodeGenWriteBarrier((&___menu_15), value);
	}
};

struct L5ShieldAgaDepression_t2788179544_StaticFields
{
public:
	// System.Single L5ShieldAgaDepression::TextWidth
	float ___TextWidth_6;
	// System.Single L5ShieldAgaDepression::TextHeight
	float ___TextHeight_7;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(L5ShieldAgaDepression_t2788179544_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5SHIELDAGADEPRESSION_T2788179544_H
#ifndef L5PROGRESSTRIGGER_T2831516982_H
#define L5PROGRESSTRIGGER_T2831516982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5ProgressTrigger
struct  L5ProgressTrigger_t2831516982  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean L5ProgressTrigger::startTalk
	bool ___startTalk_2;
	// UnityEngine.GUISkin L5ProgressTrigger::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.Vector3 L5ProgressTrigger::tempPos
	Vector3_t1986933152  ___tempPos_4;

public:
	inline static int32_t get_offset_of_startTalk_2() { return static_cast<int32_t>(offsetof(L5ProgressTrigger_t2831516982, ___startTalk_2)); }
	inline bool get_startTalk_2() const { return ___startTalk_2; }
	inline bool* get_address_of_startTalk_2() { return &___startTalk_2; }
	inline void set_startTalk_2(bool value)
	{
		___startTalk_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(L5ProgressTrigger_t2831516982, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_tempPos_4() { return static_cast<int32_t>(offsetof(L5ProgressTrigger_t2831516982, ___tempPos_4)); }
	inline Vector3_t1986933152  get_tempPos_4() const { return ___tempPos_4; }
	inline Vector3_t1986933152 * get_address_of_tempPos_4() { return &___tempPos_4; }
	inline void set_tempPos_4(Vector3_t1986933152  value)
	{
		___tempPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5PROGRESSTRIGGER_T2831516982_H
#ifndef L5ENTERTEXTS_T1471665743_H
#define L5ENTERTEXTS_T1471665743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5EnterTexts
struct  L5EnterTexts_t1471665743  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject L5EnterTexts::menu
	GameObject_t2557347079 * ___menu_3;

public:
	inline static int32_t get_offset_of_menu_3() { return static_cast<int32_t>(offsetof(L5EnterTexts_t1471665743, ___menu_3)); }
	inline GameObject_t2557347079 * get_menu_3() const { return ___menu_3; }
	inline GameObject_t2557347079 ** get_address_of_menu_3() { return &___menu_3; }
	inline void set_menu_3(GameObject_t2557347079 * value)
	{
		___menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___menu_3), value);
	}
};

struct L5EnterTexts_t1471665743_StaticFields
{
public:
	// System.String[] L5EnterTexts::m_inputTexts
	StringU5BU5D_t2511808107* ___m_inputTexts_2;

public:
	inline static int32_t get_offset_of_m_inputTexts_2() { return static_cast<int32_t>(offsetof(L5EnterTexts_t1471665743_StaticFields, ___m_inputTexts_2)); }
	inline StringU5BU5D_t2511808107* get_m_inputTexts_2() const { return ___m_inputTexts_2; }
	inline StringU5BU5D_t2511808107** get_address_of_m_inputTexts_2() { return &___m_inputTexts_2; }
	inline void set_m_inputTexts_2(StringU5BU5D_t2511808107* value)
	{
		___m_inputTexts_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputTexts_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5ENTERTEXTS_T1471665743_H
#ifndef L6NOTEBOOK_T2887596315_H
#define L6NOTEBOOK_T2887596315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L6NoteBook
struct  L6NoteBook_t2887596315  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L6NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String L6NoteBook::text1
	String_t* ___text1_3;
	// System.String L6NoteBook::text2
	String_t* ___text2_4;
	// System.String L6NoteBook::text3
	String_t* ___text3_5;
	// CapturedDataInput L6NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_6;
	// UnityEngine.Texture2D L6NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_7;
	// System.Int32 L6NoteBook::m_iIndex
	int32_t ___m_iIndex_8;
	// System.String L6NoteBook::failedFileName
	String_t* ___failedFileName_9;
	// UnityEngine.GameObject L6NoteBook::menu
	GameObject_t2557347079 * ___menu_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___instance_6)); }
	inline CapturedDataInput_t2616152122 * get_instance_6() const { return ___instance_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(CapturedDataInput_t2616152122 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_7() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___m_tNoteBook_7)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_7() const { return ___m_tNoteBook_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_7() { return &___m_tNoteBook_7; }
	inline void set_m_tNoteBook_7(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_7), value);
	}

	inline static int32_t get_offset_of_m_iIndex_8() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___m_iIndex_8)); }
	inline int32_t get_m_iIndex_8() const { return ___m_iIndex_8; }
	inline int32_t* get_address_of_m_iIndex_8() { return &___m_iIndex_8; }
	inline void set_m_iIndex_8(int32_t value)
	{
		___m_iIndex_8 = value;
	}

	inline static int32_t get_offset_of_failedFileName_9() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___failedFileName_9)); }
	inline String_t* get_failedFileName_9() const { return ___failedFileName_9; }
	inline String_t** get_address_of_failedFileName_9() { return &___failedFileName_9; }
	inline void set_failedFileName_9(String_t* value)
	{
		___failedFileName_9 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_9), value);
	}

	inline static int32_t get_offset_of_menu_10() { return static_cast<int32_t>(offsetof(L6NoteBook_t2887596315, ___menu_10)); }
	inline GameObject_t2557347079 * get_menu_10() const { return ___menu_10; }
	inline GameObject_t2557347079 ** get_address_of_menu_10() { return &___menu_10; }
	inline void set_menu_10(GameObject_t2557347079 * value)
	{
		___menu_10 = value;
		Il2CppCodeGenWriteBarrier((&___menu_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L6NOTEBOOK_T2887596315_H
#ifndef L7NOTEBOOK_T3956512365_H
#define L7NOTEBOOK_T3956512365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L7NoteBook
struct  L7NoteBook_t3956512365  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin L7NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// CapturedDataInput L7NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_3;
	// UnityEngine.Texture2D L7NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_4;
	// UnityEngine.Texture2D L7NoteBook::m_tPicture
	Texture2D_t3063074017 * ___m_tPicture_5;
	// UnityEngine.Texture2D L7NoteBook::m_tShield
	Texture2D_t3063074017 * ___m_tShield_6;
	// System.String L7NoteBook::failedFileName
	String_t* ___failedFileName_7;
	// UnityEngine.GameObject L7NoteBook::menu
	GameObject_t2557347079 * ___menu_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___instance_3)); }
	inline CapturedDataInput_t2616152122 * get_instance_3() const { return ___instance_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CapturedDataInput_t2616152122 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_4() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___m_tNoteBook_4)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_4() const { return ___m_tNoteBook_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_4() { return &___m_tNoteBook_4; }
	inline void set_m_tNoteBook_4(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_4), value);
	}

	inline static int32_t get_offset_of_m_tPicture_5() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___m_tPicture_5)); }
	inline Texture2D_t3063074017 * get_m_tPicture_5() const { return ___m_tPicture_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tPicture_5() { return &___m_tPicture_5; }
	inline void set_m_tPicture_5(Texture2D_t3063074017 * value)
	{
		___m_tPicture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tPicture_5), value);
	}

	inline static int32_t get_offset_of_m_tShield_6() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___m_tShield_6)); }
	inline Texture2D_t3063074017 * get_m_tShield_6() const { return ___m_tShield_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_tShield_6() { return &___m_tShield_6; }
	inline void set_m_tShield_6(Texture2D_t3063074017 * value)
	{
		___m_tShield_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_tShield_6), value);
	}

	inline static int32_t get_offset_of_failedFileName_7() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___failedFileName_7)); }
	inline String_t* get_failedFileName_7() const { return ___failedFileName_7; }
	inline String_t** get_address_of_failedFileName_7() { return &___failedFileName_7; }
	inline void set_failedFileName_7(String_t* value)
	{
		___failedFileName_7 = value;
		Il2CppCodeGenWriteBarrier((&___failedFileName_7), value);
	}

	inline static int32_t get_offset_of_menu_8() { return static_cast<int32_t>(offsetof(L7NoteBook_t3956512365, ___menu_8)); }
	inline GameObject_t2557347079 * get_menu_8() const { return ___menu_8; }
	inline GameObject_t2557347079 ** get_address_of_menu_8() { return &___menu_8; }
	inline void set_menu_8(GameObject_t2557347079 * value)
	{
		___menu_8 = value;
		Il2CppCodeGenWriteBarrier((&___menu_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L7NOTEBOOK_T3956512365_H
#ifndef SAVEDL1NOTEBOOK_T606761968_H
#define SAVEDL1NOTEBOOK_T606761968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL1NoteBook
struct  SavedL1NoteBook_t606761968  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL1NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject SavedL1NoteBook::menu
	GameObject_t2557347079 * ___menu_3;
	// CapturedDataInput SavedL1NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_4;
	// UnityEngine.Texture2D SavedL1NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL1NoteBook_t606761968, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_menu_3() { return static_cast<int32_t>(offsetof(SavedL1NoteBook_t606761968, ___menu_3)); }
	inline GameObject_t2557347079 * get_menu_3() const { return ___menu_3; }
	inline GameObject_t2557347079 ** get_address_of_menu_3() { return &___menu_3; }
	inline void set_menu_3(GameObject_t2557347079 * value)
	{
		___menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___menu_3), value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(SavedL1NoteBook_t606761968, ___instance_4)); }
	inline CapturedDataInput_t2616152122 * get_instance_4() const { return ___instance_4; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CapturedDataInput_t2616152122 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_5() { return static_cast<int32_t>(offsetof(SavedL1NoteBook_t606761968, ___m_tNoteBook_5)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_5() const { return ___m_tNoteBook_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_5() { return &___m_tNoteBook_5; }
	inline void set_m_tNoteBook_5(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL1NOTEBOOK_T606761968_H
#ifndef SAVEDL2NOTEBOOK_T2697330983_H
#define SAVEDL2NOTEBOOK_T2697330983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL2NoteBook
struct  SavedL2NoteBook_t2697330983  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL2NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// CapturedDataInput SavedL2NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_3;
	// UnityEngine.Texture2D SavedL2NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_4;
	// UnityEngine.GameObject SavedL2NoteBook::menu
	GameObject_t2557347079 * ___menu_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL2NoteBook_t2697330983, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SavedL2NoteBook_t2697330983, ___instance_3)); }
	inline CapturedDataInput_t2616152122 * get_instance_3() const { return ___instance_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CapturedDataInput_t2616152122 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_4() { return static_cast<int32_t>(offsetof(SavedL2NoteBook_t2697330983, ___m_tNoteBook_4)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_4() const { return ___m_tNoteBook_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_4() { return &___m_tNoteBook_4; }
	inline void set_m_tNoteBook_4(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_4), value);
	}

	inline static int32_t get_offset_of_menu_5() { return static_cast<int32_t>(offsetof(SavedL2NoteBook_t2697330983, ___menu_5)); }
	inline GameObject_t2557347079 * get_menu_5() const { return ___menu_5; }
	inline GameObject_t2557347079 ** get_address_of_menu_5() { return &___menu_5; }
	inline void set_menu_5(GameObject_t2557347079 * value)
	{
		___menu_5 = value;
		Il2CppCodeGenWriteBarrier((&___menu_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL2NOTEBOOK_T2697330983_H
#ifndef LOGOSSCREEN_T3335304941_H
#define LOGOSSCREEN_T3335304941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogosScreen
struct  LogosScreen_t3335304941  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin LogosScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Boolean LogosScreen::m_bLogoFadeInStart
	bool ___m_bLogoFadeInStart_3;
	// System.Single LogosScreen::m_fFadeInStartTime
	float ___m_fFadeInStartTime_4;
	// System.Single LogosScreen::m_fLogoStayTime
	float ___m_fLogoStayTime_5;
	// System.Boolean LogosScreen::m_bLogoFadeOutStart
	bool ___m_bLogoFadeOutStart_6;
	// System.Single LogosScreen::m_fFadeOutTime
	float ___m_fFadeOutTime_7;
	// System.Single LogosScreen::m_fFadeInLogoAlpha
	float ___m_fFadeInLogoAlpha_8;
	// System.Single LogosScreen::m_fFadeOutLogoAlpha
	float ___m_fFadeOutLogoAlpha_9;
	// System.Boolean LogosScreen::m_bScriptIsKilled
	bool ___m_bScriptIsKilled_10;
	// CapturedDataInput LogosScreen::instance
	CapturedDataInput_t2616152122 * ___instance_11;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_bLogoFadeInStart_3() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_bLogoFadeInStart_3)); }
	inline bool get_m_bLogoFadeInStart_3() const { return ___m_bLogoFadeInStart_3; }
	inline bool* get_address_of_m_bLogoFadeInStart_3() { return &___m_bLogoFadeInStart_3; }
	inline void set_m_bLogoFadeInStart_3(bool value)
	{
		___m_bLogoFadeInStart_3 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_4() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_fFadeInStartTime_4)); }
	inline float get_m_fFadeInStartTime_4() const { return ___m_fFadeInStartTime_4; }
	inline float* get_address_of_m_fFadeInStartTime_4() { return &___m_fFadeInStartTime_4; }
	inline void set_m_fFadeInStartTime_4(float value)
	{
		___m_fFadeInStartTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fLogoStayTime_5() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_fLogoStayTime_5)); }
	inline float get_m_fLogoStayTime_5() const { return ___m_fLogoStayTime_5; }
	inline float* get_address_of_m_fLogoStayTime_5() { return &___m_fLogoStayTime_5; }
	inline void set_m_fLogoStayTime_5(float value)
	{
		___m_fLogoStayTime_5 = value;
	}

	inline static int32_t get_offset_of_m_bLogoFadeOutStart_6() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_bLogoFadeOutStart_6)); }
	inline bool get_m_bLogoFadeOutStart_6() const { return ___m_bLogoFadeOutStart_6; }
	inline bool* get_address_of_m_bLogoFadeOutStart_6() { return &___m_bLogoFadeOutStart_6; }
	inline void set_m_bLogoFadeOutStart_6(bool value)
	{
		___m_bLogoFadeOutStart_6 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutTime_7() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_fFadeOutTime_7)); }
	inline float get_m_fFadeOutTime_7() const { return ___m_fFadeOutTime_7; }
	inline float* get_address_of_m_fFadeOutTime_7() { return &___m_fFadeOutTime_7; }
	inline void set_m_fFadeOutTime_7(float value)
	{
		___m_fFadeOutTime_7 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInLogoAlpha_8() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_fFadeInLogoAlpha_8)); }
	inline float get_m_fFadeInLogoAlpha_8() const { return ___m_fFadeInLogoAlpha_8; }
	inline float* get_address_of_m_fFadeInLogoAlpha_8() { return &___m_fFadeInLogoAlpha_8; }
	inline void set_m_fFadeInLogoAlpha_8(float value)
	{
		___m_fFadeInLogoAlpha_8 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutLogoAlpha_9() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_fFadeOutLogoAlpha_9)); }
	inline float get_m_fFadeOutLogoAlpha_9() const { return ___m_fFadeOutLogoAlpha_9; }
	inline float* get_address_of_m_fFadeOutLogoAlpha_9() { return &___m_fFadeOutLogoAlpha_9; }
	inline void set_m_fFadeOutLogoAlpha_9(float value)
	{
		___m_fFadeOutLogoAlpha_9 = value;
	}

	inline static int32_t get_offset_of_m_bScriptIsKilled_10() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___m_bScriptIsKilled_10)); }
	inline bool get_m_bScriptIsKilled_10() const { return ___m_bScriptIsKilled_10; }
	inline bool* get_address_of_m_bScriptIsKilled_10() { return &___m_bScriptIsKilled_10; }
	inline void set_m_bScriptIsKilled_10(bool value)
	{
		___m_bScriptIsKilled_10 = value;
	}

	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(LogosScreen_t3335304941, ___instance_11)); }
	inline CapturedDataInput_t2616152122 * get_instance_11() const { return ___instance_11; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(CapturedDataInput_t2616152122 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGOSSCREEN_T3335304941_H
#ifndef LOGOBACKGROUNDSCREEN_T3640353420_H
#define LOGOBACKGROUNDSCREEN_T3640353420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogoBackgroundScreen
struct  LogoBackgroundScreen_t3640353420  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin LogoBackgroundScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Boolean LogoBackgroundScreen::m_bUploadingFile
	bool ___m_bUploadingFile_3;
	// System.Boolean LogoBackgroundScreen::m_bGotCheckingFile
	bool ___m_bGotCheckingFile_4;
	// System.Boolean LogoBackgroundScreen::m_bGotLoginTokenDone
	bool ___m_bGotLoginTokenDone_5;
	// System.Boolean LogoBackgroundScreen::m_bGotDynamicTokenDone
	bool ___m_bGotDynamicTokenDone_6;
	// System.Boolean LogoBackgroundScreen::m_bGotDynamicURLDone
	bool ___m_bGotDynamicURLDone_7;
	// System.Boolean LogoBackgroundScreen::m_bGotLoadAvatarDone
	bool ___m_bGotLoadAvatarDone_8;
	// System.Boolean LogoBackgroundScreen::m_bHasLevelNumber
	bool ___m_bHasLevelNumber_9;
	// System.Boolean LogoBackgroundScreen::m_bGotCharacter
	bool ___m_bGotCharacter_10;
	// System.Boolean LogoBackgroundScreen::m_bGotProgress
	bool ___m_bGotProgress_11;
	// System.Boolean LogoBackgroundScreen::m_bGotNotebook
	bool ___m_bGotNotebook_12;
	// System.Boolean LogoBackgroundScreen::m_bGotSavingPoint
	bool ___m_bGotSavingPoint_13;
	// System.Boolean LogoBackgroundScreen::m_bSendEventStart
	bool ___m_bSendEventStart_14;
	// System.Boolean LogoBackgroundScreen::m_DObWaiting4Secconds
	bool ___m_DObWaiting4Secconds_15;
	// System.Boolean LogoBackgroundScreen::m_bWaiting4Secconds
	bool ___m_bWaiting4Secconds_16;
	// System.Boolean LogoBackgroundScreen::m_bWaiting4SeccondsDone
	bool ___m_bWaiting4SeccondsDone_17;
	// System.Boolean LogoBackgroundScreen::m_bReLogin
	bool ___m_bReLogin_19;
	// System.Boolean LogoBackgroundScreen::m_bWaitingForReLoginServerDone
	bool ___m_bWaitingForReLoginServerDone_20;
	// System.Boolean LogoBackgroundScreen::m_bGotConfigURLDone
	bool ___m_bGotConfigURLDone_21;
	// CapturedDataInput LogoBackgroundScreen::instance
	CapturedDataInput_t2616152122 * ___instance_24;
	// TokenHolderLegacy LogoBackgroundScreen::instanceTHL
	TokenHolderLegacy_t1380075429 * ___instanceTHL_25;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_bUploadingFile_3() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bUploadingFile_3)); }
	inline bool get_m_bUploadingFile_3() const { return ___m_bUploadingFile_3; }
	inline bool* get_address_of_m_bUploadingFile_3() { return &___m_bUploadingFile_3; }
	inline void set_m_bUploadingFile_3(bool value)
	{
		___m_bUploadingFile_3 = value;
	}

	inline static int32_t get_offset_of_m_bGotCheckingFile_4() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotCheckingFile_4)); }
	inline bool get_m_bGotCheckingFile_4() const { return ___m_bGotCheckingFile_4; }
	inline bool* get_address_of_m_bGotCheckingFile_4() { return &___m_bGotCheckingFile_4; }
	inline void set_m_bGotCheckingFile_4(bool value)
	{
		___m_bGotCheckingFile_4 = value;
	}

	inline static int32_t get_offset_of_m_bGotLoginTokenDone_5() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotLoginTokenDone_5)); }
	inline bool get_m_bGotLoginTokenDone_5() const { return ___m_bGotLoginTokenDone_5; }
	inline bool* get_address_of_m_bGotLoginTokenDone_5() { return &___m_bGotLoginTokenDone_5; }
	inline void set_m_bGotLoginTokenDone_5(bool value)
	{
		___m_bGotLoginTokenDone_5 = value;
	}

	inline static int32_t get_offset_of_m_bGotDynamicTokenDone_6() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotDynamicTokenDone_6)); }
	inline bool get_m_bGotDynamicTokenDone_6() const { return ___m_bGotDynamicTokenDone_6; }
	inline bool* get_address_of_m_bGotDynamicTokenDone_6() { return &___m_bGotDynamicTokenDone_6; }
	inline void set_m_bGotDynamicTokenDone_6(bool value)
	{
		___m_bGotDynamicTokenDone_6 = value;
	}

	inline static int32_t get_offset_of_m_bGotDynamicURLDone_7() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotDynamicURLDone_7)); }
	inline bool get_m_bGotDynamicURLDone_7() const { return ___m_bGotDynamicURLDone_7; }
	inline bool* get_address_of_m_bGotDynamicURLDone_7() { return &___m_bGotDynamicURLDone_7; }
	inline void set_m_bGotDynamicURLDone_7(bool value)
	{
		___m_bGotDynamicURLDone_7 = value;
	}

	inline static int32_t get_offset_of_m_bGotLoadAvatarDone_8() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotLoadAvatarDone_8)); }
	inline bool get_m_bGotLoadAvatarDone_8() const { return ___m_bGotLoadAvatarDone_8; }
	inline bool* get_address_of_m_bGotLoadAvatarDone_8() { return &___m_bGotLoadAvatarDone_8; }
	inline void set_m_bGotLoadAvatarDone_8(bool value)
	{
		___m_bGotLoadAvatarDone_8 = value;
	}

	inline static int32_t get_offset_of_m_bHasLevelNumber_9() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bHasLevelNumber_9)); }
	inline bool get_m_bHasLevelNumber_9() const { return ___m_bHasLevelNumber_9; }
	inline bool* get_address_of_m_bHasLevelNumber_9() { return &___m_bHasLevelNumber_9; }
	inline void set_m_bHasLevelNumber_9(bool value)
	{
		___m_bHasLevelNumber_9 = value;
	}

	inline static int32_t get_offset_of_m_bGotCharacter_10() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotCharacter_10)); }
	inline bool get_m_bGotCharacter_10() const { return ___m_bGotCharacter_10; }
	inline bool* get_address_of_m_bGotCharacter_10() { return &___m_bGotCharacter_10; }
	inline void set_m_bGotCharacter_10(bool value)
	{
		___m_bGotCharacter_10 = value;
	}

	inline static int32_t get_offset_of_m_bGotProgress_11() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotProgress_11)); }
	inline bool get_m_bGotProgress_11() const { return ___m_bGotProgress_11; }
	inline bool* get_address_of_m_bGotProgress_11() { return &___m_bGotProgress_11; }
	inline void set_m_bGotProgress_11(bool value)
	{
		___m_bGotProgress_11 = value;
	}

	inline static int32_t get_offset_of_m_bGotNotebook_12() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotNotebook_12)); }
	inline bool get_m_bGotNotebook_12() const { return ___m_bGotNotebook_12; }
	inline bool* get_address_of_m_bGotNotebook_12() { return &___m_bGotNotebook_12; }
	inline void set_m_bGotNotebook_12(bool value)
	{
		___m_bGotNotebook_12 = value;
	}

	inline static int32_t get_offset_of_m_bGotSavingPoint_13() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotSavingPoint_13)); }
	inline bool get_m_bGotSavingPoint_13() const { return ___m_bGotSavingPoint_13; }
	inline bool* get_address_of_m_bGotSavingPoint_13() { return &___m_bGotSavingPoint_13; }
	inline void set_m_bGotSavingPoint_13(bool value)
	{
		___m_bGotSavingPoint_13 = value;
	}

	inline static int32_t get_offset_of_m_bSendEventStart_14() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bSendEventStart_14)); }
	inline bool get_m_bSendEventStart_14() const { return ___m_bSendEventStart_14; }
	inline bool* get_address_of_m_bSendEventStart_14() { return &___m_bSendEventStart_14; }
	inline void set_m_bSendEventStart_14(bool value)
	{
		___m_bSendEventStart_14 = value;
	}

	inline static int32_t get_offset_of_m_DObWaiting4Secconds_15() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_DObWaiting4Secconds_15)); }
	inline bool get_m_DObWaiting4Secconds_15() const { return ___m_DObWaiting4Secconds_15; }
	inline bool* get_address_of_m_DObWaiting4Secconds_15() { return &___m_DObWaiting4Secconds_15; }
	inline void set_m_DObWaiting4Secconds_15(bool value)
	{
		___m_DObWaiting4Secconds_15 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting4Secconds_16() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bWaiting4Secconds_16)); }
	inline bool get_m_bWaiting4Secconds_16() const { return ___m_bWaiting4Secconds_16; }
	inline bool* get_address_of_m_bWaiting4Secconds_16() { return &___m_bWaiting4Secconds_16; }
	inline void set_m_bWaiting4Secconds_16(bool value)
	{
		___m_bWaiting4Secconds_16 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting4SeccondsDone_17() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bWaiting4SeccondsDone_17)); }
	inline bool get_m_bWaiting4SeccondsDone_17() const { return ___m_bWaiting4SeccondsDone_17; }
	inline bool* get_address_of_m_bWaiting4SeccondsDone_17() { return &___m_bWaiting4SeccondsDone_17; }
	inline void set_m_bWaiting4SeccondsDone_17(bool value)
	{
		___m_bWaiting4SeccondsDone_17 = value;
	}

	inline static int32_t get_offset_of_m_bReLogin_19() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bReLogin_19)); }
	inline bool get_m_bReLogin_19() const { return ___m_bReLogin_19; }
	inline bool* get_address_of_m_bReLogin_19() { return &___m_bReLogin_19; }
	inline void set_m_bReLogin_19(bool value)
	{
		___m_bReLogin_19 = value;
	}

	inline static int32_t get_offset_of_m_bWaitingForReLoginServerDone_20() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bWaitingForReLoginServerDone_20)); }
	inline bool get_m_bWaitingForReLoginServerDone_20() const { return ___m_bWaitingForReLoginServerDone_20; }
	inline bool* get_address_of_m_bWaitingForReLoginServerDone_20() { return &___m_bWaitingForReLoginServerDone_20; }
	inline void set_m_bWaitingForReLoginServerDone_20(bool value)
	{
		___m_bWaitingForReLoginServerDone_20 = value;
	}

	inline static int32_t get_offset_of_m_bGotConfigURLDone_21() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___m_bGotConfigURLDone_21)); }
	inline bool get_m_bGotConfigURLDone_21() const { return ___m_bGotConfigURLDone_21; }
	inline bool* get_address_of_m_bGotConfigURLDone_21() { return &___m_bGotConfigURLDone_21; }
	inline void set_m_bGotConfigURLDone_21(bool value)
	{
		___m_bGotConfigURLDone_21 = value;
	}

	inline static int32_t get_offset_of_instance_24() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___instance_24)); }
	inline CapturedDataInput_t2616152122 * get_instance_24() const { return ___instance_24; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_24() { return &___instance_24; }
	inline void set_instance_24(CapturedDataInput_t2616152122 * value)
	{
		___instance_24 = value;
		Il2CppCodeGenWriteBarrier((&___instance_24), value);
	}

	inline static int32_t get_offset_of_instanceTHL_25() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420, ___instanceTHL_25)); }
	inline TokenHolderLegacy_t1380075429 * get_instanceTHL_25() const { return ___instanceTHL_25; }
	inline TokenHolderLegacy_t1380075429 ** get_address_of_instanceTHL_25() { return &___instanceTHL_25; }
	inline void set_instanceTHL_25(TokenHolderLegacy_t1380075429 * value)
	{
		___instanceTHL_25 = value;
		Il2CppCodeGenWriteBarrier((&___instanceTHL_25), value);
	}
};

struct LogoBackgroundScreen_t3640353420_StaticFields
{
public:
	// System.Int32 LogoBackgroundScreen::levelSelectOverride
	int32_t ___levelSelectOverride_18;
	// System.Boolean LogoBackgroundScreen::m_levelOverride
	bool ___m_levelOverride_22;
	// System.String LogoBackgroundScreen::m_savePointOverride
	String_t* ___m_savePointOverride_23;

public:
	inline static int32_t get_offset_of_levelSelectOverride_18() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420_StaticFields, ___levelSelectOverride_18)); }
	inline int32_t get_levelSelectOverride_18() const { return ___levelSelectOverride_18; }
	inline int32_t* get_address_of_levelSelectOverride_18() { return &___levelSelectOverride_18; }
	inline void set_levelSelectOverride_18(int32_t value)
	{
		___levelSelectOverride_18 = value;
	}

	inline static int32_t get_offset_of_m_levelOverride_22() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420_StaticFields, ___m_levelOverride_22)); }
	inline bool get_m_levelOverride_22() const { return ___m_levelOverride_22; }
	inline bool* get_address_of_m_levelOverride_22() { return &___m_levelOverride_22; }
	inline void set_m_levelOverride_22(bool value)
	{
		___m_levelOverride_22 = value;
	}

	inline static int32_t get_offset_of_m_savePointOverride_23() { return static_cast<int32_t>(offsetof(LogoBackgroundScreen_t3640353420_StaticFields, ___m_savePointOverride_23)); }
	inline String_t* get_m_savePointOverride_23() const { return ___m_savePointOverride_23; }
	inline String_t** get_address_of_m_savePointOverride_23() { return &___m_savePointOverride_23; }
	inline void set_m_savePointOverride_23(String_t* value)
	{
		___m_savePointOverride_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_savePointOverride_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGOBACKGROUNDSCREEN_T3640353420_H
#ifndef LEVELNUMSCREEN_T3100306991_H
#define LEVELNUMSCREEN_T3100306991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelNumScreen
struct  LevelNumScreen_t3100306991  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GameObject LevelNumScreen::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_2;
	// UnityEngine.GameObject LevelNumScreen::m_ObjectMobileControllerMove
	GameObject_t2557347079 * ___m_ObjectMobileControllerMove_3;
	// UnityEngine.GameObject LevelNumScreen::m_ObjectMobileControllerSprint
	GameObject_t2557347079 * ___m_ObjectMobileControllerSprint_4;
	// UnityEngine.Vector3 LevelNumScreen::m_ObjectMobileController_LeftPosition
	Vector3_t1986933152  ___m_ObjectMobileController_LeftPosition_5;
	// UnityEngine.Vector3 LevelNumScreen::m_ObjectMobileController_RightPosition
	Vector3_t1986933152  ___m_ObjectMobileController_RightPosition_6;
	// UnityEngine.GUISkin LevelNumScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_7;
	// System.Boolean LevelNumScreen::m_bLogoFadeInStart
	bool ___m_bLogoFadeInStart_8;
	// System.Single LevelNumScreen::m_fFadeInStartTime
	float ___m_fFadeInStartTime_9;
	// System.Single LevelNumScreen::m_fLogoStayTime
	float ___m_fLogoStayTime_10;
	// System.Boolean LevelNumScreen::m_bLogoFadeOutStart
	bool ___m_bLogoFadeOutStart_11;
	// System.Single LevelNumScreen::m_fFadeOutTime
	float ___m_fFadeOutTime_12;
	// System.Single LevelNumScreen::m_fFadeInLogoAlpha
	float ___m_fFadeInLogoAlpha_13;
	// System.Single LevelNumScreen::m_fFadeOutLogoAlpha
	float ___m_fFadeOutLogoAlpha_14;
	// System.Boolean LevelNumScreen::m_bScriptIsKilled
	bool ___m_bScriptIsKilled_15;
	// CapturedDataInput LevelNumScreen::instance
	CapturedDataInput_t2616152122 * ___instance_16;

public:
	inline static int32_t get_offset_of_m_ObjectMobileController_2() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_ObjectMobileController_2)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_2() const { return ___m_ObjectMobileController_2; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_2() { return &___m_ObjectMobileController_2; }
	inline void set_m_ObjectMobileController_2(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_2), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerMove_3() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_ObjectMobileControllerMove_3)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerMove_3() const { return ___m_ObjectMobileControllerMove_3; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerMove_3() { return &___m_ObjectMobileControllerMove_3; }
	inline void set_m_ObjectMobileControllerMove_3(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerMove_3), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerSprint_4() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_ObjectMobileControllerSprint_4)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerSprint_4() const { return ___m_ObjectMobileControllerSprint_4; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerSprint_4() { return &___m_ObjectMobileControllerSprint_4; }
	inline void set_m_ObjectMobileControllerSprint_4(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerSprint_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerSprint_4), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_LeftPosition_5() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_ObjectMobileController_LeftPosition_5)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_LeftPosition_5() const { return ___m_ObjectMobileController_LeftPosition_5; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_LeftPosition_5() { return &___m_ObjectMobileController_LeftPosition_5; }
	inline void set_m_ObjectMobileController_LeftPosition_5(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_LeftPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_RightPosition_6() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_ObjectMobileController_RightPosition_6)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_RightPosition_6() const { return ___m_ObjectMobileController_RightPosition_6; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_RightPosition_6() { return &___m_ObjectMobileController_RightPosition_6; }
	inline void set_m_ObjectMobileController_RightPosition_6(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_RightPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_skin_7() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_skin_7)); }
	inline GUISkin_t2122630221 * get_m_skin_7() const { return ___m_skin_7; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_7() { return &___m_skin_7; }
	inline void set_m_skin_7(GUISkin_t2122630221 * value)
	{
		___m_skin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_7), value);
	}

	inline static int32_t get_offset_of_m_bLogoFadeInStart_8() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_bLogoFadeInStart_8)); }
	inline bool get_m_bLogoFadeInStart_8() const { return ___m_bLogoFadeInStart_8; }
	inline bool* get_address_of_m_bLogoFadeInStart_8() { return &___m_bLogoFadeInStart_8; }
	inline void set_m_bLogoFadeInStart_8(bool value)
	{
		___m_bLogoFadeInStart_8 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_9() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_fFadeInStartTime_9)); }
	inline float get_m_fFadeInStartTime_9() const { return ___m_fFadeInStartTime_9; }
	inline float* get_address_of_m_fFadeInStartTime_9() { return &___m_fFadeInStartTime_9; }
	inline void set_m_fFadeInStartTime_9(float value)
	{
		___m_fFadeInStartTime_9 = value;
	}

	inline static int32_t get_offset_of_m_fLogoStayTime_10() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_fLogoStayTime_10)); }
	inline float get_m_fLogoStayTime_10() const { return ___m_fLogoStayTime_10; }
	inline float* get_address_of_m_fLogoStayTime_10() { return &___m_fLogoStayTime_10; }
	inline void set_m_fLogoStayTime_10(float value)
	{
		___m_fLogoStayTime_10 = value;
	}

	inline static int32_t get_offset_of_m_bLogoFadeOutStart_11() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_bLogoFadeOutStart_11)); }
	inline bool get_m_bLogoFadeOutStart_11() const { return ___m_bLogoFadeOutStart_11; }
	inline bool* get_address_of_m_bLogoFadeOutStart_11() { return &___m_bLogoFadeOutStart_11; }
	inline void set_m_bLogoFadeOutStart_11(bool value)
	{
		___m_bLogoFadeOutStart_11 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutTime_12() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_fFadeOutTime_12)); }
	inline float get_m_fFadeOutTime_12() const { return ___m_fFadeOutTime_12; }
	inline float* get_address_of_m_fFadeOutTime_12() { return &___m_fFadeOutTime_12; }
	inline void set_m_fFadeOutTime_12(float value)
	{
		___m_fFadeOutTime_12 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInLogoAlpha_13() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_fFadeInLogoAlpha_13)); }
	inline float get_m_fFadeInLogoAlpha_13() const { return ___m_fFadeInLogoAlpha_13; }
	inline float* get_address_of_m_fFadeInLogoAlpha_13() { return &___m_fFadeInLogoAlpha_13; }
	inline void set_m_fFadeInLogoAlpha_13(float value)
	{
		___m_fFadeInLogoAlpha_13 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutLogoAlpha_14() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_fFadeOutLogoAlpha_14)); }
	inline float get_m_fFadeOutLogoAlpha_14() const { return ___m_fFadeOutLogoAlpha_14; }
	inline float* get_address_of_m_fFadeOutLogoAlpha_14() { return &___m_fFadeOutLogoAlpha_14; }
	inline void set_m_fFadeOutLogoAlpha_14(float value)
	{
		___m_fFadeOutLogoAlpha_14 = value;
	}

	inline static int32_t get_offset_of_m_bScriptIsKilled_15() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___m_bScriptIsKilled_15)); }
	inline bool get_m_bScriptIsKilled_15() const { return ___m_bScriptIsKilled_15; }
	inline bool* get_address_of_m_bScriptIsKilled_15() { return &___m_bScriptIsKilled_15; }
	inline void set_m_bScriptIsKilled_15(bool value)
	{
		___m_bScriptIsKilled_15 = value;
	}

	inline static int32_t get_offset_of_instance_16() { return static_cast<int32_t>(offsetof(LevelNumScreen_t3100306991, ___instance_16)); }
	inline CapturedDataInput_t2616152122 * get_instance_16() const { return ___instance_16; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_16() { return &___instance_16; }
	inline void set_instance_16(CapturedDataInput_t2616152122 * value)
	{
		___instance_16 = value;
		Il2CppCodeGenWriteBarrier((&___instance_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELNUMSCREEN_T3100306991_H
#ifndef GREETINGSCREEN_T3405050173_H
#define GREETINGSCREEN_T3405050173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreetingScreen
struct  GreetingScreen_t3405050173  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin GreetingScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.Boolean GreetingScreen::m_bLogoFadeInStart
	bool ___m_bLogoFadeInStart_3;
	// System.Single GreetingScreen::m_fFadeInStartTime
	float ___m_fFadeInStartTime_4;
	// System.Single GreetingScreen::m_fLogoStayTime
	float ___m_fLogoStayTime_5;
	// System.Boolean GreetingScreen::m_bLogoFadeOutStart
	bool ___m_bLogoFadeOutStart_6;
	// System.Single GreetingScreen::m_fFadeOutTime
	float ___m_fFadeOutTime_7;
	// System.Single GreetingScreen::m_fFadeInLogoAlpha
	float ___m_fFadeInLogoAlpha_8;
	// System.Single GreetingScreen::m_fFadeOutLogoAlpha
	float ___m_fFadeOutLogoAlpha_9;
	// System.Boolean GreetingScreen::m_bScriptIsKilled
	bool ___m_bScriptIsKilled_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_bLogoFadeInStart_3() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_bLogoFadeInStart_3)); }
	inline bool get_m_bLogoFadeInStart_3() const { return ___m_bLogoFadeInStart_3; }
	inline bool* get_address_of_m_bLogoFadeInStart_3() { return &___m_bLogoFadeInStart_3; }
	inline void set_m_bLogoFadeInStart_3(bool value)
	{
		___m_bLogoFadeInStart_3 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInStartTime_4() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_fFadeInStartTime_4)); }
	inline float get_m_fFadeInStartTime_4() const { return ___m_fFadeInStartTime_4; }
	inline float* get_address_of_m_fFadeInStartTime_4() { return &___m_fFadeInStartTime_4; }
	inline void set_m_fFadeInStartTime_4(float value)
	{
		___m_fFadeInStartTime_4 = value;
	}

	inline static int32_t get_offset_of_m_fLogoStayTime_5() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_fLogoStayTime_5)); }
	inline float get_m_fLogoStayTime_5() const { return ___m_fLogoStayTime_5; }
	inline float* get_address_of_m_fLogoStayTime_5() { return &___m_fLogoStayTime_5; }
	inline void set_m_fLogoStayTime_5(float value)
	{
		___m_fLogoStayTime_5 = value;
	}

	inline static int32_t get_offset_of_m_bLogoFadeOutStart_6() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_bLogoFadeOutStart_6)); }
	inline bool get_m_bLogoFadeOutStart_6() const { return ___m_bLogoFadeOutStart_6; }
	inline bool* get_address_of_m_bLogoFadeOutStart_6() { return &___m_bLogoFadeOutStart_6; }
	inline void set_m_bLogoFadeOutStart_6(bool value)
	{
		___m_bLogoFadeOutStart_6 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutTime_7() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_fFadeOutTime_7)); }
	inline float get_m_fFadeOutTime_7() const { return ___m_fFadeOutTime_7; }
	inline float* get_address_of_m_fFadeOutTime_7() { return &___m_fFadeOutTime_7; }
	inline void set_m_fFadeOutTime_7(float value)
	{
		___m_fFadeOutTime_7 = value;
	}

	inline static int32_t get_offset_of_m_fFadeInLogoAlpha_8() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_fFadeInLogoAlpha_8)); }
	inline float get_m_fFadeInLogoAlpha_8() const { return ___m_fFadeInLogoAlpha_8; }
	inline float* get_address_of_m_fFadeInLogoAlpha_8() { return &___m_fFadeInLogoAlpha_8; }
	inline void set_m_fFadeInLogoAlpha_8(float value)
	{
		___m_fFadeInLogoAlpha_8 = value;
	}

	inline static int32_t get_offset_of_m_fFadeOutLogoAlpha_9() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_fFadeOutLogoAlpha_9)); }
	inline float get_m_fFadeOutLogoAlpha_9() const { return ___m_fFadeOutLogoAlpha_9; }
	inline float* get_address_of_m_fFadeOutLogoAlpha_9() { return &___m_fFadeOutLogoAlpha_9; }
	inline void set_m_fFadeOutLogoAlpha_9(float value)
	{
		___m_fFadeOutLogoAlpha_9 = value;
	}

	inline static int32_t get_offset_of_m_bScriptIsKilled_10() { return static_cast<int32_t>(offsetof(GreetingScreen_t3405050173, ___m_bScriptIsKilled_10)); }
	inline bool get_m_bScriptIsKilled_10() const { return ___m_bScriptIsKilled_10; }
	inline bool* get_address_of_m_bScriptIsKilled_10() { return &___m_bScriptIsKilled_10; }
	inline void set_m_bScriptIsKilled_10(bool value)
	{
		___m_bScriptIsKilled_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREETINGSCREEN_T3405050173_H
#ifndef TUICHARACTERINTERACTION_T4051810501_H
#define TUICHARACTERINTERACTION_T4051810501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TuiCharacterInteraction
struct  TuiCharacterInteraction_t4051810501  : public MonoBehaviour_t1618594486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUICHARACTERINTERACTION_T4051810501_H
#ifndef TITLESCREEN_T1264819243_H
#define TITLESCREEN_T1264819243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleScreen
struct  TitleScreen_t1264819243  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin TitleScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(TitleScreen_t1264819243, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLESCREEN_T1264819243_H
#ifndef THOUGHSANDFEELING_T705500710_H
#define THOUGHSANDFEELING_T705500710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThoughsAndFeeling
struct  ThoughsAndFeeling_t705500710  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin ThoughsAndFeeling::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D ThoughsAndFeeling::m_tBarchartBackground
	Texture2D_t3063074017 * ___m_tBarchartBackground_3;
	// UnityEngine.Texture2D ThoughsAndFeeling::m_tThroughAndFeelingTexture
	Texture2D_t3063074017 * ___m_tThroughAndFeelingTexture_4;
	// UnityEngine.GameObject ThoughsAndFeeling::obj
	GameObject_t2557347079 * ___obj_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(ThoughsAndFeeling_t705500710, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_tBarchartBackground_3() { return static_cast<int32_t>(offsetof(ThoughsAndFeeling_t705500710, ___m_tBarchartBackground_3)); }
	inline Texture2D_t3063074017 * get_m_tBarchartBackground_3() const { return ___m_tBarchartBackground_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarchartBackground_3() { return &___m_tBarchartBackground_3; }
	inline void set_m_tBarchartBackground_3(Texture2D_t3063074017 * value)
	{
		___m_tBarchartBackground_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarchartBackground_3), value);
	}

	inline static int32_t get_offset_of_m_tThroughAndFeelingTexture_4() { return static_cast<int32_t>(offsetof(ThoughsAndFeeling_t705500710, ___m_tThroughAndFeelingTexture_4)); }
	inline Texture2D_t3063074017 * get_m_tThroughAndFeelingTexture_4() const { return ___m_tThroughAndFeelingTexture_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tThroughAndFeelingTexture_4() { return &___m_tThroughAndFeelingTexture_4; }
	inline void set_m_tThroughAndFeelingTexture_4(Texture2D_t3063074017 * value)
	{
		___m_tThroughAndFeelingTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tThroughAndFeelingTexture_4), value);
	}

	inline static int32_t get_offset_of_obj_5() { return static_cast<int32_t>(offsetof(ThoughsAndFeeling_t705500710, ___obj_5)); }
	inline GameObject_t2557347079 * get_obj_5() const { return ___obj_5; }
	inline GameObject_t2557347079 ** get_address_of_obj_5() { return &___obj_5; }
	inline void set_obj_5(GameObject_t2557347079 * value)
	{
		___obj_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THOUGHSANDFEELING_T705500710_H
#ifndef TERMINATETALKSCENE_T870503985_H
#define TERMINATETALKSCENE_T870503985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TerminateTalkScene
struct  TerminateTalkScene_t870503985  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TerminateTalkScene::m_bSwapGuyWalkingAway
	bool ___m_bSwapGuyWalkingAway_2;
	// UnityEngine.AudioClip TerminateTalkScene::m_aLevel7CutSceneMusic
	AudioClip_t1419814565 * ___m_aLevel7CutSceneMusic_3;
	// System.String TerminateTalkScene::interactObjectHeadToID
	String_t* ___interactObjectHeadToID_5;
	// System.Boolean TerminateTalkScene::m_bPlayerStayInDark
	bool ___m_bPlayerStayInDark_6;
	// System.Single TerminateTalkScene::m_fNPCToPlayerDistance
	float ___m_fNPCToPlayerDistance_7;
	// System.Single TerminateTalkScene::m_fSetNPCIneteractionBackDistance
	float ___m_fSetNPCIneteractionBackDistance_8;

public:
	inline static int32_t get_offset_of_m_bSwapGuyWalkingAway_2() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___m_bSwapGuyWalkingAway_2)); }
	inline bool get_m_bSwapGuyWalkingAway_2() const { return ___m_bSwapGuyWalkingAway_2; }
	inline bool* get_address_of_m_bSwapGuyWalkingAway_2() { return &___m_bSwapGuyWalkingAway_2; }
	inline void set_m_bSwapGuyWalkingAway_2(bool value)
	{
		___m_bSwapGuyWalkingAway_2 = value;
	}

	inline static int32_t get_offset_of_m_aLevel7CutSceneMusic_3() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___m_aLevel7CutSceneMusic_3)); }
	inline AudioClip_t1419814565 * get_m_aLevel7CutSceneMusic_3() const { return ___m_aLevel7CutSceneMusic_3; }
	inline AudioClip_t1419814565 ** get_address_of_m_aLevel7CutSceneMusic_3() { return &___m_aLevel7CutSceneMusic_3; }
	inline void set_m_aLevel7CutSceneMusic_3(AudioClip_t1419814565 * value)
	{
		___m_aLevel7CutSceneMusic_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aLevel7CutSceneMusic_3), value);
	}

	inline static int32_t get_offset_of_interactObjectHeadToID_5() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___interactObjectHeadToID_5)); }
	inline String_t* get_interactObjectHeadToID_5() const { return ___interactObjectHeadToID_5; }
	inline String_t** get_address_of_interactObjectHeadToID_5() { return &___interactObjectHeadToID_5; }
	inline void set_interactObjectHeadToID_5(String_t* value)
	{
		___interactObjectHeadToID_5 = value;
		Il2CppCodeGenWriteBarrier((&___interactObjectHeadToID_5), value);
	}

	inline static int32_t get_offset_of_m_bPlayerStayInDark_6() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___m_bPlayerStayInDark_6)); }
	inline bool get_m_bPlayerStayInDark_6() const { return ___m_bPlayerStayInDark_6; }
	inline bool* get_address_of_m_bPlayerStayInDark_6() { return &___m_bPlayerStayInDark_6; }
	inline void set_m_bPlayerStayInDark_6(bool value)
	{
		___m_bPlayerStayInDark_6 = value;
	}

	inline static int32_t get_offset_of_m_fNPCToPlayerDistance_7() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___m_fNPCToPlayerDistance_7)); }
	inline float get_m_fNPCToPlayerDistance_7() const { return ___m_fNPCToPlayerDistance_7; }
	inline float* get_address_of_m_fNPCToPlayerDistance_7() { return &___m_fNPCToPlayerDistance_7; }
	inline void set_m_fNPCToPlayerDistance_7(float value)
	{
		___m_fNPCToPlayerDistance_7 = value;
	}

	inline static int32_t get_offset_of_m_fSetNPCIneteractionBackDistance_8() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985, ___m_fSetNPCIneteractionBackDistance_8)); }
	inline float get_m_fSetNPCIneteractionBackDistance_8() const { return ___m_fSetNPCIneteractionBackDistance_8; }
	inline float* get_address_of_m_fSetNPCIneteractionBackDistance_8() { return &___m_fSetNPCIneteractionBackDistance_8; }
	inline void set_m_fSetNPCIneteractionBackDistance_8(float value)
	{
		___m_fSetNPCIneteractionBackDistance_8 = value;
	}
};

struct TerminateTalkScene_t870503985_StaticFields
{
public:
	// UnityEngine.GameObject TerminateTalkScene::interactObject
	GameObject_t2557347079 * ___interactObject_4;

public:
	inline static int32_t get_offset_of_interactObject_4() { return static_cast<int32_t>(offsetof(TerminateTalkScene_t870503985_StaticFields, ___interactObject_4)); }
	inline GameObject_t2557347079 * get_interactObject_4() const { return ___interactObject_4; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_4() { return &___interactObject_4; }
	inline void set_interactObject_4(GameObject_t2557347079 * value)
	{
		___interactObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINATETALKSCENE_T870503985_H
#ifndef TERMINATECASSFIREMANSCENE_T2717791514_H
#define TERMINATECASSFIREMANSCENE_T2717791514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TerminateCassFiremanScene
struct  TerminateCassFiremanScene_t2717791514  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean TerminateCassFiremanScene::m_firePersonConversationFinished
	bool ___m_firePersonConversationFinished_2;
	// UnityEngine.GameObject TerminateCassFiremanScene::interactObject
	GameObject_t2557347079 * ___interactObject_3;
	// UnityEngine.Vector3 TerminateCassFiremanScene::cassStartPos
	Vector3_t1986933152  ___cassStartPos_4;
	// UnityEngine.Vector3 TerminateCassFiremanScene::cassTargetPos
	Vector3_t1986933152  ___cassTargetPos_5;
	// UnityEngine.Vector3 TerminateCassFiremanScene::playerStartPos
	Vector3_t1986933152  ___playerStartPos_6;
	// UnityEngine.Vector3 TerminateCassFiremanScene::playerTargetPos
	Vector3_t1986933152  ___playerTargetPos_7;
	// System.Single TerminateCassFiremanScene::m_fCassFirePersonDistance
	float ___m_fCassFirePersonDistance_8;
	// System.Single TerminateCassFiremanScene::m_fPlayerFirePersonDistance
	float ___m_fPlayerFirePersonDistance_9;
	// System.Single TerminateCassFiremanScene::m_fCassPlayerDistance
	float ___m_fCassPlayerDistance_10;
	// System.Single TerminateCassFiremanScene::m_fCassWalkTime
	float ___m_fCassWalkTime_11;
	// System.Single TerminateCassFiremanScene::m_fPlayerWalkTime
	float ___m_fPlayerWalkTime_12;
	// System.Single TerminateCassFiremanScene::m_fCaseWalkSpeed
	float ___m_fCaseWalkSpeed_13;
	// System.Single TerminateCassFiremanScene::m_fPlayerWalkSpeed
	float ___m_fPlayerWalkSpeed_14;
	// System.Boolean TerminateCassFiremanScene::m_bMovingToTarget
	bool ___m_bMovingToTarget_15;
	// System.Boolean TerminateCassFiremanScene::m_bMovingToPlayerTarget
	bool ___m_bMovingToPlayerTarget_16;
	// System.Boolean TerminateCassFiremanScene::m_bCassReachesTarget
	bool ___m_bCassReachesTarget_17;
	// System.Boolean TerminateCassFiremanScene::m_bPlayerReachesTarget
	bool ___m_bPlayerReachesTarget_18;
	// System.Boolean TerminateCassFiremanScene::m_bCassPlayIdleOnce
	bool ___m_bCassPlayIdleOnce_19;

public:
	inline static int32_t get_offset_of_m_firePersonConversationFinished_2() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_firePersonConversationFinished_2)); }
	inline bool get_m_firePersonConversationFinished_2() const { return ___m_firePersonConversationFinished_2; }
	inline bool* get_address_of_m_firePersonConversationFinished_2() { return &___m_firePersonConversationFinished_2; }
	inline void set_m_firePersonConversationFinished_2(bool value)
	{
		___m_firePersonConversationFinished_2 = value;
	}

	inline static int32_t get_offset_of_interactObject_3() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___interactObject_3)); }
	inline GameObject_t2557347079 * get_interactObject_3() const { return ___interactObject_3; }
	inline GameObject_t2557347079 ** get_address_of_interactObject_3() { return &___interactObject_3; }
	inline void set_interactObject_3(GameObject_t2557347079 * value)
	{
		___interactObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactObject_3), value);
	}

	inline static int32_t get_offset_of_cassStartPos_4() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___cassStartPos_4)); }
	inline Vector3_t1986933152  get_cassStartPos_4() const { return ___cassStartPos_4; }
	inline Vector3_t1986933152 * get_address_of_cassStartPos_4() { return &___cassStartPos_4; }
	inline void set_cassStartPos_4(Vector3_t1986933152  value)
	{
		___cassStartPos_4 = value;
	}

	inline static int32_t get_offset_of_cassTargetPos_5() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___cassTargetPos_5)); }
	inline Vector3_t1986933152  get_cassTargetPos_5() const { return ___cassTargetPos_5; }
	inline Vector3_t1986933152 * get_address_of_cassTargetPos_5() { return &___cassTargetPos_5; }
	inline void set_cassTargetPos_5(Vector3_t1986933152  value)
	{
		___cassTargetPos_5 = value;
	}

	inline static int32_t get_offset_of_playerStartPos_6() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___playerStartPos_6)); }
	inline Vector3_t1986933152  get_playerStartPos_6() const { return ___playerStartPos_6; }
	inline Vector3_t1986933152 * get_address_of_playerStartPos_6() { return &___playerStartPos_6; }
	inline void set_playerStartPos_6(Vector3_t1986933152  value)
	{
		___playerStartPos_6 = value;
	}

	inline static int32_t get_offset_of_playerTargetPos_7() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___playerTargetPos_7)); }
	inline Vector3_t1986933152  get_playerTargetPos_7() const { return ___playerTargetPos_7; }
	inline Vector3_t1986933152 * get_address_of_playerTargetPos_7() { return &___playerTargetPos_7; }
	inline void set_playerTargetPos_7(Vector3_t1986933152  value)
	{
		___playerTargetPos_7 = value;
	}

	inline static int32_t get_offset_of_m_fCassFirePersonDistance_8() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fCassFirePersonDistance_8)); }
	inline float get_m_fCassFirePersonDistance_8() const { return ___m_fCassFirePersonDistance_8; }
	inline float* get_address_of_m_fCassFirePersonDistance_8() { return &___m_fCassFirePersonDistance_8; }
	inline void set_m_fCassFirePersonDistance_8(float value)
	{
		___m_fCassFirePersonDistance_8 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerFirePersonDistance_9() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fPlayerFirePersonDistance_9)); }
	inline float get_m_fPlayerFirePersonDistance_9() const { return ___m_fPlayerFirePersonDistance_9; }
	inline float* get_address_of_m_fPlayerFirePersonDistance_9() { return &___m_fPlayerFirePersonDistance_9; }
	inline void set_m_fPlayerFirePersonDistance_9(float value)
	{
		___m_fPlayerFirePersonDistance_9 = value;
	}

	inline static int32_t get_offset_of_m_fCassPlayerDistance_10() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fCassPlayerDistance_10)); }
	inline float get_m_fCassPlayerDistance_10() const { return ___m_fCassPlayerDistance_10; }
	inline float* get_address_of_m_fCassPlayerDistance_10() { return &___m_fCassPlayerDistance_10; }
	inline void set_m_fCassPlayerDistance_10(float value)
	{
		___m_fCassPlayerDistance_10 = value;
	}

	inline static int32_t get_offset_of_m_fCassWalkTime_11() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fCassWalkTime_11)); }
	inline float get_m_fCassWalkTime_11() const { return ___m_fCassWalkTime_11; }
	inline float* get_address_of_m_fCassWalkTime_11() { return &___m_fCassWalkTime_11; }
	inline void set_m_fCassWalkTime_11(float value)
	{
		___m_fCassWalkTime_11 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerWalkTime_12() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fPlayerWalkTime_12)); }
	inline float get_m_fPlayerWalkTime_12() const { return ___m_fPlayerWalkTime_12; }
	inline float* get_address_of_m_fPlayerWalkTime_12() { return &___m_fPlayerWalkTime_12; }
	inline void set_m_fPlayerWalkTime_12(float value)
	{
		___m_fPlayerWalkTime_12 = value;
	}

	inline static int32_t get_offset_of_m_fCaseWalkSpeed_13() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fCaseWalkSpeed_13)); }
	inline float get_m_fCaseWalkSpeed_13() const { return ___m_fCaseWalkSpeed_13; }
	inline float* get_address_of_m_fCaseWalkSpeed_13() { return &___m_fCaseWalkSpeed_13; }
	inline void set_m_fCaseWalkSpeed_13(float value)
	{
		___m_fCaseWalkSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerWalkSpeed_14() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_fPlayerWalkSpeed_14)); }
	inline float get_m_fPlayerWalkSpeed_14() const { return ___m_fPlayerWalkSpeed_14; }
	inline float* get_address_of_m_fPlayerWalkSpeed_14() { return &___m_fPlayerWalkSpeed_14; }
	inline void set_m_fPlayerWalkSpeed_14(float value)
	{
		___m_fPlayerWalkSpeed_14 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToTarget_15() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_bMovingToTarget_15)); }
	inline bool get_m_bMovingToTarget_15() const { return ___m_bMovingToTarget_15; }
	inline bool* get_address_of_m_bMovingToTarget_15() { return &___m_bMovingToTarget_15; }
	inline void set_m_bMovingToTarget_15(bool value)
	{
		___m_bMovingToTarget_15 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToPlayerTarget_16() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_bMovingToPlayerTarget_16)); }
	inline bool get_m_bMovingToPlayerTarget_16() const { return ___m_bMovingToPlayerTarget_16; }
	inline bool* get_address_of_m_bMovingToPlayerTarget_16() { return &___m_bMovingToPlayerTarget_16; }
	inline void set_m_bMovingToPlayerTarget_16(bool value)
	{
		___m_bMovingToPlayerTarget_16 = value;
	}

	inline static int32_t get_offset_of_m_bCassReachesTarget_17() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_bCassReachesTarget_17)); }
	inline bool get_m_bCassReachesTarget_17() const { return ___m_bCassReachesTarget_17; }
	inline bool* get_address_of_m_bCassReachesTarget_17() { return &___m_bCassReachesTarget_17; }
	inline void set_m_bCassReachesTarget_17(bool value)
	{
		___m_bCassReachesTarget_17 = value;
	}

	inline static int32_t get_offset_of_m_bPlayerReachesTarget_18() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_bPlayerReachesTarget_18)); }
	inline bool get_m_bPlayerReachesTarget_18() const { return ___m_bPlayerReachesTarget_18; }
	inline bool* get_address_of_m_bPlayerReachesTarget_18() { return &___m_bPlayerReachesTarget_18; }
	inline void set_m_bPlayerReachesTarget_18(bool value)
	{
		___m_bPlayerReachesTarget_18 = value;
	}

	inline static int32_t get_offset_of_m_bCassPlayIdleOnce_19() { return static_cast<int32_t>(offsetof(TerminateCassFiremanScene_t2717791514, ___m_bCassPlayIdleOnce_19)); }
	inline bool get_m_bCassPlayIdleOnce_19() const { return ___m_bCassPlayIdleOnce_19; }
	inline bool* get_address_of_m_bCassPlayIdleOnce_19() { return &___m_bCassPlayIdleOnce_19; }
	inline void set_m_bCassPlayIdleOnce_19(bool value)
	{
		___m_bCassPlayIdleOnce_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINATECASSFIREMANSCENE_T2717791514_H
#ifndef TALKSCENEWITHIMAGE_T4268521949_H
#define TALKSCENEWITHIMAGE_T4268521949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkSceneWithImage
struct  TalkSceneWithImage_t4268521949  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin TalkSceneWithImage::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D TalkSceneWithImage::m_tConversationBackground
	Texture2D_t3063074017 * ___m_tConversationBackground_3;
	// UnityEngine.Texture2D TalkSceneWithImage::m_tCharacterFaceTexture
	Texture2D_t3063074017 * ___m_tCharacterFaceTexture_4;
	// LoadTalkSceneXMLData TalkSceneWithImage::m_loadSceneData
	LoadTalkSceneXMLData_t2381108950 * ___m_loadSceneData_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(TalkSceneWithImage_t4268521949, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_tConversationBackground_3() { return static_cast<int32_t>(offsetof(TalkSceneWithImage_t4268521949, ___m_tConversationBackground_3)); }
	inline Texture2D_t3063074017 * get_m_tConversationBackground_3() const { return ___m_tConversationBackground_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_tConversationBackground_3() { return &___m_tConversationBackground_3; }
	inline void set_m_tConversationBackground_3(Texture2D_t3063074017 * value)
	{
		___m_tConversationBackground_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tConversationBackground_3), value);
	}

	inline static int32_t get_offset_of_m_tCharacterFaceTexture_4() { return static_cast<int32_t>(offsetof(TalkSceneWithImage_t4268521949, ___m_tCharacterFaceTexture_4)); }
	inline Texture2D_t3063074017 * get_m_tCharacterFaceTexture_4() const { return ___m_tCharacterFaceTexture_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tCharacterFaceTexture_4() { return &___m_tCharacterFaceTexture_4; }
	inline void set_m_tCharacterFaceTexture_4(Texture2D_t3063074017 * value)
	{
		___m_tCharacterFaceTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tCharacterFaceTexture_4), value);
	}

	inline static int32_t get_offset_of_m_loadSceneData_5() { return static_cast<int32_t>(offsetof(TalkSceneWithImage_t4268521949, ___m_loadSceneData_5)); }
	inline LoadTalkSceneXMLData_t2381108950 * get_m_loadSceneData_5() const { return ___m_loadSceneData_5; }
	inline LoadTalkSceneXMLData_t2381108950 ** get_address_of_m_loadSceneData_5() { return &___m_loadSceneData_5; }
	inline void set_m_loadSceneData_5(LoadTalkSceneXMLData_t2381108950 * value)
	{
		___m_loadSceneData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_loadSceneData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TALKSCENEWITHIMAGE_T4268521949_H
#ifndef TALKSCENES_T1215051795_H
#define TALKSCENES_T1215051795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkScenes
struct  TalkScenes_t1215051795  : public MonoBehaviour_t1618594486
{
public:
	// System.String TalkScenes::m_currentScene
	String_t* ___m_currentScene_3;
	// System.String TalkScenes::tuiScene
	String_t* ___tuiScene_4;
	// System.Boolean TalkScenes::m_bGuardianNeedsLoad
	bool ___m_bGuardianNeedsLoad_5;
	// System.Boolean TalkScenes::m_voiceCanStart
	bool ___m_voiceCanStart_6;
	// System.Boolean TalkScenes::m_bTalkedWithGuardian
	bool ___m_bTalkedWithGuardian_7;
	// System.Boolean TalkScenes::m_bTalkedWithMentor
	bool ___m_bTalkedWithMentor_8;
	// System.Boolean TalkScenes::m_bL7NeedHelpOptionA
	bool ___m_bL7NeedHelpOptionA_9;
	// System.Boolean TalkScenes::m_bL7UnNeedHelpOptionB
	bool ___m_bL7UnNeedHelpOptionB_10;
	// System.Single TalkScenes::m_autoSceneStartTime
	float ___m_autoSceneStartTime_11;
	// System.Boolean TalkScenes::m_bCassFiremanTalkStart
	bool ___m_bCassFiremanTalkStart_12;
	// System.Int32[] TalkScenes::m_PHQAAnswers
	Int32U5BU5D_t1965588061* ___m_PHQAAnswers_13;
	// System.Int32 TalkScenes::m_iTestingLevelNum
	int32_t ___m_iTestingLevelNum_14;
	// System.Boolean[] TalkScenes::m_bChiceSelectedDetector
	BooleanU5BU5D_t698278498* ___m_bChiceSelectedDetector_15;
	// UnityEngine.TextAsset TalkScenes::m_guideXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_guideXmlFilePathTextAsset_16;
	// UnityEngine.TextAsset TalkScenes::m_mentorXMLFilePathTextAsset
	TextAsset_t2052027493 * ___m_mentorXMLFilePathTextAsset_17;
	// UnityEngine.TextAsset TalkScenes::m_hopeXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_hopeXmlFilePathTextAsset_18;
	// UnityEngine.TextAsset TalkScenes::m_guardianXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_guardianXmlFilePathTextAsset_19;
	// UnityEngine.TextAsset TalkScenes::m_1npcXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_1npcXmlFilePathTextAsset_20;
	// UnityEngine.TextAsset TalkScenes::m_travelerXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_travelerXmlFilePathTextAsset_21;
	// UnityEngine.TextAsset TalkScenes::m_cassXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_cassXmlFilePathTextAsset_22;
	// UnityEngine.TextAsset TalkScenes::m_fireXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_fireXmlFilePathTextAsset_23;
	// UnityEngine.TextAsset TalkScenes::m_yetiXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_yetiXmlFilePathTextAsset_24;
	// UnityEngine.TextAsset TalkScenes::m_fireperson2XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_fireperson2XmlFilePathTextAsset_25;
	// UnityEngine.TextAsset TalkScenes::m_firespiritXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_firespiritXmlFilePathTextAsset_26;
	// UnityEngine.TextAsset TalkScenes::m_npcXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_npcXmlFilePathTextAsset_27;
	// UnityEngine.TextAsset TalkScenes::m_cass2XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_cass2XmlFilePathTextAsset_28;
	// UnityEngine.TextAsset TalkScenes::m_npc2XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_npc2XmlFilePathTextAsset_29;
	// UnityEngine.TextAsset TalkScenes::m_darroXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_darroXmlFilePathTextAsset_30;
	// UnityEngine.TextAsset TalkScenes::m_gnatsXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_gnatsXmlFilePathTextAsset_31;
	// UnityEngine.TextAsset TalkScenes::m_cass3XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_cass3XmlFilePathTextAsset_32;
	// UnityEngine.TextAsset TalkScenes::m_sparksXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_sparksXmlFilePathTextAsset_33;
	// UnityEngine.TextAsset TalkScenes::m_swapguyXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_swapguyXmlFilePathTextAsset_34;
	// UnityEngine.TextAsset TalkScenes::m_ntsXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_ntsXmlFilePathTextAsset_35;
	// UnityEngine.TextAsset TalkScenes::m_bridgeWomanXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_bridgeWomanXmlFilePathTextAsset_36;
	// UnityEngine.TextAsset TalkScenes::m_templeGuidianXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_templeGuidianXmlFilePathTextAsset_37;
	// UnityEngine.TextAsset TalkScenes::m_cass4XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_cass4XmlFilePathTextAsset_38;
	// UnityEngine.TextAsset TalkScenes::m_examinerXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_examinerXmlFilePathTextAsset_39;
	// UnityEngine.TextAsset TalkScenes::m_swapGuyXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_swapGuyXmlFilePathTextAsset_40;
	// UnityEngine.TextAsset TalkScenes::m_playerNeedsHelpTextAsset
	TextAsset_t2052027493 * ___m_playerNeedsHelpTextAsset_41;
	// UnityEngine.TextAsset TalkScenes::m_cassL7XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_cassL7XmlFilePathTextAsset_42;
	// UnityEngine.TextAsset TalkScenes::m_darroL7XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_darroL7XmlFilePathTextAsset_43;
	// UnityEngine.TextAsset TalkScenes::m_firepersonL7XmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_firepersonL7XmlFilePathTextAsset_44;
	// UnityEngine.TextAsset TalkScenes::m_kogXmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_kogXmlFilePathTextAsset_45;
	// System.Int32 TalkScenes::m_iPerPHQAQuestionScore
	int32_t ___m_iPerPHQAQuestionScore_46;
	// System.Int32 TalkScenes::m_iPHQAQuestionNum
	int32_t ___m_iPHQAQuestionNum_47;
	// System.Int32 TalkScenes::m_iQ9Score
	int32_t ___m_iQ9Score_48;
	// UnityEngine.GUISkin TalkScenes::m_skin
	GUISkin_t2122630221 * ___m_skin_49;
	// LoadTalkSceneXMLData TalkScenes::m_loadSceneData
	LoadTalkSceneXMLData_t2381108950 * ___m_loadSceneData_50;
	// UnityEngine.AudioClip[] TalkScenes::L1AudioFileList
	AudioClipU5BU5D_t3624204584* ___L1AudioFileList_51;
	// UnityEngine.AudioClip[] TalkScenes::L2AudioFileList
	AudioClipU5BU5D_t3624204584* ___L2AudioFileList_52;
	// UnityEngine.AudioClip[] TalkScenes::L3AudioFileList
	AudioClipU5BU5D_t3624204584* ___L3AudioFileList_53;
	// UnityEngine.AudioClip[] TalkScenes::L4AudioFileList
	AudioClipU5BU5D_t3624204584* ___L4AudioFileList_54;
	// UnityEngine.AudioClip[] TalkScenes::L5AudioFileList
	AudioClipU5BU5D_t3624204584* ___L5AudioFileList_55;
	// UnityEngine.AudioClip[] TalkScenes::L6AudioFileList
	AudioClipU5BU5D_t3624204584* ___L6AudioFileList_56;
	// UnityEngine.AudioClip[] TalkScenes::L7AudioFileList
	AudioClipU5BU5D_t3624204584* ___L7AudioFileList_57;
	// System.Single TalkScenes::spatialBlend
	float ___spatialBlend_58;
	// UnityEngine.AudioSource TalkScenes::m_characterVoiceAS
	AudioSource_t4025721661 * ___m_characterVoiceAS_59;
	// System.Boolean TalkScenes::m_bAutoConversationUnFinished
	bool ___m_bAutoConversationUnFinished_60;
	// System.Boolean TalkScenes::m_bNPC1AnimationPlayOnce
	bool ___m_bNPC1AnimationPlayOnce_61;
	// System.Boolean TalkScenes::m_bNPC2AnimationPlayOnce
	bool ___m_bNPC2AnimationPlayOnce_62;
	// System.Boolean TalkScenes::m_bMultiChoice1OptionA
	bool ___m_bMultiChoice1OptionA_63;
	// System.Boolean TalkScenes::m_bMultiChoice1OptionB
	bool ___m_bMultiChoice1OptionB_64;
	// System.Boolean TalkScenes::m_bMultiChoice1OptionC
	bool ___m_bMultiChoice1OptionC_65;
	// System.Boolean TalkScenes::m_bMultiChoice1OptionD
	bool ___m_bMultiChoice1OptionD_66;
	// System.Int32 TalkScenes::m_PHQAAnswerIndex
	int32_t ___m_PHQAAnswerIndex_67;
	// CapturedDataInput TalkScenes::instance
	CapturedDataInput_t2616152122 * ___instance_68;
	// System.String TalkScenes::m_sTestWholeDynamic
	String_t* ___m_sTestWholeDynamic_69;
	// System.String TalkScenes::m_sTestDynamicToken
	String_t* ___m_sTestDynamicToken_70;
	// System.String TalkScenes::m_sTestDynamicURL
	String_t* ___m_sTestDynamicURL_71;
	// System.Boolean TalkScenes::m_BoolSkipVoiceErrorLevel2
	bool ___m_BoolSkipVoiceErrorLevel2_72;

public:
	inline static int32_t get_offset_of_m_currentScene_3() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_currentScene_3)); }
	inline String_t* get_m_currentScene_3() const { return ___m_currentScene_3; }
	inline String_t** get_address_of_m_currentScene_3() { return &___m_currentScene_3; }
	inline void set_m_currentScene_3(String_t* value)
	{
		___m_currentScene_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentScene_3), value);
	}

	inline static int32_t get_offset_of_tuiScene_4() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___tuiScene_4)); }
	inline String_t* get_tuiScene_4() const { return ___tuiScene_4; }
	inline String_t** get_address_of_tuiScene_4() { return &___tuiScene_4; }
	inline void set_tuiScene_4(String_t* value)
	{
		___tuiScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuiScene_4), value);
	}

	inline static int32_t get_offset_of_m_bGuardianNeedsLoad_5() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bGuardianNeedsLoad_5)); }
	inline bool get_m_bGuardianNeedsLoad_5() const { return ___m_bGuardianNeedsLoad_5; }
	inline bool* get_address_of_m_bGuardianNeedsLoad_5() { return &___m_bGuardianNeedsLoad_5; }
	inline void set_m_bGuardianNeedsLoad_5(bool value)
	{
		___m_bGuardianNeedsLoad_5 = value;
	}

	inline static int32_t get_offset_of_m_voiceCanStart_6() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_voiceCanStart_6)); }
	inline bool get_m_voiceCanStart_6() const { return ___m_voiceCanStart_6; }
	inline bool* get_address_of_m_voiceCanStart_6() { return &___m_voiceCanStart_6; }
	inline void set_m_voiceCanStart_6(bool value)
	{
		___m_voiceCanStart_6 = value;
	}

	inline static int32_t get_offset_of_m_bTalkedWithGuardian_7() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bTalkedWithGuardian_7)); }
	inline bool get_m_bTalkedWithGuardian_7() const { return ___m_bTalkedWithGuardian_7; }
	inline bool* get_address_of_m_bTalkedWithGuardian_7() { return &___m_bTalkedWithGuardian_7; }
	inline void set_m_bTalkedWithGuardian_7(bool value)
	{
		___m_bTalkedWithGuardian_7 = value;
	}

	inline static int32_t get_offset_of_m_bTalkedWithMentor_8() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bTalkedWithMentor_8)); }
	inline bool get_m_bTalkedWithMentor_8() const { return ___m_bTalkedWithMentor_8; }
	inline bool* get_address_of_m_bTalkedWithMentor_8() { return &___m_bTalkedWithMentor_8; }
	inline void set_m_bTalkedWithMentor_8(bool value)
	{
		___m_bTalkedWithMentor_8 = value;
	}

	inline static int32_t get_offset_of_m_bL7NeedHelpOptionA_9() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bL7NeedHelpOptionA_9)); }
	inline bool get_m_bL7NeedHelpOptionA_9() const { return ___m_bL7NeedHelpOptionA_9; }
	inline bool* get_address_of_m_bL7NeedHelpOptionA_9() { return &___m_bL7NeedHelpOptionA_9; }
	inline void set_m_bL7NeedHelpOptionA_9(bool value)
	{
		___m_bL7NeedHelpOptionA_9 = value;
	}

	inline static int32_t get_offset_of_m_bL7UnNeedHelpOptionB_10() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bL7UnNeedHelpOptionB_10)); }
	inline bool get_m_bL7UnNeedHelpOptionB_10() const { return ___m_bL7UnNeedHelpOptionB_10; }
	inline bool* get_address_of_m_bL7UnNeedHelpOptionB_10() { return &___m_bL7UnNeedHelpOptionB_10; }
	inline void set_m_bL7UnNeedHelpOptionB_10(bool value)
	{
		___m_bL7UnNeedHelpOptionB_10 = value;
	}

	inline static int32_t get_offset_of_m_autoSceneStartTime_11() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_autoSceneStartTime_11)); }
	inline float get_m_autoSceneStartTime_11() const { return ___m_autoSceneStartTime_11; }
	inline float* get_address_of_m_autoSceneStartTime_11() { return &___m_autoSceneStartTime_11; }
	inline void set_m_autoSceneStartTime_11(float value)
	{
		___m_autoSceneStartTime_11 = value;
	}

	inline static int32_t get_offset_of_m_bCassFiremanTalkStart_12() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bCassFiremanTalkStart_12)); }
	inline bool get_m_bCassFiremanTalkStart_12() const { return ___m_bCassFiremanTalkStart_12; }
	inline bool* get_address_of_m_bCassFiremanTalkStart_12() { return &___m_bCassFiremanTalkStart_12; }
	inline void set_m_bCassFiremanTalkStart_12(bool value)
	{
		___m_bCassFiremanTalkStart_12 = value;
	}

	inline static int32_t get_offset_of_m_PHQAAnswers_13() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_PHQAAnswers_13)); }
	inline Int32U5BU5D_t1965588061* get_m_PHQAAnswers_13() const { return ___m_PHQAAnswers_13; }
	inline Int32U5BU5D_t1965588061** get_address_of_m_PHQAAnswers_13() { return &___m_PHQAAnswers_13; }
	inline void set_m_PHQAAnswers_13(Int32U5BU5D_t1965588061* value)
	{
		___m_PHQAAnswers_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PHQAAnswers_13), value);
	}

	inline static int32_t get_offset_of_m_iTestingLevelNum_14() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_iTestingLevelNum_14)); }
	inline int32_t get_m_iTestingLevelNum_14() const { return ___m_iTestingLevelNum_14; }
	inline int32_t* get_address_of_m_iTestingLevelNum_14() { return &___m_iTestingLevelNum_14; }
	inline void set_m_iTestingLevelNum_14(int32_t value)
	{
		___m_iTestingLevelNum_14 = value;
	}

	inline static int32_t get_offset_of_m_bChiceSelectedDetector_15() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bChiceSelectedDetector_15)); }
	inline BooleanU5BU5D_t698278498* get_m_bChiceSelectedDetector_15() const { return ___m_bChiceSelectedDetector_15; }
	inline BooleanU5BU5D_t698278498** get_address_of_m_bChiceSelectedDetector_15() { return &___m_bChiceSelectedDetector_15; }
	inline void set_m_bChiceSelectedDetector_15(BooleanU5BU5D_t698278498* value)
	{
		___m_bChiceSelectedDetector_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_bChiceSelectedDetector_15), value);
	}

	inline static int32_t get_offset_of_m_guideXmlFilePathTextAsset_16() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_guideXmlFilePathTextAsset_16)); }
	inline TextAsset_t2052027493 * get_m_guideXmlFilePathTextAsset_16() const { return ___m_guideXmlFilePathTextAsset_16; }
	inline TextAsset_t2052027493 ** get_address_of_m_guideXmlFilePathTextAsset_16() { return &___m_guideXmlFilePathTextAsset_16; }
	inline void set_m_guideXmlFilePathTextAsset_16(TextAsset_t2052027493 * value)
	{
		___m_guideXmlFilePathTextAsset_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_guideXmlFilePathTextAsset_16), value);
	}

	inline static int32_t get_offset_of_m_mentorXMLFilePathTextAsset_17() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_mentorXMLFilePathTextAsset_17)); }
	inline TextAsset_t2052027493 * get_m_mentorXMLFilePathTextAsset_17() const { return ___m_mentorXMLFilePathTextAsset_17; }
	inline TextAsset_t2052027493 ** get_address_of_m_mentorXMLFilePathTextAsset_17() { return &___m_mentorXMLFilePathTextAsset_17; }
	inline void set_m_mentorXMLFilePathTextAsset_17(TextAsset_t2052027493 * value)
	{
		___m_mentorXMLFilePathTextAsset_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_mentorXMLFilePathTextAsset_17), value);
	}

	inline static int32_t get_offset_of_m_hopeXmlFilePathTextAsset_18() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_hopeXmlFilePathTextAsset_18)); }
	inline TextAsset_t2052027493 * get_m_hopeXmlFilePathTextAsset_18() const { return ___m_hopeXmlFilePathTextAsset_18; }
	inline TextAsset_t2052027493 ** get_address_of_m_hopeXmlFilePathTextAsset_18() { return &___m_hopeXmlFilePathTextAsset_18; }
	inline void set_m_hopeXmlFilePathTextAsset_18(TextAsset_t2052027493 * value)
	{
		___m_hopeXmlFilePathTextAsset_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_hopeXmlFilePathTextAsset_18), value);
	}

	inline static int32_t get_offset_of_m_guardianXmlFilePathTextAsset_19() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_guardianXmlFilePathTextAsset_19)); }
	inline TextAsset_t2052027493 * get_m_guardianXmlFilePathTextAsset_19() const { return ___m_guardianXmlFilePathTextAsset_19; }
	inline TextAsset_t2052027493 ** get_address_of_m_guardianXmlFilePathTextAsset_19() { return &___m_guardianXmlFilePathTextAsset_19; }
	inline void set_m_guardianXmlFilePathTextAsset_19(TextAsset_t2052027493 * value)
	{
		___m_guardianXmlFilePathTextAsset_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_guardianXmlFilePathTextAsset_19), value);
	}

	inline static int32_t get_offset_of_m_1npcXmlFilePathTextAsset_20() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_1npcXmlFilePathTextAsset_20)); }
	inline TextAsset_t2052027493 * get_m_1npcXmlFilePathTextAsset_20() const { return ___m_1npcXmlFilePathTextAsset_20; }
	inline TextAsset_t2052027493 ** get_address_of_m_1npcXmlFilePathTextAsset_20() { return &___m_1npcXmlFilePathTextAsset_20; }
	inline void set_m_1npcXmlFilePathTextAsset_20(TextAsset_t2052027493 * value)
	{
		___m_1npcXmlFilePathTextAsset_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_1npcXmlFilePathTextAsset_20), value);
	}

	inline static int32_t get_offset_of_m_travelerXmlFilePathTextAsset_21() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_travelerXmlFilePathTextAsset_21)); }
	inline TextAsset_t2052027493 * get_m_travelerXmlFilePathTextAsset_21() const { return ___m_travelerXmlFilePathTextAsset_21; }
	inline TextAsset_t2052027493 ** get_address_of_m_travelerXmlFilePathTextAsset_21() { return &___m_travelerXmlFilePathTextAsset_21; }
	inline void set_m_travelerXmlFilePathTextAsset_21(TextAsset_t2052027493 * value)
	{
		___m_travelerXmlFilePathTextAsset_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_travelerXmlFilePathTextAsset_21), value);
	}

	inline static int32_t get_offset_of_m_cassXmlFilePathTextAsset_22() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_cassXmlFilePathTextAsset_22)); }
	inline TextAsset_t2052027493 * get_m_cassXmlFilePathTextAsset_22() const { return ___m_cassXmlFilePathTextAsset_22; }
	inline TextAsset_t2052027493 ** get_address_of_m_cassXmlFilePathTextAsset_22() { return &___m_cassXmlFilePathTextAsset_22; }
	inline void set_m_cassXmlFilePathTextAsset_22(TextAsset_t2052027493 * value)
	{
		___m_cassXmlFilePathTextAsset_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_cassXmlFilePathTextAsset_22), value);
	}

	inline static int32_t get_offset_of_m_fireXmlFilePathTextAsset_23() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_fireXmlFilePathTextAsset_23)); }
	inline TextAsset_t2052027493 * get_m_fireXmlFilePathTextAsset_23() const { return ___m_fireXmlFilePathTextAsset_23; }
	inline TextAsset_t2052027493 ** get_address_of_m_fireXmlFilePathTextAsset_23() { return &___m_fireXmlFilePathTextAsset_23; }
	inline void set_m_fireXmlFilePathTextAsset_23(TextAsset_t2052027493 * value)
	{
		___m_fireXmlFilePathTextAsset_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_fireXmlFilePathTextAsset_23), value);
	}

	inline static int32_t get_offset_of_m_yetiXmlFilePathTextAsset_24() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_yetiXmlFilePathTextAsset_24)); }
	inline TextAsset_t2052027493 * get_m_yetiXmlFilePathTextAsset_24() const { return ___m_yetiXmlFilePathTextAsset_24; }
	inline TextAsset_t2052027493 ** get_address_of_m_yetiXmlFilePathTextAsset_24() { return &___m_yetiXmlFilePathTextAsset_24; }
	inline void set_m_yetiXmlFilePathTextAsset_24(TextAsset_t2052027493 * value)
	{
		___m_yetiXmlFilePathTextAsset_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_yetiXmlFilePathTextAsset_24), value);
	}

	inline static int32_t get_offset_of_m_fireperson2XmlFilePathTextAsset_25() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_fireperson2XmlFilePathTextAsset_25)); }
	inline TextAsset_t2052027493 * get_m_fireperson2XmlFilePathTextAsset_25() const { return ___m_fireperson2XmlFilePathTextAsset_25; }
	inline TextAsset_t2052027493 ** get_address_of_m_fireperson2XmlFilePathTextAsset_25() { return &___m_fireperson2XmlFilePathTextAsset_25; }
	inline void set_m_fireperson2XmlFilePathTextAsset_25(TextAsset_t2052027493 * value)
	{
		___m_fireperson2XmlFilePathTextAsset_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_fireperson2XmlFilePathTextAsset_25), value);
	}

	inline static int32_t get_offset_of_m_firespiritXmlFilePathTextAsset_26() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_firespiritXmlFilePathTextAsset_26)); }
	inline TextAsset_t2052027493 * get_m_firespiritXmlFilePathTextAsset_26() const { return ___m_firespiritXmlFilePathTextAsset_26; }
	inline TextAsset_t2052027493 ** get_address_of_m_firespiritXmlFilePathTextAsset_26() { return &___m_firespiritXmlFilePathTextAsset_26; }
	inline void set_m_firespiritXmlFilePathTextAsset_26(TextAsset_t2052027493 * value)
	{
		___m_firespiritXmlFilePathTextAsset_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_firespiritXmlFilePathTextAsset_26), value);
	}

	inline static int32_t get_offset_of_m_npcXmlFilePathTextAsset_27() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_npcXmlFilePathTextAsset_27)); }
	inline TextAsset_t2052027493 * get_m_npcXmlFilePathTextAsset_27() const { return ___m_npcXmlFilePathTextAsset_27; }
	inline TextAsset_t2052027493 ** get_address_of_m_npcXmlFilePathTextAsset_27() { return &___m_npcXmlFilePathTextAsset_27; }
	inline void set_m_npcXmlFilePathTextAsset_27(TextAsset_t2052027493 * value)
	{
		___m_npcXmlFilePathTextAsset_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_npcXmlFilePathTextAsset_27), value);
	}

	inline static int32_t get_offset_of_m_cass2XmlFilePathTextAsset_28() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_cass2XmlFilePathTextAsset_28)); }
	inline TextAsset_t2052027493 * get_m_cass2XmlFilePathTextAsset_28() const { return ___m_cass2XmlFilePathTextAsset_28; }
	inline TextAsset_t2052027493 ** get_address_of_m_cass2XmlFilePathTextAsset_28() { return &___m_cass2XmlFilePathTextAsset_28; }
	inline void set_m_cass2XmlFilePathTextAsset_28(TextAsset_t2052027493 * value)
	{
		___m_cass2XmlFilePathTextAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cass2XmlFilePathTextAsset_28), value);
	}

	inline static int32_t get_offset_of_m_npc2XmlFilePathTextAsset_29() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_npc2XmlFilePathTextAsset_29)); }
	inline TextAsset_t2052027493 * get_m_npc2XmlFilePathTextAsset_29() const { return ___m_npc2XmlFilePathTextAsset_29; }
	inline TextAsset_t2052027493 ** get_address_of_m_npc2XmlFilePathTextAsset_29() { return &___m_npc2XmlFilePathTextAsset_29; }
	inline void set_m_npc2XmlFilePathTextAsset_29(TextAsset_t2052027493 * value)
	{
		___m_npc2XmlFilePathTextAsset_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_npc2XmlFilePathTextAsset_29), value);
	}

	inline static int32_t get_offset_of_m_darroXmlFilePathTextAsset_30() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_darroXmlFilePathTextAsset_30)); }
	inline TextAsset_t2052027493 * get_m_darroXmlFilePathTextAsset_30() const { return ___m_darroXmlFilePathTextAsset_30; }
	inline TextAsset_t2052027493 ** get_address_of_m_darroXmlFilePathTextAsset_30() { return &___m_darroXmlFilePathTextAsset_30; }
	inline void set_m_darroXmlFilePathTextAsset_30(TextAsset_t2052027493 * value)
	{
		___m_darroXmlFilePathTextAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_darroXmlFilePathTextAsset_30), value);
	}

	inline static int32_t get_offset_of_m_gnatsXmlFilePathTextAsset_31() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_gnatsXmlFilePathTextAsset_31)); }
	inline TextAsset_t2052027493 * get_m_gnatsXmlFilePathTextAsset_31() const { return ___m_gnatsXmlFilePathTextAsset_31; }
	inline TextAsset_t2052027493 ** get_address_of_m_gnatsXmlFilePathTextAsset_31() { return &___m_gnatsXmlFilePathTextAsset_31; }
	inline void set_m_gnatsXmlFilePathTextAsset_31(TextAsset_t2052027493 * value)
	{
		___m_gnatsXmlFilePathTextAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_gnatsXmlFilePathTextAsset_31), value);
	}

	inline static int32_t get_offset_of_m_cass3XmlFilePathTextAsset_32() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_cass3XmlFilePathTextAsset_32)); }
	inline TextAsset_t2052027493 * get_m_cass3XmlFilePathTextAsset_32() const { return ___m_cass3XmlFilePathTextAsset_32; }
	inline TextAsset_t2052027493 ** get_address_of_m_cass3XmlFilePathTextAsset_32() { return &___m_cass3XmlFilePathTextAsset_32; }
	inline void set_m_cass3XmlFilePathTextAsset_32(TextAsset_t2052027493 * value)
	{
		___m_cass3XmlFilePathTextAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_cass3XmlFilePathTextAsset_32), value);
	}

	inline static int32_t get_offset_of_m_sparksXmlFilePathTextAsset_33() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_sparksXmlFilePathTextAsset_33)); }
	inline TextAsset_t2052027493 * get_m_sparksXmlFilePathTextAsset_33() const { return ___m_sparksXmlFilePathTextAsset_33; }
	inline TextAsset_t2052027493 ** get_address_of_m_sparksXmlFilePathTextAsset_33() { return &___m_sparksXmlFilePathTextAsset_33; }
	inline void set_m_sparksXmlFilePathTextAsset_33(TextAsset_t2052027493 * value)
	{
		___m_sparksXmlFilePathTextAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sparksXmlFilePathTextAsset_33), value);
	}

	inline static int32_t get_offset_of_m_swapguyXmlFilePathTextAsset_34() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_swapguyXmlFilePathTextAsset_34)); }
	inline TextAsset_t2052027493 * get_m_swapguyXmlFilePathTextAsset_34() const { return ___m_swapguyXmlFilePathTextAsset_34; }
	inline TextAsset_t2052027493 ** get_address_of_m_swapguyXmlFilePathTextAsset_34() { return &___m_swapguyXmlFilePathTextAsset_34; }
	inline void set_m_swapguyXmlFilePathTextAsset_34(TextAsset_t2052027493 * value)
	{
		___m_swapguyXmlFilePathTextAsset_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_swapguyXmlFilePathTextAsset_34), value);
	}

	inline static int32_t get_offset_of_m_ntsXmlFilePathTextAsset_35() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_ntsXmlFilePathTextAsset_35)); }
	inline TextAsset_t2052027493 * get_m_ntsXmlFilePathTextAsset_35() const { return ___m_ntsXmlFilePathTextAsset_35; }
	inline TextAsset_t2052027493 ** get_address_of_m_ntsXmlFilePathTextAsset_35() { return &___m_ntsXmlFilePathTextAsset_35; }
	inline void set_m_ntsXmlFilePathTextAsset_35(TextAsset_t2052027493 * value)
	{
		___m_ntsXmlFilePathTextAsset_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_ntsXmlFilePathTextAsset_35), value);
	}

	inline static int32_t get_offset_of_m_bridgeWomanXmlFilePathTextAsset_36() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bridgeWomanXmlFilePathTextAsset_36)); }
	inline TextAsset_t2052027493 * get_m_bridgeWomanXmlFilePathTextAsset_36() const { return ___m_bridgeWomanXmlFilePathTextAsset_36; }
	inline TextAsset_t2052027493 ** get_address_of_m_bridgeWomanXmlFilePathTextAsset_36() { return &___m_bridgeWomanXmlFilePathTextAsset_36; }
	inline void set_m_bridgeWomanXmlFilePathTextAsset_36(TextAsset_t2052027493 * value)
	{
		___m_bridgeWomanXmlFilePathTextAsset_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_bridgeWomanXmlFilePathTextAsset_36), value);
	}

	inline static int32_t get_offset_of_m_templeGuidianXmlFilePathTextAsset_37() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_templeGuidianXmlFilePathTextAsset_37)); }
	inline TextAsset_t2052027493 * get_m_templeGuidianXmlFilePathTextAsset_37() const { return ___m_templeGuidianXmlFilePathTextAsset_37; }
	inline TextAsset_t2052027493 ** get_address_of_m_templeGuidianXmlFilePathTextAsset_37() { return &___m_templeGuidianXmlFilePathTextAsset_37; }
	inline void set_m_templeGuidianXmlFilePathTextAsset_37(TextAsset_t2052027493 * value)
	{
		___m_templeGuidianXmlFilePathTextAsset_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_templeGuidianXmlFilePathTextAsset_37), value);
	}

	inline static int32_t get_offset_of_m_cass4XmlFilePathTextAsset_38() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_cass4XmlFilePathTextAsset_38)); }
	inline TextAsset_t2052027493 * get_m_cass4XmlFilePathTextAsset_38() const { return ___m_cass4XmlFilePathTextAsset_38; }
	inline TextAsset_t2052027493 ** get_address_of_m_cass4XmlFilePathTextAsset_38() { return &___m_cass4XmlFilePathTextAsset_38; }
	inline void set_m_cass4XmlFilePathTextAsset_38(TextAsset_t2052027493 * value)
	{
		___m_cass4XmlFilePathTextAsset_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_cass4XmlFilePathTextAsset_38), value);
	}

	inline static int32_t get_offset_of_m_examinerXmlFilePathTextAsset_39() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_examinerXmlFilePathTextAsset_39)); }
	inline TextAsset_t2052027493 * get_m_examinerXmlFilePathTextAsset_39() const { return ___m_examinerXmlFilePathTextAsset_39; }
	inline TextAsset_t2052027493 ** get_address_of_m_examinerXmlFilePathTextAsset_39() { return &___m_examinerXmlFilePathTextAsset_39; }
	inline void set_m_examinerXmlFilePathTextAsset_39(TextAsset_t2052027493 * value)
	{
		___m_examinerXmlFilePathTextAsset_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_examinerXmlFilePathTextAsset_39), value);
	}

	inline static int32_t get_offset_of_m_swapGuyXmlFilePathTextAsset_40() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_swapGuyXmlFilePathTextAsset_40)); }
	inline TextAsset_t2052027493 * get_m_swapGuyXmlFilePathTextAsset_40() const { return ___m_swapGuyXmlFilePathTextAsset_40; }
	inline TextAsset_t2052027493 ** get_address_of_m_swapGuyXmlFilePathTextAsset_40() { return &___m_swapGuyXmlFilePathTextAsset_40; }
	inline void set_m_swapGuyXmlFilePathTextAsset_40(TextAsset_t2052027493 * value)
	{
		___m_swapGuyXmlFilePathTextAsset_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_swapGuyXmlFilePathTextAsset_40), value);
	}

	inline static int32_t get_offset_of_m_playerNeedsHelpTextAsset_41() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_playerNeedsHelpTextAsset_41)); }
	inline TextAsset_t2052027493 * get_m_playerNeedsHelpTextAsset_41() const { return ___m_playerNeedsHelpTextAsset_41; }
	inline TextAsset_t2052027493 ** get_address_of_m_playerNeedsHelpTextAsset_41() { return &___m_playerNeedsHelpTextAsset_41; }
	inline void set_m_playerNeedsHelpTextAsset_41(TextAsset_t2052027493 * value)
	{
		___m_playerNeedsHelpTextAsset_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_playerNeedsHelpTextAsset_41), value);
	}

	inline static int32_t get_offset_of_m_cassL7XmlFilePathTextAsset_42() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_cassL7XmlFilePathTextAsset_42)); }
	inline TextAsset_t2052027493 * get_m_cassL7XmlFilePathTextAsset_42() const { return ___m_cassL7XmlFilePathTextAsset_42; }
	inline TextAsset_t2052027493 ** get_address_of_m_cassL7XmlFilePathTextAsset_42() { return &___m_cassL7XmlFilePathTextAsset_42; }
	inline void set_m_cassL7XmlFilePathTextAsset_42(TextAsset_t2052027493 * value)
	{
		___m_cassL7XmlFilePathTextAsset_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_cassL7XmlFilePathTextAsset_42), value);
	}

	inline static int32_t get_offset_of_m_darroL7XmlFilePathTextAsset_43() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_darroL7XmlFilePathTextAsset_43)); }
	inline TextAsset_t2052027493 * get_m_darroL7XmlFilePathTextAsset_43() const { return ___m_darroL7XmlFilePathTextAsset_43; }
	inline TextAsset_t2052027493 ** get_address_of_m_darroL7XmlFilePathTextAsset_43() { return &___m_darroL7XmlFilePathTextAsset_43; }
	inline void set_m_darroL7XmlFilePathTextAsset_43(TextAsset_t2052027493 * value)
	{
		___m_darroL7XmlFilePathTextAsset_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_darroL7XmlFilePathTextAsset_43), value);
	}

	inline static int32_t get_offset_of_m_firepersonL7XmlFilePathTextAsset_44() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_firepersonL7XmlFilePathTextAsset_44)); }
	inline TextAsset_t2052027493 * get_m_firepersonL7XmlFilePathTextAsset_44() const { return ___m_firepersonL7XmlFilePathTextAsset_44; }
	inline TextAsset_t2052027493 ** get_address_of_m_firepersonL7XmlFilePathTextAsset_44() { return &___m_firepersonL7XmlFilePathTextAsset_44; }
	inline void set_m_firepersonL7XmlFilePathTextAsset_44(TextAsset_t2052027493 * value)
	{
		___m_firepersonL7XmlFilePathTextAsset_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_firepersonL7XmlFilePathTextAsset_44), value);
	}

	inline static int32_t get_offset_of_m_kogXmlFilePathTextAsset_45() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_kogXmlFilePathTextAsset_45)); }
	inline TextAsset_t2052027493 * get_m_kogXmlFilePathTextAsset_45() const { return ___m_kogXmlFilePathTextAsset_45; }
	inline TextAsset_t2052027493 ** get_address_of_m_kogXmlFilePathTextAsset_45() { return &___m_kogXmlFilePathTextAsset_45; }
	inline void set_m_kogXmlFilePathTextAsset_45(TextAsset_t2052027493 * value)
	{
		___m_kogXmlFilePathTextAsset_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_kogXmlFilePathTextAsset_45), value);
	}

	inline static int32_t get_offset_of_m_iPerPHQAQuestionScore_46() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_iPerPHQAQuestionScore_46)); }
	inline int32_t get_m_iPerPHQAQuestionScore_46() const { return ___m_iPerPHQAQuestionScore_46; }
	inline int32_t* get_address_of_m_iPerPHQAQuestionScore_46() { return &___m_iPerPHQAQuestionScore_46; }
	inline void set_m_iPerPHQAQuestionScore_46(int32_t value)
	{
		___m_iPerPHQAQuestionScore_46 = value;
	}

	inline static int32_t get_offset_of_m_iPHQAQuestionNum_47() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_iPHQAQuestionNum_47)); }
	inline int32_t get_m_iPHQAQuestionNum_47() const { return ___m_iPHQAQuestionNum_47; }
	inline int32_t* get_address_of_m_iPHQAQuestionNum_47() { return &___m_iPHQAQuestionNum_47; }
	inline void set_m_iPHQAQuestionNum_47(int32_t value)
	{
		___m_iPHQAQuestionNum_47 = value;
	}

	inline static int32_t get_offset_of_m_iQ9Score_48() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_iQ9Score_48)); }
	inline int32_t get_m_iQ9Score_48() const { return ___m_iQ9Score_48; }
	inline int32_t* get_address_of_m_iQ9Score_48() { return &___m_iQ9Score_48; }
	inline void set_m_iQ9Score_48(int32_t value)
	{
		___m_iQ9Score_48 = value;
	}

	inline static int32_t get_offset_of_m_skin_49() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_skin_49)); }
	inline GUISkin_t2122630221 * get_m_skin_49() const { return ___m_skin_49; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_49() { return &___m_skin_49; }
	inline void set_m_skin_49(GUISkin_t2122630221 * value)
	{
		___m_skin_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_49), value);
	}

	inline static int32_t get_offset_of_m_loadSceneData_50() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_loadSceneData_50)); }
	inline LoadTalkSceneXMLData_t2381108950 * get_m_loadSceneData_50() const { return ___m_loadSceneData_50; }
	inline LoadTalkSceneXMLData_t2381108950 ** get_address_of_m_loadSceneData_50() { return &___m_loadSceneData_50; }
	inline void set_m_loadSceneData_50(LoadTalkSceneXMLData_t2381108950 * value)
	{
		___m_loadSceneData_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_loadSceneData_50), value);
	}

	inline static int32_t get_offset_of_L1AudioFileList_51() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L1AudioFileList_51)); }
	inline AudioClipU5BU5D_t3624204584* get_L1AudioFileList_51() const { return ___L1AudioFileList_51; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L1AudioFileList_51() { return &___L1AudioFileList_51; }
	inline void set_L1AudioFileList_51(AudioClipU5BU5D_t3624204584* value)
	{
		___L1AudioFileList_51 = value;
		Il2CppCodeGenWriteBarrier((&___L1AudioFileList_51), value);
	}

	inline static int32_t get_offset_of_L2AudioFileList_52() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L2AudioFileList_52)); }
	inline AudioClipU5BU5D_t3624204584* get_L2AudioFileList_52() const { return ___L2AudioFileList_52; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L2AudioFileList_52() { return &___L2AudioFileList_52; }
	inline void set_L2AudioFileList_52(AudioClipU5BU5D_t3624204584* value)
	{
		___L2AudioFileList_52 = value;
		Il2CppCodeGenWriteBarrier((&___L2AudioFileList_52), value);
	}

	inline static int32_t get_offset_of_L3AudioFileList_53() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L3AudioFileList_53)); }
	inline AudioClipU5BU5D_t3624204584* get_L3AudioFileList_53() const { return ___L3AudioFileList_53; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L3AudioFileList_53() { return &___L3AudioFileList_53; }
	inline void set_L3AudioFileList_53(AudioClipU5BU5D_t3624204584* value)
	{
		___L3AudioFileList_53 = value;
		Il2CppCodeGenWriteBarrier((&___L3AudioFileList_53), value);
	}

	inline static int32_t get_offset_of_L4AudioFileList_54() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L4AudioFileList_54)); }
	inline AudioClipU5BU5D_t3624204584* get_L4AudioFileList_54() const { return ___L4AudioFileList_54; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L4AudioFileList_54() { return &___L4AudioFileList_54; }
	inline void set_L4AudioFileList_54(AudioClipU5BU5D_t3624204584* value)
	{
		___L4AudioFileList_54 = value;
		Il2CppCodeGenWriteBarrier((&___L4AudioFileList_54), value);
	}

	inline static int32_t get_offset_of_L5AudioFileList_55() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L5AudioFileList_55)); }
	inline AudioClipU5BU5D_t3624204584* get_L5AudioFileList_55() const { return ___L5AudioFileList_55; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L5AudioFileList_55() { return &___L5AudioFileList_55; }
	inline void set_L5AudioFileList_55(AudioClipU5BU5D_t3624204584* value)
	{
		___L5AudioFileList_55 = value;
		Il2CppCodeGenWriteBarrier((&___L5AudioFileList_55), value);
	}

	inline static int32_t get_offset_of_L6AudioFileList_56() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L6AudioFileList_56)); }
	inline AudioClipU5BU5D_t3624204584* get_L6AudioFileList_56() const { return ___L6AudioFileList_56; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L6AudioFileList_56() { return &___L6AudioFileList_56; }
	inline void set_L6AudioFileList_56(AudioClipU5BU5D_t3624204584* value)
	{
		___L6AudioFileList_56 = value;
		Il2CppCodeGenWriteBarrier((&___L6AudioFileList_56), value);
	}

	inline static int32_t get_offset_of_L7AudioFileList_57() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___L7AudioFileList_57)); }
	inline AudioClipU5BU5D_t3624204584* get_L7AudioFileList_57() const { return ___L7AudioFileList_57; }
	inline AudioClipU5BU5D_t3624204584** get_address_of_L7AudioFileList_57() { return &___L7AudioFileList_57; }
	inline void set_L7AudioFileList_57(AudioClipU5BU5D_t3624204584* value)
	{
		___L7AudioFileList_57 = value;
		Il2CppCodeGenWriteBarrier((&___L7AudioFileList_57), value);
	}

	inline static int32_t get_offset_of_spatialBlend_58() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___spatialBlend_58)); }
	inline float get_spatialBlend_58() const { return ___spatialBlend_58; }
	inline float* get_address_of_spatialBlend_58() { return &___spatialBlend_58; }
	inline void set_spatialBlend_58(float value)
	{
		___spatialBlend_58 = value;
	}

	inline static int32_t get_offset_of_m_characterVoiceAS_59() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_characterVoiceAS_59)); }
	inline AudioSource_t4025721661 * get_m_characterVoiceAS_59() const { return ___m_characterVoiceAS_59; }
	inline AudioSource_t4025721661 ** get_address_of_m_characterVoiceAS_59() { return &___m_characterVoiceAS_59; }
	inline void set_m_characterVoiceAS_59(AudioSource_t4025721661 * value)
	{
		___m_characterVoiceAS_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterVoiceAS_59), value);
	}

	inline static int32_t get_offset_of_m_bAutoConversationUnFinished_60() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bAutoConversationUnFinished_60)); }
	inline bool get_m_bAutoConversationUnFinished_60() const { return ___m_bAutoConversationUnFinished_60; }
	inline bool* get_address_of_m_bAutoConversationUnFinished_60() { return &___m_bAutoConversationUnFinished_60; }
	inline void set_m_bAutoConversationUnFinished_60(bool value)
	{
		___m_bAutoConversationUnFinished_60 = value;
	}

	inline static int32_t get_offset_of_m_bNPC1AnimationPlayOnce_61() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bNPC1AnimationPlayOnce_61)); }
	inline bool get_m_bNPC1AnimationPlayOnce_61() const { return ___m_bNPC1AnimationPlayOnce_61; }
	inline bool* get_address_of_m_bNPC1AnimationPlayOnce_61() { return &___m_bNPC1AnimationPlayOnce_61; }
	inline void set_m_bNPC1AnimationPlayOnce_61(bool value)
	{
		___m_bNPC1AnimationPlayOnce_61 = value;
	}

	inline static int32_t get_offset_of_m_bNPC2AnimationPlayOnce_62() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bNPC2AnimationPlayOnce_62)); }
	inline bool get_m_bNPC2AnimationPlayOnce_62() const { return ___m_bNPC2AnimationPlayOnce_62; }
	inline bool* get_address_of_m_bNPC2AnimationPlayOnce_62() { return &___m_bNPC2AnimationPlayOnce_62; }
	inline void set_m_bNPC2AnimationPlayOnce_62(bool value)
	{
		___m_bNPC2AnimationPlayOnce_62 = value;
	}

	inline static int32_t get_offset_of_m_bMultiChoice1OptionA_63() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bMultiChoice1OptionA_63)); }
	inline bool get_m_bMultiChoice1OptionA_63() const { return ___m_bMultiChoice1OptionA_63; }
	inline bool* get_address_of_m_bMultiChoice1OptionA_63() { return &___m_bMultiChoice1OptionA_63; }
	inline void set_m_bMultiChoice1OptionA_63(bool value)
	{
		___m_bMultiChoice1OptionA_63 = value;
	}

	inline static int32_t get_offset_of_m_bMultiChoice1OptionB_64() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bMultiChoice1OptionB_64)); }
	inline bool get_m_bMultiChoice1OptionB_64() const { return ___m_bMultiChoice1OptionB_64; }
	inline bool* get_address_of_m_bMultiChoice1OptionB_64() { return &___m_bMultiChoice1OptionB_64; }
	inline void set_m_bMultiChoice1OptionB_64(bool value)
	{
		___m_bMultiChoice1OptionB_64 = value;
	}

	inline static int32_t get_offset_of_m_bMultiChoice1OptionC_65() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bMultiChoice1OptionC_65)); }
	inline bool get_m_bMultiChoice1OptionC_65() const { return ___m_bMultiChoice1OptionC_65; }
	inline bool* get_address_of_m_bMultiChoice1OptionC_65() { return &___m_bMultiChoice1OptionC_65; }
	inline void set_m_bMultiChoice1OptionC_65(bool value)
	{
		___m_bMultiChoice1OptionC_65 = value;
	}

	inline static int32_t get_offset_of_m_bMultiChoice1OptionD_66() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_bMultiChoice1OptionD_66)); }
	inline bool get_m_bMultiChoice1OptionD_66() const { return ___m_bMultiChoice1OptionD_66; }
	inline bool* get_address_of_m_bMultiChoice1OptionD_66() { return &___m_bMultiChoice1OptionD_66; }
	inline void set_m_bMultiChoice1OptionD_66(bool value)
	{
		___m_bMultiChoice1OptionD_66 = value;
	}

	inline static int32_t get_offset_of_m_PHQAAnswerIndex_67() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_PHQAAnswerIndex_67)); }
	inline int32_t get_m_PHQAAnswerIndex_67() const { return ___m_PHQAAnswerIndex_67; }
	inline int32_t* get_address_of_m_PHQAAnswerIndex_67() { return &___m_PHQAAnswerIndex_67; }
	inline void set_m_PHQAAnswerIndex_67(int32_t value)
	{
		___m_PHQAAnswerIndex_67 = value;
	}

	inline static int32_t get_offset_of_instance_68() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___instance_68)); }
	inline CapturedDataInput_t2616152122 * get_instance_68() const { return ___instance_68; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_68() { return &___instance_68; }
	inline void set_instance_68(CapturedDataInput_t2616152122 * value)
	{
		___instance_68 = value;
		Il2CppCodeGenWriteBarrier((&___instance_68), value);
	}

	inline static int32_t get_offset_of_m_sTestWholeDynamic_69() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_sTestWholeDynamic_69)); }
	inline String_t* get_m_sTestWholeDynamic_69() const { return ___m_sTestWholeDynamic_69; }
	inline String_t** get_address_of_m_sTestWholeDynamic_69() { return &___m_sTestWholeDynamic_69; }
	inline void set_m_sTestWholeDynamic_69(String_t* value)
	{
		___m_sTestWholeDynamic_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTestWholeDynamic_69), value);
	}

	inline static int32_t get_offset_of_m_sTestDynamicToken_70() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_sTestDynamicToken_70)); }
	inline String_t* get_m_sTestDynamicToken_70() const { return ___m_sTestDynamicToken_70; }
	inline String_t** get_address_of_m_sTestDynamicToken_70() { return &___m_sTestDynamicToken_70; }
	inline void set_m_sTestDynamicToken_70(String_t* value)
	{
		___m_sTestDynamicToken_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTestDynamicToken_70), value);
	}

	inline static int32_t get_offset_of_m_sTestDynamicURL_71() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_sTestDynamicURL_71)); }
	inline String_t* get_m_sTestDynamicURL_71() const { return ___m_sTestDynamicURL_71; }
	inline String_t** get_address_of_m_sTestDynamicURL_71() { return &___m_sTestDynamicURL_71; }
	inline void set_m_sTestDynamicURL_71(String_t* value)
	{
		___m_sTestDynamicURL_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_sTestDynamicURL_71), value);
	}

	inline static int32_t get_offset_of_m_BoolSkipVoiceErrorLevel2_72() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795, ___m_BoolSkipVoiceErrorLevel2_72)); }
	inline bool get_m_BoolSkipVoiceErrorLevel2_72() const { return ___m_BoolSkipVoiceErrorLevel2_72; }
	inline bool* get_address_of_m_BoolSkipVoiceErrorLevel2_72() { return &___m_BoolSkipVoiceErrorLevel2_72; }
	inline void set_m_BoolSkipVoiceErrorLevel2_72(bool value)
	{
		___m_BoolSkipVoiceErrorLevel2_72 = value;
	}
};

struct TalkScenes_t1215051795_StaticFields
{
public:
	// TalkScenes TalkScenes::instanceRef
	TalkScenes_t1215051795 * ___instanceRef_2;

public:
	inline static int32_t get_offset_of_instanceRef_2() { return static_cast<int32_t>(offsetof(TalkScenes_t1215051795_StaticFields, ___instanceRef_2)); }
	inline TalkScenes_t1215051795 * get_instanceRef_2() const { return ___instanceRef_2; }
	inline TalkScenes_t1215051795 ** get_address_of_instanceRef_2() { return &___instanceRef_2; }
	inline void set_instanceRef_2(TalkScenes_t1215051795 * value)
	{
		___instanceRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___instanceRef_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TALKSCENES_T1215051795_H
#ifndef HOPETALK_T2140284693_H
#define HOPETALK_T2140284693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HopeTalk
struct  HopeTalk_t2140284693  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Vector3 HopeTalk::m_vStartPosition
	Vector3_t1986933152  ___m_vStartPosition_2;
	// UnityEngine.Vector3 HopeTalk::m_vTargetPosition
	Vector3_t1986933152  ___m_vTargetPosition_3;
	// System.Boolean HopeTalk::m_bMovingToTarget
	bool ___m_bMovingToTarget_4;
	// System.Single HopeTalk::m_fTuiFlyDistance
	float ___m_fTuiFlyDistance_5;
	// System.Single HopeTalk::m_fTuiFlySpeed
	float ___m_fTuiFlySpeed_6;
	// System.Single HopeTalk::time
	float ___time_7;
	// System.Boolean HopeTalk::m_bPreventTuiTalkAhead
	bool ___m_bPreventTuiTalkAhead_8;

public:
	inline static int32_t get_offset_of_m_vStartPosition_2() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_vStartPosition_2)); }
	inline Vector3_t1986933152  get_m_vStartPosition_2() const { return ___m_vStartPosition_2; }
	inline Vector3_t1986933152 * get_address_of_m_vStartPosition_2() { return &___m_vStartPosition_2; }
	inline void set_m_vStartPosition_2(Vector3_t1986933152  value)
	{
		___m_vStartPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_vTargetPosition_3() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_vTargetPosition_3)); }
	inline Vector3_t1986933152  get_m_vTargetPosition_3() const { return ___m_vTargetPosition_3; }
	inline Vector3_t1986933152 * get_address_of_m_vTargetPosition_3() { return &___m_vTargetPosition_3; }
	inline void set_m_vTargetPosition_3(Vector3_t1986933152  value)
	{
		___m_vTargetPosition_3 = value;
	}

	inline static int32_t get_offset_of_m_bMovingToTarget_4() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_bMovingToTarget_4)); }
	inline bool get_m_bMovingToTarget_4() const { return ___m_bMovingToTarget_4; }
	inline bool* get_address_of_m_bMovingToTarget_4() { return &___m_bMovingToTarget_4; }
	inline void set_m_bMovingToTarget_4(bool value)
	{
		___m_bMovingToTarget_4 = value;
	}

	inline static int32_t get_offset_of_m_fTuiFlyDistance_5() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_fTuiFlyDistance_5)); }
	inline float get_m_fTuiFlyDistance_5() const { return ___m_fTuiFlyDistance_5; }
	inline float* get_address_of_m_fTuiFlyDistance_5() { return &___m_fTuiFlyDistance_5; }
	inline void set_m_fTuiFlyDistance_5(float value)
	{
		___m_fTuiFlyDistance_5 = value;
	}

	inline static int32_t get_offset_of_m_fTuiFlySpeed_6() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_fTuiFlySpeed_6)); }
	inline float get_m_fTuiFlySpeed_6() const { return ___m_fTuiFlySpeed_6; }
	inline float* get_address_of_m_fTuiFlySpeed_6() { return &___m_fTuiFlySpeed_6; }
	inline void set_m_fTuiFlySpeed_6(float value)
	{
		___m_fTuiFlySpeed_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_m_bPreventTuiTalkAhead_8() { return static_cast<int32_t>(offsetof(HopeTalk_t2140284693, ___m_bPreventTuiTalkAhead_8)); }
	inline bool get_m_bPreventTuiTalkAhead_8() const { return ___m_bPreventTuiTalkAhead_8; }
	inline bool* get_address_of_m_bPreventTuiTalkAhead_8() { return &___m_bPreventTuiTalkAhead_8; }
	inline void set_m_bPreventTuiTalkAhead_8(bool value)
	{
		___m_bPreventTuiTalkAhead_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOPETALK_T2140284693_H
#ifndef SHIELDAGADEPRESSION_T775775458_H
#define SHIELDAGADEPRESSION_T775775458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldAgaDepression
struct  ShieldAgaDepression_t775775458  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin ShieldAgaDepression::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject ShieldAgaDepression::m_shieldPrf
	GameObject_t2557347079 * ___m_shieldPrf_3;
	// UnityEngine.Vector3 ShieldAgaDepression::m_vShieldPosition
	Vector3_t1986933152  ___m_vShieldPosition_4;
	// UnityEngine.GameObject ShieldAgaDepression::m_shieldObjectClone
	GameObject_t2557347079 * ___m_shieldObjectClone_5;
	// System.String[] ShieldAgaDepression::childObjectNames
	StringU5BU5D_t2511808107* ___childObjectNames_8;
	// System.String[] ShieldAgaDepression::shieldNames
	StringU5BU5D_t2511808107* ___shieldNames_9;
	// UnityEngine.Rect[] ShieldAgaDepression::shieldNameRects
	RectU5BU5D_t141872167* ___shieldNameRects_10;
	// System.Boolean[] ShieldAgaDepression::displayName
	BooleanU5BU5D_t698278498* ___displayName_11;
	// UnityEngine.Color[] ShieldAgaDepression::displayColor
	ColorU5BU5D_t247935167* ___displayColor_12;
	// UnityEngine.Texture ShieldAgaDepression::colourTexture
	Texture_t2119925672 * ___colourTexture_13;
	// UnityEngine.Texture ShieldAgaDepression::greyScaleTexture
	Texture_t2119925672 * ___greyScaleTexture_14;
	// ShieldMenu ShieldAgaDepression::shieldMen
	ShieldMenu_t3436809847 * ___shieldMen_15;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_shieldPrf_3() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___m_shieldPrf_3)); }
	inline GameObject_t2557347079 * get_m_shieldPrf_3() const { return ___m_shieldPrf_3; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldPrf_3() { return &___m_shieldPrf_3; }
	inline void set_m_shieldPrf_3(GameObject_t2557347079 * value)
	{
		___m_shieldPrf_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldPrf_3), value);
	}

	inline static int32_t get_offset_of_m_vShieldPosition_4() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___m_vShieldPosition_4)); }
	inline Vector3_t1986933152  get_m_vShieldPosition_4() const { return ___m_vShieldPosition_4; }
	inline Vector3_t1986933152 * get_address_of_m_vShieldPosition_4() { return &___m_vShieldPosition_4; }
	inline void set_m_vShieldPosition_4(Vector3_t1986933152  value)
	{
		___m_vShieldPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_shieldObjectClone_5() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___m_shieldObjectClone_5)); }
	inline GameObject_t2557347079 * get_m_shieldObjectClone_5() const { return ___m_shieldObjectClone_5; }
	inline GameObject_t2557347079 ** get_address_of_m_shieldObjectClone_5() { return &___m_shieldObjectClone_5; }
	inline void set_m_shieldObjectClone_5(GameObject_t2557347079 * value)
	{
		___m_shieldObjectClone_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_shieldObjectClone_5), value);
	}

	inline static int32_t get_offset_of_childObjectNames_8() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___childObjectNames_8)); }
	inline StringU5BU5D_t2511808107* get_childObjectNames_8() const { return ___childObjectNames_8; }
	inline StringU5BU5D_t2511808107** get_address_of_childObjectNames_8() { return &___childObjectNames_8; }
	inline void set_childObjectNames_8(StringU5BU5D_t2511808107* value)
	{
		___childObjectNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___childObjectNames_8), value);
	}

	inline static int32_t get_offset_of_shieldNames_9() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___shieldNames_9)); }
	inline StringU5BU5D_t2511808107* get_shieldNames_9() const { return ___shieldNames_9; }
	inline StringU5BU5D_t2511808107** get_address_of_shieldNames_9() { return &___shieldNames_9; }
	inline void set_shieldNames_9(StringU5BU5D_t2511808107* value)
	{
		___shieldNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNames_9), value);
	}

	inline static int32_t get_offset_of_shieldNameRects_10() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___shieldNameRects_10)); }
	inline RectU5BU5D_t141872167* get_shieldNameRects_10() const { return ___shieldNameRects_10; }
	inline RectU5BU5D_t141872167** get_address_of_shieldNameRects_10() { return &___shieldNameRects_10; }
	inline void set_shieldNameRects_10(RectU5BU5D_t141872167* value)
	{
		___shieldNameRects_10 = value;
		Il2CppCodeGenWriteBarrier((&___shieldNameRects_10), value);
	}

	inline static int32_t get_offset_of_displayName_11() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___displayName_11)); }
	inline BooleanU5BU5D_t698278498* get_displayName_11() const { return ___displayName_11; }
	inline BooleanU5BU5D_t698278498** get_address_of_displayName_11() { return &___displayName_11; }
	inline void set_displayName_11(BooleanU5BU5D_t698278498* value)
	{
		___displayName_11 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_11), value);
	}

	inline static int32_t get_offset_of_displayColor_12() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___displayColor_12)); }
	inline ColorU5BU5D_t247935167* get_displayColor_12() const { return ___displayColor_12; }
	inline ColorU5BU5D_t247935167** get_address_of_displayColor_12() { return &___displayColor_12; }
	inline void set_displayColor_12(ColorU5BU5D_t247935167* value)
	{
		___displayColor_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayColor_12), value);
	}

	inline static int32_t get_offset_of_colourTexture_13() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___colourTexture_13)); }
	inline Texture_t2119925672 * get_colourTexture_13() const { return ___colourTexture_13; }
	inline Texture_t2119925672 ** get_address_of_colourTexture_13() { return &___colourTexture_13; }
	inline void set_colourTexture_13(Texture_t2119925672 * value)
	{
		___colourTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___colourTexture_13), value);
	}

	inline static int32_t get_offset_of_greyScaleTexture_14() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___greyScaleTexture_14)); }
	inline Texture_t2119925672 * get_greyScaleTexture_14() const { return ___greyScaleTexture_14; }
	inline Texture_t2119925672 ** get_address_of_greyScaleTexture_14() { return &___greyScaleTexture_14; }
	inline void set_greyScaleTexture_14(Texture_t2119925672 * value)
	{
		___greyScaleTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___greyScaleTexture_14), value);
	}

	inline static int32_t get_offset_of_shieldMen_15() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458, ___shieldMen_15)); }
	inline ShieldMenu_t3436809847 * get_shieldMen_15() const { return ___shieldMen_15; }
	inline ShieldMenu_t3436809847 ** get_address_of_shieldMen_15() { return &___shieldMen_15; }
	inline void set_shieldMen_15(ShieldMenu_t3436809847 * value)
	{
		___shieldMen_15 = value;
		Il2CppCodeGenWriteBarrier((&___shieldMen_15), value);
	}
};

struct ShieldAgaDepression_t775775458_StaticFields
{
public:
	// System.Single ShieldAgaDepression::TextWidth
	float ___TextWidth_6;
	// System.Single ShieldAgaDepression::TextHeight
	float ___TextHeight_7;

public:
	inline static int32_t get_offset_of_TextWidth_6() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458_StaticFields, ___TextWidth_6)); }
	inline float get_TextWidth_6() const { return ___TextWidth_6; }
	inline float* get_address_of_TextWidth_6() { return &___TextWidth_6; }
	inline void set_TextWidth_6(float value)
	{
		___TextWidth_6 = value;
	}

	inline static int32_t get_offset_of_TextHeight_7() { return static_cast<int32_t>(offsetof(ShieldAgaDepression_t775775458_StaticFields, ___TextHeight_7)); }
	inline float get_TextHeight_7() const { return ___TextHeight_7; }
	inline float* get_address_of_TextHeight_7() { return &___TextHeight_7; }
	inline void set_TextHeight_7(float value)
	{
		___TextHeight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELDAGADEPRESSION_T775775458_H
#ifndef SETTINGS_T284352370_H
#define SETTINGS_T284352370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Settings
struct  Settings_t284352370  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean Settings::m_bShow
	bool ___m_bShow_2;
	// UnityEngine.GUISkin Settings::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.Texture2D Settings::m_tBag
	Texture2D_t3063074017 * ___m_tBag_4;
	// UnityEngine.Texture2D[] Settings::m_tItems
	Texture2DU5BU5D_t3304433276* ___m_tItems_5;
	// System.Single Settings::m_fMoveSpeed
	float ___m_fMoveSpeed_6;
	// System.Single Settings::m_fTimer
	float ___m_fTimer_9;
	// UnityEngine.Rect Settings::m_rBag
	Rect_t3039462994  ___m_rBag_10;
	// UnityEngine.Rect[] Settings::m_rItemRects
	RectU5BU5D_t141872167* ___m_rItemRects_11;
	// UnityEngine.Vector2[] Settings::m_vFinalPos
	Vector2U5BU5D_t1220531434* ___m_vFinalPos_12;
	// System.Boolean Settings::m_bToggleShowItems
	bool ___m_bToggleShowItems_13;
	// System.Boolean Settings::m_bMove
	bool ___m_bMove_14;
	// System.Boolean Settings::m_bNotebookUp
	bool ___m_bNotebookUp_15;
	// UnityEngine.GameObject Settings::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_16;
	// UnityEngine.Color Settings::bridgeColor
	Color_t2582018970  ___bridgeColor_17;
	// UnityEngine.Color Settings::bridgeBackColor
	Color_t2582018970  ___bridgeBackColor_18;
	// CapturedDataInput Settings::instance
	CapturedDataInput_t2616152122 * ___instance_19;

public:
	inline static int32_t get_offset_of_m_bShow_2() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_bShow_2)); }
	inline bool get_m_bShow_2() const { return ___m_bShow_2; }
	inline bool* get_address_of_m_bShow_2() { return &___m_bShow_2; }
	inline void set_m_bShow_2(bool value)
	{
		___m_bShow_2 = value;
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_tBag_4() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_tBag_4)); }
	inline Texture2D_t3063074017 * get_m_tBag_4() const { return ___m_tBag_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBag_4() { return &___m_tBag_4; }
	inline void set_m_tBag_4(Texture2D_t3063074017 * value)
	{
		___m_tBag_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBag_4), value);
	}

	inline static int32_t get_offset_of_m_tItems_5() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_tItems_5)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tItems_5() const { return ___m_tItems_5; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tItems_5() { return &___m_tItems_5; }
	inline void set_m_tItems_5(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tItems_5), value);
	}

	inline static int32_t get_offset_of_m_fMoveSpeed_6() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_fMoveSpeed_6)); }
	inline float get_m_fMoveSpeed_6() const { return ___m_fMoveSpeed_6; }
	inline float* get_address_of_m_fMoveSpeed_6() { return &___m_fMoveSpeed_6; }
	inline void set_m_fMoveSpeed_6(float value)
	{
		___m_fMoveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_fTimer_9() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_fTimer_9)); }
	inline float get_m_fTimer_9() const { return ___m_fTimer_9; }
	inline float* get_address_of_m_fTimer_9() { return &___m_fTimer_9; }
	inline void set_m_fTimer_9(float value)
	{
		___m_fTimer_9 = value;
	}

	inline static int32_t get_offset_of_m_rBag_10() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_rBag_10)); }
	inline Rect_t3039462994  get_m_rBag_10() const { return ___m_rBag_10; }
	inline Rect_t3039462994 * get_address_of_m_rBag_10() { return &___m_rBag_10; }
	inline void set_m_rBag_10(Rect_t3039462994  value)
	{
		___m_rBag_10 = value;
	}

	inline static int32_t get_offset_of_m_rItemRects_11() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_rItemRects_11)); }
	inline RectU5BU5D_t141872167* get_m_rItemRects_11() const { return ___m_rItemRects_11; }
	inline RectU5BU5D_t141872167** get_address_of_m_rItemRects_11() { return &___m_rItemRects_11; }
	inline void set_m_rItemRects_11(RectU5BU5D_t141872167* value)
	{
		___m_rItemRects_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_rItemRects_11), value);
	}

	inline static int32_t get_offset_of_m_vFinalPos_12() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_vFinalPos_12)); }
	inline Vector2U5BU5D_t1220531434* get_m_vFinalPos_12() const { return ___m_vFinalPos_12; }
	inline Vector2U5BU5D_t1220531434** get_address_of_m_vFinalPos_12() { return &___m_vFinalPos_12; }
	inline void set_m_vFinalPos_12(Vector2U5BU5D_t1220531434* value)
	{
		___m_vFinalPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_vFinalPos_12), value);
	}

	inline static int32_t get_offset_of_m_bToggleShowItems_13() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_bToggleShowItems_13)); }
	inline bool get_m_bToggleShowItems_13() const { return ___m_bToggleShowItems_13; }
	inline bool* get_address_of_m_bToggleShowItems_13() { return &___m_bToggleShowItems_13; }
	inline void set_m_bToggleShowItems_13(bool value)
	{
		___m_bToggleShowItems_13 = value;
	}

	inline static int32_t get_offset_of_m_bMove_14() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_bMove_14)); }
	inline bool get_m_bMove_14() const { return ___m_bMove_14; }
	inline bool* get_address_of_m_bMove_14() { return &___m_bMove_14; }
	inline void set_m_bMove_14(bool value)
	{
		___m_bMove_14 = value;
	}

	inline static int32_t get_offset_of_m_bNotebookUp_15() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_bNotebookUp_15)); }
	inline bool get_m_bNotebookUp_15() const { return ___m_bNotebookUp_15; }
	inline bool* get_address_of_m_bNotebookUp_15() { return &___m_bNotebookUp_15; }
	inline void set_m_bNotebookUp_15(bool value)
	{
		___m_bNotebookUp_15 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_16() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___m_ObjectMobileController_16)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_16() const { return ___m_ObjectMobileController_16; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_16() { return &___m_ObjectMobileController_16; }
	inline void set_m_ObjectMobileController_16(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_16), value);
	}

	inline static int32_t get_offset_of_bridgeColor_17() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___bridgeColor_17)); }
	inline Color_t2582018970  get_bridgeColor_17() const { return ___bridgeColor_17; }
	inline Color_t2582018970 * get_address_of_bridgeColor_17() { return &___bridgeColor_17; }
	inline void set_bridgeColor_17(Color_t2582018970  value)
	{
		___bridgeColor_17 = value;
	}

	inline static int32_t get_offset_of_bridgeBackColor_18() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___bridgeBackColor_18)); }
	inline Color_t2582018970  get_bridgeBackColor_18() const { return ___bridgeBackColor_18; }
	inline Color_t2582018970 * get_address_of_bridgeBackColor_18() { return &___bridgeBackColor_18; }
	inline void set_bridgeBackColor_18(Color_t2582018970  value)
	{
		___bridgeBackColor_18 = value;
	}

	inline static int32_t get_offset_of_instance_19() { return static_cast<int32_t>(offsetof(Settings_t284352370, ___instance_19)); }
	inline CapturedDataInput_t2616152122 * get_instance_19() const { return ___instance_19; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_19() { return &___instance_19; }
	inline void set_instance_19(CapturedDataInput_t2616152122 * value)
	{
		___instance_19 = value;
		Il2CppCodeGenWriteBarrier((&___instance_19), value);
	}
};

struct Settings_t284352370_StaticFields
{
public:
	// System.Single Settings::m_fWidth
	float ___m_fWidth_7;
	// System.Single Settings::m_fHeight
	float ___m_fHeight_8;

public:
	inline static int32_t get_offset_of_m_fWidth_7() { return static_cast<int32_t>(offsetof(Settings_t284352370_StaticFields, ___m_fWidth_7)); }
	inline float get_m_fWidth_7() const { return ___m_fWidth_7; }
	inline float* get_address_of_m_fWidth_7() { return &___m_fWidth_7; }
	inline void set_m_fWidth_7(float value)
	{
		___m_fWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_fHeight_8() { return static_cast<int32_t>(offsetof(Settings_t284352370_StaticFields, ___m_fHeight_8)); }
	inline float get_m_fHeight_8() const { return ___m_fHeight_8; }
	inline float* get_address_of_m_fHeight_8() { return &___m_fHeight_8; }
	inline void set_m_fHeight_8(float value)
	{
		___m_fHeight_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T284352370_H
#ifndef ROCKHELPTRIGER_T2640156150_H
#define ROCKHELPTRIGER_T2640156150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RockHelpTriger
struct  RockHelpTriger_t2640156150  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean RockHelpTriger::m_EagleTriggerEntered
	bool ___m_EagleTriggerEntered_2;

public:
	inline static int32_t get_offset_of_m_EagleTriggerEntered_2() { return static_cast<int32_t>(offsetof(RockHelpTriger_t2640156150, ___m_EagleTriggerEntered_2)); }
	inline bool get_m_EagleTriggerEntered_2() const { return ___m_EagleTriggerEntered_2; }
	inline bool* get_address_of_m_EagleTriggerEntered_2() { return &___m_EagleTriggerEntered_2; }
	inline void set_m_EagleTriggerEntered_2(bool value)
	{
		___m_EagleTriggerEntered_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROCKHELPTRIGER_T2640156150_H
#ifndef ROCKHELP_T2267172338_H
#define ROCKHELP_T2267172338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RockHelp
struct  RockHelp_t2267172338  : public MonoBehaviour_t1618594486
{
public:
	// System.Boolean RockHelp::bActivated
	bool ___bActivated_2;
	// UnityEngine.GUISkin RockHelp::GUIskin
	GUISkin_t2122630221 * ___GUIskin_3;
	// System.String RockHelp::Text
	String_t* ___Text_4;
	// UnityEngine.GameObject RockHelp::oGameRock
	GameObject_t2557347079 * ___oGameRock_5;
	// PushRockMiniGame RockHelp::scriptPushRockMiniGame
	PushRockMiniGame_t1414473092 * ___scriptPushRockMiniGame_6;

public:
	inline static int32_t get_offset_of_bActivated_2() { return static_cast<int32_t>(offsetof(RockHelp_t2267172338, ___bActivated_2)); }
	inline bool get_bActivated_2() const { return ___bActivated_2; }
	inline bool* get_address_of_bActivated_2() { return &___bActivated_2; }
	inline void set_bActivated_2(bool value)
	{
		___bActivated_2 = value;
	}

	inline static int32_t get_offset_of_GUIskin_3() { return static_cast<int32_t>(offsetof(RockHelp_t2267172338, ___GUIskin_3)); }
	inline GUISkin_t2122630221 * get_GUIskin_3() const { return ___GUIskin_3; }
	inline GUISkin_t2122630221 ** get_address_of_GUIskin_3() { return &___GUIskin_3; }
	inline void set_GUIskin_3(GUISkin_t2122630221 * value)
	{
		___GUIskin_3 = value;
		Il2CppCodeGenWriteBarrier((&___GUIskin_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(RockHelp_t2267172338, ___Text_4)); }
	inline String_t* get_Text_4() const { return ___Text_4; }
	inline String_t** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(String_t* value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of_oGameRock_5() { return static_cast<int32_t>(offsetof(RockHelp_t2267172338, ___oGameRock_5)); }
	inline GameObject_t2557347079 * get_oGameRock_5() const { return ___oGameRock_5; }
	inline GameObject_t2557347079 ** get_address_of_oGameRock_5() { return &___oGameRock_5; }
	inline void set_oGameRock_5(GameObject_t2557347079 * value)
	{
		___oGameRock_5 = value;
		Il2CppCodeGenWriteBarrier((&___oGameRock_5), value);
	}

	inline static int32_t get_offset_of_scriptPushRockMiniGame_6() { return static_cast<int32_t>(offsetof(RockHelp_t2267172338, ___scriptPushRockMiniGame_6)); }
	inline PushRockMiniGame_t1414473092 * get_scriptPushRockMiniGame_6() const { return ___scriptPushRockMiniGame_6; }
	inline PushRockMiniGame_t1414473092 ** get_address_of_scriptPushRockMiniGame_6() { return &___scriptPushRockMiniGame_6; }
	inline void set_scriptPushRockMiniGame_6(PushRockMiniGame_t1414473092 * value)
	{
		___scriptPushRockMiniGame_6 = value;
		Il2CppCodeGenWriteBarrier((&___scriptPushRockMiniGame_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROCKHELP_T2267172338_H
#ifndef RESULTBARCHART_T1314512756_H
#define RESULTBARCHART_T1314512756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultBarchart
struct  ResultBarchart_t1314512756  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.TextAsset ResultBarchart::m_xmlFilePathTextAsset
	TextAsset_t2052027493 * ___m_xmlFilePathTextAsset_2;
	// UnityEngine.GUISkin ResultBarchart::m_skin
	GUISkin_t2122630221 * ___m_skin_3;
	// UnityEngine.Texture2D ResultBarchart::m_tBarchartLines
	Texture2D_t3063074017 * ___m_tBarchartLines_4;
	// UnityEngine.Texture2D ResultBarchart::m_tBarchartBar
	Texture2D_t3063074017 * ___m_tBarchartBar_5;
	// UnityEngine.Texture2D[] ResultBarchart::m_tBarColours
	Texture2DU5BU5D_t3304433276* ___m_tBarColours_6;
	// UnityEngine.Texture2D ResultBarchart::m_tBarGreen
	Texture2D_t3063074017 * ___m_tBarGreen_7;
	// UnityEngine.Texture2D ResultBarchart::m_tBarOrange
	Texture2D_t3063074017 * ___m_tBarOrange_8;
	// UnityEngine.Texture2D ResultBarchart::m_tBarRed
	Texture2D_t3063074017 * ___m_tBarRed_9;
	// System.Boolean ResultBarchart::m_bShowBarchartText
	bool ___m_bShowBarchartText_10;
	// TalkScenes ResultBarchart::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_11;
	// System.String ResultBarchart::displayText
	String_t* ___displayText_12;
	// System.Int32 ResultBarchart::severityScore
	int32_t ___severityScore_13;
	// System.Int32 ResultBarchart::questionCount
	int32_t ___questionCount_14;
	// System.Single[] ResultBarchart::scorePercentage
	SingleU5BU5D_t2843050510* ___scorePercentage_15;
	// UnityEngine.Rect ResultBarchart::m_rBarRect1
	Rect_t3039462994  ___m_rBarRect1_19;
	// UnityEngine.Rect ResultBarchart::m_rBarRect4
	Rect_t3039462994  ___m_rBarRect4_20;
	// UnityEngine.Rect ResultBarchart::m_rBarRect7
	Rect_t3039462994  ___m_rBarRect7_21;
	// UnityEngine.Sprite[] ResultBarchart::barColours
	SpriteU5BU5D_t27653605* ___barColours_22;
	// MoodGraph ResultBarchart::moodGraph
	MoodGraph_t2484840849 * ___moodGraph_23;

public:
	inline static int32_t get_offset_of_m_xmlFilePathTextAsset_2() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_xmlFilePathTextAsset_2)); }
	inline TextAsset_t2052027493 * get_m_xmlFilePathTextAsset_2() const { return ___m_xmlFilePathTextAsset_2; }
	inline TextAsset_t2052027493 ** get_address_of_m_xmlFilePathTextAsset_2() { return &___m_xmlFilePathTextAsset_2; }
	inline void set_m_xmlFilePathTextAsset_2(TextAsset_t2052027493 * value)
	{
		___m_xmlFilePathTextAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlFilePathTextAsset_2), value);
	}

	inline static int32_t get_offset_of_m_skin_3() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_skin_3)); }
	inline GUISkin_t2122630221 * get_m_skin_3() const { return ___m_skin_3; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_3() { return &___m_skin_3; }
	inline void set_m_skin_3(GUISkin_t2122630221 * value)
	{
		___m_skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_3), value);
	}

	inline static int32_t get_offset_of_m_tBarchartLines_4() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarchartLines_4)); }
	inline Texture2D_t3063074017 * get_m_tBarchartLines_4() const { return ___m_tBarchartLines_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarchartLines_4() { return &___m_tBarchartLines_4; }
	inline void set_m_tBarchartLines_4(Texture2D_t3063074017 * value)
	{
		___m_tBarchartLines_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarchartLines_4), value);
	}

	inline static int32_t get_offset_of_m_tBarchartBar_5() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarchartBar_5)); }
	inline Texture2D_t3063074017 * get_m_tBarchartBar_5() const { return ___m_tBarchartBar_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarchartBar_5() { return &___m_tBarchartBar_5; }
	inline void set_m_tBarchartBar_5(Texture2D_t3063074017 * value)
	{
		___m_tBarchartBar_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarchartBar_5), value);
	}

	inline static int32_t get_offset_of_m_tBarColours_6() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarColours_6)); }
	inline Texture2DU5BU5D_t3304433276* get_m_tBarColours_6() const { return ___m_tBarColours_6; }
	inline Texture2DU5BU5D_t3304433276** get_address_of_m_tBarColours_6() { return &___m_tBarColours_6; }
	inline void set_m_tBarColours_6(Texture2DU5BU5D_t3304433276* value)
	{
		___m_tBarColours_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarColours_6), value);
	}

	inline static int32_t get_offset_of_m_tBarGreen_7() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarGreen_7)); }
	inline Texture2D_t3063074017 * get_m_tBarGreen_7() const { return ___m_tBarGreen_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarGreen_7() { return &___m_tBarGreen_7; }
	inline void set_m_tBarGreen_7(Texture2D_t3063074017 * value)
	{
		___m_tBarGreen_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarGreen_7), value);
	}

	inline static int32_t get_offset_of_m_tBarOrange_8() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarOrange_8)); }
	inline Texture2D_t3063074017 * get_m_tBarOrange_8() const { return ___m_tBarOrange_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarOrange_8() { return &___m_tBarOrange_8; }
	inline void set_m_tBarOrange_8(Texture2D_t3063074017 * value)
	{
		___m_tBarOrange_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarOrange_8), value);
	}

	inline static int32_t get_offset_of_m_tBarRed_9() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_tBarRed_9)); }
	inline Texture2D_t3063074017 * get_m_tBarRed_9() const { return ___m_tBarRed_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_tBarRed_9() { return &___m_tBarRed_9; }
	inline void set_m_tBarRed_9(Texture2D_t3063074017 * value)
	{
		___m_tBarRed_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_tBarRed_9), value);
	}

	inline static int32_t get_offset_of_m_bShowBarchartText_10() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_bShowBarchartText_10)); }
	inline bool get_m_bShowBarchartText_10() const { return ___m_bShowBarchartText_10; }
	inline bool* get_address_of_m_bShowBarchartText_10() { return &___m_bShowBarchartText_10; }
	inline void set_m_bShowBarchartText_10(bool value)
	{
		___m_bShowBarchartText_10 = value;
	}

	inline static int32_t get_offset_of_m_talkScene_11() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_talkScene_11)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_11() const { return ___m_talkScene_11; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_11() { return &___m_talkScene_11; }
	inline void set_m_talkScene_11(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_11), value);
	}

	inline static int32_t get_offset_of_displayText_12() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___displayText_12)); }
	inline String_t* get_displayText_12() const { return ___displayText_12; }
	inline String_t** get_address_of_displayText_12() { return &___displayText_12; }
	inline void set_displayText_12(String_t* value)
	{
		___displayText_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayText_12), value);
	}

	inline static int32_t get_offset_of_severityScore_13() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___severityScore_13)); }
	inline int32_t get_severityScore_13() const { return ___severityScore_13; }
	inline int32_t* get_address_of_severityScore_13() { return &___severityScore_13; }
	inline void set_severityScore_13(int32_t value)
	{
		___severityScore_13 = value;
	}

	inline static int32_t get_offset_of_questionCount_14() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___questionCount_14)); }
	inline int32_t get_questionCount_14() const { return ___questionCount_14; }
	inline int32_t* get_address_of_questionCount_14() { return &___questionCount_14; }
	inline void set_questionCount_14(int32_t value)
	{
		___questionCount_14 = value;
	}

	inline static int32_t get_offset_of_scorePercentage_15() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___scorePercentage_15)); }
	inline SingleU5BU5D_t2843050510* get_scorePercentage_15() const { return ___scorePercentage_15; }
	inline SingleU5BU5D_t2843050510** get_address_of_scorePercentage_15() { return &___scorePercentage_15; }
	inline void set_scorePercentage_15(SingleU5BU5D_t2843050510* value)
	{
		___scorePercentage_15 = value;
		Il2CppCodeGenWriteBarrier((&___scorePercentage_15), value);
	}

	inline static int32_t get_offset_of_m_rBarRect1_19() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_rBarRect1_19)); }
	inline Rect_t3039462994  get_m_rBarRect1_19() const { return ___m_rBarRect1_19; }
	inline Rect_t3039462994 * get_address_of_m_rBarRect1_19() { return &___m_rBarRect1_19; }
	inline void set_m_rBarRect1_19(Rect_t3039462994  value)
	{
		___m_rBarRect1_19 = value;
	}

	inline static int32_t get_offset_of_m_rBarRect4_20() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_rBarRect4_20)); }
	inline Rect_t3039462994  get_m_rBarRect4_20() const { return ___m_rBarRect4_20; }
	inline Rect_t3039462994 * get_address_of_m_rBarRect4_20() { return &___m_rBarRect4_20; }
	inline void set_m_rBarRect4_20(Rect_t3039462994  value)
	{
		___m_rBarRect4_20 = value;
	}

	inline static int32_t get_offset_of_m_rBarRect7_21() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___m_rBarRect7_21)); }
	inline Rect_t3039462994  get_m_rBarRect7_21() const { return ___m_rBarRect7_21; }
	inline Rect_t3039462994 * get_address_of_m_rBarRect7_21() { return &___m_rBarRect7_21; }
	inline void set_m_rBarRect7_21(Rect_t3039462994  value)
	{
		___m_rBarRect7_21 = value;
	}

	inline static int32_t get_offset_of_barColours_22() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___barColours_22)); }
	inline SpriteU5BU5D_t27653605* get_barColours_22() const { return ___barColours_22; }
	inline SpriteU5BU5D_t27653605** get_address_of_barColours_22() { return &___barColours_22; }
	inline void set_barColours_22(SpriteU5BU5D_t27653605* value)
	{
		___barColours_22 = value;
		Il2CppCodeGenWriteBarrier((&___barColours_22), value);
	}

	inline static int32_t get_offset_of_moodGraph_23() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756, ___moodGraph_23)); }
	inline MoodGraph_t2484840849 * get_moodGraph_23() const { return ___moodGraph_23; }
	inline MoodGraph_t2484840849 ** get_address_of_moodGraph_23() { return &___moodGraph_23; }
	inline void set_moodGraph_23(MoodGraph_t2484840849 * value)
	{
		___moodGraph_23 = value;
		Il2CppCodeGenWriteBarrier((&___moodGraph_23), value);
	}
};

struct ResultBarchart_t1314512756_StaticFields
{
public:
	// System.Int32 ResultBarchart::m_iLevel1Score
	int32_t ___m_iLevel1Score_16;
	// System.Int32 ResultBarchart::m_iLevel4Score
	int32_t ___m_iLevel4Score_17;
	// System.Int32 ResultBarchart::m_iLevel7Score
	int32_t ___m_iLevel7Score_18;

public:
	inline static int32_t get_offset_of_m_iLevel1Score_16() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756_StaticFields, ___m_iLevel1Score_16)); }
	inline int32_t get_m_iLevel1Score_16() const { return ___m_iLevel1Score_16; }
	inline int32_t* get_address_of_m_iLevel1Score_16() { return &___m_iLevel1Score_16; }
	inline void set_m_iLevel1Score_16(int32_t value)
	{
		___m_iLevel1Score_16 = value;
	}

	inline static int32_t get_offset_of_m_iLevel4Score_17() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756_StaticFields, ___m_iLevel4Score_17)); }
	inline int32_t get_m_iLevel4Score_17() const { return ___m_iLevel4Score_17; }
	inline int32_t* get_address_of_m_iLevel4Score_17() { return &___m_iLevel4Score_17; }
	inline void set_m_iLevel4Score_17(int32_t value)
	{
		___m_iLevel4Score_17 = value;
	}

	inline static int32_t get_offset_of_m_iLevel7Score_18() { return static_cast<int32_t>(offsetof(ResultBarchart_t1314512756_StaticFields, ___m_iLevel7Score_18)); }
	inline int32_t get_m_iLevel7Score_18() const { return ___m_iLevel7Score_18; }
	inline int32_t* get_address_of_m_iLevel7Score_18() { return &___m_iLevel7Score_18; }
	inline void set_m_iLevel7Score_18(int32_t value)
	{
		___m_iLevel7Score_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTBARCHART_T1314512756_H
#ifndef QUITESCREEN_T956685441_H
#define QUITESCREEN_T956685441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuiteScreen
struct  QuiteScreen_t956685441  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin QuiteScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.Texture2D QuiteScreen::m_sQuiteTexture1
	Texture2D_t3063074017 * ___m_sQuiteTexture1_3;
	// UnityEngine.Texture2D QuiteScreen::m_sQuiteTexture2
	Texture2D_t3063074017 * ___m_sQuiteTexture2_4;
	// System.Single QuiteScreen::m_fStartTime
	float ___m_fStartTime_5;
	// System.String QuiteScreen::m_sQuiteText
	String_t* ___m_sQuiteText_6;
	// UnityEngine.Texture2D QuiteScreen::m_sQuiteTexture
	Texture2D_t3063074017 * ___m_sQuiteTexture_7;
	// System.Boolean QuiteScreen::m_bFinalImageDisplayed
	bool ___m_bFinalImageDisplayed_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_sQuiteTexture1_3() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_sQuiteTexture1_3)); }
	inline Texture2D_t3063074017 * get_m_sQuiteTexture1_3() const { return ___m_sQuiteTexture1_3; }
	inline Texture2D_t3063074017 ** get_address_of_m_sQuiteTexture1_3() { return &___m_sQuiteTexture1_3; }
	inline void set_m_sQuiteTexture1_3(Texture2D_t3063074017 * value)
	{
		___m_sQuiteTexture1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_sQuiteTexture1_3), value);
	}

	inline static int32_t get_offset_of_m_sQuiteTexture2_4() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_sQuiteTexture2_4)); }
	inline Texture2D_t3063074017 * get_m_sQuiteTexture2_4() const { return ___m_sQuiteTexture2_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_sQuiteTexture2_4() { return &___m_sQuiteTexture2_4; }
	inline void set_m_sQuiteTexture2_4(Texture2D_t3063074017 * value)
	{
		___m_sQuiteTexture2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_sQuiteTexture2_4), value);
	}

	inline static int32_t get_offset_of_m_fStartTime_5() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_fStartTime_5)); }
	inline float get_m_fStartTime_5() const { return ___m_fStartTime_5; }
	inline float* get_address_of_m_fStartTime_5() { return &___m_fStartTime_5; }
	inline void set_m_fStartTime_5(float value)
	{
		___m_fStartTime_5 = value;
	}

	inline static int32_t get_offset_of_m_sQuiteText_6() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_sQuiteText_6)); }
	inline String_t* get_m_sQuiteText_6() const { return ___m_sQuiteText_6; }
	inline String_t** get_address_of_m_sQuiteText_6() { return &___m_sQuiteText_6; }
	inline void set_m_sQuiteText_6(String_t* value)
	{
		___m_sQuiteText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sQuiteText_6), value);
	}

	inline static int32_t get_offset_of_m_sQuiteTexture_7() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_sQuiteTexture_7)); }
	inline Texture2D_t3063074017 * get_m_sQuiteTexture_7() const { return ___m_sQuiteTexture_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_sQuiteTexture_7() { return &___m_sQuiteTexture_7; }
	inline void set_m_sQuiteTexture_7(Texture2D_t3063074017 * value)
	{
		___m_sQuiteTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_sQuiteTexture_7), value);
	}

	inline static int32_t get_offset_of_m_bFinalImageDisplayed_8() { return static_cast<int32_t>(offsetof(QuiteScreen_t956685441, ___m_bFinalImageDisplayed_8)); }
	inline bool get_m_bFinalImageDisplayed_8() const { return ___m_bFinalImageDisplayed_8; }
	inline bool* get_address_of_m_bFinalImageDisplayed_8() { return &___m_bFinalImageDisplayed_8; }
	inline void set_m_bFinalImageDisplayed_8(bool value)
	{
		___m_bFinalImageDisplayed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUITESCREEN_T956685441_H
#ifndef OBJECTLABEL_T1667215479_H
#define OBJECTLABEL_T1667215479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectLabel
struct  ObjectLabel_t1667215479  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.Transform ObjectLabel::target
	Transform_t362059596 * ___target_2;
	// UnityEngine.Vector3 ObjectLabel::offset
	Vector3_t1986933152  ___offset_3;
	// System.Boolean ObjectLabel::clampToScreen
	bool ___clampToScreen_4;
	// System.Single ObjectLabel::clampBorderSize
	float ___clampBorderSize_5;
	// System.Boolean ObjectLabel::useMainCamera
	bool ___useMainCamera_6;
	// UnityEngine.Camera ObjectLabel::cameraToUse
	Camera_t2839736942 * ___cameraToUse_7;
	// UnityEngine.Camera ObjectLabel::cam
	Camera_t2839736942 * ___cam_8;
	// UnityEngine.Transform ObjectLabel::thisTransform
	Transform_t362059596 * ___thisTransform_9;
	// UnityEngine.Transform ObjectLabel::camTransform
	Transform_t362059596 * ___camTransform_10;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___target_2)); }
	inline Transform_t362059596 * get_target_2() const { return ___target_2; }
	inline Transform_t362059596 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t362059596 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___offset_3)); }
	inline Vector3_t1986933152  get_offset_3() const { return ___offset_3; }
	inline Vector3_t1986933152 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t1986933152  value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_clampToScreen_4() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___clampToScreen_4)); }
	inline bool get_clampToScreen_4() const { return ___clampToScreen_4; }
	inline bool* get_address_of_clampToScreen_4() { return &___clampToScreen_4; }
	inline void set_clampToScreen_4(bool value)
	{
		___clampToScreen_4 = value;
	}

	inline static int32_t get_offset_of_clampBorderSize_5() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___clampBorderSize_5)); }
	inline float get_clampBorderSize_5() const { return ___clampBorderSize_5; }
	inline float* get_address_of_clampBorderSize_5() { return &___clampBorderSize_5; }
	inline void set_clampBorderSize_5(float value)
	{
		___clampBorderSize_5 = value;
	}

	inline static int32_t get_offset_of_useMainCamera_6() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___useMainCamera_6)); }
	inline bool get_useMainCamera_6() const { return ___useMainCamera_6; }
	inline bool* get_address_of_useMainCamera_6() { return &___useMainCamera_6; }
	inline void set_useMainCamera_6(bool value)
	{
		___useMainCamera_6 = value;
	}

	inline static int32_t get_offset_of_cameraToUse_7() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___cameraToUse_7)); }
	inline Camera_t2839736942 * get_cameraToUse_7() const { return ___cameraToUse_7; }
	inline Camera_t2839736942 ** get_address_of_cameraToUse_7() { return &___cameraToUse_7; }
	inline void set_cameraToUse_7(Camera_t2839736942 * value)
	{
		___cameraToUse_7 = value;
		Il2CppCodeGenWriteBarrier((&___cameraToUse_7), value);
	}

	inline static int32_t get_offset_of_cam_8() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___cam_8)); }
	inline Camera_t2839736942 * get_cam_8() const { return ___cam_8; }
	inline Camera_t2839736942 ** get_address_of_cam_8() { return &___cam_8; }
	inline void set_cam_8(Camera_t2839736942 * value)
	{
		___cam_8 = value;
		Il2CppCodeGenWriteBarrier((&___cam_8), value);
	}

	inline static int32_t get_offset_of_thisTransform_9() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___thisTransform_9)); }
	inline Transform_t362059596 * get_thisTransform_9() const { return ___thisTransform_9; }
	inline Transform_t362059596 ** get_address_of_thisTransform_9() { return &___thisTransform_9; }
	inline void set_thisTransform_9(Transform_t362059596 * value)
	{
		___thisTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_9), value);
	}

	inline static int32_t get_offset_of_camTransform_10() { return static_cast<int32_t>(offsetof(ObjectLabel_t1667215479, ___camTransform_10)); }
	inline Transform_t362059596 * get_camTransform_10() const { return ___camTransform_10; }
	inline Transform_t362059596 ** get_address_of_camTransform_10() { return &___camTransform_10; }
	inline void set_camTransform_10(Transform_t362059596 * value)
	{
		___camTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTLABEL_T1667215479_H
#ifndef SAVEDL7NOTEBOOK_T74762212_H
#define SAVEDL7NOTEBOOK_T74762212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL7NoteBook
struct  SavedL7NoteBook_t74762212  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL7NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// CapturedDataInput SavedL7NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_3;
	// UnityEngine.Texture2D SavedL7NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_4;
	// UnityEngine.Texture2D SavedL7NoteBook::m_tPicture
	Texture2D_t3063074017 * ___m_tPicture_5;
	// UnityEngine.Texture2D SavedL7NoteBook::m_tShield
	Texture2D_t3063074017 * ___m_tShield_6;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL7NoteBook_t74762212, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SavedL7NoteBook_t74762212, ___instance_3)); }
	inline CapturedDataInput_t2616152122 * get_instance_3() const { return ___instance_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CapturedDataInput_t2616152122 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_4() { return static_cast<int32_t>(offsetof(SavedL7NoteBook_t74762212, ___m_tNoteBook_4)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_4() const { return ___m_tNoteBook_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_4() { return &___m_tNoteBook_4; }
	inline void set_m_tNoteBook_4(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_4), value);
	}

	inline static int32_t get_offset_of_m_tPicture_5() { return static_cast<int32_t>(offsetof(SavedL7NoteBook_t74762212, ___m_tPicture_5)); }
	inline Texture2D_t3063074017 * get_m_tPicture_5() const { return ___m_tPicture_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_tPicture_5() { return &___m_tPicture_5; }
	inline void set_m_tPicture_5(Texture2D_t3063074017 * value)
	{
		___m_tPicture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tPicture_5), value);
	}

	inline static int32_t get_offset_of_m_tShield_6() { return static_cast<int32_t>(offsetof(SavedL7NoteBook_t74762212, ___m_tShield_6)); }
	inline Texture2D_t3063074017 * get_m_tShield_6() const { return ___m_tShield_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_tShield_6() { return &___m_tShield_6; }
	inline void set_m_tShield_6(Texture2D_t3063074017 * value)
	{
		___m_tShield_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_tShield_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL7NOTEBOOK_T74762212_H
#ifndef SAVEDL6NOTEBOOK_T622742190_H
#define SAVEDL6NOTEBOOK_T622742190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL6NoteBook
struct  SavedL6NoteBook_t622742190  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL6NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String SavedL6NoteBook::text1
	String_t* ___text1_3;
	// System.String SavedL6NoteBook::text2
	String_t* ___text2_4;
	// System.String SavedL6NoteBook::text3
	String_t* ___text3_5;
	// CapturedDataInput SavedL6NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_6;
	// UnityEngine.Texture2D SavedL6NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_7;
	// UnityEngine.GameObject SavedL6NoteBook::menu
	GameObject_t2557347079 * ___menu_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___instance_6)); }
	inline CapturedDataInput_t2616152122 * get_instance_6() const { return ___instance_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(CapturedDataInput_t2616152122 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_7() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___m_tNoteBook_7)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_7() const { return ___m_tNoteBook_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_7() { return &___m_tNoteBook_7; }
	inline void set_m_tNoteBook_7(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_7), value);
	}

	inline static int32_t get_offset_of_menu_8() { return static_cast<int32_t>(offsetof(SavedL6NoteBook_t622742190, ___menu_8)); }
	inline GameObject_t2557347079 * get_menu_8() const { return ___menu_8; }
	inline GameObject_t2557347079 ** get_address_of_menu_8() { return &___menu_8; }
	inline void set_menu_8(GameObject_t2557347079 * value)
	{
		___menu_8 = value;
		Il2CppCodeGenWriteBarrier((&___menu_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL6NOTEBOOK_T622742190_H
#ifndef SAVEDL5NOTEBOOK_T3467111810_H
#define SAVEDL5NOTEBOOK_T3467111810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL5NoteBook
struct  SavedL5NoteBook_t3467111810  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL5NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String SavedL5NoteBook::text1
	String_t* ___text1_3;
	// System.String SavedL5NoteBook::text2
	String_t* ___text2_4;
	// System.String SavedL5NoteBook::text3
	String_t* ___text3_5;
	// CapturedDataInput SavedL5NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_6;
	// UnityEngine.Texture2D SavedL5NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_7;
	// UnityEngine.GameObject SavedL5NoteBook::menu
	GameObject_t2557347079 * ___menu_8;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___instance_6)); }
	inline CapturedDataInput_t2616152122 * get_instance_6() const { return ___instance_6; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(CapturedDataInput_t2616152122 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_7() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___m_tNoteBook_7)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_7() const { return ___m_tNoteBook_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_7() { return &___m_tNoteBook_7; }
	inline void set_m_tNoteBook_7(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_7), value);
	}

	inline static int32_t get_offset_of_menu_8() { return static_cast<int32_t>(offsetof(SavedL5NoteBook_t3467111810, ___menu_8)); }
	inline GameObject_t2557347079 * get_menu_8() const { return ___menu_8; }
	inline GameObject_t2557347079 ** get_address_of_menu_8() { return &___menu_8; }
	inline void set_menu_8(GameObject_t2557347079 * value)
	{
		___menu_8 = value;
		Il2CppCodeGenWriteBarrier((&___menu_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL5NOTEBOOK_T3467111810_H
#ifndef SAVEDL4NOTEBOOK_T2984262978_H
#define SAVEDL4NOTEBOOK_T2984262978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL4NoteBook
struct  SavedL4NoteBook_t2984262978  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL4NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// System.String SavedL4NoteBook::text1
	String_t* ___text1_3;
	// System.String SavedL4NoteBook::text2
	String_t* ___text2_4;
	// System.String SavedL4NoteBook::text3
	String_t* ___text3_5;
	// System.String SavedL4NoteBook::text4
	String_t* ___text4_6;
	// System.String SavedL4NoteBook::text5
	String_t* ___text5_7;
	// CapturedDataInput SavedL4NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_8;
	// UnityEngine.Texture2D SavedL4NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_9;
	// UnityEngine.GameObject SavedL4NoteBook::menu
	GameObject_t2557347079 * ___menu_10;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_text1_3() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___text1_3)); }
	inline String_t* get_text1_3() const { return ___text1_3; }
	inline String_t** get_address_of_text1_3() { return &___text1_3; }
	inline void set_text1_3(String_t* value)
	{
		___text1_3 = value;
		Il2CppCodeGenWriteBarrier((&___text1_3), value);
	}

	inline static int32_t get_offset_of_text2_4() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___text2_4)); }
	inline String_t* get_text2_4() const { return ___text2_4; }
	inline String_t** get_address_of_text2_4() { return &___text2_4; }
	inline void set_text2_4(String_t* value)
	{
		___text2_4 = value;
		Il2CppCodeGenWriteBarrier((&___text2_4), value);
	}

	inline static int32_t get_offset_of_text3_5() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___text3_5)); }
	inline String_t* get_text3_5() const { return ___text3_5; }
	inline String_t** get_address_of_text3_5() { return &___text3_5; }
	inline void set_text3_5(String_t* value)
	{
		___text3_5 = value;
		Il2CppCodeGenWriteBarrier((&___text3_5), value);
	}

	inline static int32_t get_offset_of_text4_6() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___text4_6)); }
	inline String_t* get_text4_6() const { return ___text4_6; }
	inline String_t** get_address_of_text4_6() { return &___text4_6; }
	inline void set_text4_6(String_t* value)
	{
		___text4_6 = value;
		Il2CppCodeGenWriteBarrier((&___text4_6), value);
	}

	inline static int32_t get_offset_of_text5_7() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___text5_7)); }
	inline String_t* get_text5_7() const { return ___text5_7; }
	inline String_t** get_address_of_text5_7() { return &___text5_7; }
	inline void set_text5_7(String_t* value)
	{
		___text5_7 = value;
		Il2CppCodeGenWriteBarrier((&___text5_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___instance_8)); }
	inline CapturedDataInput_t2616152122 * get_instance_8() const { return ___instance_8; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(CapturedDataInput_t2616152122 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_9() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___m_tNoteBook_9)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_9() const { return ___m_tNoteBook_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_9() { return &___m_tNoteBook_9; }
	inline void set_m_tNoteBook_9(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_9), value);
	}

	inline static int32_t get_offset_of_menu_10() { return static_cast<int32_t>(offsetof(SavedL4NoteBook_t2984262978, ___menu_10)); }
	inline GameObject_t2557347079 * get_menu_10() const { return ___menu_10; }
	inline GameObject_t2557347079 ** get_address_of_menu_10() { return &___menu_10; }
	inline void set_menu_10(GameObject_t2557347079 * value)
	{
		___menu_10 = value;
		Il2CppCodeGenWriteBarrier((&___menu_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL4NOTEBOOK_T2984262978_H
#ifndef SAVEDL3NOTEBOOK_T980617563_H
#define SAVEDL3NOTEBOOK_T980617563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedL3NoteBook
struct  SavedL3NoteBook_t980617563  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SavedL3NoteBook::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// CapturedDataInput SavedL3NoteBook::instance
	CapturedDataInput_t2616152122 * ___instance_3;
	// UnityEngine.Texture2D SavedL3NoteBook::m_tNoteBook
	Texture2D_t3063074017 * ___m_tNoteBook_4;
	// UnityEngine.GameObject SavedL3NoteBook::menu
	GameObject_t2557347079 * ___menu_5;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SavedL3NoteBook_t980617563, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SavedL3NoteBook_t980617563, ___instance_3)); }
	inline CapturedDataInput_t2616152122 * get_instance_3() const { return ___instance_3; }
	inline CapturedDataInput_t2616152122 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CapturedDataInput_t2616152122 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_tNoteBook_4() { return static_cast<int32_t>(offsetof(SavedL3NoteBook_t980617563, ___m_tNoteBook_4)); }
	inline Texture2D_t3063074017 * get_m_tNoteBook_4() const { return ___m_tNoteBook_4; }
	inline Texture2D_t3063074017 ** get_address_of_m_tNoteBook_4() { return &___m_tNoteBook_4; }
	inline void set_m_tNoteBook_4(Texture2D_t3063074017 * value)
	{
		___m_tNoteBook_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tNoteBook_4), value);
	}

	inline static int32_t get_offset_of_menu_5() { return static_cast<int32_t>(offsetof(SavedL3NoteBook_t980617563, ___menu_5)); }
	inline GameObject_t2557347079 * get_menu_5() const { return ___menu_5; }
	inline GameObject_t2557347079 ** get_address_of_menu_5() { return &___menu_5; }
	inline void set_menu_5(GameObject_t2557347079 * value)
	{
		___menu_5 = value;
		Il2CppCodeGenWriteBarrier((&___menu_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDL3NOTEBOOK_T980617563_H
#ifndef SETTINGSSCREEN_T3285851604_H
#define SETTINGSSCREEN_T3285851604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsScreen
struct  SettingsScreen_t3285851604  : public MonoBehaviour_t1618594486
{
public:
	// UnityEngine.GUISkin SettingsScreen::m_skin
	GUISkin_t2122630221 * ___m_skin_2;
	// UnityEngine.GameObject SettingsScreen::m_theGuardian
	GameObject_t2557347079 * ___m_theGuardian_3;
	// UnityEngine.GameObject SettingsScreen::m_theMontor
	GameObject_t2557347079 * ___m_theMontor_4;
	// UnityEngine.Texture2D SettingsScreen::m_mouseCorsorImage
	Texture2D_t3063074017 * ___m_mouseCorsorImage_5;
	// UnityEngine.Texture2D SettingsScreen::m_closeImage
	Texture2D_t3063074017 * ___m_closeImage_6;
	// UnityEngine.Texture2D SettingsScreen::m_backPackImage
	Texture2D_t3063074017 * ___m_backPackImage_7;
	// UnityEngine.Texture2D SettingsScreen::m_UIControl
	Texture2D_t3063074017 * ___m_UIControl_8;
	// UnityEngine.Texture2D SettingsScreen::m_ControlImage
	Texture2D_t3063074017 * ___m_ControlImage_9;
	// UnityEngine.Texture2D SettingsScreen::m_AcelaratorImage
	Texture2D_t3063074017 * ___m_AcelaratorImage_10;
	// UnityEngine.Texture2D SettingsScreen::loadingIcon
	Texture2D_t3063074017 * ___loadingIcon_11;
	// UnityEngine.Texture2D SettingsScreen::m_ButtonOn
	Texture2D_t3063074017 * ___m_ButtonOn_12;
	// UnityEngine.Texture2D SettingsScreen::m_ButtonOff
	Texture2D_t3063074017 * ___m_ButtonOff_13;
	// UnityEngine.GameObject SettingsScreen::m_gardianClone
	GameObject_t2557347079 * ___m_gardianClone_14;
	// UnityEngine.GameObject SettingsScreen::m_mentorClone
	GameObject_t2557347079 * ___m_mentorClone_15;
	// TalkScenes SettingsScreen::m_talkScene
	TalkScenes_t1215051795 * ___m_talkScene_16;
	// UnityEngine.GameObject SettingsScreen::m_gGardianObject
	GameObject_t2557347079 * ___m_gGardianObject_17;
	// UnityEngine.GameObject SettingsScreen::m_ObjectMobileController
	GameObject_t2557347079 * ___m_ObjectMobileController_18;
	// UnityEngine.GameObject SettingsScreen::m_ObjectMobileControllerMove
	GameObject_t2557347079 * ___m_ObjectMobileControllerMove_19;
	// UnityEngine.GameObject SettingsScreen::m_ObjectMobileControllerSprint
	GameObject_t2557347079 * ___m_ObjectMobileControllerSprint_20;
	// UnityEngine.Vector3 SettingsScreen::m_ObjectMobileController_LeftPosition
	Vector3_t1986933152  ___m_ObjectMobileController_LeftPosition_21;
	// UnityEngine.Vector3 SettingsScreen::m_ObjectMobileController_RightPosition
	Vector3_t1986933152  ___m_ObjectMobileController_RightPosition_22;

public:
	inline static int32_t get_offset_of_m_skin_2() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_skin_2)); }
	inline GUISkin_t2122630221 * get_m_skin_2() const { return ___m_skin_2; }
	inline GUISkin_t2122630221 ** get_address_of_m_skin_2() { return &___m_skin_2; }
	inline void set_m_skin_2(GUISkin_t2122630221 * value)
	{
		___m_skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_skin_2), value);
	}

	inline static int32_t get_offset_of_m_theGuardian_3() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_theGuardian_3)); }
	inline GameObject_t2557347079 * get_m_theGuardian_3() const { return ___m_theGuardian_3; }
	inline GameObject_t2557347079 ** get_address_of_m_theGuardian_3() { return &___m_theGuardian_3; }
	inline void set_m_theGuardian_3(GameObject_t2557347079 * value)
	{
		___m_theGuardian_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_theGuardian_3), value);
	}

	inline static int32_t get_offset_of_m_theMontor_4() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_theMontor_4)); }
	inline GameObject_t2557347079 * get_m_theMontor_4() const { return ___m_theMontor_4; }
	inline GameObject_t2557347079 ** get_address_of_m_theMontor_4() { return &___m_theMontor_4; }
	inline void set_m_theMontor_4(GameObject_t2557347079 * value)
	{
		___m_theMontor_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_theMontor_4), value);
	}

	inline static int32_t get_offset_of_m_mouseCorsorImage_5() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_mouseCorsorImage_5)); }
	inline Texture2D_t3063074017 * get_m_mouseCorsorImage_5() const { return ___m_mouseCorsorImage_5; }
	inline Texture2D_t3063074017 ** get_address_of_m_mouseCorsorImage_5() { return &___m_mouseCorsorImage_5; }
	inline void set_m_mouseCorsorImage_5(Texture2D_t3063074017 * value)
	{
		___m_mouseCorsorImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_mouseCorsorImage_5), value);
	}

	inline static int32_t get_offset_of_m_closeImage_6() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_closeImage_6)); }
	inline Texture2D_t3063074017 * get_m_closeImage_6() const { return ___m_closeImage_6; }
	inline Texture2D_t3063074017 ** get_address_of_m_closeImage_6() { return &___m_closeImage_6; }
	inline void set_m_closeImage_6(Texture2D_t3063074017 * value)
	{
		___m_closeImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_closeImage_6), value);
	}

	inline static int32_t get_offset_of_m_backPackImage_7() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_backPackImage_7)); }
	inline Texture2D_t3063074017 * get_m_backPackImage_7() const { return ___m_backPackImage_7; }
	inline Texture2D_t3063074017 ** get_address_of_m_backPackImage_7() { return &___m_backPackImage_7; }
	inline void set_m_backPackImage_7(Texture2D_t3063074017 * value)
	{
		___m_backPackImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_backPackImage_7), value);
	}

	inline static int32_t get_offset_of_m_UIControl_8() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_UIControl_8)); }
	inline Texture2D_t3063074017 * get_m_UIControl_8() const { return ___m_UIControl_8; }
	inline Texture2D_t3063074017 ** get_address_of_m_UIControl_8() { return &___m_UIControl_8; }
	inline void set_m_UIControl_8(Texture2D_t3063074017 * value)
	{
		___m_UIControl_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_UIControl_8), value);
	}

	inline static int32_t get_offset_of_m_ControlImage_9() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ControlImage_9)); }
	inline Texture2D_t3063074017 * get_m_ControlImage_9() const { return ___m_ControlImage_9; }
	inline Texture2D_t3063074017 ** get_address_of_m_ControlImage_9() { return &___m_ControlImage_9; }
	inline void set_m_ControlImage_9(Texture2D_t3063074017 * value)
	{
		___m_ControlImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlImage_9), value);
	}

	inline static int32_t get_offset_of_m_AcelaratorImage_10() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_AcelaratorImage_10)); }
	inline Texture2D_t3063074017 * get_m_AcelaratorImage_10() const { return ___m_AcelaratorImage_10; }
	inline Texture2D_t3063074017 ** get_address_of_m_AcelaratorImage_10() { return &___m_AcelaratorImage_10; }
	inline void set_m_AcelaratorImage_10(Texture2D_t3063074017 * value)
	{
		___m_AcelaratorImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_AcelaratorImage_10), value);
	}

	inline static int32_t get_offset_of_loadingIcon_11() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___loadingIcon_11)); }
	inline Texture2D_t3063074017 * get_loadingIcon_11() const { return ___loadingIcon_11; }
	inline Texture2D_t3063074017 ** get_address_of_loadingIcon_11() { return &___loadingIcon_11; }
	inline void set_loadingIcon_11(Texture2D_t3063074017 * value)
	{
		___loadingIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&___loadingIcon_11), value);
	}

	inline static int32_t get_offset_of_m_ButtonOn_12() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ButtonOn_12)); }
	inline Texture2D_t3063074017 * get_m_ButtonOn_12() const { return ___m_ButtonOn_12; }
	inline Texture2D_t3063074017 ** get_address_of_m_ButtonOn_12() { return &___m_ButtonOn_12; }
	inline void set_m_ButtonOn_12(Texture2D_t3063074017 * value)
	{
		___m_ButtonOn_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ButtonOn_12), value);
	}

	inline static int32_t get_offset_of_m_ButtonOff_13() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ButtonOff_13)); }
	inline Texture2D_t3063074017 * get_m_ButtonOff_13() const { return ___m_ButtonOff_13; }
	inline Texture2D_t3063074017 ** get_address_of_m_ButtonOff_13() { return &___m_ButtonOff_13; }
	inline void set_m_ButtonOff_13(Texture2D_t3063074017 * value)
	{
		___m_ButtonOff_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ButtonOff_13), value);
	}

	inline static int32_t get_offset_of_m_gardianClone_14() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_gardianClone_14)); }
	inline GameObject_t2557347079 * get_m_gardianClone_14() const { return ___m_gardianClone_14; }
	inline GameObject_t2557347079 ** get_address_of_m_gardianClone_14() { return &___m_gardianClone_14; }
	inline void set_m_gardianClone_14(GameObject_t2557347079 * value)
	{
		___m_gardianClone_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_gardianClone_14), value);
	}

	inline static int32_t get_offset_of_m_mentorClone_15() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_mentorClone_15)); }
	inline GameObject_t2557347079 * get_m_mentorClone_15() const { return ___m_mentorClone_15; }
	inline GameObject_t2557347079 ** get_address_of_m_mentorClone_15() { return &___m_mentorClone_15; }
	inline void set_m_mentorClone_15(GameObject_t2557347079 * value)
	{
		___m_mentorClone_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_mentorClone_15), value);
	}

	inline static int32_t get_offset_of_m_talkScene_16() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_talkScene_16)); }
	inline TalkScenes_t1215051795 * get_m_talkScene_16() const { return ___m_talkScene_16; }
	inline TalkScenes_t1215051795 ** get_address_of_m_talkScene_16() { return &___m_talkScene_16; }
	inline void set_m_talkScene_16(TalkScenes_t1215051795 * value)
	{
		___m_talkScene_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_talkScene_16), value);
	}

	inline static int32_t get_offset_of_m_gGardianObject_17() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_gGardianObject_17)); }
	inline GameObject_t2557347079 * get_m_gGardianObject_17() const { return ___m_gGardianObject_17; }
	inline GameObject_t2557347079 ** get_address_of_m_gGardianObject_17() { return &___m_gGardianObject_17; }
	inline void set_m_gGardianObject_17(GameObject_t2557347079 * value)
	{
		___m_gGardianObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_gGardianObject_17), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_18() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ObjectMobileController_18)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileController_18() const { return ___m_ObjectMobileController_18; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileController_18() { return &___m_ObjectMobileController_18; }
	inline void set_m_ObjectMobileController_18(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileController_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileController_18), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerMove_19() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ObjectMobileControllerMove_19)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerMove_19() const { return ___m_ObjectMobileControllerMove_19; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerMove_19() { return &___m_ObjectMobileControllerMove_19; }
	inline void set_m_ObjectMobileControllerMove_19(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerMove_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerMove_19), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileControllerSprint_20() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ObjectMobileControllerSprint_20)); }
	inline GameObject_t2557347079 * get_m_ObjectMobileControllerSprint_20() const { return ___m_ObjectMobileControllerSprint_20; }
	inline GameObject_t2557347079 ** get_address_of_m_ObjectMobileControllerSprint_20() { return &___m_ObjectMobileControllerSprint_20; }
	inline void set_m_ObjectMobileControllerSprint_20(GameObject_t2557347079 * value)
	{
		___m_ObjectMobileControllerSprint_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectMobileControllerSprint_20), value);
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_LeftPosition_21() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ObjectMobileController_LeftPosition_21)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_LeftPosition_21() const { return ___m_ObjectMobileController_LeftPosition_21; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_LeftPosition_21() { return &___m_ObjectMobileController_LeftPosition_21; }
	inline void set_m_ObjectMobileController_LeftPosition_21(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_LeftPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ObjectMobileController_RightPosition_22() { return static_cast<int32_t>(offsetof(SettingsScreen_t3285851604, ___m_ObjectMobileController_RightPosition_22)); }
	inline Vector3_t1986933152  get_m_ObjectMobileController_RightPosition_22() const { return ___m_ObjectMobileController_RightPosition_22; }
	inline Vector3_t1986933152 * get_address_of_m_ObjectMobileController_RightPosition_22() { return &___m_ObjectMobileController_RightPosition_22; }
	inline void set_m_ObjectMobileController_RightPosition_22(Vector3_t1986933152  value)
	{
		___m_ObjectMobileController_RightPosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSSCREEN_T3285851604_H
#ifndef L5DRAGDROPWORDS2_T466088557_H
#define L5DRAGDROPWORDS2_T466088557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// L5DragDropWords2
struct  L5DragDropWords2_t466088557  : public MonoBehaviour_t1618594486
{
public:
	// System.String[] L5DragDropWords2::m_destinationText
	StringU5BU5D_t2511808107* ___m_destinationText_2;
	// System.String[] L5DragDropWords2::draggables
	StringU5BU5D_t2511808107* ___draggables_3;
	// UnityEngine.GameObject L5DragDropWords2::menu
	GameObject_t2557347079 * ___menu_4;

public:
	inline static int32_t get_offset_of_m_destinationText_2() { return static_cast<int32_t>(offsetof(L5DragDropWords2_t466088557, ___m_destinationText_2)); }
	inline StringU5BU5D_t2511808107* get_m_destinationText_2() const { return ___m_destinationText_2; }
	inline StringU5BU5D_t2511808107** get_address_of_m_destinationText_2() { return &___m_destinationText_2; }
	inline void set_m_destinationText_2(StringU5BU5D_t2511808107* value)
	{
		___m_destinationText_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_destinationText_2), value);
	}

	inline static int32_t get_offset_of_draggables_3() { return static_cast<int32_t>(offsetof(L5DragDropWords2_t466088557, ___draggables_3)); }
	inline StringU5BU5D_t2511808107* get_draggables_3() const { return ___draggables_3; }
	inline StringU5BU5D_t2511808107** get_address_of_draggables_3() { return &___draggables_3; }
	inline void set_draggables_3(StringU5BU5D_t2511808107* value)
	{
		___draggables_3 = value;
		Il2CppCodeGenWriteBarrier((&___draggables_3), value);
	}

	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(L5DragDropWords2_t466088557, ___menu_4)); }
	inline GameObject_t2557347079 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t2557347079 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t2557347079 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L5DRAGDROPWORDS2_T466088557_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (L5DragDropWords2_t466088557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[3] = 
{
	L5DragDropWords2_t466088557::get_offset_of_m_destinationText_2(),
	L5DragDropWords2_t466088557::get_offset_of_draggables_3(),
	L5DragDropWords2_t466088557::get_offset_of_menu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (L5EnterTexts_t1471665743), -1, sizeof(L5EnterTexts_t1471665743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2901[2] = 
{
	L5EnterTexts_t1471665743_StaticFields::get_offset_of_m_inputTexts_2(),
	L5EnterTexts_t1471665743::get_offset_of_menu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (L5ProgressTrigger_t2831516982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[3] = 
{
	L5ProgressTrigger_t2831516982::get_offset_of_startTalk_2(),
	L5ProgressTrigger_t2831516982::get_offset_of_m_skin_3(),
	L5ProgressTrigger_t2831516982::get_offset_of_tempPos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (L5ShieldAgaDepression_t2788179544), -1, sizeof(L5ShieldAgaDepression_t2788179544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[14] = 
{
	L5ShieldAgaDepression_t2788179544::get_offset_of_m_skin_2(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_m_shieldPrf_3(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_m_vShieldPosition_4(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_m_shieldObjectClone_5(),
	L5ShieldAgaDepression_t2788179544_StaticFields::get_offset_of_TextWidth_6(),
	L5ShieldAgaDepression_t2788179544_StaticFields::get_offset_of_TextHeight_7(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_childObjectNames_8(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_shieldNames_9(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_shieldNameRects_10(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_displayName_11(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_displayColor_12(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_colourTexture_13(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_greyScaleTexture_14(),
	L5ShieldAgaDepression_t2788179544::get_offset_of_menu_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (L6AnalyseNegativeStatement_t2433320051), -1, sizeof(L6AnalyseNegativeStatement_t2433320051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2904[5] = 
{
	L6AnalyseNegativeStatement_t2433320051::get_offset_of_m_skin_2(),
	L6AnalyseNegativeStatement_t2433320051_StaticFields::get_offset_of_m_sChoices_3(),
	L6AnalyseNegativeStatement_t2433320051::get_offset_of_headings_4(),
	L6AnalyseNegativeStatement_t2433320051::get_offset_of_titles_5(),
	L6AnalyseNegativeStatement_t2433320051::get_offset_of_menu_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (L6MatchingTextDragDrop_t1098812931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[3] = 
{
	L6MatchingTextDragDrop_t1098812931::get_offset_of_m_destinationText_2(),
	L6MatchingTextDragDrop_t1098812931::get_offset_of_draggables_3(),
	L6MatchingTextDragDrop_t1098812931::get_offset_of_menu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (L6NegativeStatement_t2202065469), -1, sizeof(L6NegativeStatement_t2202065469_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2906[4] = 
{
	L6NegativeStatement_t2202065469::get_offset_of_m_bNegativeStatementSelectList_2(),
	L6NegativeStatement_t2202065469_StaticFields::get_offset_of_m_sChoice_3(),
	L6NegativeStatement_t2202065469::get_offset_of_menu_4(),
	L6NegativeStatement_t2202065469::get_offset_of_choices_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (L6ShieldAgaDepression_t544321687), -1, sizeof(L6ShieldAgaDepression_t544321687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2907[14] = 
{
	L6ShieldAgaDepression_t544321687::get_offset_of_m_skin_2(),
	L6ShieldAgaDepression_t544321687::get_offset_of_m_shieldPrf_3(),
	L6ShieldAgaDepression_t544321687::get_offset_of_m_vShieldPosition_4(),
	L6ShieldAgaDepression_t544321687::get_offset_of_m_shieldObjectClone_5(),
	L6ShieldAgaDepression_t544321687_StaticFields::get_offset_of_TextWidth_6(),
	L6ShieldAgaDepression_t544321687_StaticFields::get_offset_of_TextHeight_7(),
	L6ShieldAgaDepression_t544321687::get_offset_of_childObjectNames_8(),
	L6ShieldAgaDepression_t544321687::get_offset_of_shieldNames_9(),
	L6ShieldAgaDepression_t544321687::get_offset_of_shieldNameRects_10(),
	L6ShieldAgaDepression_t544321687::get_offset_of_displayName_11(),
	L6ShieldAgaDepression_t544321687::get_offset_of_displayColor_12(),
	L6ShieldAgaDepression_t544321687::get_offset_of_colourTexture_13(),
	L6ShieldAgaDepression_t544321687::get_offset_of_greyScaleTexture_14(),
	L6ShieldAgaDepression_t544321687::get_offset_of_menu_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (L6SortItNegotiate_t1352594894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[1] = 
{
	L6SortItNegotiate_t1352594894::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (L6StopTrashTurnIt_t1936386210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[1] = 
{
	L6StopTrashTurnIt_t1936386210::get_offset_of_menu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (L7LessonsMatch_t1153153057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[4] = 
{
	L7LessonsMatch_t1153153057::get_offset_of_strHowDoesItRelate_2(),
	L7LessonsMatch_t1153153057::get_offset_of_strWhatsTheMessage_3(),
	L7LessonsMatch_t1153153057::get_offset_of_questions_4(),
	L7LessonsMatch_t1153153057::get_offset_of_menu_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (L7PlayerMoveRock_t3460284416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (L7ShieldAgaDepression_t3190915008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[5] = 
{
	L7ShieldAgaDepression_t3190915008::get_offset_of_m_shieldPrf_2(),
	L7ShieldAgaDepression_t3190915008::get_offset_of_m_vShieldPosition_3(),
	L7ShieldAgaDepression_t3190915008::get_offset_of_m_shieldObjectClone_4(),
	L7ShieldAgaDepression_t3190915008::get_offset_of_displayName_5(),
	L7ShieldAgaDepression_t3190915008::get_offset_of_menu_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (L7ShieldAgaDepression2_t1497217947), -1, sizeof(L7ShieldAgaDepression2_t1497217947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2913[16] = 
{
	L7ShieldAgaDepression2_t1497217947::get_offset_of_m_skin_2(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_m_shieldPrf_3(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_m_vShieldPosition_4(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_m_shieldObjectClone_5(),
	L7ShieldAgaDepression2_t1497217947_StaticFields::get_offset_of_TextWidth_6(),
	L7ShieldAgaDepression2_t1497217947_StaticFields::get_offset_of_TextHeight_7(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_childObjectNames_8(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_shieldNames_9(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_shieldNameRects_10(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_displayName_11(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_displayColor_12(),
	L7ShieldAgaDepression2_t1497217947_StaticFields::get_offset_of_m_targetDim_13(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_m_destinations_14(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_colourTexture_15(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_greyScaleTexture_16(),
	L7ShieldAgaDepression2_t1497217947::get_offset_of_menu_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (L7StartCatchSparks_t2930775878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (L7TakingHome_t92063034), -1, sizeof(L7TakingHome_t92063034_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2915[8] = 
{
	L7TakingHome_t92063034::get_offset_of_m_skin_2(),
	L7TakingHome_t92063034::get_offset_of_m_textures_3(),
	L7TakingHome_t92063034_StaticFields::get_offset_of_m_tChoice_4(),
	L7TakingHome_t92063034::get_offset_of_takeHomeArray_5(),
	L7TakingHome_t92063034::get_offset_of_m_conversationBoxText_6(),
	L7TakingHome_t92063034::get_offset_of_m_selectedIndex_7(),
	L7TakingHome_t92063034::get_offset_of_m_tConversationBackground_8(),
	L7TakingHome_t92063034::get_offset_of_menu_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (L7WeatherChange_t3480560370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[12] = 
{
	L7WeatherChange_t3480560370::get_offset_of_m_fFadeInStartTime_2(),
	L7WeatherChange_t3480560370::get_offset_of_m_fFadeOutStartTime_3(),
	L7WeatherChange_t3480560370::get_offset_of_m_fWaitTime_4(),
	L7WeatherChange_t3480560370::get_offset_of_m_fLightMainIntensity_5(),
	L7WeatherChange_t3480560370::get_offset_of_m_fLightRimIntensity_6(),
	L7WeatherChange_t3480560370::get_offset_of_m_fFogDensity_7(),
	L7WeatherChange_t3480560370::get_offset_of_m_fTime_8(),
	L7WeatherChange_t3480560370::get_offset_of_m_bFadeInFinished_9(),
	L7WeatherChange_t3480560370::get_offset_of_m_bPlayerStartsWait_10(),
	L7WeatherChange_t3480560370::get_offset_of_m_bFadeOutStarted_11(),
	L7WeatherChange_t3480560370::get_offset_of_m_bTuiIsFlyingDown_12(),
	L7WeatherChange_t3480560370::get_offset_of_paused_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (LanternHelp_t4264799245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[3] = 
{
	LanternHelp_t4264799245::get_offset_of_bActivated_2(),
	LanternHelp_t4264799245::get_offset_of_GUIskin_3(),
	LanternHelp_t4264799245::get_offset_of_Text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (MouseCursorChange_t4178418145), -1, sizeof(MouseCursorChange_t4178418145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2918[3] = 
{
	MouseCursorChange_t4178418145_StaticFields::get_offset_of_instanceRef_2(),
	MouseCursorChange_t4178418145::get_offset_of_cursorTexture_3(),
	MouseCursorChange_t4178418145::get_offset_of_ccEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (L1NoteBook_t1106537385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[5] = 
{
	L1NoteBook_t1106537385::get_offset_of_instance_2(),
	L1NoteBook_t1106537385::get_offset_of_m_tNoteBook_3(),
	L1NoteBook_t1106537385::get_offset_of_m_iIndex_4(),
	L1NoteBook_t1106537385::get_offset_of_failedFileName_5(),
	L1NoteBook_t1106537385::get_offset_of_menu_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (L2NoteBook_t3759025291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[9] = 
{
	L2NoteBook_t3759025291::get_offset_of_m_skin_2(),
	L2NoteBook_t3759025291::get_offset_of_text1_3(),
	L2NoteBook_t3759025291::get_offset_of_text2_4(),
	L2NoteBook_t3759025291::get_offset_of_text3_5(),
	L2NoteBook_t3759025291::get_offset_of_instance_6(),
	L2NoteBook_t3759025291::get_offset_of_m_tNoteBook_7(),
	L2NoteBook_t3759025291::get_offset_of_m_iIndex_8(),
	L2NoteBook_t3759025291::get_offset_of_failedFileName_9(),
	L2NoteBook_t3759025291::get_offset_of_menu_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (L3NoteBook_t121990616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[6] = 
{
	L3NoteBook_t121990616::get_offset_of_m_skin_2(),
	L3NoteBook_t121990616::get_offset_of_instance_3(),
	L3NoteBook_t121990616::get_offset_of_m_tNoteBook_4(),
	L3NoteBook_t121990616::get_offset_of_m_iIndex_5(),
	L3NoteBook_t121990616::get_offset_of_failedFileName_6(),
	L3NoteBook_t121990616::get_offset_of_menu_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (L4NoteBook_t2932888333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[11] = 
{
	L4NoteBook_t2932888333::get_offset_of_m_skin_2(),
	L4NoteBook_t2932888333::get_offset_of_text1_3(),
	L4NoteBook_t2932888333::get_offset_of_text2_4(),
	L4NoteBook_t2932888333::get_offset_of_text3_5(),
	L4NoteBook_t2932888333::get_offset_of_text4_6(),
	L4NoteBook_t2932888333::get_offset_of_text5_7(),
	L4NoteBook_t2932888333::get_offset_of_instance_8(),
	L4NoteBook_t2932888333::get_offset_of_m_tNoteBook_9(),
	L4NoteBook_t2932888333::get_offset_of_m_iIndex_10(),
	L4NoteBook_t2932888333::get_offset_of_failedFileName_11(),
	L4NoteBook_t2932888333::get_offset_of_menu_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (L5NoteBook_t443533060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[9] = 
{
	L5NoteBook_t443533060::get_offset_of_m_skin_2(),
	L5NoteBook_t443533060::get_offset_of_text1_3(),
	L5NoteBook_t443533060::get_offset_of_text2_4(),
	L5NoteBook_t443533060::get_offset_of_text3_5(),
	L5NoteBook_t443533060::get_offset_of_instance_6(),
	L5NoteBook_t443533060::get_offset_of_m_tNoteBook_7(),
	L5NoteBook_t443533060::get_offset_of_m_iIndex_8(),
	L5NoteBook_t443533060::get_offset_of_failedFileName_9(),
	L5NoteBook_t443533060::get_offset_of_menu_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (L6NoteBook_t2887596315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[9] = 
{
	L6NoteBook_t2887596315::get_offset_of_m_skin_2(),
	L6NoteBook_t2887596315::get_offset_of_text1_3(),
	L6NoteBook_t2887596315::get_offset_of_text2_4(),
	L6NoteBook_t2887596315::get_offset_of_text3_5(),
	L6NoteBook_t2887596315::get_offset_of_instance_6(),
	L6NoteBook_t2887596315::get_offset_of_m_tNoteBook_7(),
	L6NoteBook_t2887596315::get_offset_of_m_iIndex_8(),
	L6NoteBook_t2887596315::get_offset_of_failedFileName_9(),
	L6NoteBook_t2887596315::get_offset_of_menu_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (L7NoteBook_t3956512365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[7] = 
{
	L7NoteBook_t3956512365::get_offset_of_m_skin_2(),
	L7NoteBook_t3956512365::get_offset_of_instance_3(),
	L7NoteBook_t3956512365::get_offset_of_m_tNoteBook_4(),
	L7NoteBook_t3956512365::get_offset_of_m_tPicture_5(),
	L7NoteBook_t3956512365::get_offset_of_m_tShield_6(),
	L7NoteBook_t3956512365::get_offset_of_failedFileName_7(),
	L7NoteBook_t3956512365::get_offset_of_menu_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (SavedL1NoteBook_t606761968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[4] = 
{
	SavedL1NoteBook_t606761968::get_offset_of_m_skin_2(),
	SavedL1NoteBook_t606761968::get_offset_of_menu_3(),
	SavedL1NoteBook_t606761968::get_offset_of_instance_4(),
	SavedL1NoteBook_t606761968::get_offset_of_m_tNoteBook_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (SavedL2NoteBook_t2697330983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[4] = 
{
	SavedL2NoteBook_t2697330983::get_offset_of_m_skin_2(),
	SavedL2NoteBook_t2697330983::get_offset_of_instance_3(),
	SavedL2NoteBook_t2697330983::get_offset_of_m_tNoteBook_4(),
	SavedL2NoteBook_t2697330983::get_offset_of_menu_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (SavedL3NoteBook_t980617563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[4] = 
{
	SavedL3NoteBook_t980617563::get_offset_of_m_skin_2(),
	SavedL3NoteBook_t980617563::get_offset_of_instance_3(),
	SavedL3NoteBook_t980617563::get_offset_of_m_tNoteBook_4(),
	SavedL3NoteBook_t980617563::get_offset_of_menu_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (SavedL4NoteBook_t2984262978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[9] = 
{
	SavedL4NoteBook_t2984262978::get_offset_of_m_skin_2(),
	SavedL4NoteBook_t2984262978::get_offset_of_text1_3(),
	SavedL4NoteBook_t2984262978::get_offset_of_text2_4(),
	SavedL4NoteBook_t2984262978::get_offset_of_text3_5(),
	SavedL4NoteBook_t2984262978::get_offset_of_text4_6(),
	SavedL4NoteBook_t2984262978::get_offset_of_text5_7(),
	SavedL4NoteBook_t2984262978::get_offset_of_instance_8(),
	SavedL4NoteBook_t2984262978::get_offset_of_m_tNoteBook_9(),
	SavedL4NoteBook_t2984262978::get_offset_of_menu_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (SavedL5NoteBook_t3467111810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[7] = 
{
	SavedL5NoteBook_t3467111810::get_offset_of_m_skin_2(),
	SavedL5NoteBook_t3467111810::get_offset_of_text1_3(),
	SavedL5NoteBook_t3467111810::get_offset_of_text2_4(),
	SavedL5NoteBook_t3467111810::get_offset_of_text3_5(),
	SavedL5NoteBook_t3467111810::get_offset_of_instance_6(),
	SavedL5NoteBook_t3467111810::get_offset_of_m_tNoteBook_7(),
	SavedL5NoteBook_t3467111810::get_offset_of_menu_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (SavedL6NoteBook_t622742190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[7] = 
{
	SavedL6NoteBook_t622742190::get_offset_of_m_skin_2(),
	SavedL6NoteBook_t622742190::get_offset_of_text1_3(),
	SavedL6NoteBook_t622742190::get_offset_of_text2_4(),
	SavedL6NoteBook_t622742190::get_offset_of_text3_5(),
	SavedL6NoteBook_t622742190::get_offset_of_instance_6(),
	SavedL6NoteBook_t622742190::get_offset_of_m_tNoteBook_7(),
	SavedL6NoteBook_t622742190::get_offset_of_menu_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (SavedL7NoteBook_t74762212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[5] = 
{
	SavedL7NoteBook_t74762212::get_offset_of_m_skin_2(),
	SavedL7NoteBook_t74762212::get_offset_of_instance_3(),
	SavedL7NoteBook_t74762212::get_offset_of_m_tNoteBook_4(),
	SavedL7NoteBook_t74762212::get_offset_of_m_tPicture_5(),
	SavedL7NoteBook_t74762212::get_offset_of_m_tShield_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (ObjectLabel_t1667215479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[9] = 
{
	ObjectLabel_t1667215479::get_offset_of_target_2(),
	ObjectLabel_t1667215479::get_offset_of_offset_3(),
	ObjectLabel_t1667215479::get_offset_of_clampToScreen_4(),
	ObjectLabel_t1667215479::get_offset_of_clampBorderSize_5(),
	ObjectLabel_t1667215479::get_offset_of_useMainCamera_6(),
	ObjectLabel_t1667215479::get_offset_of_cameraToUse_7(),
	ObjectLabel_t1667215479::get_offset_of_cam_8(),
	ObjectLabel_t1667215479::get_offset_of_thisTransform_9(),
	ObjectLabel_t1667215479::get_offset_of_camTransform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (QuiteScreen_t956685441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[7] = 
{
	QuiteScreen_t956685441::get_offset_of_m_skin_2(),
	QuiteScreen_t956685441::get_offset_of_m_sQuiteTexture1_3(),
	QuiteScreen_t956685441::get_offset_of_m_sQuiteTexture2_4(),
	QuiteScreen_t956685441::get_offset_of_m_fStartTime_5(),
	QuiteScreen_t956685441::get_offset_of_m_sQuiteText_6(),
	QuiteScreen_t956685441::get_offset_of_m_sQuiteTexture_7(),
	QuiteScreen_t956685441::get_offset_of_m_bFinalImageDisplayed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (ResultBarchart_t1314512756), -1, sizeof(ResultBarchart_t1314512756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2935[22] = 
{
	ResultBarchart_t1314512756::get_offset_of_m_xmlFilePathTextAsset_2(),
	ResultBarchart_t1314512756::get_offset_of_m_skin_3(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarchartLines_4(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarchartBar_5(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarColours_6(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarGreen_7(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarOrange_8(),
	ResultBarchart_t1314512756::get_offset_of_m_tBarRed_9(),
	ResultBarchart_t1314512756::get_offset_of_m_bShowBarchartText_10(),
	ResultBarchart_t1314512756::get_offset_of_m_talkScene_11(),
	ResultBarchart_t1314512756::get_offset_of_displayText_12(),
	ResultBarchart_t1314512756::get_offset_of_severityScore_13(),
	ResultBarchart_t1314512756::get_offset_of_questionCount_14(),
	ResultBarchart_t1314512756::get_offset_of_scorePercentage_15(),
	ResultBarchart_t1314512756_StaticFields::get_offset_of_m_iLevel1Score_16(),
	ResultBarchart_t1314512756_StaticFields::get_offset_of_m_iLevel4Score_17(),
	ResultBarchart_t1314512756_StaticFields::get_offset_of_m_iLevel7Score_18(),
	ResultBarchart_t1314512756::get_offset_of_m_rBarRect1_19(),
	ResultBarchart_t1314512756::get_offset_of_m_rBarRect4_20(),
	ResultBarchart_t1314512756::get_offset_of_m_rBarRect7_21(),
	ResultBarchart_t1314512756::get_offset_of_barColours_22(),
	ResultBarchart_t1314512756::get_offset_of_moodGraph_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (RockHelp_t2267172338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[5] = 
{
	RockHelp_t2267172338::get_offset_of_bActivated_2(),
	RockHelp_t2267172338::get_offset_of_GUIskin_3(),
	RockHelp_t2267172338::get_offset_of_Text_4(),
	RockHelp_t2267172338::get_offset_of_oGameRock_5(),
	RockHelp_t2267172338::get_offset_of_scriptPushRockMiniGame_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (RockHelpTriger_t2640156150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[1] = 
{
	RockHelpTriger_t2640156150::get_offset_of_m_EagleTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (Settings_t284352370), -1, sizeof(Settings_t284352370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2938[18] = 
{
	Settings_t284352370::get_offset_of_m_bShow_2(),
	Settings_t284352370::get_offset_of_m_skin_3(),
	Settings_t284352370::get_offset_of_m_tBag_4(),
	Settings_t284352370::get_offset_of_m_tItems_5(),
	Settings_t284352370::get_offset_of_m_fMoveSpeed_6(),
	Settings_t284352370_StaticFields::get_offset_of_m_fWidth_7(),
	Settings_t284352370_StaticFields::get_offset_of_m_fHeight_8(),
	Settings_t284352370::get_offset_of_m_fTimer_9(),
	Settings_t284352370::get_offset_of_m_rBag_10(),
	Settings_t284352370::get_offset_of_m_rItemRects_11(),
	Settings_t284352370::get_offset_of_m_vFinalPos_12(),
	Settings_t284352370::get_offset_of_m_bToggleShowItems_13(),
	Settings_t284352370::get_offset_of_m_bMove_14(),
	Settings_t284352370::get_offset_of_m_bNotebookUp_15(),
	Settings_t284352370::get_offset_of_m_ObjectMobileController_16(),
	Settings_t284352370::get_offset_of_bridgeColor_17(),
	Settings_t284352370::get_offset_of_bridgeBackColor_18(),
	Settings_t284352370::get_offset_of_instance_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (SettingsScreen_t3285851604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[21] = 
{
	SettingsScreen_t3285851604::get_offset_of_m_skin_2(),
	SettingsScreen_t3285851604::get_offset_of_m_theGuardian_3(),
	SettingsScreen_t3285851604::get_offset_of_m_theMontor_4(),
	SettingsScreen_t3285851604::get_offset_of_m_mouseCorsorImage_5(),
	SettingsScreen_t3285851604::get_offset_of_m_closeImage_6(),
	SettingsScreen_t3285851604::get_offset_of_m_backPackImage_7(),
	SettingsScreen_t3285851604::get_offset_of_m_UIControl_8(),
	SettingsScreen_t3285851604::get_offset_of_m_ControlImage_9(),
	SettingsScreen_t3285851604::get_offset_of_m_AcelaratorImage_10(),
	SettingsScreen_t3285851604::get_offset_of_loadingIcon_11(),
	SettingsScreen_t3285851604::get_offset_of_m_ButtonOn_12(),
	SettingsScreen_t3285851604::get_offset_of_m_ButtonOff_13(),
	SettingsScreen_t3285851604::get_offset_of_m_gardianClone_14(),
	SettingsScreen_t3285851604::get_offset_of_m_mentorClone_15(),
	SettingsScreen_t3285851604::get_offset_of_m_talkScene_16(),
	SettingsScreen_t3285851604::get_offset_of_m_gGardianObject_17(),
	SettingsScreen_t3285851604::get_offset_of_m_ObjectMobileController_18(),
	SettingsScreen_t3285851604::get_offset_of_m_ObjectMobileControllerMove_19(),
	SettingsScreen_t3285851604::get_offset_of_m_ObjectMobileControllerSprint_20(),
	SettingsScreen_t3285851604::get_offset_of_m_ObjectMobileController_LeftPosition_21(),
	SettingsScreen_t3285851604::get_offset_of_m_ObjectMobileController_RightPosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (ShieldAgaDepression_t775775458), -1, sizeof(ShieldAgaDepression_t775775458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2940[14] = 
{
	ShieldAgaDepression_t775775458::get_offset_of_m_skin_2(),
	ShieldAgaDepression_t775775458::get_offset_of_m_shieldPrf_3(),
	ShieldAgaDepression_t775775458::get_offset_of_m_vShieldPosition_4(),
	ShieldAgaDepression_t775775458::get_offset_of_m_shieldObjectClone_5(),
	ShieldAgaDepression_t775775458_StaticFields::get_offset_of_TextWidth_6(),
	ShieldAgaDepression_t775775458_StaticFields::get_offset_of_TextHeight_7(),
	ShieldAgaDepression_t775775458::get_offset_of_childObjectNames_8(),
	ShieldAgaDepression_t775775458::get_offset_of_shieldNames_9(),
	ShieldAgaDepression_t775775458::get_offset_of_shieldNameRects_10(),
	ShieldAgaDepression_t775775458::get_offset_of_displayName_11(),
	ShieldAgaDepression_t775775458::get_offset_of_displayColor_12(),
	ShieldAgaDepression_t775775458::get_offset_of_colourTexture_13(),
	ShieldAgaDepression_t775775458::get_offset_of_greyScaleTexture_14(),
	ShieldAgaDepression_t775775458::get_offset_of_shieldMen_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (TalkScenes_t1215051795), -1, sizeof(TalkScenes_t1215051795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2941[71] = 
{
	TalkScenes_t1215051795_StaticFields::get_offset_of_instanceRef_2(),
	TalkScenes_t1215051795::get_offset_of_m_currentScene_3(),
	TalkScenes_t1215051795::get_offset_of_tuiScene_4(),
	TalkScenes_t1215051795::get_offset_of_m_bGuardianNeedsLoad_5(),
	TalkScenes_t1215051795::get_offset_of_m_voiceCanStart_6(),
	TalkScenes_t1215051795::get_offset_of_m_bTalkedWithGuardian_7(),
	TalkScenes_t1215051795::get_offset_of_m_bTalkedWithMentor_8(),
	TalkScenes_t1215051795::get_offset_of_m_bL7NeedHelpOptionA_9(),
	TalkScenes_t1215051795::get_offset_of_m_bL7UnNeedHelpOptionB_10(),
	TalkScenes_t1215051795::get_offset_of_m_autoSceneStartTime_11(),
	TalkScenes_t1215051795::get_offset_of_m_bCassFiremanTalkStart_12(),
	TalkScenes_t1215051795::get_offset_of_m_PHQAAnswers_13(),
	TalkScenes_t1215051795::get_offset_of_m_iTestingLevelNum_14(),
	TalkScenes_t1215051795::get_offset_of_m_bChiceSelectedDetector_15(),
	TalkScenes_t1215051795::get_offset_of_m_guideXmlFilePathTextAsset_16(),
	TalkScenes_t1215051795::get_offset_of_m_mentorXMLFilePathTextAsset_17(),
	TalkScenes_t1215051795::get_offset_of_m_hopeXmlFilePathTextAsset_18(),
	TalkScenes_t1215051795::get_offset_of_m_guardianXmlFilePathTextAsset_19(),
	TalkScenes_t1215051795::get_offset_of_m_1npcXmlFilePathTextAsset_20(),
	TalkScenes_t1215051795::get_offset_of_m_travelerXmlFilePathTextAsset_21(),
	TalkScenes_t1215051795::get_offset_of_m_cassXmlFilePathTextAsset_22(),
	TalkScenes_t1215051795::get_offset_of_m_fireXmlFilePathTextAsset_23(),
	TalkScenes_t1215051795::get_offset_of_m_yetiXmlFilePathTextAsset_24(),
	TalkScenes_t1215051795::get_offset_of_m_fireperson2XmlFilePathTextAsset_25(),
	TalkScenes_t1215051795::get_offset_of_m_firespiritXmlFilePathTextAsset_26(),
	TalkScenes_t1215051795::get_offset_of_m_npcXmlFilePathTextAsset_27(),
	TalkScenes_t1215051795::get_offset_of_m_cass2XmlFilePathTextAsset_28(),
	TalkScenes_t1215051795::get_offset_of_m_npc2XmlFilePathTextAsset_29(),
	TalkScenes_t1215051795::get_offset_of_m_darroXmlFilePathTextAsset_30(),
	TalkScenes_t1215051795::get_offset_of_m_gnatsXmlFilePathTextAsset_31(),
	TalkScenes_t1215051795::get_offset_of_m_cass3XmlFilePathTextAsset_32(),
	TalkScenes_t1215051795::get_offset_of_m_sparksXmlFilePathTextAsset_33(),
	TalkScenes_t1215051795::get_offset_of_m_swapguyXmlFilePathTextAsset_34(),
	TalkScenes_t1215051795::get_offset_of_m_ntsXmlFilePathTextAsset_35(),
	TalkScenes_t1215051795::get_offset_of_m_bridgeWomanXmlFilePathTextAsset_36(),
	TalkScenes_t1215051795::get_offset_of_m_templeGuidianXmlFilePathTextAsset_37(),
	TalkScenes_t1215051795::get_offset_of_m_cass4XmlFilePathTextAsset_38(),
	TalkScenes_t1215051795::get_offset_of_m_examinerXmlFilePathTextAsset_39(),
	TalkScenes_t1215051795::get_offset_of_m_swapGuyXmlFilePathTextAsset_40(),
	TalkScenes_t1215051795::get_offset_of_m_playerNeedsHelpTextAsset_41(),
	TalkScenes_t1215051795::get_offset_of_m_cassL7XmlFilePathTextAsset_42(),
	TalkScenes_t1215051795::get_offset_of_m_darroL7XmlFilePathTextAsset_43(),
	TalkScenes_t1215051795::get_offset_of_m_firepersonL7XmlFilePathTextAsset_44(),
	TalkScenes_t1215051795::get_offset_of_m_kogXmlFilePathTextAsset_45(),
	TalkScenes_t1215051795::get_offset_of_m_iPerPHQAQuestionScore_46(),
	TalkScenes_t1215051795::get_offset_of_m_iPHQAQuestionNum_47(),
	TalkScenes_t1215051795::get_offset_of_m_iQ9Score_48(),
	TalkScenes_t1215051795::get_offset_of_m_skin_49(),
	TalkScenes_t1215051795::get_offset_of_m_loadSceneData_50(),
	TalkScenes_t1215051795::get_offset_of_L1AudioFileList_51(),
	TalkScenes_t1215051795::get_offset_of_L2AudioFileList_52(),
	TalkScenes_t1215051795::get_offset_of_L3AudioFileList_53(),
	TalkScenes_t1215051795::get_offset_of_L4AudioFileList_54(),
	TalkScenes_t1215051795::get_offset_of_L5AudioFileList_55(),
	TalkScenes_t1215051795::get_offset_of_L6AudioFileList_56(),
	TalkScenes_t1215051795::get_offset_of_L7AudioFileList_57(),
	TalkScenes_t1215051795::get_offset_of_spatialBlend_58(),
	TalkScenes_t1215051795::get_offset_of_m_characterVoiceAS_59(),
	TalkScenes_t1215051795::get_offset_of_m_bAutoConversationUnFinished_60(),
	TalkScenes_t1215051795::get_offset_of_m_bNPC1AnimationPlayOnce_61(),
	TalkScenes_t1215051795::get_offset_of_m_bNPC2AnimationPlayOnce_62(),
	TalkScenes_t1215051795::get_offset_of_m_bMultiChoice1OptionA_63(),
	TalkScenes_t1215051795::get_offset_of_m_bMultiChoice1OptionB_64(),
	TalkScenes_t1215051795::get_offset_of_m_bMultiChoice1OptionC_65(),
	TalkScenes_t1215051795::get_offset_of_m_bMultiChoice1OptionD_66(),
	TalkScenes_t1215051795::get_offset_of_m_PHQAAnswerIndex_67(),
	TalkScenes_t1215051795::get_offset_of_instance_68(),
	TalkScenes_t1215051795::get_offset_of_m_sTestWholeDynamic_69(),
	TalkScenes_t1215051795::get_offset_of_m_sTestDynamicToken_70(),
	TalkScenes_t1215051795::get_offset_of_m_sTestDynamicURL_71(),
	TalkScenes_t1215051795::get_offset_of_m_BoolSkipVoiceErrorLevel2_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (TalkSceneWithImage_t4268521949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[4] = 
{
	TalkSceneWithImage_t4268521949::get_offset_of_m_skin_2(),
	TalkSceneWithImage_t4268521949::get_offset_of_m_tConversationBackground_3(),
	TalkSceneWithImage_t4268521949::get_offset_of_m_tCharacterFaceTexture_4(),
	TalkSceneWithImage_t4268521949::get_offset_of_m_loadSceneData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (TerminateCassFiremanScene_t2717791514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[18] = 
{
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_firePersonConversationFinished_2(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_interactObject_3(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_cassStartPos_4(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_cassTargetPos_5(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_playerStartPos_6(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_playerTargetPos_7(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fCassFirePersonDistance_8(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fPlayerFirePersonDistance_9(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fCassPlayerDistance_10(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fCassWalkTime_11(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fPlayerWalkTime_12(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fCaseWalkSpeed_13(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_fPlayerWalkSpeed_14(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_bMovingToTarget_15(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_bMovingToPlayerTarget_16(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_bCassReachesTarget_17(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_bPlayerReachesTarget_18(),
	TerminateCassFiremanScene_t2717791514::get_offset_of_m_bCassPlayIdleOnce_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (TerminateTalkScene_t870503985), -1, sizeof(TerminateTalkScene_t870503985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2944[7] = 
{
	TerminateTalkScene_t870503985::get_offset_of_m_bSwapGuyWalkingAway_2(),
	TerminateTalkScene_t870503985::get_offset_of_m_aLevel7CutSceneMusic_3(),
	TerminateTalkScene_t870503985_StaticFields::get_offset_of_interactObject_4(),
	TerminateTalkScene_t870503985::get_offset_of_interactObjectHeadToID_5(),
	TerminateTalkScene_t870503985::get_offset_of_m_bPlayerStayInDark_6(),
	TerminateTalkScene_t870503985::get_offset_of_m_fNPCToPlayerDistance_7(),
	TerminateTalkScene_t870503985::get_offset_of_m_fSetNPCIneteractionBackDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (ThoughsAndFeeling_t705500710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[4] = 
{
	ThoughsAndFeeling_t705500710::get_offset_of_m_skin_2(),
	ThoughsAndFeeling_t705500710::get_offset_of_m_tBarchartBackground_3(),
	ThoughsAndFeeling_t705500710::get_offset_of_m_tThroughAndFeelingTexture_4(),
	ThoughsAndFeeling_t705500710::get_offset_of_obj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (TitleScreen_t1264819243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[1] = 
{
	TitleScreen_t1264819243::get_offset_of_m_skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (TuiCharacterInteraction_t4051810501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (GreetingScreen_t3405050173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[9] = 
{
	GreetingScreen_t3405050173::get_offset_of_m_skin_2(),
	GreetingScreen_t3405050173::get_offset_of_m_bLogoFadeInStart_3(),
	GreetingScreen_t3405050173::get_offset_of_m_fFadeInStartTime_4(),
	GreetingScreen_t3405050173::get_offset_of_m_fLogoStayTime_5(),
	GreetingScreen_t3405050173::get_offset_of_m_bLogoFadeOutStart_6(),
	GreetingScreen_t3405050173::get_offset_of_m_fFadeOutTime_7(),
	GreetingScreen_t3405050173::get_offset_of_m_fFadeInLogoAlpha_8(),
	GreetingScreen_t3405050173::get_offset_of_m_fFadeOutLogoAlpha_9(),
	GreetingScreen_t3405050173::get_offset_of_m_bScriptIsKilled_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (LevelNumScreen_t3100306991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[15] = 
{
	LevelNumScreen_t3100306991::get_offset_of_m_ObjectMobileController_2(),
	LevelNumScreen_t3100306991::get_offset_of_m_ObjectMobileControllerMove_3(),
	LevelNumScreen_t3100306991::get_offset_of_m_ObjectMobileControllerSprint_4(),
	LevelNumScreen_t3100306991::get_offset_of_m_ObjectMobileController_LeftPosition_5(),
	LevelNumScreen_t3100306991::get_offset_of_m_ObjectMobileController_RightPosition_6(),
	LevelNumScreen_t3100306991::get_offset_of_m_skin_7(),
	LevelNumScreen_t3100306991::get_offset_of_m_bLogoFadeInStart_8(),
	LevelNumScreen_t3100306991::get_offset_of_m_fFadeInStartTime_9(),
	LevelNumScreen_t3100306991::get_offset_of_m_fLogoStayTime_10(),
	LevelNumScreen_t3100306991::get_offset_of_m_bLogoFadeOutStart_11(),
	LevelNumScreen_t3100306991::get_offset_of_m_fFadeOutTime_12(),
	LevelNumScreen_t3100306991::get_offset_of_m_fFadeInLogoAlpha_13(),
	LevelNumScreen_t3100306991::get_offset_of_m_fFadeOutLogoAlpha_14(),
	LevelNumScreen_t3100306991::get_offset_of_m_bScriptIsKilled_15(),
	LevelNumScreen_t3100306991::get_offset_of_instance_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (LogoBackgroundScreen_t3640353420), -1, sizeof(LogoBackgroundScreen_t3640353420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2950[24] = 
{
	LogoBackgroundScreen_t3640353420::get_offset_of_m_skin_2(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bUploadingFile_3(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotCheckingFile_4(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotLoginTokenDone_5(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotDynamicTokenDone_6(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotDynamicURLDone_7(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotLoadAvatarDone_8(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bHasLevelNumber_9(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotCharacter_10(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotProgress_11(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotNotebook_12(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotSavingPoint_13(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bSendEventStart_14(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_DObWaiting4Secconds_15(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bWaiting4Secconds_16(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bWaiting4SeccondsDone_17(),
	LogoBackgroundScreen_t3640353420_StaticFields::get_offset_of_levelSelectOverride_18(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bReLogin_19(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bWaitingForReLoginServerDone_20(),
	LogoBackgroundScreen_t3640353420::get_offset_of_m_bGotConfigURLDone_21(),
	LogoBackgroundScreen_t3640353420_StaticFields::get_offset_of_m_levelOverride_22(),
	LogoBackgroundScreen_t3640353420_StaticFields::get_offset_of_m_savePointOverride_23(),
	LogoBackgroundScreen_t3640353420::get_offset_of_instance_24(),
	LogoBackgroundScreen_t3640353420::get_offset_of_instanceTHL_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (U3CrestartApplicationU3Ec__Iterator0_t4049948907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[6] = 
{
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_U3CrunningU3E__0_0(),
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_bDynamicUrlISNULL_1(),
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_U24this_2(),
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_U24current_3(),
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_U24disposing_4(),
	U3CrestartApplicationU3Ec__Iterator0_t4049948907::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (LogosScreen_t3335304941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[10] = 
{
	LogosScreen_t3335304941::get_offset_of_m_skin_2(),
	LogosScreen_t3335304941::get_offset_of_m_bLogoFadeInStart_3(),
	LogosScreen_t3335304941::get_offset_of_m_fFadeInStartTime_4(),
	LogosScreen_t3335304941::get_offset_of_m_fLogoStayTime_5(),
	LogosScreen_t3335304941::get_offset_of_m_bLogoFadeOutStart_6(),
	LogosScreen_t3335304941::get_offset_of_m_fFadeOutTime_7(),
	LogosScreen_t3335304941::get_offset_of_m_fFadeInLogoAlpha_8(),
	LogosScreen_t3335304941::get_offset_of_m_fFadeOutLogoAlpha_9(),
	LogosScreen_t3335304941::get_offset_of_m_bScriptIsKilled_10(),
	LogosScreen_t3335304941::get_offset_of_instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (SparxLogoScreen_t2754749120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[11] = 
{
	SparxLogoScreen_t2754749120::get_offset_of_m_skin_2(),
	SparxLogoScreen_t2754749120::get_offset_of_m_bLogoFadeInStart_3(),
	SparxLogoScreen_t2754749120::get_offset_of_m_fFadeInStartTime_4(),
	SparxLogoScreen_t2754749120::get_offset_of_m_fLogoStayTime_5(),
	SparxLogoScreen_t2754749120::get_offset_of_m_bLogoFadeOutStart_6(),
	SparxLogoScreen_t2754749120::get_offset_of_m_fFadeOutTime_7(),
	SparxLogoScreen_t2754749120::get_offset_of_m_fFadeInLogoAlpha_8(),
	SparxLogoScreen_t2754749120::get_offset_of_m_fFadeOutLogoAlpha_9(),
	SparxLogoScreen_t2754749120::get_offset_of_m_bScriptIsKilled_10(),
	SparxLogoScreen_t2754749120::get_offset_of_m_bGotTokenDone_11(),
	SparxLogoScreen_t2754749120::get_offset_of_instance_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (HopeTalk_t2140284693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[7] = 
{
	HopeTalk_t2140284693::get_offset_of_m_vStartPosition_2(),
	HopeTalk_t2140284693::get_offset_of_m_vTargetPosition_3(),
	HopeTalk_t2140284693::get_offset_of_m_bMovingToTarget_4(),
	HopeTalk_t2140284693::get_offset_of_m_fTuiFlyDistance_5(),
	HopeTalk_t2140284693::get_offset_of_m_fTuiFlySpeed_6(),
	HopeTalk_t2140284693::get_offset_of_time_7(),
	HopeTalk_t2140284693::get_offset_of_m_bPreventTuiTalkAhead_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (L1DetectTuiFlyDown_t2612992016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[5] = 
{
	L1DetectTuiFlyDown_t2612992016::get_offset_of_m_bTuiConversationTriggered_2(),
	L1DetectTuiFlyDown_t2612992016::get_offset_of_m_bFirstConversationFinished_3(),
	L1DetectTuiFlyDown_t2612992016::get_offset_of_m_bSecondConversationFinished_4(),
	L1DetectTuiFlyDown_t2612992016::get_offset_of_m_bPool3MiniGameFinished_5(),
	L1DetectTuiFlyDown_t2612992016::get_offset_of_m_bMiniGameConversationtriggered_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (L2TuiTerminateScene_t3978838945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[6] = 
{
	L2TuiTerminateScene_t3978838945::get_offset_of_tuiAndPlayerDistance_2(),
	L2TuiTerminateScene_t3978838945::get_offset_of_tuiToPlayerDistance_3(),
	L2TuiTerminateScene_t3978838945::get_offset_of_m_bTuiIsFlying_4(),
	L2TuiTerminateScene_t3978838945::get_offset_of_interactObject_5(),
	L2TuiTerminateScene_t3978838945::get_offset_of_tuiFlyHeadToID_6(),
	L2TuiTerminateScene_t3978838945::get_offset_of_m_bTuiNeedTransmit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (L3DetectTuiFlyDown_t1649945521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[4] = 
{
	L3DetectTuiFlyDown_t1649945521::get_offset_of_m_vercanosmallPlayerDis_2(),
	L3DetectTuiFlyDown_t1649945521::get_offset_of_m_tuiNeedtoFlydownDis_3(),
	L3DetectTuiFlyDown_t1649945521::get_offset_of_m_bTuiConversationTriggered_4(),
	L3DetectTuiFlyDown_t1649945521::get_offset_of_m_bFirstConversationFinished_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (L3DetectTuiFlyDownFs_t3516154103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[2] = 
{
	L3DetectTuiFlyDownFs_t3516154103::get_offset_of_m_bTuiConversationTriggered_2(),
	L3DetectTuiFlyDownFs_t3516154103::get_offset_of_m_bFirstConversationFinished_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (L3DetectTuiFlyDownMinigames_t4225375576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[4] = 
{
	L3DetectTuiFlyDownMinigames_t4225375576::get_offset_of_m_vercanosmallPlayerDis_2(),
	L3DetectTuiFlyDownMinigames_t4225375576::get_offset_of_m_tuiNeedtoFlydownDis_3(),
	L3DetectTuiFlyDownMinigames_t4225375576::get_offset_of_m_bTuiConversationTriggered_4(),
	L3DetectTuiFlyDownMinigames_t4225375576::get_offset_of_m_bFirstConversationFinished_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (L3DetectTuiFlyDownnpclava2_t2399048424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	L3DetectTuiFlyDownnpclava2_t2399048424::get_offset_of_m_bTuiConversationTriggered_2(),
	L3DetectTuiFlyDownnpclava2_t2399048424::get_offset_of_m_bFirstConversationFinished_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (L3TuiTerminateScene_t3365052491), -1, sizeof(L3TuiTerminateScene_t3365052491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2961[4] = 
{
	L3TuiTerminateScene_t3365052491::get_offset_of_tuiAndPlayerDistance_2(),
	L3TuiTerminateScene_t3365052491::get_offset_of_interactObject_3(),
	L3TuiTerminateScene_t3365052491::get_offset_of_tuiFlyHeadToID_4(),
	L3TuiTerminateScene_t3365052491_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (L4DetectTuiFlyDown_t822593849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[9] = 
{
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_bFisConversationTriged_2(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_bFisConversationFinished_3(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_bOnCliffConversationTriged_4(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_fCorrectTriggerPlayerDis_5(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_fInCorrectTriggerPlayerDis_6(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_fAllowTrigerDis_7(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_sTuiOnCliffFlyToPoint_8(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_bTuiSratFlying_9(),
	L4DetectTuiFlyDown_t822593849::get_offset_of_m_sTuiSceneName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (L4TuiTerminateScene_t788386278), -1, sizeof(L4TuiTerminateScene_t788386278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2963[4] = 
{
	L4TuiTerminateScene_t788386278::get_offset_of_tuiAndPlayerDistance_2(),
	L4TuiTerminateScene_t788386278::get_offset_of_interactObject_3(),
	L4TuiTerminateScene_t788386278::get_offset_of_tuiFlyHeadToID_4(),
	L4TuiTerminateScene_t788386278_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (L5TuiTerminateScene_t1564783844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[3] = 
{
	L5TuiTerminateScene_t1564783844::get_offset_of_tuiAndPlayerDistance_2(),
	L5TuiTerminateScene_t1564783844::get_offset_of_interactObject_3(),
	L5TuiTerminateScene_t1564783844::get_offset_of_tuiFlyHeadToID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (L6DetectTuiFlyDown_t4267688540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[5] = 
{
	L6DetectTuiFlyDown_t4267688540::get_offset_of_m_fDoorPlayerDis_2(),
	L6DetectTuiFlyDown_t4267688540::get_offset_of_m_tuiNeedtoFlydownDis_3(),
	L6DetectTuiFlyDown_t4267688540::get_offset_of_m_bTuiConversationTriggered_4(),
	L6DetectTuiFlyDown_t4267688540::get_offset_of_m_bFirstConversationFinished_5(),
	L6DetectTuiFlyDown_t4267688540::get_offset_of_m_sTuiFlyToPoint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (L6TuiTempleGuardianSwitch_t4249979332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[1] = 
{
	L6TuiTempleGuardianSwitch_t4249979332::get_offset_of_m_bTuiTalkCanStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (L6TuiTerminateScene_t4160061475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[7] = 
{
	L6TuiTerminateScene_t4160061475::get_offset_of_tuiAndPlayerDistance_2(),
	L6TuiTerminateScene_t4160061475::get_offset_of_interactObject_3(),
	L6TuiTerminateScene_t4160061475::get_offset_of_tuiFlyHeadToID_4(),
	L6TuiTerminateScene_t4160061475::get_offset_of_m_bFirstPartWalkingFinished_5(),
	L6TuiTerminateScene_t4160061475::get_offset_of_m_bSecondpartWalkingFinished_6(),
	L6TuiTerminateScene_t4160061475::get_offset_of_m_bTuiFliedAway_7(),
	L6TuiTerminateScene_t4160061475::get_offset_of_m_bcallOnceTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (L6TuiWomanBridgelandSwitch_t1546825341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[1] = 
{
	L6TuiWomanBridgelandSwitch_t1546825341::get_offset_of_m_bTuiTalkCanStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (L7DetectTuiFlyDown_t382364299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[5] = 
{
	L7DetectTuiFlyDown_t382364299::get_offset_of_m_fDoorPlayerDis_2(),
	L7DetectTuiFlyDown_t382364299::get_offset_of_m_tuiNeedtoFlydownDis_3(),
	L7DetectTuiFlyDown_t382364299::get_offset_of_m_bTuiConversationTriggered_4(),
	L7DetectTuiFlyDown_t382364299::get_offset_of_m_bFirstConversationFinished_5(),
	L7DetectTuiFlyDown_t382364299::get_offset_of_m_sTuiFlyToPoint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (L7TuiTerminateScene_t3027460065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[3] = 
{
	L7TuiTerminateScene_t3027460065::get_offset_of_tuiAndPlayerDistance_2(),
	L7TuiTerminateScene_t3027460065::get_offset_of_interactObject_3(),
	L7TuiTerminateScene_t3027460065::get_offset_of_tuiFlyHeadToID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (TuiTerminateScene_t962315422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[8] = 
{
	TuiTerminateScene_t962315422::get_offset_of_tuiAndPlayerDistance_2(),
	TuiTerminateScene_t962315422::get_offset_of_tuiToPlayerDistance_3(),
	TuiTerminateScene_t962315422::get_offset_of_m_bTuiIsFlying_4(),
	TuiTerminateScene_t962315422::get_offset_of_interactObject_5(),
	TuiTerminateScene_t962315422::get_offset_of_tuiFlyHeadToID_6(),
	TuiTerminateScene_t962315422::get_offset_of_m_bTuiHasToWaitPointOne_7(),
	TuiTerminateScene_t962315422::get_offset_of_m_bTuiFirstStandPointArrived_8(),
	TuiTerminateScene_t962315422::get_offset_of_m_bTuiHasToWaitPointTwo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (CaveFireInteraction_t3365142469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[4] = 
{
	CaveFireInteraction_t3365142469::get_offset_of_MouseOverTexture_2(),
	CaveFireInteraction_t3365142469::get_offset_of_MouseOriTexture_3(),
	CaveFireInteraction_t3365142469::get_offset_of_StartToLight_4(),
	CaveFireInteraction_t3365142469::get_offset_of_Lighted_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (CharacterInteraction_t1587752913), -1, sizeof(CharacterInteraction_t1587752913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2973[10] = 
{
	CharacterInteraction_t1587752913::get_offset_of_InteractionRadius_2(),
	CharacterInteraction_t1587752913::get_offset_of_m_fFaceToFaceTalkDistance_3(),
	CharacterInteraction_t1587752913::get_offset_of_m_fPlayerNPCDistance_4(),
	CharacterInteraction_t1587752913::get_offset_of_IsAutomatic_5(),
	CharacterInteraction_t1587752913::get_offset_of_Interacting_6(),
	CharacterInteraction_t1587752913::get_offset_of_privateInteracted_7(),
	CharacterInteraction_t1587752913::get_offset_of_m_fCassDistance_8(),
	CharacterInteraction_t1587752913::get_offset_of_m_bHopeIsFlyingL2_9(),
	CharacterInteraction_t1587752913::get_offset_of_wasInteracting_10(),
	CharacterInteraction_t1587752913_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (CharacterWalkAway_t961142816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[3] = 
{
	CharacterWalkAway_t961142816::get_offset_of_m_vTargetPosition_2(),
	CharacterWalkAway_t961142816::get_offset_of_m_bMovingToTarget_3(),
	CharacterWalkAway_t961142816::get_offset_of_m_fWalkingSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (ChestInteraction_t173854283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[2] = 
{
	ChestInteraction_t173854283::get_offset_of_radius_2(),
	ChestInteraction_t173854283::get_offset_of_FightGnats_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (DoorTriggerEnter_t3147943931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[1] = 
{
	DoorTriggerEnter_t3147943931::get_offset_of_m_DoorTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (EagleBackToLevelS_t2712814472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[10] = 
{
	EagleBackToLevelS_t2712814472::get_offset_of_FlyTo_2(),
	EagleBackToLevelS_t2712814472::get_offset_of_WaitPos_3(),
	EagleBackToLevelS_t2712814472::get_offset_of_EagleMouseOverTexture_4(),
	EagleBackToLevelS_t2712814472::get_offset_of_EagleMouseOriTexture_5(),
	EagleBackToLevelS_t2712814472::get_offset_of_fadeOutTime_6(),
	EagleBackToLevelS_t2712814472::get_offset_of_eagleCutscene_7(),
	EagleBackToLevelS_t2712814472::get_offset_of_start_8(),
	EagleBackToLevelS_t2712814472::get_offset_of_m_fTime_9(),
	EagleBackToLevelS_t2712814472::get_offset_of_ReadyToLeave_10(),
	EagleBackToLevelS_t2712814472::get_offset_of_PlayerAtEagle_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (EagleFlyToPosition_t189743191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[8] = 
{
	EagleFlyToPosition_t189743191::get_offset_of_EagleFlyCamID_2(),
	EagleFlyToPosition_t189743191::get_offset_of_DownPosition_3(),
	EagleFlyToPosition_t189743191::get_offset_of_FlyAwayPosition_4(),
	EagleFlyToPosition_t189743191::get_offset_of_PlayerPosition_5(),
	EagleFlyToPosition_t189743191::get_offset_of_m_bIsFlyingDown_6(),
	EagleFlyToPosition_t189743191::get_offset_of_EagleEndPos_7(),
	EagleFlyToPosition_t189743191::get_offset_of_ridePos_8(),
	EagleFlyToPosition_t189743191::get_offset_of_hasBeenInitialised_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (EagleInteraction_t711864857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[12] = 
{
	EagleInteraction_t711864857::get_offset_of_StartPosition_2(),
	EagleInteraction_t711864857::get_offset_of_StationaryPosition_3(),
	EagleInteraction_t711864857::get_offset_of_FlyTowards_4(),
	EagleInteraction_t711864857::get_offset_of_m_bInteracted_5(),
	EagleInteraction_t711864857::get_offset_of_m_bPlayerGotOn_6(),
	EagleInteraction_t711864857::get_offset_of_m_bPlayerMoving_7(),
	EagleInteraction_t711864857::get_offset_of_m_fLerpTime_8(),
	EagleInteraction_t711864857::get_offset_of_CameraID_9(),
	EagleInteraction_t711864857::get_offset_of_fadeOutTime_10(),
	EagleInteraction_t711864857::get_offset_of_eagleCutscene_11(),
	EagleInteraction_t711864857::get_offset_of_EagleMouseOverTexture_12(),
	EagleInteraction_t711864857::get_offset_of_EagleMouseOriTexture_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (eagleShader_t2557250954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[3] = 
{
	eagleShader_t2557250954::get_offset_of_shader1_2(),
	eagleShader_t2557250954::get_offset_of_shader2_3(),
	eagleShader_t2557250954::get_offset_of_rend_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (IceCaveDoor_t4167718126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[9] = 
{
	IceCaveDoor_t4167718126::get_offset_of_CameraID_2(),
	IceCaveDoor_t4167718126::get_offset_of_BirdFlyToPos_3(),
	IceCaveDoor_t4167718126::get_offset_of_m_birdStartFlying_4(),
	IceCaveDoor_t4167718126::get_offset_of_m_hopeTalkFinished_5(),
	IceCaveDoor_t4167718126::get_offset_of_m_bGotL2OpenDoorFire_6(),
	IceCaveDoor_t4167718126::get_offset_of_m_bDoorOpenClicked_7(),
	IceCaveDoor_t4167718126::get_offset_of_m_bDoorCanOpen_8(),
	IceCaveDoor_t4167718126::get_offset_of_IceDoorMouseOverTexture_9(),
	IceCaveDoor_t4167718126::get_offset_of_IceDoorMouseOriTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (IceDoorObjMovement_t1143558271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[10] = 
{
	IceDoorObjMovement_t1143558271::get_offset_of_bRotation_2(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_vTarget_3(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_vMovement_4(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_bStartMoving_5(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_vRotation_6(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_bMovementComplete_7(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_vOriginalPosition_8(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_fMovementProgress_9(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_fRotateYValue_10(),
	IceDoorObjMovement_t1143558271::get_offset_of_m_fAngle_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (L7GateCloseTrigger_t558060013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[2] = 
{
	L7GateCloseTrigger_t558060013::get_offset_of_m_bGate1NeedsToClose_2(),
	L7GateCloseTrigger_t558060013::get_offset_of_m_bGate2NeedsToClose_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (LadderInteraction_t560741349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[9] = 
{
	LadderInteraction_t560741349::get_offset_of_fLengthOfLadder_2(),
	LadderInteraction_t560741349::get_offset_of_fPlayerPositionOnLadder_3(),
	LadderInteraction_t560741349::get_offset_of_m_bCorrectLadder_4(),
	LadderInteraction_t560741349::get_offset_of_EndPos_5(),
	LadderInteraction_t560741349::get_offset_of_Trigger_6(),
	LadderInteraction_t560741349::get_offset_of_m_bOnThisLadder_7(),
	LadderInteraction_t560741349::get_offset_of_m_bClimbing_8(),
	LadderInteraction_t560741349::get_offset_of_m_bReachedTop_9(),
	LadderInteraction_t560741349::get_offset_of_moveCap_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (LadderTrigger_t2224323142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[3] = 
{
	LadderTrigger_t2224323142::get_offset_of_MouseOverTexture_2(),
	LadderTrigger_t2224323142::get_offset_of_MouseOriTexture_3(),
	LadderTrigger_t2224323142::get_offset_of_closeEnough_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (LeftDoorTriggerEnter_t863745348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[1] = 
{
	LeftDoorTriggerEnter_t863745348::get_offset_of_m_leftDoorTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (Level5GnatsInteraction_t264916876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[2] = 
{
	Level5GnatsInteraction_t264916876::get_offset_of_Active_2(),
	Level5GnatsInteraction_t264916876::get_offset_of_bPlayedParticle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (Lv5GnatSparxGame_t1320105928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[10] = 
{
	Lv5GnatSparxGame_t1320105928::get_offset_of_bTriggered_2(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_fSpeed_3(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_CameraID_4(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_Trigger_5(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_TargetObj_6(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_FireBall_7(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_Firing_8(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_iCurrentNumOfGnatsShot_9(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_GnatEndPosition_10(),
	Lv5GnatSparxGame_t1320105928::get_offset_of_tempPos_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (LvSixDoor_t3751206300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[3] = 
{
	LvSixDoor_t3751206300::get_offset_of_LeftDoor_2(),
	LvSixDoor_t3751206300::get_offset_of_RightDoor_3(),
	LvSixDoor_t3751206300::get_offset_of_bOpen_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (IceMeltRoutine_t2599549131)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2990[7] = 
{
	IceMeltRoutine_t2599549131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (MeltIceYeti_t3902559005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[9] = 
{
	MeltIceYeti_t3902559005::get_offset_of_MeltStatus_2(),
	MeltIceYeti_t3902559005::get_offset_of_IceParts_3(),
	MeltIceYeti_t3902559005::get_offset_of_ZoneBox_4(),
	MeltIceYeti_t3902559005::get_offset_of_WalkToPosition_5(),
	MeltIceYeti_t3902559005::get_offset_of_FireCount_6(),
	MeltIceYeti_t3902559005::get_offset_of_YetiCameraID_7(),
	MeltIceYeti_t3902559005::get_offset_of_m_bAttackingIce_8(),
	MeltIceYeti_t3902559005::get_offset_of_CurrentAttackingPart_9(),
	MeltIceYeti_t3902559005::get_offset_of_m_bTuiConversationFirst_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (NPCMoveTo_t1450212510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[3] = 
{
	NPCMoveTo_t1450212510::get_offset_of_TargetPosition_2(),
	NPCMoveTo_t1450212510::get_offset_of_IsWalking_3(),
	NPCMoveTo_t1450212510::get_offset_of_ArrivedAtPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (ObjectMovement_t1497131487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[15] = 
{
	ObjectMovement_t1497131487::get_offset_of_bPosition_2(),
	ObjectMovement_t1497131487::get_offset_of_bRotation_3(),
	ObjectMovement_t1497131487::get_offset_of_m_vTarget_4(),
	ObjectMovement_t1497131487::get_offset_of_m_vMovement_5(),
	ObjectMovement_t1497131487::get_offset_of_m_bStartMoving_6(),
	ObjectMovement_t1497131487::get_offset_of_m_vRotation_7(),
	ObjectMovement_t1497131487::get_offset_of_m_vRotationBackwards_8(),
	ObjectMovement_t1497131487::get_offset_of_m_bMovementComplete_9(),
	ObjectMovement_t1497131487::get_offset_of_m_endRotation_10(),
	ObjectMovement_t1497131487::get_offset_of_m_vStartPosition_11(),
	ObjectMovement_t1497131487::get_offset_of_m_vOriginalPosition_12(),
	ObjectMovement_t1497131487::get_offset_of_m_fMovementProgress_13(),
	ObjectMovement_t1497131487::get_offset_of_m_fbackWardMovementProgress_14(),
	ObjectMovement_t1497131487::get_offset_of_m_vRotationElapsed_15(),
	ObjectMovement_t1497131487::get_offset_of_RotationDone_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[7] = 
{
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_go_0(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_finalRotation_1(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_U3CAngleU3E__0_2(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_U24this_3(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_U24current_4(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_U24disposing_5(),
	U3CrotateAsCoroutineU3Ec__Iterator0_t2189652608::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (PortalInteraction_t2393667535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[11] = 
{
	PortalInteraction_t2393667535::get_offset_of_CameraPosition_2(),
	PortalInteraction_t2393667535::get_offset_of_InteractionRadius_3(),
	PortalInteraction_t2393667535::get_offset_of_m_fTransitionStartTime_4(),
	PortalInteraction_t2393667535::get_offset_of_m_fTransitionTime_5(),
	PortalInteraction_t2393667535::get_offset_of_IsAutomatic_6(),
	PortalInteraction_t2393667535::get_offset_of_Interacting_7(),
	PortalInteraction_t2393667535::get_offset_of_m_bTransitionStarts_8(),
	PortalInteraction_t2393667535::get_offset_of_m_playerStartMoving_9(),
	PortalInteraction_t2393667535::get_offset_of_whiteOutStart_10(),
	PortalInteraction_t2393667535::get_offset_of_whiteout_11(),
	PortalInteraction_t2393667535::get_offset_of_newPortalEffect_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (U3CLoadGuideSceneU3Ec__Iterator0_t4090987736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[6] = 
{
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U3CfTimeUntilLoadU3E__0_0(),
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U3CopU3E__0_1(),
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U24this_2(),
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U24current_3(),
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U24disposing_4(),
	U3CLoadGuideSceneU3Ec__Iterator0_t4090987736::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (RightDoorTriggerEnter_t3471759167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[1] = 
{
	RightDoorTriggerEnter_t3471759167::get_offset_of_m_rightDoorTriggerEntered_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (SideDoorTrigger_t550685523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (GnatsGoToPos_t2206653697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[2] = 
{
	GnatsGoToPos_t2206653697::get_offset_of_TargetPos_2(),
	GnatsGoToPos_t2206653697::get_offset_of_m_bFinishedMoving_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
