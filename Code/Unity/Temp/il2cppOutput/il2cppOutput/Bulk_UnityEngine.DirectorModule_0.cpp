﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t2171935445;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t1461289625;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341;
// UnityEngine.Object
struct Object_t692178351;
// System.Char[]
struct CharU5BU5D_t3419619864;
// System.String
struct String_t;
// System.Void
struct Void_t653366341;

extern RuntimeClass* PlayableAsset_t1461289625_il2cpp_TypeInfo_var;
extern const uint32_t PlayableDirector_get_playableAsset_m116093448_MetadataUsageId;
extern RuntimeClass* PlayableGraph_t1490949931_il2cpp_TypeInfo_var;
extern const uint32_t PlayableDirector_get_playableGraph_m958404317_MetadataUsageId;



#ifndef U3CMODULEU3E_T1429447273_H
#define U3CMODULEU3E_T1429447273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1429447273 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1429447273_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T2988314022_H
#define VALUETYPE_T2988314022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2988314022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2988314022_marshaled_com
{
};
#endif // VALUETYPE_T2988314022_H
#ifndef ENUM_T853460045_H
#define ENUM_T853460045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t853460045  : public ValueType_t2988314022
{
public:

public:
};

struct Enum_t853460045_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3419619864* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t853460045_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3419619864* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3419619864** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3419619864* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t853460045_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t853460045_marshaled_com
{
};
#endif // ENUM_T853460045_H
#ifndef BOOLEAN_T3624619635_H
#define BOOLEAN_T3624619635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3624619635 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3624619635, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3624619635_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3624619635_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3624619635_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PROPERTYNAME_T65259757_H
#define PROPERTYNAME_T65259757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t65259757 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t65259757, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T65259757_H
#ifndef DOUBLE_T3752657471_H
#define DOUBLE_T3752657471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3752657471 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3752657471, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3752657471_H
#ifndef VOID_T653366341_H
#define VOID_T653366341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t653366341 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T653366341_H
#ifndef OBJECT_T692178351_H
#define OBJECT_T692178351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t692178351  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t692178351, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t692178351_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t692178351_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t692178351_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T692178351_H
#ifndef PLAYABLEGRAPH_T1490949931_H
#define PLAYABLEGRAPH_T1490949931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1490949931 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1490949931, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1490949931_H
#ifndef DIRECTORWRAPMODE_T1908949908_H
#define DIRECTORWRAPMODE_T1908949908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DirectorWrapMode
struct  DirectorWrapMode_t1908949908 
{
public:
	// System.Int32 UnityEngine.Playables.DirectorWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectorWrapMode_t1908949908, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORWRAPMODE_T1908949908_H
#ifndef SCRIPTABLEOBJECT_T1804531341_H
#define SCRIPTABLEOBJECT_T1804531341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1804531341  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_pinvoke : public Object_t692178351_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1804531341_marshaled_com : public Object_t692178351_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1804531341_H
#ifndef COMPONENT_T1632713610_H
#define COMPONENT_T1632713610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1632713610  : public Object_t692178351
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1632713610_H
#ifndef PLAYABLEASSET_T1461289625_H
#define PLAYABLEASSET_T1461289625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t1461289625  : public ScriptableObject_t1804531341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T1461289625_H
#ifndef BEHAVIOUR_T2850977393_H
#define BEHAVIOUR_T2850977393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2850977393  : public Component_t1632713610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2850977393_H
#ifndef PLAYABLEDIRECTOR_T2171935445_H
#define PLAYABLEDIRECTOR_T2171935445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableDirector
struct  PlayableDirector_t2171935445  : public Behaviour_t2850977393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEDIRECTOR_T2171935445_H



// UnityEngine.ScriptableObject UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()
extern "C"  ScriptableObject_t1804531341 * PlayableDirector_GetPlayableAssetInternal_m4054550197 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)
extern "C"  Object_t692178351 * PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081 (RuntimeObject * __this /* static, unused */, PlayableDirector_t2171935445 * ___self0, PropertyName_t65259757 * ___id1, bool* ___idValid2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableDirector::InternalGetCurrentGraph(UnityEngine.Playables.PlayableGraph&)
extern "C"  void PlayableDirector_InternalGetCurrentGraph_m3548181384 (PlayableDirector_t2171935445 * __this, PlayableGraph_t1490949931 * ___graph0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_InternalGetCurrentGraph(UnityEngine.Playables.PlayableDirector,UnityEngine.Playables.PlayableGraph&)
extern "C"  void PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381 (RuntimeObject * __this /* static, unused */, PlayableDirector_t2171935445 * ___self0, PlayableGraph_t1490949931 * ___graph1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Playables.PlayableAsset UnityEngine.Playables.PlayableDirector::get_playableAsset()
extern "C"  PlayableAsset_t1461289625 * PlayableDirector_get_playableAsset_m116093448 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableDirector_get_playableAsset_m116093448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableAsset_t1461289625 * V_0 = NULL;
	{
		ScriptableObject_t1804531341 * L_0 = PlayableDirector_GetPlayableAssetInternal_m4054550197(__this, /*hidden argument*/NULL);
		V_0 = ((PlayableAsset_t1461289625 *)IsInstClass((RuntimeObject*)L_0, PlayableAsset_t1461289625_il2cpp_TypeInfo_var));
		goto IL_0012;
	}

IL_0012:
	{
		PlayableAsset_t1461289625 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::get_extrapolationMode()
extern "C"  int32_t PlayableDirector_get_extrapolationMode_m4278953694 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*PlayableDirector_get_extrapolationMode_m4278953694_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_get_extrapolationMode_m4278953694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_get_extrapolationMode_m4278953694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::get_extrapolationMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Double UnityEngine.Playables.PlayableDirector::get_time()
extern "C"  double PlayableDirector_get_time_m3319857749 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef double (*PlayableDirector_get_time_m3319857749_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_get_time_m3319857749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_get_time_m3319857749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::get_time()");
	double retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Playables.PlayableDirector::set_time(System.Double)
extern "C"  void PlayableDirector_set_time_m3077340455 (PlayableDirector_t2171935445 * __this, double ___value0, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_set_time_m3077340455_ftn) (PlayableDirector_t2171935445 *, double);
	static PlayableDirector_set_time_m3077340455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_set_time_m3077340455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::set_time(System.Double)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Playables.PlayableDirector::set_initialTime(System.Double)
extern "C"  void PlayableDirector_set_initialTime_m3750088438 (PlayableDirector_t2171935445 * __this, double ___value0, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_set_initialTime_m3750088438_ftn) (PlayableDirector_t2171935445 *, double);
	static PlayableDirector_set_initialTime_m3750088438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_set_initialTime_m3750088438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::set_initialTime(System.Double)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Playables.PlayableDirector::Evaluate()
extern "C"  void PlayableDirector_Evaluate_m1387122786 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Evaluate_m1387122786_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_Evaluate_m1387122786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Evaluate_m1387122786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Evaluate()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.ScriptableObject UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()
extern "C"  ScriptableObject_t1804531341 * PlayableDirector_GetPlayableAssetInternal_m4054550197 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef ScriptableObject_t1804531341 * (*PlayableDirector_GetPlayableAssetInternal_m4054550197_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_GetPlayableAssetInternal_m4054550197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetPlayableAssetInternal_m4054550197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()");
	ScriptableObject_t1804531341 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Playables.PlayableDirector::Play()
extern "C"  void PlayableDirector_Play_m3285847532 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Play_m3285847532_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_Play_m3285847532_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Play_m3285847532_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Play()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Playables.PlayableDirector::Stop()
extern "C"  void PlayableDirector_Stop_m4122710442 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Stop_m4122710442_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_Stop_m4122710442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Stop_m4122710442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Stop()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Playables.PlayableDirector::Pause()
extern "C"  void PlayableDirector_Pause_m366555391 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Pause_m366555391_ftn) (PlayableDirector_t2171935445 *);
	static PlayableDirector_Pause_m366555391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Pause_m366555391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Pause()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::GetReferenceValue(UnityEngine.PropertyName,System.Boolean&)
extern "C"  Object_t692178351 * PlayableDirector_GetReferenceValue_m676566074 (PlayableDirector_t2171935445 * __this, PropertyName_t65259757  ___id0, bool* ___idValid1, const RuntimeMethod* method)
{
	Object_t692178351 * V_0 = NULL;
	{
		bool* L_0 = ___idValid1;
		Object_t692178351 * L_1 = PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081(NULL /*static, unused*/, __this, (&___id0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Object_t692178351 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)
extern "C"  Object_t692178351 * PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081 (RuntimeObject * __this /* static, unused */, PlayableDirector_t2171935445 * ___self0, PropertyName_t65259757 * ___id1, bool* ___idValid2, const RuntimeMethod* method)
{
	typedef Object_t692178351 * (*PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081_ftn) (PlayableDirector_t2171935445 *, PropertyName_t65259757 *, bool*);
	static PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1539492081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)");
	Object_t692178351 * retVal = _il2cpp_icall_func(___self0, ___id1, ___idValid2);
	return retVal;
}
// UnityEngine.Playables.PlayableGraph UnityEngine.Playables.PlayableDirector::get_playableGraph()
extern "C"  PlayableGraph_t1490949931  PlayableDirector_get_playableGraph_m958404317 (PlayableDirector_t2171935445 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableDirector_get_playableGraph_m958404317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableGraph_t1490949931  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableGraph_t1490949931  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableGraph_t1490949931_il2cpp_TypeInfo_var, (&V_0));
		PlayableDirector_InternalGetCurrentGraph_m3548181384(__this, (&V_0), /*hidden argument*/NULL);
		PlayableGraph_t1490949931  L_0 = V_0;
		V_1 = L_0;
		goto IL_0018;
	}

IL_0018:
	{
		PlayableGraph_t1490949931  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableDirector::InternalGetCurrentGraph(UnityEngine.Playables.PlayableGraph&)
extern "C"  void PlayableDirector_InternalGetCurrentGraph_m3548181384 (PlayableDirector_t2171935445 * __this, PlayableGraph_t1490949931 * ___graph0, const RuntimeMethod* method)
{
	{
		PlayableGraph_t1490949931 * L_0 = ___graph0;
		PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_InternalGetCurrentGraph(UnityEngine.Playables.PlayableDirector,UnityEngine.Playables.PlayableGraph&)
extern "C"  void PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381 (RuntimeObject * __this /* static, unused */, PlayableDirector_t2171935445 * ___self0, PlayableGraph_t1490949931 * ___graph1, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381_ftn) (PlayableDirector_t2171935445 *, PlayableGraph_t1490949931 *);
	static PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_INTERNAL_CALL_InternalGetCurrentGraph_m3687672381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_InternalGetCurrentGraph(UnityEngine.Playables.PlayableDirector,UnityEngine.Playables.PlayableGraph&)");
	_il2cpp_icall_func(___self0, ___graph1);
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::GetGenericBinding(UnityEngine.Object)
extern "C"  Object_t692178351 * PlayableDirector_GetGenericBinding_m489271951 (PlayableDirector_t2171935445 * __this, Object_t692178351 * ___key0, const RuntimeMethod* method)
{
	typedef Object_t692178351 * (*PlayableDirector_GetGenericBinding_m489271951_ftn) (PlayableDirector_t2171935445 *, Object_t692178351 *);
	static PlayableDirector_GetGenericBinding_m489271951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetGenericBinding_m489271951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetGenericBinding(UnityEngine.Object)");
	Object_t692178351 * retVal = _il2cpp_icall_func(__this, ___key0);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
