﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTENABLE : MonoBehaviour {

    public void OnEnable()
    {
        Debug.Log("Someone enabled me");
    }

    public void OnDisable()
    {
        Debug.Log("Someone disabled me.");
    }
}
