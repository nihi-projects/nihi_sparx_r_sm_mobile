﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RelaxMuscles : MonoBehaviour {

    enum ERelaxState
    {
        FADING_IN,
        FADING_OUT,
        WAITING,
        DONE
    };

    public Image[]  images;
    public string[] text;
    public float fadeRate = 1.0f;
    public Button nextButton;
    public Text textObject;
    public float waitTime = 0.5f;

    private float currentWaitTime;
    private int currentImage = 0;
    private float currentAlpha = 0f;
    private ERelaxState currentState = ERelaxState.FADING_IN;

    // Use this for initialization
    void Start () {
        currentWaitTime = waitTime;

        for ( int i = 0; i < images.Length; ++i )
        {
            images[i].color = new Color(1f, 1f, 1f, 0f);
        }
        nextButton.gameObject.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
        switch (currentState)
        {
            case ERelaxState.FADING_IN:
                if( currentAlpha < 1.0f )
                {
                    currentAlpha += Time.deltaTime * fadeRate;
                    if (currentAlpha >= 1.0f)
                        currentState = ERelaxState.WAITING;
                }
                break;
            case ERelaxState.WAITING:
                {
                    currentWaitTime -= Time.deltaTime;
                    if( currentWaitTime <= 0f )
                    {
                        currentWaitTime = waitTime;
                        currentState = ERelaxState.FADING_OUT;
                    }
                    
                    break;
                }
            case ERelaxState.FADING_OUT:
                {
                    currentAlpha -= Time.deltaTime * fadeRate;
                    if( currentAlpha <= 0f )
                    {
                        ++currentImage;
                        if( currentImage == images.Length )
                        {
                            currentState = ERelaxState.DONE;
                        }
                        else
                        {
                            currentState = ERelaxState.FADING_IN;
                        }
                    }
                    break;
                }
            case ERelaxState.DONE:
                {
                    nextButton.gameObject.SetActive(true);
                    break;
                }
        }

        if (currentImage < images.Length)
        {
            textObject.text = text[currentImage];
            images[currentImage].color = new Color(1f, 1f, 1f, currentAlpha);
        }
    }
}
