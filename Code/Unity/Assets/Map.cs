﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Province
{
    public Image provinceImage;
    public Text provinceText;
    public ProvinceScript script;

    public void SetAlpha( float _f )
    {
        Color col = provinceImage.color;
        col.a = _f;
        provinceImage.color = col;
        col = provinceText.color;
        col.a = _f;
        provinceText.color = col;
    }

    //This is a really simple unlock animation using a coroutine.
    public IEnumerator Unlock( float _delay, int _level )
    {
        float time = 0f;
        while( time < _delay )
        {
            time += Time.deltaTime;
            yield return false;
        }

        while( provinceImage.color.a < 1.0f )
        {
            provinceImage.color = IncreaseAlpha(provinceImage.color, Time.deltaTime);
            provinceText.color = IncreaseAlpha(provinceText.color, Time.deltaTime);
            yield return false;
        }

        script.EnableInteraction(_level);
    }

    public Color IncreaseAlpha( Color _col, float _amount )
    {
        Color col = _col;
        col.a += _amount;
        return col;
    }
}

public class Map : MonoBehaviour
{
    public Province[] provinces;
    public float lockedAlpha;

    private GameObject controls;

	// Use this for initialization
	void Start () {
        //Lock all the provinces.
		for( int i = 0; i < provinces.Length; ++i )
        {
            provinces[i].SetAlpha(lockedAlpha);
        }

        //Unlock the unlocked provinces.
        for( int i = 0; i < 8; ++i )
        {
            StartCoroutine(provinces[i].Unlock(i * 0.1f, i) );
        }

        controls = GameObject.Find("MobileSingleStickControl");
        if (controls)
            controls.SetActive(false);
    }
	
    //Close the map.
    public void OnMapClose()
    {
        if (controls)
            controls.SetActive(true);
        Destroy(gameObject);
    }
}
