﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseIceTrigger : MonoBehaviour {

    public LevelTwoScript l2S;

    private void OnTriggerEnter(Collider other)
    {
        l2S.CloseIceDoor();
        Destroy(gameObject);
    }
}
