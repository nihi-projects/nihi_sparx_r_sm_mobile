﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListScreen : MonoBehaviour {

    public GameObject       dragAndDropPrefab;
    public Text             title;
    public Text             description;
    public RectTransform    leftWordsSpawner;
    public RectTransform    rightWordsSpawner;

    public float snapDistance = 20f * 20f;
    public RectTransform[]  snapPoints;

    public float ySpacing;

    public GameObject nextButton;
    public bool bReadyToContinue;

    private string[]            descriptions;
    private List<RectTransform> dragAndDrops;

    public void SetTitle( string _str )
    {
        title.text = _str;
    }

    public void SetupListItems(string[] _strings)
    {
        dragAndDrops = new List<RectTransform>();
        for ( int i = 0; i < _strings.Length; ++i )
        {
            GameObject dand = TextDisplayCanvas.instance.ShowPrefab("DragAndDrop");
            dragAndDrops.Add(dand.GetComponent<RectTransform>());
            dand.transform.parent = transform;

            RectTransform rt = dand.GetComponent<RectTransform>();
            if (i % 2 == 0 )
            {
                //spawn on the left side.
                rt.anchoredPosition = leftWordsSpawner.anchoredPosition - new Vector2( 0f, (ySpacing) * (int)(i/2));
            }
            else
            {
                //spawn on the right side.
                rt.anchoredPosition = rightWordsSpawner.anchoredPosition - new Vector2(0f, (ySpacing) * (int)(i/2));
            }
            dand.GetComponentInChildren<Text>().text = _strings[i];
            dand.GetComponentInChildren<DragObject>().callback = snapTransform;
        }
    }

    public void SetupDescriptions(string[] _descriptions)
    {
        descriptions = _descriptions;
    }

    public void SetDescription( string _str )
    {
        description.text = _str;
    }
	
	// Update is called once per frame
	void Update ()
    {
        bReadyToContinue = false;
        for ( int i = 0; i < snapPoints.Length; ++i )
        {
            if( NoOneOccupiesThis( snapPoints[i], null ) == false )
            {
                bReadyToContinue = true;
                break;
            }
        }

        if(descriptions != null && descriptions.Length == dragAndDrops.Count)
        {
            for (int i = 0; i < dragAndDrops.Count; ++i)
            {
                if (dragAndDrops[i].GetComponentInChildren<DragObject>().IsDragging())
                {
                    description.text = descriptions[i];
                }
            }

            nextButton.SetActive(bReadyToContinue);
        }
	}

    //Snaps the transform to a close snapPoint
    public void snapTransform( Transform _dragAndDrop )
    {
        float minDistance = snapDistance;
        int snapPointIndex = -1;

        for( int i = 0; i < snapPoints.Length; ++i )
        {
            Vector3 distance = snapPoints[i].anchoredPosition - _dragAndDrop.GetComponent<RectTransform>().anchoredPosition;
            if( distance.sqrMagnitude < minDistance )
            {
                minDistance = distance.sqrMagnitude;
                snapPointIndex = i;
            }
        }

        _dragAndDrop.GetComponent<DragObject>().snapped = false;
        if ( snapPointIndex != -1 && NoOneOccupiesThis( snapPoints[snapPointIndex], _dragAndDrop ) )
        {
            _dragAndDrop.GetComponent<DragObject>().snapped = true;
            _dragAndDrop.GetComponent<RectTransform>().anchoredPosition = snapPoints[snapPointIndex].anchoredPosition;
        }
    }

    //Returns true if no drag and drop occupies this space.
    public bool NoOneOccupiesThis( RectTransform _rect, Transform _theOneWereChecking )
    {
        if (dragAndDrops == null)
            Debug.Log("List items have not been set up correctly.");

        for( int i = 0; i < dragAndDrops.Count; ++i )
        {
            if (_theOneWereChecking == dragAndDrops[i])
                continue; 

            Vector3 distance = _rect.anchoredPosition - dragAndDrops[i].anchoredPosition;
            if( distance.sqrMagnitude <= snapDistance )
            {
                return false;
            }
        }

        return true;
    }

    public string[] GetChosenStrings()
    {
        List<string> strings = new List<string>();

        for( int i = 0; i < dragAndDrops.Count; ++i )
        {
            if(dragAndDrops[i].GetComponent<DragObject>().snapped == true )
                strings.Add( dragAndDrops[i].GetComponentInChildren<Text>().text );
        }

        string[] stringArray = new string[strings.Count];
        for( int i = 0; i < strings.Count; ++i )
        {
            stringArray[i] = strings[i];
        }

        return stringArray;
    }
}
