﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOutline : MonoBehaviour {

    public OutlineObject outline;

    public void Start()
    {
        if (outline == null)
            Debug.LogError("No outline assigned to this object: " + transform.name);
    }

    void OnTriggerEnter(Collider other)
    {
        if(outline)
            outline.TurnOnOutline();
    }

    void OnTriggerExit(Collider other)
    {
        if(outline)
            outline.TurnOffOutline();
    }
}
