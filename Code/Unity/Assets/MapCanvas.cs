﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapCanvas : MonoBehaviour {

    public GameObject map;
    public Button mapButton;
    public GameObject[] hideWhenCantMove;

    public void Update()
    {
        if(Global.GetPlayer() != null )
        {
            if (Global.GetPlayer() != null)
            {
                mapButton.interactable = Global.GetPlayer().GetComponent<PlayerMovement>().GetMovement();
            }

            foreach (GameObject obj in hideWhenCantMove)
            {
                obj.SetActive(Global.GetPlayer().GetComponent<PlayerMovement>().GetMovement());
            }
        }
    }

    public void OnShowMap()
    {
        GameObject.Instantiate(map, transform);
    }
}
