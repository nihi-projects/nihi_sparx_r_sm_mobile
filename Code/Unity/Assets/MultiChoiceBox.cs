﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EMultiChoice
{
    CHOICE_A,
    CHOICE_B,
    CHOICE_C,
    CHOICE_D,
    CHOICE_MAX
};

public class MultiChoiceBox : MonoBehaviour {

    public Button   nextButton;
    public Button   previousButton;

    public Text     characterName;
    public Text     dialogueText;
    public Text[]   choices;
    public Color    selectedColour;
    public Image    dialogueImage;

    public float    choiceOffest = -12f;

    public bool overrideSelfHide = false;

    [HideInInspector]
    private bool nextClicked = false;
    [HideInInspector]
    private bool previousClicked;

    private bool[] selectedChoices = new bool[(int)(EMultiChoice.CHOICE_MAX)];

    private Color normalChoiceColour;
    private int lastChosen = -1;

	// Use this for initialization
	void Start ()
    {
        gameObject.SetActive(false);
        normalChoiceColour = choices[0].color;
    }

    //called when enabled.
    private void OnEnable()
    {
        overrideSelfHide = false;
        nextButton.gameObject.SetActive(false);
        previousButton.gameObject.SetActive(false);
        dialogueImage.enabled = true;

        for (int i = 0; i < selectedChoices.Length; ++i)
            selectedChoices[i] = false;

        TextDisplayCanvas.instance.textShowing = true;
    }

    public void OnDisable()
    {
        TextDisplayCanvas.instance.textShowing = false;
    }

    //Update function yeah.
    private void Update()
    {
        if (GameObject.Find("GUI") && GameObject.Find("GUI").GetComponent<TalkScenes>().enabled == false && overrideSelfHide == false)
        {
            gameObject.SetActive(false);
        }

        for (int i = 0; i < (int)(EMultiChoice.CHOICE_MAX); ++i )
        {
            if (selectedChoices[i] == true)
                choices[i].color = selectedColour;
            else
                choices[i].color = normalChoiceColour;
        }

        //Selects the multichoice thing.
        if( Global.multiChoice != "" )
        {
            string[] choiceOptions = new string[] { "A", "B", "C", "D" };
            for (int i = 0; i < choiceOptions.Length; ++i )
            {
                if( Global.multiChoice == choiceOptions[i] )
                {
                    selectedChoices[i] = true;

                    if (lastChosen != -1 && choiceOptions[lastChosen] != Global.multiChoice)
                        selectedChoices[lastChosen] = false;

                    lastChosen = i;
                }
            }
        }

        float yPos = choiceOffest;
        for( int i = 0; i < choices.Length; ++i )
        {
            RectTransform rt = choices[i].GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2( rt.anchoredPosition.x, yPos );

            yPos -= choices[i].preferredHeight;
        }
    }

    public int SelectedOption()
    {
        for (int i = 0; i < selectedChoices.Length; ++i)
        {
            if (selectedChoices[i] == true)
                return i;
        }
        return -1;
    }

    //Sets the statement data.
    public void SetStatementData( string _nameText, string _dialogue, Color _titleColour, string _choices = "" )
    {
        characterName.text      = _nameText;
        characterName.color     = _titleColour;
        dialogueText.text       = _dialogue;

        for(int i = 0; i < choices.Length; ++i )
        {
            choices[i].text = _choices;
        }

        if (_nameText == "" && _dialogue == "")
        {
            dialogueImage.enabled = false;
        }
    }

    //Sets the choice on this dialog thing to this thing.
    public void SetChoice( EMultiChoice _choice, string _str )
    {
        if( choices.Length > (int)(_choice))
        {
            choices[(int)(_choice)].text = _str;
        }
        else
        {
            Debug.LogError("Invalid choice! What happened here?");
        }
    }

    //Shows the previous button.
    public void ShowPreviousButton(bool _b)
    {
        previousButton.gameObject.SetActive(_b);
    }

    //Shows the next button.
    public void ShowNextButton(bool _b)
    {
        nextButton.gameObject.SetActive(_b);
    }

    public void ResetSelection()
    {
        for (int i = 0; i < selectedChoices.Length; ++i)
            selectedChoices[i] = false;
        Global.multiChoice = "";
        int g = 0;
    }
    
    public void OnPreviousClicked()
    {
        previousClicked = true;
    }

    //Next clicked callback.
    public void OnNextClicked()
    {
        nextClicked = true;
    }

    //Previous clicked.
    public bool WasPreviousClicked()
    {
        bool returnVal = previousClicked;
        if (previousClicked == true)
            previousClicked = false;
        return returnVal;
    }

    //Returns whether the next button was clicked.
    public bool WasNextClicked()
    {
        bool returnVal = nextClicked;
        if (nextClicked == true)
            nextClicked = false;
        return returnVal;
    }

    public string SelectedLetter()
    {
        string[] letters = new string[] { "A", "B", "C", "D" };
        for (int i = 0; i < selectedChoices.Length; ++i)
        {
            if (selectedChoices[i] == true)
                return letters[i];
        }
        return "";
    }

    public bool IsAnyOptionSelected()
    {
        for( int i = 0; i < (int)(EMultiChoice.CHOICE_MAX); ++i )
        {
            if (IsOptionSelected((EMultiChoice)(i)))
                return true;
        }
        return false;
    }

    //Returns true if an option is selected.
    public bool IsOptionSelected( EMultiChoice _choice )
    {
        int iChoice = (int)_choice;
        bool returnVal = false;
        if(selectedChoices[iChoice] == true )
        {
            returnVal = true;
            //selectedChoices[iChoice] = false;
        }
        return returnVal;
    }

    //This is a bad way to do a callback system, but it will work for now.
    //Ideally I'd implement one callback with a string or EMultiChoice.
    //--------------------------------------------------------------------------------
    public void ASelected()
    {
        OnOptionSelected(EMultiChoice.CHOICE_A);
    }

    public void BSelected()
    {
        OnOptionSelected(EMultiChoice.CHOICE_B);
    }

    public void CSelected()
    {
        OnOptionSelected(EMultiChoice.CHOICE_C);
    }

    public void DSelected()
    {
        OnOptionSelected(EMultiChoice.CHOICE_D);
    }

    public void OnOptionSelected( EMultiChoice _choice )
    {
        if (choices[(int)(_choice)].text != "")
        {
            for (int i = 0; i < selectedChoices.Length; ++i)
                selectedChoices[i] = false;

            selectedChoices[(int)(_choice)] = true;
        }
    }
    //--------------------------------------------------------------------------------
}
