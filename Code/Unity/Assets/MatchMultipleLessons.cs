﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragableInformation
{
    public GameObject   obj;
    public Transform    targetTransform;
    public int          correctAnswer;
    public bool         placedCorrectly;

    public DragableInformation( GameObject _obj, Transform _target )
    {
        obj = _obj;
        targetTransform = _target;
    }
}

public class MatchMultipleLessons : MonoBehaviour {

    public GameObject sentencePrefab;

    public float        gap = 50f;
    public Transform    leftSpawner;
    public Transform    rightSpawner;
    public Transform    howDoesItRelate;
    public Transform    whatsTheMessage;
    public Text         questionText;

    public Button       nextButton;

    private List<DragableInformation> draggables = new List<DragableInformation>();
    private string[]    questions;

    public float        snapDistance = 30f * 30f;
    [HideInInspector]
    public bool         bFinished = false;

    private int         numAnswersIndex = 0;
    private int         currentAnswerIndex = 0 ;
    private bool        overrideDone = false;

    // Use this for initialization
    void Start () {
		
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && Debug.isDebugBuild == true)
            overrideDone = true;

        //Total up the number of correctly placed items.
        List<int> correctAnswers = new List<int>();
        for (int i = 0; i < draggables.Count; ++i)
        {
            if( draggables[i].placedCorrectly )
                correctAnswers.Add(i);
        }
        //If we have two correct things in the right place.
        if( correctAnswers.Count == 2 )
        {
            for( int i = 0; i < correctAnswers.Count; ++i )
            {
                Destroy(draggables[correctAnswers[i]].obj);
                draggables[correctAnswers[i]].placedCorrectly = false;
                draggables[correctAnswers[i]].correctAnswer = 999;
            }
            ++currentAnswerIndex;
        }

        //Check to see if we've finished.
        if(currentAnswerIndex == questions.Length || overrideDone)
        {
            bFinished = true;
        }
        nextButton.gameObject.SetActive(bFinished);

        //Set text of the question.
        if( currentAnswerIndex < questions.Length )
            questionText.text = questions[currentAnswerIndex];
    }

    //Create the phrases that appear around the question.
    public void CreatePhrases( string[] _strHowDoesItRelate, string[] _strWhatsTheMessage )
    {
        CreatePhrases(_strHowDoesItRelate, howDoesItRelate);
        CreatePhrases(_strWhatsTheMessage, whatsTheMessage);
    }

    //Creates a set of phrases.
    public void CreatePhrases( string[] _str, Transform _target )
    {
        for (int i = 0; i < _str.Length; ++i)
        {
            GameObject obj = GameObject.Instantiate(sentencePrefab, transform);
            if (numAnswersIndex % 2 == 0)
                obj.transform.position = leftSpawner.position;
            else
                obj.transform.position = rightSpawner.position;

            obj.GetComponent<DragObject>().callback = SnapThing;
            obj.GetComponentInChildren<Text>().text = _str[i];
            obj.transform.position += new Vector3(0f, -gap * ((int)(numAnswersIndex/2)*2), 0f);
            
            draggables.Add( new DragableInformation( obj, _target )  );
            draggables[numAnswersIndex].correctAnswer = i;

            ++numAnswersIndex;
        }
    }

    public void SetQuestions( string[] _str )
    {
        questions = _str;
    }

    //Snap this thing to a point.
    public void SnapThing(Transform _trans)
    {
        int transIndex = -1;
        for (int i = 0; i < draggables.Count; ++i)
        {
            if (draggables[i].obj != null && draggables[i].obj.transform == _trans)
            {
                transIndex = i;
                break;
            }
        }

        //Check to see if this is a correct answer.
        if (draggables[transIndex].correctAnswer == currentAnswerIndex)
        {
            //Snap it in place etc.
            if ((draggables[transIndex].targetTransform.position - draggables[transIndex].obj.transform.position).sqrMagnitude < snapDistance)
            {
                draggables[transIndex].obj.transform.position = draggables[transIndex].targetTransform.position;
                draggables[transIndex].obj.GetComponent<DragObject>().dragable = false;
                draggables[transIndex].placedCorrectly = true;
            }
        }
    }
}
