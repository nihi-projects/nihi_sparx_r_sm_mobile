﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterTextDialog : MonoBehaviour {

    private int numFields = 1;
    public GameObject fieldPrefab;
    public float fieldGap = 40f;
    public float offset = -100f;
    public Text title;

    private List<GameObject> inputFields = null;

	// Use this for initialization
	void Start ()
    {
        CreateFields();
	}

    public void CreateFields( int _iNumFields = -1 )
    {
        if (_iNumFields != -1)
            SetNumFields(_iNumFields);

        if (inputFields == null)
        {
            inputFields = new List<GameObject>();
            for (int i = numFields-1; i >= 0; --i)
            {
                inputFields.Add(GameObject.Instantiate(fieldPrefab, transform));
            }

            for (int i = 0; i < numFields; ++i)
            {
                float pos = Utils.DistributeEqually(numFields, i, fieldGap);
                inputFields[i].transform.localPosition = new Vector3(0f, pos + offset, 0f);
            }
        }
    }

    public void SetHeadings( string[] _str )
    {
        for( int i = 0; i < numFields; ++i )
        {
            Transform trans = inputFields[(numFields-1)-i].transform.Find("Heading");
            trans.GetComponent<Text>().text = _str[i];
        }
    }

    public void SetNumFields( int _i )
    {
        numFields = _i;
    }

    public string GetText( int _i )
    {
        if (_i < inputFields.Count)
            return inputFields[_i].GetComponent<InputField>().text;
        else
            return "";
    }

    public void SetTitle( string _str )
    {
        title.text = _str;
    }
}
