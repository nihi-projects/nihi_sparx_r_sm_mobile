﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatementBox : MonoBehaviour
{
    public Button previousButton;
    public Button nextButton;

    public Text characterName;
    public Text dialogueText;

    private bool previousClicked;
    private bool nextClicked;
    

    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        previousButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(false);
        TextDisplayCanvas.instance.textShowing = true;
    }

    private void OnDisable()
    {
        TextDisplayCanvas.instance.textShowing = false;
    }

    public void Update()
    {
        //if(Global.GetPlayer())
        //    Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
    }

    public void SetStatementData( string _nameText, string _dialogue , Color _titleColor )
    {
        characterName.text  = _nameText;
        characterName.color = _titleColor;
        dialogueText.text   = _dialogue;

        gameObject.SetActive(true);
    }

    //Shows the previous button.
    public void ShowPreviousButton(bool _b)
    {
        previousButton.gameObject.SetActive(_b);
    }

    //Shows the next button.
    public void ShowNextButton( bool _b)
    {
        nextButton.gameObject.SetActive(_b);
    }

    public void OnPreviousClicked()
    {
        previousClicked = true;
    }

    public void OnNextClicked()
    {
        nextClicked = true;
    }

    //Previous clicked.
    public bool WasPreviousClicked()
    {
        bool returnVal = previousClicked;
        if (previousClicked == true)
            previousClicked = false;
        return returnVal;
    }

    //Next clicked.
    public bool WasNextClicked()
    {
        bool returnVal = nextClicked;
        if (nextClicked == true)
            nextClicked = false;
        return returnVal;
    }
}
