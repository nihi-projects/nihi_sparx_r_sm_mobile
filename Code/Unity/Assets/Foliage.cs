﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foliage : MonoBehaviour {

    public GameObject[] foliageLevels;

	// Use this for initialization
	void Start ()
    {
        foreach (GameObject obj in foliageLevels)
            obj.SetActive(false);

		for( int i = 0; i < Global.CurrentLevelNumber; ++i )
        {
            foliageLevels[i].SetActive(true);
        }
	}
}
