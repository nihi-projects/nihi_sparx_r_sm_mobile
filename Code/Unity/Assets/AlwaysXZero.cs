﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysXZero : MonoBehaviour
{
	// Update is called once per frame
	void Update () {
        transform.eulerAngles = new Vector3( 0f, transform.eulerAngles.y, transform.eulerAngles.z);
	}
}
