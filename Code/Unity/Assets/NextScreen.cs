﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextScreen : MonoBehaviour {

    bool nextClicked = false;
    
    public void OnNextClicked()
    {
        nextClicked = true;
    }

    public bool WasNextClicked()
    {
        if(nextClicked == true)
        {
            nextClicked = false;
            return true;
        }
        return false;
    }
}
