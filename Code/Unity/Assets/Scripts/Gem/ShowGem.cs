using UnityEngine;
using System.Collections;

public class ShowGem : MonoBehaviour 
{
	public GameObject gem = null;
	
	float fTimer = 0.0f;
	
	// Use this for initialization
	void Start () 
	{
		gem.SetActive(false);
		this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (GetComponent<Animation>().IsPlaying("give"))
		{
			fTimer += Time.deltaTime;
			
			if (fTimer >= 1.2f)
			{
				gem.SetActive(true);
				gem.GetComponent<GetGem>().enabled = true;
			}
		}
	}
}
