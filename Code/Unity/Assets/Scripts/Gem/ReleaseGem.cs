using UnityEngine;
using System.Collections;

public class ReleaseGem : MonoBehaviour {

	//Private Variables
	private float m_bReleaseGemRadius = 1.5f;
	private bool m_bReadyToPutGem     = false;
	
	private bool m_bGemCanRelease     = false;
	private bool m_bGemStartsRaising  = false;
	public static bool bagOff		  = false;
	public bool m_bGemRaiseCompleted  = false;

    public GameObject outlineTrigger;

    public ParticleSystem releaseGemEffect;

	public Shader shader1;
	public Shader shader2;
	public Renderer rend;

    private float waitOver = 2.0f;
	
	// Use this for initialization
	void Start () 
	{
		rend = GetComponent<Renderer>();
		shader1 = Shader.Find("Diffuse");
		//shader2 = Shader.Find("Outlined/Silhouetted Diffuse");
		shader2 = Shader.Find("Diffuse");
		string beacon = "beacon" + Global.CurrentLevelNumber;
        if (Global.LevelSVisited == false || !gameObject.name.Contains(beacon))
        {
            Destroy(GetComponent<L5GnatClick>());
        }

        if (outlineTrigger == null)
            Debug.LogError("Outline trigger not set");
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Indicating the beacon to release the Gem
		if(Global.m_bGotCurrentLevelGem && !m_bGemRaiseCompleted){
			EnableGemParticle();
		}
        
        //Trigger pedestal outline thing.
        if( LevelNumBeaconMatch() == true && Global.m_bGotCurrentLevelGem == true )
        {
            outlineTrigger.SetActive(true);
        }
        		
		//Release the Gem to the beacon
		if(LevelNumBeaconMatch() && !m_bGemRaiseCompleted && Global.m_bGotCurrentLevelGem){
			float fDistance = 10.0f;
			rend.material.shader = shader2;
			if(Global.GetPlayer() != null){
				fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
			}
			if(fDistance <= m_bReleaseGemRadius){
				if(!m_bReadyToPutGem){
					if(Input.GetMouseButtonDown(0) && Global.GetMouseToObject() == this.gameObject)
					{
						Vector3 pos = new Vector3(GetBeaconEmptyObj(Global.CurrentLevelNumber).transform.position.x, 
												  Global.GetPlayer().transform.position.y, 
												  GetBeaconEmptyObj(Global.CurrentLevelNumber).transform.position.z);
						//Move the player to the front of the beacon
						Global.GetPlayer().GetComponent<PlayerMovement>().
							MoveToPosition(pos + (GetBeaconEmptyObj(Global.CurrentLevelNumber).transform.forward * (m_bReleaseGemRadius - 0.5f)), 
								GetBeaconEmptyObj(Global.CurrentLevelNumber).transform.position, gameObject.name);
						m_bReleaseGemRadius = 2.0f;	
						m_bReadyToPutGem = true;
						//Global.SnapCameraBasedOnID("_[id]2020" + Global.CurrentLevelNumber.ToString() + "_beconcam0" + Global.CurrentLevelNumber.ToString());
						
						if (Global.CurrentLevelNumber == 1 && Application.loadedLevelName == "LevelS")
						{
							GameObject.Find("Mentor").GetComponent<CharacterInteraction>().enabled = true;
							GameObject.Find("Mentor").GetComponent<CharacterInteraction>().Interacted = false;
							GameObject.Find("Mentor").GetComponent<CharacterInteraction>().Interacting = false;
							GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("MentorScene6");
						}
						GetComponent<L5GnatClick>().CantClick();
					}
				}
				//Player is ready to release the gem
				if(!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition &&
					m_bReadyToPutGem && !m_bGemCanRelease){
					if(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("idle"))
					{	
						Global.GetPlayer().GetComponent<Animation>().Play("take");
						m_bGemCanRelease = true;
						Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "ReleaseGem");

                        CutsceneHolder.instance.gemPlacementCutscene.SetActive(true);
                        if (releaseGemEffect.isPlaying == false)
                            releaseGemEffect.Play();
                    }
				}
			}
			
			//If the player has the Gem and the player finished playing taking animation
			if(m_bGemCanRelease && Global.m_bGotCurrentLevelGem
								&& !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition 
								&& !Global.GetPlayer ().GetComponent<Animation>().IsPlaying("take")){
				GameObject.Find ("Gem" + Global.CurrentLevelNumber).GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find ("Gem" + Global.CurrentLevelNumber).GetComponent<MeshRenderer>().enabled = true;
                GameObject.Find("Gem" + Global.CurrentLevelNumber + " Lights").GetComponent<LightHolder>().EnableLights();
                /*GameObject gemLights = GameObject.Find("Gem" + Global.CurrentLevelNumber + " Lights");
				gemLights.transform.FindChild("point").GetComponent<LightFade>().enabled = true;
				gemLights.transform.FindChild("spot").GetComponent<LightFade>().enabled = true;*/
                m_bGemStartsRaising = true;
				bagOff = true;
				Global.m_bGemStartsRaising = true;
				Destroy(GetComponent<L5GnatClick>());
			}
			
			//If the Gem is fully released
			if(m_bGemStartsRaising && GameObject.Find (GetCurrentLevelGemName()).GetComponent<ObjectMovement>().m_bMovementComplete)
            {
                //Wait a little bit for everything to settle before disabling the cutscene.
                waitOver -= Time.deltaTime;
                if( waitOver <= 0f )
                {
                    //GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
                    EnableGemLighting();
                    DisableGemParticle();
                    SoundPlayer.PlaySound("sfx-gemcollect", 1.0f);
                    m_bGemRaiseCompleted = true;
                    Global.m_bGemIsFullyReleased = true;
                    Global.levelStarted = false;

                    CutsceneHolder.instance.gemPlacementCutscene.SetActive(false);

                    //If finish releasing the last Gem, then play L7 final cutscene
                    if (GetCurrentLevelGemName() == "Gem7")
                    {
                        GameObject.Find("LevelS").GetComponent<L7FinalCutScene>().enabled = true;
                    }
                    else
                    {
                        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "ReleaseGem2");
                    }
                }
			}
		}else{
			rend.material.shader = shader1;
		}
	}
	
	//
	public string GetCurrentLevelGemName()
	{
		string gemName = "";
		switch(Global.CurrentLevelNumber){
			case 1:
				gemName = "Gem1";
				break;
			case 2:
				gemName = "Gem2";
				break;
			case 3:
				gemName = "Gem3";
				break;
			case 4:
				gemName = "Gem4";
				break;
			case 5:
				gemName = "Gem5";
				break;
			case 6:
				gemName = "Gem6";
				break;
			case 7:
				gemName = "Gem7";
				break;
		}
		return gemName;
	}
	
	//This function is used to check whether this is the right beacon for the current level
	public bool LevelNumBeaconMatch()
	{
		bool levelBeaconIsMatch = false;
		
		switch(Global.CurrentLevelNumber){
			case 1:
				if(gameObject.name == "_[id]30011_[aabb]_beacon1") levelBeaconIsMatch = true;
				break;
			case 2:
				if(gameObject.name == "_[id]30012_[aabb]_beacon2") levelBeaconIsMatch = true;
				break;
			case 3:
				if(gameObject.name == "_[id]30013_[aabb]_beacon3") levelBeaconIsMatch = true;
				break;
			case 4:
				if(gameObject.name == "_[id]30014_[aabb]_beacon4") levelBeaconIsMatch = true;
				break;
			case 5:
				if(gameObject.name == "_[id]30015_[aabb]_beacon5") levelBeaconIsMatch = true;
				break;
			case 6:
				if(gameObject.name == "_[id]30016_[aabb]_beacon6") levelBeaconIsMatch = true;
				break;
			case 7:
				if(gameObject.name == "_[id]30017_[aabb]_beacon7") levelBeaconIsMatch = true;
				break;
		}
		
		return levelBeaconIsMatch;
	}
	
	//Enable the lighting of the Gem once the Gem is raised
	public void EnableGemLighting()
	{
		switch(Global.CurrentLevelNumber){
			case 1:
				GameObject.Find ("_[id]30041_lightening1").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 2:
				GameObject.Find ("_[id]30042_lightening2").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 3:
				GameObject.Find ("_[id]30043_lightening3").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 4:
				GameObject.Find ("_[id]30044_lightening4").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 5:
				GameObject.Find ("_[id]30045_lightening5").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 6:
				GameObject.Find ("_[id]30046_lightening6").GetComponent<MeshRenderer>().enabled = true;
				break;
			case 7:
				GameObject.Find ("_[id]30047_lightening7").GetComponent<MeshRenderer>().enabled = true;
				break;
		}
	}
	
	//
	public void EnableGemParticle()
	{
		switch(Global.CurrentLevelNumber){
			case 1:
				//GameObject.Find ("GemActive1 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-violet").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 2:
				//GameObject.Find ("GemActive2 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-blue").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 3:
				//GameObject.Find ("GemActive3 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-red").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 4:
				//GameObject.Find ("GemActive4 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-orange").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 5:
				//GameObject.Find ("GemActive5 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-green").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 6:
				//GameObject.Find ("GemActive6 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-yellow").GetComponent<ParticleSystem>().enableEmission = true;
				break;
			case 7:
				//GameObject.Find ("GemActive7 Particle").GetComponent<ParticleSystem>().enableEmission = true;
				GameObject.Find ("GemParticlesV2-pink").GetComponent<ParticleSystem>().enableEmission = true;
				break;
		}
	}
	
	//
	public void DisableGemParticle()
	{
		switch(Global.CurrentLevelNumber){
			case 1:
				GameObject.Find ("GemActive1 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 2:
				GameObject.Find ("GemActive2 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 3:
				GameObject.Find ("GemActive3 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 4:
				GameObject.Find ("GemActive4 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 5:
				GameObject.Find ("GemActive5 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 6:
				GameObject.Find ("GemActive6 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
			case 7:
				GameObject.Find ("GemActive7 Particle").GetComponent<ParticleSystem>().enableEmission = false;
				break;
		}
	}
	
	//
	public GameObject GetBeaconEmptyObj(int _levelNum)
	{
		GameObject emptyBeaconObj = null;
		
		switch(_levelNum){
			case 1:
				emptyBeaconObj = Global.GetGameObjectByID("30021");
				break;
			case 2:
				emptyBeaconObj = Global.GetGameObjectByID("30022");
				break;
			case 3:
				emptyBeaconObj = Global.GetGameObjectByID("30023");
				break;
			case 4:
				emptyBeaconObj = Global.GetGameObjectByID("30024");
				break;
			case 5:
				emptyBeaconObj = Global.GetGameObjectByID("30025");
				break;
			case 6:
				emptyBeaconObj = Global.GetGameObjectByID("30026");
				break;
			case 7:
				emptyBeaconObj = Global.GetGameObjectByID("30027");
				break;
		}
		
		return emptyBeaconObj;
	}
}
