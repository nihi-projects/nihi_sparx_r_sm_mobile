using UnityEngine;
using System.Collections;

public class GetGem : MonoBehaviour {
	
	//Private Variables
	private bool m_bPlayerStartTakingGem = false;
	
	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if((!GetComponent<ObjectMovement>() || GetComponent<ObjectMovement>().m_bMovementComplete) && 
			!m_bPlayerStartTakingGem){
			Global.GetPlayer().GetComponent<Animation>().Play("take");
			Global.m_bGotCurrentLevelGem = true;
			m_bPlayerStartTakingGem = true;
		}
		//Finished taking the Gem
		if(m_bPlayerStartTakingGem && Global.GetPlayer ().GetComponent<Animation>().IsPlaying("take")){
			GameObject.Destroy(gameObject);
			//Set the camera back after take the Gem
			GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
			
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "GetGem");
			this.enabled = false;
			SoundPlayer.PlaySound("sfx-gemcollect", 0.9f);
			GameObject.Find("Eagle").GetComponent<EagleBackToLevelS>().enabled = true;
			
			if (Global.CurrentLevelNumber == 1)
			{
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene5";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find ("L1GUI").GetComponent<TuiTerminateScene>().enabled = false;
			}
		}
	}
	
	void OnEnable()
	{
		if (GetComponent<ObjectMovement>())
		{
			GetComponent<ObjectMovement>().enabled = true;
			GetComponent<ObjectMovement>().m_bStartMoving = true;
		}
	}
	
	void OnMouseOver()
	{
		if (gameObject.name == "Gem4")
		{
			//Change the cursor image of the mouse
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
	}
	
	void OnMouseExit ()
	{
		if (gameObject.name == "Gem4")
		{
			//Change the cursor image of the mouse
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
	}
}
