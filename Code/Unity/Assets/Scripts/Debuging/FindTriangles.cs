﻿using UnityEngine;
using System.Collections;

public class FindTriangles : MonoBehaviour {

	MeshCollider[] glist;
	void Start ()
	{
		glist = FindObjectsOfType<MeshCollider>();
		foreach(MeshCollider mc in glist)
		{
			if (mc.isTrigger && !mc.convex)
			{
				Debug.Log ("Found 'triggered' mesh: " + mc.name);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
