using UnityEngine;
using System.Collections;

public class Lv7KingOfGnatsFadeIn : MonoBehaviour 
{
	
	//Public variables
	public bool KOGFadeInFinished = false;
	
	//Private variables
	private float KOGAppearTime = 0.0f;
	private float m_fMonsterFadeInAlpha = 0.0f;
	private float fTime = 0.0f;
	private GameObject KOG = null;

	private Color m_cMonsterColor;
	private Color m_cMonsterEyeColor;
	private Color m_cMonsterFurColor;
	
	
	// Use this for initialization
	void Start () 
	{
		KOG = GameObject.Find("KingOfGnatsm");
		m_cMonsterColor = KOG.GetComponent<Renderer>().materials[0].color;
		m_cMonsterEyeColor = KOG.GetComponent<Renderer>().materials[1].color;
		m_cMonsterFurColor = KOG.GetComponent<Renderer>().materials[2].color;
		
		KOG.GetComponent<Renderer>().materials[0].color = new Color(m_cMonsterColor.r, m_cMonsterColor.g, 
													m_cMonsterColor.b, 0.0f);
		KOG.GetComponent<Renderer>().materials[1].color = new Color(m_cMonsterColor.r, m_cMonsterColor.g, 
													m_cMonsterColor.b, 0.0f);
		KOG.GetComponent<Renderer>().materials[2].color = new Color(m_cMonsterColor.r, m_cMonsterColor.g, 
													m_cMonsterColor.b, 0.0f);
		
		this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		fTime += Time.deltaTime * 0.5f;
		m_fMonsterFadeInAlpha = Mathf.Lerp(0.0f, 1.0f, fTime);
		KOG.GetComponent<Renderer>().materials[0].color = new Color(m_cMonsterColor.r, m_cMonsterColor.g, 
													m_cMonsterColor.b, m_fMonsterFadeInAlpha);
		
		KOG.GetComponent<Renderer>().materials[1].color = new Color(m_cMonsterEyeColor.r, m_cMonsterEyeColor.g, 
													m_cMonsterEyeColor.b, m_fMonsterFadeInAlpha);
		
		KOG.GetComponent<Renderer>().materials[2].color = new Color(m_cMonsterFurColor.r, m_cMonsterFurColor.g, 
													m_cMonsterFurColor.b, m_fMonsterFadeInAlpha);
		
		if(m_fMonsterFadeInAlpha == 1.0f){
			KOGFadeInFinished = true;
			Shader invincibleShader = Shader.Find ("Transparent/Cutout/Diffuse");
			GameObject.Find ("KingOfGnatsm").GetComponent<Renderer>().materials[0].shader = invincibleShader;
			this.enabled = false;
		}
	}
	
	public void OnEnable()
	{
		KOGAppearTime = Time.time;
		
		//Get the original color of the KOG
		m_cMonsterColor = GameObject.Find ("KingOfGnatsm").GetComponent<Renderer>().materials[0].color;
		m_cMonsterEyeColor = GameObject.Find ("KingOfGnatsm").GetComponent<Renderer>().materials[1].color;
		m_cMonsterFurColor = GameObject.Find ("KingOfGnatsm").GetComponent<Renderer>().materials[2].color;
	}
}
