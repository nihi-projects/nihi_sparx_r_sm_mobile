using UnityEngine;
using System.Collections;

public enum KOGStatus
{
	Default,
	GnatsGathering,
	KOGForming,
	FireAtPlayer1,
	FireAtPlayer2,
	FireAtPlayer3,
	HopeTalkToPlayer,
	ShieldFadeIn,
	FireAtPlayer4,
	FireAtPlayer5,
	FireAtPlayer6,
	KOGFall,
	ShieldFadeOut,
}

public enum AttackMethod
{
	ChangeGnatToSpark,
	SendGnatOffScreen,
}

public enum KOGAttack
{
	KOGFire,
	GnatFlyingTowards,
	GnatFlyingBack,
	GnatFlyUp,
}

public class Lv7KingOfGnats : MonoBehaviour 
{
	public KOGStatus status = KOGStatus.Default;
	public GameObject KoG;
	public GameObject goGnat;
	public GameObject goSpark;
	public GameObject shield;
	public GameObject gem;
	
	public Transform[] GnatsStayPosition; // the position gnats stay if the 
										  // player missed them.
	public Transform gnatKilledPosition;
	
	GameObject[] AttackingGnats;
	GameObject[] Sparks;
	
	GameObject CurrentGnat;
	
	public string CameraID;
	
	public AttackMethod AttackStrategy;
	
	KOGAttack KingAttack;
	
	Vector3 hitPosition = Vector3.zero;
	
	int m_iNumOfGnatsFiredOut; // the variable that counts the number of 
							   // gnats fired out of the KOG.
	int m_iNumOfGnatsKilled = 0;
	int m_iNumToFire = 0;
	int CurrentEmptySpace = 0;
	
	float fUpCounter = 5.0f;
	float fTime = 0.0f;
	float fStatusTimer = 0.0f;
	
	bool fireNext = false;
	
	// Use this for initialization
	void Start () 
	{
		Sparks = new GameObject[5];
		gem.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		fStatusTimer += Time.deltaTime;
		if (fStatusTimer >= 1.5f)
		{
			switch(status)
			{
			case KOGStatus.Default:
			{
				if (Vector3.Distance(Global.GetPlayer().transform.position, transform.position) < 8)
				{					
					Vector3 moveTo = new Vector3(transform.position.x, Global.GetPlayer().transform.position.y, transform.position.z);
					Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(moveTo + (transform.forward * 5.9f), moveTo);
					if (Vector3.Distance(Global.GetPlayer().transform.position, transform.position) < 6)
					{
						status = KOGStatus.GnatsGathering;
						Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv7KingOfGnats");
						Global.GetPlayer().GetComponent<Animation>().Stop("walk");
						Global.GetPlayer().GetComponent<Animation>().Play("idle");
					}
				}
			}
				break;
			case KOGStatus.GnatsGathering:
				GnatsGather();
				break;
			case KOGStatus.KOGForming:
				Forming();
				break;
			case KOGStatus.FireAtPlayer1:
				FireAtPlayer1();
				break;
			case KOGStatus.FireAtPlayer2:
				FireAtPlayer2();
				break;
			case KOGStatus.FireAtPlayer3:
				FireAtPlayer3();
				break;
			case KOGStatus.HopeTalkToPlayer:
				HopeTalkToPlayer();
				break;
			case KOGStatus.ShieldFadeIn:
				ShieldFadeIn();
				break;
			case KOGStatus.FireAtPlayer4:
				FireAtPlayer4();
				break;
			case KOGStatus.FireAtPlayer5:
				FireAtPlayer5();
				break;
			case KOGStatus.FireAtPlayer6:
				FireAtPlayer6();
				break;
			case KOGStatus.KOGFall:
				KOGFall();
				break;
			case KOGStatus.ShieldFadeOut:
				ShieldFadeOut();
				break;
			default:break;
			}
		}
	}
	
	void GnatsGather()
	{
		Global.SnapCameraBasedOnID(CameraID);
		bool bGnatsGathered = true;
		foreach(Transform child in transform)
		{
			if(child.name == "Gnat")
			{
				child.gameObject.SetActive(true);
				if(!child.gameObject.GetComponent<GnatsGoToPos>().m_bFinishedMoving)
				{
					bGnatsGathered = false;
				}
			}
		}
		if(bGnatsGathered)
		{
			status = KOGStatus.KOGForming;
			KoG.GetComponent<Lv7KingOfGnatsFadeIn>().enabled = true;
		}
	}
	
	void Forming()
	{
		if(KoG.GetComponent<Lv7KingOfGnatsFadeIn>().KOGFadeInFinished)
		{
			foreach(Transform child in transform)
			{
				if(child.name == "Gnat")
				{
					Destroy(child.gameObject);
				}
			}
			status = KOGStatus.FireAtPlayer1;
			m_iNumToFire = 2;
			AttackStrategy = AttackMethod.SendGnatOffScreen;
			AttackingGnats = new GameObject[5];
			KingAttack = KOGAttack.KOGFire;
		}
	}
	
	void GnatsAttack()
	{
		if (Global.GetPlayerEffect().HasTarget())
		{
			if(Global.GetPlayerEffect().FireBallHitObject())
			{
				hitPosition = CurrentGnat.transform.position;
					
				if (AttackStrategy == AttackMethod.SendGnatOffScreen)
				{
					KingAttack = KOGAttack.GnatFlyUp;
				}
				else
				{
					Destroy(CurrentGnat);
					GameObject temp = (GameObject)Instantiate(goSpark, hitPosition, Quaternion.identity);
					for (int i = 0; i < Sparks.Length; ++i)
					{
						if (Sparks[i] == null)
						{
							temp.name = "Spark" + i.ToString();
							temp.transform.parent = transform;
							Sparks[i] = CurrentGnat = temp;
							KingAttack = KOGAttack.GnatFlyingBack;
							break;
						}
					}
				}
				fTime = 0.0f;
				++m_iNumOfGnatsKilled;
			}
		}
		
		// if the correct amount has been fired
		if(CurrentGnat == null && m_iNumOfGnatsFiredOut == m_iNumToFire)
		{
			// if all the gnats have been killed change status
			if (m_iNumOfGnatsKilled == m_iNumToFire)
			{
				bool hasSparks = false;
				Vector3 target = GameObject.Find("KOGBip Neck").transform.position;
				for (int i = 0; i < Sparks.Length; ++i)
				{
					if (Sparks[i] != null)
					{
						Sparks[i].transform.position = Vector3.Lerp(GnatsStayPosition[i].position, target, fTime);
						hasSparks = true;
					}
				}
				if (hasSparks)
				{
					fTime += Time.deltaTime;
				
					if (fTime >= 1.0f)
					{
						status = (KOGStatus)((int)status + 1);
						m_iNumOfGnatsFiredOut = 0;
						m_iNumToFire = 0;
						m_iNumOfGnatsKilled = 0;
						fStatusTimer = 0.0f;
						fTime = 0.0f;
						fStatusTimer = 0.5f;
						
						this.GetComponent<Animation>().Play ("hit");
						SoundPlayer.PlaySound("KOGHit", 1.0f);
						
						for (int i = 0; i < Sparks.Length; ++i)
						{
							if (Sparks[i] != null)
							{
								Destroy(Sparks[i]);
							}
						}
						// play the hurt animation
					}
				}
				else
				{
					status = (KOGStatus)((int)status + 1);
					m_iNumOfGnatsFiredOut = 0;
					m_iNumToFire = 0;
					m_iNumOfGnatsKilled = 0;
					fStatusTimer = 0.0f;
				}
			}
			else
			{
				for (int i = 0; i < AttackingGnats.Length; ++i)
				{
					if (AttackingGnats[i] != null)
					{
						CurrentGnat = AttackingGnats[i];
						KingAttack = KOGAttack.GnatFlyingTowards;
						hitPosition = CurrentGnat.transform.position;
						break;
					}
				}
			}
			
			return;
		}
		
		switch (KingAttack)
		{
		case KOGAttack.KOGFire:
		{
			if (!GetComponent<Animation>().IsPlaying("shoot"))
			{
				this.GetComponent<Animation>().Play ("shoot");
				SoundPlayer.PlaySound("KOGShoot", 1.0f);
			}
			
			fTime += Time.deltaTime;
			if (fTime >= 0.5f)
			{
				GameObject temp = (GameObject)Instantiate(goGnat, GameObject.Find("KOGBip R Hand").transform.position, Quaternion.identity);
				temp.name = "FiredGnat" + m_iNumOfGnatsFiredOut.ToString();
				temp.transform.parent = transform;
				AttackingGnats[m_iNumOfGnatsFiredOut] = CurrentGnat = temp;
				hitPosition = CurrentGnat.transform.position;
				KingAttack = KOGAttack.GnatFlyingTowards;
				++m_iNumOfGnatsFiredOut;
				fTime = 0.0f;
			}
		}
			break;
		case KOGAttack.GnatFlyingTowards:
		{
			fTime += Time.deltaTime;
			if (AttackStrategy != AttackMethod.ChangeGnatToSpark)
			{
				CurrentGnat.transform.position = Vector3.Lerp(hitPosition, 
														  Global.GetPlayer().transform.position, fTime);
			}
			else
			{
				CurrentGnat.transform.position = Vector3.Lerp(hitPosition, 
														 shield.transform.position, fTime);
			}
			
			if (fTime >= 1.0f)
			{
				KingAttack = KOGAttack.GnatFlyingBack;
				hitPosition = CurrentGnat.transform.position;
				if (AttackStrategy != AttackMethod.ChangeGnatToSpark)
				{
					Global.GetPlayer().GetComponent<Animation>().Play("aim hit");
				}
				fTime = 0.0f;
			}
			
			if (Input.GetMouseButton(0))
			{
				GameObject hit = Global.GetMouseToObject();
				
				if (hit == CurrentGnat)
				{
					Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
					SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
					Global.GetPlayerEffect ().SetFireBallMoveTo(CurrentGnat);
				}
			}
			
		}
			break;
		case KOGAttack.GnatFlyingBack:
		{
			fTime += Time.deltaTime;
			bool found = false;
			for (int i = 0; i < AttackingGnats.Length; ++i)
			{
				if (CurrentGnat == AttackingGnats[i])
				{
					CurrentGnat.transform.position = Vector3.Lerp(hitPosition,
																  GnatsStayPosition[i].position, 
																  fTime);
					found = true;
					break;
				}
			}
			if (found == false)
			{
				for (int i = 0; i < Sparks.Length; ++i)
				{
					if (CurrentGnat == Sparks[i])
					{
						CurrentGnat.transform.position = Vector3.Lerp(hitPosition,
																	  GnatsStayPosition[i].position, 
																	  fTime);
						break;
					}
				}
			}
			
			if (fTime >= 1.0f)
			{
				fireNext = true;
				fTime = 0.0f;
				CurrentGnat = null;
				KingAttack = KOGAttack.KOGFire;
			}
		}
			break;
		case KOGAttack.GnatFlyUp:
		{
			fTime += Time.deltaTime;
			CurrentGnat.transform.position = Vector3.Lerp(hitPosition,
														  gnatKilledPosition.transform.position,
														  fTime);
			if (fTime >= 1.0f)
			{
				fireNext = true;
				fTime = 0.0f;
				Destroy(CurrentGnat);
				CurrentGnat = null;
				KingAttack = KOGAttack.KOGFire;
			}
		}
			break;
		};		
	}
	
	void FireAtPlayer1()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 2;
		}
		
		// Processes Gnats Attack.
		GnatsAttack();
	}
	
	void FireAtPlayer2()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 3;
		}
		
		// Processes Gnats Attack.
		GnatsAttack();
	
	}
	
	void FireAtPlayer3()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 5;
		}
		
		GnatsAttack();
	}
	
	void HopeTalkToPlayer()
	{
		//Hope talk starts here
		GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37121");
		Global.CurrentInteractNPC = "Hope";
		GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
		GameObject.Find ("L7GUI").GetComponent<L7TuiTerminateScene>().enabled = false;
		GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7HopeScene5";
		
		status = KOGStatus.ShieldFadeIn;
	}
	
	void ShieldFadeIn()
	{		
		fTime += Time.deltaTime;
		float alpha = Mathf.Lerp(0.0f, 1.0f, fTime);
		shield.GetComponent<Renderer>().materials[0].color = new Color(0.5f, 0.5f, 0.5f, alpha);  
		
		if (fTime >= 1.0f)
		{
			AttackStrategy = AttackMethod.ChangeGnatToSpark;
		}
	}
	
	void FireAtPlayer4()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 2;
		}
		
		GnatsAttack();
	}
	
	void FireAtPlayer5()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 3;
		}
		
		GnatsAttack();
	}
	
	void FireAtPlayer6()
	{
		if (m_iNumToFire == 0)
		{
			m_iNumToFire = 5;
		}
		
		GnatsAttack();
	}
	
	void KOGFall()
	{
		if (fTime == 0.0f)
		{
			this.GetComponent<Animation>().Play("sit");
			SoundPlayer.PlaySound("KOGSit", 1.0f);
		}
		
		ShieldFadeOut();
	}
	
	void ShieldFadeOut()
	{
		fTime += Time.deltaTime;
		float alpha = Mathf.Lerp(1.0f, 0.0f, fTime);
		shield.GetComponent<Renderer>().materials[0].color = new Color(0.5f, 0.5f, 0.5f, alpha);  
		
		if (fTime >= 1.0f)
		{
			GetComponent<CharacterInteraction>().enabled = true;
			GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "Lv7KingOfGnats2");
			gem.SetActive(true);
			this.enabled = false;
		}
	}
	
	public void SetStatus(KOGStatus _status)
	{
		status = _status;
	}
}
