using UnityEngine;
using System.Collections;

public class GnatsGoToPos : MonoBehaviour {
	
	public Transform TargetPos;
	public bool m_bFinishedMoving = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp (transform.position,	
										   TargetPos.position,
											Time.deltaTime);
		
		if(Vector3.Distance(transform.position,
							TargetPos.position) <= 0.1f)
		{
			m_bFinishedMoving = true;
		}
	}
}
