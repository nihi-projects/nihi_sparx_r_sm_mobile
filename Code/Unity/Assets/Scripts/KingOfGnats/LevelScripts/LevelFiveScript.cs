using UnityEngine;
using System.Collections;

public class LevelFiveScript : MonoBehaviour {
	
	public bool bStartDoorOpen = false;
	// Use this for initialization
	void Start () {
		GameObject.Find ("Conversation").GetComponent<GUIText>().material.color = Color.black;
		GameObject.Find ("Gem").transform.position = Global.GetObjectPositionByID("35036");
	}
	
	// Update is called once per frame
	void Update () {
		if(bStartDoorOpen)
		{
			Global.GetGameObjectByID("35120").GetComponent<ObjectMovement>().m_bStartMoving = true;
			if(Global.GetGameObjectByID("35120").GetComponent<ObjectMovement>().m_bMovementComplete)
			{
				// Start Moving the character.
				
				GameObject.Find ("Swampguy").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find ("Swampguy").GetComponent<Animation>().Play("walk");
				bStartDoorOpen = false;
			}
		}
		else
		{
			if(GameObject.Find ("Swampguy").GetComponent<ObjectMovement>().m_bMovementComplete)
			{
				GameObject.Find("Swampguy").GetComponent<Animation>().Play ("idle");
			}
		}
	}
	
	// lift up the stone steps based on the index.
	void LiftUpStoneStep(int index)
	{
		Global.GetGameObjectByID("_rock" + index.ToString())
		.GetComponent<ObjectMovement>().m_bStartMoving = true;
	}
}
