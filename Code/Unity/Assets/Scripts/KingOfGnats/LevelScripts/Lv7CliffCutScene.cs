using UnityEngine;
using System.Collections;

public enum Lv7CliffScene
{
	WaitToStart,
	MoveToRock,
	MovingToRock,
	SparxFlyAtRock,
	RockFall,
	PlayerClimbDown1,
	TalkToTui,
	PlayerClimbDown2,
	End,
	
}

public class Lv7CliffCutScene : MonoBehaviour {
	
	public GameObject[] Sparks;
	public Transform[] SparksPosition;
	public Lv7CliffScene SceneStatus = Lv7CliffScene.WaitToStart;  
	public Transform[] TopLadderPos;
	public Transform[] BottomLadderPos;
	
	private bool m_hopeTalkFinished = false;
	private float m_fTime            = 0.0f;
	public Vector3 start            = Vector3.zero;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Global.GetPlayer() == null)
            return;

		//Start moving the Rock-----------------------------------------------------------------------------------------
		if(SceneStatus == Lv7CliffScene.MoveToRock){
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv7CliffCutScene");
            // 37035 - Rock
            // InFrontOfRock - player position
            Vector3 RockPos = GameObject.Find("LadderRock").transform.position;
			Vector3 PlayerPos = Global.GetObjectPositionByID("InFrontOfRock");
			
			Global.GetPlayer ().GetComponent<PlayerMovement>().MoveToPosition(PlayerPos,RockPos);
			
			SceneStatus = Lv7CliffScene.MovingToRock;
		}
		
		//Spark go inside of the Rock-----------------------------------------------------------------------------------
		if(!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition && SceneStatus == Lv7CliffScene.MovingToRock){
			//This point is just for Tui fly optimization
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37012");
			SceneStatus = Lv7CliffScene.SparxFlyAtRock;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv7CliffCutScene2");
			for(uint i = 0; i < Sparks.Length; ++i)
			{
				Sparks[i].transform.position = SparksPosition[i].position;
				Sparks[i].SetActive(true);
				Sparks[i].GetComponent<ObjectMovement>().enabled = true;
			}
			Global.SnapCameraBasedOnID("27004");
		}
		
		//The Rock start moving down-----------------------------------------------------------------------------------
		if(SceneStatus == Lv7CliffScene.SparxFlyAtRock){
			bool bFinished = true;
			foreach(GameObject Spark in Sparks){
				if(!Spark.GetComponent<ObjectMovement>().m_bMovementComplete){
					bFinished = false;
				}
			}	
			
			if(bFinished){
				foreach(GameObject Spark in Sparks){
					Spark.SetActive(false);
				}
				
				SceneStatus = Lv7CliffScene.RockFall;
				GameObject.Find("LadderRock").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("LadderRock").GetComponent<Animation>().Play();
			}
		}
		
		//Rock finish moving and the player ready to clim down-----------------------------------------------------------
		if(SceneStatus == Lv7CliffScene.RockFall && GameObject.Find("LadderRock").GetComponent<ObjectMovement>().m_bMovementComplete)
		{
			SceneStatus = Lv7CliffScene.PlayerClimbDown1;
			Global.GetPlayer().transform.position = TopLadderPos[0].position + new Vector3(0.0f, 1.0f, 0.0f);
			Global.GetPlayer().transform.rotation = TopLadderPos[0].rotation;
			Global.GetPlayer().GetComponent<Animation>().Play("climb");
			start = Global.GetPlayer().transform.position;
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			GameObject.Find("LadderRock").SetActive(false);
		}
		
		//The player climb on the lidder 1--------------------------------------------------------------------------------
		if(SceneStatus == Lv7CliffScene.PlayerClimbDown1){
			
			m_fTime += Time.deltaTime * 0.35f; 
			// LadderPos2 = Player climb down pos
			Global.GetPlayer().transform.position = Vector3.Lerp(start, Global.GetObjectPositionByID("LadderPos2"), m_fTime);
			Global.GetPlayer().GetComponent<Animation>().Play("climb");
			SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
			if(m_fTime >= 1.0f){
				//Tui fly down and talk
				SceneStatus = Lv7CliffScene.TalkToTui;
				Global.GetPlayer().GetComponent<Animation>().Play("idle");
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37044");
			}
		}
		
		//Tui starts talk when it is arrived
		if(SceneStatus == Lv7CliffScene.TalkToTui && !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
			if(!m_hopeTalkFinished){
                if(Global.CurrentInteractNPC != "Hope")
				    Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled            = false;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled            = true;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene  = "L7HopeScene2";
				GameObject.Find ("L7GUI").GetComponent<L7TuiTerminateScene>().enabled = false;
				
				m_fTime = 0.0f;
				m_hopeTalkFinished = true;
			}
		}
		
		//The player climb on the lidder 2--------------------------------------------------------------------------------
		if(SceneStatus == Lv7CliffScene.PlayerClimbDown2)
        {
            //if(m_fTime == 0f)
            Global.GetPlayer().GetComponent<Animation>().Play("climb");
            //Global.GetPlayer().GetComponent<Animation>()["climb"].speed = 1.0f;

            Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv7CliffCutScene3");
            m_fTime += Time.deltaTime * 0.35f; 
			Global.GetPlayer ().transform.position = Vector3.Lerp (start, Global.GetObjectPositionByID("LadderPos4"), m_fTime);
			
			SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
			if(m_fTime > 1.0f){
				Global.GetPlayer ().GetComponent<Animation>().Play ("idle");
				Global.GetPlayer ().transform.position = Global.GetObjectPositionByID("LadderEndPosition");
				Global.GetPlayer ().transform.rotation = Global.GetGameObjectByID("LadderEndPosition").transform.rotation;
				SceneStatus = Lv7CliffScene.End;
                Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "Lv7CliffCutScene4");

            }
		}
	}
}
