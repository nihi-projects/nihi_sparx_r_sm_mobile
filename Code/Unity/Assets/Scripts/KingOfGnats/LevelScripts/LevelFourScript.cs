using UnityEngine;
using System.Collections;

public class LevelFourScript : MonoBehaviour {

	bool m_bGnatsDefeated = false;
	static public float PlayerHeight; 
	static public float m_fClimbSpeed = 3.0f;
	// Use this for initialization
	void Start () {
		PlayerHeight = Global.GetPlayer().GetComponent<Collider>().bounds.extents.y * 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if(!m_bGnatsDefeated)
		{
			if(!GameObject.Find ("FightGnats"))
			{
				m_bGnatsDefeated = true;
				GameObject.Find ("Darro").GetComponent<Animation>().Play ("idle");
			}
		}
	}
}
