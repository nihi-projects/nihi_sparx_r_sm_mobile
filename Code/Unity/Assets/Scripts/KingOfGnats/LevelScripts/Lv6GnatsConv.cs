using UnityEngine;
using System.Collections;

public class Lv6GnatsConv : MonoBehaviour {

	bool bActive = false;
	bool bAnsweredCorrectly = false;
	Vector3 FlyAwayPos;
	// Use this for initialization
	void Start () {
		FlyAwayPos = transform.Find("Gnat").position;
		FlyAwayPos.x += 6.0f;
		FlyAwayPos.y += 6.0f;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		Transform Gnat = transform.Find ("Gnat");
		
		if(false == bActive)
		{
			if(Vector3.Distance(Global.GetPlayer ().transform.position,
								Gnat.transform.position) <= 2.0f)
			{
				bActive = true;
				Gnat.GetComponent<DisplayQuestion>().enabled = true;
				Gnat.GetComponent<DisplayQuestion>().Activate();
				Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(false, "Lv6GnatsConv");
				Global.GetPlayer().GetComponent<Animation>().Play ("idle");
			}
		}
		else
		{
			if(Gnat.GetComponent<DisplayQuestion>().bFinishedAnswering && !bAnsweredCorrectly)
			{
				// Decide on what to do with spark and gnat.
				if(Gnat.GetComponent<DisplayQuestion>().bCorrectAnswer)
				{
					// Change Gnat to spark.
					Vector3 pos = transform.Find ("Spark").position;
					transform.Find ("Spark").position = Gnat.position;
					Gnat.position = pos;
					bAnsweredCorrectly = true;
				}
				else	
				{
					// Reset Gnat
				}
			}
		}
		
		if(bAnsweredCorrectly)
		{
			transform.Find("Spark").position = Vector3.Lerp (transform.Find("Spark").position, FlyAwayPos, Time.deltaTime);
			if(Vector3.Distance(transform.Find("Spark").position, FlyAwayPos) <= 2.0f)
			{
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
				Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(true, "Lv6GnatsConv2");
				this.enabled = false;
			}
		}
	}
}
