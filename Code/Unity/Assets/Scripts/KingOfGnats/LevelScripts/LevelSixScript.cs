using UnityEngine;
using System.Collections;
public enum Lv6OpenChestCutScene
{
	WaitingToStart,
	ChestOpen,
	GemRiseUp,
	PlayerMoveToPos,
	PlayerGrabGem,
	GemDisappear,
	Finished
}
public class LevelSixScript : MonoBehaviour {
	Lv6OpenChestCutScene cutscene = Lv6OpenChestCutScene.WaitingToStart;
	public GameObject Trigger;
	public bool DoneWithGuard = false;
	bool m_bTriggerFirstDialog = false;
	public GameObject NPC1; 
	public GameObject[] MiniGames;
	public GameObject[] Doors;
	public int iCurrentMiniGameIndex;
	public GameObject[] MiniGameTriggers;
	bool bCheckForPlayingGame = false;
	
	public GameObject MiniGame;
	public GameObject Eagle;
	public GameObject Gem;
	bool m_bPlayerInfrontOfChest = false;
	// Use this for initialization
	void Start () {
		GameObject.Find ("Conversation").GetComponent<GUIText>().material.color = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		if(!m_bTriggerFirstDialog)
		{
			if(Trigger.GetComponent<Collider>().bounds.
			   Contains(Global.GetPlayer ().transform.position))
			{
				m_bTriggerFirstDialog = true;
				Global.GetPlayer().
				GetComponent<PlayerMovement>().
				MoveToPosition(Global.GetObjectPositionByID("36006"),
						NPC1.transform.position);
			}
		}
		if(0 == iCurrentMiniGameIndex)
		{
			if(MiniGames[iCurrentMiniGameIndex].GetComponent<JigSawPuzzle>().bSolved && MiniGames[iCurrentMiniGameIndex].activeSelf == false )
			{
				Doors[0].GetComponent<LvSixDoor>().bOpen = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "LevelSixScript");
				iCurrentMiniGameIndex += 1;
				//Eagle.SetActive(false);
			}
		}
		if(1 == iCurrentMiniGameIndex)
		{
			if(MiniGames[iCurrentMiniGameIndex].GetComponent<ThreeWordGame>().IsDone())
			{
				Doors[1].GetComponent<LvSixDoor>().bOpen = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSixScript2");
				iCurrentMiniGameIndex += 1;
			}
		}
		if(2 == iCurrentMiniGameIndex)
		{
			if(MiniGames[iCurrentMiniGameIndex].GetComponent<JigSawPuzzleWord>().m_bSolved && MiniGames[iCurrentMiniGameIndex].activeSelf == false)
			{
				Doors[2].GetComponent<LvSixDoor>().bOpen = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSixScript3");
				iCurrentMiniGameIndex += 1;
			}
		}
		if(3 == iCurrentMiniGameIndex)
		{
			if(MiniGames[iCurrentMiniGameIndex].GetComponent<WordSearchPuzzle>().allclear && MiniGames[iCurrentMiniGameIndex].activeSelf == false )
			{
				Doors[3].GetComponent<LvSixDoor>().bOpen = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSixScript4");
				iCurrentMiniGameIndex += 1;
			}
		}
		if(!MiniGame.activeSelf && Lv6OpenChestCutScene.WaitingToStart == cutscene )
		{
			cutscene = Lv6OpenChestCutScene.ChestOpen;
			Global.GetGameObjectByID("36019").GetComponent<ObjectMovement>().m_bStartMoving = true;
		}

		if(Global.GetGameObjectByID("36019").GetComponent<ObjectMovement>().m_bMovementComplete &&
			Lv6OpenChestCutScene.ChestOpen == cutscene)
		{
			cutscene = Lv6OpenChestCutScene.GemRiseUp;
			Gem.GetComponent<ObjectMovement>().m_bStartMoving = true;
		}
		
		if(Gem != null && Gem.GetComponent<ObjectMovement>().m_bMovementComplete && 
			Lv6OpenChestCutScene.GemRiseUp == cutscene)
		{
			cutscene = Lv6OpenChestCutScene.PlayerMoveToPos;
			Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(Global.GetObjectPositionByID("GrabGemPos"),
																			 Gem.transform.position);
			
		}
		if(Global.GetPlayer() && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition &&
			Lv6OpenChestCutScene.PlayerMoveToPos == cutscene)	
		{
			cutscene = Lv6OpenChestCutScene.PlayerGrabGem;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LevelSixScript5");
			GameObject.Find("Gem6").GetComponent<GetGem>().enabled = true;
		}
		if(Global.GetPlayer() && !Global.GetPlayer ().GetComponent<Animation>().IsPlaying("take") &&
			Lv6OpenChestCutScene.PlayerGrabGem == cutscene)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSixScript6");
			cutscene = Lv6OpenChestCutScene.Finished;
			Global.GemObtainedForTheLevel = true;
			Global.GetGameObjectByID("36012").GetComponent<ObjectMovement>().m_bStartMoving = true;
			Global.GetGameObjectByID("36011").GetComponent<ObjectMovement>().m_bStartMoving = true;
		}
	}
	
	public void MiniGameEnable(int index)
	{
		MiniGames[index].SetActive(true);
	}
	public void TurnOffEagle()
	{
		Eagle.SetActive(false);	
	}
}
