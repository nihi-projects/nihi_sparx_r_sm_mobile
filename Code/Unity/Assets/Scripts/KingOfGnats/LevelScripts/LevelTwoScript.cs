using UnityEngine;
using System.Collections;

public class LevelTwoScript : MonoBehaviour {
	
	public int[] NumOfLightsMeltIce;
	private int CurrentNumMeltedIce;
	public string IceMeltCutSceneCamID;
	public bool m_bPlayCutScene = false;
	public int CurrentIceMeltIndex = 0;
	public float[] IceBelowMeltPosY;
	public float[] IceAboveMeltPosY;
	public GameObject TopIce;
	public GameObject BottomIce;
	
	public bool m_hopeTalkFinished = false;
	
	GameObject Tui;
	bool m_bTriggeredIceFall =false;
	int m_iTriggeredCaveTui = 0;
	int m_iTriggeredDoorTui = 0;
	// Use this for initialization
	void Start () {
		Tui = GameObject.Find ("Hope");
	}
	
	// Update is called once per frame
	void Update () {
        if (Global.GetPlayer() == null)
            return;

		TriggeredCaveEvent();
		if(m_bTriggeredIceFall && Camera.main.GetComponent<CameraShake>() != null && !Camera.main.GetComponent<CameraShake>().enabled){
			GetComponent<IceDropLv2>().m_bIsDropping = true;
		}
		if(m_bPlayCutScene){
			PlayCutScene();
		}
		if(m_iTriggeredCaveTui == 0){
			if(Global.m_sCurrentFakeNPC == "FakeTuiIceCave" 
				&& !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition
				&& !Global.GetPlayer().GetComponent<Animation>().IsPlaying("walk")){
				if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
					SetupTuiInCaveInteraction();
					m_iTriggeredCaveTui = 1;
				}
			}
		}
		
		//Pop up the conversation box when the Hope is arrived
		if(!Tui.GetComponent<HopeTalk>().m_bMovingToTarget && m_hopeTalkFinished){
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			m_hopeTalkFinished = false;
		}
	}
	
	void SetupTuiInCaveInteraction()
	{
		//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
		Global.GetPlayer ().GetComponent<Animation>().Play("idle");
		
		Tui.GetComponent<HopeTalk>().HopeFlyTo("32044");
		m_hopeTalkFinished = true;
	}
	
	void TriggeredCaveEvent()
	{
		if(!m_bTriggeredIceFall)
		{
			GameObject Collide = GameObject.Find ("IceFallCollider");
			
			if(Collide.GetComponent<Collider>().bounds.Contains(Global.GetPlayer().transform.position))
			{
				SoundPlayer.PlaySound("sfx-icecrash", 0.8f);
				m_bTriggeredIceFall = true;
				Camera.main.GetComponent<CameraShake>().enabled = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LevelTwoScript");
				Global.GetPlayer().GetComponent<Animation>().Play("idle");
			}
			if(Collide.GetComponent<Collider>().bounds.Contains(Global.GetPlayer().transform.position) && m_bTriggeredIceFall == true && !Camera.main.GetComponent<CameraShake>().enabled){
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelTwoScript2");
			}
		}
	}

    //Closes the Ice door, will not close if it is already closed.
    public void CloseIceDoor()
    {
        ObjectMovement topIceMove = TopIce.GetComponent<ObjectMovement>();

        //This is a bit of hacky code to detect whether the ice door has actually been opened.
        //Opening something twice leads to weird issues.
        if(topIceMove.m_vOriginalPosition != topIceMove.m_vStartPosition)
        {
            topIceMove.m_vMovement.y = -SumAll(IceAboveMeltPosY);
            topIceMove.RecalcMovement();
            topIceMove.m_bStartMoving = true;
            topIceMove.m_bMovementComplete = false;

            BottomIce.GetComponent<ObjectMovement>().m_vMovement.y = -SumAll(IceBelowMeltPosY);
            BottomIce.GetComponent<ObjectMovement>().RecalcMovement();
            BottomIce.GetComponent<ObjectMovement>().m_bStartMoving = true;
            BottomIce.GetComponent<ObjectMovement>().m_bMovementComplete = false;
        }
    }

    float SumAll( float[] _f )
    {
        float total = 0f;
        foreach( float f in _f )
        {
            total += f;
        }
        return total;
    }
	
	void PrepareCutScene()
	{
		// Stop movement
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LevelTwoScript3");
		// Move camera
		Global.SnapCameraBasedOnID(IceMeltCutSceneCamID);
		m_bPlayCutScene = true;
		SoundPlayer.Stop("sfx-icecrash"); 
		SoundPlayer.PlaySound("sfx-caveopening", 0.8f);
		
		TopIce.GetComponent<ObjectMovement>().m_vMovement.y = IceAboveMeltPosY[CurrentIceMeltIndex];
		TopIce.GetComponent<ObjectMovement>().RecalcMovement();
		TopIce.GetComponent<ObjectMovement>().m_bStartMoving = true;
		TopIce.GetComponent<ObjectMovement>().m_bMovementComplete = false;
		
		BottomIce.GetComponent<ObjectMovement>().m_vMovement.y = IceBelowMeltPosY[CurrentIceMeltIndex];
		BottomIce.GetComponent<ObjectMovement>().RecalcMovement();
		BottomIce.GetComponent<ObjectMovement>().m_bStartMoving = true;
		BottomIce.GetComponent<ObjectMovement>().m_bMovementComplete = false;
	}
	public void AddCurrentNumMeltedIce(int iNum)
	{
		CurrentNumMeltedIce += iNum;
		if(CurrentNumMeltedIce == NumOfLightsMeltIce[CurrentIceMeltIndex])
		{
			PrepareCutScene();
		}
	}
	void PlayCutScene()
	{
		// check if ice arrives at the position
		if(TopIce.GetComponent<ObjectMovement>().m_bMovementComplete &&
			BottomIce.GetComponent<ObjectMovement>().m_bMovementComplete)
		{
			EndCutScene();
		}
	}
	
	void EndCutScene()
	{
		TopIce.GetComponent<ObjectMovement>().m_bStartMoving = false;
		TopIce.GetComponent<ObjectMovement>().m_bMovementComplete = false;
		
		BottomIce.GetComponent<ObjectMovement>().m_bStartMoving = false;
		BottomIce.GetComponent<ObjectMovement>().m_bMovementComplete = false;
		
		m_bPlayCutScene = false;
		CurrentIceMeltIndex +=1;
		
		if (CurrentIceMeltIndex == NumOfLightsMeltIce.Length)
		{
			Global.GetPlayer().GetComponent<PlayerFreeze>().enabled = false;
			Global.GetPlayer().GetComponent<PlayerFreeze>().Overlay.GetComponent<GUITexture>().enabled = false;
			GetComponent<IceDropLv2>().Reset();
			GetComponent<IceDropLv2>().enabled = false;
		}
		
		// Snap to player
		Global.CameraSnapBackToPlayer();
		// allow movement again
		Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelTwoScript4");
	}
}
