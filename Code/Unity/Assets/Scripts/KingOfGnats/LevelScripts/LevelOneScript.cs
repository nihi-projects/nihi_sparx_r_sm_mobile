using UnityEngine;
using System.Collections;

public enum LevelOneStatus
{
	STANBY = 0, // Do not process anything
	PLAYER_FADE_IN,
	MOVE_TO_GUARDIAN,
	TUI_HINT,
	MAX_LEVELONESTATS
};

public class LevelOneScript : MonoBehaviour {

	private static LevelOneScript instanceRef;

	// Use this for initialization
	void Start () {
		// This would be where the player is first set to appear
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.CurrentLevelNumber < 7){
			if(Global.CurrentInteractNPC == "Mentor" && Global.LevelSVisited){
                if(GameObject.Find("PortalPos"))
				    GameObject.Find("PortalPos").GetComponent<PortalInteraction>().enabled = true;
			}
		}
		else if(Global.CurrentLevelNumber == 7){
			if(Global.CurrentInteractNPC == "Mentor" && Global.LevelSVisited && Global.PreviousSceneName == "L7MentorScene6"){
                if(GameObject.Find("PortalPos"))
				    GameObject.Find("PortalPos").GetComponent<PortalInteraction>().enabled = true;
			}
		}
	}	
	void Awake()
	{
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}
	}
	
	void ResetLevelSValues()
	{

	}
	
	void OnEnable()
	{
		ResetLevelSValues();
	}
}
