using UnityEngine;
using System.Collections;

public enum EagleReturnToLvS
{
	EagleFlyDown,
	PauseForEffect,
	EagleFlyBackToSeat
}

public class LevelSScript : MonoBehaviour {
	
	bool m_bStartSetupComplete = false;
	EagleReturnToLvS status = EagleReturnToLvS.EagleFlyDown;
	public Transform SeatPosition;
	public Transform StationaryPosition;
	public Transform FlyFrom;
	public Transform PlayerPos;
	GameObject Eagle;
	Vector3 start = Vector3.zero;
	float PauseTimer = 1.7f;
	float m_fTime = 0.0f;
	public GameObject m_gNPCsOnLevels = null;
	private Texture2D m_tTrimTexture;

	void Awake(){
		string playerChildObjectName = "";
		GameObject m_gBoyObject      = GameObject.Find("Boy");
		GameObject m_gGirlObject     = GameObject.Find ("Girl");
		Transform player             = null;
		
		if(Global.CurrentPlayerName == "Boy"){
			playerChildObjectName = "boy";
			m_gBoyObject.SetActive(true);
			m_gGirlObject.SetActive(false);
			
			m_tTrimTexture = GameObject.Find ("GUI").GetComponent<BoyCustomisation>().GetTexture(Global.userTrimTextureNum);
		}
		else if(Global.CurrentPlayerName == "Girl"){
			playerChildObjectName = "girl";
			m_gGirlObject.SetActive(true);
			m_gBoyObject.SetActive(false);
			
			m_tTrimTexture = GameObject.Find ("GUI").GetComponent<GirlCustomisation>().GetTexture(Global.userTrimTextureNum);
		}
	}

	// Use this for initialization
	void Start () {

		Eagle = GameObject.Find ("Eagle");
		m_fTime = 0.0f;
		if(!Global.m_bGotCurrentLevelGem)
		{
			m_bStartSetupComplete = true;
			Eagle.GetComponent<EagleInteraction>().enabled = true;
		}
		else
		{
			try{
				GameObject.Find("Mentor").GetComponent<CharacterInteraction>().enabled = false;
				Global.SnapCameraBasedOnID("20420");
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LevelSScript");
				Eagle.transform.position = FlyFrom.position;
				Eagle.transform.rotation = FlyFrom.rotation;
				Global.GetPlayer().transform.position = FlyFrom.position;
				Global.GetPlayer().transform.position += Vector3.up * 1.8f;
				Global.GetPlayer().transform.rotation = FlyFrom.rotation;
				start = FlyFrom.position;
				m_fTime = 0.0f;
				
				GetComponent<GetAroundSparx>().enabled = false;
				GameObject.Find("GUI").GetComponent<IslandSplashScreen>().IsFadeIn(false);
				GameObject.Find("GUI").GetComponent<IslandSplashScreen>().enabled = false;
				GameObject.Find("GUI").GetComponent<IslandSplashScreen>().enabled = true;
			} catch {
				Debug.Log("Attempting to hide npcs");
			}
		}
		
		m_gNPCsOnLevels = GameObject.Find("NPCs") as GameObject;
		m_gNPCsOnLevels.SetActive(false);

        Stuff();
	}
	
	// Update is called once per frame
	void Update () {
		if(!m_bStartSetupComplete)
		{
			//switch(status)
			//{
			//case EagleReturnToLvS.EagleFlyDown:
			//	EagleFlyDown();
			//	break;
			//case EagleReturnToLvS.EagleFlyBackToSeat:
			//	EagleFlyBackToSeat();
			//	PauseForEffect();
			//	break;
			//case EagleReturnToLvS.PauseForEffect:
			//	PauseForEffect();
			//	break;
			//default:break;
			//}
		}
	}
	
	void EagleFlyDown()
	{
		m_fTime += Time.deltaTime * 0.3f;
		
		Eagle.GetComponent<Animation>().Play ("fly");
		Global.GetPlayer ().GetComponent<Animation>().Play("ride");
		Eagle.transform.position = Vector3.Lerp (start, StationaryPosition.position, m_fTime);
		Vector3 playerPos = Eagle.transform.position + (Vector3.up * 0.2f);
		Global.GetPlayer ().transform.position = playerPos;
		
		if(m_fTime >= 1.0f){
			m_fTime = 0.0f;
			Global.GetPlayer ().transform.position = PlayerPos.position;
			Global.GetPlayer ().transform.rotation = PlayerPos.rotation;
			status = EagleReturnToLvS.PauseForEffect;
			Global.GetPlayer ().GetComponent<Animation>().Play ("idle");
			start = Eagle.transform.position;
			Eagle.GetComponent<Animation>().Play("idle");
			Global.CameraSnapBackToPlayer();
		}
	}
	
	void EagleFlyBackToSeat()
	{
		m_fTime += Time.deltaTime * 0.6f;
		
		Vector3 facepos = SeatPosition.position;
		facepos.y = Eagle.transform.position.y;
		
		Eagle.transform.LookAt(facepos);
		Eagle.GetComponent<Animation>().Play ("fly");
		
		Eagle.transform.position = Vector3.Lerp (start,
												 SeatPosition.position,
												 m_fTime);
		if(m_fTime >= 1.0f)
		{
			m_fTime = 0.0f;
			Eagle.transform.rotation = SeatPosition.rotation;
			Eagle.GetComponent<Animation>().Play("idle");
			this.enabled = false;
		}
	}
	void PauseForEffect()
	{
		PauseTimer -= Time.deltaTime;
		
		if(PauseTimer <= 0.0f)
		{
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			GameObject.Find("Mentor").GetComponent<CharacterInteraction>().enabled = true;
			Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(true, "LevelSScript2");
		}
		else if (PauseTimer <= 0.7f)
		{
			status = EagleReturnToLvS.EagleFlyBackToSeat;
		}
	}

    void Stuff()
    {
        //m_fTime += Time.deltaTime * 0.3f;

        //Eagle.GetComponent<Animation>().Play("fly");
        Global.GetPlayer().GetComponent<Animation>().Play("ride");
        //Eagle.transform.position = Vector3.Lerp(start, StationaryPosition.position, m_fTime);
        //Vector3 playerPos = Eagle.transform.position + (Vector3.up * 0.2f);
        //Global.GetPlayer().transform.position = playerPos;

        //if (m_fTime >= 1.0f)
        //{
            //m_fTime = 0.0f;
            Global.GetPlayer().transform.position = PlayerPos.position;
            Global.GetPlayer().transform.rotation = PlayerPos.rotation;
            //status = EagleReturnToLvS.PauseForEffect;
            Global.GetPlayer().GetComponent<Animation>().Play("idle");
            //start = Eagle.transform.position;
            //Eagle.GetComponent<Animation>().Play("idle");
            Global.CameraSnapBackToPlayer();
        //}

        //PauseTimer -= Time.deltaTime;

        //if (PauseTimer <= 0.0f)
        //{
            Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
            GameObject.Find("Mentor").GetComponent<CharacterInteraction>().enabled = true;
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSScript3");
        //}
        //else if (PauseTimer <= 0.7f)
        //{
           // status = EagleReturnToLvS.EagleFlyBackToSeat;
        //}

        //m_fTime += Time.deltaTime * 0.6f;

        //Vector3 facepos = SeatPosition.position;
        //facepos.y = Eagle.transform.position.y;

        //Eagle.transform.LookAt(facepos);
        //Eagle.GetComponent<Animation>().Play("fly");

        //Eagle.transform.position = Vector3.Lerp(start,
        //                                         SeatPosition.position,
        //                                         m_fTime);
        //if (m_fTime >= 1.0f)
        //{
        //    m_fTime = 0.0f;
        //    Eagle.transform.rotation = SeatPosition.rotation;
        //    Eagle.GetComponent<Animation>().Play("idle");
            this.enabled = false;
        //}
    }
}
