using UnityEngine;
using System.Collections;

public class ShieldAction : MonoBehaviour {
	
	public bool shieldIsOn = false;
	public bool m_bShieldFadeInFinished = false;
	public bool m_bShieldFadeOutFinished = false;
	
	private Color m_cShieldColor;
	private float m_fShieldFadeAlfa = 0.0f;
	private bool m_bFadeInStart = false;
	private bool m_bFadeOutStart = false;
	private float m_fShieldAppearTime = 0.0f;
	
	// Use this for initialization
	public void Start () {
	
	}
	
	// Update is called once per frame
	public void Update () {
		if(m_bFadeInStart){
			m_fShieldFadeAlfa = Mathf.Lerp(0.0f, 1.0f, (Time.time - m_fShieldAppearTime) * 0.1f);
		}
		if(m_bFadeOutStart){
			m_fShieldFadeAlfa = Mathf.Lerp(1.0f, 0.0f, (Time.time - m_fShieldAppearTime) * 0.1f);
		}
		
		GameObject.Find ("_[id]37090_shield").GetComponent<Renderer>().material.color = new Color(m_cShieldColor.r, m_cShieldColor.g, m_cShieldColor.b, m_fShieldFadeAlfa);
		
		if(m_fShieldFadeAlfa == 1.0f){
			m_bShieldFadeInFinished = true;
			m_bShieldFadeOutFinished = false;
		}
		if(m_fShieldFadeAlfa == 0.0f){
			m_bShieldFadeOutFinished = true;
			m_bShieldFadeInFinished = false;
		}
	}
	
	//
	public void OnEnable()
	{
		m_cShieldColor = GameObject.Find ("_[id]37090_shield").GetComponent<Renderer>().material.color;
	}
	
	//Trun on the shield
	public void SetShieldOn()
	{
		m_bFadeInStart = true;
	}
	
	//Turn off the shield
	public void SetShieldOff()
	{
		m_bFadeOutStart = true;
	}
	
	public void ResetShield()
	{
		GameObject.Find("_[id]37090_shield").GetComponent<Renderer>().enabled = false;
		GameObject.Find("_[id]37090_shield").GetComponent<Renderer>().material.color = new Color(m_cShieldColor.r, m_cShieldColor.g, m_cShieldColor.b, 0.0f);
	}
}
