using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using Boomlagoon.JSON;


public class CapturedDataInput : MonoBehaviour {
	private static CapturedDataInput instanceRef;

	public bool bGotDynamicURL_RESPONSE_NULL = false;
	public bool bDynamicURL_ATTEMPTING = false;
	public bool bNoDynamicURL_AFTERATTEMPTS = false;
	public int attemps;

    //public variables
    // TODO: are these holding the same token???
    public string m_sGameLoginToken = "";
	public string m_sGameDynamicToken = "";

	public string m_sGameConfigServerURL = "";

    public string m_sDynamicURL   = "";     // serviceURL (TODO: should rename?)

	public int m_iServerLevelNum      = 0;
	public string m_sSaveProgressURL  = "";
	public string m_sReadProgressURL  = "";
	
	public WWW m_dynamiURLRetriever;
	
	//Progress
	public WWW m_ProcessDataPostRetriever;
	public WWW m_ProcessDataGetRetriever;
	public WWW m_ProcessNotebookDataGetRetriever;
	//Level
	public WWW m_LevelDataPostRetriever;
	public WWW m_LevelDataGetRetriever;
	//Event
	public WWW m_EventLevelStartEndDataPostRetriever;
	public WWW m_EventPHQADataPostRetriever;

	public WWW m_sLoadURLReturn;
	public WWW m_sSaveURLReturn;

	public WWW m_txtDataSender;//DIEGO

	//Login
	public WWW m_ProcessLoginAPIDrupal;//DIEGO
	public WWW m_ProcessRegistrationAPIDrupal;//DIEGO

	public string failedFileName = @"/failedRequests.txt";

	public bool m_bFoundLevelNumber            = false;
	
	//private variables
	private bool m_bLoggedInConfirmed          = false;
	private bool m_dynamicLoginSuccessful      = false;
	private bool m_loginSuccessful             = false;
	private string m_sNotebookHeader           = "";

	public string platform_type                = "";

	public GameObject LoginPanel;
	public GameObject SignUpPanel;

	//Implementation
	private static CapturedDataInput instance = new CapturedDataInput();
	public TokenHolderLegacy instanceTHL;

	private CapturedDataInput() {}

	public bool bUploadFileComplete = false;

	public bool bWaitingForSeconds = false;

	public bool bOfflineFinishLevelSave = false;

	public bool bLevelNumberLoaded = false;
	
	void Awake()
	{
		//DontDestroyOnLoad(gameObject);

		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}

	}
	
	// Use this for initialization
	void Start () 
	{
		if (Application.platform == RuntimePlatform.Android)
			platform_type = "Android";
		else if(Application.platform == RuntimePlatform.IPhonePlayer)
			platform_type = "iOS";

		instanceTHL = GameObject.Find ("TokenHolder").GetComponent<TokenHolderLegacy> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

    //--------------------------------------------------------Server Login-------------------------------------------------

    // first login - returns a dynamicURL (serviceURL)
	public IEnumerator LoginProcess_NewJsonSchema(string Username, string Password)
	{
		//var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
        //var loginURL = "https://sparx.org.nz/api/appuser/token";
		var loginURL = "https://test.sparx.org.nz/api/appuser/token";
        //var loginURL = "https://stage.sparx.nihi.auckland.ac.nz/api/appuser/token";
        //var loginURL = "https://testmeany.spnihi.auckland.ac.nz/api/appuser/token";

        var jsonString = "{\"username\":\"" + Username + "\",\"password\": \"" + Password + "\"}";//GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"] = "application/json";

        //Debug.LogError("<LOGIN> test error");
        Debug.Log("<LOGIN> POST " + loginURL + "(payload: ```" + jsonString + "```)");
		//postHeader ["Content-Length"] = jsonString.Length.ToString();
		//PlayerPrefs.SetString("userresponse_playerprefs", jsonString);
		yield return m_ProcessLoginAPIDrupal = new WWW(loginURL, encoding.GetBytes(jsonString), postHeader);

		//Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + jsonString);
		//Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
		if(m_ProcessLoginAPIDrupal != null){
			if (m_ProcessLoginAPIDrupal.isDone) {
				if (string.IsNullOrEmpty (m_ProcessLoginAPIDrupal.error))
                {
					m_sGameConfigServerURL = m_ProcessLoginAPIDrupal.text.Replace ("[\"", "").Replace ("\"]", "");
					instanceTHL.Global_sGameConfigServerURL = m_ProcessLoginAPIDrupal.text.Replace ("[\"", "").Replace ("\"]", "");
                    //Debug.Log ("<SAVING PROGRESS AND NOTEBOOK>:" + m_ProcessLoginAPIDrupal.text);
                    Debug.Log("<LOGIN> success (" + m_ProcessLoginAPIDrupal.text + ")");
                }
                else {
                    Debug.LogWarning("<LOGIN> FAILED (" + m_ProcessLoginAPIDrupal.error + ")");
                    //Debug.LogError("<LOGIN> FAILED (" + m_ProcessLoginAPIDrupal.error + ")");
                }

            }
		}

		//yield return m_ProcessDataPostRetriever = new WWW(GetUserProcessPostURL(_levelNum, _userFeedbackScore), form);
		//Debug.Log ("The user process data POST URL is = " + GetUserProcessPostURL_NewJsonSchema(_levelNum));
	}	

	public IEnumerator RegisterProcess_NewJsonSchema(string jsonString)
	{

		//var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
        //var regURL = "https://sparx.org.nz/api/appuser/register";
        var regURL = "https://test.sparx.org.nz/api/appuser/register";
        //var regURL = "https://stage.sparx.nihi.auckland.ac.nz/api/appuser/register";
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"] = "application/json";

        //postHeader ["Content-Length"] = jsonString.Length.ToString();
        //PlayerPrefs.SetString("userresponse_playerprefs", jsonString);


        Debug.Log("<REGISTER> POST " + regURL);
        Debug.Log("<REGISTER> json: ```" + jsonString + "```");

        yield return m_ProcessRegistrationAPIDrupal = new WWW(regURL, encoding.GetBytes(jsonString), postHeader);

		Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
		if(m_ProcessRegistrationAPIDrupal != null) {
            if(m_ProcessRegistrationAPIDrupal.isDone){
				if (string.IsNullOrEmpty (m_ProcessRegistrationAPIDrupal.error)) {
					m_sGameDynamicToken = m_ProcessRegistrationAPIDrupal.text.Replace ("[\"", "").Replace ("\"]", "");
					m_sGameLoginToken = m_ProcessRegistrationAPIDrupal.text.Replace ("[\"", "").Replace ("\"]", "");
					//Debug.Log ("<REGISTER>:" + m_ProcessRegistrationAPIDrupal.text);
				} else {
					Debug.Log ("<REGISTER_Error>:" + m_ProcessRegistrationAPIDrupal.error);

				}
				Debug.Log ("<REGISTER> response text: ```" + m_ProcessRegistrationAPIDrupal.text + "```");
			}
		}

		//yield return m_ProcessDataPostRetriever = new WWW(GetUserProcessPostURL(_levelNum, _userFeedbackScore), form);
		//Debug.Log ("The user process data POST URL is = " + GetUserProcessPostURL_NewJsonSchema(_levelNum));
	}

    // TODO: Merge this into the login function and be done with it!
	public void GetTokenAfterLoginDrupalAPI_NewJsonSchema()
	{
		//Debug.Log ("THIS IS BEING CALLED (DEBUG CALL @ LINE 119)");

		if(!m_bLoggedInConfirmed){
			if(m_ProcessLoginAPIDrupal.isDone && string.IsNullOrEmpty(m_ProcessLoginAPIDrupal.error)){
				m_sGameConfigServerURL = m_ProcessLoginAPIDrupal.text.Replace ("[\"", "").Replace("\"]","");
				instanceTHL.Global_sGameConfigServerURL = m_ProcessLoginAPIDrupal.text.Replace ("[\"", "").Replace("\"]","");
				Debug.Log ("Log in data retriver: " + m_ProcessLoginAPIDrupal.text);

				//----------------------------------------------------------------------------------------------------------

				m_loginSuccessful = true;//FIXED TEST LOGIN
				if(m_loginSuccessful){
					Debug.Log ("Server login is successful...");
					m_bLoggedInConfirmed = true;
					//string trim1, trim2;
					//trim1 = "{\"code\":200,\"data\":{\"token\":\"";
					//trim2 = "\"}}";
					//Get the taken after the successful login
					//m_sGameLoginToken = m_sGameLoginToken.Remove (0, trim1.Length);
					//m_sGameLoginToken = m_sGameLoginToken.TrimEnd(trim2.ToCharArray());
					Debug.Log ("Game Config URL: " + m_sGameConfigServerURL);
					//m_sGameDynamicToken = m_sGameLoginToken; // For testing only. Remove later.
					PlayerPrefs.SetString("Config_URL", m_sGameConfigServerURL);
				}

			}else{
				//m_sGameLoginToken = "c92fb78b7d03b6e58ad88cf95ee78cb7";// For testing only. Remove later.
				Debug.Log ("Server login is unsuccessful...");
				if(PlayerPrefs.HasKey("login_token")){
					Debug.Log("Loading token from Player Prefs");
					m_sGameConfigServerURL = m_sGameDynamicToken = PlayerPrefs.GetString("Config_URL");
					instanceTHL.Global_sGameConfigServerURL = m_sGameDynamicToken = PlayerPrefs.GetString("Config_URL");
					m_loginSuccessful = m_bLoggedInConfirmed = true;
				} else {
					Debug.Log("..and noone has ever logged in. Should Give an error.");
				}

			}
		} else {
            Debug.LogWarning("<post-login> - already logged in");
        }

    }
	//--------------------------------------------------------Get Dynamic URL--------------------------------------------------
    
    // follow the url returned by the login, to get the dynamicURL (serviceURL)
    // The dynamic URL is the server API
	public IEnumerator SendOffGameConfigURL_NewJsonSchema(string _url)
	{
		m_dynamiURLRetriever = new WWW(_url);		
		yield return m_dynamiURLRetriever;
	}

	//CHANGES_KEY_NEW_JSON_SCHEMA 
	public void GetTokenAndDynamicURL_NewJsonSchema()
	{
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
		string wholeDynamicURLString = "";

		if(m_dynamiURLRetriever.isDone && string.IsNullOrEmpty(m_dynamiURLRetriever.error)){
			Debug.Log ("Dynamic URL login is " + m_dynamiURLRetriever.text);
			wholeDynamicURLString = m_dynamiURLRetriever.text;
			Debug.Log ("Dynamic URL login is successful..." + m_dynamiURLRetriever.text);
			JSONObject json = new JSONObject();
			//Debug.Log ("UNO");
			json = JSONObject.Parse(wholeDynamicURLString);
			//Debug.Log ("DOS");
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_sTestWholeDynamic = wholeDynamicURLString;
			m_dynamicLoginSuccessful = json.GetValue("code").ToString().Contains ("200");//wholeDynamicURLString.Contains("\"code\":200");
			//Debug.Log ("TRES"); 
			if(m_dynamicLoginSuccessful){
				//Debug.Log ("CUATRO");
				if (null != json) {
					//Debug.Log ("CINCO");
					m_sGameLoginToken = m_sGameDynamicToken = json.GetObject ("data").GetString("token");

					m_sDynamicURL = json.GetObject ("data").GetString("serviceURL");

					//m_sDynamicURL = ""; //Test No Dynamic URL FOUND
				}

			}
			else{
				
				LoginPanel.SetActive (false);
				SignUpPanel.SetActive (false);

				Debug.Log ("Dynamic URL login is unsuccessful...");
			}
		}
	}



	//--------------------------------------------------------PROCESS--------------------------------------------------
	//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessPostURL_NewJsonSchema(int _levelNum)
	{
		string info = "";
		/*if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}*/
		info += "{\"UserResponse\":{";

		info += GetNotebookData(_levelNum);
		info += "}}";
		//string urlLevel = m_sDynamicURL + "progress?jsondata=";

		Debug.Log (info);

		string m_sProgressURL = info;

		return m_sProgressURL;
	}


    //CHANGES_KEY_NEW_JSON_SCHEMA 
    public string GetUserProcessGetURL_NewJsonSchema(int _levelNum)
	{
		string urlLevel = m_sDynamicURL + "UserResponse?sid=" + m_sGameLoginToken;
        string m_sProgressURL = urlLevel;
		return m_sProgressURL;
	}

    //CHANGES_KEY_NEW_JSON_SCHEMA 
    public string GetNotebookData(int _levelNum)
	{	
		string processDataString = "";

        processDataString += "\"LevelNumber\":" + _levelNum.ToString();
        processDataString += ", \"FeedBackScore\":[" + Global.userFeedBackScore[0].ToString();
        processDataString += ", " + Global.userFeedBackScore[1].ToString();
        processDataString += ", " + Global.userFeedBackScore[2].ToString() + "], ";
        processDataString += FullNotebookString(_levelNum);

        //processDataString += "\"LevelNumber\":" + _levelNum.ToString();
        //processDataString += ", \"FeedBackScore\":[" + Global.userFeedBackScore[0].ToString();
        //processDataString += ", " + Global.userFeedBackScore[1].ToString();
        //processDataString += ", " + Global.userFeedBackScore[2].ToString() + "], ";
        //if(_levelNum > 1){
        //	Global.lastLevelNoteBookData = Global.lastLevelNoteBookData.TrimEnd(']');
        //	Global.lastLevelNoteBookData += ",";
        //	processDataString += "\"NoteBook\":" + Global.lastLevelNoteBookData; //DIEGO FIX NOTEBOOK
        //}
        //processDataString += NoteBookDataBlocksToPost(_levelNum);
        //processDataString += "\"]}]";
        //Debug.Log ("processDataString: " + processDataString);
        return processDataString;
	}	

	//CHANGES_KEY_NEW_JSON_SCHEMA - NOTHING CHANGES IN THIS ROUTINE
	//CHANGES_KEY_NEW_JSON_SCHEMA - NOTHING CHANGES IN THIS ROUTINE
	public string NoteBookDataBlocksToPost(int _currentLevelNum)
	{
        string[][] strRefs = new string[][] {   Global.userL1NoteBookDataBlocks, Global.userL1NoteBookManualBlocks,                                     //L1Data
                                                Global.userL2NoteBookDataBlocks, Global.userL2NoteBookManualBlocks,                                     //L2Data
                                                Global.userL3NoteBookDataBlockOne, Global.userL3NoteBookDataBlockTwo, Global.userL3NoteBookDataBlockThree, //L3Data
                                                Global.userL4NoteBookDataBlockOne, Global.userL4NoteBookStepsBlocks, Global.userL4NoteBookDataBlockTwo, //L4Data
                                                Global.userL5NoteBookDataBlocks, Global.userL5NoteBookManualBlocks,                                     //L5Data
                                                Global.userL6NoteBookDataBlocks, Global.userL6NoteBookRapaBlocks, Global.userL6NoteBookManualBlocks,    //L6Data.
                                                Global.userL7NoteBookDataBlocks};                                                                       //L7Data.

        int jMin = 0;
        int jMax = 0;
		m_sNotebookHeader = "";
		switch(_currentLevelNum){
		case 1://Level 1
			m_sNotebookHeader = "\"NoteBook\":[";
                jMin = 0; jMax = 2;
			break;
		case 2://Level 2
                jMin = 2; jMax = 4;
			break;
		case 3://Level 3
                jMin = 4; jMax = 7;
			break;
		case 4://Level 4
                jMin = 7; jMax = 10;
			break;
		case 5://Level 5
                jMin = 10; jMax = 12;
			break;
		case 6://Level 6
                jMin = 12; jMax = 15;
			break;
		case 7://Level 7
                jMin = 15; jMax = 16;
            break;			
		}

        for (int j = jMin; j < jMax; j++)
        {
            if (j == jMin)
            {
                m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
            }
            else
            {
                m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
            }
            m_sNotebookHeader += "\"Answer\": [\"";
            WriteData(ref strRefs[j]);
        }

        m_sNotebookHeader = m_sNotebookHeader.Replace ("\"\",", "");
        m_sNotebookHeader += "\"]}";

        return m_sNotebookHeader;
	}

    public void WriteData( ref string[] _str)
    {
        for (int i = 0; i < _str.Length; i++)
        {
            if (i == 0 && _str[i] != "" && _str[i] != " " && _str[i] != null)
            {
                m_sNotebookHeader += _str[i].Replace("\"", "");
            }
            else if (i < _str.Length - 1 && _str[i] != "" && _str[i] != " " && _str[i] != null)
            {
                m_sNotebookHeader += "\", \"" + _str[i].Replace("\"", "");
            }
            else if (i == _str.Length - 1 && _str[i] != "" && _str[i] != " " && _str[i] != null)
            {
                m_sNotebookHeader += "\", \"" + _str[i].Replace("\"", "");
            }
            else
            {
                m_sNotebookHeader += "";
            }
        }
    }

	public IEnumerator SaveUserProcessToServer_NewJsonSchema(int _levelNum, int _userFeedbackScore)
	{	
		//	WWWForm form = new WWWForm();
		//	form.AddField("fake", "xy");

		var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
		var jsonString = GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString();
		PlayerPrefs.SetString("userresponse_playerprefs", jsonString);
		yield return m_ProcessDataPostRetriever = new WWW(postScoreURL, encoding.GetBytes(jsonString), postHeader);

		Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + jsonString);
		Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));

		if(m_ProcessDataPostRetriever.isDone){
			if(!string.IsNullOrEmpty(m_ProcessDataPostRetriever.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();
				Debug.Log ("<SAVING CHARACTER - On File: No Connection>:" + jsonString);
				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log ("<SAVING CHARACTER>:" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			}else{
				Debug.Log ("<SAVING PROGRESS AND NOTEBOOK>:" + m_ProcessDataPostRetriever.text);
			}
		}

		//yield return m_ProcessDataPostRetriever = new WWW(GetUserProcessPostURL(_levelNum, _userFeedbackScore), form);
		Debug.Log ("The user process data POST URL is = " + GetUserProcessPostURL_NewJsonSchema(_levelNum));
	}	
    
	//------------------------------------------------------------------------------
	public IEnumerator ReadUserProgressDataFromServer_NewJsonSchema(bool bOffline)
	{
		String test = "";
		if (!bOffline) {//Online
            string userResponseURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameLoginToken;
            yield return m_ProcessDataGetRetriever = new WWW (userResponseURL);

			if (m_ProcessDataGetRetriever.isDone) {
				if (!String.IsNullOrEmpty (m_ProcessDataGetRetriever.error)) {
                    Debug.LogError("GET FAILED (" + userResponseURL + "): " + m_ProcessDataGetRetriever.error );
                    //test = getLatestFromFile("progress?");
                    test = PlayerPrefs.GetString ("userresponse_playerprefs");
					//test = test.Substring(test.IndexOf("jsondata=")+9);
					test = test.Replace(@"\", "");
					test = WWW.UnEscapeURL (test);
				} else {
                    //UDATING last_progress and last_save with loaded data from server
                    PlayerPrefs.SetString ("userresponse_playerprefs", m_ProcessDataGetRetriever.text);
					//PlayerPrefs.SetString ("savingpoint_playerprefs", m_ProcessDataGetRetriever.text);
					test = PlayerPrefs.GetString ("userresponse_playerprefs");
					//Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
				}

				//Debug.Log ("<PROGRESS>:" + m_ProcessDataGetRetriever.text);
			}
		} else { //offline

			test = PlayerPrefs.GetString ("userresponse_playerprefs");
			Debug.Log ("<ReadUserProgressDataFromServer offline>");
//			Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
		}

		test = test.Replace(@"\", "");

		Debug.Log ("The proggress data from the server or playerprefs = " + test);
		m_sReadProgressURL = test;
		JSONObject json = new JSONObject();

		json = JSONObject.Parse(test);
		string tempNum;
        string SavePoint;

		if (null != json) {
            //json = json.GetObject("UserResponse").GetObject("progress");
            if (null != json.GetObject("UserResponse"))
            {
                tempNum = json.GetObject("UserResponse").GetNumber("LevelNumber").ToString();
                m_bFoundLevelNumber = true;

                if (json.GetObject("UserResponse").GetString("SavePoint") == null)
                {// ANGELA ASKED FOR SWAP NULL FOR levelEnd
                    SavePoint = "levelEnd";// ANGELA ASKED FOR SWAP NULL FOR levelEnd
                }
                else
                {
                    SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
                }

                //Only set the level number if the override is not in place.
                if (LogoBackgroundScreen.levelSelectOverride == -1)
                {
                    Global.CurrentLevelNumber = int.Parse(tempNum);
                    if (Application.isEditor && Global.TestingOnline == true)
                    {
                        Global.CurrentLevelNumber = Global.TestProgress;
                    }
                    Debug.Log("Old server level number = " + Global.CurrentLevelNumber);
                }

				if (Global.CurrentLevelNumber < 1 || (Global.CurrentLevelNumber == 7 && SavePoint.Contains ("levelEnd"))) {
					Global.CurrentLevelNumber = 1;
					Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
				} else {
					if (SavePoint.Contains ("levelEnd")) {
						Global.CurrentLevelNumber++;// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
						Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
					} else {
						Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
					}
				}

				if (Global.CurrentLevelNumber > 1) {
					JSONArray tempArray = json.GetObject ("UserResponse").GetArray ("FeedBackScore");
					tempNum = tempArray [0].ToString ();
					Global.userFeedBackScore [0] = int.Parse (tempNum);
					tempNum = tempArray [1].ToString ();
					Global.userFeedBackScore [1] = int.Parse (tempNum);
					tempNum = tempArray [2].ToString ();
					Global.userFeedBackScore [2] = int.Parse (tempNum);
				}
			}
		}
		bLevelNumberLoaded = true;
	}
    
	public IEnumerator ReadUserProgressNoteBookDataFromServer_NewJsonSchema(int _levelNum, bool bOffline)
	{

		string test = "";

		if (!bOffline) {//Online
			WWWForm form = new WWWForm ();
			form.AddField ("fakeformNotebook", "xyxy");

			yield return m_ProcessNotebookDataGetRetriever = new WWW (GetUserProcessGetURL_NewJsonSchema (_levelNum));

			if (m_ProcessNotebookDataGetRetriever.isDone) {
				Debug.Log ("<NOTEBOOK>:" + m_ProcessNotebookDataGetRetriever.text);
			}

			test = m_ProcessNotebookDataGetRetriever.text;

		} else {//Offline
			test = PlayerPrefs.GetString ("userresponse_playerprefs");
			Debug.Log ("<PROGRESS PLAYERPREFS>:" + test);
			Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
		}
		Debug.Log ("The data from server is === " + test);
		JSONObject json = new JSONObject ();
		JSONObject jsonNotebookItem = new JSONObject ();
		json = JSONObject.Parse (test);
		Global.lastLevelNoteBookData = "";
        if(json != null )
        {
            json = json.GetObject("UserResponse");
            Debug.Log("-------------------------------------\nJSON:\n-------------------------------------");
            Debug.Log(json);
            if (null != json.GetArray("NoteBook"))
            {
                Global.lastLevelNoteBookData = json.GetArray("NoteBook").ToString();
                string[][] strRefs = new string[][]{Global.userL1NoteBookDataBlocks, Global.userL1NoteBookManualBlocks, //L1Data
                                                    Global.userL2NoteBookDataBlocks, Global.userL2NoteBookManualBlocks, //L2Data
                                                    Global.userL3NoteBookDataBlockOne, Global.userL3NoteBookDataBlockTwo, Global.userL3NoteBookDataBlockThree, //L3Data
                                                    Global.userL4NoteBookDataBlockOne, Global.userL4NoteBookStepsBlocks, Global.userL4NoteBookDataBlockTwo, //L4Data
                                                    Global.userL5NoteBookDataBlocks, Global.userL5NoteBookManualBlocks, //L5Data
                                                    Global.userL6NoteBookDataBlocks, Global.userL6NoteBookRapaBlocks, Global.userL6NoteBookManualBlocks}; //L6Data.

                if (null != json)
                {
                    if (_levelNum >= 2)
                    {
                        //Debug.Log (tempArray);
                        int j = _levelNum - 2;
                        int limit = 0;
                        switch (j)
                        {
                            case 0:
                                limit = 2;
                                break;
                            case 1://Show in Level 3
                                limit = 4;
                                break;
                            case 2://Show in Level 4
                                limit = 7;
                                break;
                            case 3://Show in Level 5
                                limit = 10;
                                break;
                            case 4://Show in Level 6
                                limit = 12;
                                break;
                            case 5://Show in Level 7
                                limit = 15;
                                break;
                        };

                        for (int i = 0; i < limit; ++i)
                        {
                            if (json.GetArray("NoteBook").Length > i)
                            {
                                GetJSONFromArray(ref strRefs[i], json, i);
                            }
                        }
                    }
                }
            }
		}
        else
        {
            Debug.LogWarning("JSON that you were trying to load was null.");
        }
    }

    public void GetJSONFromArray( ref string[] _jsonArray, JSONObject _json, int _index )
    {
        string tempString = _json.GetArray("NoteBook")[_index].ToString();
        JSONObject jsonNotebookItem = JSONObject.Parse(tempString);
        JSONArray tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
        for (int i = 0; i < tempJSONStringArray.Length; i++)
        {
            if( _jsonArray.Length > i )
                _jsonArray[i] = tempJSONStringArray[i].ToString();
        }
    }

	public string GetUserLevePostURL_NewJsonSchema()
	{
		string info = "{";
		info += GetUserLevelData();
		info += "}";

		string m_sLevelsURL =info;

		return m_sLevelsURL;
	}


	/*
	 * This function saves the user data to the server
	 * */
	//CHANGES_KEY_NEW_JSON_SCHEMA - Nothing changes
	public string GetUserLevelData()
	{	
		string levelDataString = "\"Character\":{";
		levelDataString += "\"userGender\":" + "\"" + Global.CurrentPlayerName.ToString() + "\",";
		levelDataString += "\"userHairStyle\":" + Global.userHairStyleNum.ToString() + ",";
		levelDataString += "\"userHairColor\":[" + Global.userHairColor.r.ToString()
			+ "," + Global.userHairColor.g.ToString()
			+ "," + Global.userHairColor.b.ToString()
			+ "," + Global.userHairColor.a.ToString();
		levelDataString += "],\"userSkinColor\":[" + Global.userSkinColor.r.ToString()
			+ "," + Global.userSkinColor.g.ToString()
			+ "," + Global.userSkinColor.b.ToString()
			+ "," + Global.userSkinColor.a.ToString();
		levelDataString += "],\"userEyeColor\":[" + Global.userEyeColor.r.ToString()
			+ "," + Global.userEyeColor.g.ToString()
			+ "," + Global.userEyeColor.b.ToString()
			+ "," + Global.userEyeColor.a.ToString();
		levelDataString += "],\"userClothColor\":[" + Global.userClothColor.r.ToString()
			+ "," + Global.userClothColor.g.ToString()
			+ "," + Global.userClothColor.b.ToString()
			+ "," + Global.userClothColor.a.ToString();
		levelDataString += "],\"userTrimTextureNum\":" + Global.userTrimTextureNum.ToString();
		levelDataString += ",\"userTrimColor\":[" + Global.userTrimColor.r.ToString()
			+ "," + Global.userTrimColor.g.ToString()
			+ "," + Global.userTrimColor.b.ToString()
			+ "," + Global.userTrimColor.a.ToString();
        levelDataString += "],\"userBackPackNum\":" + Global.userBackPackNum.ToString();
        levelDataString += ",\"userBodyType\":" + Global.userBodyType.ToString() + "}";

		return levelDataString;
	}	

	public IEnumerator SaveUserLevelToServer_NewJsonSchema()
	{	
		var postScoreURL = m_sDynamicURL + "Character?sid=" + m_sGameDynamicToken;
		var jsonString = GetUserLevePostURL_NewJsonSchema();
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString();

		if (Application.internetReachability != NetworkReachability.NotReachable) {
			yield return m_LevelDataPostRetriever = new WWW(postScoreURL, encoding.GetBytes(jsonString), postHeader);
			PlayerPrefs.SetString("character_playerprefs", jsonString);

			if(m_LevelDataPostRetriever.isDone){
				if(!string.IsNullOrEmpty(m_LevelDataPostRetriever.error)){
					PlayerPrefs.SetString("flagSaved_character_playerprefs", "false");
				}else{
					PlayerPrefs.SetString("flagSaved_character_playerprefs", "true");
					Debug.Log ("<SAVING CHARACTER>:" + m_LevelDataPostRetriever.text);
				}
			}

		} else {
			PlayerPrefs.SetString("character_playerprefs", jsonString);
			StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
			writer.WriteLine(jsonString);
			writer.Close();
			Debug.Log ("<SAVING CHARACTER - On File: No Connection>:" + jsonString);
			Debug.Log("starting fileRead - connection failed");
			StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
			string content;
			while(null != (content = reader.ReadLine())){
				Debug.Log ("<SAVING CHARACTER>:" + content);
			}
			Debug.Log("fileRead finished");
			reader.Close();
			yield return null;
		}

	}

    // call the /Character api
    // _levelNum not used!
	public IEnumerator ReadUserLevelDataFromServer_NewJsonSchema(int _levelNum, bool bOffline)
	{
		string test = "";

		if (!bOffline) { //Online

//			Debug.Log("Started Player Data Retrival");
//			WWWForm form = new WWWForm();
//			form.AddField("fakefake", "xyxy");


            string characterURL = m_sDynamicURL + "Character?sid=" + m_sGameLoginToken;

            Debug.Log("GET " + characterURL);
            yield return m_LevelDataGetRetriever = new WWW(characterURL);

			if (m_LevelDataGetRetriever.isDone){
				if(!String.IsNullOrEmpty(m_LevelDataGetRetriever.error)){
					//test = getLatestFromFile("progress?");

					test = PlayerPrefs.GetString("character_playerprefs");
					//test = test.Substring(test.IndexOf("jsondata=")+9);
					test = WWW.UnEscapeURL(test);
					//remove the stuff before jsondata
					string thisDebugString = "<CHARACTER PLAYERPREFS>"+test;
					Debug.Log(thisDebugString);
                    if (m_LevelDataGetRetriever.error.Contains("418") == false )
                        Debug.LogError("GET failed " + m_LevelDataGetRetriever.error);
                    else
                        Debug.LogWarning("GET failed. Server thinks it's a fucking teapot again.");
				} else {
					PlayerPrefs.SetString("character_playerprefs",m_LevelDataGetRetriever.text);
					test = m_LevelDataGetRetriever.text;

				}

			}

		} else { //Offline

			test = PlayerPrefs.GetString("character_playerprefs");
			Debug.Log("<CHARACTER PLAYERPREFS>" + test);
		}

		JSONObject json = new JSONObject();
		json = JSONObject.Parse(test);
		if (null != json){
			//json = JSONObject.Parse(test);
			string tempNum;
			string tempFloat;
			string tempString;
			JSONValue tempVal;
			//tempVal= json.GetObject("data").GetArray("levels")[0];
			//tempString = tempVal.ToString();
			//json = JSONObject.Parse(tempString);
			//json = json.GetObject("levelData");

			Global.CurrentPlayerName = json.GetObject("Character").GetString("userGender");
			Debug.Log("Player Data loaded: " + Global.CurrentPlayerName); 

			tempNum = json.GetObject("Character").GetNumber("userHairStyle").ToString();
			Global.userHairStyleNum = int.Parse(tempNum);

			JSONArray hairColor = json.GetObject("Character").GetArray("userHairColor");
			tempFloat = hairColor[0].ToString();
			Global.userHairColor.r = float.Parse(tempFloat);
			tempFloat = hairColor[1].ToString();
			Global.userHairColor.g = float.Parse(tempFloat);
			tempFloat = hairColor[2].ToString();
			Global.userHairColor.b = float.Parse(tempFloat);
			tempFloat = hairColor[3].ToString();
			Global.userHairColor.a = float.Parse(tempFloat);

			JSONArray skinColor = json.GetObject("Character").GetArray("userSkinColor");
			tempFloat = skinColor[0].ToString();
			Global.userSkinColor.r = float.Parse(tempFloat);
			tempFloat = skinColor[1].ToString();
			Global.userSkinColor.g = float.Parse(tempFloat);
			tempFloat = skinColor[2].ToString();
			Global.userSkinColor.b = float.Parse(tempFloat);
			tempFloat = skinColor[3].ToString();
			Global.userSkinColor.a = float.Parse(tempFloat);

			JSONArray eyeColor = json.GetObject("Character").GetArray("userEyeColor");
			tempFloat = eyeColor[0].ToString();
			Global.userEyeColor.r = float.Parse(tempFloat);
			tempFloat = eyeColor[1].ToString();
			Global.userEyeColor.g = float.Parse(tempFloat);
			tempFloat = eyeColor[2].ToString();
			Global.userEyeColor.b = float.Parse(tempFloat);
			tempFloat = eyeColor[3].ToString();
			Global.userEyeColor.a = float.Parse(tempFloat);

			JSONArray clothColor = json.GetObject("Character").GetArray("userClothColor");
			tempFloat = clothColor[0].ToString();
			Global.userClothColor.r = float.Parse(tempFloat);
			tempFloat = clothColor[1].ToString();
			Global.userClothColor.g = float.Parse(tempFloat);
			tempFloat = clothColor[2].ToString();
			Global.userClothColor.b = float.Parse(tempFloat);
			tempFloat = clothColor[3].ToString();
			Global.userClothColor.a = float.Parse(tempFloat);

			tempNum = json.GetObject("Character").GetValue("userTrimTextureNum").ToString();
			Global.userTrimTextureNum = int.Parse(tempNum);

			JSONArray trimColor = json.GetObject("Character").GetArray("userTrimColor");
			tempFloat = trimColor[0].ToString();
			Global.userTrimColor.r = float.Parse(tempFloat);
			tempFloat = trimColor[1].ToString();
			Global.userTrimColor.g = float.Parse(tempFloat);
			tempFloat = trimColor[2].ToString();
			Global.userTrimColor.b = float.Parse(tempFloat);
			tempFloat = trimColor[3].ToString();
			Global.userTrimColor.a = float.Parse(tempFloat);

			tempNum = json.GetObject("Character").GetValue("userBackPackNum").ToString();
			Global.userBackPackNum = int.Parse(tempNum);

            JSONValue bodyVal = json.GetObject("Character").GetValue("userBodyType");
            if( bodyVal != null )
            {
                Global.userBodyType = int.Parse(bodyVal.ToString());
            }
		}
	}

    // TODO: RENAME THIS! It's not a URL
	public string GetUserLevelStartEndEventPostURL_NewJsonSchema(int _eventID, int _levelNum)
	{
		string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		info += ",\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;

		info += "\",\"source\":\"";
		info += platform_type;

		info += "\"}}";

		string m_sEventURL = info;

		return m_sEventURL;
	}

    // TODO: RENAME THIS! It's not a URL
    public string GetUserLevelSavePointEventPostURL_NewJsonSchema(int _eventID, int _levelNum, string SavingPoint)
	{
		string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		//info += "\",\"data\":{\"answers\":[";
		//info += _eventID;
		//info += "]},\"level\":\"";
		info += ",\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;

		info += "\",\"source\":\"";
		info += platform_type;

		info += "\"";
		info += ",\"data\": {";
		info += "\"savepoint\": \"";
		info += SavingPoint;
		info += "\"}}}";

		//string urlEvent = m_sDynamicURL + "event?jsondata=";

		string m_sEventURL = info;

		return m_sEventURL;
	}
    
	public IEnumerator SaveLevelStartEndEventToServer_NewJsonSchema(int _eventID, int _levelNum)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		var jsonString =GetUserLevelStartEndEventPostURL_NewJsonSchema(_eventID, _levelNum);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString ();

		m_sSaveURLReturn = null;

		if (Application.internetReachability != NetworkReachability.NotReachable) {
			
			yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);

			/*while(!m_sSaveURLReturn.isDone){
				Debug.Log ("Waiting for server response EventStart/End...");
			}*/

			Debug.Log ("Game Login Token: " + m_sGameLoginToken);
			Debug.Log ("The user level start end event data POST URL is = " + jsonString);
			if (m_sSaveURLReturn.isDone) {
				if (!string.IsNullOrEmpty (m_sSaveURLReturn.error)) {
					StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
					writer.WriteLine (jsonString);
					writer.Close ();
					Debug.Log ("<EVENT LEVEL START / END - On File: No Connection>:" + jsonString);
					Debug.Log ("starting fileRead - connection failed");
					StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
					string content;
					while (null != (content = reader.ReadLine ())) {
						Debug.Log ("<Content in file>:" + content);
					}
					Debug.Log ("fileRead finished");
					reader.Close ();
				} else {
					Debug.Log ("<EVENT_PHQ>:" + m_sSaveURLReturn.text);
				}
			}
		}else {
			StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
			writer.WriteLine (jsonString);
			writer.Close ();
			Debug.Log ("<EVENT LEVEL START / END - On File: No Connection>:" + jsonString);
			Debug.Log ("starting fileRead - connection failed");
			StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
			string content;
			while (null != (content = reader.ReadLine ())) {
				Debug.Log ("<Content in file>:" + content);
			}
			Debug.Log ("fileRead finished");
			reader.Close ();

		}

	}

	public IEnumerator SavePointEventToServer_NewJsonSchema(int _eventID, int _levelNum, string savePoint)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		//postScoreURL = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/" + "event?sid=" + m_sGameDynamicToken;
		var jsonString =GetUserLevelSavePointEventPostURL_NewJsonSchema(_eventID, _levelNum, savePoint);
		//jsonString = "userEvent: { type: 5,  level: 4, timestamp: \"2016-08-25T13:32:38\" data: { \"savepoint\":\"levelEnd\" } }";
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString ();

		m_sSaveURLReturn = null;

		if (Application.internetReachability != NetworkReachability.NotReachable) {

			yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);

			/*while(!m_sSaveURLReturn.isDone){
				Debug.Log ("Waiting for server response EventStart/End...");
			}*/

			Debug.Log ("Game Login Token: " + m_sGameLoginToken);
			Debug.Log ("The user level start end event data POST URL is = " + jsonString);
			if (m_sSaveURLReturn.isDone) {
				if (!string.IsNullOrEmpty (m_sSaveURLReturn.error)) {
					StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
					writer.WriteLine (jsonString);
					writer.Close ();
					Debug.Log ("<EVENT LEVEL START / END - On File: No Connection>:" + jsonString);
					Debug.Log ("starting fileRead - connection failed");
					StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
					string content;
					while (null != (content = reader.ReadLine ())) {
						Debug.Log ("<Content in file>:" + content);
					}
					Debug.Log ("fileRead finished");
					reader.Close ();
				} else {
					if (m_sSaveURLReturn.isDone) {
						Debug.Log ("<EVENT_PHQ>:" + m_sSaveURLReturn.text);
					}
				}
			}
		}else {
			StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
			writer.WriteLine (jsonString);
			writer.Close ();
			Debug.Log ("<EVENT LEVEL START / END - On File: No Connection>:" + jsonString);
			Debug.Log ("starting fileRead - connection failed");
			StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
			string content;
			while (null != (content = reader.ReadLine ())) {
				Debug.Log ("<Content in file>:" + content);
			}
			Debug.Log ("fileRead finished");
			reader.Close ();

		}

	}
    
	public string GetUserPHQAEventPostURL_NewJsonSchema(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{

		string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		info += ",\"data\":{\"answers\":[";
		info += _Q1A + "," + _Q2A + "," + _Q3A + "," + _Q4A + "," + _Q5A + "," + _Q6A + "," + _Q7A + "," + _Q8A + "," + _Q9A;
		info += "]},\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;

		info += "\",\"source\":\"";
		info += platform_type;

		info += "\"}}";
		string m_sPHQAEventURL = info;
		Debug.Log("m_sPHQAEventURL: " + m_sPHQAEventURL);
		return m_sPHQAEventURL;
	}

	public IEnumerator SaveUserPHQAEventToServer_NewJsonSchema(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		var jsonString = GetUserPHQAEventPostURL_NewJsonSchema(_eventID, _levelNum, _Q1A, _Q2A,  _Q3A,  _Q4A,  _Q5A,  _Q6A,  _Q7A,  _Q8A, _Q9A);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString ();

		m_sSaveURLReturn = null;
		yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);

		if(m_sSaveURLReturn.isDone){

			if(!string.IsNullOrEmpty(m_sSaveURLReturn.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();
				Debug.Log ("<EVENT_PHQ - On File: No Connection>:" + jsonString);
				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log("<DATA IN FILE>" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			}else{
			Debug.Log ("<EVENT_PHQ>:" + m_sSaveURLReturn.text);
			}
		}

		//Debug.Log ("The user level start end event data POST URL is = " + jsonString);
	}
    
	//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string savePointURL_NewJsonSchema(int _levelNum, string _feedback, string _savePoint, bool bNewPageNotebook, bool _sendNotebookData = true){
		
		string saveurl = "";
		/*saveurl += "{\"UserResponse\":{\"LevelNumber\":";
		saveurl += _levelNum;
		saveurl += ",\"SavePoint\":\"";
		saveurl += _savePoint + "\"";
		saveurl += ",\"FeedBackScore\":";
		saveurl += _feedback;
		saveurl += "}}";
		*/

		saveurl += "{\"UserResponse\":{\"LevelNumber\":" + _levelNum.ToString();
		saveurl += ",\"SavePoint\":\"";
		saveurl += _savePoint + "\"";
		saveurl += ", \"FeedBackScore\":[" + Global.userFeedBackScore[0].ToString();
		saveurl += ", " + Global.userFeedBackScore[1].ToString();
		saveurl += ", " + Global.userFeedBackScore[2].ToString() + "]";

        //if(bNewPageNotebook == true )
        //{
            //saveurl += ", ";
        //}

		if (_levelNum > 1 && bNewPageNotebook) {
			Global.lastLevelNoteBookData = Global.lastLevelNoteBookData.TrimEnd (']');
			if(Global.lastLevelNoteBookData.Substring(Global.lastLevelNoteBookData.Length - 1,1) != ","){
				Global.lastLevelNoteBookData += ",";
			}
            saveurl += ", ";
            saveurl += FullNotebookString(_levelNum);
        }
        //  22/01/2018 In the changes made on December 13rd, Level 1 Notebook save was omited,  added this line to save level 1. However I don't know what else was changed. 
        //  24/01/2018 Also I added '|| Global.lastLevelNoteBookData == ""   " Because there is no any control of the notebooks if they are empty from the server. This is should be rubustly fixed later #TODO 
        if ((_levelNum == 1 && bNewPageNotebook) || Global.lastLevelNoteBookData == "") {
            saveurl += ", ";
            saveurl += FullNotebookString(_levelNum);
        }

        //The above code is a fucking mess, so I'm using this to properly add the right number of closing brackets to the JSON string.
            saveurl = AddCorrectNumberOfClosingBrackes(saveurl);
        //Same with commas.
        saveurl = RemoveDoubleCommas( saveurl );

        if (JSONObject.Parse(saveurl) == null)
            Debug.LogError("JSON parsing error!!!:" + saveurl); //Error if we've fucked it.

		Debug.Log ("processDataString: " + saveurl);

		//saveurl = "{\"UserResponse\":{\"LevelNumber\": 7,\"SavePoint\": \"uno\"}}";
		Debug.Log(saveurl);
		string m_sSaveURI = saveurl;
		return m_sSaveURI;
	}

    public string FullNotebookString( int _levelNum )
    {
        string notebookString = "";
        for (int i = 1; i <= _levelNum; ++i)
        {
            if (i != 1)
                notebookString += ",";
            notebookString += NoteBookDataBlocksToPost(i);
        }
        notebookString += "]";
        return notebookString;
    }

    //More stuff to stop the formatting from breaking.
    public string RemoveDoubleCommas( string _txt )
    {
        while( _txt.Contains(",,"))
            _txt = _txt.Replace(",,", ",");
        return _txt;
    }

    //Adds some more squigglies to a JSON string that needs them.
    public string AddCorrectNumberOfClosingBrackes(string _txt)
    {
        int numLeftSquigglies = _txt.Split('{').Length;
        int numRightSquigglies = _txt.Split('}').Length;

        int needThisManyRightSquigglies = numLeftSquigglies - numRightSquigglies;
        if( needThisManyRightSquigglies > 0 )
        {
            for (int i = 0; i < needThisManyRightSquigglies; ++i)
                _txt += "}";
        }

        return _txt;
    }

    public void SaveToServer(bool _saveCurrentNotebookLevel = true, bool _logSave = true, string _savePoint = "levelEnd", bool _sendNotebookData = true  )
    {
        Debug.Log("trigger save point saving.");

        bool levelComplete = true;
        string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
        if(_logSave)
            StartCoroutine(SavePointEventToServer_NewJsonSchema(5, Global.CurrentLevelNumber, _savePoint));
        StartCoroutine(SaveOnServerOrLocal(Global.CurrentLevelNumber, feedbackData, _savePoint, _sendNotebookData));
    }

    public IEnumerator savePointToServer_NewJsonSchema(int _levelNum, string _feedback, string _savePoint, bool _levelComplete, bool bSaveCurrenLevelNotebook, bool _bSendNotebookdata){
		string test;
		string CurrentUserResponse;
		string _savePointString;
		var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
		var jsonString = savePointURL_NewJsonSchema (_levelNum, _feedback, _savePoint, bSaveCurrenLevelNotebook, _bSendNotebookdata); //We need thid Notebook true While testing and wont need it when runing as usual
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
	//	postHeader ["Content-Length"] = jsonString.Length.ToString ();
		//PlayerPrefs.SetString ("savingpoint_playerprefs", jsonString);

		test = jsonString;
		Debug.Log ("savePointToServer(): " + jsonString);
		test = test.Replace(@"\", "");
		test = WWW.UnEscapeURL (test);
		JSONObject json = new JSONObject();

		json = JSONObject.Parse(test);
		string FeedBackScore;
		string SavePoint;
		string SaveLevel;
		string _SaveLevel = _levelNum.ToString();
		JSONValue tempVal;

		SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
		SaveLevel = json.GetObject("UserResponse").GetValue("LevelNumber").ToString();

		if (SavePoint == null) {
			SavePoint = "\"SavePoint\":\"";
		} else {
			SavePoint = "\"SavePoint\":\"" + SavePoint + "\"";
		}

		if (SaveLevel == null) {
			SaveLevel = "\"LevelNumber\":";
		} else {
			SaveLevel = "\"LevelNumber\":" + SaveLevel;
		}

		_savePointString = "\"SavePoint\":\"" + _savePoint + "\""; 
		_SaveLevel = "\"LevelNumber\":" + _SaveLevel; 

		CurrentUserResponse = jsonString;
		Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));



		if (_savePoint != "") {
			CurrentUserResponse = CurrentUserResponse.Replace (SavePoint, _savePointString);
		}
		CurrentUserResponse = CurrentUserResponse.Replace (SaveLevel, _SaveLevel);

		PlayerPrefs.SetString("userresponse_playerprefs", CurrentUserResponse);

		Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + CurrentUserResponse);
		Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));


		if (Application.internetReachability != NetworkReachability.NotReachable) {// Connection Reachable

			m_sSaveURLReturn = null;

			yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
			//PlayerPrefs.SetString ("savingpoint_playerprefs", CurrentUserResponse);
			Debug.Log ("saving Save point data = " + postScoreURL);

            //if (m_sSaveURLReturn.isDone) {
            if (!string.IsNullOrEmpty(m_sSaveURLReturn.error))
            {
                StreamWriter writer = new StreamWriter(Application.persistentDataPath + failedFileName, true);
                writer.WriteLine(jsonString);
                writer.Close();
                Debug.Log("<SAVING POINT - On File: No Connection>:" + jsonString);
                Debug.Log("starting fileRead - connection failed");
                StreamReader reader = new StreamReader(Application.persistentDataPath + failedFileName);
                string content;
                while (null != (content = reader.ReadLine()))
                {
                    Debug.Log("<Data in file>:" + content);
                }
                Debug.Log("fileRead finished");
                reader.Close();
            }
            else
            {
                if (m_sSaveURLReturn.isDone == true)
                    Debug.Log("<SAVING POINT>:" + m_sSaveURLReturn.text);
            }
			//}
		} else {//Connection No Reachable
			StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
			writer.WriteLine (jsonString);
			//PlayerPrefs.SetString ("savingpoint_playerprefs", CurrentUserResponse);
			writer.Close ();
			Debug.Log ("<SAVING POINT - On File: No Connection>:" + jsonString);
			Debug.Log ("starting fileRead - connection failed");
			StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
			string content;
			while (null != (content = reader.ReadLine ())) {
				Debug.Log ("<Data in file>:" + content);
			}
			Debug.Log ("fileRead finished");
			reader.Close ();
			bOfflineFinishLevelSave = true;
			yield return null;
		}
	}
    

	public IEnumerator loadSavePointFromServer_NewJsonSchema(bool bOffline){
		string test ="";

		if (!bOffline) {//OnLine

			Debug.Log ("On loadSavePointFromServer");
			WWWForm loadForm = new WWWForm();
			loadForm.AddField("loadFake", "xyxyxus");

			string m_sLoadURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameLoginToken;

			yield return m_sLoadURLReturn = new WWW(m_sLoadURL);

			if (m_sLoadURLReturn.isDone) {

				Debug.Log ("<SAVEPOINT>:" + m_sLoadURLReturn.text);

				test = m_sLoadURLReturn.text;
			}
		} else {//Offline
			test = PlayerPrefs.GetString ("userresponse_playerprefs");
			Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
			string thisDebugString = "<SAVING POINT PLAYERPREFS>:" + test;
		}

		JSONObject json = new JSONObject();
		json = JSONObject.Parse(test);
		string tempNum;
		string tempString;
		bool tempBool;
		JSONValue tempVal;
		Debug.Log ("THIS IS THE LABEL 2 -->" + json);
		if (null != json) {

			if (null != json.GetObject ("UserResponse")) {

				tempVal = json.GetObject ("UserResponse");
				tempString = tempVal.ToString ();
				json = JSONObject.Parse (tempString); 

				if (json.GetString ("SavePoint") == null) {
					tempString = "levelEnd";// ANGELA ASK FOR SWAP NULL FOR levelEnd
				} else {
					tempString = json.GetString ("SavePoint");
				}
				/*if(tempString == "False"){// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
				tempBool = false;
			}else{
				tempBool = true;
			}*/// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
				if (tempString == null) {
					tempString = "";
					tempBool = false;
				} else {
					if (!tempString.Contains ("levelEnd")) {// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
						tempBool = false;
					} else {
						tempBool = true;
					}
				}
				if (Application.isEditor && Global.TestingOnline == true) {
					tempBool = false;
				}
				if (!tempBool) {
					Debug.Log ("level not yet ended");
					JSONArray tempArray = json.GetArray ("FeedBackScore");
					tempNum = tempArray [0].ToString ();
					Global.userFeedBackScore [0] = int.Parse (tempNum);
					tempNum = tempArray [1].ToString ();
					Global.userFeedBackScore [1] = int.Parse (tempNum);
					tempNum = tempArray [2].ToString ();
					Global.userFeedBackScore [2] = int.Parse (tempNum);

					Global.loadedSavePoint = tempString;

					Global.levelComplete = tempBool;

					tempNum = json.GetValue ("LevelNumber").ToString ();
					Global.savePointLoadLevelNumber = int.Parse (tempNum);
					if (Application.isEditor && Global.TestingOnline == true) {
						Global.loadedSavePoint = Global.TestSavePoint;
					}
				} else {
					if (Global.CurrentLevelNumber == 1) {
						Global.levelComplete = !tempBool;
						Global.loadedSavePoint = "";
					} else {
						Global.levelComplete = tempBool;
						Global.loadedSavePoint = tempString;
					}
					Debug.Log ("Previous level complete, loading new level.");
				}
				Debug.Log ("The save point form the server is: " + Global.loadedSavePoint);
			}
		}
	}


    public void testForConnectionFailed(string whatURL, bool ConnectionSuccessful)
    { // (Connection, WhatRL, wwwForm)

        Debug.Log("this is bunning");
        //if(Application.isMobilePlatform){
        Debug.Log("this is running is mobile");
        //if(connection.isDone){//DIEGO CONNECTION WAS SENT FROM ANOTHER ROUTINE. IF IS NECESSARY WILL BE FINE TO EVALUATE IT BEFORE THIS ROUTINE
        Debug.Log("this is running connection is done");

        //OfflineFileUploader.getInstance.startFileUpload(Application.persistentDataPath+failedFileName);//JUST FOR TEST DIEGO
        //if(!!string.IsNullOrEmpty(connection.error)){
        if (!ConnectionSuccessful)
        {
            StreamWriter writer = new StreamWriter(Application.persistentDataPath + failedFileName, true);
            writer.WriteLine(whatURL);
            writer.Close();

            Debug.Log("starting fileRead - connection failed");
            StreamReader reader = new StreamReader(Application.persistentDataPath + failedFileName);
            string content;
            while (null != (content = reader.ReadLine()))
            {
                Debug.Log(content);
            }
            Debug.Log("fileRead finished");
            reader.Close();
        }
        else if (File.Exists(Application.persistentDataPath + failedFileName))
        {
            Debug.Log("cGoingtoUploadScript");
            OfflineFileUploader.getInstance.startFileUpload(Application.persistentDataPath + failedFileName);
        }

    }
    
	public IEnumerator subConnect3(string sJsonString)
	{
		int i = 0;
		var postScoreURL = "";

		if(sJsonString.Contains("userEvent")){
			postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		}
		if(sJsonString.Contains("Character")){
			postScoreURL = m_sDynamicURL + "Character?sid=" + m_sGameDynamicToken;
		}
		if(sJsonString.Contains("UserResponse")){
			postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
		}

		Debug.Log ("...SUBCONNECT3...URL<>" + postScoreURL);
		Debug.Log ("...SUBCONNECT3...JSONSTRING<>" + sJsonString);

		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
//		postHeader ["Content-Length"] = sJsonString.Length.ToString ();

		yield return m_txtDataSender = new WWW (postScoreURL, encoding.GetBytes (sJsonString), postHeader);

		if(m_txtDataSender.isDone){
			if(!String.IsNullOrEmpty(m_txtDataSender.error)){
				string test = "<Sent LOCAL SAVE TO SERVER>:" + m_txtDataSender.text;
				Debug.Log(test + "-" + i);
			}
		}
	}

    string getLatestFromFile(string jsonPrefix)
    {
        //this should actually load from playerprefs because it will be faster.
        Debug.Log("trying to load from file");
        StreamReader reader = new StreamReader(Application.persistentDataPath + failedFileName);
        string content;
        string result = "";
        while (null != (content = reader.ReadLine()))
        {
            if (content.IndexOf(jsonPrefix) != -1)
            {
                result = content;
            }
        }
        return result;
    }

	public void unLoadAllContent(){
		m_sGameLoginToken   = "";
		m_sGameDynamicToken = "";

	
		m_iServerLevelNum   = 0;
		m_sSaveProgressURL  = "";
		m_sReadProgressURL  = "";
		
		m_dynamiURLRetriever = null;
		
		//Progress
		m_ProcessDataPostRetriever = null;
		m_ProcessDataGetRetriever = null;
		m_ProcessNotebookDataGetRetriever = null;
		//Level
		m_LevelDataPostRetriever = null;
		m_LevelDataGetRetriever = null;
		//Event
		m_EventLevelStartEndDataPostRetriever = null;
		m_EventPHQADataPostRetriever = null;
		
		m_sLoadURLReturn = null;
		m_sSaveURLReturn = null;
		
		m_bFoundLevelNumber            = false;
		
		//private variables
		m_bLoggedInConfirmed          = false;
		m_dynamicLoginSuccessful      = false;
		m_loginSuccessful             = false;
		m_sNotebookHeader             = "";
	}

	public IEnumerator SaveOnServerOrLocal(int _levelNum, string _feedback, string _savePoint, Boolean bNewPageNotebook){ // (Connection, WhatRL, wwwForm)
		//subFunctionTestAddingSavePointstoFile(whatURL);//Fill the 
		JSONObject json;
		string SavePoint;
		string SaveLevel;
		string _SaveLevel;
		string _savePointString;

		string test;
		string CurrentUserResponse;
		Debug.Log ("this is running");

		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm ();
		saveForm.AddField ("saveFake", "xyxyx");

		var jsonString = "";
		jsonString = savePointURL_NewJsonSchema (_levelNum, _feedback, _savePoint, bNewPageNotebook);
		var encoding = new System.Text.UTF8Encoding ();
				
		if (Application.internetReachability != NetworkReachability.NotReachable) {// Connection Reachable
			
			//if (string.IsNullOrEmpty (instance.m_sDynamicURL) || string.IsNullOrEmpty (instance.m_sGameDynamicToken)) { //Notoken or dynamic URL Comented 31/08/2016

				m_dynamiURLRetriever = null;

				//Get token from fixed username and password
				//yield return StartCoroutine (GetUserLogInToServer_NewSchema ());
				
				//2nd Revision Login Screen 30/09/2016
				yield return StartCoroutine (SendOffGameConfigURL_NewJsonSchema (m_sGameConfigServerURL)); //EDITOR GETS DYNAMIC URL FROM SERVER
				Debug.Log ("<Token & Dynamic URL from Server>");
			
				if (m_dynamiURLRetriever != null && !string.IsNullOrEmpty (m_dynamiURLRetriever.error) || string.IsNullOrEmpty (m_sDynamicURL)) { //NO RESPONSE ON LOGIN
				//PlayerPrefs.SetString ("savingpoint_playerprefs", jsonString);

					test = jsonString;
					Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
					test = test.Replace(@"\", "");
					test = WWW.UnEscapeURL (test);

					json = JSONObject.Parse(test);
					SavePoint = "";
					SaveLevel = "";
					_SaveLevel = _levelNum.ToString();

					SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
					SaveLevel = json.GetObject("UserResponse").GetValue("LevelNumber").ToString();

					if (SavePoint == null) { 
					SavePoint = "\"SavePoint\":\"\"";
					} else {
						SavePoint = "\"SavePoint\":\"" + SavePoint + "\"";
					}

					if (SaveLevel == null) {
						SaveLevel = "\"LevelNumber\":";
					} else {
						SaveLevel = "\"LevelNumber\":" + SaveLevel;
					}

					_savePointString = "\"SavePoint\":\"" + _savePoint + "\""; 
					_SaveLevel = "\"LevelNumber\":" + _SaveLevel; 

					CurrentUserResponse = jsonString;
					Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
					if (_savePoint != "") {
						CurrentUserResponse = CurrentUserResponse.Replace (SavePoint, _savePointString);
					}
					CurrentUserResponse = CurrentUserResponse.Replace (SaveLevel, _SaveLevel);						
					PlayerPrefs.SetString("userresponse_playerprefs", CurrentUserResponse);
					Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + CurrentUserResponse);
					Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));

					StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
					writer.WriteLine (jsonString);
					writer.Close ();
					//REading the file to show current content
					Debug.Log ("starting fileRead - connection failed");
					StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
					string content;
					while (null != (content = reader.ReadLine ())) {
						Debug.LogWarning ("<FAILED STRING IN FILE>" + content);
					}
					Debug.Log ("fileRead finished");
					reader.Close ();

				} else { //OK RESPONSE ON LOGIN

					if (m_dynamiURLRetriever != null && m_dynamiURLRetriever.isDone) {
						GetTokenAndDynamicURL_NewJsonSchema ();
						Debug.Log ("<Token & Dynamic URL from Server>");
					}

					if (File.Exists (Application.persistentDataPath + failedFileName)) {
						Debug.Log ("cGoingtoUploadScript");
						//startFileUpload ();

						yield return StartCoroutine(startFileUpload ());

						yield return StartCoroutine (Wait (10));

					} 
					//SENT THE Sending the current saving after queue

					var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
					Dictionary<string, string> postHeader = new Dictionary<string, string> ();
					postHeader ["Content-Type"] = "application/json";
				//	postHeader ["Content-Length"] = jsonString.Length.ToString ();
					WWW www;
					yield return www = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
					Debug.Log ("saving Save point data = " + postScoreURL);

					//PlayerPrefs.SetString ("savingpoint_playerprefs", jsonString);

					test = jsonString;
					Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
					test = test.Replace(@"\", "");
					test = WWW.UnEscapeURL (test);

					json = JSONObject.Parse(test);
					SavePoint = "";
					SaveLevel = "";
					_SaveLevel = _levelNum.ToString();


					SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
					SaveLevel = json.GetObject("UserResponse").GetValue("LevelNumber").ToString();

					if (SavePoint == null){
						SavePoint = "\"SavePoint\":\"\"";
					} else {
						SavePoint = "\"SavePoint\":\"" + SavePoint + "\"";
					}

					if (SaveLevel == null) {
						SaveLevel = "\"LevelNumber\":";
					} else {
						SaveLevel = "\"LevelNumber\":" + SaveLevel;
					}

					_savePointString = "\"SavePoint\":\"" + _savePoint + "\""; 
					_SaveLevel = "\"LevelNumber\":" + _SaveLevel; 

					CurrentUserResponse = jsonString;
					Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
					if (_savePoint != "") {
						CurrentUserResponse = CurrentUserResponse.Replace (SavePoint, _savePointString);
					}

					CurrentUserResponse = CurrentUserResponse.Replace (SaveLevel, _SaveLevel);
					PlayerPrefs.SetString("userresponse_playerprefs", CurrentUserResponse);
					Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + CurrentUserResponse);
					Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));


					if (www.isDone) {
						Global.LastResponseFromServer = www.text.ToString();
						if (!string.IsNullOrEmpty (www.error)) {
							StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
							writer.WriteLine (jsonString);
							writer.Close ();

							Debug.Log ("starting fileRead - connection failed");
							StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
							string content;
							while (null != (content = reader.ReadLine ())) {
								Debug.Log ("<FAILED STRING IN FILE>" + content);
							}
							Debug.Log ("fileRead finished");
							reader.Close ();
						}
					}
				}
			//}

			}else{// Connection NotReachable

			//PlayerPrefs.SetString ("savingpoint_playerprefs", jsonString);

			test = jsonString;
			Debug.Log ("<USEERRESPONSE PLAYPREFS> GET:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
			test = test.Replace(@"\", "");
			test = WWW.UnEscapeURL (test);

			json = JSONObject.Parse(test);
			SavePoint = "";
			SaveLevel = "";
			_SaveLevel = _levelNum.ToString();

			SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
			SaveLevel = json.GetObject("UserResponse").GetValue("LevelNumber").ToString();

			if (SavePoint == null) {
				SavePoint = "\"SavePoint\":\"\"";
			} else {
				SavePoint = "\"SavePoint\":\"" + SavePoint + "\"";
			}

			if (SaveLevel == null) {
				SaveLevel = "\"LevelNumber\":";
			} else {
				SaveLevel = "\"LevelNumber\":" + SaveLevel;
			}

			_savePointString = "\"SavePoint\":\"" + _savePoint + "\""; 
			_SaveLevel = "\"LevelNumber\":" + _SaveLevel; 

			CurrentUserResponse = jsonString;

			if (_savePoint != "") {
				CurrentUserResponse = CurrentUserResponse.Replace (SavePoint, _savePointString);
			}
			CurrentUserResponse = CurrentUserResponse.Replace (SaveLevel, _SaveLevel);

			PlayerPrefs.SetString("userresponse_playerprefs", CurrentUserResponse);
			Debug.Log ("<USEERRESPONSE PLAYPREFS> SET:" + CurrentUserResponse);
			Debug.Log ("<USEERRESPONSE PLAYPREFS> VALUE:" + PlayerPrefs.GetString ("userresponse_playerprefs"));

			StreamWriter writer = new StreamWriter (Application.persistentDataPath + failedFileName, true);
			writer.WriteLine (jsonString);
			writer.Close ();
			//REading the file to show current content
			Debug.Log ("starting fileRead - connection failed");
			StreamReader reader = new StreamReader (Application.persistentDataPath + failedFileName);
			string content;
			while (null != (content = reader.ReadLine ())) {
				Debug.Log ("<FAILED STRING IN FILE>" + content);
			}
			Debug.Log ("fileRead finished");
			reader.Close ();

			yield return null;

		}
		Global.toggleGUI = false;
	}

	public IEnumerator startFileUpload(){ //TEST VERSION DIEGO
        Debug.Log("startFileUpload()");

        bUploadFileComplete = false;
		string[] allLines = new string[1];
		StreamReader reader = new StreamReader(Application.persistentDataPath + failedFileName);
		//Debug.Log(reader.ToString());
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			allContent.Add(content);
		}

        reader.Close(); 
//		Debug.Log(allContent.Count);
		if(allContent.Count > 0){
			//fileName = newFileName;
			allLines = allContent.ToArray();
			foreach (string jsonStr in allLines){
				m_txtDataSender = null;
				yield return StartCoroutine(subConnect3(jsonStr.Replace(" ","")));
				/*while(!m_txtDataSender.isDone){
					Debug.Log("Waiting for Server Response Saving File Information...");
					yield return null;
				}*/
			}
		}
		File.Delete(Application.persistentDataPath+failedFileName);
        Debug.Log("startFileUpload() - file deleted");
        bUploadFileComplete = true;
	}

	public IEnumerator Wait(float seconds)
	{
		
		yield return new WaitForSeconds(seconds);
		bWaitingForSeconds = false;
	}

}