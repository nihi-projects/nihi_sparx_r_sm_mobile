﻿using UnityEngine;
using System.Collections;

public class levelSaveTrigger : MonoBehaviour {

	private CapturedDataInput instance;

	public string levelMidSavePoint;

	// Use this for initialization
	void Start () {
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
        if (GameObject.Find("CapturedDataInputHolder"))
		    instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SaveToServer(){
		
		Debug.Log("trigger save point saving: " + levelMidSavePoint);
		
		bool levelComplete = false;
		
		string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
		
		if(instance != null && Global.loadedSavePoint != levelMidSavePoint){
			StartCoroutine (instance.SavePointEventToServer_NewJsonSchema(5,Global.CurrentLevelNumber,levelMidSavePoint));
			StartCoroutine(instance.SaveOnServerOrLocal(Global.CurrentLevelNumber, feedbackData,levelMidSavePoint,false));
			//StartCoroutine(instance.savePointToServer_NewJsonSchema(Global.CurrentLevelNumber, feedbackData, levelMidSavePoint, levelComplete,false));
		}
	}

	void OnTriggerEnter(Collider other){
		string colliderName = this.gameObject.name;
		Debug.Log ("Save Point Trigger Collider Name: " + colliderName);
		if(Global.CurrentLevelNumber == 1){
			levelMidSavePoint = "L1GameMid";
			SaveToServer();
		}
		if(Global.CurrentLevelNumber == 2){
			if(colliderName == "saveTrigger1"){
				levelMidSavePoint = "L2GameMid1";
				SaveToServer();
			}else if(colliderName == "saveTrigger2"){
				levelMidSavePoint = "L2GameMid2";
				SaveToServer();
			}
		}
		if(Global.CurrentLevelNumber == 3){
			if(colliderName == "saveTrigger1"){
				levelMidSavePoint = "L3GameMid1";
				SaveToServer();
			}else if(colliderName == "saveTrigger2"){
				levelMidSavePoint = "L3GameMid2";
				SaveToServer();
			}
		}
		if(Global.CurrentLevelNumber == 4){
			levelMidSavePoint = "L4GameMid";
			SaveToServer();
		}
		if(Global.CurrentLevelNumber == 5){
			levelMidSavePoint = "L5GameMid";
			SaveToServer();
		}
		if(Global.CurrentLevelNumber == 6){
			if(colliderName == "saveTrigger1"){
				levelMidSavePoint = "L6GameMid1";
				SaveToServer();
			}else if(colliderName == "saveTrigger2"){
				levelMidSavePoint = "L6GameMid2";
				SaveToServer();
			}else if(colliderName == "saveTrigger3"){
				levelMidSavePoint = "L6GameMid3";
				SaveToServer();
			}else if(colliderName == "saveTrigger4"){
				levelMidSavePoint = "L6GameMid4";
				SaveToServer();
			}else if(colliderName == "saveTrigger5"){
				levelMidSavePoint = "L6GameMid5";
				SaveToServer();
			}
		}
		if(Global.CurrentLevelNumber == 7){
			if(colliderName == "saveTrigger1"){
				levelMidSavePoint = "L7GameMid1";
				SaveToServer();
			}else if(colliderName == "saveTrigger2"){
				levelMidSavePoint = "L7GameMid2";
				SaveToServer();
			}else if(colliderName == "saveTrigger3"){
				levelMidSavePoint = "L7GameMid3";
				GameObject.Find("saveTrigger2").SetActive(false);
				SaveToServer();
			}
		}
	}
}
