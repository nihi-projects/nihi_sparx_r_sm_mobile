﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class levelSaveOnLoad : MonoBehaviour {
	
	private CapturedDataInput instance;
	private string currentSavePoint;
	private bool levelComplete;
	private string feedbackData;
	public static GameObject saveTriggerToggle;
	string failedFileName = @"/failedRequests.txt";
	// Use this for initialization
	void Start () {
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
        if(GameObject.Find("CapturedDataInputHolder"))
		    instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		SaveToServer();

		Debug.Log ("TEST (levelSaveOnLoad @ line 18)");
		/*saveTriggerToggle = GameObject.Find("saveTrigger1");
		if(Application.loadedLevel == 3){
			saveTriggerToggle.SetActive(false);
		}*/
		//why was that there??
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SaveToServer(){


		Global.levelSSavePoint(Global.CurrentLevelNumber);
		currentSavePoint = Global.m_sSavePointCode;
		
		levelComplete = false;
		
		feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
		if(!Global.loadedSavePoint.Contains(currentSavePoint)){
			Debug.Log("starting save point saving.");
			Global.sSavePointToServerURL = "";
			IEnumerator testVal;
			//StartCoroutine(testVal = instance.savePointToServer(Global.CurrentLevelNumber, feedbackData, currentSavePoint, levelComplete));
			//Make sure savePointToServer runns before testForConnectionFailed because savePointToServer gives value to bsavePointToServerSuccess used in testForConnectionFailed
			//if(!CapturedDataInput.m_sSaveURLReturn.isDone)
			//{
		    //	Debug.Log(CapturedDataInput.m_sSaveURLReturn.isDone);
            if( instance != null)
            {
                StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, Global.CurrentLevelNumber, currentSavePoint));
                StartCoroutine(instance.SaveOnServerOrLocal(Global.CurrentLevelNumber, feedbackData, currentSavePoint, false));
            }
			
			//It WORKS startFileUpload2(); //DIEGO COMENTED THE CALL INTO SAVEPOINT TO SERVER AND ADD THIS CALL
			//Debug.Log("current progress: "+CapturedDataInput.m_sSaveURLReturn.bytesDownloaded.ToString());

			//}
		}
	}



}
