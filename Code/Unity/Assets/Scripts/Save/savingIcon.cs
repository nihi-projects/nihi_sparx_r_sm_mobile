﻿using UnityEngine;
using System.Collections;

public class savingIcon : MonoBehaviour {

	public Texture saveShieldIcon;

	// Use this for initialization
	void Start () {
		GameObject.Find("loadingIcon").GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.toggleGUI == true){
			//GUI.Label(new Rect(Screen.width*0.8f, Screen.height*0.8f, Screen.width*0.12f, Screen.height*0.12f), saveShieldIcon);
			GameObject.Find("loadingIcon").GetComponent<Renderer>().enabled = true;

			Vector3 eulerAngles = GameObject.Find("loadingIcon").GetComponent<Transform>().rotation.eulerAngles;
			eulerAngles.z += 1;
			GameObject.Find("loadingIcon").GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
		}else{
			GameObject.Find("loadingIcon").GetComponent<Renderer>().enabled = false;
		}
	}

	void OnGUI() {
		//GUI.Label (new Rect (10, 10, Screen.width - 10, 60), Global.LastResponseFromServer);
	}

}
