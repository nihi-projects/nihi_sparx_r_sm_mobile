﻿using UnityEngine;
using System.Collections;

public class loadDictionaryVars : MonoBehaviour {

	public static Vector3 tempPosition;

	// Use this for initialization
	void Start () {
	} 

	// Update is called once per frame
	void Update () {
	
	}

	public static void saveVars(){
		if(Global.CurrentLevelNumber == 1){
			GameObject.Find ("1npc").GetComponent<CharacterInteraction>().Interacted = true;
			GameObject.Find ("FightGnats").GetComponent<GnatsCombat>().m_bIsTriggered = true;
			GameObject.Find ("FightGnats").GetComponent<GnatsCombat>().bCombatEnded = true;
			GameObject.Find ("FightGnats").GetComponent<GnatsCombat>().enabled = false;
			GameObject.Find ("FightGnats").GetComponent<GnatsInteraction>().enabled = false;
			GameObject.Find ("FightGnats").GetComponent<GnatsMovement>().enabled = false;
			GameObject.Find	("_[id]31019_chestlid").GetComponent<ObjectMovement>().m_bStartMoving = true;
			GameObject.Find ("OpenChest").GetComponent<OpenChest>().enabled = true;
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31022");
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31025");
			GameObject.Find ("OpenChest").GetComponent<OpenChest>().m_bTuiStartFlying = true;
			GameObject.Find ("OpenChest").GetComponent<OpenChest>().enabled = false;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene2";
			Global.tuiScene = "HopeScene2";
		}
		if(Global.CurrentLevelNumber == 2){
			if(Global.loadedSavePoint.Contains("GameMid1")){
				Global.GetGameObjectByID("32009").GetComponent<ObjectMovement>().m_bStartMoving = true;
				Global.GetGameObjectByID("32010").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				Global.CurrentInteractNPC = "Cass";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene2";
				TerminateTalkScene.interactObject = GameObject.Find("Cass");
			}else if(Global.loadedSavePoint.Contains("GameMid2")){
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("32020");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene2";
				Global.CurrentInteractNPC = "Hope";
			}
		}
		if(Global.CurrentLevelNumber == 3){
			if(Global.loadedSavePoint.Contains("GameMid1")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3TuiTerminateScene";
				tempPosition.x = -90;
				tempPosition.y = 9;
				tempPosition.z = -70;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("33051");
				GameObject.Find ("FirespiritA").GetComponent<FireSpritFlyAway>().m_bMovingToTarget = true;
				Global.CurrentInteractNPC = "Hope";
				TerminateTalkScene.interactObject = GameObject.Find("npclava");
			}else if(Global.loadedSavePoint.Contains("GameMid2")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3TuiTerminateScene";
				tempPosition.x = -83;
				tempPosition.y = 4;
				tempPosition.z = -100;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("33046");
				GameObject.Find ("FirespiritA").GetComponent<FireSpritFlyAway>().m_bMovingToTarget = true;
				Global.CurrentInteractNPC = "Hope";
				TerminateTalkScene.interactObject = GameObject.Find("Hope");
				GameObject.Find("npclava2").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("RockGame").SetActive(false);
			}
		}
		if(Global.CurrentLevelNumber == 4){
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L4TuiTerminateScene";
			tempPosition.x = 40;
			tempPosition.y = 52;
			tempPosition.z = 0;
			GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("34031");
			Global.CurrentInteractNPC = "Hope";
		}
		if(Global.CurrentLevelNumber == 5){
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L5TuiTerminateScene";
			tempPosition.x = 40;
			tempPosition.y = 10;
			tempPosition.z = 20;
			GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
			Global.CurrentInteractNPC = "Hope";
		}
		if(Global.CurrentLevelNumber == 6){
			if(Global.loadedSavePoint.Contains("GameMid1")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6TuiWomanBridgelandSwitch");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6TuiTerminateScene";
				Global.CurrentInteractNPC = "Hope";
				tempPosition.x = 42;
				tempPosition.y = 7;
				tempPosition.z = 22;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("90001");
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("Level6").GetComponent<LevelSixScript>().iCurrentMiniGameIndex = 1;
			}else if(Global.loadedSavePoint.Contains("GameMid2")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6TuiWomanBridgelandSwitch");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6TuiTerminateScene";
				Global.CurrentInteractNPC = "Hope";
				tempPosition.x = 70;
				tempPosition.y = 7;
				tempPosition.z = 27;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("90002");
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("Level6").GetComponent<LevelSixScript>().iCurrentMiniGameIndex = 2;
			}else if(Global.loadedSavePoint.Contains("GameMid3")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6TuiWomanBridgelandSwitch");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6TuiTerminateScene";
				tempPosition.x = 89;
				tempPosition.y = 7;
				tempPosition.z = 15;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("90003");
				Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("Level6").GetComponent<LevelSixScript>().iCurrentMiniGameIndex = 3;
			}else if(Global.loadedSavePoint.Contains("GameMid4")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6TuiWomanBridgelandSwitch");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6TuiTerminateScene";
				Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation4").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation4").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("Level6").GetComponent<LevelSixScript>().iCurrentMiniGameIndex = 4;
				tempPosition.x = 98;
				tempPosition.y = 4;
				tempPosition.z = 9;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("36051");
			}else if(Global.loadedSavePoint.Contains("GameMid5")){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6TuiTempleGuardianSwitch");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6TuiTerminateScene";
				Global.CurrentInteractNPC = "";
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation1").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation2").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation3").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("GnatsConversation4").GetComponentInChildren<DisplayQuestion>().bCorrectAnswer = true;
				GameObject.Find ("GnatsConversation4").GetComponentInChildren<DisplayQuestion>().bFinishedAnswering = true;
				GameObject.Find ("Level6").GetComponent<LevelSixScript>().iCurrentMiniGameIndex = 4;
				tempPosition.x = 64;
				tempPosition.y = 10;
				tempPosition.z = -13;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("360961");
				GameObject.Find("6_npc2").GetComponent<CharacterInteraction>().Interacted = true;
			}
		}
		if(Global.CurrentLevelNumber == 7){
			if(Global.loadedSavePoint.Contains("GameMid1")){
				Global.GetPlayer().GetComponent<PlayerMovement>().m_bTuiMovementAttach = false;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7TuiTerminateScene";
				tempPosition.x = -132;
				tempPosition.y = -5;
				tempPosition.z = -16;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37011");
				GameObject.Find ("Traveller").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterWalkAway>().m_bMovingToTarget = true;
				Global.CurrentInteractNPC = "Swampguy";
				TerminateTalkScene.interactObject = GameObject.Find("Swampguy");
				string[] theDoors = new string[]{"_[id]37002_gateleft", "_[id]37003_gateright", "_[id]37005_gateleft01", "_[id]37006_gateright01"};
				foreach(string door in theDoors){
					ObjectMovement obMove = GameObject.Find(door).GetComponentInChildren<ObjectMovement>();
					obMove.m_bStartMoving = true;
				}
			}else if(Global.loadedSavePoint.Contains("GameMid2")){
				Global.GetPlayer().GetComponent<PlayerMovement>().m_bTuiMovementAttach = false;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7TuiTerminateScene";
				tempPosition.x = -70;
				tempPosition.y = 20;
				tempPosition.z = -5;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37048");
				GameObject.Find ("Traveller").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Fireperson").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterWalkAway>().m_bMovingToTarget = true;
				Global.CurrentInteractNPC = "Hope";
				TerminateTalkScene.interactObject = GameObject.Find("Fireperson");
			}else if(Global.loadedSavePoint.Contains("GameMid3")){

				Global.GetPlayer().GetComponent<PlayerMovement>().m_bTuiMovementAttach = false;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("TerminateTalkScene");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7TuiTerminateScene";
				tempPosition.x = -70;
				tempPosition.y = 20;
				tempPosition.z = -5;
				GameObject.Find ("Hope").GetComponent<Transform>().position = tempPosition;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37048");
				GameObject.Find ("Traveller").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Fireperson").GetComponent<CharacterInteraction>().Interacted = true;
				GameObject.Find ("Swampguy").GetComponent<CharacterWalkAway>().m_bMovingToTarget = true;
				Global.CurrentInteractNPC = "Hope";
				TerminateTalkScene.interactObject = GameObject.Find("Fireperson");
				GameObject.Find("BarrelGame").SetActive(false);
				GameObject.Find("_[id]37310_rock1").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("_[id]37311_rock2").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("_[id]37312_rock3").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("_[id]37313_rock4").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("_[id]37314_rock5").GetComponent<ObjectMovement>().m_bStartMoving = true;
				GameObject.Find("saveTrigger2").SetActive(false);
			}

		}
	}

	public static void loadPosition(){
		if(Global.loadedSavePoint.Contains("GameMid1")){
			Global.loadPos = GameObject.Find ("saveTrigger1").GetComponent<Transform>().position;
		}else if(Global.loadedSavePoint.Contains("GameMid2")){
			Global.loadPos = GameObject.Find ("saveTrigger2").GetComponent<Transform>().position;
		}else if(Global.loadedSavePoint.Contains("GameMid3")){
			Global.loadPos = GameObject.Find ("saveTrigger3").GetComponent<Transform>().position;
		}else if(Global.loadedSavePoint.Contains("GameMid4")){
			Global.loadPos = GameObject.Find ("saveTrigger4").GetComponent<Transform>().position;
		}else if(Global.loadedSavePoint.Contains("GameMid5")){
			Global.loadPos = GameObject.Find ("saveTrigger5").GetComponent<Transform>().position;
		} else if(Global.loadedSavePoint.Contains("GameMid")){
			Transform savePointTransform = GameObject.Find ("saveTrigger").GetComponent<Transform>();
			Global.loadPos = savePointTransform.position;
			Debug.Log("loadPos:"+Global.loadPos.ToString());
			//Global.loadPos = GameObject.Find ("saveTrigger").GetComponent<Transform>().position;
		}
	}

}