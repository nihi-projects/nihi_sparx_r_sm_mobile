﻿using UnityEngine;
using System.Collections;

public class FontSizeManager : MonoBehaviour{
	private static bool done = false;
	public static void checkFontSize(GUISkin skin){
		if(skin == null){
			return;
		}

		//if(Application.isMobilePlatform){
			/*if(!done && FontSizeManager.isiPad()){
				skin.box.fontSize *= 2;
				skin.button.fontSize *= 2;
				skin.label.fontSize *= 2;
				skin.textArea.fontSize *= 2;
				skin.textField.fontSize *= 2;
				foreach(GUIStyle style in skin.customStyles){
					style.fontSize *= 2;
				}
			}*/
		//}

		string FontInformation = "";
		int counter;
		counter = 0;

		skin.box.fontSize = 0;
		skin.button.fontSize = 15; 
		skin.label.fontSize  = 16;
		skin.textArea.fontSize = 0;
		skin.textField.fontSize = 16;
		skin.toggle.fontSize = 15;
		skin.toggle.fixedWidth = 16;
		skin.toggle.contentOffset = new Vector2(16,0);

		foreach(GUIStyle style in skin.customStyles){
			counter++;
			switch (counter){
			case 5:
				style.fontSize = 14;
				break;
			case 6:
				style.fontSize = 25; 
				break;
			case 7:
				style.fontSize = 34; 
				break;
			case 24:
				style.fontSize = 16; 
				break;
			case 29:
				style.fontSize = 14;
				break;
			case 30:
				style.fontSize = 11;
				break;
			case 31:
				style.fontSize = 18;
				break;
			case 32:
				style.fontSize = 18;
				break;
			case 33:
				style.fontSize = 18;
				break;
			case 34:
				style.fontSize = 16;
				break;
			case 35:
				style.fontSize = 16;
				break;
			case 36:
				style.fontSize = 16;
				break;
			case 38:
				style.fontSize = 34;
				break;
			case 39:
				style.fontSize = 20;
				break;
			case 40:
				style.fontSize = 20;
				break;
			case 41:
				style.fontSize = 20;
				break;
			case 43:
				style.fontSize = 18;
				break;
			case 44:
				style.fontSize = 25;
				break;
			case 45:
				style.fontSize = 70;
				break;
			case 46:
				style.fontSize = 35;
				break;
			}

		}

		//emulaRetinaDPI ();

		FontSizeManager.IncreaseSize(skin);

		done = true;

	}

	private static void IncreaseSize(GUISkin skin){

		skin.box.fontSize = Mathf.RoundToInt((float)skin.box.fontSize * (float)Global.DPI_Factor);
		skin.button.fontSize = Mathf.RoundToInt((float)skin.button.fontSize * Global.DPI_Factor);
		skin.label.fontSize = Mathf.RoundToInt((float)skin.label.fontSize * Global.DPI_Factor);
		skin.textArea.fontSize = Mathf.RoundToInt((float)skin.textArea.fontSize * Global.DPI_Factor);
		skin.textField.fontSize = Mathf.RoundToInt((float)skin.textField.fontSize * Global.DPI_Factor);
		skin.toggle.fontSize = Mathf.RoundToInt((float)skin.toggle.fontSize * Global.DPI_Factor);
		skin.toggle.fixedWidth = Mathf.RoundToInt((float)skin.toggle.fixedWidth * Global.DPI_Factor); //.8
		skin.toggle.contentOffset = new Vector2 ((float)Mathf.Round((float)skin.toggle.contentOffset.x * Global.DPI_Factor),0.0f);
		foreach(GUIStyle style in skin.customStyles){
			style.fontSize = Mathf.RoundToInt((float)style.fontSize * Global.DPI_Factor);
		}
	}
	/*
	public static void emulaRetinaDPI(){

		//if (screenDiag >= 6) {
		if (Global.TestingRetinaRes) {
			Global.DPI_Factor = 1.5f;

		}
	}
*/
}
