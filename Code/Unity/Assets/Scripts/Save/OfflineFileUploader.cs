﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class OfflineFileUploader{

	private static OfflineFileUploader theInstance;
	int currentLineNum = -1;
	string[] allLines;
	string fileName = "";
	WWW connection = null;

	private OfflineFileUploader(){}
	public static OfflineFileUploader getInstance{
		get{
			if(theInstance == null){
				theInstance = new OfflineFileUploader();
			}
			return theInstance;
		}
	}

	public void startFileUpload(string newFileName){
		Debug.Log("IM RUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		StreamReader reader = new StreamReader(newFileName);
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			allContent.Add(content);
			Debug.Log("content: \n\n"+content+"\n\n");
		}
		reader.Close();
		if(allContent.Count > 0){
			fileName = newFileName;
			allLines = allContent.ToArray();
			currentLineNum = 0;
		}
		Debug.Log("ENDS RUUUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		checkDownload();
		//subConnect(allLines[0]);

	}
	public void checkDownload(){
		if(string.IsNullOrEmpty(fileName)){
			return;
		}

		if(currentLineNum < allLines.Length){
			if(connection == null){
				//add form data to make it post. for some reason none of the other data is added to this?
				//WWWForm aForm = new WWWForm();
				//aForm.AddField("fake", "fake");
				Debug.Log("saving line: \n\n\n"+allLines[currentLineNum]);
				subConnect(allLines[currentLineNum]);
				Debug.Log("subConnect-LogWhy?-");
				//connection = new WWW(allLines[currentLineNum], aForm);
				//connection = new WWW(allLines[currentLineNum], aForm);
				//yield return connection = new WWW(allLines[currentLineNum], aForm);
			} else {
				//Debug.Log("download progress: "+newConnection.progress.ToString());

				Debug.Log("download progress: "+connection.progress.ToString()+". Upload Progress:"+connection.uploadProgress.ToString());

			}
			Debug.Log("Connection iS Done value:" + connection.isDone);
			if(connection.isDone){
				currentLineNum++;
				connection.Dispose();
				connection = null;
			}
			
		} else {
			fileName = "";
		}
		//File.Delete(Application.persistentDataPath+failedFileName);
	}

	/*public IEnumerator subConnect(string sJsonString,WWWForm aForm){
		WWWForm form = new WWWForm();
		form.AddField("fake", "xy");
		//connection = new WWW(allLines, aForm);
		yield return connection = new WWW(allLines, aForm);

	}*/

	public IEnumerator subConnect(string sJsonString)
	{	
		Debug.Log("Saving subconnect:" + sJsonString);
		WWWForm form = new WWWForm();
		form.AddField("fake", "xy");
		//string theURL = GetUserProcessPostURL(_levelNum, _userFeedbackScore);
		WWW www = new WWW(sJsonString, form);
		yield return www;
		Debug.Log (www.text);
		if(!string.IsNullOrEmpty(www.error)){
			Debug.Log("OffLine Save error www");
		}
		yield return new WaitForSeconds(5);
	}	


}
