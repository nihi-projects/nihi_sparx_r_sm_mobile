using UnityEngine;
using System.Collections;

public class ChestInteraction : MonoBehaviour {

	public float radius = 5.0f;
	public GameObject FightGnats;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(GameObject.Find ("FakeChestCharacter").GetComponent<CharacterInteraction>().Interacting){
			if(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("idle")){
				GameObject.Find ("OpenChest").GetComponent<OpenChest>().enabled = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "ChestInteraction");
			}
		}
	}
}
