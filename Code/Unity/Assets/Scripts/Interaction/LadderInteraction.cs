using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class LadderInteraction : MonoBehaviour {
	
	float fLengthOfLadder;
	public float fPlayerPositionOnLadder;
	
	public bool m_bCorrectLadder;
	public Transform EndPos;
	public GameObject Trigger;
	public bool m_bOnThisLadder;
	
	bool m_bClimbing = false;
	bool m_bReachedTop = false;
	float moveCap = 0.5f;
	
	// Use this for initialization
	void Start () {
		fLengthOfLadder = this.GetComponent<Renderer>().bounds.extents.y * 2.0f + 1.0f; 
		fPlayerPositionOnLadder = Trigger.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
		if(m_bOnThisLadder)
		{
            Debug.Log("Touch Ladder");
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LadderInteraction");
            m_bClimbing = false;
			m_bReachedTop = false;
			if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || CrossPlatformInputManager.GetAxis ("Vertical") > moveCap)
			{
				ClimbUp();
				if (!m_bReachedTop)
				{
					SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
				}
				m_bClimbing = true;
			}
			else if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || CrossPlatformInputManager.GetAxis ("Vertical") < -moveCap)
			{
				ClimbDown();
				SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
				m_bClimbing = true;
			}
            else
            {
                //if(Global.GetPlayer().GetComponent<Animation>().IsPlaying("climb") == false )
                //{
                    Global.GetPlayer().GetComponent<Animation>().Play("climb");
                    Global.GetPlayer().GetComponent<Animation>()["climb"].speed = 0f;
                //}
            }
			
			if (!m_bClimbing)
			{
				SoundPlayer.StopLoop("sfx-ladder");
			}
		}
	}
		
	void ClimbUp()
	{
		float Estimate = fPlayerPositionOnLadder + (LevelFourScript.m_fClimbSpeed * Time.deltaTime);
		if(Estimate >= (transform.position.y + fLengthOfLadder * 0.5f) - LevelFourScript.PlayerHeight * 0.5f)
		{
			if(m_bCorrectLadder && EndPos)
			{
				if(EndPos.name.Contains("Move"))
				{
					// set the player pos to center of the collider.
					Global.GetPlayer().transform.position = EndPos.GetComponent<Collider>().bounds.center;
				}
				else
				{
					// set the player pos to transform pos;
					Global.GetPlayer().transform.position = EndPos.position;
				}
				//Player reaches the top of the ladder
				Global.GetPlayer().GetComponent<Animation>().Play ("idle");
				Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement( true, "LadderInteraction2");
				Global.CameraSnapBackToPlayer();
				m_bOnThisLadder = false;
			}
			else
			{
				m_bReachedTop = true;
			}
			
			return;
		}
        // if the player is NOT playing climb, or if the player is playing 
        // climb backwards
        CommonClimb(-1.0f, 1.0f, 1.0f, 0f);
    }
	
	void ClimbDown()
	{
		float Estimate = fPlayerPositionOnLadder - (LevelFourScript.m_fClimbSpeed * Time.deltaTime);
		if(Estimate <=  Trigger.transform.position.y)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "LadderInteraction3");
			Global.GetPlayer().GetComponent<Animation>().Play ("idle");
			m_bOnThisLadder = false;
			Global.CameraSnapBackToPlayer();
			return;	
		}
        // if the player is NOT playing climb, or if the player is playing 
        // climb forward
        CommonClimb(1.0f, -1.0f, -1.0f, Global.GetPlayer().GetComponent<Animation>()["climb"].length);
	}

    public void CommonClimb( float testSpeed, float setSpeed, float _moveMultiplier, float _fTime)
    {
        if (!Global.GetPlayer().GetComponent<Animation>().IsPlaying("climb") ||
            (Global.GetPlayer().GetComponent<Animation>().IsPlaying("climb") &&
             Global.GetPlayer().GetComponent<Animation>()["climb"].speed != setSpeed))
        {
            Global.GetPlayer().GetComponent<Animation>()["climb"].speed = setSpeed;
            Global.GetPlayer().GetComponent<Animation>()["climb"].time = _fTime;
            Global.GetPlayer().GetComponent<Animation>().Play("climb");
        }
        Global.GetPlayer().transform.position += _moveMultiplier * Vector3.up * LevelFourScript.m_fClimbSpeed * Time.deltaTime;
        fPlayerPositionOnLadder += _moveMultiplier * LevelFourScript.m_fClimbSpeed * Time.deltaTime;
    }
}
