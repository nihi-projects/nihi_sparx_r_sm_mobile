using UnityEngine;
using System.Collections;

public class CharacterInteraction : MonoBehaviour {
	
	
	//public Vector3 CameraPosition;
	public float InteractionRadius         = 0.0f;
	public float m_fFaceToFaceTalkDistance = 0.0f;
	private float m_fPlayerNPCDistance      = 1000.0f;
	public bool IsAutomatic                = false;
	public bool Interacting                = false;

    private bool privateInteracted = false;
    public bool Interacted
    {
        get
        {
            return privateInteracted;
        }
        set
        {
            privateInteracted = value;
        }
    }
	
	public float m_fCassDistance           = 0.0f;
	
	private bool m_bHopeIsFlyingL2         = false;
    private bool wasInteracting = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Interacted){ return;}
		
		if (Global.GetPlayer())
		{
            Debug.Log(Global.GetPlayer().name + "<>" + transform.name);
            Vector3 pos1 = Global.GetPlayer().transform.position;
            Vector3 pos2 = transform.position;
            pos1.y = 0f;
            pos2.y = 0f;
            m_fPlayerNPCDistance = Vector3.Distance(pos1, pos2);
		}
		
		if(m_fPlayerNPCDistance <= InteractionRadius){
            //Non automatic
            if (!IsAutomatic)
            {
                if (!Interacting)
                {
                    if (Input.GetKey(KeyCode.Space))
                    {
                         

                            Global.CurrentInteractNPC = this.name;
                            Interacting = true;
                            if (this.name != "Hope" && this.name != "FirepspiritA" && this.name != "Yeti")
                            {
                                Global.GetPlayer().GetComponent<PlayerMovement>().
                                MoveToPosition(transform.position + (transform.forward * m_fFaceToFaceTalkDistance),
                                                 transform.position,
                                               gameObject.name);
                            }

                        
                    }
                }
            }
            //Automatic
            else
            {
                if (!Interacting)
                {

                    Debug.Log(Global.CurrentLevelNumber + "<>" + this.name == "Cass" + "<>" + Global.TempTalkCharacter + "<>" + Global.PreviousSceneName);

                    if (!((Global.CurrentLevelNumber == 2 && this.name == "Cass" && Global.TempTalkCharacter == "Cass" && !Global.Level2FireguyInteraction)) || (Global.CurrentLevelNumber == 2 && this.name == "Cass" && Global.TempTalkCharacter == "Cass" && Global.PreviousSceneName == "L2CassScene1_1_1_1"))
                    {
                        //Non Hope
                        if (this.name != "Hope")
                        {
                            if (m_fPlayerNPCDistance > (m_fFaceToFaceTalkDistance + 0.1f))
                            {
                                // The next line was using Global.GetPlayer().transform.position.y. However the character in level 4 was flying . The line was change as follows:
                                Vector3 moveTo = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                                Global.GetPlayer().GetComponent<PlayerMovement>().
                                MoveToPosition(moveTo + (transform.forward * m_fFaceToFaceTalkDistance), transform.position, gameObject.name);
                            }
                            else
                            {
                                Interacting = true;
                                Global.CurrentInteractNPC = this.name;

                                //Fake object for character interaction
                                if (gameObject.name.Contains("Fake"))
                                {
                                    FakeCharacterSwitch(gameObject.name);
                                }
                            }
                        }
                        //Hope
                        else
                        {
                            Interacting = true;
                            Global.CurrentInteractNPC = this.name;
                        }
                    }
                }
            }
			
			//Interacting
			if(Interacting)
            {
                if (wasInteracting == false)
                {
                    wasInteracting = true;
                    if (GameObject.Find("GUI"))
                    {
                        GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
                    }
                }
				//When the player interacting with NPCs
				if(Global.GetPlayer() != null){
					if(Global.GetPlayer().GetComponent<Animation>().IsPlaying("idle") && !Global.CurrentInteractNPC.Contains("Fake"))
                    {
                        if(GameObject.Find("GUI"))
                        {
                            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;

                            PlayerMovement mov = Global.GetPlayer().GetComponent<PlayerMovement>();
                            if (mov.m_bMovingToPosition == true)
                                mov.m_bMovingToPosition = false;

                            //Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
                            //Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;
                            GameObject.Find("GUI").GetComponent<TerminateTalkScene>().enabled = false;
                        }
					}
				}
			}
		}
		
		SetCharacterInteractionBack();
	}
	
	public void FakeCharacterSwitch(string _fakeCharacterName)
	{
		switch(_fakeCharacterName){
			//Level 1
			case "FakeChestCharacter":
				//Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeChestCharacter";
				break;
			case "FakeHopeCharStp1":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeHopeCharStp1";
				break;
			case "FakeHopeCharStp2":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeHopeCharStp2";
				break;
			//Level 2
			case "FakeTuiIceCave":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiIceCave";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L2HopeScene2");
				break;
			//Level 3
			case "FakeTuiVocanoStart":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiVocanoStart";
				break;
			case "FakeTuiVocanoSprit":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiVocanoSprit";
				break;
			case "FakeTuiBeforeMiniGames":
				//Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiBeforeMiniGames";
				break;
			//Level 4
			case "FakeCliffTuiFlyPos1":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeCliffTuiFlyPos1";
				break;
		}
	}
	
	public void SetCharacterInteractionBack()
	{
		//Level 2
		if (Global.CurrentLevelNumber == 2){
			//Before Cass talk to the fireman
			if (Global.CurrentInteractNPC == "Cass" && !GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bCassFiremanTalkStart){
				m_fCassDistance = Vector3.Distance(Global.GetPlayer().transform.position, GameObject.Find ("Cass").transform.position);
				
				if(m_fCassDistance > InteractionRadius + 2){
					GameObject.Find ("Cass").GetComponent<CharacterInteraction>().Interacted = false;
				}
			}
		}
	}
}