using UnityEngine;
using System.Collections;

public class SideDoorTrigger : MonoBehaviour {

	// Use this for initialization
	public void Start () 
	{
	
	}
	
	// Update is called once per frame
	public void Update () 
	{
	
	}
	
	/*
	 * This function triggers the side door opens once the player entered
	 * this collider.
	 * 
    */
	public void OnTriggerEnter()
	{
		if (GameObject.Find("Level6").GetComponent<LevelSixScript>().DoneWithGuard)
		{
			gameObject.transform.parent.gameObject.GetComponent<ObjectMovement>().m_bStartMoving = true;
		}
	}
}
