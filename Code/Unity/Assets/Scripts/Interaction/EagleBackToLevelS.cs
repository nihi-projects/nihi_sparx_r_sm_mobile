using UnityEngine;
using System.Collections;
using UnityEngine.Playables;

public class EagleBackToLevelS : MonoBehaviour {
	//public float        InteractiveRadius = 3.0f;
	public Transform    FlyTo;
	Transform           WaitPos;
	//public string       CameraID;
	
	public Texture2D    EagleMouseOverTexture;
	public Texture2D    EagleMouseOriTexture;

    //New variables in order to play the cutscene.
    public float        fadeOutTime = 9.2f; //Time before the cutscene switches level.
    public GameObject   eagleCutscene;

	Vector3             start = Vector3.zero;
	float               m_fTime = 0.0f;
	
	bool                ReadyToLeave = false;
	bool                PlayerAtEagle = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	void OnEnable()
	{
		transform.position = FlyTo.position;
		WaitPos = GetComponent<EagleFlyToPosition>().EagleEndPos;
		this.GetComponent<Animation>().Play("fly");
		Global.LevelSVisited = true;

        if (eagleCutscene == null)
            Debug.LogError("Eagle cutscene unassigned.");
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject player = Global.GetPlayer();
		if (!ReadyToLeave)
		{
			m_fTime += Time.deltaTime * 0.2f;
			transform.position = Vector3.Lerp(FlyTo.position, WaitPos.position, m_fTime);
			Vector3 LookAtTarget = new Vector3(WaitPos.position.x, transform.position.y, WaitPos.position.z) - transform.position;
			transform.rotation = Quaternion.LookRotation(LookAtTarget);
			
			if (m_fTime >= 1.0f)
			{
				m_fTime = 0.0f;
				ReadyToLeave = true;
				this.GetComponent<Animation>().Play("idle");
				transform.rotation = WaitPos.rotation;
			}
		}
		else
		{
			//if(Vector3.Distance(Global.GetPlayer ().transform.position, transform.position) <= InteractiveRadius)
			//{			
			if(!PlayerAtEagle && Input.GetMouseButtonDown(0)){
				if(!GetComponent<Animation>().IsPlaying("fly") && Global.GetMousePosToObj() == "Eagle"){
					//Vector3 dir = player.transform.position - transform.position;
					//dir.Normalize();
					//Vector3 newPostion = transform.position + (dir * 0.9f);
					//newPostion = new Vector3(newPostion.x, player.transform.position.y, newPostion.z);
					//player.GetComponent<PlayerMovement>().MoveToPosition(newPostion, transform.position);
					//PlayerAtEagle = true;

                    eagleCutscene.SetActive(true);
                    gameObject.GetComponentInChildren<Renderer>().enabled = false;
                    Global.GetPlayer().SetActive(false);
                    PlayerAtEagle = true;
                }
			}

            //if (PlayerAtEagle && !player.GetComponent<PlayerMovement>().m_bMovingToPosition)
            //{
            //		player.transform.position = transform.position;
            //		Vector3 newPostion = transform.position;
            //		newPostion.y += 0.38f;
            //		player.transform.position = newPostion;
            //		player.GetComponent<Animation>().Play("ride");

            //		this.GetComponent<Animation>().Play("fly");
            //		start = transform.position;
            //		PlayerAtEagle = false;
            //}
            ////}
            //if(GetComponent<Animation>().IsPlaying("fly"))
            //{
            //	m_fTime += Time.deltaTime * 0.25f;
            //	transform.position = Vector3.Lerp (start, FlyTo.position, m_fTime);
            //	Vector3 LookAtTarget = new Vector3(FlyTo.position.x, start.y, FlyTo.position.z) - start;
            //	transform.rotation = Quaternion.LookRotation(LookAtTarget);

            //	player.transform.position = transform.position;
            //	Vector3 newPostion = transform.position;
            //	newPostion.y += 0.38f;
            //	player.transform.position = newPostion;
            //	player.transform.rotation = transform.rotation;
            //	player.GetComponent<PlayerMovement>().m_bMovement = false;
            //	Camera.main.GetComponent<CameraMovement>().m_bFollow = false;
            //	Global.SnapCameraBasedOnID(CameraID);

            //	if (m_fTime >= 0.25f)
            //	{
            //		GameObject.Find("GUI").GetComponent<IslandSplashScreen>().enabled = true;
            //	}
            //}

            float playableTime = (float)(eagleCutscene.GetComponent<PlayableDirector>().time);
            if (PlayerAtEagle == true && playableTime > fadeOutTime)
            {
                GameObject.Find("GUI").GetComponent<IslandSplashScreen>().enabled = true;

                //Level 1
                if (Global.CurrentLevelNumber == 1)
                {
                    GameObject.Find("L1GUI").GetComponent<TuiTerminateScene>().enabled = false;
                }
                //level 2
                else if (Global.CurrentLevelNumber == 2)
                {
                    GameObject.Find("L2GUI").GetComponent<L2TuiTerminateScene>().enabled = false;
                }
                //level 3
                else if (Global.CurrentLevelNumber == 3)
                {
                    GameObject.Find("L3GUI").GetComponent<L3TuiTerminateScene>().enabled = false;
                }
                //level 4
                else if (Global.CurrentLevelNumber == 4)
                {
                    GameObject.Find("L4GUI").GetComponent<L4TuiTerminateScene>().enabled = false;
                }
                //level 5
                else if (Global.CurrentLevelNumber == 5)
                {
                    GameObject.Find("L5GUI").GetComponent<L5TuiTerminateScene>().enabled = false;
                }
                //level 6
                else if (Global.CurrentLevelNumber == 6)
                {
                    GameObject.Find("L6GUI").GetComponent<L6TuiTerminateScene>().enabled = false;
                }
                //level 7
                else if (Global.CurrentLevelNumber == 7)
                {
                    GameObject.Find("L7GUI").GetComponent<L7TuiTerminateScene>().enabled = false;
                }

                //After the eagle landed, the levelS is marked as visited
                Global.LevelSVisited = true;
            }
		}
	}

    /*
	*
	* This function set the cursor image back if the mouse move into the 
	* Eagle
	*
	*/
    void OnMouseOver()
	{
		//Change the cursor image of the mouse
        if(GameObject.Find("GUI"))
        {
            GameObject.Find("GUI").GetComponent<MouseCursorChange>().cursorTexture = EagleMouseOverTexture;
            GameObject.Find("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
        }
		
	}

    /*
	*
	* This function set the cursor image back if the mouse move out from the 
	* Eagle
	*
	*/
    void OnMouseExit()
    {
        if (GameObject.Find("GUI"))
        {
            //Change the cursor image of the mouse
            GameObject.Find("GUI").GetComponent<MouseCursorChange>().cursorTexture = EagleMouseOriTexture;
            GameObject.Find("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
        }
    }
}
