using UnityEngine;
using System.Collections;

public class CaveFireInteraction : MonoBehaviour {
	
	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	
	bool StartToLight = false;
	bool Lighted = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{		
		if(StartToLight)
		{
			if(GetComponent<Light>().intensity < 1.0f)
			{
				GetComponent<Light>().intensity += Time.deltaTime;
			}
			else
			{
				StartToLight = false;
				Lighted = true;
			}
		}
		if(Lighted && !Global.GetPlayer().GetComponent<Animation>().IsPlaying("aim shoot"))
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "CaveFireInteraction");
			GameObject.Find (Global.GetCurrentLevelID()).GetComponent<LevelTwoScript>().AddCurrentNumMeltedIce(1);
			this.enabled =false;
		}
	}
	
	void OnMouseDown()
	{
		if(Vector3.Distance(Global.GetPlayer ().transform.position,
							transform.position) < 3.0f)
		{
			Destroy(GetComponent<SphereCollider>());
			Global.GetPlayer().transform.LookAt(new Vector3(transform.position.x, 
												Global.GetPlayer().transform.position.y,
												 transform.position.z));
			StartToLight = true;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "CaveFireInteraction2");
			Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
			SoundPlayer.PlaySound("sfx-lanternlit", 0.6f);
			this.GetComponent<Light>().enabled = true;
			//Turn the fire on as well
			if(gameObject.name == "Light Cave Fire 1"){
				GameObject.Find ("Lantern Fire1").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 2"){
				GameObject.Find ("Lantern Fire2").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 3"){
				GameObject.Find ("Lantern Fire3").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 4"){
				GameObject.Find ("Lantern Fire4").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 5"){
				GameObject.Find ("Lantern Fire5").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 6"){
				GameObject.Find ("Lantern Fire6").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 7"){
				GameObject.Find ("Lantern Fire8").GetComponent<ParticleSystem>().enableEmission = true;
			}
			else if(gameObject.name == "Light Cave Fire 8"){
				GameObject.Find ("Lantern Fire7").GetComponent<ParticleSystem>().enableEmission = true;
			}
		}
	}
	
	void OnMouseOver()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}

	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
}
