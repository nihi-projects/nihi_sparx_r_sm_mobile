using UnityEngine;
using System.Collections;

public class EagleFlyToPosition : MonoBehaviour {
	public string    EagleFlyCamID;
	public Transform DownPosition;
	public Transform FlyAwayPosition;
	public Transform PlayerPosition;
	bool             m_bIsFlyingDown = true;
	public Transform EagleEndPos;
    public Transform ridePos;
	bool hasBeenInitialised = false;

	/*void Awake(){
		
	}
	*/
	// Use this for initialization
	void Start () {
		if(Global.loadedSavePoint.Contains("GameMid")){
			loadDictionaryVars.loadPosition();
			loadDictionaryVars.saveVars();
		}
	}
	void doTheInitialisation(){
		if(!Global.loadedSavePoint.Contains("GameMid")){
			

			// Set camera to the beginning position
			Global.SnapCameraBasedOnID(EagleFlyCamID);
            if( ridePos )
			    Global.GetPlayer().transform.position = ridePos.position;
			Global.GetPlayer().transform.rotation = transform.rotation;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "EagleFlyToPosition");
			Global.GetPlayer().GetComponent<Animation>().Play("ride");
			GetComponent<Animation>().Play("fly");
			
			
		}else{
			Debug.Log ("Loading Mid Save");
			m_bIsFlyingDown = false;
			EagleFlyAway();
			if(Global.CurrentLevelNumber == 2){
				Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
				eulerAngles.y = 180;
				Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
			}
			if(Global.CurrentLevelNumber == 5){
				Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
				eulerAngles.y = 100;
				Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
			}
			if(Global.CurrentLevelNumber == 6){
				if(Global.loadedSavePoint.Contains("GameMid1")){
					Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
					eulerAngles.y = 40;
					Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
				}else if(Global.loadedSavePoint.Contains("GameMid2")){
					Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
					eulerAngles.y = 90;
					Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
				}else if(Global.loadedSavePoint.Contains("GameMid3")){
					Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
					eulerAngles.y = 150;
					Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
				}else if(Global.loadedSavePoint.Contains("GameMid4")){
					Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
					eulerAngles.y = 230;
					Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
				}else if(Global.loadedSavePoint.Contains("GameMid5")){
					Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
					eulerAngles.y = 180;
					Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
				}
			}
			if(Global.CurrentLevelNumber == 7){
				Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
				eulerAngles.y = 270;
				Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
			}
			GameObject test1 = Global.GetPlayer();
			Transform test2 = test1.GetComponent<Transform>();

			Global.GetPlayer().GetComponent<Transform>().position = Global.loadPos;
		}
	}

    public static void EnableTransitionTo( int _levelNumber )
    {
        if (_levelNumber == 1)
        {
            if (GameObject.Find("L1GUI"))
            {
                GameObject.Find("L1GUI").GetComponent<TheCaveSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L1GUI").GetComponent<TheCaveSplashScreen>().enabled = false;
                GameObject.Find("L1GUI").GetComponent<TheCaveSplashScreen>().enabled = true;
            }

        }
        else if (_levelNumber == 2)
        {
            if (GameObject.Find("L2GUI"))
            {
                GameObject.Find("L2GUI").GetComponent<IceCaveSpashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L2GUI").GetComponent<IceCaveSpashScreen>().enabled = false;
                GameObject.Find("L2GUI").GetComponent<IceCaveSpashScreen>().enabled = true;
            }

        }
        else if (_levelNumber == 3)
        {
            if (GameObject.Find("L3GUI"))
            {
                GameObject.Find("L3GUI").GetComponent<VencanoCaveSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L3GUI").GetComponent<VencanoCaveSplashScreen>().enabled = false;
                GameObject.Find("L3GUI").GetComponent<VencanoCaveSplashScreen>().enabled = true;
            }
        }
        else if (_levelNumber == 4)
        {
            if (GameObject.Find("L4GUI"))
            {
                GameObject.Find("L4GUI").GetComponent<MoutainCaveSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L4GUI").GetComponent<MoutainCaveSplashScreen>().enabled = false;
                GameObject.Find("L4GUI").GetComponent<MoutainCaveSplashScreen>().enabled = true;
            }
        }
        else if (_levelNumber == 5)
        {
            if (GameObject.Find("L5GUI"))
            {
                GameObject.Find("L5GUI").GetComponent<TheSwampSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L5GUI").GetComponent<TheSwampSplashScreen>().enabled = false;
                GameObject.Find("L5GUI").GetComponent<TheSwampSplashScreen>().enabled = true;
            }

        }
        else if (_levelNumber == 6)
        {
            if (GameObject.Find("L6GUI"))
            {
                GameObject.Find("L6GUI").GetComponent<TheBridgeSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L6GUI").GetComponent<TheBridgeSplashScreen>().enabled = false;
                GameObject.Find("L6GUI").GetComponent<TheBridgeSplashScreen>().enabled = true;
            }

        }
        else if (_levelNumber == 7)
        {
            if (GameObject.Find("L7GUI"))
            {
                GameObject.Find("L7GUI").GetComponent<TheCanyonSplashScreen>().m_bIsFadeIn = false;
                GameObject.Find("L7GUI").GetComponent<TheCanyonSplashScreen>().enabled = false;
                GameObject.Find("L7GUI").GetComponent<TheCanyonSplashScreen>().enabled = true;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(Global.GetPlayer() == null){
			return;
		} else if(!hasBeenInitialised){
			doTheInitialisation();
			hasBeenInitialised = true;
		}
		if(m_bIsFlyingDown){
			if (Global.CurrentLevelNumber == 4)
			{
				GameObject.Find("FakeCliffTuiFlyPos1").GetComponent<CharacterInteraction>().enabled = false;
			}
			EagleFlyDown();
		}
		else{
			EagleFlyAway();
			
			//Tui needs to fly down after the Eagle fly away
			if(Global.CurrentLevelNumber == 7 && !Global.loadedSavePoint.Contains("GameMid")){
				TuiFlyDown();
			}
		}
	}
	
	void EagleFlyDown()
	{
		transform.position = Vector3.Lerp (transform.position, DownPosition.position, Time.deltaTime *0.8f);
		Global.GetPlayer().transform.position = ridePos.position;
		Global.GetPlayer().transform.rotation = transform.rotation;
		if(Vector3.Distance(transform.position, DownPosition.position) <= 1.0f){
			m_bIsFlyingDown = false;
			//Global.GetPlayer ().transform.position = ridePos.position;
			//Global.GetPlayer().transform.rotation = PlayerPosition.rotation;
			Global.GetPlayer ().GetComponent<Animation>().Play("idle");
			Global.CameraSnapBackToPlayer();
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "EagleFlyToPosition2");
			if (Global.CurrentLevelNumber == 4)
			{
				GameObject.Find("FakeCliffTuiFlyPos1").GetComponent<CharacterInteraction>().enabled = true;
			}
		}
	}
	
	//Eagle Fly away once the player is landed to a new level
	void EagleFlyAway()
	{
		transform.position = Vector3.Lerp (transform.position, FlyAwayPosition.position, Time.deltaTime);
		if(Vector3.Distance(transform.position, FlyAwayPosition.position) <= 1.0f){
			transform.position = GetComponent<EagleBackToLevelS>().FlyTo.position;
			transform.rotation = EagleEndPos.rotation;
			this.enabled = false;
			//this.GetComponent<EagleBackToLevelS>().enabled = true;
		}
	}
	
	//Tui follows the player along, only happen in level 7
	void TuiFlyDown()
	{
		Vector3 tuiFlyToPosition;
		
		//Position on top of the player
		tuiFlyToPosition = Global.GetPlayer().transform.position;
		tuiFlyToPosition.y += 1.0f;
		tuiFlyToPosition.z -= 3.0f;
		
		GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo(tuiFlyToPosition);
	}
}
