using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PortalInteraction : MonoBehaviour {
	public Vector3 CameraPosition;
	private float InteractionRadius      = 0.5f;
	private float m_fTransitionStartTime = 0.0f;
	private float m_fTransitionTime      = 2.0f;
	private bool IsAutomatic             = false;
	private bool Interacting             = false;
	private bool m_bTransitionStarts     = false;
	private bool m_playerStartMoving     = false;

    private bool whiteOutStart = false;
    private GameObject whiteout = null;

    public GameObject newPortalEffect;

	// Use this for initialization
	void Start ()
    {

	}
	
	void OnEnable() 
	{
		SoundPlayer.PlayLoopSound("sfx-potal", 1.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//When the player finish moving the cerntre point of the portal
		if(m_playerStartMoving && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
			Interacting = true;
		}
		
		float fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);

		if(Global.m_bGemIsFullyReleased){
			if(fDistance <= InteractionRadius){	
				if(!Interacting){
					Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(transform.position, transform.position + (transform.forward * 0.5f));
					m_playerStartMoving = true;
                    GameObject.Find("Portal Transition Particle").GetComponent<ParticleSystem>().Play();
                }
				else if(Interacting && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
					if(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("idle") && !m_bTransitionStarts){
						Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "PortalInteraction");
						
						Global.CurrentInteractNPC = "Guide";
						m_bTransitionStarts = true;
						m_fTransitionStartTime = Time.time;
                        
                        if(whiteOutStart == false)
                        {
                            TriggerWhiteout();
                            whiteOutStart = true;
                        }
                    }
				}
			}
		}
		
		if(m_bTransitionStarts && (Time.time - m_fTransitionStartTime) > m_fTransitionTime){
            whiteOutStart = false;
			//Application.LoadLevel("GuideScene");
		}
	}

    public void TriggerWhiteout()
    {
        whiteout = TextDisplayCanvas.instance.ShowPrefab("Whiteout");
        whiteout.GetComponent<Whiteout>().fadeState = Whiteout.FadeState.FADE_IN;
        whiteout.GetComponent<Whiteout>().del = WhiteoutCallback;
        
    }

    public void WhiteoutCallback(Whiteout.FadeState _fadeState)
    {
        whiteout.GetComponent<Whiteout>().fadeState = Whiteout.FadeState.FADE_OUT;
        whiteout.GetComponent<Whiteout>().del = null;
        newPortalEffect.SetActive(true);

        StartCoroutine( LoadGuideScene()  );
    }

    public IEnumerator LoadGuideScene()
    {
        float fTimeUntilLoad = 0.5f;
        while( fTimeUntilLoad > 0f )
        {
            fTimeUntilLoad -= Time.deltaTime;
            yield return null;
        }

        AsyncOperation op = SceneManager.LoadSceneAsync("GuideScene");
        while( op.isDone == false )
        {
            yield return null;
        }

        if(whiteout != null )
        {
            Destroy(whiteout);
                whiteout = null;
        }
    }
	
}
