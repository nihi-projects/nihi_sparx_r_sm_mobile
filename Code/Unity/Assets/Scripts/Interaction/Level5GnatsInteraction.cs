using UnityEngine;
using System.Collections;

public class Level5GnatsInteraction : MonoBehaviour {
	
	public bool Active = true;
	public bool bPlayedParticle = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Active)
		{
			transform.RotateAround(GameObject.Find ("GnatSparxMiniGame").transform.position,
								   Vector3.up, Time.deltaTime * 20.0f);
		}
		if(bPlayedParticle && !transform.Find ("Gnat Explosion").GetComponent<ParticleSystem>().isPlaying)
		{
			this.gameObject.SetActive(false);
		}
	}
	
	public void SetInactive()
	{
		if(!bPlayedParticle)
		{
			transform.Find ("Gnat Explosion").GetComponent<ParticleSystem>().Play();
			bPlayedParticle = true;
		}
	}
}
