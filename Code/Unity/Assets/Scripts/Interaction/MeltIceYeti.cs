using UnityEngine;
using System.Collections;

public enum IceMeltRoutine
{
	WaitingToEnterZone,
	WalkingToDestination,
	TuiFlyIn,
	TuiConversation,
	TuiFlyOut,
	FireAtIce // After freeing the yeti, start yeti conversation and 
			  // Disable this script.
}

public class MeltIceYeti : MonoBehaviour {
	
	IceMeltRoutine MeltStatus = IceMeltRoutine.WaitingToEnterZone;
	public GameObject[] IceParts;
	public GameObject ZoneBox;
	public Transform WalkToPosition;
	public int[] FireCount;
	public string YetiCameraID;
	bool m_bAttackingIce = false;
	int CurrentAttackingPart = 0;
	
	private bool m_bTuiConversationFirst = true;
	
	
	
	// Use this for initialization
	void Start () {
		FireCount = new int[IceParts.Length];
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch(MeltStatus)
		{
		case IceMeltRoutine.WaitingToEnterZone:
			CheckPlayerEnteredZone();
			break;
		case IceMeltRoutine.WalkingToDestination:
			CheckPlayerArrivedDestination();
			break;
		case IceMeltRoutine.FireAtIce:
			FireAtIce();
			break;
		default:break;
		}
	}
	
	/*
	 * This function performs the player fire at the ice on the Yeti
	 * 
    */
	void FireAtIce()
	{
		
		if(Input.GetMouseButton(0))
		{
			for(int i = 0; i < IceParts.Length; ++i)
			{
				
				string[] AllObj = Global.GetMousePosToAllObj();
				foreach(string obj in AllObj)
				{
					if(obj == IceParts[i].name)
					{
						if(IceParts[i].GetComponent<Renderer>().enabled)
						{
							Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
							SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
							m_bAttackingIce = true;
							Global.GetPlayerEffect().SetFireBallMoveTo(IceParts[i]);
							CurrentAttackingPart = i;
						}
					}
				}
			}
		}
		if(m_bAttackingIce)
		{
			if(Global.GetPlayerEffect().FireBallHitObject())
			{
				m_bAttackingIce = false;
				FireCount[CurrentAttackingPart] += 1;
				
				if(FireCount[CurrentAttackingPart] == 3)
				{
					IceParts[CurrentAttackingPart].GetComponent<Renderer>().enabled = false;
				}
			}
		}
		
		bool bIceMeltComplete = true;
		// Check if all the Ice is melted
		for(uint i = 0; i < IceParts.Length; ++i)
		{
			if(IceParts[i].GetComponent<Renderer>().enabled)
			{
				bIceMeltComplete = false;
			}
		}
		
		if(bIceMeltComplete)
		{
			// Continue
			Global.GetPlayerEffect().SetFireBallActive(false);
			Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement( true, "MeltIceYeti");
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			
			/// NOTE!!! This is where to start the Yeti talking scene.
			GameObject.Find ("Yeti").GetComponent<CharacterInteraction>().enabled = true;
			
			/// End NOTE
			
			this.enabled = false;
		}
	}
	
	/*
	 * This function make the player to do some reaction once he reached the position
	 * in front of the Yeti.
	 * 
    */
	void CheckPlayerArrivedDestination()
	{
		//If the player move in front of yeti and stopped
		if(!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition)
		{
			//Make sure the player cannot move before the TUI conversation starts
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "MeltIceYeti2");
			
			//Tui fly to the front of the Yeti
			if(m_bTuiConversationFirst){
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("32020");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene3";
				
				m_bTuiConversationFirst = false;
			}
			
			//Tui arrived and start talking with the player
			if(!m_bTuiConversationFirst){
				if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				}
			}
			
			if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene == "L2TuiTerminateScene" && m_bTuiConversationFirst == false){
				// the FireAtIce status.
				MeltStatus = IceMeltRoutine.FireAtIce;
				GameObject.Find("Level2").GetComponent<IceDropLv2>().enabled = false;
				
				// Stops Movement
				Global.GetPlayerEffect().SetFireBallActive(true);
				Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement( false, "MeltIceYeti3");
				Global.MoveCameraBasedOnID("22004");
			}
		}
	}
	
	void CheckPlayerEnteredZone()
	{
		if(ZoneBox.GetComponent<Collider>().bounds.Contains(Global.GetPlayer().transform.position))
		{
			float y = Global.GetPlayer().transform.position.y;
			WalkToPosition.transform.position.Set(WalkToPosition.transform.position.x, 
											  y,
											  WalkToPosition.transform.position.z);
			
			MeltStatus = IceMeltRoutine.WalkingToDestination;
			// Start walking
			Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(WalkToPosition.position, GameObject.Find("Yeti").transform.position);
		}
	}
}
