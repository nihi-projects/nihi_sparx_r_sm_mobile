using UnityEngine;
using System.Collections;

public class IceCaveDoor : MonoBehaviour {
	
	public string CameraID;
	public string BirdFlyToPos;
	public bool m_birdStartFlying   = false;
	
	public bool m_hopeTalkFinished   = false;
	public bool m_bGotL2OpenDoorFire = false;
	
	private bool m_bDoorOpenClicked  = false;
	private bool m_bDoorCanOpen      = false;
	
	public Texture2D IceDoorMouseOverTexture;
	public Texture2D IceDoorMouseOriTexture;
	
	
	
	// Use this for initialization
	void Start () {
		toggleDoorHighlight(false);
	}
	
	// Update is called once per frame
	void Update () {
		//Click on doorshine
		if(Input.GetMouseButtonDown(0)){
			if(Global.GetMousePosToObj() == Global.GetGameObjectByID("32003").name){
				Global.GetPlayer().GetComponent<PlayerMovement>().
						MoveToPosition(GameObject.Find ("FakeDoorShine").transform.position
										+ (GameObject.Find ("FakeDoorShine").transform.forward * 1.7f), transform.position, "");
				
				m_bDoorOpenClicked = true;
				m_hopeTalkFinished = false;
				m_birdStartFlying = false;
			}
		}
		
		//Check whether open doors or not
		if(m_bDoorOpenClicked && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
			TryToOpenDoor();
			m_bDoorOpenClicked = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "IceCaveDoor");
		}
		
		//Tui fisrt talk in front of the ice doors
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_birdStartFlying){
			if(!m_hopeTalkFinished){
				Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled         = false;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled         = true;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene  = "L2HopeScene1";
				GameObject.Find ("GUI").GetComponent<TerminateTalkScene>().enabled = false;
				m_hopeTalkFinished = true;
			}
		}
		
		//Doors start opening
		if(m_bDoorCanOpen && !Global.GetPlayer().GetComponent<Animation>().IsPlaying("aim shoot") && !Global.GetPlayer().GetComponent<PlayerMovement>().GetMovement() )
		{
			Global.GetGameObjectByID("32009").GetComponent<ObjectMovement>().m_bStartMoving = true;
			Global.GetGameObjectByID("32010").GetComponent<ObjectMovement>().m_bStartMoving = true;
			SoundPlayer.PlaySound("sfx-caveopening", 0.8f);
			this.GetComponent<Renderer>().enabled = false;
			toggleDoorHighlight(false);
		}
		
		//The door open is complete
		if(Global.GetGameObjectByID("32009").GetComponent<ObjectMovement>().m_bMovementComplete 
		   && Global.GetGameObjectByID("32010").GetComponent<ObjectMovement>().m_bMovementComplete)
		{
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene2";
			// Finish Cutscene
			GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "IceCaveDoor2");
			Global.GetGameObjectByID("32003").GetComponent<MeshCollider>().enabled = false;
			this.enabled = false;
		}
	}
	
	public void Reset()
	{
		m_hopeTalkFinished   = false;
		m_bGotL2OpenDoorFire = false;
	
		m_birdStartFlying   = false;
		m_bDoorOpenClicked  = false;
		m_bDoorCanOpen      = false;
	}
	
    public void CloseDoor()
    {

    }

	void TryToOpenDoor()
	{
		//Has door fire
		if(m_bGotL2OpenDoorFire){
			if(!m_bDoorCanOpen){
				Global.GetPlayer().GetComponent<Animation>().Play ("aim shoot");
				m_bDoorCanOpen = true;
			}
		}
		//No door fire
		else{
			//Bird fly to the front of the door.
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("32002");
			m_birdStartFlying = true;
		}
	}
	
	/*
	*
	* This function set the cursor image back if the mouse move into the 
	* Eagle
	*
	*/
	void OnMouseOver()
	{
		//Change the cursor image of the mouse
		//IceDoorMouseOverTexture = (Texture2D)Resources.Load("UI/corsor_1", typeof(Texture2D));
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = IceDoorMouseOverTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
	
	/*
	*
	* This function set the cursor image back if the mouse move out from the 
	* Eagle
	*
	*/
	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		//IceDoorMouseOriTexture = (Texture2D)Resources.Load("UI/cursor", typeof(Texture2D));
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = IceDoorMouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
	public static void toggleDoorHighlight(bool toggleOn){
		//MeshRenderer[] renderers = GameObject.Find("DoorSelectionHighlight").GetComponentsInChildren<MeshRenderer>();
		//foreach(MeshRenderer rend in renderers){
		//	//rend.enabled = toggleOn;
		//}
	}
}
