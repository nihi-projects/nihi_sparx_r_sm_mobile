using UnityEngine;
using System.Collections;

public class Lv5GnatSparxGame : MonoBehaviour {
	
	bool bTriggered= false;
	public float fSpeed = 15.0f;
	public string CameraID;
	public GameObject Trigger;
	GameObject TargetObj;
	GameObject FireBall;
	public bool Firing = false;
	int iCurrentNumOfGnatsShot = 0;
	public Transform[] GnatEndPosition;
	Vector3 tempPos;
    

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Global.GetPlayer())
        {
            if (false == bTriggered)
            {
                if (Vector3.Distance(Trigger.transform.position,
                                     Global.GetPlayer().transform.position)
                                     <= 2.0f)
                {
                    GameObject.Find("Settings").GetComponent<Settings>().m_bShow = false;
                    bTriggered = true;
                    Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv5GnatSparxGame");
                    Global.GetPlayer().GetComponent<Animation>().Play("idle");
                    Global.MoveCameraBasedOnID(CameraID);
                    Global.GetPlayerEffect().SetFireBallActive(true);
                }
            }
            else
            {
                // Fire at the gnats
                if (Input.GetMouseButtonDown(0) && false == Firing)
                {
                    TargetObj = Global.GetMouseToObject();
                    if (TargetObj && TargetObj.name == "Gnat" && TargetObj.GetComponent<Level5GnatsInteraction>().Active)
                    {
                        // Fire at the Gnats!!
                        // Start shooting the gnat
                        Global.GetPlayerEffect().SetFireBallMoveTo(TargetObj);
                        SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
                        Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
                        Firing = true;
                    }
                }
                if (Global.GetPlayerEffect().FireBallHitObject())
                {
                    Firing = false;
                    // Set the target object to the correct position
                    TargetObj.gameObject.GetComponent<Level5GnatsInteraction>().Active = false;
                    TargetObj.transform.position = GnatEndPosition[iCurrentNumOfGnatsShot].position;
                    iCurrentNumOfGnatsShot += 1;
                }
                bool finished = true;
                foreach (Transform child in transform)
                {
                    if (child.gameObject.GetComponent<Level5GnatsInteraction>().Active &&
                       child.name == "Gnat")
                    {
                        finished = false;
                    }
                }
                if (finished)
                {
                    foreach (Transform child in transform)
                    {
                        if (child.name == "Gnat")
                        {
                            child.gameObject.GetComponent<Level5GnatsInteraction>().SetInactive();
                        }
                        else
                        {
                            child.gameObject.SetActive(false);
                        }
                    }
                    Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "Lv5GnatSparxGame2");
                    Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
                    Global.GetPlayerEffect().SetFireBallActive(false);
                    GameObject.Find("Level5").GetComponent<LevelFiveScript>().bStartDoorOpen = true;
                    GameObject.Find("ProgressWall").GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("ProgressTrigger").SetActive(false);

                    tempPos.y = 1.5f;
                    GameObject.Find("5_barrels").GetComponent<Transform>().position += tempPos;
                    tempPos.y = 2f;
                    GameObject.Find("BarrelGame").GetComponent<Transform>().position += tempPos;

                    GameObject.Find("Settings").GetComponent<Settings>().m_bShow = true;

                    this.enabled = false;
                }
            }
        }
		
	}
}
