﻿using UnityEngine;
using System.Collections;

public class DoorTriggerEnter : MonoBehaviour {

	public bool m_DoorTriggerEntered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) 
	{
		m_DoorTriggerEntered = true;
		//other.gameObject.GetComponent<ObjectMovement>().m_bMovementComplete = true;
	}

	void OnTriggerExit(Collider other) 
	{
		m_DoorTriggerEntered = false;
		//other.gameObject.GetComponent<ObjectMovement>().m_bMovementComplete = true;
	}

}
