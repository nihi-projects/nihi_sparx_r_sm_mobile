using UnityEngine;
using System.Collections;

public class LadderTrigger : MonoBehaviour 
{
	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	bool closeEnough = false;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	
	void OnMouseDown()
	{
		// check for triggered or not
		if(closeEnough)
		{
			if(!transform.parent.gameObject.GetComponent<LadderInteraction>().m_bOnThisLadder)
			{
				transform.parent.gameObject.GetComponent<LadderInteraction>().fPlayerPositionOnLadder = transform.position.y;
				transform.parent.gameObject.GetComponent<LadderInteraction>().m_bOnThisLadder = true;
				// Setting the player's position
				Vector3 Position = transform.position;
				Position.x -= 0.5f;
				Global.GetPlayer ().transform.position = Position;
				Vector3 Target = transform.position;
				Target.y = Global.GetPlayer().transform.position.y;
				// Set player position to be on the ladder,
				Global.GetPlayer().transform.LookAt(Target);
				// Disable movement
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LadderTrigger");
				Global.GetPlayer().GetComponent<Animation>().Play ("climb");
				Global.GetPlayer().GetComponent<Animation>().enabled = true;
				Global.CameraSnapBackToPlayer();
			}
		}
	}
	
	void OnTriggerStay(Collider col)
	{
		closeEnough = true;
	}
	
	void OnTriggerExit(Collider col)
	{
		closeEnough = false;
	}
	
	void OnMouseOver()
	{
		if (closeEnough)
		{
			//Change the cursor image of the mouse
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
	}

	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
}
