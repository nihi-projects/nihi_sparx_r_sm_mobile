using UnityEngine;
using System.Collections;
using UnityEngine.Playables;

public class EagleInteraction : MonoBehaviour
{	
	public Transform StartPosition;
	public Transform StationaryPosition;
	public Transform FlyTowards;
	bool m_bInteracted     = false;
	bool m_bPlayerGotOn    = false;
	bool m_bPlayerMoving   = false;
	float m_fLerpTime      = 0.0f;
	public string CameraID;
    public float fadeOutTime;

    public GameObject eagleCutscene;

	public Texture2D EagleMouseOverTexture;
	public Texture2D EagleMouseOriTexture;
	
	// Use this for initialization
	void Start () {

		this.gameObject.transform.position = StartPosition.position;
		this.gameObject.transform.rotation = StartPosition.rotation;
		if(!GameObject.Find("guardian"))
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bTalkedWithGuardian = true;
		}
		GetComponent<SphereCollider>().enabled = true;
	}
	
	void Update () {

        if (GameObject.Find("GUI") == null)
            return;


        if (m_bInteracted)
		{
            //if (m_bPlayerGotOn)
            //{
                if (eagleCutscene.GetComponent<PlayableDirector>().time > fadeOutTime)
                {
                    //LOAD the Cave level, fade in the the cave image;
                    if (Global.CurrentLevelNumber == 1)
                    {
                        GameObject.Find("L1GUI").GetComponent<TheCaveSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L1GUI").GetComponent<TheCaveSplashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 2)
                    {
                        GameObject.Find("L2GUI").GetComponent<IceCaveSpashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L2GUI").GetComponent<IceCaveSpashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 3)
                    {
                        GameObject.Find("L3GUI").GetComponent<VencanoCaveSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L3GUI").GetComponent<VencanoCaveSplashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 4)
                    {
                        GameObject.Find("L4GUI").GetComponent<MoutainCaveSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L4GUI").GetComponent<MoutainCaveSplashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 5)
                    {
                        GameObject.Find("L5GUI").GetComponent<TheSwampSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L5GUI").GetComponent<TheSwampSplashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 6)
                    {
                        GameObject.Find("L6GUI").GetComponent<TheBridgeSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L6GUI").GetComponent<TheBridgeSplashScreen>().enabled = true;
                    }
                    else if (Global.CurrentLevelNumber == 7)
                    {
                        GameObject.Find("L7GUI").GetComponent<TheCanyonSplashScreen>().m_bIsFadeIn = true;
                        GameObject.Find("L7GUI").GetComponent<TheCanyonSplashScreen>().enabled = true;
                    }
                }

                //Global.GetPlayer().transform.position = transform.position;
                //Vector3 newPostion = transform.position;
                //newPostion.y += 0.38f;
                //Global.GetPlayer().transform.position = newPostion;

                //m_fLerpTime += Time.deltaTime * 0.01f;
                //if(Vector3.Distance(transform.position, FlyTowards.position) > 1.0f){
                //	transform.position = Vector3.Lerp (transform.position, FlyTowards.position, m_fLerpTime);
                //}
                //else
                //{

                //}
            //}
			//}
			//else
			//{
			//	m_fLerpTime += Time.deltaTime * 0.3f;
			//	if(m_fLerpTime < 0.3f){
			//		transform.position = Vector3.Lerp(transform.position, StationaryPosition.position, m_fLerpTime);
			//	}
			//	else if(m_bPlayerMoving == false){
			//		// Set the player transform to eagle's transform.
			//		Vector3 dir = transform.position - Global.GetPlayer().transform.position;
			//		dir.Normalize();
			//		Vector3 pos = new Vector3(transform.position.x, Global.GetPlayer().transform.position.y, transform.position.z);
			//		Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(pos - (dir * 0.7f), transform.position);
			//		m_bPlayerMoving = true;
			//		this.GetComponent<Animation>().Play("idle");
			//	}
				
			//	if (m_bPlayerMoving == true && 
			//		Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition == false)
			//	{
			//		Global.GetPlayer().transform.position = transform.position;
			//		Global.GetPlayer().transform.rotation = transform.rotation;
			//		m_fLerpTime = 0.0f;
			//		m_bPlayerGotOn = true;
					
			//		Global.SnapCameraBasedOnID(CameraID);
			//		this.GetComponent<Animation>().Play("fly");
					
			//		Global.GetPlayer().GetComponent<Animation>().Play("ride");
			//		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
			//	}
			//}
		}
		else
		{
			bool CheckSceneComplete = false;
			
			if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bTalkedWithGuardian &&
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bTalkedWithMentor)
			{
				CheckSceneComplete= true;
			}
			if(Global.GetPlayer() != null){
				Vector2 a, b;
				a.x = Global.GetPlayer().transform.position.x;
				a.y = Global.GetPlayer().transform.position.z;
				b.x = transform.position.x;
				b.y = transform.position.z;
				float fDistance = Vector2.Distance(a, b);
			}
            
			if(Input.GetMouseButtonDown(0) && CheckSceneComplete){
				if(Global.GetMousePosToObj() == "Eagle"){

                    eagleCutscene.SetActive(true);
                    gameObject.GetComponentInChildren<Renderer>().enabled = false;
                    Global.GetPlayer().SetActive(false);
                    m_bInteracted = true;
                    
                    //GetComponent<SphereCollider>().enabled = false;
                    //GameObject.Find ("Main Camera").GetComponent<CameraMovement>().m_bFollow = false;
                    //this.GetComponent<Animation>().Play ("fly");
                }
			}
		}
	}
	
	/*
	*
	* This function set the cursor image back if the mouse move into the 
	* Eagle
	*
	*/
	void OnMouseOver()
	{
		//Change the cursor image of the mouse
		//EagleMouseOverTexture = (Texture2D)Resources.Load("UI/corsor_1", typeof(Texture2D));
		if (!Global.m_bGotCurrentLevelGem) {
			GameObject.Find ("GUI").GetComponent<MouseCursorChange> ().cursorTexture = EagleMouseOverTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange> ().SetCustomCursor ();
		}
	}
	
	/*
	*
	* This function set the cursor image back if the mouse move out from the 
	* Eagle
	*
	*/
	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		//EagleMouseOriTexture = (Texture2D)Resources.Load("UI/cursor", typeof(Texture2D));
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = EagleMouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
}

