using UnityEngine;
using System.Collections;

public class NPCMoveTo : MonoBehaviour {
	
	public Transform TargetPosition;
	//public Vector3 LookAtPosition;
	public bool IsWalking= false;
	public bool ArrivedAtPosition = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (IsWalking && !ArrivedAtPosition)
        {
            GetComponent<Animation>().Play("walk");
            transform.position = Vector3.Lerp(transform.position, TargetPosition.position,
                                              Time.deltaTime);
            transform.LookAt(TargetPosition.position);
            //NPCs arrived the target position
            if (Vector3.Distance(transform.position, TargetPosition.position) <= 0.5f)
            {
                GetComponent<Animation>().Play("idle");
                ArrivedAtPosition = true;
                transform.LookAt(Global.GetPlayer().transform.position);
            }
        }
	}
	
	//Start walking to the target position
	public void StartWalking()
	{
		IsWalking = true;
	}
}
