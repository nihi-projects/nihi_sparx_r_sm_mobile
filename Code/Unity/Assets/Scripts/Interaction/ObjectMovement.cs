using UnityEngine;
using System.Collections;

public class ObjectMovement : MonoBehaviour {
	//Public variables
	public bool bPosition = false;
	public bool bRotation = false;
	//public bool m_bRotationBackwards = false;
	public Vector3 m_vTarget;
	public Vector3 m_vMovement;
	public bool m_bStartMoving = false;
	public Vector3 m_vRotation;
	public Vector3 m_vRotationBackwards;
	public bool m_bMovementComplete = false;

	public Vector3 m_endRotation;

    //Private variables
    public Vector3 m_vStartPosition;
	public Vector3 m_vOriginalPosition;
	private float m_fMovementProgress = 0.0f;
	private float m_fbackWardMovementProgress = 0.0f;
	private Vector3 m_vRotationElapsed;
	private bool RotationDone;
	// Use this for initialization
	void Start () {
		if(m_vMovement != Vector3.zero)
		{
			m_vTarget = transform.position + m_vMovement;
		}
		m_vOriginalPosition = transform.position;
        m_vStartPosition = transform.position;

    }

	// Update is called once per frame
	void Update () 
	{
		if(m_bStartMoving && !m_bMovementComplete)
		{
			m_fMovementProgress += Time.deltaTime * 0.5f;
			//m_fbackWardMovementProgress -= Time.deltaTime * 0.5f;

			if(bPosition){
				transform.position = Vector3.Lerp(m_vOriginalPosition, m_vTarget, m_fMovementProgress);
			}

			//Rotating Forwards
			if(bRotation){

				StartCoroutine(rotateAsCoroutine(this.gameObject,m_endRotation));
				//m_vRotationElapsed = m_vRotation * m_fMovementProgress;
				//transform.Rotate(m_vRotationElapsed);
			}

			//Movement is completed
			if(m_fMovementProgress >= 1.0f){
				m_bMovementComplete = true;
				if (!m_endRotation.Equals (Vector3.zero)) {
					//easy way first:
					//this.transform.rotation = Quaternion.Euler(m_endRotation);

					//slightly harder way:
					//StartCoroutine(rotateAsCoroutine(this.gameObject,m_endRotation));
				}
			}
			if(Application.loadedLevel == 7){
				if (GameObject.Find ("Level6") == null) {
					return;
				}
				if (GameObject.Find("Level6").GetComponent<LevelSixScript>().DoneWithGuard == false){
					//Door 1
					if(Global.m_L6CurrentDoorNum == 1){
						if(GameObject.Find ("Door1LeftTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
						if(GameObject.Find ("Door1RightTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
					}
					//Door 2
					if(Global.m_L6CurrentDoorNum == 2){
						if(GameObject.Find ("Door2LeftTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
						if(GameObject.Find ("Door2RightTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
					}
					//Door 3
					if(Global.m_L6CurrentDoorNum == 3){
						if(GameObject.Find ("Door3LeftTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
						if(GameObject.Find ("Door3RightTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
					}
					//Door 4
					if(Global.m_L6CurrentDoorNum == 4){
						if(GameObject.Find ("Door4LeftTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
						if(GameObject.Find ("Door4RightTrigger").GetComponent<DoorTriggerEnter>().m_DoorTriggerEntered){
							m_bMovementComplete = true;
						}
					}
				}
			}
		}
	}

	public void RecalcMovement()
	{
		m_fMovementProgress = 0.0f;
		m_vOriginalPosition = transform.position;
		if(m_vMovement != Vector3.zero)
		{
			m_vTarget = transform.position + m_vMovement;
		}
	}

	public void ResetOriginalPos()
	{
		m_vOriginalPosition = transform.position;
	}

	IEnumerator rotateAsCoroutine(GameObject go, Vector3 finalRotation){
		//float finalTime = Time.fixedTime + 1;
		//while (Time.fixedTime < finalTime) {
		RotationDone = false;
		float Angle;
		Angle = Quaternion.Angle (go.transform.rotation, Quaternion.Euler (finalRotation));

		while(!RotationDone){
			if(Quaternion.Angle(go.transform.rotation, Quaternion.Euler (finalRotation)) < 0.5f){
				RotationDone = true;
			}
			go.transform.rotation = Quaternion.RotateTowards (go.transform.rotation, Quaternion.Euler (finalRotation), Time.deltaTime * 1f);
			yield return null;
		}
	}

}
