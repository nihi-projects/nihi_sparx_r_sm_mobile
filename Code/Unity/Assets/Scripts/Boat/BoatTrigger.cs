﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class BoatTrigger : MonoBehaviour {

	#region PUBLIC fields

	//Reference of the manager that contains all the data
	public BoatCutSceneManager _boatCutSceneManagerRef;

	//Times for this trigger
	public float CutSceneInitialTime = 0f;
	public float CutSceneStopTime = 0f;

	//Teleport gameobject
	public GameObject teleportPos;

	#endregion




	#region PRIVATE callbacks/methods/coroutines

	void OnTriggerEnter(Collider col){

		if (_boatCutSceneManagerRef.CinematicTrigger == false) {


			if (col.gameObject.name == "Girl" || col.gameObject.name == "Boy") {

				Debug.Log ("Yes. the name is " + col.gameObject.name);

				//Let's setup the initial time for this cut-scene
				_boatCutSceneManagerRef.boatTimeline.initialTime = CutSceneInitialTime;

				//Trigger cut-scene
				_boatCutSceneManagerRef.boatTimeline.gameObject.SetActive (true);	

				//Time to delay
				StartCoroutine (timeToDelay (teleportPos.transform)); //As the cinematic starts, 

				//We make sure to speak to the manager and tell the cinematic is already running
				//We disable mesh renders using this property as well
				_boatCutSceneManagerRef.CinematicTrigger = true;
			}

		}
			
	}


	//We use this coroutine to calculate the time we need on the timeline and trigger things
	private IEnumerator timeToDelay (Transform teleportPos){

		//Set the player in position
		Global.GetPlayer ().transform.position = teleportPos.transform.position = teleportPos.transform.position; 

		//During the cinematic, we disable both placeholders
		_boatCutSceneManagerRef.SetUpPlaceholders (false, false);


		while (true) {

			//Exit condition
			if (_boatCutSceneManagerRef.boatTimeline.time >= CutSceneStopTime) {


				_boatCutSceneManagerRef.boatTimeline.gameObject.SetActive (false);

				//At the end of the cinematic, we enable one of the placeholders
				switch (transform.name) {

				case "BoatTrigger-Dock":

					_boatCutSceneManagerRef.SetUpPlaceholders (false, true);

					break;

				case "BoatTrigger-Collectible":

					_boatCutSceneManagerRef.SetUpPlaceholders (true, false);

					break;


				}
					
				//Meshes are back
				_boatCutSceneManagerRef.CinematicTrigger = false;
				yield break;

			}

			yield return null;

		}

	}
	#endregion




}
