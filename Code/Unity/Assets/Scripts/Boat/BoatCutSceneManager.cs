﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;



public class BoatCutSceneManager : MonoBehaviour {

	#region PUBLIC fields

	//Gameobjects
	public GameObject[] boatsPlaceHolder; //Just the original boat positions when we are not in the cut-scene

	//A timeline
	public PlayableDirector boatTimeline; //Timeline ref

	//Fire particle that goes with the player but not in the same hierarchy
	public GameObject fireball;

	#endregion

	#region PRIVATE fields

	[SerializeField]
	private bool cinematicTrigger = false;

	#endregion


	#region PROPERTIES

	public bool CinematicTrigger{

		get { return cinematicTrigger; }
		set {

			cinematicTrigger = value;

			if (cinematicTrigger) {

				//Each time we set up this property as "true", we must hide all the character renderers
				MeshRenderersActivation (false);
				fireball.SetActive (false);

			} else {

				//Same with the opposite. We get back the mesh renderers.
				MeshRenderersActivation (true);
				fireball.SetActive (true);
			}

		}

	}
		
	#endregion



	// Use this for initialization
	void Start () {

		//First and second boat
		SetUpPlaceholders (true, false);
				
	}


	public void SetUpPlaceholders(bool p1, bool p2){

		//Set up placeholders
		if (boatsPlaceHolder [(int)boatPlaceholder.firstBoat].activeSelf != p1) {
			boatsPlaceHolder[(int)boatPlaceholder.firstBoat].SetActive  (p1);
		}

		if (boatsPlaceHolder [(int)boatPlaceholder.secondBoat].activeSelf != p2) {
			boatsPlaceHolder[(int)boatPlaceholder.secondBoat].SetActive  (p2);

		}

	}

	#region Misc

	public enum boatPlayerSpawner{

		CollectiblePlace, DockPlace

	}

	public enum boatPlaceholder{

		firstBoat, secondBoat

	}

	#endregion


	#region PRIVATE methods

	private void MeshRenderersActivation(bool activation){

		GameObject currentPlayerObj = Global.GetPlayer (); //We store the current player object after detecting the trigger

		foreach (Transform hierarchy in currentPlayerObj.transform.GetComponentsInChildren <Transform>()) {

			if (hierarchy.GetComponent <Renderer> ()) {

				Renderer temp;
				temp = hierarchy.GetComponent <Renderer> ();
				temp.enabled = activation;

			}
		}

        if (activation == true)
            currentPlayerObj.GetComponent<BodyPartHelper>().SetHairStyle();
    }

	#endregion
}

