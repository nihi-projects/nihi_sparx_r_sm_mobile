using UnityEngine;
using System.Collections;

public enum Lv7BridgeGnatStatus
{
	Attacking,
	Standby,
	BackToOriginalPos,
	FlyingUp,
	Defeated_BackToOriginalPos,
	WaitForAttack,
	Pause,
	End,
}

public class Lv7BridgeGnatAction : MonoBehaviour {
	
	Vector3 OriginalPos;
	public Lv7BridgeGnatStatus action;
	float fStandbyTime = 3.0f;
	float fRespawnTime = 2.0f;
	// Use this for initialization
	void Start () {
		OriginalPos = transform.position;
		Reset();
	}
	
	// Update is called once per frame
	void Update () {
		switch(action)
		{
		case Lv7BridgeGnatStatus.Attacking:
			Attacking ();
			break;
		case Lv7BridgeGnatStatus.Standby:
			DefeadedStandby();
			break;
		case Lv7BridgeGnatStatus.FlyingUp:
			FlyingUp();
			break;
		case Lv7BridgeGnatStatus.WaitForAttack:
			WaitForAttack();
			break;
		case Lv7BridgeGnatStatus.End:
			Ending();
			break;
		case Lv7BridgeGnatStatus.Defeated_BackToOriginalPos: // Fall through
		case Lv7BridgeGnatStatus.BackToOriginalPos:
			ReturnToOriginalPos();
			break;
		case Lv7BridgeGnatStatus.Pause: // Fall through
		default:break;
		}
	}
	void Attacking()
	{
		if(MoveToPos(Global.GetPlayer ().transform.position))
		{
			action = Lv7BridgeGnatStatus.BackToOriginalPos;
			Global.GetPlayer ().GetComponent<Animation>().Play ("aim hit");
		}
	}
	
	void ReturnToOriginalPos()
	{
		if(MoveToPos(OriginalPos))
		{
			if(Lv7BridgeGnatStatus.BackToOriginalPos == action)
			{
				fStandbyTime = Random.Range(2.0f, 5.0f);
				action = Lv7BridgeGnatStatus.WaitForAttack;
			}
			else if(Lv7BridgeGnatStatus.Defeated_BackToOriginalPos == action)
			{
				action = Lv7BridgeGnatStatus.FlyingUp;
			}
		}
	}
	void WaitForAttack()
	{
		fStandbyTime -= Time.deltaTime;
		if(fStandbyTime <= 0.0f)
		{
			action = Lv7BridgeGnatStatus.Attacking;
		}
	}
	
	bool MoveToPos(Vector3 moveto)
	{
		transform.position = Vector3.Lerp(transform.position,
										  moveto,
										  Time.deltaTime);
		if(Vector3.Distance(transform.position,	
							moveto) <= 1.0f)
		{
			return (true);
		}
		return(false);
	}
	
	void DefeadedStandby()
	{
		fRespawnTime -= Time.deltaTime;
		if(fRespawnTime <= 0.0f)
		{
			action = Lv7BridgeGnatStatus.BackToOriginalPos;
		}
	}	
	void FlyingUp()
	{
		Vector3 Destination = OriginalPos;
		Destination.y += 5.0f;
		
		if(MoveToPos(Destination))
		{
			action = Lv7BridgeGnatStatus.Standby;
		}
	}
	void Ending()
	{
		Vector3 Destination = OriginalPos;
		Destination.y += 5.0f;
		
		if(MoveToPos(Destination))
		{
			this.gameObject.SetActive(false);
		}
	}
	
	public void Reset()
	{
		action = Lv7BridgeGnatStatus.WaitForAttack;
		fStandbyTime = Random.Range (1.0f, 10.0f);
		fRespawnTime = 2.0f;
	}
}
