﻿using UnityEngine;
using System.Collections;

public class L4LadderHelpTrigerRight : MonoBehaviour {

	public bool m_rightLadderTriggerEntered = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other) 
	{
		m_rightLadderTriggerEntered = true;
	}

	void OnTriggerExit(Collider other) 
	{
		m_rightLadderTriggerEntered = false;
	}
}
