using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WordSearchPuzzle : MonoBehaviour {
	public GameObject Particles;
	public string Grid;
	public int GridWidth = 6;
	public int GridHeight = 6;
	public string GoalString;
	public int NumOfPhrase = 3;
	private float BlockLeft = Screen.width*0.3135f;
	private float BlockTop = Screen.height*0.175f;
	private float BlockWidth = Screen.width*0.429f;
	private float BlockHeight = Screen.height*0.67f;
	public GUISkin Skin;
	private float m_fEndTimer = 2.0f;
	private bool[] ClearedLetters = new bool[36];
	public bool allclear = false;
	
	public Texture2D m_5Action2Texture;
	
	// Use this for initialization
	void Start () {
		for(uint i = 0; i < NumOfPhrase; ++i)
		{
			GameObject.Find ("Phrase" + (i + 1)).GetComponent<GUIText>().material.color = new Color(0.44f, 0.44f, 0.44f, 1.0f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		allclear = true;
		for(uint i = 0; i < NumOfPhrase; ++i)
		{
			
			if(!GameObject.Find ("Phrase" + (i + 1)).GetComponent<PhraseScript>().m_bClear)
			{
				allclear = false;
			}
		}
		
		if(allclear)
		{
			//gameObject.guiTexture.texture = (Texture2D)Resources.Load ("Mini Game/5_action2");
			gameObject.GetComponent<GUITexture>().texture = m_5Action2Texture;
			m_fEndTimer -= Time.deltaTime;
			if(m_fEndTimer <= 0.0f)
			{
				gameObject.SetActive(false);
			}
		}
	}
	
	void OnGUI()
	{
		int index = 0;
		
		// Cell width and height
		float CellWidth = BlockWidth/6;
		float CellHeight = BlockHeight/6;
		for(int h = 0; h < GridHeight; ++h)
		{
			for(int w = 0; w < GridWidth; ++w)
			{
				Rect Cell = new Rect(BlockLeft +(w * CellWidth), BlockTop +(h * CellHeight), CellWidth, CellHeight);
				GUIStyle Style = Skin.customStyles[6];
				if(ClearedLetters[index])
				{
					Style = Skin.customStyles[37];
				}
				
				if(GUI.Button(Cell, Grid[index].ToString(), Style))
				{
					// found it
					for(int i = 0; i < NumOfPhrase; ++i)
					{
						bool CorrectLetter = GameObject.Find ("Phrase" + (i + 1)).GetComponent <PhraseScript>().CheckLetter(index);
						if(CorrectLetter)
						{
							// Change the button into GUI text.
							ClearedLetters[index] = true;
							Vector3 ScreenToWorld =  Camera.main.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Cell.center.x, Cell.center.y, 20));
							ScreenToWorld.y = -ScreenToWorld.y;
							//GameObject.Find ("PuzzleParticle").transform.position = ScreenToWorld;
							//GameObject.Find ("PuzzleParticle").GetComponent<ParticleSystem>().Play();
						}
					}
				}
				index +=1;
			}
		}
	}
	
	void Awake()
	{
		if (Particles)
		{
			Particles.SetActive(true);
		}
	}
}
