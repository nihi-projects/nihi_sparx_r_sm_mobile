using UnityEngine;
using System.Collections;

public class PushRockMiniGame : MonoBehaviour {
	
	public GameObject[] Rocks;
	public int[] RockFallToPos;
	public bool[] EmptySpace;
	public string CameraID;
	public Transform TriggerPos;
	public Transform[] Barriers;
	public Transform[] MoveToPos;
	public bool Triggered = false;
	public float FallInterval = 0.0f;
	public float fMin;
	public float fMax;
	public int m_iCurrentRock = 0;
	public int m_iNumOfRockPushed = 0;
	public float m_fExplodeTimer = 10.0f;

    public float endCutsceneLength = 8.0f;
	public float m_fEndTimer = 0f;
	
	public Texture2D m_stream4Texture;
	public Texture2D m_stream3Texture;
	public Texture2D m_stream2Texture;
	public Texture2D m_stream1Texture;
	
	private bool m_bStartMovebackToLava2 = false;
	
	// pirvates
	
	Vector3 CurrentPos = Vector3.zero;
	Vector3 TargetPos = Vector3.zero;
	
	int RockPushed = -1;
	
	bool PlayGame = false;
	bool Moving = false;

    public GameObject explosionCutscene;
	
	void Start()
	{
        m_fEndTimer = endCutsceneLength;
        EmptySpace = new bool[3];
		for(int i = 0; i < EmptySpace.Length;++i)
		{
			EmptySpace[i] = true;
		}
		FallInterval = Random.Range (fMin, fMax);
		
		this.enabled = false;
	}
	
	void Update()
	{
        if( Global.GetPlayer() == null )
        {
            return;
        }

		if (!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "PushRockMiniGame");
		}
		
		if(false == Triggered)
		{
			CheckTriggered();
		}
		else
		{
			if(m_iCurrentRock < Rocks.Length)
			{
				ProcessRockFall();
			}
			ProcessPushCheck();
		}
		
		if(!m_bStartMovebackToLava2 && m_iNumOfRockPushed >= Rocks.Length)
		{
			// Wait then switch camera and turn off all script
			StartCoroutine(WaitToEnd());
		}
		
		bool FulledUp = true;
		foreach(bool space in EmptySpace)
		{
			if(space == true)
			{
				FulledUp = false;
			}
		}
		if(FulledUp)
		{
			// Timer for explosion
			m_fExplodeTimer -= Time.deltaTime;
			if(m_fExplodeTimer <= 0.0f)
			{
				foreach(GameObject rock in Rocks)
				{
					rock.transform.Find("Text").GetComponent<GUIText>().enabled = false;
				}
                //GameObject.Find ("Overlay").guiTexture.texture = (Texture2D)Resources.Load("UI/steam4");
                //GameObject.Find ("Overlay").GetComponent<GUITexture>().texture = m_stream4Texture;

                explosionCutscene.SetActive(true);
                WaitTilEnd ();
			}
			else if(m_fExplodeTimer <= 2.0f)
			{
				//GameObject.Find ("Overlay").guiTexture.texture = (Texture2D)Resources.Load("UI/steam3");
				GameObject.Find ("Overlay").GetComponent<GUITexture>().texture = m_stream3Texture;
			}
			else if(m_fExplodeTimer <= 4.0f)
			{
				//GameObject.Find ("Overlay").guiTexture.texture = (Texture2D)Resources.Load("UI/steam2");
				GameObject.Find ("Overlay").GetComponent<GUITexture>().texture = m_stream2Texture;
			}
			else if(m_fExplodeTimer <= 7.0f)
			{
				//GameObject.Find ("Overlay").guiTexture.texture = (Texture2D)Resources.Load("UI/steam1");
				GameObject.Find ("Overlay").GetComponent<GUITexture>().texture = m_stream1Texture;
				GameObject.Find ("Overlay").GetComponent<GUITexture>().enabled = true;
			}
		}
		
		//Second conversation with lava2 starts after moving rock mini game is finished 
		if(m_bStartMovebackToLava2)// && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition)
		{
			Global.CurrentInteractNPC = "npclava2";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3Npc2Scene2");
			//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true);
			this.enabled = false;
		}
	}
	void WaitTilEnd()
	{
		m_fEndTimer -= Time.deltaTime;
		if(m_fEndTimer <= 0.0f)
		{
            explosionCutscene.SetActive(false);
			GameObject.Find ("Overlay").GetComponent<GUITexture>().enabled = false;
			m_fEndTimer = endCutsceneLength;
			m_fExplodeTimer = 10.0f;
			foreach(GameObject rock in Rocks)
			{
				rock.GetComponent<DropRock>().Reset();
			}
			for(uint i = 0; i < EmptySpace.Length; ++i)
			{
				EmptySpace[i] = true;
			}
			foreach(GameObject rock in Rocks)
			{
				rock.transform.Find("Text").GetComponent<GUIText>().enabled = true;
			}
			FallInterval = Random.Range (fMin, fMax);
			m_iCurrentRock = 0;
			m_iNumOfRockPushed = 0;
		}
	}
	
	IEnumerator WaitToEnd()
	{
		yield return new WaitForSeconds(4.0f);
		GameObject.Find ("Overlay").GetComponent<GUITexture>().enabled = false;
		Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
		for(uint i = 0; i < Barriers.Length;++i)
		{
			Barriers[i].GetComponent<Collider>().enabled = false;
		}
		
		//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true);
		
		//Player automatically walk to npc2lalva
		GameObject.Find("npclava2").GetComponent<CharacterInteraction>().InteractionRadius = 25.0f;
		GameObject.Find("npclava2").GetComponent<CharacterInteraction>().Interacting = false;
		GameObject.Find("npclava2").GetComponent<CharacterInteraction>().Interacted = false;
		//Global.GetPlayer().GetComponent<PlayerMovement>().
		//				MoveToPosition(GameObject.Find ("npclava2").transform.position
		//								+ (GameObject.Find ("npclava2").transform.forward * 1.7f), transform.position);
		
		m_bStartMovebackToLava2 = true;
	}
	
	void CheckTriggered()
	{
		if(PlayGame)
		{
			Triggered = true;
			PlayGame = false;
			Global.MoveCameraBasedOnID(CameraID);
			GameObject player =  Global.GetPlayer();
			player.GetComponent<PlayerMovement>().MoveToPosition(TriggerPos.position, new Vector3(Rocks[1].transform.position.x, 
																						 player.transform.position.y, 
																						 Rocks[1].transform.position.z));
			//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true);
			for(uint i = 0; i < Barriers.Length;++i)
			{
				Barriers[i].GetComponent<Collider>().enabled = true;
			}
		}
	}
	
	void ProcessRockFall()
	{
		// Process fall interval
		FallInterval -= Time.deltaTime;
		if(FallInterval <=0.0f)
		{
			FallInterval = 0.0f;
			// Make the boulder fall
			if(EmptySpace[RockFallToPos[m_iCurrentRock]])
			{
				// The place is empty
				// drop the boulder.
				Rocks[m_iCurrentRock].GetComponent<DropRock>().m_bIsDropping = true;
				
				// Set the empty space to false
				EmptySpace[RockFallToPos[m_iCurrentRock]] = false;
				m_iCurrentRock += 1;
			}
			// Reset interval
			FallInterval = Random.Range(fMin, fMax);
		}
	}
	
	void ProcessPushCheck()
	{
		GameObject player = Global.GetPlayer();
		
		if (!Moving && !player.GetComponent<PlayerMovement>().m_bMovingToPosition)
		{
			if (Input.GetMouseButtonDown(0))
			{
				for (int i = 0; i < Rocks.Length; ++i)
				{
					// check to see if the player clicked on a rock
					if (Rocks[i].name == Global.GetMousePosToObj())
					{
						RockPushed = i;
						// move the player
						CurrentPos = player.transform.position;
						TargetPos = MoveToPos[RockFallToPos[i]].position;
						Vector3 vRock = Rocks[i].transform.position;
						vRock = new Vector3(vRock.x, player.transform.position.y, vRock.z);
						player.GetComponent<PlayerMovement>().MoveToPosition(TargetPos, vRock);
						Moving = true;
						//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true);
					}
				}
			}
		}
		// if the character is moving
		else if (Moving)
		{
			// check if the player ahs stopped moving
			if (!player.GetComponent<PlayerMovement>().m_bMovingToPosition)
			{
				Moving = false;
				// PUSH!
				Rocks[RockPushed].GetComponent<DropRock>().PushOff();
				Rocks[RockPushed].GetComponent<SphereCollider>().enabled = false;
				EmptySpace[RockFallToPos[RockPushed]] = true;
				m_iNumOfRockPushed += 1;
			}
		}
	}
	
	public void StartGame()
	{
		PlayGame = true;
	}
}
