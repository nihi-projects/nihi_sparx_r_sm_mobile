﻿using UnityEngine;
using System.Collections;

public class L4LadderHelpTrigerLeft : MonoBehaviour {

	public bool m_leftLadderTriggerEntered = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other) 
	{
		m_leftLadderTriggerEntered = true;
	}

	void OnTriggerExit(Collider other) 
	{
		m_leftLadderTriggerEntered = false;
	}

}
