using UnityEngine;
using System.Collections;

public class PuzzlePiece : MonoBehaviour {
	
	private Vector2 m_vCorrectPos;
	private bool m_bOnFocus = false;
	private bool m_bCanClick = true;
	public bool m_bCorrectPosition = false;
	public ParticleSystem Particles;
	// Use this for initialization
	void Start () {
		foreach(GUIText text in GetComponentsInChildren<GUIText>())
		{
			text.material.color = Color.black;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_bCanClick && Input.GetMouseButtonDown(0))
		{
			Vector3 mouse = Input.mousePosition;
			mouse = new Vector3(mouse.x / Screen.width, mouse.y / Screen.height, mouse.z);
			if (mouse.y > (transform.position.y - 0.07) && 
				mouse.y < (transform.position.y + 0.07) &&
				mouse.x > (transform.position.x - 0.04375) &&
				mouse.x < (transform.position.x + 0.04375))
			{
				m_bOnFocus = true;
				m_bCorrectPosition = false;
				for(int i = 0; i < 9; ++i)
				{
					if (this != GameObject.Find("Piece" + (i+1)))
					{
						GameObject.Find("Piece" + (i+1)).GetComponent<PuzzlePiece>().CanClick(false);
					}
				}
			}
		}
		
		if (m_bOnFocus)
		{
			Vector3 mousepos = Input.mousePosition;
			mousepos.x /= Screen.width;
			mousepos.y /= Screen.height;
			this.transform.position = mousepos;
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			if(m_bOnFocus)
			{
				m_bOnFocus = false;
				// Check position against main jig saw puzzle.
				if(Vector2.Distance(m_vCorrectPos, this.transform.localPosition) < 0.1f)
				{
					this.transform.localPosition = m_vCorrectPos;
					m_bCorrectPosition = true;
				}
			}
		}
	}
	
//	void OnMouseDrag()
//	{
//		m_bOnFocus = true;
//		m_bCorrectPosition = false;
//		Vector3 mousepos = Input.mousePosition;
//		mousepos.x /= Screen.width;
//		mousepos.y /= Screen.height;
//		this.transform.position = mousepos;
//	}
	
//	void OnMouseUp()
//	{
//		if(m_bOnFocus)
//		{
//			m_bOnFocus = false;
//			// Check position against main jig saw puzzle.
//			if(Vector2.Distance(m_vCorrectPos, this.transform.localPosition) < 0.1f)
//			{
//				this.transform.localPosition = m_vCorrectPos;
//				m_bCorrectPosition = true;
//			}
//		}
//	}
	
	public void SetCorrectPos(Vector2 CorrectPos)
	{
		m_vCorrectPos = CorrectPos;
	}
	
	public bool IsFocused()
	{
		return (m_bOnFocus);
	}
	
	public void CanClick(bool click)
	{
		m_bCanClick = click;
	}
}
