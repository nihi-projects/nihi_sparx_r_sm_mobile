using UnityEngine;
using System.Collections;

public class LevelSixInBuildingMiniGame : MonoBehaviour {
	
	
	bool m_bMoveToPos = false;
	bool m_bStart = false;
	public GnatStatus CurrentStatus = GnatStatus.Attacking;
	public GameObject[] Gnats;
	Vector3[] BasePos;
	public float DefeatedFlyUp;
	int iCurrentGnat = 0;
	// Use this for initialization
	void Start () {
		iCurrentGnat = Random.Range (0, 4);
		BasePos = new Vector3[Gnats.Length];
		for(int i = 0; i < Gnats.Length; ++i)
		{	
			BasePos[i] = Gnats[i].transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(false == Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovingToPosition && m_bMoveToPos)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "LevelSixInBuildingMiniGame");
			if (!Global.GetPlayer().GetComponent<Animation>().IsPlaying("aim shoot"))
			{
				Global.GetPlayer ().GetComponent<Animation>().Play("aim idle");
			}
			m_bStart = true;
		}
		if(m_bStart)
		{
			if(CheckEnd())
			{
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "LevelSixInBuildingMiniGame2");
				// Open Chest, display gem, turn on eagle.
				Global.GetGameObjectByID("36019").GetComponent<ObjectMovement>().m_bStartMoving = true;
				Global.GetGameObjectByID("36012_").GetComponent<ObjectMovement>().m_bStartMoving = true;
				//GameObject.Find("Gem6").GetComponent<GetGem>().enabled = true;
				
				this.gameObject.SetActive(false);
			}
			else
			{ 
				switch(CurrentStatus)
				{
				case GnatStatus.Attacking:
					Attack ();
					CheckAttack();
					break;
					
				case GnatStatus.Defeated_BackToBase: // Fall through	
				case GnatStatus.BackToBase:
					BackToBase();
					break;
					
				case GnatStatus.Defeated:
					Defeated ();
					break;
				}
			}
		}
	}
	
	void Attack()
	{
		Gnats[iCurrentGnat].transform.position = 
			Vector3.Lerp(Gnats[iCurrentGnat].transform.position,
						 Global.GetPlayer ().transform.position,
						 Time.deltaTime);
		if(Vector3.Distance(Gnats[iCurrentGnat].transform.position,
							Global.GetPlayer ().transform.position) <= 1.0f)
		{
			CurrentStatus = GnatStatus.BackToBase;
		}
	}
	
	void BackToBase()
	{
		Gnats[iCurrentGnat].transform.position = 
			Vector3.Lerp (Gnats[iCurrentGnat].transform.position,
						  BasePos[iCurrentGnat], Time.deltaTime);
		
		if(Vector3.Distance(Gnats[iCurrentGnat].transform.position,
							BasePos[iCurrentGnat]) <= 1.0f)
		{
			if(GnatStatus.BackToBase == CurrentStatus)
			{
				if(!CheckEnd())
				{
					iCurrentGnat = Random.Range (0, 4);
					while(!Gnats[iCurrentGnat].activeSelf)
					{
						iCurrentGnat = Random.Range (0, 4);
					}
					CurrentStatus = GnatStatus.Attacking;
				}
			}
			else
			{	
				CurrentStatus = GnatStatus.Defeated;
			}
		}
	}
	
	void Defeated()
	{
		Vector3 TargetPos = BasePos[iCurrentGnat];
		TargetPos.y += DefeatedFlyUp;
		
		Gnats[iCurrentGnat].transform.position = Vector3.Lerp (Gnats[iCurrentGnat].transform.position,
																TargetPos, Time.deltaTime);
		
		if(Vector3.Distance(Gnats[iCurrentGnat].transform.position,TargetPos) <= 1.0f)
		{
			Gnats[iCurrentGnat].SetActive(false);
			if(!CheckEnd())
			{
				iCurrentGnat = Random.Range (0, 4);
				while(!Gnats[iCurrentGnat].activeSelf)
				{
					//Debug.Log (iCurrentGnat);
					iCurrentGnat = Random.Range (0, 4);
				}
				CurrentStatus = GnatStatus.Attacking;
			}
		}
	}

	void CheckAttack()
	{
		if(!Global.GetPlayerEffect().HasTarget() && Input.GetMouseButton(0))
		{
			GameObject hit = Global.GetMouseToObject();
			
			if(Gnats[iCurrentGnat] == hit)
			{
				Global.GetPlayerEffect().SetFireBallMoveTo(hit);
				SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
				Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
			}
		}
		
		if(Global.GetPlayerEffect().FireBallHitObject())
		{
			CurrentStatus = GnatStatus.Defeated_BackToBase;
		}
	}
	

	
	void OnTriggerEnter()
	{
		m_bMoveToPos = true;
		Vector3 lookatpos = Global.GetObjectPositionByID("36019");
		lookatpos.y = 1.0f;

		Global.GetPlayer ().GetComponent<PlayerMovement>().MoveToPosition(transform.position, lookatpos);
	}
	
	bool CheckEnd()
	{
		foreach(GameObject gnat in Gnats)
		{
			if(gnat.activeSelf)
			{
				return false;
			}
		}
		return true;
	}
}
