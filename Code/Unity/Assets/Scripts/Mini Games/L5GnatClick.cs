using UnityEngine;
using System.Collections;

public class L5GnatClick : MonoBehaviour 
{
	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	
	bool ClickAble = true;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	void OnMouseOver()
	{
		if (ClickAble)
		{
			//Change the cursor image of the mouse
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
		else
		{
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
			GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
	}
	
	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
	
	public void CantClick()
	{
		ClickAble = false;
	}
}
