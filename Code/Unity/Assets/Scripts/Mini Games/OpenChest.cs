using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpenChest : MonoBehaviour 
{
	public GameObject[] Buttons =new GameObject[6];
	public string[] ListOne =new string[3];
	public string[] ListTwo =new string[3];
	public string[] ListThree = new string[3];
	private int[] Index = new int[3];
	public int[] CorrectAnswer = new int[3];
	public string strCameraID;
	public bool m_bTextEnabled = false;
	private bool m_bActive = false;
	Component[] Texts;
	public Texture2D Background;
	public Texture2D Background_Unlocked;
	public Texture2D m_chestLight3y;
	public Texture2D m_chestLight3;
	public Texture2D m_chestLight2y;
	public Texture2D m_chestLight2;
	public Texture2D m_chestLight1y;
	public Texture2D m_chestLight1;
	// This can be changed to public based on future designs.
	private Rect[] m_Rect = new Rect[6];
	public Transform ChestLid;
	public Transform CaveDoor;
	bool m_bEndTimer= false;
	float m_fEndTimer = 0.0f;
	
	public bool m_bIs2D = false;
	public bool m_bTuiStartFlying = false;
	private bool m_bEffectsStarted = false;
	private bool m_bIsOpened = false;
	private GameObject m_gTuiFlyoutParticleClone = null;
	public GameObject m_gTuiFlyoutparticle = null;

    public Text[] m_threeWords;
	
	// Use this for initialization
	void Start () {
		if(!m_bIs2D)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "OpenChest");
			Global.GetPlayer().GetComponent<Animation>().Play ("idle");
			Initialise();
			m_Rect[0].Set (215f * Global.ScreenWidth_Factor, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[1].Set (460f * Global.ScreenWidth_Factor, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[2].Set (727f * Global.ScreenWidth_Factor, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[3].Set (215f * Global.ScreenWidth_Factor, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[4].Set (460f * Global.ScreenWidth_Factor, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[5].Set (727f * Global.ScreenWidth_Factor, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
		}
		else
		{
			Initialise();
			m_Rect[0].Set (Screen.width * 0.3223f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[1].Set (Screen.width * 0.45f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[2].Set (Screen.width * 0.6348f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[3].Set (Screen.width * 0.3223f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[4].Set (Screen.width * 0.45f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
			m_Rect[5].Set (Screen.width * 0.6348f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
		}
	}
	
	// Update is called once per frame
	void Update () {

      


        if ((Camera.main.GetComponent<CameraMovement>().m_bArrivedAtTarget && !m_bTextEnabled)
            ||
           (m_bIs2D && !m_bTextEnabled))
        {
            //Texts = this.GetComponentsInChildren<GUIText>();
            for (int i = 0; i < m_threeWords.Length; ++i)
            {
                //Texts[i].GetComponent<GUIText>().enabled = true;
                //Texts[i].GetComponent<GUIText>().material.color = Color.black;
                m_threeWords[i].enabled = true;
                
            }
            m_threeWords[0].text = ListOne[Index[0]];
            m_threeWords[1].text = ListTwo[Index[1]];
            m_threeWords[2].text = ListThree[Index[2]];

            //Texts[0].GetComponent<GUIText>().text = ListOne[Index[0]];
            //Texts[1].GetComponent<GUIText>().text = ListTwo[Index[1]];
            //Texts[2].GetComponent<GUIText>().text = ListThree[Index[2]];
            m_bTextEnabled = true;
        }
		//else if (m_bIs2D && !m_bTextEnabled)
		//{
		//	Texts = this.GetComponentsInChildren<GUIText>();
		//	for(int i = 0; i < 3; ++i)
		//	{
		//		//Texts[i].GetComponent<GUIText>().enabled = true;
		//		//Texts[i].GetComponent<GUIText>().material.color = Color.black;

  //              m_threeWords[i].enabled = true;

  //          }
		//	Texts[0].GetComponent<GUIText>().text = ListOne[Index[0]];
		//	Texts[1].GetComponent<GUIText>().text = ListTwo[Index[1]];
		//	Texts[2].GetComponent<GUIText>().text = ListThree[Index[2]];
		//	m_bTextEnabled = true;
		//}
		
		if(m_bTuiStartFlying){
			if(GameObject.Find("GUI") != null && !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
				Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene1";
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				this.enabled = false;
			}
		}
	}
	
	public void Initialise()
	{
		Index[0] = CorrectAnswer[0];
		Index[1] = CorrectAnswer[1];
		Index[2] = CorrectAnswer[2];
		while(Index[0] == CorrectAnswer[0] &&
			Index[1] == CorrectAnswer[1] && 
			Index[2] == CorrectAnswer[2])
		{
			for(uint i = 0 ; i < 3; ++i)
			{
				Index[i] = Random.Range(0,2);
			}
		}
		if(!m_bIs2D)
		{
			Global.MoveCameraBasedOnID(strCameraID);
			
			//Background = (Texture2D)Resources.Load("Mini Game/5_lockbg");
			//Background_Unlocked = (Texture2D)Resources.Load("Mini Game/5_lockbg2");
		}
		else
		{
			//Background = (Texture2D)Resources.Load("Mini Game/5_lockbg");
			//Background_Unlocked = (Texture2D)Resources.Load("Mini Game/5_lockbg2");
			GetComponent<GUITexture>().texture = Background;
			GetComponent<GUITexture>().enabled = true;
		}
		m_bActive = true;
		m_bIsOpened = false;
	}
	public void OnGUI()
	{
		if(!m_bIs2D && CheckUnlock())
		{
			// This is where the chest opens.
			ChestLid.GetComponent<ObjectMovement>().m_bStartMoving = true;
			CaveDoor.GetComponent<ObjectMovement>().m_bStartMoving = true;
			if(ChestLid.GetComponent<ObjectMovement>().m_bMovementComplete)
			{
				//Tui fly out particle play here
				Vector3 tuiFlyoutPaticalPos = Global.GetObjectPositionByID("31020");
				if (m_bEffectsStarted == false){
					m_bEffectsStarted = true;
					SoundPlayer.PlaySound("sfx_chest_opening", 1.0f);
					m_gTuiFlyoutParticleClone = (GameObject)Instantiate(m_gTuiFlyoutparticle, tuiFlyoutPaticalPos, Quaternion.identity);
				}
				
				//for(int i = 0; i < 3; ++i){
				//	Texts[i].GetComponent<GUIText>().enabled = false;
				//}
				
				//Tui fly out of the chest
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31022");
				m_bTuiStartFlying = true;
				
				Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "OpenChest2");
				GameObject.Find("Level1_Chest").GetComponent<ChestInteraction>().enabled = false;
			}
		}
		else if (CheckUnlock2())
		{
			this.GetComponent<GUITexture>().texture = Background_Unlocked;
			
			StartEndTimer();
		}
		else
		{
			if (!m_bIs2D) {
					
				m_Rect [0].Set ((float)Camera.main.WorldToScreenPoint (Buttons [0].transform.position).x, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);
				m_Rect [1].Set ((float)Camera.main.WorldToScreenPoint (Buttons [1].transform.position).x, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);
				m_Rect [2].Set ((float)Camera.main.WorldToScreenPoint (Buttons [2].transform.position).x, 268f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);
				m_Rect [3].Set ((float)Camera.main.WorldToScreenPoint (Buttons [0].transform.position).x, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);
				m_Rect [4].Set ((float)Camera.main.WorldToScreenPoint (Buttons [1].transform.position).x, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);
				m_Rect [5].Set ((float)Camera.main.WorldToScreenPoint (Buttons [2].transform.position).x, 422f * Global.ScreenHeight_Factor, 80 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor);

			} else {

				m_Rect[0].Set (Screen.width * 0.3223f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
				m_Rect[1].Set (Screen.width * 0.45f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
				m_Rect[2].Set (Screen.width * 0.6348f, Screen.height * 0.35f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
				m_Rect[3].Set (Screen.width * 0.3223f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
				m_Rect[4].Set (Screen.width * 0.45f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);
				m_Rect[5].Set (Screen.width * 0.6348f, Screen.height * 0.55f, 80 * Global.ScreenWidth_Factor,60 * Global.ScreenHeight_Factor);

			}

            // I added this code to get the interaction form the raycast 3D object. So we don't need now to reflect the 3D object's position on a 2D canvasas 2d buttons. SO I commented the MoveUp and Movedown lines from the next block in this function
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.transform.name);

                    switch (hit.transform.name) {

                        case "Cube0":
                            MoveUp(0);
                            break;
                        case "Cube1":
                            MoveUp(1);
                            break;
                        case "Cube2":
                            MoveUp(2);
                            break;
                        case "Cube5":
                            MoveDown(0);
                            break;
                        case "Cube4":
                            MoveDown(1);
                            break;
                        case "Cube3":
                            MoveDown(2);
                            break;

                    }
                }
            }

            if (GUI.Button (m_Rect[0], "", GUI.skin.label))
			{
				//MoveUp (0);
			}
			else if(GUI.Button (m_Rect[1], "", GUI.skin.label))
			{
				//MoveUp (1);
			}
			else if(GUI.Button (m_Rect[2], "", GUI.skin.label))
			{
				//MoveUp (2);
			}
			else if(GUI.Button (m_Rect[3], "", GUI.skin.label))
			{
				//MoveDown (0);
			}
			else if(GUI.Button (m_Rect[4], "", GUI.skin.label))
			{
				//MoveDown (1);
			}
			else if(GUI.Button (m_Rect[5], "", GUI.skin.label))
			{
				//MoveDown (2);
			}
		}
	
		if(m_fEndTimer > 0.0f && m_bIs2D)
		{
			m_fEndTimer -= Time.deltaTime;
			if(m_fEndTimer <= 0.0f)
			{
				gameObject.SetActive(false);
			}
		}
	}
	void StartEndTimer()
	{
		if(!m_bEndTimer)
		{
			m_fEndTimer = 2.0f;
			m_bEndTimer = true;
		}
	}
	
	public void MoveUp( int Row )
	{
		SoundPlayer.PlaySound("sfx_chest_scroll", 1.0f);
		int Num = Index[Row];
		Num -= 1;
		// Row to the last word
		if(Num < 0)
		{
			Num = 2;
		}
		Index[Row] = Num;

        //if (Texts [Row].GetComponent<GUIText> () != null) {
        switch (Row)
        {
            case 0:
                m_threeWords[Row].text = ListOne[Num];
                break;
            case 1:
                m_threeWords[Row].text = ListTwo[Num];
                break;
            case 2:
                m_threeWords[Row].text = ListThree[Num];
                break;
            default:
                break;
        }
		//}
	}
	
	public void MoveDown( int Row)
	{
        //if( Texts !=null && Texts[Row] != null )
        //{
            SoundPlayer.PlaySound("sfx_chest_scroll", 1.0f);
            int Num = Index[Row];
            Num += 1;
            // Row to the last word
            if (Num > 2)
            {
                Num = 0;
            }
            Index[Row] = Num;

            switch (Row)
            {
                case 0:
                    m_threeWords[Row].text = ListOne[Num];
                    break;
                case 1:
                    m_threeWords[Row].text = ListTwo[Num];
                    break;
                case 2:
                    m_threeWords[Row].text = ListThree[Num];
                    break;
                default:
                    break;
            }
        //}
	}
	
	public bool CheckUnlock()
	{
		if (ChestLid == null)
		{
			return (false);
		}
		bool unlocked = true;
		Texture mainTexture;
		if(Index[0] == CorrectAnswer[0])
		{
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight3y");
			mainTexture = m_chestLight3y;
		}
		else 
		{
			unlocked = false;
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight3");
			mainTexture = m_chestLight3;
		}
		ChestLid.GetComponent<MeshRenderer>().materials[3].SetTexture("_MainTex", mainTexture);
		
		if (Index[1] == CorrectAnswer[1])
		{
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight2y");
			mainTexture = m_chestLight2y;
		}
		else 
		{
			unlocked = false;
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight2");
			mainTexture = m_chestLight2;
		}
		ChestLid.GetComponent<MeshRenderer>().materials[1].SetTexture("_MainTex", mainTexture);
		
		if (Index[2] == CorrectAnswer[2])
		{
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight1y");
			mainTexture = m_chestLight1y;
		}
		else 
		{
			unlocked = false;
			//mainTexture = (Texture)Resources.Load("Mini Game/ChestLight1");
			mainTexture = m_chestLight1;
		}
		ChestLid.GetComponent<MeshRenderer>().materials[2].SetTexture("_MainTex", mainTexture);
		
		return(unlocked);
	}
	
	public bool CheckUnlock2()
	{
		if (Index[0] == CorrectAnswer[0] && 
			Index[1] == CorrectAnswer[1] &&
			Index[2] == CorrectAnswer[2])
		{
			return (true);
		}
		
		return (false);
	}
	
//	void OnMouseOver()
//	{
//		//Change the cursor image of the mouse
//		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
//		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
//	}
//	
//	void OnMouseExit ()
//	{
//		//Change the cursor image of the mouse
//		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
//		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
//	}
}
