using UnityEngine;
using System.Collections;

public class Lv7CatchSparx : MonoBehaviour {
	
	static public bool PrepareStart = false;
	static bool GameIsStarted = false;
	static public int NumOfSparksCaught = 0;
	static public float fTimer = 20.0f;
	static public bool m_bGameIsEnded = false;
	private GameObject move;
	private GameObject sprint;
	private GameObject bag;

	// Use this for initialization
	void OnEnable () {

		if(Application.isMobilePlatform){
			/*
			move = GameObject.Find("Move");
			sprint = GameObject.Find("Sprint");
			*/
		}
		bag = GameObject.Find("Bag");
	
	}
	
	// Update is called once per frame
	void Update () {
		if(PrepareStart){
			if(!Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovingToPosition){
				foreach(Transform spark in this.transform){
					spark.gameObject.SetActive(true);
				}
				
				Global.SnapCameraBasedOnID("100000");
				GameIsStarted = true;
				StartUpSparks();
				if(Application.isMobilePlatform){
					/*
					move.SetActive(false);
					sprint.SetActive(false);
					*/
					GameObject.Find("MobileSingleStickControl").GetComponent<Canvas>().enabled = false;
				}
				bag.SetActive(false);
				m_bGameIsEnded = false;
			}
		}
		if(GameIsStarted){
			fTimer -= Time.deltaTime;
			if(fTimer <= 0.0f){
				TerminateGame();
			}
			CheckHit();
		}
		if(NumOfSparksCaught >= 20 && m_bGameIsEnded){
			if(Application.isMobilePlatform){
				/*move.SetActive(true);
				sprint.SetActive(true);*/

				GameObject.Find("MobileSingleStickControl").GetComponent<Canvas>().enabled = true;
			}
			bag.SetActive(true);
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.CameraSnapBackToPlayer();
			
			//Dorra talk starts
			Global.CurrentInteractNPC = "Darro";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			this.enabled = false;
		}
		else if(GameObject.Find ("Level7").GetComponent<LevelSevenScript>().m_bPickSparkStarted && m_bGameIsEnded){
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.CameraSnapBackToPlayer();
			
			//Case talk again
			Global.CurrentInteractNPC = "Cass";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L7CassScene2");
			//The pick sparks game needs to start again
			GameObject.Find ("Level7").GetComponent<LevelSevenScript>().m_bCassFirstTalkFinished = true;
		}
	}
	
	/*
	 * Check whether the mouse is clicking on the spark
	 * 
    */
	void CheckHit()
	{
		if(Input.GetMouseButton(0))
		{
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit[] hit;
			
			hit = Physics.RaycastAll(ray);
			
			for(uint i = 0; i < hit.Length;++i)
			{
				if(hit[i].collider.gameObject.name == "Spark")
				{
					NumOfSparksCaught += 1;
					hit[i].collider.gameObject.GetComponent<FlyingSparx>().Spawn();
				}
			}
		}
	}
	
	/*
	 * Sparx start flying
	 * 
    */
	void StartUpSparks()
	{
		foreach(Transform spark in this.transform){
			spark.gameObject.GetComponent<FlyingSparx>().enabled = true;
		}
	}
	
	/*
	 * End the game
	 * 
    */
	void TerminateGame()
	{
		Transform tempSpark = null;
		foreach(Transform spark in this.transform){
			tempSpark = spark;
			tempSpark.gameObject.SetActive(false);
			
			//tempSpark.GetComponent<FlyingSparx>().Kill();
		}
		
		Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(true, "Lv7CatchSparx");
		
		m_bGameIsEnded = true;
	}
	
	void OnTriggerStay(Collider colObj)
	{
		if (colObj.gameObject == Global.GetPlayer())
		{
			GameObject.Find("Level7").GetComponent<LevelSevenScript>().StartHelpTimer();
		}
	}
}
