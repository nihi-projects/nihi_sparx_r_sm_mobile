using UnityEngine;
using System.Collections;

public enum Lv5MiniGame2Status
{
	PickingGnat,
	GnatFlyDown,
	AnsweringQuestions,
	GnatFlyBack,
	GnatChangeToSparx,
	AfterDialog,
	SparxFlyAway,
	
	// After all the gnats are defeated
	WalkToGemPos,
	GetGem,
};
public class Level5MiniGame2 : MonoBehaviour {
	
	public Transform CenterPos;
	Vector3 GnatOriginalPos;
	public GameObject CurrentObject;
	public bool GameStart = false;
	public string CameraID;
	Lv5MiniGame2Status Status = Lv5MiniGame2Status.PickingGnat;
	Vector3 SparkFlyToPos;
	
	GameObject explosionEffect = null;
	
	Vector3 StartPos = Vector3.zero;
	float m_fTime = 0.0f;
	
	private bool m_bTuiConversationStarts = false;
	private bool m_bPlayerMovingToPoint   = false;
	
	// Use this for initialization
	void Start () 
	{
		SparkFlyToPos = transform.Find("SparxTo").position;
	}
	
	// Update is called once per frame
	void Update () 
	{
        //Player arrived the start mini game 2 position, then mini game 2 starts
        if (m_bPlayerMovingToPoint && !Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovingToPosition){
			GameStart = true;
		
			this.GetComponent<Collider>().enabled = false;
			Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(false, "L5StepsMiniGame2");
			Global.MoveCameraBasedOnID(CameraID);
			Global.GetPlayer ().GetComponent<Animation>().Play ("aim idle");
			
			//Tui needs fly down
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("35033");
			m_bTuiConversationStarts = true;
			
			m_bPlayerMovingToPoint = false;
		}
		
		//Tui is flying down
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiConversationStarts){
			GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L5HopeScene2";
			m_bTuiConversationStarts = false;
			//Trigger the mini game starts in the "L5TuiTerminateScene.cs" file
		}
		
		if(!GameStart || m_bTuiConversationStarts || GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L5TuiTerminateScene")
		{
			return;
		}

        if (GameStart == true)
        {
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L5StepsMiniGame2");
        }
        switch (Status)
		{
			case Lv5MiniGame2Status.PickingGnat:
				PickingGnat();
				break;
			case Lv5MiniGame2Status.GnatFlyDown:
				GnatFlyDown();
				break;
			case Lv5MiniGame2Status.AnsweringQuestions:
				AnswerQuestion();
				break;
			case Lv5MiniGame2Status.GnatChangeToSparx:
				GnatChangeToSparx();
				break;
			case Lv5MiniGame2Status.AfterDialog:
				AfterDialog();
				break;
			case Lv5MiniGame2Status.SparxFlyAway:
				SparxFlyAway();
				break;
			case Lv5MiniGame2Status.GnatFlyBack:
				GnatsFlyBack();
				break;
			case Lv5MiniGame2Status.WalkToGemPos:
                Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L5StepsMiniGame4");
                WalkToGemPos();
				break;
			case Lv5MiniGame2Status.GetGem:
                Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L5StepsMiniGame2");
                TakeGem();
				break;
			default:break;
		}
		
		if (explosionEffect && explosionEffect.GetComponent<ParticleSystem>().time >= 0.1f && 
			explosionEffect.GetComponent<ParticleSystem>().particleCount == 0)
		{
			Destroy(explosionEffect);
		}
	}
	
	void WalkToGemPos()
	{
		//Switch the camera to look at the Gem5
		Global.MoveCameraBasedOnID("25011");
		
		if(!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L5StepsMiniGame5");
			GameObject.Find("Gem").GetComponent<GetGem>().enabled = true;
			Status = Lv5MiniGame2Status.GetGem;
		}
	}
	void TakeGem()
	{
		if(GameObject.Find("Gem") != null)
		{
			Global.GemObtainedForTheLevel = true;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "L5StepsMiniGame6");
			this.enabled = false;
		}
	}
	
	void PickingGnat()
	{
		if(Input.GetMouseButton(0))
		{
			string[] AllHits = Global.GetMousePosToAllObj();
			foreach(string hit in AllHits)
			{
				if(hit.Contains("Gnat"))
				{
					// turn on the gnat's script.
					CurrentObject = transform.Find (hit).gameObject;
					Status = Lv5MiniGame2Status.GnatFlyDown;
					GnatOriginalPos = CurrentObject.transform.position;
					StartPos = GnatOriginalPos;
				}
			}
		}
	}
	
	void AfterDialog()
	{
		if(!CurrentObject.GetComponent<PlayDialog>().StartPlaying)
		{
			CurrentObject.GetComponent<PlayDialog>().StartDialog();
		}
		
		if(!CurrentObject.GetComponent<PlayDialog>().IsPlaying)
		{
			Status = Lv5MiniGame2Status.SparxFlyAway;
		}
	}
	
	void SparxFlyAway()
	{
		m_fTime += Time.deltaTime * 0.35f;
		CurrentObject.transform.position = Vector3.Lerp(CurrentObject.transform.position,
														SparkFlyToPos,
														m_fTime);
		if(m_fTime >= 1.0f)
		{
			m_fTime = 0.0f;
			CurrentObject.SetActive(false);
			bool bFinished = true;
			
			foreach(Transform allobj in transform)
			{
				if(allobj.name.Contains("Gnat"))
				{
					if(allobj.gameObject.activeSelf)
					{
						bFinished = false;
						break;
					}
				}
			}
			
			if(!bFinished)
			{
				Status = Lv5MiniGame2Status.PickingGnat;
			}
			else
			{
				/// NOTE!!! Put in the animation for getting the Gem;
				GameObject.Find ("Gem").GetComponent<ObjectMovement>().enabled = true;
				Status = Lv5MiniGame2Status.WalkToGemPos;
				Global.GetPlayer ().GetComponent<PlayerMovement>().MoveToPosition(transform.Find("WalkToPos").position,
																		  GameObject.Find ("Gem").transform.position);
				Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(true, "L5StepsMiniGame7");
				Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			}
		}
	}
	
	void GnatsFlyBack()
	{
		m_fTime += Time.deltaTime * 0.6f;
		CurrentObject.transform.position = Vector3.Lerp (StartPos,
														 GnatOriginalPos, m_fTime);
		CurrentObject.GetComponent<DisplayQuestion>().enabled = false;
		if(m_fTime >= 1.0f)
		{
			m_fTime = 0.0f;
			Status = Lv5MiniGame2Status.PickingGnat;
		}
	}
	
	void GnatFlyDown()
	{	
		m_fTime += Time.deltaTime * 0.6f;
		CurrentObject.transform.position = Vector3.Lerp (StartPos,
														 CenterPos.position,
														 m_fTime);
		if(m_fTime >= 1.0f)
		{
			m_fTime = 0.0f;
			CurrentObject.GetComponent<DisplayQuestion>().enabled = true;
			CurrentObject.GetComponent<DisplayQuestion>().Activate();
			Status = Lv5MiniGame2Status.AnsweringQuestions;
		}
	}
	
	void AnswerQuestion()
	{
		if(CurrentObject.GetComponent<DisplayQuestion>().bFinishedAnswering)
		{
			if(CurrentObject.GetComponent<DisplayQuestion>().bCorrectAnswer)
			{ 
				Status = Lv5MiniGame2Status.GnatChangeToSparx;
				StartPos = CurrentObject.transform.position;				
			}
			else
			{
				Status = Lv5MiniGame2Status.GnatFlyBack;
				StartPos = CurrentObject.transform.position;
			}
		}
	}
	
	void GnatChangeToSparx()
	{
		GameObject Sparx = CurrentObject.GetComponent<DisplayQuestion>().CorrespondingSparx;
		Sparx.transform.position = CurrentObject.transform.position;
		CurrentObject.SetActive(false);
		CurrentObject = Sparx;
		SoundPlayer.PlaySound("sfx-pop", 1.0f);
		explosionEffect = (GameObject)Instantiate(GameObject.Find("Gnat Explosion"), CurrentObject.transform.position, Quaternion.identity);
		explosionEffect.GetComponent<ParticleSystem>().Play();
		
		Status = Lv5MiniGame2Status.AfterDialog;
	}
	
	void OnTriggerEnter()
	{		
		//Player move to a point to play the mini game 2
		Global.GetPlayer ().GetComponent<PlayerMovement>().MoveToPosition(transform.Find("StandingPt").position,
																			GameObject.Find ("Gem").transform.position);
		m_bPlayerMovingToPoint = true;
	}
}
