using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class JigSawPuzzleWord : MonoBehaviour {
	
	public Vector2[] StartingPositions;
	public Vector2[] CorrectPosition;
	public string CorrectWord;
	public bool m_bSolved = false;
	bool m_bFinished = false;
	float m_fTimer = 2.0f;
	
	public Texture2D m_6perpsectivebg2;
	
	void Start()
	{
		ResetToStartingPos();
		AssignCorrectPosition();
	}
	
	void Update()
	{
		CheckSolved();
		if(m_bFinished)
		{
			m_fTimer -= Time.deltaTime;
			if(m_fTimer <= 0.0f)
			{
				
				m_bSolved = true;
				this.gameObject.SetActive(false);
			}
		}
	}
	
	void CheckSolved()
	{
		m_bFinished = true;
		for(uint i = 0; i < StartingPositions.Length; ++i)
		{
			Transform obj = transform.Find ("Piece" + (i+1));
			if(!obj.GetComponent<PuzzlePieceWord>().m_bCorrectPosition)
			{
				m_bFinished = false;
			}
		}
		if(m_bFinished)
		{
			//guiTexture.texture = Resources.Load ("Mini Game/6_perpsectivebg2") as Texture2D;
			GetComponent<GUITexture>().texture = m_6perpsectivebg2;
		}
	}
	
	void AssignCorrectPosition()
	{
		for(int i = 0; i < StartingPositions.Length; ++i)
		{
			Transform obj = transform.Find ("Piece" + (i+1));
			List<Vector2> correctpositions = new List<Vector2>();
			for(int j = 0; j < StartingPositions.Length; ++j)
			{
				char TEXT = obj.GetComponent<GUIText>().text[0];
				
				if(CorrectWord[j] == TEXT)
				{
					obj.GetComponent<PuzzlePieceWord>().SetCorrectPos(CorrectPosition[j]);
					break;
				}
			}
			
		}
	}
	
	void ResetToStartingPos()
	{
		for(int t = 0; t < StartingPositions.Length; ++t)
        {
			// Copy the current starting position
            Vector2 tmp = StartingPositions[t];
			// Get a ramdomized array element.
            int r = Random.Range(t, StartingPositions.Length);
			// Assing the randomized position to position t.
            StartingPositions[t] = StartingPositions[r];
			// copies the position t to the randomized position.
            StartingPositions[r] = tmp;
        }
		
		for(int i = 0; i < StartingPositions.Length; ++i)
		{
			Vector3 Vec = Vector3.zero;
			Vec.Set(StartingPositions[i].x, StartingPositions[i].y, 0.5f);
			Transform temp = transform.Find ("Piece" + (i+1));
			temp.localPosition = Vec;
			temp.GetComponent<PuzzlePieceWord>().SetStartPos(new Vector2(Vec.x, Vec.y));
		}
	}
}
