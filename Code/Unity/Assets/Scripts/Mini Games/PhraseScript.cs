using UnityEngine;
using System.Collections;

public class PhraseScript : MonoBehaviour {
	
	public int[] CorrectLetterIndex;
	private bool[] LettersCovered;
	public bool m_bClear = false;
	// Use this for initialization
	void Start () {
		LettersCovered = new bool[CorrectLetterIndex.Length];
		for(int i =0 ;i < LettersCovered.Length; ++i)
		{
			LettersCovered[i] = false;
		}
	}
	public bool CheckLetter(int index)
	{
		for(int i =0 ;i < CorrectLetterIndex.Length; ++i)
		{
			if(index == CorrectLetterIndex[i])
			{
				LettersCovered[i] = true;
				return(true);
			}
		}
		return(false);
	}
	// Update is called once per frame
	void Update () {
		if(!m_bClear)
		{
			bool bAllclear = true;
			foreach(bool letters in LettersCovered)
			{
				if(!letters)
				{
					bAllclear = false;
				}
			}
			if(bAllclear)
			{
				m_bClear = true;
				GetComponent<GUIText>().material.color = Color.black;
			}
		}
	}
}
