using UnityEngine;
using System.Collections;

public class JigSawPuzzle : MonoBehaviour {
	
	public Vector2[] StartingPositions;
	public Vector2[] CorrectPosition;
	private bool m_bAnimatePuzzleClear = false;
	private bool m_bCheckPuzzle = true;
	public bool bSolved = false;
	public int NumOfJigSawPuzzle = 9;
	
	public Texture2D m_5jigsawbg2;
	public Texture2D m_5jigsawbg;
	
	// Use this for initialization
	void Start () {
		
		Initialise();
	}
	
	// Update is called once per frame
	void Update () {
	
		// Testing the starting position position	
		if(m_bCheckPuzzle)
		{
			CheckPuzzleSolved();
		}
		if(m_bAnimatePuzzleClear)
		{
			AnimatePuzzleClear();
		}
		
		bool hasFocus = false;
		int piece =  -1;
		for(int i = 0; i < NumOfJigSawPuzzle; ++i)
		{
			if (GameObject.Find ("Piece" + (i+1)))
			{
				if (hasFocus && piece != i)
				{
					GameObject.Find ("Piece" + (i+1)).GetComponent<PuzzlePiece>().CanClick(false);
				}
				else if (!hasFocus)
				{
					GameObject.Find ("Piece" + (i+1)).GetComponent<PuzzlePiece>().CanClick(true);
					if (GameObject.Find ("Piece" + (i+1)).GetComponent<PuzzlePiece>().IsFocused())
					{
						piece = i;
						i = -1;
						hasFocus = true;
					}
				}
			}
		}
	}
	
	// This function should be called when displaying the JigSawPuzzle.
	void Initialise()
	{
		// Reset all the pieces to random positions.
		// Shuffle all the starting positions
		for(int t = 0; t < NumOfJigSawPuzzle; ++t)
        {
			// Copy the current starting position
            Vector2 tmp = StartingPositions[t];
			// Get a ramdomized array element.
            int r = Random.Range(t, NumOfJigSawPuzzle);
			// Assing the randomized position to position t.
            StartingPositions[t] = StartingPositions[r];
			// copies the position t to the randomized position.
            StartingPositions[r] = tmp;
        }
		for(uint i = 0; i < NumOfJigSawPuzzle; ++i)
		{
			GameObject.Find ("Piece" + (i+1)).transform.localPosition = StartingPositions[i];
			GameObject.Find ("Piece" + (i+1)).GetComponent<PuzzlePiece>().SetCorrectPos(CorrectPosition[i]);
		}
	}
	void CheckPuzzleSolved()
	{
		bSolved = true;
		for(uint i = 0; i < 9; ++i)
		{
			if(!GameObject.Find ("Piece" + (i+1)).GetComponent<PuzzlePiece>().m_bCorrectPosition)
			{
				bSolved = false;
			}
		}
		if(bSolved)
		{
			// Solve blink
			m_bAnimatePuzzleClear = true;
			m_bCheckPuzzle = false;
			//gameObject.guiTexture.texture = (Texture2D)Resources.Load ("Mini Game/5_jigsawbg2");
			gameObject.GetComponent<GUITexture>().texture = m_5jigsawbg2;
		}
	}
	private bool m_bAnimateRed = false;
	private float m_fDelays = 0.7f;
	private float m_fDelayElapsed = 0.0f;
	private int NumOfChanges = 5;
	void AnimatePuzzleClear()
	{
		m_fDelayElapsed += Time.deltaTime;
		if(m_fDelayElapsed >= m_fDelays)
		{
			if(m_bAnimateRed)
			{
				//gameObject.guiTexture.texture = (Texture2D)Resources.Load ("Mini Game/5_jigsawbg2");
				gameObject.GetComponent<GUITexture>().texture = m_5jigsawbg2;
			}
			else
			{
				//gameObject.guiTexture.texture = (Texture2D)Resources.Load ("Mini Game/5_jigsawbg");
				gameObject.GetComponent<GUITexture>().texture = m_5jigsawbg;
			}
			m_bAnimateRed = m_bAnimateRed ? false : true;
			m_fDelayElapsed = 0.0f;
			NumOfChanges -= 1;
			m_fDelays -=0.1f;
			if(NumOfChanges == 0)
			{
				m_bAnimatePuzzleClear = false;
				gameObject.SetActive(false);
			}
		}
	}
}
