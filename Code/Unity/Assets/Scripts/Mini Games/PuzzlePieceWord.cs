using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PuzzlePieceWord : MonoBehaviour {
	
	private Vector2 m_vCorrectPos;
	private Vector2 m_vStartPos;
	private bool m_bOnFocus = false;
	private bool m_bCanClick = true;
	public bool m_bCorrectPosition = false;
	public bool m_bMovement = true;
	// Use this for initialization
	void Start () {
		GetComponent<GUIText>().material.color = Color.black;
		//m_vStartPos = new Vector2(transform.localPosition.x, transform.localPosition.y);
	}
	
	// Update is called once per frame
	void Update () {
		if (m_bCanClick && Input.GetMouseButtonDown(0))
		{
			Vector3 mouse = Input.mousePosition;
			mouse = new Vector3(mouse.x / Screen.width, mouse.y / Screen.height, mouse.z);
			if (mouse.y > (transform.position.y - 0.07) && 
			    mouse.y < (transform.position.y + 0.07) &&
			    mouse.x > (transform.position.x - 0.04375) &&
			    mouse.x < (transform.position.x + 0.04375))
			{
				m_bOnFocus = true;
				m_bCorrectPosition = false;
				for(int i = 0; i < 9; ++i)
				{
					if (this != GameObject.Find("Piece" + (i+1)))
					{
						if (GameObject.Find ("Piece" + (i + 1)) != null) {
							if (GameObject.Find ("Piece" + (i + 1)).GetComponent<PuzzlePiece> () != null) {
								GameObject.Find ("Piece" + (i + 1)).GetComponent<PuzzlePiece> ().CanClick (false);
							}
						}
					}
				}
			}
		}
		
		if (m_bOnFocus)
		{
			if(!m_bMovement)
			{
				return;
			}
			m_bCorrectPosition = false;
			Vector3 mousepos = Input.mousePosition;
			mousepos.x /= Screen.width;
			mousepos.y /= Screen.height;
			this.transform.position = mousepos;
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			if(!m_bMovement)
			{
				return;
			}
			if(m_bOnFocus)
			{
				m_bOnFocus = false;
				// Check position against main jig saw puzzle.
				if(Vector2.Distance(m_vCorrectPos, this.transform.localPosition) < 0.05f)
				{
					transform.localPosition = m_vCorrectPos;
					m_bCorrectPosition = true;
				}
				else
				{
					transform.localPosition = m_vStartPos;
				}
			}
		}
	}
	
	//	void OnMouseDrag()
	//	{
	//		if(!m_bMovement)
	//		{
	//			return;
	//		}
	//		m_bOnFocus = true;
	//		m_bCorrectPosition = false;
	//		Vector3 mousepos = Input.mousePosition;
	//		mousepos.x /= Screen.width;
	//		mousepos.y /= Screen.height;
	//		this.transform.position = mousepos;
	//	}
	
	//	public void OnMouseUp()
	//	{		
	//		if(!m_bMovement)
	//		{
	//			return;
	//		}
	//		if(m_bOnFocus)
	//		{
	//			m_bOnFocus = false;
	//			// Check position against main jig saw puzzle.
	//			if(Vector2.Distance(m_vCorrectPos, this.transform.localPosition) < 0.05f)
	//			{
	//				transform.localPosition = m_vCorrectPos;
	//				m_bCorrectPosition = true;
	//			}
	//			else
	//			{
	//				transform.localPosition = m_vStartPos;
	//			}
	//		}
	//	}
	
	public void SetCorrectPos(Vector2 CorrectPos)
	{
		m_vCorrectPos = CorrectPos;
	}
	
	public void SetStartPos(Vector2 StartPos)
	{
		m_vStartPos = StartPos;
	}
	
	public bool IsFocused()
	{
		return (m_bOnFocus);
	}
	
	public void CanClick(bool click)
	{
		m_bCanClick = click;
	}
}
