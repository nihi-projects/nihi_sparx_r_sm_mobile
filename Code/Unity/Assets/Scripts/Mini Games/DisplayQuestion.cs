using UnityEngine;
using System.Collections;

public class DisplayQuestion : MonoBehaviour {
	public bool bCorrectAnswer = false;
	public bool bFinishedAnswering = false;
	public GUISkin GUIskin;
	bool bActivated = false;
	
	public string[] Options;
	public string CorrectOption;
	public string Question;
	public GameObject CorrespondingSparx;
	public float fStartVoice, fEndVoice;
	public string VoiceFile;
	public AudioClip L4GnatsAudioClip; 
	AudioSource m_Voice;
	public float fHeight = 20.0f;
	public string Name;
	float countdown = 1.0f;
	// Use this for initialization
	void Start () {
		Question.Replace("\\n", "\n");
		for(int i = 0; i < Options.Length; ++i)
		{
			Options[i].Replace("\\n", "\n");
		}

		FontSizeManager.checkFontSize (GUIskin);

//		if (GUIskin == null)
//		{
//			GUIskin = (GUISkin)Resources.Load("Skins/SparxSkin");
//		}
	}
	
	// Update is called once per frame
	void Update () {	
		if(bActivated)
		{
			if(m_Voice.time >= fEndVoice)
			{
				m_Voice.Stop ();
			}
		}
		else
		{
			if(!bCorrectAnswer)
			{
				countdown -= Time.deltaTime;
				if(countdown <= 0.0f)
				{
					Activate();
				}
			}
		}
	}
	public void Activate()
	{
		bActivated = true;
		bFinishedAnswering = false;
		
		m_Voice = GameObject.Find (Name).GetComponent<AudioSource>();
		//m_Voice.clip = Resources.Load("AudioSounds/Voice/" + VoiceFile) as AudioClip;
		m_Voice.clip = L4GnatsAudioClip;
		m_Voice.time = fStartVoice;
		m_Voice.volume = 1;
		m_Voice.dopplerLevel = 1;
		m_Voice.minDistance = 100;
		m_Voice.Play();
	}
	public void Reset()
	{
		bActivated = false;
		bFinishedAnswering = false;
	}
	
	void OnGUI()
	{
		if(bFinishedAnswering || !bActivated)
		{
			return;
		}
		
		GUI.skin = GUIskin;

        MultiChoiceBox multiBox = TextDisplayCanvas.instance.multiChoiceBox;
        multiBox.gameObject.SetActive(true);
        multiBox.overrideSelfHide = true;

        multiBox.SetStatementData("Gnat", Question, new Color(0.44f, 0.44f, 0.44f, 1.0f));
        for( int i = 0; i < Options.Length; ++i )
        {
            multiBox.SetChoice((EMultiChoice)(i), Options[i]);
        }

        if (multiBox.IsAnyOptionSelected())
        {
            multiBox.ShowNextButton(true);

            if (multiBox.WasNextClicked() || Global.arrowKey == "right" )
            {
                Global.arrowKey = "";
                if ( Options[multiBox.SelectedOption()] == CorrectOption )
                {
                    bCorrectAnswer = true;
                }

                bFinishedAnswering = true;
                m_Voice.Stop();
                if (GameObject.Find("TalkPaper") != null)
                {
                    GameObject.Find("TalkPaper").GetComponent<GUITexture>().enabled = false;
                }
                if (GameObject.Find("TalkPaperSmall") != null)
                {
                    GameObject.Find("TalkPaperSmall").GetComponent<GUITexture>().enabled = false;
                }
                bActivated = false;
                countdown = 1.0f;
                m_Voice.Stop();
                multiBox.gameObject.SetActive(false);
            }
        }
	}
}
