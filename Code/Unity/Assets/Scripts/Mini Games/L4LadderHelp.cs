﻿using UnityEngine;
using System.Collections;

public class L4LadderHelp : MonoBehaviour {
	public GameObject oLadderLeft;
	public GameObject oLadderRight;
	private bool bActivated;
	public GUISkin GUIskin;
	public string Text;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (GameObject.Find ("CubeHelpLeft").GetComponent<L4LadderHelpTrigerLeft> ().m_leftLadderTriggerEntered) {
			bActivated = true;	
		} else {
			bActivated = false;	
		}

		if (GameObject.Find ("CubeHelpRight").GetComponent<L4LadderHelpTrigerRight> ().m_rightLadderTriggerEntered) {
			bActivated = true;	
		} else {
			if (!bActivated) {
				bActivated = false;	
			}
		}

	}

	void OnGUI()
	{
		if(!bActivated)
		{
			return;
		}
		//GUI.skin = GUIskin;
		//GUIStyle temp = GUI.skin.GetStyle("Label");
		//GUI.Label(new Rect(200f * Global.ScreenWidth_Factor,537f * Global.ScreenHeight_Factor,620f * Global.ScreenWidth_Factor,153f * Global.ScreenHeight_Factor), "", "NormalSizeDilogBox");
		//temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		//GUI.Label(new Rect(306f * Global.ScreenWidth_Factor,583f * Global.ScreenHeight_Factor,725 * Global.ScreenWidth_Factor,100 * Global.ScreenHeight_Factor), Text, "Label");
		//float y = Screen.height*0.84f;
        TextDisplayCanvas.instance.ShowTooltip(Text);
    }

}
