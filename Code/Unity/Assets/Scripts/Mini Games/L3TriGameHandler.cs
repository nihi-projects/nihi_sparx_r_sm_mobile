using UnityEngine;
using System.Collections;

public class L3TriGameHandler : MonoBehaviour 
{
	// publics
	public GameObject[] m_Games = null;
	public GameObject[] m_Birds = null;
	public Transform[] m_TriggerPos = null;
	public Transform[] m_MoveToPos = null;
	public Transform[] m_LookAts = null;
	
	public float m_fGameStartDistance = 10.0f;
	
	// privates
	GameObject m_player = null;
	
	Vector3 startPos = Vector3.zero;
	
	int m_iCurrentGame = 0;
	
	bool[] m_bIsGameDone;
	bool m_bMovingTo = false;
	bool m_bGameDone = false;
	bool m_bWaiting = false;
	
	// Use this for initialization
	void Start () 
	{
		if (m_Games.Length > 0)
		{
			// create the arry if the games are there.
			m_bIsGameDone = new bool[3];
			m_Games[0].SetActive(false);
			m_Games[1].SetActive(false);
			m_Games[2].SetActive(false);
		}
		
		// get the player
		m_player = Global.GetPlayer();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_iCurrentGame == m_Games.Length)
		{
			
			return ;
		}
		
		if (!m_bWaiting && CheckGameTriggered() && !m_Games[m_iCurrentGame].activeInHierarchy)
		{			
			if (m_bMovingTo == false)
			{
				m_player.GetComponent<PlayerMovement>().MoveToPosition(m_MoveToPos[m_iCurrentGame].position, m_LookAts[m_iCurrentGame].position);
				m_bMovingTo = true;
			}
			
			if (!m_player.GetComponent<PlayerMovement>().m_bMovingToPosition)
			{
				m_Games[m_iCurrentGame].SetActive(true);
				m_player.GetComponent<PlayerMovement>().SetMovement(false, "L3TriGameHandler");
			}
		}
		else if (m_Games[m_iCurrentGame].activeInHierarchy)
		{
			if (m_iCurrentGame == 0)
			{
				if (m_Games[m_iCurrentGame].GetComponent<ThreeWordGame>().IsDone())
				{
					m_bGameDone = true;
				}
				
			}
			else if (m_iCurrentGame == 1)
			{
				if (m_Games[m_iCurrentGame].GetComponent<JigSawPuzzle>().bSolved)
				{
					m_bGameDone = true;
				}
			}
			else
			{
				if (m_Games[m_iCurrentGame].GetComponent<WordSearchPuzzle>().allclear)
				{
					m_bGameDone = true;
					
				}
			}
			
			if (m_bGameDone == true)
			{
				GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
				m_player.GetComponent<PlayerMovement>().SetMovement(true, "L3TriGameHandler2");
				m_bGameDone = false;
				m_bWaiting = true;
			}
		}
		
		if (m_bWaiting == true && !m_Games[m_iCurrentGame].activeInHierarchy)
		{
			if (Global.CurrentLevelNumber == 3)
			{
				if(m_iCurrentGame < 2){
					m_Birds[m_iCurrentGame].GetComponent<FireSpritFlyAway>().m_bMovingToTarget = true;
				}
				//Conversation with the firespritD
				else{
					Global.CurrentInteractNPC = "FirespiritA";
					GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
					GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3FirepspiritScene2");
				}
			}
			
			++m_iCurrentGame;
			m_bWaiting = false;
			m_bMovingTo = false;
		}
	}
	
	bool CheckGameTriggered()
	{	try{
			if (Vector3.Distance(m_player.transform.position, m_TriggerPos[m_iCurrentGame].position) < m_fGameStartDistance)
			{
				return (true);
			}
		} catch{
			Debug.Log("player exists:"+( m_player != null).ToString() + ", triggerThing: "+(m_TriggerPos[m_iCurrentGame] != null).ToString());
			if(m_player == null){
				m_player = Global.GetPlayer();
				Debug.Log("Player gotted");
			}
		}
		
		return (false);
	}
}
