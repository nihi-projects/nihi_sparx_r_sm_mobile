using UnityEngine;
using System.Collections;

enum EndPartStatus {SparksExit, SparksBreakRock, GemAppear, Default};

public class L4StepsMiniGame : MonoBehaviour 
{
	// Publics
	public GameObject m_Gem = null;
	public GameObject[] m_Sparks = null;
	
	public Transform[] m_SparksWaitPos = null;
	Transform[] m_HoverPos = null;
	public Transform m_PlayerMoveTo = null;
	public Transform m_SparkMoveTo = null;
	
	
	// Privates
	GameObject player = null;
	
	Vector3 m_vSparksMoveFrom = Vector3.zero;
	
	EndPartStatus m_EndStatus = EndPartStatus.SparksExit;
	
	float m_fTime = 0.0f;
	
	int m_iCurrentQuestion = 0;
	int m_iDisplayedQuestion = 1;
	
	public bool m_bInteracted = false;
	
	bool m_bMovingPlayer = false;
	bool m_bTalkingToSpark = false;
	bool m_bGameActive = false;
	bool m_bDoneTalking = false;
	bool m_bEndScene = false;
	bool m_bSparkMovingToGem = false;
	
	private bool m_bTuiFlyingDown = false;
	
	void Start()
	{
		player = Global.GetPlayer();
		m_HoverPos = new Transform[4];
		m_HoverPos[0] = GameObject.Find("STexture").transform;
		m_HoverPos[1] = GameObject.Find("TTexture").transform;
		m_HoverPos[2] = GameObject.Find("PTexture").transform;
		m_HoverPos[3] = GameObject.Find("LastSTexture").transform;
		
	}
	
	void Update()
	{
		if (m_bEndScene)
		{
			RunEndPart();
			
			return ;
		}
		
		if (!m_bGameActive && CheckInteraction())
		{
			m_fTime += Time.deltaTime;
			for (int i = 0; i < m_Sparks.Length; ++i)
			{
				m_Sparks[i].transform.position = Vector3.Lerp(m_Sparks[i].transform.position, m_SparksWaitPos[i].position, m_fTime);
			}
			if (m_fTime >= 1.0f)
			{
				m_bGameActive = true;
				GameObject.Find("STexture").GetComponent<MeshRenderer>().enabled = true;
			}
		}
		
		if (m_bGameActive)
		{
			if (m_bMovingPlayer) // moving to the gem
			{	
				Vector3 moveToPos = m_PlayerMoveTo.position;
				if (Vector3.Distance(player.transform.position, moveToPos) < 0.5f)
				{
					m_fTime += Time.deltaTime;
					if (m_fTime > 0.5f)
					{
						// playe rhas reached the stone again, start talking to spark
						player.GetComponent<PlayerMovement>().SetMovement(false, "L4StepsMiniGame");
						m_fTime = 0.0f;
						m_bMovingPlayer = false;
						m_bTalkingToSpark = true;
						player.GetComponent<Animation>().Play("take");
						Global.MoveCameraBasedOnID("24003");
						m_fTime = 0.0f;
					}
				}
				else
				{
					// move player
					player.GetComponent<PlayerMovement>().MoveToPosition(moveToPos,
								  									 	 m_Gem.transform.position,
																	 	 m_Gem.name);
				}
			}
			else if (m_bTalkingToSpark) // talking to spark
			{
				// insert code here to talk to spark
				if (player.GetComponent<Animation>().IsPlaying("take"))
				{
					m_fTime += Time.deltaTime;
					
					m_Sparks[m_iCurrentQuestion].SetActive(true);
					if (m_fTime < 0.9f)
					{
						string name = player.name == "Boy" ? "boy" : "girl";
						m_Sparks[m_iCurrentQuestion].transform.position = GameObject.Find(name + " L Hand").transform.position;
					}
				}
				if (!m_bDoneTalking)
				{
					m_bDoneTalking = true;
					SwitchSceneName();
					Global.CurrentInteractNPC = m_Sparks[m_iCurrentQuestion].name;
					GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				}
				
				// if the player can move again then the conversation is over
				if (player.GetComponent<PlayerMovement>().GetMovement() )
				{
					m_fTime += Time.deltaTime * 0.5f;
					m_Sparks[m_iCurrentQuestion].transform.position = Vector3.Lerp(m_vSparksMoveFrom, m_SparkMoveTo.position, m_fTime);
					
					if (m_fTime >= 1.0f)
					{
						// display the next letter
						m_bTalkingToSpark = false;
						m_bSparkMovingToGem = true;
						ShowLetter();
						m_Sparks[m_iCurrentQuestion].SetActive(true);
						m_Sparks[m_iCurrentQuestion].GetComponent<L4Spark>().m_bCollected = true;
						GameObject.Find ("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
						m_fTime = 0.0f;
					}
				}
			}
			else if (m_bSparkMovingToGem)
			{
				
				m_fTime += Time.deltaTime;
				
				m_Sparks[m_iCurrentQuestion].transform.position = Vector3.Lerp(m_Sparks[m_iCurrentQuestion].transform.position, m_Gem.transform.position, m_fTime);
				
				if (m_fTime >= 1.0f)
				{
					m_bSparkMovingToGem = false;
					m_Sparks[m_iCurrentQuestion].SetActive(false);
				}
			}
			else // defualt state
			{
				bool bAllCollected = true;
				for (int i = 0; i < m_Sparks.Length; ++i)
				{
					if (m_Sparks[i].GetComponent<L4Spark>().m_bGrabbed && 
						!m_Sparks[i].GetComponent<L4Spark>().m_bCollected)
					{
						if (m_bMovingPlayer == false)
						{
							m_bMovingPlayer = true;
							player.GetComponent<PlayerMovement>().SetMovement( true, "L4StepsMiniGame2");
							m_Sparks[i].SetActive(false);
							m_iCurrentQuestion = i;
							m_bDoneTalking = false;
							bAllCollected = false;
							break;
						}
					}
					
					if (!m_Sparks[i].GetComponent<L4Spark>().m_bCollected)
					{
						bAllCollected = false;
					}
				}
				
				if (bAllCollected)
				{
					GameObject.Find("STexture").GetComponent<MeshRenderer>().enabled = false;
					GameObject.Find("TTexture").GetComponent<MeshRenderer>().enabled = false;
					GameObject.Find("ETexture").GetComponent<MeshRenderer>().enabled = false;
					GameObject.Find("PTexture").GetComponent<MeshRenderer>().enabled = false;
					GameObject.Find("LastSTexture").GetComponent<MeshRenderer>().enabled = false;	
					m_bEndScene = true;
					Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L4StepsMiniGame3");
					Global.MoveCameraBasedOnID("24003");
					m_fTime = 0.0f;
				}
			}
		}
		
		//If Tui fly down finished
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiFlyingDown){
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L4HopeScene9";
			m_bTuiFlyingDown = false;
		}
	}
	
	void RunEndPart()
	{
		switch (m_EndStatus)
		{
		case EndPartStatus.SparksExit:
		{
			// move all the sparks
			m_fTime += Time.deltaTime * 0.4f;
			for (int i = 0; i < m_Sparks.Length; ++i)
			{
				m_Sparks[i].SetActive(true);
				m_Sparks[i].transform.position = Vector3.Lerp(m_Sparks[i].transform.position, m_HoverPos[i].position, m_fTime);
			}
			
			if(m_fTime >= 1.5f)
			{
				m_EndStatus = EndPartStatus.SparksBreakRock;	
				m_fTime = 0.0f;
			}
		}
			break;
			
		case EndPartStatus.SparksBreakRock:
		{
			// move all the sparks back in
			m_fTime += Time.deltaTime * 0.4f;
			if (m_fTime < 1.0f)
			{
				for (int i = 0; i < m_Sparks.Length; ++i)
				{
					m_Sparks[i].transform.position = Vector3.Lerp(m_Sparks[i].transform.position, m_Gem.transform.position, m_fTime);
				}
			}
			else
			{
				for (int i = 0; i < m_Sparks.Length; ++i)
				{
					m_Sparks[i].SetActive(false);
				}
			}
			if(m_fTime >= 1.0f)
			{
				m_EndStatus = EndPartStatus.GemAppear;	
			}
		}
			break;
			
		case EndPartStatus.GemAppear:
		{
			//Switch the camera to look at the Gem1
			Global.MoveCameraBasedOnID("24003");
			
			// show the gem
			m_Gem.GetComponent<ObjectMovement>().enabled = true;
			m_Gem.GetComponent<GetGem>().enabled = true;
			
			//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
			m_EndStatus = EndPartStatus.Default;
		}
			break;
			
		};
	}
	
	void ShowLetter()
	{
		if (m_iDisplayedQuestion == 1)
		{
			GameObject.Find("TTexture").GetComponent<MeshRenderer>().enabled = true;
		}
		else if (m_iDisplayedQuestion == 2)
		{
			GameObject.Find("ETexture").GetComponent<MeshRenderer>().enabled = true;
		}
		else if (m_iDisplayedQuestion == 3)
		{
			GameObject.Find("PTexture").GetComponent<MeshRenderer>().enabled = true;
		}
		else
		{
			GameObject.Find("LastSTexture").GetComponent<MeshRenderer>().enabled = true;
		}
		
		++m_iDisplayedQuestion;
	}
	
	bool CheckInteraction()
	{
		Vector3 moveToPos = transform.position - (transform.right * 2.0f);
		try{
			if (Vector3.Distance(player.transform.position, moveToPos) < 3.0f)
			{
				if (Input.GetMouseButtonDown(0))
				{
					if (Global.GetMouseToObject() && Global.GetMouseToObject().name == "Gem4")
					{
						//Tui fly down here and talk
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("34035");
						GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
						Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L4StepsMiniGame4");
						Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
						
						m_bTuiFlyingDown = true;
					}
				}
			}
		} catch {
			if(player == null){
				player = Global.GetPlayer();
			}
		}
		return (m_bInteracted);
	}
	
	void SwitchSceneName()
	{
		string Scene = "L4SparkScene" + m_iDisplayedQuestion.ToString();
		
		GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue(Scene);
	}
	
//	public bool m_bSparkInteracted = false;
//	public float m_bSparkInteractionRadius = 3.0f;
//	public bool m_bSparkIsInteracting = false;
//	
//	private Vector3 m_vPlayerStandPosition;
//	private bool m_bGotSpark = false;
//	private bool m_bWalkToPowerGem = false;
//	private bool m_bPlayerReleasedSpark = false;
//	
//	// Use this for initialization
//	void Start () {
//		//Global.GetPlayer().transform.position.z = gameObject.transform.position.z + 0.5f;
//		//m_vPlayerStandPosition = Global.GetPlayer().transform.position;
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		if(m_bSparkInteracted){
//			return;
//		}
//		
//		float fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
//		if(fDistance <= m_bSparkInteractionRadius && m_bWalkToPowerGem == false){
//			if(!m_bSparkIsInteracting){
//				if(Input.GetKey(KeyCode.Space))
//				{
//					m_bSparkIsInteracting = true;
//					Global.GetPlayer().GetComponent<PlayerMovement>().
//						MoveToPosition(transform.position + (transform.forward * fDistance),
//						  									 transform.position, gameObject.name);
//				}
//			}
//			
//			//Player stand in front of Spark
//			if(m_bSparkIsInteracting && !m_bGotSpark){
//				//Player interacting with Spark
//				if(Global.GetPlayer ().animation.IsPlaying("idle"))
//				{	
//					Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
//					//Player picks up the spark
//					Global.GetPlayer().animation.Play("take");
//					m_bGotSpark = true;
//				}
//			}
//		}
//		
//		//Finish taking spark
//		if(m_bGotSpark && !Global.GetPlayer().animation.IsPlaying("take") && m_bPlayerReleasedSpark == false){
//			Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = true;
//			//Spark disappear
//			transform.position = Global.GetObjectPositionByID("34002");
//			
//			//Walk to the power gem
//			Global.GetPlayer().animation.Play("walk");
//			Vector3 standGemPoint = Global.GetObjectPositionByID("34004");
//			
//			Global.GetPlayer().transform.LookAt(Global.GetObjectPositionByID("34004"));
//			Global.GetPlayer().GetComponent<PlayerMovement>().
//					MoveToPosition(standGemPoint, Global.GetObjectPositionByID("34004"));
//			
//			m_bWalkToPowerGem = true;
//		}
//		
//		//Player release the spark
//		if(m_bWalkToPowerGem && Vector3.Distance(Global.GetPlayer().transform.position, GameObject.Find ("Gem4").transform.position) <= 2.0){
//			if(m_bPlayerReleasedSpark == false){
//				//The player wave hand and release the spark
//				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false);
//				Global.MoveCameraBasedOnID("24003");
//				Global.GetPlayer().GetComponent<Animation>().Play("take");
//				
//				m_bPlayerReleasedSpark = true;
//			}
//			
//			if(m_bPlayerReleasedSpark && !Global.GetPlayer().animation.IsPlaying("take")){
//				//Spark appear position
//				Vector3 SparkAppearPos = Global.GetObjectPositionByID("34004");
//				transform.position = SparkAppearPos;
//				
//				SwitchSceneName();
//				//Enable tale scene
//				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
//				this.enabled = false;
//			}
//		}
//	}
//	
//	void SwitchSceneName()
//	{
//		if(gameObject.name == "Spark1"){
//			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentScene"L4SparkScene1";
//		}
//		else if(gameObject.name == "Spark2"){
//			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentScene"L4SparkScene2";
//		}
//		else if(gameObject.name == "Spark3"){
//			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentScene"L4SparkScene3";
//		}
//		else if(gameObject.name == "Spark4"){
//			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentScene"L4SparkScene4";
//		}
//	}
	
}
