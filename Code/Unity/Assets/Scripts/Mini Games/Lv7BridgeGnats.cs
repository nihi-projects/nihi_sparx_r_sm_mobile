using UnityEngine;
using System.Collections;

public class Lv7BridgeGnats : MonoBehaviour {
	
	bool StartGame = false;
	float Timer = 20.0f;
	bool StartAttack = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.Find ("Cube").GetComponent<Collider>().bounds.
		   Contains(Global.GetPlayer().transform.position) &&
			!StartGame && !StartAttack)
		{
			//Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(transform.Find ("Cube").position,
								//transform.Find ("Cube").position);
			StartGame = true;
			foreach(Transform gnat in transform)
			{
				if(gnat.name == "Gnat")
				{
					gnat.GetComponent<ObjectMovement>().m_bStartMoving = true;
				}
			}
		}
		if(StartGame)
		{
			if(Vector3.Distance(this.transform.Find("Cube").position, 
				Global.GetPlayer().transform.position) <= 2.0f)
			{
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "Lv7BridgeGnats");
				Global.GetPlayer ().GetComponent<Animation>().Play ("aim idle");
				Global.GetPlayer ().transform.LookAt(transform.Find ("Lookat"));
				bool MoveComplete = true;
				foreach(Transform gnats in transform)
				{
					if(gnats.name == "Gnat")
					{
						if(gnats.GetComponent<ObjectMovement>().m_bMovementComplete)
						{
							gnats.GetComponent<Lv7BridgeGnatAction>().enabled = true;
						}
						else
						{
							MoveComplete = false;
						}
					}
				}
				if(MoveComplete)
				{
					StartAttack = true;
					StartGame = false;
				}
			}
		}
		if(StartAttack)
		{
			if(Input.GetMouseButton(0))
			{
				Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				RaycastHit[] hits;
				
				hits = Physics.RaycastAll(ray);
				
				foreach(RaycastHit hit in hits)
				{
					if(hit.collider.gameObject.name == "Gnat")
					{
						Lv7BridgeGnatStatus act = hit.collider.gameObject.GetComponent<Lv7BridgeGnatAction>().action;
						if(act == Lv7BridgeGnatStatus.Attacking)
						{
							Global.GetPlayerEffect().SetFireBallMoveTo(hit.collider.gameObject);
							SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
							Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
							
						}
					}
				}
			}
			if(Global.GetPlayerEffect().FireBallHitObject())
			{
				Global.GetPlayerEffect().GetTarget().GetComponent<Lv7BridgeGnatAction>().action = 
								Lv7BridgeGnatStatus.Defeated_BackToOriginalPos;
			}
			Timer -= Time.deltaTime;
			if(Timer <= 0.0f)
			{
				//Pause the Gnats from attacking the player
				foreach(Transform gnats in transform){
					if(gnats.name == "Gnat")
					{
						gnats.GetComponent<Lv7BridgeGnatAction>().action = Lv7BridgeGnatStatus.Pause;
					}
				}
				//Tui fly down and talk to the player
				Global.GetPlayer().GetComponent<PlayerMovement>().m_bTuiMovementAttach = false;
				GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37012");
				//Enable the Tui conversation
				Global.CurrentInteractNPC = "Hope";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7HopeScene1";
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_voiceCanStart = true;
				
				//Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovement = true;
				Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
				//End the Gnats from attacking the player
				foreach(Transform gnats in transform){
					if(gnats.name == "Gnat")
					{
						gnats.GetComponent<Lv7BridgeGnatAction>().action = Lv7BridgeGnatStatus.End;
					}
				}
				
				Global.GetPlayerEffect().Reset();
				this.enabled = false;
			}
		}
	}
	
	public void RestartMiniGame()
	{
		foreach(Transform gnats in transform)
		{
			if(gnats.name == "Gnat")
			{
				gnats.GetComponent<Lv7BridgeGnatAction>().Reset();
			}
		}
	}
	
}
