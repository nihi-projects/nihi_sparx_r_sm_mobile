using UnityEngine;
using System.Collections;

public enum GnatsTalkStatus
{
	Default,
	PickGnat,
	GnatFlyDown,
	AnsweringQuestions,
	GnatFlyBack,
	GnatChangeToSparx,
	AfterDialog,
	MoverFlyAway,
};

public class L4GnatsTalk : MonoBehaviour 
{
	// publics 
	public GameObject[] m_Gnats;
	public GameObject m_Spark;
	public GameObject m_Effect;
	
	public Transform m_FlyAwayPos;
	public Transform m_QuestionPos;
	
	public string CameraID;
	
	// privates
	GameObject m_CurMover = null;
	GameObject m_TempSpark = null;
	GnatsTalkStatus m_status = GnatsTalkStatus.Default;
	
	Vector3 m_vStartPos = Vector3.zero;
	
	float m_fMoveTime = 0.0f;
	float m_fStatusTime = 0.0f;
	float m_fTalkEnd = 0.0f;
	
	int m_iCurrentGnat = 0;
	
	bool m_bTriggered = false;
	
	// Use this for initialization
	void Start ()  
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		bool GnatsLeft = false;
		for(int i =0 ;i < m_Gnats.Length; ++i)
		{
			bool move = (m_bTriggered && m_fStatusTime > 1.5f) ? (i != m_iCurrentGnat) : true;
			if(move && m_Gnats[i] != null)
			{
				m_Gnats[i].transform.RotateAround(transform.position, 
												Vector3.up,
												3.0f);
				GnatsLeft = true;
			}
		}
		
		if (m_bTriggered)
		{
			m_fStatusTime += Time.deltaTime;
			if (m_fStatusTime > 1.5f)
			{
				switch (m_status)
				{
				case GnatsTalkStatus.PickGnat:
				{
					PickAGnat();
				}
					break;
				case GnatsTalkStatus.GnatFlyDown:
				{
					GnatFlyDown();
				}
					break;
				case GnatsTalkStatus.AnsweringQuestions:
				{
					QuestionAndAnswer();
				}
					break;
				case GnatsTalkStatus.GnatFlyBack:
				{
					GnatFlyBack();
				}
					break;
				case GnatsTalkStatus.GnatChangeToSparx:
				{
					ChangeToSpark();
				}
					break;
				case GnatsTalkStatus.AfterDialog:
				{
					ExecutiongAfterDialog();
				}
					break;
				case GnatsTalkStatus.MoverFlyAway:
				{
					if (m_CurMover)
					{
						MoverFlysAway(m_CurMover, m_vStartPos);
					}
					if (m_TempSpark)
					{
						MoverFlysAway(m_TempSpark, m_FlyAwayPos.position);
					}
				}
					break;
				};
			}
		}
		else 
		{
			if (!GnatsLeft)
			{
				m_bTriggered = false;
			}
			else
			{
				CheckTrigger();
			}
		}
		
		
	}
	
	void PickAGnat()
	{
		if (m_iCurrentGnat >= 4)
		{
			bool bStillGnatsLeft = false;
			for(int i =0 ;i < m_Gnats.Length; ++i)
			{
				if (m_Gnats[i] != null)
				{
					m_iCurrentGnat = i;
					bStillGnatsLeft = true;
					break;
				}
			}
			
			if (!bStillGnatsLeft)
			{
				m_status = GnatsTalkStatus.Default;
				// stops movement.
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "L4GnatsTalk");
				// set the camera.
				GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
				return ;
			}
		}
		m_CurMover = m_Gnats[m_iCurrentGnat];
		m_vStartPos = m_CurMover.transform.position;
		m_fStatusTime = 0.0f;
		m_status = GnatsTalkStatus.GnatFlyDown;
	}
	
	void GnatFlyDown()
	{
		m_fMoveTime += Time.deltaTime;
		
		m_CurMover.transform.position = Vector3.Lerp(m_vStartPos, m_QuestionPos.position, m_fMoveTime);
		
		if (m_fMoveTime >= 1.0f)
		{
			m_status = GnatsTalkStatus.AnsweringQuestions;
			m_CurMover.GetComponent<DisplayQuestion>().Activate();
			m_fMoveTime = 0.0f;
		}
	}
	
	void QuestionAndAnswer()
	{
		DisplayQuestion question = m_CurMover.GetComponent<DisplayQuestion>();
		question.enabled = true;
		if (question.bFinishedAnswering)
		{
			// if answeris wrong then make gnat fly back 
			if (!question.bCorrectAnswer)
			{
				m_status = GnatsTalkStatus.GnatFlyBack;
				question.enabled = false;
				m_fStatusTime = 1.0f;
			}
			// if answer is right then change to spark
			else
			{
				m_status = GnatsTalkStatus.GnatChangeToSparx;
				m_fStatusTime = 1.5f;
			}
		}
	}
	
	void GnatFlyBack()
	{
		if (m_fMoveTime == 0.0f)
		{
			int iRand = Random.Range(0, 2);
			
			if (iRand == 0)
			{
				GetComponent<AudioSource>().time = 16.0f;
				m_fTalkEnd = 18.0f;
			}
			else
			{
				GetComponent<AudioSource>().time = 24.0f;
				m_fTalkEnd = 28.0f;
			}
			GetComponent<AudioSource>().Play();
		}
		
		m_fMoveTime += Time.deltaTime * 0.5f;
		
		m_CurMover.transform.position = Vector3.Lerp(m_QuestionPos.position, m_vStartPos, m_fMoveTime);
		
		if (m_fMoveTime >= 1.0f)
		{
			m_status = GnatsTalkStatus.AfterDialog;
			m_fStatusTime = 0.0f;
			m_fMoveTime = 0.0f;
		}
	}
	
	void ChangeToSpark()
	{
		// create spark and set to tempSpark
		m_TempSpark = (GameObject)Instantiate(m_Spark, m_CurMover.transform.position, Quaternion.identity);
		Instantiate(m_Effect, m_CurMover.transform.position, Quaternion.identity);
		
		SoundPlayer.PlaySound("sfx-pop", 1.0f);
		
		// destroy the gnat, its no longer needed
		Destroy(m_CurMover);
		
		m_status = GnatsTalkStatus.MoverFlyAway;
		m_fStatusTime = 0.0f;
	}
	
	void ExecutiongAfterDialog()
	{
		if (GetComponent<AudioSource>().time >= m_fTalkEnd)
		{
			GetComponent<AudioSource>().Stop();
			m_status = GnatsTalkStatus.PickGnat;
			m_fStatusTime = 0.5f;
			++m_iCurrentGnat;
			while (m_iCurrentGnat >= m_Gnats.Length || m_Gnats[m_iCurrentGnat] == null)
			{
				++m_iCurrentGnat;
				if (m_iCurrentGnat >= m_Gnats.Length)
				{
					break;
				}
			}
		}
	}
	
	void MoverFlysAway(GameObject Go, Vector3 target)
	{
		m_fMoveTime += Time.deltaTime * 0.5f;
		
		Go.transform.position = Vector3.Lerp(m_QuestionPos.position, target, m_fMoveTime);
		
		if (m_fMoveTime >= 1.0f)
		{
			while (m_Gnats[m_iCurrentGnat] == null)
			{
				++m_iCurrentGnat;
				if (m_iCurrentGnat >= m_Gnats.Length)
				{
					break;
				}
			}
			Destroy(m_TempSpark);
			m_status = GnatsTalkStatus.PickGnat;
			m_fStatusTime = 0.0f;
			m_fMoveTime = 0.0f;
		}
	}
	
	void CheckTrigger()
	{
		if(Vector3.Distance(Global.GetPlayer().transform.position,
							transform.position) < 5.5f)
		{
			GameObject player = Global.GetPlayer();
			player.transform.LookAt(new Vector3(transform.position.x, player.transform.position.y, transform.position.z));
			m_bTriggered = true;
			m_QuestionPos.position = player.transform.position + 
						   (player.transform.forward * 2.0f) + 
						   (player.transform.up);
			m_status = GnatsTalkStatus.PickGnat;
			
			// stops movement.
			player.GetComponent<PlayerMovement>().SetMovement(false, "L4GnatsTalk2");
			GameObject camera = Global.GetCameraObj(CameraID, false);
			camera.transform.position = player.transform.position + (player.transform.forward * -1.0f) + (player.transform.up * 1.3f);
			// set the camera.
			Global.MoveCameraBasedOnID(CameraID);
			
			// player animation.
			player.GetComponent<Animation>().Play("aim idle");
		}
	}
}
