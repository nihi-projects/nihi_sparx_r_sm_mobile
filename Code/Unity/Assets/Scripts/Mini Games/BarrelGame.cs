using UnityEngine;
using System.Collections;

public enum BarrelStatus
{
	GnatFlyToPosition,
	GnatInConversation,
	GnatFlyingAway,
	GnatStandby,
	SparkFlyToPosition,
	SparkInConversation,
	SparkFlyingAway
}

public class BarrelGame : MonoBehaviour {

    public OutlineObject[] outlines;
    public GameObject[] Gnats;
    public GameObject[] Sparx;
    BarrelStatus Status;
    int iCurrentPair = 0;
    public string BarrelName = "[mini]3_barrel";

    public bool m_bEnd = false;
    public bool m_bCanPlay = false;
    bool CorrectBarrel = false;

    public Transform LeftPos, RightPos, CenterPos, FlyFrom;
    public bool m_bGameStart = false;

    public GameObject[] Platforms;

    GameObject CurrentObject = null;
    GameObject explosionEffect;

    private bool m_bTuiConversationStarts = false;
    public string PlayerLookat = "25002";

    Vector3 start = Vector3.zero;
    float m_fTime = 0.0f;

    // Use this for initialization
    void Start()
    {
        CurrentObject = Gnats[0];

        if (Global.CurrentLevelNumber == 7)
        {
            m_bCanPlay = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Tui is flying down

        if (Global.GetCurrentLevelID() == "Level5")
        {
            if (!GameObject.Find("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiConversationStarts)
            {
                GameObject.Find("Hope").transform.LookAt(Global.GetPlayer().transform.position);
                Global.CurrentInteractNPC = "Hope";
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            }
        }

        if (!m_bGameStart)
        {
            return;
        }

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "BarrelGame");

        switch (Status)
        {
            case BarrelStatus.GnatFlyToPosition:
                GnatFlyTo();
                break;
            case BarrelStatus.GnatInConversation:
                GnatConversation();
                break;
            case BarrelStatus.GnatStandby:
                GnatStandby();
                break;
            case BarrelStatus.GnatFlyingAway:
                GnatFlyingAway();
                break;
            case BarrelStatus.SparkFlyToPosition:
                SparkFlyToPos();
                break;
            case BarrelStatus.SparkInConversation:
                SparkInConversation();
                break;
            case BarrelStatus.SparkFlyingAway:
                SparkFlyingAway();
                break;
            default:
                break;
        }

        if (explosionEffect)
        {
            float time = explosionEffect.GetComponent<ParticleSystem>().time;
            int count = explosionEffect.GetComponent<ParticleSystem>().particleCount;
            if (time >= 0.1f && count == 0)
            {
                Destroy(explosionEffect);
            }
        }
    }

    void GnatFlyTo()
    {
        m_fTime += Time.deltaTime * 0.45f;

        // Move current object to center position;
        CurrentObject.transform.position = Vector3.Lerp(RightPos.position, CenterPos.position, m_fTime);

        if (m_fTime >= 1.0f)
        {
            m_fTime = 0.0f;
            Status = BarrelStatus.GnatInConversation;
            // Start to play the conversation.
            CurrentObject.GetComponent<PlayDialog>().StartDialog();
        }
    }

    void GnatConversation()
    {
        // Check if conversation is finished.
        if (!CurrentObject.GetComponent<PlayDialog>().IsPlaying)
        {
            Status = BarrelStatus.GnatStandby;
        }
    }

    void GnatStandby()
    {
        // Check clicking on the barrels
        if (Input.GetMouseButton(0))
        {
            string[] hits = Global.GetMousePosToAllObj();
            foreach (string hit in hits)
            {
                if (hit.Contains(BarrelName))
                {
                    CorrectBarrel = false;
                    if (hit == CurrentObject.GetComponent<PlayDialog>().CorrectBarrel.name)
                    {
                        // Correct barrel!!
                        CorrectBarrel = true;
                    }
                    start = CurrentObject.transform.position;
                    Status = BarrelStatus.GnatFlyingAway;
                    break;
                }
            }
        }
    }

    void GnatFlyingAway()
    {
        m_fTime += Time.deltaTime * 0.45f;

        if (CorrectBarrel)
        {
            GameObject barrel = CurrentObject.GetComponent<PlayDialog>().CorrectBarrel;
            CurrentObject.transform.position = Vector3.Lerp(start, barrel.transform.position, m_fTime);

            if (m_fTime >= 1.0f)
            {
                // Turn off this gnat, Sparx fly to pos
                CurrentObject.GetComponent<PlayDialog>().enabled = false;
                Sparx[iCurrentPair].transform.position = CurrentObject.transform.position;
                CurrentObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
                CurrentObject = Sparx[iCurrentPair];
                SoundPlayer.PlaySound("sfx-pop", 1.0f);
                explosionEffect = (GameObject)Instantiate(GameObject.Find("Gnat Explosion"), CurrentObject.transform.position, Quaternion.identity);
                explosionEffect.GetComponent<ParticleSystem>().Play();
                Status = BarrelStatus.SparkFlyToPosition;
                start = CurrentObject.transform.position;
                foreach (GameObject platform in Platforms)
                {
                    if (!platform.GetComponent<ObjectMovement>().m_bStartMoving)
                    {
                        platform.GetComponent<ObjectMovement>().m_bStartMoving = true;
                        break;
                    }
                }
                m_fTime = 0.0f;
            }
        }
        else
        {

            // away~~~;
            CurrentObject.transform.position = Vector3.Lerp(start, LeftPos.position, m_fTime);

            if (m_fTime >= 1.0f)
            {
                // Next Gnats
                if (iCurrentPair < 4)
                {
                    do {
                        iCurrentPair += 1;
                        if (iCurrentPair > 4)
                        {
                            iCurrentPair = 0;
                        }
                    } while (Gnats[iCurrentPair] && !Gnats[iCurrentPair].GetComponent<PlayDialog>().enabled);
                }
                else
                {
                    for (int i = 0; i < 5; ++i)
                    {
                        if (Gnats[i].GetComponent<PlayDialog>().enabled)
                        {
                            iCurrentPair = i;
                            CurrentObject = Gnats[i];
                            break;
                        }
                    }
                }
                CurrentObject = Gnats[iCurrentPair];
                if (!CurrentObject.GetComponent<PlayDialog>().enabled)
                {
                    CurrentObject.GetComponent<PlayDialog>().enabled = true;
                }
                CurrentObject.transform.position = FlyFrom.position;
                start = FlyFrom.position;
                Status = BarrelStatus.GnatFlyToPosition;
                m_fTime = 0.0f;
            }
        }
    }

    void SparkFlyToPos()
    {
        m_fTime += Time.deltaTime * 0.45f;
        // Move current object to center position;
        CurrentObject.transform.position = Vector3.Lerp(start, CenterPos.position, m_fTime);

        if (m_fTime >= 1.0f)
        {
            m_fTime = 0.0f;
            Status = BarrelStatus.SparkInConversation;
            // Start to play the conversation.
            CurrentObject.GetComponent<PlayDialog>().StartDialog();
        }
    }

    void SparkInConversation()
    {
        // Check if conversation is finished.
        if (!CurrentObject.GetComponent<PlayDialog>().IsPlaying)
        {
            start = CurrentObject.transform.position;
            Status = BarrelStatus.SparkFlyingAway;
        }
    }

    void SparkFlyingAway()
    {
        m_fTime += Time.deltaTime * 0.45f;

        CurrentObject.transform.position = Vector3.Lerp(start, RightPos.position, m_fTime);

        if (m_fTime >= 1.0f)
        {
            m_fTime = 0.0f;
            bool finish = true;
            foreach (GameObject gnat in Gnats)
            {
                if (gnat && gnat.GetComponent<PlayDialog>().enabled)
                {
                    finish = false;
                }
            }

            // Check and set the next current object.
            // if it was the last object, go back to the front of the queue
            // and find the undead gnats.
            if (iCurrentPair < 4)
            {
                do {
                    iCurrentPair += 1;
                    if (iCurrentPair > 4)
                    {
                        if (finish)
                        {
                            break;
                        }
                        iCurrentPair = 0;
                    }
                } while (Gnats[iCurrentPair] && !Gnats[iCurrentPair].GetComponent<PlayDialog>().enabled);
            }
            else
            {
                for (int i = 0; i < 5; ++i)
                {
                    if (Gnats[i].GetComponent<PlayDialog>().enabled)
                    {
                        iCurrentPair = i;
                        break;
                    }
                }
            }

            if (iCurrentPair != 5)
            {
                CurrentObject = Gnats[iCurrentPair];
                CurrentObject.transform.position = FlyFrom.position;
                start = Gnats[iCurrentPair].transform.position;
            }
            Status = BarrelStatus.GnatFlyToPosition;

            if (Status != BarrelStatus.GnatFlyToPosition || finish)
            {
                EndGame();
            }
            else
            {
                CurrentObject.transform.position = FlyFrom.position;
            }
        }
    }

    void EndGame()
    {
        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "BarrelGame2");
        Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
        foreach (GameObject gnat in Gnats)
        {
            GameObject barrel = gnat.GetComponent<PlayDialog>().CorrectBarrel;
            barrel.transform.Find("Word").GetComponent<GUIText>().enabled = false;
        }
        Destroy(this);
        m_bEnd = true;
        ActivateOutlines(false);
    }

    void OnTriggerEnter()
    {
        if (m_bGameStart ||
            !m_bCanPlay)
        {
            return;
        }

        ActivateOutlines(true);

        Global.GetPlayer().transform.position = Global.GetObjectPositionByID("PlayerPos");
        Vector3 pos = Global.GetObjectPositionByID(PlayerLookat);
        Vector3 lookat = new Vector3(pos.x, Global.GetPlayer().transform.position.y, pos.z);
        Global.GetPlayer().transform.LookAt(lookat);
        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "BarrelGame3");
        Global.CameraSnapBackToPlayer();
        Global.GetPlayer().GetComponent<Animation>().Play("idle");
        CurrentObject.transform.position = FlyFrom.position;

        foreach (GameObject gnat in Gnats)
        {
            GameObject barrel = gnat.GetComponent<PlayDialog>().CorrectBarrel;
            barrel.transform.Find("Word").GetComponent<GUIText>().enabled = true;
        }
        //Tui needs fly down
        if (Global.GetCurrentLevelID() == "Level5")
        {
            GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyTo("35021");
            m_bTuiConversationStarts = true;
        }
        else
        {
            m_bGameStart = true;
        }
        GetComponent<BoxCollider>().enabled = false;
    }

    public void ActivateOutlines(bool _b)
    {
        foreach(OutlineObject line in outlines )
        {
            if (_b)
                line.TurnOnOutline();
            else
                line.TurnOffOutline();
                    
        }
    }
}
