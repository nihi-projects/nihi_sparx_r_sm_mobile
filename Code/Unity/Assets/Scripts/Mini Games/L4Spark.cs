using UnityEngine;
using System.Collections;

public class L4Spark : MonoBehaviour 
{

	public bool m_bSparkInteracted = false;
	public float m_bSparkInteractionRadius = 1.5f;
	public bool m_bSparkIsInteracting = false;
	
	//private Vector3 m_vPlayerStandPosition;
	public bool m_bGrabbed = false;
	public bool m_bCollected = false;
	//private bool m_bPlayerReleasedSpark = false;
	
	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	
	float m_fTime = 0.0f;
	// Use this for initialization
	void Start () 
	{
		//Global.GetPlayer().transform.position.z = gameObject.transform.position.z + 0.5f;
		//m_vPlayerStandPosition = Global.GetPlayer().transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_bSparkInteracted)
		{
			return;
		}
		
		//Player stand in front of Spark
		if(m_bSparkIsInteracting && !m_bGrabbed)
		{
			//Player interacting with Spark
			if(!Global.GetPlayer().GetComponent<Animation>().IsPlaying("take"))
			{	
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L4Spark");
				//Player picks up the spark
				Global.GetPlayer().GetComponent<Animation>().Play("take");
			}
			m_fTime += Time.deltaTime;
			if (m_fTime >= 1.0f)
			{
				m_bGrabbed = true;
			}
		}
	}
		
	void OnMouseDown()
	{
		float fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
		if(fDistance <= m_bSparkInteractionRadius && m_bGrabbed == false)
		{
			if(!m_bSparkIsInteracting)
			{
				m_bSparkIsInteracting = true;
				Global.GetPlayer().transform.LookAt(new Vector3(transform.position.x, Global.GetPlayer().transform.position.y, transform.position.z));
				Destroy(GetComponent<SphereCollider>());
			}
		}
	}
	
	void OnMouseOver()
	{
		float fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
		if(fDistance <= m_bSparkInteractionRadius && m_bGrabbed == false)
		{
			//Change the cursor image of the mouse
			GameObject.Find("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
			GameObject.Find("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
		}
	}
	
	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		GameObject.Find("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
		GameObject.Find("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
}
