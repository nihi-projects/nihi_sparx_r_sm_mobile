using UnityEngine;
using System.Collections;

public class IngameMusicSwitch : MonoBehaviour {
	private static IngameMusicSwitch instanceRef;

	//Game music audio source
	private AudioSource m_gameMusicAs; 
	
	private string currentSceneName = "";
	
	public AudioClip m_gameStartFadeMusic;
	public AudioClip m_guideFadeMusic;
	public AudioClip m_mentorMusic;
	
	//Music for cave levels
	public AudioClip m_l1Music;
	public AudioClip m_l2Music;
	public AudioClip m_l3Music;
	public AudioClip m_l4Music;
	public AudioClip m_l5Music;
	public AudioClip m_l6Music;
	public AudioClip m_l7Music;
	
	public bool m_playStartFadeMisic = false;
	public bool m_playFadeMisic = false;
	
	/*
	*
	* Awake function.
	*
	* The Awake function is run before Start.
	*
	*/
	public void Awake()
	{
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
			m_gameMusicAs = gameObject.GetComponent<AudioSource>();//Add audio source in the game object
		}else{
			DestroyImmediate (gameObject);
		}
		
	}
	
	// Use this for initialization
	public void Start () 
	{
		
	}
	
	public void OnEnable()
	{
		//m_playStartFadeMisic = false;
		m_playFadeMisic = false;

	}
		
	// Update is called once per frame
	public void Update () 
	{
		//When the scene changes
		if(currentSceneName != Application.loadedLevelName){
			m_gameMusicAs.GetComponent<AudioSource>().clip = GameMusicAudioClip();
			m_gameMusicAs.Play();
			
			//Update the scene name
			currentSceneName = Application.loadedLevelName;
		}
		else{ 
			if(Application.loadedLevelName == "GuideScene"
				&& GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen>().enabled
				&& !m_playStartFadeMisic){
				m_gameMusicAs.GetComponent<AudioSource>().clip = m_gameStartFadeMusic;
				m_gameMusicAs.Play();
				m_playStartFadeMisic = true;
				m_playFadeMisic = false;
			}
			if(Application.loadedLevelName == "GuideScene"
				&& !GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen>().enabled
				&& !m_playFadeMisic){
				m_gameMusicAs.GetComponent<AudioSource>().clip = m_guideFadeMusic;
				m_gameMusicAs.Play();
				m_playStartFadeMisic = false;
				m_playFadeMisic = true;
			}
		}
	}
	
	/*
	*
	* GameMusicSwitch function.
	*
	* This method is used to switch over the music in different levels
	*
	*/
	public AudioClip GameMusicAudioClip()
	{
		AudioClip currentAudioClip = null;
		
		if(Application.loadedLevelName == "LevelS"){
			currentAudioClip = m_mentorMusic;
		}
		
		if(Application.loadedLevelName != "GuideScene" && Application.loadedLevelName != "LevelS"){
			//Level 1
			if(Application.loadedLevelName == "Level1_Cave"){
				currentAudioClip = m_l1Music;
			}
			//Level 2
			if(Application.loadedLevelName == "Level2_Ice"){
				currentAudioClip = m_l2Music;
			}
			//Level 3
			if(Application.loadedLevelName == "Level3_Lava"){
				currentAudioClip = m_l3Music;
			}
			//Level 4
			if(Application.loadedLevelName == "Level4_Cliff"){
				currentAudioClip = m_l4Music;
			}
			//Level 5
			if(Application.loadedLevelName == "Level5_Swamp"){
				currentAudioClip = m_l5Music;
			}
			//Level 6
			if(Application.loadedLevelName == "Level6_Bridge"){
				currentAudioClip = m_l6Music;
			}
			//Level 7
			if(Application.loadedLevelName == "Level7_Canyon"){
				currentAudioClip = m_l7Music;
			}
		}
		
		return currentAudioClip;
	}
}
