﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RemoveDuplicateObjects : MonoBehaviour {
	int frameCount  = 0;
	const int FRAMES_REQUIRED = 5;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(frameCount == FRAMES_REQUIRED){
			//make sure everything is instatiated before calling
			removeTheObjects();
		}
		frameCount ++;

	}

	void removeTheObjects(){
		GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
		Dictionary<string, int> duplicateCheck = new Dictionary<string, int>();
		List<GameObject> objectDeleteQueue = new List<GameObject>();
		foreach(GameObject go in allObjects){
			if(duplicateCheck.ContainsKey(go.name)){
				duplicateCheck[go.name] += 1;
			} else {
				duplicateCheck[go.name] = 1;
			}
		}
		foreach(GameObject go in allObjects){
			if(duplicateCheck[go.name] > 1){
				objectDeleteQueue.Add(go);
				duplicateCheck[go.name] -= 1;
			}
		}
		while(objectDeleteQueue.Count > 0){
			GameObject thisObject = objectDeleteQueue[0];
			objectDeleteQueue.RemoveAt(0);
			Destroy(thisObject);
			//Destroy(objectDeleteQueue[0]);//might need to seperate this in to set var, remove from queue, destroy.
		}
	}
}
