using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
  
public class LoadTalkSceneXMLData : MonoBehaviour // the Class
{
 	// Static Private Variables
  	private string sm_strFilename;
  	private TextReader sm_Read = null;
	private int MulitiQuestionNum = 0;
	
	//This list is used to contain different blocks of scenes
	private XmlNodeList scenesList;
	
	//Contains all the scene blocks
	private List<Dictionary<string,string>> m_xmlScenes = new List<Dictionary<string,string>>();
	//Contains the elements in each scene block
	private Dictionary<string,string> m_xmlElement = new Dictionary<string,string>();

	Goodbye GoodByeScript;

	public Transform MiniGame_L3DragAndDropBlinc;

//	public string LoadXML(string _fileName)
//	{
//		string url = "http://localhost/unity/" + _fileName;
//		Debug.Log ("The file URL is ==== " + url);
//		WWW www = new WWW(url);
//		while(!www.isDone)
//		{
//			//yield WaitForSeconds(1);
//			yield return new WaitForSeconds(2);
//		}
//		//yield www;
//		yield return www;
//		string _info = www.data;
//
//		return _info;
//	}
	
	/*
	*
	* This function generally read the file and find out the question type of the next scene
	*
	*/
	public bool GeneralReadFromXML(TextAsset _strFileAsset)
	{
		XmlDocument xmlDoc = new XmlDocument();
        //For editor build
        if (_strFileAsset == null || _strFileAsset.text == "")
            Debug.LogError("There's no file asset to load.");
                
		xmlDoc.LoadXml(_strFileAsset.text); 
		
	  	// array of the scene nodes.
		scenesList = xmlDoc.GetElementsByTagName("scene");
	  	
		XmlNodeList sceneContent;
	  	//Get the scene information from the xml file, read from one by one
	  	foreach(XmlNode sceneInfo in scenesList){
			//Get all the xml scene blocks
	  		sceneContent = sceneInfo.ChildNodes;
			
	  		//Go through each element in each scene node
	  		foreach(XmlNode sceneElement in sceneContent){
				//Public nodes for all the question types
	  			if(sceneElement.Name == "sceneName"){
					if(m_xmlElement.ContainsKey("sceneName")){
						m_xmlElement.Remove("sceneName");
					}
					m_xmlElement.Add("sceneName",sceneElement.InnerText); // put this in the dictionary.
	  			}
				
	  			if(sceneElement.Name == "PersonName"){
					if(m_xmlElement.ContainsKey("PersonName")){
						m_xmlElement.Remove("PersonName");
					}
					m_xmlElement.Add("PersonName",sceneElement.InnerText); // put this in the dictionary.
		    	}
				
				if(sceneElement.Name == "QuestionType"){
					if(m_xmlElement.ContainsKey("QuestionType")){
						m_xmlElement.Remove("QuestionType");
					}
					m_xmlElement.Add("QuestionType",sceneElement.InnerText); // put this in the dictionary.
				}
				
				//Check the question types for each element, then use different ways of reading the file
				if(sceneElement.Name == "object"){
					switch(m_xmlElement["QuestionType"]){
						case "Statement":
							ReadStateMentFromXML(sceneElement);
							break;
						
						case "MultiChoice":
							ReadMultiChoiceFromXML(sceneElement);
							break;
						
						case "NonVoiceStatement":
							ReadNonVoiceStatementFromXML(sceneElement);
							break;
						
						case "SubMultiChoiceQuestion":
							ReadSubMultiChoiceQuestionFromXML(sceneElement);
							break;
					}	
				}
		    }
			
			//Add the block of scene elements into the scene list
		    m_xmlScenes.Add(m_xmlElement);
			m_xmlElement = new Dictionary<string, string>();
		}
	  	
		sm_strFilename = _strFileAsset.name;
//		Debug.Log ("XML File Name: " + sm_strFilename);
		// Check if file exists and record appropriate message.
		FileInfo source = null;
		try{
			//Debug.Log ("Directory:"+Directory.GetCurrentDirectory());
			source = new FileInfo(sm_strFilename);
		}catch(System.Exception e){
			Debug.Log ("Exception: " + e);
		}

		//Debug.Log ("Source: " + source);
		if (source != null && source.Exists){			
			// OpenText returns a StreamReader, which is a subclass of TextReader.
			//sm_Read = source.Open(FileMode.Open); 
			Debug.Log (sm_strFilename + " file is found");
		}
		// Load from internal resources.
		else{
			//Debug.Log("Source:"+sm_strFilename+" does not exist");
			TextAsset LoadedFile = null;
			
			// Attempt to load as an internal resource.
			LoadedFile = _strFileAsset;
			// StringReader is a subclass of TextReader.
			if (LoadedFile != null){
				sm_Read = new StringReader(LoadedFile.text);
			}
		}
		
		// File do exist.
		if (sm_Read != null){	
			#if UNITY_WEBPLAYER
				Debug.Log("Opened " + sm_strFilename + " successfully!");
			#endif
		}
		// File does not exist.
		else if(!source.Exists){
			#if UNITY_WEBPLAYER
				Debug.LogWarning(sm_strFilename + " does not exist!");
			#endif
			
			return false;
		}
		// File exists but could not be read.
		else{
			#if UNITY_WEBPLAYER
				Debug.LogWarning("Could not read " + sm_strFilename + ".");
			#endif
			
			return false;
		} 
		
		// Close the text reader.
		sm_Read.Close();
		sm_Read = null;
		
		return true;
	}
	
	/*
	*
	* This function read the statement content
	*
	*/
	public void ReadStateMentFromXML(XmlNode _aSceneElement)
	{
		switch(_aSceneElement.Attributes["name"].Value){
  			case "VoiceStartTime":
				if(m_xmlElement.ContainsKey("VoiceStartTime")){
					m_xmlElement.Remove("VoiceStartTime");
				}
				m_xmlElement.Add("VoiceStartTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "VoiceEndTime":
				if(m_xmlElement.ContainsKey("VoiceEndTime")){
					m_xmlElement.Remove("VoiceEndTime");
				}
				m_xmlElement.Add("VoiceEndTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "VoiceText":
				if(m_xmlElement.ContainsKey("VoiceText")){
					m_xmlElement.Remove("VoiceText");
				}
				m_xmlElement.Add("VoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NonVoiceText":
				if(m_xmlElement.ContainsKey("NonVoiceText")){
					m_xmlElement.Remove("NonVoiceText");
				}
				m_xmlElement.Add("NonVoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "PreviousbuttonExist":
				if(m_xmlElement.ContainsKey("PreviousbuttonExist")){
					m_xmlElement.Remove("PreviousbuttonExist");
				}
				m_xmlElement.Add("PreviousbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "NextbuttonExist":
				if(m_xmlElement.ContainsKey("NextbuttonExist")){
					m_xmlElement.Remove("NextbuttonExist");
				}
				m_xmlElement.Add("NextbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "PreviousSceneName":
				if(m_xmlElement.ContainsKey("PreviousSceneName")){
					m_xmlElement.Remove("PreviousSceneName");
				}
				m_xmlElement.Add("PreviousSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NextSceneName":
				if(m_xmlElement.ContainsKey("NextSceneName")){
					m_xmlElement.Remove("NextSceneName");
				}
				m_xmlElement.Add("NextSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "AudioName":
				if(m_xmlElement.ContainsKey("AudioName")){
					m_xmlElement.Remove("AudioName");
				}
				m_xmlElement.Add("AudioName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "CameraID":
				if(m_xmlElement.ContainsKey("CameraID")){
					m_xmlElement.Remove("CameraID");
				}
				m_xmlElement.Add("CameraID",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "InteractObjectsHeadTo":
				if(m_xmlElement.ContainsKey("InteractObjectsHeadTo")){
					m_xmlElement.Remove("InteractObjectsHeadTo");
				}
				m_xmlElement.Add("InteractObjectsHeadTo",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "ObjectAnimation":
				if(m_xmlElement.ContainsKey("ObjectAnimation")){
					m_xmlElement.Remove("ObjectAnimation");
				}
				m_xmlElement.Add("ObjectAnimation",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "OtherObjAnimation":
				if(m_xmlElement.ContainsKey("OtherObjAnimation")){
					m_xmlElement.Remove("OtherObjAnimation");
				}
				m_xmlElement.Add("OtherObjAnimation",_aSceneElement.InnerText); // put this in the dictionary.
				break;
 		}
	}
	
	/*
	*
	* This function read the multichice questions
	*
	*/
	public void ReadMultiChoiceFromXML(XmlNode _aSceneElement)
	{
		if(_aSceneElement.Attributes["name"].Value.Equals("VoiceStartTime")){
			if(m_xmlElement.ContainsKey("VoiceStartTime")){
				m_xmlElement.Remove("VoiceStartTime");
			}
			m_xmlElement.Add("VoiceStartTime",_aSceneElement.InnerText); // put this in the dictionary.
		}
		
		if(_aSceneElement.Attributes["name"].Value.Equals("VoiceEndTime")){
			if(m_xmlElement.ContainsKey("VoiceEndTime")){
				m_xmlElement.Remove("VoiceEndTime");
			}
			m_xmlElement.Add("VoiceEndTime",_aSceneElement.InnerText); // put this in the dictionary.
		}
		
		//Read the question number
		if(_aSceneElement.Attributes["name"].Value.Equals("QuestionNumber")){
			if(m_xmlElement.ContainsKey("QuestionNumber")){
				m_xmlElement.Remove("QuestionNumber");
			}
			MulitiQuestionNum = int.Parse(_aSceneElement.InnerText);
			m_xmlElement.Add("QuestionNumber",_aSceneElement.InnerText); // put this in the dictionary.
		}
		
		//Read the question one by one depends the number of questions the xml file has
		for(int i = 1; i <= MulitiQuestionNum; i++)
		{	
			if(_aSceneElement.Attributes["name"].Value.Equals("Question" + i.ToString())){
				if(m_xmlElement.ContainsKey("Question" + i.ToString())){
					m_xmlElement.Remove("Question" + i.ToString());
				}
				m_xmlElement.Add("Question" + i.ToString(),_aSceneElement.InnerText); // put this in the dictionary.
			}
			
			if(_aSceneElement.Attributes["name"].Value.Equals("Q" + i.ToString() + "OptionA")){
				if(m_xmlElement.ContainsKey("Q" + i.ToString() + "OptionA")){
					m_xmlElement.Remove("Q" + i.ToString() + "OptionA");
				}
				m_xmlElement.Add("Q" + i.ToString() + "OptionA",_aSceneElement.InnerText); // put this in the dictionary.
			}
			
			if(_aSceneElement.Attributes["name"].Value.Equals("Q" + i.ToString() + "OptionB")){
				if(m_xmlElement.ContainsKey("Q" + i.ToString() + "OptionB")){
					m_xmlElement.Remove("Q" + i.ToString() + "OptionB");
				}
				m_xmlElement.Add("Q" + i.ToString() + "OptionB",_aSceneElement.InnerText); // put this in the dictionary.
			}
			
			if(_aSceneElement.Attributes["name"].Value.Equals("Q" + i.ToString() + "OptionC")){
				if(m_xmlElement.ContainsKey("Q" + i.ToString() + "OptionC")){
					m_xmlElement.Remove("Q" + i.ToString() + "OptionC");
				}
				m_xmlElement.Add("Q" + i.ToString() + "OptionC",_aSceneElement.InnerText); // put this in the dictionary.
			}
			
			if(_aSceneElement.Attributes["name"].Value.Equals("Q" + i.ToString() + "OptionD")){
				if(m_xmlElement.ContainsKey("Q" + i.ToString() + "OptionD")){
					m_xmlElement.Remove("Q" + i.ToString() + "OptionD");
				}
				m_xmlElement.Add("Q" + i.ToString() + "OptionD",_aSceneElement.InnerText); // put this in the dictionary.
			}
		}
		
		switch(_aSceneElement.Attributes["name"].Value)
		{
			case "VoiceText":
				if(m_xmlElement.ContainsKey("VoiceText")){
					m_xmlElement.Remove("VoiceText");
				}
				m_xmlElement.Add("VoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "NonVoiceText":
				if(m_xmlElement.ContainsKey("NonVoiceText")){
					m_xmlElement.Remove("NonVoiceText");
				}
				m_xmlElement.Add("NonVoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "PreviousbuttonExist":
				if(m_xmlElement.ContainsKey("PreviousbuttonExist")){
					m_xmlElement.Remove("PreviousbuttonExist");
				}
				m_xmlElement.Add("PreviousbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "NextbuttonExist":
				if(m_xmlElement.ContainsKey("NextbuttonExist")){
					m_xmlElement.Remove("NextbuttonExist");
				}
				m_xmlElement.Add("NextbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "PreviousSceneName":
				if(m_xmlElement.ContainsKey("PreviousSceneName")){
					m_xmlElement.Remove("PreviousSceneName");
				}
				m_xmlElement.Add("PreviousSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "NextSceneName":
				if(m_xmlElement.ContainsKey("NextSceneName")){
					m_xmlElement.Remove("NextSceneName");
				}
				m_xmlElement.Add("NextSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "ObjectAnimation":
				if(m_xmlElement.ContainsKey("ObjectAnimation")){
					m_xmlElement.Remove("ObjectAnimation");
				}
				m_xmlElement.Add("ObjectAnimation",_aSceneElement.InnerText); // put this in the dictionary.
				break;
		}
	}
	
	/*
	*
	* This function read the non voice statement in the scene
	*
	*/
	public void ReadNonVoiceStatementFromXML(XmlNode _aSceneElement)
	{
		switch(_aSceneElement.Attributes["name"].Value){
			case "VoiceStartTime":
				if(m_xmlElement.ContainsKey("VoiceStartTime")){
					m_xmlElement.Remove("VoiceStartTime");
				}
				m_xmlElement.Add("VoiceStartTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "VoiceEndTime":
				if(m_xmlElement.ContainsKey("VoiceEndTime")){
					m_xmlElement.Remove("VoiceEndTime");
				}
				m_xmlElement.Add("VoiceEndTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "VoiceText":
				if(m_xmlElement.ContainsKey("VoiceText")){
					m_xmlElement.Remove("VoiceText");
				}
				m_xmlElement.Add("VoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NonVoiceText":
				if(m_xmlElement.ContainsKey("NonVoiceText")){
					m_xmlElement.Remove("NonVoiceText");
				}
				m_xmlElement.Add("NonVoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;

  			case "PreviousbuttonExist":
				if(m_xmlElement.ContainsKey("PreviousbuttonExist")){
					m_xmlElement.Remove("PreviousbuttonExist");
				}
				m_xmlElement.Add("PreviousbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "NextbuttonExist":
				if(m_xmlElement.ContainsKey("NextbuttonExist")){
					m_xmlElement.Remove("NextbuttonExist");
				}
				m_xmlElement.Add("NextbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "PreviousSceneName":
				if(m_xmlElement.ContainsKey("PreviousSceneName")){
					m_xmlElement.Remove("PreviousSceneName");
				}
				m_xmlElement.Add("PreviousSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NextSceneName":
				if(m_xmlElement.ContainsKey("NextSceneName")){
					m_xmlElement.Remove("NextSceneName");
				}
				m_xmlElement.Add("NextSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "ObjectAnimation":
				if(m_xmlElement.ContainsKey("ObjectAnimation")){
					m_xmlElement.Remove("ObjectAnimation");
				}
				m_xmlElement.Add("ObjectAnimation",_aSceneElement.InnerText); // put this in the dictionary.
				break;
 		}
	}
	
	/*
	*
	* This function read the submultichoise questions in the scene
	*
	*/
	public void ReadSubMultiChoiceQuestionFromXML(XmlNode _aSceneElement)
	{
		switch(_aSceneElement.Attributes["name"].Value){
  			case "VoiceStartTime":
				if(m_xmlElement.ContainsKey("VoiceStartTime")){
					m_xmlElement.Remove("VoiceStartTime");
				}
				m_xmlElement.Add("VoiceStartTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;

  			case "VoiceEndTime":
				if(m_xmlElement.ContainsKey("VoiceEndTime")){
					m_xmlElement.Remove("VoiceEndTime");
				}
				m_xmlElement.Add("VoiceEndTime",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "VoiceText":
				if(m_xmlElement.ContainsKey("VoiceText")){
					m_xmlElement.Remove("VoiceText");
				}
				m_xmlElement.Add("VoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NonVoiceText":
				if(m_xmlElement.ContainsKey("NonVoiceText")){
					m_xmlElement.Remove("NonVoiceText");
				}
				m_xmlElement.Add("NonVoiceText",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "OptionA":
				if(m_xmlElement.ContainsKey("OptionA")){
					m_xmlElement.Remove("OptionA");
				}
				m_xmlElement.Add("OptionA",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionB":
				if(m_xmlElement.ContainsKey("OptionB")){
					m_xmlElement.Remove("OptionB");
				}
				m_xmlElement.Add("OptionB",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionC":
				if(m_xmlElement.ContainsKey("OptionC")){
					m_xmlElement.Remove("OptionC");
				}
				m_xmlElement.Add("OptionC",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionD":
				if(m_xmlElement.ContainsKey("OptionD")){
					m_xmlElement.Remove("OptionD");
				}
				m_xmlElement.Add("OptionD",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionGotoA":
				if(m_xmlElement.ContainsKey("OptionGotoA")){
					m_xmlElement.Remove("OptionGotoA");
				}
				m_xmlElement.Add("OptionGotoA",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionGotoB":
				if(m_xmlElement.ContainsKey("OptionGotoB")){
					m_xmlElement.Remove("OptionGotoB");
				}
				m_xmlElement.Add("OptionGotoB",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionGotoC":
				if(m_xmlElement.ContainsKey("OptionGotoC")){
					m_xmlElement.Remove("OptionGotoC");
				}
				m_xmlElement.Add("OptionGotoC",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			case "OptionGotoD":
				if(m_xmlElement.ContainsKey("OptionGotoD")){
					m_xmlElement.Remove("OptionGotoD");
				}
				m_xmlElement.Add("OptionGotoD",_aSceneElement.InnerText); // put this in the dictionary.
				break;

  			case "PreviousbuttonExist":
				if(m_xmlElement.ContainsKey("PreviousbuttonExist")){
					m_xmlElement.Remove("PreviousbuttonExist");
				}
				m_xmlElement.Add("PreviousbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
  			case "NextbuttonExist":
				if(m_xmlElement.ContainsKey("NextbuttonExist")){
					m_xmlElement.Remove("NextbuttonExist");
				}
				m_xmlElement.Add("NextbuttonExist",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "PreviousSceneName":
				if(m_xmlElement.ContainsKey("PreviousSceneName")){
					m_xmlElement.Remove("PreviousSceneName");
				}
				m_xmlElement.Add("PreviousSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "NextSceneName":
				if(m_xmlElement.ContainsKey("NextSceneName")){
					m_xmlElement.Remove("NextSceneName");
				}
				m_xmlElement.Add("NextSceneName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "AudioName":
				if(m_xmlElement.ContainsKey("AudioName")){
					m_xmlElement.Remove("AudioName");
				}
				m_xmlElement.Add("AudioName",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "CameraID":
				if(m_xmlElement.ContainsKey("CameraID")){
					m_xmlElement.Remove("CameraID");
				}
				m_xmlElement.Add("CameraID",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "InteractObjectsHeadTo":
				if(m_xmlElement.ContainsKey("InteractObjectsHeadTo")){
					m_xmlElement.Remove("InteractObjectsHeadTo");
				}
				m_xmlElement.Add("InteractObjectsHeadTo",_aSceneElement.InnerText); // put this in the dictionary.
				break;
			
			case "ObjectAnimation":
				if(m_xmlElement.ContainsKey("ObjectAnimation")){
					m_xmlElement.Remove("ObjectAnimation");
				}
				m_xmlElement.Add("ObjectAnimation",_aSceneElement.InnerText); // put this in the dictionary.
				break;
 		}
	}
	
	/*
	*
	* This function loads up a specific scene information from the xml file
	*
	*/
	public string LoadVoiceText(string _sceneName)
	{	
		string m_voiceText = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			//Find the scene block name first
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				//Find the perticular element in this scene block
				m_voiceText = m_xmlScenes[i]["VoiceText"];
			}
		}
		//Split up the text into lines
		m_voiceText = m_voiceText.Replace("\\n", "\n");
		
		return m_voiceText;
	}
	/*
	*
	* This function loads up a specific scene information from the xml file
	*
	*/
	public string LoadNonVoiceText(string _sceneName)
	{	
		string m_nonVoiceText = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			//Find the scene block name first
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				//Find the perticular element in this scene block
				m_nonVoiceText = m_xmlScenes[i]["NonVoiceText"];
			}
		}
		//Split up the text into lines
		m_nonVoiceText = m_nonVoiceText.Replace("\\n", "\n");
		
		return m_nonVoiceText;
	}
	
	/*
	*
	* This function returns the next scene information
	*
	*/
	public string GetPreSceneName(string _sceneName)
	{	
		string m_tempPartSceneName = "";
		string m_sPreSceneName     = "";
		bool m_bScriptFindInGUI    = false;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				m_sPreSceneName = m_xmlScenes[i]["PreviousSceneName"];
				if(m_sPreSceneName != ""){
					//Check if the previous scene name contains text or just a image
					m_tempPartSceneName = m_sPreSceneName.Substring(0, 4);
					
					//text
					if(m_tempPartSceneName.Equals("Text")){
						m_sPreSceneName = m_sPreSceneName.Substring(4);
					}
					
					//Image
					else if(m_tempPartSceneName.Equals("Imag")){
						m_sPreSceneName = m_sPreSceneName.Substring(4);
						
						foreach (MonoBehaviour temp in GetComponents<MonoBehaviour>()) {
							//Get the name of the script from the XML
							string partName = temp.ToString().Substring(5);
							int partNameLength = partName.Length;
							string scriptName = partName.Substring(0, partNameLength - 1);
							
							if(scriptName.Equals(m_sPreSceneName)){
								GetComponent<TalkScenes>().enabled = false;
								m_bScriptFindInGUI = true;
								SwitchScriptByName(scriptName);
							}
						}
						
						if(!m_bScriptFindInGUI){
							string guiObjectName = "";
							switch(Global.CurrentLevelNumber){
								case 1:
									guiObjectName = "L1GUI";
									break;
								case 2:
									guiObjectName = "L2GUI";
									break;
								case 3:
									guiObjectName = "L3GUI";
									break;
								case 4:
									guiObjectName = "L4GUI";
									break;
								case 5:
									guiObjectName = "L5GUI";
									break;
								case 6:
									guiObjectName = "L6GUI";
									break;
								case 7:
									guiObjectName = "L7GUI";
									break;
							}
							if(guiObjectName != ""){
								//GUI children object components
								foreach (MonoBehaviour temp in GameObject.Find (guiObjectName).GetComponents<MonoBehaviour>()) {
									//Get the name of the script from the XML
									//Temp.ToString value is = L1GUI (filename)
									string partName = temp.ToString().Substring(7);
									int partNameLength = partName.Length;
									string scriptName = partName.Substring(0, partNameLength - 1);
									
									if(scriptName.Equals(m_sPreSceneName)){
										GetComponent<TalkScenes>().enabled = false;
										
										SwitchScriptByName(scriptName);
									}
								}
							}
						}
						
					}
				}
			}
		}
		
		return m_sPreSceneName;
	}
	
	/*
	*
	* This function returns the next scene information
	*
	*/
	public string GetNextSceneName(string _sceneName)
	{	
		string m_tempPartSceneName = "";
		string m_sNextSceneName    = "";
		bool m_bScriptFindInGUI    = false;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				m_sNextSceneName = m_xmlScenes[i]["NextSceneName"];
				if(m_sNextSceneName != ""){
					//Check if the next scene contains text or just a image
					m_tempPartSceneName = m_sNextSceneName.Substring(0, 4);
					
					//text
					if(m_tempPartSceneName.Equals("Text")){
						m_sNextSceneName = m_sNextSceneName.Substring(4);
					}
					
					//Image
					else if(m_tempPartSceneName.Equals("Imag")){
						m_sNextSceneName = m_sNextSceneName.Substring(4);
						
						//GUI object components
						foreach (MonoBehaviour temp in GetComponents<MonoBehaviour>()) {
							//Get the name of the script from the XML
							//Temp.ToString value is = GUI (filename)
							string partName = temp.ToString().Substring(5);
							int partNameLength = partName.Length;
							string scriptName = partName.Substring(0, partNameLength - 1);

                            if (scriptName.Equals(m_sNextSceneName))
                            {
                                m_bScriptFindInGUI = SwitchOffDialogue(m_sNextSceneName);
                            }
						}
                        if (m_sNextSceneName.Equals("GoodbyeQuit"))
                        {
                            m_bScriptFindInGUI = SwitchOffDialogue(m_sNextSceneName);
                        }
						if(!m_bScriptFindInGUI){
							string guiObjectName = "";
							switch(Global.CurrentLevelNumber){
								case 1:
									guiObjectName = "L1GUI";
									break;
								case 2:
									guiObjectName = "L2GUI";
									break;
								case 3:
									guiObjectName = "L3GUI";
									break;
								case 4:
									guiObjectName = "L4GUI";
									break;
								case 5:
									guiObjectName = "L5GUI";
									break;
								case 6:
									guiObjectName = "L6GUI";
									break;
								case 7:
									guiObjectName = "L7GUI";
									break;
							}
							//GUI children object components
							if(guiObjectName != ""){

                                if (GameObject.Find(guiObjectName) == null)
                                    Debug.LogError("Could not find: " + guiObjectName);

								foreach (MonoBehaviour temp in GameObject.Find (guiObjectName).GetComponents<MonoBehaviour>()) {
									//Get the name of the script from the XML
									//Temp.ToString value is = L1GUI (filename)
									string partName = temp.ToString().Substring(7);
									int partNameLength = partName.Length;
									string scriptName = partName.Substring(0, partNameLength - 1);
									
									if(scriptName.Equals(m_sNextSceneName)){
                                        SwitchOffDialogue(m_sNextSceneName);
									}
								}
							}
						}

					}
				}
			}
		}
		
		return m_sNextSceneName;
	}
	
    private bool SwitchOffDialogue( string _str )
    {
        GetComponent<TalkScenes>().enabled = false;
        SwitchScriptByName(_str);
        TextDisplayCanvas.instance.HideDialogue();
        return true;
    }

	/*
	*
	* This function returns the time of the voice start to play
	*
	*/
	public float GetVoiceStartTime(string _sceneName)
	{
		float m_voiceStartTime = 0.0f;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				m_voiceStartTime = float.Parse((m_xmlScenes[i])["VoiceStartTime"]);
			}
		}
		return m_voiceStartTime;
	}
	
	/*
	*
	* This function returns time of the voice end to play
	*
	*/
	public float GetVoiceEndTime(string _sceneName)
	{
		float m_voiceLengthTime = 0.0f;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				m_voiceLengthTime = float.Parse((m_xmlScenes[i])["VoiceEndTime"]);
				//break;
			}
		}
		return m_voiceLengthTime;
	}
	
	/*
	*
	* This function returns a boolean variable show the previous button exist or not
	*
	*/
	public bool PreviousButtonExist(string _sceneName)
	{
		string previousButton = "";
		bool m_preButtonExist = false;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				previousButton = (m_xmlScenes[i])["PreviousbuttonExist"];
				if(previousButton == "yes"){
					m_preButtonExist = true;
				}
			}
		}
		return m_preButtonExist;
	}
	
	/*
	*
	* This function returns a boolean variable show the next button exist or not
	*
	*/
	public bool NextButtonExist(string _sceneName)
	{
		string nextButton = "";
		bool m_nextButtonExist = false;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				nextButton = (m_xmlScenes[i])["NextbuttonExist"];
				if(nextButton == "yes"){
					m_nextButtonExist = true;
				}
			}
		}
		return m_nextButtonExist;
	}
	
	/*
	*
	* This function returns the question type in the xml file
	*
	*/
	public string GetQuestionType (string _sceneName)
	{
		string questionType = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				questionType = m_xmlScenes[i]["QuestionType"];
			}
		}
		return questionType;
	}
	
	/*
	*
	* This function returns the question content
	*
	*/
	public string GetQuestionText (string _sceneName, int _questionNum)
	{
		string questionText = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				questionText = m_xmlScenes[i]["Question" + _questionNum.ToString()];
			}
		}
		
		return questionText;
	}
	
	/*
	*
	* This function returns the options of the Multichoice question
	*
	*/
	public string GetQuestionOptions (string _sceneName, int _levelNum, string _option)
	{
		string optionText = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				optionText = (m_xmlScenes[i])["Q" + _levelNum.ToString() + "Option" + _option];
			}
		}
		
		//Split up the text into lines
		optionText = optionText.Replace("\\n", "\n");
		
		return optionText;
	}
	
	/*
	*
	* This function returns the options of the SubMultichoice question
	*
	*/
	public string SubMultiChoiceGetQuestionOptions (string _sceneName, string _option)
	{
		string optionText = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
            if (m_xmlScenes[i].ContainsValue(_sceneName) && m_xmlScenes[i].ContainsKey("Option" + _option))
            {
                optionText = m_xmlScenes[i]["Option" + _option];
            }
		}
		
		//Split up the text into lines
		optionText = optionText.Replace("\\n", "\n");
		
		return optionText;
	}
	
	/*
	*
	* This function returns the number of the questions in this scene
	*
	*/
	public int GetQuestionNum(string _sceneName)
	{
		int questionNum = 0;
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				questionNum = int.Parse((m_xmlScenes[i])["QuestionNumber"]);
			}
		}
		
		return questionNum;
	}
	
	/*
	*
	* This function returns the name of the character talk in the scene
	*
	*/
	public string GetCharacterName(string _sceneName)
	{
		string characterName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				characterName = m_xmlScenes[i]["PersonName"];
			}
		}
		
		return characterName;
	}
	
	/*
	*
	* This function returns the next scene name that the option leads to
	*
	*/
	public string GetSubMultichoiceNextSceneName(string _sceneName, string _option)
	{
		string gotoSceneName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				gotoSceneName = m_xmlScenes[i]["OptionGoto" + _option];		
				if(gotoSceneName != ""){
					gotoSceneName = gotoSceneName.Substring(4);
				}
			}
		}
		
		return gotoSceneName;
	}
	
	/*
	*
	* This function returns the type that options goes to
	* It can be either "Text" or "Image"
	*
	*/
	public string GetSubMultichoiceGotoType(string _sceneName, string _option)
	{
		string optionGotoType = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				optionGotoType = m_xmlScenes[i]["OptionGoto" + _option];		
				if(optionGotoType != ""){
					 optionGotoType = optionGotoType.Substring(0, 4);
				}
			}
		}
		
		return optionGotoType;
	}
	
	/*
	*
	* This function returns the name of the sound file name
	*
	*/
	public string GetSoundFileName(string _sceneName)
	{
		string soundFileName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				soundFileName = m_xmlScenes[i]["AudioName"];
			}	
		}
		
		return soundFileName;
	}
	
	/*
	*
	* This function returns the ID of the camera needs to switch to
	*
	*/
	public string GetCameraID(string _sceneName)
	{
		string cameraID = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				cameraID = m_xmlScenes[i]["CameraID"];
			}
		}
		
		return cameraID;
	}
	
	/*
	*
	* This function returns the target position of the interacting gameobject 
	*
	*/
	public string GetInteractObjHeadTo(string _sceneName)
	{
		string targetPosition = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				targetPosition = m_xmlScenes[i]["InteractObjectsHeadTo"];
			}
		}
		
		return targetPosition;
	}
	
	/*
	*
	* This function returns the name of the object animation
	*
	*/
	public string GetObjectAnimationName(string _sceneName)
	{
		string animationName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				animationName = m_xmlScenes[i]["ObjectAnimation"];
			}
		}
		
		return animationName;
	}
	
	/*
	*
	* This function returns the name of the other interacting character
	*
	*/
	public string GetOtherObjCharacterName(string _sceneName)
	{
		string otherCharacterName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName)){
				otherCharacterName = m_xmlScenes[i]["OtherObjAnimation"];
				int dashCharacterPos = otherCharacterName.IndexOf("_");
				otherCharacterName = otherCharacterName.Substring(0, dashCharacterPos);
			}
		}
		
		return otherCharacterName;
	}
	
	/*
	*
	* This function returns the animation name of the other object
	*
	*/
	public string GetOtherObjAniName(string _sceneName)
	{
		string aniName = "";
		
		//Go through each scene block
		for(int i = 0; i < m_xmlScenes.Count; i++){
			if(m_xmlScenes[i].ContainsValue(_sceneName) && m_xmlScenes[i].ContainsKey("OtherObjAnimation") ){
				aniName = m_xmlScenes[i]["OtherObjAnimation"];
				int dashCharacterPos = aniName.IndexOf("_");
				aniName = aniName.Substring(dashCharacterPos + 1);
			}
		}
		
		return aniName;
	}
	
	/*
	*
	* This function returns the name of the next scene script
	*
	*/
	public void SwitchScriptByName(string _scriptName)
	{
		Global.PreviousSceneName = GetComponent<TalkScenes>().GetCurrentScene();
		switch(_scriptName){
			case "Goodbye":
				GoodByeScript = GameObject.Find ("GUI").GetComponent<Goodbye> ();
				GoodByeScript.m_bGameWasQuit = false;
				GoodByeScript.enabled = true;
				//GameObject.Find ("GUI").GetComponent<Goodbye>().enabled = true;
				break;
				//------------------------------------------Level 1 scripts-------------------------------------------
				//Guide scene
			case "GoodbyeQuit":
				GoodByeScript = GameObject.Find ("GUI").GetComponent<Goodbye>();
				GoodByeScript.m_bGameWasQuit = true;
				GoodByeScript.enabled = true;
				//GameObject.Find ("GUI").GetComponent<Goodbye>().enabled = true;
				break;
			//------------------------------------------Level 1 scripts-------------------------------------------
			//Guide scene
			case "CharacterSelection":
				GameObject.Find ("L1GUI").GetComponent<CharacterSelection>().enabled = true;
				break;
			case "ResultBarchart":
				GetComponent<ResultBarchart>().enabled = true;
				break;
			case "GuideEndTransition":
				GetComponent<GuideEndTransition>().enabled = true;
				break;
			//Island scene
			case "CutSceneStart":
				transform.Find("L1GUI").GetComponent<CutSceneStart>().enabled = true;
				break;
			case "TerminateTalkScene":
				GetComponent<TerminateTalkScene>().enabled = true;
				break;
			//Cave scene
			case "TuiTerminateScene":
				GameObject.Find ("L1GUI").GetComponent<TuiTerminateScene>().enabled = true;
				break;
			//Back to the Guide scene again
			case "DragAndDropWords":
				GameObject.Find ("L1GUI").GetComponent<DragAndDropWords>().enabled = true;
				break;
			case "ThoughsAndFeeling":
				GameObject.Find ("L1GUI").GetComponent<ThoughsAndFeeling>().enabled = true;
				break;
			case "ShieldAgaDepression":
				GameObject.Find ("L1GUI").GetComponent<ShieldAgaDepression>().enabled = true;
				break;
			case "DragDropShield":
				GameObject.Find ("L1GUI").GetComponent<DragDropShield>().enabled = true;
				break;
			case "L1NoteBook":
				GameObject.Find ("L1GUI").GetComponent<L1NoteBook>().enabled = true;
				break;
			case "QuiteScreen":
				GetComponent<QuiteScreen>().enabled = true;
				break;
			//------------------------------------------Level 2 scripts-------------------------------------------
			case "FeelingDownScreen":
				GameObject.Find ("L2GUI").GetComponent<FeelingDownScreen>().enabled = true;
				break;
			case "TerminateCassFiremanScene":
				GameObject.Find ("L2GUI").GetComponent<TerminateCassFiremanScene>().enabled = true;
				break;
			//Ice Cave scene
			case "L2TuiTerminateScene":
				GameObject.Find ("L2GUI").GetComponent<L2TuiTerminateScene>().enabled = true;
				break;
			//Back to the Guide again
			case "L2DragAndDropWords":
				GameObject.Find ("L2GUI").GetComponent<L2DragAndDropWords>().enabled = true;
				break;
			case "L2ShieldAgaDepression":
				GameObject.Find ("L2GUI").GetComponent<L2ShieldAgaDepression>().enabled = true;
				break;
			case "L2RelaxYourMuscles":
				GameObject.Find ("L2GUI").GetComponent<L2RelaxYourMuscles>().enabled = true;
				break;
			case "L2NoteBook":
				GameObject.Find ("L2GUI").GetComponent<L2NoteBook>().enabled = true;
				break;
			//------------------------------------------Level 3 scripts-------------------------------------------
			case "L3HowIFeelMiniGame":
				GameObject.Find ("L3GUI").GetComponent<L3HowIFeelMiniGame>().enabled = true;
				break;
			case "L3ShieldAgaDepression":
				GameObject.Find ("L3GUI").GetComponent<L3ShieldAgaDepression>().enabled = true;
				break;
			case "L3DragWordsHowIReact":
				GameObject.Find ("L3GUI").GetComponent<L3DragWordsHowIReact>().enabled = true;
				break;
			case "L3TuiTerminateScene":
				GameObject.Find ("L3GUI").GetComponent<L3TuiTerminateScene>().enabled = true;
				break;
			case "L3DragDropWords":
				GameObject.Find ("L3GUI").GetComponent<L3DragDropWords>().enabled = true;
				break;
			case "L3DragDropWords2":
				GameObject.Find ("L3GUI").GetComponent<L3DragDropWords2>().enabled = true;
				break;
			case "L3DragDropWords3":
				GameObject.Find ("L3GUI").GetComponent<L3DragDropWords3>().enabled = true;
				break;
			case "L3DragDropWords4":
				GameObject.Find ("L3GUI").GetComponent<L3DragDropWords4>().enabled = true;
				break;
			case "L3StopIt":
				GameObject.Find ("L3GUI").GetComponent<L3StopIt>().enabled = true;
				break;
			case "L3StopItWithText":
				GameObject.Find ("L3GUI").GetComponent<L3StopItWithText>().enabled = true;
				break;
			case "L3TrashIt":
				GameObject.Find ("L3GUI").GetComponent<L3TrashIt>().enabled = true;
				break;
			case "L3TurnItDown":
				GameObject.Find ("L3GUI").GetComponent<L3TurnItDown>().enabled = true;
				break;
		case "L3DragAndDropBlinc":
				//GameObject.Find ("L3GUI").GetComponent<L3DragAndDropBlinc>().enabled = true;
			if ((GameObject.Find("L3DragAndDropBlinc") == null)) {
					//GameObject Minigame = Instantiate (MiniGame_L3DragAndDropBlinc, new Vector3 (0f, 0f, 0f), Quaternion.identity) as GameObject;
					GameObject Minigame = Instantiate (Resources.Load("L3DragAndDropBlinc", typeof(GameObject))) as GameObject;
					Minigame.name = MiniGame_L3DragAndDropBlinc.name;
				}
				break;
			case "L3DragDropWords5":
				GameObject.Find ("L3GUI").GetComponent<L3DragDropWords5>().enabled = true;
				break;
			case "L3NoteBook":
				GameObject.Find ("L3GUI").GetComponent<L3NoteBook>().enabled = true;
				break;
			//------------------------------------------Level 4 scripts-------------------------------------------
			case "L4TuiTerminateScene":
				GameObject.Find ("L4GUI").GetComponent<L4TuiTerminateScene>().enabled = true;
				break;
		    case "L4ShieldAgaDepression":
				GameObject.Find ("L4GUI").GetComponent<L4ShieldAgaDepression>().enabled = true;
				break;
			case "L4DragDropWords":
				GameObject.Find ("L4GUI").GetComponent<L4DragDropWords>().enabled = true;
				break;
			case "L4DragDropWords2":
				GameObject.Find ("L4GUI").GetComponent<L4DragDropWords2>().enabled = true;
				break;
			case "L4NoteBook":
				GameObject.Find ("L4GUI").GetComponent<L4NoteBook>().enabled = true;
				break;
			case "L4EnterTexts":
				GameObject.Find("L4GUI").GetComponent<L4EnterTexts>().enabled = true;
				break;
			//------------------------------------------Level 5 scripts-------------------------------------------
			case "L5ShieldAgaDepression":
				GameObject.Find ("L5GUI").GetComponent<L5ShieldAgaDepression>().enabled = true;
				break;
			case "L5TuiTerminateScene":
				GameObject.Find ("L5GUI").GetComponent<L5TuiTerminateScene>().enabled = true;
				break;
			case "L5DragDropWords":
				GameObject.Find ("L5GUI").GetComponent<L5DragDropWords>().enabled = true;
				break;
			case "L5DragDropWords2":
				GameObject.Find ("L5GUI").GetComponent<L5DragDropWords2>().enabled = true;
				break;
			case "L5DragDropShield":
				GameObject.Find ("L5GUI").GetComponent<L5DragDropShield>().enabled = true;
				break;
			case "L5EnterTexts":
				GameObject.Find ("L5GUI").GetComponent<L5EnterTexts>().enabled = true;
				break;
			case "L5NoteBook":
				GameObject.Find ("L5GUI").GetComponent<L5NoteBook>().enabled = true;
				break;
			//------------------------------------------Level 6 scripts-------------------------------------------
			case "L6ShieldAgaDepression":
				GameObject.Find ("L6GUI").GetComponent<L6ShieldAgaDepression>().enabled = true;
				break;
			case "L6TuiWomanBridgelandSwitch":
				GameObject.Find ("L6GUI").GetComponent<L6TuiWomanBridgelandSwitch>().enabled = false;
				GameObject.Find ("L6GUI").GetComponent<L6TuiWomanBridgelandSwitch>().enabled = true;
				break;
			case "L6TuiTempleGuardianSwitch":
				GameObject.Find ("L6GUI").GetComponent<L6TuiTempleGuardianSwitch>().enabled = false;
				GameObject.Find ("L6GUI").GetComponent<L6TuiTempleGuardianSwitch>().enabled = true;
				break;
			case "L6TuiTerminateScene":
				GameObject.Find ("L6GUI").GetComponent<L6TuiTerminateScene>().enabled = true;
				break;
			case "L6NegativeStatement":
				GameObject.Find ("L6GUI").GetComponent<L6NegativeStatement>().enabled = true;
				break;
			case "L6StopTrashTurnIt":
				GameObject.Find ("L6GUI").GetComponent<L6StopTrashTurnIt>().enabled = true;
				break;
			case "L6MatchingTextDragDrop":
				GameObject.Find ("L6GUI").GetComponent<L6MatchingTextDragDrop>().enabled = true;
				break;
			case "L6SortItNegotiate":
				GameObject.Find ("L6GUI").GetComponent<L6SortItNegotiate>().enabled = true;
				break;
			case "L6NoteBook":
				GameObject.Find ("L6GUI").GetComponent<L6NoteBook>().enabled = true;
				break;
			//------------------------------------------Level 7 scripts-------------------------------------------
			case "L7ShieldAgaDepression":
				GameObject.Find ("L7GUI").GetComponent<L7ShieldAgaDepression>().enabled = true;
				break;
			case "L7ShieldAgaDepression2":
				GameObject.Find ("L7GUI").GetComponent<L7ShieldAgaDepression2>().enabled = true;
				break;
			case "L7TuiTerminateScene":
				GameObject.Find ("L7GUI").GetComponent<L7TuiTerminateScene>().enabled = true;
				break; 
			case "L7LessonsMatch":
				GameObject.Find ("L7GUI").GetComponent<L7LessonsMatch>().enabled = true;
				break;
			case "L7TakingHome":
				GameObject.Find ("L7GUI").GetComponent<L7TakingHome>().enabled = true;
				break;
			case "L7NoteBook":
				GameObject.Find ("L7GUI").GetComponent<L7NoteBook>().enabled = true;
				break;
		}
	}
}