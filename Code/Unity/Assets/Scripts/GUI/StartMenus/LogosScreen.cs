using UnityEngine;
using System.Collections;

public class LogosScreen : MonoBehaviour {
	//Public variables
	public GUISkin m_skin            = null;
	
	//Private variables
	private bool m_bLogoFadeInStart   = true;
	private float m_fFadeInStartTime  = 0.0f;
	private float m_fLogoStayTime     = 0.0f;
	private bool m_bLogoFadeOutStart  = true;
	private float m_fFadeOutTime      = 0.0f;
	private float m_fFadeInLogoAlpha  = 0.0f;
	private float m_fFadeOutLogoAlpha = 1.0f;
	private bool m_bScriptIsKilled    = false;

	private CapturedDataInput instance;

	/*
	*
	* Start function
	*
	*/
	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	/*
	*
	* Update function
	*
	*/
	public void Update () {
		
	}
	
	public void OnEnable(){
		m_fFadeInStartTime = Time.time;
	}
	
	/*
	*
	* Fade In Wonders Logo
	* Display the fist splash screen image
	*/
	public void OnGUI(){
		if(m_bScriptIsKilled == false){
			GUI.depth = -2;
			GUI.skin = m_skin;
		
			if(m_bLogoFadeInStart){
				m_fFadeInLogoAlpha = Mathf.Lerp(0.0f, 1.0f, (Time.time - m_fFadeInStartTime) * 0.3f);
				GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInLogoAlpha);
			}
			
			if(m_fFadeInLogoAlpha == 1.0f && m_bLogoFadeInStart){
				m_fLogoStayTime = Time.time;
				m_bLogoFadeInStart = false;
			}
			//Keep the logo stay on the screen for 4 secs
			if(Time.time - m_fLogoStayTime > 2.0f && m_bLogoFadeOutStart){
				m_fFadeOutTime = Time.time;
				m_bLogoFadeOutStart = false;
			}
			
			if(!m_bLogoFadeOutStart){
				m_fFadeOutLogoAlpha = Mathf.Lerp(1.0f, 0.0f, (Time.time - m_fFadeOutTime) * 0.3f);
				GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeOutLogoAlpha);
			}
			
			GUI.Label(new Rect(0,0,Screen.width,Screen.height), "", "SplashScreen1");
			
			if(m_fFadeOutLogoAlpha == 0.0f){
				
				if((!instance.bDynamicURL_ATTEMPTING && !instance.bGotDynamicURL_RESPONSE_NULL)|| instance.bGotDynamicURL_RESPONSE_NULL){
					this.enabled = false;
					m_bScriptIsKilled = true;
					//Enable the next sparx logo screen
					GetComponent<SparxLogoScreen> ().enabled = true;
				} else {
					m_bLogoFadeInStart   = true;
					m_fFadeInStartTime  = 0.0f;
					m_fLogoStayTime     = 0.0f;
					m_bLogoFadeOutStart  = true;
					m_fFadeOutTime      = 0.0f;
					m_fFadeInLogoAlpha  = 0.0f;
					m_fFadeOutLogoAlpha = 1.0f;
					m_bScriptIsKilled    = false;
				}

			}
		}
	}
}
