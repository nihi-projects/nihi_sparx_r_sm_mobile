using UnityEngine;
using System;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;
public class LogoBackgroundScreen : MonoBehaviour {

    //Public variables

    //Private variables
    public GUISkin m_skin = null;
    public bool m_bUploadingFile = false;
    public bool m_bGotCheckingFile = false;
    private bool m_bGotLoginTokenDone = false;
    private bool m_bGotDynamicTokenDone = false;
    private bool m_bGotDynamicURLDone = false;
    private bool m_bGotLoadAvatarDone = false;
    private bool m_bHasLevelNumber = false;

    private bool m_bGotCharacter = false;
    private bool m_bGotProgress = false;
    private bool m_bGotNotebook = false;
    private bool m_bGotSavingPoint = false;
    private bool m_bSendEventStart = false;

    private bool m_DObWaiting4Secconds = false;
    private bool m_bWaiting4Secconds = false;
    private bool m_bWaiting4SeccondsDone = false;

    public static int levelSelectOverride = -1;

    public bool m_bReLogin = false;
    public bool m_bWaitingForReLoginServerDone = false;
    private bool m_bGotConfigURLDone = false;

    public static bool m_levelOverride = false;
    public static string m_savePointOverride = "";

    // Use this for initialization
    private CapturedDataInput instance;

    private TokenHolderLegacy instanceTHL;


    public void Start() {
        SetAudioListener();

        //m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");

        //Connecting to the server
        instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
        instanceTHL = GameObject.Find("TokenHolder").GetComponent<TokenHolderLegacy>();
        //Get token from fixed username and password
        //if(Application.isEditor || Application.isMobilePlatform){

        if (Application.internetReachability != NetworkReachability.NotReachable) {
            //StartCoroutine (instance.GetUserLogInToServer_NewSchema ()); //TEST LOGIN SCREEN 07/09/2016
        }
        //}
        //StartCoroutine(instance.GetUserLogInToServer());
        //Get token from configURL
#if UNITY_WEBGL
    	// Call the javascript method in HTML page (then the javascript will call our setConfig() with thew config url)
	    Application.ExternalCall("unityGameLoaded", "GUI");
#endif
        try {
            m_skin.label.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        } catch (Exception e) {
            Debug.Log("ERROR - something about textColor");
        }

        FontSizeManager.checkFontSize(m_skin);

        //		Debug.Log ("Character_PLAYERPREFS:" + PlayerPrefs.GetString ("character_playerprefs"));
        //		Debug.Log ("UserResponse_PLAYERPREFS:" + PlayerPrefs.GetString ("userresponse_playerprefs"));
        //Debug.Log ("savingpoint_PLAYERPREFS:" + PlayerPrefs.GetString ("userresponse_playerprefs"));

    }

    public void ResetMemberVariables()
    {
        m_bGotDynamicTokenDone = false;
        m_bGotDynamicURLDone = false;
        m_bGotLoadAvatarDone = false;
        m_bHasLevelNumber = false;

        m_bGotCharacter = false;
        m_bGotProgress = false;
        m_bGotNotebook = false;
        m_bGotSavingPoint = false;
        m_bSendEventStart = false;

        m_DObWaiting4Secconds = false;
        m_bWaiting4Secconds = false;
        m_bWaiting4SeccondsDone = false;
    }
	
	// Update is called once per frame
	public void Update ()
	{
		//turn off the start menus if the player got the gem, only when the guide scene loads again
		if (Global.LevelSVisited && Application.loadedLevelName == "GuideScene") {
			this.enabled = false;
			GetComponent<LogosScreen> ().enabled = false;
			GetComponent<TalkScenes> ().enabled = true;
		}

        //STATIC URL

        if ((Application.internetReachability != NetworkReachability.NotReachable) && !instance.bNoDynamicURL_AFTERATTEMPTS)
        {
            //ONLY IN CASE OF RELOGIN
            if (m_bReLogin || instance.m_ProcessLoginAPIDrupal == null)
            {
                StartCoroutine(instance.LoginProcess_NewJsonSchema(instanceTHL.username, instanceTHL.password));
                m_bReLogin = false;
                m_bWaitingForReLoginServerDone = true;
            }

            if (m_bWaitingForReLoginServerDone && !m_bGotConfigURLDone && instance.m_ProcessLoginAPIDrupal.isDone)
            {
                instance.GetTokenAfterLoginDrupalAPI_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
                m_bGotConfigURLDone = true;
                m_bWaitingForReLoginServerDone = false;
            }
            //ONLY IN CASE OF RELOGIN

            // request the dynamicURL....
            if (!string.IsNullOrEmpty(instance.m_sGameConfigServerURL) && (instance.m_ProcessLoginAPIDrupal==null || instance.m_ProcessLoginAPIDrupal.isDone) && !m_bReLogin && !m_bWaitingForReLoginServerDone)
            {
                if (!m_bGotDynamicTokenDone)
                {
                    // Request the gameConfigURL in order to get dynamic URL (serviceURL)
                    StartCoroutine(instance.SendOffGameConfigURL_NewJsonSchema(instance.m_sGameConfigServerURL));
                    m_bGotDynamicTokenDone = true;
                    Debug.Log("<Token & Dynamic URL from Server>");
                }
            }


            // ... now parse the response to get the dynamicURL
            if (m_bGotDynamicTokenDone && instance.m_dynamiURLRetriever != null && instance.m_dynamiURLRetriever.isDone && !m_bGotDynamicURLDone)
            {
                instance.GetTokenAndDynamicURL_NewJsonSchema();
                m_bGotDynamicURLDone = true;
                Debug.Log("<Token & Dynamic URL from JSON>");
            }

            /*if (instance.attemps < 5) {
                instance.m_sDynamicURL = "";
            }*/

            // if we didn'[t get the dynamic URL, retry a few times
            if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone && (string.IsNullOrEmpty(instance.m_sDynamicURL)))
            {
                Debug.Log("Dynamic URL NULL:" + instance.m_sDynamicURL);
                instance.attemps++;
                if (instance.attemps < 5)
                {
                    instance.bDynamicURL_ATTEMPTING = true;
                    ResetMemberVariables();
                    m_bReLogin = true;
                }
                else
                {
                    instance.bDynamicURL_ATTEMPTING = false;
                    instance.bGotDynamicURL_RESPONSE_NULL = true;
                    instance.bNoDynamicURL_AFTERATTEMPTS = true;
                }
            }

            if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone && (!string.IsNullOrEmpty(instance.m_sDynamicURL)))
            {
                if (instance.attemps < 5)
                {
                    instance.bDynamicURL_ATTEMPTING = false;
                    instance.bGotDynamicURL_RESPONSE_NULL = false;
                }
            }

            if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone)
            {
                // upload any previously-failed data (eg connection went down last session,
                // so progress was saved locally)
                if (!m_bGotCheckingFile && !m_bUploadingFile)
                {
                    if (File.Exists(Application.persistentDataPath + instance.failedFileName))
                    {
                        Debug.Log("cGoingtoUploadScript");
                        m_bUploadingFile = true;
                        StartCoroutine(instance.startFileUpload());
                    }
                }
                if (m_bUploadingFile)
                {
                    if (instance.bUploadFileComplete = true)
                    {
                        m_bGotCheckingFile = true;
                        m_bUploadingFile = false;
                        m_DObWaiting4Secconds = true;
                    }
                }
                else
                {
                    m_bGotCheckingFile = true;
                    m_bUploadingFile = false;
                    if (!m_DObWaiting4Secconds)
                    {
                        m_bWaiting4SeccondsDone = true;
                    }
                }
            }

            //Override current level here.
            if (levelSelectOverride != -1 && m_levelOverride == true)
            {
                m_levelOverride = false;
                Global.CurrentLevelNumber = levelSelectOverride;
                instance.SaveToServer(false, false, m_savePointOverride, false);
            }

            // pause for a few seconds before hitting the server again...
            if (m_DObWaiting4Secconds)
            {
                if (!m_bWaiting4SeccondsDone)
                {
                    if (!m_bWaiting4Secconds)
                    {
                        instance.bWaitingForSeconds = true;
                        StartCoroutine(instance.Wait(10));
                        m_bWaiting4Secconds = true;
                    }

                    if (instance.bWaitingForSeconds)
                    {
                        Debug.Log("WAITING....");
                    }

                    if (m_bWaiting4Secconds && !instance.bWaitingForSeconds)
                    {
                        Debug.Log("STOP WAITING");
                        m_bWaiting4Secconds = false;
                        m_bWaiting4SeccondsDone = true;
                    }
                }
            }

            //WAIT FOR SECONDS ROUTINE --->
            // CHANGE THE SAVE POINT TEST PORPOUSES  
            /*if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone) {
                StartCoroutine (instance.savePointToServer_NewJsonSchema (2, "[0,0,0]", "LevelsSP2", false, false));
            }*/

            //"LevelsSP2"
            //"levelEnd"
            //"L2Game"
            //"L2GameMid1"
            //"L2GameMid2"
            // CHANGE THE SAVE POINT TEST PURPOSES


            // ftech userleveldata from server....
            if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone && m_bGotCheckingFile && m_bWaiting4SeccondsDone && !instance.bDynamicURL_ATTEMPTING)
            {//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
             //StartCoroutine (instance.SavePointEventToServer_NewJsonSchema(5,1,"levelEnd"));
                StartCoroutine(instance.ReadUserLevelDataFromServer_NewJsonSchema(Global.CurrentLevelNumber, false));  //CHANGES_KEY_NEW_JSON_SCHEMA INSERT
                Debug.Log("<Character from Server>");
                m_bGotLoadAvatarDone = true;
            }   //CHANGES_KEY_NEW_JSON_SCHEMA INSERT

            if (instance.m_sGameDynamicToken != "" && instance.m_ProcessDataGetRetriever == null && m_bGotDynamicURLDone && instance.m_LevelDataGetRetriever != null && m_bGotCheckingFile && m_bWaiting4SeccondsDone && !instance.bDynamicURL_ATTEMPTING)
            {
                if (instance.m_LevelDataGetRetriever.isDone)
                {
                    //LOAD PROGRESS, Level num and Score
                    StartCoroutine(instance.ReadUserProgressDataFromServer_NewJsonSchema(false));
                    Debug.Log("<Progress from Server>");
                }
            }

            if(instance.m_ProcessDataGetRetriever != null)
                Debug.Log("Con1: " + (instance.m_ProcessDataGetRetriever != null).ToString() + 
                        " Con 2: " + m_bGotDynamicURLDone.ToString() + 
                        " Con 3: " + (instance.m_ProcessDataGetRetriever.isDone).ToString() + 
                        " Con 4: " + m_bGotCheckingFile.ToString() + 
                        " Con 5: " + m_bWaiting4SeccondsDone.ToString() + 
                        " Con 6: " + (!instance.bDynamicURL_ATTEMPTING).ToString() );
            if (instance.m_ProcessDataGetRetriever != null && m_bGotDynamicURLDone && instance.m_ProcessDataGetRetriever.isDone && m_bGotCheckingFile && m_bWaiting4SeccondsDone && !instance.bDynamicURL_ATTEMPTING)
            {
                if (Global.CurrentLevelNumber > 1)
                {
                    //LOAD NOTEBOOK CONTENT IN GAME
                    if (instance.m_ProcessNotebookDataGetRetriever == null)
                    {
                        StartCoroutine(instance.ReadUserProgressNoteBookDataFromServer_NewJsonSchema(Global.CurrentLevelNumber, false));
                        Debug.Log("<Notebook from Server>");
                    }
                }
            }
            //save point loading call
            if (instance.m_sLoadURLReturn == null && instance.m_sGameDynamicToken != "" && m_bGotDynamicURLDone && instance.m_ProcessDataGetRetriever != null && m_bGotCheckingFile && m_bWaiting4SeccondsDone && !instance.bDynamicURL_ATTEMPTING)
            {
                if (instance.m_ProcessDataGetRetriever.isDone)
                {
                    Debug.Log("Call - loadSavePointFromServer");
                    StartCoroutine(instance.loadSavePointFromServer_NewJsonSchema(false));
                    Debug.Log("<Saving point>");
                    m_bGotLoadAvatarDone = true;
                    m_bGotSavingPoint = true;
                }
            }

            /*} else {
                Debug.Log ("<<<<<<<<<<iPAD>>>>>>>>>> m_logInDataRetriever SOLVED BUT ERROR");
            }*/
            //}
        }
        else
        {
            //NO CONNECTION AVAILABLE
            Debug.Log("<<<<<<<<<<iPAD>>>>>>>>>> NO CONNECTION");

            if (!m_bGotCharacter)
            {
                Debug.Log("<<<<<<<<<<iPAD>>>>>>>>>> m_bGotCharacter");
                StartCoroutine(instance.ReadUserLevelDataFromServer_NewJsonSchema(Global.CurrentLevelNumber, true));//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
                Debug.Log("<Character from Server>");
                m_bGotCharacter = true;
            }
            if (m_bGotCharacter && !m_bGotProgress)
            {
                Debug.Log("<<<<<<<<<<iPAD>>>>>>>>>> m_bGotProgress");
                //LOAD PROGRESS, Level num and Score
                StartCoroutine(instance.ReadUserProgressDataFromServer_NewJsonSchema(true));
                Debug.Log("<Progress from Server>");
                m_bGotProgress = true;
            }
            if (m_bGotProgress && !m_bGotNotebook)
            {
                Debug.Log("<<<<<<<<<<iPAD>>>>>>>>>> m_bGotNotebook");
                if (Global.CurrentLevelNumber > 1)
                {
                    Debug.Log("<<<<<<<<<<iPAD>>>>>>>>>> Global.CurrentLevelNumber > 1");
                    //LOAD NOTEBOOK CONTENT IN GAME
                    StartCoroutine(instance.ReadUserProgressNoteBookDataFromServer_NewJsonSchema(Global.CurrentLevelNumber, true));
                    Debug.Log("<Notebook from Server>");
                }
                m_bGotNotebook = true;
            }

            if (m_bGotNotebook && !m_bGotSavingPoint)
            {
                //save point loading call
                Debug.Log("Call - loadSavePointFromServer");
                StartCoroutine(instance.loadSavePointFromServer_NewJsonSchema(true));
                Debug.Log("<Saving point>");

                m_bGotLoadAvatarDone = true;
                m_bGotSavingPoint = true;
            }
        }
		if(m_bGotSavingPoint && !m_bSendEventStart && instance.bLevelNumberLoaded){
			StartCoroutine (instance.SaveLevelStartEndEventToServer_NewJsonSchema (2, Global.CurrentLevelNumber));
			m_bSendEventStart = true;
            levelSelectOverride = -1;
		}
	}

#if UNITY_WEBGL
    // this is part of the login bypass for the webGL version
    // this is invoked from javascript in the the web browser (see unityGameLoaded() in sparx_game.js)
    public void setConfig(string _gameConfigUrl)
	{		
//		int equalSignIndex = _gameConfigUrl.IndexOf('=');
//		string token = _gameConfigUrl.Substring(equalSignIndex + 1);
//		instance.m_sGameLoginToken = token;

        instance.m_sGameConfigServerURL = _gameConfigUrl;
    }
#endif

    /*
	*
	* OnGUI Function.
	*
	*/
    public void OnGUI () 
	{
		GUI.depth = -1;
		GUI.skin = m_skin;
		// start the logo flash black background
		GUI.Box(new Rect(0,0,Screen.width,Screen.height),"","SplashScreenBackground");	
	}

	public void SetAudioListener(){
		if (PlayerPrefs.GetInt ("playerprefs_SoundOff") == 0) {
			AudioListener.pause = false;
		} else {
			AudioListener.pause = true;
		}
	}

	IEnumerator restartApplication(bool bDynamicUrlISNULL){
		bool running = true;
		while (running) {
			yield return this;
			if (Application.internetReachability != NetworkReachability.NotReachable || bDynamicUrlISNULL) {
				if ((instance.m_sSaveURLReturn != null && instance.m_sSaveURLReturn.isDone) || bDynamicUrlISNULL) {
					running = false;
					instance.unLoadAllContent ();
					Destroy (instance);
					Destroy (GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad> ());
					Global.resetAllValues ();
					for (int i = 0; i < 2; i++) {
						//destroy both instances of all that probably exist here
						string[] objectsToDestroy = new string[] {
							"Global",
							"GUI Text",
							"GUI",
							"PlayerInfoLoad",
							"ScriptHolder",
							"CapturedDataInputHolder"
						};
						foreach (string s in objectsToDestroy) {
							GameObject anObject = GameObject.Find (s);
							Destroy (anObject);
						}

					}

					Application.LoadLevel ("GuideScene");

				}
			} else {
				running = false;
				instance.unLoadAllContent ();
				Destroy (instance);
				Destroy (GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad> ());
				Global.resetAllValues ();
				for (int i = 0; i < 2; i++) {
					//destroy both instances of all that probably exist here
					string[] objectsToDestroy = new string[] {
						"Global",
						"GUI Text",
						"GUI",
						"PlayerInfoLoad",
						"ScriptHolder",
						"CapturedDataInputHolder"
					};
					foreach (string s in objectsToDestroy) {
						GameObject anObject = GameObject.Find (s);
						Destroy (anObject);
					}

				}

				Application.LoadLevel ("GuideScene");

			}
		}
	}
}
