﻿using UnityEngine;
using System.Collections;

public class Settings : MonoBehaviour {

	public bool m_bShow = true;

	// publics
	public GUISkin m_skin = null;

	public Texture2D m_tBag;
	public Texture2D[] m_tItems;

	// privates
	private float m_fMoveSpeed = 2.0f;

	static private float m_fWidth = 70.0f * Global.ScreenWidth_Factor;
	static private float m_fHeight = 70.0f * Global.ScreenHeight_Factor;
	private float m_fTimer = 0.0f;

	private Rect m_rBag = new Rect(Screen.height * 0.01f, Screen.height * 0.01f, m_fWidth, m_fHeight);
	private Rect[] m_rItemRects;

	private Vector2[] m_vFinalPos;

	private bool m_bToggleShowItems = false;
	private bool m_bMove = false;
	private bool m_bNotebookUp = false;

	private GameObject m_ObjectMobileController;

	Color bridgeColor;
	Color bridgeBackColor;

	private CapturedDataInput instance;

	// Use this for initialization
	void Start () 
	{

		FontSizeManager.checkFontSize(m_skin);

        if(GameObject.Find("CapturedDataInputHolder"))
            instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		m_ObjectMobileController = GameObject.Find ("MobileSingleStickControl");

		m_rItemRects = new Rect[m_tItems.Length];
		m_vFinalPos = new Vector2[m_tItems.Length];
		float x = 0.0f;
		for (int i = 0; i < m_vFinalPos.Length; ++i)
		{
			x = (m_fWidth + 25.0f) * (i+1) + 25.0f;
			m_vFinalPos[i] = new Vector2(x, Screen.height * 0.9f);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (!m_bShow) { return ;}
		if (!CheckPlayingAutoscene ()) {
			CheckBagClick ();
		}
		if (m_bMove)
		{

			//m_ObjectMobileController.SetActive (false);
			//m_ObjectMobileController.SetActive (true);

			GameObject.Find("GUI").GetComponent<SettingsScreen>().enabled = true; 
			m_bMove = false;
			/*if (true == m_bToggleShowItems)
			{
				GameObject.Find("GUI").GetComponent<SettingsScreen>().enabled = true; 
				//ShowItems();
			}
			else 
			{
				
				GameObject.Find("GUI").GetComponent<SettingsScreen>().enabled = true; 
				//HideItems();
			}*/
		}
	}

	void OnGUI()
	{

        //GUI.Label(new Rect(100, 0, 700, 100), "<" + Global.debugControls[0] + "> <" + Global.debugControls[1] + "> <" + Global.debugControls[2] + "><" + Global.debugControls[3] + "> <" + Global.debugControls[4] + "> <" + Global.debugControls[5] + ">" + "> <" + Global.debugControls[6] + "> <" + Global.debugControls[7] + "><" + Global.debugControls[8] + "> <" + Global.debugControls[9] + ">");

        if (!m_bShow) { return ;}

		if (Global.Global_bshowVersionOnScreen){
			GUI.Label(new Rect(0, 0, 500, 50), Global.versionNumber + "<W:" + Screen.width.ToString() + "H:" + Screen.height + ">");
		}

		if (instance != null && instance.bGotDynamicURL_RESPONSE_NULL) {
			GUI.Label(new Rect(100, 0, 600, 50), "This Sparx session is not connected to the server. Please restart the app to store your game progress");
		}

		GUI.depth = -1;
		GUI.skin = m_skin;
		if (!CheckPlayingAutoscene ()) {
			GUI.Label (m_rBag, m_tBag);
		}
		bridgeColor = GUI.color;
		bridgeBackColor = GUI.backgroundColor;
		GUI.color = Color.grey;
		GUI.backgroundColor = Color.clear;
		//Debug.Log (Global.m_sSavePointCode);
		if (Global.CurrentLevelNumber == 1 && (Global.m_sSavePointCode != "LevelsSP1" && Global.m_sSavePointCode != "L1Game" && Global.m_sSavePointCode != "L1GameMid1" && Global.m_sSavePointCode != "L1GameMid2" && Global.m_sSavePointCode != "LevelsSP2")) {
			if (GUI.Button (new Rect (95 , 0 * Global.ScreenHeight_Factor, 435* Global.ScreenWidth_Factor, 50* Global.ScreenHeight_Factor), "Info and help www.sparx.org.nz"))
				Application.OpenURL ("https://sparx.org.nz");
		
			if (GUI.Button (new Rect (95, 20 * Global.ScreenHeight_Factor , 435 * Global.ScreenWidth_Factor, 50 * Global.ScreenHeight_Factor), "Feeling bad? 0508 477 279 or free txt SPARX to 234"))
				Application.OpenURL ("https://sparx.org.nz");
		
			if (GUI.Button (new Rect (95 , 40 * Global.ScreenHeight_Factor, 435 * Global.ScreenWidth_Factor, 50 * Global.ScreenHeight_Factor), "Turn off notifications ‘untick’ under ‘My Profile’ www.sparx.org.nz"))
				Application.OpenURL ("https://sparx.org.nz");
		}
		GUI.color = bridgeColor;
		GUI.backgroundColor = bridgeBackColor;
		/*if (m_bMove || m_bToggleShowItems)
		{
			for (int i = 1; i < m_rItemRects.Length; ++i)
			{
				if (Global.CurrentLevelNumber == 2 && i > 0)
				{
					break;
				}
				GUI.Label(m_rItemRects[i], m_tItems[i]);
			}

			GameObject guiObject = GameObject.Find("GUI");
			if ( GUI.Button(m_rItemRects[0], m_tItems[0]))
			{
				bool closing = false;
				if (guiObject.GetComponent<SavedL1NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL1NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL2NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL2NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL3NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL3NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL4NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL4NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL5NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL5NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL6NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL6NoteBook>().enabled = false;
				}

				if (!closing)
				{
					// Here is the code for loading the saved notebooks
					// Always starts opening the first notebook
					//Cannot view the notebook for the first level
					guiObject.GetComponent<SavedL1NoteBook>().enabled = true;
					m_bNotebookUp = true;
				}
				else 
				{
					m_bNotebookUp = false;
				}
			}
			else if (!m_bNotebookUp && GUI.Button(m_rItemRects[1], ""))
			{
				guiObject.transform.FindChild("L1GUI").gameObject.SetActive(true);
				guiObject.transform.FindChild("L1GUI").GetComponent<ShieldAgaDepression>().enabled = true; 
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;

				m_fTimer = 0.0f;

				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
		}*/
	}

	bool CheckPlayingAutoscene()
    {
        if(GameObject.Find("GUI"))
        {
            if (GameObject.Find("GUI").GetComponent<TalkScenes>().enabled)
            {
                if (GameObject.Find("GUI").GetComponent<TalkScenes>().IsAutoTalkScene(GameObject.Find("GUI").GetComponent<TalkScenes>().GetCurrentScene()))
                    return true;
            }
        }
        return false;
    }


	void CheckBagClick()
	{
		if (!m_bMove && Input.GetMouseButtonUp(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;

			if (m_rBag.Contains(new Vector2(mouseX, Screen.height - mouseY)))
			{
				//m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;

				m_fTimer = 0.0f;

				//for (int i = 0; i < m_rItemRects.Length; ++i)
				//{
				//	m_rItemRects[i] = m_rBag;
				//}
			}
		}
	}

	void ShowItems()
	{
		m_fTimer += Time.deltaTime * m_fMoveSpeed;

		if (m_fTimer <= 1.0f)
		{
			for (int i = 0; i < m_vFinalPos.Length; ++i)
			{
				Vector2 temp = Vector2.Lerp(new Vector2(m_rBag.x, m_rBag.y), m_vFinalPos[i], m_fTimer);
				m_rItemRects[i].x = temp.x;
				m_rItemRects[i].y = temp.y;
			}
		}
		else
		{
			m_fTimer = 0.0f;
			m_bMove = false;
		}
	}

	void HideItems()
	{
		m_fTimer += Time.deltaTime * m_fMoveSpeed;

		if (m_fTimer <= 1.0f)
		{
			for (int i = 0; i < m_vFinalPos.Length; ++i)
			{
				Vector2 temp = Vector2.Lerp(m_vFinalPos[i], new Vector2(m_rBag.x, m_rBag.y), m_fTimer);
				m_rItemRects[i].x = temp.x;
				m_rItemRects[i].y = temp.y;
			}
		}
		else
		{
			m_fTimer = 0.0f;
			m_bMove = false;
		}
	}
}
