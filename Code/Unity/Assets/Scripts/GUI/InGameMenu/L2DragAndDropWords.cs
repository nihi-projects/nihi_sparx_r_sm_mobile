using UnityEngine;
using System.Collections;

public class L2DragAndDropWords : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;
	
	// privates
	private string m_conversationBoxText = "";
	
	static private float m_keywordWidth = 100.0f;
	static private float m_keywordHeight = 30.0f;
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(45.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // walk
		new Vector2(120.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // family/cousins
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // sports
		new Vector2(60.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // WebSite
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Relax
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Music
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Movie
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Book
		new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Bike-ride
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Invite
		new Vector2(45.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Shop
		new Vector2(85.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Bake/Cook
		new Vector2(65.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Haircut
		new Vector2(45.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Visit
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Create
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Decorate
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Beach
		new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Magazine
	};
	
	//Record the original keyword position
	//private Rect m_myOriRect1 = new Rect(130.0f, 100.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect2 = new Rect(100.0f, 160.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect3 = new Rect(160.0f, 220.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect4 = new Rect(200.0f, 280.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect5 = new Rect(430.0f, 280.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect6 = new Rect(500.0f, 260.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect7 = new Rect(590.0f, 200.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect8 = new Rect(550.0f, 90.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect9 = new Rect(400.0f, 40.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myOriRect10 = new Rect(250.0f, 40.0f, m_keywordWidth, m_keywordHeight);
	private Rect[] m_myOriRect; 
	
	//private string m_keyWordText1 = "Joylessness";
	//private string m_keyWordText2 = "Moodiness";
	//private string m_keyWordText3 = "Anger";
	//private string m_keyWordText4 = "Loneliness";
	//private string m_keyWordText5 = "Aches";
	//private string m_keyWordText6 = "Tiredness";
	//private string m_keyWordText7 = "Hopelessness";
	//private string m_keyWordText8 = "Hunger";
	//private string m_keyWordText9 = "Sadness";
	//private string m_keyWordText10 = "No Energy";
	
	private string[] m_keyWordText = {"Move", "Create","Music","Read", "Watch","Go out", "Cook", "Sport",
                                        "Family/Friends", "Do up", "Freshen up", "Learn", "Talk", "Do it", "Kind"};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_dextinationRectHeight = 35.0f * Global.ScreenHeight_Factor;
	//private Rect m_destinationOriKeyWordRect1 = new Rect(240.0f, 130.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationOriKeyWordRect2 = new Rect(240.0f, 160.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationOriKeyWordRect3 = new Rect(240.0f, 190.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationOriKeyWordRect4 = new Rect(240.0f, 220.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationOriKeyWordRect5 = new Rect(240.0f, 250.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationOriKeyWordRect6 = new Rect(240.0f, 280.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	//private string m_L2destinationText1 = "";
	//private string m_L2destinationText2 = "";
	//private string m_L2destinationText3 = "";
	//private string m_L2destinationText4 = "";
	//private string m_L2destinationText5 = "";
	//private string m_L2destinationText6 = "";
	static public string[] m_L2destinationText = {"", "", "", "", "", ""};
	
	//The destination keyword background
	//private Rect m_destinationRect1 = new Rect(240.0f, 130.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationRect2 = new Rect(240.0f, 160.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationRect3 = new Rect(240.0f, 190.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationRect4 = new Rect(240.0f, 220.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationRect5 = new Rect(240.0f, 250.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	//private Rect m_destinationRect6 = new Rect(240.0f, 280.0f, m_dextinationRectWidth, m_dextinationRectHeight);
	private Rect[] m_destinationRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	//private Rect m_myTempStringCarrierRect1 = new Rect(130.0f, 100.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect2 = new Rect(100.0f, 160.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect3 = new Rect(160.0f, 220.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect4 = new Rect(200.0f, 280.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect5 = new Rect(430.0f, 280.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect6 = new Rect(500.0f, 260.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect7 = new Rect(590.0f, 200.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect8 = new Rect(550.0f, 90.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect9 = new Rect(400.0f, 40.0f, m_keywordWidth, m_keywordHeight);
	//private Rect m_myTempStringCarrierRect10 = new Rect(250.0f, 40.0f, m_keywordWidth, m_keywordHeight);
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		Global.levelStarted = false;
		 m_myOriRect = new Rect[] 
		{	
			new Rect(540.0f * Global.ScreenWidth_Factor, 435.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // walk
			new Rect(680.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // family/cousins
			new Rect(680.0f * Global.ScreenWidth_Factor, 145.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // sports
			new Rect(680.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Website
			new Rect(680.0f * Global.ScreenWidth_Factor, 255.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Relax
			new Rect(680.0f * Global.ScreenWidth_Factor, 310.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Music
			new Rect(680.0f * Global.ScreenWidth_Factor, 365.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Movie
			new Rect(680.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[7].x, m_KeywordDimensions[7].y), // Book
			new Rect(540.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[8].x, m_KeywordDimensions[8].y), // Bike-ride
			new Rect(385.0f * Global.ScreenWidth_Factor, 435.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[9].x, m_KeywordDimensions[9].y), // Invite
			new Rect(385.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[10].x, m_KeywordDimensions[10].y), // Shop
			new Rect(180.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[11].x, m_KeywordDimensions[11].y), // Bake/Cook
			new Rect(180.0f * Global.ScreenWidth_Factor, 365.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[12].x, m_KeywordDimensions[12].y), // Haircut
			new Rect(180.0f * Global.ScreenWidth_Factor, 310.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[13].x, m_KeywordDimensions[13].y), // Visit
			new Rect(180.0f * Global.ScreenWidth_Factor, 255.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[14].x, m_KeywordDimensions[14].y), // Create
			new Rect(180.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[15].x, m_KeywordDimensions[15].y), // Decorate
			new Rect(180.0f * Global.ScreenWidth_Factor, 145.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[16].x, m_KeywordDimensions[16].y), // Beach
			new Rect(180.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[17].x, m_KeywordDimensions[17].y) // Magazine
		};
		
		m_movableStringRect = new Rect[m_myOriRect.Length];
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			m_movableStringRect[i] = m_myOriRect[i];
		}
		FontSizeManager.checkFontSize(m_skin);
		m_currentClickedStringRect = k_zeroRect;

        //m_tFillFieldBackground    = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
        //m_tDestinationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
        //m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));

        
    }
	
	// Update is called once per frame
	void Update () 
	{
        if( menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("ListScreen");
            menu.GetComponent<ListScreen>().SetupListItems(m_keyWordText);
            menu.GetComponent<ListScreen>().SetTitle("Things I'm going to do:");

            string[] descriptions = new string[m_keyWordText.Length];
            for (int i = 0; i < m_keyWordText.Length; ++i)
            {
                descriptions[i] = WordsHoverOverText(m_keyWordText[i]);
            }
            menu.GetComponent<ListScreen>().SetupDescriptions(descriptions);
        }
		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText("");
		
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
			}
		}
		
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												   m_KeywordDimensions[m_currentIndex].x, 
												   m_KeywordDimensions[m_currentIndex].y);
				
				m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
			}
		}
		else
		{
			//KeyWordDrag();
		}

        m_L2destinationText = menu.GetComponent<ListScreen>().GetChosenStrings();

		//DestinationDrop();
	}
	
	public void OnGUI () 
	{
        //GUI.skin = m_skin;
        //GUI.depth = -1;
        ////GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
        //// the background of the mingame
        //GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");

        //// The middle area
        //GUI.Label(new Rect(330 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 300 * Global.ScreenWidth_Factor, 300 * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
        //GUI.Label(new Rect(345 * Global.ScreenWidth_Factor, 145 * Global.ScreenHeight_Factor, 270 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "Things I'm going to do:", "Titles");
        //bool done = false;
        //for (int i = 0; i < m_destinationRect.Length; ++i)
        //{
        //	GUI.DrawTexture(m_destinationRect[i], m_tFillFieldBackground);
        //	GUI.Label(m_destinationOriKeyWordRect[i], m_L2destinationText[i], "CenteredFont");
        //	//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
        //	//GUI.Box(m_destinationOriKeyWordRect[i], m_L2destinationText[i]);

        //	if (m_L2destinationText[i] != "")
        //	{
        //		done = true;
        //	}
        //}

        //// the circle of words
        //for (int i = 0; i < m_myOriRect.Length; ++i)
        //{
        //	//GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
        //	GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
        //}

        //// The current movable keyword box and text
        //if (m_currentClickedString != "")
        //{
        //	GUI.Label(m_currentClickedStringRect, m_currentClickedString);
        //	//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
        //}

        ////Conversation box
        //GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);
        //GUI.Label(new Rect(300.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 360.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");

        //The next button
        //if (true == done)
        //{
        if (true == menu.GetComponent<ListScreen>().bReadyToContinue == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu.gameObject);
                Global.arrowKey = "";

                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L2GuideScene18");
            }
        }
		//}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	//public void KeyWordDrag()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex != -1) { return ;}
		
	//	//If the mouse button is clicked and if the mouse position is on the keywords
	//	if(Input.GetMouseButton(0))
	//	{
	//		// save the mouse pos for ease of access
	//		float mouseX = Input.mousePosition.x;
	//		float mouseY = Input.mousePosition.y;
			
	//		// boolean to see if a box has been clicked on.
	//		bool clicked = false;
			
	//		// loop through all the original rects to see if the user has clicked on one of the boxes.
	//		for (int i = 0; i < m_myOriRect.Length; ++i)
	//		{
	//			if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// set the current active key text
	//				// check if the current text has been set
	//				if (m_currentClickedString == "")
	//				{
	//					// if the keyword does not exist that means that it is in one of the destination boxes
	//					if (m_keyWordText[i] == "")
	//					{
	//						// loop through the destination boxes to find which contains the key word
	//						for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
	//						{
	//							// set the min and max of the box
	//							Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
	//							Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
	//							// check if this destination box contains the current movable box
	//							if(m_destinationOriKeyWordRect[j].Contains(min) &&
	//								m_destinationOriKeyWordRect[j].Contains(max))
	//							{
	//								// set the destination text and remove the clicked text.
	//								m_keyWordText[i] = m_L2destinationText[j];
	//								m_L2destinationText[j] = "";
	//								break;
	//							}
	//						}
	//					}
						
	//					m_currentClickedString = m_keyWordText[i];
	//					m_keyWordText[i] = "";
	//					m_currentIndex = i;
	//					clicked = true;
	//					b_dragging = true;
	//					m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
	//					break;
	//				}
	//			}
	//		}	
			
	//		// this is an early release from the function
	//		if (clicked == true) { return ;}
	//	}
	//}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	//public void DestinationDrop()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex == -1) { return ;}
		
	//	//If the mouse button is UnClicked and if  the mouse position is on the fill field
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		b_dragging = false;
			
	//		// boolean for if the text was placed in the box.
	//		bool successful = false;
			
	//		// loop through the destination rects to see which box the word is being placed in
	//		for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
	//		{
	//			// save the mouse pos for ease of access
	//			float mouseX = Input.mousePosition.x;
	//			float mouseY = Input.mousePosition.y;
				
	//			// check if this destination box contains the current clicked keyword box
	//			if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// if the destination text is empty
	//				if(m_L2destinationText[i] == "")
	//				{
	//					// set the destination text and remove the clicked text.
	//					m_L2destinationText[i] = m_currentClickedString;
	//					m_currentClickedString = "";
	//					successful = true;
						
	//					float width = m_dextinationRectWidth - 2;
	//					float height = m_dextinationRectHeight - 2;
	//					m_movableStringRect[m_currentIndex].Set(
	//						m_destinationOriKeyWordRect[i].center.x - width/2, 
	//						m_destinationOriKeyWordRect[i].center.y - height/2, 
	//						width, 
	//						height);

	//					m_currentClickedStringRect = k_zeroRect;
	//					m_currentIndex = -1;
	//				}
	//			}
	//		}
			
	//		// another early release because everything is done already
	//		if (successful) { return ;}
			
	//		// execute this code if the user missed the destination box.
	//		ResetCurrentString();
	//	}
	//}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
		m_keyWordText[m_currentIndex] = m_currentClickedString;
		m_currentClickedStringRect = k_zeroRect;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keyWordExplaination = "";

		switch(_keyWord){
            case "Move":
                keyWordExplaination = "walk, run, bike, dance...";
				break;
			case "Create":
                keyWordExplaination = "draw, cook, sing, write a song...";
				break;
            case "Music":
                keyWordExplaination = "Listen to music.";
				break;
            case "Read":
                keyWordExplaination = "Read a book.";
				break;
            case "Watch":
                keyWordExplaination = "watch a movie.";
				break;
            case "Go out":
                keyWordExplaination = "beach, park, library, gym..";
				break;
            case "Cook":
                keyWordExplaination = "Make something yummy.";
				break;
            case "Sport":
                keyWordExplaination = "Play a sport.";
				break;
            case "Family/Friends":
                keyWordExplaination = "Spend time together.";
				break;
            case "Do up":
                keyWordExplaination = "Do up or tidy up my space.";
				break;
            case "Freshen up":
                keyWordExplaination = "shower or make yourself look good.";
				break;
            case "Learn":
                keyWordExplaination = "Learn something new or practice a skill.";
				break;
            case "Talk":
                keyWordExplaination = "Talk to someone.";
				break;
            case "Do it":
                keyWordExplaination = "Finish something you have been putting off.";
				break;
			case "Kind":
                keyWordExplaination = "Do something kind.";
				break;
			default:
			    keyWordExplaination = "Doing things makes you feel better.\nDrag and drop the activities you are going to try.\nThese will be saved in your Notebook.";
				break;
		}
			
		return keyWordExplaination;
	}
}
