using UnityEngine;
using System.Collections;

public class L4DragDropWords2 : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string m_conversationBoxText = "";
	
	static private float m_keywordWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_keywordHeight = 35.0f * Global.ScreenHeight_Factor;
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(120.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Fighting at home
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Getting bullied
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Being left out
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Too much work
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Being ignored
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Being grounded
		new Vector2(90.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Bad exams
	};
	
	private Rect[] m_myOriRect;

    private string[] m_keyWordText = {"I am really stressed",
    "I had a fight ",
        "My family don’t trust me",
        "I’m bored a lot of the time",
        "I have too much work to do right now",
        "I keep getting into trouble",
        "Someone is talking behind my back",
        "Being ignored",
        "Being left out"};

	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_dextinationRectHeight = 35.0f * Global.ScreenHeight_Factor;
	
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};

	static public string[] m_L4destinationText = {"", "", "", "", "", ""};

	private Rect[] m_destinationRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		 m_myOriRect = new Rect[] 
		{	new Rect(680.0f * Global.ScreenWidth_Factor, 390.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // Fighting at home
			new Rect(180.0f * Global.ScreenWidth_Factor, 165.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // Getting bullied
			new Rect(680.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // Being left out
			new Rect(180.0f * Global.ScreenWidth_Factor, 255.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Too much work
			new Rect(680.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Being ignored
			new Rect(180.0f * Global.ScreenWidth_Factor, 345.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Being grounded
			new Rect(680.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Bad exams
		};
		
		m_movableStringRect = new Rect[m_myOriRect.Length];
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			m_movableStringRect[i] = m_myOriRect[i];
		}
		
		m_currentClickedStringRect = k_zeroRect;
		
		//m_tFillFieldBackground    = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
		//m_tDestinationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}

    private void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("ListScreen");
        menu.GetComponent<ListScreen>().SetupListItems(m_keyWordText);

        string[] descriptions = new string[m_keyWordText.Length];
        for( int i = 0; i < descriptions.Length; ++i )
        {
            descriptions[i] = WordsHoverOverText(m_keyWordText[i]);
        }
        menu.GetComponent<ListScreen>().SetupDescriptions(descriptions);
    }

    // Update is called once per frame
    void Update () 
	{

		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText("");
		
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
			}
		}
		
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												   m_KeywordDimensions[m_currentIndex].x, 
												   m_KeywordDimensions[m_currentIndex].y);
				
				m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
			}
		}
		//else
		//{
		//	KeyWordDrag();
		//}
		
		//DestinationDrop();

        //The next button
        if (menu.GetComponent<ListScreen>().GetChosenStrings().Length > 0 )
        {
            m_L4destinationText = menu.GetComponent<ListScreen>().GetChosenStrings();
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";

                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L4GuideScene25");
            }
        }
    }
	
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//// the background of the mingame
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//// The middle area
		//GUI.Label(new Rect(330 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 300 * Global.ScreenWidth_Factor, 300 * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
		//GUI.Label(new Rect(345 * Global.ScreenWidth_Factor, 145 * Global.ScreenHeight_Factor, 270 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "The problem I want to solve is:", "Titles");
		//bool done = false;
		//for (int i = 0; i < m_destinationRect.Length; ++i)
		//{
		//	GUI.Label(m_destinationRect[i], m_tFillFieldBackground);
		//	GUI.Label(m_destinationOriKeyWordRect[i], m_L4destinationText[i], "CenteredFont");
		//	//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
		//	//GUI.Box(m_destinationOriKeyWordRect[i], m_L4destinationText[i]);

		//	Debug.Log ("111111111111 TEXT HERE 111111111111");
		//	Debug.Log (m_L4destinationText[i]);
			
		//	if (m_L4destinationText[i] != "")
		//	{
		//		Global.userL4NoteBookDataBlockOne[0] = m_L4destinationText[i];
		//		Debug.Log (Global.userL4NoteBookDataBlockOne[0]);
		//		done = true;
		//	}
		//}
		
		//// the circle of words
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	//GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//	GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//}
		
		//// The current movable keyword box and text
		//if (m_currentClickedString != "")
		//{
		//	GUI.Label(m_currentClickedStringRect, m_currentClickedString, "CenteredFont");
		//	//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
		//}
		
		////Conversation box
		//GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);

		//GUI.Label(new Rect(300.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 360.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");
		
		
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	//public void KeyWordDrag()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex != -1) { return ;}
		
	//	//If the mouse button is clicked and if the mouse position is on the keywords
	//	if(Input.GetMouseButton(0))
	//	{
	//		// save the mouse pos for ease of access
	//		float mouseX = Input.mousePosition.x;
	//		float mouseY = Input.mousePosition.y;
			
	//		// boolean to see if a box has been clicked on.
	//		bool clicked = false;
			
	//		// loop through all the original rects to see if the user has clicked on one of the boxes.
	//		for (int i = 0; i < m_myOriRect.Length; ++i)
	//		{
	//			if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// set the current active key text
	//				// check if the current text has been set
	//				if (m_currentClickedString == "")
	//				{
	//					// if the keyword does not exist that means that it is in one of the destination boxes
	//					if (m_keyWordText[i] == "")
	//					{
	//						// loop through the destination boxes to find which contains the key word
	//						for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
	//						{
	//							// set the min and max of the box
	//							Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
	//							Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
	//							// check if this destination box contains the current movable box
	//							if(m_destinationOriKeyWordRect[j].Contains(min) &&
	//								m_destinationOriKeyWordRect[j].Contains(max))
	//							{
	//								// set the destination text and remove the clicked text.
	//								m_keyWordText[i] = m_L4destinationText[j];
	//								m_L4destinationText[j] = "";
	//								break;
	//							}
	//						}
	//					}
						
	//					m_currentClickedString = m_keyWordText[i];
	//					m_keyWordText[i] = "";
	//					m_currentIndex = i;
	//					clicked = true;
	//					b_dragging = true;
	//					m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
	//					break;
	//				}
	//			}
	//		}	
			
	//		// this is an early release from the function
	//		if (clicked == true) { return ;}
	//	}
	//}
	
	///*
	// * 
	// * Thie function drop the keyword to the destination area.
	// * 
	//*/
	//public void DestinationDrop()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex == -1) { return ;}
		
	//	//If the mouse button is UnClicked and if  the mouse position is on the fill field
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		b_dragging = false;
			
	//		// boolean for if the text was placed in the box.
	//		bool successful = false;
			
	//		// loop through the destination rects to see which box the word is being placed in
	//		for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
	//		{
	//			// save the mouse pos for ease of access
	//			float mouseX = Input.mousePosition.x;
	//			float mouseY = Input.mousePosition.y;
				
	//			// check if this destination box contains the current clicked keyword box
	//			if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// if the destination text is empty
	//				if(m_L4destinationText[i] == "")
	//				{
	//					// set the destination text and remove the clicked text.
	//					m_L4destinationText[i] = m_currentClickedString;
	//					m_currentClickedString = "";
	//					successful = true;
						
	//					float width = m_dextinationRectWidth - 2;
	//					float height = m_dextinationRectHeight - 2;
	//					m_movableStringRect[m_currentIndex].Set(
	//						m_destinationOriKeyWordRect[i].center.x - width/2, 
	//						m_destinationOriKeyWordRect[i].center.y - height/2, 
	//						width, 
	//						height);

	//					m_currentClickedStringRect = k_zeroRect;
	//					m_currentIndex = -1;
	//				}
	//			}
	//		}
			
	//		// another early release because everything is done already
	//		if (successful) { return ;}
			
	//		// execute this code if the user missed the destination box.
	//		ResetCurrentString();
	//	}
	//}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
		m_keyWordText[m_currentIndex] = m_currentClickedString;
		m_currentClickedStringRect = k_zeroRect;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keuWordExplaination = "";
			
		switch(_keyWord)
		{
			case "Fighting at home":
				keuWordExplaination = "I had a fight at home recently.";
				break;
			case "Getting bullied":
				keuWordExplaination = "Someone or some people are bullying me.";
				break;
			case "Being left out":
				keuWordExplaination = "I didn't get into the team.";
				break;
			case "Too much work":
				keuWordExplaination = "I have too much work to do right now.";
				break;
			case "Being ignored":
				keuWordExplaination = "My friend ignored me.";
				break;
			case "Being grounded":
				keuWordExplaination = "Parents grounded me.";
				break;
			case "Bad exams":
				keuWordExplaination = "I just got a bad exam result.";
				break;
			default:
			    keuWordExplaination = "Drag and drop the problem you most want to solve. " +
			    	"The problem you chose will be saved in your Notebook. " +
			    	"You'll be able to test out STEPS on this problem. " +
			    	"I'd like you to try out STEPS in your life.";
				break;
		}
			
		return keuWordExplaination;
	}
}
