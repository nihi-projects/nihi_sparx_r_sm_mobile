using UnityEngine;
using System.Collections;

public class L5EnterTexts : MonoBehaviour
{
	static public string[] m_inputTexts = 
	{
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text."
	};

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{
        menu = TextDisplayCanvas.instance.ShowPrefab( "EnterText" );
        menu.GetComponent<EnterTextDialog>().SetNumFields(m_inputTexts.Length);
        menu.GetComponent<EnterTextDialog>().SetTitle("I SPOT these annoying Gnats (Gloomy Negative Automatic Thoughts):");
    }
	
	// Update is called once per frame
	void Update () 
	{
        for (int i = 0; i < m_inputTexts.Length; ++i)
        {
            m_inputTexts[i] = menu.GetComponent<EnterTextDialog>().GetText(i);
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L5GuideScene26");
        }
    }
}
