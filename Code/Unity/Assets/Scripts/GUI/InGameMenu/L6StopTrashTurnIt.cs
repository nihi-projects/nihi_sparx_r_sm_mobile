using UnityEngine;
using System.Collections;

public class L6StopTrashTurnIt : MonoBehaviour {

    //public GUISkin m_skin = null;
    //public Texture2D m_tStopITrashTurntTexture;

    // Use this for initialization
    //void Start () 
    //{
    //	//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
    //	FontSizeManager.checkFontSize(m_skin);
    //	//m_tStopITrashTurntTexture = (Texture2D)Resources.Load ("UI/mg_l5_e_mg1", typeof(Texture2D));	
    //}

    private GameObject menu;
	
	// Update is called once per frame
	void Update () {

        if(menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("StopTrashTurn");
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene12_1_1");
        }
    }
	
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//GUI.DrawTexture(new Rect(220.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor), m_tStopITrashTurntTexture);
		//GUI.Label(new Rect(250.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"Stop it","PictureTextSmall");
		//GUI.Label(new Rect(655.0f * Global.ScreenWidth_Factor, 225.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"Trash it","PictureTextSmall");
		
		
	}
}
