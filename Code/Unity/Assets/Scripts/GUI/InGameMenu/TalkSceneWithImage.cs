using UnityEngine;
using System.Collections;

public class TalkSceneWithImage : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin = null;
	
	//Private variables
	public Texture2D m_tConversationBackground;
	public Texture2D m_tCharacterFaceTexture;
	private LoadTalkSceneXMLData m_loadSceneData;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Load the xml character conversation content here
		m_loadSceneData = GetComponent<LoadTalkSceneXMLData>();
		
		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/frontendbg", typeof(Texture2D));
		//m_tCharacterFaceTexture   = (Texture2D)Resources.Load ("UI/mg_l1_e_mg3", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
		//Check which texture should be displayed
		if(GameObject.Find ("GUI").GetComponent<TalkScenes>().GetCurrentScene() == "TextGuideScene24"){
			//m_tCharacterFaceTexture = Resources.Load ("UI/mg_l1_e_mg3.png");
		}
	}
	
	void OnGUI(){
		//The conversation text background
		GUI.Label(new Rect(0.0f, 0.0f, 800.0f * Global.ScreenWidth_Factor, 500.0f * Global.ScreenHeight_Factor), "", "FullSizeDialogBox");
		
		GUI.Label(new Rect(100 * Global.ScreenWidth_Factor,270 * Global.ScreenHeight_Factor,600 * Global.ScreenWidth_Factor,200 * Global.ScreenHeight_Factor), m_loadSceneData.LoadVoiceText(GameObject.Find ("GUI").GetComponent<TalkScenes>().GetCurrentScene()));
		
		//The character face image
		GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor), m_tCharacterFaceTexture);
		
		//The next button
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene24");
		}
	}
}
