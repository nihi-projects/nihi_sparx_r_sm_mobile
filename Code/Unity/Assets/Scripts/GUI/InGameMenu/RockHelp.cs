﻿using UnityEngine;
using System.Collections;

public class RockHelp : MonoBehaviour {

	private bool bActivated;
	public GUISkin GUIskin;
	public string Text;
	GameObject oGameRock;
	PushRockMiniGame scriptPushRockMiniGame;
	// Use this for initialization
	void Start () {
		oGameRock = GameObject.Find ("RockGame");
		scriptPushRockMiniGame = oGameRock.GetComponent<PushRockMiniGame> ();
	}

	// Update is called once per frame
	void Update () {
		//if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_bTalkedWithMentor) {
		if (scriptPushRockMiniGame.m_iNumOfRockPushed == 0) {
			if (GameObject.Find ("TriggerRockHelp").GetComponent<RockHelpTriger> ().m_EagleTriggerEntered) {
				bActivated = true;	
			} else {
				bActivated = false;	
			}
		} else {
			bActivated = false;	
		}
		//}

	}

	void OnGUI()
	{
		if(!bActivated)
		{
			return;
		}
		//GUI.skin = GUIskin;
		//GUIStyle temp = GUI.skin.GetStyle("Label");
		//GUI.Label(new Rect(200f * Global.ScreenWidth_Factor,537f * Global.ScreenHeight_Factor,620f * Global.ScreenWidth_Factor,153f * Global.ScreenHeight_Factor), "", "NormalSizeDilogBox");
		//temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		//GUI.Label(new Rect(306f * Global.ScreenWidth_Factor,583f * Global.ScreenHeight_Factor,725 * Global.ScreenWidth_Factor,100 * Global.ScreenHeight_Factor), Text, "Label");
		//float y = Screen.height*0.84f;

        TextDisplayCanvas.instance.ShowTooltip(Text);
    }
}
