using UnityEngine;
using System.Collections;

public class MouseCursorChange : MonoBehaviour {

	private static MouseCursorChange instanceRef;

	//The texture that's going to replace the current cursor  
    public Texture2D cursorTexture = null;  
  
    //This variable flags whether the custom cursor is active or not  
    public bool ccEnabled = false;  
  
	public void Awake(){
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}
		
        Invoke("SetCustomCursor",2.0f);
	}
	
    public void Start()  
    {  
         //cursorTexture = (Texture2D)Resources.Load ("UI/cursor", typeof(Texture2D));
    }  
	
	public void Update()
	{
		
	}
  
    public void OnDisable()   
    {  
        //Resets the cursor to the default  
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);  
        //Set the _ccEnabled variable to false  
        this.ccEnabled = false;  
    }  
  
    public void SetCustomCursor()  
    {  
        //Replace the 'cursorTexture' with the cursor    
        Cursor.SetCursor(this.cursorTexture, Vector2.zero, CursorMode.Auto);  
        //Set the ccEnabled variable to true  
        this.ccEnabled = true;  
    }
}
