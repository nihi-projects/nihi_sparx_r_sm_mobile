using UnityEngine;
using System.Collections;

public class L3StopIt : MonoBehaviour
{
    
    private GameObject menu = null;
	
	// Update is called once per frame
	void Update () {
	    if(menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("StopIt");
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu.gameObject);
            Global.arrowKey = "";

            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene28");
        }
    }
}
