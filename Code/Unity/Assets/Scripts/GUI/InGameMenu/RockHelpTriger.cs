﻿using UnityEngine;
using System.Collections;

public class RockHelpTriger : MonoBehaviour {

	public bool m_EagleTriggerEntered = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other) 
	{
		m_EagleTriggerEntered = true;
	}

	void OnTriggerExit(Collider other) 
	{
		m_EagleTriggerEntered = false;
	}

}
