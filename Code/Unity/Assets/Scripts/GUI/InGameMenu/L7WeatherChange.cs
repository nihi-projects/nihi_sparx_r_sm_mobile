using UnityEngine;
using System.Collections;

public class L7WeatherChange : MonoBehaviour {
	
	private float m_fFadeInStartTime    = 0.0f;
	private float m_fFadeOutStartTime   = 0.0f;
	private float m_fWaitTime           = 0.0f;
	
	private float m_fLightMainIntensity = 0.0f;
	private float m_fLightRimIntensity  = 0.0f;
	private float m_fFogDensity         = 0.0f;
	private float m_fTime               = 0.0f;
	
	private bool m_bFadeInFinished      = false;
	private bool m_bPlayerStartsWait    = false;
	private bool m_bFadeOutStarted      = false;
	private bool m_bTuiIsFlyingDown     = false;
	
	private bool paused = false;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!paused)
		{
			m_fTime += Time.deltaTime * 0.5f;
		}
		
		//The weather start getting dark, when player reaches the point
		if(m_fTime - m_fFadeInStartTime < 1.0f){			
			if (!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
				SetWeatherDark();
				//Starts raining
				GameObject.Find ("Rain Level7").GetComponent<ParticleSystem>().Play();
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L7WeatherChange" + "_Update");
			}
		}
		else if(!m_bFadeInFinished){
			m_bPlayerStartsWait = true;
			m_bFadeInFinished = true;
			
			//Tui flys down when the weather is dark
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("37047");
			GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L7WeatherChange" + "_Update2");
			m_bTuiIsFlyingDown = true;
		}
		
		//Tui fly down and talk to the player
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiIsFlyingDown){
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L7HopeScene3";
			m_bTuiIsFlyingDown = false;
			GameObject.Find("L7GUI").GetComponent<L7TuiTerminateScene>().enabled = false;
			paused = true;
		}
		
		//Wait finished
		if(!GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled){
			if(m_bPlayerStartsWait && (m_fTime - m_fWaitTime) > 9.0f){
				m_bPlayerStartsWait = false;
				m_fFadeOutStartTime = m_fTime;
				m_bFadeOutStarted = true;
                Global.GetPlayer().GetComponent<PlayerMovement>().SetAfterTheStrom(true);
            }
		}
		
		//If the Tui conversation is not finished, the weather will stay dark
		//if(!GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled){
			if(m_bFadeOutStarted){
				SetWeatherBright();
				
				//The weather is bright again
				if(m_fTime - m_fFadeOutStartTime >= 1.0f){
					RenderSettings.fogDensity = 0.01f;
					
					//Stop raining
					GameObject.Find ("Rain Level7").GetComponent<ParticleSystem>().Pause();
					//Destory the particle system
					GameObject.Find ("Rain Level7").SetActive(false);
					
					Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L7WeatherChange" + "_Update3");
					Global.CameraSnapBackToPlayer();
					GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
					
					GetComponent<AudioSource>().enabled = false;
					this.enabled = false;
				}
			}
		//}
	}
	
	public void OnEnable()
	{
		m_fFadeInStartTime = m_fTime;
		m_fWaitTime = m_fTime;
		GetComponent<AudioSource>().enabled = true;
	}
	
	/*
	 * The function sets the weather back to dark slowly, weather change starts.
	 * 
	 * */
	public void SetWeatherDark()
	{
		//Change Light Main slowly
		m_fLightMainIntensity = Mathf.Lerp(0.7f, 0.0f, (m_fTime - m_fFadeInStartTime));
		GameObject.Find ("Light Main").GetComponent<Light>().intensity = m_fLightMainIntensity;
		
		//Change Light Rim slowly
		m_fLightRimIntensity = Mathf.Lerp(0.3f, 0.0f, (m_fTime - m_fFadeInStartTime));
		GameObject.Find ("Light Rim").GetComponent<Light>().intensity = m_fLightRimIntensity;
		
		//Change Render Setting slowly
		RenderSettings.fogColor = new Color(0.14f, 0.13f, 0.2f, 1.0f);
		m_fFogDensity = Mathf.Lerp(0.1f, 0.01f, (m_fTime - m_fFadeInStartTime));
		RenderSettings.fogDensity = m_fFogDensity;
	}
	
	/*
	 * The function sets the weather back to bright slowly, weather change ends.
	 * 
	 * */
	public void SetWeatherBright()
	{
		//Change Light Main slowly
		m_fLightMainIntensity = Mathf.Lerp(0.0f, 0.7f, (m_fTime - m_fFadeOutStartTime));
		GameObject.Find ("Light Main").GetComponent<Light>().intensity = m_fLightMainIntensity;
		
		//Change Light Rim slowly
		m_fLightRimIntensity = Mathf.Lerp(0.0f, 0.3f, (m_fTime - m_fFadeOutStartTime));
		GameObject.Find ("Light Rim").GetComponent<Light>().intensity = m_fLightRimIntensity;
		
		//Change Render Setting slowly
		RenderSettings.fogColor = new Color(0.235f, 0.168f, 0.56f, 1.0f);
		m_fFogDensity = Mathf.Lerp(0.01f, 0.1f, (m_fTime - m_fFadeOutStartTime));
		RenderSettings.fogDensity = m_fFogDensity;
	}
	
	public void UnPause()
	{
		paused = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L7WeatherChange" + "_Update4");
	}
}
