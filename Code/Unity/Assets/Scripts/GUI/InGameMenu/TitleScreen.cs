using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin = null;
	
	// Use this for initialization
	public void Start () {
	
	}
	
	// Update is called once per frame
	public void Update () {
	
	}
	
	public void OnGUI(){
		GUI.depth = -2;
		GUI.skin = m_skin;
		GUI.Box(new Rect(0,0,960,600),"","TitleScreenBackground");
		// menu and buttons
		GUILayout.BeginArea (new Rect (160,100,480,300),"","TitleMenuFont");
			GUILayout.BeginVertical();
				GUILayout.BeginHorizontal();
					if (GUILayout.Button ("New Game")){
						gameObject.GetComponent<CharacterSelection>().enabled = true;
						gameObject.GetComponent<LogoBackgroundScreen>().enabled = false;
						this.enabled = false;
					}
				GUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
				GUILayout.BeginHorizontal();
					if (GUILayout.Button ("Continue Game")){

					}
				GUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
				GUILayout.BeginHorizontal();
					if (GUILayout.Button ("Options")){

				    }
				GUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
				GUILayout.BeginHorizontal();
					if (GUILayout.Button ("Extras")){

					}
				GUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
				GUILayout.BeginHorizontal();
					if (GUILayout.Button ("Quit")){
						
					}
				GUILayout.EndHorizontal();
			GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}
