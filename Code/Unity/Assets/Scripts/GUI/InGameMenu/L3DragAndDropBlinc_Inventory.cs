﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class L3DragAndDropBlinc_Inventory : MonoBehaviour {

	public GameObject oNextButton;
	public int nItems;
	// Use this for initialization
	void Start () {
		nItems = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (nItems == 5) {
			oNextButton.SetActive(true);
		}
	}

	public void NextButton(){
		GameObject.Find("L3DragAndDropBlinc").SetActive(false);
		GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
		//Update the current scene name
		GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene36");

	}
}
