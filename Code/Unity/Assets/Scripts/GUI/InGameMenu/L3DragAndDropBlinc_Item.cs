﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public class L3DragAndDropBlinc_Item : MonoBehaviour, IDropHandler  {
	public GameObject oMinigameManager;
	public GameObject item {
		get{ 
			if (transform.childCount > 0) {
				return transform.GetChild (0).gameObject;
			}
			return null;
		}
	}

	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
		if (!item) {
			if (L3DragAndDropBlinc_DragHandler.itemBeingDregged.transform.name == transform.name) {
				L3DragAndDropBlinc_DragHandler.itemBeingDregged.transform.SetParent (transform);
				L3DragAndDropBlinc_DragHandler.itemBeingDregged.transform.position = transform.position;
				oMinigameManager.GetComponent<L3DragAndDropBlinc_Inventory> ().nItems++;
			}
		}
	}

	#endregion

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
