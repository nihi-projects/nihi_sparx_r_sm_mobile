using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ShieldAgaDepression : MonoBehaviour 
{

	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_shieldPrf = null;

	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	static private float TextWidth = 130.0f * Global.ScreenWidth_Factor;
	static private float TextHeight = 30.0f * Global.ScreenHeight_Factor;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};
	
	private Rect[] shieldNameRects = 
	{
//		new Rect(570.0f, 85.0f, TextWidth, TextHeight),
//		new Rect(690.0f, 290.0f, TextWidth, TextHeight),
//		new Rect(570.0f, 495.0f, TextWidth, TextHeight),
//		new Rect(265.0f, 495.0f, TextWidth, TextHeight),
//		new Rect(145.0f, 290.0f, TextWidth, TextHeight),
//		new Rect(265.0f, 85.0f, TextWidth, TextHeight),
		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
	};
	
	private bool[] displayName = 
	{
		true,
		true,
		true,
		true,
		true,
		true
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	public Texture colourTexture = null;
	public Texture greyScaleTexture = null;

    private ShieldMenu shieldMen = null;
	
	void OnEnable()
	{
        shieldMen = TextDisplayCanvas.instance.ShowPrefab("ShieldAgainstDepression").GetComponent<ShieldMenu>();

        displayName = new bool[]{true, true, true, true, true, true};

        Transform pTrans = null;
        if (GameObject.Find("ShieldCamera"))
        {
            pTrans = GameObject.Find("ShieldCamera").transform;
        }
        m_vShieldPosition = new Vector3(0.0f, 0.0f, 0.65f);
        if (Global.PreviousSceneName == "GuideScene28")
		{
			
            //The shield object clone
            m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity, pTrans);
			GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
			displayName = new bool[]{true, true, true, true, true, true};
		}
		else
		{
			//rimColour = GameObject.Find("Light Rim").GetComponent<Light>().color;
			GameObject.Find("Shield Light").GetComponent<Light>().enabled = true;
			//The shield object clone
			m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity, pTrans);
			//Vector3 newPos = m_vShieldPosition - new Vector3(0.0f, 0.0f, 0.60f);
			//m_shieldObjectClone.transform.LookAt(newPos);
			//GameObject.Find ("ShiledBG").GetComponent<GUITexture>().enabled = true;
			float width = Screen.width - Screen.width*0.1455f;
			float height = Screen.height - Screen.height*0.1276f;
			var Temp = new Rect(0,0,width,height); 
			GameObject.Find ("ShiledBG").GetComponent<GUITexture>().pixelInset = Temp;
		}

        if( GameObject.Find("ShieldPlane") )
        {
            GameObject.Find("ShieldPlane").GetComponent<MeshRenderer>().enabled = true;
        }
        if( GameObject.Find("ShieldCamera"))
        {
            GameObject.Find("ShieldCamera").GetComponent<Camera>().enabled = true;
        }

        m_shieldObjectClone.transform.localPosition = m_vShieldPosition;

        if (Global.GetPlayer() != null)
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "ShieldAgaDepression");
    }

    // Update is called once per frame
    void Update()
    {
        if (shieldMen == null || shieldMen.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            if (Global.GetPlayer() != null)
                Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "ShieldAgaDepression2");

            Destroy(shieldMen.gameObject);
            Global.arrowKey = "";
            this.enabled = false;

           

            //Destroy the shield clone
            GameObject.Destroy(m_shieldObjectClone);

            if( SceneManager.GetActiveScene().name == "GuideScene")
                UpdateCurrentSceneName();
            else
                GameObject.Find("ShiledBG").GetComponent<GUITexture>().enabled = false;

            if(GameObject.Find("ShieldPlane"))
                GameObject.Find("ShieldPlane").GetComponent<MeshRenderer>().enabled = false;

            if (GameObject.Find("ShieldCamera"))
            {
                GameObject.Find("ShieldCamera").GetComponent<Camera>().enabled = false;
            }
        }
    }
	
	void ChangePieceColour(int _index)
	{
        if( m_shieldObjectClone != null )
        {
            // get the child of teh shiled that we are changing
            Transform child = m_shieldObjectClone.transform.Find(childObjectNames[_index]);

            // if it is being displayed then the colour needs active
            if (displayName[_index])
            {
                child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
            }
            // fi it is not being displayed then the coulour needs to be grey scale
            else
            {
                child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
            }
        }
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//Depend on the different scene, the shield is different as well
		if(Global.PreviousSceneName == "GuideScene28")
		{
			GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene29");
		}
		else 
		{
			GameObject.Find("Shield Light").GetComponent<Light>().enabled = false;
			//GameObject.Find("Light Rim").GetComponent<Light>().color = rimColour;
			GameObject.Find ("ShiledBG").GetComponent<GUITexture>().enabled = false;
			gameObject.SetActive(false);
			this.enabled = false;
		}
	}
}
