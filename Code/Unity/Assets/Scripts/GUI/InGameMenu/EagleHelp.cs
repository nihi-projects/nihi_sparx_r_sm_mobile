﻿using UnityEngine;
using System.Collections;

public class EagleHelp : MonoBehaviour {

	private bool bActivated;
	public GUISkin GUIskin;
	public string Text;

    public OutlineObject eagleOutline;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update ()
    {
        if (eagleOutline)
        {
            if (bActivated)
                eagleOutline.TurnOnOutline();
            else
                eagleOutline.TurnOffOutline();
        }

        if (!Global.m_bGotCurrentLevelGem)
        {
            if(GameObject.Find("GUI"))
            {
                TalkScenes talkScenes = GameObject.Find("GUI").GetComponent<TalkScenes>();
                if (GameObject.Find("TriggerEagleHelp").GetComponent<EagleHelpTrigger>().m_EagleTriggerEntered && talkScenes != null && talkScenes.m_bTalkedWithMentor == true)
                {
                    bActivated = true;
                }
                else
                {
                    bActivated = false;
                }
            }
            
        }
        else
        {
            bActivated = false;
        }    
	}

	void OnGUI()
	{
		if(!bActivated)
		{
			return;
		}
        //GUI.skin = GUIskin;
        //GUIStyle temp = GUI.skin.GetStyle("Label");
        //GUI.Label(new Rect(Screen.width * 0.15f,Screen.height*0.7f,Screen.width * 0.7f,Screen.height*0.2f), "", "NormalSizeDilogBox");
        //temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        //GUI.Label(new Rect(Screen.width*0.25f,Screen.height*0.76f,725,100), Text, "Label");
        //float y = Screen.height*0.84f;
        TextDisplayCanvas.instance.ShowTooltip(Text);
    }
}
