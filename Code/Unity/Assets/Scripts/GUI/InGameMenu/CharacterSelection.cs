using UnityEngine;
using System.Collections;

public class CharacterSelection : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin;
	public GameObject m_boyObjectPrf;
	public GameObject m_girlObjectPrf;

    //Protected
    protected GameObject characterCreation;

    //Private variables
    private GameObject m_boyObjectClone;
    private GameObject m_girlObjectClone;

    private Texture2D m_backgroundImage;
	private GameObject m_characterCustomiseBG;
	
	private Vector3 m_vBoysPosition; 
	private Vector3 m_vGirlsPosition; 
	
	//private Camera m_Camera = null;
	private Rect m_rect;
	private Transform theBoyTransform;
	private Vector3 m_v3WorldSpace; 
	
	// Use this for initialization
	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	public void Update () 
	{	
		//Update the postion once the character is selected for customisation
		if (Input.GetMouseButtonDown(0)){
			if(Global.GetMousePosToObj() == "Boy(Clone)"){
				this.enabled = false;
				//Destory the boy clone object on this page
				GameObject.Destroy(m_boyObjectClone);
				GameObject.Destroy(m_girlObjectClone);
				
				GameObject.Find ("GUI").GetComponent<BoyCustomisation>().enabled = true;
                characterCreation = TextDisplayCanvas.instance.ShowPrefab("CharacterCreation");
                GameObject.Find("GUI").GetComponent<BoyCustomisation>().creationScreen = characterCreation.GetComponent<CharacterCreation>();
                Global.CurrentPlayerName = "Boy";
			}
			else if (Global.GetMousePosToObj() == "Girl(Clone)"){
				this.enabled = false;
				//Destory the boy clone object on this page
				GameObject.Destroy(m_girlObjectClone);
				GameObject.Destroy(m_boyObjectClone);
				
				GameObject.Find ("GUI").GetComponent<GirlCustomisation>().enabled = true;
                characterCreation = TextDisplayCanvas.instance.ShowPrefab("CharacterCreation");
                GameObject.Find("GUI").GetComponent<GirlCustomisation>().creationScreen = characterCreation.GetComponent<CharacterCreation>();
                Global.CurrentPlayerName = "Girl";

			}
		}
	}
	
	// Title
	public void OnGUI () {
        //GUI.skin = m_skin;
        //GUI.Label(new Rect(320 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor, 320 * Global.ScreenWidth_Factor, 16 * Global.ScreenHeight_Factor), "Choose your Character" ,"TitleMenuFont");
        //TextDisplayCanvas.instance.ShowTitle("Choose Your Character");
	}
	
	//The boy and girl clones need to be created every time when
	//this script is called.
	public void OnEnable(){
		m_characterCustomiseBG = GameObject.Find("CharacterCustomiseBackground") as GameObject;
		m_characterCustomiseBG.GetComponent<GUITexture>().enabled = true;
		
		//The boy's position on the character selection position
		m_vBoysPosition = new Vector3(-0.7f, 0.1f, 3f);
		m_vGirlsPosition = new Vector3(0.7f, 0.1f, 3f);
		
		//Load the boy object on the screen
		m_boyObjectClone = (GameObject) Instantiate(m_boyObjectPrf, m_vBoysPosition, Quaternion.identity);
		m_boyObjectClone.transform.rotation = new Quaternion(0.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 0.0f * Global.ScreenWidth_Factor, 1.0f * Global.ScreenHeight_Factor);
		Destroy(m_boyObjectClone.GetComponent<PlayerMovement>());
		
		//Load the girl object on the screen
		m_girlObjectClone = (GameObject) Instantiate(m_girlObjectPrf, m_vGirlsPosition, Quaternion.identity);
		m_girlObjectClone.transform.rotation = new Quaternion(0.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 0.0f * Global.ScreenWidth_Factor, 1.0f * Global.ScreenHeight_Factor);
		Destroy(m_girlObjectClone.GetComponent<PlayerMovement>());
	}
}
