using UnityEngine;
using System.Collections;

public class L5DragDropWords : MonoBehaviour 
{
    private string[] m_destinationText = 
	{
		"Attempts to make you feel guilty about things you have no control over.\n'We are losing every game and it's my fault'.", 
		"Only perfect will do.\n'I can't miss a single practise this season'\nor\n'We have to win all our games'.", 
        "Ignore the positives and only see things that go wrong, not right. It's the opposite of a silver lining.\n'We won our game but anyone could beat that team'.", 
		"Blows everything out of proportion.\n'We lost this game, it's the end of the world!'", 
        "Everything is seen as extremes (often has words like 'always' or 'never'), it's either wonderful or horrible, with nothing in between.\n'The referee is always unfair to our team' or 'if someone isn’t nice to me at all times, then they are not my real friend'", 
        "We think we know what is going to happen and what everyone else is thinking.\n'I bet I won't make the team because the coach thinks I'll drop the ball' or 'He doesn’t like me, I bet he thinks I’m ugly'"
	};

    string[] draggables = new string[] { "Guilty", "Perfectionist", "Downer", "Disaster", "All-or-Nothing", "Mind Reader" };

    public GameObject menu;
	
	// Update is called once per frame
	void Update ()
    {
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("MatchGnats");
            MatchGnatsScreen mgs = menu.GetComponent<MatchGnatsScreen>();
            mgs.SetText(m_destinationText, draggables);
        }

        //The next button
        if (menu.GetComponent<MatchGnatsScreen>().isFinished == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";

                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L5GuideScene21");
            }
        }
    }
}
