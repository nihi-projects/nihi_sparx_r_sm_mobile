using UnityEngine;
using System.Collections;

public class QuiteScreen : MonoBehaviour {

	public GUISkin m_skin = null;
	
	public Texture2D m_sQuiteTexture1;
	public Texture2D m_sQuiteTexture2;
	
	private float m_fStartTime = 0.0f;
	private string m_sQuiteText = "";
	private Texture2D m_sQuiteTexture;
	private bool m_bFinalImageDisplayed = false;
	
	// Use this for initialization
	void Start () {
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnEnable(){
		m_sQuiteText = "\nOK, you said you'd like to leave SPARX.\nThere are lots of other ways of dealing with feeling down.\nHere are some ideas.";
		
		m_fStartTime = Time.time;
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () {		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		if((Time.time - m_fStartTime) > 0.0f && (Time.time - m_fStartTime) < 3.0f){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(300.0f, 0.0f, 960.0f, 530.0f), m_sQuiteText);
		}
		else if((Time.time - m_fStartTime) > 3.0f && m_bFinalImageDisplayed == false){
			m_sQuiteText = "";
			GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "EndHelpRemainder");
		}
		
		if(m_bFinalImageDisplayed){
			GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "EndGreeting");
		}
		
		if((Time.time - m_fStartTime) > 6.0f && m_bFinalImageDisplayed == false){
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				m_bFinalImageDisplayed = true;
			}
		}
		
		if((Time.time - m_fStartTime) > 12.0f){
			this.enabled = false;
		}
	}
}
