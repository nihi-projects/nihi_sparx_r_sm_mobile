using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class L4EnterTexts : MonoBehaviour {

	//public GUISkin m_skin = null;
	
	//static private float m_inputWidth = 300.0f * Global.ScreenWidth_Factor;
	//static private float m_inputHeight = 30.0f * Global.ScreenHeight_Factor;
	static public bool m_bTriggered = false;
	//private Rect m_inputRects = new Rect(340.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_inputWidth, m_inputHeight);
	
	static public string m_inputTexts = "Click here to enter text";

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	void Update () 
	{
        if(menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("EnterText");
        }
        if( menu != null && menu.GetComponentInChildren<InputField>())
            m_inputTexts = menu.GetComponentInChildren<InputField>().text;

        m_bTriggered = true;

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L4GuideScene25");

        }
    }

	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//// the background of the mingame
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//GUI.Label(new Rect(180 * Global.ScreenWidth_Factor, 150 * Global.ScreenHeight_Factor, 600 * Global.ScreenWidth_Factor, 30 * Global.ScreenHeight_Factor), "Enter the problem that \nyou are currently struggling with:","Titles");

		//// draw the text input fields
		//GUI.SetNextControlName ("text1");
		//m_inputTexts = GUI.TextField(m_inputRects, m_inputTexts, 64);
		//if (GUI.GetNameOfFocusedControl () != "") {
		//	m_inputTexts = Global.doTextFieldCheck ("text1", m_inputTexts, m_inputRects);
		//}

		
	}
}
