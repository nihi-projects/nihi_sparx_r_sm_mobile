using UnityEngine;
using System.Collections;

public class TerminateCassFiremanScene : MonoBehaviour {
	
	//Public variables
	public bool m_firePersonConversationFinished = false;
	
	//Private variables
	private GameObject interactObject;
	
	private Vector3 cassStartPos;
	private Vector3 cassTargetPos;
	private Vector3 playerStartPos;
	private Vector3 playerTargetPos;
	
	
	private float m_fCassFirePersonDistance    = 0.0f;
	private float m_fPlayerFirePersonDistance  = 0.0f;
	private float m_fCassPlayerDistance        = 0.0f;
	
	private float m_fCassWalkTime              = 0.0f;
	private float m_fPlayerWalkTime            = 0.0f;
	
	private float m_fCaseWalkSpeed             = 1.0f;
	private float m_fPlayerWalkSpeed           = 1.0f;
	
	private bool m_bMovingToTarget             = false;
	private bool m_bMovingToPlayerTarget       = false;
	private bool m_bCassReachesTarget          = false;
	private bool m_bPlayerReachesTarget        = false;
	
	private bool m_bCassPlayIdleOnce           = false;
	
	
	//Implementation
	
	// Use this for initialization
	void Start () {

	}
	// Update is called once per frame
	void Update () {
		if(!m_firePersonConversationFinished){
			//Cass and fireperson conversation
			CassWalkToFirePersonAnimation();
		}
		else if(Global.m_ObjectTaken && !GameObject.Find ("Cass").GetComponent<Animation>().IsPlaying("take")){
			GameObject.Find("Fireperson").GetComponent<Animation>().Play ("idle");
			
			Global.SnapCameraBasedOnID("22423");
			//Cass and player conversation
			CassWalkToPlayerAnimation();
		}
	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "TerminateCassFiremanScene");
		
		//Find the interacting game object
		interactObject = GameObject.Find (Global.CurrentInteractNPC) as GameObject;
		interactObject.GetComponent<CharacterInteraction>().Interacted = false;
		interactObject.GetComponent<CharacterInteraction>().Interacting = false;
		
		//Distance between the Cass and Fireperson
		cassStartPos = GameObject.Find ("Cass").transform.position;
		cassTargetPos = Global.GetObjectPositionByID("32004");
		m_fCassFirePersonDistance = Vector3.Distance(cassStartPos, cassTargetPos);
		m_fCaseWalkSpeed = m_fCassFirePersonDistance * 0.25f;
		
		//Distance between the Player and Fireperson
		playerStartPos = Global.GetPlayer().transform.position;
		playerTargetPos = Global.GetObjectPositionByID("32005");
		m_fPlayerFirePersonDistance = Vector3.Distance(playerStartPos, playerTargetPos);	
		m_fPlayerWalkSpeed = m_fPlayerFirePersonDistance * 0.25f;
		
		m_bMovingToTarget = true;
	}
	
	/*
	 * This function performs all the animation that when the cass and player
	 * walk towards to the fireperson.
	 * 
    */
	public void CassWalkToFirePersonAnimation()
	{
		if(m_bMovingToTarget){
			if(!m_bCassReachesTarget){
				GameObject.Find ("Cass").transform.LookAt(cassTargetPos);
				if (!GameObject.Find ("Cass").GetComponent<Animation>().IsPlaying("walk"))
				{
					GameObject.Find ("Cass").GetComponent<Animation>().Play("walk");
				}
				m_fCassWalkTime   += Time.deltaTime / m_fCaseWalkSpeed;
				GameObject.Find ("Cass").transform.position = Vector3.Lerp(cassStartPos, cassTargetPos, m_fCassWalkTime);
			}
			
			if(!m_bPlayerReachesTarget){
				Global.GetPlayer().transform.LookAt(playerTargetPos);
				Global.GetPlayer().GetComponent<Animation>().Play("walk");
				m_fPlayerWalkTime += Time.deltaTime / m_fPlayerWalkSpeed;
				Global.GetPlayer().transform.position       = Vector3.Lerp(playerStartPos, playerTargetPos, m_fPlayerWalkTime);
			}
			
			//Cass reaches target
			if(m_fCassWalkTime >= 1.0f){
				GameObject.Find("Cass").transform.LookAt(GameObject.Find ("Fireperson").transform.position);
				//if(!m_bCassPlayIdleOnce){
					//GameObject.Find("Cass").animation.Play ("idle");
					//m_bCassPlayIdleOnce = true;
				//}
				m_bCassReachesTarget = true;
				m_fCassWalkTime = 0.0f;
				
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("AutoL2CassScene2");
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_autoSceneStartTime = Time.time;
				
				//After fireperson conversation
				cassStartPos = GameObject.Find ("Cass").transform.position;
				cassTargetPos = Global.GetObjectPositionByID("32007");
				m_fCassPlayerDistance = Vector3.Distance(cassStartPos, cassTargetPos);
				m_fCaseWalkSpeed = m_fCassPlayerDistance * 0.5f;
			}
			//Player reaches target
			if(m_fPlayerWalkTime >= 1.0f){
				Global.GetPlayer().transform.LookAt(GameObject.Find ("Fireperson").transform.position);
				Global.GetPlayer().GetComponent<Animation>().Play ("idle");
				m_bPlayerReachesTarget = true;
				m_fPlayerWalkTime = 0.0f;
			}
			
			if(m_bCassReachesTarget && m_bPlayerReachesTarget){
				m_bMovingToTarget = false;
			}
		}
	}
	
	/*
	 * This function performs all the animation that when the cass walk
	 * towards to the player after the conversation to the fireperson.
	 * 
    */
	public void CassWalkToPlayerAnimation()
	{
		if(!m_bMovingToPlayerTarget){
			GameObject.Find ("Cass").transform.LookAt(cassTargetPos);
			if (!GameObject.Find ("Cass").GetComponent<Animation>().IsPlaying("walk"))
			{
				GameObject.Find ("Cass").GetComponent<Animation>().Play("walk");
			}
			m_fCassWalkTime   += Time.deltaTime / m_fCaseWalkSpeed;
			GameObject.Find ("Cass").transform.position = Vector3.Lerp(cassStartPos, cassTargetPos, m_fCassWalkTime);
			
			if(m_fCassWalkTime >= 1.0f){
				GameObject.Find("Cass").transform.LookAt(Global.GetPlayer().transform.position);
				GameObject.Find("Cass").GetComponent<Animation>().Play ("idle");
				m_bMovingToPlayerTarget = true;
				m_fCassWalkTime = 0.0f;
				Global.GetGameObjectByID("32003").GetComponent<IceCaveDoor>().m_bGotL2OpenDoorFire = true;
				IceCaveDoor.toggleDoorHighlight(true);
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L2CassScene1_2");
				Global.CurrentInteractNPC = "Cass";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				
				Global.GetPlayer().transform.LookAt(GameObject.Find("Cass").transform.position);
				
				this.enabled = false;
			}
		}
	}
}
