using UnityEngine;
using System.Collections;

public class CutSceneStart : MonoBehaviour {

	//Public variables
	public GUISkin m_skin;
	public int m_currentSceneNum = 1;
	
	
	//Private variables
	private TalkScenes m_talkScene;
	public Texture2D m_tCutSceneStartTexture1;
	public Texture2D m_tCutSceneStartTexture2;
	public Texture2D m_tCutSceneStartTexture3;
	public Texture2D m_tCutSceneStartTexture4;
	private Texture2D m_displayedTexture;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tCutSceneStartTexture1 = (Texture2D)Resources.Load ("UI/cutscene_start_1", typeof(Texture2D));
		//m_tCutSceneStartTexture2 = (Texture2D)Resources.Load ("UI/cutscene_start_2", typeof(Texture2D));
		//m_tCutSceneStartTexture3 = (Texture2D)Resources.Load ("UI/cutscene_start_3", typeof(Texture2D));
		//m_tCutSceneStartTexture4 = (Texture2D)Resources.Load ("UI/cutscene_start_4", typeof(Texture2D));
		m_displayedTexture = m_tCutSceneStartTexture1;
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () {		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.DrawTexture(new Rect(100.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor, 830.0f * Global.ScreenWidth_Factor, 530.0f * Global.ScreenHeight_Factor), m_displayedTexture);
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			m_talkScene.enabled = true;
			m_talkScene.SetCurrentDialogue("GuardianScene" + m_currentSceneNum.ToString());
		}
	}
	
	/*
	*
	* OnEnable Function, this function will be called everytime when the script is enabled.
	*
	*/
	public void OnEnable(){
		m_talkScene = GameObject.Find ("GUI").GetComponent<TalkScenes>();
		
		//Add up the scene name and go to the next statement scene
		switch(m_talkScene.GetCurrentScene()){
			case "GuardianScene1":
				m_displayedTexture = m_tCutSceneStartTexture1;
				m_currentSceneNum = 2;
				break;
			case "GuardianScene2":
				m_displayedTexture = m_tCutSceneStartTexture2;
				m_currentSceneNum = 3;
				break;
			case "GuardianScene3":
				m_displayedTexture = m_tCutSceneStartTexture3;
				m_currentSceneNum = 4;
				break;
			case "GuardianScene4":
				m_displayedTexture = m_tCutSceneStartTexture4;
				m_currentSceneNum = 5;
				break;
		}
	}
}
