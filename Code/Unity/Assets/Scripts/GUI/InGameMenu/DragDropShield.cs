using UnityEngine;
using System.Collections;

public struct DraggableObject
{
	public Rect original;
	public Rect movable;
	public string words;
	
	public DraggableObject(Rect _originalAndMovable, string _words)
	{
		original = _originalAndMovable;
		movable = _originalAndMovable;
		words = _words;
	}
	
	public DraggableObject(Rect _original, Rect _movable, string _words)
	{
		original = _original;
		movable = _movable;
		words = _words;
	}
	
	public DraggableObject(Rect _original, Rect _movable)
	{
		original = _original;
		movable = _movable;
		words = "";
	}
};

public class DragDropShield : MonoBehaviour 
{
	//Public variables
	public GUISkin m_skin;
	public GameObject m_shieldPrf = null;
	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	private string m_conversationBoxText = "";
	
	static private float TextWidth = 120.0f * Global.ScreenWidth_Factor;
	static private float TextHeight = 30.0f * Global.ScreenHeight_Factor;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};
	
	private Rect[] shieldNameRects = 
	{
//		new Rect(570.0f, 95.0f, TextWidth, TextHeight),
//		new Rect(660.0f, 260.0f, TextWidth, TextHeight),
//		new Rect(530.0f, 455.0f, TextWidth, TextHeight),
//		new Rect(320.0f, 455.0f, TextWidth, TextHeight),
//		new Rect(190.0f, 260.0f, TextWidth, TextHeight),
//		new Rect(260.0f, 95.0f, TextWidth, TextHeight),
		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
	};
	
	private bool[] displayName = 
	{
		false,
		false,
		false,
		false,
		false,
		false
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	static private float m_collisiondimension = 10.0f;
	static private float m_movingTextwidth = 200.0f * Global.ScreenWidth_Factor;
	static private float m_movingTextHeight = 40.0f * Global.ScreenHeight_Factor;
	
	private DraggableObject[] m_wordsRects = 
	{
		new DraggableObject(new Rect(125.0f * Global.ScreenWidth_Factor, 140.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
			"Control your breathing"),
		new DraggableObject(new Rect(640.0f * Global.ScreenWidth_Factor, 140.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
			"Find and defeat the Gnats"),
	};
	
	static float m_targetDim = 70.0f * Global.ScreenWidth_Factor;
	private Rect[] m_destinations = 
	{
//		new Rect(510.0f, 130.0f, m_targetDim, m_targetDim),
//		new Rect(570.0f, 240.0f, m_targetDim, m_targetDim),
//		new Rect(510.0f, 350.0f, m_targetDim, m_targetDim),
//		new Rect(380.0f, 350.0f, m_targetDim, m_targetDim),
//		new Rect(320.0f, 240.0f, m_targetDim, m_targetDim),
//		new Rect(380.0f, 130.0f, m_targetDim, m_targetDim)
		new Rect(Screen.width*0.5469f, Screen.height*0.2344f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.625f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.5469f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3906f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3125f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3809f, Screen.height*0.2474f, m_targetDim, m_targetDim)
	};
	
	private int[] m_correctIndex = 
	{
		-1, 
		1, 
		-1, 
		-1, 
		-1,
		0
	};
	
	private string m_currentDragText = "";
	private int m_currentDragIndex = -1;
	private bool m_dragging = false;
	
	public Texture colourTexture = null;
	public Texture colourFrameTexture = null;
	public Texture greyScaleTexture = null;
	public Texture2D m_tConversationBackground = null;

    private ShieldMenu menu;
	
	// Use this for initialization
	void Start () 
	{
        string[] descriptions = new string[] { "Slowing down your breathing is not just a game trick. It is a simple but powerful thing that you can do when you feel stressed, worried or angry.",
                                                        "Gnats are like negative thoughts and you need to learn to spot and swap them. You'll learn more about this next time."};

        menu = TextDisplayCanvas.instance.ShowPrefab("ShieldAgainstDepression").GetComponent<ShieldMenu>();
        for (int i = 0; i < m_wordsRects.Length; ++i)
        {
            menu.AddWord(m_wordsRects[i].words, descriptions[i] );
        }
        
    }
	
	// Update is called once per frame
	void Update () 
	{
        Transform trans = null;
        int index = -1;
        int wordIndex = -1;

        for( int i = 0; i < childObjectNames.Length; ++i )
        {
            ChangePieceColour(i);
        }

        //The default conversation box text
        menu.SetDescription("Match the Cave Province skills to the right part of the shield.");
		
		if (menu.IsDragging() == true)
		{
            wordIndex = menu.GetDraggingIndex();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo);

            if( hitInfo.transform )
            {
                trans = hitInfo.transform;
                for ( int i = 0; i < childObjectNames.Length; ++i )
                {
                    if( childObjectNames[i] == hitInfo.transform.name )
                    {
                        index = i;
                        ChangeFrameColour(i);
                    }
                }
            }
		}
		
		if( DestinationDrop( trans, index, wordIndex ) )
        {
            menu.DestroyDragged();
        }
	}
	
	void OnEnable()
	{
		//The shield against depression position
		m_vShieldPosition = new Vector3(0.0f, 0.03f, 0.70f );
		//The shield object clone
		m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity);
		
		GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
		
	}
	
	void OnGUI()
	{
        if( menu.IsFinished() )
        {
            //The next button
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu.gameObject);
                Global.arrowKey = "";

                this.enabled = false;

                //Destroy the shield clone
                GameObject.Destroy(m_shieldObjectClone);
                GameObject.Find("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                UpdateCurrentSceneName();
            }
        }
	}
	
	void ChangePieceColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.Find(childObjectNames[_index]);
			
		// if it is being displayed then the colour needs active
		if (displayName[_index])
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
		}
		// fi it is not being displayed then the coulour needs to be grey scale
		else
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
		}
	}

	void ChangeFrameColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.Find (childObjectNames [_index]);
		
		// if it is being displayed then the colour needs active
		//if (displayName[_index])
		//{
		child.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", colourFrameTexture);
		//}
		// if it is not being displayed then the coulour needs to be grey scale
		//else
		//{
		//child.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", greyScaleTexture);
		//}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public bool DestinationDrop( Transform _destinationTransform, int _index, int _wordIndex )
	{
		// release early if there is no active keyword box being moved.
		if (_destinationTransform == null || _index == -1 ) { return false  ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
            if (m_correctIndex[_index] == _wordIndex)
            {
                // set the destination text and remove the clicked text.
                m_currentDragText = "";
                m_currentDragIndex = -1;
                displayName[_index] = true;
                return true;
            }
        }
        return false;
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_wordsRects[m_currentDragIndex].movable = m_wordsRects[m_currentDragIndex].original;
		m_wordsRects[m_currentDragIndex].words = m_currentDragText;
		m_currentDragText = "";
		m_currentDragIndex = -1;
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//level 1
		if(Global.CurrentLevelNumber == 1)
		{
			//Depend on the different scene, the shield is different as well
			if(Global.PreviousSceneName == "GuideScene29")
			{
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene30");
			}
		}
	}
}
