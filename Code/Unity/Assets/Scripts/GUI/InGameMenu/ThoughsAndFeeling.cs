using UnityEngine;
using System.Collections;

public class ThoughsAndFeeling : MonoBehaviour {
	
	public GUISkin m_skin = null;
	
	public Texture2D m_tBarchartBackground;
	public Texture2D m_tThroughAndFeelingTexture;

    private GameObject obj;

	// Use this for initialization
	void Start () {
        //m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");

        //m_tBarchartBackground = (Texture2D)Resources.Load ("UI/frontendbg", typeof(Texture2D));
        //m_tThroughAndFeelingTexture = (Texture2D)Resources.Load ("UI/mg_l1_s_mg1", typeof(Texture2D));

        obj = TextDisplayCanvas.instance.ShowPrefab("ThoughtsFeelings");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(obj == null)
            obj = TextDisplayCanvas.instance.ShowPrefab("ThoughtsFeelings");

        if (obj.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Global.arrowKey = "";
            Destroy(obj);


            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            UpdateCurrentSceneName();
        }
    }
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//GUI.DrawTexture(new Rect(190.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 650.0f * Global.ScreenWidth_Factor, 501.0f * Global.ScreenHeight_Factor), m_tThroughAndFeelingTexture);
		
		
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		if(Global.PreviousSceneName == "GuideScene10")
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene11");
		}
		else if(Global.PreviousSceneName == "GuideScene22")
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene23");
		}
	}
}
