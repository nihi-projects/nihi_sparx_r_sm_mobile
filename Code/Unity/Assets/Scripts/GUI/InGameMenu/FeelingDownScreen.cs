using UnityEngine;
using System.Collections;

public class FeelingDownScreen : MonoBehaviour {

	public GUISkin m_skin = null;
	
	
	public Texture2D m_tFeelingDownTexture;

    public NextScreen screen;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tFeelingDownTexture = (Texture2D)Resources.Load ("UI/mg_l2_s_mg2", typeof(Texture2D));
		FontSizeManager.checkFontSize(m_skin);
	}

    public void OnEnable()
    {
        screen = TextDisplayCanvas.instance.ShowPrefab("FeelingDown").GetComponent<NextScreen>();
    }

    public void OnDisable()
    {
        Destroy(screen.gameObject);
    }

    /*
	*
	* OnGUI Function.
	*
	*/
    public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//GUI.DrawTexture(new Rect(185.0f * Global.ScreenWidth_Factor, 55.0f * Global.ScreenHeight_Factor, 680.0f * Global.ScreenWidth_Factor, 501.0f * Global.ScreenHeight_Factor), m_tFeelingDownTexture);
		//GUI.Label(new Rect(240.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, 240.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"Feeling Down");
		//GUI.Label(new Rect(680.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 240.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"No Energy");
		//GUI.Label(new Rect(310.0f * Global.ScreenWidth_Factor, 360.0f * Global.ScreenHeight_Factor, 240.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"Doing Less");
		//GUI.Label(new Rect(620.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor, 240.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor),"Feeling Worse");
		
		if(screen.WasNextClicked() || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			UpdateCurrentSceneName();
		}
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//Before go to the island scene
		if(Global.LevelSVisited == false){
			//level 1
			if(Global.CurrentLevelNumber == 1){
				
			}
			//level 2
			if(Global.CurrentLevelNumber == 2){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L2GuideScene11");
			}
		}
		//After visited the island scene
		else{
			//level 1
			if(Global.CurrentLevelNumber == 1){
				
			}
			//level 2
			if(Global.CurrentLevelNumber == 2){
				
			}
		}
	}
}
