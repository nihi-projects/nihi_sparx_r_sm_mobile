using UnityEngine;
using System.Collections;

public class GetAroundSparx : MonoBehaviour {

	public GUISkin m_skin = null;
	public GameObject m_theGuardian;
	public GameObject m_theMontor;
	
	//Small icon images in the dialog box
	public Texture2D m_mouseCorsorImage;
	public Texture2D m_closeImage;
	public Texture2D m_backPackImage;
	public Texture2D m_sparxManulImage;
	public Texture2D loadingIcon;

	public Texture2D m_Eagle;
	public Texture2D m_Latern;
	public Texture2D m_Gnat;
	public Texture2D m_Lader;

	public Texture2D m_Control;
	public Texture2D m_Control2;

	private GameObject m_gardianClone;
	private GameObject m_mentorClone;
	
	private TalkScenes m_talkScene;


	
	public GameObject m_gGardianObject = null;
	//private float fTime 		  = 0.0f;
	
	// Use this for initialization
	void Start () 
	{
		//m_mouseCorsorImage = (Texture2D)Resources.Load ("UI/corsor_1", typeof(Texture2D));
		m_closeImage       = (Texture2D)Resources.Load ("UI/exit", typeof(Texture2D));
		//m_backPackImage    = (Texture2D)Resources.Load ("UI/backpack", typeof(Texture2D));
		//m_sparxManulImage  = (Texture2D)Resources.Load ("UI/sbook_icon", typeof(Texture2D));
		FontSizeManager.checkFontSize(m_skin);

	}
	
	// Update is called once per frame
	void Update () 
	{
        if (GameObject.Find("GUI") == null)
            return;


        GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;

		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "GetAroundSparx" + "_Update");
		Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;

		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bFollow = true;

	}
	
	void OnEnable(){

		m_gGardianObject = GameObject.Find ("guardian") as GameObject;
		
		//Other levels does not have the guardian object
		if(Global.CurrentLevelNumber != 1 || Global.LevelSVisited == true){
			m_gGardianObject.SetActive(false);
			this.enabled = false;
		}
	}
	
	/*
	*
	* OnGUI function
	*
	*/
	void OnGUI()
	{
		//FontSizeManager.emulaRetinaDPI ();
		//m_skin.FindStyle("Label").fontSize = (int)29f * (int)Global.DPI_Factor;


		if(Global.CurrentLevelNumber == 1){
			GUI.depth = -1;
			GUI.skin = m_skin;
	
			GUI.Label(new Rect(0.0f * Global.ScreenWidth_Factor, 0.0f * Global.ScreenHeight_Factor, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(0.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, 960.0f * Global.ScreenWidth_Factor, 40.0f * Global.ScreenHeight_Factor), "GETTING AROUND SPARX", "Titles");
			
			//GUI.Label(new Rect(150.0f, 135.0f, 36.0f, 36.0f), m_mouseCorsorImage);
			//GUI.Label(new Rect(200.0f, 135.0f, 655.0f, 150.0f), "If a rainbow cursor appears, click on the object to see what happens.");
			//GUI.Label(new Rect(150.0f, 185.0f, 705.0f, 150.0f), "If you are lost, explore the environment to see if this cursor appears.");
			//GUI.Label(new Rect(150.0f, 245.0f, 705.0f, 150.0f), "Use the Arrow keys or WASD keys to walk around.");
			//GUI.Label(new Rect(150.0f, 300.0f, 705.0f, 150.0f), "If you want to run, hold the shift button while walking.");
			
			//GUI.Label(new Rect(130.0f, 260.0f, 36.0f, 36.0f), m_closeImage);
			//GUI.Label(new Rect(180.0f, 260.0f, 655.0f, 150.0f), "If you want to leave at any time you can press this key on the keyboard or the exit icon in the top right corner of the screen. When you come back you will start you from the beginning of that level again.");
			
			//GUI.Label(new Rect(150.0f, 185.0f, 36.0f, 36.0f), m_backPackImage);
			//GUI.Label(new Rect(200.0f, 185.0f, 655.0f, 150.0f), "Your backpack has these instructions and other useful things like your Notebook.");
			
			//GUI.Label(new Rect(150.0f, 245.0f, 36.0f, 36.0f), m_sparxManulImage);
			//GUI.Label(new Rect(200.0f, 245.0f, 655.0f, 150.0f), "Your Notebook is for you to use. It has summaries of each Level and things you write in it.");

			
			///GUI.Label(new Rect(150.0f, 300.0f, 655.0f, 150.0f), "You can use the Left and Right Arrow keys to move forward and back in conversations.");
			//GUI.Label(new Rect(150.0f, 355.0f, 655.0f, 150.0f), "You can also use A,B,C,D or 1,2,3,4 for multichoice questions.");


			//GUI.Label(new Rect(150.0f, 355.0f, 80.0f, 80.0f), loadingIcon);
			//GUI.Label(new Rect(210.0f, 355.0f, 655.0f, 150.0f), "This symbol indicates the game is auto-saving your progress.");


			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 195.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_backPackImage);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 195.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Your backpack has these instructions and other useful things like your Notebook.");

			//GUI.Label(new Rect(150.0f, 425.0f, 36.0f, 36.0f), m_sparxManulImage);
			//GUI.Label(new Rect(200.0f, 425.0f, 655.0f, 150.0f), "Your Notebook is for you to use. It has summaries of each Level and things you write in it.");
			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_sparxManulImage);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Your Notebook is for you to use. It has summaries of each Level and things you write in it.");

			//GUI.Label(new Rect(150.0f, 480.0f, 655.0f, 150.0f), "You can use the Left and Right Arrow keys to move forward and back in conversations.");
			//GUI.Label(new Rect(200.0f, 525.0f, 655.0f, 150.0f), "You can also use A,B,C,D or 1,2,3,4 for multichoice questions.");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Eagle);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Ride Hoikio: Stand up near Hoikio and touch it.");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Gnat);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Shoot gnats: Touch the gnat.");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Latern);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Turn lantern on: Stand up close to the lantern and touch it");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 450.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Lader);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 450.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Climb laders: Stand up in fron of the lader and touch it.");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 500.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), loadingIcon);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 500.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "This symbol indicates the game is auto-saving your progress.");
	
			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 550.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Control);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 550.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Use this button to control the character (forward, backward, right and left).");

			GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 600.0f * Global.ScreenHeight_Factor, 36.0f * Global.ScreenWidth_Factor, 36.0f * Global.ScreenHeight_Factor), m_Control2);
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 600.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "Use this button to run.");

			//Next button
			if(Time.timeSinceLevelLoad > 2f)
			{
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "GetAroundSparx" + "_OnGUI2");
					GameObject.Find ("guardian").GetComponent<CharacterInteraction>().InteractionRadius = 6;
					this.enabled = false;
				}
			}
			/*else
			{
				fTime += Time.deltaTime;
			}*/
		}
		else{
			this.enabled = false;
		}
	}
}
