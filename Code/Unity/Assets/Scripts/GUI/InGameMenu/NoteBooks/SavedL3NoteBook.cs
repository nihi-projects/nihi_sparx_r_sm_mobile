using UnityEngine;
using System.Collections;

public class SavedL3NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		FontSizeManager.checkFontSize(m_skin);
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));

	}

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L3Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL3NoteBookDataBlockOne), 0 );
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL3NoteBookDataBlockTwo), 1);
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL3NoteBookDataBlockThree), 2);

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL3NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update () 
	{
		GameObject.Find ("Bag").GetComponent<Bag> ().m_bToggleShowItems = false;

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL3NoteBook" + "_Update2");
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 4)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL4NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }

    }
	
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
