using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class L6NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	string failedFileName = @"/failedRequests.txt";

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    private void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L6Notebook");
        menu.GetComponent<CanvasNotebook>().SetChangeText(L6NegativeStatement.m_sChoice, 0);
        for ( int i = 0; i < L6AnalyseNegativeStatement.m_sChoices.Length; ++i )
        {
            menu.GetComponent<CanvasNotebook>().SetChangeText(L6AnalyseNegativeStatement.m_sChoices[i], i+1);
        }
    }

    // Update is called once per frame
    void Update () 
	{
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            SaveL6NotebookData();

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
            //StartCoroutine(instance2.SaveUserProcessToServer_NewJsonSchema(6, severityScore));
            //StartCoroutine(testForConnectionFailed(6, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 6, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(6, feedbackData, "levelEnd", true));
        }
    }
	
	void OnGUI()
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "SWAP IT\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Thoughts are not facts. You can SWAP\nthem.\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Spot a negative thought and swap it for\nsomething better!", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 240.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "RAPA = 4 steps to SWAP a thought.\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "R=REALITY CHECK. How do you know\nyour thought is true?\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 330.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "A=ANOTHER VIEW. Is there another\nway to think about it?\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 380.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "P=PERSPECTIVE. Is it really as bad as\nyou think?\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "A=THINK 'ACTION! Think solutions,\nnot problems.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 490.0f * Global.ScreenHeight_Factor, 310.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "SORT IT\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 540.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Negotiate a deal. Listen, explain what you\nneed, give a little, take a little and aim for\n a compromise.", "NoteBookFont");

		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 85.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "A thought I'd like to swap:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 145.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "REALITY CHECK", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 205.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "ANOTHER VIEW", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 265.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "Put it into PERSPECTIVE", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 325.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "THINK ACTION!", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "What I would like to try out from today:", "NoteBookFont");
	
		//if (L6NegativeStatement.m_sChoice != "")
		//{
		//	GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 115.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L6NegativeStatement.m_sChoice, "NoteBookFont");
		//}
		
		//float y = 170.0f;
		//for (int i = 0; i < L6AnalyseNegativeStatement.m_sChoices.Length; ++i)
		//{
		//	GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L6AnalyseNegativeStatement.m_sChoices[i], "NoteBookFont");
		//	y += 60;
		//}
		
		//text1 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 425.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text1, 32);
		//text2 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 455.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text2, 32);
		//text3 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 485.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text3, 32);
	}

	void SaveL6NotebookData()
	{
		Global.userL6NoteBookDataBlocks[0] = L6NegativeStatement.m_sChoice;

		for (int i = 0; i < L6AnalyseNegativeStatement.m_sChoices.Length; ++i)
		{
			Global.userL6NoteBookRapaBlocks[m_iIndex] = L6AnalyseNegativeStatement.m_sChoices[i];
			m_iIndex++;
		}
		
		Global.userL6NoteBookManualBlocks [0] = menu.GetComponent<CanvasNotebook>().GetEnterText(0);
		Global.userL6NoteBookManualBlocks [1] = menu.GetComponent<CanvasNotebook>().GetEnterText(1);
		Global.userL6NoteBookManualBlocks [2] = menu.GetComponent<CanvasNotebook>().GetEnterText(2);
	}
	/*
	public IEnumerator testForConnectionFailed(int _levelNum, int _userFeedbackScore){ // (Connection, WhatRL, wwwForm)
		//subFunctionTestAddingSavePointstoFile(whatURL);//Fill the 
		Debug.Log("this is running");

		if( string.IsNullOrEmpty(instance.m_sDynamicURL) || string.IsNullOrEmpty(instance.m_sGameDynamicToken)){

			instance.m_logInDataRetriever = null;
			instance.m_dynamiURLRetriever = null;

			//Get token from fixed username and password
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever == null){
					yield return StartCoroutine(instance.GetUserLogInToServer_NewSchema());
				}
			//}

			//STATIC URL
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever.isDone ){
					instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
					Debug.Log("<Token from URL>");
				}
			//}

			if (instance.m_sGameLoginToken != "") {
				//if(!Application.isEditor && !m_bGotDynamicTokenDone){//CHANGES_KEY_NEW_JSON_SCHEMA
				if (instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever == null) {//CHANGES_KEY_NEW_JSON_SCHEMA EDITOR AND WEBGL GET DYNAMIC URL 
					//Send off the gameConfigURL in order to get dynamic URL
					yield return StartCoroutine (instance.SendOffGameConfigURL_NewJsonSchema ("")); //EDITOR GETS DYNAMIC URL FROM SERVER
					Debug.Log ("<Token & Dynamic URL from Server>");
				}
			}

			if(instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever.isDone){
				//Clearing the Dynamic URL to make sure it has to take one 
				//For webplayer build only
				//if(!Application.isEditor){//CHANGES_KEY_NEW_JSON_SCHEMA
				//Debug.Log (">>>> TOKEN - IN");
				instance.GetTokenAndDynamicURL_NewJsonSchema ();
				//m_bGotDynamicURLDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
				//}//CHANGES_KEY_NEW_JSON_SCHEMA
			}

		}

		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm();
		saveForm.AddField("saveFake", "xyxyx");


		//string m_sSaveURL = instance.savePointURL_NewJsonSchema(_levelNum, _feedback, _savePoint, _levelComplete);//CHANGES_KEY_NEW_JSON_SCHEMA 
		//Debug.Log("saving Save point data. - "+_savePoint);

		//yield return new WaitForSeconds(3);
		Global.toggleGUI = false;

		var postScoreURL = instance.m_sDynamicURL + "UserResponse?sid=" + instance.m_sGameDynamicToken;
		var jsonString = instance.GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding ();

		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION
		string url = "";

		WWW www;

		//if(Application.isEditor){  
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken;
			www = new WWW(url);		
		/*}else{
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken; // This is a fixed URL Just to get the Dynamic
			www = new WWW(url);		
		}*/
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
	/*	yield return www;
		//Debug.Log ("The web page Dynamic url call is = " + _url);
		if (www.isDone) {
			Debug.Log ("<URL FOR CHECK AVAILABILITY OF CONNECTION>:" + www.text);
		}
		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION

		if(www.isDone){

			//if(Application.isMobilePlatform){
			Debug.Log("this is running is mobile ");
			Debug.Log("this is running connection is done");

			if(!string.IsNullOrEmpty(www.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();

				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log("<FAILED STRING IN FILE>" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			} else{
				if(File.Exists(Application.persistentDataPath+failedFileName)){
					Debug.Log("cGoingtoUploadScript");
					startFileUpload();

					//SENT THE L6AnalyseNegativeStatement UPDATE Because the server overwites the record
					Dictionary<string, string> postHeader = new Dictionary<string, string> ();
					postHeader ["Content-Type"] = "application/json";
					postHeader ["Content-Length"] = jsonString.Length.ToString ();
					WWW www2;

					yield return www2 = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
					/*
				yield return www = new WWW(jsonString, saveForm);
				*/
		/*			PlayerPrefs.SetString("savingpoint_playerprefs", jsonString);

					Debug.Log ("saving Save point data = " + postScoreURL);
					//SENT THE L6AnalyseNegativeStatement UPDATE

				}
			}
			//}
		}

	}

	public void startFileUpload(){ //TEST VERSION DIEGO
		int currentLineNum = -1;
		string[] allLines = new string[1];
		string fileName = "";
		WWW connection = null; 
		Debug.Log("IM RUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
		Debug.Log(reader.ToString());
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			//currentLineNum = allContent.Count;
			allContent.Add(content);
		}
		reader.Close();
		Debug.Log(allContent.Count);
		if(allContent.Count > 0){
			//fileName = newFileName;
			allLines = allContent.ToArray();
			foreach (string jsonStr in allLines){
				Debug.Log("REEEGGGGGGGG");
				Debug.Log("content: \n\n" + jsonStr.Replace(" ","") + "\n\n");
				StartCoroutine(instance.subConnect3_NewJsonSchema(jsonStr.Replace(" ","")));
				Debug.Log("REEEGGGGGGGG    ND");
			}
			currentLineNum = 0;
		}
		Debug.Log("ENDS RUUUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		File.Delete(Application.persistentDataPath+failedFileName);
		Debug.Log("File Deleted");
	}*/


}
