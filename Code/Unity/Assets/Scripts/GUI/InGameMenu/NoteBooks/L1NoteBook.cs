using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
public class L1NoteBook : MonoBehaviour 
{
	// publics
	//public GUISkin m_skin = null;
	
	// privates
	//private string text1 = "Click here to enter text";
	//private string text2 = "Click here to enter text";
	//private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	string failedFileName = @"/failedRequests.txt";

    GameObject menu;

	// Use this for initialization
	void Start () 
	{
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (menu == null)
            menu = TextDisplayCanvas.instance.ShowPrefab("L1Notebook");

        string str = "";
        for (int i = 0; i < DragAndDropWords.m_L1destinationText.Length; ++i)
        {
            if (i != 0)
                str += "\n";
            if (DragAndDropWords.m_L1destinationText[i] != "")
            {
                str += DragAndDropWords.m_L1destinationText[i];
            }
        }
        menu.GetComponent<CanvasNotebook>().SetChangeText(str);

        if ( menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu.gameObject);
            Global.arrowKey = "";
            SaveL1NotebookData();

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT (For the US developers, )	
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
            //StartCoroutine(testForConnectionFailed(1, severityScore));//24052016
            //StartCoroutine(instance.SaveUserProcessToServer_NewJsonSchema(1, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 1, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(1, feedbackData, "levelEnd", true));
        }
    }

	void SaveL1NotebookData()
	{
		for (int i = 0; i < DragAndDropWords.m_L1destinationText.Length; ++i){
			//if (DragAndDropWords.m_L1destinationText[i] != "" && DragAndDropWords.m_L1destinationText[i] != " " && DragAndDropWords.m_L1destinationText[i] != null){
			if (DragAndDropWords.m_L1destinationText[i] != null){
				//Save notbook data to server
				Global.userL1NoteBookDataBlocks[m_iIndex] = DragAndDropWords.m_L1destinationText[i];
				m_iIndex++;
			}
		}

		//Save notbook data to server
		Global.userL1NoteBookManualBlocks[0] = menu.GetComponent<CanvasNotebook>().GetEnterText(0);
		Global.userL1NoteBookManualBlocks[1] = menu.GetComponent<CanvasNotebook>().GetEnterText(1);
		Global.userL1NoteBookManualBlocks[2] = menu.GetComponent<CanvasNotebook>().GetEnterText(2);
	}
    
}
