using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class L4NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;

	
	// privates
	private string text1 = "Say what the problem is";
	private string text2 = "Think of solutions";
	private string text3 = "Examine each solution";
	private string text4 = "Pick one and try it";
	private string text5 = "See what happens";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	string failedFileName = @"/failedRequests.txt";

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L4Notebook");
    }


    // Update is called once per frame
    void Update () 
	{
        //menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(L4DragDropWords2.m_L4destinationText), 0);
        menu.GetComponent<CanvasNotebook>().SetChangeText(L4EnterTexts.m_inputTexts);
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(L4DragDropWords.m_L4destinationText), 1);

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            SaveL4NotebookData();

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
            //StartCoroutine(instance2.SaveUserProcessToServer_NewJsonSchema(4, severityScore));
            //StartCoroutine(testForConnectionFailed(4, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 4, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(4, feedbackData, "levelEnd", true));

        }
    }
	
	void OnGUI()
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
/*		GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 85.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "SOLVE IT:\n" +
														"-When you're down, problems seem like\nmountains - too hard to climb.", "NoteBookFont");
		GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "SOLVE IT WITH STEPS:\n" +
														"-SAY what the problem is.\nBe clear and specific as you can.\n" +
														"-THINK of solutions. Lots of different\nones. Even ones you wouldn't do.\n" +
														"-EXAMINE these ideas - look  at the\npros and cons of the best ones.\n" +
														"-PICK one and try it!\n" +
														"-SEE what happens. Try again if that\nsolution didn't work.", "NoteBookFont");
		GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 390.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor), "SPOT IT\n" +
														 "-Sparks are positive or helpful thoughts\nabout you and your future. They make you \nfeel good.\n" +
														 "-Spot Sparks in your life. Believe them.\nKeep them.", "NoteBookFont");

		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 85.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My problem is:", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 155.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "Im going to figure it out using STEPS:", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "S", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "T", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 225.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "E", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "P", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 275.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "S", "NoteBookFont");
		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 320.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My Sparks are:", "NoteBookFont");
*/		
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 140.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "SOLVE IT:\n" +
		//	"\n-When you're down, problems seem like\nmountains - too hard to climb.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "SOLVE IT WITH STEPS:\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "-SAY what the problem is.\nBe clear and specific as you can.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 240.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "-THINK of solutions. Lots of different\nones. Even ones you wouldn't do.\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 290.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "-EXAMINE these ideas - look  at the\npros and cons of the best ones.\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 330.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "-PICK one and try it!\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 360.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor), "-SEE what happens. Try again if that\nsolution didn't work." , "NoteBookFont");


		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 520.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor), "SPOT IT\n" +
		//	"\n-Sparks are positive or helpful thoughts\nabout you and your future. They make you \nfeel good.\n" +
		//	"\n-Spot Sparks in your life. Believe them.\nKeep them.", "NoteBookFont");

		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My problem is:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "Im going to figure it out using STEPS:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "S", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 240.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "T", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 270.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "E", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "P", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 330.0f * Global.ScreenHeight_Factor, 20.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "S", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 380.0f * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My Sparks are:", "NoteBookFont");
				
		//float y = 140.0f;
		//if (L4EnterTexts.m_bTriggered == false)
		//{
		//	for (int i = 0; i < L4DragDropWords2.m_L4destinationText.Length; ++i)//6 text
		//	{
		//		if (L4DragDropWords2.m_L4destinationText[i] != "")
		//		{
		//			GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 200.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L4DragDropWords2.m_L4destinationText[i], "NoteBookFont");
		//			y += 30.0f;
		//		}
		//	}
		//}
		//else
		//{
		//	GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L4EnterTexts.m_inputTexts, "NoteBookFont");//1 text
		//}
		
		//text1 = GUI.TextField(new Rect(570.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text1, 32);//5 text
		//text2 = GUI.TextField(new Rect(570.0f * Global.ScreenWidth_Factor, 240.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text2, 32);
		//text3 = GUI.TextField(new Rect(570.0f * Global.ScreenWidth_Factor, 270.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text3, 32);
		//text4 = GUI.TextField(new Rect(570.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text4, 32);
		//text5 = GUI.TextField(new Rect(570.0f * Global.ScreenWidth_Factor, 330.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text5, 32);
		
		//y = 410.0f;
		//for (int i = 0; i < L4DragDropWords.m_L4destinationText.Length; ++i)//6 text
		//{
		//	if (L4DragDropWords.m_L4destinationText[i] != "")
		//	{
		//		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 190.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L4DragDropWords.m_L4destinationText[i], "NoteBookFont");
		//		y += 30.0f;
		//	}
		//}
		
		
	}

	void SaveL4NotebookData()
	{
		if (L4EnterTexts.m_bTriggered == false) {
			for (int i = 0; i < L4DragDropWords2.m_L4destinationText.Length; ++i)
			{
				if (L4DragDropWords2.m_L4destinationText[i] != "")
				{
					Debug.Log ("111111111111 TEXT HERE 111111111111");
					Debug.Log(L4DragDropWords2.m_L4destinationText[i]);
					Global.userL4NoteBookDataBlockOne[0] = L4DragDropWords2.m_L4destinationText[i];
				}
			}
		} else {
			Global.userL4NoteBookDataBlockOne[0] = L4EnterTexts.m_inputTexts;
		}

		Global.userL4NoteBookStepsBlocks[0] = menu.GetComponent<CanvasNotebook>().GetEnterText(0);
		Global.userL4NoteBookStepsBlocks[1] = menu.GetComponent<CanvasNotebook>().GetEnterText(1);
		Global.userL4NoteBookStepsBlocks[2] = menu.GetComponent<CanvasNotebook>().GetEnterText(2);
		Global.userL4NoteBookStepsBlocks[3] = menu.GetComponent<CanvasNotebook>().GetEnterText(3);
		Global.userL4NoteBookStepsBlocks[4] = menu.GetComponent<CanvasNotebook>().GetEnterText(4);

		for (int i = 0; i < L4DragDropWords.m_L4destinationText.Length; ++i)//6 text
		{
			//if (L4DragDropWords.m_L4destinationText[i] != "")
			//{
				Global.userL4NoteBookDataBlockTwo [m_iIndex] = L4DragDropWords.m_L4destinationText[i];
				m_iIndex++;
			//}
		}
	}
}
