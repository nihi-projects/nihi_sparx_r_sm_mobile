using UnityEngine;
using System.Collections;

public class SavedL7NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;
	public Texture2D m_tPicture;
	public Texture2D m_tShield;
	
	// Use this for initialization
	void Start () 
	{

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
		//m_tPicture  = (Texture2D)Resources.Load ("UI/cutscene_start_1", typeof(Texture2D));
		//m_tShield   = (Texture2D)Resources.Load ("UI/shield_icon", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject.Find ("Bag").GetComponent<Bag> ().m_bToggleShowItems = false;

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 80.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 105.0f, 300.0f, 100.0f), "WHEN YOU FEEL REALLY BAD:\n" +
		        "-Try another skill.\n-Carry on even though you feel down - the\n feelings will pass.\n" +
		        "-Ask for help.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 205.0f, 310.0f, 100.0f), "YOU MIGHT FEEL DOWN SOMETIMES:\n" +
		        "-Use the Shield - the sooner\nyou get out your skills\nand practise tham to get\nmore help - the better.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 315.0f, 310.0f, 255.0f), "IF YOU WANT MORE:\n" +
		        "-Talk to family or the person who\nintroduced you to SPARX.\n" +
		        "-Try professional help - a guidance\ncouncellor, doctor, mental health service\ncan help you with this. You can also contact Youthline 0508 4 SPARX (0508 477 279) or free text SPARX to 234 and Lifeline on 0508 4 SPARX (0508 477 279).\n" +
		        "-Other things that can help are:\nsorting out problems, regular exercise,\nrelaxation, meditation and other types of\ntherapy.", "NoteBookFont");
		
		GUI.Box(new Rect(560.0f, 80.0f, 300.0f, 25.0f), "What I would like to take from today:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 270.0f, 300.0f, 25.0f), "Good luck and thanks for doing SPARX!", "NoteBookFont");
		
		GUI.Label(new Rect(340.0f, 220.0f, 100.0f, 100.0f), m_tShield);
		GUI.Label(new Rect(560.0f, 290.0f, 300.0f, 250.0f), m_tPicture);
		
		if (L7TakingHome.m_tChoice != null)
		{
			GUI.Label(new Rect(595.0f, 110.0f, 140.0f, 140.0f), L7TakingHome.m_tChoice);
		}
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
