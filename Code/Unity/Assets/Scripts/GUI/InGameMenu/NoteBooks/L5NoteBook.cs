using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class L5NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	string failedFileName = @"/failedRequests.txt";

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    private void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L5Notebook");
        menu.GetComponent<CanvasNotebook>().SetChangeText( CanvasNotebook.ArrayToStringList(L5EnterTexts.m_inputTexts));
    }

    // Update is called once per frame
    void Update () 
	{
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            GameObject.Destroy(menu);
            Global.arrowKey = "";

            SaveL5NotebookData();

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
            //StartCoroutine(instance2.SaveUserProcessToServer_NewJsonSchema(5, severityScore));
            //StartCoroutine(testForConnectionFailed(5, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 5, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(5, feedbackData, "levelEnd", true));

        }
    }
	
	void OnGUI()
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 130.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "SPOT IT", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-Spot the Gnats (Gloomy Negative\n Automatic Thoughts).\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-Why? Because Gnats are toxic.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor), "HOW TO SPOT A GNAT?\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 230.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-DOWNER: Am I looking on the downside\n and overlooking the positive.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-PERFECTIONIST: Am I  expecting myself\n to be perfect?", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 330.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-MIND READER: Am I trying to read\n people's minds or predict what will hppen?", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 380.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-GUILTY: Am I thinking this is my fault\n even it's not up to me?", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-DISASTER: Am I making a bigger deal out\n of it than it really is?", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 480.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor),"-ALL-OR-NOTHING: Am I seeing things as\n extreme with nothing in between?", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 40.0f * Global.ScreenHeight_Factor), "I SPOT these annoying Gnats\n(Gloomy Negative Automatic Thoughts):", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 380.0f * Global.ScreenHeight_Factor, 250.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "Things that stood out for me today:", "NoteBookFont");

		//float y = 140.0f;
		//for (int i = 0; i < L5EnterTexts.m_inputTexts.Length; ++i)
		//{
		//	if (L5EnterTexts.m_inputTexts[i] != "Click here to enter text.")
		//	{
		//		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L5EnterTexts.m_inputTexts[i], "NoteBookFont");
		//		y += 30.0f;
		//	}
		//}
		//GUI.SetNextControlName ("text1");
		//text1 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 410.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text1, 32);
		//GUI.SetNextControlName ("text2");
		//text2 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text2, 32);
		//GUI.SetNextControlName ("text3");
		//text3 = GUI.TextField(new Rect(560.0f * Global.ScreenWidth_Factor, 470.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), text3, 32);

		//if (GUI.GetNameOfFocusedControl () != "") {
		//	text1 = Global.doTextFieldCheck("text1", text1, new Rect(560.0f * Global.ScreenWidth_Factor, 410.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor));
		//	text2 = Global.doTextFieldCheck("text2", text2, new Rect(560.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor));
		//	text3 = Global.doTextFieldCheck("text3", text3, new Rect(560.0f * Global.ScreenWidth_Factor, 470.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor));
		//}
		
	}

	void SaveL5NotebookData()
	{
		for (int i = 0; i < L5EnterTexts.m_inputTexts.Length; ++i)
		{
			if (L5EnterTexts.m_inputTexts [i] != "Click here to enter text.") {
				Global.userL5NoteBookDataBlocks [m_iIndex] = L5EnterTexts.m_inputTexts [i];
			} else {
				Global.userL5NoteBookDataBlocks [m_iIndex] = "";
			}
			m_iIndex++;
		}

		Global.userL5NoteBookManualBlocks [0] = menu.GetComponent<CanvasNotebook>().GetEnterText(0);
		Global.userL5NoteBookManualBlocks [1] = menu.GetComponent<CanvasNotebook>().GetEnterText(1);
		Global.userL5NoteBookManualBlocks [2] = menu.GetComponent<CanvasNotebook>().GetEnterText(2);
	}

/*	public IEnumerator testForConnectionFailed(int _levelNum, int _userFeedbackScore){ // (Connection, WhatRL, wwwForm)
		//subFunctionTestAddingSavePointstoFile(whatURL);//Fill the 
		Debug.Log("this is running");

		if( string.IsNullOrEmpty(instance.m_sDynamicURL) || string.IsNullOrEmpty(instance.m_sGameDynamicToken)){

			instance.m_logInDataRetriever = null;
			instance.m_dynamiURLRetriever = null;

			//Get token from fixed username and password
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever == null){
					yield return StartCoroutine(instance.GetUserLogInToServer_NewSchema());
				}
			//}

			//STATIC URL
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever.isDone ){
					instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
					Debug.Log("<Token from URL>");
				}
			//}

			if (instance.m_sGameLoginToken != "") {
				//if(!Application.isEditor && !m_bGotDynamicTokenDone){//CHANGES_KEY_NEW_JSON_SCHEMA
				if (instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever == null) {//CHANGES_KEY_NEW_JSON_SCHEMA EDITOR AND WEBGL GET DYNAMIC URL 
					//Send off the gameConfigURL in order to get dynamic URL
					yield return StartCoroutine (instance.SendOffGameConfigURL_NewJsonSchema ("")); //EDITOR GETS DYNAMIC URL FROM SERVER
					Debug.Log ("<Token & Dynamic URL from Server>");
				}
			}

			if(instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever.isDone){
				//Clearing the Dynamic URL to make sure it has to take one 
				//For webplayer build only
				//if(!Application.isEditor){//CHANGES_KEY_NEW_JSON_SCHEMA
				//Debug.Log (">>>> TOKEN - IN");
				instance.GetTokenAndDynamicURL_NewJsonSchema ();
				//m_bGotDynamicURLDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
				//}//CHANGES_KEY_NEW_JSON_SCHEMA
			}

		}

		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm();
		saveForm.AddField("saveFake", "xyxyx");


		//string m_sSaveURL = instance.savePointURL_NewJsonSchema(_levelNum, _feedback, _savePoint, _levelComplete);//CHANGES_KEY_NEW_JSON_SCHEMA 
		//Debug.Log("saving Save point data. - "+_savePoint);

		//yield return new WaitForSeconds(3);
		Global.toggleGUI = false;

		var postScoreURL = instance.m_sDynamicURL + "UserResponse?sid=" + instance.m_sGameDynamicToken;
		var jsonString = instance.GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding ();

		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION
		string url = "";

		WWW www;

		//if(Application.isEditor){  
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken;
			www = new WWW(url);		
		/*}else{
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken; // This is a fixed URL Just to get the Dynamic
			www = new WWW(url);		
		}*/
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
	/*	yield return www;
		//Debug.Log ("The web page Dynamic url call is = " + _url);
		if (www.isDone) {
			Debug.Log ("<URL FOR CHECK AVAILABILITY OF CONNECTION>:" + www.text);
		}
		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION

		if(www.isDone){

			//if(Application.isMobilePlatform){
			Debug.Log("this is running is mobile ");
			Debug.Log("this is running connection is done");

			if(!string.IsNullOrEmpty(www.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();

				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log("<FAILED STRING IN FILE>" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			} else{
				if(File.Exists(Application.persistentDataPath+failedFileName)){
					Debug.Log("cGoingtoUploadScript");
					startFileUpload();

					//SENT THE L6AnalyseNegativeStatement UPDATE Because the server overwites the record
					Dictionary<string, string> postHeader = new Dictionary<string, string> ();
					postHeader ["Content-Type"] = "application/json";
					postHeader ["Content-Length"] = jsonString.Length.ToString ();
					WWW www2;

					yield return www2 = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
					/*
				yield return www = new WWW(jsonString, saveForm);
				*/
	/*				PlayerPrefs.SetString("savingpoint_playerprefs", jsonString);

					Debug.Log ("saving Save point data = " + postScoreURL);
					//SENT THE L6AnalyseNegativeStatement UPDATE

				}
			}
			//}
		}

	}

	public void startFileUpload(){ //TEST VERSION DIEGO
		int currentLineNum = -1;
		string[] allLines = new string[1];
		string fileName = "";
		WWW connection = null; 
		Debug.Log("IM RUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
		Debug.Log(reader.ToString());
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			//currentLineNum = allContent.Count;
			allContent.Add(content);
		}
		reader.Close();
		Debug.Log(allContent.Count);
		if(allContent.Count > 0){
			//fileName = newFileName;
			allLines = allContent.ToArray();
			foreach (string jsonStr in allLines){
				Debug.Log("REEEGGGGGGGG");
				Debug.Log("content: \n\n" + jsonStr.Replace(" ","") + "\n\n");
				StartCoroutine(instance.subConnect3_NewJsonSchema(jsonStr.Replace(" ","")));
				Debug.Log("REEEGGGGGGGG    ND");
			}
			currentLineNum = 0;
		}
		Debug.Log("ENDS RUUUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		File.Delete(Application.persistentDataPath+failedFileName);
		Debug.Log("File Deleted");
	}*/


}
