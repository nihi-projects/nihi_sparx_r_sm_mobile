using UnityEngine;
using System.Collections;

public class SavedL2NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates

	
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L2Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL2NoteBookDataBlocks));

        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL2NoteBookManualBlocks[0], 0);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL2NoteBookManualBlocks[1], 1);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL2NoteBookManualBlocks[2], 2);

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL2NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update () 
	{
		GameObject.Find ("Bag").GetComponent<Bag> ().m_bToggleShowItems = false;

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL2NoteBook" + "_Update2");
            Destroy(menu);
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 3)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL3NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }
    }
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
