using UnityEngine;
using System.Collections;

public class SavedL6NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "";
	private string text2 = "";
	private string text3 = "";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L6Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL6NoteBookManualBlocks[0], 0);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL6NoteBookManualBlocks[1], 1);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL6NoteBookManualBlocks[2], 2);

        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL6NoteBookDataBlocks));

        for( int i = 0; i < 4; ++i )
        {
            menu.GetComponent<CanvasNotebook>().SetChangeText(Global.userL6NoteBookRapaBlocks[i],i+1);
        }

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL6NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update () 
	{
		GameObject.Find ("Bag").GetComponent<Bag> ().m_bToggleShowItems = false;


        if ( menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL6NoteBook" + "_Update2");
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 7)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL7NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }
    }
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
