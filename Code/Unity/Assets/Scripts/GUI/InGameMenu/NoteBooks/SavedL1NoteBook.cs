using UnityEngine;
using System.Collections;

public class SavedL1NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;


    // privates
    private GameObject menu;
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin      = (GUISkin) Resources.Load("Skins/SparxSkin");
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}

    private void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L1Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetChangeText( CanvasNotebook.ArrayToStringList(Global.userL1NoteBookDataBlocks) );

        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL1NoteBookManualBlocks[0], 0);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL1NoteBookManualBlocks[1], 1);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL1NoteBookManualBlocks[2], 2);

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL1NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update () 
	{
        if ( menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL1NoteBook" + "_OnEnable2");
            Destroy(menu);
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 2)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL2NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }
    }
}
