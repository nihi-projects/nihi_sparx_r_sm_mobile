using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class L7NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;


	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;
	public Texture2D m_tPicture;
	public Texture2D m_tShield;
	string failedFileName = @"/failedRequests.txt";

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
		//m_tPicture  = (Texture2D)Resources.Load ("UI/cutscene_start_1", typeof(Texture2D));
		//m_tShield   = (Texture2D)Resources.Load ("UI/shield_icon", typeof(Texture2D));
	}

    private void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L7Notebook");
        menu.GetComponent<CanvasNotebook>().SetImage(L7TakingHome.m_tChoice as Texture2D);
    }

    // Update is called once per frame
    void Update () 
	{
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
            //StartCoroutine(instance2.SaveUserProcessToServer_NewJsonSchema(7, severityScore));
            //StartCoroutine(testForConnectionFailed(7, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 7, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(7, feedbackData, "levelEnd", true));
        }

    }
	
	void OnGUI()
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "WHEN YOU FEEL REALLY BAD:\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Try another skill.\n-Carry on even though you feel down - the\n feelings will pass.\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Ask for help.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 230.0f * Global.ScreenHeight_Factor, 310.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "YOU MIGHT FEEL DOWN SOMETIMES:\n", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Use the Shield - the sooner you get out \nyour skills and practise them to get\nmore help - the better.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, 310.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "IF YOU WANT MORE:\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 390.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Talk to family or the person who\nintroduced you to SPARX.\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 480.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Try professional help - a guidance\ncouncellor, doctor, mental health service\ncan help you with this. You can also contact Youthline 0508 4 SPARX (0508 477 279) or free text SPARX to 234 and Lifeline on 0508 4 SPARX (0508 477 279).\n" , "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 580.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "-Other things that can help are:\nsorting out problems, regular exercise,\nrelaxation, meditation and other types of\ntherapy.", "NoteBookFont");

		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "What I would like to take from today:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 270.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "Good luck and thanks for doing SPARX!", "NoteBookFont");
		
		//GUI.Label(new Rect(360.0f * Global.ScreenWidth_Factor, 340.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), m_tShield);
		//GUI.Label(new Rect(560.0f * Global.ScreenWidth_Factor, 290.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor), m_tPicture);
		
		//if (L7TakingHome.m_tChoice != null)
		//{
		//	GUI.Label(new Rect(595.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 140.0f * Global.ScreenWidth_Factor, 140.0f * Global.ScreenHeight_Factor), L7TakingHome.m_tChoice);
		//}
		
		
	}

/*	public IEnumerator testForConnectionFailed(int _levelNum, int _userFeedbackScore){ // (Connection, WhatRL, wwwForm)
		//subFunctionTestAddingSavePointstoFile(whatURL);//Fill the 
		Debug.Log("this is running");

		if( string.IsNullOrEmpty(instance.m_sDynamicURL) || string.IsNullOrEmpty(instance.m_sGameDynamicToken)){

			instance.m_logInDataRetriever = null;
			instance.m_dynamiURLRetriever = null;

			//Get token from fixed username and password
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever == null){
					yield return StartCoroutine(instance.GetUserLogInToServer_NewSchema());
				}
			//}

			//STATIC URL
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever.isDone ){
					instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
					Debug.Log("<Token from URL>");
				}
			//}

			if (instance.m_sGameLoginToken != "") {
				//if(!Application.isEditor && !m_bGotDynamicTokenDone){//CHANGES_KEY_NEW_JSON_SCHEMA
				if (instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever == null) {//CHANGES_KEY_NEW_JSON_SCHEMA EDITOR AND WEBGL GET DYNAMIC URL 
					//Send off the gameConfigURL in order to get dynamic URL
					yield return StartCoroutine (instance.SendOffGameConfigURL_NewJsonSchema ("")); //EDITOR GETS DYNAMIC URL FROM SERVER
					Debug.Log ("<Token & Dynamic URL from Server>");
				}
			}

			if(instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever.isDone){
				//Clearing the Dynamic URL to make sure it has to take one 
				//For webplayer build only
				//if(!Application.isEditor){//CHANGES_KEY_NEW_JSON_SCHEMA
				//Debug.Log (">>>> TOKEN - IN");
				instance.GetTokenAndDynamicURL_NewJsonSchema ();
				//m_bGotDynamicURLDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
				//}//CHANGES_KEY_NEW_JSON_SCHEMA
			}

		}

		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm();
		saveForm.AddField("saveFake", "xyxyx");


		//string m_sSaveURL = instance.savePointURL_NewJsonSchema(_levelNum, _feedback, _savePoint, _levelComplete);//CHANGES_KEY_NEW_JSON_SCHEMA 
		//Debug.Log("saving Save point data. - "+_savePoint);

		//yield return new WaitForSeconds(3);
		Global.toggleGUI = false;

		var postScoreURL = instance.m_sDynamicURL + "UserResponse?sid=" + instance.m_sGameDynamicToken;
		var jsonString = instance.GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding ();

		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION
		string url = "";

		WWW www;

		//if(Application.isEditor){  
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken;
			www = new WWW(url);		
		/*}else{
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken; // This is a fixed URL Just to get the Dynamic
			www = new WWW(url);		
		}*/
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
	/*	yield return www;
		//Debug.Log ("The web page Dynamic url call is = " + _url);
		if (www.isDone) {
			Debug.Log ("<URL FOR CHECK AVAILABILITY OF CONNECTION>:" + www.text);
		}
		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION

		if(www.isDone){

			//if(Application.isMobilePlatform){
			Debug.Log("this is running is mobile ");
			Debug.Log("this is running connection is done");

			if(!string.IsNullOrEmpty(www.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();

				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log("<FAILED STRING IN FILE>" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			} else{
				if(File.Exists(Application.persistentDataPath+failedFileName)){
					Debug.Log("cGoingtoUploadScript");
					startFileUpload();

					//SENT THE L6AnalyseNegativeStatement UPDATE Because the server overwites the record
					Dictionary<string, string> postHeader = new Dictionary<string, string> ();
					postHeader ["Content-Type"] = "application/json";
					postHeader ["Content-Length"] = jsonString.Length.ToString ();
					WWW www2;

					yield return www2 = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
					/*
				yield return www = new WWW(jsonString, saveForm);
				*/
		/*			PlayerPrefs.SetString("savingpoint_playerprefs", jsonString);

					Debug.Log ("saving Save point data = " + postScoreURL);
					//SENT THE L6AnalyseNegativeStatement UPDATE

				}
			}
			//}
		}

	}

	public void startFileUpload(){ //TEST VERSION DIEGO
		int currentLineNum = -1;
		string[] allLines = new string[1];
		string fileName = "";
		WWW connection = null; 
		Debug.Log("IM RUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
		Debug.Log(reader.ToString());
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			//currentLineNum = allContent.Count;
			allContent.Add(content);
		}
		reader.Close();
		Debug.Log(allContent.Count);
		if(allContent.Count > 0){
			//fileName = newFileName;
			allLines = allContent.ToArray();
			foreach (string jsonStr in allLines){
				Debug.Log("REEEGGGGGGGG");
				Debug.Log("content: \n\n" + jsonStr.Replace(" ","") + "\n\n");
				StartCoroutine(instance.subConnect3_NewJsonSchema(jsonStr.Replace(" ","")));
				Debug.Log("REEEGGGGGGGG    ND");
			}
			currentLineNum = 0;
		}
		Debug.Log("ENDS RUUUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		File.Delete(Application.persistentDataPath+failedFileName);
		Debug.Log("File Deleted");
	}*/


}
