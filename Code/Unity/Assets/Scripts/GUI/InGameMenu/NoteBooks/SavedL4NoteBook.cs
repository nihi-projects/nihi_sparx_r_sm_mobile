using UnityEngine;
using System.Collections;

public class SavedL4NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;


	// privates
	private string text1 = "Say what the problem is";
	private string text2 = "Think of solutions";
	private string text3 = "Examine each solution";
	private string text4 = "Pick one and try it";
	private string text5 = "See what happens";

	private CapturedDataInput instance;

	public Texture2D m_tNoteBook;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");

		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L4Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL4NoteBookDataBlockOne), 0);
        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL4NoteBookDataBlockTwo), 1);

        for( int i = 0; i < 5; ++i )
            menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL4NoteBookStepsBlocks[i], i);

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL4NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update () 
	{
		GameObject.Find ("Bag").GetComponent<Bag> ().m_bToggleShowItems = false;

        //if(Global.CurrentLevelNumber > 5 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL4NoteBook" + "_Update2");
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 5)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL5NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }
    }

	

	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{

	}
}
