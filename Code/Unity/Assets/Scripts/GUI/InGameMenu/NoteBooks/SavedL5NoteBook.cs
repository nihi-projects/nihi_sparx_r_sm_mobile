using UnityEngine;
using System.Collections;

public class SavedL5NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "";
	private string text2 = "";
	private string text3 = "";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
    }

    public void OnEnable()
    {
        menu = TextDisplayCanvas.instance.ShowPrefab("L5Notebook");
        menu.GetComponent<CanvasNotebook>().DisableInputFields();
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL5NoteBookManualBlocks[0], 0);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL5NoteBookManualBlocks[1], 1);
        menu.GetComponent<CanvasNotebook>().SetInputField(Global.userL5NoteBookManualBlocks[2], 2);

        menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(Global.userL5NoteBookDataBlocks));
            

        Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SavedL5NoteBook" + "_OnEnable");
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Bag").GetComponent<Bag>().m_bToggleShowItems = false;

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SavedL5NoteBook" + "_Update2");
            Global.arrowKey = "";

            if (Global.CurrentLevelNumber > 6)
            {
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<SavedL6NoteBook>().enabled = true;
            }
            else
            {
                this.enabled = false;
                if (GameObject.Find("GUI") != null)
                {
                    GameObject.Find("Bag").GetComponent<Bag>().m_bNotebookUp = false;
                }
            }
        }
    }

	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
