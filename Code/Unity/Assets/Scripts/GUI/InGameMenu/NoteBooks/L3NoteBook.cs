using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
public class L3NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	string failedFileName = @"/failedRequests.txt";

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}

    public void OnEnable()
    {
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("L3Notebook");
            menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(L3DragDropWords3.m_L3destinationText), 0);
            menu.GetComponent<CanvasNotebook>().SetChangeText(CanvasNotebook.ArrayToStringList(L3DragDropWords4.m_L3destinationText), 1);
            menu.GetComponent<CanvasNotebook>().SetChangeText(L3DragDropWords5.m_L3destinationText[0], 2);
        }
    }

    // Update is called once per frame
    void Update () 
	{
        if ( menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            SaveL3NotebookData();

            this.enabled = false;
            GameObject.Find("GUI").GetComponent<HelpLine>().enabled = true;

            //SAVE LEVEL END EVENT
            StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));

            //SAVE PROGRESS
            int severityScore = 0;
            if (Global.CurrentLevelNumber == 1) { severityScore = Global.userFeedBackScore[0]; }
            if (Global.CurrentLevelNumber == 4) { severityScore = Global.userFeedBackScore[1]; }
            if (Global.CurrentLevelNumber == 7) { severityScore = Global.userFeedBackScore[2]; }

            string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
            //CapturedDataInput instance2 = CapturedDataInput.GetInstance;	
            //StartCoroutine(instance2.SaveUserProcessToServer_NewJsonSchema(3, severityScore));
            //StartCoroutine(testForConnectionFailed(3, severityScore));
            StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5, 3, "levelEnd"));
            StartCoroutine(instance.SaveOnServerOrLocal(3, feedbackData, "levelEnd", true));
        }

    }
	
	void OnGUI()
	{
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "In a nutshell", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 130.0f * Global.ScreenHeight_Factor, 100.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "SPOT IT", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 160.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 40.0f * Global.ScreenHeight_Factor), "Spot feelings of anger or hurt and choose\nhow you react.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 220.0f * Global.ScreenHeight_Factor, 320.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "Level 1: DISTRACTION (walk away; count;\nlisten to music; make up rhymes;\ntake time out).", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 310.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 40.0f * Global.ScreenHeight_Factor), "Level 2: STOP IT; TRASH IT,\nTURN IT DOWN.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 370.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "Level 3: SORT IT! If it's a real problem,\ncalm down, pick a good time and\nSORT IT.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor, 250.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor), "LISTEN WITH BLINC \nBite your tongue \nLook at the speaker\nbe Interested \nNo interruptions \nCheck you understand", "NoteBookFont");
		//GUI.Box(new Rect(150.0f * Global.ScreenWidth_Factor, 580.0f * Global.ScreenHeight_Factor, 400.0f * Global.ScreenWidth_Factor, 60.0f * Global.ScreenHeight_Factor), "BE ASSERTIVE \nNot aggro, not a push over. Say what you\nneed in a calm way.", "NoteBookFont");

		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 85.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My triggers are:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor, 180.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "My distraction skills:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor, 280.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), "What I would like to try out from today:", "NoteBookFont");
		//GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, 520.0f * Global.ScreenHeight_Factor, 300.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), "Its OK to have strong feelings but it's what\nyou do with the feelings that matter.\nDon't hurt yourself, hurt others, or damage\n other people's property.", "NoteBookFont");

		//float y = 110.0f;
		//for (int i = 0; i < L3DragDropWords3.m_L3destinationText.Length; ++i)
		//{
		//	if (L3DragDropWords3.m_L3destinationText[i] != "")
		//	{
		//		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L3DragDropWords3.m_L3destinationText[i], "NoteBookFont");
		//		y += 25.0f;
		//	}
		//}
		
		//y = 295.0f;
		//for (int i = 0; i < L3DragDropWords4.m_L3destinationText.Length; ++i)
		//{
		//	if (L3DragDropWords4.m_L3destinationText[i] != "")
		//	{
		//		GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L3DragDropWords4.m_L3destinationText[i], "NoteBookFont");
		//		y += 25.0f;
		//	}
		//}
		
		//y = 480.0f;
		//if (L3DragDropWords5.m_L3destinationText[0] != "")
		//{
		//	GUI.Box(new Rect(560.0f * Global.ScreenWidth_Factor, y * Global.ScreenHeight_Factor, 240.0f * Global.ScreenWidth_Factor, 25.0f * Global.ScreenHeight_Factor), L3DragDropWords5.m_L3destinationText[0], "NoteBookFont");
		//	y += 25.0f;
		//}
		
		
	}

	void SaveL3NotebookData()
	{
		for (int i = 0; i < L3DragDropWords3.m_L3destinationText.Length; ++i)
		{
			//if (L3DragDropWords3.m_L3destinationText[i] != "")
			//{
				Global.userL3NoteBookDataBlockOne[i] = L3DragDropWords3.m_L3destinationText[i];
			//}else{
			//	Global.userL3NoteBookDataBlockOne[i] = "";
			//}
				
		}

		for (int i = 0; i < L3DragDropWords4.m_L3destinationText.Length; ++i)
		{
			//if (L3DragDropWords4.m_L3destinationText[i] != "")
			//{
				Global.userL3NoteBookDataBlockTwo[i] = L3DragDropWords4.m_L3destinationText[i];
			//}else{
			//	Global.userL3NoteBookDataBlockTwo[i] = "";
			//}
		}

		//if (L3DragDropWords5.m_L3destinationText[0] != "")
		//{
			Global.userL3NoteBookDataBlockThree[0] = L3DragDropWords5.m_L3destinationText[0];
		//}else{
		//	Global.userL3NoteBookDataBlockThree[0] = "";
		//}
	}
	/*
	public IEnumerator testForConnectionFailed(int _levelNum, int _userFeedbackScore){ // (Connection, WhatRL, wwwForm)
		//subFunctionTestAddingSavePointstoFile(whatURL);//Fill the 
		Debug.Log("this is running");

		if( string.IsNullOrEmpty(instance.m_sDynamicURL) || string.IsNullOrEmpty(instance.m_sGameDynamicToken)){

			instance.m_logInDataRetriever = null;
			instance.m_dynamiURLRetriever = null;

			//Get token from fixed username and password
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever == null){
					yield return StartCoroutine(instance.GetUserLogInToServer_NewSchema());
				}
			//}

			//STATIC URL
			//if(Application.isEditor || Application.isMobilePlatform){
				if(instance.m_logInDataRetriever.isDone ){
					instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
					Debug.Log("<Token from URL>");
				}
			//}

			if (instance.m_sGameLoginToken != "") {
				//if(!Application.isEditor && !m_bGotDynamicTokenDone){//CHANGES_KEY_NEW_JSON_SCHEMA
				if (instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever == null) {//CHANGES_KEY_NEW_JSON_SCHEMA EDITOR AND WEBGL GET DYNAMIC URL 
					//Send off the gameConfigURL in order to get dynamic URL
					yield return StartCoroutine (instance.SendOffGameConfigURL_NewJsonSchema ("")); //EDITOR GETS DYNAMIC URL FROM SERVER
					Debug.Log ("<Token & Dynamic URL from Server>");
				}
			}

			if(instance.m_logInDataRetriever.isDone && instance.m_dynamiURLRetriever.isDone){
				//Clearing the Dynamic URL to make sure it has to take one 
				//For webplayer build only
				//if(!Application.isEditor){//CHANGES_KEY_NEW_JSON_SCHEMA
				//Debug.Log (">>>> TOKEN - IN");
				instance.GetTokenAndDynamicURL_NewJsonSchema ();
				//m_bGotDynamicURLDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
				//}//CHANGES_KEY_NEW_JSON_SCHEMA
			}

		}

		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm();
		saveForm.AddField("saveFake", "xyxyx");


		//string m_sSaveURL = instance.savePointURL_NewJsonSchema(_levelNum, _feedback, _savePoint, _levelComplete);//CHANGES_KEY_NEW_JSON_SCHEMA 
		//Debug.Log("saving Save point data. - "+_savePoint);

		//yield return new WaitForSeconds(3);
		Global.toggleGUI = false;

		var postScoreURL = instance.m_sDynamicURL + "UserResponse?sid=" + instance.m_sGameDynamicToken;
		var jsonString = instance.GetUserProcessPostURL_NewJsonSchema(_levelNum);
		var encoding = new System.Text.UTF8Encoding ();

		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION
		string url = "";

		WWW www;

		//if(Application.isEditor){  
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken;
			www = new WWW(url);		
		/*}else{
			url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + instance.m_sGameLoginToken; // This is a fixed URL Just to get the Dynamic
			www = new WWW(url);		
		}*/
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
	/*	yield return www;
		//Debug.Log ("The web page Dynamic url call is = " + _url);
		if (www.isDone) {
			Debug.Log ("<URL FOR CHECK AVAILABILITY OF CONNECTION>:" + www.text);
		}
		//ROUTINE JUST TO CHECK AVAILABILITY OF CONNECTION

		if(www.isDone){

			//if(Application.isMobilePlatform){
			Debug.Log("this is running is mobile ");
			Debug.Log("this is running connection is done");

			if(!string.IsNullOrEmpty(www.error)){
				StreamWriter writer = new StreamWriter(Application.persistentDataPath+failedFileName, true);
				writer.WriteLine(jsonString);
				writer.Close();

				Debug.Log("starting fileRead - connection failed");
				StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
				string content;
				while(null != (content = reader.ReadLine())){
					Debug.Log("<FAILED STRING IN FILE>" + content);
				}
				Debug.Log("fileRead finished");
				reader.Close();
			} else{
				if(File.Exists(Application.persistentDataPath+failedFileName)){
					Debug.Log("cGoingtoUploadScript");
					startFileUpload();

					//SENT THE L6AnalyseNegativeStatement UPDATE Because the server overwites the record
					Dictionary<string, string> postHeader = new Dictionary<string, string> ();
					postHeader ["Content-Type"] = "application/json";
					postHeader ["Content-Length"] = jsonString.Length.ToString ();
					WWW www2;

					yield return www2 = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
					/*
				yield return www = new WWW(jsonString, saveForm);
				*/
	/*				PlayerPrefs.SetString("savingpoint_playerprefs", jsonString);

					Debug.Log ("saving Save point data = " + postScoreURL);
					//SENT THE L6AnalyseNegativeStatement UPDATE

				}
			}
			//}
		}

	}

	public void startFileUpload(){ //TEST VERSION DIEGO
		int currentLineNum = -1;
		string[] allLines = new string[1];
		string fileName = "";
		WWW connection = null; 
		Debug.Log("IM RUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		StreamReader reader = new StreamReader(Application.persistentDataPath+failedFileName);
		Debug.Log(reader.ToString());
		List<string> allContent = new List<string>();
		string content;
		while(null != (content = reader.ReadLine())){
			//currentLineNum = allContent.Count;
			allContent.Add(content);
		}
		reader.Close();
		Debug.Log(allContent.Count);
		if(allContent.Count > 0){
			//fileName = newFileName;
			allLines = allContent.ToArray();
			foreach (string jsonStr in allLines){
				Debug.Log("REEEGGGGGGGG");
				Debug.Log("content: \n\n" + jsonStr.Replace(" ","") + "\n\n");
				StartCoroutine(instance.subConnect3_NewJsonSchema(jsonStr.Replace(" ","")));
				Debug.Log("REEEGGGGGGGG    ND");
			}
			currentLineNum = 0;
		}
		Debug.Log("ENDS RUUUNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNING");
		File.Delete(Application.persistentDataPath+failedFileName);
		Debug.Log("File Deleted");
	}*/


}
