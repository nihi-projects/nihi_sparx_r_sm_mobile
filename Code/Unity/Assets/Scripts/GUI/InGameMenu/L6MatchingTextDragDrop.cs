using UnityEngine;
using System.Collections;

public class L6MatchingTextDragDrop : MonoBehaviour 
{
	private string[] m_destinationText = 
	{
		"'Am I making a bigger deal out \nof this than it really is?'", 
		"'Am I thinking things are my fault even \nwhen it's not up to me?'", 
		"'Am I just looking at the downside and \noverlooking the positive?'", 
		"'Am I seeing things as extremes with no \nmiddle ground?'", 
		"'Am I trying to read people's minds?'", 
		"'Am I expecting myself to be perfect?'"
	};

    string[] draggables = new string[] { "Disaster", "Guilty", "Downer", "All-or-Nothing", "Mind Reader", "Perfectionist" };

    public GameObject menu;

    // Update is called once per frame
    void Update()
    {
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("MatchGnats");
            MatchGnatsScreen mgs = menu.GetComponent<MatchGnatsScreen>();
            mgs.SetText(m_destinationText, draggables);
        }

        //The next button
        if (menu.GetComponent<MatchGnatsScreen>().isFinished == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";

                this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene12_2");
			}
		}
	}
}
