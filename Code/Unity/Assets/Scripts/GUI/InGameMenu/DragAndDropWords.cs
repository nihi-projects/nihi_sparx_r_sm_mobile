using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DragAndDropWords : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin;
	
	// privates
	private string m_conversationBoxText = "";
	
	static private float m_keywordWidth = 100.0f * Global.ScreenWidth_Factor;
	static private float m_keywordHeight = 30.0f * Global.ScreenHeight_Factor;
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Joylessness
		new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Moodiness
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Anger
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Loneliness
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Aches
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Tiredness
		new Vector2(130.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Hopelessness
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Hunger
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Sadness
		new Vector2(90.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // No Energy
	};
	
	private Rect[] m_myOriRect; 
	
	private string[] m_keyWordText = {"Hopeless", "No fun", "Angry", "Negative", "Sad", "Moody", "Lonely", "Getting into trouble", "Fights",
                                      "Sleeping", "Eating", "Tiredness", "Aches", "Stressed"};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_dextinationRectHeight = 35.0f * Global.ScreenHeight_Factor;
	
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};

	static public string[] m_L1destinationText = {"", "", "", "", "", ""};

	private Rect[] m_destinationRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f	* Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;

    private GameObject menu;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		Global.levelStarted = false;
		 m_myOriRect = new Rect[] 
		{	new Rect(680.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // Joylessness
			new Rect(680.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // Moodiness
			new Rect(680.0f * Global.ScreenWidth_Factor, 205.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // Anger
			new Rect(680.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Loneliness
			new Rect(680.0f * Global.ScreenWidth_Factor, 370.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Aches
			new Rect(180.0f * Global.ScreenWidth_Factor, 370.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Tiredness
			new Rect(180.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Hopelessness
			new Rect(180.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[7].x, m_KeywordDimensions[7].y), // Hunger
			new Rect(180.0f * Global.ScreenWidth_Factor, 205.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[8].x, m_KeywordDimensions[8].y), // Sadness
			new Rect(180.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor , m_KeywordDimensions[9].x, m_KeywordDimensions[9].y) // No Energy
		};
		
		m_movableStringRect = new Rect[m_myOriRect.Length];
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			m_movableStringRect[i] = m_myOriRect[i];
		}
		
		m_currentClickedStringRect = k_zeroRect;
		FontSizeManager.checkFontSize(m_skin);
        //m_tFillFieldBackground    = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
        //m_tDestinationBackground  = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
        //m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));

        
    }
	
	// Update is called once per frame
	void Update () 
	{
        if( menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("ListScreen");
            menu.GetComponent<ListScreen>().SetupListItems(m_keyWordText);
            menu.GetComponent<ListScreen>().SetTitle("Which of These Affect You?");

            string[] descriptions = new string[m_keyWordText.Length];
            for (int i = 0; i < m_keyWordText.Length; ++i)
            {
                descriptions[i] = WordsHoverOverText(m_keyWordText[i]);
            }
            menu.GetComponent<ListScreen>().SetupDescriptions(descriptions);
        }

        menu.GetComponent<ListScreen>().SetDescription(WordsHoverOverText(""));
		//The default conversation box text
		//m_conversationBoxText = WordsHoverOverText("");
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		        //m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
                menu.GetComponent<ListScreen>().SetDescription(WordsHoverOverText(m_keyWordText[i]));
                break;
            }
		}
		
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												   m_KeywordDimensions[m_currentIndex].x, 
												   m_KeywordDimensions[m_currentIndex].y);
				
				m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
			}
		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	public void OnGUI () 
	{
		//GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		// The middle area
		//GUI.Label(new Rect(330 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 300 * Global.ScreenWidth_Factor, 300 * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
		//GUI.Label(new Rect(345 * Global.ScreenWidth_Factor, 145 * Global.ScreenHeight_Factor, 270 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "Which of these affect you?", "Titles");
		//bool done = false;
		//for (int i = 0; i < m_destinationRect.Length; ++i)
		//{
		//	GUI.DrawTexture(m_destinationRect[i], m_tFillFieldBackground);
		//	GUI.Label(m_destinationOriKeyWordRect[i], m_L1destinationText[i], "CenteredFont");
		//	//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
		//	//GUI.Box(m_destinationOriKeyWordRect[i], m_L1destinationText[i]);
			
		//	if (m_L1destinationText[i] != "")
		//	{
		//		done = true;
		//	}
		//}
		
		// the circle of words
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	//GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//	GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//	//GUI.Box(m_myOriRect[i], "");
		//}
		
		//// The current movable keyword box and text
		//if (m_currentClickedString != "")
		//{
		//	GUI.Label(m_currentClickedStringRect, m_currentClickedString, "CenteredFont");
		//	//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
		//}
		
		//Conversation box
		//GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);
		//GUI.Label(new Rect(300.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 360.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");
		
		//The next button
		if (true == menu.GetComponent<ListScreen>().bReadyToContinue == true )
		{
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                GameObject.Destroy(menu);

                Global.arrowKey = "";

                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene18");
            }
		}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_myOriRect.Length; ++i)
			{
				if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentClickedString == "")
					{
						// if the keyword does not exist that means that it is in one of the destination boxes
						if (m_keyWordText[i] == "")
						{
							// loop through the destination boxes to find which contains the key word
							for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
							{
								// set the min and max of the box
								Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
								Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
								// check if this destination box contains the current movable box
								if(m_destinationOriKeyWordRect[j].Contains(min) &&
									m_destinationOriKeyWordRect[j].Contains(max))
								{
									// set the destination text and remove the clicked text.
									m_keyWordText[i] = m_L1destinationText[j];
									m_L1destinationText[j] = "";
									break;
								}
							}
						}
						
						m_currentClickedString = m_keyWordText[i];
						m_keyWordText[i] = "";
						m_currentIndex = i;
						clicked = true;
						b_dragging = true;
						m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
        m_L1destinationText = menu.GetComponent<ListScreen>().GetChosenStrings();
		// release early if there is no active keyword box being moved.
		//if (strings.Count == 0) { return ;}

  //      m_L1destinationText = new string[strings.Count];
  //      for ( int i  =0; i< strings.Count; ++i )
  //      {
  //          m_L1destinationText[i] = strings[i];
  //      }

		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		//if(Input.GetMouseButtonUp(0))
		//{
		//	b_dragging = false;
			
		//	// boolean for if the text was placed in the box.
		//	bool successful = false;
			
		//	// loop through the destination rects to see which box the word is being placed in
		//	for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
		//	{
		//		// save the mouse pos for ease of access
		//		float mouseX = Input.mousePosition.x;
		//		float mouseY = Input.mousePosition.y;
				
		//		// check if this destination box contains the current clicked keyword box
		//		if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
		//		{
		//			// if the destination text is empty
		//			if(m_L1destinationText[i] == "")
		//			{
		//				// set the destination text and remove the clicked text.
		//				m_L1destinationText[i] = m_currentClickedString;
		//				m_currentClickedString = "";
		//				successful = true;
						
		//				float width = (m_dextinationRectWidth - 2) * Global.ScreenWidth_Factor;
		//				float height = (m_dextinationRectHeight - 2)  * Global.ScreenHeight_Factor ;
		//				m_movableStringRect[m_currentIndex].Set(
		//					(m_destinationOriKeyWordRect[i].center.x - width/2) * Global.ScreenWidth_Factor, 
		//					(m_destinationOriKeyWordRect[i].center.y - height/2) * Global.ScreenHeight_Factor, 
		//					width, 
		//					height);

		//				m_currentClickedStringRect = k_zeroRect;
		//				m_currentIndex = -1;
		//			}
		//		}
		//	}
			
		//	// another early release because everything is done already
		//	if (successful) { return ;}
			
		//	// execute this code if the user missed the destination box.
		//	ResetCurrentString();
		//}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
		m_keyWordText[m_currentIndex] = m_currentClickedString;
		m_currentClickedStringRect = k_zeroRect;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keuWordExplaination = "";
           
		switch(_keyWord)
		{
            case "Hopeless":
                    keuWordExplaination = "I feel like nothing could ever change.";
				break;
            case "No fun":
                    keuWordExplaination = "I don’t enjoy doing things I used to like.";
				break;
            case "Angry":
                    keuWordExplaination = "I feel hoha, angry or irritated.";
				break;
            case "Negative":
                    keuWordExplaination = "I don’t like myself or the world around me.";
				break;
            case "Sad":
                    keuWordExplaination = "I cry or feel really down.";
				break;
            case "Moody":
                    keuWordExplaination = "My feelings go up and down a lot.";
				break;
            case "Lonely":
                    keuWordExplaination = " I feel cut off from others.";
				break;
            case "Getting into trouble":
                    keuWordExplaination = "I make bad decisions or get into trouble.";
				break;
            case "Fights":
                    keuWordExplaination = "I keep getting into fights or arguments.";
				break;
            case "Sleeping":
                    keuWordExplaination = "I sleep heaps or I can’t sleep at all.";
				break;
            case "Eating":
                    keuWordExplaination = "I’m not hungry or I feel like eating all the time.";
                break;
            case "Tiredness":
                    keuWordExplaination = "I have no energy and feel tired a lot.";
                break;
            case "Aches":
                    keuWordExplaination = "I get headaches, stomach pains, tense muscles etc.";
                break;
            case "Stressed":
                    keuWordExplaination = "I just can’t relax.";
                break;
			default:
			    keuWordExplaination = "You probably don't experience all of these.\nDrag and drop to the centre up to 6 you would most like " +
			    	"to change. This will be saved in your Notebook.";
				break;
		}
			
		return keuWordExplaination;
	}
}
