﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class SettingsScreen : MonoBehaviour {
	public GUISkin m_skin = null;
	public GameObject m_theGuardian;
	public GameObject m_theMontor;
	//Small icon images in the dialog box
	public Texture2D m_mouseCorsorImage;
	public Texture2D m_closeImage;
	public Texture2D m_backPackImage;
	public Texture2D m_UIControl;
	public Texture2D m_ControlImage;
	public Texture2D m_AcelaratorImage;
	public Texture2D loadingIcon;

	public Texture2D m_ButtonOn;
	public Texture2D m_ButtonOff;

	private GameObject m_gardianClone;
	private GameObject m_mentorClone;

	private TalkScenes m_talkScene;

	public GameObject m_gGardianObject = null;
	//private float fTime 		  = 0.0f;

	private GameObject m_ObjectMobileController;
	private GameObject m_ObjectMobileControllerMove;
	private GameObject m_ObjectMobileControllerSprint;
	private Vector3 m_ObjectMobileController_LeftPosition = new Vector3(-370, -220, 0);
	private Vector3 m_ObjectMobileController_RightPosition =  new Vector3(290, -220, 0);
	// Use this for initialization

	//private GUIStyle contentStyle;

	void Start () 
	{
		/*m_mouseCorsorImage = (Texture2D)Resources.Load ("UI/corsor_1", typeof(Texture2D));
		m_closeImage       = (Texture2D)Resources.Load ("UI/exit", typeof(Texture2D));
		m_backPackImage    = (Texture2D)Resources.Load ("UI/backpack", typeof(Texture2D));
		m_sparxManulImage  = (Texture2D)Resources.Load ("UI/sbook_icon", typeof(Texture2D));*/
		FontSizeManager.checkFontSize(m_skin);

		//SetMobileController ();
		//contentStyle = new GUIStyle();
		//contentStyle.alignment = TextAnchor.UpperLeft;

	}

	// Update is called once per frame
	void Update () 
	{
		GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name != "GuideScene") {
			Global.GetPlayer ().GetComponent<PlayerMovement> ().SetMovement(false, "SettingsScreen" + "_Update");
			Global.GetPlayer ().GetComponent<PlayerMovement> ().m_bMovingToPosition = false;

			GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bCameraLookAt = false;
			GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bFollow = true;
		}
	}

	void OnEnable(){
		AudioListener.pause = true;
		/*m_gGardianObject = GameObject.Find ("guardian") as GameObject;

		//Other levels does not have the guardian object
		if(Global.CurrentLevelNumber != 1 || Global.LevelSVisited == true){
			m_gGardianObject.SetActive(false);
			this.enabled = false;
		}*/
	}

	/*
	*
	* OnGUI function
	*
	*/
	void OnGUI()
	{
		//if(Global.CurrentLevelNumber == 1){
			GUI.depth = -1;
			GUI.skin = m_skin;

			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(0.0f, 80.0f * Global.ScreenHeight_Factor,  Screen.width, 40.0f), "SPARX SETTINGS", "Titles");

			//GUI.Label(new Rect(150.0f, 135.0f, 36.0f, 36.0f), m_mouseCorsorImage);
		GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 135.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");
		GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 185.0f * Global.ScreenHeight_Factor, 705.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "SOUND");

			if (PlayerPrefs.GetInt ("playerprefs_SoundOff") == 1) {
			if (GUI.Button (new Rect (300f * Global.ScreenWidth_Factor, 165.0f * Global.ScreenHeight_Factor, 70.0f * Global.ScreenWidth_Factor, 65.0f * Global.ScreenHeight_Factor), m_ButtonOff)) {
					PlayerPrefs.SetInt ("playerprefs_SoundOff", 0);
				}
			} else {
			if (GUI.Button (new Rect (300f * Global.ScreenWidth_Factor, 165.0f * Global.ScreenHeight_Factor, 70.0f * Global.ScreenWidth_Factor, 65.0f * Global.ScreenHeight_Factor), m_ButtonOn)) {
					PlayerPrefs.SetInt ("playerprefs_SoundOff", 1);
				}
			}
				
		GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, 705.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");
		GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 705.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");

			//GUI.Label(new Rect(130.0f, 260.0f, 36.0f, 36.0f), m_closeImage);
			//GUI.Label(new Rect(180.0f, 260.0f, 655.0f, 150.0f), "If you want to leave at any time you can press this key on the keyboard or the exit icon in the top right corner of the screen. When you come back you will start you from the beginning of that level again.");

			//GUI.Label(new Rect(150.0f, 355.0f, 36.0f, 36.0f), m_backPackImage);
		GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 355.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");

			//GUI.Label(new Rect(200.0f, 300.0f, 500.0f, 100.0f), m_UIControl);
			//GUI.Label(new Rect(600.0f, 300.0f, 500.0f, 100.0f), m_AcelaratorImage);
		GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 425.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");


		GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 480.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");
		GUI.Label(new Rect(200f * Global.ScreenWidth_Factor, 445.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "LEFT HANDED CONTROL");

			if (PlayerPrefs.GetInt ("playerprefs_LeftHanded") == 0) {
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), m_UIControl);
			GUI.Label(new Rect(600.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), m_AcelaratorImage);
			if (GUI.Button (new Rect (450f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor, 70.0f * Global.ScreenWidth_Factor, 65.0f * Global.ScreenHeight_Factor), m_ButtonOn)) {
					PlayerPrefs.SetInt ("playerprefs_LeftHanded", 1);
					SetMobileController ();
				}

			} else {
			GUI.Label(new Rect(200.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), m_AcelaratorImage);
			GUI.Label(new Rect(600.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), m_UIControl);
			if (GUI.Button (new Rect (450f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor,70.0f * Global.ScreenWidth_Factor, 65.0f * Global.ScreenHeight_Factor), m_ButtonOff)) {
					PlayerPrefs.SetInt ("playerprefs_LeftHanded", 0);
					SetMobileController ();
				}
			}

			//GUI.Label(new Rect(150.0f, 560.0f, 80.0f, 80.0f), loadingIcon);
		GUI.Label(new Rect(210.0f * Global.ScreenWidth_Factor, 585.0f * Global.ScreenHeight_Factor, 655.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor), "");

		if (GUI.Button (new Rect(260.0f * Global.ScreenWidth_Factor, 500.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "Info and help www.sparx.org.nz"))
				Application.OpenURL ("https://sparx.org.nz");

		if (GUI.Button (new Rect(260.0f * Global.ScreenWidth_Factor, 540.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "Feeling bad? 0508 477 279 or free txt SPARX to 234"))
				Application.OpenURL ("https://sparx.org.nz");

		if (GUI.Button (new Rect(260.0f * Global.ScreenWidth_Factor, 580.0f * Global.ScreenHeight_Factor, 500.0f * Global.ScreenWidth_Factor, 100.0f * Global.ScreenHeight_Factor), "Turn off notifications ‘untick’ under ‘My Profile’ www.sparx.org.nz"))
				Application.OpenURL ("https://sparx.org.nz");

			//Next button
			if(Time.timeSinceLevelLoad > 2f)
			{
				if(GUI.Button(new Rect(Screen.width * 0.80f, Screen.height * 0.79f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					SetAudioListener ();
					Global.arrowKey = "";
				if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name != "GuideScene") {
					Global.GetPlayer ().GetComponent<PlayerMovement> ().SetMovement(true, "SettingsScreen" + "_OnGUI2");
				} else {
					GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				}
					//GameObject.Find ("guardian").GetComponent<CharacterInteraction>().InteractionRadius = 6;
					this.enabled = false;
				}
			}
			/*else
			{
				fTime += Time.deltaTime;
			}*/
		/*}
		else{
			this.enabled = false;
		}*/
	}

	public void SetMobileController(){
		Joystick.SetMobileController ();
	}

	public void SetAudioListener(){
		if (PlayerPrefs.GetInt ("playerprefs_SoundOff") == 0) {
			AudioListener.pause = false;
		} else {
			AudioListener.pause = true;
		}
	}


}
