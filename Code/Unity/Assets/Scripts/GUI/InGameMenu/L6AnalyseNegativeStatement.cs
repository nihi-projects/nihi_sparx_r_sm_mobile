using UnityEngine;
using System.Collections;

public class L6AnalyseNegativeStatement : MonoBehaviour {

	public GUISkin m_skin = null;
	
	static public string[] m_sChoices = {"Click here to enter text", "Click here to enter text", "Click here to enter text", "Click here to enter text"};

    private string[] headings = new string[] 
    {
        "Reality Check:",
        "Another view:",
        "Perspective:",
        "Action:"
    };

    private string[] titles = new string[] 
    {
        "Analyse the negative statement you chose using RAPA.\nMy life sucks...",
        "Analyse the negative statement you chose using RAPA.\nNobody likes me...",
        "Analyse the negative statement you chose using RAPA.\nSPARX is useless...",
        "Analyse the negative statement you chose using RAPA.\n\nI'll never feel good again...",
        "Analyse your own negative statement using RAPA."
    };

    GameObject menu = null;
	
	// Update is called once per frame
	void Update () {
	    if( menu == null )
        {
            string heading = GetComponent<L6NegativeStatement>().GetStatementChoice();

            menu = TextDisplayCanvas.instance.ShowPrefab("EnterTextWithHeadings");
            menu.GetComponent<EnterTextDialog>().CreateFields( m_sChoices.Length );
            menu.GetComponent<EnterTextDialog>().SetTitle(titles[GetComponent<L6NegativeStatement>().GetStatementID()]);
            menu.GetComponent<EnterTextDialog>().SetHeadings(headings);
        }

        for( int i = 0; i < m_sChoices.Length; ++i )
        {
            m_sChoices[i] = menu.GetComponent<EnterTextDialog>().GetText( i );
        }

        //The next button
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Global.arrowKey = "";
            Destroy(menu);

            this.enabled = false;

            //Load the guide talk scene again
            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene24");
        }
    }
	
	//public void OnGUI () 
	//{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;

  //      L6NegativeStatement negativeStatement = GetComponent<L6NegativeStatement>();

  //      //1st negative statement analyze
  //      if (negativeStatement.m_bNegativeStatementSelectList[0]){
		//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//	GUI.Label(new Rect(270.0f * Global.ScreenWidth_Factor,170.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), );
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,253.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Reality Check:");
		//	m_sChoices[0] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,263.0f * Global.ScreenHeight_Factor,400.0f,30.0f * Global.ScreenHeight_Factor), m_sChoices[0]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,307.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Another view:");
		//	m_sChoices[1] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,317.0f * Global.ScreenHeight_Factor,400.0f,30.0f * Global.ScreenHeight_Factor), m_sChoices[1]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,363.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Perspective:");
		//	m_sChoices[2] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,373.0f * Global.ScreenHeight_Factor,400.0f,30.0f * Global.ScreenHeight_Factor), m_sChoices[2]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,417.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Action:");
		//	m_sChoices[3] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,427.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[3]);
		//}
		
		////2nd negative statement analyze
		//if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[1]){
		//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//	GUI.Label(new Rect(270.0f * Global.ScreenWidth_Factor,170.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Analyse the negative statement you chose using RAPA.\n\nNobody likes me...");
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,253.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Reality Check:");
		//	m_sChoices[0] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,263.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), m_sChoices[0]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,307.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Another view:");
		//	m_sChoices[1] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,317.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[1]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,363.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Perspective:");
		//	m_sChoices[2] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,373.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[2]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,417.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Action:");
		//	m_sChoices[3] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,427.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[3]);
		//}
		
		////3nd negative statement analyze
		//if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[2]){
		//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//	GUI.Label(new Rect(270.0f * Global.ScreenWidth_Factor,170.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Analyse the negative statement you chose using RAPA.\n\nSPARX is useless...");
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,253.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Reality Check:");
		//	m_sChoices[0] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,263.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[0]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,307.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Another view:");
		//	m_sChoices[1] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,317.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[1]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,363.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Perspective:");
		//	m_sChoices[2] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,373.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[2]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,417.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Action:");
		//	m_sChoices[3] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,427.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[3]);
		//}
		
		////4th negative statement analyze
		//if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[3]){
		//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//	GUI.Label(new Rect(270.0f * Global.ScreenWidth_Factor,170.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Analyse the negative statement you chose using RAPA.\n\nI'll never feel good again...");
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,253.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Reality Check:");
		//	m_sChoices[0] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,263.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[0]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,307.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Another view:");
		//	m_sChoices[1] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,317.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[1]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,363.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Perspective:");
		//	m_sChoices[2] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,373.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[2]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,417.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Action:");
		//	m_sChoices[3] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,427.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[3]);
		//}
		
		////5th negative statement analyze
		//if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[4]){
		//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//	GUI.Label(new Rect(270.0f * Global.ScreenWidth_Factor,170.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Analyse your own negative statement using RAPA.");
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,253.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Reality Check:");
		//	m_sChoices[0] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,263.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[0]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,307.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Another view:");
		//	m_sChoices[1] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,317.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[1]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,363.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Perspective:");
		//	m_sChoices[2] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,373.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[2]);
			
		//	GUI.Label(new Rect(420.0f * Global.ScreenWidth_Factor,417.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Action:");
		//	m_sChoices[3] = GUI.TextField(new Rect(270.0f * Global.ScreenWidth_Factor,427.0f * Global.ScreenHeight_Factor,400.0f * Global.ScreenWidth_Factor,30.0f * Global.ScreenHeight_Factor), m_sChoices[3]);
		//}
	//}
}
