using UnityEngine;
using System.Collections;

public class L7TakingHome : MonoBehaviour 
{
	// public
	public GUISkin m_skin = null;
	
	
	public Texture[] m_textures;
	
	static public Texture m_tChoice;

	private string[] takeHomeArray 	=  {"REALITY CHECK. ANOTHER VIEW. PERSPECTIVE. Think ACTION",
                                        "I know that even if I feel really down, sooner or later hope will return.", 
										"I'll ask for help.",
                                        "I'll let strong feelings pass.",
                                        "I know that I have all these skills for dealing with low mood.", 
										"I'll make changes even though I don't always feel like it."};

	// privates
	//private Rect[] m_boxes = 
	//{
	//	new Rect(230.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor),
	//	new Rect(400.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor),
	//	new Rect(570.0f * Global.ScreenWidth_Factor, 110.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor),
	//	new Rect(230.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor),
	//	new Rect(400.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor),
	//	new Rect(570.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, 150.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor)
	//};
	
	private string m_conversationBoxText = "";
	
	private int m_selectedIndex = -1;
	
	public Texture2D m_tConversationBackground;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
        //m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
        menu = TextDisplayCanvas.instance.ShowPrefab("TakeHome");
        menu.GetComponent<TakeHomeDialog>().SetDescriptions(takeHomeArray);

    }
	
	// Update is called once per frame
	void Update () 
	{
        //The default conversation box text
        //m_conversationBoxText = WordsHoverOverText(-1);

        //for (int i = 0; i < m_boxes.Length; ++i)
        //{
        //	if(m_boxes[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
        //	{
        //         m_conversationBoxText = WordsHoverOverText(i);
        //		break;
        //	}
        //}

        if (menu.GetComponent<TakeHomeDialog>().SelectedIndex() != -1)
        {
            //The next button
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                m_selectedIndex = menu.GetComponent<TakeHomeDialog>().SelectedIndex();
                Destroy(menu.gameObject);
                Global.arrowKey = "";
                m_tChoice = m_textures[m_selectedIndex];

                Global.userL7NoteBookDataBlocks[0] = "";
                for (int i = 1; i < 7; ++i)
                {
                    if (m_tChoice.name == "taketoday_" + i.ToString())
                        Global.userL7NoteBookDataBlocks[0] = takeHomeArray[i - 1];
                }
                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L7GuideScene17");
            }
        }
        //CheckClick();
	}
	
	//void OnGUI()
	//{
	//	GUI.skin = m_skin;
	//	GUI.depth = -1;
		
	//	// the background of the mingame
	//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
	//	//Conversation box
	//	GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);
	//	GUI.Label(new Rect(300.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 360.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");
		
	//	for (int i = 0; i < m_boxes.Length; ++i)
	//	{
	//		GUI.Label(m_boxes[i], m_textures[i]);
	//	}
		
	//	if (m_selectedIndex != -1)
	//	{
	//		Rect temp = new Rect(m_boxes[m_selectedIndex].x - 10.0f, m_boxes[m_selectedIndex].y - 10.0f,
	//							 m_boxes[m_selectedIndex].width + 20.0f, m_boxes[m_selectedIndex].height + 20.0f);
	//		GUI.DrawTexture(temp, m_textures[m_selectedIndex]);
	//	}
	//	bool done = true;
		
	//	//The next button
	//	if (true == done)
	//	{
	//		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
	//				Global.arrowKey = "";

	//			m_tChoice = m_textures[m_selectedIndex];

	//			if(m_tChoice.name == "taketoday_1"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[0];
	//			}else if(m_tChoice.name == "taketoday_2"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[1];
	//			}else if(m_tChoice.name == "taketoday_3"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[2];
	//			}else if(m_tChoice.name == "taketoday_4"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[3];
	//			}else if(m_tChoice.name == "taketoday_5"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[4];
	//			}else if(m_tChoice.name == "taketoday_6"){
	//				Global.userL7NoteBookDataBlocks[0] = takeHomeArray[5];
	//			}else{
	//				Global.userL7NoteBookDataBlocks[0] = "";
	//			}
	//			this.enabled = false;
				
	//			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
	//			GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L7GuideScene17");
	//		}
	//	}
	//}
	
	//void CheckClick()
	//{
	//	//If the mouse button is clicked and if the mouse position is on the keywords
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		// save the mouse pos for ease of access
	//		float mouseX = Input.mousePosition.x;
	//		float mouseY = Input.mousePosition.y;
			
	//		// loop through all the original rects to see if the user has clicked on one of the boxes.
	//		for (int i = 0; i < m_boxes.Length; ++i)
	//		{
	//			if (m_boxes[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				if (m_selectedIndex == i)
	//				{
	//					m_selectedIndex = -1;
	//				}
	//				else 
	//				{
	//					m_selectedIndex = i;
	//				}
	//			}
	//		}
	//	}
	//}
	
	//public string WordsHoverOverText(int _index)
	//{
	//	string keuWordExplaination = "";
			
	//	switch(_index)
	//	{
	//		case 0:
	//			keuWordExplaination = "REALITY CHECK.\nANOTHER VIEW.\nPERSPECTIVE.\nThink ACTION";
	//			break;
	//		case 1:
	//			keuWordExplaination = "I know that even if I feel really down, sooner or later hope will return.";
	//			break;
	//		case 2:
	//			keuWordExplaination = "I'll ask for help.";
	//			break;
	//		case 3:
	//			keuWordExplaination = "I'll let strong feelings pass.";
	//			break;
	//		case 4:
	//			keuWordExplaination = "I know that I have all these skills for dealing with low mood.";
	//			break;
	//		case 5:
	//			keuWordExplaination = "I'll make changes even though I don't always feel like it.";
	//			break;
	//		default:
	//		    keuWordExplaination = "...";
	//			break;
	//	}
			
	//	return keuWordExplaination;
	//}
}
