using UnityEngine;
using System.Collections;

public class L5DragDropShield : MonoBehaviour 
{
	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_shieldPrf = null;

	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	private string m_conversationBoxText = "";
	
	static private float TextWidth = 120.0f * Global.ScreenWidth_Factor;
	static private float TextHeight = 30.0f * Global.ScreenHeight_Factor;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};
	
	private Rect[] shieldNameRects = 
	{
//		new Rect(570.0f, 95.0f, TextWidth, TextHeight),
//		new Rect(660.0f, 260.0f, TextWidth, TextHeight),
//		new Rect(530.0f, 455.0f, TextWidth, TextHeight),
//		new Rect(320.0f, 455.0f, TextWidth, TextHeight),
//		new Rect(190.0f, 260.0f, TextWidth, TextHeight),
//		new Rect(260.0f, 95.0f, TextWidth, TextHeight),
		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
	};
	
	private bool[] displayName = 
	{
		false,
		false,
		false,
		false,
		false,
		false
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	static private float m_collisiondimension = 10.0f;
	static private float m_movingTextwidth = 180.0f * Global.ScreenWidth_Factor;
	static private float m_movingTextHeight = 80.0f * Global.ScreenHeight_Factor;

    private GameObject menu;
	
	private DraggableObject[] m_wordsRects = 
	{
//		new DraggableObject(new Rect(130.0f, 120.0f, m_movingTextwidth, m_movingTextHeight), 
//			"Slow controlled breathing"),
//		new DraggableObject(new Rect(650.0f, 130.0f, m_movingTextwidth, m_movingTextHeight), 
//			"Being active to beat the blues"),
//		new DraggableObject(new Rect(120.0f, 390.0f, m_movingTextwidth, m_movingTextHeight), 
//			"Dealing with strong emotions and communicating with others"),
//		new DraggableObject(new Rect(650.0f, 380.0f, m_movingTextwidth, m_movingTextHeight), 
//			"Using STEPS to solve problems"),
		new DraggableObject(new Rect(130.0f * Global.ScreenWidth_Factor, 120.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
		                    "Slow controlled breathing"),
		new DraggableObject(new Rect(650.0f * Global.ScreenWidth_Factor, 130.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
		                    "Being active to beat the blues"),
		new DraggableObject(new Rect(120.0f * Global.ScreenWidth_Factor, 410.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
		                    "Dealing with strong emotions and communicating with others"),
		new DraggableObject(new Rect(760.0f * Global.ScreenWidth_Factor, 390.0f * Global.ScreenHeight_Factor, m_movingTextwidth, m_movingTextHeight), 
		                    "Using STEPS to solve problems"),
	};
	
	static float m_targetDim = 70.0f * Global.ScreenWidth_Factor;
	private Rect[] m_destinations = 
	{
//		new Rect(510.0f, 130.0f, m_targetDim, m_targetDim),
//		new Rect(570.0f, 240.0f, m_targetDim, m_targetDim),
//		new Rect(510.0f, 350.0f, m_targetDim, m_targetDim),
//		new Rect(380.0f, 350.0f, m_targetDim, m_targetDim),
//		new Rect(320.0f, 240.0f, m_targetDim, m_targetDim),
//		new Rect(380.0f, 130.0f, m_targetDim, m_targetDim)
		new Rect(Screen.width*0.5469f, Screen.height*0.2344f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.625f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.5469f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3906f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3125f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3809f, Screen.height*0.2474f, m_targetDim, m_targetDim)
	};
	
	private int[] m_correctIndex = 
	{
		2, 
		-1, 
		-1, 
		3, 
		1,
		0
	};
	
	private string m_currentDragText = "";
	private int m_currentDragIndex = -1;
	private bool m_dragging = false;
	
	public Texture colourTexture = null;
	public Texture colourFrameTexture = null;
	public Texture greyScaleTexture = null;
	public Texture m_tConversationBackground = null;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
		//colourTexture    = (Texture2D)Resources.Load ("Textures/UI/shield_new_color", typeof(Texture2D));
		//greyScaleTexture = (Texture2D)Resources.Load ("Textures/UI/shield_new_gray", typeof(Texture2D));
		//m_tConversationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (menu == null)
        {
            string[] descriptions = new string[] {  "You learnt to 'Relax' - to calm your body and mind by slow controlled breathing and relaxing your muscles." ,
                                                    "You learnt to 'Do it' by being active to beat the blues.",
                                                    "Last time you were using STEPS to 'Solve it'.",
                                                    "Match each skill to the correct part of the shield."};

            string[] strs = new string[] { "Slow controlled breathing", "Being active to beat the blues", "Dealing with strong emotions and communicating with others", "Using STEPS to solve problems" };

            menu = TextDisplayCanvas.instance.ShowPrefab("ShieldAgainstDepression");
            menu.GetComponent<ShieldMenu>().SetNames( new bool[] { true, true, true, true, true, true } );

            for( int i = 0; i < strs.Length; ++i )
            {
                menu.GetComponent<ShieldMenu>().AddWord( strs[i], descriptions[i] );
            }
            
        }

        for (int i = 0; i < childObjectNames.Length; ++i)
        {
            ChangePieceColour(i);
        }
        
        ShieldMenu sm = menu.GetComponent<ShieldMenu>();
        Transform trans = null;
        int index = -1;
        int wordIndex = -1;

        if (sm.IsDragging() == true)
        {
            wordIndex = sm.GetDraggingIndex();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo);

            if (hitInfo.transform)
            {
                trans = hitInfo.transform;
                for (int i = 0; i < childObjectNames.Length; ++i)
                {
                    if (childObjectNames[i] == hitInfo.transform.name)
                    {
                        index = i;
                        ChangeFrameColour(i);
                    }
                }
            }
        }

        if (DestinationDrop(trans, index, wordIndex))
        {
            sm.DestroyDragged();
        }

        if (menu.GetComponent<ShieldMenu>().IsFinished())
        {
            //The next button
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";

                this.enabled = false;

                //Destroy the shield clone
                GameObject.Destroy(m_shieldObjectClone);
                GameObject.Find("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                UpdateCurrentSceneName();
            }
        }
    }
	
	void OnEnable()
	{
		//The shield against depression position
		m_vShieldPosition = new Vector3(0.0f, 0.03f , 0.70f );
		//The shield object clone
		m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity);
		
		GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
		
	}

	void ChangePieceColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.Find(childObjectNames[_index]);
			
		// if it is being displayed then the colour needs active
		if (displayName[_index])
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
		}
		// fi it is not being displayed then the coulour needs to be grey scale
		else
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
		}
	}

	void ChangeFrameColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.Find (childObjectNames [_index]);
		child.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", colourFrameTexture);
	}

    /*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
    public bool DestinationDrop(Transform _destinationTransform, int _index, int _wordIndex)
    {
        // release early if there is no active keyword box being moved.
        if (_destinationTransform == null || _index == -1) { return false; }

        //If the mouse button is UnClicked and if  the mouse position is on the fill field
        if (Input.GetMouseButtonUp(0))
        {
            if (m_correctIndex[_index] == _wordIndex)
            {
                // set the destination text and remove the clicked text.
                m_currentDragText = "";
                m_currentDragIndex = -1;
                displayName[_index] = true;
                return true;
            }
        }
        return false;
    }

    /*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
    private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_wordsRects[m_currentDragIndex].movable = m_wordsRects[m_currentDragIndex].original;
		m_wordsRects[m_currentDragIndex].words = m_currentDragText;
		m_currentDragText = "";
		m_currentDragIndex = -1;
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//level 5
		if(Global.CurrentLevelNumber == 5)
		{
			//Depend on the different scene, the shield is different as well
			if(Global.PreviousSceneName == "L5GuideScene9")
			{
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L5GuideScene10");
			}
		}
	}
}
