﻿using UnityEngine;
using System.Collections;

public class EagleHelpTrigger : MonoBehaviour
{	
	public bool m_EagleTriggerEntered = false;

	void OnTriggerEnter(Collider other) 
	{
		m_EagleTriggerEntered = true;
	}

	void OnTriggerExit(Collider other) 
	{
		m_EagleTriggerEntered = false;
	}
}
