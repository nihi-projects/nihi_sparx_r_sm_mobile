using UnityEngine;
using System.Collections;

public class L7LessonsMatch : MonoBehaviour 
{	
	// publics
    //public GUISkin m_skin = null;
	
	
	// privates
	//private string m_conversationBoxText = "";
	
	//private Vector2[] m_KeywordDimensions = 
	//{
	//	new Vector2(8.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Disaster
	//	new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Downer
	//	new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // All-or-Nothing
	//	new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Guilty
	//	new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Perfectionist
	//	new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Mind Reader
	//};
	
	//static private float m_collisiondimension = 10.0f * Global.ScreenHeight_Factor;
	//static private float m_textWidth = 180.0f * Global.ScreenWidth_Factor;
	//static private float m_textHeight = 90.0f;
	//private DraggableObject[] m_realLifeText = 
	//{
	//	new DraggableObject(new Rect(140.0f * Global.ScreenWidth_Factor, 160.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight), 
	//		"You can't sort everything by yourself."),
	//	new DraggableObject(new Rect(140.0f * Global.ScreenWidth_Factor, 243.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//		"You need to practise skills."),
	//	new DraggableObject(new Rect(140.0f * Global.ScreenWidth_Factor, 336.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//		"Sometimes you can't get rid of all your negative thoughts."),
	//	new DraggableObject(new Rect(140.0f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//		"Sometimes feelings get really strong - like a storm.")
	//};

    string[] strHowDoesItRelate = new string[] 
    {
        "Sometimes you can't get rid of all your negative thoughts.",
        "You can't sort everything by yourself.",
        "Sometimes feelings get really strong - like a storm.",
        "You need to practise skills.",
    };

    string[] strWhatsTheMessage = new string[]
    {
        "At times you need to carry on even with negative thoughts still buzzing around.",
        "Asking for help can work wonders (sometimes you need to try it more than once).",
        "Just like storms move on, so do strong feelings. Wait till they pass.",
        "Use all your different skills to deal with negative thoughts."
    };

    private string[] questions =
    {
        "You can't shoot all the Gnats.",
        "You got stuck unless you asked for help.",
        "You had to shelter and wait out the storm.",
        "You had to polish your shield."
    };

    //private DraggableObject[] m_messageText = 
    //{
    //	new DraggableObject(new Rect(645.0f * Global.ScreenWidth_Factor, 160.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
    //		"Just like storms move on, so do strong feelings. Wait till they pass."),
    //	new DraggableObject(new Rect(645.0f * Global.ScreenWidth_Factor, 243.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
    //		"At times you need to carry on even with negative thoughts still buzzing around."),
    //	new DraggableObject(new Rect(645.0f * Global.ScreenWidth_Factor, 336.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
    //		"Use all your different skills to deal with negative thoughts."),
    //	new DraggableObject(new Rect(645.0f * Global.ScreenWidth_Factor, 430.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
    //		"Asking for help can work wonders (sometimes you need to try it more than once).")
    //};

    //KeyWord destination positions 
    //   static private float m_dextinationRectWidth = 200.0f * Global.ScreenWidth_Factor;
    //static private float m_dextinationRectHeight = 70.0f * Global.ScreenHeight_Factor;

    //private Rect[] m_destinationRect = 
    //{
    //	new Rect(390.0f * Global.ScreenWidth_Factor, 260.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
    //	new Rect(390.0f * Global.ScreenWidth_Factor, 380.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
    //};

    //private Vector2[] m_finalTextPos = 
    //{
    //	new Vector2(490.0f * Global.ScreenWidth_Factor, 305.0f * Global.ScreenHeight_Factor),
    //	new Vector2(490.0f * Global.ScreenWidth_Factor, 435.0f * Global.ScreenHeight_Factor)
    //};

    //private int[,] m_correctIndex = 
    //{
    //	{2, 1},
    //	{0, 3}, 
    //	{3, 0}, 
    //	{1, 2},
    //};

    //private bool[] m_placed = 
    //{
    //	false, false,
    //};

    //private string m_currentClickedString = "";

    //private float m_timer = 0.0f;
    //private int[] m_currentIndex = {-1, -1};
    //private int m_currentMatch = 0;

    //private bool b_dragging = false;

    //public Texture2D m_tFillBoxBackground;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//FontSizeManager.checkFontSize(m_skin);
		//m_tFillBoxBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
        if( menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab( "MatchMultipleLessons" );
            menu.GetComponent<MatchMultipleLessons>().SetQuestions(questions);
            menu.GetComponent<MatchMultipleLessons>().CreatePhrases(strHowDoesItRelate, strWhatsTheMessage);
        }

        //The next button
        if (menu.GetComponent<MatchMultipleLessons>().bFinished == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";
                this.enabled = false;
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L7GuideScene16");
            }
        }

        //if (b_dragging)
        //{
        //	// check to see if the moveable rect is found
        //	if (m_currentIndex[0] != -1)
        //	{
        //		// save the mouse pos for ease of access
        //		float mouseX = Input.mousePosition.x;
        //		float mouseY = Input.mousePosition.y;
        //		m_realLifeText[m_currentIndex[0]].movable.Set(mouseX - m_textWidth/2, 
        //										     mouseY * (-1) + Screen.height - m_textHeight/2, 
        //										     m_textWidth, 
        //										     m_textHeight);
        //	}
        //	else if (m_currentIndex[1] != -1)
        //	{
        //		// save the mouse pos for ease of access
        //		float mouseX = Input.mousePosition.x;
        //		float mouseY = Input.mousePosition.y;
        //		m_messageText[m_currentIndex[1]].movable.Set(mouseX - m_textWidth/2, 
        //										     mouseY * (-1) + Screen.height - m_textHeight/2, 
        //										     m_textWidth, 
        //										     m_textHeight);
        //	}
        //}
        //else
        //{
        //	KeyWordDrag();
        //}

        //DestinationDrop();

        //// if both are placed
        //if (m_placed[0] && m_placed[1])
        //{
        //	m_timer += Time.deltaTime;

        //	if (m_timer >= 1.5f)
        //	{
        //		m_placed[0] = false;
        //		m_placed[1] = false;
        //		m_currentClickedString = "";
        //		m_realLifeText[m_correctIndex[m_currentMatch, 0]].words = "";
        //		m_messageText[m_correctIndex[m_currentMatch, 1]].words = "";
        //		m_currentMatch += 1;
        //		m_timer = 0.0f;
        //	}
        //}
    }
	
	//public void OnGUI () 
	//{		
	//	GUI.skin = m_skin;
	//	GUI.depth = -1;
		
	//	// the background of the mingame
	//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
	//	GUI.Label(new Rect(345.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor, 295.0f * Global.ScreenWidth_Factor, 440.0f * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
		
	//	bool done = false;
		
	//	// The middle area
	//	GUI.Label(new Rect(390 * Global.ScreenWidth_Factor, 95 * Global.ScreenHeight_Factor, 200 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor), "In the GAME:", "Titles");
	//	if (m_currentMatch < 4)
	//	{
	//		GUI.Label(new Rect(370 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 240 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor), questions[m_currentMatch], "CenteredFont");
	//	}
	//	else 
	//	{
	//		done = true;
	//	}
	//	GUI.Label(new Rect(370 * Global.ScreenWidth_Factor, 220 * Global.ScreenHeight_Factor, 240 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor), "How does it relate to real life?", "Titles");
	//	GUI.Label(new Rect(390 * Global.ScreenWidth_Factor, 350 * Global.ScreenHeight_Factor, 200 * Global.ScreenWidth_Factor, 80 * Global.ScreenHeight_Factor), "What's the message?", "Titles");
	//	GUI.Label(new Rect(115 * Global.ScreenWidth_Factor, 115 * Global.ScreenHeight_Factor, 240 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "How does it relate to real life?", "Titles");
	//	GUI.Label(new Rect(630 * Global.ScreenWidth_Factor, 115 * Global.ScreenHeight_Factor, 220 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "What's the message?", "Titles");
		
	//	if (!m_placed[0])
	//	{
	//		GUI.Label(m_destinationRect[0], "...", "CenteredFont");
	//	}
	//	if (!m_placed[1])
	//	{
	//		GUI.Label(m_destinationRect[1], "...", "CenteredFont");
	//	}
		
	//	for (int i = 0; i < m_realLifeText.Length; ++i)
	//	{
	//		Rect temp = new Rect(m_realLifeText[i].movable.x, m_realLifeText[i].movable.y, 
	//							m_realLifeText[i].original.width, m_realLifeText[i].original.height);
	//		GUI.Label(temp, m_realLifeText[i].words, "CenteredFont");
	//		temp = new Rect(m_messageText[i].movable.x, m_messageText[i].movable.y, 
	//							m_messageText[i].original.width, m_messageText[i].original.height);
	//		GUI.Label(temp, m_messageText[i].words, "CenteredFont");
	//	}
	//}
	
	///*
	// * 
	// * Thie function drag the keyword and move with the mouse.
	// * 
	//*/
	//public void KeyWordDrag()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex[0] != -1 || m_currentIndex[1] != -1) { return ;}
		
	//	//If the mouse button is clicked and if the mouse position is on the keywords
	//	if(Input.GetMouseButton(0))
	//	{
	//		// save the mouse pos for ease of access
	//		float mouseX = Input.mousePosition.x;
	//		float mouseY = Input.mousePosition.y;
			
	//		// boolean to see if a box has been clicked on.
	//		bool clicked = false;
			
	//		// loop through all the original rects to see if the user has clicked on one of the boxes.
	//		for (int i = 0; i < m_realLifeText.Length; ++i)
	//		{
	//			if (m_realLifeText[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// set the current active key text
	//				// check if the current text has been set
	//				if (m_currentClickedString == "")
	//				{
	//					m_currentClickedString = "..";
	//					m_currentIndex[0] = i;
	//					clicked = true;
	//					b_dragging = true;
	//					break;
	//				}
	//				m_currentClickedString = "...";
	//			}
	//			else if (m_messageText[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// set the current active key text
	//				// check if the current text has been set
	//				if (m_currentClickedString == "")
	//				{
	//					m_currentClickedString = "..";
	//					m_currentIndex[1] = i;
	//					clicked = true;
	//					b_dragging = true;
	//					break;
	//				}
	//			}
	//		}	
	//	}
	//}
	
	///*
	// * 
	// * Thie function drop the keyword to the destination area.
	// * 
	//*/
	//public void DestinationDrop()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex[0] == -1 && m_currentIndex[1] == -1) { return ;}
		
	//	int index = m_currentIndex[0] == -1 ? 1 : 0;
		
	//	//If the mouse button is UnClicked and if  the mouse position is on the fill field
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		b_dragging = false;
			
	//		// boolean for if the text was placed in the box.
	//		bool successful = false;
			
	//		// loop through the destination rects to see which box the word is being placed in
	//		for (int i = 0; i < m_destinationRect.Length; ++i)
	//		{
	//			float mouseX = Input.mousePosition.x;
	//			float mouseY = Input.mousePosition.y;
				
	//			// check if this destination box contains the current clicked keyword box
	//			if(m_destinationRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// if the destination text is empty
	//				if(m_correctIndex[m_currentMatch, index] == m_currentIndex[index])
	//				{
	//					// set the destination text and remove the clicked text.
	//					m_currentClickedString = "";
	//					successful = true;
						
	//					float width = 0.0f;
	//					float height = 0.0f;
	//					if (index == 0)
	//					{
	//						width = m_realLifeText[m_currentIndex[index]].original.width;
	//						height = m_realLifeText[m_currentIndex[index]].original.height;
	//						m_realLifeText[m_currentIndex[index]].movable.Set(m_finalTextPos[0].x - width/2, 
	//																		  m_finalTextPos[0].y - height/2, 
	//																		  width, height);
	//						m_placed[0] = true;
	//					}
	//					else 
	//					{
	//						width = m_messageText[m_currentIndex[index]].original.width;
	//						height = m_messageText[m_currentIndex[index]].original.height;
	//						m_messageText[m_currentIndex[index]].movable.Set(m_finalTextPos[1].x - width/2, 
	//																		 m_finalTextPos[1].y - height/2, 
	//																		 width, height);
	//						m_placed[1] = true;
	//					}

	//					m_currentIndex[index] = -1;
	//				}
	//				break;
	//			}
	//		}
			
	//		// another early release because everything is done already
	//		if (successful) { return ;}
			
	//		// execute this code if the user missed the destination box.
	//		ResetCurrentString();
	//	}
	//}
	
	///*
	// * 
	// * This function resets the missed keyword text and text rect back to their original positions
	// * 
	// */
	//private void ResetCurrentString()
	//{
	//	// set the movable rect to the original area, set the keyword back to the current,
	//	// reset the current keyword and break the loop.
	//	if (m_currentIndex[0] != -1)
	//	{
	//		m_realLifeText[m_currentIndex[0]].movable = m_realLifeText[m_currentIndex[0]].original;
	//		m_currentIndex[0] = -1;
	//	}
	//	else 
	//	{
	//		m_messageText[m_currentIndex[1]].movable = m_messageText[m_currentIndex[1]].original;
	//		m_currentIndex[1] = -1;
	//	}
	//	m_currentClickedString = "";
		
	//}
}
