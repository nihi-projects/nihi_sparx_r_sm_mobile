using UnityEngine;
using System.Collections;

public class Bag : MonoBehaviour 
{
	public bool m_bShow = true;

	// publics
	public GUISkin m_skin = null;
	
	public Texture2D m_tBag;
	public Texture2D[] m_tItems;
	
	// privates
	private float m_fMoveSpeed = 2.0f;
	
	static private float m_fWidth = 70.0f;
	static private float m_fHeight = 70.0f;
	private float m_fTimer = 0.0f;

	private Rect m_rBag = new Rect(Screen.height * 0.01f, Screen.height * 0.89f, m_fWidth * Global.ScreenWidth_Factor, m_fHeight * Global.ScreenHeight_Factor );
	private Rect[] m_rItemRects;
	
	private Vector2[] m_vFinalPos;
	
	public bool m_bToggleShowItems = false;
	private bool m_bMove = false;
	public bool m_bNotebookUp = false;

	// Use this for initialization
	void Start () 
	{
		Debug.Log ("Factor" + Global.ScreenHeight_Factor.ToString() + "/" + Screen.height);

		m_rItemRects = new Rect[m_tItems.Length];
		m_vFinalPos = new Vector2[m_tItems.Length];
		float x = 0.0f;
		for (int i = 0; i < m_vFinalPos.Length; ++i)
		{
			x = ((m_fWidth + 25.0f) * (i+1) + 25.0f) * Global.ScreenWidth_Factor;
			m_vFinalPos[i] = new Vector2(x, Screen.height * 0.9f);
		}
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!m_bShow) { return ;}
		if (!CheckPlayingAutoscene ()) {
			CheckBagClick ();
		}
		if (m_bMove)
		{
			if (true == m_bToggleShowItems)
			{
				ShowItems();
			}
			else 
			{
				HideItems();
			}
		}
	}
	
	void OnGUI()
	{
		if (!m_bShow) { return ;}
		
		GUI.depth = -1;
		GUI.skin = m_skin;

		if (!CheckPlayingAutoscene ()) {
			GUI.Label(m_rBag, m_tBag);
		}

		
		if (m_bMove || m_bToggleShowItems)
		{
			for (int i = 1; i < m_rItemRects.Length; ++i)
			{
				if (Global.CurrentLevelNumber == 2 && i > 0)
				{
					break;
				}
				GUI.Label(m_rItemRects[i], m_tItems[i]);
			}
		
			GameObject guiObject = GameObject.Find("GUI");


			if (GUI.Button (m_rItemRects [0], m_tItems [0])) {
				//guiObject.transform.FindChild("GUI").gameObject.SetActive(true);
				guiObject.SetActive(true);
				guiObject.gameObject.GetComponent<HelpSparx> ().enabled = true;
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;

				m_fTimer = 0.0f;

				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
			else if (Global.CurrentLevelNumber > 1 && GUI.Button(m_rItemRects[1], m_tItems[1]))
			{
				bool closing = false;
				if (guiObject.GetComponent<SavedL1NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL1NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL2NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL2NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL3NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL3NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL4NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL4NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL5NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL5NoteBook>().enabled = false;
				}
				else if (guiObject.GetComponent<SavedL6NoteBook>().enabled == true)
				{
					closing = true;
					guiObject.GetComponent<SavedL6NoteBook>().enabled = false;
				}
				
				if (!closing)
				{
                    // Here is the code for loading the saved notebooks
                    // Always starts opening the first notebook
                    //Cannot view the notebook for the first level
                    switch (Global.CurrentLevelNumber)
                    {
                        case 2:
                            {
                                guiObject.GetComponent<SavedL1NoteBook>().enabled = true;
                                break;
                            }
                        case 3:
                            {
                                guiObject.GetComponent<SavedL2NoteBook>().enabled = true;
                                break;
                            }
                        case 4:
                            {
                                guiObject.GetComponent<SavedL3NoteBook>().enabled = true;
                                break;
                            }
                        case 5:
                            {
                                guiObject.GetComponent<SavedL4NoteBook>().enabled = true;
                                break;
                            }
                        case 6:
                            {
                                guiObject.GetComponent<SavedL5NoteBook>().enabled = true;
                                break;
                            }
                        case 7:
                            {
                                guiObject.GetComponent<SavedL6NoteBook>().enabled = true;
                                break;
                            }
                    }


					
					m_bNotebookUp = true;
				}
				else 
				{
					m_bNotebookUp = false;
				}
			}
			else if (Global.CurrentLevelNumber > 1 && !m_bNotebookUp && GUI.Button(m_rItemRects[2], ""))
			{
				guiObject.transform.Find("L1GUI").gameObject.SetActive(true);
				guiObject.transform.Find("L1GUI").GetComponent<ShieldAgaDepression>().enabled = true; 
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;
				
				m_fTimer = 0.0f;
				
				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
			else if (Global.CurrentLevelNumber > 2 && !m_bNotebookUp && GUI.Button(m_rItemRects[3], ""))
			{
				guiObject.transform.Find("L6GUI").gameObject.SetActive(true);
				guiObject.transform.Find("L6GUI").GetComponent<L6SortItNegotiate>().enabled = true; 
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;

				m_fTimer = 0.0f;

				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
			else if (Global.CurrentLevelNumber > 5 && !m_bNotebookUp && GUI.Button(m_rItemRects[4], ""))
			{
				guiObject.transform.Find("L6GUI").gameObject.SetActive(true);
				guiObject.transform.Find("L6GUI").GetComponent<KeyButtonScriptRAPA>().enabled = true; 
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;

				m_fTimer = 0.0f;

				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
		}
	}


	bool CheckPlayingAutoscene(){
        if(GameObject.Find("GUI"))
        {
            if (GameObject.Find("GUI").GetComponent<TalkScenes>().enabled)
            {
                if (GameObject.Find("GUI").GetComponent<TalkScenes>().IsAutoTalkScene(GameObject.Find("GUI").GetComponent<TalkScenes>().GetCurrentScene()))
                    return true;
            }
		}
        return false;
	}

	void CheckBagClick()
	{
		if (!m_bMove && Input.GetMouseButtonUp(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			if (m_rBag.Contains(new Vector2(mouseX, Screen.height - mouseY)))
			{
				m_bToggleShowItems = !m_bToggleShowItems;
				m_bMove = true;
				
				m_fTimer = 0.0f;
				
				for (int i = 0; i < m_rItemRects.Length; ++i)
				{
					m_rItemRects[i] = m_rBag;
				}
			}
		}
	}
	
	void ShowItems()
	{
		m_fTimer += Time.deltaTime * m_fMoveSpeed;
		
		if (m_fTimer <= 1.0f)
		{
			for (int i = 0; i < m_vFinalPos.Length; ++i)
			{
				Vector2 temp = Vector2.Lerp(new Vector2(m_rBag.x, m_rBag.y), m_vFinalPos[i], m_fTimer);
				m_rItemRects[i].x = temp.x;
				m_rItemRects[i].y = temp.y;
			}
		}
		else
		{
			m_fTimer = 0.0f;
			m_bMove = false;
		}
	}
	
	void HideItems()
	{
		m_fTimer += Time.deltaTime * m_fMoveSpeed;
		
		if (m_fTimer <= 1.0f)
		{
			for (int i = 0; i < m_vFinalPos.Length; ++i)
			{
				Vector2 temp = Vector2.Lerp(m_vFinalPos[i], new Vector2(m_rBag.x, m_rBag.y), m_fTimer);
				m_rItemRects[i].x = temp.x;
				m_rItemRects[i].y = temp.y;
			}
		}
		else
		{
			m_fTimer = 0.0f;
			m_bMove = false;
		}
	}
}
