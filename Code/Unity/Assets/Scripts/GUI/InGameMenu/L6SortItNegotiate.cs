using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class L6SortItNegotiate : MonoBehaviour
{
	//public GUISkin m_skin = null;
	
	
	//private Rect[] m_blincRects = 
	//{
	//	new Rect(Screen.width*0.4323f, Screen.height*0.5406f, Screen.width*0.0313f, Screen.height*0.0391f),
	//	new Rect(Screen.width*0.4688f, Screen.height*0.5406f, Screen.width*0.0313f, Screen.height*0.0391f),
	//	new Rect(Screen.width*0.5052f, Screen.height*0.5276f, Screen.width*0.0208f, Screen.height*0.0651f),
	//	new Rect(Screen.width*0.5260f, Screen.height*0.5406f, Screen.width*0.0313f, Screen.height*0.0391f),
	//	new Rect(Screen.width*0.5625f, Screen.height*0.5406f, Screen.width*0.0313f, Screen.height*0.0391f),
	//};
	
	//private string[] m_blinc = 
	//{
	//	"B", "L", "I", "N", "C"
	//};
	
	//private string m_conversationBoxText = "";
	
	//public Texture m_tBLINC;

    private GameObject menu;
	
	// Use this for initialization
	public void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//FontSizeManager.checkFontSize(m_skin);
		//m_tBLINC = (Texture2D)Resources.Load ("UI/BlincStoneOn", typeof(Texture2D));
	}
	
	// Update is called once per frame
	public void Update () 
	{
        if( menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("SortItNegotiate");
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;

            if (SceneManager.GetActiveScene().name == "GuideScene")
            {
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                //Update the current scene name
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene25");
            }

        }

  //      m_conversationBoxText = WordsHoverOverText("");
		
		//for (int i = 0; i < m_blincRects.Length; ++i)
		//{
		//	if(m_blincRects[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
		//	{
		//         m_conversationBoxText = WordsHoverOverText(m_blinc[i]);
		//	}
		//}
	}
	
	//public void OnGUI () 
	//{		
	//	GUI.skin = m_skin;
	//	GUI.depth = -1;
		
	//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
	//	GUI.Label(new Rect(Screen.width*0.3645f, Screen.height*0.1042f, Screen.width*0.2604f, Screen.height*0.0651f), "SORT IT - NEGOTIATE", "Titles");
	//	GUI.Label(new Rect(Screen.width*0.3958f, Screen.height*0.1563f, Screen.width*0.2083f, Screen.height*0.0651f), "To negotiate a good deal:", "CenteredFont");
		
	//	GUI.Label(new Rect(Screen.width*0.2917f, Screen.height*0.1693f, Screen.width*0.4167f, Screen.height*0.1953f), "1) Be calm and clear (not aggro and not pushover).\n\n" +
	//						  "2) Ask for what you need.\n\n" +
	//						  "3) Listen - get their view and make sure you understand.", "CenteredFont");
		
	//	GUI.Label(new Rect(Screen.width*0.3958f, Screen.height*0.3385f, Screen.width*0.4092f, Screen.height*0.3083f), m_tBLINC);
	//	//for (int i = 0; i < m_blincRects.Length; ++i)
	//	//{
	//	//	GUI.Box(m_blincRects[i], "");
	//	//}
		
	//	GUI.Label(new Rect(Screen.width*0.2917f, Screen.height*0.5992f, Screen.width*0.4375f, Screen.height*0.1302f), m_conversationBoxText, "CenteredFont");
		
	//	GUI.Label(new Rect(Screen.width*0.2917f, Screen.height*0.6518f, Screen.width*0.4375f, Screen.height*0.1953f), "4) Give a little, take a little. See if you can find a compromise or a deal which is good for you both.", "CenteredFont");
		
	//	if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
	//				Global.arrowKey = "";

	//		this.enabled = false;

	//		if(Global.CurrentLevelNumber == 6){
	//			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
	//			//Update the current scene name
	//			GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene25");
	//		}

	//	}
	//}
	
	//public string WordsHoverOverText(string _keyWord)
	//{
	//	string keyWordExplaination = "";
			
	//	switch(_keyWord)
	//			{
	//		case "B":
	//			keyWordExplaination = "Bite your tongue (do not say to much).";
	//			break;
	//		case "L":
	//			keyWordExplaination = "Look at the speaker.";
	//			break;
	//		case "I":
	//			keyWordExplaination = "be Interested (if you can't be interested at least look interested).";
	//			break;
	//		case "N":
	//			keyWordExplaination = "No interuptions.";
	//			break;
	//		case "C":
	//			keyWordExplaination = "Check you understand (ask them if you've got it right).";
	//			break;
	//		default:
	//		    keyWordExplaination = "...";
	//			break;
	//	}
			
	//	return keyWordExplaination;
	//}
}
