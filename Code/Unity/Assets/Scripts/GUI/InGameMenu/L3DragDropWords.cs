using UnityEngine;
using System.Collections;

public class L3DragDropWords : MonoBehaviour
{
    Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
    // publics
    public GUISkin m_skin = null;

    private GameObject menu;

    // privates
    private string m_conversationBoxText = "";

    static private float m_keywordWidth = 100.0f * Global.ScreenWidth_Factor;
    static private float m_keywordHeight = 30.0f * Global.ScreenHeight_Factor;
    private Vector2[] m_KeywordDimensions =
    {
        new Vector2(75.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Headache
		new Vector2(85.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Butterflies
		new Vector2(100.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Bite my nails
		new Vector2(130.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Tensed shoulders
		new Vector2(100.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Clenched jaw
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Shaky
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Leave
		new Vector2(95.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Heart racing
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Red face
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Short of breath
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Fight
		new Vector2(90.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Weak knees
		new Vector2(60.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Crying
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Stomach-ache
		new Vector2(90.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Yell/Swear
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Blood pumping
	};

    private Rect[] m_myOriRect;

    private string[] m_keyWordText = { "Blood pumping", "Yell/Swear", "Sick", "Cry", "Fight", "Breath", "Red",
        "Bite nails", "Tense shoulders", "Clench fists", "Shaky", "Leave",
        "Heart racing", "Headache"};

	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_dextinationRectHeight = 35.0f * Global.ScreenHeight_Factor;
	
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};

	private string[] m_destinationText = {"", "", "", "", "", ""};

	private Rect[] m_destinationRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;
	
	// Use this for initialization
	void Start () 
	{
		FontSizeManager.checkFontSize(m_skin);
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		Global.levelStarted = false;
		m_myOriRect = new Rect[] 
		{	new Rect(680.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // Headache
			new Rect(680.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // Butterflies
			new Rect(680.0f * Global.ScreenWidth_Factor, 137.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // Bite my nails
			new Rect(680.0f * Global.ScreenWidth_Factor, 184.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Tensed shoulders
			new Rect(680.0f * Global.ScreenWidth_Factor, 231.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Clenched jaw
			new Rect(680.0f * Global.ScreenWidth_Factor, 278.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Shaky
			new Rect(680.0f * Global.ScreenWidth_Factor, 325.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Leave
			new Rect(680.0f * Global.ScreenWidth_Factor, 372.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[7].x, m_KeywordDimensions[7].y), // Heart racing
			new Rect(180.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[8].x, m_KeywordDimensions[8].y), // Red face
			new Rect(180.0f * Global.ScreenWidth_Factor, 372.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[9].x, m_KeywordDimensions[9].y), // Short of breath
			new Rect(180.0f * Global.ScreenWidth_Factor, 325.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[10].x, m_KeywordDimensions[10].y), // Fight
			new Rect(180.0f * Global.ScreenWidth_Factor, 278.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[11].x, m_KeywordDimensions[11].y), // Weak knees
			new Rect(180.0f * Global.ScreenWidth_Factor, 231.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[12].x, m_KeywordDimensions[12].y), // Crying
			new Rect(180.0f * Global.ScreenWidth_Factor, 184.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[13].x, m_KeywordDimensions[13].y), // Stomach-ache
			new Rect(180.0f * Global.ScreenWidth_Factor, 137.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[14].x, m_KeywordDimensions[14].y), // Yell/Swear
			new Rect(180.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[15].x, m_KeywordDimensions[15].y) // Blood pumping
		};
		
		m_movableStringRect = new Rect[m_myOriRect.Length];
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			m_movableStringRect[i] = m_myOriRect[i];
		}
		
		m_currentClickedStringRect = k_zeroRect;
		
		//m_tFillFieldBackground     = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
		//m_tDestinationBackground   = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
		//m_tConversationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
        menu = CommonListCode.LoadMenu(menu, "I can spot feeling angry when:", m_keyWordText, WordsHoverOverText);

        //The default conversation box text
        m_conversationBoxText = WordsHoverOverText("");
		
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
			}
		}
		
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												   m_KeywordDimensions[m_currentIndex].x, 
												   m_KeywordDimensions[m_currentIndex].y);
				
				m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
			}
		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		//GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
		//// the background of the mingame
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//// The middle area
		//GUI.Label(new Rect(330 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 300 * Global.ScreenWidth_Factor, 300 * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
		//GUI.Label(new Rect(345 * Global.ScreenWidth_Factor, 145 * Global.ScreenHeight_Factor, 270 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "I can spot feeling angry when:", "Titles");
		//bool done = false;
		//for (int i = 0; i < m_destinationRect.Length; ++i)
		//{
		//	GUI.DrawTexture(m_destinationRect[i], m_tFillFieldBackground);
		//	GUI.Label(m_destinationOriKeyWordRect[i], m_destinationText[i], "CenteredFont");
		//	//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
		//	//GUI.Box(m_destinationOriKeyWordRect[i], m_destinationText[i]);
			
		//	if (m_destinationText[i] != "")
		//	{
		//		done = true;
		//	}
		//}
		
		//// the circle of words
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//	//GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//}
		
		//// The current movable keyword box and text
		//if (m_currentClickedString != "")
		//{
		//	GUI.Label(m_currentClickedStringRect, m_currentClickedString, "CenteredFont");
		//	//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
		//}
		
		////Conversation box
		//GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);

		//GUI.Label(new Rect(275.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 400.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");

        //The next button
        if (true == menu.GetComponent<ListScreen>().bReadyToContinue == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu.gameObject);
                Global.arrowKey = "";

				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene17");
			}
		}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_myOriRect.Length; ++i)
			{
				if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentClickedString == "")
					{
						// if the keyword does not exist that means that it is in one of the destination boxes
						if (m_keyWordText[i] == "")
						{
							// loop through the destination boxes to find which contains the key word
							for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
							{
								// set the min and max of the box
								Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
								Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
								// check if this destination box contains the current movable box
								if(m_destinationOriKeyWordRect[j].Contains(min) &&
									m_destinationOriKeyWordRect[j].Contains(max))
								{
									// set the destination text and remove the clicked text.
									m_keyWordText[i] = m_destinationText[j];
									m_destinationText[j] = "";
									break;
								}
							}
						}
						
						m_currentClickedString = m_keyWordText[i];
						m_keyWordText[i] = "";
						m_currentIndex = i;
						clicked = true;
						b_dragging = true;
						m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex == -1) { return ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			b_dragging = false;
			
			// boolean for if the text was placed in the box.
			bool successful = false;
			
			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				
				// check if this destination box contains the current clicked keyword box
				if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// if the destination text is empty
					if(m_destinationText[i] == "")
					{
						// set the destination text and remove the clicked text.
						m_destinationText[i] = m_currentClickedString;
						m_currentClickedString = "";
						successful = true;
						
						float width = m_dextinationRectWidth - 2;
						float height = m_dextinationRectHeight - 2;
						m_movableStringRect[m_currentIndex].Set(
							m_destinationOriKeyWordRect[i].center.x - width/2, 
							m_destinationOriKeyWordRect[i].center.y - height/2, 
							width, 
							height);

						m_currentClickedStringRect = k_zeroRect;
						m_currentIndex = -1;
					}
				}
			}
			
			// another early release because everything is done already
			if (successful) { return ;}
			
			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
		m_keyWordText[m_currentIndex] = m_currentClickedString;
		m_currentClickedStringRect = k_zeroRect;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keuWordExplaination = "";

		switch(_keyWord)
		{
            case "Blood pumping":
                keuWordExplaination = "My blood is pumping.";
				break;
            case "Yell/Swear":
                keuWordExplaination = "I yell/swear.";
				break;
            case "Sick":
                keuWordExplaination = "I feel sick.";
				break;
            case "Cry":
                keuWordExplaination = "I cry.";
				break;
            case "Fight":
                keuWordExplaination = "I want to fight.";
				break;
            case "Breath":
                keuWordExplaination = "I am short of breath.";
				break;
            case "Red":
                keuWordExplaination = "I have a red face.";
				break;
            case "Bite nails":
                keuWordExplaination = "I bite my nails.";
				break;
            case "Tense shoulders":
                keuWordExplaination = "I tense my shoulders.";
				break;
            case "Clench fists":
                keuWordExplaination = "I clench my fists.";
				break;
            case "Shaky":
                keuWordExplaination = "I get shaky.";
				break;
            case "Leave":
                keuWordExplaination = "I want to leave.";
				break;
            case "Heart racing":
                keuWordExplaination = "My heart is racing.";
				break;
            case "Headache":
                keuWordExplaination = "I get a headache.";
				break;
			default:
			    keuWordExplaination = "Drag and drop all the ways you can SPOT\nfeeling angry.";
				break;
		}
			
		return keuWordExplaination;
	}
}
