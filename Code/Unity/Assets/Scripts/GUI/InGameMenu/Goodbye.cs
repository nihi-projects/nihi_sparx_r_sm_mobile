using UnityEngine;
using System.Collections;

public class Goodbye : MonoBehaviour {
	
	public GUISkin m_skin            = null;
	private bool m_bPlayerIsWaving    = false;
	private bool m_bShowGoodbyeScreen = true;
	private bool m_bSavePointRoutine = false;
	public bool m_bGameWasQuit;

    private GameObject menu;
	private CapturedDataInput instance;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;		
		m_bSavePointRoutine = false;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		FontSizeManager.checkFontSize(m_skin);
	}

    private void OnEnable()
    {
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("Goodbye");
        }
    }

    // Update is called once per frame
    void Update () 
	{
		if (m_bPlayerIsWaving && !GameObject.Find ("Guide").GetComponent<Animation> ().IsPlaying ("wave")) {
			if (!m_bGameWasQuit) {
				if (!m_bSavePointRoutine) {
					instance.m_sSaveURLReturn = null;
					instance.bOfflineFinishLevelSave = false;
					SaveToServer();
					m_bSavePointRoutine = true;
				}
				if (Application.internetReachability != NetworkReachability.NotReachable) {// Connection Reachable
					if (instance.m_sSaveURLReturn.isDone && m_bSavePointRoutine) {
						Debug.Log ("This level is finished!");
						if (!Application.isEditor) {
							Application.ExternalCall ("gameLevelEnd");
						}
						this.enabled = false;
						StartCoroutine (restartApplication ());

					}
				} else {
					if (instance.bOfflineFinishLevelSave) {
						Debug.Log ("This level is finished!");
						if (!Application.isEditor) {
							Application.ExternalCall ("gameLevelEnd");
						}
						this.enabled = false;
						StartCoroutine (restartApplication ());
					} else {
						Debug.Log ("Waiting for offline save process!");
					}
				}
			} else {
				Debug.Log ("This level was quit!");
				Debug.Log ("This level is finished!");
				if (!Application.isEditor) {
					Application.ExternalCall ("gameLevelEnd");
				}

				this.enabled = false;
			}
		}

        if (m_bShowGoodbyeScreen)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu);
                Global.arrowKey = "";
                GameObject.Find("Guide").GetComponent<Animation>().Play("wave");
                m_bShowGoodbyeScreen = false;
                m_bPlayerIsWaving = true;
            }
        }
    }

	public void SaveToServer(){
        instance.SaveToServer();
	}
	IEnumerator restartApplication(){
		bool running = true;
		while (running) {
			yield return this;
			if (Application.internetReachability != NetworkReachability.NotReachable) {
				if (instance.m_sSaveURLReturn != null && instance.m_sSaveURLReturn.isDone) {
					running = false;
					instance.unLoadAllContent ();
					Destroy (instance);
					Destroy (GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad> ());
					Global.resetAllValues ();
					for (int i = 0; i < 2; i++) {
						//destroy both instances of all that probably exist here
						string[] objectsToDestroy = new string[] {
							"Global",
							"GUI Text",
							"GUI",
							"PlayerInfoLoad",
							"ScriptHolder",
							"CapturedDataInputHolder"
						};
						foreach (string s in objectsToDestroy) {
							GameObject anObject = GameObject.Find (s);
							Destroy (anObject);
						}

					}

					Application.LoadLevel ("GuideScene");
					
				}
			} else {
				running = false;
				instance.unLoadAllContent ();
				Destroy (instance);
				Destroy (GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad> ());
				Global.resetAllValues ();
				for (int i = 0; i < 2; i++) {
					//destroy both instances of all that probably exist here
					string[] objectsToDestroy = new string[] {
						"Global",
						"GUI Text",
						"GUI",
						"PlayerInfoLoad",
						"ScriptHolder",
						"CapturedDataInputHolder"
					};
					foreach (string s in objectsToDestroy) {
						GameObject anObject = GameObject.Find (s);
						Destroy (anObject);
					}

				}

				Application.LoadLevel ("GuideScene");

			}
		}
	}
}
