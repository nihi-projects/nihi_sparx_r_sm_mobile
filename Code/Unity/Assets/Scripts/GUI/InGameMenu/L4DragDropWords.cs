using UnityEngine;
using System.Collections;

public class L4DragDropWords : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;

    private GameObject menu;

    // privates
    private string m_conversationBoxText = "";
	
	static private float m_keywordWidth = 100.0f * Global.ScreenWidth_Factor;
	static private float m_keywordHeight = 30.0f * Global.ScreenHeight_Factor;
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(75.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Strenth
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Humour
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Loyalty
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Talents
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Getting sorted
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Good day
		new Vector2(80.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Good chat
		new Vector2(60.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Family
		new Vector2(50.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Proud
		new Vector2(180.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Not getting bogged down
		new Vector2(45.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Care
		new Vector2(70.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Smiling
		new Vector2(120.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // People who care
		new Vector2(110.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Bright future
		new Vector2(95.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Good deed
		new Vector2(60.0f * Global.ScreenWidth_Factor, 30.0f * Global.ScreenHeight_Factor), // Trust
	};
	
	private Rect[] m_myOriRect; 
	
	private string[] m_keyWordText = {"Strength", "Humour", "Loyalty", "Talents", 
									"Getting sorted", "Good day", "Good chat", "Family",
									"Proud", "Not getting stuck", "Care", "Smiling", 
        "People who care", "Bright future", "Good deed", "Making changes"};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f * Global.ScreenWidth_Factor;
	static private float m_dextinationRectHeight = 35.0f * Global.ScreenHeight_Factor;
	
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};

	static public string[] m_L4destinationText = {"", "", "", "", "", ""};

	private Rect[] m_destinationRect = 
	{
		new Rect(360.0f * Global.ScreenWidth_Factor, 175.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 210.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 245.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 280.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 315.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
   //     m_myOriRect = new Rect[]
   //    {   new Rect(180.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // Strenth
			//new Rect(680.0f * Global.ScreenWidth_Factor, 90.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // Humour
			//new Rect(680.0f * Global.ScreenWidth_Factor, 137.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // Loyalty
			//new Rect(680.0f * Global.ScreenWidth_Factor, 184.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Talents
			//new Rect(680.0f * Global.ScreenWidth_Factor, 231.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Getting sorted
			//new Rect(680.0f * Global.ScreenWidth_Factor, 278.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Good day
			//new Rect(680.0f * Global.ScreenWidth_Factor, 325.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Good chat
			//new Rect(680.0f * Global.ScreenWidth_Factor, 372.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[7].x, m_KeywordDimensions[7].y), // Family
			//new Rect(680.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[8].x, m_KeywordDimensions[8].y), // Proud
			//new Rect(180.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[9].x, m_KeywordDimensions[9].y), // Not getting bogged down
			//new Rect(180.0f * Global.ScreenWidth_Factor, 372.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[10].x, m_KeywordDimensions[10].y), // Care
			//new Rect(180.0f * Global.ScreenWidth_Factor, 325.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[11].x, m_KeywordDimensions[11].y), // Smiling
			//new Rect(180.0f * Global.ScreenWidth_Factor, 278.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[12].x, m_KeywordDimensions[12].y), // People who care
			//new Rect(180.0f * Global.ScreenWidth_Factor, 231.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[13].x, m_KeywordDimensions[13].y), // Bright future
			//new Rect(180.0f * Global.ScreenWidth_Factor, 184.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[14].x, m_KeywordDimensions[14].y), // Good deed
			//new Rect(180.0f * Global.ScreenWidth_Factor, 137.0f * Global.ScreenHeight_Factor, m_KeywordDimensions[15].x, m_KeywordDimensions[15].y) // Trust
   //    };

  //      m_movableStringRect = new Rect[m_myOriRect.Length];
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	m_movableStringRect[i] = m_myOriRect[i];
		//}
		
		//m_currentClickedStringRect = k_zeroRect;
		
		//m_tFillFieldBackground    = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
		//m_tDestinationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
        menu = CommonListCode.LoadMenu(menu, "My Sparks:", m_keyWordText, WordsHoverOverText);
        m_L4destinationText = menu.GetComponent<ListScreen>().GetChosenStrings();

        //The default conversation box text
        //m_conversationBoxText = WordsHoverOverText("");
		
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
		//	{
		//         m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
		//	}
		//}
		
		//if (b_dragging)
		//{
		//	// check to see if the moveable rect is found
		//	if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
		//	{
		//		// save the mouse pos for ease of access
		//		float mouseX = Input.mousePosition.x;
		//		float mouseY = Input.mousePosition.y;
		//		m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
		//										   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
		//										   m_KeywordDimensions[m_currentIndex].x, 
		//										   m_KeywordDimensions[m_currentIndex].y);
				
		//		m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
		//	}
		//}
        
        //The next button
        if (true == menu.GetComponent<ListScreen>().bReadyToContinue == true)
        {
            if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
            {
                Destroy(menu.gameObject);
                Global.arrowKey = "";

                this.enabled = false;

                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L4GuideScene27");
            }
        }

        //else
        //{
        //	KeyWordDrag();
        //}

        //DestinationDrop();
    }
	
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//// the background of the mingame
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//// The middle area
		//GUI.Label(new Rect(330 * Global.ScreenWidth_Factor, 120 * Global.ScreenHeight_Factor, 300 * Global.ScreenWidth_Factor, 300 * Global.ScreenHeight_Factor), "", "SmallBoarderPanel");
		//GUI.Label(new Rect(345 * Global.ScreenWidth_Factor, 145 * Global.ScreenHeight_Factor, 270 * Global.ScreenWidth_Factor, 40 * Global.ScreenHeight_Factor), "My Sparks:", "Titles");
		//bool done = false;
		//for (int i = 0; i < m_destinationRect.Length; ++i)
		//{
		//	GUI.DrawTexture(m_destinationRect[i], m_tFillFieldBackground);
		//	GUI.Label(m_destinationOriKeyWordRect[i], m_L4destinationText[i], "CenteredFont");
		//	//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
		//	//GUI.Box(m_destinationOriKeyWordRect[i], m_L4destinationText[i]);
			
		//	if (m_L4destinationText[i] != "")
		//	{
		//		done = true;
		//	}
		//}
		
		//// the circle of words
		//for (int i = 0; i < m_myOriRect.Length; ++i)
		//{
		//	//GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//	GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		//}
		
		//// The current movable keyword box and text
		//if (m_currentClickedString != "")
		//{
		//	GUI.Label(m_currentClickedStringRect, m_currentClickedString, "CenteredFont");
		//	//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
		//}
		
		////Conversation box
		//GUI.DrawTexture(new Rect(225 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,550 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), m_tConversationBackground);

		//GUI.Label(new Rect(300.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 360.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");

        
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	//public void KeyWordDrag()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex != -1) { return ;}
		
	//	//If the mouse button is clicked and if the mouse position is on the keywords
	//	if(Input.GetMouseButton(0))
	//	{
	//		// save the mouse pos for ease of access
	//		float mouseX = Input.mousePosition.x;
	//		float mouseY = Input.mousePosition.y;
			
	//		// boolean to see if a box has been clicked on.
	//		bool clicked = false;
			
	//		// loop through all the original rects to see if the user has clicked on one of the boxes.
	//		for (int i = 0; i < m_myOriRect.Length; ++i)
	//		{
	//			if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// set the current active key text
	//				// check if the current text has been set
	//				if (m_currentClickedString == "")
	//				{
	//					// if the keyword does not exist that means that it is in one of the destination boxes
	//					if (m_keyWordText[i] == "")
	//					{
	//						// loop through the destination boxes to find which contains the key word
	//						for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
	//						{
	//							// set the min and max of the box
	//							Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
	//							Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
	//							// check if this destination box contains the current movable box
	//							if(m_destinationOriKeyWordRect[j].Contains(min) &&
	//								m_destinationOriKeyWordRect[j].Contains(max))
	//							{
	//								// set the destination text and remove the clicked text.
	//								m_keyWordText[i] = m_L4destinationText[j];
	//								m_L4destinationText[j] = "";
	//								break;
	//							}
	//						}
	//					}
						
	//					m_currentClickedString = m_keyWordText[i];
	//					m_keyWordText[i] = "";
	//					m_currentIndex = i;
	//					clicked = true;
	//					b_dragging = true;
	//					m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
	//					break;
	//				}
	//			}
	//		}	
			
	//		// this is an early release from the function
	//		if (clicked == true) { return ;}
	//	}
	//}
	
	///*
	// * 
	// * Thie function drop the keyword to the destination area.
	// * 
	//*/
	//public void DestinationDrop()
	//{
	//	// release early if there is no active keyword box being moved.
	//	if (m_currentIndex == -1) { return ;}
		
	//	//If the mouse button is UnClicked and if  the mouse position is on the fill field
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		b_dragging = false;
			
	//		// boolean for if the text was placed in the box.
	//		bool successful = false;
			
	//		// loop through the destination rects to see which box the word is being placed in
	//		for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
	//		{
	//			// save the mouse pos for ease of access
	//			float mouseX = Input.mousePosition.x;
	//			float mouseY = Input.mousePosition.y;
				
	//			// check if this destination box contains the current clicked keyword box
	//			if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//			{
	//				// if the destination text is empty
	//				if(m_L4destinationText[i] == "")
	//				{
	//					// set the destination text and remove the clicked text.
	//					m_L4destinationText[i] = m_currentClickedString;
	//					m_currentClickedString = "";
	//					successful = true;
						
	//					float width = m_dextinationRectWidth - 2;
	//					float height = m_dextinationRectHeight - 2;
	//					m_movableStringRect[m_currentIndex].Set(
	//						m_destinationOriKeyWordRect[i].center.x - width/2, 
	//						m_destinationOriKeyWordRect[i].center.y - height/2, 
	//						width, 
	//						height);

	//					m_currentClickedStringRect = k_zeroRect;
	//					m_currentIndex = -1;
	//				}
	//			}
	//		}
			
	//		// another early release because everything is done already
	//		if (successful) { return ;}
			
	//		// execute this code if the user missed the destination box.
	//		ResetCurrentString();
	//	}
	//}
	
	///*
	// * 
	// * This function resets the missed keyword text and text rect back to their original positions
	// * 
	// */
	//private void ResetCurrentString()
	//{
	//	// set the movable rect to the original area, set the keyword back to the current,
	//	// reset the current keyword and break the loop.
	//	m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
	//	m_keyWordText[m_currentIndex] = m_currentClickedString;
	//	m_currentClickedStringRect = k_zeroRect;
	//	m_currentClickedString = "";
	//	m_currentIndex = -1;
	//}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keuWordExplaination = "";
			
		switch(_keyWord)
		{
			case "Strength":
				keuWordExplaination = "I am strong.";
				break;
			case "Humour":
				keuWordExplaination = "I can tell a good joke.";
				break;
			case "Loyalty":
				keuWordExplaination = "I am loyal.";
				break;
			case "Talents":
				keuWordExplaination = "There are things I am good at.";
				break;
			case "Getting sorted":
				keuWordExplaination = "I'm sorting things out.";
				break;
			case "Good day":
				keuWordExplaination = "Today is a good day.";
				break;
			case "Good chat":
				keuWordExplaination = "I had a good chat with a friend today.";
				break;
			case "Family":
				keuWordExplaination = "I am grateful for my family.";
				break;
			case "Proud":
                keuWordExplaination = "I feel proud of something I've done.";
				break;
			case "Not getting stuck":
                keuWordExplaination = "I don’t get stuck in problems.";
				break;
			case "Care":
				keuWordExplaination = "I look out for someone I care about.";
				break;
			case "Smiling":
				keuWordExplaination = "I made someone smile today.";
				break;
			case "People who care":
				keuWordExplaination = "I have people who care about me.";
				break;
			case "Bright future":
				keuWordExplaination = "My future looks bright.";
				break;
			case "Good deed":
				keuWordExplaination = "I'll do something nice for someone.";
				break;
            case "Making changes":
                keuWordExplaination = "I am on a better track.";
				break;
			default:
			    keuWordExplaination = "Drag and drop all your Sparks.";
				break;
		}
			
		return keuWordExplaination;
	}
}
