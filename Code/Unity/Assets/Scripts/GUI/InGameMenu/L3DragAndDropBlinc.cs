using UnityEngine;
using System.Collections;

public class L3DragAndDropBlinc : MonoBehaviour 
{
	//Public variables
	public GUISkin m_skin = null;

	public static int ScreenHeight_Factor = Screen.height / 768;
	public static int ScreenWidth_Factor = Screen.width / 1024;

	//Private variables
	private string m_conversationBoxText = "";

	static private float m_collisiondimension = 10.0f;

	private DraggableObject[] m_letterRects;

	private bool [] m_placed = 
	{
		false, 
		false,
		false,
		false,
		false
	};

	private Rect[] m_destinations = 
	{
		new Rect(360.0f * ScreenWidth_Factor, 340.0f * ScreenHeight_Factor, 70.0f * ScreenWidth_Factor, 80.0f * ScreenHeight_Factor),
		new Rect((360.0f * ScreenWidth_Factor) + 80, 340.0f * ScreenHeight_Factor, 60.0f * ScreenWidth_Factor, 75.0f * ScreenHeight_Factor),
		new Rect((360.0f * ScreenWidth_Factor) + 150, 340.0f * ScreenHeight_Factor, 30.0f * ScreenWidth_Factor, 80.0f * ScreenHeight_Factor),
		new Rect((360.0f * ScreenWidth_Factor) + 190, 340.0f * ScreenHeight_Factor, 70.0f * ScreenWidth_Factor, 70.0f * ScreenHeight_Factor),
		new Rect((360.0f * ScreenWidth_Factor) + 270, 340.0f * ScreenHeight_Factor, 60.0f * ScreenWidth_Factor, 60.0f * ScreenHeight_Factor)
	};

	private Vector2[] m_finalLetterPos = 
	{
		new Vector2(((360.0f + 25.0f) * ScreenWidth_Factor), (340.0f + 30.0f) * ScreenHeight_Factor),
		new Vector2(((360.0f + 25.0f) * ScreenWidth_Factor) + 70, (340.0f + 30.0f) * ScreenHeight_Factor),
		new Vector2(((360.0f + 25.0f) * ScreenWidth_Factor) + 130, (340.0f + 30.0f) * ScreenHeight_Factor),
		new Vector2(((360.0f + 25.0f) * ScreenWidth_Factor) + 190, (340.0f + 30.0f) * ScreenHeight_Factor),
		new Vector2(((360.0f + 25.0f) * ScreenWidth_Factor) + 270, (340.0f + 30.0f) * ScreenHeight_Factor)
	};

	private int m_currentDragIndex = -1;
	private bool m_dragging = false;

	public Texture m_letterB;
	public Texture m_letterL;
	public Texture m_letterI;
	public Texture m_letterN;
	public Texture m_letterC;
	public Texture m_wordBLINC;

	public Texture2D m_tConversationBackground;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");

		//m_letterB   = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincB", typeof(Texture2D));
		//m_letterL   = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincL", typeof(Texture2D));
		//m_letterI   = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincI", typeof(Texture2D));
		//m_letterN   = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincN", typeof(Texture2D));
		//m_letterC   = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincC", typeof(Texture2D));
		//m_wordBLINC = (Texture2D)Resources.Load ("Textures/UI/Blinc Stone/BlincStone", typeof(Texture2D));

		m_letterRects = new DraggableObject[]
		{
			new DraggableObject(new Rect(290.0f * ScreenWidth_Factor, 80.0f * ScreenHeight_Factor, m_letterB.width, m_letterB.height),
				new Rect(320.0f * ScreenWidth_Factor, 130.0f * ScreenHeight_Factor, m_collisiondimension, m_collisiondimension), "B"),
			new DraggableObject(new Rect(650.0f * ScreenWidth_Factor, 100.0f * ScreenHeight_Factor, m_letterL.width, m_letterL.height),
				new Rect(650.0f * ScreenWidth_Factor, 135.0f * ScreenHeight_Factor, m_collisiondimension, m_collisiondimension), "L"),
			new DraggableObject(new Rect(780.0f * ScreenWidth_Factor, 220.0f * ScreenHeight_Factor, m_letterI.width, m_letterI.height),
				new Rect(800.0f * ScreenWidth_Factor, 250.0f * ScreenHeight_Factor, m_collisiondimension, m_collisiondimension), "I"),
			new DraggableObject(new Rect(150.0f * ScreenWidth_Factor, 220.0f * ScreenHeight_Factor, m_letterN.width, m_letterN.height),
				new Rect(200.0f * ScreenWidth_Factor, 250.0f * ScreenHeight_Factor, m_collisiondimension, m_collisiondimension), "N"),
			new DraggableObject(new Rect(460.0f * ScreenWidth_Factor, 90.0f * ScreenHeight_Factor, m_letterC.width, m_letterC.height),
				new Rect(480.0f * ScreenWidth_Factor, 100.0f * ScreenHeight_Factor, m_collisiondimension, m_collisiondimension), "C"),
		};
		for (int i = 0; i < m_letterRects.Length; ++i)
		{
			Rect orig = m_letterRects[i].original;
			m_letterRects[i].movable = new Rect(orig.x + orig.width/2, orig.y + orig.height/2, 10.0f * ScreenWidth_Factor, 10.0f * ScreenHeight_Factor);
		}		

		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}

	// Update is called once per frame
	void Update () 
	{
		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText("");

		for (int i = 0; i < m_letterRects.Length; ++i)
		{
			if(m_letterRects[i].original.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
				m_conversationBoxText = WordsHoverOverText(m_letterRects[i].words);
			}
		}

		if (m_dragging)
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			m_letterRects[m_currentDragIndex].movable.Set(mouseX - m_collisiondimension/2, 
				mouseY * (-1) + Screen.height - m_collisiondimension/2, 
				m_collisiondimension, 
				m_collisiondimension);
		}
		else
		{
			KeyWordDrag();
		}

		DestinationDrop();
	}

	void OnGUI()
	{
		GUI.depth = -1;
		GUI.skin = m_skin;

		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, 960.0f * ScreenWidth_Factor, 600.0f * ScreenHeight_Factor), "", "FullSizeDialogBox");

		// blinc stone
		Rect stoneRect = new Rect((float)(Screen.width - m_wordBLINC.width) * 0.5f, // x
			(float)(Screen.height - m_wordBLINC.height) * 0.5f, // y
			(float)m_wordBLINC.width, (float)m_wordBLINC.height); // width and height
		GUI.Label(stoneRect, m_wordBLINC);

		bool done = true;

		// draw all the letters and destinations
		for (int i = 0; i < m_letterRects.Length; ++i)
		{
			if (i != m_currentDragIndex)
			{
				Rect temp = new Rect(m_letterRects[i].movable.x - m_letterRects[i].original.width /2, 
					m_letterRects[i].movable.y - m_letterRects[i].original.height /2, 
					m_letterRects[i].original.width, 
					m_letterRects[i].original.height);
				GUI.Label(temp, GetLetter(m_letterRects[i].words[0])); 

				//GUI.Box(m_letterRects[i].movable, "");
				//GUI.Box(m_destinations[i], "");

				if (!m_placed[i])
				{
					done  = false;
				}
			}
		}

		if (m_currentDragIndex != -1)
		{
			Rect tempRect = new Rect(m_letterRects[m_currentDragIndex].movable.x - m_letterRects[m_currentDragIndex].original.width /2, 
				m_letterRects[m_currentDragIndex].movable.y - m_letterRects[m_currentDragIndex].original.height /2, 
				m_letterRects[m_currentDragIndex].original.width, 
				m_letterRects[m_currentDragIndex].original.height);
			GUI.Label(tempRect, GetLetter(m_letterRects[m_currentDragIndex].words[0])); 
		}

		GUI.Label(m_destinations[0], "HELLO1");
		GUI.Label(m_destinations[1], "HELLO2");
		GUI.Label(m_destinations[2], "HELLO3");
		GUI.Label(m_destinations[3], "HELLO4");
		GUI.Label(m_destinations[4], "HELLO5");

		//Conversation box
		GUI.Label(new Rect(265 * ScreenWidth_Factor,470 * ScreenHeight_Factor,620 * ScreenWidth_Factor,135 * ScreenHeight_Factor), m_tConversationBackground);
		GUI.Label(new Rect(300.0f * ScreenWidth_Factor, 497.0f * ScreenHeight_Factor, 360.0f * ScreenWidth_Factor, 80.0f * ScreenHeight_Factor), m_conversationBoxText, "CenteredFont");

		if (done)
		{
			//The next button
			if(GUI.Button(new Rect(800 * ScreenWidth_Factor, 495 * ScreenHeight_Factor, 60 * ScreenWidth_Factor, 60 * ScreenHeight_Factor), "", "dilogBoxNextButton"))
			{
				this.enabled = false;

				GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
				//Update the current scene name
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene36");
			}
		}
	}

	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentDragIndex != -1) { return ;}

		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;

			// boolean to see if a box has been clicked on.
			bool clicked = false;

			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_letterRects.Length; ++i)
			{
				// skip this one because its been placed already
				if (m_placed[i]) { continue; }

				if (m_letterRects[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentDragIndex == -1)
					{
						m_currentDragIndex = i;
						clicked = true;
						m_dragging = true;
						break;
					}
				}
			}	

			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}

	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentDragIndex == -1) { return ;}

		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			m_dragging = false;

			// boolean for if the text was placed in the box.
			bool successful = false;

			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinations.Length; ++i)
			{
				Rect moving = m_letterRects[m_currentDragIndex].movable;
				Vector2 min = new Vector2(moving.xMin, moving.yMin);
				Vector2 max = new Vector2(moving.xMax, moving.yMax);

				// check if this destination box contains the current clicked keyword box
				if(m_destinations[i].Contains(min) &&
					m_destinations[i].Contains(max))
				{
					// if the destination text is empty
					if(i == m_currentDragIndex)
					{
						// set the destination text and remove the clicked text.
						m_currentDragIndex = -1;
						m_placed[i] = true;
						m_letterRects[i].movable = new Rect(m_finalLetterPos[i].x, 
							m_finalLetterPos[i].y, 
							m_letterRects[i].movable.width, 
							m_letterRects[i].movable.height);
						m_letterRects[i].original = new Rect(m_letterRects[i].movable.x - m_letterRects[i].original.width/2, 
							m_letterRects[i].movable.y - m_letterRects[i].original.height/2, 
							m_letterRects[i].original.width, m_letterRects[i].original.height);
						successful = true;
						break;
					}
				}
			}

			// another early release because everything is done already
			if (successful) { return ;}

			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}

	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		Rect orig = m_letterRects[m_currentDragIndex].original;
		m_letterRects[m_currentDragIndex].movable.center = orig.center;
		m_letterRects[m_currentDragIndex].movable.center.Set(orig.center.x, orig.center.y);
		m_currentDragIndex = -1;
	}

	private Texture GetLetter(char _letter)
	{
		switch (_letter)
		{
		case 'B':
			return m_letterB;
		case 'L':
			return m_letterL;
		case 'I':
			return m_letterI;
		case 'N':
			return m_letterN;
		case 'C':
			return m_letterC;
		default:
			return null;
		};
	}

	public string WordsHoverOverText(string _keyWord)
	{
		string keyWordExplaination = "";

		switch(_keyWord)
		{
		case "B":
			keyWordExplaination = "Bite your tongue\n(do not say to much).";
			break;
		case "L":
			keyWordExplaination = "Look at the speaker.";
			break;
		case "I":
			keyWordExplaination = "be Interested\n(if you can't be interested at least look interested).";
			break;
		case "N":
			keyWordExplaination = "No interuptions.";
			break;
		case "C":
			keyWordExplaination = "Check you understand\n(ask them if you've got it right).";
			break;
		default:
			keyWordExplaination = "...";
			break;
		}

		return keyWordExplaination;
	}
}

