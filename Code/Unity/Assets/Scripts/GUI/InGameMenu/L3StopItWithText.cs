using UnityEngine;
using System.Collections;

public class L3StopItWithText : MonoBehaviour
{
    private GameObject menu;
	
	// Update is called once per frame
	void Update () {
	    if(menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab( "StopItWithText" );
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";

            this.enabled = false;

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            //Update the current scene name
            GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene30");
        }
    }
}
