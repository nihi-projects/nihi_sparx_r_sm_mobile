using UnityEngine;
using System.Collections;

public class L2RelexYourMusel : MonoBehaviour {

	public GUISkin m_skin = null;
	public Texture2D m_tBodyTexture0;
	public Texture2D m_tArmTexture1;
	public Texture2D m_tLegsTexture2;
	public Texture2D m_tChestTexture3;
	
	private TalkScenes m_talkScene;
	private Texture2D m_displayedTexture;
	private float m_fStartTime = 0.0f;
	
	private string m_sTitleName = "";
	
	// Use this for initialization
	void Start () {
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnEnable(){
		m_fStartTime = Time.time;
		m_displayedTexture = m_tBodyTexture0;
		m_sTitleName = "RELAXING YOUR MUSCLES.";
		
		m_talkScene = GameObject.Find ("GUI").GetComponent<TalkScenes>();
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () {		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(400.0f, -200.0f, 960.0f, 530.0f), m_sTitleName);
		
		UpdateTexture();
		GUI.Label(new Rect(220.0f, 20.0f, 960.0f, 530.0f), m_displayedTexture);
		
		if((Time.time - m_fStartTime) > 12.0f){
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				m_talkScene.enabled = true;
				m_talkScene.SetCurrentDialogue("L2GuideScene32");
			}
		}
	}
	
	/*
	*
	* OnEnable Function, this function will be called everytime when the script is enabled.
	*
	*/
	public void UpdateTexture(){
		//Update the display images
		if((Time.time - m_fStartTime) > 3.0f && (Time.time - m_fStartTime) < 6.0f){
			m_sTitleName = "Arms";
			m_displayedTexture = m_tArmTexture1;
			Debug.Log ("Display arm texture");
		}
		else if((Time.time - m_fStartTime) > 6.0f && (Time.time - m_fStartTime) < 9.0f){
			m_sTitleName = "Legs";
			m_displayedTexture = m_tLegsTexture2;
			Debug.Log ("Display legs texture");
		}
		else if((Time.time - m_fStartTime) > 9.0f && (Time.time - m_fStartTime) < 12.0f){
			m_sTitleName = "Back and chest";
			m_displayedTexture = m_tChestTexture3;
			Debug.Log ("Display legs texture");
		}
	}
}
