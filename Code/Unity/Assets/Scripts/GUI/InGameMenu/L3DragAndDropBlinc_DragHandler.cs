﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class L3DragAndDropBlinc_DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public static GameObject itemBeingDregged;
	public Vector3 startPosittion;
	public Text oText;
	Transform startParent;
	#region IBeginDragHandler implementation
	public void OnBeginDrag (PointerEventData eventData)
	{
		itemBeingDregged = gameObject;
		startPosittion = transform.position;

		oText.text = WordsHoverOverText (transform.name);

	}
	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		transform.position = Input.mousePosition;

	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		itemBeingDregged = null;
		if (transform.parent.name != transform.name) {
			transform.position = startPosittion;
		}
		oText.text = "";
	}

	#endregion

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string WordsHoverOverText(string _keyWord)
	{
		string keyWordExplaination = "";

		switch(_keyWord)
		{
		case "B":
			keyWordExplaination = "Bite your tongue\n(do not say to much).";
			break;
		case "L":
			keyWordExplaination = "Look at the speaker.";
			break;
		case "I":
			keyWordExplaination = "be Interested\n(if you can't be interested at least look interested).";
			break;
		case "N":
			keyWordExplaination = "No interuptions.";
			break;
		case "C":
			keyWordExplaination = "Check you understand\n(ask them if you've got it right).";
			break;
		default:
			keyWordExplaination = "...";
			break;
		}

		return keyWordExplaination;
	}
}
