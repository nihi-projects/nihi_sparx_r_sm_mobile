using UnityEngine;
using System.Collections;

public class HelpLine : MonoBehaviour {

    //Public variables
    //public GUISkin m_skin = null;

    ////Private variables
    //private string m_sHelpLineText = "";
    //private string m_sl1tol6text   = "Between now and the next time, t";

    private GameObject menu;

	// Use this for initialization
	void Start () {
		//FontSizeManager.checkFontSize(m_skin);

		////m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//if(Global.CurrentLevelNumber == 7){
		//	m_sl1tol6text = "T";
		//}
		//m_sHelpLineText = "\n\n\n\n\n\n\n\n" + m_sl1tol6text + "ry out some of the skills " +
		//	"you've learnt.\n\nThe Notebook for today's level will be saved in your backpack for you if you want to view it again later." +
		//	"\n\nIf you're feeling really stressed and down, it's important that you get help.\nYou can talk to a school counsellor, " +
		//	"a doctor or you can phone a helpline.\nDifferent things work for different people so make sure you get the support " +
		//	"that's right for you.\nA list of places where you can find more help is on the SPARX website.";
	}
	
	// Update is called once per frame
	void Update () {
        if( menu == null )
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("FinishedSparxLevel");
        }

        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";
            this.enabled = false;
            GetComponent<Goodbye>().enabled = true;
        }
    }
	
	void OnGUI()
	{
		//GUI.depth = -1;
		//GUI.skin = m_skin;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//if(Global.CurrentLevelNumber != 7){
		//	GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 75.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 10.0f * Global.ScreenHeight_Factor), "\n\n\n\n\n\n\nYou've finished this Level of SPARX.", "Titles");
		//}
		//else{
		//	GUI.Label(new Rect(150.0f * Global.ScreenWidth_Factor, 75.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 10.0f * Global.ScreenHeight_Factor), "\n\n\n\n\n\n\nYou've finished the last Level of SPARX.", "Titles");
		//}
		
		//GUI.Label(new Rect(180.0f * Global.ScreenWidth_Factor, 75.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor), m_sHelpLineText);
		//if(Global.CurrentLevelNumber != 7){
		//	GUI.Label(new Rect(130.0f * Global.ScreenWidth_Factor, 75.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor), "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nSee you soon!", "CenteredFont");
		//}
		//else{
		//	GUI.Label(new Rect(130.0f * Global.ScreenWidth_Factor, 75.0f * Global.ScreenHeight_Factor, 700.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor), "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nStay well and safe!", "CenteredFont");
		//}
	}
}
