using UnityEngine;
using System.Collections;

public class L3HowIFeelMiniGame : MonoBehaviour 
{
    private GameObject widhif = null;
	// publics
	//public GUISkin m_skin = null;
	//public Texture[] m_tActiveFaces;
	//public Texture[] m_tDeactiveFaces;
	
	// privates
	//private string[] m_worksDone = 
	//{
	//	"I put off my assignments/work",
	//	"I finished my assignments/work",
	//	"I sat on my own doing nothing",
	//	"I did something fun with family/friends",
	//	"I got some exercise/played sports",
	//	"I relaxed/took some time for myself"
	//};
	
	//static private float m_textWidth = 300.0f * Global.ScreenWidth_Factor;
	//static private float m_textHeight = 50.0f * Global.ScreenHeight_Factor;
	//private Rect[] m_wordsRect = 
	//{
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 170.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 220.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 270.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 320.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 370.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight),
	//	new Rect(200.0f * Global.ScreenWidth_Factor, 420.0f * Global.ScreenHeight_Factor, m_textWidth, m_textHeight)
	//};
	
	//private int[] m_ActiveFace = 
	//{
	//	-1, -1, -1, -1, -1, -1
	//};
	
	//static private float m_fFaceSize = 50.0f * Global.ScreenWidth_Factor;
	//private Rect[,] m_faceRects =
	//{
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 150.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)},
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 200.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)},
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 250.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)},
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 300.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)},
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 350.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)},
	//	{new Rect(550.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(650.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize), new Rect(750.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor, m_fFaceSize, m_fFaceSize)}
	//};

    public void OnEnable()
    {
        widhif = TextDisplayCanvas.instance.ShowPrefab("WhatIDidHowIFelt");
    }

    public void OnDisable()
    {
        Destroy(widhif);
    }

    // Use this for initialization
    void Start () 
	{
		//FontSizeManager.checkFontSize(m_skin);
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	public void OnGUI () 
	{		
		//GUI.skin = m_skin;
		//GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		//GUI.Label(new Rect(250.0f * Global.ScreenWidth_Factor,100.0f * Global.ScreenHeight_Factor,150.0f * Global.ScreenWidth_Factor,50.0f * Global.ScreenHeight_Factor), "What I did", "Titles");
		//GUI.Label(new Rect(625.0f * Global.ScreenWidth_Factor,100.0f * Global.ScreenHeight_Factor,150.0f * Global.ScreenWidth_Factor,50.0f * Global.ScreenHeight_Factor), "How I felt", "Titles");
		
		//Conversation box
		//GUI.Label(new Rect(275 * Global.ScreenWidth_Factor,470 * Global.ScreenHeight_Factor,490 * Global.ScreenWidth_Factor,135 * Global.ScreenHeight_Factor), "", "MiddleAreaBackGround");
		//GUI.Label(new Rect(320.0f * Global.ScreenWidth_Factor, 497.0f * Global.ScreenHeight_Factor, 410.0f * Global.ScreenWidth_Factor, 80.0f * Global.ScreenHeight_Factor), "Click on the faces which best match how you felt", "CenteredFont");
		//GUI.Label(new Rect(300.0f,470.0f,350.0f,50.0f), "Click on the faces which best match how you felt", "CenteredFont");
		
		//for (int i = 0; i < 6; ++i)
		//{
		//	for (int j = 0; j < 3; ++j)
		//	{
		//		if (j == m_ActiveFace[i])
		//		{
		//			GUI.DrawTexture(m_faceRects[i,j], m_tActiveFaces[j]);
		//		}
		//		else
		//		{
		//			GUI.DrawTexture(m_faceRects[i,j], m_tDeactiveFaces[j]);
		//		}
		//	}
			
		//	GUI.Label(m_wordsRect[i], m_worksDone[i]);
		//}
		
		//The next button
		if(widhif.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right"){
			Global.arrowKey = "";
			this.enabled =false;
			
			//Load the guide talk scene again
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//update the current scene name
			UpdateCurrentSceneName();
		}
	}
	
	//void CheckFaceClick()
	//{
	//	if(Input.GetMouseButtonUp(0))
	//	{
	//		for (int i = 0; i < 6; ++i)
	//		{
	//			bool found = false;
	//			for (int j = 0; j < 3; ++j)
	//			{	
	//				// save the mouse pos for ease of access
	//				float mouseX = Input.mousePosition.x;
	//				float mouseY = Input.mousePosition.y;
	//				if (m_faceRects[i,j].Contains(new Vector2(mouseX, Screen.height - mouseY)))
	//				{
	//					m_ActiveFace[i] = j;
	//					found = true;
	//					break;
	//				}
	//			}
				
	//			if (found) { break;}
	//		}
	//	}
	//}
	
	public void UpdateCurrentSceneName()
	{
		if(Global.CurrentLevelNumber == 3)
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene8");
		}
	}
}
