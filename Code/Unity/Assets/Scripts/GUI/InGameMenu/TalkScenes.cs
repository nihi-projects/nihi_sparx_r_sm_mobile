using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TalkScenes: MonoBehaviour {

	private static TalkScenes instanceRef;

	//Puiblic variables
	private string m_currentScene            = "";

    private string tuiScene = "";
	public string m_currentTuiScene { 
        get
        {
            return tuiScene;
        }
        set
        {
            tuiScene = value;
        }
    }
	
	public bool m_bGuardianNeedsLoad        = true;
	public bool m_voiceCanStart             = true;
	
	public bool m_bTalkedWithGuardian       = false;
	public bool m_bTalkedWithMentor         = false;
	
	public bool m_bL7NeedHelpOptionA        = false;
	public bool m_bL7UnNeedHelpOptionB      = false;
	
	public float m_autoSceneStartTime       = 0.0f;
	public bool m_bCassFiremanTalkStart     = false;
	
	public int[] m_PHQAAnswers;
	
	public int m_iTestingLevelNum           = 0;
	
	public bool[] m_bChiceSelectedDetector;
	
	//Private variables
	//Normal character txt files
	//----------------------------Conversation XML data files--------------------------
	private TextAsset m_guideXmlFilePathTextAsset;
	private TextAsset m_mentorXMLFilePathTextAsset;
	private TextAsset m_hopeXmlFilePathTextAsset;
	
	//1st level
	private TextAsset m_guardianXmlFilePathTextAsset;
	private TextAsset m_1npcXmlFilePathTextAsset;
	private TextAsset m_travelerXmlFilePathTextAsset;
	//2nd level
	private TextAsset m_cassXmlFilePathTextAsset;
	private TextAsset m_fireXmlFilePathTextAsset;
	private TextAsset m_yetiXmlFilePathTextAsset;
	private TextAsset m_fireperson2XmlFilePathTextAsset;
	//3nd level
	private TextAsset m_firespiritXmlFilePathTextAsset;
	private TextAsset m_npcXmlFilePathTextAsset;
    private TextAsset m_cass2XmlFilePathTextAsset;
    private TextAsset m_npc2XmlFilePathTextAsset;
	//4nd level
	private TextAsset m_darroXmlFilePathTextAsset;
	private TextAsset m_gnatsXmlFilePathTextAsset;
    private TextAsset m_cass3XmlFilePathTextAsset;
    private TextAsset m_sparksXmlFilePathTextAsset;
	//5nd level
	private TextAsset m_swapguyXmlFilePathTextAsset;
	private TextAsset m_ntsXmlFilePathTextAsset;
	//6nd level
	private TextAsset m_bridgeWomanXmlFilePathTextAsset;
	private TextAsset m_templeGuidianXmlFilePathTextAsset;
    private TextAsset m_cass4XmlFilePathTextAsset;
    //7nd level
    private TextAsset m_examinerXmlFilePathTextAsset;
	private TextAsset m_swapGuyXmlFilePathTextAsset;
	private TextAsset m_playerNeedsHelpTextAsset;
	private TextAsset m_cassL7XmlFilePathTextAsset;
	private TextAsset m_darroL7XmlFilePathTextAsset;
	private TextAsset m_firepersonL7XmlFilePathTextAsset;
	private TextAsset m_kogXmlFilePathTextAsset;
	
	private int m_iPerPHQAQuestionScore  = 0;
	private int m_iPHQAQuestionNum       = 1;
	private int m_iQ9Score				 = 0;
	public GUISkin m_skin               = null;
	
	private LoadTalkSceneXMLData m_loadSceneData;
	
	//----------------------------Audio files--------------------------------
	//Level 1 Audio list
	public AudioClip[] L1AudioFileList;
	//Level 2 Audio list
	public AudioClip[] L2AudioFileList;
	//Level 3 Audio list
	public AudioClip[] L3AudioFileList;
	//Level 4 Audio list
	public AudioClip[] L4AudioFileList;
	//Level 5 Audio list
	public AudioClip[] L5AudioFileList;
	//Level 6 Audio list
	public AudioClip[] L6AudioFileList;
	//level 7 Audio list
	public AudioClip[] L7AudioFileList;

    public float spatialBlend = 0.8f;
	private AudioSource m_characterVoiceAS;
	
	private bool m_bAutoConversationUnFinished = true;
	private bool m_bNPC1AnimationPlayOnce      = false;
	private bool m_bNPC2AnimationPlayOnce      = false;
	
	private bool m_bMultiChoice1OptionA = false;
	private bool m_bMultiChoice1OptionB = false;
	private bool m_bMultiChoice1OptionC = false;
	private bool m_bMultiChoice1OptionD = false;	
	
	private int m_PHQAAnswerIndex   = 0;
	private CapturedDataInput instance;
	
	//For web player test only
	public string m_sTestWholeDynamic   = "";
	public string m_sTestDynamicToken   = "";
	public string m_sTestDynamicURL     = "";

	public bool m_BoolSkipVoiceErrorLevel2 = false;

	//Implementation
	/*
	*
	* Awake function.
	*
	* The Awake function is run before Start.
	*
	*/
	public void Awake()
	{
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
			LoadLevelConversation(Global.CurrentLevelNumber);

			//Load the xml character conversation content here
			m_loadSceneData = GetComponent<LoadTalkSceneXMLData>();
		}else{
			DestroyImmediate (gameObject);
		}

		FontSizeManager.checkFontSize(m_skin);

		//Debug.Log ("Talk Scenes Awake: " + Global.CurrentLevelNumber);
		//This function load all the conversation contexts
	}
	
	/*
	*
	* Start function
	*
	*/
	public void Start ()
	{	
		if (Global.ScreenWidth_Factor == 0) 
			Global.ScreenWidth_Factor = 2;
		
		if (Global.ScreenHeight_Factor == 0) 
			Global.ScreenHeight_Factor = 2;
		
		//DontDestroyOnLoad(gameObject);

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//For local game test only
//		if(Application.isEditor){
//			Global.CurrentLevelNumber = m_iTestingLevelNum;
     //		}
		
		//Fill up the GUI objects list
		for(int i = 1; i <= 7; i++){
			Global.GUIObjects[i-1] = GameObject.Find ("L" + i.ToString() + "GUI");
		}
		for(int j = 1; j <= 7; j++){
			//Never set the L1GUI to inactive
			if(j != Global.CurrentLevelNumber){
                if(Global.GUIObjects[j - 1] != null )
				    Global.GUIObjects[j-1].SetActive(false);
			}
		}
		
		//Initialise the option selection array
		m_bChiceSelectedDetector = new bool[4];
		m_PHQAAnswers = new int[9];
		for(int i = 0; i< 9; i++){
			m_PHQAAnswers[i] = 0;
		}
		//AudioSource
		m_characterVoiceAS = (AudioSource)gameObject.AddComponent<AudioSource>();
		
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
    public string GetCurrentScene()
    {
        return m_currentScene;
    }

    public void SetCurrentDialogue(string _scene )
    {
        m_currentScene      = _scene;
    }

	/*
	*
	* Update function
	*
	*/
	public void Update ()
	{
		FontSizeManager.checkFontSize(m_skin);
		//HOPE ONLY have Statement and SubMultichoise conversation types
		if(Global.CurrentInteractNPC == "Hope"){
			//------------------------------------------------------------Tui Statement Display-----------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("Statement") && m_voiceCanStart)
            {
                //Stop the voice
                if (m_currentTuiScene != "")
                {
                    PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
                }
				m_voiceCanStart = false;
			}
            //------------------------------------------------------------Tui SubMultiChoice Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion"))
            {
                if (GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt && m_currentScene == "TerminateTalkScene" && m_currentTuiScene == "L3HopeScene2" && Vector3.Distance(GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position) > 7)
                {
                    GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
                }
                
                //Stop the voice
                if (m_voiceCanStart)
                {
                    if (m_currentTuiScene != "")
                    {
                        PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
                    }
                    m_voiceCanStart = false;
                }
            }
		}
		//NON HOPE
		else{
            //------------------------------------------------------------Non Hope Statement Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("Statement"))
            {
                //Start the voice
                if (m_voiceCanStart)
                {
                    PlayVoice(m_currentScene, m_loadSceneData.GetSoundFileName(m_currentScene));
                    m_voiceCanStart = false;
                }
            }
            //------------------------------------------------------------Non Hope SubMultiChoice Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("SubMultiChoiceQuestion"))
            {
                //Switch the camera
                if (!m_loadSceneData.GetCameraID(m_currentScene).Equals("") && !m_BoolSkipVoiceErrorLevel2)
                {
                    //Global.MoveCameraBasedOnID (m_loadSceneData.GetCameraID (m_currentScene));
                }
                else
                {
                    m_BoolSkipVoiceErrorLevel2 = false;
                }
                
                //Stop the voice
                if (m_voiceCanStart)
                {
                    PlayVoice(m_currentScene, m_loadSceneData.GetSoundFileName(m_currentScene));
                    m_voiceCanStart = false;
                }
            }
		}

		//----- HERE STARTS THE ORIGINAL UPDATE()
		//-----
		//Stop Tui's voice
		if(Global.CurrentInteractNPC == "Hope"){
			if(m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentTuiScene)){
				m_characterVoiceAS.Stop();
			}
		}
		else if(!Global.CurrentInteractNPC.Contains("Fake")){
			if(m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentScene)){
				m_characterVoiceAS.Stop();
				if(!(GameObject.Find("Cass") && GameObject.Find("Cass").GetComponent<Animation>().IsPlaying("walk")) && Global.CurrentInteractNPC != "FirespiritA" && 
					Global.CurrentInteractNPC != "KingOfGnats" && Global.CurrentInteractNPC != "OptimizationDoorPoint"){

					Debug.Log ("NPC: " + Global.CurrentInteractNPC);
                    if(GameObject.Find(Global.CurrentInteractNPC) != null && GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>() )
                    {
                        Animation NPCAnim = GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>();
                        Debug.Log("NPC: " + NPCAnim.GetClipCount().ToString());

                        GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>().Play("idle");
                    }
				}
			}
		}

		if(Global.CurrentInteractNPC == "FirespiritA"){
			if(!Global.GetPlayer().GetComponent<Animation>().IsPlaying("idle")){
				Global.GetPlayer().GetComponent<Animation>().Play("idle");
			}
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "TalkScenes");

        }
	}
    
    /*
	*
	* This function will be called everytime when this script is enabled
	*
	*/
    public void OnEnable(){
		FontSizeManager.checkFontSize(m_skin);
		if(Global.CurrentLevelNumber != 1){
			//Debug.Log ("Talk Scenes OnEnable: " + Global.CurrentLevelNumber);
			//This function load all the conversation contexts
			LoadLevelConversation(Global.CurrentLevelNumber);
			
			//Load the xml character conversation content here
			m_loadSceneData = GetComponent<LoadTalkSceneXMLData>();
		}
		if(Global.levelStarted == false){
			//Guide Interacted
			if(Global.CurrentInteractNPC == "Guide"){
				
				//Before the player go to the LevelS
				if(Global.LevelSVisited == false && SceneManager.GetActiveScene().name == "GuideScene"){
					if(Global.CurrentLevelNumber == 1){
						SetCurrentDialogue("GuideScene1");		
						
						Debug.Log ("Level 1 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 2){
						SetCurrentDialogue("L2GuideScene1");		
						
						Debug.Log ("Level 2 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 3){
						SetCurrentDialogue("L3GuideScene1");		
						
						Debug.Log ("Level 3 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 4){
						SetCurrentDialogue("L4GuideScene1");	
						
						Debug.Log ("Level 4 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 5){
						SetCurrentDialogue("L5GuideScene1");
						
						Debug.Log ("Level 5 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 6){
						SetCurrentDialogue("L6GuideScene1");	
						
						Debug.Log ("Level 6 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 7){
						SetCurrentDialogue("L7GuideScene1");
						
						Debug.Log ("Level 7 Guide conversation data is loaded...");
					}
				}
				//After the player visited the LevelS
				else{
					if(Global.CurrentLevelNumber == 1){
						SetCurrentDialogue("GuideScene13");		
						Debug.Log ("Level 1 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 2){
						SetCurrentDialogue("L2GuideScene13");		
						Debug.Log ("Level 2 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 3){
						SetCurrentDialogue("L3GuideScene14");		
						Debug.Log ("Level 3 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 4){
						SetCurrentDialogue("L4GuideScene17");		
						Debug.Log ("Level 4 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 5){
						SetCurrentDialogue("L5GuideScene18");		
						Debug.Log ("Level 5 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 6){
						SetCurrentDialogue("L6GuideScene11");		
						Debug.Log ("Level 6 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 7){
						SetCurrentDialogue("L7GuideScene11");		
						Debug.Log ("Level 7 Guide conversation data continue is loaded...");
					}
				}
				
				m_loadSceneData.GeneralReadFromXML(m_guideXmlFilePathTextAsset);
				Debug.Log ("Loaded Guide XML: " + m_loadSceneData.GetCharacterName(m_currentScene));
				Global.levelStarted = true;
			}
		}

		string currentInteractingNPC = Global.CurrentInteractNPC;
		
		if (Global.GetPlayer() && m_currentScene != "TerminateTalkScene" && m_currentTuiScene != "TuiTerminateScene" && m_currentTuiScene != "L2TuiTerminateScene")
		{
            if(Global.GetPlayer())
			    Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "TalkScenes2");
		}

		//-------------------------------------------------Load all the (LEVEL 1) character data------------------------------------------------------(Global.levelStarted == false){
		if(Global.CurrentLevelNumber == 1){
			//Guardian
			if(Global.CurrentInteractNPC == "guardian" && Global.TempTalkCharacter != "guardian"){
				Global.TempTalkCharacter = "guardian";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("GuardianScene1");
					m_bTalkedWithGuardian = true;
				}
			
				m_loadSceneData.GeneralReadFromXML(m_guardianXmlFilePathTextAsset);
				Debug.Log ("Level 1 Guardian conversation data is loaded...");
			}
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("MentorScene4");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 1 Mentor conversation data is loaded...");
			}
			//Npc
			if(Global.CurrentInteractNPC == "1npc" && Global.TempTalkCharacter != "1npc"){
				Global.TempTalkCharacter = "1npc";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("RangerScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_1npcXmlFilePathTextAsset);
				Debug.Log ("Level 1 Npc conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 1 Hope conversation data is loaded...");
			}
			//Traveller
			if(Global.CurrentInteractNPC == "Traveller" && Global.TempTalkCharacter != "Traveller"){
				Global.TempTalkCharacter = "Traveller";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("TravellerScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_travelerXmlFilePathTextAsset);
				Debug.Log ("Level 1 Traveller conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 2) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 2){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
                    SetCurrentDialogue("L2MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L2MentorScene5");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 2 Mentor conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					if(!Global.GetGameObjectByID("32003").GetComponent<IceCaveDoor>().m_bGotL2OpenDoorFire){
						m_currentTuiScene = "L2HopeScene1";
					}
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 2 Hope conversation data is loaded...");
			}
			//Cass
			if(Global.CurrentInteractNPC == "Cass" && m_currentScene != "TerminateCassFiremanScene" && m_currentScene != "L2CassScene1_2")
            {
				Global.TempTalkCharacter = "Cass";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L2CassScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_cassXmlFilePathTextAsset);
				m_loadSceneData.GeneralReadFromXML(m_fireXmlFilePathTextAsset);
				Debug.Log ("Level 2 Cass conversation data is loaded...");
			}
			//Fireperson
			if(Global.CurrentInteractNPC == "Fireperson" && Global.TempTalkCharacter != "Fireperson"){
				Global.TempTalkCharacter = "Fireperson";
                Global.Level2FireguyInteraction = true;

                if (Global.LevelSVisited == false){
					SetCurrentDialogue("L2FireScene1");
					GameObject.Find ("Cass").GetComponent<CharacterInteraction>().enabled = true;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_fireXmlFilePathTextAsset);
				Debug.Log ("Level 2 Fire Person conversation data is loaded...");
			}
			//Yeti
			if(Global.CurrentInteractNPC == "Yeti" && Global.TempTalkCharacter != "Yeti"){
				Global.TempTalkCharacter = "Yeti";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L2YetiScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_yetiXmlFilePathTextAsset);
				Debug.Log ("Level 2 Yeti conversation data is loaded...");
			}
			//Fireperson2
			if(Global.CurrentInteractNPC == "Fireperson2" && Global.TempTalkCharacter != "Fireperson2"){
				Global.TempTalkCharacter = "Fireperson2";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L2Fireman2Scene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_fireperson2XmlFilePathTextAsset);
				Debug.Log ("Level 2 Fireperson2 conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 3) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 3){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L3MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L3MentorScene5");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 3 Mentor conversation data is loaded...");
			}
			//npclava
			if(Global.CurrentInteractNPC == "npclava" && Global.TempTalkCharacter != "npclava"){
				Global.TempTalkCharacter = "npclava";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L3NpcScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_npcXmlFilePathTextAsset);
				Debug.Log ("Level 3 npclava conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L3HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 3 Hope conversation data is loaded...");
			}
			if(Global.CurrentInteractNPC == "FakeTuiBeforeMiniGames" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L3HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 3 Hope conversation data is loaded...");
			}
			//FireSpirit
			if(Global.CurrentInteractNPC == "FirespiritA" && Global.TempTalkCharacter != "FirespiritA"){
				Global.TempTalkCharacter = "FirespiritA";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					SetCurrentDialogue("L3FirepspiritScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_firespiritXmlFilePathTextAsset);
				Debug.Log ("Level 3 FirepspiritA conversation data is loaded...");
			}
            //Cass
            if (Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass")
            {
                Global.TempTalkCharacter = "Cass";

                if (Global.LevelSVisited == false)
                {
                    SetCurrentDialogue("L3CassScene1");
                }

                m_loadSceneData.GeneralReadFromXML(m_cass2XmlFilePathTextAsset);
                Debug.Log("Level 3 Cass conversation data is loaded...");
            }

            //npc2
            if (Global.CurrentInteractNPC == "npclava2" && Global.TempTalkCharacter != "npclava2"){
				Global.TempTalkCharacter = "npclava2";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L3Npc2Scene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_npc2XmlFilePathTextAsset);
				Debug.Log ("Level 3 npclava2 conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 4) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 4){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L4MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L4MentorScene5");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 4 Mentor conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L4HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 4 Hope conversation data is loaded...");
			}
            //Cass
            if (Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass")
            {
                Global.TempTalkCharacter = "Cass";

                if (Global.LevelSVisited == false)
                {
                    SetCurrentDialogue("L4CassScene1");
                }

                m_loadSceneData.GeneralReadFromXML(m_cass3XmlFilePathTextAsset);
                Debug.Log("Level 4 Cass conversation data is loaded...");
            }
            //Cass_Top
            if (Global.CurrentInteractNPC == "Cass_Top" && Global.TempTalkCharacter != "Cass_Top")
            {
                Global.TempTalkCharacter = "Cass_Top";

                if (Global.LevelSVisited == false)
                {
                    SetCurrentDialogue("L4CassScene2");
                }

                m_loadSceneData.GeneralReadFromXML(m_cass3XmlFilePathTextAsset);
                Debug.Log("Level 4 Cass conversation data is loaded...");
            }
            //Darro
            if (Global.CurrentInteractNPC == "Darro" && Global.TempTalkCharacter != "Darro"){
				Global.TempTalkCharacter = "Darro";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L4DarroScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_darroXmlFilePathTextAsset);
				Debug.Log ("Level 4 Darro conversation data is loaded...");
			}
			//Spark1
			string[] names = {"Spark1", "Spark2", "Spark3", "Spark4"};
			for (int i = 0; i < names.Length; ++i)
			{
				if(Global.CurrentInteractNPC == names[i] && Global.TempTalkCharacter != names[i]){
					Global.TempTalkCharacter = names[i];
					
					if(Global.LevelSVisited == false){
						//SetCurrentScene"L4SparkScene" + (i + 1).ToString();
					}
					
					m_loadSceneData.GeneralReadFromXML(m_sparksXmlFilePathTextAsset);
					Debug.Log ("Level 4 spark conversation data is loaded...");
					break;
				}
			}
		}
		//-------------------------------------------------Load all the (LEVEL 5) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 5){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L5MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L5MentorScene6");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 5 Mentor conversation data is loaded...");
			}
			//Swampguy
			if(Global.CurrentInteractNPC == "Swampguy" && Global.TempTalkCharacter != "Swampguy"){
				Global.TempTalkCharacter = "Swampguy";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L5SwampguyScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_swapguyXmlFilePathTextAsset);
				Debug.Log ("Level 5 Swampguy conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L5HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 5 Hope conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 6) character data------------------------------------------------------------			//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 6){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L6MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L6MentorScene5");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 6 Mentor conversation data is loaded...");
			}
            //Cass
            if (Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass")
            {
                Global.TempTalkCharacter = "Cass";

                if (Global.LevelSVisited == false)
                {
                    SetCurrentDialogue("L6CassScene1");
                }

                m_loadSceneData.GeneralReadFromXML(m_cass4XmlFilePathTextAsset);
                Debug.Log("Level 6 Cass conversation data is loaded...");
            }
            //Gatekeeper_female
            if (Global.CurrentInteractNPC == "Gatekeeper_female" && Global.TempTalkCharacter != "Gatekeeper_female"){
				Global.TempTalkCharacter = "Gatekeeper_female";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L6WomanBridgeScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_bridgeWomanXmlFilePathTextAsset);
				Debug.Log ("Level 6 Gatekeeper_female conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L6HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 6 Hope conversation data is loaded...");
			}
			//Temple Guardian
			if(Global.CurrentInteractNPC == "6_npc2" && Global.TempTalkCharacter != "6_npc2"){
				Global.TempTalkCharacter = "6_npc2";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L6TempleGuardianScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_templeGuidianXmlFilePathTextAsset);
				Debug.Log ("Level 6 Temple Guardian conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------			//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 7){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7MentorScene1");
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					SetCurrentDialogue("L7MentorScene3");
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 7 Mentor conversation data is loaded...");
			}
			//Traveller
			if(Global.CurrentInteractNPC == "Traveller" && Global.TempTalkCharacter != "Traveller"){
				Global.TempTalkCharacter = "Traveller";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7ExaminerScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_examinerXmlFilePathTextAsset);
				Debug.Log ("Level 7 Traveller conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L7HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 7 Hope conversation data is loaded...");
			}
			//Swampguy
			if(Global.CurrentInteractNPC == "Swampguy" && Global.TempTalkCharacter != "Swampguy"){
				Global.TempTalkCharacter = "Swampguy";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7GuardScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_swapGuyXmlFilePathTextAsset);
				Debug.Log ("Level 7 Swampguy conversation data is loaded...");
			}
			//Ask for help
			if(Global.CurrentInteractNPC == "Boy" || Global.CurrentInteractNPC == "Girl"){
				SetCurrentDialogue("L7PlayerScene1");
				
				m_loadSceneData.GeneralReadFromXML(m_playerNeedsHelpTextAsset);
				Debug.Log ("Level 7 player ask for help conversation data is loaded...");
			}
			//Cass
			if(Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass"){
				Global.TempTalkCharacter = "Cass";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7CassScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_cassL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 Cass conversation data is loaded...");
			}
			//Darro
			if(Global.CurrentInteractNPC == "Darro" && Global.TempTalkCharacter != "Darro"){
				Global.TempTalkCharacter = "Darro";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7DarroScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_darroL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 Darro conversation data is loaded...");
			}
			//Fireperson
			if(Global.CurrentInteractNPC == "Fireperson" && Global.TempTalkCharacter != "Fireperson"){
				Global.TempTalkCharacter = "Fireperson";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7CanyonDwellerScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_firepersonL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 FirePerson conversation data is loaded...");
			}
			//KOG
			if(Global.CurrentInteractNPC == "KingOfGnats" && Global.TempTalkCharacter != "KingOfGnats"){
				Global.TempTalkCharacter = "KingOfGnats";
				
				if(Global.LevelSVisited == false){
					SetCurrentDialogue("L7KogScene1");
				}
				
				m_loadSceneData.GeneralReadFromXML(m_kogXmlFilePathTextAsset);
				Debug.Log ("Level 7 KOG conversation data is loaded...");
			}
		}
	}

    /*
	*
	* OnGUI function create the GUI of the conversation box
	* This OnGUI function displays all the conversation context
	* in the dialog box.
	*
	*/
    public void OnGUI()
    {
        GUI.depth = -1;
        GUI.skin = m_skin;

        //-------------------- TEST CODE FONT SIZE ISSUE V

        string FontInformation = "";
        int counter;
        counter = 0;

        FontInformation = "Box:" + m_skin.box.fontSize;
        FontInformation = FontInformation + " Button:" + m_skin.button.fontSize;
        FontInformation = FontInformation + " Label:" + m_skin.label.fontSize;
        FontInformation = FontInformation + " TextArea:" + m_skin.textArea.fontSize;
        FontInformation = FontInformation + " TextField:" + m_skin.textField.fontSize;
        FontInformation = FontInformation + " Toggle:" + m_skin.toggle.fontSize;
        FontInformation = FontInformation + " ToggleImage:" + m_skin.toggle.fixedWidth;
        FontInformation = FontInformation + " ToggleOffsetText:" + m_skin.toggle.contentOffset.x;

        foreach (GUIStyle style in m_skin.customStyles)
        {
            counter++;
            FontInformation = FontInformation + " FontStyle_" + counter.ToString() + ":" + style.fontSize;
        }
        Debug.Log(Screen.dpi.ToString());
        //-------------------- TEST CODE FONT SIZE ISSUE A
        //HOPE ONLY have Statement and SubMultichoise conversation types
        if (Global.CurrentInteractNPC == "Hope")
        {
            //------------------------------------------------------------Tui Statement Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("Statement"))
            {
                TextDisplayCanvas.instance.HideMultiChoice();
                //Switch the camera
                if (!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals(""))
                {
                    Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
                }

                //Display the statement conversation
                DisplayStatementText();
            }
            //------------------------------------------------------------Tui SubMultiChoice Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion"))
            {
                TextDisplayCanvas.instance.HideStatement();
                //Switch the camera
                if (!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals(""))
                {
                    Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
                }

                if (GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt && m_currentScene == "TerminateTalkScene" && m_currentTuiScene == "L3HopeScene2" && Vector3.Distance(GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position) > 7)
                {
                    GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
                }

                DisplaySubMultichoicetext();
            }
        }
        //NON HOPE
        else
        {
            //------------------------------------------------------------Non Hope Statement Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("Statement"))
            {
                if (TextDisplayCanvas.instance != null)
                    TextDisplayCanvas.instance.HideMultiChoice();
                else
                    Debug.LogError("No text display canvas! Is it enabled in the heirachy.");

                //The 1st NPC's animation
                if (m_bNPC1AnimationPlayOnce &&
                    m_loadSceneData.GetObjectAnimationName(m_currentScene) != "")
                {
                    GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>()
                        .Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));

                    m_bNPC1AnimationPlayOnce = false;
                }

                //The 2nd NPC's animation
                if (m_bNPC2AnimationPlayOnce &&
                    m_loadSceneData.GetOtherObjAniName(m_currentScene) != "")
                {
                    GameObject.Find(m_loadSceneData.GetOtherObjCharacterName(m_currentScene)).GetComponent<Animation>()
                        .Play(m_loadSceneData.GetOtherObjAniName(m_currentScene));

                    m_bNPC2AnimationPlayOnce = false;
                }

                //Switch the camera
                if (!m_loadSceneData.GetCameraID(m_currentScene).Equals("") && Application.loadedLevelName != "GuideScene")
                {
                    Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentScene));
                }

                //Display the statement conversation
                DisplayStatementText();
            }
            //------------------------------------------------------------Non Hope SubMultiChoice Display-----------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("SubMultiChoiceQuestion"))
            {
                TextDisplayCanvas.instance.HideStatement();
                //Animation play
                if (m_loadSceneData.GetObjectAnimationName(m_currentScene) != "")
                {
                    //Play the animation if it exists
                    GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>().Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));
                }

                //Switch the camera
                if (!m_loadSceneData.GetCameraID(m_currentScene).Equals("") && !m_BoolSkipVoiceErrorLevel2)
                {
                    Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentScene));
                }
                else
                {
                    m_BoolSkipVoiceErrorLevel2 = false;
                }

                DisplaySubMultichoicetext();
            }
            //------------------------------------------------------------Multichoice Display------------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("MultiChoice"))
            {
                TextDisplayCanvas.instance.HideStatement();
                GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>().Play("idle");
                DisplayMultiChoiceText(Global.CurrentLevelNumber);
            }
            //------------------------------------------------------------NonVoice Statement Display------------------------------------
            if (m_loadSceneData.GetQuestionType(m_currentScene).Equals("NonVoiceStatement"))
            {
                DisplayNonVoiceText();
            }
        }
    }
	
	/*
	*
	* This function displays the statement text
	*
	*/
	public void DisplayStatementText()
	{
		Color characterTitleColour = SwitchCharacterNameColor();

        //Normal size dialog box
        string characterText = m_currentScene;
        if (Global.CurrentInteractNPC == "Hope")
            characterText = m_currentTuiScene;
        else
        {
            if (IsAutoTalkScene(m_currentScene))
            {
                AutoConversationSceneSwitch();
                if( m_currentScene == "TerminateTalkScene")
                {
                    return;
                }
            }
        }

        //Display a dialogue box.
        StatementBox box = TextDisplayCanvas.instance.ShowStatementBox( m_loadSceneData.GetCharacterName(characterText),
                                                    m_loadSceneData.LoadVoiceText(characterText),
                                                    characterTitleColour, 
                                                    m_loadSceneData.PreviousButtonExist(characterText), 
                                                    m_loadSceneData.NextButtonExist(characterText));

        //Hope-----------------------------------------------------------------------------------------------------------
        if (Global.CurrentInteractNPC == "Hope"){
            //Previous button
            if (m_loadSceneData.PreviousButtonExist(m_currentTuiScene))
            {
                

                if (box.WasPreviousClicked() || Global.arrowKey == "left")
                {
                    //Stop the orginal voice, and loads up new voice
                    m_characterVoiceAS.Stop();

                    //Update the new scene (Hope)
                    m_currentTuiScene = m_loadSceneData.GetPreSceneName(m_currentTuiScene);
                    m_voiceCanStart = true;
                }
            }
			
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentTuiScene)){
                //Shows the next button on the display canvas.
                

				if(TextDisplayCanvas.instance.statementBox.WasNextClicked() || Global.arrowKey == "right"){
					Global.arrowKey = "";
                    
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					//Update the new scene (Hope)
					m_currentTuiScene = m_loadSceneData.GetNextSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
		}
		//Non Hope-----------------------------------------------------------------------------------------------------------
		else{
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentScene))
            {

                if (box.WasPreviousClicked() || Global.arrowKey == "left"){
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					//Update the new scene (Non Hope)
					SetCurrentDialogue(m_loadSceneData.GetPreSceneName(m_currentScene));
					m_voiceCanStart = true;
				}
			}
			
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentScene))
            {
				if(box.WasNextClicked() || Global.arrowKey == "right"){
					Global.arrowKey = "";

					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					if (Global.CurrentLevelNumber == 3)
					{
						if (m_currentScene == "L3MentorScene2")
						{
							Global.NPCGiveAwayObject("Mentor", "give", "_[id]30008_blinc");
							Global.m_pickNPCName = Global.GetPlayer().name;
							Global.m_PickAniName = "take";
						}
					}
					else if (Global.CurrentLevelNumber == 6)
					{
						if (m_currentScene == "L6MentorScene3")
						{
							Global.NPCGiveAwayObject("Mentor", "give", "_[id]30009_rapakey");
							Global.m_pickNPCName = Global.GetPlayer().name;
							Global.m_PickAniName = "take";
						}
					}
					
					//Update the new scene (Non Hope)
					if (Global.CurrentInteractNPC == "Guide" && m_currentScene.Contains("_A"))
					{
						/*if (m_iQ9Score == 1)
						{
							SetCurrentScene"GuideScene9_A10";
						}
						else if (m_iQ9Score == 2 || m_iQ9Score == 3)
						{*/
						if(m_iQ9Score >= 1){
							SetCurrentDialogue("GuideScene9_A11");
						}
						else
						{
							SetCurrentDialogue(m_loadSceneData.GetNextSceneName(m_currentScene));
						}
						m_iQ9Score = 0;
					}
					else
					{
						SetCurrentDialogue(m_loadSceneData.GetNextSceneName(m_currentScene));
					}
					m_voiceCanStart = true;
					
					m_bNPC1AnimationPlayOnce = true;
					m_bNPC2AnimationPlayOnce = true;
					
					
				}
			}
		}
	}
	
	/*
	*
	* This function displays the multichoice text
	*
	*/
	public void DisplayMultiChoiceText(int _levelNum)
	{
		Debug.Log ("DisplayMultiChoiceText");
		Color nameColour = SwitchCharacterNameColor();

        MultiChoiceBox multiBox = TextDisplayCanvas.instance.multiChoiceBox;
        if (multiBox == null)
            Debug.LogError("Couldn't find the multi choice box.");

        multiBox.gameObject.SetActive(true);
        multiBox.SetStatementData( m_loadSceneData.GetCharacterName(m_currentScene), m_loadSceneData.GetQuestionText(m_currentScene, 1), nameColour, "" );

        ResultBarchart chart = GetComponent<ResultBarchart>();
        
        //Set all the options.
        multiBox.SetChoice(EMultiChoice.CHOICE_A, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "A"));
        multiBox.SetChoice(EMultiChoice.CHOICE_B, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "B"));
        multiBox.SetChoice(EMultiChoice.CHOICE_C, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "C"));
        multiBox.SetChoice(EMultiChoice.CHOICE_D, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "D"));

        string[] options = new string[] { "A", "B", "C", "D" };
        int[] iq9Scores = new int[] { 0, 1, 2, 3 };
        bool showButton = false;
        int selectedOption = 0;
        for (int i = 0; i < options.Length; ++i)
        {
            if (m_loadSceneData.GetQuestionOptions(m_currentScene, 1, options[i]) != "")
            {
                if (multiBox.IsOptionSelected((EMultiChoice)(i)) || Global.multiChoice == options[i])
                {
                    //CommonMultichoiceCode(options[i], i, m_currentScene, true);
                    showButton = true;
                    selectedOption = i;
                    if (m_currentScene.Contains("_9"))
                    {
                        m_iQ9Score = iq9Scores[i];
                    }
                    break;
                }
            }
              
        }

        //Previous button
        if (m_loadSceneData.PreviousButtonExist(m_currentScene))
        {
            multiBox.ShowPreviousButton(true);

            if (multiBox.WasPreviousClicked() || Global.arrowKey == "left")
            {
                multiBox.ResetSelection();
                //Stop the orginal voice, and loads up new voice
                m_characterVoiceAS.Stop();

                //Update the new scene (Non Hope)
                SetCurrentDialogue(m_loadSceneData.GetPreSceneName(m_currentScene));
                m_voiceCanStart = true;

                if (m_PHQAAnswerIndex > 0)
                {
                    m_PHQAAnswerIndex--;
                    chart.RemoveScore(m_PHQAAnswers[m_PHQAAnswerIndex]);
                }
            }
        }

        //Next button
        if (m_loadSceneData.NextButtonExist(m_currentScene) && showButton)
        {
            multiBox.ShowNextButton(true);

            if (multiBox.WasNextClicked() || Global.arrowKey == "right")
            {
                multiBox.ResetSelection();
                Global.arrowKey = "";

                chart.AddScore(selectedOption);
                m_iPerPHQAQuestionScore = selectedOption;

                SetCurrentDialogue(m_loadSceneData.GetNextSceneName(m_currentScene));

                //The voice will be played again if the question type is statement or subMultichoice
                m_voiceCanStart = true;

                if (m_PHQAAnswers.Length == 0)
                {
                    m_PHQAAnswers = new int[9];
                }
                m_PHQAAnswers[m_PHQAAnswerIndex] = m_iPerPHQAQuestionScore;

                //CapturedDataInput instance2 = CapturedDataInput.GetInstance;
                //Save the PHQA data once all the multi choice questions are answered
                if (m_PHQAAnswerIndex == 8)
                {
                    //SAVE PHQA EVENT
                    StartCoroutine(instance.SaveUserPHQAEventToServer_NewJsonSchema(1, Global.CurrentLevelNumber,
                                                                         m_PHQAAnswers[0], m_PHQAAnswers[1], m_PHQAAnswers[2],
                                                                          m_PHQAAnswers[3], m_PHQAAnswers[4], m_PHQAAnswers[5],
                                                                          m_PHQAAnswers[6], m_PHQAAnswers[7], m_PHQAAnswers[8]));
                }

                m_PHQAAnswerIndex += 1;
            }
        }
	}
	
	/*
	*
	* This function displays the nonVoice text
	*
	*/
	public void DisplayNonVoiceText()
	{
		SwitchCharacterNameColor();
		
		//Display character's name
		GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
		
		//It is used to display the normal size dilog box
		if(m_loadSceneData.LoadNonVoiceText(m_currentScene).Length < 100){
			GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
			GUI.Label(new Rect(120,407,725 * Global.ScreenWidth_Factor,100 * Global.ScreenHeight_Factor), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}
		
		//Large size dialog box
		else{
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(130.0f, 75.0f, 700.0f * Global.ScreenWidth_Factor, 400.0f * Global.ScreenHeight_Factor), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}
		
		//Next button
		if(m_loadSceneData.NextButtonExist(m_currentScene)){
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					SetCurrentDialogue(m_loadSceneData.GetNextSceneName(m_currentScene));
					
					//The voice will be played again if the question type is statement or subMultichoice
					m_voiceCanStart = true;
				}
			}
		}
	}
	
	/*
	*
	* This function displays the subMultichoice text
	*
	*/
	public void DisplaySubMultichoicetext()
	{	
		string tempSceneName = "";
		string hopeTempSceneName = "";

        MultiChoiceBox multiBox = TextDisplayCanvas.instance.multiChoiceBox;
        multiBox.ShowNextButton(false);
        
        SwitchCharacterNameColor();

        string currentSceneName = m_currentScene;
        if (Global.CurrentInteractNPC == "Hope")
            currentSceneName = m_currentTuiScene;

        //Hope-------------------------------------------------------------------------------------
        if (Global.CurrentInteractNPC == "Hope")
        {
            //Set character data.
            Color characterNameColour = SwitchCharacterNameColor();
            multiBox.gameObject.SetActive(true);
            multiBox.SetStatementData(m_loadSceneData.GetCharacterName(currentSceneName), m_loadSceneData.LoadVoiceText(currentSceneName), characterNameColour, "");

            //Set all the options.
            multiBox.SetChoice(EMultiChoice.CHOICE_A, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "A"));
            multiBox.SetChoice(EMultiChoice.CHOICE_B, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "B"));
            multiBox.SetChoice(EMultiChoice.CHOICE_C, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "C"));
            multiBox.SetChoice(EMultiChoice.CHOICE_D, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "D"));
            
            string[] options = new string[] { "A", "B", "C", "D" };
            for( int i = 0; i < options.Length; ++i )
            {
                if (multiBox.IsOptionSelected((EMultiChoice)(i)) || Global.multiChoice == options[i])
                {
                    hopeTempSceneName = CommonMultichoiceCode(options[i], i, currentSceneName);
                }
            }   

            //Previous button
            if (m_loadSceneData.PreviousButtonExist(currentSceneName))
            {
                multiBox.ShowPreviousButton(true);

                if (multiBox.WasPreviousClicked() || Global.arrowKey == "left")
                {
                    //Stop the orginal voice, and loads up new voice
                    m_characterVoiceAS.Stop();
                    currentSceneName = m_loadSceneData.GetPreSceneName(currentSceneName);
                    m_voiceCanStart = true;
                }
            }

            //Next button
            if (m_loadSceneData.NextButtonExist(currentSceneName))
            {
                if (IsOptionsSelected())
                {
                    multiBox.ShowNextButton(true);

                    if (multiBox.WasNextClicked() || Global.arrowKey == "right")
                    {
                        Global.arrowKey = "";

                        if (Global.CurrentLevelNumber == 4)
                        {
                            Debug.Log("Releasing Camera");
                            GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
                        }
                        string selectedLabel = multiBox.SelectedLetter();

                        if (m_loadSceneData.GetQuestionType(currentSceneName).Equals("SubMultiChoiceQuestion"))
                        {
                            if (m_loadSceneData.GetSubMultichoiceGotoType(currentSceneName, selectedLabel).Equals("Imag"))
                            {
                                //Load the script
                                m_loadSceneData.SwitchScriptByName(m_loadSceneData.GetSubMultichoiceNextSceneName(currentSceneName, selectedLabel));
                            }
                            else
                            {
                                //Load to another scene
                                m_currentTuiScene = hopeTempSceneName;
                            }
                        }

                        //The voice will be played again if the question type is statement or subMultichoice
                        //Stop the orginal voice, and loads up new voice
                        m_characterVoiceAS.Stop();
                        m_voiceCanStart = true;
                    }
                }
            }//End of next button
        }
        //Non Hope-------------------------------------------------------------------------------------
        else
        {
            //Set character data.
            Color characterNameColour = SwitchCharacterNameColor();
            multiBox.gameObject.SetActive(true);
            multiBox.SetStatementData(m_loadSceneData.GetCharacterName(currentSceneName), m_loadSceneData.LoadVoiceText(currentSceneName), characterNameColour, "");

            //Display Choices
            multiBox.SetChoice(EMultiChoice.CHOICE_A, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "A"));
            multiBox.SetChoice(EMultiChoice.CHOICE_B, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "B"));
            multiBox.SetChoice(EMultiChoice.CHOICE_C, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "C"));
            multiBox.SetChoice(EMultiChoice.CHOICE_D, m_loadSceneData.SubMultiChoiceGetQuestionOptions(currentSceneName, "D"));

            //Options
            if (multiBox.IsOptionSelected(EMultiChoice.CHOICE_A) || Global.multiChoice == "A")
            {
                tempSceneName = CommonMultichoiceCode("A", 0, currentSceneName);

                //MAGIC NUMBERS
                //----------------------------------------------------------------------------
                if (currentSceneName == "L4GuideScene24")
                {
                    L4EnterTexts.m_bTriggered = true;
                }
                //For level 7 only
                m_bL7NeedHelpOptionA = true;
                m_bL7UnNeedHelpOptionB = false;
                //----------------------------------------------------------------------------
            }

            if (multiBox.IsOptionSelected(EMultiChoice.CHOICE_B) || Global.multiChoice == "B")
            {
                tempSceneName = CommonMultichoiceCode("B", 1, currentSceneName);

                //MAGIC NUMBERS
                //----------------------------------------------------------------------------
                if (currentSceneName == "L4GuideScene24")
                {
                    L4EnterTexts.m_bTriggered = false;
                }

                //For level 7 only
                m_bL7UnNeedHelpOptionB = true;
                m_bL7NeedHelpOptionA = false;
                //----------------------------------------------------------------------------
            }
            if (multiBox.IsOptionSelected(EMultiChoice.CHOICE_C) || Global.multiChoice == "C")
            {
                tempSceneName = CommonMultichoiceCode("C", 2, currentSceneName);
            }
            if (multiBox.IsOptionSelected(EMultiChoice.CHOICE_D) || Global.multiChoice == "D")
            {
                tempSceneName = CommonMultichoiceCode("D", 3, currentSceneName);
            }

            //Previous button
            if (m_loadSceneData.PreviousButtonExist(currentSceneName) == true)
            {
                multiBox.ShowPreviousButton(true);
                    
                if (multiBox.WasPreviousClicked() || Global.arrowKey == "left")
                {
                    //Stop the orginal voice, and loads up new voice
                    m_characterVoiceAS.Stop();
                    SetCurrentDialogue(m_loadSceneData.GetPreSceneName(currentSceneName));
                    m_voiceCanStart = true;
                }
            }
            //Next button
            if (m_loadSceneData.NextButtonExist(currentSceneName))
            {
                if (IsOptionsSelected())
                {
                    //Show the next button until something is clicked.
                    multiBox.ShowNextButton(true);

                    if ( multiBox.WasNextClicked() || Global.arrowKey == "right")
                    {
                        Global.arrowKey = "";

                        string selectedLabel = multiBox.SelectedLetter();

                        if (m_loadSceneData.GetQuestionType(currentSceneName).Equals("SubMultiChoiceQuestion"))
                        {
                            if (m_loadSceneData.GetSubMultichoiceGotoType(currentSceneName, selectedLabel).Equals("Imag"))
                            {
                                //Load the script
                                string nextScene = m_loadSceneData.GetSubMultichoiceNextSceneName(currentSceneName, selectedLabel);
                                m_loadSceneData.SwitchScriptByName(nextScene);
                                SetCurrentDialogue(nextScene);
                            }
                            else
                            {
                                //Load to another scene
                                SetCurrentDialogue(tempSceneName);
                            }
                        }

                        if (currentSceneName == "L7PlayerScene1" && selectedLabel == "A")
                        {
                            Global.GetPlayer().GetComponent<PlayerMovement>().
                            MoveToPosition(Global.GetObjectPositionByID("TalkToPeople"),
                            Global.GetObjectPositionByID("LookAtMiniGameOne"));
                            Global.m_bPlayerAcceptHelp = true;
                        }

                        if (currentSceneName == "L7CanyonDwellerScene2_1")
                        {
                            Global.m_bPlayerAfterFire = true;
                        }
                        
                        //The voice will be played again if the question type is statement or subMultichoice
                        //Stop the orginal voice, and loads up new voice
                        m_characterVoiceAS.Stop();
                        m_voiceCanStart = true;

                        m_bL7NeedHelpOptionA = false;
                        m_bL7UnNeedHelpOptionB = false;

                        TextDisplayCanvas.instance.HideDialogue();
                    }
                }
            }
        }
	}

    string CommonMultichoiceCode( string _choice, int _resetSelectionNumber, string _sceneName, bool _bNextSceneNameOrSub = false )
    {
        if (Global.multiChoice == _choice)
        {
            Global.multiChoice = "";
        }
        //This code is used to record which option is being selected
        OptionSelecetionReset(_resetSelectionNumber);

        if (_bNextSceneNameOrSub == false)
            return m_loadSceneData.GetSubMultichoiceNextSceneName(_sceneName, _choice);
        else
            return m_loadSceneData.GetNextSceneName(_sceneName);
    }
	
	/*
	*
	* This function is used to check whether any of the submultichoice 
	* options is being selected
	*
	*/
	public void OptionSelecetionReset(int _selectIndex)
	{
		//Only for web player build
		if(m_bChiceSelectedDetector.Length == 0){
			m_bChiceSelectedDetector = new bool[4];
		}
		
		for(int i = 0; i < m_bChiceSelectedDetector.Length; i++){
			if(i != _selectIndex){
				m_bChiceSelectedDetector[i] = false;
			}
			else{
				m_bChiceSelectedDetector[i] = true;
			}
		}
	}
	
	/*
	*
	* This function is used to check whether any of the option is being selected
	* If any of the option is being selected, then it needs to be reset.
	*
	*/
	public bool IsOptionsSelected()
	{
		bool isSelected = false;
		
		if(m_bMultiChoice1OptionA || m_bMultiChoice1OptionB || m_bMultiChoice1OptionC || m_bMultiChoice1OptionD){
			isSelected = true;
		}

        if (TextDisplayCanvas.instance.multiChoiceBox.IsAnyOptionSelected())
            isSelected = true;
		return isSelected;
	}
	
	/*
	*
	* This function loads all the conversation of one perticular level
	*
	*/
	public void LoadLevelConversation(int _levelNum)
	{
        //Load the public conversation for all the levels
        //Guide conversation data
        string txt = "LevelData/Level " + _levelNum.ToString() + "/guide_" + _levelNum.ToString();
        m_guideXmlFilePathTextAsset = (TextAsset)Resources.Load (txt, typeof(TextAsset));
        if (m_guideXmlFilePathTextAsset == null || m_guideXmlFilePathTextAsset.text == "" )
            Debug.LogError("Couldn't load: " + txt);

		//Mentor conversation data
		m_mentorXMLFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/mentor_" + _levelNum.ToString(), typeof(TextAsset));
		//Hope conversation data
		m_hopeXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/hope_" + _levelNum.ToString(), typeof(TextAsset));
		
		
		//Other character conversation data in different levels
		if(_levelNum == 1){
			//Load the Island character conversation data
			m_guardianXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/guardian_" + _levelNum.ToString(), typeof(TextAsset));
	
			//Load the level 1 caver character conversation data
			m_1npcXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cavenpc_" + _levelNum.ToString(), typeof(TextAsset));
			m_travelerXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/traveller_" + _levelNum.ToString(), typeof(TextAsset));
			
			Debug.Log ("Level 1 conversation data is loaded!");
		}
		
		if(_levelNum == 2){
			//Load the level 2 cave character data
			m_cassXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));
			m_fireXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fire_" + _levelNum.ToString(), typeof(TextAsset));	
			m_yetiXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/yeti_" + _levelNum.ToString(), typeof(TextAsset));
			m_fireperson2XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fire2_" + _levelNum.ToString(), typeof(TextAsset));
				
			Debug.Log ("Level 2 conversation data is loaded!");
		}
		
		if(_levelNum == 3){
			//Load the level 3 cave character data
			m_firespiritXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/firespirit_" + _levelNum.ToString(), typeof(TextAsset));
			m_npcXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc_" + _levelNum.ToString(), typeof(TextAsset));	
			m_npc2XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc2_" + _levelNum.ToString(), typeof(TextAsset));
            m_cass2XmlFilePathTextAsset = (TextAsset)Resources.Load("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));

            Debug.Log ("Level 3 conversation data is loaded!");
		}
		
		if(_levelNum == 4){
			//Load the level 4 cave character data
			m_darroXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/darro_" + _levelNum.ToString(), typeof(TextAsset));
			//m_gnatsXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/gnats_" + _levelNum.ToString(), typeof(TextAsset));
			m_sparksXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/sparks_" + _levelNum.ToString(), typeof(TextAsset));
            m_cass3XmlFilePathTextAsset = (TextAsset)Resources.Load("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));

            Debug.Log ("Level 4 conversation data is loaded!");
		}
		
		if(_levelNum == 5){
			//Load the level 5 cave character data
			m_swapguyXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/swampguy_" + _levelNum.ToString(), typeof(TextAsset));
			//NOTE: Other characters' conversations are being hard coded in the mini game
				
			Debug.Log ("Level 5 conversation data is loaded!");
		}
		
		if(_levelNum == 6){
			//Load the level 6 cave character data
			m_bridgeWomanXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/WomanoftheBridgeland_" + _levelNum.ToString(), typeof(TextAsset));
			m_templeGuidianXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/templeGudian_" + _levelNum.ToString(), typeof(TextAsset));
            m_cass4XmlFilePathTextAsset = (TextAsset)Resources.Load("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));

            Debug.Log ("Level 6 conversation data is loaded!");
		}
		
		if(_levelNum == 7){
			//Load the level 7 cave character data
			m_examinerXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc_" + _levelNum.ToString(), typeof(TextAsset));
			m_swapGuyXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/guard_" + _levelNum.ToString(), typeof(TextAsset));
			m_playerNeedsHelpTextAsset = (TextAsset)Resources.Load ("LevelData/Level 7/player_7", typeof(TextAsset));
			m_cassL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));
			m_darroL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/darro_" + _levelNum.ToString(), typeof(TextAsset));
			m_firepersonL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fireman_" + _levelNum.ToString(), typeof(TextAsset));
			m_kogXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/kog_" + _levelNum.ToString(), typeof(TextAsset));
			
			Debug.Log ("Level 7 conversation data is loaded!");
		}
	}
	
	/*
	*
	* This function is used to get the audio clip base on the clip name
	* in the XML data files.
	*
	*/
	public AudioClip GetCharacterVoiceFile(string _audioNameFromXML)
	{
		AudioClip tempAudioClip = new AudioClip();
		int i;
		
		//Audio files for level 1
		if(Global.CurrentLevelNumber == 1){
			for (i = 0; i < L1AudioFileList.Length; ++i){
				if (_audioNameFromXML == L1AudioFileList[i].name)
				{
					tempAudioClip = L1AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 2
		else if(Global.CurrentLevelNumber == 2){
			for (i = 0; i < L2AudioFileList.Length; ++i){
				if (_audioNameFromXML == L2AudioFileList[i].name)
				{
					tempAudioClip = L2AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 3
		else if(Global.CurrentLevelNumber == 3){
			for (i = 0; i < L3AudioFileList.Length; ++i){
				if (_audioNameFromXML == L3AudioFileList[i].name)
				{
					tempAudioClip = L3AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 4
		else if(Global.CurrentLevelNumber == 4){
			for (i = 0; i < L4AudioFileList.Length; ++i){
				if (_audioNameFromXML == L4AudioFileList[i].name)
				{
					tempAudioClip = L4AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 5
		else if(Global.CurrentLevelNumber == 5){
			for (i = 0; i < L5AudioFileList.Length; ++i){
				if (_audioNameFromXML == L5AudioFileList[i].name)
				{
					tempAudioClip = L5AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 6
		else if(Global.CurrentLevelNumber == 6){
			for (i = 0; i < L6AudioFileList.Length; ++i){
				if (_audioNameFromXML == L6AudioFileList[i].name)
				{
					tempAudioClip = L6AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 7
		else if(Global.CurrentLevelNumber == 7){
			for (i = 0; i < L7AudioFileList.Length; ++i){
				if (_audioNameFromXML == L7AudioFileList[i].name)
				{
					tempAudioClip = L7AudioFileList[i];
					break;
				}
			}
		}
		
		return tempAudioClip;
	}
	
	/*
	*
	* This function plays the voice of the guide of the current scene
	*
	*/
	public void PlayVoice(string _theSceneName, string _soundFileName)
	{	
		//m_characterVoice = Resources.Load ("AudioSounds/Voice/" + _soundFileName) as AudioClip;
		m_characterVoiceAS.clip = GetCharacterVoiceFile(_soundFileName);

        if (GameObject.Find(Global.CurrentInteractNPC))
            m_characterVoiceAS.transform.position = GameObject.Find(Global.CurrentInteractNPC).transform.position;
        m_characterVoiceAS.spatialBlend = spatialBlend;

        //Get the voice start time of the current scene
        m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime(_theSceneName);
		
		m_characterVoiceAS.Play();
		
		if (Global.CurrentInteractNPC != "Hope" && !Global.CurrentInteractNPC.Contains("Firespirit") && 
			!Global.CurrentInteractNPC.Contains("Spark") && Global.CurrentInteractNPC != "Boy" && Global.CurrentInteractNPC != "Girl")
			//&& Global.CurrentInteractNPC != "KingOfGnats")
		{
			GameObject interact = GameObject.Find (Global.CurrentInteractNPC);
			//Play talk animation
			if (!(Global.CurrentInteractNPC == "guardian" && interact.GetComponent<Animation>().IsPlaying("point")) && 
				!(Global.CurrentInteractNPC == "Mentor" && interact.GetComponent<Animation>().IsPlaying("give")) &&
				!(Global.CurrentInteractNPC == "Traveller" && interact.GetComponent<Animation>().IsPlaying("breath")))
			{
                if(GameObject.Find(Global.CurrentInteractNPC))
				    GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("talk");
			}
			if(Global.CurrentInteractNPC == "Swampguy"){
				GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("talk");
			}
		}
	}
	
	/*
	*
	* This function reruns whether is scene is auto scene,
	* if the scene is an auto scene, then it will play automatically. 
	*
	*/
	public bool IsAutoTalkScene(string _currentSceneName)
	{
		bool isAutoScene = false;
		
		//If the scene is a auto scene
		if( _currentSceneName != "" && _currentSceneName.Substring(0, 4) == "Auto"){
			isAutoScene = true;
		}
		return isAutoScene;
	}
	
	/*
	*
	* This function plays scene information automatically
	* If the current scene continue with sutomatic scenes, then
	* the continues scene will pop up automatically.
	* 
	* _startSceneName: The scene starts the auto talk scene
	* _firstDelayTime: The time delay of the first auto conversation
	* _secondDelayTime: The time delay of the second auto conversation
	*
	*/
	public void PlayAutoTalkScenes(string _autoSceneName, float _eachConversationTime)
	{
		//Define the scenes that needs auto talk scene
		if(Time.time - m_autoSceneStartTime > _eachConversationTime){
			//Traveller
			if(Global.CurrentLevelNumber == 1){
			}
			//Change the talking character(Cass & Fireman)
			if(Global.CurrentLevelNumber == 2){
				if(Global.CurrentInteractNPC == "Cass"){
					Global.CurrentInteractNPC = "Fireperson";
					GameObject.Find("Cass").GetComponent<Animation>().Play("idle");
					GameObject.Find("Fireperson").GetComponent<Animation>().Play("talk");
					GameObject.Find("Cass").GetComponent<CharacterInteraction>().enabled = false;
				}
				else if (Global.CurrentInteractNPC == "Fireperson"){
					Global.CurrentInteractNPC = "Cass";
					GameObject.Find("Cass").GetComponent<Animation>().Play("talk");
					GameObject.Find("Fireperson").GetComponent<Animation>().Play("idle");
					if(Global.PreviousSceneName == "AutoL2FireScene5"){
						GameObject.Find("Cass").GetComponent<Animation>().Play("take");
					}
				}
				if(m_currentScene == "AutoL2FireScene5"){//Last sentense of fireman
					GameObject.Find ("L2GUI").GetComponent<TerminateCassFiremanScene>().m_firePersonConversationFinished = true;
					Global.NPCGiveAwayObject("Fireperson", "give", "Ember");
					Global.m_pickNPCName = "Cass";
					Global.m_PickAniName = "take";
					m_voiceCanStart = true;
					Debug.Log ("Fireman gives ember to Cass..........");
				}
			}
			
			//Change the scene name when time is up
			SetCurrentDialogue(m_loadSceneData.GetNextSceneName(_autoSceneName));
			m_bAutoConversationUnFinished = true;
			
			//The player walk to the gate and breath
			if(m_currentScene == "TerminateTalkScene"){
				Global.GetPlayer().GetComponent<Animation>().Play("walk");
				
				Global.GetPlayer().transform.LookAt(Global.GetObjectPositionByID("37004"));
				Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(Global.GetObjectPositionByID("37004"), Global.GetObjectPositionByID("37001"));
                TextDisplayCanvas.instance.HideStatement();
			}
		}
		else{
			m_bAutoConversationUnFinished = false;
		}
	}
	
	/*
	*
	* This function handles all the auto conversation in each level 
	*
	*/
	public void AutoConversationSceneSwitch()
	{
		//Level 1 travel auto talk------------------------------------------------------
		if(Global.CurrentLevelNumber == 1){
			//Travel auto conversation
			if(Global.CurrentInteractNPC == "Traveller"){
				if(m_currentScene == "AutoTravellerScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						//Player breath animation
						Global.GetPlayer().GetComponent<Animation>().Play("breath");
						//Global.GetPlayer().animation["breath"].normalizedSpeed = 1.1f;
						SoundPlayer.PlaySound("sfx_breath", 0.3f);
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 6.8f);
				}
				if(m_currentScene == "AutoTravellerScene4_1"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 3.2f);
				}
				if(m_currentScene == "AutoTravellerScene4_2"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 5.1f);
				}
			}
		}
		//Level 2 Cass and fireman auto talk---------------------------------------------
		else if(Global.CurrentLevelNumber == 2){
			//Cass auto conversation
			if(Global.CurrentInteractNPC == "Cass"){
				if(m_currentScene == "AutoL2CassScene2"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_bCassFiremanTalkStart = true;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 3.9f);

				}
				
				if(m_currentScene == "AutoL2CassScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 6.0f);

				}
			}
			//Firman 1 auto conversation
			if(Global.CurrentInteractNPC == "Fireperson"){
				if(m_currentScene == "AutoL2FireScene3"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 4.9f);

				}
				
				if(m_currentScene == "AutoL2FireScene5"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 5.0f);

				}
			}
		}
		//Level 7 examiner auto talk------------------------------------------------------
		else if(Global.CurrentLevelNumber == 7){
			//The Examiner auto conversation
			if(Global.CurrentInteractNPC == "Traveller"){
				if(m_currentScene == "AutoL7ExaminerScene3"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 6.0f);
				}
				
				if(m_currentScene == "AutoL7ExaminerScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 3.0f);
				}
				
				if(m_currentScene == "AutoL7ExaminerScene5"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						m_voiceCanStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 10.0f);
				}
			}
		}
	}
	
	public Color SwitchCharacterNameColor()
	{
		Color playerNameColor = new Color(0.3f, 0.08f, 0.5f, 1.0f);
		Color guideNameColor = new Color(0.07f, 0.14f, 0.4f, 1.0f);
		Color mentorNameColor = new Color(0.64f, 0.0f, 0.0f, 1.0f);
		Color otherNPCNameColor = new Color(0.82f, 0.0f, 0.54f, 1.0f);

        Color chosenColour = Color.black;
		//Player's information
		if(Global.CurrentInteractNPC == "Guide"){
			GUI.skin.customStyles[30].normal.textColor = guideNameColor;
            chosenColour = guideNameColor;

        }
		else if(Global.CurrentInteractNPC == "Mentor"){
			GUI.skin.customStyles[30].normal.textColor = mentorNameColor;
            chosenColour = mentorNameColor;

        }
		else{
			GUI.skin.customStyles[30].normal.textColor = otherNPCNameColor;
            chosenColour = otherNPCNameColor;
        }

        return chosenColour;
	}
	
}
