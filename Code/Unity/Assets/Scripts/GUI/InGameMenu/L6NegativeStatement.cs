using UnityEngine;
using System.Collections;

public class L6NegativeStatement : MonoBehaviour {
	
	public bool[] m_bNegativeStatementSelectList;
	static public string m_sChoice = "";

    private GameObject menu;
    private string[] choices = new string[]
    {
        "My life sucks...",
        "Nobody likes me...",
        "SPARX is useless...",
        "I'll never feel good again...",
        "Your own 'problem thought'."
    };

    // Update is called once per frame
    public void Update()
    {
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("PickAThing");
            menu.GetComponent<PickAThingDialog>().SetItems(choices, ItemClicked);
        }
    }
	
    public void ItemClicked( int i )
    {
        Destroy(menu);
        
        m_bNegativeStatementSelectList = new bool[choices.Length];
        m_bNegativeStatementSelectList[i] = true;
        GetComponent<L6AnalyseNegativeStatement>().enabled = true;
        m_sChoice = choices[i];

        this.enabled = false;
    }

    public string GetStatementChoice()
    {
        for( int i = 0; i < choices.Length; ++i )
        {
            if(m_bNegativeStatementSelectList[i])
            {
                return choices[i];
            }
        }
        return "";
    }

    public int GetStatementID()
    {
        for (int i = 0; i < choices.Length; ++i)
        {
            if (m_bNegativeStatementSelectList[i])
            {
                return i;
            }
        }
        return -1;
    }

	//public void OnGUI () 
	//{
	//	GUI.depth = -1;
		
	//	GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
	//	GUI.Label(new Rect(180.0f * Global.ScreenWidth_Factor,50.0f * Global.ScreenHeight_Factor,600.0f * Global.ScreenWidth_Factor,200.0f * Global.ScreenHeight_Factor), "Choose a Gnat (negative thought) or use your own negative statement.", "Titles");
		
	//	if(GUI.Toggle(new Rect(370.0f * Global.ScreenWidth_Factor,230.0f * Global.ScreenHeight_Factor,220.0f * Global.ScreenWidth_Factor,20.0f * Global.ScreenHeight_Factor), false, "My life sucks...")){
	//		this.enabled = false;
	//		GetComponent<L6AnalyseNegativeStatement>().enabled = true;
	//		m_bNegativeStatementSelectList[0] = true;
	//		m_sChoice = "My life sucks...";
	//	}
	//	if(GUI.Toggle(new Rect(370.0f * Global.ScreenWidth_Factor,270.0f * Global.ScreenHeight_Factor,220.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor), false, "Nobody likes me...")){
	//		this.enabled = false;
	//		GetComponent<L6AnalyseNegativeStatement>().enabled = true;
	//		m_bNegativeStatementSelectList[1] = true;
	//		m_sChoice = "Nobody likes me...";
	//	}
	//	if(GUI.Toggle(new Rect(370.0f * Global.ScreenWidth_Factor,310.0f * Global.ScreenHeight_Factor,220.0f * Global.ScreenWidth_Factor,20.0f * Global.ScreenHeight_Factor), false, "SPARX is useless...")){
	//		this.enabled = false;
	//		GetComponent<L6AnalyseNegativeStatement>().enabled = true;
	//		m_bNegativeStatementSelectList[2] = true;
	//		m_sChoice = "SPARX is useless...";
	//	}
	//	if(GUI.Toggle(new Rect(370.0f * Global.ScreenWidth_Factor,350.0f * Global.ScreenHeight_Factor,220.0f * Global.ScreenWidth_Factor,20.0f * Global.ScreenHeight_Factor), false, "I'll never feel good again...")){
	//		this.enabled = false;
	//		GetComponent<L6AnalyseNegativeStatement>().enabled = true;
	//		m_bNegativeStatementSelectList[3] = true;
	//		m_sChoice = "I'll never feel good again...";
	//	}
	//	if(GUI.Toggle(new Rect(370.0f * Global.ScreenWidth_Factor,390.0f * Global.ScreenHeight_Factor,220.0f * Global.ScreenWidth_Factor, 20.0f * Global.ScreenHeight_Factor), false, "Your own 'problem thought'.")){
	//		this.enabled = false;
	//		GetComponent<L6AnalyseNegativeStatement>().enabled = true;
	//		m_bNegativeStatementSelectList[4] = true;
	//		m_sChoice = "Your own 'problem thought'.";
	//	}
	//}
}
