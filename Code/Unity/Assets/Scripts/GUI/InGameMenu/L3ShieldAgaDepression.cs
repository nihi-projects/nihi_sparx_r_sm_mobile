using UnityEngine;
using System.Collections;

public class L3ShieldAgaDepression : MonoBehaviour {

	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_shieldPrf = null;
	
	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	static private float TextWidth = 130.0f * Global.ScreenWidth_Factor;
	static private float TextHeight = 30.0f * Global.ScreenHeight_Factor;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};

//    private Rect[] shieldNameRects =
//    {
////		new Rect(570.0f, 85.0f, TextWidth, TextHeight),
////		new Rect(690.0f, 290.0f, TextWidth, TextHeight),
////		new Rect(570.0f, 495.0f, TextWidth, TextHeight),
////		new Rect(265.0f, 495.0f, TextWidth, TextHeight),
////		new Rect(145.0f, 290.0f, TextWidth, TextHeight),
////		new Rect(265.0f, 85.0f, TextWidth, TextHeight),
//		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
//        new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
//        new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
//        new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
//        new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
//        new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
//    };

    private bool[] displayName = 
	{
		true,
		true,
		true,
		true,
		true,
		true
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	public Texture colourTexture = null;
	public Texture greyScaleTexture = null;

    private GameObject shieldScreen = null;
	
	// Use this for initialization
	void Start () 
	{
        //m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
        FontSizeManager.checkFontSize(m_skin);
		//colourTexture    = (Texture2D)Resources.Load ("Textures/UI/shield_new_color", typeof(Texture2D));
		//greyScaleTexture = (Texture2D)Resources.Load ("Textures/UI/shield_new_gray", typeof(Texture2D));
	}

    private void OnDisable()
    {
        Destroy(shieldScreen);
    }

    void OnEnable()
    {
        shieldScreen = TextDisplayCanvas.instance.ShowPrefab("ShieldAgainstDepression");
        shieldScreen.GetComponent<ShieldMenu>().SetNames(new bool[] { true, true, false, false, false, false });

        //The shield against depression position
        m_vShieldPosition = new Vector3(0.0f, -0.005f, 0.60f);
		//The shield object clone
		m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity);
		
		GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
		
		//if(Global.PreviousSceneName == "L3GuideScene10")
		//{
		//	displayName = new bool[]{true, true, false, false, false, false};
		//	//displayName = new bool[]{true, true, true, true, true, true};
		//}
		
	}
	
	void OnGUI()
	{
		//GUI.depth = -1;
		//GUI.skin = m_skin;
		
		// shield of depression text
		//GUI.Label(new Rect(310.0f * Global.ScreenWidth_Factor, 55.0f * Global.ScreenHeight_Factor, 350.0f * Global.ScreenWidth_Factor, 50.0f * Global.ScreenHeight_Factor), "SHIELD AGAINST DEPRESSION", "Titles");
		
		//The next button
		if( shieldScreen.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
			Global.arrowKey = "";
			this.enabled = false;
			
			//Destroy the shield clone
			GameObject.Destroy(m_shieldObjectClone);
			GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;
			
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			UpdateCurrentSceneName();
		}

        //The text on the shield
        //GUI.Label(new Rect(10.0f, -300.0f, 800.0f, 500.0f), "Get help when you need it");

        //GUIStyle style = m_skin.GetStyle("ShieldText");

        //loop through all the shild names rects

        for (int i = 0; i < displayColor.Length; ++i)
        {
            // if the name needs to be displayed the display
            //if (displayName[i])
            //{
            //    style.normal.textColor = displayColor[i];
            //    GUI.Label(shieldNameRects[i], shieldNames[i], "ShieldText");
            //}
            ChangePieceColour(i);
        }

        //style.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    }

    void ChangePieceColour(int _index)
    {
        // get the child of teh shiled that we are changing
        Transform child = m_shieldObjectClone.transform.Find(childObjectNames[_index]);

        // if it is being displayed then the colour needs active
        if (displayName[_index])
        {
            child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
        }
        // fi it is not being displayed then the coulour needs to be grey scale
        else
        {
            child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
        }
    }

    /*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
    public void UpdateCurrentSceneName()
	{
		//Before go to the island scene
		if(Global.LevelSVisited == false)
		{
			//level 3
			if(Global.CurrentLevelNumber == 3)
			{
				//Depend on the different scene, the shield is different as well
				if(Global.PreviousSceneName == "L3GuideScene10")
				{
					GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene11");
				}
			}
		}
		//After visited the island scene
		else
		{
			//level 3
			if(Global.CurrentLevelNumber == 3)
			{
				GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L3GuideScene20");
			}
		}
	}
}
