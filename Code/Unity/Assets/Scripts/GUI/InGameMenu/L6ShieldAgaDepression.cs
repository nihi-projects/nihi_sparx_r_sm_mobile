using UnityEngine;
using System.Collections;

public class L6ShieldAgaDepression : MonoBehaviour {

	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_shieldPrf = null;

	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	static private float TextWidth = 130.0f * Global.ScreenWidth_Factor;
	static private float TextHeight = 30.0f * Global.ScreenHeight_Factor;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};
	
	private Rect[] shieldNameRects = 
	{
//		new Rect(570.0f, 85.0f, TextWidth, TextHeight),
//		new Rect(690.0f, 290.0f, TextWidth, TextHeight),
//		new Rect(570.0f, 495.0f, TextWidth, TextHeight),
//		new Rect(265.0f, 495.0f, TextWidth, TextHeight),
//		new Rect(145.0f, 290.0f, TextWidth, TextHeight),
//		new Rect(265.0f, 85.0f, TextWidth, TextHeight),
		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
	};
	
	private bool[] displayName = 
	{
		true,
		true,
		true,
		true,
		true,
		true
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	public Texture colourTexture = null;
	public Texture greyScaleTexture = null;

    private GameObject menu;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//colourTexture    = (Texture2D)Resources.Load ("Textures/UI/shield_new_color", typeof(Texture2D));
		//greyScaleTexture = (Texture2D)Resources.Load ("Textures/UI/shield_new_gray", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
        // loop through all the shild names rects
        for (int i = 0; i < shieldNameRects.Length; ++i)
        {
            ChangePieceColour(i);
        }

        //The next button
        if (menu.GetComponent<NextScreen>().WasNextClicked() || Global.arrowKey == "right")
        {
            Destroy(menu);
            Global.arrowKey = "";
            this.enabled = false;

            //Destroy the shield clone
            GameObject.Destroy(m_shieldObjectClone);
            GameObject.Find("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;

            if (Global.LevelSVisited == true)
            {
                Global.levelStarted = false;
            }

            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
            UpdateCurrentSceneName();
        }
    }
	
	void OnEnable()
	{
        if (menu == null)
        {
            menu = TextDisplayCanvas.instance.ShowPrefab("ShieldAgainstDepression");
        }

        //The shield against depression position
        m_vShieldPosition = new Vector3(0.0f, 0.0f, 0.60f);
		//The shield object clone
		m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity);
		
		GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
		
		if(Global.PreviousSceneName == "L6GuideScene9")
		{
			displayName = new bool[]{false, false, true, false, false, false};

        }
		else if(Global.PreviousSceneName == "L6GuideScene11")
		{
			displayName = new bool[]{false, false, true, false, false, false};
        }
        menu.GetComponent<ShieldMenu>().SetNames(displayName);
    }
	
	void ChangePieceColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.Find(childObjectNames[_index]);
			
		// if it is being displayed then the colour needs active
		if (displayName[_index])
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
		}
		// fi it is not being displayed then the coulour needs to be grey scale
		else
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
		}
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//level 6
		if(Global.CurrentLevelNumber == 6)
		{
			//Depend on the different scene, the shield is different as well
			if (Global.PreviousSceneName == "L6GuideScene9")
			{
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene10");
			}
			else if (Global.PreviousSceneName == "L6GuideScene11")
			{
				GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6GuideScene12");
			}
		}
	}
}
