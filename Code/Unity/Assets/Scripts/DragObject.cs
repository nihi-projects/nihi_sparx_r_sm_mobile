﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragObject : MonoBehaviour
{
    public delegate void DragCallback( Transform _trans );
    public DragCallback callback = null;

    public bool dragging = false;
    public bool dragable = true;
    public bool snapped = false;

    public static bool dragOne = false;

	// Update is called once per frame
	void Update ()
    {
		if( (Input.touchCount > 0 || Input.GetMouseButton(0)) && dragable == true)
        {
            if (dragging == false && dragOne == false )
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    dragging = Cast(Input.touches[i].position);
                    if (dragging)
                        break;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    dragging = Cast(Input.mousePosition);
                }
                dragOne = dragging;
            }
        }
        else
        {
            if(dragging == true )
            {
                //callback.
                if (callback != null)
                    callback(transform);
                dragOne = false;
            }
            dragging = false;
        }

        if( dragging == true)
        {
            Vector3 pos = transform.position;
            if (Input.touchCount > 0)
            {
                pos.x = Input.touches[0].position.x;
                pos.y = Input.touches[0].position.y;
            }
            else
            {
                pos.x = Input.mousePosition.x;
                pos.y = Input.mousePosition.y;
            }

            transform.position = pos;
        }
	}

    public bool Cast(
        Vector3 _pos )
    {

        PointerEventData pointerData = new PointerEventData(EventSystem.current);

        pointerData.position = _pos;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        if (results.Count > 0 )
        {
            for (int i = 0; i < results.Count; ++i)
            {
                if( results[i].gameObject.transform == transform)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool IsDragging()
    {
        return dragging;
    }
}
