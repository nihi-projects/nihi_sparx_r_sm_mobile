using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Profiling;
using System;

public enum EMovement
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	MAX_MOVEMENT
};

public class PlayerMovement : MonoBehaviour
{
    // publics
    public float m_fSpeed;
    public float m_fRunningSpeed = 6.0f;
    public float m_fWalkingSpeed = 3.0f;
    public float m_fRotationSpeed = 1.0f;

    public bool m_bWalkOnly = false;

    private bool m_bMovement = false;

    private bool m_movingPrivate = false;
    public bool m_bMovingToPosition
    {
        set
        {
            m_movingPrivate = value;
            m_bMoveTowardsNPC = false;
        }
        get
        {
            return m_movingPrivate;
        }
    }
    //public bool m_bMovingToPosition     = false;
	public bool m_bTuiMovementAttach    = true;
	
	// privates
	private Vector3 m_vTargetPosition;
	private Vector3 m_vMoveFromPosition;
	private Vector3 m_vTargetLookAt;
	
	private string m_strInteractionName = "";
	
	private float m_fTime = 0.0f;
	private float m_fNormalisedSpeed = 1.0f;
	
	private bool m_bMoveTowardsNPC      = false;
	private bool m_bRotateTowardsTarget = false;
    private bool isJumping = false;
	
	private static GameObject bagGlowMat;
	public Material[] mats;
	public static Material[] allMats;
    public float jumpHeight;
    private Vector3 jumpForce = Vector3.zero;
    public float gravity = 15f;

    private double moveCap = 0.25;
    private double runCap = 0.65f;

    private GameObject mobileControl;

	private GameObject m_ObjectMobileController;
	private GameObject m_ObjectMobileControllerMove;
	private GameObject m_ObjectMobileControllerSprint;
	private Vector3 m_ObjectMobileController_LeftPosition = new Vector3(-370, -220, 0);
	private Vector3 m_ObjectMobileController_RightPosition =  new Vector3(290, -220, 0);
    private Vector3 slideMove = Vector3.zero;
    private bool scriptsCanStopMovement = true;
    private Vector3 moveVector = Vector3.zero;

    // Use this for initialization
    void Start () {

		mobileControl = GameObject.Find("MobileSingleStickControl");

		m_bTuiMovementAttach = true;

		allMats = new Material[mats.Length];
		for(int i = 0; i < mats.Length; i ++)
        {
			allMats[i] = mats[i];
		}
		SetMobileController ();
		if(!Application.isMobilePlatform){
			if(mobileControl != null){
				//mobileControl.SetActive(false);
				//mobileControl.SetActive(true);
				SetMobileController();
			}
		}else{
			//mobileControl.SetActive(true);
			SetMobileController ();
		}

        SetMovement(true, "PlayerMovement");
    }

    // Update is called once per frame
    void Update()
    {
        ScreenLogger.ClearFrameLog();

        //this seems like the best place to put something that is guaranteed to be on every stage
        jumpForce.y -= gravity * Time.deltaTime;

        RaycastHit info = new RaycastHit();
        Vector3 rayStart = GetComponent<CharacterController>().bounds.min;
        rayStart.x = transform.position.x;
        rayStart.z = transform.position.z;
        Debug.DrawRay(rayStart, new Vector3(0f, -10f, 0f));
        if (Physics.Raycast(rayStart, new Vector3(0f, -10f, 0f), out info))
        {
            //Debug.Log("Raycast distance: " + info.distance);
            if (info.distance < 0.4f && jumpForce.y < 0f)
            {
                isJumping = false;
            }
        }

#if( UNITY_EDITOR)
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetMovement(true, "PlayerMovement2");
            scriptsCanStopMovement = false;
        }
#endif

        gemCheck();

        bool onLadder = IsOnLadder();

        if (m_bMovingToPosition)
        {
            ScreenLogger.LogEachFrame("Moving to position");

            GetComponent<Animation>().Play("walk");
            SoundPlayer.PlayLoopSound("walk", 1.0f);

            m_fTime += Time.deltaTime / m_fNormalisedSpeed;

            transform.position = Vector3.Lerp(m_vMoveFromPosition, m_vTargetPosition, m_fTime);

            Quaternion rotation = Quaternion.LookRotation(m_vTargetLookAt - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_fTime);

            //If the player reaches the destination
            if (m_fTime >= 1.0f)
            {
                m_bMovingToPosition = false;
                if (!Global.GetPlayer().GetComponent<Animation>().IsPlaying("take"))
                {
                    GetComponent<Animation>().Play("idle");
                    Debug.Log("The player is playing idle...");
                }

                m_fTime = 1.0f;

                transform.position = Vector3.Lerp(m_vMoveFromPosition, m_vTargetPosition, m_fTime);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_fTime);

                if (m_bMoveTowardsNPC == true)
                {
                    m_bRotateTowardsTarget = true;
                    m_bMoveTowardsNPC = false;
                }
            }
        }
        else if (m_bRotateTowardsTarget)
        {
            m_fTime += Time.deltaTime * 0.5f;
            Quaternion rotation = Quaternion.LookRotation(m_vTargetLookAt - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_fTime);
            if (m_fTime >= 1.0f)
            {
                if( GameObject.Find("GUI"))
                {
                    GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false; //Reset the talk scenes.
                }
                m_bRotateTowardsTarget = false;
                m_fTime = 0.0f;
                Global.GetPlayer().GetComponent<Animation>().Stop("walk");
            }
        }

        if (!GetMovement() && !m_bRotateTowardsTarget)
        {
            SoundPlayer.StopLoop("walk");
            SoundPlayer.StopLoop("run");
            //if (Global.GetPlayer().GetComponent<Animation>().IsPlaying("walk"))
            //{
            //    GetComponent<Animation>().Play("idle");
            //}
            return;
        }
        if (m_bMoveTowardsNPC || m_bMovingToPosition)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            
        }

        if (Global.CurrentLevelNumber == 2){
			Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
			eulerAngles.z = 0;
			Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
		}
		
		
		if( m_bRotateTowardsTarget == false )
		{
			bool Running = false;
			
			//Bind the Tui and Tui fly floows the player
			AttachTuiMovement();
			
			if(!m_bWalkOnly && (Input.GetKey(KeyCode.LeftShift) || !m_bWalkOnly && Input.GetKey(KeyCode.RightShift)) )
				Running = true;
			
			// Check arrow keys down.
			EMovement movement = EMovement.MAX_MOVEMENT;
			if((Input.GetKey(KeyCode.LeftArrow)|| Input.GetKey(KeyCode.A) || CrossPlatformInputManager.GetAxis ("Horizontal") < -moveCap) && !onLadder)
			{
				movement = EMovement.LEFT;
				Move (movement, Running);
			}
			if((Input.GetKey(KeyCode.RightArrow)|| Input.GetKey(KeyCode.D) || CrossPlatformInputManager.GetAxis ("Horizontal") > moveCap) && !onLadder)
			{
				movement = EMovement.RIGHT;
				Move (movement, Running);
			}
			if(Input.GetKey(KeyCode.UpArrow)|| Input.GetKey(KeyCode.W) || CrossPlatformInputManager.GetAxis ("Vertical") > moveCap)
			{
                if (CrossPlatformInputManager.GetAxis("Vertical") > runCap && !m_bWalkOnly)
                   Running = true;

				movement = EMovement.UP;
				Move (movement, Running);
			}
			if(Input.GetKey(KeyCode.DownArrow)|| Input.GetKey(KeyCode.S) || CrossPlatformInputManager.GetAxis ("Vertical") < -moveCap)
			{
				movement = EMovement.DOWN;
				Move (movement, Running);
			}
			
            if( isJumping == true)
            {
                GetComponent<Animation>().Play("jump");
            }
			else if (movement == EMovement.MAX_MOVEMENT)
			{
				if(!GetComponent<Animation>().IsPlaying("ride") && isJumping == false)
				{
					GetComponent<Animation>().Play("idle");
					SoundPlayer.StopLoop("walk");
					SoundPlayer.StopLoop("run");
				}
			}
			else
			{
				if(GetComponent<Animation>().IsPlaying ("idle"))
				{
					GetComponent<Animation>().Stop();
				}
				
				

                if (!onLadder)
                {
                    if (Running)
                    {
                        GetComponent<Animation>().Play("run");
                        SoundPlayer.PlayLoopSound("run", 1.0f, transform.position, 0.8f );
                    }
                    else
                    {
                        ScreenLogger.LogEachFrame("Playing walk normally.");
                        GetComponent<Animation>().Play("walk");
                        SoundPlayer.PlayLoopSound("walk", 0.8f, transform.position, 0.8f );
                    }
                }
			}
		}

        if( !onLadder )
        {
            moveVector += jumpForce;
            if (slideMove != Vector3.zero)
            {
                moveVector += slideMove;
                //GetComponent<CharacterController>().SimpleMove(slideMove);
                slideMove = Vector3.zero;
            }
        }

        GetComponent<CharacterController>().Move(moveVector * Time.deltaTime);
        moveVector = Vector3.zero;
	}

    public bool IsOnLadder()
    {
        GameObject level4 = GameObject.Find("Level4");
        if (level4 != null)
        {
            LadderInteraction[] ladders = level4.GetComponentsInChildren<LadderInteraction>();
            foreach (LadderInteraction ladder in ladders)
            {
                if (ladder.m_bOnThisLadder)
                {
                    return true;
                    break;
                }
            }
        }
        return false;
    }

    public void MyMethod3()
    {
        Console.WriteLine("TestCallStack: {0}",
                          Environment.StackTrace);
    }

    private bool afterStrom = false;

    public void SetAfterTheStrom(bool val)
    {
        afterStrom = val;
    }

    public bool IsAfterTheStrom()
    {
        return afterStrom;
    }
        public void SetMovement( bool _b, string moduleName)
    {
        MyMethod3();

        Debug.Log("^^SetMovement^^: " + _b + " FROM:" + moduleName );

        if (Global.LastEntry != "^^SetMovement^^: " + _b + " FROM:" + moduleName) {
            Global.LogID++;
            Global.debugControls[9] = Global.debugControls[8];
            Global.debugControls[8] = Global.debugControls[7];
            Global.debugControls[7] = Global.debugControls[6];
            Global.debugControls[6] = Global.debugControls[5];
            Global.debugControls[5] = Global.debugControls[4];
            Global.debugControls[4] = Global.debugControls[3];
            Global.debugControls[3] = Global.debugControls[2];
            Global.debugControls[2] = Global.debugControls[1];
            Global.debugControls[1] = Global.debugControls[0];
            Global.debugControls[0] = Global.LogID.ToString() + "^^SetMovement^^: " + _b + " FROM:" + moduleName;
            Global.LastEntry = "^^SetMovement^^: " + _b + " FROM:" + moduleName;
        }
        if (scriptsCanStopMovement)
            m_bMovement = _b;
    }

    public bool GetMovement()
    {
        return m_bMovement && TextDisplayCanvas.instance.textShowing == false && m_bMovingToPosition == false;
    }

	static public void gemCheck(){
		//Debug.Log("in gemCheck "+Global.m_bGotCurrentLevelGem.ToString()+" ,player exists in global "+(Global.GetPlayer() != null));
		try{
			if(Global.m_bGotCurrentLevelGem == true && ReleaseGem.bagOff == false){
				if(Global.GetPlayer().name == "Boy"){
					if(Global.userBackPackNum == 1){
						bagGlowMat = GameObject.Find("boy_bag_1");
						if(Global.CurrentLevelNumber == 1){
							bagGlowMat.GetComponent<Renderer>().material = allMats[13];
						}else if(Global.CurrentLevelNumber == 2){
							bagGlowMat.GetComponent<Renderer>().material = allMats[9];
						}else if(Global.CurrentLevelNumber == 3){
							bagGlowMat.GetComponent<Renderer>().material = allMats[14];
						}else if(Global.CurrentLevelNumber == 4){
							bagGlowMat.GetComponent<Renderer>().material = allMats[11];
						}else if(Global.CurrentLevelNumber == 5){
							bagGlowMat.GetComponent<Renderer>().material = allMats[10];
						}else if(Global.CurrentLevelNumber == 6){
							bagGlowMat.GetComponent<Renderer>().material = allMats[15];
						}else if(Global.CurrentLevelNumber == 7){
							bagGlowMat.GetComponent<Renderer>().material = allMats[12];
						}	
					}else{
						bagGlowMat = GameObject.Find("boy_bag_2");
						if(Global.CurrentLevelNumber == 1){
							bagGlowMat.GetComponent<Renderer>().material = allMats[6];
						}else if(Global.CurrentLevelNumber == 2){
							bagGlowMat.GetComponent<Renderer>().material = allMats[2];
						}else if(Global.CurrentLevelNumber == 3){
							bagGlowMat.GetComponent<Renderer>().material = allMats[7];
						}else if(Global.CurrentLevelNumber == 4){
							bagGlowMat.GetComponent<Renderer>().material = allMats[4];
						}else if(Global.CurrentLevelNumber == 5){
							bagGlowMat.GetComponent<Renderer>().material = allMats[3];
						}else if(Global.CurrentLevelNumber == 6){
							bagGlowMat.GetComponent<Renderer>().material = allMats[8];
						}else if(Global.CurrentLevelNumber == 7){
							bagGlowMat.GetComponent<Renderer>().material = allMats[5];
						}
					}
				}else if(Global.GetPlayer().name == "Girl"){
					if(Global.userBackPackNum == 1){
						bagGlowMat = GameObject.Find("girl_bag_1");
						if(Global.CurrentLevelNumber == 1){
							bagGlowMat.GetComponent<Renderer>().material = allMats[13];
						}else if(Global.CurrentLevelNumber == 2){
							bagGlowMat.GetComponent<Renderer>().material = allMats[9];
						}else if(Global.CurrentLevelNumber == 3){
							bagGlowMat.GetComponent<Renderer>().material = allMats[14];
						}else if(Global.CurrentLevelNumber == 4){
							bagGlowMat.GetComponent<Renderer>().material = allMats[11];
						}else if(Global.CurrentLevelNumber == 5){
							bagGlowMat.GetComponent<Renderer>().material = allMats[10];
						}else if(Global.CurrentLevelNumber == 6){
							bagGlowMat.GetComponent<Renderer>().material = allMats[15];
						}else if(Global.CurrentLevelNumber == 7){
							bagGlowMat.GetComponent<Renderer>().material = allMats[12];
						}	
					}else{
						bagGlowMat = GameObject.Find("girl_bag_2");
						if(Global.CurrentLevelNumber == 1){
							bagGlowMat.GetComponent<Renderer>().material = allMats[6];
						}else if(Global.CurrentLevelNumber == 2){
							bagGlowMat.GetComponent<Renderer>().material = allMats[2];
						}else if(Global.CurrentLevelNumber == 3){
							bagGlowMat.GetComponent<Renderer>().material = allMats[7];
						}else if(Global.CurrentLevelNumber == 4){
							bagGlowMat.GetComponent<Renderer>().material = allMats[4];
						}else if(Global.CurrentLevelNumber == 5){
							bagGlowMat.GetComponent<Renderer>().material = allMats[3];
						}else if(Global.CurrentLevelNumber == 6){
							bagGlowMat.GetComponent<Renderer>().material = allMats[8];
						}else if(Global.CurrentLevelNumber == 7){
							bagGlowMat.GetComponent<Renderer>().material = allMats[5];
						}
					}
				}
			}else{
				if(Global.GetPlayer().name == "Boy"){
					if(Global.userBackPackNum == 1){
						bagGlowMat = GameObject.Find("boy_bag_1");
						bagGlowMat.GetComponent<Renderer>().material = allMats[0];
					}else{
						bagGlowMat = GameObject.Find("boy_bag_2");
						bagGlowMat.GetComponent<Renderer>().material = allMats[1];
					}
				}else{
					if(Global.userBackPackNum == 1){
						bagGlowMat = GameObject.Find("girl_bag_1");
						bagGlowMat.GetComponent<Renderer>().material = allMats[0];
					}else{
						bagGlowMat = GameObject.Find("girl_bag_2");
						bagGlowMat.GetComponent<Renderer>().material = allMats[1];
					}
				}
			}
		} catch {
			Debug.Log("GemCheck can't be used yet");
		}
	}

    //Causes the player to jump.
    public void Jump()
    {
        if(isJumping == false)
        {
            isJumping = true;
            GetComponent<Animation>().Stop();
            jumpForce.y = jumpHeight;
        }
    }

	public void Move(EMovement move, bool bRun)
	{
		if(move == EMovement.MAX_MOVEMENT)
		{
			return;
		}
		m_fSpeed = bRun ? m_fRunningSpeed : m_fWalkingSpeed;
		if(move == EMovement.LEFT)
		{
			gameObject.transform.Rotate(0.0f, -m_fRotationSpeed, 0.0f);
		}
		else if(move == EMovement.RIGHT)
		{
			gameObject.transform.Rotate(0.0f, m_fRotationSpeed, 0.0f);
		}
		else if(move == EMovement.UP)
		{
            //gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * m_fSpeed * Time.deltaTime);
			//gameObject.GetComponent<CharacterController>().SimpleMove(transform.forward * m_fSpeed);
            if(slideMove == Vector3.zero)
                moveVector += transform.forward * m_fSpeed;

        }
		else if(move == EMovement.DOWN)
		{
            //gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * m_fSpeed * Time.deltaTime);
            //gameObject.GetComponent<CharacterController>().SimpleMove(-transform.forward * m_fSpeed);
            if( slideMove == Vector3.zero )
            moveVector -= transform.forward * m_fSpeed;
        }
	}
    
    void OnControllerColliderHit(ControllerColliderHit collisionInfo)
    {
        //Debug.Log("Hit a thing.");
        Vector3 newMove = collisionInfo.normal;
        if(collisionInfo.normal.y < 0.5f )
        {
            newMove.y = 0f;
            slideMove = newMove;
        }
        else
        {
            slideMove = Vector3.zero;
        }
    }

    public void MoveToPosition(Vector3 Position, Vector3 TargetNPCPos, string NameOfNPC)
	{
		MoveToPosition(Position, TargetNPCPos);
		m_bMoveTowardsNPC = true;
		m_strInteractionName = NameOfNPC;
	}
	
	public void MoveToPosition(Vector3 Position, Vector3 LookAtPos)
	{
        SetMovement(false, "PlayerMovement3");
		m_vTargetLookAt = LookAtPos;
		m_vTargetLookAt.y = transform.position.y;
		m_vTargetPosition = Position;
		if (m_bMovingToPosition == false)
		{
			m_fTime = 0.0f;
			m_vMoveFromPosition = transform.position;
			float dist = Vector3.Distance(m_vMoveFromPosition, Position);
			m_fNormalisedSpeed = dist / m_fWalkingSpeed;
		}
		m_bMovingToPosition = true;	
	}
	
	public void AttachTuiMovement()
	{
		Vector3 tuiFlyToPosition = new Vector3(0.0f, 0.0f, 0.0f);
		
		//If Tui needs to move along with the player
		if(m_bTuiMovementAttach){
			if(Application.loadedLevelName == "Level7_Canyon" && !Global.loadedSavePoint.Contains("GameMid")){
				tuiFlyToPosition = Global.GetPlayer().transform.position;
				tuiFlyToPosition.x -= 1.2f;
				tuiFlyToPosition.y += 1.0f;
				tuiFlyToPosition.z -= 0.5f;
				GameObject hope = GameObject.Find ("Hope");
				if (hope)
				{
					hope.transform.position = tuiFlyToPosition;
					hope.transform.localRotation = gameObject.transform.rotation;
				}
			}
		}
	}


	public void SetMobileController(){
		Joystick.SetMobileController ();
	}

}
