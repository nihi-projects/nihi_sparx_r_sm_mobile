using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	
	public Transform PlayerTransform = null;
	public float UpScaler = 0.8f;
	public float ForwardScaler = 2.0f;
	public float LookAtScaler = 0.5f;
	public bool m_bFollow = false;
	public bool m_bCameraLookAt = false; // the camera looking at a target is higher piority
										 // than the camera following the player.
	public float m_fDamping = 6.0f;
	public Vector3 m_vLookAtTarget;
	public Vector3 m_vTargetPosition;
	public bool m_bArrivedAtTarget = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bCameraLookAt){
			if(!m_bArrivedAtTarget){
				Quaternion rotation = Quaternion.LookRotation(m_vLookAtTarget - transform.position);
				transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * m_fDamping);
				
				transform.position = Vector3.Lerp (transform.position, m_vTargetPosition, Time.deltaTime * 1.4f);
				if(Vector3.Distance(transform.position, m_vTargetPosition) <= 0.01f){
					m_bArrivedAtTarget = true;
					transform.position = m_vTargetPosition;
					transform.LookAt(m_vLookAtTarget);
				}
			}
		}
		else{
			if(m_bFollow && PlayerTransform != null ){
				Vector3 Position = PlayerTransform.position;
				Position -=  PlayerTransform.forward * ForwardScaler;
				Position += PlayerTransform.up * UpScaler;
				Vector3 TargetPosition = Vector3.Lerp (transform.position, Position, Time.deltaTime * 5.0f);
				
				transform.position = TargetPosition;
				Vector3 LookAtPos = PlayerTransform.position;
				LookAtPos += PlayerTransform.up * LookAtScaler;
				transform.LookAt(LookAtPos);
			}
		}
	}
	
	public void SetCameraTo(Vector3 Target, Vector3 Position)
	{
		m_bCameraLookAt = true;
		m_vTargetPosition = Position;
		m_vLookAtTarget = Target;
		m_bArrivedAtTarget = false;
	}
	
	public void SnapCameraTo(Vector3 Target, Vector3 Position)
	{
		m_bCameraLookAt = true;
		m_vTargetPosition = Position;
		m_vLookAtTarget = Target;
		transform.position = Position;
		transform.LookAt(Target);
		m_bArrivedAtTarget = false;
	}
	
	public void SnapBackToPlayer()
	{
        if( PlayerTransform != null )
        {
            Vector3 Position = PlayerTransform.position;
            Position -= PlayerTransform.forward * ForwardScaler;
            Position += PlayerTransform.up * UpScaler;
            transform.position = Position;
            Vector3 LookAtPos = PlayerTransform.position;
            LookAtPos += PlayerTransform.up * LookAtScaler;
            transform.LookAt(LookAtPos);
        }
	}
}
