using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoyCustomisation : Customisation {
	
	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_boyCharacterPrf = null;
	
	//Private variables
	private Vector3 m_vBoysCustomisePosition;
	private float m_rotationAngle = 0.0f;
	
	public void OnGUI()
	{
        GUI.depth = -1;
        GUI.skin = m_skin;

        if (creationScreen.WasClicked(ECustomisationButton.HAIR_STYLE))
        {
            ChangeCharacterHair();
        }
        if (creationScreen.WasClicked(ECustomisationButton.HAIR_COLOUR))
        {
            ChangeHairColour();
        }
        if (creationScreen.WasClicked(ECustomisationButton.SKIN_COLOUR))
        {
            ChangeSkinColour();
        }
        if (creationScreen.WasClicked(ECustomisationButton.EYE_COLOUR))
        {
            ChangeEyeColour();
        }
        if (creationScreen.WasClicked(ECustomisationButton.CLOTHING_COLOUR))
        {
            ChangeClothesColour();
        }
        if (creationScreen.WasClicked(ECustomisationButton.TRIM_COLOUR))
        {
            ChangeTrimColour();
        }
        if (creationScreen.WasClicked(ECustomisationButton.TRIM))
        {
            ChangeCharacterTrim();
        }
        if (creationScreen.WasClicked(ECustomisationButton.BACKPACK))
        {
            ChangeBackpack();
        }
        if (creationScreen.WasClicked(ECustomisationButton.CONFIRM))
        {
            ConfirmCharacter("Boy");
        }
        if (creationScreen.WasClicked(ECustomisationButton.CANCEL))
        {
            //Destory the girl object clone on the girl customization screen
            GameObject.Destroy(character);

            this.enabled = false;
            GameObject.Find("L1GUI").GetComponent<CharacterSelection>().enabled = true;
        }
        //Left Rotation
        if (creationScreen.WasClicked(ECustomisationButton.LEFT_SPIN))
        {
            m_rotationAngle -= 90.0f;
            m_rotationAngle = -90.0f;

            character.transform.Rotate(new Vector3(0.0f, m_rotationAngle, 0.0f));
            Debug.Log("Left rotation angle = " + m_rotationAngle);
        }
        //Right Rotation
        if (creationScreen.WasClicked(ECustomisationButton.RIGHT_SPIN))
        {
            m_rotationAngle += 90.0f;
            m_rotationAngle = 90.0f;

            character.transform.Rotate(new Vector3(0.0f, m_rotationAngle, 0.0f));
            Debug.Log("Right rotation angle = " + m_rotationAngle);
        }
        if (creationScreen.WasClicked(ECustomisationButton.CLOTHING))
        {
            ChangePants();
        }

    }
	
	//The boy and girl clones need to be created every time when
	//this script is called.
	public void OnEnable(){
		//Character's customisation position
		m_vBoysCustomisePosition = new Vector3(0.0f, 0.15f, 3f);
        character  = CommonEnable(m_boyCharacterPrf, m_vBoysCustomisePosition);
	}
	
	
}
