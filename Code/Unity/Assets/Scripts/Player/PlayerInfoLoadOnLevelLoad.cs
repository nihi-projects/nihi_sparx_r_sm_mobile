using UnityEngine;
using System.Collections;

public class PlayerInfoLoadOnLevelLoad : MonoBehaviour {
	private static PlayerInfoLoadOnLevelLoad instanceRef;

	//Public variables
	public bool m_bLevelDataIsLoaded = false;
		
	//Private variables
	private int m_iTotalHairStyleNumber = 11;
	private bool m_bRetrievingLevelData = false;
	private bool m_bLoadDataOnce        = false;
	private Texture2D m_tTrimTexture;
	private CapturedDataInput instance;
	
	
	//Implementation
	void Awake(){
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			if (m_bRetrievingLevelData && !m_bLoadDataOnce) {
				if (instance.m_LevelDataGetRetriever.isDone) {
					if (Application.loadedLevelName != "GuideScene" && Application.loadedLevelName != "LevelSelection") {
						LoadUserLevelInfo ();
						m_bLevelDataIsLoaded = true;
					}
				}
			}
		} else {
			if (Application.loadedLevelName != "GuideScene" && Application.loadedLevelName != "LevelSelection") {
				LoadUserLevelInfo ();
				m_bLevelDataIsLoaded = true;
			}
		}
	}
	
	/*
	 * This function is called everytime when a new scene is loaded.
	 * */
	public void OnLevelWasLoaded()
	{
		m_bLevelDataIsLoaded = false;
		m_bLoadDataOnce = false;
			
		if(Application.loadedLevelName != "GuideScene"){
			//LOAD LEVEL DATA from server
			if (Application.internetReachability != NetworkReachability.NotReachable) {
				StartCoroutine (instance.ReadUserLevelDataFromServer_NewJsonSchema (Global.CurrentLevelNumber, false));
			} else {
				StartCoroutine (instance.ReadUserLevelDataFromServer_NewJsonSchema (Global.CurrentLevelNumber, true));
			}
			m_bRetrievingLevelData = true;
			
			//LOAD GEM locally
			if(Application.loadedLevelName == "LevelS"){
				GameObject.Find ("Portal Transition Particle").GetComponent<ParticleSystem>().Play();
				LoadOnGemInfo();
			}
		}
		
		Debug.Log ("Level information is loaded from the server...");
    }
	
	public void LoadUserLevelInfo()
	{
		string playerChildObjectName = "";
		GameObject m_gBoyObject      = GameObject.Find("Boy");
		GameObject m_gGirlObject     = GameObject.Find ("Girl");
		Transform player             = null;
		
		if(Global.CurrentPlayerName == "Boy"){
			playerChildObjectName = "boy";
			m_gBoyObject.SetActive(true);
			if(m_gGirlObject){
				m_gGirlObject.SetActive(false);
			}
			
			m_tTrimTexture = GameObject.Find ("GUI").GetComponent<BoyCustomisation>().GetTexture(Global.userTrimTextureNum);
		}
		else if(Global.CurrentPlayerName == "Girl"){
			playerChildObjectName = "girl";
			m_gGirlObject.SetActive(true);
			if(m_gBoyObject){
				m_gBoyObject.SetActive(false);
			}
			
			m_tTrimTexture = GameObject.Find ("GUI").GetComponent<GirlCustomisation>().GetTexture(Global.userTrimTextureNum);
		}
		
		if (Global.CurrentPlayerName != "")
		{
			player = GameObject.Find(Global.CurrentPlayerName).transform;
			m_bLoadDataOnce = true;
		}
		
		if (player)
		{
            Transform hairObj = null;
            for (int i = 1; i <= m_iTotalHairStyleNumber; i++)
            {
                hairObj = player.Find(playerChildObjectName + "_hair_" + i.ToString());
                if(hairObj != null)
                {
                    if (Global.userHairStyleNum == i)
                    {
                        hairObj.GetComponent<SkinnedMeshRenderer>().enabled = true;
                    }
                    else
                    {
                        hairObj.GetComponent<SkinnedMeshRenderer>().enabled = false;
                    }
                }
			}

            //CUSTOMISE THE PLAYER'S BODY
            //---------------------------------------------------------------------------------------------------------------------------
            BodyPartHelper bpf = player.GetComponent<BodyPartHelper>();
            bpf.SetCharacterUsingBodyData();

   //         //Hair color
   //         bpf.SetHairColour(Global.userHairColor);
   //         //Skin color
   //         Renderer bodyRenderer = bpf.body.GetComponent<Renderer>();
   //         bpf.clothMaterial.SetColor("_Color", Global.userClothColor);
			//player.Find(playerChildObjectName + "_head").GetComponent<Renderer>().materials[0].SetColor("_Color", Global.userSkinColor);
			////Eye color
			//bpf.eyeMaterial.SetColor("_Color", Global.userEyeColor);
			////Cloth color
			//bpf.bodyMaterial.SetColor("_Color", Global.userSkinColor);
			////Trim Stuff.
   //         for( int i = 0; i < bodyRenderer.materials.Length; ++i )
   //         {
   //             if( bodyRenderer.materials[i].name.Contains("trim"))
   //             {
   //                 bodyRenderer.materials[i].SetTexture("_MainTex", m_tTrimTexture);
   //                 bodyRenderer.materials[i].SetColor("_Color", Global.userTrimColor);
   //             }
   //         }
			
			
			////Backpack number
			//if(Global.userBackPackNum == 1){
			//	player.Find(playerChildObjectName + "_bag_1").GetComponent<SkinnedMeshRenderer>().enabled = true;
			//	player.Find(playerChildObjectName + "_bag_2").GetComponent<SkinnedMeshRenderer>().enabled = false;
			//}
			//else {
			//	player.Find(playerChildObjectName + "_bag_1").GetComponent<SkinnedMeshRenderer>().enabled = false;
			//	player.Find(playerChildObjectName + "_bag_2").GetComponent<SkinnedMeshRenderer>().enabled = true;
			//}
			Debug.Log ("Customization data is loaded from server...");
            //---------------------------------------------------------------------------------------------------------------------------
        }
    }
	
	public void LoadOnGemInfo()
	{
		int currentLevelNum = Global.CurrentLevelNumber;
		//Enable the finished level Gems
		for(int i = 1; i < currentLevelNum; i++){
			GameObject.Find ("GemActive" + i + " Particle").GetComponent<ParticleSystem>().enableEmission = true;
			GameObject.Find ("_[id]3004" + i + "_lightening" + i).GetComponent<MeshRenderer>().enabled = true;
			//Raise up the gem
			GameObject.Find("Gem" + i).GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("Gem" + i).GetComponent<ObjectMovement>().m_bStartMoving = true;
			
			GameObject gemLights = GameObject.Find("Gem" + i + " Lights");
            gemLights.GetComponent<LightHolder>().EnableLights();
            //gemLights.transform.FindChild("point").GetComponent<LightFade>().enabled = true;
            //gemLights.transform.FindChild("spot").GetComponent<LightFade>().enabled = true;
        }
	}
}
