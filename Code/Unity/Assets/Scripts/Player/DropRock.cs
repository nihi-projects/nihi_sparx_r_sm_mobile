using UnityEngine;
using System.Collections;

public class DropRock : MonoBehaviour {
	
	public float m_fDropRate = 10.0f;
	public float m_fSpeed = 3.0f;

	public Texture2D MouseOverTexture;
	public Texture2D MouseOriTexture;
	
	public bool m_bIsDropping = false;
	public bool m_bPushOff=  false;
	public bool m_bFinished = false;
	
	Vector3 position;
	// Use this for initialization
	void Start () {
		position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bIsDropping && m_fDropRate > 0.0f)
		{
			Vector3 ModifiedPos = this.transform.position;
			ModifiedPos.z -= 1.0f;
			Vector3 temp =  Camera.main.WorldToScreenPoint(ModifiedPos);
			Vector2 screen = temp;
			
			
			Vector2 gui = GUIUtility.ScreenToGUIPoint(screen);
			transform.Find("Text").GetComponent<GUIText>().enabled = true;
			transform.Find("Text").transform.position = gui;
			m_fDropRate -= Time.deltaTime * m_fSpeed;
			Vector3 Pos = transform.position;
			Pos.y -= Time.deltaTime * m_fSpeed;
			transform.position = Pos;
		}
		if(m_bPushOff)
		{
			if(!Global.GetPlayer ().GetComponent<Animation>().IsPlaying("take"))
			{
				this.GetComponent<Animation>().Play ("Move");
				StartCoroutine(WaitForAnimToFinish());
				m_bPushOff = false;
				m_bFinished = true;
			}
		}
	}
	public IEnumerator WaitForAnimToFinish()
	{
		do
		{
			yield return null;
		} while ( GetComponent<Animation>().IsPlaying("Move") );
		gameObject.SetActive(false);
		//Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true);
	}
	
	public void PushOff()
	{
		if(!m_bPushOff)
		{
			m_bPushOff = true;
			Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(false, "DropRock");
			Global.GetPlayer().GetComponent<Animation>().Play("take");
		}
	}
	
	public void Reset()
	{
		gameObject.SetActive(true);
		m_bPushOff = false;
		transform.position = position;
		m_bIsDropping = false;
		m_fDropRate = 10.0f;
		GetComponent<SphereCollider>().enabled = true;
	}
	
	void OnMouseOver()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOverTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
	
	void OnMouseExit ()
	{
		//Change the cursor image of the mouse
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().cursorTexture = MouseOriTexture;
		GameObject.Find ("GUI").GetComponent<MouseCursorChange>().SetCustomCursor();
	}
}
