using UnityEngine;
using System.Collections;

public class PlayerFreeze : MonoBehaviour {
	
	float fFreezeGauge = 0.0f;
	public GameObject Overlay;
	public string ResourceNameBase;
	public string CollideObjectName;
	public Transform ResetPos;
	public GameObject FinishPos;
	public float numberIncrease = 2.0f;
	
	public Texture2D m_resourceNameBase4Texture;
	public Texture2D m_resourceNameBase3Texture;
	public Texture2D m_resourceNameBase2Texture;
	public Texture2D m_resourceNameBase1Texture;
	
	private float time = 0.0f;
	
	// Use this for initialization
	void Start () {
		Overlay = GameObject.Find ("Overlay");
	}
	
	void OnEnable()
	{
		fFreezeGauge = 0.0f;
		Overlay.GetComponent<GUITexture>().texture = m_resourceNameBase1Texture;
		Overlay.GetComponent<GUITexture>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(fFreezeGauge >= 8.0f)
		{
			time += Time.deltaTime;
			
			if (time >= 1.0f)
			{
				// Return to original position;
				// reset gauge
				transform.position = ResetPos.position;
				transform.rotation = ResetPos.rotation;
				
				fFreezeGauge = 0.0f;
				
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "PlayerFreeze");
				time = 0.0f;
			}
		}
		if(fFreezeGauge >= 8.0f)
		{
			WaitForHit();
			//Overlay.guiTexture.texture = (Texture2D)Resources.Load("UI/" + ResourceNameBase+"4");
			Overlay.GetComponent<GUITexture>().texture = m_resourceNameBase4Texture;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "PlayerFreeze2");
		}
		else if(fFreezeGauge >= 6.0f)
		{
			WaitForHit();
			//Overlay.guiTexture.texture = (Texture2D)Resources.Load("UI/" + ResourceNameBase+"3");
			Overlay.GetComponent<GUITexture>().texture = m_resourceNameBase3Texture;
		}
		else if(fFreezeGauge >= 4.0f)
		{
			WaitForHit();
			//Overlay.guiTexture.texture = (Texture2D)Resources.Load("UI/" + ResourceNameBase+"2");
			Overlay.GetComponent<GUITexture>().texture = m_resourceNameBase2Texture;
		}
		else if(fFreezeGauge >= 2.0f)
		{
			WaitForHit();
			//Overlay.guiTexture.texture = (Texture2D)Resources.Load("UI/" + ResourceNameBase+"1");
			Overlay.GetComponent<GUITexture>().texture = m_resourceNameBase1Texture;
			Overlay.GetComponent<GUITexture>().enabled = true;
		}
		else
		{
			Overlay.GetComponent<GUITexture>().enabled = false;
		}
		if(FinishPos.GetComponent<Collider>().bounds.Contains(transform.position))
		{
			Overlay.GetComponent<GUITexture>().enabled = false;
			this.enabled = false;
		}
	}
	
	IEnumerator WaitForHit()
	{
		GetComponent<Animation>().Play ("aim hit");
		yield return new WaitForSeconds(GetComponent<Animation>().clip.length);
	}
	
	
	void OnTriggerEnter(Collider other)
	{
		if(other.name.Contains(CollideObjectName))		
		{
			fFreezeGauge += numberIncrease;
		}
	}
}
