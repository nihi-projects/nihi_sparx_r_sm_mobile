using UnityEngine;
using System.Collections;

public class PlayerEffect : MonoBehaviour {
	GameObject FireBall = null;
	GameObject FireEffect = null;
	GameObject AttackThisObject = null;
	GameObject tempEffect = null;
	public GameObject ExplosionEffect = null;
	bool m_bAttack = false;
	
	float fTime = 0.0f;
	
	// Use this for initialization
	void Start () 
	{
		if(Application.loadedLevelName != "GuideScene" && Application.loadedLevelName != "LevelS")
		{
			FireBall = GameObject.Find ("Fireball");
			FireBall.GetComponent<Renderer>().enabled = false;
			FireEffect = GameObject.Find("FireEffect");
		}
	}
	public GameObject GetFireBall() 
	{
		return(FireBall);
	}
	
	public void Reset()
	{
		AttackThisObject = null;
		m_bAttack = false;
		FireBall.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!FireBall && tempEffect != null && tempEffect.GetComponent<ParticleSystem>().time > 0.0f &&
			tempEffect.GetComponent<ParticleSystem>().particleCount == 0)
		{
			Destroy(tempEffect);
		}
		
		if(m_bAttack)
		{
			fTime += Time.deltaTime;
			FireBall.transform.position = Vector3.Lerp (FireBall.transform.position,
														AttackThisObject.transform.position,
														fTime);
		}
		else
		{	
			Vector3 Pos = transform.position;
			if(Application.loadedLevelName != "GuideScene" && Application.loadedLevelName != "LevelS"){
				FireBall.transform.position = Pos;
				FireEffect.transform.position = Pos;
			}
		}
	}
	
	public void SetFireBallActive(bool bActive)
	{
		FireBall.GetComponent<Renderer>().enabled = bActive;
		if (false == bActive)
		{
			
			SoundPlayer.PlaySound("sfx-shoot-hit", 1.0f);
		}
	}
	
	public void SetFireBallMoveTo(GameObject Attack)
	{
		AttackThisObject = Attack;
		if(AttackThisObject != null)
		{
			m_bAttack = true;
			FireBall.GetComponent<Renderer>().enabled = true;
			fTime = 0.0f;
		}
		else
		{
			m_bAttack = false;	
		}
	}
	
	public bool FireBallHitObject()
	{
		if(!AttackThisObject)
		{
			return false;
		}
		// Check the distance between AttackThisObject
		// and Fireball(transform.position)
		if(fTime >= 1.0f || Vector3.Distance(FireBall.transform.position, AttackThisObject.transform.position) < 0.5f)
		{
			m_bAttack = false;
			fTime = 0.0f;
			tempEffect = (GameObject)Instantiate(ExplosionEffect, AttackThisObject.transform.position, Quaternion.identity);
			FireBall.transform.position = transform.position;
			FireBall.GetComponent<Renderer>().enabled = false;
			SoundPlayer.PlaySound("sfx-shoot-hit", 1.0f);
			return(true);
		}
		return(false);
	}
	
	public GameObject GetTarget()
	{
		return (AttackThisObject);
	}
	
	public bool HasTarget()
	{
		return (m_bAttack);
	}
}
