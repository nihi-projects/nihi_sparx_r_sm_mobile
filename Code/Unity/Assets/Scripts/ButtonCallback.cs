﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Better button callback class that can take a delegate and pass back a parameter.
/// </summary>
public class ButtonCallback : MonoBehaviour
{
    public delegate void ButtonCallbackDelegate(string _buttonName);
    public ButtonCallbackDelegate callback;
    public string param;

	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(DefaultClick);
    }

    void DefaultClick()
    {
        if (callback != null)
            callback(param);
        else
            Debug.LogError("Please set the callback for this button");
    }
}
