using UnityEngine;
using System.Collections;

public class TuiTerminateScene : MonoBehaviour {
	
	public float tuiAndPlayerDistance = 5.0f;
	public float tuiToPlayerDistance = 10.0f;
	public bool m_bTuiIsFlying = false;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	private bool m_bTuiHasToWaitPointOne = false;
	private bool m_bTuiFirstStandPointArrived = false;
	private bool m_bTuiHasToWaitPointTwo = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.CurrentLevelNumber == 1){
			//First wait point
			if(m_bTuiHasToWaitPointOne){
				if (!GameObject.Find("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && TuiNeedsFlyAway()){
					GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyTo("31025");
					m_bTuiHasToWaitPointOne = false;
					m_bTuiFirstStandPointArrived = true;
				}
			}
			
			if(m_bTuiFirstStandPointArrived && GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget == false){
				GameObject.Find ("Hope").GetComponent<Animation>().Play ("idle");
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene2";
				m_bTuiFirstStandPointArrived = false;
				this.enabled = false;
			}
			
			//Second wait point
			if(m_bTuiHasToWaitPointTwo && !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
				if(TuiNeedsFlyAway()){
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31014");
					m_bTuiHasToWaitPointTwo = false;
					this.enabled = false;
				}
			}
		}
	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		//Get rid of the conversation box and let the player continue moving.
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "TuiTerminateScene");
		
		//See whether the Tui needs to fly away and wait for the player
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		//Update the Tui scene for the next conversation
		UpdateTuiNextSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		FakeCharacterInteractionOff();
		
		Debug.Log ("Terminated Tui scene is enabled!");
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * 
    */
	public void UpdateTuiNextSceneName(string _CurrentSceneName)
	{
        TextDisplayCanvas.instance.HideDialogue();
		if(Global.CurrentLevelNumber == 1){
			switch(_CurrentSceneName){
				case "HopeScene1":
					if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "";
					m_bTuiHasToWaitPointOne = true;
					break;
				case "":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene2";
					break;
				case "HopeScene2":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene3";
					Global.tuiScene = "HopeScene3";
					m_bTuiHasToWaitPointTwo = true;
					break;
				case "HopeScene3":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
                    GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene4";
                    GameObject.Find("FightGnatsPool3").GetComponent<GnatsInteraction>().distanceInteraction = 3;
                    break;
				case "HopeScene4":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene";
				
					GameObject.Find ("Gem1").GetComponent<GetGem>().enabled = true;
					Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "TuiTerminateScene2");
					Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;
					GameObject.Find("FakeHopeCharStp2").GetComponent<CharacterInteraction>().enabled = false;
					Global.MoveCameraBasedOnID("21102");
					break;
				case "HopeScene5":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
			}
		}
	}
	
	/*
	 * This function will detect whether the tui needs to flyaway without having any conversation
	 * with the player.
	 * 
    */
	public bool TuiNeedsFlyAway(){
		bool TuiNeedToFlyAway = false;
		
		float TuiToPlayerDistance = Vector3.Distance(Global.GetPlayer().transform.position, GameObject.Find ("Hope").transform.position);
		
		if(TuiToPlayerDistance < tuiToPlayerDistance){
			TuiNeedToFlyAway = true;
		}
		
		return TuiNeedToFlyAway;
	}
	
	public void FakeCharacterInteractionOff()
	{
		switch(Global.m_sCurrentFakeNPC){
			case "FakeChestCharacter":
				GameObject fakeChestCharObj = GameObject.Find ("FakeChestCharacter");
				fakeChestCharObj.GetComponent<CharacterInteraction>().enabled = false;
				fakeChestCharObj.GetComponent<CharacterInteraction>().Interacted = true;
				fakeChestCharObj.GetComponent<CharacterInteraction>().Interacting = false;
				break;
			case "FakeHopeCharStp1":
				GameObject fakeHopeCharObj1 = GameObject.Find ("FakeHopeCharStp1");
				fakeHopeCharObj1.GetComponent<CharacterInteraction>().enabled = false;
				fakeHopeCharObj1.GetComponent<CharacterInteraction>().Interacted = true;
				fakeHopeCharObj1.GetComponent<CharacterInteraction>().Interacting = false;
				break;
			case "FakeHopeCharStp2":
				GameObject fakeHopeCharObj2 = GameObject.Find ("FakeHopeCharStp2");
				fakeHopeCharObj2.GetComponent<CharacterInteraction>().enabled = false;
				fakeHopeCharObj2.GetComponent<CharacterInteraction>().Interacted = true;
				fakeHopeCharObj2.GetComponent<CharacterInteraction>().Interacting = false;
				break;		
		}
	}
}
