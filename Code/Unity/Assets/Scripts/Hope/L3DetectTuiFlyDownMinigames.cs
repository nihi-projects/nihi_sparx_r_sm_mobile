using UnityEngine;
using System.Collections;

public class L3DetectTuiFlyDownMinigames : MonoBehaviour {
	
	private float m_vercanosmallPlayerDis = 0.0f;
	private float m_tuiNeedtoFlydownDis = 37.0f;
	
	private bool m_bTuiConversationTriggered = false;
	private bool m_bFirstConversationFinished = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.CurrentInteractNPC == "FakeTuiBeforeMiniGames" && !m_bTuiConversationTriggered){	
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("33047");
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L3DetectTuiFlyDownMinigames");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bTuiConversationTriggered = true;
		}
		
		//When the player is waiting for the Tui to flying down
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiConversationTriggered
			&& !m_bFirstConversationFinished){
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene6";
			Global.CurrentInteractNPC = "Hope";
			m_bFirstConversationFinished = true;
		}
	}
}
