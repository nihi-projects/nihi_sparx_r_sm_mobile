using UnityEngine;
using System.Collections;

public class L3DetectTuiFlyDownnpclava2 : MonoBehaviour {
	
	private bool m_bTuiConversationTriggered = false;
	private bool m_bFirstConversationFinished = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.CurrentInteractNPC == "npclava2" && !m_bTuiConversationTriggered
			&& Global.PreviousSceneName == "L3Npc2Scene1"){	
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("33050");
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L3DetectTuiFlyDownnpclava2");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bTuiConversationTriggered = true;
		}
		
		//When the player is waiting for the Tui to flying down
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiConversationTriggered
			&& !m_bFirstConversationFinished){
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene4";
			m_bFirstConversationFinished = true;
		}
	}
}
