using UnityEngine;
using System.Collections;

public class L6DetectTuiFlyDown : MonoBehaviour {

	private float m_fDoorPlayerDis = 0.0f;
	public float m_tuiNeedtoFlydownDis = 1.5f;
	
	private bool m_bTuiConversationTriggered = false;
	private bool m_bFirstConversationFinished = false;
	
	private string m_sTuiFlyToPoint = "";
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Global.GetPlayer())
        {
            m_fDoorPlayerDis = Vector3.Distance(Global.GetPlayer().transform.position, transform.position);

            //Tui needs fly down
            if (m_fDoorPlayerDis < m_tuiNeedtoFlydownDis && m_bTuiConversationTriggered == false)
            {
                SwitchTuiTargetPoint();

                GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyTo(m_sTuiFlyToPoint);
                GameObject.Find("Hope").transform.LookAt(Global.GetPlayer().transform.position);
                Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L6DetectTuiFlyDown");
                Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play("idle");
                m_bTuiConversationTriggered = true;
            }

            //Tui start talking
            if (GameObject.Find("Hope").GetComponent<HopeTalk>().m_bMovingToTarget == false && m_bTuiConversationTriggered
                && m_bFirstConversationFinished == false)
            {
                Global.CurrentInteractNPC = "Hope";
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                m_bFirstConversationFinished = true;
            }
        }
	}
	
	void SwitchTuiTargetPoint()
	{
        //Door 1
        if (gameObject.name == "Door1Collider")
        {
            Global.CurrentInteractNPC = "Hope";
            Global.TempTalkCharacter = "Hope";
            m_sTuiFlyToPoint = "26011";
            //GameObject.Find("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L6HopeScene5");
            GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene5";
			Global.GetPlayer().GetComponent<Inventory>().m_icurrentPlayingMinigameIndex = 0;
			Global.m_L6CurrentDoorNum = 1;
			Global.GetPlayer().GetComponent<Inventory>().m_sTempSceneName = "L6HopeScene5";
            
            //m_bFirstConversationFinished = true;
            GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
        }
		//Door 2
		else if(gameObject.name == "Door2Collider"){
			m_sTuiFlyToPoint = "26012";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene6";
			Global.GetPlayer().GetComponent<Inventory>().m_icurrentPlayingMinigameIndex = 1;
			Global.m_L6CurrentDoorNum = 2;
			Global.GetPlayer().GetComponent<Inventory>().m_sTempSceneName = "L6HopeScene6";
        }
		//Door 3
		else if(gameObject.name == "Door3Collider"){
			m_sTuiFlyToPoint = "26013";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene7";
			Global.GetPlayer().GetComponent<Inventory>().m_icurrentPlayingMinigameIndex = 2;
			Global.m_L6CurrentDoorNum = 3;
			Global.GetPlayer().GetComponent<Inventory>().m_sTempSceneName = "L6HopeScene7";
        }
		//Door 4
		else if(gameObject.name == "Door4Collider"){
			m_sTuiFlyToPoint = "26014";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene8";
			Global.GetPlayer().GetComponent<Inventory>().m_icurrentPlayingMinigameIndex = 3;
			Global.m_L6CurrentDoorNum = 4;
			Global.GetPlayer().GetComponent<Inventory>().m_sTempSceneName = "L6HopeScene8";
        }
	}
}
