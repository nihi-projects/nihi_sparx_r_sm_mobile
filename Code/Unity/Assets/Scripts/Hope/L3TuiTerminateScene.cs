using UnityEngine;
using System.Collections;

public class L3TuiTerminateScene : MonoBehaviour {

	public float tuiAndPlayerDistance = 5.0f;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		//Get rid of the conversation box and let the player continue moving.
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L3TuiTerminateScene");
		
		//See whether the Tui needs to fly away
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		//Update the Tui scene for the next conversation
		UpdateTuiNextSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		FakeCharacterInteractionOff();
		
		Debug.Log ("Level 3 Terminated Tui scene is enabled!");
		this.enabled = false;
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * 
    */
	public void UpdateTuiNextSceneName(string _CurrentSceneName)
	{	
		switch(_CurrentSceneName){
			case "L3HopeScene1":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene2";
				break;
			case "L3HopeScene2":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene2";
				GameObject pos = GameObject.Find("PlayerPosReset");
				Global.GetPlayer().transform.position = pos.transform.position;
				Global.GetPlayer().transform.rotation = pos.transform.rotation;
                if(Global.GetPlayer().GetComponent<PlayerFreeze>() != null)
				    Global.GetPlayer().GetComponent<PlayerFreeze>().enabled = true;
				GameObject.Find("FakeTuiVocanoSprit").GetComponent<CharacterInteraction>().Interacting = false;
				GameObject.Find("FakeTuiVocanoSprit").GetComponent<CharacterInteraction>().Interacted = false;
				break;
			case "L3HopeScene2_1":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene3";
				Global.CurrentInteractNPC = "FirespiritA";
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				break;
			case "L3HopeScene5":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				Global.CurrentInteractNPC = "FirespiritA";
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				break;
			case "L3HopeScene3":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				break;
			case "L3HopeScene4":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("RockGame").GetComponent<PushRockMiniGame>().StartGame();	
				//GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "";
				break;
			case "L3HopeScene6"://This is the conversation before the 3 mini games
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				break;
			case "L3HopeScene7"://This is the conversation before the 3 mini games
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				break;
		}
	}

	public void FakeCharacterInteractionOff()
	{
		switch(Global.m_sCurrentFakeNPC){
		case "FakeTuiVocanoStart":
			GameObject fakeTuiVocanoStartObj = GameObject.Find ("FakeTuiVocanoStart");
			fakeTuiVocanoStartObj.GetComponent<CharacterInteraction>().enabled = false;
			fakeTuiVocanoStartObj.GetComponent<CharacterInteraction>().Interacted = true;
			fakeTuiVocanoStartObj.GetComponent<CharacterInteraction>().Interacting = false;
			break;	
		case "FakeTuiVocanoSprit":
			if (GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L3HopeScene2"){
				GameObject fakeTuiVocanoSpritObj = GameObject.Find ("FakeTuiVocanoSprit");
				fakeTuiVocanoSpritObj.GetComponent<CharacterInteraction>().enabled = false;
				fakeTuiVocanoSpritObj.GetComponent<CharacterInteraction>().Interacted = true;
				fakeTuiVocanoSpritObj.GetComponent<CharacterInteraction>().Interacting = false;
			}
			break;
		case "FakeTuiBeforeMiniGames":
			GameObject fakeTuiBeforeMiniGameObj = GameObject.Find ("FakeTuiBeforeMiniGames");
			fakeTuiBeforeMiniGameObj.GetComponent<CharacterInteraction>().enabled = false;
			fakeTuiBeforeMiniGameObj.GetComponent<CharacterInteraction>().Interacted = true;
			fakeTuiBeforeMiniGameObj.GetComponent<CharacterInteraction>().Interacting = false;
			break;
		}
	}
}
