using UnityEngine;
using System.Collections;

public class L6TuiWomanBridgelandSwitch : MonoBehaviour {

	private bool m_bTuiTalkCanStart = false;
	
	// Use this for initialization
	public void Start () 
	{
	
	}
	
	// Update is called once per frame
	public void Update () 
	{
		//Tui fly down and arrived target position
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && !m_bTuiTalkCanStart){
			GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//Switch to Tui's conversation
			SwitchTuiSceneName();
			
			m_bTuiTalkCanStart = true;
			this.enabled = false;
		}
	}
	
	public void OnEnable()
	{
		//Stop the bridge woman conversation 
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Gatekeeper_female").GetComponent<CharacterInteraction>().enabled = false;
		GameObject.Find("Gatekeeper_female").GetComponent<Animation>().Play ("idle");
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L6TuiWomanBridgelandSwitch");
		
		GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyTo("36092");
		m_bTuiTalkCanStart = false;
	}
	
	/*
	*
	* This function switches the conversation between the TUI and 
	* the woman on the bridge
	*
	*/
	public void SwitchTuiSceneName()
	{	
		//Check whether Tui needs to fly down
		switch(Global.PreviousSceneName)
        {
			case "L6WomanBridgeScene1_1":
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene1";
				break;
			case "L6WomanBridgeScene2":
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[0]){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene2";
				}
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[1]){
					
				}
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[2]){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene3";
				}
				break;
			case "L6WomanBridgeScene3":
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene4";
				break;
		}
	}
}
