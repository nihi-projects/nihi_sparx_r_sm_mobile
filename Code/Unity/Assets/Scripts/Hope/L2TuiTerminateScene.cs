using UnityEngine;
using System.Collections;

public class L2TuiTerminateScene : MonoBehaviour {

	public float tuiAndPlayerDistance = 5.0f;
	public float tuiToPlayerDistance = 2.0f;
	public bool m_bTuiIsFlying = false;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	private bool m_bTuiNeedTransmit = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		//Get rid of the conversation box and let the player continue moving.
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L2TuiTerminateScene");
		
		//See whether the Tui needs to fly away and wait for the player
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		Debug.Log ("The target position ID is === " + tuiFlyHeadToID);
		
		FakeCharacterInteractionOff();
		Debug.Log ("Level 2 Terminated Tui scene is enabled!");
		
		//Update the Tui scene for the next conversation
		UpdateTuiNextSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * 
    */
	public void UpdateTuiNextSceneName(string _CurrentSceneName)
	{	
		switch(_CurrentSceneName){
			case "L2HopeScene1":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene1";
				Global.GetGameObjectByID("32003").GetComponent<IceCaveDoor>().Reset();
				this.enabled = false;
				break;
			case "L2HopeScene2":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L2HopeScene3";
				this.enabled = false;
				break;
			case "L2HopeScene3":
				if(tuiFlyHeadToID != ""){//Fly to one point without any conversation
					GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
				}
				this.enabled = false;
				break;
		}
	}
	
	/*
	 * This function will check weather the Tui needs to fly way (Without the conversation between them),
	 * when the player walk towards it.
	 * 
    */	
	public void FakeCharacterInteractionOff()
	{
		switch(Global.m_sCurrentFakeNPC){
			case "FakeTuiIceCave":
				GameObject fakeTuiIceCaveCharObj = GameObject.Find ("FakeTuiIceCave");
				fakeTuiIceCaveCharObj.GetComponent<CharacterInteraction>().enabled = false;
				fakeTuiIceCaveCharObj.GetComponent<CharacterInteraction>().Interacted = true;
				fakeTuiIceCaveCharObj.GetComponent<CharacterInteraction>().Interacting = false;
				break;	
		}
	}
}
