using UnityEngine;
using System.Collections;

public class L3DetectTuiFlyDownFs : MonoBehaviour {
		
	private bool m_bTuiConversationTriggered = false;
	private bool m_bFirstConversationFinished = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {		
		if(Global.m_sCurrentFakeNPC == "FakeTuiVocanoSprit" && !m_bTuiConversationTriggered){	
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion("33044", "33042");
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L3DetectTuiFlyDownFs");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bTuiConversationTriggered = true;
		}
		
		//When the player is waiting for the Tui to flying down
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget
			&& m_bTuiConversationTriggered && !m_bFirstConversationFinished){
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L3HopeScene2";
			Global.CurrentInteractNPC = "Hope";
			m_bFirstConversationFinished = true;
		}
	}
}