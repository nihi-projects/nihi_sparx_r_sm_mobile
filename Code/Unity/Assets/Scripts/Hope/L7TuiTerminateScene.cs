using UnityEngine;
using System.Collections;

public class L7TuiTerminateScene : MonoBehaviour {

	public float tuiAndPlayerDistance = 5.0f;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		if (GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L7HopeScene5" && 
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L7HopeScene3")
		{
			GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
			if(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L7HopeScene2"){
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement( true, "L7TuiTerminateScene");
			}
		}
		
		//See whether the Tui needs to fly away
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		if(tuiFlyHeadToID != ""){
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
		}
		
		//Update the Tui scene for the next conversation
		UpdateTuiSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		Debug.Log ("Level 7 Terminated Tui scene is enabled!");
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * Conversation with the Temple Guardian
	 * 
    */
	public void UpdateTuiSceneName(string _CurrentSceneName)
	{	
		if(_CurrentSceneName == "L7HopeScene2"){
			//Let the player carry on climbing the second ladder
			GameObject.Find ("Level7").GetComponent<Lv7CliffCutScene>().SceneStatus = Lv7CliffScene.PlayerClimbDown2;
			
			Global.GetPlayer().transform.position = Global.GetObjectPositionByID("LadderPos3")  + new Vector3(0.0f, 1.0f, 0.0f);
			Global.GetPlayer().transform.rotation = Global.GetGameObjectByID("LadderPos3").transform.rotation;

			GameObject.Find ("Level7").GetComponent<Lv7CliffCutScene>().start = Global.GetPlayer().transform.position;
		}
		else if(_CurrentSceneName == "L7HopeScene3"){
			GameObject.Find ("WeatherChange").GetComponent<L7WeatherChange>().UnPause();
		}
		else if (_CurrentSceneName == "L7HopeScene5"){
			GameObject.Find("KingOfGnats").GetComponent<Lv7KingOfGnats>().SetStatus(KOGStatus.FireAtPlayer4);
			GameObject.Find("Hope").SetActive(false);
		}
	}
}
