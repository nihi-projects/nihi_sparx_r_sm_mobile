using UnityEngine;
using System.Collections;

public class HopeTalk : MonoBehaviour {
	
	public Vector3 m_vStartPosition;
	public Vector3 m_vTargetPosition;
	public bool m_bMovingToTarget   = false;
	
	private float m_fTuiFlyDistance = 0.0f;
	private float m_fTuiFlySpeed    = 1.0f;
	float time                      = 0.0f;
	private bool m_bPreventTuiTalkAhead  = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update (){

        if(Global.GetPlayer())
        {
            if (Input.GetKeyDown("space"))
            {
                this.GetComponent<Animation>().Play("fly");
            }
            if (m_bMovingToTarget)
            {
                //Tui look at the target position
                transform.LookAt(m_vTargetPosition);
                time += Time.deltaTime / m_fTuiFlySpeed;
                transform.position = Vector3.Lerp(m_vStartPosition, m_vTargetPosition, time);

                //Tui reaches target
                if (time >= 1.0f)
                {
                    GameObject.Find("Hope").transform.LookAt(Global.GetPlayer().transform.position);
                    m_bMovingToTarget = false;
                    time = 0.0f;
                    if (Global.loadedSavePoint.Contains("GameMid") && Global.tuiScene == "HopeScene2")
                    {
                        GameObject.Find("Hope").GetComponent<Animation>().Play("idle");
                    }
                    m_bPreventTuiTalkAhead = true;
                }
            }
        }
		
	}
	
	/*
	 * This function set the Tui fly from one point to another point base on point ID
	 * */
	public void HopeFlyMotion(string _flyTargetPositionID, string _flyStartPositionID)
	{
		GetComponent<Animation>().Play ("fly");
		
		m_vStartPosition = Global.GetObjectPositionByID(_flyStartPositionID);
		m_vTargetPosition = Global.GetObjectPositionByID(_flyTargetPositionID);
		m_fTuiFlyDistance = Vector3.Distance(m_vStartPosition, m_vTargetPosition);
		m_fTuiFlySpeed = m_fTuiFlyDistance * 0.15f;
		
		m_bMovingToTarget = true;
	}
	
	/*
	 * This function set the Tui fly from one point to another point
	 * */
	public void HopeFlyMotion(string _flyTargetPositionID, Vector3 _flyStartPosition)
	{
		GetComponent<Animation>().Play ("fly");
		
		m_vStartPosition = _flyStartPosition;
		m_vTargetPosition = Global.GetObjectPositionByID(_flyTargetPositionID);
		m_fTuiFlyDistance = Vector3.Distance(m_vStartPosition, m_vTargetPosition);
		m_fTuiFlySpeed = m_fTuiFlyDistance * 0.15f;
		
		m_bMovingToTarget = true;
	}
	
	/*
	 * This function set the Tui fly to a certain point ID
	 * */
	public void HopeFlyTo(string _flyTargetPositionID)
	{
		//Debug.Log (_flyTargetPositionID);
		GetComponent<Animation>().Play ("fly");
		
		m_vStartPosition = gameObject.transform.position;
		m_vTargetPosition = Global.GetObjectPositionByID(_flyTargetPositionID);
		m_fTuiFlyDistance = Vector3.Distance(m_vStartPosition, m_vTargetPosition);
		m_fTuiFlySpeed = m_fTuiFlyDistance * 0.15f;
		
		m_bMovingToTarget = true;
	}
	
	/*
	 * This function set the Tui fly to a certain point position
	 * */
	public void HopeFlyTo(Vector3 _flyTargetPosition)
	{
		//Debug.Log (_flyTargetPosition);
		GetComponent<Animation>().Play ("fly");
		
		m_vStartPosition = gameObject.transform.position;
		m_vTargetPosition = _flyTargetPosition;
		m_fTuiFlyDistance = Vector3.Distance(m_vStartPosition, m_vTargetPosition);
		m_fTuiFlySpeed = m_fTuiFlyDistance * 0.15f;
		
		m_bMovingToTarget = true;
	}
}
