using UnityEngine;
using System.Collections;

public class L6TuiTempleGuardianSwitch : MonoBehaviour {

	private bool m_bTuiTalkCanStart = false;
	
	// Use this for initialization
	public void Start () 
	{
	
	}
	
	// Update is called once per frame
	public void Update () 
	{
		//Tui fly down and arrived target position
		if(!GameObject.Find("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && !m_bTuiTalkCanStart){
			GameObject.Find("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Switch to Tui's conversation
			SwitchTuiSceneName();
			
			m_bTuiTalkCanStart = true;
			this.enabled = false;
		}
	}
	
	public void OnEnable()
	{
		//Stop the temple guardian
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("6_npc2").GetComponent<CharacterInteraction>().enabled = false;
		GameObject.Find("6_npc2").GetComponent<Animation>().Play ("idle");
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L6TuiTempleGuardianSwitch");
		
		GameObject.Find("Hope").GetComponent<HopeTalk>().HopeFlyTo("36095");
		m_bTuiTalkCanStart = false;
	}
	
	/*
	*
	* This function switches the conversation between the TUI and 
	* the woman on the bridge
	*
	*/
	public void SwitchTuiSceneName()
	{	
		//Check whether Tui needs to fly down
		switch(Global.PreviousSceneName){
			case "L6TempleGuardianScene1":
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene9";
				break;
			case "L6TempleGuardianScene2":
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene10";
				break;
			case "L6TempleGuardianScene3":
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[0]){
					
				}
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[1]){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene11";
				}
				if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[2]){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene12";
				}
				//if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bChiceSelectedDetector[3]){
					
				//}
				break;
			case "L6TempleGuardianScene6":
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L6HopeScene14";
				GameObject.Find("6_npc2").GetComponent<CharacterInteraction>().Interacting = false;
				GameObject.Find("6_npc2").GetComponent<CharacterInteraction>().Interacted = true;

                GameObject.Find("L6GUI").GetComponent<L6TuiTerminateScene>().enabled = false;

				break;
		}
	}
}
