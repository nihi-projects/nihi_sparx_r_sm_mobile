using UnityEngine;
using System.Collections;

public class L4TuiTerminateScene : MonoBehaviour {

	public float tuiAndPlayerDistance = 5.0f;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		//Get rid of the conversation box and let the player continue moving.
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L4TuiTerminateScene");
		
		//See whether the Tui needs to fly away
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		//Update the Tui scene for the next conversation
		UpdateTuiNextSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		FakeCharacterInteractionOff();
		
		Debug.Log ("Level 4 Terminated Tui scene is enabled!");
		this.enabled = false;
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * 
    */
	public void UpdateTuiNextSceneName(string _CurrentSceneName)
	{	
		if(Global.CurrentLevelNumber == 4){
			switch(_CurrentSceneName){
				case "L4HopeScene1":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene2":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene3":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene4":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene5":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene8":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					break;
				case "L4HopeScene9_2":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					//Start the steps mini game
					GameObject.Find ("Steps minigame").GetComponent<L4StepsMiniGame>().m_bInteracted = true;
					break;
				//Show up the "STEPS" text in the sky
				case "L4TuiTerminateScene":
					GameObject.Find ("STexture").GetComponent<MeshRenderer>().enabled = true;
					GameObject.Find ("Spark1").transform.position = Global.GetObjectPositionByID("34042");
					GameObject.Find ("Gem4").GetComponent<L4DetectTuiFlyDown>().enabled = false;
					break;
			}
		}
	}
	
	public void FakeCharacterInteractionOff()
	{
		switch(Global.m_sCurrentFakeNPC){
			case "FakeCliffTuiFlyPos1":
				GameObject fakeTuiCliffPosObj = GameObject.Find ("FakeCliffTuiFlyPos1");
				fakeTuiCliffPosObj.GetComponent<CharacterInteraction>().enabled = false;
				fakeTuiCliffPosObj.GetComponent<CharacterInteraction>().Interacted = true;
				fakeTuiCliffPosObj.GetComponent<CharacterInteraction>().Interacting = false;
				break;	
		}
	}
}
