using UnityEngine;
using System.Collections;

public class L5TuiTerminateScene : MonoBehaviour {

	public float tuiAndPlayerDistance = 5.0f;
	
	private GameObject interactObject;
	private string tuiFlyHeadToID;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.CurrentLevelNumber == 5){
			//Keep updating the Tui character interaction
			ActivateTuiIncteraction();
		}
	}
	
	/*
	 * OnEnabled function is used to terminate the talkscene
	 * Everytime when the conversation is completed.
	 * This function will renew the current interacted game object
	 * The game object with move onto the correspond position as well.
	 * 
    */
	public void OnEnable()
	{	
		//Get rid of the conversation box and let the player continue moving.
		GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = false;
		if (GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene != "L5HopeScene2")
		{
			GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L5TuiTerminateScene");
		}
		
		//See whether the Tui needs to fly away
		tuiFlyHeadToID = GameObject.Find("GUI").GetComponent<LoadTalkSceneXMLData>().GetInteractObjHeadTo(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		//Update the Tui scene for the next conversation
		UpdateTuiNextSceneName(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene);
		
		//Find the interacting game object
		interactObject = GameObject.Find (Global.CurrentInteractNPC) as GameObject;
		interactObject.GetComponent<CharacterInteraction>().Interacted = true;
		interactObject.GetComponent<CharacterInteraction>().Interacting = false;
		
		Debug.Log ("Level 5 Terminated Tui scene is enabled!");
		this.enabled = false;
	}
	
	/*
	 * This function returns the next scene name of the Tui
	 * 
    */
	public void UpdateTuiNextSceneName(string _CurrentSceneName)
	{	
		if(Global.CurrentLevelNumber == 5){
			switch(_CurrentSceneName){
				case "L5HopeScene1":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "L5HopeScene2";
				
					//Start the level 5 swamp mini game after the Tui conversation
					GameObject.Find ("BarrelGame").GetComponent<BarrelGame>().m_bGameStart = true;
					break;
				
				case "L5HopeScene2":
					if(tuiFlyHeadToID != ""){
						GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyMotion(tuiFlyHeadToID, GameObject.Find ("Hope").transform.position);
					}
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "";
				
					//Start the level 5 swamp mini game after the Tui conversation
					GameObject.Find ("MiniGame2").GetComponent<Level5MiniGame2>().GameStart = true;
					break;
			}
		}
	}
	
	/*
	 * This function will check weather the Tui needs to fly way (Without the conversation between them),
	 * when the player walk towards it.
	 * 
    */
	public void ActivateTuiIncteraction()
	{
		//The distance between the player and the Tui
		float TuiToObjectDistance = Vector3.Distance(Global.GetPlayer().transform.position, GameObject.Find ("Hope").transform.position);
		
		if(TuiToObjectDistance > tuiAndPlayerDistance){
			GameObject.Find ("Hope").GetComponent<CharacterInteraction>().enabled = true;
			GameObject.Find ("Hope").GetComponent<CharacterInteraction>().Interacting = false;
		}
	}
}
