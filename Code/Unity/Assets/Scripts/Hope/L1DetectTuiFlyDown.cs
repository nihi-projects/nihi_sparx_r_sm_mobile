using UnityEngine;
using System.Collections;

public class L1DetectTuiFlyDown : MonoBehaviour {
	
	private bool m_bTuiConversationTriggered = false;
	private bool m_bFirstConversationFinished = false;
	
	private bool m_bSecondConversationFinished = false;
	public bool m_bPool3MiniGameFinished = false;
	public bool m_bMiniGameConversationtriggered = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Player interacting with the FakeHopeCharStp2
		if(Global.m_sCurrentFakeNPC == "FakeHopeCharStp2" && !m_bTuiConversationTriggered
			&& !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition
			&& !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget)
		{	
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31015");
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L1DetectTuiFlyDown");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bTuiConversationTriggered = true;
		}
		
		//Tui start talking
		if(m_bTuiConversationTriggered && !m_bFirstConversationFinished)
		{
            if(GameObject.Find("GUI")!= null)
            {
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene3";
            }
			
			Global.CurrentInteractNPC = "Hope";
			m_bFirstConversationFinished = true;
		}
		
		//Pool3 mini game is finished, Tui fly down again
		if(m_bPool3MiniGameFinished && !m_bSecondConversationFinished){
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("31015");
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L1DetectTuiFlyDown2");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			
			m_bSecondConversationFinished = true;
		}
		
		if(m_bSecondConversationFinished && !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget
			&& !m_bMiniGameConversationtriggered){
			//Global.CurrentInteractNPC = "Hope";
            if(GameObject.Find("GUI"))
            {
                GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentTuiScene = "HopeScene4";
                GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
                GameObject.Find("L1GUI").GetComponent<TuiTerminateScene>().enabled = false;
            }
			
			this.enabled = false;
			
			m_bMiniGameConversationtriggered = true;
		}
	}
}
