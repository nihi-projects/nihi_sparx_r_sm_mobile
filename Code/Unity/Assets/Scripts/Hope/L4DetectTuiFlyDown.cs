using UnityEngine;
using System.Collections;

public class L4DetectTuiFlyDown : MonoBehaviour {
	
	//Public variables
	
	//Private variables
	//Before Cliff
	private bool m_bFisConversationTriged       = false;
	private bool m_bFisConversationFinished     = false;
	//On Cliff
	private bool m_bOnCliffConversationTriged   = false;
	
	private float m_fCorrectTriggerPlayerDis    = 0.0f;
	private float m_fInCorrectTriggerPlayerDis  = 0.0f;
	private float m_fAllowTrigerDis             = 1.0f;
	
	private string m_sTuiOnCliffFlyToPoint      = "";
	private bool m_bTuiSratFlying               = false;
	private string m_sTuiSceneName              = "";
	
	
	void Start () 
	{
	
	}
	
	void Update () 
	{
		if(Global.GetPlayer() == null){
			return;
		}
		m_fCorrectTriggerPlayerDis = Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
		
		//Before Cliff
		if(Global.m_sCurrentFakeNPC == "FakeCliffTuiFlyPos1" && !m_bFisConversationTriged){	
			
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo("34039");
			GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L4DetectTuiFlyDown");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bFisConversationTriged = true;
		}
		
		//On Cliff
		if(m_fCorrectTriggerPlayerDis < m_fAllowTrigerDis && !m_bOnCliffConversationTriged){	
			
			SwitchTuiTargetPoint();
			
			GameObject.Find ("Hope").GetComponent<HopeTalk>().HopeFlyTo(m_sTuiOnCliffFlyToPoint);
			GameObject.Find ("Hope").transform.LookAt(Global.GetPlayer().transform.position);
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L4DetectTuiFlyDown2");
			Global.GetPlayer().GetComponent<Animation>().GetComponent<Animation>().Play ("idle");
			m_bOnCliffConversationTriged = true;
		}
		
		if(!GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget && m_bTuiSratFlying){
			Global.CurrentInteractNPC = "Hope";
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentTuiScene = m_sTuiSceneName;
			m_bTuiSratFlying = false;
		}
	}
	
	void SwitchTuiTargetPoint()
	{
		//Right lidder
		if(gameObject.name == "LadderTrigger1Up" ){
			m_sTuiOnCliffFlyToPoint = "34021";
			m_sTuiSceneName = "L4HopeScene2";
			m_bTuiSratFlying = true;
		}
		else if(gameObject.name == "LadderTrigger3Up"){
			m_sTuiOnCliffFlyToPoint = "34023";
			m_sTuiSceneName = "L4HopeScene4";
			m_bTuiSratFlying = true;
		}
		else if(gameObject.name == "LadderTrigger6Up"){
			m_sTuiOnCliffFlyToPoint = "34027";
			m_sTuiSceneName = "L4HopeScene5";
			m_bTuiSratFlying = true;
		}
		else if(gameObject.name == "LadderTrigger8Up"){
			m_sTuiOnCliffFlyToPoint = "34030";
			m_sTuiSceneName = "L4HopeScene8";
			m_bTuiSratFlying = true;
		}
		
		// Wrong Lidder
		else if(gameObject.name == "LadderTrigger4Move"){
			m_sTuiOnCliffFlyToPoint = "34065";
			m_sTuiSceneName = "L4HopeScene3";
			m_bTuiSratFlying = true;
		}
		else if(gameObject.name == "LadderTrigger5Move"){
			m_sTuiOnCliffFlyToPoint = "34066";
			m_sTuiSceneName = "L4HopeScene6";
			m_bTuiSratFlying = true;
		}
		else if(gameObject.name == "LadderTrigger9Move"){
			m_sTuiOnCliffFlyToPoint = "34070";
			m_sTuiSceneName = "L4HopeScene7";
			m_bTuiSratFlying = true;
		}
	}
}
