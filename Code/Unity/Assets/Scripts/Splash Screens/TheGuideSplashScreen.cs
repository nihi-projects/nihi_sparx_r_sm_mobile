using UnityEngine;
using System.Collections;




public class TheGuideSplashScreen : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin = null;
	
	private float m_fFadeInStartTime = 0.0f;
	private float m_fFadeInTheGuideAlpha = 0.0f;
	
	private LoadTalkSceneXMLData m_loadSceneData;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		m_fFadeInStartTime = Time.time;
		
		//Load the xml content from here
		m_loadSceneData = gameObject.GetComponent<LoadTalkSceneXMLData>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* Fade In The Guide Screen
	* Display the fist splash screen image
	*/
	public void OnGUI(){
		GUI.depth = 0;
		GUI.skin = m_skin;
	
		//m_fFadeInTheGuideAlpha = Mathf.Lerp(0.0f, 1.0f, (Time.time - m_fFadeInStartTime) * 0.3f);
		//GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInTheGuideAlpha);
		
		//GUI.Label(new Rect(0,0,960,600), "", "TheGuideFadeImage");
		
		//Display the loading icon on the right corner
		
		//if(m_fFadeInTheGuideAlpha == 1.0f){
			//Destory all the back end stuff
			//Destory Boy Clone
			GameObject.Destroy(GameObject.Find ("Boy(Clone)"));
			//Destory Girl Clone
			GameObject.Destroy(GameObject.Find ("Girl(Clone)"));
			GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;
			
			//Disable the current screen and 
			this.enabled = false;
			
			//Load the guide talk scene again
			GetComponent<TalkScenes>().enabled = true;
			GetComponent<TalkScenes>().SetCurrentDialogue("GuideScene8");
			GetComponent<TalkScenes>().PlayVoice("GuideScene8", m_loadSceneData.GetSoundFileName("GuideScene8"));
		//}
	}
}
