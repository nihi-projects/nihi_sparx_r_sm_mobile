using UnityEngine;
using System.Collections;

public class IceCaveSpashScreen : MonoBehaviour {
	
	public bool m_bIsFadeIn = true;
	//private variables
	public GUISkin m_skin = null;
	
	private float m_fFadeInTheGuideAlpha = 0.0f;
	float m_fTime = 0.0f;
	
	private PlayerInfoLoadOnLevelLoad m_PlayerInfoLevelLoad;
	
	private CapturedDataInput instance;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnEnable()
	{
		m_fTime = 0.0f;
		m_PlayerInfoLevelLoad = GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad>();
	}
	
	/*
	*
	* Fade In The Guide Screen
	* Display the fist splash screen image
	*/
	public void OnGUI(){
		GUI.depth = 0;
		GUI.skin = m_skin;
	
		m_fTime += Time.deltaTime * 0.3f;
		
		if (m_bIsFadeIn)
		{
			m_fFadeInTheGuideAlpha = Mathf.Lerp(0.0f, 1.0f, m_fTime);
		}
		else
		{
			m_fFadeInTheGuideAlpha = Mathf.Lerp(1.0f, 0.0f, m_fTime);
		}
		GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInTheGuideAlpha);
		
		GUI.Label(new Rect(0,0,Screen.width, Screen.height), "", "TheIceCaveFadeImage");
		
		if(m_bIsFadeIn && m_fTime >= 1.0f){
			// Load new scene when the cave fade in finished;
			Application.LoadLevel(Global.GetNextLevelName());
            m_bIsFadeIn = false;

            m_fTime = 0.0f;
        }
		else if (m_bIsFadeIn == false && m_fTime >= 1.0f)
		{
			this.enabled = false;
		}
	}
}
