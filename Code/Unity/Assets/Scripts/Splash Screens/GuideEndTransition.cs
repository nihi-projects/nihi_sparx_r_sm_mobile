using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GuideEndTransition : MonoBehaviour {

	public GUISkin m_skin         = null;

	private float yPosition       = 0.0f;
	private float fTime 		  = 0.0f;
	private PlayerInfoLoadOnLevelLoad m_PlayerInfoLevelLoad;

    private GameObject whiteout = null;

	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Global.arrowKey = "";
        if( fTime < 1.5f )
        {
            fTime += Time.deltaTime;
            if (Global.GetPlayer() && fTime >= 1.5f)
            {
                fTime = 0f;
                this.enabled = false;
            }
        }
	}
	
	public void OnEnable()
	{
		m_PlayerInfoLevelLoad = GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad>();
        whiteout = TextDisplayCanvas.instance.ShowPrefab("Whiteout");
        whiteout.GetComponent<Whiteout>().fadeState = Whiteout.FadeState.FADE_IN;
        whiteout.GetComponent<Whiteout>().del = WhiteoutFinished;
    }

    public void WhiteoutFinished( Whiteout.FadeState _state )
    {
        whiteout.GetComponent<Whiteout>().del = null;
        whiteout.GetComponent<Whiteout>().fadeState = Whiteout.FadeState.FADE_OUT;

        GameObject portal = GameObject.Find("PortalHolder");
        portal.transform.GetChild(0).gameObject.SetActive(true);
        
        StartCoroutine(LoadLevelS());
    }

    public IEnumerator LoadLevelS()
    {
        float fTimeUntilLoad = 0.5f;
        while (fTimeUntilLoad > 0f)
        {
            fTimeUntilLoad -= Time.deltaTime;
            yield return null;
        }

        AsyncOperation op = SceneManager.LoadSceneAsync("LevelS");
        while (op.isDone == false)
        {
            yield return null;
        }

        if (whiteout != null)
        {
            Destroy(whiteout);
            whiteout = null;
        }
    }
}
