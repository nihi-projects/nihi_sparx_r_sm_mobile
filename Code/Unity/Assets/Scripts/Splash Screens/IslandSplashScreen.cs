using UnityEngine;
using System.Collections;

public class IslandSplashScreen : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin = null;
	
	private float m_fFadeInTheGuideAlpha = 0.0f;
	private float m_fTime = 0.0f;
	private float m_fMoveTime = 0.0f;
	
	private LoadTalkSceneXMLData m_loadSceneData;
	
	private CapturedDataInput instance;
	
	private bool m_bFadeIn = true;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		m_fTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* Fade In cave scene and goes back to the island scene
	* 
	*/
	void OnGUI(){
		GUI.depth = 0;
		GUI.skin = m_skin;
	
		m_fTime += Time.deltaTime * 0.3f;
		
		if (m_bFadeIn)
		{
			m_fFadeInTheGuideAlpha = Mathf.Lerp(0.0f, 1.0f, m_fTime);
		}
		else
		{
			m_fFadeInTheGuideAlpha = Mathf.Lerp(1.0f, 0.0f, m_fTime);
		}
		GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInTheGuideAlpha);
		
		GUI.Label(new Rect(0,0,Screen.width,Screen.height), "", "TheIslandFadeImage");
		
		if(m_fTime >= 1.0f)
		{
			if (m_bFadeIn)
			{
				Application.LoadLevel("LevelS");
			}
			
			m_fTime = 1.0f;
			//this.enabled = false;
		}
	}
	
	public void OnEnable()
	{
		m_fTime = 0.0f;
	}
	
	public void IsFadeIn(bool fadeIn)
	{
		m_bFadeIn = fadeIn;
	}
}
