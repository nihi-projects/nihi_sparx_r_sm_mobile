using UnityEngine;
using System.Collections;

public enum GnatStatus
{
	Attacking,
	BackToBase,
	Defeated,
	Pause,
	Defeated_BackToBase
};
public class GnatsCombat : MonoBehaviour {
	
	public bool m_bIsTriggered = false;
	public GameObject[] Gnats;
	public Transform[] CombatTargets;
	private int iCurrentAttackingUnit = 0;
	private bool[] Defeated;
	public Transform[] DefeatPos;
	private bool m_bHitPlayer = false;
	public bool bAttackFirst = false;
	public bool bSecondGnatAtacck = false;
	public bool bCombatEnded = false;
	
	// Use this for initialization
	void Start () {
		Defeated = new bool[Gnats.Length];
		for(int i = 0; i < Defeated.Length; ++i)
		{
			Defeated[i] = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bIsTriggered)
		{
			UnitAttack();
			PlayerAttack();
			Circling ();
			ProcessDefeated();
		}
	}
	
	public void SetTriggered()
	{
		m_bIsTriggered = true;
	}
	
	public bool IsTriggered()
	{
		return(m_bIsTriggered);
	}
	
	void UnitAttack()
	{
		if(Defeated[iCurrentAttackingUnit])
		{
			return;
		}
		// if the unit hasn't hit the player
		if(!m_bHitPlayer)
		{
			Gnats[iCurrentAttackingUnit].transform.position = 
				Vector3.Lerp (Gnats[iCurrentAttackingUnit].transform.position,
							  Global.GetPlayer().transform.position,
							  Time.deltaTime * 0.7f);
			if(Vector3.Distance(Gnats[iCurrentAttackingUnit].transform.position,
								Global.GetPlayer().transform.position) < 1.0f)
			{
				m_bHitPlayer = true;
				Global.GetPlayer().GetComponent<Animation>().Play("aim hit");
			}
		}
		else
		{
			Gnats[iCurrentAttackingUnit].transform.position = 
				Vector3.Lerp (Gnats[iCurrentAttackingUnit].transform.position,
							  CombatTargets[iCurrentAttackingUnit].position,
							  Time.deltaTime * 0.5f);
			
			if(Vector3.Distance(Gnats[iCurrentAttackingUnit].transform.position,
								CombatTargets[iCurrentAttackingUnit].position) < 2.0f){
				SelectNextAttackingUnit();

			}
		}
	}
	void ProcessDefeated()
	{
		bCombatEnded = true;
		for(int i = 0; i < Gnats.Length;++i)
		{
			if(Defeated[i])
			{
				Gnats[i].transform.position = Vector3.Lerp(Gnats[i].transform.position,
															DefeatPos[i].position,
															Time.deltaTime);
				if(Vector3.Distance(Gnats[i].transform.position,
									DefeatPos[i].position) > 2.0f)
				{
					bCombatEnded = false;
				}
			}
			else
			{
				bCombatEnded = false;
			}
		}
		if(bCombatEnded)
		{
			//Use for the Tui to fly down and talk with the player
			if(Global.CurrentLevelNumber == 1 && Global.CurrentInteractNPC != "1npc"){
				GameObject.Find ("1_pool3").GetComponent<L1DetectTuiFlyDown>().m_bPool3MiniGameFinished = true;
			}
			
			this.enabled = false;
			this.gameObject.GetComponent<GnatsInteraction>().enabled = false;
			this.gameObject.GetComponent<GnatsMovement>().enabled = false;
			Camera.main.GetComponent<CameraMovement>().m_bCameraLookAt = false;
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "GnatsCombat" + "_ProcessDefeated");
		}
	}
	void PlayerAttack()
	{
		// the player should not be able to attack the Gnat if
		// it is too close to the player.
		if(Vector3.Distance(Global.GetPlayer().transform.position,
							Gnats[iCurrentAttackingUnit].transform.position) > 1.0f)
		{
			// Check mouse click
			if(Input.GetKey(KeyCode.Mouse0))
			{
                
                RaycastHit[] hits;
                
                hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition));

                int i = 0;
                while (i < hits.Length)
                {
                    RaycastHit hit = hits[i];
                    Debug.Log(hit.collider.gameObject.name);
                    
                    //Debug.Log(Global.GetMousePosToObj().ToString());
				    if(hit.collider.gameObject.name == Gnats[iCurrentAttackingUnit].name)
				    {
					    // start shooting fireball
					    Global.GetPlayer().GetComponent<Animation>().Play("aim shoot");
					    SoundPlayer.PlaySound("sfx-staff-shoot", 1.0f);
                        if (Global.GetPlayerEffect())
                            Global.GetPlayerEffect().SetFireBallMoveTo(Gnats[iCurrentAttackingUnit]);
                        else
                        {
                            Defeated[iCurrentAttackingUnit] = true;
                            SelectNextAttackingUnit();
                        }
                        bAttackFirst = true;
                        if (!bSecondGnatAtacck) {
						    StartCoroutine(WaitToRemoveHelpBanner ());
					    }

				    }

                    i++;

                }

            }
			// Check fireball hit
			if(Global.GetPlayerEffect() && Global.GetPlayerEffect ().FireBallHitObject())
			{
				Defeated[iCurrentAttackingUnit] = true;
				SelectNextAttackingUnit();
			}
		}
	}
	void Circling()
	{
		for(int i =0 ;i < Gnats.Length;++i)
		{
			if(i != iCurrentAttackingUnit && !Defeated[i])
			{
				Gnats[i].transform.RotateAround(transform.position, 
												Vector3.up,
												3.0f);
			}
		}
	}
	
	// Randomly select the next attacking unit, 
	// it can not be the same as the current Attacking unit
	void SelectNextAttackingUnit()
	{
		bool AllDefeated = true;
		foreach(bool b in Defeated)
		{
			if(!b)
			{
				AllDefeated = false;
			}
		}
		if(AllDefeated)
		{
			return;
		}
		m_bHitPlayer = false;
		
		iCurrentAttackingUnit = iCurrentAttackingUnit + 1 == Gnats.Length ? 0 : iCurrentAttackingUnit + 1;
		
		if (Defeated [iCurrentAttackingUnit]) {
			SelectNextAttackingUnit ();
		}

	}

	private IEnumerator WaitToRemoveHelpBanner(){

		yield return new WaitForSeconds(5);
		bSecondGnatAtacck = true;

	}

}
