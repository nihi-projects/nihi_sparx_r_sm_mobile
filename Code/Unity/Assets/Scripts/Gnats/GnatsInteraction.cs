using UnityEngine;
using System.Collections;

public enum GnatsAction
{
	MOVING,
	CIRCLING,
	MAX_ACTION,
};

public class GnatsInteraction : MonoBehaviour {
	public int IndexOfAction = 0;
	public GnatsAction[] Actions;
	public Transform[] TriggerPoints;
	public string CameraID;
	public GUISkin GUIskin;
	public bool bActivated;
	public string Text;
    TalkScenes m_talkScene;
    public float distanceInteraction;
    // Use this for initialization
    void Start () {

        distanceInteraction = 2.5f;

        if (Actions.GetLength(0) != TriggerPoints.GetLength(0))
		{
			Debug.LogError("Actions should match the trigger points!!!");
		}
		GetComponent<GnatsCombat>().Gnats = GetComponent<GnatsMovement>().Gnats;
		GetComponent<GnatsCombat>().CombatTargets = GetComponent<GnatsMovement>().BattleFormation;

		Text = "Touch the gnats to shoot them when they attack you";
        
        m_talkScene = GameObject.Find("GUI").GetComponent<TalkScenes>();
        
    }
	
	// Update is called once per frame
	void Update () {

		if (GetComponent<GnatsCombat> ().bAttackFirst) {
			Text = "Well Done";
		}

		if (GetComponent<GnatsCombat> ().bSecondGnatAtacck) {
			bActivated = false;
		}

		if(Actions.Length == 0)
		{
			return;
		}
		switch(Actions[IndexOfAction])
		{
		case GnatsAction.MOVING:
			CheckMovement();
			break;
		case GnatsAction.CIRCLING:
			CheckAttack();
			break;
		default:break;
		}
	}
	
	void SetToNextAction(){
		if(IndexOfAction < Actions.Length -1)
		{
			IndexOfAction += 1;
		}
		if(Actions[IndexOfAction] == GnatsAction.CIRCLING)
		{
			GetComponent<GnatsCombat>().enabled =true;
		}
		if(Actions[IndexOfAction] == GnatsAction.MOVING)
		{
			GetComponent<GnatsMovement>().enabled = true;
		}
	}
	
	void CheckMovement()
	{
		if(!GetComponent<GnatsMovement>().IsTriggered())
		{
			// Check triggering it
			if(CheckTrigger())
			{
				GetComponent<GnatsMovement>().SetTriggered();
				bActivated = true;
			}
			else
			{
				// if movement is already triggered,
				// check if finished moving
				if(GetComponent<GnatsMovement>().IsMovementComplete())
				{
					// Start to prepare for the next action
					SetToNextAction();
				}
			}
		}
	}
	
	void CheckAttack()
	{
		if(CheckTrigger() && !GetComponent<GnatsCombat>().IsTriggered()
			&& !GetComponent<GnatsCombat>().bCombatEnded)
		{
			// start attack
			GetComponent<GnatsCombat>().SetTriggered();
			
			// Stops the Gnats movement
			GetComponent<GnatsMovement>().enabled = false;
			
			// stops movement.
			Global.GetPlayer ().GetComponent<PlayerMovement>().SetMovement(false, "GnatsInteraction" + "_CheckAttack");
			// set the camera.
			Global.MoveCameraBasedOnID(CameraID);
			
			// player animation.
			Global.GetPlayer ().GetComponent<Animation>().Play ("aim idle");
			// fireball
            if(Global.GetPlayerEffect())
			    Global.GetPlayerEffect().SetFireBallActive(true);

			Vector3 look = Global.GetPlayer().transform.position - GameObject.Find("Level1_Chest").transform.position;
			Global.GetPlayer().transform.rotation = Quaternion.LookRotation(new Vector3(look.x, Global.GetPlayer().transform.position.y, look.z));
		}
	}
	
	bool CheckTrigger()
	{
        if(Global.GetPlayer())
        {
            
            if (Vector3.Distance(Global.GetPlayer().transform.position, TriggerPoints[IndexOfAction].position) < distanceInteraction)
            {
                return (true);
            }
        }
		return(false);
	}

	void OnGUI()
	{
		if(!bActivated)
		{
			return;
		}
        //GUI.skin = GUIskin;
        //GUIStyle temp = GUI.skin.GetStyle("Label");
        //GUI.Label(new Rect(200f * Global.ScreenWidth_Factor,537f * Global.ScreenHeight_Factor,620f * Global.ScreenWidth_Factor,153f * Global.ScreenHeight_Factor), "", "NormalSizeDilogBox");
        //temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        //GUI.Label(new Rect(306f * Global.ScreenWidth_Factor,583f * Global.ScreenHeight_Factor,725 * Global.ScreenWidth_Factor,100 * Global.ScreenHeight_Factor), Text, "Label");
        //float y = Screen.height*0.84f;
        TextDisplayCanvas.instance.ShowTooltip(Text);
	}

}
