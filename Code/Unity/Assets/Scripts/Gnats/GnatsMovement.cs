using UnityEngine;
using System.Collections;

public class GnatsMovement : MonoBehaviour {
	private bool m_bIsTriggered;
	public bool m_bCircling;
	public Transform[] MovementTargets;
	public Transform[] BattleFormation;
	private int[] CurrentTarget;
	public GameObject[] Gnats;
	// Use this for initialization
	void Start () {
		if(null == Gnats || null == BattleFormation)
		{
			Debug.LogError("Gnats and BattleFormations needs to be assigned in the editor");	
		}
		m_bIsTriggered = false;
		CurrentTarget = new int[MovementTargets.Length];
		for(int i =0; i < CurrentTarget.Length; ++i)
		{
			CurrentTarget[i] = i;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bIsTriggered)
		{
			// movement
			for(int i = 0; i < Gnats.Length; ++i)
			{ 
				if(CurrentTarget[i] != -1)
				{
					Gnats[i].transform.position = Vector3.Lerp(Gnats[i].transform.position,
															MovementTargets[CurrentTarget[i]].position, 
															Time.deltaTime * 0.8f);

					if(Vector3.Distance(Gnats[i].transform.position,
										MovementTargets[CurrentTarget[i]].position) <= 2.0f)
					{
						CurrentTarget[i] = CurrentTarget[i] + 1;
						if(CurrentTarget[i] == MovementTargets.Length)
						{
							CurrentTarget[i] = -1;
						}
					}
				}
				else
				{
					Gnats[i].transform.position = Vector3.Lerp (Gnats[i].transform.position,
																BattleFormation[i].position,
																Time.deltaTime);
				}
				
			}
			bool bMovementFinished = true;
			// if movement is finished
			for(int i = 0; i < Gnats.Length; ++i)
			{
				if(Vector3.Distance(Gnats[i].transform.position,
									BattleFormation[i].position) >= 2.0f)
				{
					bMovementFinished = false;
				}	
			}
			if(bMovementFinished)
			{
				Global.GetPlayer().GetComponent<PlayerMovement>().m_bWalkOnly = false;
				m_bIsTriggered = false;
				m_bCircling = true;
			}
		}
		if(m_bCircling)
		{
			// Rotate all the gnats based on the FightGnats object.
			foreach(GameObject gnat in Gnats)
			{
				gnat.transform.RotateAround(transform.position,
											Vector3.up, 3.0f);
			}
		}
	}
	public bool IsTriggered()
	{
		return(m_bIsTriggered && !m_bCircling);
	}
	public void SetTriggered()
	{
		// if the gnats are circling, that means the movement is complete
		if(false == m_bCircling)
		{
			m_bIsTriggered = true;
			Global.GetPlayer().GetComponent<PlayerMovement>().m_bWalkOnly = true;
		}
	}
	public bool IsMovementComplete()
	{
		// when the movement is complete and the gnats 
		// start to circling, the movement is complete
		return(!m_bIsTriggered && m_bCircling);
	}
}
