using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Utils
{
    //This will distribute a number of things equally with a gap between them. 
    //Only works for one dimension.
    //The objects will be distributed with a center around 0,0.
    public static float DistributeEqually( int _numberOfThings, int _indexOfThisThing, float _gap )
    {
        float position = (-(_numberOfThings / 2 - 0.5f) + _indexOfThisThing) * _gap;
        return position;
    }

    //Returns 0 if given 1, and 1 if given zero.
    public static int ZeroToOneAndOneToZero( int _oneOrZero )
    {
        return -(_oneOrZero - 1);
    }

    //Turns a string into a unity colour. This is the reverse of doing "Color.ToString();"
    public static Color StringToColour(string _str)
    {
        _str = _str.Replace("RGBA(", "");
        _str = _str.Replace(")", "");
        string[] strings = _str.Split(","[0]);

        Color output = Color.white;
        for (var i = 0; i < 4; i++)
        {
            output[i] = System.Single.Parse(strings[i]);
        }
        return output;
    }
}