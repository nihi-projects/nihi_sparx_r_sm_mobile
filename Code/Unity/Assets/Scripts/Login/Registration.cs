﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Registration : MonoBehaviour {

	public Dropdown DropWho;

	public GameObject UserPanel;
	public GameObject YoungPersonPanel;
	public GameObject GenderPanel;
	public GameObject EmailPanel;
	public GameObject LivePanel;
	public GameObject ContinueButton;
	public GameObject BackButton;

	public GameObject OtherEthnicToggle;
	public Dropdown FindDropdown;

	public GameObject OtherWho;
	public GameObject OtherEthnic;
	public GameObject OtherFind;
	public GameObject OtherFindLabel;

	 
	//FIELDS

	public InputField Field_UserUserName;
	public Dropdown Field_Who;
	public InputField Field_WhoOtro;

	public InputField Field_Old;
	public GameObject Field_NZ;
	public GameObject Field_Maori;
	public GameObject Field_Samoan;
	public GameObject Field_Cook;
	public GameObject Field_Tongan;
	public GameObject Field_Niuean;
	public GameObject Field_Chinesse;
	public GameObject Field_Indian;
	public GameObject Field_Other;
	public InputField Field_OtherTEXT;

	public Dropdown Field_Gender;

	public InputField Field_eMail;
	public InputField Field_ReeMail;

	public GameObject Field_Information;

	public InputField Field_Password;
	public InputField Field_RePassword;

	public Dropdown Field_Code;
	public InputField Field_Mobile;

	public Dropdown Field_Live;

	public Dropdown Field_Find;
	public InputField Field_SpecifyFind;

	public GameObject Field_Conact;
	public GameObject Field_ExtraHelp;
	public GameObject Field_Terms;

	private CapturedDataInput instance;
	// Use this for initialization

	private bool m_bGotRegistrationTokenDone = false;

	public GameObject LoginPanel;
	public GameObject SignUpPanel;

	public Text oTextRegisterNotificationMessage;

	private bool bSubmit;

	private Vector3 EmailPanelRectTransform;
	private GameObject oNotifications;
	void Start () {
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

		LoginPanel = GameObject.Find ("IUScreen/Canvas/LoginScreen");

		OtherWho.SetActive(false);
		OtherEthnic.SetActive(false);
		OtherFind.SetActive(false);
		OtherFindLabel.SetActive(false);
		ContinueButton.SetActive (false);
		BackButton.SetActive (false);
		EmailPanelRectTransform = EmailPanel.GetComponent<RectTransform> ().localPosition;
		oNotifications = GameObject.Find ("NotificationRegister");
	}
	
	// Update is called once per frame
	void Update () {

		if (bSubmit) {

			StartCoroutine (ValidateandSend());
			bSubmit = false;

		}
	}

	public IEnumerator ValidateandSend(){

		oNotifications.GetComponent<Notifications> ().bshowNotification = false;
		m_bGotRegistrationTokenDone = false;
		//instance.m_ProcessRegistrationAPIDrupal = null;
		//
		if (FieldsValidation (false) == "OK") {
			
			if (!(instance.m_ProcessRegistrationAPIDrupal.isDone == null)) {
				yield return StartCoroutine (instance.RegisterProcess_NewJsonSchema (GetString ()));
			}

			bSubmit = false;

			while (!instance.m_ProcessRegistrationAPIDrupal.isDone) {
				yield return false;
			}

		} else {
			oNotifications.GetComponent<Notifications> ().stext = FieldsValidation (false);
			oNotifications.GetComponent<Notifications> ().bshowNotification = true;
			oNotifications.GetComponent<Notifications> ().bhideNotification = false;
			oNotifications.GetComponent<Notifications> ().fFade = 0.1f;
			//instance.m_ProcessRegistrationAPIDrupal = null;
			bSubmit = false;
			//return null;
		}
		//---COPY

		if (instance.m_ProcessRegistrationAPIDrupal != null) {
			
			if (instance.m_ProcessRegistrationAPIDrupal.isDone) {

				Debug.Log ("m_logInDataRetriever ISDONE");

				if (string.IsNullOrEmpty (instance.m_ProcessRegistrationAPIDrupal.error)) { //CONECTION AVAILABLE
					if (instance.m_ProcessRegistrationAPIDrupal.isDone && !m_bGotRegistrationTokenDone && string.IsNullOrEmpty (instance.m_ProcessRegistrationAPIDrupal.error)) {
						Debug.Log ("<Token from URL>");
						m_bGotRegistrationTokenDone = true;
						LoginPanel.SetActive (true);
						GameObject.Find ("NotificationLogin").GetComponent<Notifications> ().stext = Field_UserUserName.text + " regisration has been successful.";
						GameObject.Find ("NotificationLogin").GetComponent<Notifications> ().bshowNotification = true;
						SignUpPanel.SetActive (false);
						bSubmit = false;
					}
				} else {
					if (!oNotifications.GetComponent<Notifications> ().bshowNotification) {
						oNotifications.GetComponent<Notifications> ().stext = ErrorMessage (instance.m_ProcessRegistrationAPIDrupal.text);
						oNotifications.GetComponent<Notifications> ().bshowNotification = true;
						oNotifications.GetComponent<Notifications> ().bhideNotification = false;
						oNotifications.GetComponent<Notifications> ().fFade = 0.1f;
						//instance.m_ProcessRegistrationAPIDrupal = null;
						bSubmit = false;
					}
				}
			}	

		}
	}


	public string ErrorMessage(string cError){
		string ErrorMessageResponse = "";
		ErrorMessageResponse = FieldsValidation (false);
		if (ErrorMessageResponse == "OK") {
			if (cError.Contains ("Username already")) {
				ErrorMessageResponse = "The username [The username] is already taken. Please select a different one.";	
				ErrorMessageResponse = ErrorMessageResponse.Replace ("[The username]", Field_UserUserName.text);
			}
			else if (cError.Contains ("Email already")) {
				ErrorMessageResponse = "Email already in use";	
			} else {
				ErrorMessageResponse = cError;
			}
		
			return ErrorMessageResponse;
		} else {
			return ErrorMessageResponse;
		}
	
	}

	public void closeButton(){
		LoginPanel.SetActive (true);
		SignUpPanel.SetActive (false);
		Destroy (SignUpPanel);
	}

	public void OnDropdownWhoChange(){
		if (DropWho.captionText.text != "Young Person") {
			UserPanel.SetActive (true);
			YoungPersonPanel.SetActive (false);
			GenderPanel.SetActive (false);
			EmailPanel.SetActive (true);
			EmailPanel.GetComponent<RectTransform> ().localPosition = EmailPanelRectTransform;
			LivePanel.SetActive (true);
			LivePanel.GetComponent<RectTransform> ().localPosition = new Vector3 (GenderPanel.GetComponent<RectTransform>().localPosition.x,-1.78f,0f);

			ContinueButton.SetActive (false);
			BackButton.SetActive (false);

			if (DropWho.captionText.text == "Other") {
				OtherWho.SetActive (true);
			} else {
				OtherWho.SetActive (false);
			}

		} else {
			UserPanel.SetActive (true);
			YoungPersonPanel.SetActive (true);
			GenderPanel.SetActive (true);
			EmailPanel.SetActive (true);
			EmailPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (GenderPanel.GetComponent<RectTransform>().localPosition.x,-1.78f,0f);
			LivePanel.SetActive (true);
			LivePanel.GetComponent<RectTransform> ().localPosition = new Vector3 (EmailPanelRectTransform.x,0f + 700,0f);

			ContinueButton.SetActive (true);
			BackButton.SetActive (false);
			OtherWho.SetActive (false);
		}
	}

	public void ContinueButtonClick(){
		UserPanel.SetActive (true);
		UserPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (UserPanel.GetComponent<RectTransform> ().localPosition.x,UserPanel.GetComponent<RectTransform> ().localPosition.y + 1700,0f);
		YoungPersonPanel.SetActive (true);
		YoungPersonPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (YoungPersonPanel.GetComponent<RectTransform> ().localPosition.x,YoungPersonPanel.GetComponent<RectTransform> ().localPosition.y + 1700,0f);
		GenderPanel.SetActive (true);
		GenderPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (GenderPanel.GetComponent<RectTransform> ().localPosition.x,GenderPanel.GetComponent<RectTransform> ().localPosition.y + 1700,0f);
		EmailPanel.SetActive (true);
		EmailPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (EmailPanel.GetComponent<RectTransform> ().localPosition.x,EmailPanel.GetComponent<RectTransform> ().localPosition.y + 1700,0f);
		LivePanel.SetActive (true);
		LivePanel.GetComponent<RectTransform> ().localPosition = new Vector3 (LivePanel.GetComponent<RectTransform> ().localPosition.x,LivePanel.GetComponent<RectTransform> ().localPosition.y - 700,0f);

		ContinueButton.SetActive (false);
		BackButton.SetActive (true);

	}

	public void BackButtonClick(){

		UserPanel.SetActive (true);
		UserPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (UserPanel.GetComponent<RectTransform> ().localPosition.x,UserPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
		YoungPersonPanel.SetActive (true);
		YoungPersonPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (YoungPersonPanel.GetComponent<RectTransform> ().localPosition.x,YoungPersonPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
		GenderPanel.SetActive (true);
		GenderPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (GenderPanel.GetComponent<RectTransform> ().localPosition.x,GenderPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
		EmailPanel.SetActive (true);
		EmailPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (EmailPanel.GetComponent<RectTransform> ().localPosition.x,EmailPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
		LivePanel.SetActive (true);
		LivePanel.GetComponent<RectTransform> ().localPosition = new Vector3 (LivePanel.GetComponent<RectTransform> ().localPosition.x,LivePanel.GetComponent<RectTransform> ().localPosition.y + 700,0f);
	
		ContinueButton.SetActive (true);
		BackButton.SetActive (false);

	}

	public void OtherEthnicClick(){
		if (OtherEthnicToggle.GetComponent<Toggle> ().isOn) {
			OtherEthnic.SetActive (true);
		} else {
			OtherEthnic.SetActive (false);
		}
	}

	public void DropdownFindClick(){
		if (FindDropdown.captionText.text == "Other") {
			OtherFind.SetActive (true);
			OtherFindLabel.SetActive (true);
		} else {
			OtherFind.SetActive (false);
			OtherFindLabel.SetActive (false);
		}
	}

	/*public string Validation(){
		Field_UserUserName;
		Field_Who;
		Field_WhoOtro;

		Field_Old;
		Field_NZ;
		Field_Maori;
		Field_Samoan;
		Field_Cook;
		Field_Tongan;
		Field_Niuean;
		Field_Chinesse;
		Field_Indian;
		Field_Other;
		Field_OtherTEXT;

		Field_Gender;

		Field_eMail;
		Field_ReeMail;

		Field_Information;

		Field_Password;
		Field_RePassword;

		Field_Code;
		Field_Mobile;

		Field_Live;

		Field_Find;
		Field_SpecifyFind;

		Field_Conact;
		Field_ExtraHelp;
		Field_Terms;

	}*/ 

	public void Submit(){
		bSubmit = true;
		instance.m_ProcessRegistrationAPIDrupal = null;
	}

	public string FieldsValidation(bool bTerms){

		string ErrorMessageResponse = "";

		string Field_Information_YN = "N";
		string Field_Conact_YN = "N";
		string Field_ExtraHelp_YN = "N";
		string Field_Terms_YN = "N";
		string Field_Ethnicity_S = "";



			if (Field_NZ.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}

				Field_Ethnicity_S += "\"" + "NZ European" + "\"";
			}
			if (Field_Maori.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Maori" + "\"";
			}
			if (Field_Samoan.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Samoan" + "\"";

			}
			if (Field_Cook.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Cook Island Maori" + "\"";

			}
			if (Field_Tongan.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Tongan" + "\"";

			}
			if (Field_Niuean.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Niuean" + "\"";

			}
			if (Field_Chinesse.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Chinesse" + "\"";

			}
			if (Field_Indian.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Indian" + "\"";

			}
			if (Field_Other.GetComponent<Toggle> ().isOn) {
				if (Field_Ethnicity_S != "") {
					Field_Ethnicity_S += ",";
				}
				Field_Ethnicity_S += "\"" + "Other" + "\"";

			}

			if (Field_Information.GetComponent<Toggle> ().isOn) {
				Field_Information_YN = "Y";
			}
			if (Field_Conact.GetComponent<Toggle> ().isOn) {
				Field_Conact_YN = "Y";
			}
			if (Field_ExtraHelp.GetComponent<Toggle> ().isOn) {
				Field_ExtraHelp_YN = "Y";
			}
			if (Field_Terms.GetComponent<Toggle> ().isOn) {
				Field_Terms_YN = "Y";
			}


		if (bTerms) {

			if (Field_Terms_YN != "Y") {
				ErrorMessageResponse = "Please agree to the Terms of use.";
				return ErrorMessageResponse;
			}

			if (Field_ExtraHelp_YN != "Y") {
				ErrorMessageResponse = "Please agree to I agree to get extra help.";
				return ErrorMessageResponse;
			}

			if ( Field_eMail.text == "") {
				ErrorMessageResponse = "Please enter a valid email address.";
				return ErrorMessageResponse;
			}

			if (!Field_eMail.text.Contains("@") || Field_eMail.text.IndexOf("@") == 0 || Field_eMail.text.IndexOf("@") == Field_eMail.text.Length-1 || !Field_eMail.text.Contains(".") || Field_eMail.text.IndexOf(".") == Field_eMail.text.Length-1 || Field_eMail.text.IndexOf("@") + 1 == Field_eMail.text.IndexOf(".") ) {
				ErrorMessageResponse = "The e-mail address [The email] is not valid.";
				ErrorMessageResponse = ErrorMessageResponse.Replace ("[The email]", Field_eMail.text);
				return ErrorMessageResponse;
			}

			if ( Field_eMail.text != Field_ReeMail.text) {
				ErrorMessageResponse = "Email confirmation should match email address.";
				return ErrorMessageResponse;
			}

			if (Field_Password.text == "") {
				ErrorMessageResponse = "Please enter a valid password.";
				return ErrorMessageResponse;
			}

			if (Field_Password.text.ToString() != Field_RePassword.text.ToString()) {
				ErrorMessageResponse = "The specified passwords do not match.";
				return ErrorMessageResponse;
			}


		} else {
			

			if (Field_Terms_YN != "Y") {
				ErrorMessageResponse = "Please agree to the Terms of use.";
				return ErrorMessageResponse;
			}

			if (Field_ExtraHelp_YN != "Y") {
				ErrorMessageResponse = "Please agree to I agree to get extra help.";
				return ErrorMessageResponse;
			}

			if ( Field_eMail.text == "") {
				ErrorMessageResponse = "Please enter a valid email address.";
				return ErrorMessageResponse;
			}



			if(Field_UserUserName.text == ""){
				ErrorMessageResponse = "Please enter a username.";
				return ErrorMessageResponse;
			}

			if (Field_Who.captionText.text == "Please select") {
				ErrorMessageResponse = "Please select a valid 'Who are you?' answer.";
				return ErrorMessageResponse;
			}


			if (DropWho.captionText.text == "Young Person") {
			
				if (Field_Old.text == "") {
					ErrorMessageResponse = "Please enter a valid age.";
					return ErrorMessageResponse;
				}

				if (Field_Ethnicity_S == "") {
					ErrorMessageResponse = "Please tell us your ethnic group.";
					return ErrorMessageResponse;
				}

				if (Field_Gender.captionText.text == "") {
					ErrorMessageResponse = "Please select a gender.";
					return ErrorMessageResponse;
				}
			}
			if ( Field_eMail.text == "") {
				ErrorMessageResponse = "Please enter a valid email address.";
				return ErrorMessageResponse;
			}

			if (!Field_eMail.text.Contains("@") || Field_eMail.text.IndexOf("@") == 0 || Field_eMail.text.IndexOf("@") == Field_eMail.text.Length-1 || !Field_eMail.text.Contains(".") || Field_eMail.text.IndexOf(".") == Field_eMail.text.Length-1 || Field_eMail.text.IndexOf("@") + 1 == Field_eMail.text.IndexOf(".") ) {
				ErrorMessageResponse = "The e-mail address [The email] is not valid.";
				ErrorMessageResponse = ErrorMessageResponse.Replace ("[The email]", Field_eMail.text);
				return ErrorMessageResponse;
			}

			if ( Field_eMail.text != Field_ReeMail.text) {
				ErrorMessageResponse = "Email confirmation should match email address.";
				return ErrorMessageResponse;
			}

			if (Field_Password.text == "") {
				ErrorMessageResponse = "Please enter a valid password.";
				return ErrorMessageResponse;
			}

			if (Field_Password.text.ToString() != Field_RePassword.text.ToString()) {
				ErrorMessageResponse = "The specified passwords do not match.";
				return ErrorMessageResponse;
			}

			if (Field_Mobile.textComponent.text == "" || Field_Mobile.text == "") {
				//ErrorMessageResponse = "Please enter a valid mobile number.";
				//return ErrorMessageResponse;
			}

			if (Field_Live.captionText.text == "Please select") {
				ErrorMessageResponse = "Please select a region.";
				return ErrorMessageResponse;
			}

			if (Field_Find.captionText.text == "Please select") {
				ErrorMessageResponse = "Please choose how you heaard about SPARX.";
				return ErrorMessageResponse;
			}

			if (Field_Live.captionText.text == "") {
				ErrorMessageResponse = "Please tick to confirm that you know you need to get extra help if SPARX is not helping you.";
				return ErrorMessageResponse;
			}

		}

		return "OK";

	}


	public string GetString(){

		string sRegistration = "";

		string Field_Information_YN = "N";
		string Field_Conact_YN = "N";
		string Field_ExtraHelp_YN = "N";
		string Field_Terms_YN = "N";
		string Field_Ethnicity_S = "";

		if (Field_NZ.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}

			Field_Ethnicity_S += "\"" + "NZ European" + "\"";
		}
		if (Field_Maori.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Maori" + "\"";
		}
		if (Field_Samoan.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Samoan" + "\"";

		}
		if (Field_Cook.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Cook Island Maori" + "\"";

		}
		if (Field_Tongan.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Tongan" + "\"";

		}
		if (Field_Niuean.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Niuean" + "\"";

		}
		if (Field_Chinesse.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Chinesse" + "\"";

		}
		if (Field_Indian.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Indian" + "\"";

		}
		if (Field_Other.GetComponent<Toggle> ().isOn) {
			if (Field_Ethnicity_S != "") {
				Field_Ethnicity_S += ",";
			}
			Field_Ethnicity_S += "\"" + "Other" + "\"";

		}

		if (Field_Information.GetComponent<Toggle> ().isOn) {
			Field_Information_YN = "Y";
		}
		if (Field_Conact.GetComponent<Toggle> ().isOn) {
			Field_Conact_YN = "Y";
		}
		if (Field_ExtraHelp.GetComponent<Toggle> ().isOn) {
			Field_ExtraHelp_YN = "Y";
		}
		if (Field_Terms.GetComponent<Toggle> ().isOn) {
			Field_Terms_YN = "Y";
		}

		sRegistration += "{\"username\":";
		sRegistration += "\"" + Field_UserUserName.text + "\""; 
		sRegistration += ",\"password\":";
		sRegistration += "\"" + Field_Password.text + "\"";
		sRegistration += ",\"email\":";
		sRegistration += "\"" + Field_eMail.text + "\"";
		sRegistration += ",\"emailallow\":";
		sRegistration += "\"" + Field_Information_YN + "\"";
		sRegistration += ",\"whoareyou\":";
		sRegistration += "\"" + Field_Who.captionText.text + " \"";
		sRegistration += ",\"region\":";
		sRegistration += "\"" + Field_Live.captionText.text + "\"";
		sRegistration += ",\"findout\":";
		sRegistration += "\"" + Field_Find.captionText.text + "\"";
		sRegistration += ",\"findoutother\":";
		sRegistration += "\"" + Field_SpecifyFind.text + "\"";
		sRegistration += ",\"research\":";
		sRegistration += "\"" + Field_Conact_YN + "\"";
		sRegistration += ",\"ethnicity\":";
		sRegistration += "[" + Field_Ethnicity_S + "]";
		sRegistration += ",\"ethnicityother\":";
		sRegistration += "\"" + Field_OtherTEXT.text + "\"";
		sRegistration += ",\"age\":";
		sRegistration += "\"" + Field_Old.text + "\"";
		sRegistration += ",\"gender\":";
		sRegistration += "\"" + Field_Gender.captionText.text + "\"";
		sRegistration += ",\"mobile\":";
		sRegistration += "\"" + Field_Mobile.text + "\"}";
		sRegistration = sRegistration.ToLower ();
		sRegistration = sRegistration.Replace (" ","");
		sRegistration = sRegistration.Replace ("\"n\"","\"N\"");
		sRegistration = sRegistration.Replace ("\"y\"","\"Y\"");

		sRegistration = sRegistration.Replace ("bayofplenty","bay_of_plenty");
		sRegistration = sRegistration.Replace ("hawke'sbay","hawkes_bay");
		sRegistration = sRegistration.Replace ("manawatu_wanganui'","manawatu_wanganui'");
		sRegistration = sRegistration.Replace ("manawatu-wanganui","\"Y\"");
		sRegistration = sRegistration.Replace ("tasman/nelson","tasman_nelson");
		sRegistration = sRegistration.Replace ("westcoast","west_coast");
		sRegistration = sRegistration.Replace ("fromacounsellor","counsellor");
		sRegistration = sRegistration.Replace ("atschoolorfromaschoolguidancecounsellor","school");
		sRegistration = sRegistration.Replace ("fromadoctor/nurse","doctor_nurse");
		sRegistration = sRegistration.Replace ("fromayouthworker","youth_worker");
		sRegistration = sRegistration.Replace ("throughafriend","friend");

		return sRegistration;

	}
		
	void OnEnable(){
		ClearForm();
	}
	public void ClearForm(){

		oTextRegisterNotificationMessage.GetComponent<Notifications> ().fFade = 0;

		UserPanel.SetActive (true);
		YoungPersonPanel.SetActive (false);
		GenderPanel.SetActive (false);

		ContinueButton.SetActive (false);
		BackButton.SetActive (false);

		if (DropWho.captionText.text == "Other") {
			OtherWho.SetActive (true);
		} else {
			OtherWho.SetActive (false);
		}
			
		if (DropWho.captionText.text == "Young Person") {
			UserPanel.SetActive (true);
			//Debug.Log ("<><><" + UserPanel.GetComponent<RectTransform> ().localPosition.y.ToString());
			if(!(UserPanel.GetComponent<RectTransform> ().localPosition.y > 200 && UserPanel.GetComponent<RectTransform> ().localPosition.y < 250)){
				UserPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (UserPanel.GetComponent<RectTransform> ().localPosition.x,UserPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
			}
			YoungPersonPanel.SetActive (true);
			YoungPersonPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (YoungPersonPanel.GetComponent<RectTransform> ().localPosition.x,YoungPersonPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
			GenderPanel.SetActive (true);
			GenderPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (GenderPanel.GetComponent<RectTransform> ().localPosition.x,GenderPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
			EmailPanel.SetActive (true);
			Debug.Log ("<><><" + EmailPanel.GetComponent<RectTransform> ().localPosition.y.ToString());
			EmailPanel.GetComponent<RectTransform> ().localPosition = new Vector3 (EmailPanel.GetComponent<RectTransform> ().localPosition.x,EmailPanel.GetComponent<RectTransform> ().localPosition.y - 1700,0f);
			LivePanel.SetActive (true);
			LivePanel.GetComponent<RectTransform> ().localPosition = new Vector3 (LivePanel.GetComponent<RectTransform> ().localPosition.x,LivePanel.GetComponent<RectTransform> ().localPosition.y + 700,0f);

			ContinueButton.SetActive (true);
			BackButton.SetActive (false);
		}
		Field_NZ.GetComponent<Toggle> ().isOn = false;
		Field_Maori.GetComponent<Toggle> ().isOn = false;
		Field_Samoan.GetComponent<Toggle> ().isOn = false;
		Field_Cook.GetComponent<Toggle> ().isOn = false;
		Field_Tongan.GetComponent<Toggle> ().isOn = false;
		Field_Niuean.GetComponent<Toggle> ().isOn = false;
		Field_Chinesse.GetComponent<Toggle> ().isOn = false;
		Field_Indian.GetComponent<Toggle> ().isOn = false;
		Field_Other.GetComponent<Toggle> ().isOn = false;

		Field_Information.GetComponent<Toggle> ().isOn = true;
		Field_Conact.GetComponent<Toggle> ().isOn = false;
		Field_ExtraHelp.GetComponent<Toggle> ().isOn = false;
		Field_Terms.GetComponent<Toggle> ().isOn = false;

		Field_UserUserName.text = "";
		Field_Password.text = "";
		Field_RePassword.text = "";
		Field_eMail.text = "";
		Field_ReeMail.text = "";
		Field_Who.value = 0;
		Field_Live.value = 0;
		Field_Find.value = 0;
		Field_SpecifyFind.text = "";
		Field_OtherTEXT.text = "";
		Field_Old.text = "";
		Field_Gender.value = 0;
		Field_Mobile.text = "";
	}

	public void HelpButton(){
		Application.OpenURL ("https://sparx.org.nz/help");
	}

	public void TermsButton(){
		Application.OpenURL ("https://sparx.org.nz/terms");
	}
}
