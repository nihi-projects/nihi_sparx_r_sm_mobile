﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

	public GameObject RegistrationForm;
	public Transform ParentRegistrationForm;

	public CapturedDataInput instance;
	public TokenHolderLegacy instanceTHL;
	private bool m_bGotLoginTokenDone    = false;
	public InputField UserName;
	public InputField Password;

	public GameObject LoginPanel;
	public GameObject SignUpPanel;

	private GameObject oNotifications;
	private string sNotificationError; 
	void Awake () {
		GameObject.Find ("GUI").GetComponent<LogosScreen> ().enabled = false;
		GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen> ().enabled = false;
	}
	// Use this for initialization
	void Start () {
		//PlayerPrefs.SetString ("character_playerprefs","");
		instance = GameObject.Find ("CapturedDataInputHolder").GetComponent<CapturedDataInput> ();
		instanceTHL = GameObject.Find ("TokenHolder").GetComponent<TokenHolderLegacy> ();
		oNotifications = GameObject.Find ("NotificationLogin");


        /*if(!(Application.isEditor || Application.isMobilePlatform)){
    //Call the javascript method in HTML page
    Application.ExternalCall("unityGameLoaded", "GUI");
}*/
#if UNITY_WEBGL

        // For webgl version, the login screen is bypassed completely, and LogoBackgroundScreen will get the login token from the web environment
        LoginPanel.SetActive(false);
        SignUpPanel.SetActive(false);
        GameObject.Find("GUI").GetComponent<LogosScreen>().enabled = true;
        GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>().enabled = true;
        return;
#endif



        if (Application.internetReachability != NetworkReachability.NotReachable) {
			if (!string.IsNullOrEmpty(instanceTHL.Global_sGameConfigServerURL)) {
                // already logged in - can skip the login screen
				instance.m_sGameConfigServerURL = instanceTHL.Global_sGameConfigServerURL;
				LoginPanel.SetActive (false);
				SignUpPanel.SetActive (false);
				GameObject.Find ("GUI").GetComponent<LogosScreen> ().enabled = true;
				GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen> ().enabled = true;
			}
		} else {
			if (PlayerPrefs.GetString ("character_playerprefs") != "") {
				LoginPanel.SetActive (false);
				SignUpPanel.SetActive (false);
				GameObject.Find ("GUI").GetComponent<LogosScreen> ().enabled = true;
				GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen> ().enabled = true;
			}
		}

	}
	
	// Update is called once per frame
	void Update () {

		if(instance.m_ProcessLoginAPIDrupal != null){
			if (instance.m_ProcessLoginAPIDrupal.isDone) {

				Debug.Log ("<<<<<<<<<<iPAD>>>>>>>>>> m_logInDataRetriever ISDONE");

				if (string.IsNullOrEmpty (instance.m_ProcessLoginAPIDrupal.error)) { //CONECTION AVAILABLE

					if (instance.m_ProcessLoginAPIDrupal.isDone && !m_bGotLoginTokenDone && string.IsNullOrEmpty (instance.m_ProcessLoginAPIDrupal.error)) {
						instance.GetTokenAfterLoginDrupalAPI_NewJsonSchema (); //EDITOR GETS TOKEN FROM SERVER
						Debug.Log ("<Token from URL>");
						m_bGotLoginTokenDone = true;
						//if (instanceTHL.Global_sGameConfigServerURL != "") {
						if (!string.IsNullOrEmpty(instanceTHL.Global_sGameConfigServerURL)) {
							//StartCoroutine (instance.SavePointEventToServer_NewJsonSchema(5,1,"levelEnd"));
							//Application.LoadLevel ("GuideScene");
							//Application.LoadLevel("LevelS");
							LoginPanel.SetActive (false);
                            if(SignUpPanel != null )
							    SignUpPanel.SetActive (false);
							GameObject.Find ("GUI").GetComponent<LogosScreen> ().enabled = true;
							GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen> ().enabled = true;

						}
					}
				} else {
					if (!oNotifications.GetComponent<Notifications> ().bshowNotification) {
						Debug.Log ("Error from SPARX SERVER DRUPAL:" + instance.m_ProcessLoginAPIDrupal.error.ToString ());
						if (instance.m_ProcessLoginAPIDrupal.error.Contains("Unauthorized") || instance.m_ProcessLoginAPIDrupal.error.Contains("unauthorized")){
							sNotificationError = "User login unsuccessful";
						}else{
							sNotificationError = instance.m_ProcessLoginAPIDrupal.error;
						}
						oNotifications.GetComponent<Notifications> ().stext = sNotificationError;
						oNotifications.GetComponent<Notifications> ().bshowNotification = true;
						oNotifications.GetComponent<Notifications> ().bhideNotification = false;
						oNotifications.GetComponent<Notifications> ().fFade = 0.1f;
						instance.m_ProcessLoginAPIDrupal = null;
					}
				}
			}	
		}
	}


    public void LoginButton(){
		instanceTHL.username = UserName.text;
		instanceTHL.password = Password.text;
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			//StartCoroutine (instance.SavePointEventToServer_NewJsonSchema(5,1,"levelEnd"));
			StartCoroutine (instance.LoginProcess_NewJsonSchema(UserName.text, Password.text));
			//StartCoroutine (instance.LoginProcess_NewJsonSchema("Test level 1", "Welcome123"));
			//StartCoroutine (instance.GetUserLogInToServer_NewSchema());
		} else {

			if (PlayerPrefs.GetString ("character_playerprefs") != "") {
				LoginPanel.SetActive (false);
				SignUpPanel.SetActive (false);
				GameObject.Find ("GUI").GetComponent<LogosScreen> ().enabled = true;
				GameObject.Find ("GUI").GetComponent<LogoBackgroundScreen> ().enabled = true;
			} else {
				oNotifications.GetComponent<Notifications> ().stext = "To login for the first time you must be online.";
				oNotifications.GetComponent<Notifications> ().bshowNotification = true;
				oNotifications.GetComponent<Notifications> ().bhideNotification = false;
				oNotifications.GetComponent<Notifications> ().fFade = 0.1f;
			}
		}

	} 

	public void SignUpButton(){
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			//Destroy (SignUpPanel);
			SignUpPanel = Instantiate (RegistrationForm, ParentRegistrationForm) as GameObject;
			SignUpPanel.GetComponent<RectTransform> ().right =  new Vector3(0,0,0) ;
			SignUpPanel.GetComponent<RectTransform> ().offsetMax = new Vector2(0,0) ;
			SignUpPanel.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1) ;
			SignUpPanel.SetActive (true);
			LoginPanel.SetActive (false);
			//SignUpPanel.SetActive (true);
		} else {

			oNotifications.GetComponent<Notifications> ().stext = "To register you must be online.";
			oNotifications.GetComponent<Notifications> ().bshowNotification = true;
			oNotifications.GetComponent<Notifications> ().bhideNotification = false;
			oNotifications.GetComponent<Notifications> ().fFade = 0.1f;

		}
	}

	public void NewPasswordButton(){
		Application.OpenURL ("https://sparx.org.nz/user/password");
	}

	void OnEnable(){
		//UserName.text = "";
		//Password.text = "";
	}
}
