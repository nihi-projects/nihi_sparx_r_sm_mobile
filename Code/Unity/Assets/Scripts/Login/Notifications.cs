﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Notifications : MonoBehaviour {
	public string stext;
	public Text oText;
	public bool bshowNotification = false;
	public bool bhideNotification = false;
	public float fFade;
	// Use this for initialization
	void Start () {
		Color oColor;
		oColor = oText.color;
		fFade = 0;
	}

	// Update is called once per frame
	private void Update () {
		Color oColor;
		//for (int x = 0; x < 255; x++) {
		oColor = oText.color;
		//oColor.a += 0.1f;
		oColor.a = fFade;
		oText.color = oColor;

		if (fFade >= 1) {
			bshowNotification = false;
			bhideNotification = true;
		}
		if (fFade <= 0) {
			bhideNotification = false;
		}
		if (bshowNotification) {
			oText.text = stext;
			StartCoroutine (ShowText ());
			//bshowNotification = false;
		}

		if (bhideNotification) {
			oText.text = stext;
			StartCoroutine (HideText ());
			//bhideNotification = false;
		}

	}

	private IEnumerator ShowText(){
		//oText.color = new Color32 (255,238,73,255);
		Color oColor;
		//for (int x = 0; x < 255; x++) {
		oColor = oText.color;
		fFade += 0.005f;

		yield return new WaitForSeconds(0.5f);

		//Debug.Log (oColor.a.ToString());
		//}

		//bhideNotification = true;
	}

	private IEnumerator HideText(){

		Color oColor;

		//for (int i = 0; i < 255; i++) {
		oColor = oText.color;
		//fFade -= 0.005f;
		//oText.color = oColor;
		yield return new WaitForSeconds(0.1f);
		//Debug.Log (oColor.a.ToString());
		//}

	}

}
