using UnityEngine;
using System.Collections;

public class SetAnimationStartFrame1 : MonoBehaviour {
	
	public float fTime = 0.0f;
	// Use this for initialization
	void Start () {
		GetComponent<Animation>()["idle"].time = fTime;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
