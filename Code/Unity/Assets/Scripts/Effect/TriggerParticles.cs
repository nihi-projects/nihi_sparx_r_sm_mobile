using UnityEngine;
using System.Collections;

public class TriggerParticles : MonoBehaviour {
    public ParticleSystem rocks;
    void TriggerRocks() 
	{
        rocks.Play();
		SoundPlayer.PlaySound("sfx-rockbreak", 1.0f);
    }
}
