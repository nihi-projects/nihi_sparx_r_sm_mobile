using UnityEngine;
using System.Collections;

public class LeafFlying : MonoBehaviour {
	
	public int MaximumLeafDisplay = 4;
	public float LeafDropRate = 5.0f;
	private float LeafDropElapsed = 0.0f;
	static private int NumOfLeafOnScreen= 0;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(NumOfLeafOnScreen < MaximumLeafDisplay)
		{
			LeafDropElapsed += Time.deltaTime * 0.5f;
			if(LeafDropElapsed >= LeafDropRate)
			{
				LeafDropElapsed = 0.0f;
				DropNewLeaf();
			}
		}
	}
	
	void DropNewLeaf()
	{
		// Randomly select a leaf.
		int iRand = Random.Range(0, 2);
		
		GameObject NewLeaf = GameObject.Instantiate(transform.Find("Leaf" + iRand).gameObject) as GameObject;
		Vector3 NewPos = Global.GetPlayer().transform.position;
		
		float RandZ = Random.Range(1.0f, 3.0f);
		NewPos += RandZ * Global.GetPlayer().transform.forward;
		NewPos.y = Global.GetPlayer().transform.position.y + 12.0f;
		float fRand = Random.Range(-4.0f, 4.0f);
		NewPos.x += fRand;
		NewLeaf.transform.position = NewPos;
		
		NewLeaf.GetComponent<Rigidbody>().useGravity = true;
		NewLeaf.GetComponent<LeafDrop>().SetStartDroping();
		NumOfLeafOnScreen +=1;
	}
	static public void ChangeNumOfLeafDrop(int Change)
	{
		NumOfLeafOnScreen += Change;
	}
}
