using UnityEngine;
using System.Collections;

public class Lv7BridgeBreak : MonoBehaviour {
	public float Distance = 8.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.GetPlayer() && Global.GetPlayer().transform != null){
			if(Vector3.Distance(transform.position, Global.GetPlayer().transform.position) <= Distance)
			{
				GetComponent<Animation>().Play();
			}
		}
	}
}
