using UnityEngine;
using System.Collections;

public class IceDropLv2 : MonoBehaviour {
	public GameObject[] IceParts;
	public bool m_bIsDropping = false;
	public float FallSpeedMin = 2.0f;
	public float FallSpeedMax = 4.0f;
	float[] FallSpeeds;
	public float IntervalMin = 1.0f;
	public float IntervalMax = 2.5f;
    public GameObject snowPuff;
	float[] Intervals;
	bool[] Dropping;
	Vector3[] OriginalPos;
	
	// Use this for initialization
	void Start () {
		FallSpeeds = new float[IceParts.Length];
		Intervals = new float[IceParts.Length];
		Dropping = new bool[IceParts.Length];
		OriginalPos = new Vector3[IceParts.Length];
		
		for(uint i = 0; i < Dropping.Length; ++i)
		{
			Dropping[i] = true;
			OriginalPos[i] = IceParts[i].transform.position;
			FallSpeeds[i] = Random.Range(FallSpeedMin, FallSpeedMax);
			Intervals[i] = Random.Range(IntervalMin, IntervalMax);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bIsDropping)
		{
			// each individual drop
			for(uint i = 0; i < IceParts.Length; ++i)
			{
				if(Dropping[i])
				{
					IceParts[i].transform.position += Vector3.down * FallSpeeds[i] * Time.deltaTime;
					float y = IceParts[i].transform.position.y;
					// Check if dropped on the ground.
					if(y <= 0.0f)
					{
						// if yes, stop droping and calculate interval.
						Dropping[i] = false;
                        GameObject.Instantiate(snowPuff, IceParts[i].transform.position, Quaternion.identity );
                        IceParts[i].transform.position = OriginalPos[i];
						Intervals[i] = Random.Range(IntervalMin, IntervalMax);

						SoundPlayer.PlaySound("sfx-icecrash", 0.35f);
					}
				}
				else
				{
					Intervals[i] -= Time.deltaTime;
					
					// check if interval finished,
					if(Intervals[i] <= 0.0f)
					{
						// if yes, calculate droping speed and start dropping
						Dropping[i] = true;
						FallSpeeds[i] = Random.Range (FallSpeedMin, FallSpeedMax);
					}
				}
			}
		}
	}
	
	public void Reset()
	{
		for(uint i = 0; i < IceParts.Length; ++i)
		{
			IceParts[i].transform.position = OriginalPos[i];
		}
	}
}
