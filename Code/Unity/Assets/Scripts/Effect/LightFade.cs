using UnityEngine;
using System.Collections;

public class LightFade : MonoBehaviour 
{
	
	float m_fTime = 0.0f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_fTime >= 1.0f)
		{
			return ;
		}
		
		m_fTime += Time.deltaTime * 0.6f;
		
		GetComponent<Light>().intensity = Mathf.Lerp(0.0f, 3.0f, m_fTime);
		
	}
}
