using UnityEngine;
using System.Collections;

public class LeafDrop : MonoBehaviour {
	
	private bool StartDroping = false;
	private float RandomRotation = 0.0f;
	Vector3 Force;
	// Use this for initialization
	void Start () {
		Force = Vector3.zero;
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(StartDroping)
		{
			Vector3 NewPos = transform.position;
			transform.LookAt(Camera.main.transform.position);
			transform.Rotate (90.0f, RandomRotation * Time.deltaTime * 5.0f, 0.0f);
			transform.position = NewPos;
			if(transform.position.y <= 0.0f)
			{
				DestroyObject(this.gameObject);
				LeafFlying.ChangeNumOfLeafDrop(-1);
			}
		}
	}
	
	void FixedUpdate()
	{
		transform.GetComponent<Rigidbody>().AddForce(Force);
	}
	
	public void SetStartDroping()
	{
		StartDroping = true;
		RandomRotation = Random.Range(-90, 90);
		Force.x = Random.Range (-10.0f, 10.0f);
	}
	
	void OnCollision()
	{
		DestroyObject(this.gameObject);
	}
}
