using UnityEngine;
using System.Collections;

public class QuickSandEffect : MonoBehaviour {
	float AccumulatedTime = 0.0f;
	public float Speed = 5.0f;
	public bool m_bInverseRotation = false;
	float fRotateDegrees;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {	
		Vector2 Center = new Vector2(0.5f,0.5f);
		if(m_bInverseRotation)
		{
			fRotateDegrees -= Time.deltaTime;
		}
		else
		{
			fRotateDegrees += Time.deltaTime;
		}
		
		if (fRotateDegrees > 360.0f)
		{
			fRotateDegrees -= 360.0f;
		}
		else if (fRotateDegrees < -360.0f)
		{
			fRotateDegrees += 360.0f;
		}
		SetVector(this.GetComponent<Renderer>().material, 
			//"_2DRotationMatrix", 
			"_MainTex",
			fRotateDegrees, 
			Center, true);
		
//    	 Matrix4x4 t = Matrix4x4.TRS(-pivot, Quaternion.identity, Vector3.one);
//		 Quaternion rotation = Quaternion.Euler(0, 0, fRotateDegrees);
//       Matrix4x4 r = Matrix4x4.TRS(Vector3.zero, rotation, Vector3.one);  
//       Matrix4x4 tInv = Matrix4x4.TRS(pivot, Quaternion.identity, Vector3.one);
//       renderer.material.SetMatrix("_Matrix", tInv*r*t);
	}
	
	public void SetVector(Material material,
        string propertyName, float angle, 
		Vector2 center, 
		bool scaleCenter)
    {          
        float cosine = Mathf.Cos(angle);
        float sine = Mathf.Sin(angle);
        if (scaleCenter)
		{
			center.Scale(material.mainTextureScale);
		}
        material.SetVector(propertyName,
            	new Vector4(cosine, sine,
                -center.x * (cosine - sine) + center.x,
                -center.y * (sine + cosine) + center.y));
    }
}
