using UnityEngine;
using System.Collections;

public class SetAnimationStartFrame : MonoBehaviour {
	
	public float fTime = 0.0f;
	// Use this for initialization
	void Start () {
		GetComponent<Animation>()["flapping"].time = fTime;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
