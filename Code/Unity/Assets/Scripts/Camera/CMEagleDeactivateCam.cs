﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class CMEagleDeactivateCam : MonoBehaviour {

	public GameObject timelineGO;



	void Update(){

		if (timelineGO.gameObject.activeSelf) {

			if (gameObject.GetComponent <Camera>().enabled) {
				Camera.main.enabled = false;


			}
				
		} else {

			if (!gameObject.GetComponent <Camera>().enabled) {
				gameObject.GetComponent <Camera>().enabled = true;
			}

		}


	}


}
