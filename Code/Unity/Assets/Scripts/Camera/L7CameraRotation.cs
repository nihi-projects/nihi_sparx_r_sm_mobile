using UnityEngine;
using System.Collections;

public class L7CameraRotation : MonoBehaviour {
	
	private float fAngleRotated      = 0.0f;
	private bool m_bCameraIsRotating = false;
	private bool m_bCameraCanRotate  = true;
	private bool m_bMovingToPoint    = false;
	
	private Vector3 m_vStartVector;
	private Vector3 m_vCurrentVector;
	
		
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bCameraCanRotate){
			Debug.Log ("AAAAAAAAAAA");
			//Let the camera look at the shield
			Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "L7CameraRotation");
			transform.LookAt(GameObject.Find ("shield").transform);
	    	transform.Translate(Vector3.right * Time.deltaTime * 1.5f);
			
			m_vCurrentVector = GameObject.Find ("shield").transform.position - transform.position;
			fAngleRotated = Vector3.Angle(m_vStartVector, m_vCurrentVector);
			
			if(fAngleRotated > 90.0f){
				m_bCameraIsRotating = true;
			}
			
			//If camera rotation is finished
			if(m_bCameraIsRotating && fAngleRotated < 5.0f){
				m_bCameraCanRotate = false;
				
				GetComponent<CameraMovement>().m_bCameraLookAt = false;
				GetComponent<CameraMovement>().m_bFollow = true;
				Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "L7CameraRotation2");

				//Turn off the particle
				foreach(Transform particle in GameObject.Find("Shield Particles").transform){
					particle.GetComponent<ParticleSystem>().enableEmission = false;
				}
				
				//Player walk back to the Mentor and talk with Mentor again once the camera rotation is finished
				Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(Global.GetObjectPositionByID("30001"), GameObject.Find ("Mentor").transform.position);
				m_bCameraIsRotating = false;
				m_bMovingToPoint = true;	
			}
		}
		
		//When the player arrived the point 30001
		if(!Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition && m_bMovingToPoint){
			//Let the player talk with mentor again after release the level 7 Gem
			GameObject.Find ("Mentor").GetComponent<CharacterInteraction>().enabled = true;
			GameObject.Find ("Mentor").GetComponent<CharacterInteraction>().InteractionRadius = 12;
			GameObject.Find ("Mentor").GetComponent<CharacterInteraction>().Interacted = false;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().SetCurrentDialogue("L7MentorScene4");
			
			this.enabled = false;
			GameObject.Find("LevelS").GetComponent<L7FinalCutScene>().enabled = false;
		}
	}
	
	void OnEnable()
	{
		//Start vector
		m_vStartVector = GameObject.Find ("shield").transform.position - transform.position;
	}
}
