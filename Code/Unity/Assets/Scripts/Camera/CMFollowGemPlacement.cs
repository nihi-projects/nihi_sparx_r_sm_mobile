﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class CMFollowGemPlacement : MonoBehaviour {

	#region PUBLIC fields

	public GameObject timeline;
	public Camera camera2;

	#endregion

	#region PRIVATE fields

	private PlayerMovement _pMovemenrRef;
    private bool bMovementSet = false;

	#endregion

	#region INIT

	void Start(){

		GameObject player = Global.GetPlayer();
		_pMovemenrRef = player.GetComponent <PlayerMovement> ();

	}

    #endregion

    #region UPDATE

    void Update()
    {
        if (timeline.activeSelf)
        {
            if (Camera.main.enabled)
            {
                Camera.main.enabled = false;
                _pMovemenrRef.SetMovement( false, "CMFollowGemPlacement");

            }
        }
        else
        {
            if (camera2.transform.position != Vector3.zero)
            {
                if (Camera.main)
                {
                    Camera.main.enabled = true;

                    camera2.transform.localPosition = Vector3.zero;
                    camera2.transform.localEulerAngles = Vector3.zero;

                    camera2.gameObject.SetActive(false);

                    //Set the movement back on.
                    if(bMovementSet == false )
                    {
                        bMovementSet = true;
                        _pMovemenrRef.SetMovement(true, "CMFollowGemPlacement2");
                    }
                }
            }
        }
    }

	#endregion


}
