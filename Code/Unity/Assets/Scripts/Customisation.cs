﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customisation : MonoBehaviour {

    protected GameObject character;
    protected Color m_rCurrentHairColor;
    protected Color m_rCurrentTrimColor;
    protected Texture2D m_tCurrentTrimTexture;

    //Hair color
    protected int m_iTotalHairColorNum = 10;
    protected int m_iCurrentHairColorNum = 1;

    //Eye Color
    protected int m_iTotalEyeColorNum = 6;
    protected int m_iCurrentEyeColorNum = 1;
    protected Color m_rCurrentEyeColor;

    //Skin Color
    protected int m_iTotalSkinColorNum = 4;
    protected int m_iCurrentSkinColorNum = 1;

    //Hair style
    protected int m_iCurrentHairStyleNum = 1;

    //Trim Style
    protected int m_iCurrentTrimNum = 1;

    //Backpack.
    protected int m_iCurrentBackpackNum = 1;

    //Trim color
    protected int m_iTotalTrimColorNum = 5;
    protected int m_iCurrentTrimColorNum = 1;

    //Cloth Color
    protected int m_iTotalClothColorNum = 5;
    protected int m_iCurrentClothColorNum = 1;
    protected Color m_rCurrentColorColor;

    protected int m_iTotalTrimNum = 4;

    //Backpack
    protected int m_iTotalBackpackNum = 2;

    protected int m_iBodyType = 0;

    //Trim textures
    public Texture2D[] m_tTrimTextures;

    protected int m_iTotalHairStyleNumber = 11; //Can get this from HideHair

    protected List<SkinnedMeshRenderer> m_hairMeshes = new List<SkinnedMeshRenderer>();

    private CapturedDataInput instance;

    [HideInInspector]
    public CharacterCreation creationScreen;

    public void Start()
    {
        //Connecting to the server
        instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
    }

    public GameObject CommonEnable(GameObject _prefab, Vector3 _position )
    {
        //Load the girl object on the screen
        character = (GameObject)Instantiate(_prefab, _position, Quaternion.identity);
        character.transform.rotation = new Quaternion(0.0f * Global.ScreenWidth_Factor, 180.0f * Global.ScreenHeight_Factor, 0.0f * Global.ScreenWidth_Factor, 1.0f * Global.ScreenHeight_Factor);
        Destroy(character.GetComponent<PlayerMovement>());

        BodyPartHelper bpf = character.GetComponent<BodyPartHelper>();
        m_rCurrentHairColor = bpf.hair1.GetComponent<SkinnedMeshRenderer>().material.color;
        m_rCurrentTrimColor = bpf.bodyMaterial.color;
        m_tCurrentTrimTexture = m_tTrimTextures[0];

        SkinnedMeshRenderer hairMesh = null;

        //Get the all the hair style children
        for (int i = 1; i < m_iTotalHairStyleNumber; i++)
        {
            //Go through each hair style in  the girl object
            hairMesh = character.transform.Find(bpf.characterName + "_hair_" + i.ToString()).GetComponent<SkinnedMeshRenderer>();
            m_hairMeshes.Add(hairMesh);
        }

        return character;
    }

    public void OnDisable()
    {
        GameObject.Destroy(creationScreen.gameObject);
    }

    public Texture2D GetTexture(int index)
    {
        if (m_tTrimTextures.Length >= index)
            return m_tTrimTextures[index];
        return (null);
    }

    public BodyPartHelper GetBPF()
    {
        return character.GetComponent<BodyPartHelper>();
    }

    public void SetGlobals()
    {
        Global.userHairStyleNum = m_iCurrentHairStyleNum;
        Global.userHairColor = GetBPF().GetHair(m_iCurrentHairStyleNum).GetComponent<Renderer>().material.color;
        Global.userSkinColor = GetBPF().bodyMaterial.color;
        Global.userEyeColor = GetBPF().eyeMaterial.color;
        Global.userClothColor = GetBPF().clothMaterial.color;
        Global.userTrimTextureNum = m_iCurrentTrimNum;
        Global.userTrimColor = GetBPF().trimMaterial.color;
        Global.userBackPackNum = m_iCurrentBackpackNum;
        Global.userBodyType = m_iBodyType;
    }

    public void ChangeTrimColour()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(0.0f, 0.15f, 3f);

        m_iCurrentTrimColorNum++;

        if (m_iCurrentTrimColorNum > m_iTotalTrimColorNum)
        {
            m_iCurrentTrimColorNum = 1;
        }
        Color newTrimColor = new Color(0.52f, 0.45f, 0.0f, 1.0f);
        switch (m_iCurrentTrimColorNum)
        {
            case 1:
                newTrimColor = new Color(0.52f, 0.45f, 0.0f, 1.0f);
                break;
            case 2:
                newTrimColor = new Color(0.49f, 0.02f, 0.02f, 1.0f);
                break;
            case 3:
                newTrimColor = new Color(0.1f, 0.48f, 0.68f, 1.0f);
                break;
            case 4:
                newTrimColor = new Color(0.34f, 0.08f, 0.64f, 1.0f);
                break;
            case 5:
                newTrimColor = new Color(0.1f, .5f, 0.05f, 1.0f);
                break;
        }
        //Find the hair mesh, then change its colour
        GetBPF().trimMaterial.SetColor("_Color", newTrimColor);
    }

    protected void ChangeCharacterHair()
    {
        //Change the position of the girl object
			character.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);
				
			//Disable the old hair style
			GetBPF().GetHair(m_iCurrentHairStyleNum).GetComponent<SkinnedMeshRenderer>().enabled = false;
			
			m_iCurrentHairStyleNum++;
			
			//Go back to the first hair style
			if(m_iCurrentHairStyleNum > m_iTotalHairStyleNumber){
				m_iCurrentHairStyleNum = 1;
			}

            //Enable the new hair style
            GetBPF().GetHair(m_iCurrentHairStyleNum).GetComponent<SkinnedMeshRenderer>().enabled = true;
    }

    protected void ChangeHairColour()
    {
        Color[] hairColours = new Color[] { new Color(0.11f, 0.11f, 0.11f, 1.0f) , new Color(0.25f, 0.17f, 0.1f, 1.0f) , new Color(0.56f, 0.49f, 0.26f, 1.0f),
                                            new Color(0.48f, 0.26f, 0.11f, 1.0f), new Color(0.35f, 0.1f, 0.05f, 1.0f), new Color(0.21f, 0.38f, 0.54f, 1.0f),
                                            new Color(0.35f, 0.5f, 0.21f, 1.0f), new Color(0.63f, 0.34f, 0.49f, 1.0f), new Color(0.46f, 0.28f, 0.5f, 1.0f),
                                            new Color(0.49f, 0.49f, 0.52f, 1.0f) };

        //Change the position of the girl object
        character.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);

        m_iCurrentHairColorNum++;

        if (m_iCurrentHairColorNum >= hairColours.Length )
        {
            m_iCurrentHairColorNum = 0;
        }
        Color newHairColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        
        //New hair colour.
        newHairColor = hairColours[m_iCurrentHairColorNum];

        //Find the hair mesh, then change its colour
        character.GetComponent<BodyPartHelper>().SetHair(newHairColor);
    }

    protected void ChangeSkinColour()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);

        m_iCurrentSkinColorNum++;

        if (m_iCurrentSkinColorNum > m_iTotalSkinColorNum)
        {
            m_iCurrentSkinColorNum = 1;
        }

        Color newSkinColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        switch (m_iCurrentSkinColorNum)
        {
            case 1:
                //pale
                newSkinColor = new Color(0.6f, 0.6f, 0.6f, 1.0f);
                break;
            case 2:
                //tan
                newSkinColor = new Color(0.49f, 0.4f, 0.35f, 1.0f);
                break;
            case 3:
                //pacific
                newSkinColor = new Color(0.3f, 0.23f, 0.19f, 1.0f);
                break;
            case 4:
                //african
                newSkinColor = new Color(0.18f, 0.168f, 0.164f, 1.0f);
                break;
        }

        //Find the skin mesh, then change its colour
        GetBPF().SetSkin(newSkinColor);
    }

    protected void ChangeEyeColour()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);

        m_iCurrentEyeColorNum++;

        if (m_iCurrentEyeColorNum > m_iTotalEyeColorNum)
        {
            m_iCurrentEyeColorNum = 1;
        }
        Color newEyeColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        switch (m_iCurrentEyeColorNum)
        {
            case 1:
                newEyeColor = new Color(0.55f, 0.73f, 0.94f, 1.0f);
                break;
            case 2:
                newEyeColor = new Color(0.77f, 0.84f, 0.95f, 1.0f);
                break;
            case 3:
                newEyeColor = new Color(0.84f, 0.64f, 0.47f, 1.0f);
                break;
            //case 4:
            //newEyeColor = m_rCurrentHairColor;
            //break;
            case 4:
                newEyeColor = new Color(0.59f, 0.32f, 0.19f, 1.0f);
                break;
            case 5:
                newEyeColor = new Color(0.51f, 0.86f, 0.54f, 1.0f);
                break;
            case 6:
                newEyeColor = new Color(0.95f, 0.42f, 0.42f, 1.0f);
                break;
        }
        //Find the hair mesh, then change its colour
        GetBPF().eyeMaterial.SetColor("_Color", newEyeColor);
    }

    public void ChangeClothesColour()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(0.0f, 0.15f, 3f);

        m_iCurrentClothColorNum++;

        if (m_iCurrentClothColorNum > m_iTotalClothColorNum)
        {
            m_iCurrentClothColorNum = 1;
        }
        Color newClothColor = new Color(0.35f, 0.54f, 0.9f, 1.0f);
        switch (m_iCurrentClothColorNum)
        {
            case 1:
                newClothColor = new Color(0.82f, 0.2f, 0.2f, 1.0f);
                break;
            case 2:
                newClothColor = new Color(1.0f, 0.87f, 0.0f, 1.0f);
                break;
            case 3:
                newClothColor = new Color(0.77f, 0.44f, 0.97f, 1.0f);
                break;
            case 4:
                newClothColor = new Color(0.35f, 0.54f, 0.9f, 1.0f);
                //newClothColor = m_rCurrentHairColor;
                break;
            case 5:
                newClothColor = new Color(0.1f, 1.0f, 0.4f, 1.0f);
                break;
        }
        //Find the hair mesh, then change its colour
        GetBPF().GetMaterial(GetBPF().body, "_body").SetColor("_Color", newClothColor);
    }

    protected void ChangeCharacterTrim()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(0.0f, 0.15f, 3f);

        m_iCurrentTrimNum++;

        if (m_iCurrentTrimNum >= m_tTrimTextures.Length)
        {
            m_iCurrentTrimNum = 0;
        }
        m_tCurrentTrimTexture = m_tTrimTextures[m_iCurrentTrimNum];

        //Find the trim
        GetBPF().trimMaterial.SetTexture("_MainTex", m_tCurrentTrimTexture);
    }

    protected void ChangeBackpack()
    {
        //Change the position of the girl object
        character.transform.position = new Vector3(0.0f, 0.15f, 3f);

        //Disable the old hair style
        GetBPF().GetBag(m_iCurrentBackpackNum).GetComponent<SkinnedMeshRenderer>().enabled = false;

        m_iCurrentBackpackNum++;

        if (m_iCurrentBackpackNum > m_iTotalBackpackNum)
        {
            m_iCurrentBackpackNum = 1;
        }

        //Enable the new hair style
        GetBPF().GetBag(m_iCurrentBackpackNum).GetComponent<SkinnedMeshRenderer>().enabled = true;
    }

    protected void ChangePants()
    {
        ++m_iBodyType;
        if(m_iBodyType >= BodyPartHelper.bodyObjects.Length )
        {
            m_iBodyType = 0;
        }
        GetBPF().SetPants(m_iBodyType);
    }

    protected void ConfirmCharacter( string _girlOrBoy )
    {
        this.enabled = false;
        //Go to the guide splash screen
        GameObject.Find("GUI").GetComponent<TheGuideSplashScreen>().enabled = true;

        Global.CurrentPlayerName = _girlOrBoy;

        SetGlobals();

        //SAVE LEVEL
        StartCoroutine(instance.SaveUserLevelToServer_NewJsonSchema());
    }
}
