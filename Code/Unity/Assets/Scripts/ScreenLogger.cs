﻿using UnityEngine;
using System.Collections.Generic;

public class ScreenLogger : MonoBehaviour {

    public static ScreenLogger instance = null;
    public static int maxStrings = 20;
    public static bool showStrings = true;
    public GUISkin optionalSkin;

    [HideInInspector]
    public List<string> screenStrings;
    [HideInInspector]
    public List<string> singleFrameStrings;

    // Use this for initialization
    void Awake () {
        screenStrings = new List<string>();
        singleFrameStrings = new List<string>();
        
        if( instance == null )
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Update()
    {
        ClearFrameLog();
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            showStrings = !showStrings;
        }
    }

    // Update is called once per frame
    private void OnGUI()
    {
        if ( Debug.isDebugBuild == true  && showStrings)
        {
            GUISkin oldSkin = GUI.skin;
            if (optionalSkin != null)
                GUI.skin = optionalSkin;

            GUI.contentColor = Color.green;
            if (singleFrameStrings.Count > 0)
            {
                for (int j = (singleFrameStrings.Count - 1); j >= 0; --j)
                {
                    string str = singleFrameStrings[j];
                    GUILayout.Label(str);
                }
            }

            GUI.contentColor = Color.red;
            for (int i = screenStrings.Count - 1; i >= 0; --i)
            {
                GUILayout.Label(screenStrings[i]);
            }

            GUI.skin = oldSkin;
        }
    }

    public static void Log( string _thing )
    {
        ScreenLogger inst = Get();
        inst.screenStrings.Add(_thing);
        if (inst.screenStrings.Count > maxStrings)
            inst.screenStrings.RemoveAt(0);
    }

    public static void LogEachFrame( string _thing )
    {
        ScreenLogger inst = Get();
        if( inst != null )
        {
            bool alreadyLogged = false;
            for (int i = 0; i < instance.singleFrameStrings.Count; ++i)
            {
                if (instance.singleFrameStrings[i] == _thing)
                {
                    alreadyLogged = true;
                    break;
                }
            }
            if (alreadyLogged == false)
                instance.singleFrameStrings.Add(_thing);
        }
    }

    private static ScreenLogger Get()
    {
        if (instance != null )
            return instance;
        return null;
    }

    public static void ClearFrameLog()
    {
        ScreenLogger inst = Get();
        if (inst != null )
            inst.singleFrameStrings.Clear();
    }
}
