using UnityEngine;
using System.Collections;

public class L7FinalCutScene : MonoBehaviour {

	public GUISkin m_skin;
	
	//private variables
	//private GUISkin m_skin                = null;
	private float m_fFadeInAlpha            = 0.0f;
	private float m_fFadeOutAlpha           = 1.0f;
	private float m_fFadeInStartTime        = 0.0f;
	private float m_fFadeOutStartTime       = 0.0f;
	
	private bool m_bCutsceneFadeInStart     = true;
	private bool m_bCutsceneFadeOutStart    = true;
	private bool m_shieldStartMoving        = false;
	private bool m_bShieldRaising           = false;  
	
	
	// Use this for initialization
	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		FontSizeManager.checkFontSize(m_skin);
	}
	
	// Update is called once per frame
	public void Update () 
	{
		//Start rotating camera after shield raise is finished
		if(m_shieldStartMoving && GameObject.Find ("shield").GetComponent<ObjectMovement>().m_bMovementComplete){
			CameraRotate();
			m_shieldStartMoving = false;
		}
	}
	
	public void OnEnable()
	{
		//Get fade in start time
		m_fFadeInStartTime = Time.time;
	}
	
	/**
	 * This is the white screen appear at the begining of
	 * the cut scene.
    */
	public void OnGUI(){
		GUI.depth = 0;
		GUI.skin = m_skin;
	
		//Fade in starts
		if(m_bCutsceneFadeInStart){
			m_fFadeInAlpha = Mathf.Lerp(0.0f, 1.0f, (Time.time - m_fFadeInStartTime) * 0.3f);
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInAlpha);
			GUI.Label(new Rect(0,0,Screen.width,Screen.height), "", "FinalCutWhiteScreen");
		}
		//Fade in finishes, fix island mesh
		if(m_fFadeInAlpha == 1.0f && m_bCutsceneFadeInStart){
			//Turn the LevelS_broken off
			foreach (Transform child in GameObject.Find ("LevelS_broken").transform){
				child.GetComponent<MeshRenderer>().enabled = false;
			}
			//Turn the LevelS_fixed on
			GameObject.Find ("LevelS_fixed").GetComponent<MeshRenderer>().enabled = true;
			
			//Fade out start here
			m_bCutsceneFadeInStart = false;
		}
		
		//Get fade out start time
		if(m_fFadeInAlpha == 1.0f && m_bCutsceneFadeOutStart){
			m_fFadeOutStartTime = Time.time;
			m_bCutsceneFadeOutStart = false;
		}
		//Fade out starts
		if(!m_bCutsceneFadeOutStart){
			m_fFadeOutAlpha = Mathf.Lerp(1.0f, 0.0f, (Time.time - m_fFadeOutStartTime) * 0.3f);
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeOutAlpha);
			GUI.Label(new Rect(0,0,Screen.width,Screen.height), "", "FinalCutWhiteScreen");
		}
		//Fade out finishes
		if(m_fFadeOutAlpha == 0.0f && !m_bShieldRaising){
			ShieldRaiseUp();
			m_bShieldRaising = true;
		}
	}
	
	public void ShieldRaiseUp()
	{
		//Raise up the shield and simulate particle
		GameObject.Find ("shield").GetComponent<ObjectMovement>().m_bStartMoving = true;
		m_shieldStartMoving = true;
		
		foreach(Transform particle in GameObject.Find("Shield Particles").transform){
			particle.GetComponent<ParticleSystem>().Play();
		}
	}
	
	public void CameraRotate()
	{
		//Set off the main camera from the player
		GameObject.Find ("Main Camera").GetComponent<CameraMovement>().m_bFollow = false;
		GameObject.Find ("Main Camera").GetComponent<Transform>().position = new Vector3(3.037f, 2.3f, 3.231f);
		//Rotate the main camera
		GameObject.Find ("Main Camera").GetComponent<L7CameraRotation>().enabled = true;
	}
}
