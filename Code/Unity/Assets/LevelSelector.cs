﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour {

    public string levelString = "Level1";
    private CapturedDataInput dataInput = null;
    private bool sendOffMoreCalls = true;

    public InputField usernameField;

    private bool reset = false;

    public void Level1()
    {
        levelString = "Level1_Cave";
        LogoBackgroundScreen.levelSelectOverride = 1;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level2()
    {
        levelString = "Level2_Ice";
        LogoBackgroundScreen.levelSelectOverride = 2;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level3()
    {
        levelString = "Level3_Lava";
        LogoBackgroundScreen.levelSelectOverride = 3;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level4()
    {
        levelString = "Level4_Cliff";
        LogoBackgroundScreen.levelSelectOverride = 4;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level5()
    {
        levelString = "Level5_Swamp";
        LogoBackgroundScreen.levelSelectOverride = 5;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level6()
    {
        levelString = "Level6_Bridge";
        LogoBackgroundScreen.levelSelectOverride = 6;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void Level7()
    {
        levelString = "Level7_Canyon";
        LogoBackgroundScreen.levelSelectOverride = 7;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void LevelS()
    {
        levelString = "LevelS";
        LogoBackgroundScreen.levelSelectOverride = 2;
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void G1()
    {
        GotoLevel(7, "GuideScene", "levelEnd" );
    }

    public void G2()
    {
        GotoLevel(1, "GuideScene", "levelEnd");
    }

    public void G3()
    {
        GotoLevel(2, "GuideScene", "levelEnd");
    }

    public void G4()
    {
        GotoLevel(3, "GuideScene", "levelEnd");
    }

    public void G5()
    {
        GotoLevel(4, "GuideScene", "levelEnd");
    }

    public void G6()
    {
        GotoLevel(5, "GuideScene", "levelEnd");
    }

    public void G7()
    {
        GotoLevel(6, "GuideScene", "levelEnd"); 
    }

    public void LS1()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(1, "LevelS", "");
    }
    public void LS2()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(2, "LevelS", "");
    }
    public void LS3()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(3, "LevelS", "");
    }
    public void LS4()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(4, "LevelS", "");
    }
    public void LS5()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(5, "LevelS", "");
    }
    public void LS6()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(6, "LevelS", "");
    }
    public void LS7()
    {
        Global.LevelSVisited = true;
        Global.m_bGotCurrentLevelGem = true;
        GotoLevel(7, "LevelS", "");
    }


    public void GotoLevel( int _levelNumber, string _sceneName, string _savePoint )
    {
        levelString = _sceneName;
        LogoBackgroundScreen.levelSelectOverride = _levelNumber;
        LogoBackgroundScreen.m_savePointOverride = _savePoint;
        LogoBackgroundScreen.m_levelOverride = true;
        StartLevel();
    }

    public void ResetUser()
    {
        levelString = "GuideScene";
        LogoBackgroundScreen.levelSelectOverride = 1;
        reset = true;
        StartLevel(usernameField.text);
    }

    public void StartLevel( string _user = "test13", string _password = "1234")
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            dataInput = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
            StartCoroutine(dataInput.LoginProcess_NewJsonSchema(_user, _password));
        }
        else
        {
            if (PlayerPrefs.GetString("character_playerprefs") != "")
            {
                GameObject.Find("GUI").GetComponent<LogosScreen>().enabled = true;
                GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>().enabled = true;
            }
        }
    }
    
    public void EraseSave()
    {
        GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>().SaveToServer(false, false, "", false);
    }

    public void Update()
    {
        if (dataInput != null && dataInput.m_ProcessLoginAPIDrupal != null)
        {
            if (dataInput.m_ProcessLoginAPIDrupal.isDone)
            {
                GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>().enabled = true;

                if (dataInput.m_LevelDataGetRetriever != null && dataInput.m_LevelDataGetRetriever.isDone &&
                    dataInput.m_ProcessDataGetRetriever != null && dataInput.m_ProcessNotebookDataGetRetriever!= null && dataInput.m_ProcessDataGetRetriever.isDone && 
                    dataInput.m_ProcessNotebookDataGetRetriever.isDone)
                {
                    Debug.Log("Finished logging in.");

                    if( reset == true )
                    {
                        PlayerPrefs.SetString("userresponse_playerprefs", null);
                    }

                    Application.LoadLevel(levelString);

                    GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>().enabled = false;
                }
            }
        }
    }
}
