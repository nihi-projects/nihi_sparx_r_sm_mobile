﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHair : MonoBehaviour {

    [HideInInspector]
    public int numHairStyles;

    // Use this for initialization
    void Awake ()
    {

        bool skipFirstHair = false;
		for( int i = 0; i < transform.childCount; ++i )
        {
            Transform trans = transform.GetChild(i);
            if (trans.name.Contains("hair"))
            {
                ++numHairStyles;
                if (skipFirstHair == false)
                {
                    skipFirstHair = true;
                    continue;
                }
                
                trans.GetComponent<SkinnedMeshRenderer>().enabled = false;
            }
        }
	}
}
