﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipBox : MonoBehaviour
{
	public Text toolTipText;
    [HideInInspector]
    public bool bTextSet = true;

    public void OnEnable()
    {
        bTextSet = true;
    }

    public void SetText( string _str )
    {
        toolTipText.text = _str;
        bTextSet = true;
    }

    public void Update()
    {
        if(bTextSet == false)
        {
            gameObject.SetActive(false);
        }

        bTextSet = false;
    }
}
