﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SortItNegotiateDialog : MonoBehaviour {

    public Image[] images;
    public string[] blincDescriptions;
    public string defaultDescription;
    public Text description;

    public void OnEnable()
    {
        if (Global.GetPlayer() != null)
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(false, "SortItNegotiateDialog");
    }

    public void OnDisable()
    {
        if (Global.GetPlayer() != null)
            Global.GetPlayer().GetComponent<PlayerMovement>().SetMovement(true, "SortItNegotiateDialog2");
    }

    // Update is called once per frame
    void Update ()
    {
        description.text = defaultDescription;
        for( int i = 0; i < images.Length; ++i )
        {
            Bounds bounds = new Bounds(images[i].rectTransform.position, new Vector3(images[i].rectTransform.rect.width, images[i].rectTransform.rect.height, 0f));
            if (  bounds.Contains(Input.mousePosition) || ( Input.touchCount > 0 && bounds.Contains(Input.touches[0].position)) )
            {
                description.text = blincDescriptions[i];
            }
        }
    }
}
