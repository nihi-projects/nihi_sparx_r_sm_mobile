﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    public string strID;

    private TokenHolderLegacy instanceTHL;

    private void Awake()
    {
        instanceTHL = GameObject.Find("TokenHolder").GetComponent<TokenHolderLegacy>();

    }

    // Use this for initialization
    void Start ()
    {
		if( strID != null && strID != "")
        {
            int pickedUp = PlayerPrefs.GetInt(instanceTHL.username + "_" + strID, 0);
            if( pickedUp == 1 )
            {
                Destroy(gameObject);
            }
        }
        else
        {
            Debug.LogError("No id assigned to this collectible");
            Destroy(gameObject);
        }
	}

    public void OnTriggerEnter(Collider _col)
    {
        if( strID != null && strID != "" )
        {
            PlayerPrefs.SetInt(instanceTHL.username + "_" + strID, 1);
            Destroy(gameObject);
        }
    }
}
