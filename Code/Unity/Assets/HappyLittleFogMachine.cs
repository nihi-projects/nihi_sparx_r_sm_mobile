﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EffectAttachPoint
{
    public GameObject obj;
    public Vector3 offset;
}

public class HappyLittleFogMachine : MonoBehaviour {

    public GameObject fogPrefab;
    public EffectAttachPoint[] characterAttachPoint;

	// Use this for initialization
	void Start () {

        if (characterAttachPoint.Length == 0)
            Debug.LogError("No characters to attach fog to.");

		foreach(EffectAttachPoint character in characterAttachPoint)
        {
            if( character != null )
            {
                GameObject effect = GameObject.Instantiate(fogPrefab, character.obj.transform);
                effect.transform.position += character.offset;
            }
            else
                Debug.LogError( "Character is null!" );
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
