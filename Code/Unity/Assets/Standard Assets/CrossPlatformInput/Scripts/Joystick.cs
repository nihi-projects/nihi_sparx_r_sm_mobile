using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical, // Only vertical
            None
		}

		private GameObject m_ObjectMobileController;
		private GameObject m_ObjectMobileControllerMove;
		private GameObject m_ObjectMobileControllerSprint;

		//This will work for both controls.
		private Vector2 m_controlOffset = new Vector2(200, 200);

		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis;
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis;


		private Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis

		private RectTransform rectTrans = null;
		private bool isLeft = true;

		void Awake()
		{
			rectTrans = GetComponent<RectTransform> ();
            //m_StartPos = transform.position;
        }

		void Start() // Changed from OnEnable to Start
		{
			//rectTrans.anchoredPosition = m_controlOffset;
			//SetMobileController ();
			//m_StartPos = transform.position;
			CreateVirtualAxes();
			//Debug.Log(this);

			SetMobileController ();
		}

		void OnEnable() // Changed from OnEnable to Start
		{
			//SetMobileController ();
			//m_StartPos = transform.position;
			CreateVirtualAxes();
			//Debug.Log(this);
		}

		private void RepositionControl( int _zeroIsLeft )
		{
			rectTrans.pivot = new Vector2 (0.5f, 0.5f);
			if (_zeroIsLeft == 0) 
			{
				rectTrans.anchorMax = new Vector2 (0f, 0f);
				rectTrans.anchorMin = new Vector2 (0f, 0f);

				rectTrans.anchoredPosition = m_controlOffset;
			} 
			else 
			{
				rectTrans.anchorMax = new Vector2 (1f, 0f);
				rectTrans.anchorMin = new Vector2 (1f, 0f);

				rectTrans.anchoredPosition = new Vector2(-1*m_controlOffset.x, m_controlOffset.y);
			}

			m_StartPos = transform.position;
		}

		void UpdateVirtualAxes(Vector3 value)
		{

			//Debug.Log (CrossPlatformInputManager.GetAxis ("Vertical"));
			//Debug.Log (CrossPlatformInputManager.GetAxis ("Horizontal"));
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(-delta.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(delta.y);
			}
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
                CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
                CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);
                m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		public void OnDrag(PointerEventData data)
		{
			Vector3 newPos = Vector3.zero;

			if (m_UseX)
			{
				int delta = (int)(data.position.x - m_StartPos.x);
				//delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
				newPos.x = delta;
			}

			if (m_UseY)
			{
				int delta = (int)(data.position.y - m_StartPos.y);
				//delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
				newPos.y = delta;
			}
			transform.position = Vector3.ClampMagnitude( new Vector3(newPos.x, newPos.y, newPos.z), MovementRange) + m_StartPos; //Changed pos clamp from individual axis to both, changing clamp range from square to round.
			UpdateVirtualAxes(transform.position);
		}


		public void OnPointerUp(PointerEventData data)
		{
			SetMobileController ();
			//transform.localPosition = m_StartPos;
			UpdateVirtualAxes(m_StartPos);
		}


		public void OnPointerDown(PointerEventData data) { }

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}

		public static void SetMobileController()
		{
			GameObject m_ObjectMobileController = GameObject.Find ("MobileSingleStickControl");
			GameObject m_ObjectMobileControllerMove = GameObject.Find ("MobileSingleStickControl/Move");
			GameObject m_ObjectMobileControllerSprint = GameObject.Find ("MobileSingleStickControl/Sprint");
			if (m_ObjectMobileControllerMove != null && m_ObjectMobileControllerSprint != null){
				if (PlayerPrefs.GetInt ("playerprefs_LeftHanded") == 0) {

					m_ObjectMobileControllerMove.GetComponent<Joystick> ().RepositionControl (0);
					m_ObjectMobileControllerSprint.GetComponent<Joystick> ().RepositionControl (1);
				}else{
					m_ObjectMobileControllerMove.GetComponent<Joystick> ().RepositionControl (1);
					m_ObjectMobileControllerSprint.GetComponent<Joystick> ().RepositionControl (0);
				}
				//m_ObjectMobileController.SetActive (false);
				m_ObjectMobileController.SetActive (true);
			}
		}
	}
}