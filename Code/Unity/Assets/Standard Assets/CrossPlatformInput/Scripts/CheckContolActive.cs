﻿using UnityEngine;
using System.Collections;

public class CheckContolActive : MonoBehaviour {
	public bool bValidated = false;
	private GameObject m_ObjectMobileController;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!bValidated)
        {            
            if (GameObject.Find("MobileSingleStickControl") != null )
            {
                m_ObjectMobileController = GameObject.Find("MobileSingleStickControl");
                if (m_ObjectMobileController.activeSelf)
                {
                    m_ObjectMobileController.SetActive(false);
                    m_ObjectMobileController.SetActive(true);
                    bValidated = !bValidated;
                }
            }
        }
	}
}
