using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput
{
	[ExecuteInEditMode]
	public class MobileControlRig : MonoBehaviour
	{
        public Image moveImage;
        public Image jumpImage;

		//public GameObject m_ObjectMobileController;
		private Transform m_ObjectMobileControllerMove;
		private Transform m_ObjectMobileControllerSprint;
		private Vector3 m_ObjectMobileController_LeftPosition = new Vector3(-370, -220, 0);
		private Vector3 m_ObjectMobileController_RightPosition =  new Vector3(290, -220, 0);

		// this script enables or disables the child objects of a control rig
		// depending on whether the USE_MOBILE_INPUT define is declared.
		
		// This define is set or unset by a menu item that is included with
		// the Cross Platform Input package.

		
		#if !UNITY_EDITOR
		void OnEnable()
		{
			CheckEnableControlRig();
		}
		#endif
		
		#if UNITY_EDITOR
		
		private void OnEnable()
		{
			EditorUserBuildSettings.activeBuildTargetChanged += Update;
			EditorApplication.update += Update;
			//SetMobileController ();

		}
		
		
		private void OnDisable()
		{
			EditorUserBuildSettings.activeBuildTargetChanged -= Update;
			EditorApplication.update -= Update;

			//SetMobileController ();

		}
		
		
		private void Update()
		{
			CheckEnableControlRig();
		}
		#endif
		
		
		private void CheckEnableControlRig()
		{
			#if MOBILE_INPUT
			EnableControlRig(true);
			#else
			EnableControlRig(true);
			#endif
		}
		
		
		private void EnableControlRig(bool enabled)
		{
			foreach (Transform t in transform)
			{
				t.gameObject.SetActive(enabled);
			}
		}

		public void SetMobileController(){
			Joystick.SetMobileController ();
		}

	}
}
