﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineObject : MonoBehaviour {

    private Material mat;
    [HideInInspector]
    public bool shaderOn = false;

    public bool enableOnAwake = false;

	// Use this for initialization
	void Start ()
    {
        mat = Resources.Load("OutlineEffect/Outline") as Material;
        if (mat == null)
            Debug.Log("Couldn't find the outline shader.");

        if (enableOnAwake)
            TurnOnOutline();
	}

    //Add the outline material.
    public void TurnOnOutline()
    {
        if(shaderOn == false )
        {
            shaderOn = true;
            Material[] mats = GetMaterials();
            Material[] mats2 = new Material[mats.Length + 1];
            for (int i = 0; i < mats.Length; ++i)
            {
                mats2[i] = mats[i];
            }
            mats2[mats.Length] = mat;
            SetMaterials(mats2);
        }
        
    }

    //Turn off the outline.
    public void TurnOffOutline()
    {
        if (shaderOn == true)
        {
            shaderOn = false;
            Material[] mats = GetMaterials();
            Material[] mats2 = new Material[mats.Length - 1];

            int j = 0;
            for (int i = 0; i < mats.Length; ++i)
            {
                if (mats[i].name.Contains("Outline") == false)
                {
                    mats2[j] = mats[i];
                    ++j;
                }
            }
            SetMaterials( mats2 );
        }
    }

    public Material[] GetMaterials()
    {
        if (GetComponent<MeshRenderer>())
            return GetComponent<MeshRenderer>().materials;
        else if( GetComponent<SkinnedMeshRenderer>())
            return GetComponent< SkinnedMeshRenderer > ().materials;
        Debug.LogError("No material on : " + transform.name);
        return null;
    }

    public void SetMaterials( Material[] _mats )
    {
        if (GetComponent<MeshRenderer>())
            GetComponent<MeshRenderer>().materials = _mats;
        else if (GetComponent<SkinnedMeshRenderer>())
            GetComponent<SkinnedMeshRenderer>().materials = _mats;
    }
}
