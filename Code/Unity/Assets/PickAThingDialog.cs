﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickAThingDialog : MonoBehaviour {

    public delegate void PickAThingCallback(int _i);

    public GameObject button;
    public float buttonGap = 40f;
    public float offset = -10f;

    int numThings;
    List<GameObject> buttons = new List<GameObject>();
    public PickAThingCallback callback = null;

    private void SetNumItems( int _i )
    {
        numThings = _i;
    }

    public void SetItems(string[] _strArray, PickAThingCallback _callback)
    {
        callback = _callback;
        SetNumItems(_strArray.Length);
        CreateItems();
        for (int i = 0; i < numThings; ++i)
        {
            buttons[i].GetComponentInChildren<Text>().text = _strArray[i];
            buttons[i].GetComponent<ButtonCallback>().param = i.ToString();
            buttons[i].GetComponent<ButtonCallback>().callback = ButtonClicked;
        }
    }

    void CreateItems()
    {
        for (int i = 0; i < numThings; ++i)
        {
            buttons.Add(GameObject.Instantiate(button, transform));
            buttons[i].transform.localPosition = new Vector3(0f, Utils.DistributeEqually(numThings, i, buttonGap) + offset, 0f);
        }
    }

    public void ButtonClicked( string _i )
    {
        if( callback != null )
        {
            callback(int.Parse(_i));
        }
    }
}

