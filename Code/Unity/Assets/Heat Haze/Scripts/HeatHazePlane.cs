using UnityEngine;
using System.Collections;

public class HeatHazePlane : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Camera cam = Camera.main;

		transform.position = cam.transform.position;
		//transform.Rotate (transform.rotation.x, cam.transform.rotation.eulerAngles.y, transform.rotation.z);

		transform.LookAt(transform.position + cam.transform.rotation * Vector3.down,
            cam.transform.rotation * Vector3.up);
	}
}
