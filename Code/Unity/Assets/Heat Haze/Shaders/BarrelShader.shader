Shader "Convex Games/BarrelShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Wrap ("Wrap factor (0 - 1)", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Wrap

		sampler2D _MainTex;
		float _Wrap;


		half4 LightingWrap (SurfaceOutput s, half3 lightDir, half atten) {
			half nl = dot(s.Normal, lightDir);
			half4 c;
			half nlWrap = (nl + _Wrap) / (1 + _Wrap);
			half diff = max(0, nlWrap);
						
			c.rgb = s.Albedo * _LightColor0.rgb * (diff * atten * 2);
			c.a = s.Alpha;
			return c;
		}
		
		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
