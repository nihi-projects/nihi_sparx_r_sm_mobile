// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Convex Games/HeatHaze" {
	Properties {
		_NormalTex("Normals (2D)", 2D) = "white" {}
		_Strength("Distort strength", Float) = 1.0
		_Speed("Distort speed", Float) = 3.0
		_EdgeFade("Edge fade (0-1)", Float) = 1.0
	}
	
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		
		Cull Off
    	Lighting Off
    	ZWrite Off
    
    	GrabPass { "_ScreenTex" }
    	
		Pass {
			CGPROGRAM
			#include "UnityCG.cginc"
			
			//#pragma exclude_renderers flash
			#pragma vertex vert
			#pragma fragment frag
	
			sampler2D _ScreenTex : register(s0);
			sampler2D _NormalTex : register(s1);
			
			float _Strength;
			float _Speed;
			float _EdgeFade;
	
			struct v2f {
				half4 pos: SV_POSITION;
				half4 screenPos: TEXCOORD0;
				half2 uvbump: TEXCOORD1;
			};
			
			v2f vert(appdata_full v) {
			
				v2f o;
				
				o.pos = UnityObjectToClipPos(v.vertex);
				o.screenPos = ComputeGrabScreenPos(o.pos);
				
				o.uvbump.xy = v.texcoord.xy;
				
				return o;
			}
			
			
			half4 frag(v2f i): COLOR {
				
				half2 uv1 = i.uvbump + _Time.x * _Speed;
				
				half2 edgeFactor = 1.0;
				
				half fadeStart = 1.0 - _EdgeFade;
				half fadeEnd = 1.0;
				
				
				edgeFactor.x = lerp(fadeStart, fadeEnd, i.uvbump.x) * lerp(fadeStart, fadeEnd, saturate(1-i.uvbump.x));
				edgeFactor.y = lerp(fadeStart, fadeEnd, i.uvbump.y) * lerp(fadeStart, fadeEnd, saturate(1-i.uvbump.y));
				
				half2 normal = tex2D(_NormalTex, uv1).rg * 2.0 - 1.0;
				
				half4 uv = i.screenPos + half4(normal * (edgeFactor.y * edgeFactor.x)*4.0, 0.0, 0.0) * _Strength;
				
				half4 finalColor = tex2Dproj(_ScreenTex, UNITY_PROJ_COORD(uv));
				
				return finalColor;
			}
			ENDCG

		}
	
	}
}
