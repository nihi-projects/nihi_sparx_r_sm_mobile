﻿Shader "Liberty Coding/Lava" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_DepthTex("Height (RGB)", 2D) = "white" {}
		_Power("Power", float) = 1
		[MaterialToggle]_ScaleFromOrigin("Scale height from center", Float) = 0
		[MaterialToggle]_ScrollXAxis("Scroll X Axis", Float) = 0
		_SpeedX("Scroll Speed X", float) = 1
		[MaterialToggle]_ScrollYAxis("Scroll Y Axis", Float) = 0
		_SpeedY("Scroll Speed Y", float) = 1
		[MaterialToggle]_Fog("Enable Fog", Float) = 0
	}
		SubShader{
			Pass {
			ZWrite On
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_fog
			#pragma glsl
			#include "UnityCG.cginc"

			struct PS_INPUT
			{
			  float4 Position : SV_POSITION;
			  float4 UV : TEXCOORD0;
			  UNITY_FOG_COORDS(1)
			};

			sampler2D _MainTex;
			sampler2D _DepthTex;
			float _Power;
			float _SpeedX;
			float _SpeedY;
			bool _ScrollXAxis;
			bool _ScrollYAxis;
			bool _Fog;
			bool _ScaleFromOrigin;

			PS_INPUT vert(appdata_base v) 
			{
				PS_INPUT output;

				output.UV = v.texcoord;

				if(_ScrollXAxis)
					output.UV.x += (_Time.x * _SpeedX); 
				if(_ScrollYAxis)
					output.UV.y += (_Time.x * _SpeedY);

				//load the data from the depthmap.
				float3 offset = tex2Dlod(_DepthTex, output.UV);

				//normalize vertex scale by offset
				float3 displacement = v.normal * offset * _Power;

				if(_ScaleFromOrigin)
					displacement.y -= (_Power * 0.5);

				// Multiply each model vertex by the world, view, projection matrix.
				float4 posWVP = mul(unity_ObjectToWorld, float4(v.vertex.xyz + displacement , 1));
				posWVP = mul(UNITY_MATRIX_V, posWVP);
				posWVP = mul(UNITY_MATRIX_P, posWVP);

				output.Position = posWVP;
				if (_Fog)
				{
					UNITY_TRANSFER_FOG(output, output.Position);
				}
				return output;
			}

			fixed4 frag(PS_INPUT i) : COLOR 
			{
				fixed4 c = tex2D(_MainTex, i.UV.xy);
				if (_Fog)
				{
					UNITY_APPLY_FOG(i.fogCoord, c);
				}
				return c;
			}

			ENDCG
			}
		}
			FallBack "Diffuse"
}