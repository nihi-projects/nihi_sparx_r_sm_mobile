﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DemoCameraSelector : MonoBehaviour
{
    public Camera[] cameras;
    private int index = 0;

    public RawImage diff;
    public RawImage heig;

    public Text power;
    public Text speed;
    public Text meshName;

    private void Awake()
    {
        for (int i = 0; i < cameras.Length; i++)
            cameras[i].gameObject.SetActive(false);
        cameras[index].gameObject.SetActive(true);
        GetData();
    }

    public void Next()
    {
        cameras[index].gameObject.SetActive(false);
        index++;
        if (index >= cameras.Length) index = 0;
        cameras[index].gameObject.SetActive(true);
        GetData();
    }

    public void Previous()
    {
        cameras[index].gameObject.SetActive(false);
        index--;
        if (index < 0) index = cameras.Length - 1;
        cameras[index].gameObject.SetActive(true);
        GetData();
    }

    private void GetData()
    {
        var cam = cameras[index];
        var ray = new Ray(cam.transform.position, cam.transform.forward.normalized);
        RaycastHit info;
        if (!Physics.Raycast(ray, out info)) return;

        var renderer = info.rigidbody.gameObject.GetComponent<Renderer>();
        diff.texture = renderer.material.GetTexture("_MainTex");
        heig.texture = renderer.material.GetTexture("_DepthTex");
        power.text = renderer.material.GetFloat("_Power").ToString("F3");
        var speeds = new Vector2(renderer.material.GetFloat("_SpeedX"), renderer.material.GetFloat("_SpeedY"));
        speed.text = speeds.ToString("F3");
        meshName.text = info.rigidbody.gameObject.name;
    }
}