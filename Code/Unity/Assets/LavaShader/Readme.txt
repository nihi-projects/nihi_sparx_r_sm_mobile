Liberty Coding Lava Shader:

How to use:
1.	Create a new material. 
2.	Change the materials shader to the "Liberty Coding/Lava" shader.
3.	Apply the material to a mesh renderer. 
4.	Apply a custom diffuse and a custom height map.