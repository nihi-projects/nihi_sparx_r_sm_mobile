﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImproveLightQuality : MonoBehaviour {

    public Color startColour;
    public Color endColour;

    public float startIntensity;
    public float endIntensity;

    private Light light = null;

	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {

        float t = (Global.CurrentLevelNumber-1) / 6;
        if (t > 0f)
        {
            light.color = Color.Lerp(startColour, endColour, t);
            light.intensity = (endIntensity - startIntensity) * t + startIntensity;
        }
	}
}
