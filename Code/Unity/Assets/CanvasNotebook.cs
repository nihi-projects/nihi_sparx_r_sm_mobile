﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasNotebook : MonoBehaviour {

    //public Text ChangeText;
    public Text[] ChangeableText;
    public InputField[] fields;
    public Image[] images;

    public void SetImage( Texture2D _tex, int _i = 0)
    {
        images[_i].sprite = Sprite.Create(_tex, new Rect(new Vector2(0f, 0f), new Vector2(256f, 256f)), new Vector2(0.5f, 0.5f));
    }

    public void SetChangeText( string _str, int _index = 0 )
    {
        if( _index < ChangeableText.Length)
            ChangeableText[_index].text = _str;
    }

    public string GetEnterText( int _index )
    {
        return fields[_index].text;
    }

    public static string ArrayToStringList( string[] _str )
    {
        string finalString = "";

        for( int i = 0; i < _str.Length; ++i )
        {
            if( i != 0 )
            {
                finalString += "\n";
            }
            finalString += _str[i];
        }

        return finalString;
    }

    public void DisableInputFields()
    {
        InputField[] fields = GetComponentsInChildren<InputField>();
        foreach( InputField field in fields )
        {
            field.interactable = false;
        }
    }

    public void SetInputField( string _str, int _i )
    {
        fields[_i].text = _str;
    }
}
