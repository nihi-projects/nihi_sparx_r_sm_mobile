﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonListCode
{
    public delegate string GetDescriptionDelegate(string _str);

    public static GameObject LoadMenu( GameObject _menu, string _title, string[] _keywords, GetDescriptionDelegate _del )
    {
        if (_menu == null)
        {
            _menu = TextDisplayCanvas.instance.ShowPrefab("ListScreen");
            _menu.GetComponent<ListScreen>().SetupListItems(_keywords);
            _menu.GetComponent<ListScreen>().SetTitle(_title);

            string[] descriptions = new string[_keywords.Length];
            for (int i = 0; i < _keywords.Length; ++i)
            {
                descriptions[i] = _del(_keywords[i]);
            }
            _menu.GetComponent<ListScreen>().SetupDescriptions(descriptions);
        }
        return _menu;
    }
    
}
