﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
public class JumpButton : MonoBehaviour, IPointerDownHandler {

    RectTransform rt = null;

	// Use this for initialization
	void Start () {
        rt = GetComponent<RectTransform>();
    }

    public void OnPointerDown(PointerEventData data)
    {
        Debug.Log("Jumping");
        Global.GetPlayer().GetComponent<PlayerMovement>().Jump();
    }
}
