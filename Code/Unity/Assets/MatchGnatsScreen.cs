﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchGnatsScreen : MonoBehaviour
{
    public RectTransform[]  snapPoints;
    public DragObject[]     dragAndDrops;
    public Text[]           descriptions;

    public Button           nextButton;

    public float snapDistance = 30f * 30f;

    public bool isFinished = false;
    public bool overrideFinished = false;

	// Use this for initialization
	void Start () {
		for( int i = 0; i < dragAndDrops.Length; ++i )
        {
            dragAndDrops[i].callback = SnapThing;
        }

        nextButton.gameObject.SetActive(false);

        ShufflePositions();
    }

    //Shuffle the position of all the words.
    public void ShufflePositions()
    {
        for( int i = 0; i < 40; ++i )
        {
            int rand1 = Random.Range(0, dragAndDrops.Length);
            int rand2 = Random.Range(0, dragAndDrops.Length);

            Vector3 pos = dragAndDrops[rand1].transform.position;
            dragAndDrops[rand1].transform.position = dragAndDrops[rand2].transform.position;
            dragAndDrops[rand2].transform.position = pos;
        }
    }

    public void SetText( string[] _descriptions, string[] _dragableItems )
    {
        for( int i = 0; i < descriptions.Length; ++i )
        {
            descriptions[i].text = _descriptions[i];
        }

        for( int i = 0; i < dragAndDrops.Length; ++i )
        {
            dragAndDrops[i].GetComponent<Text>().text = _dragableItems[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isFinished == false )
        {
            isFinished = true;
            for (int i = 0; i < dragAndDrops.Length; ++i )
            {
                if(dragAndDrops[i].dragable == true)
                {
                    isFinished = false;
                    break;
                }
            }
        }

        if( Debug.isDebugBuild == true && Input.GetKeyDown(KeyCode.R) )
        {
            overrideFinished = true;
        }

        if (overrideFinished == true)
            isFinished = true;

        nextButton.gameObject.SetActive(isFinished);
    }

    public void SnapThing( Transform _trans )
    {
        float minDistance = snapDistance;
        int snapPointIndex = -1;

        for (int i = 0; i < snapPoints.Length; ++i)
        {
            Vector3 distance = snapPoints[i].transform.position - _trans.GetComponent<RectTransform>().transform.position;
            if (distance.sqrMagnitude < minDistance)
            {
                minDistance = distance.sqrMagnitude;
                snapPointIndex = i;
            }
        }
        
        if(snapPointIndex != -1 && snapPointIndex == IndexOf( _trans ) )
        {
            _trans.GetComponent<RectTransform>().transform.position = snapPoints[snapPointIndex].transform.position;
            _trans.GetComponent<DragObject>().dragable = false;
        }
    }
	
    public int IndexOf( Transform _trans )
    {
        for (int i = 0; i < dragAndDrops.Length; ++i )
        {
            if (dragAndDrops[i].transform == _trans)
                return i;
        }
        return -1;
    }

    
}

