﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Reel
{
    public string[] words;
    public int rightWord;
    public int startWord;
    public int currentWord;
    public Text wordText;

    public void IncreaseWord()
    {
        ++currentWord;
        if (currentWord >= words.Length)
            currentWord = 0;
    }

    public void DecreaseWord()
    {
        --currentWord;
        if (currentWord < 0)
            currentWord = words.Length - 1;
    }

    public void RefreshWord()
    {
        wordText.text = words[currentWord];
    }

    public bool IsCorrect()
    {
        return currentWord == rightWord;
    }
}

public class ThreeWordGame : MonoBehaviour
{
    public Reel[] reels;
    public float solveLag = 1.5f;

    private float solveCounter=0f;
    private bool animationPlaying = false;
    private bool animationDone = false;

    public void Start()
    {
        for( int i = 0; i < reels.Length; ++i )
        {
            reels[i].currentWord = reels[i].startWord;
        }
    }

    public void Update()
    {
        foreach( Reel r in reels )
        {
            r.RefreshWord();
        }

        if( Solved() )
        {
            if(animationPlaying == false )
            {
                animationPlaying = true;
                GetComponent<Animator>().Play("ThreePuzzleSolved");
            }
            solveCounter += Time.deltaTime;
            if (solveCounter >= solveLag)
                animationDone = true;
        }
    }

    public void UpClicked(int _i)
    {
        reels[_i].IncreaseWord();
    }

    public void DownClicked(int _i)
    {
        reels[_i].DecreaseWord();
    }

    private bool Solved()
    {
        for (int i = 0; i < reels.Length; ++i)
        {
            if (reels[i].IsCorrect() == false)
                return false;
        }
        return true;
    }

    public bool IsDone()
    {
        if (animationDone == false || Solved() == false )
            return false;

        gameObject.SetActive(false);
        return true;
    }
}
