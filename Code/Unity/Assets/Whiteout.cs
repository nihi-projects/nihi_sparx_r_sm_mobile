﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Whiteout : MonoBehaviour
{
    public delegate void FadeFinishDelegate(FadeState state);

    public enum FadeState
    {
        FADE_IN,
        NO_FADE,
        FADE_OUT
    };

    public float fadeSpeed = 10f;
    public FadeFinishDelegate del = null;
    public FadeState fadeState = FadeState.NO_FADE;

    private Image myImage;
    private float alpha = 0f;

	// Use this for initialization
	void Start ()
    {
        myImage = GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(fadeState == FadeState.FADE_IN)
        {
            alpha += Time.deltaTime * fadeSpeed;
            if (alpha > 1.0f)
            {
                alpha = Mathf.Min(1.0f, alpha);
                fadeState = FadeState.NO_FADE;
                if (del != null )
                    del(fadeState);
            }
        }
        
        if( fadeState == FadeState.FADE_OUT )
        {
            alpha -= Time.deltaTime * fadeSpeed;
            if (alpha < 0f)
            {
                alpha = Mathf.Max(0f, alpha);
                fadeState = FadeState.NO_FADE;
                if (del != null)
                    del(fadeState);
            }
        }
        
        Color col = myImage.color;
        col.a = alpha;
        myImage.color = col;
    }
}
