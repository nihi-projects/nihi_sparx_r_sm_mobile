﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileControlEnabler : MonoBehaviour {

    public Image moveImage;
    public Image jumpImage;
	
	// Update is called once per frame
	void Update () {
        if (Global.GetPlayer() != null)
        {
            if (Global.GetPlayer().GetComponent<PlayerMovement>().GetMovement() == true)
            {
                moveImage.enabled = true;
                jumpImage.enabled = true;
            }
            else
            {
                moveImage.enabled = false;
                jumpImage.enabled = false;
            }
        }
    }
}
