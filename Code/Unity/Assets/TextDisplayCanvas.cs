﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplayCanvas : MonoBehaviour {

    [HideInInspector]
    public static TextDisplayCanvas instance = null;

    [Tooltip("This is the statement text box. Should work on all resolutions")]
    public StatementBox     statementBox;
    [Tooltip("This is the LARGE statement box")]
    public StatementBox     largeStatementBox;
    [Tooltip("This is a multi-choice dialogue box")]
    public MultiChoiceBox   multiChoiceBox;
    [Tooltip("This is a tool tip, it appears when the game wants to tell you something specific.")]
    public TooltipBox       toolTipBox;

    public Text             title;

    private bool    textShowingPrivate = false;
    public bool     textShowing {
        get {
            return textShowingPrivate; }
        set {
            textShowingPrivate = value; }
    }

    private string lastText = "";
    private StatementBox box;

    // Use this for initialization
    void Start()
    {
        if( instance == null )
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void HideDialogue()
    {
        statementBox.gameObject.SetActive(false);
        multiChoiceBox.gameObject.SetActive(false);
        largeStatementBox.gameObject.SetActive(false);
    }

    public void HideMultiChoice()
    {
        multiChoiceBox.gameObject.SetActive(false);
    }

    public void HideStatement()
    {
        statementBox.gameObject.SetActive(false);
        largeStatementBox.gameObject.SetActive(false);
    }

    //Tooltip.
    public void ShowTooltip( string _tooltip )
    {
        toolTipBox.gameObject.SetActive(true);
        toolTipBox.SetText(_tooltip);
    }

    public void ShowTitle( string _str )
    {
        title.gameObject.SetActive(true);
        title.text = _str;
    }

    //Show's a prefab from the resources 
    public GameObject ShowPrefab( string _strName )
    {
        GameObject obj = Resources.Load("UI/" + _strName) as GameObject;
        if( obj )
        {
            obj = GameObject.Instantiate(obj, transform, false);
        }
        else
        {
            Debug.LogError("Couldn't find " + _strName);
        }

        return obj;
    }

    public StatementBox ShowStatementBox(   string _characterName,
                                            string _strText, 
                                            Color _col,
                                            bool _prevButton, bool _nextButton )
    {
        if(lastText != _strText)
        {
            HideStatement();
        }

        lastText = _strText;

        box = GetStatementBox(_strText);

        box.SetStatementData(_characterName, _strText, _col);

        box.ShowPreviousButton(_prevButton);
        box.ShowNextButton(_nextButton);

        return box;
    }

    private StatementBox GetStatementBox( string _strText )
    {
        StatementBox finalBox = statementBox;
        if (_strText.Split('\n') != null)
        {
            int length = _strText.Split('\n').Length;
            if (length > 4)
            {
                finalBox = largeStatementBox;
            }
        }
        return finalBox;
    }
}
