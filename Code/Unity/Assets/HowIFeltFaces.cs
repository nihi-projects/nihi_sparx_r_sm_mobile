﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowIFeltFaces : MonoBehaviour
{ 
    public enum FaceType
    {
        FACE_TYPE_INVALID = -1,
        FACE_TYPE_MIN,
        HAPPY = 0,
        NEUTRAL,
        SAD,
        FACE_TYPE_MAX
    };

    public Image happyFace;
    public Image neutralFace;
    public Image sadFace;

    public Sprite[] faceSprites;

    private Image[]     faces;
    private FaceType activeFace = FaceType.FACE_TYPE_INVALID;

    public FaceType GetActiveFace()
    {
        return activeFace;
    }

	// Use this for initialization
	void Start () {
        faces = new Image[] { happyFace, neutralFace, sadFace };
	}
    
    // Turns the selected face on or off.
    void SwitchFace( FaceType _faceType, bool _onOrOff)
    {
        int faceSpriteIndex = (int)(_faceType)*2;
        if (_onOrOff == true)
        {
            faceSpriteIndex++;
        }

        faces[(int)(_faceType)].sprite = faceSprites[faceSpriteIndex];
    }

    //Switches to the specified face.
    void SwitchToFace( FaceType _faceType )
    {
        for(FaceType i = FaceType.FACE_TYPE_MIN; i < FaceType.FACE_TYPE_MAX; ++i )
        {
            SwitchFace(i, false);
        }
        SwitchFace(_faceType, true);
        activeFace = _faceType;
    }

    public void onHappyClicked()
    {
        SwitchToFace(FaceType.HAPPY);
    }

    public void onNeutralClicked()
    {
        SwitchToFace(FaceType.NEUTRAL);
    }

    public void onSadClicked()
    {
        SwitchToFace(FaceType.SAD);
    }

}
