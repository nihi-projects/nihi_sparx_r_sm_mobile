﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightHolder : MonoBehaviour {

    public GameObject[] lights;

    public void EnableLights()
    {
        foreach( GameObject obj in lights )
        {
            obj.SetActive(true);
        }
    }
}
