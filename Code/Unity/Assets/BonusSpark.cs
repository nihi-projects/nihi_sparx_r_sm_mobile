﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusSpark : MonoBehaviour {

    public static int[] bonusSparkNums = new int[] 
    {
        0, 0, 0, 0, 0, 0, 0, 0
    };

    public static int[] maxLevelSparks = new int[]
    {
        0, 0, 0, 0, 0, 0, 0, 0 
    };

    public static int numLevelSparx;
    public static Text sparxText;
    public static Image img;
    public string myID;
    private TokenHolderLegacy instanceTHL;


    private void Awake()
    {
        numLevelSparx = 0;
        instanceTHL = GameObject.Find("TokenHolder").GetComponent<TokenHolderLegacy>();

    }

    private void Start()
    {
        myID = instanceTHL.username + "_" + Global.CurrentLevelNumber.ToString() + "_" + maxLevelSparks[Global.CurrentLevelNumber];
        int collected = PlayerPrefs.GetInt(myID, 0);
        if (collected == 1)
            OnTriggerEnter(null);

        maxLevelSparks[Global.CurrentLevelNumber]++;

        RefreshSparxLevels();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerPrefs.SetInt(myID, 1);

        ++numLevelSparx;
        bonusSparkNums[Global.CurrentLevelNumber] = numLevelSparx;

        Debug.Log("Bonus Spark Trigger");
        Destroy(gameObject);

        //Check to see if we have them all.
        if(bonusSparkNums[Global.CurrentLevelNumber] == maxLevelSparks[Global.CurrentLevelNumber] )
        {
            PlayerPrefs.SetInt(instanceTHL.username + "_" + "Level" + Global.CurrentLevelNumber + "BonusSparx", 1);
        }

        RefreshSparxLevels();
    }

    public static void RefreshSparxLevels()
    {
        if (sparxText == null)
        {
            sparxText = GameObject.Find("BonusSparxText").GetComponent<Text>();
            sparxText.enabled = true;
            img = GameObject.Find("BonusSparxUI").GetComponent<Image>();
            img.enabled = true;
        }
            
        sparxText.text = numLevelSparx + "/" + maxLevelSparks[Global.CurrentLevelNumber];
    }
}

