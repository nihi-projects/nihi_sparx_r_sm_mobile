using UnityEngine;
using System.Collections;

public class FrameCount : MonoBehaviour {
	private static FrameCount instanceRef;
	
	// Use this for initialization
float updateInterval = 0.5f;
 
private float accum = 0.0f; // FPS accumulated over the interval
private int frames = 0; // Frames drawn over the interval
private float timeleft; // Left time for current interval
 
	void Start()
	{
	    if( !GetComponent<GUIText>() )
	    {
	        print ("FramesPerSecond needs a GUIText component!");
	        enabled = false;
	        return;
	    }
	    timeleft = updateInterval;  
	}
	 
	void Update()
	{
	    timeleft -= Time.deltaTime;
	    accum += Time.timeScale/Time.deltaTime;
	    ++frames;
	 
	    // Interval ended - update GUI text and start new interval
	    if( timeleft <= 0.0 )
	    {
			if(accum/frames < 30.0f)
			{
				GetComponent<GUIText>().material.color = Color.red;
			}
			else
			{
				GetComponent<GUIText>().material.color = Color.black;
			}
	        // display two fractional digits (f2 format)
	        GetComponent<GUIText>().text = "FPS: " + (accum/frames).ToString("f2");
	        timeleft = updateInterval;
	        accum = 0.0f;
	        frames = 0;
	    }
	}
	void Awake()
	{
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}
	}
}