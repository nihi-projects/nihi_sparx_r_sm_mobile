﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProvinceScript : MonoBehaviour {

    public GameObject pinPrefab;
    public Vector3 pinOffset;
    public string level;
    public bool canTravelHere = true;

    private int levelID;
    private GameObject pin;
    private Vector3 pinStartPos;
    private bool pinClicked = false;

    private CapturedDataInput dataInput;

    // Use this for initialization
    void Start () {
        pin = GameObject.Instantiate(pinPrefab,transform,false);
        pinStartPos = pin.transform.position;
        pin.transform.SetAsFirstSibling();
        pin.GetComponent<Button>().interactable = false;

        dataInput = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
    }

    public void EnableInteraction( int _level )
    {
        levelID = _level;

        if(canTravelHere)
        {
            pin.GetComponent<Button>().interactable = true;
            pin.GetComponent<Button>().onClick.AddListener(PinClicked);
        }
    }

    public void PinClicked()
    {
        if(level != "")
        {
            LogoBackgroundScreen.levelSelectOverride = levelID;
            LogoBackgroundScreen lbs = GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>();
            if( lbs != null )
            {
                //dataInput.unLoadAllContent();
                //StartCoroutine(dataInput.LoginProcess_NewJsonSchema("testfish5", "password"));
                LogoBackgroundScreen.levelSelectOverride = levelID;
                LogoBackgroundScreen.m_levelOverride = true;

                lbs.ResetMemberVariables();

                lbs.enabled = true;
                pinClicked = true;

                Global.m_bGotCurrentLevelGem = false;
                Global.GemObtainedForTheLevel = false;

                dataInput.m_LevelDataGetRetriever = null;
                dataInput.m_ProcessDataGetRetriever = null;
            }
            else
            {
                Debug.LogWarning("Couldn't find the logo background screen to load a new level.");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        pin.transform.position = pinStartPos + pinOffset;
        if(dataInput != null && pinClicked == true)
        {
            if (dataInput.m_LevelDataGetRetriever != null && dataInput.m_LevelDataGetRetriever.isDone &&
                dataInput.m_ProcessDataGetRetriever != null && dataInput.m_ProcessDataGetRetriever.isDone)
            {
                RecorrectTheGUI();

                Application.LoadLevel(level);
                GameObject.Find("GUI").GetComponent<LogoBackgroundScreen>().enabled = false;
                pinClicked = false;
            }
        }
    }

    public void RecorrectTheGUI()
    {
        for (int i = 0; i < Global.GUIObjects.Length; ++i)
        {
            if( Global.GUIObjects == null )
            {
                Debug.LogError("No GUIObjects.");
            }

            if (Global.GUIObjects[i] != null)
            {
                Global.GUIObjects[i].SetActive(false);
                if (i == levelID - 1)
                {
                    Global.GUIObjects[i].SetActive(true);
                }
            }
            else
                Debug.LogWarning("Could not find GUIObjects[i] where i is: " + i);
        }
    }
}
