﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ECustomisationButton
{
    HAIR_STYLE,
    HAIR_COLOUR,
    SKIN_COLOUR,
    EYE_COLOUR,
    CLOTHING_COLOUR,
    TRIM,
    TRIM_COLOUR,
    BACKPACK,
    CONFIRM,
    CANCEL,
    LEFT_SPIN,
    RIGHT_SPIN,
    CLOTHING
};

public class CharacterCreation : MonoBehaviour
{
    //Buttons for changing the colour.
    public Button hairStyle;
    public Button hairColour;
    public Button skinColour;
    public Button eyeColour;

    public Button clothingColour;
    public Button trim;
    public Button trimColour;
    public Button backpack;

    public Button confirm;
    public Button cancel;

    public Button leftSpin;
    public Button rightSpin;

    public Button clothing;

    private Button[] customisationButtons;
    private bool[] wasClicked;

    // Use this for initialization
    void Start()
    {
        //Sets up the customisation buttons.
        customisationButtons = new Button[] {
            hairStyle,
            hairColour,
            skinColour,
            eyeColour,
            clothingColour,
            trim,
            trimColour,
            backpack,
            confirm,
            cancel,
            leftSpin,
            rightSpin,
            clothing
        };

        wasClicked = new bool[customisationButtons.Length];

        //Set up the callbacks for the buttons.
        for( int i = 0; i < customisationButtons.Length; ++i )
        {
            customisationButtons[i].GetComponent<ButtonCallback>().param    = i.ToString();
            customisationButtons[i].GetComponent<ButtonCallback>().callback = ButtonCallback;
        }
    }

    public void ButtonCallback(string _str)
    {
        wasClicked[System.Convert.ToInt32(_str)] = true;
    }

    //Returns true if the button was clicked.
    public bool WasClicked(ECustomisationButton _button)
    {
        bool clicked = wasClicked[(int)_button];
        if (clicked == true )
        {
            wasClicked[(int)_button] = false;
        }
        return clicked;
    }
}
