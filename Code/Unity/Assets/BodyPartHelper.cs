﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyData
{
    public int  hairStyle;
    public Color hairColour;
    public Color skinColour;
    public Color clothColour;
    public Color eyeColour;
    public Color trimColour;
    public int backPackNumber = -1;
    public int bodyType = 0;

    public Texture2D trimTexture = null;

    //Constructor for body data sets everything to global values.
    public BodyData()
    {
        SetDataToGlobal();
    }

    //Uses the global values to create a body data struct.
    public void SetDataToGlobal()
    {
        //Colours for everything.
        skinColour  = Global.userSkinColor;
        hairColour  = Global.userHairColor;
        clothColour = Global.userClothColor;
        eyeColour   = Global.userEyeColor;
        trimColour  = Global.userTrimColor;
        hairStyle   = Global.userHairStyleNum;

        //Backpack.
        backPackNumber = Global.userBackPackNum;

        //Body.
        bodyType = Global.userBodyType;

        //Trim texture.
        if (Global.CurrentPlayerName == "Girl")
            trimTexture = GameObject.Find("GUI").GetComponent<GirlCustomisation>().GetTexture(Global.userTrimTextureNum);
        else if (Global.CurrentPlayerName == "Boy")
            trimTexture = GameObject.Find("GUI").GetComponent<BoyCustomisation>().GetTexture(Global.userTrimTextureNum);
    }
}

public class BodyPartHelper : MonoBehaviour {

    [HideInInspector]
    public Material bodyMaterial;
    public int trimMaterialNumber;
    public int hairMaterialNumber;
    public int eyeMaterialNumber;
    [HideInInspector]
    public Material trimMaterial;
    [HideInInspector]
    public Material clothMaterial;
    [HideInInspector]
    public Material hairMaterial;
    [HideInInspector]
    public Material eyeMaterial;
    [HideInInspector]
    public Transform body;
    [HideInInspector]
    public Transform head;
    [HideInInspector]
    public Transform hair1;
    [HideInInspector]
    public Transform bag;

    public string characterName;

    public static string[] bodyObjects = new string[] { "_body_dress", "_body_pants" };

    public bool setBodyDataOnRefresh = false;

    // Use this for initialization
    void Awake ()
    {
        RefreshVariables();
	}

    public void OnEnable()
    {
        RefreshVariables();
    }

    public void RefreshVariables()
    {
        SetPants( Global.userBodyType );
        
        //Hair n stuff.
        head = transform.Find(characterName + "_head");
        hair1 = transform.Find(characterName + "_hair_1");
        
        if (body.GetComponent<Renderer>())
        {
            hairMaterial = head.GetComponent<Renderer>().materials[hairMaterialNumber];
            eyeMaterial = head.GetComponent<Renderer>().materials[eyeMaterialNumber];
        }
        else
        {
            Debug.LogError("No renderer on the body.");
        } 

        if (bodyMaterial == null)
            Debug.LogError("Character: " + gameObject.name + " does not have a face material");

        if (setBodyDataOnRefresh == true)
            SetCharacterUsingBodyData();
    }

    public Transform GetBag( int _i )
    {
        return transform.Find( characterName + "_bag_" + _i.ToString() );
    }

    public Material GetMaterial( Transform _trans, string _name )
    {
        Material[] mats = body.GetComponent<Renderer>().materials;
        for( int i = 0; i < mats.Length; ++ i )
        {
            if (mats[i].name.Contains(characterName + _name) )
                return mats[i];
        }
        return null;
    }

    //Sets the player's hair colour.
    public void SetHair( Color _color, int _style = -1)
    {
        int hairIndex = 1;
        while(true)
        {
            Transform hairObj = transform.Find(characterName + "_hair_" + hairIndex.ToString());
            if( hairObj == null )
            {
                break; //No hair, no good.
            }
            else
            {
                //Hair found. Change its colour.
                hairObj.GetComponent<SkinnedMeshRenderer>().material.color = _color;

                if(_style != -1 )
                {
                    if (_style == hairIndex)
                        hairObj.GetComponent<Renderer>().enabled = true;
                    else
                        hairObj.GetComponent<Renderer>().enabled = false;
                }
               
            }

            ++hairIndex;
        }
    }

    //Sets the hair style
    public void SetHairStyle( int _style = -1)
    {
        if (_style == -1)
            _style = Global.userHairStyleNum;

        int hairIndex = 1;
        while (true)
        {
            Transform hairObj = transform.Find(characterName + "_hair_" + hairIndex.ToString());
            if (hairObj == null)
            {
                break; //No hair, no good.
            }
            else
            {
                if (_style != -1)
                {
                    if (_style == hairIndex)
                        hairObj.GetComponent<Renderer>().enabled = true;
                    else
                        hairObj.GetComponent<Renderer>().enabled = false;
                }

            }
            ++hairIndex;
        }
    }

    public void SetSkin( Color _color )
    {
        GetMaterial(body, "_face").SetColor("_Color", _color);
        head.GetComponent<Renderer>().materials[0].SetColor("_Color", _color);
    }

    public Transform GetHair( int _hairStyle )
    {
        return transform.Find(characterName + "_hair_" + _hairStyle);
    }

    //Sets the character to a specific body data. If no body data is provided, one will be created using the default values.
    public void SetCharacterUsingBodyData( BodyData _data = null  )
    {
        if (_data == null)
            _data = new BodyData();

        //Hair color
        SetHair(_data.hairColour, _data.hairStyle);
        //Skin color
        Renderer bodyRenderer = body.GetComponent<Renderer>();
        bodyMaterial.SetColor("_Color", _data.skinColour);
        head.GetComponent<Renderer>().materials[0].SetColor("_Color", _data.skinColour);
        //Eye color
        eyeMaterial.SetColor("_Color", _data.eyeColour);
        //Cloth color
        clothMaterial.SetColor("_Color", _data.clothColour);
        //Trim Stuff.
        for (int i = 0; i < bodyRenderer.materials.Length; ++i)
        {
            if (bodyRenderer.materials[i].name.Contains("trim"))
            {
                bodyRenderer.materials[i].SetTexture("_MainTex", _data.trimTexture);
                bodyRenderer.materials[i].SetColor("_Color", _data.trimColour);
            }
        }
        
        //Backpack number
        if (Global.userBackPackNum == 1)
        {
            transform.Find(characterName + "_bag_1").GetComponent<SkinnedMeshRenderer>().enabled = true;
            transform.Find(characterName + "_bag_2").GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
        else
        {
            transform.Find(characterName + "_bag_1").GetComponent<SkinnedMeshRenderer>().enabled = false;
            transform.Find(characterName + "_bag_2").GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
        
        if (Global.CurrentPlayerName.ToLower() != characterName)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetPants( int _pantsNum )
    {
        Global.userBodyType = _pantsNum;

        //Get the right body type thing.
        for (int i = 0; i < bodyObjects.Length; ++i)
        {
            Transform trans = transform.Find(characterName + bodyObjects[i]);
            if (i == Global.userBodyType)
            {
                trans.GetComponent<SkinnedMeshRenderer>().enabled = true;
            }
            else
            {
                trans.GetComponent<SkinnedMeshRenderer>().enabled = false;
            }
        }
        
        body = transform.Find(characterName + bodyObjects[Global.userBodyType]);
        
        clothMaterial = GetMaterial(body, "_body");
        bodyMaterial = GetMaterial(body, "_face");
        trimMaterial = GetMaterial(body, "_trim");

        clothMaterial.SetColor("_Color", Global.userClothColor);

    }
}
