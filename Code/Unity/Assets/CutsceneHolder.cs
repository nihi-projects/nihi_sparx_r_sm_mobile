﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneHolder : MonoBehaviour {

    public static CutsceneHolder instance = null;

    public GameObject gemPlacementCutscene;

	// Use this for initialization
	void Start () {
		if( instance == null )
        {
            instance = this;
        }
	}
}
