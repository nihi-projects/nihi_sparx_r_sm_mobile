﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeHomeDialog : MonoBehaviour {

    public ButtonCallback[] takeHomeObjects;
    public Button nextButton;
    public Text description;

    private string[] descriptions;
    private GameObject selected;
    private int selectedIndex = -1;

	// Use this for initialization
	void Start ()
    {
		for( int i = 0; i < takeHomeObjects.Length; ++i )
        {

            takeHomeObjects[i].param    = i.ToString();
            takeHomeObjects[i].callback = ButtonCallback;
        }
	}

    public void ButtonCallback(string _str)
    {
        selectedIndex = int.Parse(_str);
        selected = takeHomeObjects[ selectedIndex ].gameObject;
    } 

    public void SetDescriptions( string[] _str )
    {
        descriptions = _str;
    }

	// Update is called once per frame
	void Update ()
    {
		for( int i = 0; i < takeHomeObjects.Length; ++i )
        {
            takeHomeObjects[i].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        if (selected != null)
        {
            selected.gameObject.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            description.text = descriptions[selectedIndex];
        }
        nextButton.gameObject.SetActive( selected != null );
    }

    public int SelectedIndex()
    {
        return selectedIndex;
    }
}
