﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldMenu : MonoBehaviour {

    public delegate void ShieldCallback( int _i );

    public Text[] shieldText;

    public GameObject[] words;
    public GameObject[] colouredWords;

    public List<string> descriptions = new List<string>();

    public GameObject   descriptionBox;
    public GameObject   nextButton;

    public bool         wordsClickable = false;
    private bool        nextClicked = false;

    public ShieldCallback callback;

    public void Awake()
    {
        descriptionBox.SetActive(false);

        for(int i = 0; i < words.Length; ++i)
        {
            words[i].gameObject.SetActive(false);
        }
    }

    public void Start()
    {
        for (int i = 0; i < colouredWords.Length; ++i)
        {
            colouredWords[i].GetComponent<Button>().interactable = wordsClickable;
            colouredWords[i].GetComponent<ButtonCallback>().param = i.ToString();
            colouredWords[i].GetComponent<ButtonCallback>().callback = OnWordClicked;
        }
    }

    public void OnWordClicked( string _param )
    {
        if(callback != null)
        {
            callback(int.Parse(_param));
        }
    }

    public void Update()
    {
        if(wordsClickable == false)
            nextButton.SetActive(IsFinished());

        int dragIndex = GetDraggingIndex();
        if ( dragIndex != - 1 )
        {
            SetDescription(descriptions[dragIndex]);
        }
    }

    public void AddWord( string _str, string _description )
    {
        foreach( GameObject word in words )
        {
            if(word != null && word.activeSelf == false )
            {
                word.SetActive(true);
                word.GetComponentInChildren<Text>().text = _str;

                descriptions.Add( _description );

                break;
            }
        }
    }

    public bool IsDragging()
    {
        foreach(GameObject word in words)
        {
            if(word != null && word.GetComponent<DragObject>().dragging )
            {
                return true;
            }
        }
        return false;
    }

    public int GetDraggingIndex()
    {
        for( int i = 0; i < words.Length; ++i )
        {
            if (words[i] != null && words[i].GetComponent<DragObject>().dragging)
            {
                return i;
            }
        }
        return -1;
    }

    public void DestroyDragged()
    {
        foreach (GameObject word in words)
        {
            if (word != null && word.GetComponent<DragObject>().dragging)
            {
                DragObject.dragOne = false;
                Destroy(word);
                break;
            }
        }
    }

    public bool IsFinished()
    {
        foreach( GameObject word in words )
        {
            if( word == null || word.active == false )
            {
                continue;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    public void SetDescription( string _str )
    {
        descriptionBox.SetActive(true);
        descriptionBox.GetComponentInChildren<Text>().text = _str;
    }

    //Turn the names off or on. Boolean array should be the same size as the number of names.
    public void SetNames( bool[] _b )
    {
        if (_b.Length == shieldText.Length)
        {
            for (int i = 0; i < shieldText.Length; ++i)
            {
                shieldText[i].gameObject.SetActive(_b[i]);
            }
        }
        else
            Debug.LogError("Two arrays are a different size");
    }

    public void OnNextClicked()
    {
        nextClicked = true;
    }

    public bool WasNextClicked()
    {
        if(nextClicked == true )
        {
            nextClicked = false;
            return true;
        }
        return false;
    }
}
