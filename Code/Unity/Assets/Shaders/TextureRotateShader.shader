// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "TextureRotateShader" {

Properties {

    _MainTex ("Texture", 2D) = ""

    [HideInInspector] _2DRotationMatrix ("2D Rotation Matrix", Vector) = (1, 0, 0, 0)

}

Subshader {

    Pass {

        CGPROGRAM

        #pragma vertex vert

        #pragma fragment frag

        struct v2f {

            float4 position : SV_POSITION;

            float2 uv_mainTex : TEXCOORD;

        };

       

        uniform float4 _MainTex_ST, _2DRotationMatrix;

        v2f vert(float4 position : POSITION, float2 uv : TEXCOORD0) {

            v2f o;

            o.position = UnityObjectToClipPos(position);

           

            float2x2 rotation = float2x2(

                _2DRotationMatrix.x, -_2DRotationMatrix.y,

                _2DRotationMatrix.y, _2DRotationMatrix.x

            );

            float2 center = _2DRotationMatrix.zw * _MainTex_ST.xy;

            o.uv_mainTex = mul(rotation, uv * _MainTex_ST.xy - center)

                + center + _MainTex_ST.zw;

           

            return o;

        }

       

        uniform sampler2D _MainTex;

        fixed4 frag(float2 uv_mainTex : TEXCOORD) : COLOR {

            return tex2D(_MainTex, uv_mainTex);

        }

        ENDCG

    }

Fallback "VertexLit"
}