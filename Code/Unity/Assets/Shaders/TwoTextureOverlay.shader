Shader "Custom/TwoTextureOverlay" {

Properties {
    _Color ("Color", Color) = (1,1,1,0)
    _Default("Default Color", Color) = (1,1,1,0)
    _Blend ("Blend", Range (0,1)) = 1
    _MainTex ("Main Texture", 2D) = "white" 
    _PathTex ("Path Texture (RGBA)", 2D) = "white"

}

Category {
    Material {
        Ambient[_Color]
        Diffuse[_Color]
    }
	
    ZWrite On
    Alphatest Greater 0.9
    Cull Off
    SubShader {
        Pass {
        	Blend SrcAlpha OneMinusSrcAlpha
        	SetTexture[_MainTex]
        }
        Pass {
            Lighting On
            Blend DstColor SrcColor

        }
        Pass {
        	Blend SrcAlpha OneMinusSrcAlpha
        	
        	SetTexture[_PathTex] {
        		
        		constantColor[_Color]
  
        	}
        }
    }
}

}